<?php

/**
 * Helper for view
 *
 * @package    App\Helpers
 * @subpackage Helper
 * @copyright  Copyright (c) 2018 RiverCrane! Corporation. All Rights Reserved.
 * @author     Truong Nghia<truong.van.nghia@rivercrane.vn>
 */

namespace App\Helpers;

use App\Models\Backend\MstTantou;
use App\Models\Backend\MstController;

class Helper
{
    /**
     * Check permission in view show menu.
     *
     * @param  $controller
     * @param  $action
     * @return boolean
     */
    public static function checkPermission($controller)
    {
        $modelTantou = new MstTantou();
        $dataCheck = $modelTantou->getDataCheckPermissionByKey(
            [
                'mst_controller.screen_class' => $controller,
                'mst_tantou.tantou_code' => \Auth::user()->tantou_code
            ]
        );

        if (!empty($dataCheck)) {
            switch ($dataCheck->permission) {
                case 0:
                    return false;
                    break;

                case 1:
                    if ($dataCheck->no_list === 1) {
                        return false;
                    }
                    return true;
                    break;

                case 2:
                    return true;
                    break;

                default:
                    break;
            }
        } else {
            $modelC = new MstController();
            if (!empty($modelC->getDataCheckAccess($controller))) {
                return true;
            }
            return false;
        }
    }

    /**
     * Combine keys and values
     *
     * @param  array   $keys
     * @param  array   $values
     * @return array
     */
    public static function combineColumn($keys, $values)
    {
        $data = [];
        foreach ($keys as $key) {
            $data[$key] = isset($values[$key])?$values[$key]:null;
        }
        return $data;
    }

    /**
     * Get value from object xml
     *
     * @param type $object
     * @param type $defaultValue
     */
    public static function val($object, $defaultValue = null)
    {
        if (isset($object)) {
            if ((string)$object === "") {
                return $defaultValue;
            } else {
                return (string)$object;
            }
        } else {
            return $defaultValue;
        }
    }
}
