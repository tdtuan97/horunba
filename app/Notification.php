<?php

/**
 * Set channel for slack.
 *
 * @package App
 * @subpackage Notification
 * @author Truong Nghia<truong.van.nghia@rivercrane.vn>
 * @copyright Copyright (c) 2017 RiverCrane! Corporation. All Rights Reserved.
 */

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Notification extends Authenticatable
{
    use Notifiable;

    public $channel;

    public function __construct($channel)
    {
        $this->channel = $channel;
    }
    /**
     * Route notifications for the Slack channel.
     *
     * @return string
     */
    public function routeNotificationForSlack()
    {
        return $this->channel;
    }
}
