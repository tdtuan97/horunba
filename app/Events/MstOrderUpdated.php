<?php

namespace App\Events;

use App\Models\Backend\MstOrder;
use App\Models\Backend\LogUpdateStatusOrder;
use Illuminate\Queue\SerializesModels;

class MstOrderUpdated
{
    use SerializesModels;
    
    public function __construct(MstOrder $order)
    {
        if ($order->isDirty('order_status') || $order->isDirty('order_sub_status')) {
            $modelLog  = new LogUpdateStatusOrder();
            $className = '';
            $backtrack = debug_backtrace(false, 10);
            foreach ($backtrack as $item) {
                if (isset($item['class'])) {
                    if (strpos($item['class'], 'Controller') !== false) {
                        $className = $item['class'];
                        $function  = '';
                        if (isset($item['function'])) {
                            $function = '@' . $item['function'];
                        }
                        $action = $className . $function;
                        break;
                    }
                }
            }
            $modelLog->insertLog([$order->receive_id], $order->toArray(), $action);
        }
    }
}
