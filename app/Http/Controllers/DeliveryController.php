<?php
/**
 * Controller for store order
 *
 * @package    App\Http\Controllers
 * @subpackage DeliveryController
 * @copyright  Copyright (c) 2018 RiverCrane! Corporation. All Rights Reserved.
 * @author     Le Hung<le.hung.rcvn2012@gmail.com>
 */

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Backend\DtDelivery;
use App\Models\Backend\DtDeliveryDetail;
use App\Models\Backend\MstOrder;
use App\Models\Backend\MstMall;
use Validator;
use Auth;
use Config;
use DB;

class DeliveryController extends Controller
{
    /**
     * Load view.
     */
    public function index()
    {
        return view('delivery.index');
    }

    /**
     * Get all data.
     * Return $object json
     */
    public function dataList(Request $request)
    {
        if ($request->ajax() === true) {
            $modelDtDelivery = new DtDelivery();

            $statusConf = Config::get('common.delivery_status');
            $statusOpt  = array();
            foreach ($statusConf as $key => $value) {
                $statusOpt[] = ['key' => $key, 'value' => $value];
            }

            $deliveryOptions = array();
            foreach ($statusConf as $key => $value) {
                $deliveryOptions[] = ['key' => $key, 'value' => $value];
            }

            $arrSort = [
                'received_order_id'     => $request->input('sort_received_order_id', null),
                'name_jp'               => $request->input('sort_name_jp', null),
                'delivery_date'         => $request->input('sort_delivery_date', null),
                'company_name'          => $request->input('sort_company_name', null),
                'subdivision_num'       => $request->input('sort_subdivision_num', null),
                'inquiry_no'            => $request->input('sort_inquiry_no', null),
                'delivery_date'         => $request->input('sort_delivery_date', null),
                'delivery_plan_date'    => $request->input('sort_delivery_plan_date', null) ,
                'delivery_time'         => $request->input('sort_delivery_time', null),
            ];
            $rule = [
                'order_id_from'           => 'nullable|numeric',
                'order_id_to'             => 'nullable|numeric',
                'delivery_date'           => 'nullable|date',
                'delivery_plan_date_from' => 'nullable|date',
                'delivery_plan_date_to'   => 'nullable|date'
            ];
            $arrSearch = [
                'name_jp'               => $request->input('name_jp', null),
                'delivery_date'         => $request->input('delivery_date', null),
                'received_order_id'     => $request->input('received_order_id', null),
                'delivery_plan_date_from'=> $request->input('delivery_plan_date_from', null),
                'delivery_plan_date_to' => $request->input('delivery_plan_date_to', null),
                'error_message'         => $request->input('error_message', null),
                'delivery_status'       => $request->input('delivery_status', null),
                'per_page'              => $request->input('per_page', null)
            ];
            $modelMall = new MstMall();
            $mallData  = $modelMall->getDataSelectBox();
            $shipWishDateOpt = Config::get('common.ship_wish_time');
            $validator = Validator::make($arrSearch, $rule);
            if ($validator->fails()) {
                $data = [];
            } else {
                $data = $modelDtDelivery->getData($arrSearch, $arrSort);
            }
            return response()->json([
                'data'      => $data,
                'statusOpt' => $deliveryOptions,
                'mall_data' => $mallData,
                'shipWishDateOpt' => $shipWishDateOpt
            ]);
        }
    }
    /**
     * Get form data
     *
     * @param   $request  Request
     * @return  json
     */
    public function getFormData(Request $request)
    {
        if ($request->ajax() === true) {
            $modelDtDelivery    = new DtDelivery();
            $data               = $modelDtDelivery->getItem([
                'received_order_id'  => $request->input('received_order_id', null),
                'subdivision_num'    => $request->input('subdivision_num', null)
            ]);
            $dataGroup               = $modelDtDelivery->getItemGroup([
                'dt_delivery.received_order_id'     => $request->input('received_order_id', null),
                'dt_delivery.subdivision_num'       => $request->input('subdivision_num', null)
            ]);
            $deliveryCodeOpt    = [['key' => '_none', 'value' => '-----']];
            $deliveryCodeCommon = Config::get('common.delivery_code');
            foreach ($deliveryCodeCommon as $key => $item) {
                $deliveryCodeOpt[] = [
                    'key'   => $key,
                    'value' => $item,
                ];
            }

            $deliveryTimeOpt    = [['key' => '_none', 'value' => '-----']];
            $deliveryTimeCommon = Config::get('common.delivery_time');
            foreach ($deliveryTimeCommon as $key => $item) {
                $deliveryTimeOpt[] = [
                    'key'   => $key,
                    'value' => $item,
                ];
            }

            $modelMstOrder    = new MstOrder();
            $deliDays         = $modelMstOrder->getDeliDays([
                'received_order_id'  => $request->input('received_order_id', null)
            ]);
            if (!empty($deliDays)) {
                $deliDays = $deliDays->deli_days;
            } else {
                $deliDays = 0;
            }

            return response()->json([
                    'data'            => $data,
                    'dataGroup'       => $dataGroup,
                    'deliveryCodeOpt' => $deliveryCodeOpt,
                    'deliveryTimeOpt' => $deliveryTimeOpt,
                    'deliDays'        => $deliDays
            ]);
        }
    }
    /**
     * Save order status settings
     *
     * @param   $request  Request
     * @return json
     */
    public function save(Request $request)
    {
        if ($request->ajax() === true) {
            if ($request->input('delivery_status') !== 9
                && $request->input('delivery_status') !== 7
                && $request->input('delivery_status') !== 5
                && $request->input('delivery_status') !== 0
            ) {
                return response()->json([
                    'status'  => 0,
                    'method'  => 'save',
                    'message' => 'Can not update item, status is wrong.'
                ]);
            }
            $rules = [
                'received_order_id'    => 'required|exists:horunba.dt_delivery,received_order_id',
                'subdivision_num'      => 'required|numeric',
                'delivery_postal'      => 'required|max:7',
                'delivery_perfecture'  => 'required',
                'delivery_city'        => 'required',
                'delivery_sub_address' => 'required',
                'delivery_tel'         => 'required',
                'delivery_code'        => 'required|in:3,4',
            ];
            $validator = Validator::make($request->all(), $rules);
            if ($validator->fails()) {
                return response()->json([
                    'status'  => 0,
                    'method'  => 'save',
                    'message' => $validator->errors()
                ]);
            } else {
                $deliveryPlanDate = $request->input('delivery_plan_date', null);
                $deliveryDate = $request->input('delivery_date', null);
                $deliveryTime = $request->input('delivery_time', null);
                if (!empty($deliveryPlanDate)) {
                    $cal = strtotime(date('Y-m-d', strtotime($deliveryPlanDate)));
                    $calCurrent = strtotime(date('Y-m-d'));
                    if ($cal === $calCurrent && time() > strtotime('15:00')) {
                        return response()->json([
                            'status'  => 0,
                            'method'  => 'save',
                            'message' => '※15:00以降ので、出荷予定日が明日に更新してください。'
                        ]);
                    } elseif ($cal < $calCurrent) {
                        return response()->json([
                            'status'  => 0,
                            'method'  => 'save',
                            'message' => ['delivery_plan_date' => '※出荷予定日'
                                . 'が間に合わないになります。']
                        ]);
                    }
                    if (!empty($deliveryDate)) {
                        $modelMstOrder    = new MstOrder();
                        $deliDays = $modelMstOrder->getDeliDays([
                            'received_order_id'  => $request->input('received_order_id', null)
                        ]);
                        if (!empty($deliDays)) {
                            $deliDays = $deliDays->deli_days;
                        } else {
                            $deliDays = 0;
                        }
                        if ((!empty($deliveryTime) && $deliveryTime === '01')) {
                               $deliDays = $deliDays + 1;
                        }
                        if ($cal >= $calCurrent) {
                            $datePlus = strtotime(date('Y-m-d', strtotime($deliveryPlanDate . '+' . $deliDays . 'days')));
                            if ($datePlus > strtotime(date('Y-m-d', strtotime($deliveryDate)))) {
                                return response()->json([
                                    'status'  => 0,
                                    'method'  => 'save',
                                    'message' => '※出荷予定日'
                                    . 'が間に合わないになります。'
                                ]);
                            }
                        }
                    }
                } else {
                    return response()->json([
                        'status'  => 0,
                        'method'  => 'save',
                        'message' => ['delivery_plan_date' => '※出荷予定日'
                            . 'が間に合わないになります。']
                    ]);
                }
            }
            $deliveryStatus = $request->input('delivery_status', null);

            $data= [
                'delivery_postal'       => $request->input('delivery_postal', 0),
                'shipping_instructions' => $request->input('shipping_instructions', null),
                'delivery_city'         => $request->input('delivery_city', null),
                'delivery_tel'          => $request->input('delivery_tel', null),
                'delivery_sub_address'  => $request->input('delivery_sub_address', null),
                'delivery_perfecture'   => $request->input('delivery_perfecture', null),
                'delivery_plan_date'    => $request->input('delivery_plan_date', null),
                'delivery_code'         => $request->input('delivery_code', null),
                'delivery_time'         => $request->input('delivery_time', null),
                'delivery_date'         => $request->input('delivery_date', null),
                'error_message'         => $request->input('error_message', null),
                'up_date'               => date('Y-m-d H:m:s'),
                'up_ope_cd'             => Auth::user()->tantou_code

            ];
            $data['delivery_sub_address'] = empty($data['delivery_sub_address']) ? '' : $data['delivery_sub_address'];
            $dataDetail= [
                'up_date'               => date('Y-m-d H:m:s'),
                'up_ope_cd'             => Auth::user()->tantou_code

            ];
            if ((int)$deliveryStatus === 9 || (int)$deliveryStatus === 5) {
                $data['delivery_status'] = 7;
            }
            if (!empty($data['delivery_plan_date'])) {
                $data['delivery_plan_date'] = date('Ymd', strtotime($data['delivery_plan_date']));
            }
            if (!empty($data['delivery_date'])) {
                $data['delivery_date'] = date('Ymd', strtotime($data['delivery_date']));
            }

            // Update DtDelivery
            $modelDtDelivery = new DtDelivery();
            $modelDtDelivery->updateData(
                [
                    'received_order_id' => $request->input('received_order_id', null),
                    'subdivision_num' => $request->input('subdivision_num', null)
                ],
                $data
            );
            // Update DtDeliveryDetail
            $modelDtDetailDelivery = new DtDeliveryDetail();
            $modelDtDetailDelivery->updateData(
                [
                    'received_order_id' => $request->input('received_order_id', null),
                    'subdivision_num'   => $request->input('subdivision_num', null)
                ],
                $dataDetail
            );
            return response()->json([
                'status'  => 1,
                'method'  => 'save',
                'message' => 'Update delivery item successfully.'
            ]);
        }
        return view('delivery.save');
    }

    /**
     * Check validate date
     *
     * @param   $request  Request
     * @return json
     */
    public function checkValidate(Request $request)
    {
        if ($request->ajax() === true) {
            $deliveryPlanDate = $request->input('delivery_plan_date', null);
            $deliveryDate     = $request->input('delivery_date', null);
            $action           = $request->input('action', null);
            $deliveryTime     = $request->input('delivery_time', null);
            $arrError         = [];
            $modelMstOrder    = new MstOrder();
            $deliDays         = $modelMstOrder->getDeliDays([
                'received_order_id'  => $request->input('received_order_id', null)
            ]);
            if (!empty($deliDays)) {
                $deliDays = $deliDays->deli_days;
            } else {
                $deliDays = 0;
            }
            if ((!empty($deliveryTime) && $deliveryTime === '01')) {
                 $deliDays = $deliDays + 1;
            }

            $calCurrent = strtotime(date('Y-m-d'));
            if ($action === 'delivery_date' && !empty($deliveryDate)) {
                $deliveryPlanDate = date('Y-m-d', strtotime($deliveryDate . '-' . $deliDays . 'days'));
            }

            if (!empty($deliveryPlanDate)) {
                $cal = strtotime(date('Y-m-d', strtotime($deliveryPlanDate)));
                if ($cal >= $calCurrent) {
                    $datePlanPlus = strtotime(date('Y-m-d', strtotime($deliveryPlanDate . '+' . $deliDays . 'days')));
                    if (!empty($deliveryDate)
                        && $datePlanPlus > strtotime(date('Y-m-d', strtotime($deliveryDate)))) {
                        $arrError['delivery_plan_date'] = "※出荷手続がかかりますので、出荷予定日の{$deliveryPlanDate} + 出荷期間の{$deliDays}日が間に合わないになります。";
                    }
                } else {
                    $arrError['delivery_plan_date'] = "※出荷予定日は本日からしてください。";
                }
                if ($cal === $calCurrent && time() > strtotime('15:00')) {
                    $arrError['delivery_plan_date'] ='※15:00以降ので、出荷予定日が明日に更新してください。';
                }
            } else {
                $arrError['delivery_plan_date'] = "※出荷予定日は本日からしてください。";
            }
            return response()->json([
                'message'            => $arrError,
                'delivery_plan_date' => $deliveryPlanDate
            ]);
        }
    }
}
