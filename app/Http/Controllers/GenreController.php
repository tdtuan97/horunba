<?php
/**
 * Controller for genre management
 *
 * @package    App\Http\Controllers
 * @subpackage GenreController
 * @copyright  Copyright (c) 2018 RiverCrane! Corporation. All Rights Reserved.
 * @author     Van Qui<van.qui@rivercrane.vn>
 */

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Common;
use App\Http\Controllers\Controller;
use Validator;
use Auth;
use App\Models\Backend\MstGenre;
use Barryvdh\Debugbar\Facade as Debugbar;

class GenreController extends Controller
{
    /**
     * Load view.
     */
    public function index()
    {
        return view('genre.index');
    }

    /**
     * Get all data.
     *
     * @param $request Request
     * @return json
     */
    public function dataList(Request $request)
    {
        if ($request->ajax() === true) {
            $modelGenre = new MstGenre();
            $arrSort = [
                'genre_name' => $request->input('sort_genre_name', null),
                'remarks'    => $request->input('sort_remarks', null)
            ];
            
            $arrSearch = [
                'genre_name' => $request->input('genre_name', null),
                'remarks'    => $request->input('remarks', null),
                'per_page'   => $request->input('per_page', null)
            ];
            $data = $modelGenre->getData($arrSearch, $arrSort);
            return response()->json([
                'data' => $data
            ]);
        }
    }
    
    /**
     * Save data
     *
     * @param  $request Request
     * @return json
     */
    public function save(Request $request)
    {
        if ($request->ajax() === true) {
            $method = $request->input('method', null);
            if (!in_array($method, ['add', 'edit'])) {
                return response()->json([
                    'status'  => 0,
                    'message' => 'Method not valid'
                ]);
            }
            
            $rules = [
                'genre_name' => 'required'
            ];
            if ($method === 'edit') {
                $rules['genre_id'] = 'required|numeric|exists:horunba.mst_genre,genre_id';
            }
            
            $validator = Validator::make($request->all(), $rules);
            if ($validator->fails()) {
                return response()->json([
                    'status'  => 0,
                    'message' => $validator->errors()
                ]);
            }
            
            $genreId   = $request->input('genre_id', null);
            $genreName = $request->input('genre_name', null);
            $remarks   = $request->input('remarks', null);
            
            try {
                $modelGenre = new MstGenre();
                if ($method === 'edit') {
                    $modelGenre = $modelGenre->find($genreId);
                } else {
                    $modelGenre->in_ope_cd = Auth::user()->tantou_code;
                    $modelGenre->in_date   = date('Y-m-d H:i:s');
                    $modelGenre->del_flg   = 0;
                }
                $modelGenre->up_ope_cd  = Auth::user()->tantou_code;
                $modelGenre->up_date    = date('Y-m-d H:i:s');
                $modelGenre->genre_name = $genreName;
                $modelGenre->remarks    = $remarks;
                $modelGenre->save();
                return response()->json([
                    'status'  => 1,
                    'message' => 'Save genre success'
                ]);
            } catch (\Exception $e) {
                $message = $e->getMessage();
                return response()->json([
                    'status'  => 0,
                    'message' => $message
                ]);
            }
        }
    }
    
    /**
     * Delete data
     *
     * @param  $request Request
     * @return json
     */
    public function delete(Request $request)
    {
        if ($request->ajax() === true) {
            $rules = [
                'genre_id'   => 'required|numeric|exists:horunba.mst_genre,genre_id'
            ];
            $validator = Validator::make($request->all(), $rules);
            if ($validator->fails()) {
                $errors = $validator->errors();
                return response()->json([
                    'status'  => 0,
                    'message' => $errors->all('<p>:message</p>')
                ]);
            }
            
            try {
                $modelGenre = new MstGenre();
                $modelGenre = $modelGenre->find($request->genre_id);
                $modelGenre->del_flg = 1;
                $modelGenre->up_ope_cd  = Auth::user()->tantou_code;
                $modelGenre->up_date    = date('Y-m-d H:i:s');
                $modelGenre->save();
                return response()->json([
                    'status'  => 1,
                    'message' => "Delete genre {$request->genre_id} success"
                ]);
            } catch (\Exception $e) {
                $message = $e->getMessage();
                return response()->json([
                    'status'  => 0,
                    'message' => $message
                ]);
            }
        }
    }
}
