<?php
/**
 * Controller for not permission
 *
 * @package    App\Http\Controllers
 * @subpackage NotPermissionController
 * @copyright  Copyright (c) 2019 RiverCrane! Corporation. All Rights Reserved.
 * @author     Truong Nghia<truong.van.nghia@rivercrane.vn>
 */

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use Auth;

class NotPermissionController extends Controller
{
    /**
     * Load view.
     */
    public function index()
    {
        return view('permission.not-permission');
    }
}
