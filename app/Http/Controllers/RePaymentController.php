<?php
/**
 * Controller for re-payment management
 *
 * @package    App\Http\Controllers
 * @subpackage Controllers
 * @copyright  Copyright (c) 2018 RiverCrane! Corporation. All Rights Reserved.
 * @author     le.hung<le.hung@rivercrane.vn>
 */

namespace App\Http\Controllers;

use Barryvdh\Debugbar\Facade as Debugbar;
use Illuminate\Http\Request;
use App\Http\Controllers\Common;
use App\Http\Controllers\Controller;
use App\Models\Backend\DtRePayment;
use App\Models\Backend\DtOrderProductDetail;
use App\Models\Backend\MstOrder;
use Validator;
use Storage;
use Auth;
use DB;
use Config;

class RePaymentController extends Controller
{
    /**
     * Load view.
     */
    public function index()
    {
        return view('repayment.index');
    }

    /**
     * Get all data.
     * Return $object json
     */
    public function dataList(Request $request)
    {
        if ($request->ajax() === true) {
            $modelDtRePayment      = new DtRePayment();
            $arrSort = [
                'repay_no'              => $request->input('sort_repay_no', null),
                'repay_name'            => $request->input('sort_repay_name', null),
                'repayment_date'        => $request->input('sort_repay_date', null),
                'repayment_date'        => $request->input('sort_repay_time', null),
                'repay_bank_name'       => $request->input('sort_repay_bank_name', null),
                'full_name'             => $request->input('sort_customer_name', null),
                'repay_bank_branch_name'=> $request->input('sort_repay_bank_branch_name', null),
                'repay_bank_account'    => $request->input('sort_repay_bank_account', null),
                'total_return_price'    => $request->input('sort_total_return_price', null),
                'request_price'         => $request->input('sort_request_price', null),
                'receive_id'            => $request->input('sort_receive_id', null),
                'repayment_status'      => $request->input('sort_repayment_status', null)
            ];
            $arrSearch = [
                'per_page'                  => $request->input('per_page', null),
                'repay_date_from'           => $request->input('repay_date_from', null),
                'repay_date_to'             => $request->input('repay_date_to', null),
                'repay_name'                => $request->input('repay_name', null),
                'repay_bank_name'           => $request->input('repay_bank_name', null),
                'repay_bank_branch_name'    => $request->input('repay_bank_branch_name', null),
                'repay_bank_account'        => $request->input('repay_bank_account', null),
                'repayment_status'          => $request->input('repayment_status', null),
                'received_order_id'         => $request->input('received_order_id', null)
            ];
            $type       = $request->input('type', null);
            if (!is_null($type)) {
                $data = $modelDtRePayment->getData($arrSearch, $arrSort, ['type' => $type]);
            } else {
                $data = $modelDtRePayment->getData($arrSearch, $arrSort);
            }
            $arrIsReturn = Config::get('common.is_return');
            return response()->json([
                'data'          => $data,
                'arrIsReturn'   => $arrIsReturn,
            ]);
        }
    }
    /**
 * Get form data
 *
 * @param   $request  Request
 * @return  json
 */
    public function getFormData(Request $request)
    {
        if ($request->ajax() === true) {
            $DtRePayment    = new DtRePayment();
            $MstOrder       = new MstOrder();
            $data = array();
            if ($request->get('repay_no') !== null) {
                $data = $DtRePayment->getRePaymentById($request->get('repay_no'));
            }
            $isReturn = [['key' => '', 'value' => '-----']];
            foreach (Config::get('common.repayment_is_return') as $key => $item) {
                $isReturn[] = ['key' => $key, 'value' => $item];
            }
            if (empty($data)) {
                return response()->json([
                    'data'    => $data,
                    'isReturn' => $isReturn
                ]);
            }
            $dataOrder = $MstOrder->getItemsForRePayMent(
                [
                    'repay_no' => $request->get('repay_no'),
                    'received_order_id' => $data->received_order_id
                ],
                true
            );

            return response()->json([
                'data'    => $data,
                'dataOrder'    => $dataOrder,
                'isReturn' => $isReturn
            ]);
        }
    }
    /**
     * Get info data
     *
     * @param   $request  Request
     * @return  json
     */
    public function getInfoOrderData(Request $request)
    {
        if ($request->ajax() === true) {
            $DtRePayment    = new DtRePayment();
            $MstOrder       = new MstOrder();
            $data = array();
            $dataOrder = array();
            if ($request->get('received_order_id') !== null) {
                // Validator::extend('checkHasOrder', function ($parameters, $value, $attribute) use ($MstOrder) {
                //     $checkData = $MstOrder->getItemsForRePayMent([$parameters => $value]);
                //     if (count($checkData) === 0) {
                //         return false;
                //     }
                //     return true;
                // }, trans('validation.check_order_product'));
                Validator::extend(
                    'checkNotHasReceiveId',
                    function ($parameters, $value, $attribute) use ($DtRePayment) {
                        $check = $DtRePayment->checkExistByReceivedOrderId($value);
                        if (!$check) {
                            return true;
                        }
                        return false;
                    },
                    trans('validation.check_not_has_receive_id')
                );
                $addRule = '|checkNotHasReceiveId';
                $rules = [
                    'received_order_id' => 'required|exists:mst_order,received_order_id' . $addRule,
                ];
                $validator = Validator::make($request->all(), $rules);
                if ($validator->fails()) {
                    return response()->json([
                        'status'  => 0,
                        'method'  => 'save',
                        'message' => $validator->errors()
                    ]);
                }
                $data = $MstOrder->getInfoForRepaymentById($request->get('received_order_id'));
                $dataOrder = $MstOrder->getItemsForRePayMent($request->all());
                // if (count($dataOrder) === 0) {
                //     $data = [];
                // }
            }
            return response()->json([
                    'dataInfo'    => $data,
                    'dataOrder'    => $dataOrder,
            ]);
        }
    }
    /**
     * Submit save form data
     *
     * @param   $request  Request
     * @return  json
     */
    public function save(Request $request)
    {
        $DtRePayment    = new DtRePayment();
        $MstOrder       = new MstOrder();
        if ($request->ajax() === true) {
            $params = $request->all();
            Validator::extend('checkHasOrder', function ($parameters, $value, $attribute) use ($MstOrder) {
                $checkData = $MstOrder->getItemsForRePayMent([$parameters => $value]);
                if (count($checkData) === 0) {
                    return false;
                }
                return true;
            }, trans('validation.check_order_product'));

            Validator::extend('checkNotHasReceiveId', function ($parameters, $value, $attribute) use ($DtRePayment) {
                $check = $DtRePayment->checkExistByReceivedOrderId($value);
                if (!$check) {
                    return true;
                }
                return false;
            }, trans('validation.check_not_has_receive_id'));
            $addRule = '';
            if (empty($request->input('repay_no', ''))) {
                $addRule = '|checkNotHasReceiveId';
            }

            $keyIsReturn = implode(",", array_keys(Config::get('common.repayment_is_return')));
            $rules = [
                'received_order_id'     => 'required|exists:mst_order,received_order_id' . $addRule,
                'repay_bank_name'       => 'required',
                'repay_bank_branch_name'=> 'required',
                'repay_bank_account'    => 'required',
                'repay_name'            => 'required',
                'repay_price'           => 'required|numeric|min:0',
                'return_point'          => 'nullable|numeric|min:0',
                'is_return'             => 'required|in:' . $keyIsReturn,
                'repay_coupon'         => 'nullable|numeric',
                'other_commission'     => 'nullable|numeric',
                'debosit_commission'   => 'nullable|numeric',
                'total_return_price'   => 'nullable|numeric',
            ];

            $validator = Validator::make($request->all(), $rules);
            if ($validator->fails()) {
                return response()->json([
                    'status'  => 0,
                    'method'  => 'save',
                    'message' => $validator->errors()
                ]);
            }
            // Update sale_price for DtOrderProductDetail
            if (!empty($params)) {
                $DtOrderProductDetail = new DtOrderProductDetail();
                $where = [];
                $updateData = [];
                foreach ($params as $key => $val) {
                    if (strpos($key, 'sale_price-') !== false) {
                        $updateKeys = explode('-', $key);
                        $where['receive_id']     = $updateKeys[1];
                        $where['detail_line_num']= $updateKeys[2];
                        $where['sub_line_num']   = $updateKeys[3];
                        $updateData['sale_price']= $val;
                        $updateData['up_date']   = now();
                        $DtOrderProductDetail->updateData($where, $updateData);
                    }
                }
            }
            $repay_no = $request->input('repay_no', '');
            $currentDataEdit = '';
            if ($repay_no !== '') {
                $DtRePayment            = $DtRePayment->find($repay_no);
                $DtRePayment->up_ope_cd = Auth::user()->tantou_code;
                $DtRePayment->up_date   = date('Y-m-d H:i:s');
            } else {
                $DtRePayment->in_ope_cd = Auth::user()->tantou_code;
                $DtRePayment->up_ope_cd = Auth::user()->tantou_code;
                $DtRePayment->in_date   = date('Y-m-d H:i:s');
                $DtRePayment->up_date   = date('Y-m-d H:i:s');
            }
            $DtRePayment->receive_id             = $request->input('receive_id');
            $DtRePayment->repay_bank_code        = $request->input('repay_bank_code', '');
            $DtRePayment->repay_bank_name        = $request->input('repay_bank_name', '');
            $DtRePayment->repay_bank_branch_code = $request->input('repay_bank_branch_code', '');
            $DtRePayment->repay_bank_branch_name = $request->input('repay_bank_branch_name', '');
            $DtRePayment->repay_bank_account     = $request->input('repay_bank_account', '');
            $DtRePayment->repay_name             = $request->input('repay_name', '');
            $DtRePayment->repay_price            = $request->input('repay_price', '');
            $DtRePayment->repayment_type         = $request->input('repayment_type', '');
            $DtRePayment->return_point           = $request->input('return_point', '');
            $DtRePayment->is_return              = $request->input('is_return', '');
            if ((int) $request->input('repayment_type', '') === 1) {
                $DtRePayment->return_fee = $request->input('return_fee', '');
            } else {
                $DtRePayment->return_fee = 0;
            }
            $DtRePayment->return_coupon      = $request->input('repay_coupon', '');
            $DtRePayment->deposit_transfer_commission = $request->input('debosit_commission', '');
            $DtRePayment->other_commission   = $request->input('other_commission', '');
            $DtRePayment->total_return_price = $request->input('total_return_price', '');
            $DtRePayment->save();
            
            $orderReturnPrice = (int)$request->input('order_return_price', 0);
            $orderReturnPoint = (int)$request->input('order_return_point', 0);
            $orderReturnCoupon = (int)$request->input('order_return_coupon', 0);
            $orderData = $MstOrder->find($request->input('receive_id'));
            $orderData->return_price += $orderReturnPrice;
            $orderData->return_point += $orderReturnPoint;
            $orderData->return_coupon += $orderReturnCoupon;
            $orderData->save();
            return response()->json([
                'status'            => 1,
                'method'            => 'save'
            ]);
        }
        return view('repayment.save');
    }
    /**
     * Update more flags
     *
     * @param   $request  Request
     * @return json
     */
    public function changeFlag(Request $request)
    {
        $dtRePayment = new DtRePayment();
        if ($request->ajax() === true) {
            $params                 = $request->all();
            $params['data']['up_ope_cd']    = Auth::user()->tantou_code;
            $params['data']['up_date']      = date('Y-m-d H:i:s');
            $dataWhere              = ['repay_no' => $params['repay_no']];
            $dataUpdate             = [
                'repayment_status'  => $params['repayment_status'] === 1 ? 0 : 1,
                'repayment_date'    => $params['repayment_status'] === 1 ? null : date('Y-m-d H:i:s') ,
            ];
            $dtRePayment->updateData($dataWhere, $dataUpdate);
        }
        return response()->json([
            'status'            => 1,
            'method'            => 'save'
        ]);
    }
    /**
     * Process export data to csv
     *
     * @param   $request  Request
     * @return json
     */
    public function processExportCsv(Request $request)
    {
        $data = $this->dataList($request)->getData();
        Debugbar::info($request);
        $type       = $request->input('type', null);
        $fileName   = "";
        $column     = [];
        $column = [
            'repay_name'                => __('messages.repayment'),
            'total_return_price'        => __('messages.total_return_price_screen'),
            'repay_date'                => __('messages.repay_date'),
            'repay_time'                => __('messages.repay_time'),
            'rep_bank_name'             => __('messages.rep_bank_name'),
            'rep_bank_branch_name'      => __('messages.rep_bank_branch_name'),
            'rep_bank_account'          => __('messages.rep_bank_account'),
            'customer_name'             => __('messages.customer_name'),
            'rep_name'                  => __('messages.rep_name'),
            'request_price_2'           => __('messages.request_price_2'),
            'repayment_receive_id'      => __('messages.repayment_receive_id'),
            'status'                    => __('messages.repayment_status'),
            'is_return'                 => __('messages.is_return'),
            'in_date'                   => __('messages.repayment_in_date'),
            'name_jp'                   => __('messages.mall_name'),
            'received_order_id'         => __('messages.received_order_id'),
        ];
        $arrIsReturn = Config::get('common.is_return');
        $fileName   = 'Repayment-' . date('YmdHis') . ".csv";
        Storage::disk('local')->put($fileName, '');
        $url = Storage::disk('local')->getDriver()->getAdapter()->getPathPrefix() . $fileName;
        $file = fopen($url, 'a');
        fputs($file, mb_convert_encoding(implode(",", $column), 'Shift-JIS', 'UTF-8'). "\r\n");
        if (isset($data->data)) {
            if (count($data->data) > 0) {
                foreach ($data->data as $key => $value) {
                    $arrTemp = [];
                    $date = explode(" ", $value->repayment_date);
                    $arrTemp[] = $value->repay_name;
                    $arrTemp[] = $value->total_return_price;
                    $arrTemp[] = current($date);
                    $arrTemp[] = end($date);
                    $arrTemp[] = $value->repay_bank_name;
                    $arrTemp[] = $value->repay_bank_branch_name;
                    $arrTemp[] = $value->repay_bank_account;
                    $arrTemp[] = $value->full_name;
                    $arrTemp[] = '';
                    $arrTemp[] = $value->request_price;
                    $arrTemp[] = $value->receive_id;
                    $arrTemp[] = $value->repayment_status === 0 ? '未':'済';
                    $arrTemp[] = is_null($value->is_return) ? '' : $arrIsReturn[(int)$value->is_return];
                    $arrTemp[] = date("Y/m/d", strtotime($value->in_date));
                    $arrTemp[] = $value->name_jp;
                    $arrTemp[] = $value->received_order_id;

                    $filter     = array("\r\n", "\n", "\r");
                    $contentCsv = implode(",", $arrTemp);
                    $contentCsv = str_replace($filter, '', $contentCsv);
                    fputs($file, mb_convert_encoding($contentCsv, 'Shift-JIS', 'UTF-8'). "\r\n");
                }
            }
        }
        fclose($file);
        return response()->json([
            'file_name' => $fileName,
            'status'     => 1,
        ]);
    }
}
