<?php
/**
 * Return Management Controller
 *
 * @package     App\Controllers
 * @subpackage  ReturnManagementController
 * @copyright   Copyright (c) 2018 RiverCrane! Corporation. All Rights Reserved.
 * @author      van.qui<van.qui.rcvn2012@gmail.com>
 */

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Validator;
use App\User;
use Auth;
use DB;
use Config;
use Cache;
use App;
use Storage;
use App\Models\Backend\DtReturn;
use App\Models\Backend\MstReturnTypeLarge;
use App\Models\Backend\MstReturnTypeMid;
use App\Models\Backend\MstProduct;
use App\Models\Backend\MstProductSet;
use App\Models\Backend\MstOrder;
use App\Models\Backend\MstOrderDetail;
use App\Models\Backend\MstCustomer;
use App\Models\Backend\DtRePayment;
use App\Models\Backend\DtReorder;
use App\Models\Backend\DtOrderProductDetail;
use App\Models\Backend\TInsertShippingGroupKey;
use App\Models\Backend\TInsertShipping;
use App\Models\Backend\MstReturnAddress;
use App\Models\Backend\MstStockStatus;
use App\Models\Backend\DtOrderToSupplier;
use App\Models\Backend\DtDelivery;
use App\Models\Backend\DtDeliveryDetail;
use App\Models\Batches\DtReceiptLedger;
use App\Models\Backend\MstReturnDetail;
use App\Models\Backend\MstReturn;
use App\Models\Backend\DtReceiptLedger as BackendDtReceiptLedger;

class ReturnManagementController extends Controller
{
    /**
     * Show template for append data
     *
     * @param   $request  Request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        return view('return-management.index');
    }

    /**
     * Show data list view
     *
     * @param   $request  Request
     * @return json
     */
    public function dataList(Request $request)
    {
        $modelLarge       = new MstReturnTypeLarge();
        $modelMid         = new MstReturnTypeMid();
        $arrDefaultOption = array(array('key' => '_none', 'value' => '-----'));
        if ($request->ajax() === true) {
            $arraySearch    = array(
                'return_no'         => $request->input('return_no', null),
                'return_date_from'  => $request->input('return_date_from', null),
                'return_date_to'    => $request->input('return_date_to', null),
                'deje_no'           => $request->input('deje_no', null),
                'product_code_from' => $request->input('product_code_from', null),
                'product_code_to'   => $request->input('product_code_to', null),
                'supplier_nm'       => $request->input('supplier_name', null),
                'return_type_large_id'  => $request->input('return_type_large_id', null),
                'return_type_mid_id'    => $request->input('return_type_mid_id', null),
                'status'                => $request->input('return_status', null),
                'receive_instruction'   => $request->input('receive_instruction', null),
                'delivery_instruction'  => $request->input('delivery_instruction', null),
                'red_voucher'           => $request->input('red_voucher', null),
                'per_page'              => $request->input('per_page', null)
            );
            $arraySort = [
                'return_no' => $request->input('sort_return_no', null),
            ];
            $model   = new DtReturn();
            $rules = [
                'return_date_from' => 'date',
                'return_date_to'   => 'date'
            ];
            $validator = Validator::make($request->all(), $rules);
            if ($validator->fails()) {
                return response()->json([
                    'data'    => []
                ]);
            }
            foreach (Config::get("common.common_status") as $key => $value) {
                $arrCommonStatus[] = ['key' => $key, 'value' => $value];
            }

            $arrStatus              = Config::get("common.return_status");
            foreach ($arrStatus as $key => $value) {
                $arrStatusOption[] = ['key' => $key, 'value' => $value];
            }
            $datas = $model->getData($arraySearch, $arraySort)->toArray();
            $arrnLarge              = $modelLarge->getData();
            $arrMid                 = $modelMid->getData();
            $arrReceiveInstruction  = Config::get("common.receive_instruction");
            $arrDeliveryInstruction = Config::get("common.receive_instruction");
            $urlDeje = 'https://hrnb.diyfactory.jp/deje/save?deje_id=';
            if (App::environment(['local', 'test'])) {
                $urlDeje = 'https://hrnb.daitolab.com/deje/save?deje_id=';
            }
            return response()->json([
                'data'                   => $datas,
                'arrStatus'              => $arrStatus,
                'arrReceiveInstruction'  => $arrReceiveInstruction,
                'arrDeliveryInstruction' => $arrDeliveryInstruction,
                'arrStatusOption'        => $arrStatusOption,
                'arrCommonStatus'        => $arrCommonStatus,
                'arrLarge'               => $arrnLarge->toArray(),
                'arrMid'                 => $arrMid->toArray(),
                'urlDeje'                 => $urlDeje,
            ]);
        }
    }

    /**
     * Process add and edit return order
     *
     * @param   $request  Request
     * @return json
     */
    public function updateQuantity(Request $request)
    {
        if ((int)$request->input('updateQuantity') === 1) {
            $modelR = new DtReturn();
            $child = $request->input('child');
            if (!empty($child)) {
                $arrLineNo = array_column($child, 'return_line_no');
            } else {
                $arrLineNo = [$request->input('return_line_no')];
            }
            $dataCurrent = $modelR->getDataCurrentByCondition($request->input('return_no'), $arrLineNo);
            try {
                DB::beginTransaction();
                $flgCheck = false;
                $arrProductCheck = [];
                foreach ($dataCurrent as $item) {
                    $componentNum = empty($item->component_num) ? 1 : $item->component_num;
                    if ($item->return_type_large_id === 3 && $item->return_type_mid_id === 3) {
                        if (!empty($item->product_code)) {
                            $arrProductCheck[$item->product_code] = [
                                'product_code'   => $item->product_code,
                                'quantity'       => ($request->input('quantity') * $componentNum) - $item->return_quantity,
                                'return_line_no' => $item->return_line_no
                            ];
                        } else {
                            $arrProductCheck[$item->parent_product_code] = [
                                'product_code' => $item->parent_product_code,
                                'quantity'     => $request->input('quantity') - $item->return_quantity,
                                'return_line_no' => $item->return_line_no
                            ];
                        }
                    }
                    $modelR->updateData(
                        [
                            'return_no'      => $item->return_no,
                            'return_line_no' => $item->return_line_no,
                            'return_time'    => $item->return_time,
                        ],
                        [
                            'return_quantity' => $request->input('quantity') * $componentNum,
                            'error_code'      => null,
                        ]
                    );
                }
                if (count($arrProductCheck) !== 0) {
                    $dataUpdate = $arrProductCheck;
                    $dataProduct = $modelR->getDataCheckStockEdit($request->input('return_no'), $arrProductCheck);
                    foreach ($dataProduct as $value) {
                        $productCode = !empty($value->product_code) ? $value->product_code : $value->parent_product_code;
                        if (isset($arrProductCheck[$productCode])) {
                            $arrProductCheck[$productCode]['quantity'] += $value->return_quantity;
                        }
                    }
                    $modelSS = new MstStockStatus();
                    $arrKeyProduct = array_column($arrProductCheck, 'product_code');
                    $dataProductStock = $modelSS->getDataStockCheckReturn($arrKeyProduct)->keyBy(function ($item) {
                        return strtoupper($item['product_code']);
                    });
                    $flg = false;
                    foreach ($arrProductCheck as $key => $item) {
                        $key = strtoupper($item['product_code']);
                        if (isset($dataProductStock[$key]) && $dataProductStock[$key]->cal < $item['quantity']) {
                            $flg = true;
                            break;
                        }
                    }
                    if ($flg) {
                        DB::rollback();
                        return response()->json([
                            'status' => 0,
                            'messageErrorQuantity' => __('messages.message_stock_have_error'),
                        ]);
                    }

                }
                DB::commit();
            } catch (Exception $e) {
                DB::rollback();
                return response()->json([
                    'status' => 0,
                    'messageErrorQuantity' => $e->getMessage(),
                ]);
            }

            // $product_code = $request->input('return_time')
            // $quantity = $request->input('quantity');
            // if ($quantity !== null) {
            //     $status = DtReturn::where($where)
            //             ->update($data);
            //     return response()->json(
            //         ['status' => $status]
            //     );
            // }
        }
        return response()->json(
            ['status' => 1]
        );
    }
    /**
     * Process add and edit return order
     *
     * @param   $request  Request
     * @return json
     */
    public function save(Request $request)
    {
        // if ($request->ajax() === true) {
        //     $modelLarge       = new MstReturnTypeLarge();
        //     $modelMid         = new MstReturnTypeMid();
        //     $modelOD          = new MstOrderDetail();
        //     $modelOPD         = new DtOrderProductDetail();
        //     $modelReturn      = new DtReturn();
        //     $arrDefaultOption = array(array('key' => '_none', 'value' => '-----'));
        //     $dataLarge        = $modelLarge->getData();
        //     $dataMid          = $modelMid->getData();
        //     $returnNo         = $request->input('return_no', null);
        //     $productCode      = $request->input('product_code', null);
        //     $currentPro       = $request->input('parent_product_code', null);
        //     $receiveId        = $request->input('receive_id', null);
        //     $returnTime       = $request->input('return_time', null);
        //     if ($request->status === 'add' || $request->status === 'edit') {
        //         goto end;
        //     }
        //     unset($request['product_code']);
        //     $rules = [
        //         'return_quantity'      => 'nullable|numeric',
        //         'return_tanka'         => 'nullable|numeric',
        //         'parent_product_code'  => 'required',
        //         'return_type_mid_id'   => 'required',
        //         'return_type_large_id' => 'required',
        //     ];
        //     $validator = Validator::make($request->all(), $rules);
        //     if ($validator->fails()) {
        //         return response()->json([
        //             'status'  => 0,
        //             'message' => $validator->errors(),
        //             'data'    => []
        //         ]);
        //     } else {
        //         $modelProduct = new MstProduct();
        //         $check = $modelProduct->getDataProcessReturn($request->parent_product_code);
        //         if (count($check) === 0) {
        //             return response()->json([
        //                 'status'  => 0,
        //                 'message' => array('parent_product_code' => __('messages.product_not_exists')),
        //                 'data'    => []
        //             ]);
        //         }
        //     }
        //     $checkChild = false;
        //     $arrFieldDate = ['return_date', 'receive_plan_date', 'delivery_plan_date'];
        //     $arrMid = array_column($dataMid->toArray(), 'value', 'key');
        //     $count = $modelReturn->countOrderByReturnNo($returnNo, $productCode);
        //     if (!empty($returnNo)) {
        //         if ($count->parent_product_code === $currentPro) {
        //             $modelReturns = $modelReturn->getOrderByReturnNo($returnNo, $currentPro, $returnTime);
        //             foreach ($modelReturns as $item) {
        //                 $arrUpdate = [];
        //                 foreach ($request->all() as $key => $value) {
        //                     if ($value === 'null' || $value === 'undefined') {
        //                         $value = in_array($key, $arrFieldDate) ? null : '';
        //                     }
        //                     if ($key === 'return_type_mid_id') {
        //                         if (in_array((int)$value, [2, 3])) {
        //                             $arrUpdate['receive_instruction']  = 0;
        //                             $arrUpdate['delivery_instruction'] = 0;
        //                         } else {
        //                             $arrUpdate['delivery_instruction'] = -1;
        //                             if ((int)$value === 1) {
        //                                 $arrUpdate['receive_instruction'] = 0;
        //                             } else {
        //                                 $arrUpdate['receive_instruction'] = -1;
        //                             }
        //                         }
        //                     }
        //                     $arrUpdate[$key] = $value;
        //                 }
        //                 // if ($arrMid[$arrUpdate['return_type_mid_id']] !== '仕入先返品') {
        //                 //     $arrUpdate['return_postal']      = '';
        //                 //     $arrUpdate['return_pref']        = '';
        //                 //     $arrUpdate['return_sub_address'] = '';
        //                 //     $arrUpdate['return_name']        = '';
        //                 //     $arrUpdate['return_tel']         = '';
        //                 // }
        //                 $arrUpdate['up_ope_cd'] = Auth::user()->tantou_code;
        //                 $arrUpdate['up_date']   = date('Y-m-d H:i:s');

        //                 $modelReturn->where([
        //                     'return_no'      => $item->return_no,
        //                     'return_line_no' => $item->return_line_no,
        //                     'return_time'    => $item->return_time,
        //                 ])->update($arrUpdate);
        //             }
        //         } else {
        //             $modelReturn->where('return_no', $returnNo)
        //                         ->where('parent_product_code', $count->parent_product_code)
        //                         ->delete();
        //         }
        //     }
        //     if (empty($returnNo) || (!empty($returnNo) && $count->parent_product_code !== $currentPro)) {
        //         $productInfo = $modelOPD->getProByReceiveAndPro($receiveId, $currentPro);
        //         $arrInsert = [];
        //         $maxDataReturn = $modelReturn->getMaxDataByReceive($receiveId);
        //         $returnNo =0;
        //         $returnLine = 0;
        //         if (empty($maxDataReturn)) {
        //             $maxNo = $modelReturn->getMaxNo();
        //             if (empty($maxNo->maxNo)) {
        //                 $returnNo = '400000';
        //             } else {
        //                 $returnNo = (string)((int)$maxNo->maxNo + 1);
        //             }
        //         } else {
        //             $returnNo = $maxDataReturn->return_no;
        //             $returnLine = $maxDataReturn->return_line_no;
        //         }
        //         foreach ($productInfo as $k => $item) {
        //             $returnLine++;
        //             if (!empty($item->child_product_code)) {
        //                 $arrInsert[$k]['product_code'] = $item->child_product_code;
        //             }
        //             $arrInsert[$k]['return_no'] = $returnNo;
        //             $arrInsert[$k]['return_line_no'] = $returnLine;
        //             foreach ($request->all() as $key => $value) {
        //                 if ($value === 'null' || $value === 'undefined') {
        //                     $value = in_array($key, $arrFieldDate) ? null : '';
        //                 }
        //                 if ($key === 'return_type_mid_id') {
        //                     if (in_array((int)$value, [2, 3])) {
        //                         $arrInsert[$k]['receive_instruction']  = 0;
        //                         $arrInsert[$k]['delivery_instruction'] = 0;
        //                     } else {
        //                         $arrInsert[$k]['delivery_instruction'] = -1;
        //                         if ((int)$value === 1) {
        //                             $arrInsert[$k]['receive_instruction'] = 0;
        //                         } else {
        //                             $arrInsert[$k]['receive_instruction'] = -1;
        //                         }
        //                     }
        //                 }
        //                 $arrInsert[$k][$key] = $value;
        //             }
        //             // if ($arrMid[$arrInsert[$k]['return_type_mid_id']] !== '仕入先返品') {
        //             //     $arrInsert[$k]['return_postal']      = '';
        //             //     $arrInsert[$k]['return_pref']        = '';
        //             //     $arrInsert[$k]['return_sub_address'] = '';
        //             //     $arrInsert[$k]['return_name']        = '';
        //             //     $arrInsert[$k]['return_tel']         = '';
        //             // }
        //             $arrInsert[$k]['status']            = 0;
        //             $arrInsert[$k]['receive_result']    = -1;
        //             $arrInsert[$k]['delivery_result']   = -1;
        //             $arrInsert[$k]['red_voucher']       = 0;
        //             $arrInsert[$k]['receive_real_num']  = 0;
        //             $arrInsert[$k]['delivery_real_num'] = 0;
        //             $arrInsert[$k]['receive_count']     = 0;
        //             $arrInsert[$k]['delivery_count']    = 0;
        //             $arrInsert[$k]['up_ope_cd']         = Auth::user()->tantou_code;
        //             $arrInsert[$k]['up_date']           = date('Y-m-d H:i:s');
        //             $arrInsert[$k]['in_ope_cd']         = Auth::user()->tantou_code;
        //             $arrInsert[$k]['in_date']           = date('Y-m-d H:i:s');
        //         }
        //         $modelReturn->insert($arrInsert);
        //     }
        //     return response()->json([
        //         'status'    => 1,
        //         'method'    => 'save',
        //         'return_no' => $modelReturn->return_no,
        //         'message'   => 'Save success',
        //         'data'      => [],
        //     ]);
        //     end :
        //     $dataReturn = $modelReturn->getReturnEditByReturnNo($returnNo, $currentPro, $returnTime);
        //     $count = $modelReturn->countOrderByReturnNo($returnNo, $currentPro);
        //     $checkChild = $count === 1 ? false : false;
        //     $arrProductCode = [];
        //     $status = '';
        //     if (!empty($receiveId)) {
        //         $arrProductCode = $modelOD->getProductCodeByReceiveId($receiveId);
        //         $dataCheck      = $modelReturn->checkInfoReturn($receiveId, $currentPro, $returnTime);
        //         if (count($dataCheck) !== 0) {
        //             $returnNo = $dataCheck->return_no;
        //         }
        //         if (count($arrProductCode) === 0 || count($dataCheck) !== 0) {
        //             $status = 999;
        //         }
        //     } else {
        //         if (count($dataReturn) === 0 && !is_null($returnNo)) {
        //             $status = 999;
        //             $returnNo = '';
        //         }
        //     }
        //     return response()->json([
        //         'return_no'    => $returnNo,
        //         'product_code' => $currentPro,
        //         'return_time'  => $returnTime,
        //         'status'       => $status,
        //         'data'         => $dataReturn ? $dataReturn : null,
        //         'dataLarge'    => array_merge($arrDefaultOption, $dataLarge->toArray()),
        //         'dataMid'      => array_merge($arrDefaultOption, $dataMid->toArray()),
        //         'dataProduct'  => $arrProductCode,
        //     ]);
        // }
        // return view('return-management.save');
    }

    /**
     * Get info product from product_code
     *
     * @param   $request  Request
     * @return json
     */
    public function getDataProduct(Request $request)
    {
        if ($request->ajax() === true) {
            $productCode = $request->input('product_code', null);
            $modelProduct = new MstProduct();
            $dataSupplier = $modelProduct->getDataProcessReturn($productCode);
            $dataProduct = $modelProduct->getInfoByProdCodeReturn($productCode);
            return response()->json([
                'dataSupplier'    => $dataSupplier,
                'dataProduct'    => $dataProduct,
            ]);
        }
    }
    /**
     * Process add and edit return order detail
     *
     * @param   $request  Request
     * @return json
     */
    public function saveReturnDetail(Request $request)
    {
        if ($request->ajax() === true) {
            $arrDefaultOption = array(array('key' => '_none', 'value' => '-----'));
            $modelLarge       = new MstReturnTypeLarge();
            $modelMid         = new MstReturnTypeMid();
            $modeOPD          = new DtOrderProductDetail();
            $modelReturn      = new DtReturn();
            $modelO           = new MstOrder();
            $modelRP          = new DtRePayment();
            $modelOD          = new MstOrderDetail();
            $modelReorder     = new DtReorder();
            $modelSS          = new MstStockStatus();
            $modelOTS         = new DtOrderToSupplier();
            $modelDRL         = new DtReceiptLedger();
            $modelDRLB        = new BackendDtReceiptLedger();
            $dataLarge        = $modelLarge->getData();
            $dataMid          = $modelMid->getData();
            $receiveId        = $request->input('receive_id', null);
            $productCode      = $request->input('product_code', null);
            $statusProcess    = 'add';
            $checkReceive     = $modelO->find($receiveId);
            $dateReturn       = $modelReturn->checkOrderExists($receiveId);
            $arrTemp          = $modeOPD->getProductReturnByReceive($receiveId);
            $arrTimeConfig    = Config::get('common.china_count');
            foreach ($arrTemp as $key => $product) {
                if (!empty($product->order_code)) {
                    $dataReceipt = $modelOTS->getDataReturnProuct($product->order_code);
                    if (!empty($dataReceipt)) {
                        $arrTemp[$key]->price_invoice = $dataReceipt->price_invoice;
                        $arrTemp[$key]->edi_order_code = $dataReceipt->edi_order_code;
                    } else {
                        $arrTemp[$key]->edi_order_code = '';
                    }
                } else {
                    $receivedOrderId = !empty($checkReceive) ? $checkReceive->received_order_id : '';
                    $dataReceipt = $modelDRLB->getDataReturnProuct($receivedOrderId, $product->product_code);
                    if (!empty($dataReceipt)) {
                        $arrTemp[$key]->price_invoice = $dataReceipt->price_invoice;
                        $arrTemp[$key]->edi_order_code = $dataReceipt->edi_order_code;
                    } else {
                        $arrTemp[$key]->edi_order_code = '';
                    }
                }
            }
            $arrProducts      = [];
            foreach ($arrTemp as $key => $product) {
                $arrProducts[] = $product;
                if (!empty($product->child_product_code)) {
                    $arrChild = $modeOPD->getChildProReturm($product->product_code, $receiveId);
                    foreach ($arrChild as $item) {
                        $arrProducts[] = $item;
                    }
                }
            }
            $dataRe = $modelRP->getDataByReceive($receiveId);
            if (count($dateReturn) !== 0) {
                $statusProcess = 'edit';
            }
            if ($request->status === 'check' || $request->status === 'reload') {
                goto end;
            }
            $rules = [
                'repay_price'          => 'nullable|numeric',
                'return_point'         => 'nullable|numeric',
                'repay_coupon'         => 'nullable|numeric',
                'return_fee'           => 'nullable|numeric',
                'other_commission'     => 'nullable|numeric',
                'debosit_commission'   => 'nullable|numeric',
                'total_return_price'   => 'nullable|numeric',
                'return_type'          => 'required',
                'return_type_large_id' => 'required',
                'return_type_mid_id'   => 'required',
                'deje_no'              => 'required',
            ];
            $checkReturn     = $request->input('check_return', []);
            $arrCheckPro     = $arrTemp->keyBy('detail_line_num');
            $returnQuantity  = $request->input('return_quantity', []);
            $validator       = Validator::make($request->all(), $rules);
            if (count($request->data_update) !== 0) {
                DB::beginTransaction();
                foreach ($request->data_update as $k => $dataUpdate) {
                    $ruleUpdate = [
                        $k => 'required',
                    ];
                    $messUpdate = [
                        $k . '.required' => __('messages.required_return_type'),
                    ];
                    $vali = Validator::make($dataUpdate, $ruleUpdate, $messUpdate);
                    if ($vali->fails()) {
                        DB::rollback();
                        return response()->json([
                            'status'  => 0,
                            'message' => $vali->errors(),
                            'data'    => []
                        ]);
                    }
                    $arrUpdate = ['return_type_mid_id' => $dataUpdate[$k]];
                    if ($dataUpdate[$k] === '1') {
                        if ($dataUpdate['receive_instruction'] === 2) {
                            $dataGet = $modelReturn->getDataProductByKey(
                                $dataUpdate['return_no'],
                                $dataUpdate['return_time']
                            );
                            foreach ($dataGet as $kSub => $vSub) {
                                if (!is_null($vSub->product_code)) {
                                    $productCode = $vSub->product_code;
                                } else {
                                    $productCode = $vSub->parent_product_code;
                                }
                                $modelSS->where(['product_code' => $productCode])
                                        ->decrement('wait_delivery_num', $vSub->return_quantity);
                            }
                        }
                        $arrUpdate['delivery_instruction'] = -1;
                        $arrUpdate['is_mojax'] = 0;
                    }
                    if ($dataUpdate[$k] === '2') {
                        $arrUpdate['is_mojax'] = 0;
                    }
                    if ($dataUpdate[$k] === '3') {
                        $arrUpdate['is_mojax'] = 1;
                    }
                    $modelReturn->updateData(
                        [
                            'return_no'   => $dataUpdate['return_no'],
                            'return_time' => $dataUpdate['return_time']
                        ],
                        $arrUpdate
                    );
                }
                DB::commit();
            }
            if (count(array_filter($checkReturn)) !== 0) {
                if ($validator->fails()) {
                    return response()->json([
                        'status'  => 0,
                        'message' => $validator->errors(),
                        'data'    => []
                    ]);
                } else {
                    $dataVali = array_filter($checkReturn);
                    $arrMessage = [];
                    $message = __('messages.message_return_quantity_is_zero');
                    foreach ($dataVali as $key => $value) {
                        $productInfo = $arrCheckPro[$key];
                        $quantity = $productInfo->received_order_num;
                        if (!is_numeric($returnQuantity[$key]) || $returnQuantity[$key] <= 0 ||
                            $returnQuantity[$key] > $quantity) {
                            $arrMessage['return_quantity'][$key] = $message . "(Quantity = $quantity).";
                        }
                    }
                    if (count($arrMessage) !== 0) {
                        return response()->json([
                            'status'  => 0,
                            'message' => $arrMessage,
                            'data'    => []
                        ]);
                    }
                }
                $times = $request->input('return_time', null);
                $returnNo = '';
                $arrProduct = [];
                $lineNo = 1;
                $dataReorder = $modelReorder->getMaxItem();
                if (!empty($dataReorder)) {
                    $reOrderId  = filter_var($dataReorder->reorder_id, FILTER_SANITIZE_NUMBER_INT);
                    $reOrderId  = 'returns' . sprintf("%07d", (int)$reOrderId + 1);
                } else {
                    $reOrderId  = 'returns0000001';
                }
                $modelReorder->reorder_id        = $reOrderId;
                $modelReorder->received_order_id = $checkReceive->received_order_id;
                $modelReorder->re_order_type     = 1;
                $modelReorder->return_time       = $times + 1;
                $modelReorder->in_ope_cd         = Auth::user()->tantou_code;
                $modelReorder->in_date           = date('Y-m-d H:i:s');
                $modelReorder->up_ope_cd         = Auth::user()->tantou_code;
                $modelReorder->up_date           = date('Y-m-d H:i:s');
                $modelReorder->save();
                $dataInsertOrder = [
                    'received_order_id'     => $reOrderId,
                    'mall_id'               => $checkReceive->mall_id,
                    'customer_id'           => $checkReceive->customer_id,
                    'used_coupon'           => 0,
                    'used_point'            => 0,
                    'discount'              => 0,
                    'total_price'           => -($request->input('sum_price', null)),
                    'request_price'         => -($request->input('sum_price', null)),
                    'order_date'            => date('Y-m-d H:i:s'),
                    'ship_charge'           => 0,
                    'pay_charge'            => 0,
                    'pay_after_charge'      => 0,
                    'pay_charge_discount'   => 0,
                    'goods_price'           => -($request->input('sum_price', null)),
                    'goods_tax'             => 0,
                    'payment_status'        => 0,
                    'payment_method'        => 11,
                    'company_name'          => $checkReceive->company_name,
                    'is_delay'              => 0,
                    'delay_priod'           => null,
                    'is_mail_sent'          => 1,
                    'order_status'          => ORDER_STATUS['FINISHED'],
                    'order_sub_status'      => ORDER_SUB_STATUS['DONE'],
                    'in_ope_cd'             => Auth::user()->tantou_code,
                    'in_date'               => date('Y-m-d H:i:s'),
                    'up_ope_cd'             => Auth::user()->tantou_code,
                    'up_date'               => date('Y-m-d H:i:s'),
                ];
                $returnReceiveId = $modelO->insertGetId($dataInsertOrder);
                $detailLineNum = 0;
                try {
                    DB::beginTransaction();
                    foreach (array_filter($checkReturn) as $key => $value) {
                        $detailLineNum++;
                        $quantity = $returnQuantity[$key];
                        if (!empty($arrCheckPro[$key]->child_product_code)) {
                            foreach ($arrProducts as $item) {
                                if (!empty($item->product_code)) {
                                    continue;
                                } elseif ($item->detail_line_num === $key) {
                                    $item->product_code   = $arrCheckPro[$key]->product_code;
                                    $item->return_id      = $arrCheckPro[$key]->return_id;
                                    $item->edi_order_code = $arrCheckPro[$key]->edi_order_code;
                                    $quantity = $quantity * $item->component_num;
                                    $returnNo = $this->saveReturn(
                                        $receiveId,
                                        $request,
                                        $item,
                                        $quantity,
                                        $lineNo,
                                        $request->input('sum_price', null)
                                    );
                                    $lineNo++;
                                }
                            }
                        } else {
                            $returnNo = $this->saveReturn(
                                $receiveId,
                                $request,
                                $arrCheckPro[$key],
                                $quantity,
                                $lineNo,
                                $request->input('sum_price', null)
                            );
                            $lineNo++;
                        }
                        $arrProduct[] = $arrCheckPro[$key]->product_code;
                        $dataInsertOrderDetail = [
                            'receive_id'            => $returnReceiveId,
                            'detail_line_num'       => $detailLineNum,
                            'product_code'          => $arrCheckPro[$key]->product_code,
                            'product_name'          => $arrCheckPro[$key]->product_name,
                            'price'                 => $arrCheckPro[$key]->price,
                            'quantity'              => $quantity,
                            'item_tax'              => 0,
                            'ship_price'            => 0,
                            'ship_tax'              => 0,
                            'receiver_id'           => $receiveId,
                            'in_ope_cd'             => Auth::user()->tantou_code,
                            'in_date'               => date('Y-m-d H:i:s'),
                            'up_ope_cd'             => Auth::user()->tantou_code,
                            'up_date'               => date('Y-m-d H:i:s')
                        ];
                        $modelOD->insert($dataInsertOrderDetail);
                        $modelOD->updateData(
                            $receiveId,
                            $arrCheckPro[$key]->detail_line_num,
                            ['return_quantity' => ($quantity + $arrCheckPro[$key]->return_quantity)]
                        );
                        $arrUpdate = [];
                        if ($arrCheckPro[$key]->quantity === $quantity + $arrCheckPro[$key]->return_quantity) {
                            $arrUpdate['product_status'] = PRODUCT_STATUS['RETURN'];
                        } else {
                            $arrUpdate['product_status'] = PRODUCT_STATUS['RETURN_A_PART'];
                        }
                        $modeOPD->updateData(
                            [
                                'receive_id'   => $receiveId,
                                'product_code' => $arrCheckPro[$key]->product_code
                            ],
                            $arrUpdate
                        );
                    }
                    $returnType = (int) $request->input('return_type', null);
                    $modelRP->repay_price            = $request->input('repay_price', null);
                    $modelRP->repay_name             = $request->input('repay_name', null);
                    $modelRP->repay_bank_code        = $request->input('repay_bank_code', null);
                    $modelRP->repay_bank_name        = $request->input('repay_bank_name', null);
                    $modelRP->repay_bank_branch_code = $request->input('repay_bank_branch_code', null);
                    $modelRP->repay_bank_branch_name = $request->input('repay_bank_branch_name', null);
                    $modelRP->repay_bank_account     = $request->input('repay_bank_account', null);
                    $modelRP->receive_id             = $receiveId;
                    $modelRP->repayment_time         = $times + 1;
                    $modelRP->payment_id             = null;
                    $modelRP->repayment_status       = 0;
                    $modelRP->is_return              = 1;
                    $modelRP->repayment_type         = $returnType;
                    $modelRP->return_point           = $request->input('return_point', null);
                    $modelRP->return_fee             = $request->input('return_fee', null);
                    $modelRP->return_coupon          = $request->input('repay_coupon', null);
                    $modelRP->deposit_transfer_commission = $request->input('debosit_commission', null);
                    $modelRP->other_commission       = $request->input('other_commission', null);
                    $modelRP->total_return_price     = $request->input('total_return_price', null);
                    $modelRP->return_no              = $returnNo;
                    $modelRP->repayment_date         = null;
                    $modelRP->up_ope_cd              = Auth::user()->tantou_code;
                    $modelRP->up_date                = date('Y-m-d H:i:s');
                    $modelRP->in_ope_cd              = Auth::user()->tantou_code;
                    $modelRP->in_date                = date('Y-m-d H:i:s');
                    $modelRP->save();

                    $orderReturnPrice = (int)$request->input('order_return_price', 0);
                    $orderReturnPoint = (int)$request->input('order_return_point', 0);
                    $orderReturnCoupon = (int)$request->input('order_return_coupon', 0);
                    $orderData = $modelO->find($receiveId);
                    $orderData->return_price += $orderReturnPrice;
                    $orderData->return_point += $orderReturnPoint;
                    $orderData->return_coupon += $orderReturnCoupon;
                    $orderData->save();
                    DB::commit();
                } catch (\Exception $e) {
                    DB::rollback();
                    report($e);
                }
            }
            return response()->json([
                'status'     => 'done',
                'time'       => time(),
            ]);
            end :

            $data = ['return_time' => 0];
            $arrCheckEdit      = [];
            $arrReturnQuantity = [];
            $arrCheckDefault   = [];
            $arrQuantity       = [];
            if (count($checkReceive) === 0) {
                $statusProcess = 'done';
            } else {
                foreach ($arrTemp as $value) {
                    $data['checked'][$value->detail_line_num] = 0;
                    $data['return_quantity'][$value->detail_line_num] = $value->received_order_num;
                    if ($value->product_code === $productCode) {
                        $data['check_return'][$value->detail_line_num] = 1;
                    } else {
                        $data['check_return'][$value->detail_line_num] = 0;
                    }
                    $arrCheckEdit[$value->detail_line_num] = 0;
                    $arrReturnQuantity[$value->detail_line_num] = $value->received_order_num;
                    $arrQuantity[$value->detail_line_num]      = $value->quantity;
                }
                if ($statusProcess === 'edit') {
                    $times           = 0;
                    $arrReturn       = [];
                    $defutlCheck     = $data['checked'];
                    $defaultQuantity = $data['return_quantity'];
                    foreach ($dateReturn as $dataR) {
                        if ($times !== $dataR->return_time) {
                            $times = $dataR->return_time;
                            $arrReturn[$times]['times']                = $arrTimeConfig[$times];
                            $arrReturn[$times]['deje_no']              = $dataR->deje_no;
                            $arrReturn[$times]['return_date']          = $dataR->return_date;
                            $arrReturn[$times]['return_type_large_id'] = (string)$dataR->return_type_large_id;
                            $arrReturn[$times]['return_type_mid_id']   = (string)$dataR->return_type_mid_id;
                            $arrReturn[$times]['receive_plan_date']    = $dataR->receive_plan_date;
                            $arrReturn[$times]['delivery_plan_date']   = $dataR->delivery_plan_date;
                            $arrReturn[$times]['payment_on_delivery']  = $dataR->payment_on_delivery;
                            $arrReturn[$times]['return_type_detail']   = $dataR->return_type_detail;
                            $arrReturn[$times]['note']                 = $dataR->note;
                            $arrReturn[$times]['return_type']          = $dataR->return_type;
                            $arrReturn[$times]['return_point']         = $dataR->return_point;
                            $arrReturn[$times]['return_time']          = $dataR->return_time;
                            $arrReturn[$times]['return_line_no']       = $dataR->return_line_no;
                            $arrReturn[$times]['return_no']            = $dataR->return_no;
                            $arrReturn[$times]['receive_instruction']  = $dataR->receive_instruction;
                            foreach ($dataRe as $key => $repay) {
                                if ($times === $repay->repayment_time) {
                                    $arrReturn[$times]['repay_bank_code']        = $repay->repay_bank_code;
                                    $arrReturn[$times]['repay_bank_name']        = $repay->repay_bank_name;
                                    $arrReturn[$times]['repay_bank_branch_code'] = $repay->repay_bank_branch_code;
                                    $arrReturn[$times]['repay_bank_branch_name'] = $repay->repay_bank_branch_name;
                                    $arrReturn[$times]['repay_bank_account']     = $repay->repay_bank_account;
                                    $arrReturn[$times]['repay_name']             = $repay->repay_name;
                                    $arrReturn[$times]['repay_price']            = $repay->repay_price;
                                    $arrReturn[$times]['return_fee']             = $repay->return_fee;
                                    $arrReturn[$times]['return_coupon']          = $repay->return_coupon;
                                    $arrReturn[$times]['deposit_transfer_commission']
                                                                            = $repay->deposit_transfer_commission;
                                    $arrReturn[$times]['other_commission']       = $repay->other_commission;
                                    $arrReturn[$times]['total_return_price']     = $repay->total_return_price;
                                    unset($dataRe[$key]);
                                    break;
                                }
                            }
                            $arrReturn[$times]['check_return']    = $defutlCheck;
                            $arrReturn[$times]['return_quantity'] = $defaultQuantity;
                        }
                        foreach ($arrTemp as $value) {
                            if ($dataR->parent_product_code === $value->product_code) {
                                if ($value->received_order_num === 0) {
                                    $data['checked'][$value->detail_line_num] = 1;
                                    $data['check_return'][$value->detail_line_num] = 0;
                                }
                                $arrReturn[$times]['check_return'][$value->detail_line_num]    = 1;
                                $arrReturn[$times]['return_quantity'][$value->detail_line_num] = $dataR->return_quantity;
                                $data['return_quantity'][$value->detail_line_num]  = $dataR->return_quantity;
                                break;
                            }
                        }
                        $data['return_time']     = $dataR->return_time;
                    }
                    $data['return'] = $arrReturn;
                }
                if ($checkReceive->payment_method === 1 && $checkReceive->mall_id !== 3) {
                    $data['repay_bank_code']        = '';
                    $data['repay_bank_name']        = 'クレカ返金';
                    $data['repay_bank_branch_code'] = '';
                    $data['repay_bank_branch_name'] = 'クレカ返金';
                    $data['repay_name']             = 'クレカ返金';
                    $data['repay_bank_account']     = 'クレカ返金';
                } elseif ($checkReceive->mall_id === 3) {
                    $data['repay_bank_code']        = '';
                    $data['repay_bank_name']        = 'アマゾン';
                    $data['repay_bank_branch_code'] = '';
                    $data['repay_bank_branch_name'] = 'アマゾン';
                    $data['repay_name']             = 'アマゾン';
                    $data['repay_bank_account']     = 'アマゾン';
                } elseif ($checkReceive->mall_id === 9 && in_array($checkReceive->payment_method, [14, 15, 16, 17])) {
                    $data['repay_bank_code']        = '';
                    $data['repay_bank_name']        = '楽天ペイ返金';
                    $data['repay_bank_branch_code'] = '';
                    $data['repay_bank_branch_name'] = '楽天ペイ返金';
                    $data['repay_name']             = '楽天ペイ返金';
                    $data['repay_bank_account']     = '楽天ペイ返金';
                } elseif ($checkReceive->mall_id === 10 && in_array($checkReceive->payment_method, [18, 19])) {
                    $data['repay_bank_code']        = '';
                    $data['repay_bank_name']        = 'WOW返金';
                    $data['repay_bank_branch_code'] = '';
                    $data['repay_bank_branch_name'] = 'WOW返金';
                    $data['repay_name']             = 'WOW返金';
                    $data['repay_bank_account']     = 'WOW返金';
                }
                $data['times_return'] = isset($arrTimeConfig[$data['return_time'] + 1]) ?
                                    $arrTimeConfig[$data['return_time'] + 1] :
                                    $data['return_time'] + 1;
            }
            $modelMRD = new MstReturnDetail();
            $returnTypeAllPotion = $modelMRD->getData();
            $modelMR = new MstReturn();
            $dataReturnType = $modelMR->getData();
            return response()->json([
                'status'                  => $statusProcess,
                'data'                    => $data ? $data : null,
                'dataReturnType'          => array_merge($arrDefaultOption, $dataReturnType->toArray()),
                'dataMid'                 => array_merge($arrDefaultOption, $dataMid->toArray()),
                'dataLarge'               => [['key' => 1, 'value' => 'お客様返品']],
                'dataProduct'             => $arrProducts,
                'default_check_return'    => $arrCheckEdit,
                'default_return_quantity' => $arrReturnQuantity,
                'default_quantity'        => $arrQuantity,
                'type'                    => $request->status,
                'returnTypeAllPotion'     => $returnTypeAllPotion,
                'returnTypePotion'        => [['key' => '_none', 'value' => '-----']],
                'time'                    => time(),
            ]);
        }
        return view('return-management.return_detail');
    }
    /**
     * Process get repayment price
     *
     * @param   $request  Request
     * @return json
     */
    public function getRepaymentPrice(Request $request)
    {
        $modelO          = new MstOrder();
        $modelMM         = new MstReturn();
        $returnType      = $request->input('return_type', '');
        $receiveId       = $request->input('receive_id', '');
        $checkReturn     = $request->input('check_return', []);
        // $status          = $request->input('status', '');
        $defaultQuantity = $request->input('default_quantity', '');
        $returnQuantity  = $request->input('return_quantity', []);
        $typeCheckReturn = $request->input('type_check_return', null);
        $repayPrice      = 0;
        $repayPoint      = 0;
        $repayCoupon     = 0;
        $dataOrder       = $modelO->find($receiveId);
        $sumPrice        = 0;
        $orderReturnPrice  = 0;
        $orderReturnPoint  = 0;
        $orderReturnCoupon = 0;

        if ($typeCheckReturn === 'check_repayment') {
            if (empty($returnType)) {
                $returnType = 0;
            }
            $resReturnCheck = $modelO->getReturnCheck($receiveId)->keyBy('detail_line_num')->toArray();
            foreach ($resReturnCheck as $key => $value) {
                if ($value['order_status'] === ORDER_STATUS['ON_HOLD']
                    || $value['product_status'] !== PRODUCT_STATUS['RETURN']) {
                    $checkReturn[$key] = 0;
                } else {
                    $checkReturn[$key] = $value['quantity'];
                }
                $returnQuantity[$key]  = $value['quantity'];
                $defaultQuantity[$key] = $value['quantity'];
            }
        }
        $check = $modelMM->find($returnType);
        // if (count(array_filter($checkReturn)) !== 0 && in_array($returnType, ['0', '1'])) {
        if (count(array_filter($checkReturn)) !== 0 && !empty($check)) {
            $returnType = (int)$returnType;
            $checkFull = true;
            $sum = 0;
            $dataOrderDetail = $modelO->getOrderByReceive($receiveId)->keyBy('detail_line_num');
            foreach (array_filter($checkReturn) as $key => $value) {
                $quantity = $returnQuantity[$key];
                $sum += ($dataOrderDetail[$key]->price + $dataOrderDetail[$key]->item_tax) * $quantity ;
                $sumPrice += $dataOrderDetail[$key]->price * $quantity;
            }
            foreach ($returnQuantity as $key => $quantity) {
                if (isset($defaultQuantity[$key]) && $defaultQuantity[$key] !== $quantity) {
                    $checkFull = false;
                    break;
                }
            }
            if (!in_array(0, $checkReturn) && $checkFull) {
                $repayPrice  = $dataOrder->request_price;
                $repayPoint  = $dataOrder->used_point;
                $repayCoupon = $dataOrder->used_coupon;
                $orderReturnPrice = $repayPrice;
                $orderReturnPoint = $repayPoint;
                $orderReturnCoupon = $repayCoupon;
            } else {
                if ($sum <= $dataOrder->request_price - $dataOrder->return_price) {
                    $repayPrice  = $sum;
                    $repayPoint  = 0;
                    $repayCoupon = 0;
                    $orderReturnPrice = $sum;
                } elseif (($sum - ($dataOrder->used_point - $dataOrder->return_point)
                    - ($dataOrder->request_price - $dataOrder->return_price)) < 0) {
                    $repayPrice  = $dataOrder->request_price - $dataOrder->return_price;
                    $repayPoint  = $sum - ($dataOrder->request_price - $dataOrder->return_price);
                    $repayCoupon = 0;
                    $orderReturnPrice = $repayPrice;
                    $orderReturnPoint = $repayPoint;
                } else {
                    $repayPrice  = $dataOrder->request_price - $dataOrder->return_price;
                    $repayPoint  = $dataOrder->used_point - $dataOrder->return_point;
                    $repayCoupon = $sum - ($dataOrder->request_price - $dataOrder->return_price)
                        - ($dataOrder->used_point - $dataOrder->return_point);
                    $orderReturnCoupon = $repayCoupon;
                }
            }
        }

        return response()->json([
            'repayPrice'    => $repayPrice,
            'repayPoint'    => $repayPoint,
            'repayCoupon'   => $repayCoupon,
            'sumPrice'      => $sumPrice,
            'orderReturnPrice'  => $orderReturnPrice,
            'orderReturnPoint'  => $orderReturnPoint,
            'orderReturnCoupon' => $orderReturnCoupon
        ]);
    }
    /**
     * Save return product
     *
     * @param   int $receiveId
     * @param   obj $request
     * @param   obj $dataProduct
     * @param   int $returnQuantity
     * @param   int $lineNo
     * @return string $returnNo
     */
    public function saveReturn($receiveId, $request, $dataProduct, $returnQuantity, $lineNo)
    {
        $times = $request->input('return_time', null);
        $modelReturn = new DtReturn();
        $modelO      = new MstOrder();
        $dataR = $modelReturn->getMaxDataByReceive($receiveId);
        $dataOrder = $modelO->find($receiveId);
        if (empty($dataR->return_no)) {
            $maxNo  = $modelReturn->getMaxNo();
            if (empty($maxNo->maxNo)) {
                $returnNo = '9900000';
            } else {
                $returnNo = (int)$maxNo->maxNo + 1;
            }
        } else {
            $returnNo = $dataR->return_no;
        }
        $returnType = (int) $request->input('return_type', null);
        $returnMid  = $request->input('return_type_mid_id', null);
        $returnLarge= $request->input('return_type_large_id', null);
        if (in_array($returnType, [0, 2, 3])) {
            $returnFee = 0;
            $returnCosFee = 0;
        } elseif ($returnType === 1) {
            $returnFee = 1010;
            if ($dataOrder->payment_method !== 3) {
                $returnCosFee = 0;
            } else {
                $returnCosFee = $dataOrder->pay_after_charge;
            }
        }
        $isMojax = in_array($returnMid, ['1', '2']) ?
                    0 :
                    (($returnMid === '3') ?
                        1 :
                        (($returnMid === '6') ?
                            2 :
                            null
                        )
                    );
                    $modelReturn->return_no            = $returnNo;
                    $modelReturn->return_line_no       = $lineNo;
                    $modelReturn->return_time          = $times + 1;
                    $modelReturn->return_address_id    = (int)$returnMid === 2 ? $dataProduct->return_id : 0;
                    $modelReturn->return_type          = $returnType;
                    $modelReturn->return_point         = $request->input('return_point', null);
                    $modelReturn->return_price         = $dataProduct->price_invoice;
                    $modelReturn->return_fee           = $returnFee;
                    $modelReturn->return_cod_fee       = $returnCosFee;
                    $modelReturn->receive_id           = $receiveId;
                    $modelReturn->return_date          = $request->input('return_date', null);
                    $modelReturn->deje_no              = $request->input('deje_no', null);
                    $modelReturn->return_type_large_id = $request->input('return_type_large_id', null);
                    $modelReturn->return_type_detail   = $request->input('return_type_detail', null);
                    $modelReturn->return_type_mid_id   = $returnMid;
                    $modelReturn->status               = !in_array($returnMid, ['4']) ? 0 : -1;
                    $modelReturn->receive_instruction  = in_array($returnMid, ['1', '2', '3', '6']) ? 0 : -1;
                    $modelReturn->receive_result       = -1;
                    $modelReturn->delivery_instruction = in_array($returnMid, ['2', '3']) ? 0 : -1;
                    $modelReturn->is_mojax             = $isMojax;
                    $modelReturn->delivery_result      = -1;
                    $modelReturn->red_voucher          = in_array($returnMid, ['2', '5']) ? 0 : -1;
                    $modelReturn->parent_product_code  = $dataProduct->product_code;
                    $modelReturn->product_code         = $dataProduct->child_product_code;
                    $modelReturn->return_quantity      = $returnQuantity;
                    $modelReturn->return_tanka         = $dataProduct->price_invoice;
                    $modelReturn->supplier_id          = $dataProduct->price_supplier_id;
                    $modelReturn->edi_order_code       = $dataProduct->edi_order_code;
                    $modelReturn->receive_plan_date    = $request->input('receive_plan_date', null);
                    $modelReturn->receive_real_date    = null;
                    $modelReturn->receive_real_num     = 0;
                    $modelReturn->delivery_plan_date   = $request->input('delivery_plan_date', null);
                    $modelReturn->delivery_real_date   = null;
                    $modelReturn->delivery_real_num    = 0;
                    $modelReturn->payment_on_delivery  = $request->input('payment_on_delivery', null);
                    $modelReturn->note                 = $request->input('note', null);
                    $modelReturn->receive_count        = 0;
                    $modelReturn->delivery_count       = 0;
                    $modelReturn->error_code           = null;
                    $modelReturn->error_message        = null;
                    $modelReturn->in_ope_cd            = Auth::user()->tantou_code;
                    $modelReturn->in_date              = date('Y-m-d H:i:s');
                    $modelReturn->up_ope_cd            = Auth::user()->tantou_code;
                    $modelReturn->up_date              = date('Y-m-d H:i:s');
                    $modelReturn->save();
                    return $returnNo;
    }
    /**
     * Process new return
     *
     * @param   $request  Request
     * @return json
     */
    public function saveReturnProduct(Request $request)
    {
        if ($request->ajax() === true) {
            $arrDefaultOption = array(array('key' => '_none', 'value' => '-----'));
            $modelLarge       = new MstReturnTypeLarge();
            $modelMid         = new MstReturnTypeMid();
            $modelProduct     = new MstProduct();
            $modelSS          = new MstStockStatus();
            $modeOPD          = new DtOrderProductDetail();
            $modelReturn      = new DtReturn();
            $modelO           = new MstOrder();
            $modelRP          = new DtRePayment();
            $modelOD          = new MstOrderDetail();
            $dataLarge        = $modelLarge->getData();
            $dataMid          = $modelMid->getData();
            $dataProduct      = $request->input('dataProduct', null);
            $statusProcess = 'add';
            if ($request->status === 'check') {
                goto end;
            }
            $returnMid = $request->return_type_mid_id;
            $rules = [
                'deje_no'              => 'required',
                'return_date'          => 'required',
                'return_type_large_id' => 'required',
                'return_type_mid_id'   => 'required',
            ];
            $validator       = Validator::make($request->all(), $rules);
            if ($validator->fails()) {
                return response()->json([
                    'status'  => 0,
                    'message' => $validator->errors(),
                    'data'    => []
                ]);
            }
            $arrOld = [];
            $arrNew = [];
            $arrError = [];
            if (count($dataProduct) !== 0) {
                $i = 0;
                $arrProductCheckStock = [];
                foreach ($dataProduct as $key => $item) {
                    if (!empty($item['product_code'])) {
                        if (!isset($item['child'])) {
                            $item['child'] = [];
                        }
                        $childErr = false;
                        if (isset($item['is_add']) && $item['is_add'] === 1) {
                            $arrNew[$key] = $item;
                            if (empty($item['quantity']) ||$item['quantity'] <= 0) {
                                $arrError[$i]['quantityMessage'] = __('messages.message_quantity_less_0');
                                $childErr = true;
                            }
                            if (empty($item['return_tanka']) || $item['return_tanka'] <= 0) {
                                $arrError[$i]['returnTankaMessage'] = __('messages.message_return_tanka_less_0');
                                $childErr = true;
                            }
                            if (!empty($item['productMessage'])) {
                                $arrError[$i]['product_code'] = $item['productMessage'];
                                $childErr = true;
                            }
                            if ($childErr) {
                                $arrError[$i]['index'] = $key;
                                $i++;
                            }
                        } else {
                            $arrOld[$key] = $item;
                        }

                        if (count($item['child']) !== 0) {
                            foreach ($item['child'] as $child) {
                                if (isset($arrProductCheckStock[$child['product_code']])) {
                                    $temp = $arrProductCheckStock[$child['product_code']];
                                } else {
                                    $temp = ['quantity' => 0, 'index' => $key, 'check' => false];
                                }
                                if (isset($item['is_add']) && $item['is_add'] === 1) {
                                    $temp['quantity'] += $item['quantity'] * $child['component_num'];
                                    $temp['check']    = true;
                                    $temp['index']    = $key;
                                } else {
                                    $temp['quantity'] += $child['return_quantity'];
                                }
                                $arrProductCheckStock[$child['product_code']] = $temp;
                            }
                        } else {
                            if (isset($arrProductCheckStock[$item['product_code']])) {
                                $temp = $arrProductCheckStock[$item['product_code']];
                            } else {
                                $temp = ['quantity' => 0, 'index' => $key, 'check' => false];
                            }
                            if (isset($item['is_add']) && $item['is_add'] === 1) {
                                $temp['quantity'] += $item['quantity'];
                                $temp['check']    = true;
                                $temp['index']    = $key;
                            } else {
                                $temp['quantity'] += $item['return_quantity'];
                            }
                            $arrProductCheckStock[$item['product_code']] = $temp;
                        }
                        if ((int)$returnMid === 2 && empty($item['return_id'])) {
                            $arrError[$i]['index'] = $key;
                            $arrError[$i]['address'] = 'ERROR';
                        }
                    }
                }
                if (count($arrError) !==0) {
                    return response()->json([
                        'messageError' => $arrError,
                    ]);
                } else {
                    if (count($arrNew) && ((string)$request->return_type_large_id === '3' && (string)$returnMid === '3')) {
                        $arrKeyProduct = array_keys($arrProductCheckStock);
                        $dataProductStock = $modelSS->getDataStockCheckReturn($arrKeyProduct)->keyBy(function ($item) {
                            return strtoupper($item['product_code']);
                        });
                        $j = 0;
                        $arrError = [];
                        foreach ($arrProductCheckStock as $key => $item) {
                            $key = strtoupper($key);
                            if ($item['check']
                                && isset($dataProductStock[$key]) && $dataProductStock[$key]->cal < $item['quantity']) {
                                $arrError[$j]['index'] = $item['index'];
                                $arrError[$j]['quantityMessage'] = __('messages.message_stock_have_error');
                                $j++;
                            }
                        }
                        if (count($arrError)) {
                            return response()->json([
                                'messageError' => $arrError,
                            ]);
                        }
                    }
                }
            }
            $returnNo = 0;
            $returnLine = 0;
            if ($request->status === 'add') {
                $maxNo  = $modelReturn->getMaxNo();
                if (empty($maxNo->maxNo)) {
                    $returnNo = '9900000';
                } else {
                    $returnNo = (int)$maxNo->maxNo + 1;
                }
            } else {
                $returnNo = $request->return_no;
            }

            $receiveInstruction = -1;
            if (!in_array($request->return_type_large_id, ['3', '4', '5']) &&
                in_array($returnMid, ['1', '2', '3', '6'])) {
                $receiveInstruction = 0;
            }
            $deliveryInstruction = -1;
            if ($request->return_type_large_id !== '5' &&
                in_array($returnMid, ['2', '3'])) {
                $deliveryInstruction = 0;
            }
            $isMojax = in_array($returnMid, ['1', '2']) ? 0 : ($returnMid === '3' ? 1 : ($returnMid === '6' ? 2 :null));
            $dataOld = $modelReturn->getOrderByReturnNo($returnNo);
            $flgUpdateStock = false;
            if (count($dataOld) && $dataOld->return_type_mid_id === 6) {
                if ($returnMid === '1') {
                    if ($dataOld->receive_instruction === 2) {
                        $flgUpdateStock = true;
                    }
                    $deliveryInstruction = -1;
                    $isMojax = 0;
                }
                if ($returnMid === '2') {
                    $isMojax = 0;
                }
                if ($returnMid === '3') {
                    $isMojax = 1;
                }
            }

            $flgUpdate = true;
            foreach ($arrOld as $value) {
                if ($flgUpdate) {
                    $key = [
                        'return_no'      => $returnNo,
                        'parent_product_code'   => $value['product_code']
                    ];
                    $arrUpdate = [
                        'return_date'          => $request->return_date,
                        'return_address_id'    => $value['return_id'],
                        'deje_no'              => $request->deje_no,
                        'return_type_large_id' => $request->return_type_large_id,
                        'return_type_mid_id'   => $returnMid,
                        'status'               => !in_array($returnMid, ['4']) ? 0 : -1,
                        'receive_instruction'  => $receiveInstruction,
                        'delivery_instruction' => $deliveryInstruction,
                        'is_mojax'             => $isMojax,
                        'receive_plan_date'    => $request->receive_plan_date ? $request->receive_plan_date : null,
                        'delivery_plan_date'   => $request->delivery_plan_date ? $request->delivery_plan_date : null,
                        'payment_on_delivery'  => $request->payment_on_delivery,
                        'note'                 => $request->note,
                    ];
                    $modelReturn->updateData($key, $arrUpdate);
                }
                if ($flgUpdateStock) {
                    $dataGet = $modelReturn->getDataProductByKey(
                        $returnNo,
                        $request->return_time
                    );
                    foreach ($dataGet as $kSub => $vSub) {
                        if (!is_null($vSub->product_code)) {
                            $productCode = $vSub->product_code;
                        } else {
                            $productCode = $vSub->parent_product_code;
                        }
                        $modelSS->where(['product_code' => $productCode])
                                ->decrement('wait_delivery_num', $vSub->return_quantity);
                    }
                }
                if ((int)$value['return_line_no'] > $returnLine) {
                    $returnLine = $value['return_line_no'];
                }
            }
            if (count($arrNew)) {
                $arrInsert = [];
                foreach ($arrNew as $new) {
                    if (count($new['child']) !== 0) {
                        foreach ($new['child'] as $child) {
                            $arrTemp = [];
                            $returnLine++;
                            $returnQuantity = $new['quantity'] * $child['component_num'];
                            $arrTemp['return_no']            = $returnNo;
                            $arrTemp['return_line_no']       = $returnLine;
                            $arrTemp['return_time']          = 1;
                            $arrTemp['return_date']          = $request->return_date;
                            $arrTemp['return_address_id']    = (int) $new['return_id'];
                            $arrTemp['deje_no']              = $request->deje_no;
                            $arrTemp['return_type']          = 0;
                            $arrTemp['return_type_large_id'] = $request->return_type_large_id;
                            $arrTemp['return_type_mid_id']   = $returnMid;
                            $arrTemp['status']               = !in_array($returnMid, ['4']) ? 0 : -1;
                            $arrTemp['receive_instruction']  =  $receiveInstruction;
                            $arrTemp['receive_result']       = -1;
                            $arrTemp['delivery_instruction'] = $deliveryInstruction;
                            $arrTemp['is_mojax']             = $isMojax;
                            $arrTemp['delivery_result']      = -1;
                            $arrTemp['parent_product_code']  = $new['product_code'];
                            $arrTemp['product_code']         = $child['product_code'];
                            $arrTemp['return_quantity']      = $returnQuantity;
                            $arrTemp['return_point']         = 0;
                            $arrTemp['return_fee']           = 0;
                            $arrTemp['return_cod_fee']       = 0;
                            $arrTemp['red_voucher']          = in_array($returnMid, ['2', '5']) ? 0 : -1;
                            $arrTemp['return_price']         = $child['price_invoice'];
                            $arrTemp['return_tanka']         = $child['price_invoice'];
                            $arrTemp['supplier_id']          = $new['supplier_id'];
                            $arrTemp['edi_order_code']       = isset($new['edi_order_code']) ? $new['edi_order_code'] : null;
                            $arrTemp['receive_id']           = null;
                            $arrTemp['receive_plan_date']    = $request->receive_plan_date ? $request->receive_plan_date : null;
                            $arrTemp['receive_real_date']    = null;
                            $arrTemp['receive_real_num']     = 0;
                            $arrTemp['delivery_plan_date']   = $request->delivery_plan_date ? $request->delivery_plan_date : null;
                            $arrTemp['delivery_real_date']   = null;
                            $arrTemp['delivery_real_num']    = 0;
                            $arrTemp['payment_on_delivery']  = $request->payment_on_delivery;
                            $arrTemp['note']                 = $request->note;
                            $arrTemp['receive_count']        = 0;
                            $arrTemp['delivery_count']       = 0;
                            $arrTemp['supplier_return_no']   = null;
                            $arrTemp['supplier_return_line'] = null;
                            $arrTemp['error_code']           = null;
                            $arrTemp['error_message']        = null;
                            $arrTemp['in_ope_cd']            = Auth::user()->tantou_code;
                            $arrTemp['in_date']              = now();
                            $arrTemp['up_ope_cd']            = Auth::user()->tantou_code;
                            $arrTemp['up_date']              = now();
                            $arrInsert[] = $arrTemp;
                            if ((string)$request->return_type_large_id === '3' && (string)$returnMid === '3') {
                                $modelSS->updateData(
                                    ['product_code' => $child['product_code']],
                                    [
                                        'return_num'        => DB::raw("return_num + {$returnQuantity}"),
                                        'wait_delivery_num' => DB::raw("wait_delivery_num + {$returnQuantity}"),
                                    ]
                                );
                            }
                            if ((string)$request->return_type_large_id === '1' && (string)$returnMid === '5') {
                                $modelRL = new DtReceiptLedger();
                                $strTemp = $arrTemp['return_no'] . $arrTemp['return_line_no'] . $arrTemp['return_time'];
                                $modelRL->add2ReceiptLedgerInStockRIR([
                                    'pa_order_code'        => $strTemp,
                                    'pa_product_code'      => $child['product_code'],
                                    'pa_arrival_num'       => $returnQuantity,
                                    'pa_supplier_id'       => $new['supplier_id'],
                                    'pa_stock_type'        => 4,
                                    'pa_stock_detail_type' => 4,
                                ]);
                            }
                        }
                    } else {
                        $returnLine++;
                        $arrTemp = [];
                        $returnQuantity = $new['quantity'];
                        $arrTemp['return_no']            = $returnNo;
                        $arrTemp['return_line_no']       = $returnLine;
                        $arrTemp['return_time']          = 1;
                        $arrTemp['return_date']          = $request->return_date;
                        $arrTemp['return_address_id']    = (int) $new['return_id'];
                        $arrTemp['deje_no']              = $request->deje_no;
                        $arrTemp['return_type']          = 0;
                        $arrTemp['return_type_large_id'] = $request->return_type_large_id;
                        $arrTemp['return_type_mid_id']   = $returnMid;
                        $arrTemp['status']               = !in_array($returnMid, ['4']) ? 0 : -1;
                        $arrTemp['receive_instruction']  = $receiveInstruction;
                        $arrTemp['receive_result']       = -1;
                        $arrTemp['delivery_instruction'] = $deliveryInstruction;
                        $arrTemp['is_mojax']             = in_array($returnMid, ['1', '2']) ? 0 : ($returnMid === '3' ? 1 : null);
                        $arrTemp['delivery_result']      = -1;
                        $arrTemp['edi_order_code']       = isset($new['edi_order_code']) ? $new['edi_order_code'] : null;
                        $arrTemp['parent_product_code']  = $new['product_code'];
                        $arrTemp['product_code']         = null;
                        $arrTemp['return_quantity']      = $returnQuantity;
                        $arrTemp['return_point']         = 0;
                        $arrTemp['return_fee']           = 0;
                        $arrTemp['return_cod_fee']       = 0;
                        $arrTemp['red_voucher']          = in_array($returnMid, ['2', '5']) ? 0 : -1;
                        $arrTemp['return_price']         = $new['return_price'];
                        $arrTemp['return_tanka']         = $new['return_tanka'];
                        $arrTemp['supplier_id']          = $new['supplier_id'];
                        $arrTemp['receive_id']           = null;
                        $arrTemp['receive_plan_date']    = $request->receive_plan_date;
                        $arrTemp['receive_real_date']    = null;
                        $arrTemp['receive_real_num']     = 0;
                        $arrTemp['delivery_plan_date']   = $request->delivery_plan_date;
                        $arrTemp['delivery_real_date']   = null;
                        $arrTemp['delivery_real_num']    = 0;
                        $arrTemp['payment_on_delivery']  = $request->payment_on_delivery;
                        $arrTemp['note']                 = $request->note;
                        $arrTemp['receive_count']        = 0;
                        $arrTemp['delivery_count']       = 0;
                        $arrTemp['supplier_return_no']   = null;
                        $arrTemp['supplier_return_line'] = null;
                        $arrTemp['error_code']           = null;
                        $arrTemp['error_message']        = null;
                        $arrTemp['in_ope_cd']            = Auth::user()->tantou_code;
                        $arrTemp['in_date']              = now();
                        $arrTemp['up_ope_cd']            = Auth::user()->tantou_code;
                        $arrTemp['up_date']              = now();
                        $arrInsert[] = $arrTemp;
                        if ((string)$request->return_type_large_id === '3' && (string)$returnMid === '3') {
                            $modelSS->updateData(
                                ['product_code' => $new['product_code']],
                                [
                                    'return_num'        => DB::raw("return_num + {$returnQuantity}"),
                                    'wait_delivery_num' => DB::raw("wait_delivery_num + {$returnQuantity}"),
                                ]
                            );
                        }
                        if ((string)$request->return_type_large_id === '1' && (string)$returnMid === '5') {
                            $modelRL = new DtReceiptLedger();
                            $strTemp = $arrTemp['return_no'] . $arrTemp['return_line_no'] . $arrTemp['return_time'];
                            $modelRL->add2ReceiptLedgerInStockRIR([
                                'pa_order_code'        => $strTemp,
                                'pa_product_code'      => $new['product_code'],
                                'pa_arrival_num'       => $returnQuantity,
                                'pa_supplier_id'       => $new['supplier_id'],
                                'pa_stock_type'        => 4,
                                'pa_stock_detail_type' => 4,
                            ]);
                        }
                    }
                }
                $modelReturn->insert($arrInsert);

                // $orderCode = $request->input('order_code', '');
                // if (!empty($orderCode)) {
                //     $orderToSup = DtOrderToSupplier::find($orderCode);
                //     $orderToSup->ope_tantou = Auth::user()->tantou_code;
                //     $orderToSup->ope_status = 2;
                //     $orderToSup->save();
                // }
            }
            return response()->json([
                'time'   => time(),
                'status' => 'done',
            ]);
            end :
            $arrProducts = [];
            $data = [];
            if ($request->return_no) {
                $datas = $modelReturn->getDataByReturnNo($request->return_no);
                if (count($datas) === 0) {
                    return response()->json([
                        'time'   => time(),
                        'status' => 'done',
                    ]);
                } else {
                    $productCodeCheck = '';
                    $childCheck       = [];
                    $statusProcess    = 'edit';
                    $productCode      = [];
                    $quantity         = 0;
                    $temp             = [];
                    $child            = [];
                    $i                = -1;
                    $j                = 0;
                    foreach ($datas as $value) {
                        if (empty($productCodeCheck) ||
                            $productCodeCheck !== $value->parent_product_code ||
                            ($productCodeCheck === $value->parent_product_code &&
                                (empty($value->product_code)) ||
                                (!empty($value->product_code) && in_array($value->product_code, $childCheck)))
                        ) {
                            $productCodeCheck = $value->parent_product_code;
                            $childCheck = [$value->product_code];
                            $j++;
                        }
                        if (!in_array($value->parent_product_code . $j, $productCode)) {
                            $productCode[] = $value->parent_product_code . $j;
                            $child         = [];
                            $quantity      = 0;
                            $i++;
                        }
                        if (!empty($value->product_code)) {
                            $child[] = [
                                'product_code'    => $value->product_code,
                                'product_name'    => $value->child_product_name,
                                'price_invoice'   => $value->return_price,
                                'return_quantity' => $value->return_quantity,
                                'return_line_no'  => $value->return_line_no,
                            ];
                            $childCheck[] = $value->product_code;
                            $quantity = $value->return_quantity / $value->component_num;
                        } else {
                            $quantity = $value->return_quantity;
                        }
                        $arrProducts[$i] = [
                            'product_code'    => $value->parent_product_code,
                            'product_name'    => $value->parent_product_name,
                            'return_price'    => $value->return_price,
                            'return_tanka'    => $value->return_tanka,
                            'address_name'    => $value->address,
                            'supplier_id'     => $value->price_supplier_id,
                            'return_id'       => $value->return_address_id,
                            'return_no'       => $value->return_no,
                            'return_line_no'  => $value->return_line_no,
                            'return_time'     => $value->return_time,
                            'error_code'      => $value->error_code,
                            'edi_order_code'  => $value->edi_order_code,
                            'child'           => $child,
                            'is_add'          => 0,
                            'return_quantity' => $quantity,
                        ];
                        $data = [
                            'return_date'          => $value->return_date,
                            'return_address_id'    => $value->return_address_id,
                            'deje_no'              => $value->deje_no,
                            'return_type_large_id' => $value->return_type_large_id,
                            'return_type_mid_id'   => $value->return_type_mid_id,
                            'receive_plan_date'    => $value->receive_plan_date,
                            'delivery_plan_date'   => $value->delivery_plan_date,
                            'payment_on_delivery'  => $value->payment_on_delivery,
                            'note'                 => $value->note,
                            'return_no'            => $value->return_no,
                            'return_line_no'       => $value->return_line_no,
                            'return_time'          => $value->return_time,
                        ];
                    }
                }
            } elseif (!empty($request->product_code)) {
                $productCode = $request->product_code;
                $modelProduct = new MstProduct();
                $modelProductSet = new MstProductSet();
                $datas = $modelProduct->getDataReturnProduct($productCode);
                $dataChild = $modelProductSet->getInfoProductSet($productCode);
                $productInfo = ['is_add' => 1, 'quantity' => $request->order_num];
                foreach ($datas as $value) {
                    $productInfo['product_code'] = $productCode;
                    $productInfo['product_name'] = $value->product_name;
                    $productInfo['return_price'] = $value->price_invoice;
                    $productInfo['return_tanka'] = $value->price_invoice;
                    $productInfo['address_name'] = $value->address;
                    $productInfo['supplier_id']  = $value->price_supplier_id;
                    $productInfo['return_id']    = $value->return_id;
                    $child = [];
                    foreach ($dataChild as $key => $item) {
                        $child[$key]['product_code']  = $item->product_code;
                        $child[$key]['product_name']  = $item->product_name;
                        $child[$key]['price_invoice'] = $item->price_invoice;
                        $child[$key]['component_num'] = $item->component_num;
                    }
                    $productInfo['child'] = $child;
                }
                $arrProducts[] = $productInfo;
            }

            $arrDefault = [
                'product_code'   => '',
                'product_name'   => '',
                'quantity'       => '',
                'return_price'   => '',
                'return_tanka'   => '',
                'supplier_id'    => '',
                'is_add'         => 1,
            ];
            $arrProducts[] = $arrDefault;
            return response()->json([
                'status'                  => $statusProcess,
                'data'                    => $data ? $data : [],
                'dataMid'                 => array_merge($arrDefaultOption, $dataMid->toArray()),
                'dataLarge'               => array_merge($arrDefaultOption, $dataLarge->toArray()),
                'dataProduct'             => $arrProducts,
                'time'                    => time(),
                'default'                 => $arrDefault,
            ]);
        }
        return view('return-management.save_product');
    }

    /**
     * Get info product from product_code
     *
     * @param   $request  Request
     * @return json
     */
    public function getDataProductInput(Request $request)
    {
        if ($request->ajax() === true) {
            $productCode  = $request->input('product_code', null);
            $modelRL      = new BackendDtReceiptLedger();
            // $ediOrderCode = $request->input('edi_order_code', null);
            // if (!empty($ediOrderCode)) {
            //     $priceInvoice = $modelRL->getPriceInvoice($ediOrderCode, $productCode);
            //     if (!empty($priceInvoice)) {
            //         $priceInvoice = $priceInvoice->price_invoice;
            //     } else {
            //         return response()->json([
            //             'productMessage' => __('messages.product_not_exists'),
            //             'show_popup'     => 1,
            //         ]);
            //     }
            // }
            $modelProduct = new MstProduct();
            $modelProductSet = new MstProductSet();
            $datas = $modelProduct->getDataReturnProduct($productCode);
            $dataChild = $modelProductSet->getInfoProductSet($productCode);
            $productInfo = [];
            foreach ($datas as $value) {
                if (empty($ediOrderCode)) {
                    $priceInvoice = $value->price_invoice;
                }
                $productInfo['product_code']   = $productCode;
                $productInfo['product_name']   = $value->product_name;
                $productInfo['return_price']   = $priceInvoice;
                $productInfo['return_tanka']   = $priceInvoice;
                $productInfo['address_name']   = $value->address;
                $productInfo['supplier_id']    = $value->price_supplier_id;
                $productInfo['return_id']      = $value->return_id;
                $productInfo['edi_order_code'] = $value->edi_order_code;
                $child = [];
                foreach ($dataChild as $key => $item) {
                    $child[$key]['product_code']  = $item->product_code;
                    $child[$key]['product_name']  = $item->product_name;
                    $child[$key]['price_invoice'] = $item->price_invoice;
                    $child[$key]['component_num'] = $item->component_num;
                }
                $productInfo['child'] = $child;
            }
            if (count($productInfo) === 0) {
                return response()->json([
                    'productMessage' => __('messages.product_not_exists'),
                ]);
            }
            return response()->json([
                'productInfo' => $productInfo,
            ]);
        }
    }
    /**
     * Process Mojax
     *
     * @param   $request  Request
     * @return json
     */
    public function processMojax(Request $request)
    {
        if ($request->ajax() === true) {
            $modelP   = new MstProduct();
            $modelDR  = new DtReturn();
            $modelD   = new DtDelivery();
            $modelDD  = new DtDeliveryDetail();
            $modelKey = new TInsertShippingGroupKey();
            $datas    = $modelP->getDataProcessMojax();
            $count    = 0;
            if (count($datas) === 0) {
                return response()->json([
                    'msg' => 'No data',
                ]);
            } else {
                try {
                    DB::beginTransaction();
                    $arrUpdateAfterSave = [];
                    $arrFirst = [];
                    $arrInserted = [];
                    foreach ($datas as $value) {
                        $ShippingInstructions = '「送り状取消し」';
                        if (!empty($value->note)) {
                            $ShippingInstructions .= $value->note;
                        }
                        $receivedOrderId = 'MOJAX-' . $value->return_no . $value->return_line_no . $value->return_time;
                        $modelKey->where('tShippingG_Key', '=', 1)->increment('GroupKey', 1);
                        $groupKey       = $modelKey->where('tShippingG_Key', '=', 1)->first();
                        $detailLineNum  = 1;
                        $subdivisionNum = $value->delivery_count + 1;
                        $arrInsD = [];
                        $arrInsD['received_order_id']     = $receivedOrderId;
                        $arrInsD['subdivision_num']       = $subdivisionNum;
                        $arrInsD['delivery_date']         = null;
                        $arrInsD['delivery_time']         = '';
                        $arrInsD['delivery_instrustions'] = empty($value->note) ? null : $value->note;
                        $arrInsD['account']               = '';
                        $arrInsD['delivery_plan_date']    = date('Ymd');
                        $arrInsD['total_price']           = 0;
                        $arrInsD['shipping_instructions'] = $ShippingInstructions;
                        $arrInsD['store_code']            = 1;
                        $arrInsD['invoice_remarks']       = null;
                        $arrInsD['delivery_code']         = $value->delivery_code;
                        $arrInsD['customer_name']         = '株式会社mojax';
                        $arrInsD['delivery_name']         = 'GLSロジスティクスセンター【大都行】';
                        $arrInsD['delivery_postal']       = '5590033';
                        $arrInsD['delivery_perfecture']   = '大阪府';
                        $arrInsD['delivery_city']         = '大阪市住之江区南港中7-1-43';
                        $arrInsD['delivery_sub_address']  = '';
                        $arrInsD['delivery_tel']          = '8041265727';
                        $arrInsD['group_key']             = $groupKey->GroupKey;
                        $arrInsD['delivery_status']       = 0;
                        $arrInsD['in_part_cd']            = 2;
                        $arrInsD['in_date']               = now();
                        $arrInsD['in_ope_cd']             = Auth::user()->tantou_code;
                        $arrInsD['up_date']               = now();
                        $arrInsD['up_ope_cd']             = Auth::user()->tantou_code;
                        $modelD->insert($arrInsD);
                        $arrTemp = [];
                        $arrTemp['received_order_id'] = $receivedOrderId;
                        $arrTemp['subdivision_num']   = $subdivisionNum;
                        $arrTemp['detail_line_num']   = $detailLineNum;
                        $arrTemp['product_code']      = $value->product_code;
                        $arrTemp['product_name']      = $value->product_name_long;
                        $arrTemp['product_jan']       = $value->product_jan;
                        $arrTemp['maker_name']        = $value->maker_full_nm;
                        $arrTemp['maker_code']        = $value->product_maker_code;
                        $arrTemp['product_image_url'] = $value->rak_img_url_1;
                        $arrTemp['record_tag']        = null;
                        $arrTemp['delivery_num']      = $value->return_quantity;
                        $arrTemp['in_date']           = now();
                        $arrTemp['in_ope_cd']         = Auth::user()->tantou_code;
                        $arrTemp['up_date']           = now();
                        $arrTemp['up_ope_cd']         = Auth::user()->tantou_code;
                        $arrInsDD = [];
                        $arrInsDD[] = $arrTemp;
                        $arrTemp['detail_line_num']   = $detailLineNum + 1;
                        $arrTemp['product_code']      = 'Return-Dmy001';
                        $arrTemp['product_name']      = '返品出荷識別用';
                        $arrTemp['product_jan']       = '2016112109003';
                        $arrTemp['maker_name']        = null;
                        $arrTemp['maker_code']        = null;
                        $arrTemp['product_image_url'] = null;
                        $arrTemp['delivery_num']      = 1;
                        $arrInsDD[] = $arrTemp;
                        $modelDD->insert($arrInsDD);
                        $modelDR->updateData(
                            [
                                'return_no'      => $value->return_no,
                                'return_line_no' => $value->return_line_no,
                                'return_time'    => $value->return_time,
                            ],
                            [
                                'delivery_count'       => $subdivisionNum,
                                'delivery_instruction' => 1,
                                'supplier_return_no'   => $receivedOrderId,
                                'supplier_return_line' => $detailLineNum,
                                'status'               => 2,
                                'up_date'              => now(),
                                'up_ope_cd'            => Auth::user()->tantou_code,
                            ]
                        );
                        $count++;
                    }
                    DB::commit();
                } catch (\Exception $e) {
                    DB::rollback();
                    return response()->json([
                        'msg' => $e->getMessage(),
                    ]);
                }
                return response()->json([
                    'msg' => "Process success $count records.",
                ]);
            }
        }
    }

    /**
     * process import data.
     * Return $object json
     */
    public function import(Request $request)
    {
        $type        = $request->input("type", "");
        $fileName    = $request->input("filename", "");
        $timeRun     = $request->input("timeRun", 0);
        $currPage    = $request->input("currPage", 0);
        $total       = $request->input("total", 0);
        $countErr    = $request->input("count_err", 0);
        $realFile    = $request->input("realFilename", "");
        $fileCorrect = $request->input("fileCorrect", "");
        $checkAfter  = (int)$request->input("checkAfter");
        $returnTypeMid  = (int)$request->input("return_type_mid");
        $flg         = false;
        $common      = new Common;
        //
        $info = $common->checkFile($fileName);
        if ($info["flg"] === 0) {
            return response()->json($info);
        } else {
            $readers = $info["msg"];
            if ($checkAfter === 0) {
                $check = $this->checkAfterImport(
                    $readers,
                    $currPage,
                    $total,
                    $fileName,
                    $returnTypeMid,
                    (int)$countErr
                );
                $arrProcess = array(
                    "fileName"     => $fileName,
                    "timeRun"      => $timeRun,
                    "currPage"     => $currPage,
                    "total"        => $total,
                    "realFilename" => $realFile,
                    "fileCorrect"  => $fileCorrect,
                );
                $infoCheck = $common->processDataCheck($check, $arrProcess);
                return response()->json($infoCheck);
            }
        }
        $table = '';
        if ($type === 'import_mojax_csv') {
            $flg = $this->processMojaxCsv(
                $readers,
                $currPage,
                $total,
                $fileName,
                (int)$countErr,
                $realFile,
                $fileCorrect,
                $returnTypeMid
            );
        }
        $arrProcess = array(
            "fileName"     => $fileName,
            "timeRun"      => $timeRun,
            "currPage"     => $currPage,
            "total"        => $total,
            "realFilename" => $realFile,
            "fileCorrect"  => $fileCorrect,
            "checkAfter"   => $checkAfter,
        );
        $infoRun = $common->processDataRun($flg, $arrProcess);
        return response()->json($infoRun);
    }

        /**
     * Process data in file csv for diy
     * @param   string  $readers file name in storage
     * @return  boolean
     */
    public function checkAfterImport($arr, $offset, $length, $fileName, $returnTypeMid, $countErr = 0)
    {
        $readers = array_slice($arr, $offset*$length, $length);
        if ($readers === null) {
            return false;
        }
        $totalColumn = 9;
        $arrError = array();
        $col =  [
            'deje_num',
            'return_date',
            'product_code',
            'quantity',
            'unit_price',
            'supplier_id',
            'return_address_id',
            'delivery_plan_date',
            'note',
        ];
        $num = 1;
        $rule = [
            'return_date'        => 'isValidDate',
            'delivery_plan_date' => 'isValidDate',
            'quantity'           => 'numeric|min:1',
            'product_code'       => 'checkProductCodeExists',
            'supplier_id'        => 'checkSupplierIdExists',
        ];
        if ($returnTypeMid === 0) {
            $rule['return_address_id'] = 'required|exists:mst_return_address,return_id';
            $rule['supplier_id']       = 'checkSupplierIdExists';
        }
        foreach ($readers as $key => $row) {
            $message = '';
            if (empty($row) || ($offset === '0' && $num === 1)) {
                $num++;
                continue;
            }
            $column   = str_getcsv($row, ",");
            if (count($column) !== $totalColumn) {
                $arrError[] = trim($row)  . ',-->' .  __('messages.number_column_not_match');
                continue;
            }
            $data = array_combine($col, $column);
            if (!empty($data['delivery_plan_date'])) {
                $data['delivery_plan_date'] = date('Y-m-d', strtotime($data['delivery_plan_date']));
            }
            if (!empty($data['return_date'])) {
                $data['return_date'] = date('Y-m-d', strtotime($data['return_date']));
            }
            $data['quantity'] = (int)$data['quantity'];
            $validator = Validator::make($data, $rule);
            if ($validator->fails()) {
                $errorVal = $validator->errors();
                $arrError[] = trim($row)  . ',-->' . implode(", ", array_column($errorVal->toArray(), 0));
            }
        }
        $Common = new Common;
        $nameCached  = explode(".", $fileName);
        $nameCached  = $nameCached[0];
        if (count($arrError) > 0) {
            if (!Cache::has($nameCached)) {
                $header = array_slice($arr, 0, 1);
                $arrError = array_merge($header, $arrError);
            }
            mb_convert_variables('SJIS', 'UTF-8', $arrError);
            $Common->updateCached($nameCached, implode("\r\n", $arrError));
        } elseif (Cache::has($nameCached)) {
            $Common->updateCached($nameCached, '');
        }
        return true;
    }
    /**
     * Process data in file csv for diy
     * @param   string  $readers file name in storage
     * @return  boolean
     */
    public function processMojaxCsv($arr, $offset, $length, $fileName, $countErr = 0, $realFile = "", $fileCorrect = "", $returnTypeMid = 0)
    {
        $modelRA = new MstReturnAddress();
        $modelSS = new MstStockStatus();
        $modelReturn = new DtReturn();
        $readers = array_slice($arr, $offset*$length, $length);
        if ($readers === null) {
            return false;
        }
        $arrInsert = array();
        $num = 1;
        $checkHead = false;
        $checkFail = false;
        if ($countErr >= 3) {
            if ($offset === '0') {
                $checkHead = true;
            }
            $checkFail = true;
        }
        $col =  [
            'deje_num',
            'return_date',
            'product_code',
            'quantity',
            'unit_price',
            'supplier_id',
            'return_address_id',
            'delivery_plan_date',
            'note',
        ];
        $maxNo = $modelReturn->getMaxNo();
        if (empty($maxNo->maxNo)) {
            $returnNo = '9900000';
        } else {
            $returnNo = $maxNo->maxNo;
        }
        try {
            DB::beginTransaction();
            foreach ($readers as $key => $row) {
                if (empty($row) || ($offset === '0' && $num === 1)) {
                    $num++;
                    continue;
                }
                $returnNo++;
                $column   = str_getcsv($row, ",");
                $column = array_map('trim', $column);
                $data = array_combine($col, $column);
                $returnTypeMidId = '';
                $isMojax         = '';
                $isRedVoucher    = '';
                if ($returnTypeMid === 1) {
                    $returnTypeMidId = 2;
                    $isMojax         = 0;
                    $isRedVoucher    = 0;
                } elseif ($returnTypeMid === 2) {
                    $returnTypeMidId = 3;
                    $isMojax         = 1;
                    $isRedVoucher    = -1;
                }
                $dataSS =  $modelSS->where(['product_code' => $data['product_code']])->first();
                $errorCode = null;
                $errorMessage = null;
                if (!empty($dataSS)) {
                    $dataSS = $dataSS->toArray();
                    if ($data['quantity'] > ($dataSS['nanko_num'] - $dataSS['order_zan_num'])) {
                        $errorCode = 'E001';
                        $errorMessage = '在庫数が不足';
                    }
                }
                $arrInsert[$key]['return_time']          = 1;
                $arrInsert[$key]['return_address_id']    = (!empty($data['return_address_id']) && $isMojax === 0) ? $data['return_address_id'] : 0;
                $arrInsert[$key]['return_no']            = $returnNo;
                $arrInsert[$key]['return_line_no']       = 1;
                $arrInsert[$key]['return_date']          = $data['return_date'] ? $data['return_date'] : null;
                $arrInsert[$key]['deje_no']              = $data['deje_num'];
                $arrInsert[$key]['return_type_large_id'] = 3;
                $arrInsert[$key]['return_type_mid_id']   = $returnTypeMidId;
                $arrInsert[$key]['status']               = 0;
                $arrInsert[$key]['receive_instruction']  = -1;
                $arrInsert[$key]['receive_result']       = -1;
                $arrInsert[$key]['delivery_instruction'] = 0;
                $arrInsert[$key]['delivery_result']      = -1;
                $arrInsert[$key]['red_voucher']          = $isRedVoucher;
                $arrInsert[$key]['parent_product_code']  = $data['product_code'];
                $arrInsert[$key]['product_code']         = null;
                $arrInsert[$key]['return_quantity']      = $data['quantity'];
                $arrInsert[$key]['return_price']         = $data['unit_price'];
                $arrInsert[$key]['return_tanka']         = $data['unit_price'];
                $arrInsert[$key]['supplier_id']          = $isMojax === 1 ? 0 :$data['supplier_id'];
                $arrInsert[$key]['receive_plan_date']    = null;
                $arrInsert[$key]['receive_real_date']    = null;
                $arrInsert[$key]['receive_real_num']     = 0;
                $arrInsert[$key]['delivery_plan_date']   = $data['delivery_plan_date'] ? $data['delivery_plan_date'] : null;
                $arrInsert[$key]['delivery_real_date']   = null;
                $arrInsert[$key]['delivery_real_num']    = 0;
                $arrInsert[$key]['receive_id']           = null;
                $arrInsert[$key]['payment_on_delivery']  = 0;
                $arrInsert[$key]['note']                 = $data['note'];
                $arrInsert[$key]['receive_count']        = 0;
                $arrInsert[$key]['delivery_count']       = 0;
                $arrInsert[$key]['is_mojax']             = $isMojax;
                $arrInsert[$key]['error_code']           = $errorCode;
                $arrInsert[$key]['error_message']        = $errorMessage;
                $arrInsert[$key]['in_ope_cd']            = Auth::user()->tantou_code;
                $arrInsert[$key]['in_date']              = now();
                $arrInsert[$key]['up_ope_cd']            = Auth::user()->tantou_code;
                $arrInsert[$key]['up_date']              = now();
                if ($isMojax === 1) {
                    if (!empty($dataSS)) {
                        if (($dataSS['nanko_num'] - $dataSS['order_zan_num'] - $dataSS['return_num']) > $data['quantity']) {
                            $arrUpdateSS = [
                                'return_num' => DB::raw("return_num + {$data['quantity']}"),
                                'wait_delivery_num' => DB::raw("wait_delivery_num + {$data['quantity']}"),
                            ];
                            $modelSS->where('product_code', $data['product_code'])
                                    ->update($arrUpdateSS);
                        } else {
                            $errorCode = 'E001';
                            $errorMessage = '在庫数が不足';
                        }
                    }

                }

            }
            if (count($arrInsert) !== 0) {
                $modelReturn->insert($arrInsert);
            }
            DB::commit();
        } catch (Exception $e) {
            DB::rollback();
        }
        return true;
    }
    /**
     * Get data address
     * @param   Request $request
     * @return  json
     */
    public function getAddress(Request $request)
    {
        $modelProduct = new MstProduct();
        $currentData = $request->dataProduct;
        $arrProductCode = array_column($currentData, 'product_code');
        $datasAdress = $modelProduct->getDataAdressProduct($arrProductCode)->keyBy(function ($item) {
            return strtoupper($item['product_code']);
        });
        foreach ($request->dataProduct as $key => $value) {
            $productCode = strtoupper($value['product_code']);
            if (isset($datasAdress[$productCode])) {
                $currentData[$key]['supplier_id']  = $datasAdress[$productCode]['price_supplier_id'];
                $currentData[$key]['address_name'] = $datasAdress[$productCode]['address'];
                $currentData[$key]['return_id']    = $datasAdress[$productCode]['return_id'];
            }
        }
        return response()->json([
            'dataProduct' => $currentData,
        ]);
    }
    /**
     * Delete data
     * Return $object json
     */
    public function deleteItem(Request $request)
    {
        if ($request->ajax() === true && $request->input('index_key') !== '') {
            $index = $request->input('index_key');
            $modelDtReturn = new DtReturn();
            try {
                DB::beginTransaction();
                $modelDtReturn->where($index)->delete();
                DB::commit();
                return response()->json([
                    'status' => 1
                ]);
            } catch (\Exception $e) {
                DB::rollback();
                $message = $e->getPrevious()->getMessage();
                return response()->json([
                    'error' => "ERROR: $message.",
                    'status'=> 0
                ]);
            }
        }
    }
    /**
     * Delete data
     * Return $object json
     */
    public function deleteAllItem(Request $request)
    {
        if ($request->ajax() === true) {
            $checkbox = $request->input('return_index_checkbox');

            if (count($checkbox) > 0) {
                $modelDtReturn = new DtReturn();
                foreach ($checkbox as $key => $value) {
                    $values = explode('_', $value);
                    $arrayKey = ['return_no', 'return_line_no', 'return_time'];
                    $where = array_combine($arrayKey, $values);
                    try {
                        DB::beginTransaction();
                        $modelDtReturn->where($where)->delete();
                        DB::commit();
                    } catch (\Exception $e) {
                        DB::rollback();
                        $message = $e->getPrevious()->getMessage();
                        return response()->json([
                            'error' => "ERROR: $message.",
                            'status'=> 0
                        ]);
                    }
                }
                return response()->json([
                    'status' => 1
                ]);

            }
            return false;
        }
        return false;
    }
    /**
     * Update flag
     * Return $object json
     */
    public function changeFlag(Request $request)
    {
        if ($request->ajax() === true && $request->input('index_key') !== '') {
            $modelDtReturn = new DtReturn();
            $dataWhere = [
                'return_no'         => $request->input('return_no'),
                'return_line_no'    => $request->input('return_line_no'),
                'return_time'       => $request->input('return_time')
            ];
            $dataUpdate = [
                'red_voucher' =>  $request->input('red_voucher')
            ];
            if ($request->input('red_voucher') === 2) {
                $dataUpdate['status'] = 4;
            } else {
                $dataUpdate['status'] = 3;
            }
            try {
                DB::beginTransaction();
                $modelDtReturn->updateData($dataWhere, $dataUpdate);
                DB::commit();
                return response()->json([
                    'status' => 1
                ]);
            } catch (\Exception $e) {
                DB::rollback();
                $message = $e->getPrevious()->getMessage();
                return response()->json([
                    'error' => "ERROR: $message.",
                    'status'=> 0
                ]);
            }
        }
    }

    /**
     * get data option return detail
     * @return $object json
     */
    public function getOptionReturnDetail(Request $request)
    {
        $modelMRD = new MstReturnDetail();
        $returnTypeAllPotion = $modelMRD->getData($request->return_id);
        $default = [['key' => '_none', 'value' => '-----']];
        if ($returnTypeAllPotion->count() === 0) {
            $returnTypeAllPotion = $default;
        } else {
            $returnTypeAllPotion = array_merge($default, $returnTypeAllPotion->toArray());
        }
        return response()->json([
            'returnTypePotion' => $returnTypeAllPotion,
        ]);
    }
}
