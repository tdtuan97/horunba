<?php
/**
 * Settings Controller
 *
 * @package     App\Controllers
 * @subpackage  SettingsController
 * @copyright   Copyright (c) 2017 RiverCrane! Corporation. All Rights Reserved.
 * @author      le.hung<le.hung.rcvn2012@gmail.com>
 */

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Backend\MstOrderStatus;
use Validator;

class SettingsController extends Controller
{
    protected $error = [];
    /**
     * Get form data
     *
     * @param   $request  Request
     * @return  json
     */
    public function orderStatusFormData(Request $request)
    {
        if ($request->ajax() === true) {
            $mstOrderStatus    = new MstOrderStatus();
            $orderStatus = $mstOrderStatus->getData();
            return response()->json([
                    'data'          => $orderStatus,
            ]);
        }
         return view('settings.index');
    }
    /**
     * Save order status settings
     *
     * @param   $request  Request
     * @return json
     */
    public function save(Request $request)
    {
        if ($request->ajax() === true) {
            $params = $request->all();
            $rules = [];
            $data  = [];
            $mstOrderStatus     = new MstOrderStatus();
            $orderStatus        = $mstOrderStatus->getData();
            $origanalData       = '';
            if (count($params['delay_priod']) > 0) {
                $origanalData = $orderStatus->toArray();
                foreach ($params['delay_priod'] as $key => $value) {
                    $rules = [
                       'delay_priod_'.$key => 'required|numeric|min:0',
                       'expiration_priod_'.$key => 'required|numeric|min:0',
                    ];
                    $data = [
                                'delay_priod_'.$key => $value,
                                'expiration_priod_'.$key => $params['expiration_priod'][$key],
                            ];
                    $origanalData['data'][$key]['delay_priod'] = $value;
                    $origanalData['data'][$key]['expiration_priod'] = $params['expiration_priod'][$key];
                    $validator = Validator::make($data, $rules);
                    if ($validator->fails()) {
                        $this->error[$key] = $validator->errors()->toArray();
                    }
                }
            }
            $flg  = [];
            if (count($this->error) > 0) {
                return response()->json([
                    'status'  => 0,
                    'method'  => 'save',
                    'data'    => $origanalData,
                    'message' => $this->error
                ]);
            } else {
                $data = null;
                foreach ($params['delay_priod'] as $key => $value) {
                    $data['delay_priod']        = $value;
                    $data['expiration_priod']   = $params['expiration_priod'][$key];
                    $key    = ['order_status_id' => $key];
                    $flg[]  = $mstOrderStatus->updateData($key, $data);
                }
            }
            $data   = $mstOrderStatus->getData();
            return response()->json([
                    'status'  => 0,
                    'method'  => 'save',
                    'data'    => $data,
            ]);
        }
        return view('setting.index');
    }
}
