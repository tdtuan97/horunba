<?php
/**
 * Change password controller
 *
 * @package    App\Http\Controllers\Auth
 * @subpackage ChangePasswordController
 * @copyright  Copyright (c) 2017 RiverCrane! Corporation. All Rights Reserved.
 * @author     Truong Nghia<truong.van.nghia@rivercrane.vn>
 */

namespace App\Http\Controllers\Auth;

use Illuminate\Auth\Events\PasswordReset;
use Illuminate\Support\Facades\Password;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use App\Models\Backend\MstTantou;
use Validator;
use Redirect;

class ChangePasswordController extends Controller
{

    public $message = [
        'tantou_password.min' => 'パスワードの桁数が6桁以上ご設定ください。',
    ];
    /**
     * Where to redirect users after change their password.
     *
     * @var string
     */
    protected $redirectTo = '/logout';

    /**
     * Display the password reset view for the given token.
     *
     * If no token is present, display the link request form.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  string|null  $token
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function showChangeForm()
    {
        return view('auth.passwords.change');
    }

    /**
     * Reset the given user's password.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\JsonResponse
     */
    public function change(Request $request)
    {
        $check = Validator::make($request->all(), $this->rules(), $this->message);
        if ($check->fails()) {
            return response()->json([
                'status'  => 0,
                'message' => $check->errors()
            ]);
        } else {
            if (password_verify($request->old_tantou_password, Auth::user()->tantou_password)) {
                $this->changePassword($request->tantou_password);
                return response()->json([
                    'status'  => 0,
                    'redirect' => true,
                ]);
            } else {
                return response()->json([
                    'status'  => 0,
                    'message' => (object) array('old_tantou_password' => 'Please input password correct'),
                ]);
            }
        }
    }

    /**
     * Get the password reset validation rules.
     *
     * @return array
     */
    protected function rules()
    {
        return [
            'old_tantou_password' => 'required',
            'tantou_password' => 'required|min:6'
                . '|regex:/^.*(?=.{1,})(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[\d\X]).*$/',
            'tantou_password_confirmation' => 'required|same:tantou_password',
        ];
    }

        /**
     * Reset the given user's password.
     *
     * @param  \Illuminate\Contracts\Auth\CanResetPassword  $user
     * @param  string  $password
     * @return void
     */
    protected function changePassword($password)
    {
        $userId = Auth::User()->tantou_code;
        $user = MstTantou::find($userId);
        $user->tantou_password = Hash::make($password);
        $user->last_renew_pass = date('Y-m-d H:i:s');

        $user->setRememberToken(Str::random(60));

        $user->save();

        $this->guard()->logout();
    }

    /**
     * Get the guard to be used during password reset.
     *
     * @return \Illuminate\Contracts\Auth\StatefulGuard
     */
    protected function guard()
    {
        return Auth::guard();
    }
}
