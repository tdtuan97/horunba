<?php
/**
 * Controller for Receipt pdf
 *
 * @package    App\Http\Controllers
 * @subpackage ReceiptPDFController
 * @copyright  Copyright (c) 2018 RiverCrane! Corporation. All Rights Reserved.
 * @author     Van Qui<van.qui@rivercrane.vn>
 */

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Models\Backend\MstOrder;
use App\Models\Backend\MstCustomer;
use Auth;

class ReceiptPDFController extends Controller
{
    /**
     * Receipt displays form view and download pdf.
     *
     * @param object $request contains request
     * @return view
     */
    public function receipt($mailSerial, Request $request)
    {
        $params['mail_serial'] = $mailSerial;

        $modelO   = new MstOrder();
        $modelC   = new MstCustomer();
        $data     = $modelO->getReceipt($mailSerial);
        if ($data->count() === 0) {
            return view('receipt-pdf.none')->with(['type' => 'mail_serial_invalid']);
        }

        $dlDate = date('Y-m-d', strtotime($data[0]->shipment_date . ' +90 days'));
        $now    = date('Y-m-d');
        $status = $data[0]->pdf_status;
        if ($dlDate < $now || is_null($data[0]->shipment_date)) {
            return view('receipt-pdf.none')->with(['type' => 'download_expired']);
        }

        if ($request->isMethod('post')) {
            $isDownload = true;
            if ((int)$status !== 1) {
                $input['proviso']      = $request->input('proviso', '');
                $input['full_name']    = $request->input('full_name', '');
                if ($request->has('receipt_date') && Auth::check()) {
                    $input['receipt_date'] = $request->input('receipt_date', '');
                    $data[0]->receipt_date = $input['receipt_date'];
                }

                $pattern                 = "/(^\s+)|(\s+$)/us";
                $inputCheck['proviso']   = preg_replace($pattern, "", $input['proviso']);
                $inputCheck['full_name'] = preg_replace($pattern, "", $input['full_name']);
                $validator = Validator::make($inputCheck, [
                    'proviso'      => 'required|max:100',
                    'full_name' => 'required|max:100',
                ]);

                $data[0]->proviso      = $input['proviso'];
                $data[0]->full_name    = $input['full_name'];
                if ($validator->fails()) {
                    $isDownload       = false;
                    $params['errors'] = $validator->errors()->all();
                } else {
                    $upOrder = [
                        'pdf_status'   => 1,
                        'proviso'   => $input['proviso'],
                        'up_date'      => now()
                    ];
                    if ($request->has('receipt_date') && Auth::check()) {
                        $upOrder['receipt_date'] = $input['receipt_date'];
                    }
                    $upCustomer = [
                        'full_name' => $input['full_name'],
                        'up_date'   => now()
                    ];
                    $data[0]->status = 1;
                    $modelO->updateData(['receive_id'  => $data[0]->receive_id], $upOrder);
                    $modelC->updateData(['customer_id' => $data[0]->customer_id], $upCustomer);
                }
            }
            if ($isDownload) {
                if (empty($data[0]->receipt_date)) {
                    $data[0]->receipt_date = $data[0]->shipment_date;
                }
                $dataView = view('receipt-pdf.export_pdf')->with(['data' => $data]);
                $pdf      = \PDF::loadView('receipt-pdf.export_pdf', compact('data'));
                $pdf->setOptions(['isFontSubsettingEnabled' => true]);
                $pdf->setPaper('A4', 'portrait');
                $fileName = date('YmdHis') . '_' . $request->mail_serial . '.pdf';
                return $pdf->download($fileName);
            }
        }

        if (empty($data[0]->receipt_date)) {
            $data[0]->receipt_date = $data[0]->shipment_date;
        }

        $params['data'] = $data;
        if ($dlDate >= $now && (int)$status === 1) {
            $params['popup'] = true;
        }



        return view('receipt-pdf.form')->with($params);
    }
}
