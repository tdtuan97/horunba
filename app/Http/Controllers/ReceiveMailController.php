<?php
/**
 * Controller for receive mail management
 *
 * @package    App\Http\Controllers
 * @subpackage ReceiveMailController
 * @copyright  Copyright (c) 2018 RiverCrane! Corporation. All Rights Reserved.
 * @author     Van Qui<van.qui@rivercrane.vn>
 */

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Backend\DtReceiveMailList;
use App\Models\Backend\MstTantou;
use App\Models\Backend\MstOrder;
use App\Models\Backend\DtSendMailList;
use App\Models\Backend\MstInquiryReason;
use Validator;
use Auth;
use Config;
use URL;
use DB;
use App\Custom\Utilities;
use App\Custom\NextPrevBar;

class ReceiveMailController extends Controller
{
    /**
     * Load view.
     */
    public function index()
    {
        return view('receive-mail.index');
    }

    /**
     * Get all data.
     * Return $object json
     */
    public function dataList(Request $request)
    {
        if ($request->ajax() === true) {
            $dtReceiveMailList = new DtReceiveMailList();
            $mstTantou         = new MstTantou();
            $statusConf        = Config::get('common.receive_mail_status');
            $isReplyConf       = Config::get('common.flg');
            $deparmentConf     = Config::get('common.receive_mail_deparment');

            foreach ($statusConf as $key => $value) {
                $status[] = ['key' => $key, 'value' => $value];
            }

            foreach ($deparmentConf as $value) {
                $deparment[] = ['key' => $value, 'value' => $value];
            }

            $tantou[] = ['key' => '', 'value' => '-----'];
            foreach ($mstTantou->get(['tantou_last_name']) as $item) {
                $tantou[] = ['key' => $item['tantou_last_name'], 'value' => $item['tantou_last_name']];
            }

            $arrSort = [
                'rec_mail_index'  => $request->input('sort_rec_mail_index', null),
                'status'          => $request->input('sort_status', null),
                'deparment'       => $request->input('sort_deparment', null),
                'incharge_person' => $request->input('sort_incharge_person', null),
                'mail_subject'    => $request->input('sort_mail_subject', null),
                'mail_content'    => $request->input('sort_mail_content', null),
                'receive_date'    => $request->input('sort_receive_date', 'desc'),
                'mail_from'       => $request->input('sort_mail_from', null),
            ];
            $rule = [
                'receive_date_from' => 'nullable|date',
                'receive_date_to'   => 'nullable|date'
            ];
            $arrSearch = [
                'status'            => $request->input('status', 0),
                'is_reply'          => $request->input('is_reply', null),
                'deparment'         => $request->input('deparment', null),
                'incharge_person'   => $request->input('incharge_person', null),
                'mail_subject'      => $request->input('mail_subject', null),
                'receive_date_from' => $request->input('receive_date_from', null),
                'receive_date_to'   => $request->input('receive_date_to', null),
                'mail_from'         => $request->input('mail_from', null),
                'mail_to'           => $request->input('mail_to', null),
                'per_page'          => $request->input('per_page', null)
            ];

            $validator = Validator::make($arrSearch, $rule);
            if ($validator->fails()) {
                return response()->json([
                    'data'          => [],
                    'message'       => $validator->errors()
                ]);
            }
            $data = $dtReceiveMailList->getData($arrSearch, $arrSort);
            return response()->json([
                'data'          => $data,
                'isReplyConf'   => $isReplyConf,
                'statusData'    => $status,
                'deparmentData' => $deparment,
                'testing'       => $arrSearch,
                'tantou'        => $tantou,
            ]);
        }
    }

    /**
     * Get form data
     *
     * @param  Request $request
     * @return ajax
     */
    public function getFormData(Request $request)
    {
        if ($request->ajax() === true) {
            $dtReceiveMailList = new DtReceiveMailList();
            $dtSendMailList    = new DtSendMailList();
            $modelIR           = new MstInquiryReason();
            $mstTantou         = new MstTantou();
            $statusConf        = Config::get('common.receive_mail_status');
            $deparmentConf     = Config::get('common.receive_mail_deparment');
            $inquiryConf       = $modelIR->getData();

            $status[] = ['key' => '', 'value' => '-----'];
            foreach ($statusConf as $key => $value) {
                $status[] = ['key' => $key, 'value' => $value];
            }

            $deparment[] = ['key' => '', 'value' => '-----'];
            foreach ($deparmentConf as $value) {
                $deparment[] = ['key' => $value, 'value' => $value];
            }

            $reason[] = ['key' => '', 'value' => '-----'];
            foreach ($inquiryConf as $value) {
                $reason[] = ['key' => $value->key, 'value' => $value->value];
            }

            $tantou[] = ['key' => '', 'value' => '-----'];
            foreach ($mstTantou->get(['tantou_last_name']) as $item) {
                $tantou[] = ['key' => $item['tantou_last_name'], 'value' => $item['tantou_last_name']];
            }

            $recMailIndex = $request->input('rec_mail_index', null);
            $dataReply = [];
            if (!empty($recMailIndex)) {
                $data = $dtReceiveMailList->getItem($recMailIndex);
                if (!empty($data)) {
                    if (strip_tags($data->mail_content) === $data->mail_content) {
                        $data->mail_content = str_replace(["\n", "\r\n"], "<br>", $data->mail_content);
                    }
                    if ($data->uid_mail !== 0) {
                        $dataReply = $dtSendMailList->getMailReply($data->uid_mail);
                    }
                }
            } else {
                $data = [];
            }

            return response()->json([
                'data'          => $data,
                'statusData'    => $status,
                'deparmentData' => $deparment,
                'reasonData'    => $reason,
                'tantouData'    => $tantou,
                'dataReply'     => $dataReply
            ]);
        }
    }

    /**
     * Get order
     *
     * @param  Request $request
     * @return json
     */
    public function getOrder(Request $request)
    {
        $mstOrder       = new MstOrder();
        $receiveOrderId = $request->input('receive_order_id', null);
        $data           = $mstOrder->getDataByReceivedOrderId($receiveOrderId);

        if ($request->input('type', '') === 'edit_send_mail') {
            $dtSendMailList = new DtSendMailList();
            $resMail = $dtSendMailList->where(['receive_order_id' => $receiveOrderId])->first();
            if (!empty($resMail)) {
                return response()->json([
                    'data'    => null,
                    'status'  => 0,
                    'message' => __('messages.email_send_created')
                ]);
            }
        }

        if (empty($data)) {
            return response()->json([
                'data'    => null,
                'status'  => 0,
                'message' => __('messages.order_not_exit')
            ]);
        }
        return response()->json([
            'data'   => $data,
            'status' => 1
        ]);
    }

    /**
     * Process save receive input
     *
     * @param  object $request
     * @return json
     */
    public function save(Request $request)
    {
        if ($request->ajax() === true) {
            $recMailIndex   = $request->input('rec_mail_index');
            $receiveOrderId = $request->input('receive_order_id', null);
            $status         = $request->input('status', null);
            $inquiryCode    = $request->input('inquiry_code', null);
            $deparment      = $request->input('deparment', null);
            $inchargePerson = $request->input('incharge_person', null);
            $csNote         = $request->input('cs_note', null);

            $dtReceiveMailList = new DtReceiveMailList();
            $statusConf        = Config::get('common.receive_mail_status');
            $deparmentConf     = Config::get('common.receive_mail_deparment');
            $inquiryConf       = Config::get('common.receive_mail_reason');
            $rules = [
                'rec_mail_index_key'   => 'exists:horunba.dt_receive_mail_list,rec_mail_index',
                'status'           => 'in:' . implode(",", array_keys($statusConf)),
                'inquiry_code'     => 'exists:horunba.mst_inquiry_reason,inquiry_code',
                'deparment'        => 'in:' . implode(",", $deparmentConf),
                'incharge_person'  => 'exists:horunba.mst_tantou,tantou_last_name'
            ];
            if (!empty($receiveOrderId)) {
                 $rules['receive_order_id'] = 'exists:horunba.mst_order,received_order_id';
            }
            $validator = Validator::make($request->all(), $rules);
            if ($validator->fails()) {
                return response()->json([
                    'status'  => 0,
                    'message' => $validator->errors(),
                    'data'    => []
                ]);
            }
            $receiveMail                   = $dtReceiveMailList->find($recMailIndex);
            $receiveMail->receive_order_id = $receiveOrderId;
            $receiveMail->status           = $status;
            $receiveMail->inquiry_code     = $inquiryCode;
            $receiveMail->cs_note          = $csNote;
            $receiveMail->deparment        = $deparment;
            $receiveMail->incharge_person  = $inchargePerson;
            $receiveMail->up_ope_cd        = Auth::user()->tantou_code;
            $receiveMail->up_date          = date('Y-m-d H:i:s');
            try {
                $receiveMail->save();
            } catch (Exception $ex) {
                return response()->json([
                    'status'  => -1,
                    'message' => $ex->getMessage()
                ]);
            }

            return response()->json([
                'status' => 1,
            ]);
        }
        $back   = Utilities::getBackLink($request, 'receive_mail_save', url()->previous());
        $arrUrl = explode("/", parse_url($back, PHP_URL_PATH));
        if (isset($arrUrl[1]) && $arrUrl[1] === 'receive-mail') {
            $indexAction  = action('ReceiveMailController@index');
            $detailAction = action('ReceiveMailController@save');
            $idNameKeys   = ['rec_mail_index'];

            $sortKeys = [
                'rec_mail_index',
                'status',
                'deparment',
                'incharge_person',
                'mail_subject',
                'mail_content',
                'receive_date' => 'desc',
                'mail_from',
            ];
            $searchKeys = [
                'status',
                'deparment',
                'incharge_person',
                'mail_subject',
                'receive_date_from',
                'receive_date_to',
                'mail_from',
                'mail_to',
                'per_page'
                ];
            $model  = new DtReceiveMailList();
            $params = NextPrevBar::generateLink(
                $indexAction,
                $detailAction,
                $idNameKeys,
                $searchKeys,
                $sortKeys,
                $model
            );
            if (gettype($params) === 'object') {
                return $params;
            }
        } else {
            $params = [
                'pageOnPages' => trans('messages.pos_per_page', ['pos' => 1, 'total' => 1]),
                'nextUrl'  => null,
                'prevUrl'  => null
            ];
        }
        $params['indexUrl'] = null;
        $params['backUrl']  = $back;
        return view('receive-mail.save', $params);
    }

    /**
     * Load content from db to iframe
     *
     * @param  Request $request
     * @return view
     */
    public function iframe(Request $request)
    {
        \Debugbar::disable();
        $dtReceiveMailList = new DtReceiveMailList();
        $recMailIndex = $request->input('rec_mail_index', null);
        $data = [];
        if (!empty($recMailIndex)) {
            $data = $dtReceiveMailList->getItem($recMailIndex);
        }
        return view('iframe.iframe_mail_receive', ['content' => $data->mail_content]);
    }
    /**
     * Update data when change in grid view
     *
     * @param  Request $request
     * @
     */
    public function updateData(Request $request)
    {
        if ($request->ajax() === true) {
            $dtReceiveMailList = new DtReceiveMailList();
            $arrUpdate = [];
            $arrWhere  = [];
            if ($request->name === 'status_list') {
                $arrUpdate['status'] = $request->value;
                $arrWhere['rec_mail_index'] = $request->rec_mail_index;
            } elseif ($request->name === 'deparment_list') {
                $arrUpdate['deparment'] = $request->value;
                $arrWhere['rec_mail_index'] = $request->rec_mail_index;
            } elseif ($request->name === 'incharge_person_list') {
                $arrUpdate['incharge_person'] = $request->value;
                $arrWhere['rec_mail_index'] = $request->rec_mail_index;
            } elseif ($request->name === 'update_status_4') {
                if (!empty($request->input('rec_mail_index_checkbox', null))) {
                    $dtReceiveMailList::whereIn('rec_mail_index', $request->input('rec_mail_index_checkbox'))
                            ->update(['status' => 4]);
                }
            }
            if (!empty($arrUpdate) && !empty($arrWhere)) {
                $dtReceiveMailList->updateData($arrWhere, $arrUpdate);
            }
        }
    }
}
