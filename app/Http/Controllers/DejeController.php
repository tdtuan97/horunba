<?php
/**
 * Deje Controller
 *
 * @package     App\Controllers
 * @subpackage  DejeController
 * @copyright   Copyright (c) 2017 RiverCrane! Corporation. All Rights Reserved.
 * @author      le.hung<le.hung.rcvn2012@gmail.com>
 */

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Events\Command as eCommand;
use App\Models\Backend\MstBusinessStyle;
use Auth;
use Config;
use Validator;
use App\Models\Backend\MstGenre;
use App\Models\Backend\MstMailParameter;
use App\Models\Backend\DtDeje;
use App\Models\Backend\DtDejeComment;
use App\Models\Backend\DtEcuRequestData;
use App\Models\Backend\MstTantou;
use App\Models\Backend\DtOrderToSupplier;
use App\Models\Backend\MstOrder;
use App\Models\Backend\MstProduct;
use App\Custom\Utilities;
use Storage;
use File;
use PDF;
use App\User;

class DejeController extends Controller
{
    /**
     * Process add and edit
     * @param   $request  Request
     * @return json
     */
    public function getDataDetail(Request $request)
    {
        if ($request->ajax() === true) {
            $cmbObjectType[] = ['key' => '', 'value' => '------'];
            foreach (Config::get('common.cmb_object_type') as $key => $val) {
                $cmbObjectType[] = ['key' => $key, 'value' => $val];
            }
            $cmbProcessStatus[] = ['key' => '', 'value' => '------'];
            foreach (Config::get('common.cmb_process_status') as $key => $val) {
                $cmbProcessStatus[] = ['key' => $key, 'value' => $val];
            }
            $cmbProcessMethod[] = ['key' => '', 'value' => '------'];
            foreach (Config::get('common.cmb_process_method') as $key => $val) {
                $cmbProcessMethod[] = ['key' => $key, 'value' => $val];
            }
            $dataTantou[] = ['key' => '', 'value' => '------'];
            foreach (MstTantou::whereIn('tantou_department', [1, 200, 201])->where('is_active', 1)->get() as $item) {
                $dataTantou[] = ['key' => $item['tantou_code'], 'value' => $item['tantou_last_name']];
            }
            $dataTantouAll[] = ['key' => '', 'value' => '------'];
            foreach (MstTantou::where('is_active', 1)->get() as $item) {
                $dataTantouAll[] = ['key' => $item['tantou_code'], 'value' => $item['tantou_last_name']];
            }
            $dataBusinesssTyle[] = ['key' => '', 'value' => '------'];
            foreach (MstBusinessStyle::orderBy('style_name', 'asc')->get(['style_id', 'style_name']) as $item) {
                $dataBusinesssTyle[] = ['key' => $item['style_id'], 'value' => $item['style_name']];
            }
            $modelD     = new DtDeje();
            $modelDC    = new DtDejeComment();
            $modeEcu    = new DtEcuRequestData();
            $dejeId     = $request->input('deje_id');
            $cloneFlg   = false;
            if (!empty($request->input('deje_clone_id'))) {
                $dejeId = $request->input('deje_clone_id');
                $cloneFlg   = true;
            }
            $data       = $modelD->find($dejeId);

            $process    = 'new';
            if (!empty($data)) {
                $dataTantouAll = MstTantou::where('is_active', 1)->get()->keyBy('tantou_code');
                $data->in_ope_cd = isset($dataTantouAll[$data->in_ope_cd]) ? $dataTantouAll[$data->in_ope_cd]->tantou_last_name . $dataTantouAll[$data->in_ope_cd]->tantou_first_name : '';
                $data->up_ope_cd = isset($dataTantouAll[$data->up_ope_cd]) ? $dataTantouAll[$data->up_ope_cd]->tantou_last_name . $dataTantouAll[$data->up_ope_cd]->tantou_first_name : '';
                $data       = $data->toArray();
                $data['order_id_export'] = $data['order_id'];
                unset($data['order_id']);
                if (!$cloneFlg) {
                    $process    = 'edit';
                } else {
                    unset($data['deje_id']);
                    unset($data['attach_file_1']);
                    unset($data['attach_file_2']);
                    unset($data['attach_file_3']);
                    unset($data['attach_file_4']);
                    unset($data['attach_file_5']);
                    unset($data['attach_file_6']);
                    unset($data['dai_tantou']);
                    unset($data['object_type']);
                    unset($data['process_method']);
                    $data['process_status'] = 3;
                }
                $arrAttachs = [1, 2, 3, 4, 5, 6];
                foreach ($arrAttachs as $value) {
                    if (!empty($data['attach_file_' . $value])) {
                        $data['attach_file_' . $value . '_before'] = $data['attach_file_' . $value];
                        $curFileName = $data['attach_file_' . $value];
                        $arrTmpName  = explode("_", $curFileName);
                        $prefixName  = 'attached_file_' . $arrTmpName[2] . '_';
                        $data['attach_file_' . $value . '_realname'] = str_replace($prefixName, "", $curFileName);
                    }
                }
            }
            $show = $request->input('show');
            $dataComment = null;
            $dataComment = $modelDC->getData(['deje_id' => $request->input('deje_id')]);
            $dataEcu     = $modeEcu->getDataByDejeId($request->input('deje_id'));

            return response()->json([
                'data'      => $data,
                'opeName'   => Auth::user()->tantou_first_name . " " . Auth::user()->tantou_last_name,
                'dataComment'      => $dataComment,
                'fileUrl'   => url('/open/file/'). '/',
                'action'    => 'detail',
                'cmbObjectType'     => $cmbObjectType,
                'cmbProcessStatus'  => $cmbProcessStatus,
                'cmbProcessMethod'  => $cmbProcessMethod,
                'dataTantou'        => $dataTantou,
                'dataTantouAll'     => $dataTantouAll,
                'dataBusinesssTyle' => $dataBusinesssTyle,
                'dataEcu'           => $dataEcu,
                'process'           => $process
            ]);
        }
        return view('deje.save');
    }

    /**
     * Get product price
     *
     * @param  Request $request
     * @return json
     */
    public function getProduct(Request $request)
    {
        if ($request->ajax() === true) {
            $productCode = $request->input('product_code', null);
            $modelP = new MstProduct();
            $product = $modelP->getProduct($productCode);
            if (empty($product)) {
                return null;
            }
            if (is_array($productCode)) {
                $product = $product->keyBy(function ($item) {
                    return strtoupper($item['product_code']);
                });
                $orderProduct = [];
                foreach ($productCode as $pro) {
                    $pro = strtoupper($pro);
                    if (isset($product[$pro])) {
                        $orderProduct[] = $product[$pro];
                    }
                }
                return json_encode($orderProduct);
            }
            return json_encode($product);
        }
    }

    /**
     * Process add and edit
     * @param   $request  Request
     * @return json
     */
    public function save(Request $request)
    {
        if ($request->ajax() === true) {
            $rules = [
                'subject'        => 'required',
                'business_stype' => 'required',
                'process_status' => 'required',
                'contents'       => 'required',
            ];
            $arrAttachs = [1, 2, 3, 4, 5, 6];
            $arrFiles   = [];
            $arrRemove  = [];
            $arrError   = [];
            foreach ($arrAttachs as $value) {
                if ($request->file('input_attach_file_' . $value)) {
                    $attachedFile    = $request->file('input_attach_file_' . $value);
                    $rules['input_attach_file_' . $value]    = 'max:20480'; //KB
                    $arrFiles[$value] = $attachedFile;
                    if (!empty($request->input('attach_file_' . $value . '_before', null))) {
                         $arrRemove[] = $request->input('attach_file_' . $value . '_before', null);
                    }
                } elseif (!empty($request->input('input_attach_file_' . $value)) && $request->input('input_attach_file_' . $value) === 'error') {
                    $field = '添付資料.' . $value;
                    $arrError['input_attach_file_' . $value] = ["The $field may not be greater than 20480 kilobytes."];
                } elseif (!empty($request->input('attach_file_' . $value . '_before', null)) && empty($request->input('attach_file_' . $value))) {
                    $arrFiles[$value] = null;
                    $arrRemove[] = $request->input('attach_file_' . $value . '_before', null);
                }
            }
            $validator = Validator::make($request->all(), $rules);
            if ($validator->fails()) {
                $arrError = array_merge($arrError, $validator->errors()->toArray());
            }

            $dataEcu = $request->input('dataEcu', null);
            $showEcu = $request->input('show_ecu', null);
            if (empty($dataEcu) || $showEcu === 'none') {
                $dataEcu = [];
            } else {
                $dataEcu = json_decode($dataEcu);
            }
            $ruleEcu = [
                'line_num'     => 'required',
                'product_code' => 'required|exists:mst_product,product_code',
                'quantity'     => 'required|numeric',
                'price_komi'   => 'required|numeric',
            ];
            $attName = [
                'product_code' => '商品番号',
                'quantity'     => '数量',
                'price_komi'   => '仕入金額(税込)',
            ];
            $ecuError = [];
            foreach ($dataEcu as $item) {
                $validatorEcu = Validator::make((Array)$item, $ruleEcu);
                $validatorEcu->setAttributeNames($attName);
                if ($validatorEcu->fails()) {
                    $ecuError[$item->line_num] = $validatorEcu->errors()->toArray();
                }
            }
            if (count($ecuError) > 0) {
                $arrError['ecuError'] = $ecuError;
            }
            if (count($arrError) !== 0) {
                return response()->json([
                    'status'  => 0,
                    'message' => $arrError,
                    'data'    => []
                ]);
            }
            $action = __('messages.update_success');
            $modelD = new DtDeje();
            if (!empty($request->input('deje_id'))) {
                $modelD = $modelD->find($request->input('deje_id'));
                $action = __('messages.insert_success');
            } else {
                $modelD->in_ope_cd = Auth::user()->tantou_code;
                $modelD->in_date   = now();
            }
            $destinationPath = storage_path('uploads/attached_file');
            foreach ($arrFiles as $key => $value) {
                if (!empty($value)) {
                    $extension                     = $value->getClientOriginalExtension();
                    $realName                      = pathinfo(' ' . $value->getClientOriginalName())['filename'];
                    $realName                      = substr($realName, 1);
                    $filename                      = 'attached_file_' . date('YmdHis') . $key
                        . '_' . $realName . '.' . $extension;
                    $modelD['attach_file_' . $key] = $filename;
                    $attachedFiles[]               = $filename;
                    $value->move($destinationPath, $filename);
                } else {
                    $modelD['attach_file_' . $key] = null;
                }
            }
            $modelD->subject        = $request->input('subject', null);
            $modelD->object_type    = $request->input('object_type', null);
            $modelD->process_status = $request->input('process_status', null);
            $modelD->dai_tantou     = $request->input('dai_tantou', null);
            $modelD->process_method = $request->input('process_method', null);
            $modelD->contents       = $request->input('contents', null);
            $modelD->business_stype = $request->input('business_stype', null);
            $modelD->order_id       = $request->input('order_id_export');
            $modelD->customer_name  = $request->input('customer_name');
            $modelD->up_ope_cd      = Auth::user()->tantou_code;
            $modelD->up_date        = now();
            $modelD->save();
            if (count($arrRemove) !== 0) {
                foreach ($arrRemove as $value) {
                    $file_path = $destinationPath . '/' . $value;
                    if (File::exists($file_path)) {
                        File::delete($file_path);
                    }
                }
            }

            $modeEcu = new DtEcuRequestData();
            $maxIdEcu = $modeEcu->where('deje_id', $modelD->deje_id)->max('line_num');
            if (count($dataEcu) > 0) {
                $ecuNew = [];
                foreach ($dataEcu as $item) {
                    if (!empty($item->item_type)) {
                        if ($item->item_type === 'new') {
                            $ecuNew[] = [
                                'deje_id'      => $modelD->deje_id,
                                'line_num'     => ++$maxIdEcu,
                                'product_code' => $item->product_code,
                                'quantity'     => $item->quantity,
                                'price_komi'   => $item->price_komi,
                                'in_ope_cd'    => Auth::user()->tantou_code,
                                'in_date'      => date('Y-m-d H:i:s'),
                                'up_ope_cd'    => Auth::user()->tantou_code,
                                'up_date'      => date('Y-m-d H:i:s')
                            ];
                        } elseif ($item->item_type === 'delete') {
                            $modeEcu->where('deje_id', $modelD->deje_id)
                                ->where('line_num', $item->line_num)
                                ->delete();
                        }
                    } else {
                        $modeEcu->where('deje_id', $modelD->deje_id)
                                ->where('line_num', $item->line_num)
                                ->update([
                                    'product_code' => $item->product_code,
                                    'quantity'     => $item->quantity,
                                    'price_komi'   => $item->price_komi,
                                    'up_ope_cd'    => Auth::user()->tantou_code,
                                    'up_date'      => date('Y-m-d H:i:s')
                                ]);
                    }
                }
                if (count($ecuNew) > 0) {
                    $modeEcu->insert($ecuNew);
                }
            }
            return response()->json([
                'status'  => 1,
                'method'  => 'save',
                'message' => $action
            ]);
        }
        $back   = Utilities::getBackLink($request, 'deje_save', url()->previous());

        $params['backUrl']  = $back;
        return view('deje.save', $params);
    }
    /**
     * Process display detail
     * @param   $request  Request
     * @return json
     */
    public function detail(Request $request)
    {
        if ($request->ajax() === true) {
            $rules = [
                'comment_content'        => 'required',
                'deje_id'                => 'required',
            ];
            $action = $request->input('action');
            if ($action === 'add') {
                $validator = Validator::make($request->all(), $rules);
                if ($validator->fails()) {
                    return response()->json([
                        'status'  => 0,
                        'message' => $validator->errors(),
                        'data'    => []
                    ]);
                }
            }
            $modelDC = new DtDejeComment();
            if ($action === 'add') {
                $modelDC->deje_id   = $request->input('deje_id', null);
                $modelDC->in_ope_cd = Auth::user()->tantou_code;
                $modelDC->in_date   = date("Y-m-d H:i:s");
                $modelDC->contents  = $request->input('comment_content', null);
            }

            try {
                if ($action === 'add') {
                    $modelDC->save();
                } elseif ($action === 'remove') {
                    $modelDC::find($request->input('comment_id', null))->delete();
                }
            } catch (\Exception $ex) {
                return response()->json([
                    'status'  => -1,
                    'message' => $ex->getMessage()
                ]);
            }
            $dataComment = null;
            $dataComment = $modelDC->getData(['deje_id' => $request->input('deje_id')]);
            return response()->json([
                'status'      => 1,
                'dataComment' => $dataComment,
            ]);
        }
        $back   = Utilities::getBackLink($request, 'deje_save', url()->previous());

        $params['backUrl']  = $back;
        return view('deje.detail', $params);
    }

    /**
     * Show template for append data
     *
     * @param   $request  Request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        return view('deje.index');
    }
    /**
     * Show data list view
     *
     * @param   $request  Request
     * @return json
     */
    public function dataList(Request $request)
    {
        if ($request->ajax() === true) {
            $arraySearch    = array(
                'deje_id'           => $request->input('deje_id', null),
                'subject'           => $request->input('subject', null),
                'object_type'       => $request->input('object_type', null),
                'process_status'    => $request->input('process_status', null),
                'dai_tantou'        => $request->input('dai_tantou', null),
                'business_stype'    => $request->input('business_stype', null),
                'process_method'    => $request->input('process_method', null),
                'contents'          => $request->input('contents', null),
                'search_all'        => $request->input('search_all', null),
                'per_page'          => $request->input('per_page', null),
            );
            $arraySort = [
                'deje_id'           => $request->input('sort_deje_id', null),
                'subject'           => $request->input('sort_subject', null),
                'object_type'       => $request->input('sort_object_type', null),
                'process_status'    => $request->input('sort_process_status', null),
                'dai_tantou'        => $request->input('sort_dai_tantou', null),
                'business_stype'    => $request->input('sort_business_stype', null),
                'process_method'    => $request->input('sort_process_method', null),
                'in_date'            => $request->input('sort_in_date', null),
                'up_date'            => $request->input('sort_up_date', null)
            ];
            $model                      = new DtDeje();
            $mstTantou                  = new MstTantou();
            $mstBusinessStyle           = new MstBusinessStyle();

            $objectType                 = Config::get('common.object_type');
            $processMethod              = Config::get('common.process_method');
            $processStatus              = Config::get('common.process_status');
            foreach ($mstTantou->get(['tantou_code','tantou_last_name']) as $item) {
                $tantou[] = ['key' => $item['tantou_code'], 'value' => $item['tantou_last_name']];
            }
            foreach ($mstBusinessStyle->get(['style_id', 'style_name']) as $item) {
                $businessStype[] = ['key' => $item['style_id'], 'value' => $item['style_name']];
            }
            $data = $model->getData($arraySearch, $arraySort);
            foreach ($objectType as $key => $value) {
                $objectTypeData[] = ['key' => $key, 'value' => $value];
            }
            foreach ($processMethod as $key => $value) {
                $processMethodData[] = ['key' => $key, 'value' => $value];
            }
            foreach ($processStatus as $key => $value) {
                $processStatusData[] = ['key' => $key, 'value' => $value];
            }
            return response()->json([
                        'data'      => $data,
                        'sort'      => $arraySort,
                        'sorted'    => ($request->input('sort')) ? true : false,
                        'params'    => $request->all(),
                        'tantou'    => $tantou,
                        'businessStype'  => $businessStype,
                        'objectType'     => $objectTypeData,
                        'processMethod'  => $processMethodData,
                        'processStatus'  => $processStatusData
            ]);
        }
    }
    public function processParseContent(Request $request)
    {
        if ($request->ajax() === true) {
            $businessStype = $request->input('business_stype', null);
            $orderCode0 = $request->input('order_id[0]', null);
            $orderCode1 = $request->input('order_id[1]', null);
            $orderCode2 = $request->input('order_id[2]', null);
            $orderCode = [];
            if (!empty($orderCode0)) {
                $orderCode[] = $orderCode0;
            }
            if (!empty($orderCode1)) {
                $orderCode[] = $orderCode1;
            }
            if (!empty($orderCode2)) {
                $orderCode[] = $orderCode2;
            }

            $productCode0 = $request->input('product_code[0]', null);
            $productCode1 = $request->input('product_code[1]', null);
            $productCode2 = $request->input('product_code[2]', null);
            $productCode = [];
            if (!empty($productCode0)) {
                $productCode[] = $productCode0;
            }
            if (!empty($productCode1)) {
                $productCode[] = $productCode1;
            }
            if (!empty($productCode2)) {
                $productCode[] = $productCode2;
            }
            $rules = [
                'business_stype'    => 'required',
            ];
            if (empty($businessStype)) {
                $rules['order_id'] = 'required';
                $rules['product_code'] = 'required';
            } else {
                if (in_array($businessStype, ['6', '9', '10', '11'])) {
                    $rules['order_id'] = 'required';
                } elseif ($businessStype === '14') {
                    $rules['product_code'] = 'required';
                } elseif (!in_array($businessStype, ['13', '15'])) {
                    $rules['product_code'] = 'required_without:order_id';
                    $rules['order_id']     = 'required_without:product_code';
                }
            }
            $allData = $request->all();
            $allData['order_id'] = $orderCode;
            $allData['product_code'] = $productCode;
            $validator = Validator::make($allData, $rules);
            if ($validator->fails()) {
                return response()->json([
                    'status'  => 0,
                    // 'message' => $validator->errors(),
                    'contents'    => ' '
                ]);
            }
            switch ($businessStype) {
                case '1':
                    $modelOTS = new DtOrderToSupplier();
                    $datas    = $modelOTS->getDataParseContent1($orderCode, $productCode, $businessStype);
                    if ($datas->count() === 0) {
                        return response()->json([
                            'status' => 0,
                            'contents' => ' ',
                        ]);
                    }
                    $content  = view('deje/template-content', [
                                    // 'param' => 'SC【未入荷】調査',
                                    'param' => $businessStype,
                                    'datas' => $datas->toArray()
                                ])->render();
                    return response()->json([
                        'status' => 1,
                        'contents' => $content,
                    ]);
                    break;
                case '2':
                    $modelOTS = new DtOrderToSupplier();
                    $datas    = $modelOTS->getDataParseContent1($orderCode, $productCode, $businessStype);
                    if ($datas->count() === 0) {
                        return response()->json([
                            'status' => 0,
                            'contents' => ' ',
                        ]);
                    }
                    $content  = view('deje/template-content', [
                                    // 'param' => 'SC【入荷】予定外',
                                    'param' => $businessStype,
                                    'datas' => $datas->toArray()
                                ])->render();
                    return response()->json([
                        'status' => 1,
                        'contents' => $content,
                    ]);
                    break;
                case '3':
                    $modelOTS = new DtOrderToSupplier();
                    $datas    = $modelOTS->getDataParseContent1($orderCode, $productCode, $businessStype);
                    if ($datas->count() === 0) {
                        return response()->json([
                            'status' => 0,
                            'contents' => ' ',
                        ]);
                    }
                    $content = view('deje/template-content', [
                                    // 'param' => 'SC【入荷】破損',
                                    'param' => $businessStype,
                                    'datas' => $datas->toArray()
                                    ])->render();
                    return response()->json([
                        'status' => 1,
                        'contents' => $content,
                    ]);
                    break;
                case '4':
                    $modelOTS = new DtOrderToSupplier();
                    $datas    = $modelOTS->getDataParseContent4($orderCode, $productCode);
                    if ($datas->count() === 0) {
                        return response()->json([
                            'status' => 0,
                            'contents' => ' ',
                        ]);
                    }
                    $content = view('deje/template-content', [
                                    // 'param' => 'SC【入荷】商品確認',
                                    'param' => $businessStype,
                                    'datas' => $datas->toArray()
                                    ])->render();
                    return response()->json([
                        'status' => 1,
                        'contents' => $content,
                    ]);
                    break;
                case '5':
                    $modelO = new MstOrder();
                    $datas  = $modelO->getDataParseContent5($orderCode, $productCode);
                    if ($datas->count() === 0) {
                        return response()->json([
                            'status' => 0,
                            'contents' => ' ',
                        ]);
                    }
                    $content = view('deje/template-content', [
                                    // 'param' => 'CS【顧客返送商品】',
                                    'param' => $businessStype,
                                    'datas' => $datas->toArray()
                                    ])->render();
                    return response()->json([
                        'status' => 1,
                        'contents' => $content,
                    ]);
                    break;
                case '6':
                    $modelO = new MstOrder();
                    $datas  = $modelO->getDataParseContent6($businessStype, $orderCode, $productCode);
                    if ($datas->count() === 0) {
                        return response()->json([
                            'status' => 0,
                            'contents' => ' ',
                        ]);
                    }
                    $content = view('deje/template-content', [
                                    // 'param' => 'CS【佐川JP調査依頼】',
                                    'param' => $businessStype,
                                    'datas' => $datas->toArray()
                                    ])->render();
                    return response()->json([
                        'status' => 1,
                        'contents' => $content,
                    ]);
                    break;
                case '7':
                    $modelOTS = new DtOrderToSupplier();
                    $datas    = $modelOTS->getDataParseContent7($orderCode, $productCode);
                    if ($datas->count() === 0) {
                        return response()->json([
                            'status' => 0,
                            'contents' => ' ',
                        ]);
                    }
                    $content = view('deje/template-content', [
                                    // 'param' => 'SC【出荷不可】',
                                    'param' => $businessStype,
                                    'datas' => $datas->toArray()
                                    ])->render();
                    return response()->json([
                        'status' => 1,
                        'contents' => $content,
                    ]);
                    break;
                case '8':
                    $modelO = new MstOrder();
                    $datas  = $modelO->getDataParseContent8($orderCode, $productCode);
                    if ($datas->count() === 0) {
                        return response()->json([
                            'status' => 0,
                            'contents' => ' ',
                        ]);
                    }
                    $content = view('deje/template-content', [
                                    // 'param' => 'SC【入出荷ミス・庫内破損】',
                                    'param' => $businessStype,
                                    'datas' => $datas->toArray()
                                    ])->render();
                    return response()->json([
                        'status' => 1,
                        'contents' => $content,
                    ]);
                    break;
                case '9':
                    $modelO = new MstOrder();
                    $datas  = $modelO->getDataParseContent9($businessStype, $orderCode, $productCode);
                    if ($datas->count() === 0) {
                        return response()->json([
                            'status' => 0,
                            'contents' => ' ',
                        ]);
                    }
                    $content = view('deje/template-content', [
                                    // 'param' => 'CS【請求】佐川JP破損',
                                    'param' => $businessStype,
                                    'datas' => $datas->toArray()
                                    ])->render();
                    return response()->json([
                        'status' => 1,
                        'contents' => $content,
                    ]);
                    break;
                case '10':
                    $modelO = new MstOrder();
                    $datas  = $modelO->getDataParseContent9($businessStype, $orderCode, $productCode);
                    if ($datas->count() === 0) {
                        return response()->json([
                            'status' => 0,
                            'contents' => ' ',
                        ]);
                    }
                    $content = view('deje/template-content', [
                                    // 'param' => 'CS【引取依頼】',
                                    'param' => $businessStype,
                                    'datas' => $datas->toArray()
                                    ])->render();
                    return response()->json([
                        'status' => 1,
                        'contents' => $content,
                    ]);
                    break;
                case '11':
                    $modelO = new MstOrder();
                    $datas  = $modelO->getDataParseContent9($businessStype, $orderCode, $productCode);
                    if ($datas->count() === 0) {
                        return response()->json([
                            'status' => 0,
                            'contents' => ' ',
                        ]);
                    }
                    $content = view('deje/template-content', [
                                    // 'param' => 'CS【配送情報変更】',
                                    'param' => $businessStype,
                                    'datas' => $datas->toArray()
                                    ])->render();
                    return response()->json([
                        'status' => 1,
                        'contents' => $content,
                    ]);
                    break;
                case '12':
                    $modelO = new MstOrder();
                    $datas  = $modelO->getDataParseContent6($businessStype, $orderCode, $productCode);
                    if ($datas->count() === 0) {
                        return response()->json([
                            'status'   => 0,
                            'contents' => ' ',
                        ]);
                    }
                    $content = view('deje/template-content', [
                                    // 'param' => 'CS【出荷トラブル】',
                                    'param' => $businessStype,
                                    'datas' => $datas->toArray()
                                    ])->render();
                    return response()->json([
                        'status' => 1,
                        'contents' => $content,
                    ]);
                    break;
                case '13':
                    $content = view('deje/template-content', [
                                    // 'param' => 'SC【資材管理】',
                                    'param' => $businessStype,
                                    'datas' => []
                                    ])->render();
                    return response()->json([
                        'status' => 1,
                        'contents' => $content,
                    ]);
                    break;
                case '14':
                    $modelP = new MstProduct();
                    $datas  = $modelP->getDataParseContent14($productCode);
                    if (empty($datas)) {
                        return response()->json([
                            'status' => 0,
                            'contents' => ' ',
                        ]);
                    }
                    $content = view('deje/template-content', [
                                    // 'param' => 'SC【在庫確認】',
                                    'param' => $businessStype,
                                    'datas' => $datas->toArray()
                                    ])->render();
                    return response()->json([
                        'status' => 1,
                        'contents' => $content,
                    ]);
                    break;
                case '15':
                    $content = view('deje/template-content', [
                                    // 'param' => 'SC【その他依頼】',
                                    'param' => $businessStype,
                                    'datas' => []
                                    ])->render();
                    return response()->json([
                        'status' => 1,
                        'contents' => $content,
                    ]);
                    break;
                default:
                    break;
            }
        }
    }

    /**
     * Export issue estimate
     *
     * @param  Request $request
     * @return mixed
     */
    public function exportIssue(Request $request)
    {
        $rules = [
            'deje_id'       => 'required|numeric',
            'order_id'      => 'required',
            'customer_name' => 'required|max:100'
        ];

        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            if ($request->ajax() === true) {
                return response()->json([
                    'status'  => 0,
                    'messages' => $validator->errors()
                ]);
            } else {
                foreach ($validator->errors()->all() as $error) {
                    echo $error . '<br/>';
                }
                exit;
            }
        }

        $modelEcu = new DtEcuRequestData();
        $data = $modelEcu->getDataIssue($request->input('deje_id'));
        $totalPrice = 0;
        foreach ($data as $item) {
            $tmp = $this->mbStrSplit($item->product_name_long, 25);
            $item->product_name_long = implode("\n", $tmp);
            $totalPrice += ($item->price_komi * $item->quantity);
        }

        $customerName = implode("<br/>", $this->mbStrSplit($request->input('customer_name'), 20));
        $modelDJ = new DtDeje();
        $modelDJ->updateData(
            ['deje_id' => $request->input('deje_id')],
            [
                'order_id'      => $request->input('order_id'),
                'customer_name' => $request->input('customer_name'),
            ]
        );
        $pdf = PDF::loadView(
            'deje.pdf.issue',
            [
                'data'            => $data,
                'issue_date'      => date('Y/m/d'),
                'deje_id'         => $request->input('deje_id'),
                'order_id'        => $request->input('order_id'),
                'customer_name'   => $customerName,
                'incharge_person' => Auth::user()->tantou_last_name,
                'total_price'     => $totalPrice
            ]
        );
        return $pdf->stream();
    }

    /**
     * Split multiple byte string
     *
     * @param  string $str
     * @param  int    $splitLength
     * @return array
     */
    private function mbStrSplit($str, $splitLength)
    {
        $chars = array();
        $len = mb_strlen($str);
        for ($i = 0; $i < $len; $i += $splitLength) {
            $chars[] = mb_substr($str, $i, $splitLength);
        }
        return $chars;
    }

    /**
     * Update data
     *
     * @param  Request $request
     * @return mixed
     */
    public function updateDataDetail(Request $request)
    {
        if ($request->ajax() === true) {
            $dejeId = $request->deje_id;
            $field  = $request->name;
            $value  = $request->value;
            $modelDJ = new DtDeje();
            $modelDJ->updateData(
                ['deje_id' => $dejeId],
                [$field => $value]
            );
            return response()->json([
                'status' => 1,
                $field => $value
            ]);
        }
    }

    /**
     * Clone data
     *
     * @param  Request $request
     * @return mixed
     */
    public function cloneData(Request $request)
    {
        if ($request->ajax() === true) {
            $dejeId    = $request->deje_id;
            $modelDJ   = new DtDeje();
            $modelDDC  = new DtDejeComment();
            $modelDERD = new DtEcuRequestData();
            $dataDeje  = $modelDJ->find($dejeId);
            if (!empty($dataDeje)) {
                $dataDeje = $dataDeje->toArray();
                unset($dataDeje['deje_id']);
                $dataDeje['up_date'] = now();
                $dejeIdClone = $modelDJ->insertGetId($dataDeje);
                $dataComment = $modelDDC->where(['deje_id' => $dejeId])->get();
                if ($dataComment->count()) {
                    foreach ($dataComment->toArray() as $key => $value) {
                        unset($value['comment_id']);
                        $value['deje_id'] = $dejeIdClone;
                        $modelDDC->insert($value);
                    }
                }
                $dataEcu = $modelDERD->where(['deje_id' => $dejeId])->get();
                if ($dataEcu->count()) {
                    foreach ($dataEcu->toArray() as $key => $value) {
                        $value['deje_id'] = $dejeIdClone;
                        $modelDERD->insert($value);
                    }
                }
                return response()->json([
                    'status'       => 1,
                    'url_redirect' => action('DejeController@save') . '?deje_id=' . $dejeIdClone,
                ]);
            }
        }
    }

    /**
     * Delete data
     *
     * @param  Request $request
     * @return mixed
     */
    public function delete(Request $request)
    {
        if ($request->ajax() === true) {
            $dejeId    = $request->deje_id;
            $modelDJ   = new DtDeje();
            $modelDDC  = new DtDejeComment();
            $modelDERD = new DtEcuRequestData();
            $modelDJ->where('deje_id', $dejeId)
                    ->delete();
            $modelDDC->where('deje_id', $dejeId)
                    ->delete();
            $modelDERD->where('deje_id', $dejeId)
                    ->delete();
            return response()->json([
                'status'       => 1,
            ]);
        }
    }
    
    /**
     * Multibyte string to array
     *
     * @param string $string
     * @return array
     */
    public function mbStringToArray($string)
    {
        $strlen = mb_strlen($string);
        while ($strlen) {
            $array[] = mb_substr($string, 0, 1, "UTF-8");
            $string = mb_substr($string, 1, $strlen, "UTF-8");
            $strlen = mb_strlen($string);
        }
        return $array;
    }
    
    /**
     * Calculate length content
     *
     * @param string $str
     * @param int $maxLength
     * @return array
     */
    public function calLength($str, $maxLength)
    {
        $arrStr = $this->mbStringToArray($str);
        $index = 0;
        $arrRes[$index] = '';
        foreach ($arrStr as $s) {
            if (!isset($arrRes[$index])) {
                $arrRes[$index] = '';
            }
            $arrRes[$index] .= $s;
            if (mb_strwidth($arrRes[$index]) >= $maxLength) {
                $index++;
            }
        }
        return $arrRes;
    }
    
    /**
     * Print deje detail
     *
     * @param Request $request
     * @return view
     */
    public function printDetail(Request $request)
    {
        $objectType = Config::get('common.cmb_object_type');
        $processStatus = Config::get('common.cmb_process_status');
        $processMethod = Config::get('common.cmb_process_method');
        
        $dejeId = $request->input('deje_id', null);
        $dtDeje     = new DtDeje();
        $dejeData = $dtDeje->getDataPdf($dejeId);
        $dejeData->object_type = $objectType[$dejeData->object_type]??$dejeData->object_type;
        $dejeData->process_status = $processStatus[$dejeData->process_status]??$dejeData->process_status;
        $dejeData->process_method = $processMethod[$dejeData->process_method]??$dejeData->process_method;
        if (empty($dejeData)) {
            return "No data";
        }
        
        $resContent = explode("\n", $dejeData->contents);
        $arrContent = [];
        foreach ($resContent as $content) {
            if (mb_strwidth($content) > 58) {
                $tmpContent = $this->calLength($content, 58);
                foreach ($tmpContent as $con) {
                    $arrContent[] = $con;
                }
            } else {
                $arrContent[] = $content;
            }
        }
        
        $dejeData['arrContent']   = $arrContent;
        $dejeData['countContent'] = count($dejeData['arrContent']);
        $dejeData['posHead']      = (int)floor($dejeData['countContent'] / 2);
        $dompdf = new \Dompdf\Dompdf();
        $dompdf->set_option('fontDir', public_path('fonts/'));
        $dompdf->set_option('fontCache', storage_path('fonts/'));
        $dompdf->set_option('isFontSubsettingEnabled', true);
        $dompdf->loadHtml(
            view(
                'deje.pdf.print',
                [
                    'dejeData' => $dejeData
                ]
            )
        );
        $dompdf->render();
        if ($dompdf->getCanvas()->get_page_count() > 1) {
            $font = $dompdf->getFontMetrics()->get_font("meiryo", "normal");
            $dompdf->getCanvas()->page_text(570, 760, "{PAGE_NUM}/{PAGE_COUNT}", $font, 10, array(0,0,0));
        }
        return $dompdf->stream("dompdf_out.pdf", array("Attachment" => false));
    }
}
