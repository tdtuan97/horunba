<?php
/**
 * Controller for batch management
 *
 * @package    App\Http\Controllers
 * @subpackage BatchManagementController
 * @copyright  Copyright (c) 2018 RiverCrane! Corporation. All Rights Reserved.
 * @author     Truong Nghia<truong.van.nghia@rivercrane.vn>
 */

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Backend\MstStatusBatch;
use App\Events\Command as eCommand;
use Artisan;
use Event;
use Auth;

class BatchManagementController extends Controller
{
    /**
     * Load view.
     */
    public function index()
    {
        return view('batch.index');
    }

    /**
     * Get all data.
     * Return $object json
     */
    public function dataList(Request $request)
    {
        if ($request->ajax() === true) {
            $data = MstStatusBatch::all();
            return response()->json([
                'data'      => $data,
            ]);
        }
    }

    /**
     * Process event when click start or end
     * @param  Request $request
     * @return json
     */
    public function processEvent(Request $request)
    {
        if ($request->ajax() === true) {
            $modelMSB = new MstStatusBatch();
            $name = '';
            // Process when click start
            if ($request->status === 'start') {
                $name = $request->command;
                $path = base_path('artisan');
                $cmd  = "php $path $name";
                if (!empty($request->startDate)) {
                    $cmd .= " " . $request->startDate;
                }
                if (!empty($request->endDate)) {
                    $cmd .= " " . $request->endDate;
                }
                if (substr(php_uname(), 0, 7) == "Windows") {
                    $cmd .= " >NUL 2>NUL";
                    pclose(popen('start /B cmd /C "' . $cmd . '"', 'r'));
                } else {
                    exec($cmd . " > /dev/null 2>/dev/null &");
                }
            } elseif ($request->status === 'reset') {
                $modelMSB->where('command', '=', $request->command)
                             ->update(['status_flag' => 0,'error_message'=> '']);
            } else {
                if ($request->status === 'Disactive') {
                    $modelMSB->where('command', '=', $request->command)
                             ->update(['is_active' => 0]);
                } elseif ($request->status === 'Active') {
                    $modelMSB->where('command', '=', $request->command)
                             ->update(['is_active' => 1]);
                }
            }
            $data = $modelMSB->all();
            return response()->json([
                'status'    => 1,
                'data'      => $data,
                'name'      => $name,
            ]);
        }
    }
}
