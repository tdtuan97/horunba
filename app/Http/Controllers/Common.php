<?php
/**
 * Controller common
 *
 * @package    App\Http\Controllers
 * @subpackage Common
 * @copyright  Copyright (c) 2018 RiverCrane! Corporation. All Rights Reserved.
 * @author     Truong Nghia<truong.van.nghia@rivercrane.vn>
 */
namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\Backend\MstCsvLimit;
use Illuminate\Http\Request;
use Storage;
use File;
use Input;
use Cache;
use Response;
use Config;
use DB;
use Validator;
use App\Models\Backend\MstTantou;
use Illuminate\Support\Facades\Log;
use App\Http\Controllers\StockConfirmController;
use App\Http\Controllers\OrderToSupplierController;
use App\Http\Controllers\MembershipController;
use App\Models\Backend\DtOrderProductDetail;
use App\Models\Backend\DtOrderToSupplier;
use App\Models\Backend\TOrder;
use App\Models\Backend\TOrderDetail;
use App\Models\Backend\TOrderDirect;
use App\Models\Backend\DtRePayment;
use App\Models\Backend\MstStockStatus;
use App\Models\Backend\MstOrder;
use App\Models\Backend\MstOrderDetail;
use App\Models\Backend\MstCustomer;

class Common extends Controller
{
    public $checkRules = [
        'receive_id_checkPostalCodeAndAddressReceiver' => 'checkPostalCodeAndAddressReceiver',
        'receive_id_checkLimitRequestPrice'            => 'checkLimitRequestPrice',
        'receive_id_checkTelDigits'                    => 'checkTelDigits',
        'receive_id_checkTelDigits1'                   => 'checkTelDigits1',
        'receive_id_checkBlackList'                    => 'checkBlackList',
        'receive_id_checkChargeFeeTransport'           => 'checkChargeFeeTransport',
        'receive_id_checkChargeFeeDelivery'            => 'checkChargeFeeDelivery',
        'receive_id_checkChargeFeeCulDelivery'         => 'checkChargeFeeCulDelivery',
        'receive_id_checkPaymentDeliveryFee'           => 'checkPaymentDeliveryFee',
        'receive_id_checkPaidByPoint'                  => 'checkPaidByPoint',
        'receive_id_checkPaidNotByPoint'               => 'checkPaidNotByPoint',
        'order_firstname'                              => 'required',
        'order_lastname'                               => 'required',
        'order_tel'                                    => 'required',
        'order_zip_code'                               => 'required',
        'total_price'                                  => 'required|not_in:0',
        'order_date'                                   => 'required|date',
        'request_price'                                => 'required',
        'payment_method'                               => 'required|not_in:0',
        'product_code'                                 => 'checkProductCodeExists|checkProductSetValid',
        'price'                                        => 'required|not_in:0',
        'quantity'                                     => 'required|not_in:0',
        'ship_to_last_name'                            => 'required',
        'ship_zip_code'                                => 'required',
        'ship_tel_num'                                 => 'required',
    ];
    /**
     * List message result validate.
     *
     * @var array
     */
    private $message = [
        'total_price.not_in'                  => 'The total_price field must be greater than 0',
        'request_price.not_in'                => 'The request_price field must be greater than 0',
        'payment_method.not_in'               => 'The payment_method field must be greater than 0',
        'price.not_in'                        => 'The price field must be greater than 0',
        'quantity.not_in'                     => 'The quantity field must be greater than 0',
        'product_code.checkProductCodeExists' => 'マスタDBに存在していない商品があります。',
    ];
    /**
     * Process import file: read content and parse
     * @param request
     * @return  json
     */
    public function importCSV(Request $request)
    {
        $nameCsv    = $request->input('name_csv', '');
        $fileUpload = $request->file('attached_file');
        $modelLimit = new MstCsvLimit();
        $pathUpload = "uploads";
        $msg = $this->upload($fileUpload, $nameCsv);
        if ($msg["flg"] === 0) {
            return response()->json($msg);
        }
        $fileName = $msg["msg"];
        //Get file from store
        $contentCsv = Storage::disk('local')->get($fileName);
        if ($contentCsv === "") {
            return response()->json([
                "process" => 'uploadCsv',
                "flg"     => 0,
                "msg"     => array("importFile"=>__('messages.message_csv_empty_content'))
            ]);
        }
        $arrIsUnix = ['import_pro_mfk', 'seinou_csv', 'pro_mfk_csv', 'amazon_pay_csv'];
        $breakCharacter = "\r\n";
        if (in_array($nameCsv, $arrIsUnix)) {
            $breakCharacter = "\n";
        }
        $readers      = str_getcsv($contentCsv, $breakCharacter);
        $total        = count($readers);
        $limit        = $modelLimit->getLimitByName($nameCsv);
        $maxTotalRead = !empty($limit->limit) ? $limit->limit : config('constants.MAX_READ_RECORD');
        if ($total<=$maxTotalRead) {
            $maxTotalRead = $total;
        }
        $timeTotal = round($total/$maxTotalRead);
        $fileCorrect     = date("YmdHis").'_correct.csv';
        Storage::disk('local')->put($fileCorrect, '');
        return response()->json([
                "process"      => 'uploadCsv',
                "flg"          => 1,
                "timeRun"      => $timeTotal,
                "currPage"     => 0,
                "total"        => $maxTotalRead,
                "filename"     => $fileName,
                "checkAfter"   => 0,
                "error"        => 0,
                "realFilename" => $fileUpload->getClientOriginalName(),
                "fileCorrect" => $fileCorrect,
            ]);
    }

    /**
     * Check file and content of file
     * @param   request
     * @return  array       contains value of flag and msg
     */
    public function checkFile($fileName, $isUnix = false, $isConvert = true)
    {
        //Check file exist
        $existFile = Storage::disk('local')->exists($fileName);
        if ($existFile === false) {
            return array(
                "flg" => 0,
                "msg" => __('messages.message_csv_not_found'),
            );
        }
        //Check content file
        $contentCsv = Storage::disk('local')->get($fileName);
        if ($contentCsv === "") {
            return array(
                "flg" => 0,
                "msg" => __('messages.message_csv_empty_content'),
            );
        }
        $fileContentUtf = $contentCsv;
        if ($isConvert) {
            $fileContentUtf=  mb_convert_encoding($contentCsv, 'UTF-8', 'Shift-JIS');
        }
        $breakCharacter = "\r\n";
        if ($isUnix) {
            $breakCharacter = "\n";
        }
        $readers = str_getcsv($fileContentUtf, $breakCharacter);
        return array(
                "flg" => 1,
                "msg" => $readers
            );
    }

        /**
     * Process data return for importing file
     *
     * @param   boolean $flg        flag for process function: true continue, false: stopt
     * @param   array   $arrProcess contains value of a processing
     * @return  array   contains value of flag and value of processing
     */
    public function processDataRun($flg, $arrProcess)
    {
        $fileName     = $arrProcess["fileName"];
        $timeRun      = $arrProcess["timeRun"];
        $currPage     = $arrProcess["currPage"];
        $total        = $arrProcess["total"];
        $fileCorrect  = $arrProcess["fileCorrect"];
        $realFilename = !empty($arrProcess["realFilename"]) ? $arrProcess["realFilename"] : '';
        $checkAfter   = !empty($arrProcess["checkAfter"]) ? $arrProcess["checkAfter"] : '';

        if ($flg === true) {
            $nameCached  = explode(".", $fileName);
            $nameCached  = $nameCached[0];
            $nextPage = $currPage+1;
            $error = 0 ;
            if ($currPage === $timeRun) {
                //If finish process when current page = total time run
                //Clear file import
                Storage::delete($fileName);
                Storage::delete($fileCorrect);
                //Check cached error
                if (Cache::has($nameCached)) {
                    $error = 1 ;
                }
                //Return data
                return array(
                    "process"      => 'uploadCsv',
                    "flg"          => 0,
                    "msg"          => "Finish",
                    "timeRun"      => $timeRun,
                    "currPage"     => $currPage,
                    "total"        => $total,
                    "filename"     => $nameCached,
                    "error"        => $error,
                    "realFilename" => $realFilename,
                    "fileCorrect"  => $fileCorrect,
                    "checkAfter"   => $checkAfter,
                );
            } else {
                return array(
                    "process"      => 'uploadCsv',
                    "flg"          => 1,
                    "timeRun"      => $timeRun,
                    "currPage"     => $nextPage,
                    "total"        => $total,
                    "filename"     => $fileName,
                    "realFilename" => $realFilename,
                    "fileCorrect"  => $fileCorrect,
                    "checkAfter"   => $checkAfter,
                );
            }
        } else {
            //Return error when process is false
            return array(
                "process" => 'uploadCsv',
                "flg"     => 0,
                "msg"     => $flg
            );
        }
    }
    /**
     * Update content cached
     * @param   string  $keyCached  key of cached
     * @param   string  $content    content cached
     * @return  boolean
     */
    public function updateCached($keyCached, $content)
    {
        $newContent = $content;
        $oldData = "";
        if (Cache::has($keyCached)) {
            $oldData = Cache::get($keyCached);
            if (!empty($content)) {
                $oldData .= "\r\n";
            }
        }
        $resultContent = $oldData.$newContent;
        return Cache::put($keyCached, $resultContent, 600);
    }

    /**
     * Convert csv from cached file
     * @param   string  $keyCached   key of cached
     * @return  string
     */
    public function downloadError($keyCached)
    {
        if (!Cache::has($keyCached)) {
            return response("No data");
        }
        $value = Cache::get($keyCached);
        return response($value, 200)
              ->header('Content-Type', "text/csv")
              ->header('Content-Encoding', 'Shift-JIS')
              ->header('Content-Disposition', 'attachment; filename="error'.date("YmdHis").'.csv"');
    }

    /**
     * Process import file correct
     * @param   string  $fileName
     * @param   string  $column
     * @param   string  $table
     * @return  string
     */
    public function processCorrect($fileName, $column, $table)
    {
        $strColumn = implode(",", $column);
        $temp = Storage::disk('local')->getDriver()->getAdapter()->getPathPrefix() . $fileName;
        $temp = str_replace("\\", '/', $temp);
        $query = <<<eof
    LOAD DATA LOW_PRIORITY LOCAL INFILE '$temp'
     REPLACE INTO TABLE $table
     CHARACTER SET sjis
     FIELDS TERMINATED BY ',' ENCLOSED BY '"'
     LINES TERMINATED BY '\r\n'
    ($strColumn);select NOW();
eof;
        try {
            DB::beginTransaction();
            $result = DB::connection()->getpdo()->exec($query);
            if ($result) {
                DB::commit();
                return true;
            }
        } catch (\Exception $e) {
            DB::rollback();
        }
    }
    /**
     * Process check data after process
     *
     * @param   boolean $flg        flag for process function: true continue, false: stopt
     * @param   array   $arrProcess contains value of a processing
     * @return  array   contains value of flag and value of processing
     */
    public function processDataCheck($flg, $arrProcess)
    {
        $fileName     = $arrProcess["fileName"];
        $timeRun      = $arrProcess["timeRun"];
        $currPage     = $arrProcess["currPage"];
        $total        = $arrProcess["total"];
        $fileCorrect  = $arrProcess["fileCorrect"];
        $realFilename = !empty($arrProcess["realFilename"]) ? $arrProcess["realFilename"] : '';
        $checkAfter   = !empty($arrProcess["checkAfter"]) ? $arrProcess["checkAfter"] : '';

        if ($flg === true) {
            $nameCached  = explode(".", $fileName);
            $nameCached  = $nameCached[0];
            $nextPage = $currPage+1;
            $error = 0 ;
            if ($currPage === $timeRun) {
                if (Cache::has($nameCached)) {
                    $error = 1 ;
                    $fileName = $nameCached;
                    Storage::delete($fileName);
                    Storage::delete($fileCorrect);
                }
                //Return data
                return array(
                    "process"      => 'uploadCsv',
                    "flg"          => 1,
                    "timeRun"      => $timeRun,
                    "currPage"     => 0,
                    "total"        => $total,
                    "filename"     => $fileName,
                    "error"        => $error,
                    "realFilename" => $realFilename,
                    "fileCorrect"  => $fileCorrect,
                    "checkAfter"   => 1,
                );
            } else {
                return array(
                    "process"      => 'uploadCsv',
                    "flg"          => 1,
                    "timeRun"      => $timeRun,
                    "currPage"     => $nextPage,
                    "total"        => $total,
                    "filename"     => $fileName,
                    "realFilename" => $realFilename,
                    "fileCorrect"  => $fileCorrect,
                    "checkAfter"   => 0,
                );
            }
        }
    }
    /**
     * Export file csv
     * @param   string  $fileName
     * @param   array  $column
     * @param  object $data
     * @return  object
     */
    public function processExportCSV(Request $request)
    {
        $type       = $request->input('type', null);
        $fileName   = "";
        $column     = [];
        $mstTantou  = new MstTantou();
        $mOrderType = Config::get('common.m_order_type_id');
        $delayNote  = Config::get('common.delay_note');
        $reasonNote = Config::get('common.reason_note');
        $opeStatus  = Config::get('common.ope_status');
        if ($type === 'stock-confirm') {
            $column = [
                'estimate_date'              => __('messages.estimate_date'),
                'estimate_code'              => __('messages.estimate_code'),
                'item_code'                  => __('messages.product_code'),
                'jan_code'                   => __('messages.product_jan'),
                'product_name:string'        => __('messages.product_name'),
                'm_order_type_id|mOrderType' => __('messages.product_status'),
                'quantity'                   => __('messages.quantity'),
                'supplier_nm'                => __('messages.supplier_name'),
                'maker_name'                 => __('messages.maker_name'),
                'updated_at'                 => __('messages.edi_answer_date'),
                'memo'                       => __('messages.edi_memo'),
                'other'                      => __('messages.edi_other'),
            ];
            $fileName   = $type . '-' . date('YmdHis') . ".csv";
            $data = StockConfirmController::dataList($request);
        } elseif ($type === 'order-to-supplier') {
            $orderType = [
                '0' => '臨時',
                '1' => '客注',
                '2' => '客注',
                '3' => '補充',
            ];
            $deliveryType = [
                '0' => '直送',
                '1' => '南港',
                '3' => '南港',
                '4' => '南港',
            ];
            $column = [
                'order_date'              => __('messages.order_date_ots'),
                'order_code'              => __('messages.order_code_ots'),
                'order_type|orderType'    => __('messages.order_type'),
                'order_type|deliveryType' => __('messages.delivery_type'),
                'product_code'            => __('messages.product_code_ots'),
                'product_jan'             => __('messages.jan_code_ots'),
                'product_name:string'     => __('messages.product_name'),
                'price_invoice'           => __('messages.price_invoice_ots'),
                'order_num'               => __('messages.quantity'),
                'edi_delivery_date'       => __('messages.edi_answer_date_o2s'),
                'incomplete_quantity'     => __('messages.in_stock_left'),
                'arrival_date_plan'       => __('messages.arrival_plan_date_o2s'),
                'arrival_date'            => __('messages.arrival_date_o2s'),
                'supplier_nm'             => __('messages.supplier_name'),
                'maker_full_nm'           => __('messages.maker_name'),
                'order_note'              => __('messages.edi_note'),
                'reason_note|reasonNote'  => __('messages.reason_note'),
                'remarks'                 => __('messages.remarks'),
                'ope_tantou'              => __('messages.receive_mail_incharge_person'),
                'ope_status|opeStatus'    => __('messages.ope_status'),
            ];
            $fileName   = $type . '-' . date('YmdHis') . ".csv";
            $data = OrderToSupplierController::dataList($request);
        } elseif ($type === 'membership') {
            $arrSex = [
                '0' => '女性',
                '1' => '男性',
                '2' => '不明',
            ];
            $arrIsChintai = [
                '0' => '賃貸',
                '1' => '持家',
            ];
            $arrDiyExp = [
                '0' => '初めて',
                '1' => '少しだけやったことがある',
                '2' => 'やっている',
            ];
            $column = [
                'member_id'                  => __('messages.member_id'),
                'last_name'                  => __('messages.membership_last_name'),
                'first_name'                 => __('messages.first_name'),
                'last_name_kana'             => __('messages.last_name_kana'),
                'first_name_kana'            => __('messages.first_name_kana'),
                'tel_no'                     => __('messages.tel_num'),
                'birthday'                   => __('messages.birthday'),
                'sex|arrSex'                 => __('messages.sex'),
                'address'                    => __('messages.address'),
                'enrolment_day'              => __('messages.enrolment_day'),
                'remain_point'               => __('messages.remain_point'),
                'mail_address'               => __('messages.mail_address'),
                'nick_name'                  => __('messages.nick_name'),
                'is_chintai|arrIsChintai'    => __('messages.is_chintai'),
                'opportunity|arrOpportunity' => __('messages.opportunity'),
                'opportunity_text'           => __('messages.opportunity_text'),
                'diy_exp|arrDiyExp'          => __('messages.diy_exp'),
                'interests|arrInterests'     => __('messages.interests'),
                'interests_text'             => __('messages.interests_text'),
                'your_wish'                  => __('messages.your_wish'),
                'remarks'                    => __('messages.note'),
                'point_num'                  => __('messages.buy_point'),
                'paid_price'                 => __('messages.payment_price'),
                'paid_date'                  => __('messages.lbl_paid_date'),
                'unit_price'                 => __('messages.lbl_unit_price'),
            ];
            $fileName   = $type . '-' . date('YmdHis') . ".csv";
            $data = MembershipController::dataList($request);
            $arrOpportunityData = $data->pluck('opportunity');
            $arrOpportunity = [];
            $arrOpportunityData = array_filter($arrOpportunityData->toArray());
            if (count($arrOpportunityData)) {
                $arrData = Config::get('common.opportunity');;
                foreach ($arrOpportunityData as $value) {
                    $arrTemp = explode(',', $value);
                    $temp = array_map(function($key) use ($arrData){ return isset($arrData[$key]) ? $arrData[$key] : ''; }, $arrTemp);
                    $arrOpportunity[$value] = implode('|', $temp);
                }
            }
            $arrInterestsData = $data->pluck('interests');
            $arrInterests = [];
            $arrInterestsData = array_filter($arrInterestsData->toArray());
            if (count($arrInterestsData)) {
                $arrData = Config::get('common.interests');;
                foreach ($arrInterestsData as $value) {
                    $arrTemp = explode(',', $value);
                    $temp = array_map(function($key) use ($arrData){ return isset($arrData[$key]) ? $arrData[$key] : ''; }, $arrTemp);
                    $arrInterests[$value] = implode('|', $temp);
                }
            }
        }

        Storage::disk('local')->put($fileName, '');
        $url = Storage::disk('local')->getDriver()->getAdapter()->getPathPrefix() . $fileName;
        $file = fopen($url, 'a');
        fputs($file, mb_convert_encoding(implode(",", $column), 'Shift-JIS', 'UTF-8'). "\r\n");
        foreach ($data as $value) {
            $arrTemp = [];
            foreach ($column as $k => $v) {
                if (count($temp = explode('|', $k)) === 2) {
                    if (isset(${$temp[1]}[$value->{$temp[0]}])) {
                        $arrTemp[] = $this->replaceString(trim(${$temp[1]}[$value->{$temp[0]}]), array(','));
                    } else {
                        if (isset(${$temp[1]}['0'])) {
                            $arrTemp[] = $this->replaceString(trim(${$temp[1]}['0']), array(','));
                        } else {
                            $arrTemp[] = '';
                        }
                    }
                } elseif (count($temp2 = explode(':', $k)) === 2) {
                    $arrTemp[] = $this->replaceString('"' . trim($value->{$temp2[0]}) . '"', array(','));
                } else {
                    $arrTemp[] = $this->replaceString(trim($value->{$k}), array(','));
                }
            }
            $filter     = array("\r\n", "\n", "\r");
            $contentCsv = implode(",", $arrTemp);
            $contentCsv = str_replace($filter, '', $contentCsv);
            fputs($file, mb_convert_encoding($contentCsv, 'Shift-JIS', 'UTF-8'). "\r\n");
        }
        fclose($file);
        return response()->json([
            'file_name' => $fileName,
            'status'     => 1,
        ]);
    }

    /**
     * Download csv
     *
     * @param   $request  Request
     * @return file download
     */
    public function exportCSV(Request $request)
    {
        $params = $request->all();
        register_shutdown_function('unlink', storage_path('app/' . $params['fileName']));
        $headers = array(
            'Content-Encoding: Shift-JIS',
            'Content-Type' => 'text/csv',
        );
        $path = storage_path('app/' . $params['fileName']);
        return response()->download($path, $params['fileName'], $headers);
    }

   /**
    * Update order edi
    *
    * @param $receiveID
    * @param $productCode
    *
    * @return file download
    */
    public function cancelOrderEdi($receiveID, $productCode = null)
    {
        $modelODP = new DtOrderProductDetail();
        if (is_null($productCode)) {
            $checkALl = $modelODP->checkReturnAll($receiveID, $productCode);
            if ($checkALl === 0) {
                $productCode = null;
            }
        }
        $datas = $modelODP->getDataUpdateCancelEDI($receiveID, $productCode);
        if ($datas->count() > 0) {
            $tOrderDetail      = new TOrderDetail();
            $tOrder            = new TOrder();
            $tOrderDirect      = new TOrderDirect();
            $DtOrderToSupplier = new DtOrderToSupplier();

            foreach ($datas as $data) {
                $arrUpdate = [
                    'cancel_flg' => 1,
                ];
                if (!empty($data->m_order_type_id_detail)) {
                    $arrUpdateOrder = [
                        'cancel_flg' => 1,
                    ];
                    if ($data->m_order_type_id_detail === 2) {
                        $arrUpdate['m_order_type_id'] = 13;
                    } elseif ($data->m_order_type_id_detail === 9) {
                        $arrUpdate['m_order_type_id'] = 14;
                    }
                    $tOrderDetail->updateData(
                        ['order_code' => $data->edi_order_code],
                        $arrUpdate
                    );
                    $tOrder->updateData(
                        ['order_code' => $data->edi_order_code],
                        $arrUpdateOrder
                    );
                } elseif (!empty($data->m_order_type_id_direct)) {
                    if ($data->m_order_type_id_direct === 2) {
                        $arrUpdate['m_order_type_id'] = 15;
                    } elseif ($data->m_order_type_id_direct === 9) {
                        $arrUpdate['m_order_type_id'] = 16;
                    }
                    $tOrderDirect->updateData(
                        ['order_code' => $data->edi_order_code],
                        $arrUpdate
                    );
                }
                $DtOrderToSupplier->updateData(
                    [
                        'order_code'     => $data->order_code,
                        'edi_order_code' => $data->edi_order_code
                    ],
                    ['is_cancel' => 1]
                );
            }
        }
    }

    /**
    * Replace character
    *
    * @param string $stringReplace
    * @param array $arrReplace
    *
    * @return string $newString
    */
    public function replaceString($stringReplace, $arrReplace)
    {
        $newString = str_replace($arrReplace, "", $stringReplace);
        return $newString;
    }

    /**
    * Re-payment cancel.
    *
    * @param object $dataOrder
    * @return void
    */
    public function processRePaymentCancel($dataOrder)
    {
        $modelRP = new DtRePayment();
        $modelRP->repay_price = $dataOrder->request_price;
        if ($dataOrder->payment_method === 1 && $dataOrder->mall_id !== 3) {
            $modelRP->repay_bank_code        = null;
            $modelRP->repay_bank_name        = 'クレカ返金';
            $modelRP->repay_bank_branch_code = null;
            $modelRP->repay_bank_branch_name = 'クレカ返金';
            $modelRP->repay_name             = 'クレカ返金';
            $modelRP->repay_bank_account     = 'クレカ返金';
        } elseif ($dataOrder->mall_id === 3) {
            $modelRP->repay_bank_code        = null;
            $modelRP->repay_bank_name        = 'アマゾン';
            $modelRP->repay_bank_branch_code = null;
            $modelRP->repay_bank_branch_name = 'アマゾン';
            $modelRP->repay_name             = 'アマゾン';
            $modelRP->repay_bank_account     = 'アマゾン';
        }
        $modelRP->receive_id                  = $dataOrder->receive_id;
        $modelRP->return_no                   = null;
        $modelRP->repayment_time              = 0;
        $modelRP->payment_id                  = null;
        $modelRP->repayment_status            = 0;
        $modelRP->repayment_type              = 0;
        $modelRP->return_coupon               = $dataOrder->used_coupon;
        $modelRP->return_point                = $dataOrder->used_point;
        $modelRP->deposit_transfer_commission = 0;
        $modelRP->return_fee                  = 0;
        $modelRP->total_return_price          = $modelRP->repay_price + $modelRP->deposit_transfer_commission;
        $modelRP->is_return                   = 0;
        $modelRP->repayment_date              = null;
        $modelRP->in_ope_cd                   = 'OPE99999';
        $modelRP->in_date                     = now();
        $modelRP->up_ope_cd                   = 'OPE99999';
        $modelRP->up_date                     = now();
        $modelRP->save();
        return true;
    }

    /**
     * Process update stock
     *
     * @param  int  receiveID
     * @return void
     */
    public function processUpdateZan($receiveId)
    {
        $modelODP    = new DtOrderProductDetail();
        $modelSS     = new MstStockStatus();
        $dataProcess = $modelODP->getDataUpdateStock($receiveId);
        foreach ($dataProcess as $value) {
            $productCode = $value->product_code;
            $receivedOrderNum = $value->received_order_num;
            $arrUpdate = [];
            if (in_array($value->delivery_type, [1, 3])
                && $value->product_status !== PRODUCT_STATUS['CANCEL']
                && $value->order_status >= ORDER_STATUS['ESTIMATION']) {
                if (!empty($value->child_product_code)) {
                    $productCode = $value->child_product_code;
                }
                if ($value->product_status === 1 && $value->order_type !== 3) {
                    $arrUpdate['wait_delivery_num'] = DB::raw("wait_delivery_num - {$receivedOrderNum}");
                }
                $arrUpdate['order_zan_num'] = DB::raw("order_zan_num - {$receivedOrderNum}");
            }
            if (count($arrUpdate) !== 0) {
                $modelSS->updateData(['product_code' => $productCode], $arrUpdate);
            }
        }
    }

    /**
    * Re-calculate price order.
    *
    * @param int receiveId
    * @return void
    */
    public function reCalculate($receiveId)
    {
        $modelO         = new MstOrder();
        $modelOD        = new MstOrderDetail();
        $dataOrders     = $modelO->getDataReCalculate($receiveId);
        $arrGPD = [];
        $dataNewOrders = [];
        foreach ($dataOrders as $data) {
            if (!isset($arrGPD[$data->receiver_id])) {
                $arrGPD[$data->receiver_id] = 0;
            }
            $arrGPD[$data->receiver_id] += $data->price * $data->quantity;
            $dataNewOrders[$data->receiver_id] = $data;
        }
        $arrSD = [];
        foreach ($dataNewOrders as $data) {
            if ($data->add_ship_charge === 1 && $data->maill_id !== 3) {
                $arrSD[$data->receiver_id] = 5400;
            } elseif ($arrGPD[$data->receiver_id] >= $data->below_limit_price && $data->mall_id !== 3) {
                $arrSD[$data->receiver_id] = 0;
            } elseif ($arrGPD[$data->receiver_id] < $data->below_limit_price && $data->mall_id !== 3) {
                $arrSD[$data->receiver_id] = $data->charge_price;
            }
        }

        $dataOrder      = $dataOrders[0];
        $shipCharge     = 0;
        $payAfterCharge = 0;
        foreach ($arrSD as $sd) {
            $shipCharge += $sd;
        }
        if ($dataOrder->payment_method === 3) {
            $codFree = $modelO->getCodFee($receiveId);
            if (count($codFree) !== 0) {
                $payAfterCharge = $codFree->cod_fee;
            }
        }
        $totalPrice = $dataOrder->goods_price + $shipCharge + $payAfterCharge + $dataOrder->pay_charge;
        $requestPrice = $totalPrice - $dataOrder->pay_charge_discount -
                        $dataOrder->discount - $dataOrder->used_point - $dataOrder->used_coupon;
        try {
            DB::beginTransaction();
            $data = $modelO->sharedLock()->find($receiveId);
            $data->ship_charge      = $shipCharge;
            $data->total_price      = $totalPrice;
            $data->pay_after_charge = $payAfterCharge;
            $data->request_price    = $requestPrice;
            $data->is_mall_update   = 1;
            $data->is_recalculate   = 0;
            $data->save();
            foreach ($arrGPD as $receiverId => $price) {
                $modelOD->updateDataByKey(
                    [
                        'receive_id'  => $receiveId,
                        'receiver_id' => $receiverId
                    ],
                    ['goods_price_detail' => $price]
                );
            }
            foreach ($arrSD as $receiverId => $price) {
                $modelOD->updateDataByKey(
                    [
                        'receive_id'  => $receiveId,
                        'receiver_id' => $receiverId
                    ],
                    ['ship_charge_detail' => $price]
                );
            }
            if ($data->is_ignore_error === 0) {
                $dataReceivers = $modelOD->getListReceiver($receiveId);
                foreach ($dataReceivers as $value) {
                    $this->checkInfoAfterSave($this->checkRules, $data, $value->receiver_id);
                }
            }

            DB::commit();
        } catch (\Exception $e) {
            DB::rollback();
            report($e);
        }
    }
        /**
     * Check info order after save
     *
     * @param   object  $mstOrder
     * @param   bool    $checkStatus
     * @param   object  $productStatus
     * @return  mixed
     */
    public function checkInfoAfterSave(
        $checkRules,
        $mstOrder,
        $receiverId = null,
        $checkStatus = false,
        $productStatus = null
    ) {
        $modelO        = new MstOrder();
        $modelCustomer = new MstCustomer();
        $value = [];
        $dataCheck    = $modelO->getDataCheckError($mstOrder->receive_id, $receiverId);
        $dataCustomer = $modelCustomer->find($receiverId);
        if ($dataCheck === null) {
            return false;
        } else {
            $value = $dataCheck->toArray();
            $value['receive_id_checkPostalCodeAndAddress']         = $value['receive_id'];
            $value['receive_id_checkPostalCodeAndAddressReceiver'] = $value['receive_id'];
            $value['receive_id_checkLimitRequestPrice']            = $value['receive_id'];
            $value['receive_id_checkChargeFeeCulDelivery']         = $value['receive_id'];
            $value['receive_id_checkChargeFeeTransport']           = $value['receive_id'];
            $value['receive_id_checkChargeFeeDelivery']            = $value['receive_id'];
            $value['receive_id_checkPaymentDeliveryFee']           = $value['receive_id'];
            $value['receive_id_checkBlackList']                    = $value['receive_id'];
            $value['receive_id_checkTelDigits']                    = $value['receive_id'];
             $value['receive_id_checkTelDigits1']                  = $value['receive_id'];
            $value['receive_id_checkPaidByPoint']                  = $value['receive_id'];
            $value['receive_id_checkPaidNotByPoint']               = $value['receive_id'];
            if (!empty($value['delivery_type'])) {
                $checkRules['receive_id_checkDeliveryDirectAndPayment'] = 'checkDeliveryDirectAndPayment';
                $value['receive_id_checkDeliveryDirectAndPayment'] = $value['receive_id'];
            }
            if ($value['mall_id'] === 3) {
                $value['order_firstname']    = 'AMAZON';
                $value['ship_to_first_name'] = 'AMAZON';
            }
            if ($value['mall_id'] === 6) {
                $checkRules = [];
            }
            if ($value['mall_id'] === 7 && $value['payment_method'] === 11) {
                unset($checkRules['total_price']);
                unset($checkRules['request_price']);
                unset($checkRules['price']);
                unset($checkRules['receive_id_checkPaidByPoint']);
            }
            if ($value['mall_id'] === 11) {
                unset($checkRules['total_price']);
                unset($checkRules['price']);
            }
        }
        if ($receiverId !== null) {
                $checkRules['receive_id_checkPostalCodeAndAddressReceiver']
                    = 'checkPostalCodeAndAddressReceiver:' . $receiverId;
                /*
                $checkRules['receive_id_checkPostalCodeAndAddressReceiverLength']
                    = 'checkPostalCodeAndAddressReceiverLength:' . $receiverId;*/
                $checkRules['receive_id_checkTelDigits']
                    = 'checkTelDigits:' . $receiverId;
                $checkRules['receive_id_checkTelDigits1']
                    = 'checkTelDigits1:' . $receiverId;
                $checkRules['receive_id_checkChargeFeeTransport']
                    = 'checkChargeFeeTransport:' . $receiverId;
                $checkRules['receive_id_checkChargeFeeDelivery']
                    = 'checkChargeFeeDelivery:' . $receiverId;
                $checkRules['receive_id_checkChargeFeeCulDelivery']
                    = 'checkChargeFeeCulDelivery:' . $receiverId;
        }

        $subValidator  = Validator::make($value, $checkRules, $this->message);
        $isRecalculate = 0;
        $isConfirmed   = $mstOrder->is_confirmed;
        $isError       = $mstOrder->order_sub_status === ORDER_SUB_STATUS['ERROR'] ?  false : true;
        $checkTel      = false;
        $checkNotPaid  = false;
        if ($subValidator->fails()) {
            $errorVal       = $subValidator->errors();
            $message = [];
            $arrKey = [
                'receive_id_checkChargeFeeTransport',
                'receive_id_checkChargeFeeDelivery',
                'receive_id_checkChargeFeeCulDelivery',
                'receive_id_checkPaymentDeliveryFee',
            ];
            $arrKeyConfirm = [
                'receive_id_checkLimitRequestPrice',
                'receive_id_checkBlackList',
            ];
            $arrKeyTel = [
                'receive_id_checkTelDigits1'
            ];
            $arrKeyPaidNot = [
                'receive_id_checkPaidNotByPoint'
            ];
            $orderSubStatus = ORDER_SUB_STATUS['ERROR'];
            foreach ($errorVal->toArray() as $key => $error) {
                $message[] = $error[0];
                if (in_array($key, $arrKey)) {
                    if ($key === 'receive_id_checkChargeFeeTransport') {
                        $orderSubStatus = ORDER_SUB_STATUS['ERROR_FEE_TRANSPORT'];
                    }
                    $isRecalculate = 1;
                }
                if (in_array($key, $arrKeyConfirm)) {
                    $isConfirmed = 1;
                }
                if (in_array($key, $arrKeyTel)) {
                    $checkTel = true;
                }
                if (in_array($key, $arrKeyPaidNot)) {
                    $checkNotPaid = true;
                }
            }
            $message = implode("\n", $message);
            $isError = true;
        } else {
            $message        = '';
            if ($mstOrder->order_status >= 7) {
                $orderSubStatus = $mstOrder->order_sub_status;
            } else {
                $orderSubStatus = 0;
            }
            if ($checkStatus && $mstOrder->order_sub_status !== ORDER_SUB_STATUS['ERROR'] && count($productStatus) !== 0) {
                $flgCheckStatus = true;
                if (in_array($mstOrder->order_sub_status, [
                        ORDER_SUB_STATUS['OUT_OF_STOCK'],
                        ORDER_SUB_STATUS['STOP_SALE'],
                        ORDER_SUB_STATUS['DELAY'],
                        ORDER_SUB_STATUS['EXPIRES']
                    ])) {
                    foreach ($productStatus as $item) {
                        if (in_array($item->product_status, [
                                PRODUCT_STATUS['OUT_OF_STOCK'],
                                PRODUCT_STATUS['SALE_STOP'],
                                PRODUCT_STATUS['DELAY'],
                                PRODUCT_STATUS['EXPIRATION']
                            ])) {
                            $flgCheckStatus = false;
                        }
                    }
                } else {
                    $flgCheckStatus = false;
                }
                if (!$flgCheckStatus) {
                    $orderSubStatus = $mstOrder->order_sub_status;
                }
            }
        }
        try {
            $mstOrder->order_sub_status = $orderSubStatus;
            $mstOrder->err_text         = $message;
            $mstOrder->is_recalculate   = $isRecalculate;
            $mstOrder->is_confirmed     = $isConfirmed;
            if ($checkNotPaid) {
                $mstOrder->order_sub_status = ORDER_SUB_STATUS['DONE'];
                $mstOrder->order_status     = ORDER_SUB_STATUS['VERIFY_DATA'];
                $mstOrder->err_text         = '';
                $mstOrder->payment_method   = 10;
            }
            if ($checkTel) {
                $mstOrder->order_sub_status    = 0;
                $mstOrder->err_text            = '';
                $mstOrder->urgent_mail_id      = 814;
                $mstOrder->is_send_mail_urgent = 1;
                $dataCustomer->tel_num         = '0667151162';
                $dataCustomer->save();
            }
            $mstOrder->save();
        } catch (\Exception $ex) {
            return $ex;
        }
        return $isError;
    }

}
