<?php
/**
 * Controller for store order
 *
 * @package    App\Http\Controllers
 * @subpackage StoreOrderController
 * @copyright  Copyright (c) 2019 RiverCrane! Corporation. All Rights Reserved.
 * @author     Truong Nghia<truong.van.nghia@rivercrane.vn>
 */

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Backend\MstMembership;
use App\Models\Backend\DtOpenedLesson;
use App\Models\Backend\DtPurchasedPoint;
use App\Models\Backend\MstLesson;
use Validator;
use Auth;
use Config;
use DB;

class MembershipController extends Controller
{
    /**
     * Load view.
     */
    public function index()
    {
        return view('membership.index');
    }

    /**
     * Get all data.
     * Return $object json
     */
    public static function dataList(Request $request)
    {
        if ($request->ajax() === true) {
            $modelMMS = new MstMembership();
            $type     = $request->input('type', null);
            $arrSort  = [
                'member_id'    => $request->input('sort_member_id', null),
                'full_name'    => $request->input('sort_full_name', null),
                'birthday'     => $request->input('sort_birthday', null),
                'sex'          => $request->input('sort_sex', null),
                'tel_no'       => $request->input('sort_tel_no', null),
                'address'      => $request->input('sort_address', null),
                'mail_address' => $request->input('sort_mail_address', null),
                'remain_point' => $request->input('sort_remain_point', null),
            ];
            $rule = [
                'birthday' => 'nullable|date',
            ];
            $arrSearch = [
                'member_id'    => $request->input('member_id', null),
                'full_name'    => $request->input('full_name', null),
                'birthday'     => $request->input('birthday', null),
                'tel_no'       => $request->input('tel_no', null),
                'address'      => $request->input('address', null),
                'mail_address' => $request->input('mail_address', null),
                'per_page'     => $request->input('per_page', null)
            ];
            $validator = Validator::make($arrSearch, $rule);
            if ($validator->fails()) {
                $data = [];
            } else {
                if (!is_null($type)) {
                    $data = $modelMMS->getData($arrSearch, $arrSort, $type);
                    return $data;
                } else {
                    $data = $modelMMS->getData($arrSearch, $arrSort);
                }
            }

            return response()->json([
                'data'        => $data,
                'link_detail' => action('MembershipController@review')
            ]);
        }
    }

    /**
     * Get detail member.
     * @param request
     * @return $object json
     */
    public function detail(Request $request)
    {
        if ($request->ajax() === true) {
            $arrYear = [['key' => '', 'value' => '------']];
            for ($i = date('Y'); $i > date('Y') - 100; $i--) {
                $arrYear[] = ['key' => $i, 'value' => $i];
            }
            $arrMonth = [['key' => '', 'value' => '------']];
            for ($i = 1; $i <= 12; $i++) {
                $arrMonth[] = ['key' => $i, 'value' => $i];
            }
            $arrDay = [['key' => '', 'value' => '------']];
            for ($i = 1; $i <= 31; $i++) {
                $arrDay[] = ['key' => $i, 'value' => $i];
            }

            $memberId = $request->input('member_id', null);
            $data = [];
            $modelMMS = new MstMembership();
            $data['action'] = [
                'value'    => 'add',
                'is_error' => false,
                'message'  => '',
            ];
            if (!empty($memberId)) {
                $dataMember = $modelMMS->find($memberId);
                if (!empty($dataMember)) {
                    $data['action'] = [
                        'value'    => 'edit',
                        'is_error' => false,
                        'message'  => '',
                    ];
                    foreach ($dataMember->toArray() as $key => $value) {
                        if ($key === 'birthday') {
                            $month = '';
                            $day = '';
                            $year = '';
                            if (!empty($value)) {
                                $month = date('m', strtotime($value));
                                $day = date('d', strtotime($value));
                                $year = date('Y', strtotime($value));
                            }
                            $data['day'] = [
                                'value'    => $day,
                                'is_error' => false,
                                'message'  => '',
                            ];
                            $data['month'] = [
                                'value'    => $month,
                                'is_error' => false,
                                'message'  => '',
                            ];
                            $data['year'] = [
                                'value'    => $year,
                                'is_error' => false,
                                'message'  => '',
                            ];
                        } else {
                            if ($key === 'member_id') {
                                $data[$key] = [
                                    'value'    => $value,
                                    'is_error' => false,
                                    'message'  => '',
                                    'readonly' => true
                                ];
                            } elseif ($key === 'opportunity' || $key === 'interests') {
                                $data[$key] = [
                                    'value'    => explode(",", $value),
                                    'is_error' => false,
                                    'message'  => '',
                                ];
                            } else {
                                $data[$key] = [
                                    'value'    => $value,
                                    'is_error' => false,
                                    'message'  => '',
                                ];
                            }
                        }
                    }
                } else {
                    return response()->json([
                        'status'        => 0,
                        'link_redirect' => action('MembershipController@save'),
                    ]);
                }
            } else {
                $data['sex'] = [
                    'value'    => 2,
                    'is_error' => false,
                    'message'  => '',
                ];
            }
            $opportunity = Config::get('common.opportunity');
            $diyExp      = Config::get('common.diy_exp');
            $interests   = Config::get('common.interests');
            return response()->json([
                'data'        => $data,
                'optYear'     => $arrYear,
                'optMonth'    => $arrMonth,
                'optDay'      => $arrDay,
                'opportunityOpt' => $opportunity,
                'diyExpOpt'      => $diyExp,
                'interestsOpt'   => $interests,
            ]);
        }
    }

    /**
     * Loadview.
     */
    public function save()
    {
        $opportunity = Config::get('common.opportunity');
        $diyExp      = Config::get('common.diy_exp');
        $interests   = Config::get('common.interests');
        return view('membership.save');
    }

    /**
     * Submit form.
     * @param request
     * Return $object json
     */
    public function submitForm(Request $request)
    {
        $rules = [
            'last_name'       => 'required',
            'first_name'      => 'required',
            'tel_no'          => 'required|phone_number',
            'address'         => 'required',
            'year'            => 'required_with:day,month',
            'day'             => 'required_with:year,month',
            'month'           => 'required_with:day,year',
            'mail_address'    => 'nullable|email',
            'birthday'        => 'nullable|date',
        ];

        $arrInput = [
            'member_id'        => $request->input('member_id', null),
            'last_name'        => $request->input('last_name', null),
            'first_name'       => $request->input('first_name', null),
            'last_name_kana'   => $request->input('last_name_kana', null),
            'first_name_kana'  => $request->input('first_name_kana', null),
            'nick_name'        => $request->input('nick_name', null),
            'tel_no'           => $request->input('tel_no', null),
            'day'              => $request->input('day', null),
            'month'            => $request->input('month', null),
            'year'             => $request->input('year', null),
            'sex'              => $request->input('sex', null),
            'mail_address'     => $request->input('mail_address', null),
            'address'          => $request->input('address', null),
            'enrolment_day'    => $request->input('enrolment_day', null),
            'remain_point'     => $request->input('remain_point', null),
            'remarks'          => $request->input('remarks', null),
            'is_chintai'       => $request->input('is_chintai', null),
            'remain_point'     => 0,
            'action'           => $request->input('action', null),
            'opportunity'      => $request->input('opportunity', null),
            'opportunity_text' => $request->input('opportunity_text', null),
            'diy_exp'          => $request->input('diy_exp', null),
            'interests'        => $request->input('interests', null),
            'interests_text'   => $request->input('interests_text', null),
            'your_wish'        => $request->input('your_wish', null),
        ];
        $action = $arrInput['action'];
        if ($action === 'add') {
            $rules['member_id'] = 'required|unique:mst_membership,member_id';
        }

        $arrInput['birthday'] = null;
        if (!empty($arrInput['year']) && !empty($arrInput['month']) && !empty($arrInput['day'])) {
            $arrInput['birthday'] = $arrInput['year'] . '/' . $arrInput['month'] . '/' . $arrInput['day'];
        }
        if (is_array($arrInput['opportunity'])) {
            if (!in_array("64", $arrInput['opportunity'])) {
                $arrInput['opportunity_text'] = null;
            } elseif (empty($arrInput['opportunity_text'])) {
                $rules['opportunity_text'] = 'required';
            }
        } else {
            $arrInput['opportunity_text'] = null;
        }
        if (is_array($arrInput['interests'])) {
            if (!in_array("256", $arrInput['interests'])) {
                $arrInput['interests_text'] = null;
            } elseif (empty($arrInput['interests_text'])) {
                $rules['interests_text'] = 'required';
            }
        } else {
            $arrInput['interests_text'] = null;
        }

        $validator = Validator::make($arrInput, $rules);
        if ($validator->fails()) {
            $arrResponse = [];
            $error = $validator->errors()->toArray();
            foreach ($arrInput as $key => $value) {
                $arrResponse[$key] = [
                    'value'    => is_null($value) ? '' : $value,
                    'is_error' => false,
                    'message'  => '',
                ];
                if (($key === 'year' || $key === 'month' || $key === 'day') && isset($error['birthday'])) {
                    $arrResponse[$key]['is_error'] = true;
                    $arrResponse[$key]['message']  = $error['birthday'][0];
                    continue;
                }
                if (isset($error[$key])) {
                    $arrResponse[$key]['is_error'] = true;
                    $arrResponse[$key]['message']  = $error[$key][0];
                }
            }
            $arrYear = [['key' => '', 'value' => '------']];
            for ($i = date('Y'); $i > date('Y') - 100; $i--) {
                $arrYear[] = ['key' => $i, 'value' => $i];
            }
            $arrMonth = [['key' => '', 'value' => '------']];
            for ($i = 1; $i <= 12; $i++) {
                $arrMonth[] = ['key' => $i, 'value' => $i];
            }
            $arrDay = [['key' => '', 'value' => '------']];
            for ($i = 1; $i <= 31; $i++) {
                $arrDay[] = ['key' => $i, 'value' => $i];
            }
            if ($action === 'edit') {
                $arrResponse['member_id']['readonly'] = true;
            }
            $opportunity = Config::get('common.opportunity');
            $diyExp      = Config::get('common.diy_exp');
            $interests   = Config::get('common.interests');
            return response()->json([
                'data'        => $arrResponse,
                'status'      => 0,
                'optYear'     => $arrYear,
                'optMonth'    => $arrMonth,
                'optDay'      => $arrDay,
                'opportunityOpt' => $opportunity,
                'diyExpOpt'      => $diyExp,
                'interestsOpt'   => $interests,
            ]);
        } else {
            $arrInput['opportunity'] = is_array($arrInput['opportunity']) ? implode("," , $arrInput['opportunity']) : null;
            $arrInput['interests']   = is_array($arrInput['interests']) ? implode("," , $arrInput['interests']) : null;
            $memberId = \Request::get('member_id');
            $modelMMS = new MstMembership();
            unset($arrInput['year']);
            unset($arrInput['month']);
            unset($arrInput['day']);
            unset($arrInput['action']);
            $arrInput['up_ope_cd'] = \Auth::user()->tantou_code;
            $arrInput['up_date']   = date('Y-m-d H:i:s');
            if ($action === 'edit') {
                unset($arrInput['member_id']);
                $modelMMS->updateData(['member_id' => $memberId], $arrInput);
            } else {
                $arrInput['in_ope_cd'] = \Auth::user()->tantou_code;
                $arrInput['in_date']   = date('Y-m-d H:i:s');
                $modelMMS->insert($arrInput);
            }
        }
        return response()->json([
            'status'        => 1,
            'link_redirect' => action('MembershipController@index'),
        ]);
    }

    /**
     * Loadview page review membership.
     */
    public function review()
    {
        $modelMMS = new MstMembership();
        $modelDOL = new DtOpenedLesson();
        $modelDPP = new DtPurchasedPoint();
        $memberId = \Request::get('member_id');
        $data = [];
        if (!empty($memberId)) {
            $dataMember = $modelMMS->find($memberId);
            if (empty($dataMember)) {
                return redirect()->action('MembershipController@index');
            } else {
                $dataMember = $dataMember->toArray();
                if (!empty($dataMember['birthday'])) {
                    $dataMember['month'] = date('m', strtotime($dataMember['birthday']));
                    $dataMember['day']   = date('d', strtotime($dataMember['birthday']));
                    $dataMember['year']  = date('Y', strtotime($dataMember['birthday']));
                } else {
                    $dataMember['month'] = null;
                    $dataMember['day']   = null;
                    $dataMember['year']  = null;
                }
                $dataLession = $modelDOL->getDataOpenedLessonOfMember($memberId);
                $dataPurchased = $modelDPP->getDataByMemberId($memberId);
            }
        }
        return view(
            'membership.review',
            [
                'member_info'      => $dataMember,
                'member_purchaseds' => $dataPurchased,
                'member_lessions'  => $dataLession
            ]
        );
    }

    /**
     * Process free and delete lesson
     * @param object $request
     * @return json
     */
    public function processFreeOrDelete(Request $request)
    {
        if ($request->ajax() === true) {
            $type = $request->input('type', null);
            $modelDOL = new DtOpenedLesson();
            $modelMMS = new MstMembership();
            $member_id = $request->input('member_id', null);
            $lesson_id = $request->input('lesson_id', null);
            $open_date = $request->input('open_date', null);
            $dataOL = $modelDOL->getData([
                'member_id' => $member_id,
                'lesson_id' => $lesson_id,
                'open_date' => $open_date,
            ]);
            if (empty($dataOL)) {
                return response()->json([
                    'status'  => 2,
                    'message' => __('messages.data_do_not_exists')
                ]);
            }
            if ($type === 'free') {
                $modelMMS->updateData(
                    ['member_id' => $member_id],
                    [
                        'remain_point' => DB::raw("remain_point + {$dataOL->used_point}"),
                        'up_ope_cd'    => \Auth::user()->tantou_code,
                        'up_date'      => date('Y-m-d H:i:s'),
                    ]
                );
                $modelDOL->updateData(
                    [
                        'member_id' => $member_id,
                        'lesson_id' => $lesson_id,
                        'open_date' => $open_date,
                    ],
                    [
                        'used_point' => 0,
                        'up_ope_cd'  => \Auth::user()->tantou_code,
                        'up_date'    => date('Y-m-d H:i:s'),
                    ]
                );
                return response()->json([
                    'status'  => 1,
                    'message' => __('messages.ajax_update_success')
                ]);
            } elseif ($type === 'delete') {
                $modelMMS->updateData(
                    ['member_id' => $member_id],
                    [
                        'remain_point' => DB::raw("remain_point + {$dataOL->used_point}"),
                        'up_ope_cd'    => \Auth::user()->tantou_code,
                        'up_date'      => date('Y-m-d H:i:s'),
                    ]
                );
                $modelDOL->where(
                    [
                        'member_id' => $member_id,
                        'lesson_id' => $lesson_id,
                        'open_date' => $open_date,
                    ]
                )->delete();
                return response()->json([
                    'status'  => 1,
                    'message' => __('messages.ajax_update_success')
                ]);
            }
        }
    }

    /**
     * Loadview
     */
    public function buyPoint()
    {
        return view('membership.buy_point');
    }

    /**
     * Process buy
     * @param object $request
     * @return json
     */
    public function buyPointDetail(Request $request)
    {
        if ($request->ajax() === true) {
            $memberId  = $request->input('member_id', null);
            $detailNum = $request->input('detail_num', null);
            $data = [];
            $modelMMS = new MstMembership();
            $modelDPP = new DtPurchasedPoint();
            if (!empty($memberId)) {
                $dataMember = $modelMMS->find($memberId);
                if (!empty($dataMember)) {
                    if (empty($detailNum)) {
                        $data['member_id'] = [
                            'value'    => $memberId,
                            'is_error' => false,
                            'message'  => '',
                        ];
                    } else {
                        $dataPurchased = $modelDPP->getDataDetail($memberId, $detailNum);
                        if (empty($dataPurchased)) {
                            return response()->json([
                                'status'        => 0,
                                'link_redirect' => action('MembershipController@review') . '?member_id=' . $memberId,
                            ]);
                        }
                        foreach ($dataPurchased->toArray() as $key => $value) {
                            $data[$key] = [
                                'value'    => $value,
                                'is_error' => false,
                                'message'  => '',
                            ];
                        }
                    }
                    return response()->json([
                        'data'      => $data,
                    ]);
                } else {
                    return response()->json([
                        'status'        => 0,
                        'link_redirect' => action('MembershipController@review') . '?member_id=' . $memberId,
                    ]);
                }
            }
            return response()->json([
                'status'        => 0,
                'link_redirect' => action('MembershipController@index'),
            ]);
        }
    }

    /**
     * Process submit buy point
     * @param object $request
     * @return json
     */
    public function buyPointSubmit(Request $request)
    {
        $rules = [
            'member_id'  => 'required',
            'point_num'  => 'required|numeric',
            'paid_price' => 'required|numeric',
            'paid_date'  => 'required',
        ];

        $arrInput = [
            'member_id'    => $request->input('member_id', null),
            'point_num'    => $request->input('point_num', null),
            'paid_price'   => $request->input('paid_price', null),
            'paid_date'    => $request->input('paid_date', null),
            'unit_price'   => $request->input('unit_price', null),
            'note'         => $request->input('note', null),
        ];
        if (empty($arrInput['member_id'])) {
            return response()->json([
                'status'        => 1,
                'link_redirect' => action('MembershipController@index'),
            ]);
        }

        $validator = Validator::make($arrInput, $rules);
        if ($validator->fails()) {
            $arrResponse = [];
            $error = $validator->errors()->toArray();
            foreach ($arrInput as $key => $value) {
                $arrResponse[$key] = [
                    'value'    => is_null($value) ? '' : $value,
                    'is_error' => false,
                    'message'  => '',
                ];
                if (isset($error[$key])) {
                    $arrResponse[$key]['is_error'] = true;
                    $arrResponse[$key]['message']  = $error[$key][0];
                }
            }
            return response()->json([
                'data'     => $arrResponse,
                'status'   => 0,
            ]);
        } else {
            try {
                DB::beginTransaction();
                $modelDPP = new DtPurchasedPoint();
                $arrInput['unit_price'] = FLOOR($arrInput['paid_price']/$arrInput['point_num']);
                $arrInput['up_ope_cd'] = \Auth::user()->tantou_code;
                $arrInput['up_date']   = date('Y-m-d H:i:s');
                $detailNum = $request->input('detail_num', null);
                $memberId = $arrInput['member_id'];
                if (!empty($detailNum)) {
                    unset($arrInput['member_id']);
                    $dataPP = DtPurchasedPoint::where(
                        [
                            'member_id'  => $memberId,
                            'detail_num' => $detailNum,
                        ]
                    )->first();
                    $modelDPP->updateData(
                        [
                            'member_id'  => $memberId,
                            'detail_num' => $detailNum,
                        ],
                        $arrInput
                    );
                    if (!empty($dataPP)) {
                        $arrInput['point_num'] -= $dataPP->point_num;
                    }
                } else {
                    $detailNum = $modelDPP->getDataDetailNum($memberId);
                    $arrInput['detail_num'] = empty($detailNum) ? 0 : $detailNum->detail_num + 1;
                    $arrInput['in_ope_cd']  = \Auth::user()->tantou_code;
                    $arrInput['in_date']    = date('Y-m-d H:i:s');
                    $modelDPP->insert($arrInput);
                }
                $memberShipData = MstMembership::find($memberId);
                $memberShipData->remain_point += $arrInput['point_num'];
                $memberShipData->save();
                DB::commit();
                return response()->json([
                    'status'        => 1,
                    'link_redirect' => action('MembershipController@review') . '?member_id=' . $memberId,
                ]);
            } catch (\Exception $ex) {
                DB::rollBack();
                throw new \Exception("[" . $ex->getCode() . "]" . $ex->getMessage());
            }
        }
    }

    /**
     * Process take lesson
     * @param object $request
     * @return json
     */
    public function takeLesson(Request $request)
    {
        return view('membership.take_lesson');
    }

    /**
     * Process get lesson detail
     * @param object $request
     * @return json
     */
    public function takeLessonDetail(Request $request)
    {
        if ($request->ajax() === true) {
            $memberId = $request->input('member_id', null);
            $lessonId = $request->input('lesson_id', null);
            $openDate = $request->input('open_date', null);
            $data     = [];
            $modelL   = new MstLesson();
            $modelMMS = new MstMembership();
            $modelOL  = new DtOpenedLesson();
            $optCategory = [['key' => '', 'value' => '------']];
            foreach ($modelL->groupBy('category_name')->get(['category_name']) as $item) {
                $optCategory[] = ['key' => $item['category_name'], 'value' => $item['category_name']];
            }
            $optLesson = [];
            if (!empty($memberId)) {
                $dataMember = $modelMMS->find($memberId);
                if (!empty($dataMember)) {
                    if (empty($lessonId)) {
                        $data['member_id'] = [
                            'value'    => $memberId,
                            'is_error' => false,
                            'message'  => '',
                        ];
                    } else {
                        $lessData = $modelL->where('lesson_id', $lessonId)->first();
                        if (!empty($lessData)) {
                            $optLesson = [['key' => '', 'value' => '------']];
                            $dataL     = $modelL->where('category_name', $lessData->category_name)->get(
                                ['lesson_id', 'lesson_name']
                            );
                            foreach ($dataL as $item) {
                                $optLesson[] = ['key' => $item['lesson_id'], 'value' => $item['lesson_name']];
                            }
                            $data['category_name'] = [
                                'value'    => $lessData->category_name,
                                'is_error' => false,
                                'message'  => '',
                            ];
                        }

                        $dataOL = $modelOL->getDataDetail($memberId, $lessonId, $openDate);
                        if (empty($dataOL)) {
                            return response()->json([
                                'status'        => 0,
                                'link_redirect' => action('MembershipController@review') . '?member_id=' . $memberId,
                            ]);
                        }
                        foreach ($dataOL->toArray() as $key => $value) {
                            $data[$key] = [
                                'value'    => $value,
                                'is_error' => false,
                                'message'  => '',
                            ];
                        }
                    }
                    return response()->json([
                        'data'        => $data,
                        'optCategory' => $optCategory,
                        'optLesson'   => $optLesson
                    ]);
                } else {
                    return response()->json([
                        'status'        => 0,
                        'link_redirect' => action('MembershipController@review') . '?member_id=' . $memberId,
                    ]);
                }
            }
            return response()->json([
                'status'        => 0,
                'link_redirect' => action('MembershipController@index'),
            ]);
        }
    }

    /**
     * Process submit take lesson
     * @param object $request
     * @return json
     */
    public function takeLessonSubmit(Request $request)
    {
        $arrInput = [
            'member_id'      => $request->input('member_id', null),
            'lesson_id'      => $request->input('lesson_id', null),
            'category_name'  => $request->input('category_name', null),
            'used_point'     => $request->input('used_point', null),
            'open_date'      => $request->input('open_date', null),
            'lesson_id_old'  => $request->input('lesson_id_old', null),
            'open_date_old'  => $request->input('open_date_old', null),
            'open_lesson_id' => 1,
        ];

        Validator::extend(
            'unique_open_lesson',
            function ($attribute, $value, $parameters, $validator) use ($arrInput) {
                $openLesson = DtOpenedLesson::where('lesson_id', $arrInput['lesson_id'])
                    ->where('member_id', $arrInput['member_id'])
                    ->whereDate('open_date', $arrInput['open_date'])->first();
                if (!empty($openLesson)) {
                    return false;
                }
                return true;
            }
        );

        $rules = [
            'member_id'     => 'required',
            'lesson_id'     => 'required',
            'category_name' => 'required',
            'used_point'    => 'required|numeric',
            'open_date'     => 'required|date',
            'open_lesson_id' => 'unique_open_lesson'
        ];

        if (empty($arrInput['member_id'])) {
            return response()->json([
                'status'        => 1,
                'link_redirect' => action('MembershipController@index'),
            ]);
        }

        $modelL = new MstLesson();
        $optCategory = [['key' => '', 'value' => '------']];
        foreach ($modelL->groupBy('category_name')->get(['category_name']) as $item) {
            $optCategory[] = ['key' => $item['category_name'], 'value' => $item['category_name']];
        }
        $optLesson = [['key' => '', 'value' => '------']];
        $dataL     = $modelL->where('category_name', $arrInput['category_name'])->get(
            ['lesson_id', 'lesson_name', 'require_point']
        );
        foreach ($dataL as $item) {
            $optLesson[] = ['key' => $item['lesson_id'], 'value' => $item['lesson_name']];
        }

        $validator = Validator::make($arrInput, $rules);
        if ($validator->fails()) {
            $arrResponse = [];
            $error = $validator->errors()->toArray();
            foreach ($arrInput as $key => $value) {
                $arrResponse[$key] = [
                    'value'    => is_null($value) ? '' : $value,
                    'is_error' => false,
                    'message'  => '',
                ];
                if (in_array($key, ['member_id', 'lesson_id', 'open_date']) && isset($error['open_lesson_id'])) {
                    $arrResponse[$key]['is_error'] = true;
                    $arrResponse[$key]['message']  = $error['open_lesson_id'][0];
                    continue;
                }
                if (isset($error[$key])) {
                    $arrResponse[$key]['is_error'] = true;
                    $arrResponse[$key]['message']  = $error[$key][0];
                }
            }
            return response()->json([
                'data'        => $arrResponse,
                'optCategory' => $optCategory,
                'optLesson'   => $optLesson,
                'status'      => 0,
            ]);
        } else {
            $memberData = MstMembership::find($arrInput['member_id']);
            $lessonIdKey = $arrInput['lesson_id'];
            $openDateKey = $arrInput['open_date'];
            $checkPoint  = false;
            if (!empty($arrInput['lesson_id_old']) && $arrInput['lesson_id_old'] != $arrInput['lesson_id']) {
                $lessonIdKey = $arrInput['lesson_id_old'];
                $checkPoint  = true;
            }
            if (!empty($arrInput['open_date_old']) && $arrInput['open_date_old'] != $arrInput['open_date']) {
                $openDateKey = $arrInput['open_date_old'];
            }
            $dataOL  = DtOpenedLesson::where(
                [
                    'member_id' => $arrInput['member_id'],
                    'lesson_id' => $lessonIdKey,
                    'open_date' => $openDateKey,
                ]
            )->first();
            $modelOL = new DtOpenedLesson();
            $pointData = $dataL->pluck('require_point', 'lesson_id');
            $pointRecalculate = $point = $pointData[$arrInput['lesson_id']]??0;
            if ($checkPoint && !empty($dataOL)) {
                $pointRecalculate = $pointRecalculate - $dataOL->used_point;
            }
            if ((empty($dataOL) || (!empty($dataOL) && $checkPoint)) && $pointRecalculate > $memberData->remain_point) {
                $arrResponse = [];
                foreach ($arrInput as $key => $value) {
                    $arrResponse[$key] = [
                        'value'    => is_null($value) ? '' : $value,
                        'is_error' => false,
                        'message'  => '',
                    ];
                }
                return response()->json([
                    'data'           => $arrResponse,
                    'optCategory'    => $optCategory,
                    'optLesson'      => $optLesson,
                    'param_callback' => ['POINT'],
                    'status'         => 0,
                ]);
            }
            $dataChange = [
                'used_point' => $point,
                'open_date'  => $arrInput['open_date'],
                'up_ope_cd'  => \Auth::user()->tantou_code,
                'up_date'    => date('Y-m-d H:i:s')
            ];
            try {
                DB::beginTransaction();
                if (empty($dataOL)) {
                    $dataChange['in_ope_cd'] = \Auth::user()->tantou_code;
                    $dataChange['in_date']   = date('Y-m-d H:i:s');
                    $dataChange['lesson_id'] = $arrInput['lesson_id'];
                    $dataChange['member_id'] = $arrInput['member_id'];

                    $modelOL->insert($dataChange);
                    $memberData->remain_point -= $pointRecalculate;
                    $memberData->save();
                } else {
                    $dataChange['lesson_id'] = $arrInput['lesson_id'];
                    $dataChange['open_date'] = $arrInput['open_date'];
                    $modelOL->where(
                        [
                            'member_id' => $arrInput['member_id'],
                            'lesson_id' => $lessonIdKey,
                            'open_date' => $openDateKey,
                        ]
                    )->update($dataChange);
                    if ($checkPoint) {
                        $memberData->remain_point -= $pointRecalculate;
                        $memberData->save();
                    }
                }



                DB::commit();
                return response()->json([
                    'status'        => 1,
                    'link_redirect' => action('MembershipController@review') . '?member_id=' . $arrInput['member_id'],
                ]);
            } catch (\Exception $ex) {
                DB::rollBack();
                throw new \Exception("[" . $ex->getCode() . "]" . $ex->getMessage());
            }
        }
    }

    /**
     * Get info opt change
     * @param Request $request
     *
     * @return json
     */
    public function getInfoOpt(Request $request)
    {
        $modelL   = new MstLesson();
        $modelMMS = new MstMembership();
        $modelOL  = new DtOpenedLesson();
        $arrInput = [
            'member_id'     => $request->input('member_id', null),
            'lesson_id'     => $request->input('lesson_id', null),
            'category_name' => $request->input('category_name', null),
            'used_point'    => $request->input('used_point', null),
            'open_date'     => $request->input('open_date', null)
        ];
        $optCategory = [['key' => '', 'value' => '------']];
        foreach ($modelL->groupBy('category_name')->get(['category_name']) as $item) {
            $optCategory[] = ['key' => $item['category_name'], 'value' => $item['category_name']];
        }
        $optLesson = [['key' => '', 'value' => '------']];
        $dataL     = $modelL->where('category_name', $arrInput['category_name'])->get(
            ['lesson_id', 'lesson_name', 'require_point']
        );
        foreach ($dataL as $item) {
            $optLesson[] = ['key' => $item['lesson_id'], 'value' => $item['lesson_name']];
        }
        $pointData = $dataL->pluck('require_point', 'lesson_id');

        $point = $pointData[$arrInput['lesson_id']]??'';
        foreach ($arrInput as $key => $value) {
            if ($key === 'used_point') {
                $arrResponse[$key] = [
                    'value'    => $point,
                    'is_error' => false,
                    'message'  => '',
                ];
            } else {
                $arrResponse[$key] = [
                    'value'    => is_null($value) ? '' : $value,
                    'is_error' => false,
                    'message'  => '',
                ];
            }
        }
        return response()->json([
            'data'        => $arrResponse,
            'optCategory' => $optCategory,
            'optLesson'   => $optLesson,
            'status'      => 0,
        ]);
    }

    public function karutePdf(Request $request)
    {
        $data = [];
        $dataView = view('membership.export_pdf')->with(['data' => $data]);
        $pdf      = \PDF::loadView('membership.export_pdf', compact('data'));
        $pdf->setOptions(['isFontSubsettingEnabled' => true]);
        $pdf->setPaper('A4', 'portrait');
        $fileName = date('YmdHis') . '_karute.pdf';
        return $pdf->stream();
    }
}
