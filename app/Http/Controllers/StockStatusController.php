<?php
/**
 * Controller for store status
 *
 * @package    App\Http\Controllers
 * @subpackage DeliveryController
 * @copyright  Copyright (c) 2018 RiverCrane! Corporation. All Rights Reserved.
 * @author     Le Hung<le.hung.rcvn2012@gmail.com>
 */

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Backend\MstStockStatus;
use Validator;
use Auth;
use Config;
use DB;

class StockStatusController extends Controller
{
    /**
     * Load view.
     */
    public function index()
    {
        return view('stock-status.index');
    }

    /**
     * Get all data.
     * Return $object json
     */
    public function dataList(Request $request)
    {
        if ($request->ajax() === true) {
            $modelSS = new MstStockStatus();

            $arrSort = [
                'product_code'      => $request->input('sort_product_code', null),
                'nanko_num'         => $request->input('sort_nanko_num', null),
                'glsc_num'          => $request->input('sort_glsc_num', null),
                'order_zan_num'     => $request->input('sort_order_zan_num', null),
                'rep_orderring_num' => $request->input('sort_rep_orderring_num', null),
                'return_num'        => $request->input('sort_return_num', null),
                'wait_delivery_num' => $request->input('sort_wait_delivery_num', null),
                'up_date'           => $request->input('sort_up_date', null) ,
            ];
            $arrSearch = [
                'product_code' => $request->input('product_code', null),
                'nanko_num'    => $request->input('nanko_num', null),
                'glsc_num'     => $request->input('glsc_num', null),
                'nanko_glsc'   => $request->input('nanko_glsc', null),
                'return_num'            => $request->input('return_num', null),
                'wait_delivery_num'     => $request->input('wait_delivery_num', null),
                'rep_orderring_num'     => $request->input('rep_orderring_num', null),
                'order_zan_num'         => $request->input('order_zan_num', null),
                'free_zan_num'          => $request->input('free_zan_num', null),
                'cheetah_status'        => $request->input('cheetah_status', null),
                'per_page'              => $request->input('per_page', null),
            ];
            
            foreach (Config::get("common.cheetah_status") as $key => $value) {
                $arrCheetahStatus[] = ['key' => $key, 'value' => $value];
            }
            $data = $modelSS->getData($arrSearch, array_filter($arrSort));
            return response()->json([
                'data'      => $data,
                'cheetah_status' => Config::get('common.cheetah_status'),
                'cheetah_status_opt' => $arrCheetahStatus
            ]);
        }
    }
}
