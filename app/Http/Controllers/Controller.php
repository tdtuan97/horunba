<?php
/**
 * Controller
 *
 * @package    App\Http\Controllers
 * @subpackage Controllers
 * @copyright  Copyright (c) 2018 RiverCrane! Corporation. All Rights Reserved.
 * @author     Truong Nghia<truong.van.nghia@rivercrane.vn>
 */
namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Support\Facades\Validator;
use Storage;
use File;
use Log;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
    /**
     * Upload file to storage
     * @param   string  $fileUpload file upload
     * @return  array   contains success/unsuccess flag and msg
     */
    public function upload($fileUpload, $nameCsv)
    {
        $file = array('importFile' => $fileUpload);
        Validator::extend('mineCSV', function ($parameters, $value, $attribute) {
            $ext = $value->getClientOriginalExtension();
            if ($ext === "csv") {
                return true;
            } else {
                return false;
            }
        });
        Validator::extend('amazonUri', function ($parameters, $value, $attribute) {
            $ext = $value->getClientOriginalExtension();
            if ($ext === "txt" || $ext === "tab") {
                return true;
            } else {
                return false;
            }
        });
        Validator::extend('isSijs', function ($parameters, $value, $attribute) {
            $encode = mb_detect_encoding(File::get($value), mb_list_encodings(), true);
            if ($encode === "SJIS" || $encode === "SJIS-win") {
                return true;
            } else {
                return false;
            }
        });
        // setting up rules
        $rules = array('importFile' => 'required|mineCSV|isSijs|max:10000');
        if ($nameCsv === 'amazon_pay_csv') {
            $rules = array('importFile' => 'required|amazonUri|max:10000');
        } elseif (in_array($nameCsv, ['pro_mfk_csv'])) {
            $rules = array('importFile' => 'required|max:10000');
        }
        // doing the validation, passing post data, rules and the messages
        $validator = Validator::make($file, $rules);
        if ($validator->fails()) {
            Log::error($validator->errors());
            return array(
                "process" => 'uploadCsv',
                "flg"     => 0,
                "msg"     => $validator->errors()->toArray()['importFile'][0],
            );
        }
        if ($fileUpload->isValid()) {
            $extension = $fileUpload->getClientOriginalExtension(); // getting image extension
            if ($nameCsv === 'amazon_pay_csv') {
                $extension = 'csv';
            }
            $fileName = date("YmdHis").'.'.$extension; // renameing file
            Storage::disk('local')->put($fileName, File::get($fileUpload));
        }
        return array(
                "flg" => 1,
                "msg" => $fileName
            );
    }
}
