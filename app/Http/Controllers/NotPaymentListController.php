<?php
/**
 * Controller for not payment list
 *
 * @package    App\Http\Controllers
 * @subpackage NotPaymentListController
 * @copyright  Copyright (c) 2018 RiverCrane! Corporation. All Rights Reserved.
 * @author     Van Qui<van.qui@rivercrane.vn>
 */
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Backend\MstOrder;
use App\Models\Backend\MstMall;
use Validator;

class NotPaymentListController extends Controller
{
    /**
     * Load view.
     */
    public function index()
    {
        return view('not-payment-list.index');
    }

    /**
     * Show data list view
     *
     * @param   $request  Request
     * @return json
     */
    public function dataList(Request $request)
    {
        if ($request->ajax() === true) {
            $arrSearch    = array(
                'mall_id'           => $request->input('mall_id', null),
                'order_date_from'   => $request->input('order_date_from', null),
                'order_date_to'     => $request->input('order_date_to', null),
                'received_order_id' => $request->input('received_order_id', null),
                'full_name'         => $request->input('full_name', null),
                'full_name_kana'    => $request->input('full_name_kana', null),
                'payment_account'   => $request->input('payment_account', null),
                'request_price'     => $request->input('request_price', null),
                'per_page'          => $request->input('per_page', null),
            );
            $arrSort = [
                'mall_id'            => $request->input('sort_mall_id', null),
                'order_date'         => $request->input('sort_order_date', null),
                'received_order_id'  => $request->input('sort_received_order_id', null)
            ];

            $modelO    = new MstOrder();
            $modelMall = new MstMall();

            $rules = [
                'order_date_from' => 'date',
                'order_date_to'   => 'date|after_or_equal:order_date_from'
            ];
            $validator = Validator::make($request->all(), $rules);
            if ($validator->fails()) {
                $data = [];
            } else {
                $data = $modelO->getDataNotPaymentList($arrSearch, $arrSort);
            }

            $mallData  = $modelMall->getData();

            return response()->json([
                'data'     => $data,
                'mallData' => $mallData->toArray()
            ]);
        }
    }
}
