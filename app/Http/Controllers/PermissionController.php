<?php
/**
 * Controller for Controller for not permission
 *
 * @package    App\Http\Controllers
 * @subpackage PermissionController
 * @copyright  Copyright (c) 2019 RiverCrane! Corporation. All Rights Reserved.
 * @author     Truong Nghia<truong.van.nghia@rivercrane.vn>
 */

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use Auth;
use App\Models\Backend\MstPermission;
use App\Models\Backend\MstController;

class PermissionController extends Controller
{
    /**
     * Load view.
     */
    public function index()
    {
        return view('permission.index');
    }

    /**
     * Get all data.
     *
     * @param $request Request
     * @return json
     */
    public function dataList(Request $request)
    {
        if ($request->ajax() === true) {
            $modelPer  = new MstPermission();
            $modelC    = new MstController();
            $arrSearch = [
                'depart_id' => $request->input('depart_id', null),
                'per_page' => $request->input('per_page', null),
            ];
            $data        = $modelPer->getData($arrSearch);
            $departOpt   = $modelPer->getDepartOption();
            $dataControl = $modelC->getDepartOption();
            $controlOpt  = [['key' => '' , 'value' => '-----']];
            foreach ($dataControl as $value) {
                $temp = empty($value->screen_name) ?
                                    $value->screen_class :
                                    ($value->screen_name . '-' . $value->screen_class);
                $controlOpt[] = [
                    'key'   => $value->id,
                    'value' => $temp
                ];
            }
            return response()->json([
                'data'       => $data,
                'departOpt'  => $departOpt,
                'controlOpt' => $controlOpt
            ]);
        }
    }

    /**
     * Process update permission.
     *
     * @param $request Request
     * @return json
     */
    public function updatePermission(Request $request)
    {
        if ($request->ajax() === true) {
            $departId = $request->input('depart_id', null);
            $screenClass = $request->input('screen_class', null);
            $name = $request->input('name', null);
            $value = $request->input('value', null);
            if (!empty($departId) && !empty($screenClass)) {
                $modelPer = new MstPermission();
                $modelPer->updateData(
                    [
                        'depart_id'    => $departId,
                        'screen_class' => $screenClass,
                    ],
                    [
                        $name => $value
                    ]
                );
            }
            return response()->json([
                'status' => 1
            ]);
        }
    }

    /**
     * Save Permission
     *
     * @param $request Request
     * @return json
     */
    public function savePermission(Request $request)
    {
        if ($request->ajax() === true) {
            $model = new MstPermission();
            $rules = [
                'depart_id'    => 'required',
                'depart_nm'    => 'required',
                'func_cap_cd'  => 'required',
                'func_cd'      => 'required',
                'screen_class' => 'required',
            ];
            $validator = Validator::make($request->all(), $rules);
            if ($validator->fails()) {
                return response()->json([
                    'status'  => 0,
                    'message' => $validator->errors(),
                ]);
            }
            $dataCheck = $model->getDataCheckExists(
                $request->input('depart_id', null),
                $request->input('screen_class', null)
            );
            if ($dataCheck->count() !== 0 && $request->input('action', null) !== 'edit') {
                return response()->json([
                    'status'  => 0,
                    'message' => ['exists_record' => __('messages.permission_exists')],
                ]);
            } else {
                if ($request->input('action', null) === 'edit') {
                    $model->updateData(
                        [
                            'depart_id'    => $request->input('depart_id', null),
                            'screen_class' => $request->input('screen_class', null),
                        ],
                        [
                            'depart_nm'     => $request->input('depart_nm', null),
                            'func_cap_cd'   => $request->input('func_cap_cd', null),
                            'func_cd'       => $request->input('func_cd', null),
                            'action_denied' => $request->input('action_denied', null),
                        ]
                    );
                } else {
                    $model->depart_id     = $request->input('depart_id', null);
                    $model->screen_class  = $request->input('screen_class', null);
                    $model->depart_nm     = $request->input('depart_nm', null);
                    $model->func_cap_cd   = $request->input('func_cap_cd', null);
                    $model->func_cd       = $request->input('func_cd', null);
                    $model->action_denied = $request->input('action_denied', null);
                    $model->save();
                }
            }

            return response()->json([
                'status'  => 1,
            ]);
        }
    }

    /**
     * Process delete Permission
     *
     * @param $request Request
     * @return json
     */
    public function deletePermission(Request $request)
    {
        if ($request->ajax() === true) {
            $id = $request->input('key', null);
            $model = new MstPermission();
            $model->where('depart_id', $request->input('depart_id', null))
                      ->where('screen_class', $request->input('screen_class', null))
                      ->delete();
            return response()->json([
                'status' => 1
            ]);
        }
    }
}
