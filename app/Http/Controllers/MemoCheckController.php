<?php
/**
 * MemoCheck Controller
 *
 * @package     App\Controllers
 * @subpackage  MemoCheckController
 * @copyright   Copyright (c) 2017 RiverCrane! Corporation. All Rights Reserved.
 * @author      le.hung<le.hung.rcvn2012@gmail.com>
 */

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Backend\DtOrderUpdateLog;
use App\Models\Backend\MstTantou;
use App\Models\Backend\MstMall;
use Validator;
use App\User;
use Auth;

class MemoCheckController extends Controller
{
    /**
     * Show template for append data
     *
     * @param   $request  Request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        return view('memo-check.index');
    }
    /**
     * Show data list view
     *
     * @param   $request  Request
     * @return json
     */
    public function dataList(Request $request)
    {
        if ($request->ajax() === true) {
            $arraySearch    = array(
                'in_date_from'       => $request->input('in_date_from', null),
                'in_date_to'         => $request->input('in_date_to', null),
                'per_page'           => $request->input('per_page', null),
                'operator'           => $request->input('operator', null),
                'search_log_title'   => $request->input('search_log_title', null),
                'search_log_content' => $request->input('search_log_content', null),
                'search_log_status'  => $request->input('search_log_status', null),
                'name_jp'            => $request->input('name_jp', null),
            );
            $arraySort = [];
            $modelMall      = new MstMall();
            $model          = new DtOrderUpdateLog();
            $modelTantou    = new MstTantou();
            $mallData       = $modelMall->getData();
            $opeData        = $modelTantou->getDataSelectBox();
            $dataTantouOptions  = $modelTantou->get(['tantou_code', 'tantou_last_name']);
            $tantouOptions[]  = ['key' => '' , 'value' => '-----'];
            if (!empty($dataTantouOptions)) {
                foreach ($dataTantouOptions as $key => $value) {
                    $tantouOptions[] = [
                        'key' => $value->tantou_code,
                        'value' => $value->tantou_last_name
                    ];
                }
            }
            $data   = $model->getDataLog($arraySearch, array_filter($arraySort), Auth::user()->tantou_code);
            return response()->json([
                        'data'          => $data,
                        'mallData'      => $mallData->toArray(),
                        'opeData'       => $opeData->toArray(),
                        'tantouOptions' => $tantouOptions,
                        'sort'          => $arraySort,
                        'sorted'        => ($request->input('sort')) ? true : false,
                        'params'        => $request->all()
            ]);
        }
    }
    /**
     * Count memo
     *
     * @param   $request  Request
     * @return json
     */
    public function countMemo(Request $request)
    {
        if ($request->ajax() === true) {
            $model          = new DtOrderUpdateLog();
            $count   = $model->countDataLog(Auth::user()->tantou_code);
            return response()->json([
                'count'      => $count
            ]);
        }
    }

    /**
     * Create/Edit mail template
     *
     * @param   $request  Request
     * @return json
     */
    public function save(Request $request)
    {
        $params = $request->all();
        $model          = new DtOrderUpdateLog();
        if ($request->ajax() === true) {
            $method = $request->input('method', '');
            $rules['log_id'] = 'required|numeric|exists:horunba.dt_order_update_log,log_id';
            if (isset($params['log_title'])) {
                $rules['log_title'] = 'required|max:125';
            }
            if (isset($params['log_contents'])) {
                $rules['log_contents'] = 'required|max:512';
            }
            if (isset($params['up_ope_cd'])) {
                $rules['up_ope_cd'] = 'required|exists:horunba.mst_tantou,tantou_code';
            }
            if (isset($params['log_status'])) {
                $rules['log_status'] = 'required|string';
            }
            $validator = Validator::make($request->all(), $rules);
            if ($validator->fails()) {
                return response()->json([
                    'status'  => 0,
                    'message' => $validator->errors(),
                    'data'    => []
                ]);
            }
            $logId = $request->input('log_id', '');
            $dataUpdate = [];
            if (isset($params['log_title']) && isset($params['log_contents'])) {
                $dataUpdate['log_title'] = $params['log_title'];
                $dataUpdate['log_contents'] = $params['log_contents'];
            }
            if (isset($params['log_status'])) {
                $status = 0;
                if ($params['log_status'] === '済') {
                    $status = 0;
                } elseif ($params['log_status'] === '未') {
                    $status = 1;
                }
                $dataUpdate['log_status'] = $status;
            }
            if (isset($params['up_ope_cd'])) {
                $dataUpdate['up_ope_cd'] = $params['up_ope_cd'];
            }
            $model->updateData(['log_id' => $logId], $dataUpdate);
            return response()->json([
                'status'  => 1,
                'method'  => 'save'
            ]);
        }
        return view('memo-check.index');
    }
}
