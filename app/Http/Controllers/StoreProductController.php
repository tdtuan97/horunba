<?php
/**
 * Controller for store product
 *
 * @package    App\Http\Controllers
 * @subpackage StoreProductController
 * @copyright  Copyright (c) 2018 RiverCrane! Corporation. All Rights Reserved.
 * @author     Van Qui<van.qui@rivercrane.vn>
 */

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Backend\MstStoreProduct;
use App\Models\Backend\MstProductControl;
use App\Models\Backend\MstProduct;
use Validator;
use Auth;
use Config;
use DB;

class StoreProductController extends Controller
{
    /**
     * Load view.
     */
    public function index()
    {
        return view('store-product.index');
    }

    /**
     * Get all data.
     * Return $object json
     */
    public function dataList(Request $request)
    {
        if ($request->ajax() === true) {
            $modelSP = new MstStoreProduct();

            $arrDefault = [['key' => '', 'value' => '-----']];
            $opt        = array_merge($arrDefault, Config::get('common.flg'));

            $janOpt        = $opt;
            $dfHandlingOpt = $opt;

            $arrSort = [
                'product_code'     => $request->input('sort_product_code', null),
                'store_product_id' => $request->input('sort_store_product_id', null),
                'product_jan'      => $request->input('sort_product_jan', null)
            ];
            $rule = [
                'store_product_id_from' => 'nullable',
                'store_product_id_to'   => 'nullable'
            ];
            $arrSearch = [
                'product_code_from'     => $request->input('product_code_from', null),
                'product_code_to'       => $request->input('product_code_to', null),
                'store_product_id_from' => $request->input('store_product_id_from', null),
                'store_product_id_to'   => $request->input('store_product_id_to', null),
                'product_jan_new'       => $request->input('product_jan_new', null),
                'df_handling'           => $request->input('df_handling', null),
                'per_page'              => $request->input('per_page', null)
            ];
            $validator = Validator::make($arrSearch, $rule);
            if ($validator->fails()) {
                $data = [];
            } else {
                $data = $modelSP->getData($arrSearch, $arrSort);
            }

            return response()->json([
                'data'          => $data,
                'janOpt'        => $janOpt,
                'dfHandlingOpt' => $dfHandlingOpt
            ]);
        }
    }

    /**
     * Check is_updated
     * @param  Request $request
     * @return json
     */
    public function changeIsUpdated(Request $request)
    {
        if ($request->ajax() === true) {
            $rules = [
                'product_code.*' => 'required|exists:horunba.mst_store_product,product_code',
                'is_updated'     => 'required|numeric|in:1,2'
            ];

            $validator = Validator::make($request->all(), $rules);
            if ($validator->fails()) {
                return response()->json([
                    'status'   => 0,
                    'messages' => implode("\n", $validator->errors()->all())
                ]);
            } else {
                try {
                    $productCode = $request->input('product_code');
                    $isUpdated   = $request->input('is_updated');
                    $modelSP     = new MstStoreProduct();
                    $modelSP->updateByProductCodes($productCode, ['is_updated' => $isUpdated, 'up_date' => now()]);
                    return response()->json([
                        'status'   => 1,
                        'messages' => 'Update success'
                    ]);
                } catch (\Exception $ex) {
                    return response()->json([
                        'status'  => 0,
                        'messages' => $ex->getMessage()
                    ]);
                }
            }
        }
    }
    /**
     * Process delete or up smaregi
     * @param  Request $request
     * @return json
     */
    public function processSmaregi(Request $request)
    {
        if ($request->ajax() === true) {
            $action      = $request->action;
            $productCode = $request->product_code;
            $modelPCC    = new MstProductControl();
            $modelSP     = new MstStoreProduct();
            try {
                DB::beginTransaction();
                if ($action === 'up') {
                    $datas = $modelPCC->getDataProcessUpSmaregi($productCode);
                    if (!empty($datas)) {
                        $dataReplace['product_code']              = $datas->product_code;
                        $dataReplace['store_product_id']          = empty($datas->store_product_id) ? $datas->product_jan : $datas->store_product_id;
                        $dataReplace['df_handling']               = $datas->df_handling;
                        $dataReplace['product_jan']               = $datas->product_jan;
                        $dataReplace['store_product_category_id'] = '2020';
                        $dataReplace['store_product_name']        = $datas->product_name_long;
                        $dataReplace['product_tax_division']      = null;
                        $dataReplace['product_price_division']    = null;
                        $dataReplace['product_price']             = empty($datas->price_calc_diy_komi) ? $datas->price_hanbai : $datas->price_calc_diy_komi;
                        $dataReplace['product_cost']              = $datas->price_invoice;
                        $dataReplace['product_size']              = $datas->product_size;
                        $dataReplace['product_color']             = $datas->product_color;
                        $dataReplace['product_tag']               = null;
                        $dataReplace['product_group_code']        = null;
                        $dataReplace['product_url']               = null;
                        $dataReplace['stock_control_division']    = 0;
                        $dataReplace['display_flag']              = 1;
                        $dataReplace['point_not_applicable']      = 0;
                        $dataReplace['is_updated']                = 1;
                        $dataReplace['is_send_mail']              = 0;
                        $dataReplace['product_jan_new']           = null;
                        $dataReplace['in_date']                   = empty($datas->in_date) ? now() : $datas->in_date;
                        $dataReplace['up_date']                   = now();

                        $modelSP->replace($dataReplace);
                        $modelPCC->where('product_code', $productCode)->update(['df_handling' => 1]);
                        DB::commit();
                        //Call batch
                        $name = 'process:smaregi-add-product';
                        $path = base_path('artisan');
                        $cmd  = "php $path $name";
                        if (substr(php_uname(), 0, 7) == "Windows") {
                            $cmd .= " >NUL 2>NUL";
                            pclose(popen('start /B cmd /C "' . $cmd . '"', 'r'));
                        } else {
                            exec($cmd . " > /dev/null 2>/dev/null &");
                        }
                    }
                } else {
                    $modelSP->where('product_code', $productCode)->update(['is_updated' => 2]);
                    DB::commit();
                    $name = 'process:smaregi-del-product';
                    $path = base_path('artisan');
                    $cmd  = "php $path $name";
                    if (substr(php_uname(), 0, 7) == "Windows") {
                        $cmd .= " >NUL 2>NUL";
                        pclose(popen('start /B cmd /C "' . $cmd . '"', 'r'));
                    } else {
                        exec($cmd . " > /dev/null 2>/dev/null &");
                    }
                }
            } catch (\Exception $e) {
                DB::rollback();
                return response()->json([
                    'status'  => 0,
                    'message' => $e->getMessage()
                ]);
            }
            return response()->json([
                'status'  => 1,
            ]);
        }
    }

    /**
     * Process add smaregi
     * @param  Request $request
     * @return json
     */
    public function addNew(Request $request)
    {
        if ($request->ajax() === true) {
            if (isset($request->dataProduct)) {
                $modelPCC    = new MstProductControl();
                $modelSP     = new MstStoreProduct();
                try {
                    DB::beginTransaction();
                    foreach ($request->dataProduct as $key => $value) {
                        if (!empty($value['productMessage']) || empty($value['product_code'])) {
                            DB::rollback();
                            return response()->json([
                                'status'         => 0,
                                'message'        => __('messages.product_not_exists'),
                                'productMessage' => __('messages.product_not_exists'),
                                'index'          => $key
                            ]);
                        }
                        $datas = $modelPCC->getDataProcessUpSmaregi($value['product_code']);
                        if (!empty($datas)) {
                            $dataReplace['product_code']              = $datas->product_code;
                            $dataReplace['store_product_id']          = empty($datas->store_product_id) ? $datas->product_jan : $datas->store_product_id;
                            $dataReplace['df_handling']               = 1;
                            $dataReplace['product_jan']               = $datas->product_jan;
                            $dataReplace['store_product_category_id'] = '2020';
                            $dataReplace['store_product_name']        = $datas->product_name_long;
                            $dataReplace['product_tax_division']      = null;
                            $dataReplace['product_price_division']    = null;
                            $dataReplace['product_price']             = empty($datas->price_calc_diy_komi) ? $datas->price_hanbai : $datas->price_calc_diy_komi;
                            $dataReplace['product_cost']              = $datas->price_invoice;
                            $dataReplace['product_size']              = $datas->product_size;
                            $dataReplace['product_color']             = $datas->product_color;
                            $dataReplace['product_tag']               = null;
                            $dataReplace['product_group_code']        = null;
                            $dataReplace['product_url']               = null;
                            $dataReplace['stock_control_division']    = 0;
                            $dataReplace['display_flag']              = 1;
                            $dataReplace['point_not_applicable']      = 0;
                            $dataReplace['is_updated']                = 1;
                            $dataReplace['is_send_mail']              = 0;
                            $dataReplace['product_jan_new']           = null;
                            $dataReplace['in_date']                   = empty($datas->in_date) ? now() : $datas->in_date;
                            $dataReplace['up_date']                   = now();

                            $modelSP->replace($dataReplace);
                            $modelPCC->where('product_code', $value['product_code'])->update(['df_handling' => 1]);
                            //Call batch
                            $name = 'process:smaregi-add-product';
                            $path = base_path('artisan');
                            $cmd  = "php $path $name";
                            if (substr(php_uname(), 0, 7) == "Windows") {
                                $cmd .= " >NUL 2>NUL";
                                pclose(popen('start /B cmd /C "' . $cmd . '"', 'r'));
                            } else {
                                exec($cmd . " > /dev/null 2>/dev/null &");
                            }
                        }
                    }
                    DB::commit();
                } catch (\Exception $e) {
                    DB::rollback();
                    return response()->json([
                        'status'  => 0,
                        'message' => $e->getMessage()
                    ]);
                }

                return response()->json([
                    'status'    => 1,
                ]);
            }
            $arrDefault = [
                'product_code'       => '',
                'product_name'       => '',
                'product_jan'        => '',
                'product_maker_code' => '',
            ];
            return response()->json([
                'data'    => [],
                'default' => $arrDefault
            ]);
        }
        return view('store-product.add');
    }

    /**
     * Get data product
     * @param  Request $request
     * @return json
     */
    public function getDataProduct(Request $request)
    {
        if ($request->ajax() === true) {
            $modelP = new MstProduct();
            $dataProduct = $modelP->getDataProductAddSmaregi($request->all());
            if (empty($dataProduct)) {
                return response()->json([
                    'productMessage' => __('messages.product_not_exists'),
                ]);
            } else {
                return response()->json([
                    'productInfo' => $dataProduct->toArray(),
                ]);
            }
        }
    }
}
