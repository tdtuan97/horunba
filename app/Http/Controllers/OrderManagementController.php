<?php
/**
 * OrderManagement Controller
 *
 * @package     App\Controllers
 * @subpackage  OrderManagementController
 * @copyright   Copyright (c) 2017 RiverCrane! Corporation. All Rights Reserved.
 * @author      le.hung<le.hung.rcvn2012@gmail.com>
 */

namespace App\Http\Controllers;

use Barryvdh\Debugbar\Facade as Debugbar;
use Illuminate\Http\Request;
use App\Models\Backend\MstOrder;
use App\Models\Backend\MstMall;
use App\Models\Backend\DtOrderUpdateLog;
use App\Models\Backend\DtOrderProductDetail;
use App\Models\Backend\MstOrderStatus;
use App\Models\Backend\MstSettlementManage;
use App\Models\Backend\DtReturn;
use App\Models\Backend\MstCustomer;
use App\Models\Backend\MstShippingCompany;
use App\Models\Backend\MstShippingFee;
use App\Models\Backend\MstCancelReason;
use App\Models\Backend\MstProduct;
use App\Models\Backend\MstCodeFee;
use App\Models\Backend\DtDelivery;
use App\Models\Backend\MstOrderDetail;
use App\Models\Backend\TAdmin;
use App\Models\Backend\TOrder;
use App\Models\Backend\TOrderDetail;
use App\Models\Backend\Cheetah;
use App\Models\Backend\TSeq;
use App\Models\Backend\DtOrderToSupplier;
use App\Models\Backend\TCustomer;
use App\Models\Backend\TOrderDirect;
use App\Models\Backend\MstStockStatus;
use App\Models\Backend\MstOrderSubStatus;
use App\Models\Backend\DtRePayment;
use App\Models\Backend\DtReceiveMailList;
use App\Models\Backend\DtSendMailList;
use App\Models\Backend\DtReorder;
use App\Models\Backend\MstTantou;
use App\Models\Backend\MstProductSet;
use App\Models\Backend\MstBacklistComment;
use App\Models\Backend\MstClaim;
use App\Custom\Utilities;
use App;
use Validator;
use Storage;
use App\User;
use Auth;
use PDF;
use DB;
use Config;
use App\Http\Controllers\Common;
use Cache;

class OrderManagementController extends Controller
{
    public $error = null;

    /**
     * List rules check require.
     *
     * @var array
     */
    private $checkRules = [
        // 'receive_id_checkPostalCodeAndAddress'         => 'checkPostalCodeAndAddress',
        'receive_id_checkPostalCodeAndAddressReceiver' => 'checkPostalCodeAndAddressReceiver',
        //'receive_id_checkPostalCodeAndAddressReceiverLength' => 'checkPostalCodeAndAddressReceiverLength',
        'receive_id_checkLimitRequestPrice'            => 'checkLimitRequestPrice',
        'receive_id_checkTelDigits'                    => 'checkTelDigits',
        'receive_id_checkTelDigits1'                   => 'checkTelDigits1',
        'receive_id_checkBlackList'                    => 'checkBlackList',
        'receive_id_checkChargeFeeTransport'           => 'checkChargeFeeTransport',
        'receive_id_checkChargeFeeDelivery'            => 'checkChargeFeeDelivery',
        'receive_id_checkChargeFeeCulDelivery'         => 'checkChargeFeeCulDelivery',
        'receive_id_checkPaymentDeliveryFee'           => 'checkPaymentDeliveryFee',
        'receive_id_checkPaidByPoint'                  => 'checkPaidByPoint',
        'receive_id_checkPaidNotByPoint'               => 'checkPaidNotByPoint',
        'order_firstname'                              => 'required',
        'order_lastname'                               => 'required',
        'order_tel'                                    => 'required',
        'order_zip_code'                               => 'required',
        'total_price'                                  => 'required|not_in:0',
        'order_date'                                   => 'required|date',
        'request_price'                                => 'required',
        'payment_method'                               => 'required|not_in:0',
        'product_code'                                 => 'checkProductCodeExists|checkProductSetValid',
        'price'                                        => 'required|not_in:0',
        'quantity'                                     => 'required|not_in:0',
        'ship_to_last_name'                            => 'required',
        'ship_zip_code'                                => 'required',
        'ship_tel_num'                                 => 'required',
    ];

    /**
     * List message result validate.
     *
     * @var array
     */
    private $message = [
        'total_price.not_in'                  => 'The total_price field must be greater than 0',
        'request_price.not_in'                => 'The request_price field must be greater than 0',
        'payment_method.not_in'               => 'The payment_method field must be greater than 0',
        'price.not_in'                        => 'The price field must be greater than 0',
        'quantity.not_in'                     => 'The quantity field must be greater than 0',
        // 'product_code.required'               => '納期表示のテーブルに存在していない商品があります。',
        'product_code.checkProductCodeExists' => 'マスタDBに存在していない商品があります。',
    ];

    /**
     * Show the index.
     *
     * @param   $request  Request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        return view('order-management.index');
    }

    /**
    * Get all data.
    *
    * @param Request $request
    * Return json    $object
    */
    public function dataList(Request $request)
    {
        $arraySearch = [];
        if ($request->ajax() === true) {
            $arraySearch    = array(
                'payment_method'       => $request->input('payment_method', null),
                'product_code'         => $request->input('product_code', null),
                'per_page'             => $request->input('per_page', 20),
                'order_status'         => $request->input('order_status', null),
                'order_sub_status'     => $request->input('order_sub_status', null),
                'full_name'            => $request->input('full_name', null),
                'name_kana'            => $request->input('name_kana', null),
                'tel_num'              => $request->input('tel_num', null),
                'receive_id'           => $request->input('receive_id', null),
                'name_jp'              => $request->input('name_jp', null),
                'is_mall_cancel'       => $request->input('is_mall_cancel', null),
                'is_mall_update'       => $request->input('is_mall_update', null),
                'order_sub_status'     => $request->input('order_sub_status', null),
                'received_order_id'    => $request->input('received_order_id', null),
                'is_delay'             => $request->input('is_delay', null),
                'delay_priod_from'     => $request->input('delay_priod_from', null),
                'delay_priod_to'       => $request->input('delay_priod_to', null),
                'another_order_status' => $request->input('another_order_status', null),
                'address'              => $request->input('address', null),
                'request_price_from'   => $request->input('request_price_from', null),
                'request_price_to'     => $request->input('request_price_to', null),
            );
            $rules = [
                'order_date_from' => 'date',
                'order_date_to'   => 'date|after_or_equal:order_date_from',
                'request_price_from' => 'numeric',
                'request_price_to'   => 'numeric'
            ];
            $validator = Validator::make($request->all(), $rules);
            if ($validator->fails()) {
                $this->error = $validator->errors();
            } else {
                $arraySearch['order_date_from'] = $request->input('order_date_from', null);
                $arraySearch['order_date_to']   = $request->input('order_date_to', null);
            }
            $arrSort    = array(
                'full_name'         => $request->input('sort_full_name', null),
                'name_jp'           => $request->input('sort_name_jp', null),
                'order_date'        => $request->input('sort_order_date', null),
                'order_status'      => $request->input('sort_order_status', null),
                'payment_method'    => $request->input('sort_payment_method', null),
                'receive_id'        => $request->input('sort_receive_id', null),
                'received_order_id' => $request->input('sort_received_order_id', null),
                'request_price'     => $request->input('sort_request_price', null),
            );
            $model                  = new MstOrder();
            $modelMall              = new MstMall();
            $modelOrderStatus       = new MstOrderStatus();
            $modelOrderSubStatus    = new MstOrderSubStatus();
            $modelSettlementManage  = new MstSettlementManage();
            $mallData               = $modelMall->getDataSelectBox();
            $orderStatusData        = $modelOrderStatus->getDataSelectBox();
            $orderSubStatusData     = $modelOrderSubStatus->getData();
            $settlementManageData   = $modelSettlementManage->getDataSelectBox();
            $type       = $request->input('type', null);
            if (!is_null($type)) {
                $data = $model->getData($arraySearch, $arrSort, ['type' => $type]);
                $pageData = $data;
            } else {
                $arrIgnore = [
                    'page',
                    'per_page'
                ];
                $flag = Utilities::checkPerformance($arraySearch, $arrSort, $arrIgnore);
                if ($flag) {
                    $pageData   = $model->getDataPage($arraySearch['per_page']);
                    $listdata   = $pageData->toArray();
                    $listdata   = $listdata['data'];
                    $arrayOrder = [];
                    foreach ($listdata as $item) {
                        $arrayOrder[] = $item['receive_id'];
                    }
                    $data = $model->getDataAll($arrayOrder);
                } else {
                    $data = $model->getData($arraySearch, $arrSort);
                    $pageData = $data;
                }
            }
            return response()->json([
                'data'      => $data,
                'pageData'  => $pageData,
                'mall_data' => $mallData,
                'settlement_manage_data' => $settlementManageData,
                'order_status_data'      => $orderStatusData,
                'order_sub_status_data'  => $orderSubStatusData,
                'error'     => $this->error,
                'sort'      => $arrSort,
                'sorted'    => ($request->input('sort')) ? true : false,
                'params'    => $request->all()
            ]);
        }
    }
    /**
    * Get item data.
    *
    * @param   Request  $request
    * @return  json     $object
    */
    public function detail(Request $request)
    {
        if ($request->ajax() === true) {
            $modelCancelReason = new MstCancelReason();
            $dataCancelReason = $modelCancelReason->get(['reason_id', 'reason_content']);
            $optionCancelReason = [];
            $optionCancelReason[''] = '-----';
            if (count($dataCancelReason) > 0) {
                foreach ($dataCancelReason as $key => $value) {
                    $optionCancelReason[$value->reason_id] = $value->reason_content;
                }
            }
            $model = new MstOrder();
            $paramKey['receive_id'] = $request->input('receive_id', null);
            $data = $model->getItem($paramKey);
            if ($data === null) {
                return response()->json([
                    'data'      => null
                ]);
            }
            if ((int) $data['is_delay'] === 0) {
                $data['hold_by'] = '';
            } else {
                $data['hold_by'] = $data['delay_by'];
            }
            $data['delivery_type'] = 0;
            if (!empty($data)) {
                $modelDetail = new  DtOrderProductDetail();
                $dataDetail = $modelDetail->getItem($paramKey);
                $supTmp         = 0;
                $delivery_type  = 0;
                $delivery_type_1_3 = 0;
                $delivery_type_2 = 0;
                foreach ($dataDetail as $key => $value) {
                    if ($value->delivery_type === 1 || $value->delivery_type === 3) {
                        $delivery_type_1_3 = 1;
                    } elseif ($value->delivery_type === 2) {
                        $delivery_type_2++;
                    }
                }
                $imgIsClaim = '';
                if ($data->is_claim === 0) {
                    $imgIsClaim = '/img/claim_0.gif';
                } else {
                    $imgIsClaim = '/img/claim_1.gif';
                }
                $data['img_is_claim'] = $imgIsClaim;
                $delivery_type = $delivery_type_1_3 + $delivery_type_2;
                $data['delivery_type'] = $delivery_type;
                $option = Config::get('common.ship_wish_time');
                $shipWishTime = !empty($data->delivery_time) ? $data->delivery_time : $data->ship_wish_time;
                if (!empty($shipWishTime) && isset($option[$shipWishTime])) {
                    $data->ship_wish_time = $option[$shipWishTime];
                }
                $data->ship_wish_date = !empty($data->delivery_date) ? $data->delivery_date : $data->ship_wish_date;
                if (!empty($data->ship_wish_date)) {
                    $data->ship_wish_date = date('Y/m/d', strtotime($data->ship_wish_date));
                }
            }
            $orderDetail = $this->getInfoOrderDetail($paramKey);
            $model = new DtOrderUpdateLog();
            $dataLog['data'] = $model->getData($paramKey);
            $dataReturn = $this->getRefundList($data['received_order_id']);
            $dataMail = $this->getDataMailByReceive($data->received_order_id);

            $linkOrder = [
                1 => "https://order.rms.rakuten.co.jp/rms/mall/order/rb/vc?__event=BO02_001_013&order_number=%s",
                2 => "https://pro.store.yahoo.co.jp/pro.diy-tool/order/manage/detail/%s",
                3 => "https://sellercentral.amazon.co.jp/hz/orders/details?_encoding=UTF8&orderId=%s",
                4 => "http://42.127.237.184/admin/order/edit.php?order_id=%s",
                5 => "https://www.diy-tool.com/FutureShop2/AcceptDetailHook.htm?acceptno=%s",
                8 => "https://shop.diyfactory.jp/admin/index.php?route=sale/order/info&order_id=%s",
                9 => "https://order-rp.rms.rakuten.co.jp/order-rb/individual-order-detail-sc/init?orderNumber=%s",
            ];

            if (App::environment(['local', 'test'])) {
                $linkOrder[8] = "http://naruto.daitotest.tk/admin/index.php?route=sale/order/info&order_id=%s";
            }
            $linkMall = '';
            foreach ($linkOrder as $key => $val) {
                if ($key === $data['mall_id']) {
                    $receivedOrderId = $data['received_order_id'];
                    if ($data['mall_id'] === 5) {
                        $receivedOrderId = str_replace("diy-", "", $receivedOrderId);
                    }
                    if ($data['mall_id'] === 8) {
                        $receivedOrderId = str_replace("DIY-", "", $receivedOrderId);
                    }
                    $linkMall = sprintf($val, $receivedOrderId);
                }
            }

            foreach (MstTantou::get(['tantou_code', 'tantou_last_name']) as $item) {
                $dataTantou[] = ['key' => $item['tantou_code'], 'value' => $item['tantou_last_name']];
            }

            $data['delay_priod_limit'] = date('Y-m-d', strtotime($data['delay_priod'] . '+1days'));

            $linkReceipt = action('ReceiptPDFController@receipt', ['mailSerial' => $data['mail_seri']]);
            $data['backlist_rank_string'] = '';
            for ($i=1; $i <= $data['backlist_rank']; $i++) {
                $data['backlist_rank_string'] .= "<span class='fa fa-star fa-4 " . (($data['backlist_rank'] >= $i && !empty($data['backlist_rank'])) ? 'checked-start' : '') . "'></span>";
            }

            $modelBC = new MstBacklistComment();
            $bankListOpt = $modelBC->getDataSelectBox();
            $dataClaim = MstClaim::select(['claim_id as key', 'claim_content as value'])->get();
            return response()->json([
                'data'         => $data,
                'order_detail' => $orderDetail,
                'memo'         => $dataLog,
                'return'       => $dataReturn,
                'optionCancelReason' => $optionCancelReason,
                'dataClaim'    => array_merge([['key' => '', 'value' => '------']], $dataClaim->toArray()),
                'mail'         => $dataMail,
                'error'        => $this->error,
                'params'       => $request->all(),
                'link_mall'    => $linkMall,
                'linkReceipt'  => $linkReceipt,
                'tantou'       => ($dataTantou??[]),
                'bankListOpt'  => array_merge([['key' => '', 'value' => '------']], $bankListOpt->toArray()),
            ]);
        }

        $queryStr     = $_SERVER['QUERY_STRING'];
        $params       = $this->parseUrl($queryStr);
        $indexAction  = action('OrderManagementController@index');
        $detailAction = action('OrderManagementController@detail');
        $indexQuery   = $this->mergeUrl($params, ['receive_id_key']);
        $indexUrl     = $indexAction . (!empty($indexQuery)?("?" . $indexQuery):"");

        $arraySearch = array(
            'payment_method'    => isset($params['payment_method'])?$params['payment_method']:null,
            'product_code'      => isset($params['product_code'])?$params['product_code']:null,
            'per_page'          => isset($params['per_page'])?$params['per_page']:null,
            'order_status'      => isset($params['order_status'])?$params['order_status']:null,
            'full_name'         => isset($params['full_name'])?$params['full_name']:null,
            'receive_id'        => isset($params['receive_id'])?$params['receive_id']:null,
            'name_jp'           => isset($params['name_jp'])?$params['name_jp']:null,
            'order_date_from'   => isset($params['order_date_from'])?$params['order_date_from']:null,
            'order_date_to'     => isset($params['order_date_to'])?$params['order_date_to']:null,
            'is_mall_cancel'    => isset($params['is_mall_cancel'])?$params['is_mall_cancel']:null,
            'is_mall_update'    => isset($params['is_mall_update'])?$params['is_mall_update']:null,
            'order_sub_status'  => isset($params['order_sub_status'])?$params['order_sub_status']:null,
            'received_order_id' => isset($params['received_order_id'])?$params['received_order_id']:null,
            'is_delay'          => isset($params['is_delay'])?$params['is_delay']:null,
            'delay_priod_from'  => isset($params['delay_priod_from'])?$params['delay_priod_from']:null,
            'delay_priod_to'    => isset($params['delay_priod_to'])?$params['delay_priod_to']:null,
            'another_order_status' => isset($params['another_order_status'])?$params['another_order_status']:null,
            'address'              => isset($params['address'])?$params['address']:null,
            'request_price_from'   => $request->input('request_price_from', null),
            'request_price_to'     => $request->input('request_price_to', null),
        );
        $arrSort = array(
            'full_name'         => isset($params['sort_full_name'])?$params['sort_full_name']:null,
            'name_jp'           => isset($params['sort_name_jp'])?$params['sort_name_jp']:null,
            'order_date'        => isset($params['sort_order_date'])?$params['sort_order_date']:null,
            'order_status'      => isset($params['sort_order_status'])?$params['sort_order_status']:null,
            'payment_method'    => isset($params['sort_payment_method'])?$params['sort_payment_method']:null,
            'receive_id'        => isset($params['sort_receive_id'])?$params['sort_receive_id']:null,
            'received_order_id' => isset($params['sort_received_order_id'])?$params['sort_received_order_id']:null,
            'request_price'     => isset($params['sort_request_price'])?$params['sort_request_price']:null,
        );

        $receiveIdKey = isset($params['receive_id_key'])?(int)$params['receive_id_key']:null;
        $modelOrder   = new MstOrder();

        if (count(array_filter($arrSort)) === 0) {
            $posResult = $modelOrder->getPositionRowNotCodition($receiveIdKey, $arraySearch, $arrSort);
        } else {
            $posResult = $modelOrder->getPositionRow($receiveIdKey, $arraySearch, $arrSort);
        }

        $arrayOrderCountAll = $arraySearch;
        unset($arrayOrderCountAll['received_order_id']);
        unset($arrayOrderCountAll['per_page']);
        unset($arrayOrderCountAll['page']);
        if (count(array_filter($arrayOrderCountAll)) === 0) {
            $countOrder = $modelOrder->countItemsByPositionAll();
        } else {
            $countOrder = $modelOrder->countItemsByPosition($arraySearch, $arrSort);
        }
        if (empty($posResult)) {
            return redirect()->to($indexUrl);
        }
        $rawPos   = (int)$posResult->position;

        $perPage  = empty($params['per_page'])?20:(int)$params['per_page'];
        $page     = ceil($rawPos/$perPage);
        if ($page > 1) {
            $params['page'] = $page;
        } else {
            unset($params['page']);
        }

        $indexQuery = $this->mergeUrl($params, ['receive_id_key']);
        $indexUrl   = $indexAction . (!empty($indexQuery)?("?" . $indexQuery):"");

        $curPos   = $rawPos - 1;
        $prevPos  = $curPos - 1;
        $result   = $modelOrder->getItemByPosition($prevPos, $arraySearch, $arrSort);
        $nextUrl = null;
        $prevUrl = null;
        $nextId  = null;
        $prevId  = null;
        foreach ($result as $key => $val) {
            if ($val->receive_id === $receiveIdKey) {
                if (isset($result[$key - 1])) {
                    $prevId = $result[$key - 1]->receive_id;
                }
                if (isset($result[$key + 1])) {
                    $nextId = $result[$key + 1]->receive_id;
                }
                break;
            }
        }
        if ($nextId !== null) {
            $nextQuery = $this->mergeUrl($params, ['page'], ['receive_id_key' => $nextId]);
            $nextUrl   = $detailAction . (!empty($nextQuery)?("?" . $nextQuery):"");
        }
        if ($prevId !== null) {
            $prevQuery = $this->mergeUrl($params, ['page'], ['receive_id_key' => $prevId]);
            $prevUrl   = $detailAction . (!empty($prevQuery)?("?" . $prevQuery):"");
        }
        return view('order-management.detail', [
            'pageOnPages' => trans('messages.pos_per_page', ['pos' => $rawPos, 'total' => $countOrder]),
            'nextUrl'  => $nextUrl,
            'prevUrl'  => $prevUrl,
            'indexUrl' => $indexUrl
        ]);
    }

    /**
     * Update memo up ope
     *
     * @param  Request $request
     * @return json
     */
    public function updateMemoUpOPE(Request $request)
    {
        if ($request->ajax() === true) {
            $rules     = [
                'log_id'    => 'exists:horunba.dt_order_update_log,log_id',
                'up_ope_cd' => 'exists:horunba.mst_tantou,tantou_code'
            ];
            $input     = $request->all();
            $validator = Validator::make($input, $rules);
            if ($validator->fails()) {
                return [
                    'flg' => 0,
                    'msg' => implode("\n", $validator->messages()->all())
                ];
            } else {
                try {
                    $modelOUL = new DtOrderUpdateLog();
                    $modelOUL->updateData(
                        ['log_id' => $input['log_id']],
                        [
                            'up_ope_cd' => $input['up_ope_cd'],
                            'up_date'   => now()
                        ]
                    );
                    return [
                        'flg' => 1,
                        'msg' => __('ajax_update_success')
                    ];
                } catch (\Exception $ex) {
                    return [
                        'flg' => 0,
                        'msg' => $ex->getMessage()
                    ];
                }
            }
        }
    }

    /**
     * Parse url to array
     *
     * @param  string $queryStr
     * @return array
     */
    private function parseUrl($queryStr)
    {
        $arrQuery = explode("&", $queryStr);
        $arrParam = array();
        foreach ($arrQuery as $item) {
            $tmp = explode("=", $item);
            if (count($tmp) > 1) {
                $index = urldecode($tmp[0]);
                $value = urldecode($tmp[1]);
                $arrParam[$index][] = $value;
            }
        }
        $params = array();
        foreach ($arrParam as $key => $val) {
            $count = count($val);
            if ($count === 1) {
                $params[$key] = $val[0];
            } elseif ($count > 1) {
                $params[$key] = $val;
            }
        }
        return $params;
    }

    /**
     * Merge array to url
     *
     * @param  array  $params
     * @param  array  $arrIgnore
     * @param  array  $arrChange
     * @return string
     */
    private function mergeUrl($params, $arrIgnore = [], $arrChange = null)
    {
        $arrUrl = array();
        foreach ($params as $key => $val) {
            if (!in_array($key, $arrIgnore)) {
                if (isset($arrChange[$key])) {
                    $arrUrl[] = "$key=$arrChange[$key]";
                } elseif (is_array($val)) {
                    foreach ($val as $item) {
                        $arrUrl[] = "$key=$item";
                    }
                } else {
                    $arrUrl[] = "$key=$val";
                }
            }
        }
        return implode("&", $arrUrl);
    }

    /**
     * Get data by receive_id , for show detail order tab
     *
     * @param  array $param
     * @return object
     */
    public function getInfoOrderDetail($param)
    {
        $mstShippingCompany = new MstShippingCompany();
        $productStatus = Config::get('common.product_status');
        $model         = new MstOrder();
        $data          = $model->getOrderDetail($param);
        $newData       = [];
        $supplier      = $mstShippingCompany->getItemByDeliveryType(1);
        $dataDetail    = array();
        $arrDLN        = array();
        $isReturn      = false;
        $receiverId    = [];
        $receiverErr   = [];
        $deliCount     = [];
        if (!empty($data)) {
            foreach ($data as $key => $value) {
                if (!isset($deliCount[$value->receiver_id])) {
                    $deliCount[$value->receiver_id]['deli2']  = 0;
                    $deliCount[$value->receiver_id]['deli13'] = 0;
                }
                if ($value->delivery_type  === 2) {
                    $newData[$value->receiver_id]['supplier'] = $supplier->company_name;
                    $deliCount[$value->receiver_id]['deli2']++;
                } elseif ($value->delivery_type === 1 ||
                    $value->delivery_type === 3) {
                    $deliCount[$value->receiver_id]['deli13']++;
                    if (!empty($value->company_name)) {
                        $newData[$value->receiver_id]['nanko'] = $value->company_name;
                    }
                }
                if ($value->product_status === PRODUCT_STATUS['DIRECTLY_SHIPPING']
                    || $value->product_status === PRODUCT_STATUS['SHIPPED']) {
                    $isReturn = true;
                }
                if (!in_array($value->detail_line_num, $arrDLN)) {
                    $arrChild = array();
                    $arrDLN[] = $value->detail_line_num;
                }
                $productStatusName = '';
                if (isset($productStatus[$value->product_status])) {
                    $productStatusName = $productStatus[$value->product_status];
                }
                if (!empty($value->child_product_code)) {
                    $arrChild[] = [
                        'child_product_code'   => $value->child_product_code,
                        'child_product_name'   => $value->child_product_name,
                        'child_supplier_id'    => $value->child_supplier_id,
                        'received_order_num'   => $value->received_order_num,
                        'child_edi_order_code' => $value->edi_order_code,
                        'child_order_code'     => $value->order_code,
                        'order_type'           => $value->order_type,
                        'product_status'       => $value->product_status,
                        'product_status_name'  => $productStatusName,
                    ];
                }
                $dataDetail[$value->receiver_id][$value->detail_line_num] = [
                    'delivery_type'       => $value->delivery_type,
                    'receive_id'          => $value->receive_id,
                    'product_code'        => $value->product_code,
                    'product_name'        => $value->product_name,
                    'parrent_supplier_id' => $value->parrent_supplier_id,
                    'price'               => $value->price,
                    'quantity'            => $value->quantity,
                    'sub_total'           => $value->sub_total,
                    'product_status'      => $value->product_status,
                    'product_status_name' => $productStatusName,
                    'return_no'           => $value->return_no,
                    'child_data'          => $arrChild,
                    'edi_order_code'      => $value->edi_order_code,
                    'order_code'          => $value->order_code,
                    'order_type'          => $value->order_type,
                    'detail_line_num'     => $value->detail_line_num,
                    'sub_line_num'        => $value->sub_line_num,
                    'received_order_num'  => $value->received_order_num,
                    'received_order_num'  => $value->received_order_num,
                    'specify_deli_type'   => $value->specify_deli_type,
                    'receiver_full_name'   => $value->receiver_full_name,
                    'receiver_prefecture'  => $value->receiver_prefecture,
                    'receiver_city'        => $value->receiver_city,
                    'receiver_sub_address' => $value->receiver_sub_address,
                    'receiver_zip_code'    => $value->receiver_zip_code,
                    'receiver_tel_num'     => $value->receiver_tel_num,
                    'claim_id'             => $value->claim_id
                ];
                if (!isset($receiverId[$value->receiver_id])) {
                    $ruleCheck = [
                        'checkPostalCodeAndAddressReceiver:' . $value->receiver_id,
                        //'checkPostalCodeAndAddressReceiverLength:' . $value->receiver_id,
                        'checkTelDigits:' . $value->receiver_id,
                        'checkTelDigits1:' . $value->receiver_id,
                        'checkChargeFeeTransport:' . $value->receiver_id,
                        'checkChargeFeeDelivery:' . $value->receiver_id,
                        'checkChargeFeeCulDelivery:' . $value->receiver_id,
                    ];
                    $strRuleCheck = implode("|", $ruleCheck);
                    $validator = Validator::make(
                        ['receive_id' => $value->receive_id],
                        ['receive_id' => $strRuleCheck]
                    );
                    if ($validator->fails()) {
                        $receiverErr[] = $value->receiver_id;
                    }
                }
                $receiverId[$value->receiver_id] = $value->receiver_id;
            }
        }
        $dataDetailNew = [];
        foreach ($dataDetail as $key => $dat) {
            $dataDetailNew[$key] = array_values($dat);
        }
        $receiverId = array_values($receiverId);
        if (App::environment(['local'])) {
            $url = '//edi.local';
        } elseif (App::environment(['test'])) {
            $url = 'https://edi-test.diyfactory.jp';
        } else {
            $url = 'https://edi.diyfactory.jp';
        }
        return [
            'data'      => $dataDetailNew,
            'company'   => $newData,
            'deliCount' => $deliCount,
            'is_return' => $isReturn,
            'url'       => $url,
            'receiver_id' => $receiverId,
            'receiver_err' => $receiverErr
        ];
        // }
    }

    /**
     * Get return list by receive_id
     *
     * @param  array $param
     * @return json
     */
    public function getRefundList($param)
    {
        $refStatus = Config::get('common.refund_status');
        $dtReorder = new DtReorder();
        $data = $dtReorder->getDataByReceivedOrderId($param);
        $data = $data->map(function ($item, $key) use ($refStatus) {
            $item->re_order_type = isset($refStatus[$item->re_order_type])
                ?$refStatus[$item->re_order_type]
                :$item->re_order_type;
            return $item;
        });
        return [
            'data'      => $data
        ];
    }

    /**
     * Export issue estimate
     *
     * @param  Request $request
     * @return mixed
     */
    public function exportIssue(Request $request)
    {
        $rules = [
            'receive_id'    => 'required|numeric|exists:horunba.mst_order,receive_id',
            'issue_date'    => 'required|date',
            'customer_name' => 'required|max:100'
        ];

        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            if ($request->ajax() === true) {
                return response()->json([
                    'status'  => 0,
                    'messages' => $validator->errors()
                ]);
            } else {
                foreach ($validator->errors()->all() as $error) {
                    echo $error . '<br/>';
                }
                exit;
            }
        }

        $action = $request->input('action', '');
        $arrView = [
            'estimate',
            'invoice',
            'order-detail'
        ];
        if (!in_array($action, $arrView)) {
            if ($request->ajax() === true) {
                return response()->json([
                    'status'  => 0,
                    'messages' => __('messages.error_action')
                ]);
            } else {
                echo __('messages.error_action');
                exit;
            }
        }

        $mstOrder = new MstOrder();
        $data = $mstOrder->getIssue($request->input('receive_id'));
        foreach ($data as $item) {
            $tmp = $this->mbStrSplit($item->product_name, 25);
            $item->product_name = implode("\n", $tmp);
        }

        $pdf = PDF::loadView(
            'order-management.pdf.' . $action,
            [
                'data'            => $data,
                'issue_date'      => date('Y/m/d', strtotime($request->input('issue_date'))),
                'customer_name'   => $request->input('customer_name'),
                'incharge_person' => Auth::user()->tantou_last_name
            ]
        );
        return $pdf->stream();
    }

    /**
     * Split multiple byte string
     *
     * @param  string $str
     * @param  int    $splitLength
     * @return array
     */
    private function mbStrSplit($str, $splitLength)
    {
        $chars = array();
        $len = mb_strlen($str);
        for ($i = 0; $i < $len; $i += $splitLength) {
            $chars[] = mb_substr($str, $i, $splitLength);
        }
        return $chars;
    }

    /**
     * Change delay , update state 1 -> 0 , 0 -> 1
     *
     * @param  Request $request
     * @return mixed
     */
    public function changeDelay(Request $request)
    {
        if ($request->ajax() === true) {
            $mstOrder = new MstOrder();
            $name = $request->input('name', null);
            if ($name === 'delay_priod') {
                $dataUpdate = [
                    'delay_priod' => $request->input('delay_priod', null),
                    'up_date'     => date('Y-m-d H:i:s'),
                    'up_ope_cd'   => Auth::user()->tantou_code,
                ];
                $flg = $mstOrder->updateData(
                    ['receive_id' => $request->input('receive_id')],
                    $dataUpdate
                );
                $data['receive_id']     = (int) $request->input('receive_id');
                $data['log_title']      = '期間変更';
                $data['log_contents']   = '【期間】 ' . $request->input('date_old') . ' → ' . $request->input('delay_priod');
                $data['log_status']     = 1;
                $data['in_ope_cd']      = Auth::user()->tantou_code;
                $data['in_date']        = date('Y-m-d H:i:s');
                $data['up_ope_cd']      = Auth::user()->tantou_code;
                $data['up_date']        = date('Y-m-d H:i:s');
                $dtOrderUpdateLog       = new DtOrderUpdateLog();
                $dtOrderUpdateLog->insertData($data);
                return response()->json([
                    'status'  => $flg
                ]);
            } else {
                $rules = [
                    'receive_id'    => 'required|numeric|exists:horunba.mst_order,receive_id',
                    'is_delay'      => 'required|numeric',
                ];
                $validator = Validator::make($request->all(), $rules);
                if ($validator->fails()) {
                    return response()->json([
                        'status'  => 0,
                        'messages' => $validator->errors()
                    ]);
                } else {
                    $dataReason = [
                        'is_delay'      => $request->input('is_delay', 0),
                        'receive_id'    => $request->input('receive_id', 0),
                        'reason'        => $request->input('reason', null),
                    ];
                    $flg                    = $mstOrder->changeDelay($dataReason);
                    $data['receive_id']     = (int) $request->input('receive_id');
                    $data['log_title']      =
                        ((int) $request->input('is_delay') === 1 ? '保留はずし' : '保留チェック');
                    $data['log_contents']   = $request->input('reason');
                    $data['log_status']     = 1;
                    $data['in_ope_cd']      = Auth::user()->tantou_code;
                    $data['in_date']        = date('Y-m-d H:i:s');
                    $data['up_ope_cd']      = Auth::user()->tantou_code;
                    $data['up_date']        = date('Y-m-d H:i:s');
                    $dtOrderUpdateLog       = new DtOrderUpdateLog();
                    $dtOrderUpdateLog->insertData($data);
                    return response()->json([
                        'status'  => $flg
                    ]);
                }
            }
        }
    }
    /**
     * This function , let for update is_confirmed
     *
     * @param  Request $request
     * @return mixed
     */
    public function changeIsConfirmed(Request $request)
    {
        if ($request->ajax() === true) {
            $dtOrderUpdateLog = new DtOrderUpdateLog();
            $mstOrder      = new MstOrder();
            $dtOrderDetail = new DtOrderProductDetail();
            $params        = $request->all();
            $dataOrder     = $mstOrder->find($params['receive_id']);
            $dataKey = [
                'receive_id'      => (int) $params['receive_id']
            ];
            $check = false;
            if ($params['type'] === 'confirm') {
                $dataUpdate = [
                    'is_confirmed'      => 2,
                    'order_sub_status'  => ORDER_SUB_STATUS['NEW']
                ];
                $check = true;
            // } elseif ($params['type'] === 'cancel') {
            //     $datas = $dtOrderDetail->getDataUpdateCancelEDI($params['receive_id']);
            //     foreach ($datas as $data) {
            //         if (!empty($data->m_order_type_id_detail)) {
            //             if ($data->m_order_type_id_detail === 1) {
            //                 $dataNotCancel[$data->product_code] = $data;
            //             } elseif ($data->m_order_type_id_detail === 9) {
            //                 $timeCmp = (time() - strtotime($dataO->order_date))/86400;
            //                 if ($timeCmp <= 5) {
            //                     $dataNotCancel[$data->product_code] = $data;
            //                 }
            //             }
            //         } elseif (!empty($data->m_order_type_id_direct)) {
            //             if ($data->m_order_type_id_direct === 1) {
            //                 $dataNotCancel[$data->product_code] = $data;
            //             }
            //         }
            //     }
            //     if (!empty($dataNotCancel)) {
            //         return response()->json([
            //             'status'     => -1,
            //             'not_cancel' => $dataNotCancel,
            //         ]);
            //     }
            //     $dataUpdate = [
            //         'is_confirmed'          => 2,
            //         'order_status'          => ORDER_STATUS['CANCEL'],
            //         'order_sub_status'      => ORDER_SUB_STATUS['DONE'],
            //         'ship_charge'           => 0,
            //         'pay_charge'            => 0,
            //         'pay_after_charge'      => 0,
            //         'total_price'           => 0,
            //         'pay_charge_discount'   => 0,
            //         'discount'              => 0,
            //         'used_point'            => 0,
            //         'used_coupon'           => 0,
            //         'request_price'         => 0,
            //         'goods_tax'             => 0,
            //         'goods_price'           => 0,
            //         'err_text'              => '',
            //     ];

            //     $dtOrderDetail->updateData($dataKey, ['product_status' => PRODUCT_STATUS['CANCEL']]);
            //     $common = new Common();
            //     $common->cancelOrderEdi((int) $params['receive_id']);
            //     $modelOD = new MstOrderDetail();
            //     $modelOD->updateDataByKey(
            //         $dataKey,
            //         [
            //             'price'    => 0,
            //             'quantity' => 0,
            //         ]
            //     );
            } else {
                return response()->json([
                    'status'   => 0
                ]);
            }
            $rules = [
                'receive_id'    => 'required|numeric|exists:horunba.mst_order,receive_id'
            ];
            $validator = Validator::make($request->all(), $rules);
            if ($validator->fails()) {
                return response()->json([
                    'status'  => 0,
                    'messages' => $validator->errors()
                ]);
            } else {
                $flg       = $mstOrder->updateData($dataKey, $dataUpdate);

                if ($check && $dataOrder->is_ignore_error === 0) {
                    $modelOD = new MstOrderDetail();
                    $dataReceivers = $modelOD->getListReceiver($params['receive_id']);
                    foreach ($dataReceivers as $value) {
                        $this->checkInfoAfterSave($this->checkRules, $dataOrder, $value->receiver_id);
                    }
                    // $this->checkInfoAfterSave($this->checkRules, $dataOrder);
                }
                $btnName = __('messages.btn_confirmed');
                $dataLog[] = [
                    'receive_id' => $params['receive_id'],
                    'log_title'  => __('messages.change_order_info'),
                    'log_contents' =>
                        "【Button {$btnName}】 → Clicked",
                    'log_status'   => 1,
                    'in_ope_cd'    => Auth::user()->tantou_code,
                    'up_ope_cd'    => Auth::user()->tantou_code
                ];
                $dtOrderUpdateLog->insert($dataLog);
                return response()->json([
                    'status'  => $flg
                ]);
            }
        }
    }

    /**
     * Get form data for order editing
     *
     * @param  Request $request
     * @return json
     */
    public function getFormOrderData(Request $request)
    {
        if ($request->ajax() === true) {
            $mstOrder            = new MstOrder();
            $mstSettlementManage = new MstSettlementManage();
            $data = $mstOrder->getOrderEditing($request->receive_id);
            if ($data !== null && $data->order_status > ORDER_STATUS['SETTLEMENT']) {
                return response()->json([
                    'data'       => null
                ]);
            }
            $paymentOpt[] = ['key' => '', 'value' => '------'];
            foreach ($mstSettlementManage->get(['payment_code', 'payment_name']) as $item) {
                $paymentOpt[] = ['key' => $item['payment_code'], 'value' => $item['payment_name']];
            }
            $modelBC = new MstBacklistComment();
            $bankListOpt = $modelBC->getDataSelectBox();
            $data->is_black_list_string = '';
            for ($i = 1; $i <= 5; $i++) {
                $data->is_black_list_string .= "<span class='fa fa-star " . (($data->backlist_rank >= $i && !empty($data->backlist_rank)) ? 'checked-start' : '') . "'></span>";
            }
            $dataStar = $modelBC->get(['ID', 'backlist_rank'])->keyBy('ID')->toArray();
            return response()->json([
                'data'        => $data,
                'paymentOpt'  => $paymentOpt,
                'dataStar'    => $dataStar,
                'bankListOpt' => array_merge([['key' => '', 'value' => '------']], $bankListOpt->toArray()),
            ]);
        }
    }

    /**
     * Edit order submit
     *
     * @param  Request $request
     * @return json
     */
    public function orderEditing(Request $request)
    {
        $paramsFilter = $request->input('paramsFilter');
        if ($request->ajax() === true) {
            $mstOrder            = new MstOrder();
            $mstCustomer         = new MstCustomer();
            $dtOrderUpdateLog    = new DtOrderUpdateLog();
            $mstSettlementManage = new MstSettlementManage();
            $dataPayment = $mstSettlementManage->get(['payment_code', 'payment_name'])
                                               ->keyBy('payment_code')
                                               ->toArray();
            $paymentMethod = (int)$request->input('payment_method', 0);
            $isError             = false;
            $arrUpdate           = [];
            $rules = [
                'receive_id'                           => 'exists:mst_order,receive_id',
                'tel_num'                              => 'phone_number',
                'zip_code'                             => 'exists:mst_postal,postal_code',
                'city'                                 => 'required',
                'fax_num'                              => 'fax_number|nullable',
                'total_price'                          => 'numeric',
                'urgent_tel_num'                       => 'phone_number|nullable',
                'email'                                => 'email',
                'payment_method'                       => 'exists:mst_settlement_manage,payment_code',
                'sub_total'                            => 'numeric',
                'pay_charge_discount'                  => 'numeric',
                'ship_charge'                          => 'numeric',
                'discount'                             => 'numeric',
                'pay_after_charge'                     => 'numeric',
                'used_point'                           => 'numeric',
                'pay_charge'                           => 'numeric',
                'used_coupon'                          => 'numeric',
                'request_price'                        => 'numeric'
            ];
            DB::beginTransaction();
            $mstOrder    = $mstOrder->sharedLock()->find($request->receive_id);
            if ($mstOrder->mall_id === 3) {
                unset($rules['tel_num']);
                unset($rules['zip_code']);
                unset($rules['city']);
                unset($rules['fax_num']);
                unset($rules['urgent_tel_num']);
                unset($rules['email']);
            }
            $validator = Validator::make($request->all(), $rules);
            if ($validator->fails()) {
                return response()->json([
                    'status'  => 0,
                    'messages' => $validator->errors()
                ]);
            }
            $mstCustomer = $mstCustomer->find($mstOrder->customer_id);
            $requestPriceOld = $mstOrder->request_price;
            $oldPaymentMethod = (int)$mstOrder->payment_method;
            try {
                $tableUpdate = [
                    'mst_customer' => [
                        'zip_code'       => __('messages.customer_postal'),
                        'prefecture'     => __('messages.customer_prefecture'),
                        'city'           => __('messages.customer_city'),
                        'sub_address'    => __('messages.customer_sub_address'),
                        'tel_num'        => __('messages.tel_num'),
                        'fax_num'        => __('messages.fax_num'),
                        'urgent_tel_num' => __('messages.urgent_tel_num'),
                        'email'          => __('messages.email'),
                    ],
                    'mst_order'    => [
                        'payment_method'        => __('messages.payment_name'),
                        'pay_charge_discount'   => __('messages.pay_charge_discount'),
                        'ship_charge'           => __('messages.ship_charge'),
                        'discount'              => __('messages.discount'),
                        'pay_after_charge'      => __('messages.pay_after_charge'),
                        'used_point'            => __('messages.used_point'),
                        'pay_charge'            => __('messages.pay_charge'),
                        'used_coupon'           => __('messages.used_coupon'),
                        'total_price'           => __('messages.total_price'),
                        'request_price'         => __('messages.request_price'),
                        'shop_answer'           => __('messages.shop_answer'),
                        'delivery_instrustions' => __('messages.delivery_instrustions_col')
                    ]
                ];
                $arrCheckChange = [
                    'payment_method',
                    'pay_charge_discount',
                    'ship_charge',
                    'discount',
                    'pay_after_charge',
                    'used_point',
                    'pay_charge',
                    'used_coupon',
                    'total_price',
                    'request_price',
                ];
                $dataLog = array();
                $input   = $request->all();

                if ($isError) {
                    $tableUpdate['mst_order']['order_sub_status'] = __('messages.order_sub_status_id');
                    $tableUpdate['mst_order']['err_text']         = __('messages.err_text');
                    if (array_key_exists('is_recalculate', $arrUpdate)) {
                        $tableUpdate['mst_order']['is_recalculate'] = __('messages.is_recalculate');
                    }
                    $input = array_merge($input, $arrUpdate);
                }
                $isCustomer = false;
                $isOrder    = false;
                $isChange   = false;
                foreach ($tableUpdate as $tableKey => $tableVal) {
                    foreach ($tableVal as $columnKey => $columnName) {
                        if ($tableKey === 'mst_customer') {
                            if (($mstCustomer[$columnKey] != $input[$columnKey] ||
                                ($mstCustomer[$columnKey] !== $input[$columnKey] &&
                                ($columnKey === 'tel_num' ||
                                    $columnKey === 'fax_num' ||
                                    $columnKey === 'urgent_tel_num')))
                                && (!empty($mstCustomer[$columnKey]) || !empty($input[$columnKey]))) {
                                $dataLog[] = [
                                    'receive_id' => $mstOrder->receive_id,
                                    'log_title'  => __('messages.change_order_info'),
                                    'log_contents' =>
                                            "【{$columnName}】{$mstCustomer[$columnKey]} → {$input[$columnKey]}",
                                    'log_status'   => 1,
                                    'in_ope_cd'    => Auth::user()->tantou_code,
                                    'up_ope_cd'    => Auth::user()->tantou_code
                                ];
                                $mstCustomer[$columnKey] = $input[$columnKey];
                                if ($columnKey === 'sub_address') {
                                    if (empty($input['sub_address'])) {
                                        $mstCustomer[$columnKey] = '';
                                    }
                                }
                                if ($columnKey === 'prefecture') {
                                    if (empty($input['prefecture'])) {
                                        $mstCustomer[$columnKey] = '';
                                    }
                                }
                                $isCustomer              = true;
                            }
                        }
                        if ($tableKey === 'mst_order') {
                            if ($mstOrder[$columnKey] != $input[$columnKey]) {
                                $valueTo   = $mstOrder[$columnKey];
                                $valueFrom = $input[$columnKey];
                                if ($columnKey === 'payment_method') {
                                    $valueTo   = isset($dataPayment[$mstOrder[$columnKey]]['payment_name']) ?
                                                    $dataPayment[$mstOrder[$columnKey]]['payment_name'] :
                                                    $mstOrder[$columnKey];
                                    $valueFrom = isset($dataPayment[$input[$columnKey]]['payment_name']) ?
                                                    $dataPayment[$input[$columnKey]]['payment_name'] :
                                                    $input[$columnKey];
                                }
                                $dataLog[] = [
                                    'receive_id' => $mstOrder->receive_id,
                                    'log_title'  => __('messages.change_order_info'),
                                    'log_contents' => "【{$columnName}】{$valueTo} → {$valueFrom}",
                                    'log_status'   => 1,
                                    'in_ope_cd'    => Auth::user()->tantou_code,
                                    'up_ope_cd'    => Auth::user()->tantou_code
                                ];
                                if (in_array($columnKey, $arrCheckChange)) {
                                    $isChange = true;
                                }
                                $mstOrder[$columnKey] = $input[$columnKey];
                                $isOrder              = true;
                            }
                        }
                    }
                }

                if ($isCustomer) {
                    $mstCustomer->up_ope_cd = Auth::user()->tantou_code;
                    $mstCustomer->save();
                }
                $requestPrice = $mstOrder->request_price;
                if ($isOrder) {
                    if ($isChange) {
                        //Process re-calculate
                        $totalPrice   = $mstOrder->goods_price +
                                        $mstOrder->ship_charge +
                                        $mstOrder->pay_after_charge +
                                        $mstOrder->pay_charge;
                        $requestPrice = $totalPrice -
                                        $mstOrder->pay_charge_discount -
                                        $mstOrder->discount -
                                        $mstOrder->used_point -
                                        $mstOrder->used_coupon;
                        $returnCP = [
                            'return_point'  => 0,
                            'return_coupon' => 0
                        ];
                        if ($requestPrice < 0) {
                            if (($mstOrder->used_point + $requestPrice) > 0) {
                                $mstOrder->used_point    += $requestPrice;
                                $returnCP['return_point'] = -$requestPrice;
                                $mstOrder->request_price = 0;
                            } elseif (($mstOrder->used_point + $mstOrder->used_coupon + $requestPrice) > 0) {
                                $returnCP['return_point']  = $mstOrder->used_point;
                                $returnCP['return_coupon'] = - ($mstOrder->used_point + $requestPrice);
                                $mstOrder->used_coupon = $mstOrder->used_point + $mstOrder->used_coupon + $requestPrice;
                                $mstOrder->used_point  = 0;
                                $mstOrder->request_price = 0;
                            }
                        } else {
                            $mstOrder->request_price = $requestPrice;
                        }
                        $mstOrder->total_price    = $totalPrice;
                        $mstOrder->is_mall_update = 1;
                    }
                    $mstOrder->up_ope_cd = Auth::user()->tantou_code;
                    $mstOrder->save();
                }

                if (count($dataLog) > 0) {
                    $dtOrderUpdateLog->insert($dataLog);
                }
                if ($isChange) {
                    if (in_array((int)$mstOrder->payment_method, [2, 6, 7])) {
                        if ($mstOrder->payment_status === 1) {
                            if ($requestPrice < $requestPriceOld) {
                                if ((int)$request->input('check_show_repay_popup', 0) === 1) {
                                    DB::rollback();
                                    return response()->json([
                                        'status' => 3
                                    ]);
                                }
                                $repayType = (int)$request->input('repay_type', 1);
                                $this->processRePayment(
                                    $mstOrder,
                                    $mstOrder->request_price,
                                    $requestPriceOld,
                                    $returnCP,
                                    $repayType
                                );
                            }
                        }
                    }
                }
                if ((int)$request->input('check_show_repay_popup', 0) === 1) {
                    DB::rollback();
                    return response()->json([
                        'status' => 4
                    ]);
                }
                if (($oldPaymentMethod !== $paymentMethod) &&
                        in_array($paymentMethod, [2, 6, 7]) &&
                        (int)$mstOrder->order_status === ORDER_STATUS['PAYMENT_CONFIRM']) {
                    $mstOrder->order_status     = ORDER_STATUS['ORDER'];
                    $mstOrder->order_sub_status = ORDER_SUB_STATUS['NEW'];
                    $mstOrder->save();
                }
                if ($mstOrder->is_ignore_error === 0) {
                    // $resCheck = $this->checkInfoAfterSave($this->checkRules, $mstOrder);
                    $modelOD = new MstOrderDetail();
                    $dataReceivers = $modelOD->getListReceiver($request->receive_id);
                    foreach ($dataReceivers as $value) {
                        $resCheck = $this->checkInfoAfterSave($this->checkRules, $mstOrder, $value->receiver_id);
                    }
                    if (!is_bool($resCheck)) {
                        return response()->json([
                            'status' => -1,
                            'messages' => $resCheck
                        ]);
                    } elseif (!$resCheck) {
                        $mstOrder->is_mail_sent = 0;
                        $mstOrder->save();
                    }
                }
                DB::commit();
                return response()->json([
                    'status' => 1,
                ]);
            } catch (\Exception $ex) {
                DB::rollBack();
                return response()->json([
                    'status' => -1,
                    'messages' => $ex
                ]);
            }
        }
        return view('order-management.order_editing', ['paramsFilter' => $paramsFilter]);
    }

    /**
     * Get data order detail editing
     *
     * @param  Request $request
     * @return json
     */
    public function getFormOrderDetailData(Request $request)
    {
        $mstOrder           = new MstOrder();
        $mstShippingCompany = new MstShippingCompany();
        $mstCancelReason    = new MstCancelReason();

        $result = $mstOrder->getOrderDetailEditing($request->receive_id, $request->receiver_id);
        if ($result->count() === 0 || $result[0]->order_status > ORDER_STATUS['SETTLEMENT']) {
            return json_encode([
                'data' => null
            ]);
        }

        $companyOpt = $mstShippingCompany->get(['company_id', 'company_name'])->map(function ($item, $key) {
                return ['key' => $item['company_id'], 'value' => $item['company_name']];
        });
        $cancelReasonOpt = $mstCancelReason->get(['reason_id', 'reason_content'])->map(function ($item, $key) {
                return ['key' => $item['reason_id'], 'value' => $item['reason_content']];
        });
        $cancelReasonOpt->prepend(['key' => '', 'value' => '------']);

        $shipWishDateOpt[] = ['key' => '', 'value' => '------'];
        foreach (Config::get('common.ship_wish_time') as $key => $val) {
            $shipWishDateOpt[] = ['key' => $key, 'value' => $val];
        }
        $deliveryCode = $result[0]->delivery_code;
        $deliveryType = $result[0]->delivery_type;
        if ($deliveryType === 2) {
            $deliveryCode = $mstShippingCompany->getItemByDeliveryType(1)->company_id;
        }

        $dataDetail    = array();
        $arrDLN        = array();
        $isError       = false;

        $mallId = $result[0]->mall_id;
        $flg13  = 0;
        $flg2   = 0;
        foreach ($result as $item) {
            if (!in_array($item->detail_line_num, $arrDLN)) {
                $arrChild = array();
                $arrDLN[] = $item->detail_line_num;
            }
            if ($item->delivery_type === 2) {
                $prefix = 'c';
                $flg2   = 1;
            } else {
                $flg13  = 1;
                $prefix = $item->delivery_type === 3 ? 'b' : 'a';
            }
            if (!empty($item->child_product_code)) {
                $arrChild[] = [
                    'sub_line_num'       => $item->sub_line_num,
                    'child_product_code' => $item->child_product_code,
                    'child_product_name' => $item->child_product_name,
                    'received_order_num' => $item->received_order_num,
                    'delivery_type'      => $item->delivery_type,
                    'product_status'     => $item->product_status,
                    'component_num'      => $item->component_num
                ];
            }
            $price  = $item->price;
            $dataDetail[$prefix . $item->detail_line_num] = [
                'detail_line_num'     => $item->detail_line_num,
                'sub_line_num'        => $item->sub_line_num,
                'product_code'        => $item->product_code,
                'product_name'        => $item->product_name,
                'price'               => $price,
                'quantity'            => $item->quantity,
                'product_status'      => $item->product_status,
                'product_status_root' => $item->product_status,
                'delivery_type'       => $item->delivery_type,
                'order_code'          => $item->order_code,
                'new_product'         => 0,
                'child_data'          => $arrChild
            ];
            if (empty($item->received_order_num) && empty($item->sub_line_num) && !$isError) {
                $isError = true;
            }
        }
        if ($isError) {
            $productStatusOpt[] = ['key' => '', 'value' => '------'];
        } else {
            ksort($dataDetail);
        }
        foreach (Config::get('common.product_status') as $key => $val) {
            $productStatusOpt[] = ['key' => $key, 'value' => $val];
        }
        $dataDetail = array_values($dataDetail);
        $total  = $mstOrder->getTotalPriceOrder($request->receive_id);
        if ($total === false) {
            $total  = $result[0]->goods_price;
        }
        $dataDelivery = $mstOrder->getCheckDeliveryExists($request->receive_id, $request->receiver_id);
        $hasDelivery  = false;
        if (!empty($dataDelivery)) {
            $hasDelivery  = true;
            $deliveryCode = $dataDelivery->delivery_code;
        }
        $companyOpt->prepend(['key' => 0, 'value' => '------']);

        $data = [
            'receive_id'              => $result[0]->receive_id,
            'mall_id'                 => $result[0]->mall_id,
            'order_status'            => $result[0]->order_status,
            'order_sub_status'        => $result[0]->order_sub_status,
            'last_name'               => $result[0]->last_name,
            'first_name'              => $result[0]->first_name,
            'tel_num'                 => $result[0]->tel_num,
            'zip_code'                => $result[0]->zip_code,
            'prefecture'              => $result[0]->prefecture,
            'city'                    => $result[0]->city,
            'sub_address'             => $result[0]->sub_address,
            'delivery_plan_date'      => $result[0]->delivery_plan_date,
            'ship_wish_date'          => $result[0]->ship_wish_date,
            'ship_wish_time'          => $result[0]->ship_wish_time,
            'ship_charge'             => $result[0]->ship_charge,
            'pay_after_charge'        => $result[0]->pay_after_charge,
            'pay_charge'              => $result[0]->pay_charge,
            'total_price'             => $result[0]->total_price,
            'pay_charge_discount'     => $result[0]->pay_charge_discount,
            'is_ship_charge_discount' => $result[0]->pay_charge_discount > 0 ? 1 : 0,
            'discount'                => $result[0]->discount,
            'used_point'              => $result[0]->used_point,
            'used_coupon'             => $result[0]->used_coupon,
            'request_price'           => $result[0]->request_price,
            'delivery_code'           => $deliveryCode,
            'product_detail'          => $dataDetail,
            'isError'                 => $isError,
            'total'                   => $total,
            'data_delivery'           => $dataDelivery,
            'has_delivery'            => $hasDelivery,
            'flg13'                   => $flg13,
            'flg2'                    => $flg2,
        ];
        return json_encode([
            'data'             => $data,
            'cancelReasonOpt'  => $cancelReasonOpt,
            'companyOpt'       => $companyOpt,
            'productStatusOpt' => $productStatusOpt,
            'shipWishDateOpt'  => $shipWishDateOpt,
        ]);
    }

    /**
     * Get info product
     *
     * @param  Request $request
     * @return json
     */
    public function getDataProduct(Request $request)
    {
        $mstProduct = new MstProduct();
        $data       = $mstProduct->getItemDetailByProductCode($request->product_code);
        $mallId     = (int) $request->input('mall_id', 0);
        if ($data->count() === 0) {
            return json_encode([
                'data' => null
            ]);
        }
        $childData  = array();
        foreach ($data as $item) {
            if (!empty($item->child_product_code)) {
                $childData[] = [
                    'child_product_code' => $item->child_product_code,
                    'child_product_name' => $item->child_product_name,
                    'component_num'      => $item->component_num
                ];
            }
        }
        $price = $data[0]->price;
        if ($mallId === 1) {
            $price = $data[0]->price_calc_rak_komi;
        } elseif ($mallId === 2) {
            $price = $data[0]->price_calc_yah_komi;
        } elseif ($mallId === 3) {
            $price = $data[0]->price_calc_ama_komi;
        } elseif ($mallId === 4) {
            $price = ceil($data[0]->price_calc_biz_nuki*1.08);
        } elseif ($mallId === 5) {
            $price = ceil($data[0]->price_calc_diy_nuki*1.08);
        } else {
            $price = ceil($item->price_calc_diy_nuki*1.08);
        }
        $newData = [
            'product_code' => $data[0]->product_code,
            'product_name' => $data[0]->product_name,
            'price'        => $price,
            'child_data'   => $childData
        ];

        return json_encode([
            'data' => $newData
        ]);
    }
    /**
     * Get info product
     *
     * @param  Request $request
     * @return json
     */
    public function getDataProductName(Request $request)
    {
        $mstProduct = new MstProduct();
        $data       = $mstProduct->getNameByProductCode($request->product_code);
        if ($data->count() === 0) {
            return json_encode([
                'data' => []
            ]);
        }
        $data = $data->toArray();
        return json_encode([
            'data' => $data
        ]);
    }
    /**
     * Get info mst code fee
     *
     * @param  Request $request
     * @return json
     */
    public function getDataCodeFee(Request $request)
    {
        $mstCodeFee = new MstCodeFee();
        $data       = $mstCodeFee->getCodeFeeByIndex((int) $request->payment_method);
        if ($data->count() === 0) {
            return json_encode([
                'data' => []
            ]);
        }
        $data = $data->toArray();
        return json_encode([
            'data' => $data
        ]);
    }

    /**
     * Order detail editing
     *
     * @param  Request $request
     * @return json
     */
    public function orderDetailEditing(Request $request)
    {
        $paramsFilter = $request->input('paramsFilter');
        if ($request->ajax() === true) {
            if (isset($request->cancel_all)) {
                return response()->json([
                    'status'  => 1
                ]);
            }
            $commonRules = [
                'receive_id'     => 'exists:mst_order,receive_id',
                'tel_num'        => 'phone_number',
                'zip_code'       => 'exists:mst_postal,postal_code',
                'city'           => 'required',
                'ship_wish_date' => 'string|nullable',
                'cancel_reason'  => 'required_if:show_cancel_reason,1|exists:mst_cancel_reason,reason_id',
            ];
            $chkOrder = MstOrder::find($request->input('receive_id', null));
            // $deliCode = $request->input('delivery_code', 0);
            // if ((!empty($deliCode) && $deliCode !== 0) || ($chkOrder !== null && !empty($chkOrder->delivery_method))) {
            //     $commonRules['delivery_code'] = 'exists:mst_shipping_company,company_id';
            // }
            $message   = array();
            $validator = Validator::make($request->all(), $commonRules);
            if ($validator->fails()) {
                $message = $validator->errors();
            }
            $isError       = $request->input('isError', null);
            $productDetail = $request->input('product_detail', null);
            $receiveId     = $request->input('receive_id', null);
            $receiverId    = $request->input('receiver_id', null);
            $productStatus = Config::get('common.product_status');

            $productMess   = array();
            $newProducts   = array();
            $oldProducts   = array();
            $childProducts = array();
            $total         = 0;
            if (!is_null($productDetail) && !$isError) {
                foreach ($productDetail as $key => $val) {
                    $ruleProduct = [
                        'product_code'   => 'required|exists:mst_product,product_code',
                        'product_name'   => 'required',
                        'price'          => 'required|numeric',
                    ];
                    if ($val['new_product'] === 1) {
                        $ruleProduct['quantity'] = 'required|numeric|min:1';
                        $total += $val['price'] * (int)$val['quantity'];
                        $newProducts[] = $val;
                    } else {
                        $checkQuantity = false;
                        if (array_key_exists('new_quantity', $val)) {
                            $checkQuantity = true;
                            $ruleProduct['new_quantity'] = 'required|numeric|min:1';
                        }
                        if ((int)$val['product_status'] !== 15) {
                            $total += $val['price'] *
                                (!$checkQuantity ? $val['quantity'] : (int)$val['new_quantity']);
                        }
                        $ruleProduct['product_status'] = 'required|in:' . implode(",", array_keys($productStatus));
                        $oldProducts[] = [
                            'receive_id'      => $receiveId,
                            'detail_line_num' => $val['detail_line_num'],
                            'product_status'  => $val['product_status'],
                            'product_code'    => $val['product_code'],
                            'quantity'        => $val['quantity'],
                            'price'           => $val['price'],
                            'new_quantity'    => !$checkQuantity ? false : (int)$val['new_quantity'],
                        ];
                        if (count($val['child_data']) > 0) {
                            foreach ($val['child_data'] as $childData) {
                                $temp = [
                                    'receive_id'          => $receiveId,
                                    'detail_line_num'     => $val['detail_line_num'],
                                    'sub_line_num'        => $childData['sub_line_num'],
                                    'product_status'      => $val['product_status'],
                                    'delivery_type'       => $childData['delivery_type'],
                                    'product_code'        => $childData['child_product_code'],
                                    'quantity'            => $val['quantity'],
                                    'parent_product_code' => $val['product_code'],
                                    'order_code'          => $val['order_code'],
                                    'is_child'            => 1,
                                ];
                                if (isset($val['new_quantity'])) {
                                    $temp['new_quantity'] = (int)$val['new_quantity'];
                                }
                                $childProducts[] = $temp;
                            }
                        } else {
                            $temp = [
                                'receive_id'      => $receiveId,
                                'detail_line_num' => $val['detail_line_num'],
                                'sub_line_num'    => $val['sub_line_num'],
                                'product_status'  => $val['product_status'],
                                'delivery_type'   => $val['delivery_type'],
                                'product_code'    => $val['product_code'],
                                'quantity'        => $val['quantity'],
                                'is_child'        => 0,
                                'order_code'      => $val['order_code'],
                            ];
                            if (isset($val['new_quantity'])) {
                                $temp['new_quantity'] = (int)$val['new_quantity'];
                            }
                            $childProducts[] = $temp;
                        }
                    }
                    $validator = Validator::make($val, $ruleProduct);
                    if ($validator->fails()) {
                        $productMess[$key] = $validator->errors();
                    }
                }
            }


            if (count($message) > 0 || count($productMess) > 0) {
                return response()->json([
                    'status'       => 0,
                    'messages'     => $message,
                    'product_mess' => $productMess
                ]);
            }
            $checkCancel = $request->input('check_cancel', false);
            $resCheckEdi = $this->checkOrderEdi($receiveId, $receiverId, $childProducts, $checkCancel);
            if ($resCheckEdi === false) {
                return response()->json([
                    'status' => -1,
                    'messages' => __('messages.message_cannot_change_quantity'),
                ]);
            } elseif (isset($resCheckEdi['not_cancel'])) {
                return response()->json([
                    'status'         => -1,
                    'flg_not_cancel' => 1,
                    'not_cancel'     => $resCheckEdi['not_cancel']
                ]);
            }
            $isChange = $resCheckEdi['is_change'];
            $isCancel = $resCheckEdi['is_cancel'];
            $mstOrder             = new MstOrder();
            $mstCustomer          = new MstCustomer();
            $dtDelivery           = new DtDelivery();
            $dtOrderUpdateLog     = new DtOrderUpdateLog();
            $dtOrderProductDetail = new DtOrderProductDetail();
            $tOrderDetail         = new TOrderDetail();
            $tOrder               = new TOrder();
            $tOrderDirect         = new TOrderDirect();
            $modelStock           = new MstStockStatus();
            $mstOrderDetail       = new MstOrderDetail();
            $mstShippingCompany   = new MstShippingCompany();
            $dtOrderToSupplier    = new DtOrderToSupplier();
            $dataShipCompany      = $mstShippingCompany->get(['company_id', 'company_name'])
                                                       ->keyBy('company_id')
                                                       ->toArray();
            $dataShipWishTime     = Config::get('common.ship_wish_time');
            $oldRequestPrice      = 0;
            $flgChangeOrder       = false;
            try {
                DB::beginTransaction();
                DB::connection('edi')->beginTransaction();
                $mstOrder        = $mstOrder->sharedLock()->find($request->receive_id);
                $mstCustomer     = $mstCustomer->find($receiverId);
                // $resDelivery     = $mstOrder->getDeliveryCode($request->receive_id, $receiverId);
                $oldRequestPrice = $mstOrder->request_price;
                if (empty($oldProducts)) {
                    $resOPD = [];
                } else {
                    $resOPD = [];
                    foreach ($dtOrderProductDetail->getProductStatusByKey($oldProducts) as $item) {
                        $resOPD[$item->receive_id.$item->detail_line_num] = [
                            'product_status' => $item->product_status
                        ];
                    }
                }

                $dataDelivery = array();
                $isCustomer = false;
                $isOrder    = false;
                $isDelivery = false;

                $tableUpdate = [
                    'mst_customer' => [
                        'tel_num'        => __('messages.tel_num'),
                        'zip_code'       => __('messages.customer_postal'),
                        'prefecture'     => __('messages.customer_prefecture'),
                        'city'           => __('messages.customer_city'),
                        'sub_address'    => __('messages.customer_sub_address'),
                        'first_name'     => __('messages.first_name'),
                        'last_name'      => __('messages.last_name'),
                    ],
                    'mst_order' => [
                        'ship_wish_date'    => __('messages.ship_wish_date'),
                        'ship_wish_time'    => __('messages.ship_wish_time'),
                    ],
                    'mst_order_detail' => [
                        'quantity' => __('messages.quantity'),
                    ],
                ];
                if (!$request->has_delivery) {
                    $tableUpdate['mst_order']['delivery_method'] = __('messages.delivery_method');
                } else {
                    $tableUpdate['dt_delivery'] = [
                        'delivery_code' => __('messages.delivery_method'),
                    ];
                }
                if (!$isError) {
                    $tableUpdate['dt_order_product_detail'] = [
                        'product_status' => __('messages.product_status'),
                    ];
                }

                $dataLog = array();
                $input   = $request->all();
                $input['delivery_method'] = $input['delivery_code'];
                $upPS    = array();
                $upQuan  = array();
                $upOD    = array();
                $delKey  = array();
                foreach ($tableUpdate as $tableKey => $tableVal) {
                    foreach ($tableVal as $columnKey => $columnName) {
                        if ($tableKey === 'mst_customer') {
                            if ($mstCustomer[$columnKey] != $input[$columnKey]
                                    || $mstCustomer['tel_num'] !== $input['tel_num']) {
                                $dataLog[] = [
                                    'receive_id' => $mstOrder->receive_id,
                                    'log_title'  => __('messages.change_order_info'),
                                    'log_contents' =>
                                        "【{$columnName}】{$mstCustomer[$columnKey]} → {$input[$columnKey]}",
                                    'log_status'   => 1,
                                    'in_ope_cd'    => Auth::user()->tantou_code,
                                    'up_ope_cd'    => Auth::user()->tantou_code
                                ];
                                $mstCustomer[$columnKey] =  $input[$columnKey];
                                $isCustomer              = true;
                                $flgChangeOrder          = true;
                            }
                        }
                        if ($tableKey === 'mst_order') {
                            if (!isset($input[$columnKey])) {
                                $input[$columnKey] = null;
                            }
                            if ($mstOrder[$columnKey] != $input[$columnKey]) {
                                $valueTo   = $mstOrder[$columnKey];
                                $valueFrom = $mstOrder[$columnKey];
                                if ($columnKey === 'delivery_method') {
                                    $valueTo   = isset($dataShipCompany[$mstOrder[$columnKey]]['company_name']) ?
                                                        $dataShipCompany[$mstOrder[$columnKey]]['company_name'] :
                                                        $mstOrder[$columnKey];
                                    $valueFrom = isset($dataShipCompany[$input[$columnKey]]['company_name']) ?
                                                        $dataShipCompany[$input[$columnKey]]['company_name'] :
                                                        $input[$columnKey];
                                } elseif ($columnKey === 'ship_wish_time') {
                                    $valueTo   = isset($dataShipWishTime[$mstOrder[$columnKey]]) ?
                                                        $dataShipWishTime[$mstOrder[$columnKey]] :
                                                        $mstOrder[$columnKey];
                                    $valueFrom = isset($dataShipWishTime[$input[$columnKey]]) ?
                                                        $dataShipWishTime[$input[$columnKey]] :
                                                        $input[$columnKey];
                                }
                                $dataLog[] = [
                                    'receive_id' => $mstOrder->receive_id,
                                    'log_title'  => __('messages.change_order_info'),
                                    'log_contents' => "【{$columnName}】{$valueTo} → {$valueFrom}",
                                    'log_status'   => 1,
                                    'in_ope_cd'    => Auth::user()->tantou_code,
                                    'up_ope_cd'    => Auth::user()->tantou_code
                                ];
                                // Update delivery method of mst_order

                                $mstOrder[$columnKey] =  $input[$columnKey];
                                $isOrder              = true;
                            }
                        }
                        if ($tableKey === 'dt_delivery') {
                            if (isset($input['data_delivery']['delivery_code']) &&
                                $input['data_delivery']['delivery_code'] != $input[$columnKey]) {
                                $dataLog[] = [
                                    'receive_id' => $mstOrder->receive_id,
                                    'log_title'  => __('messages.change_order_info'),
                                    'log_contents' =>
                                        "【{$columnName}】{$input['data_delivery']['delivery_code']} → {$input[$columnKey]}",
                                    'log_status'   => 1,
                                    'in_ope_cd'    => Auth::user()->tantou_code,
                                    'up_ope_cd'    => Auth::user()->tantou_code
                                ];
                                $dataDelivery[$columnKey]  = $input[$columnKey];
                                $delKey = [
                                    'received_order_id' => $input['data_delivery']['received_order_id'],
                                    'subdivision_num'   => $input['data_delivery']['subdivision_num'],
                                ];
                                $isDelivery                = true;
                            }
                        }
                        if ($tableKey === 'dt_order_product_detail' && count($resOPD) > 0) {
                            foreach ($oldProducts as $item) {
                                if (isset($resOPD[$item['receive_id'].$item['detail_line_num']])) {
                                    $curOPD = $resOPD[$item['receive_id'].$item['detail_line_num']];
                                    if ($curOPD[$columnKey] != $item[$columnKey]) {
                                        if ((int)$item['product_status'] === PRODUCT_STATUS['CANCEL']) {
                                            $dataLog[] = [
                                                'receive_id' => $mstOrder->receive_id,
                                                'log_title'  => __('messages.change_order_info'),
                                                'log_contents' =>
                                                    "【{$columnName}】".
                                                        "【detail_line_num: {$item['detail_line_num']}】".
                                                            "{$curOPD[$columnKey]} → {$item[$columnKey]}",
                                                'log_status'   => 1,
                                                'in_ope_cd'    => Auth::user()->tantou_code,
                                                'up_ope_cd'    => Auth::user()->tantou_code
                                            ];
                                            $upPS[] = [
                                                'receive_id'      => $item['receive_id'],
                                                'detail_line_num' => $item['detail_line_num'],
                                                'product_status'  => (int)$item['product_status'],
                                            ];
                                            $upOD[] = [
                                                'receive_id'      => $item['receive_id'],
                                                'detail_line_num' => $item['detail_line_num'],
                                            ];
                                        }
                                    }
                                }
                            }
                        }

                        if ($tableKey === 'mst_order_detail') {
                            foreach ($oldProducts as $item) {
                                if ($item['new_quantity'] !== false) {
                                    $dataLog[] = [
                                        'receive_id' => $mstOrder->receive_id,
                                        'log_title'  => __('messages.change_order_info'),
                                        'log_contents' =>
                                            "【{$columnName}】".
                                                "【detail_line_num: {$item['detail_line_num']}】".
                                                    "quantity → {$item['new_quantity']}",
                                        'log_status'   => 1,
                                        'in_ope_cd'    => Auth::user()->tantou_code,
                                        'up_ope_cd'    => Auth::user()->tantou_code
                                    ];
                                    $upQuan[] = [
                                        'receive_id'      => $item['receive_id'],
                                        'detail_line_num' => $item['detail_line_num'],
                                        'quantity'        => $item['new_quantity'],
                                    ];
                                }
                            }
                        }
                    }
                }
                if ($isCustomer) {
                    $mstCustomer->up_ope_cd = Auth::user()->tantou_code;
                    $mstCustomer->save();
                }
                if ($isDelivery) {
                    $dataDelivery['up_ope_cd'] = Auth::user()->tantou_code;
                    $dtDelivery->updateData($delKey, $dataDelivery);
                }
                if (count($upPS) > 0) {
                    foreach ($upPS as $ps) {
                        $dtOrderProductDetail->updateGroupProduct(
                            $ps['receive_id'],
                            $ps['detail_line_num'],
                            ['product_status' => $ps['product_status']]
                        );
                    }
                }
                if (count($upQuan) > 0) {
                    foreach ($upQuan as $uq) {
                        $mstOrderDetail->updateData(
                            $uq['receive_id'],
                            $uq['detail_line_num'],
                            [
                                'quantity' => $uq['quantity'],
                            ]
                        );
                    }
                }
                $dataCheckKepHai = [];
                if (count($upOD) > 0) {
                    $arrUpdate = [
                        'quantity' => 0,
                        'price'    => 0,
                    ];
                    foreach ($upOD as $uq) {
                        $mstOrderDetail->updateData(
                            $uq['receive_id'],
                            $uq['detail_line_num'],
                            $arrUpdate
                        );
                    }
                }
                if ($isError) {
                    foreach ($productDetail as $key => $val) {
                        $total += $val['price'] * (int)$val['quantity'];
                        if ($val['new_product'] === 1) {
                            $newProducts[] = $val;
                        }
                    }
                }

                if (count($dataLog) > 0) {
                    $dtOrderUpdateLog->insert($dataLog);
                }
                //Update t_order_detail
                if (count($resCheckEdi['detail']) > 0 && !$isError) {
                    foreach ($resCheckEdi['detail'] as $key => $val) {
                        $tOrderDetail->updateData(['order_code' => (string)$key], $val);
                        $tOrder->updateData(['order_code' => (string)$key], ['cancel_flg' => $val['cancel_flg']]);
                    }
                }
                //Update t_order_direct
                if (count($resCheckEdi['direct']) > 0 && !$isError) {
                    foreach ($resCheckEdi['direct'] as $key => $val) {
                        $tOrderDirect->updateData(['order_code' => (string)$key], $val);
                    }
                }
                //Update column wait_delivery_num table mst_stock_status
                if (count($resCheckEdi['deli']) > 0 && !$isError) {
                    foreach ($resCheckEdi['deli'] as $key => $val) {
                        $modelStock->where('product_code', $key)->decrement(
                            'wait_delivery_num',
                            $val,
                            ['up_date' => now()]
                        );
                    }
                }
                //update cancel order to supplier
                if (count($resCheckEdi['order_supplier']) > 0 && !$isError) {
                    foreach ($resCheckEdi['order_supplier'] as $val) {
                        $dtOrderToSupplier ->updateData($val['key'], $val['value']);
                    }
                }
                //Update column order_zan_num table mst_stock_status
                if (count($resCheckEdi['zan']) > 0 && !$isError) {
                    foreach ($resCheckEdi['zan'] as $key => $val) {
                        $modelStock->where('product_code', $key)->decrement(
                            'order_zan_num',
                            $val,
                            ['up_date' => now()]
                        );
                    }
                }
                //Update when change quantity
                if (count($resCheckEdi['change']) > 0) {
                    foreach ($resCheckEdi['change'] as $key => $val) {
                        if ($key === 'dt_order_product_detail') {
                            foreach ($val as $value) {
                                $dtOrderProductDetail->updateData($value['key'], $value['update']);
                            }
                        } else {
                            $modelStock->where('product_code', $key)
                                       ->update($val);
                        }
                    }
                }
                //Process add new product
                if (count($newProducts) > 0) {
                    foreach ($newProducts as $newProduct) {
                        $dataLog[] = [
                            'receive_id' => $mstOrder->receive_id,
                            'log_title'  => __('messages.change_order_info'),
                            'log_contents' =>"【Add Product】 Product Code: {$newProduct['product_code']} | ".
                                "Quantity: {$newProduct['quantity']}",
                            'log_status'   => 1,
                            'in_ope_cd'    => Auth::user()->tantou_code,
                            'up_ope_cd'    => Auth::user()->tantou_code
                        ];
                    }
                    if (count($dataLog) > 0) {
                        $dtOrderUpdateLog->insert($dataLog);
                    }
                    if ($isError) {
                        $this->processAddNewProductError($newProducts, $mstOrder, $mstCustomer, $receiverId);
                    } else {
                        $this->processAddNewProduct($newProducts, $mstOrder, $mstCustomer, $receiverId);
                    }
                    $isChange = true;
                }
                $requestPrice = $mstOrder->request_price;
                //Process re-calculate
                if ($isChange || $isCancel) {
                    $dataOrders = $mstOrder->getDataReCalculate($mstOrder->receive_id);
                    $dataOrder  = $dataOrders[0];
                    $shipChargeDiscount = $dataOrder->pay_charge_discount;
                    $point              = $dataOrder->used_point;
                    $coupon             = $dataOrder->used_coupon;

                    $arrSD              = [];
                    $arrSD[$receiverId] = 0;
                    if ($total >= $dataOrder->below_limit_price &&
                        $dataOrder->add_ship_charge === 1 &&
                        $dataOrder->mall_id !== 3) {
                        $arrSD[$receiverId] = 5400;
                    }
                    if ($total < $dataOrder->below_limit_price &&
                        $dataOrder->add_ship_charge === 1 &&
                        $dataOrder->mall_id !== 3) {
                        $arrSD[$receiverId] = 5400;
                    }
                    if ($total < $dataOrder->below_limit_price && $dataOrder->add_ship_charge === 0 &&
                        $dataOrder->mall_id !== 3) {
                        $arrSD[$receiverId] = $dataOrder->charge_price;
                    }

                    $arrGPD = [];
                    $arrGPD[$receiverId] = $total;
                    foreach ($dataOrders as $data) {
                        if ($data->receiver_id !== $receiverId) {
                            if (!isset($arrGPD[$data->receiver_id])) {
                                $arrGPD[$data->receiver_id] = 0;
                            }
                            $arrGPD[$data->receiver_id] += $data->price * $data->quantity;
                            $arrSD[$data->receiver_id]  =  $data->ship_charge_detail;
                        }
                    }

                    $totalGPD = 0;
                    foreach ($arrGPD as $rev => $gpd) {
                        $totalGPD += $gpd;
                    }
                    $shipCharge = 0;
                    foreach ($arrSD as $rev => $sd) {
                        $shipCharge += $sd;
                        if ($dataOrder->goods_price >= $dataOrder->below_limit_price
                            && $arrGPD[$rev] < $dataOrder->below_limit_price
                        ) {
                            $shipChargeDiscount += $sd;
                        }
                        if ($dataOrder->goods_price < $dataOrder->below_limit_price
                            && $arrGPD[$rev] >= $dataOrder->below_limit_price) {
                            $shipChargeDiscount = 0;
                        }
                    }

                    $totalPrice   = $totalGPD + $shipCharge + $dataOrder->pay_after_charge + $dataOrder->pay_charge;
                    $requestPrice = $totalPrice - $shipChargeDiscount -
                                    $dataOrder->discount - $point - $coupon;
                    $returnCP = [
                        'return_point'  => 0,
                        'return_coupon' => 0
                    ];
                    if ($requestPrice < 0) {
                        if (($point + $requestPrice) > 0) {
                            $point += $requestPrice;
                            $returnCP['return_point'] = -$requestPrice;
                            $mstOrder->request_price = 0;
                        } elseif (($point + $coupon + $requestPrice) > 0) {
                            $returnCP['return_point']  = $point;
                            $returnCP['return_coupon'] = - ($point + $requestPrice);
                            $coupon       = $point + $coupon + $requestPrice;
                            $point        = 0;
                            $mstOrder->request_price = 0;
                        }
                    } else {
                        $mstOrder->request_price = $requestPrice;
                    }

                    $mstOrderDetail->updateDataByKey(
                        [
                            'receive_id'  => $mstOrder->receive_id,
                            'receiver_id' => $receiverId
                        ],
                        [
                            'ship_charge_detail' => $arrSD[$receiverId],
                            'goods_price_detail' => $arrGPD[$receiverId]
                        ]
                    );

                    $mstOrder->ship_charge         = $shipCharge;
                    $mstOrder->total_price         = $totalPrice;
                    $mstOrder->pay_charge_discount = $shipChargeDiscount;
                    $mstOrder->used_point          = $point;
                    $mstOrder->used_coupon         = $coupon;
                    $mstOrder->goods_price         = $totalGPD;
                    $mstOrder->is_mall_update      = 1;
                }
                $mstOrder->up_ope_cd = Auth::user()->tantou_code;
                $mstOrder->up_date   = now();
                $mstOrder->save();
                //Process is_change
                if ($isChange) {
                    if (in_array($mstOrder->payment_method, [2, 6, 7])) {
                        if ($mstOrder->payment_status === 1) {
                            if ($requestPrice < $oldRequestPrice) {
                                if ((int)$request->input('check_show_repay_popup', 0) === 1) {
                                    DB::rollback();
                                    return response()->json([
                                        'status' => 3
                                    ]);
                                }
                                $repayType = (int)$request->input('repay_type', 1);
                                $this->processRePayment(
                                    $mstOrder,
                                    $mstOrder->request_price,
                                    $oldRequestPrice,
                                    $returnCP,
                                    $repayType
                                );
                            }
                        }
                    }
                    if ($mstOrder->order_status > ORDER_STATUS['PAYMENT_CONFIRM']) {
                        $mstOrder->order_status     = ORDER_STATUS['ORDER'];
                        $mstOrder->order_sub_status = ORDER_SUB_STATUS['NEW'];
                    }
                    $mstOrder->is_mall_update = 1;
                    $mstOrder->up_ope_cd = Auth::user()->tantou_code;
                    $mstOrder->up_date   = now();
                    $mstOrder->save();
                } elseif ($isCancel) {
                    if (in_array($mstOrder->payment_method, [2, 6, 7])) {
                        if ($mstOrder->payment_status === 1) {
                            if ($requestPrice < $oldRequestPrice) {
                                if ((int)$request->input('check_show_repay_popup', 0) === 1) {
                                    DB::rollback();
                                    return response()->json([
                                        'status' => 3
                                    ]);
                                }
                                $repayType = (int)$request->input('repay_type', 1);
                                $this->processRePayment(
                                    $mstOrder,
                                    $mstOrder->request_price,
                                    $oldRequestPrice,
                                    $returnCP,
                                    $repayType
                                );
                            }
                        }
                    }
                }
                if ((int)$request->input('check_show_repay_popup', 0) === 1) {
                    DB::rollback();
                    return response()->json([
                        'status' => 4
                    ]);
                }
                if (($isChange || $isCancel || $flgChangeOrder) && $mstOrder->is_ignore_error === 0) {
                    $dtOPD  = new DtOrderProductDetail();
                    $cols   = ['product_status'];
                    $resOPD = $dtOPD->getDataByReceiverId($mstOrder->receive_id, $receiverId, $cols);
                    $resCheckInfo = $this->checkInfoAfterSave($this->checkRules, $mstOrder, $receiverId, true, $resOPD);
                    if (!is_bool($resCheckInfo)) {
                        return response()->json([
                            'status'   => -1,
                            'messages' => $resCheckInfo
                        ]);
                    } elseif (!$resCheckInfo) {
                        $mstOrder->is_mail_sent = 0;
                        $mstOrder->save();
                    }
                }
                DB::commit();
                DB::connection('edi')->commit();
                return response()->json([
                    'status'  => 1
                ]);
            } catch (\Exception $ex) {
                DB::rollBack();
                DB::connection('edi')->rollback();
                return response()->json([
                    'status' => -1,
                    'messages' => $ex->getMessage(),
                ]);
            }
        }
        return view('order-management.order_detail_editing', ['paramsFilter' => $paramsFilter]);
    }

    /**
     * Process add new product
     *
     * @param  array  $newProducts
     * @param  object $mstOrder
     * @param  object $mstCustomer
     * @return void
     */
    private function processAddNewProduct($newProducts, $mstOrder, $mstCustomer, $receiverId)
    {
        $mstProduct           = new MstProduct();
        $mstOrderDetail       = new MstOrderDetail();
        $dtOrderProductDetail = new DtOrderProductDetail();
        $mstStockStatus       = new MstStockStatus();

        $maxOD   = $mstOrderDetail->getMaxItem($mstOrder->receive_id, $receiverId);
        $maxDLN  = $mstOrderDetail->where('receive_id', $mstOrder->receive_id)->max('detail_line_num');

        $maxOPD  = $dtOrderProductDetail->getMaxItem($mstOrder->receive_id);
        $maxPDLN = $maxOPD->detail_line_num;
        $maxSLN  = $maxOPD->sub_line_num;

        $dataOrderDetail    = array();
        $dataOrderProDetail = array();
        foreach ($newProducts as $product) {
            $maxDLN++;
            $maxPDLN++;
            $dataOrderDetail[$mstOrder->receive_id . '_' . $maxDLN] = [
                'receive_id'          => $mstOrder->receive_id,
                'detail_line_num'     => ($maxDLN),
                'product_code'        => $product['product_code'],
                'product_name'        => $product['product_name'],
                'price'               => $product['price'],
                'quantity'            => $product['quantity'],
                'receiver_id'         => $maxOD->receiver_id,
                'ship_date'           => $maxOD->ship_date,
                'delivery_person_id'  => $maxOD->delivery_person_id,
                'package_ship_number' => $maxOD->package_ship_number,
                'time_wish_ship'      => $maxOD->time_wish_ship,
                'in_ope_cd'           => Auth::user()->tantou_code,
                'up_ope_cd'           => Auth::user()->tantou_code
            ];
            $productData    = $mstProduct->getItemByProductCode($product['product_code']);
            $productService = $productData->product_service;
            if ($productService === 1) {
                $result = $mstProduct->getProductSet($product['product_code']);
                if ($result->count() > 0) {
                    foreach ($result as $item) {
                        $maxSLN++;
                        $freeNum         = $item->nanko_num - $item->order_zan_num - $item->return_num;
                        $receiveOrderNum = $item->component_num * $product['quantity'];
                        $dataCal         = $this->calOrderInfo(
                            $item->product_daihiki,
                            $item->stock_lower_limit,
                            $freeNum,
                            $receiveOrderNum
                        );
                        $dataOrderProDetail[] = [
                            'receive_id'         => $mstOrder->receive_id,
                            'detail_line_num'    => $maxPDLN,
                            'sub_line_num'       => $maxSLN,
                            'product_code'       => $item->product_code,
                            'child_product_code' => $item->child_product_code,
                            'received_order_num' => $receiveOrderNum,
                            'stock_num'          => $dataCal['stock_num'],
                            'order_num'          => $dataCal['order_num'],
                            'suplier_id'         => $item->supplier_id,
                            'price_invoice'      => $item->price_invoice,
                            'delivery_type'      => $dataCal['delivery_type'],
                            'arrival_date_plan'  => null,
                            'stock_status'       => $dataCal['stock_status'],
                            'product_status'     => $dataCal['product_status'],
                            'sub_process_status' => 3
                        ];
                        $arrUpdate = [];
                        if ($dataCal['delivery_type'] === 1 || $dataCal['delivery_type'] === 3) {
                            $arrUpdate['order_zan_num'] = $item->order_zan_num + $receiveOrderNum;
                            if ($dataCal['product_status'] === PRODUCT_STATUS['WAIT_TO_SHIP']) {
                                $arrUpdate['wait_delivery_num'] = $item->wait_delivery_num + $receiveOrderNum;
                            }
                        }
                        if (count($arrUpdate) !== 0) {
                            $keys = ['product_code' => $item->child_product_code];
                            $mstStockStatus->updateData($keys, $arrUpdate);
                        }
                    }
                }
            } else {
                $result = $mstProduct->getNoProductSet($product['product_code']);
                if ($result->count() > 0) {
                    foreach ($result as $item) {
                        $maxSLN++;
                        $freeNum         = $item->nanko_num - $item->order_zan_num - $item->return_num;
                        $dataCal         = $this->calOrderInfo(
                            $item->product_daihiki,
                            $item->stock_lower_limit,
                            $freeNum,
                            $product['quantity']
                        );
                        $dataOrderProDetail[] = [
                            'receive_id'         => $mstOrder->receive_id,
                            'detail_line_num'    => $maxPDLN,
                            'sub_line_num'       => $maxSLN,
                            'product_code'       => $item->product_code,
                            'child_product_code' => null,
                            'received_order_num' => $product['quantity'],
                            'stock_num'          => $dataCal['stock_num'],
                            'order_num'          => $dataCal['order_num'],
                            'suplier_id'         => $item->supplier_id,
                            'price_invoice'      => $item->price_invoice,
                            'delivery_type'      => $dataCal['delivery_type'],
                            'arrival_date_plan'  => null,
                            'stock_status'       => $dataCal['stock_status'],
                            'product_status'     => $dataCal['product_status'],
                            'sub_process_status' => 3
                        ];
                        $arrUpdate = [];
                        if ($dataCal['delivery_type'] === 1 || $dataCal['delivery_type'] === 3) {
                            $arrUpdate['order_zan_num'] = $item->order_zan_num + $product['quantity'];
                            if ($dataCal['product_status'] === PRODUCT_STATUS['WAIT_TO_SHIP']) {
                                $arrUpdate['wait_delivery_num'] = $item->wait_delivery_num + $product['quantity'];
                            }
                        }
                        if (count($arrUpdate) !== 0) {
                            $keys = ['product_code' => $item->product_code];
                            $mstStockStatus->updateData($keys, $arrUpdate);
                        }
                    }
                }
            }
        }

        $mstOrderDetail->insert($dataOrderDetail);
        $dtOrderProductDetail->insert($dataOrderProDetail);

        // if ($mstOrder->order_status === 5) {
        //     $otsData  = [];
        //     $otsdData = [];
        //     foreach ($dataOrderProDetail as $opd) {
        //         if ($opd['product_status'] === 2) {
        //             if (in_array($opd['delivery_type'], [1, 3])) {
        //                 $otsData[] = $opd;
        //             }
        //             if ($opd['delivery_type'] === 2) {
        //                 $otsdData[] = $opd;
        //             }
        //         }
        //     }
        //     if (count($otsData) > 0) {
        //         $this->orderToSupplier($otsData);
        //     }
        //     if (count($otsdData) > 0) {
        //         $this->orderToDirect($otsdData, $mstCustomer, $dataOrderDetail);
        //     }
        // }
    }

    /**
     * Calculate order info
     *
     * @param  int   $productDaihiki
     * @param  int   $stockDefault
     * @param  int   $freeNum
     * @param  int   $inputQuantity
     * @return array
     */
    private function calOrderInfo($productDaihiki, $stock_lower_limit, $freeNum, $inputQuantity)
    {
        $deliveryType = 1;
        $orderNum = 0;
        if ($productDaihiki > 0) {
            $deliveryType = 2;
        } elseif ($stock_lower_limit === 0) {
            $deliveryType = 3;
            $orderNum = $inputQuantity;
        }

        $stockNum = 0;
        if ($deliveryType === 1 && $freeNum >= $inputQuantity) {
            $stockNum = $inputQuantity;
        }

        //$orderNum = 0;
        if ($deliveryType === 1 && $freeNum < $inputQuantity) {
            $orderNum = $inputQuantity;
        }

        $stockStatus = 0;
        if ($deliveryType === 1 && $freeNum >= $inputQuantity) {
            $stockStatus = 1;
        } elseif ($deliveryType === 1 && $freeNum < $inputQuantity) {
            $stockStatus = 2;
        }

        $productStatus = 2;
        if ($stockStatus === 1) {
            $productStatus = 1;
        }
        return [
            'delivery_type'  => $deliveryType,
            'stock_num'      => $stockNum,
            'order_num'      => $orderNum,
            'stock_status'   => $stockStatus,
            'product_status' => $productStatus
        ];
    }

    /**
     * Get delivery period
     *
     * @param  string $deliveryFlg
     * @return int
     */
    private function getDeliveryPeriod($deliveryFlg)
    {
        switch ($deliveryFlg) {
            case '14営業日':
                return 14;
            case '３営業日':
                return 3;
            case '５営業日':
                return 5;
            case '７営業日':
                return 7;
            case '即納':
                return 3;
        }
        return 30;
    }

    /**
     * Order to supplier
     *
     * @param  array  $dataOrderProDetail
     * @return void
     */
    // private function orderToSupplier($dataOrderProDetail)
    // {
    //     $mstProduct           = new MstProduct();
    //     $tAdmin               = new TAdmin();
    //     $tOrder               = new TOrder();
    //     $tOrderDetail         = new TOrderDetail;
    //     $cheetah              = new Cheetah();
    //     $tSeq                 = new TSeq();
    //     $dtOrderToSupplier    = new DtOrderToSupplier();
    //     $dtOrderProductDetail = new DtOrderProductDetail();

    //     $maxOrder   = $dtOrderToSupplier->getMaxItemByOrderCode();
    //     $indexOrder = 0;
    //     if ($maxOrder !== null && !empty($maxOrder->order_code)) {
    //         $indexOrder = (int)substr($maxOrder->order_code, 8);
    //     }

    //     foreach ($dataOrderProDetail as $od) {
    //         $indexOrder++;
    //         $productFind = empty($od['child_product_code'])?$od['product_code']:$od['child_product_code'];
    //         $productData = $mstProduct->getItemByProductCode($productFind);
    //         $adminData   = $tAdmin->getItemBySuppliercd($od['suplier_id']);
    //         $cheetahData = $cheetah->find($productFind);
    //         $orderCode      = 'HO' . date('Ym') . sprintf('%07d', $indexOrder);
    //         $deliveryperiod = $this->getDeliveryPeriod($cheetahData->delivery_flg);
    //         $fax            = '';
    //         $faxFlg         = 0;

    //         if ($adminData !== null && $adminData->send_fax_flg === 1) {
    //             $fax    = $adminData->fax;
    //             $faxFlg = 1;
    //         }
    //         $tOrderInsert = [
    //             'suppliercd'     => $productData->price_supplier_id,
    //             'SerialNo'       => null,
    //             'order_date'     => date('Y-m-d H:i:s'),
    //             'order_code'     => $orderCode,
    //             'supplieritemcd' => (string)$productData->supplier_code,
    //             'deiiveryperiod' => $deliveryperiod,
    //             'supplykb'       => '客注',
    //             'item_code'      => $productFind,
    //             'jan_code'       => (string)$productData->product_jan,
    //             'maker_name'     => (string)$productData->maker_full_nm,
    //             'maker_code'     => (string)$productData->product_maker_code,
    //             'note_code'      => '空',
    //             'name'           => (string)$productData->product_name,
    //             'delivery'       => '南港',
    //             'quantity'       => $od['received_order_num'],
    //             'color'          => (string)$productData->product_color,
    //             'size'           => (string)$productData->product_size,
    //             'price'          => (int)$productData->price_invoice,
    //             'other'          => null,
    //             'itemurl'        => (string)$productData->rak_img_url_1,
    //             'send_fax_flg'   => 0,
    //             'cancel_flg'     => 0,
    //             'del_flg'        => 0,
    //             'valid_flg'      => 1,
    //             'created_at'     => date('Y-m-d H:i:s'),
    //             'updated_at'     => date('Y-m-d H:i:s'),
    //             'fax'            => $fax,
    //             'fax_flg'        => $faxFlg,
    //             'mail'           => '',
    //             'mail_flg'       => 0,
    //             'send_mail_flg'  => 0,
    //             'send_mail_date' => null
    //         ];

    //         $tOrderId = $tOrder->insertGetId($tOrderInsert);
    //         $tSeqId   = $tSeq->insertGetId(['directkb' => 0]);

    //         $tOrderDetailInsert = [
    //             'suppliercd'         => $productData->price_supplier_id,
    //             't_order_id'         => $tOrderId,
    //             'm_order_type_id'    => 2,
    //             't_nouhin_id'        => null,
    //             't_seq_id'           => $tSeqId,
    //             'requestbno'         => null,
    //             'order_code'         => $orderCode,
    //             'shipment_date_plan' => null,
    //             'shipment_date'      => null,
    //             'quantity'           => $od['received_order_num'],
    //             'price'              => (int) $productData->price_invoice,
    //             'other'              => null,
    //             'memo'               => null,
    //             'direct_flg'         => 0,
    //             'pass'               => null,
    //             'state_flg'          => 1,
    //             'cancel_flg'         => 0,
    //             'filmaker_flg'       => 1,
    //             'new_flg'            => 0,
    //             'request_flg'        => 0,
    //             'reminder_flg'       => 0,
    //             'stock_quantity'     => 0,
    //             'shortage_quantity'  => 0,
    //             'stock_other'        => null,
    //             'stock_flg'          => 0,
    //             't_admin_id'         => 0,
    //             'del_flg'            => 0,
    //             'valid_flg'          => 1,
    //             'created_at'         => date('Y-m-d H:i:s'),
    //             'updated_at'         => date('Y-m-d H:i:s')
    //         ];

    //         $tOrderDetail->insert($tOrderDetailInsert);

    //         $keys = [
    //             'receive_id'      => $od['receive_id'],
    //             'detail_line_num' => $od['detail_line_num'],
    //             'sub_line_num'    => $od['sub_line_num']
    //         ];
    //         $dtOrderProductDetail->updateData($keys, ['product_status' => PRODUCT_STATUS['ORDERING']]);

    //         $dtOTSData = [
    //             'order_code'          => $orderCode,
    //             'order_date'          => date('Y-m-d H:i:s'),
    //             'order_type'          => '客注',
    //             'arrival_date_plan'   => null,
    //             'arrival_date'        => null,
    //             'arrival_quantity'    => 0,
    //             'incomplete_quantity' => 0,
    //             'other'               => null,
    //             'remarks'             => null,
    //             'wait_days'           => null,
    //             'process_status'      => 0,
    //             'receive_id'          => $od['receive_id'],
    //             'detail_line_num'     => $od['detail_line_num'],
    //             'sub_line_num'        => $od['sub_line_num'],
    //             'in_date'             => date('Y-m-d H:i:s'),
    //             'up_date'             => date('Y-m-d H:i:s')
    //         ];
    //         $dtOrderToSupplier->insert($dtOTSData);
    //     }
    // }

    /**
     * Order to direct
     * @param  array  $dataOrderProDetail
     * @param  object $customer
     * @param  array  $dataOrderDetail
     * @return void
     */
    // private function orderToDirect($dataOrderProDetail, $customer, $dataOrderDetail)
    // {
    //     $mstProduct           = new MstProduct();
    //     $tAdmin               = new TAdmin();
    //     $tOrder               = new TOrder();
    //     $tOrderDetail         = new TOrderDetail;
    //     $cheetah              = new Cheetah();
    //     $tSeq                 = new TSeq();
    //     $dtOrderToSupplier    = new DtOrderToSupplier();
    //     $dtOrderProductDetail = new DtOrderProductDetail();
    //     $mstCustomer          = new MstCustomer();
    //     $tCustomer            = new TCustomer();
    //     $tOrderDirect         = new TOrderDirect();

    //     $maxOrder   = $dtOrderToSupplier->getMaxItemByOrderCode();
    //     $indexOrder = 0;
    //     if ($maxOrder !== null && !empty($maxOrder->order_code)) {
    //         $indexOrder = (int)substr($maxOrder->order_code, 8);
    //     }
    //     foreach ($dataOrderProDetail as $od) {
    //         $indexOrder++;
    //         $productFind  = empty($od['child_product_code'])?$od['product_code']:$od['child_product_code'];
    //         $productData  = $mstProduct->getItemByProductCode($productFind);
    //         $adminData    = $tAdmin->getItemBySuppliercd($od['suplier_id']);
    //         $cheetahData  = $cheetah->find($productFind);

    //         $orderCode      = 'HO' . date('Ym') . sprintf('%07d', $indexOrder);
    //         $deliveryperiod = $this->getDeliveryPeriod($cheetahData->delivery_flg);
    //         $fax            = '';
    //         $faxFlg         = 0;

    //         if ($adminData !== null && $adminData->send_fax_flg === 1) {
    //             $fax    = $adminData->fax;
    //             $faxFlg = 1;
    //         }

    //         $receiverId   = $dataOrderDetail[$od['receive_id'] . '_' . $od['detail_line_num']]['receiver_id'];
    //         $receiver     = $mstCustomer->find($receiverId);

    //         $checkCustomer = [
    //             'SerialNo'  => 0,
    //             'c_nm'      => $customer['last_name'] . $customer['first_name'],
    //             'c_postal'  => $customer['zip_code'],
    //             'c_address' => $customer['prefecture'] . $customer['city'] . $customer['sub_address'],
    //             'c_tel'     => $customer['tel_num'],
    //             'd_nm'      => $receiver['last_name'] . $receiver['first_name'],
    //             'd_postal'  => $receiver['zip_code'],
    //             'd_address' => $receiver['prefecture'] . $receiver['city'] . $receiver['sub_address'],
    //             'd_tel'     => $receiver['tel_num'],
    //             'del_flg'   => 0,
    //             'valid_flg' => 1
    //         ];

    //         $resCustomer = $tCustomer->checkCustomerExist($checkCustomer);

    //         if ($resCustomer !== null) {
    //             $customerId = $resCustomer->t_customer_id;
    //         } else {
    //             $customerId     = $tCustomer->insertGetId($checkCustomer);
    //             $deliveryperiod = null;
    //         }

    //         $tSeqId = $tSeq->insertGetId(['directkb' => 1]);

    //         $tODD = [
    //             'suppliercd'           => $productData->price_supplier_id,
    //             't_customer_id'        => $customerId,
    //             't_delivery_id'        => null,
    //             'm_order_type_id'      => 2,
    //             'SerialNo'             => 0,
    //             't_seq_id'             => $tSeqId,
    //             'order_date'           => date('Y-m-d H:i:s'),
    //             'order_code'           => $orderCode,
    //             'supplieritemcd'       => (string) $productData->supplier_code,
    //             'supplykb'             => '客注',
    //             'deiiveryperiod'       => $deliveryperiod,
    //             'item_code'            => $productFind,
    //             'jan_code'             => (string) $productData->product_jan,
    //             'maker_name'           => (string) $productData->maker_full_nm,
    //             'maker_code'           => (string) $productData->product_maker_code,
    //             'note_code'            => '',
    //             'name'                 => (string) $productData->product_name_long,
    //             'color'                => (string) $productData->product_color,
    //             'size'                 => (string) $productData->product_size,
    //             'daito_price'          => (int) $productData->price_invoice,
    //             'price'                => (int) $productData->price_invoice,
    //             'quantity'             => $od['received_order_num'],
    //             'shipment_date_plan'   => null,
    //             'other'                => null,
    //             'memo'                 => null,
    //             'mail'                 => '',
    //             'send_mail_flg'        => 0,
    //             'send_mail_date'       => null,
    //             'fax'                  => $fax,
    //             'fax_flg'              => $faxFlg,
    //             'send_fax_flg'         => 0,
    //             'state_flg'            => 1,
    //             'cancel_flg'           => 0,
    //             'send_cancel_fax_flg'  => 0,
    //             'send_cancel_fax_date' => null,
    //             'filmaker_flg'         => 1,
    //             'del_flg'              => 0,
    //             'valid_flg'            => 1
    //         ];

    //         $tOrderDirect->insert($tODD);

    //         $keys = [
    //             'receive_id'      => $od['receive_id'],
    //             'detail_line_num' => $od['detail_line_num'],
    //             'sub_line_num'    => $od['sub_line_num']
    //         ];
    //         $dtOrderProductDetail->updateData($keys, ['product_status' => PRODUCT_STATUS['ORDERING']]);

    //         $dtOTSData = [
    //             'order_code'          => $orderCode,
    //             'order_date'          => date('Y-m-d H:i:s'),
    //             'order_type'          => '客注',
    //             'arrival_date_plan'   => null,
    //             'arrival_date'        => null,
    //             'arrival_quantity'    => 0,
    //             'incomplete_quantity' => 0,
    //             'other'               => null,
    //             'remarks'             => null,
    //             'wait_days'           => null,
    //             'process_status'      => 0,
    //             'receive_id'          => $od['receive_id'],
    //             'detail_line_num'     => $od['detail_line_num'],
    //             'sub_line_num'        => $od['sub_line_num'],
    //             'in_date'             => date('Y-m-d H:i:s'),
    //             'up_date'             => date('Y-m-d H:i:s')
    //         ];
    //         $dtOrderToSupplier->insert($dtOTSData);
    //     }
    // }

    /**
     * Check info order after save
     *
     * @param   object  $mstOrder
     * @param   bool    $checkStatus
     * @param   object  $productStatus
     * @return  mixed
     */
    private function checkInfoAfterSave(
        $checkRules,
        $mstOrder,
        $receiverId = null,
        $checkStatus = false,
        $productStatus = null
    ) {
        $modelO        = new MstOrder();
        $modelCustomer = new MstCustomer();
        $value = [];
        $dataCheck    = $modelO->getDataCheckError($mstOrder->receive_id, $receiverId);
        $dataCustomer = $modelCustomer->find($receiverId);
        if ($dataCheck === null) {
            return false;
        } else {
            $value = $dataCheck->toArray();
            $value['receive_id_checkPostalCodeAndAddress']         = $value['receive_id'];
            $value['receive_id_checkPostalCodeAndAddressReceiver'] = $value['receive_id'];
            //$value['receive_id_checkPostalCodeAndAddressReceiverLength'] = $value['receive_id'];
            $value['receive_id_checkLimitRequestPrice']            = $value['receive_id'];
            $value['receive_id_checkChargeFeeCulDelivery']         = $value['receive_id'];
            $value['receive_id_checkChargeFeeTransport']           = $value['receive_id'];
            $value['receive_id_checkChargeFeeDelivery']            = $value['receive_id'];
            $value['receive_id_checkPaymentDeliveryFee']           = $value['receive_id'];
            $value['receive_id_checkBlackList']                    = $value['receive_id'];
            $value['receive_id_checkTelDigits']                    = $value['receive_id'];
             $value['receive_id_checkTelDigits1']                  = $value['receive_id'];
            $value['receive_id_checkPaidByPoint']                  = $value['receive_id'];
            $value['receive_id_checkPaidNotByPoint']               = $value['receive_id'];
            if (!empty($value['delivery_type'])) {
                $checkRules['receive_id_checkDeliveryDirectAndPayment'] = 'checkDeliveryDirectAndPayment';
                $value['receive_id_checkDeliveryDirectAndPayment'] = $value['receive_id'];
            }
            if ($value['mall_id'] === 3) {
                $value['order_firstname']    = 'AMAZON';
                $value['ship_to_first_name'] = 'AMAZON';
            }
            if ($value['mall_id'] === 6) {
                $checkRules = [];
            }
            if ($value['mall_id'] === 7 && $value['payment_method'] === 11) {
                unset($checkRules['total_price']);
                unset($checkRules['request_price']);
                unset($checkRules['price']);
                unset($checkRules['receive_id_checkPaidByPoint']);
            }
            if ($value['mall_id'] === 11) {
                unset($checkRules['total_price']);
                unset($checkRules['price']);
            }
        }
        if ($receiverId !== null) {
                $checkRules['receive_id_checkPostalCodeAndAddressReceiver']
                    = 'checkPostalCodeAndAddressReceiver:' . $receiverId;
                /*
                $checkRules['receive_id_checkPostalCodeAndAddressReceiverLength']
                    = 'checkPostalCodeAndAddressReceiverLength:' . $receiverId;*/
                $checkRules['receive_id_checkTelDigits']
                    = 'checkTelDigits:' . $receiverId;
                $checkRules['receive_id_checkTelDigits1']
                    = 'checkTelDigits1:' . $receiverId;
                $checkRules['receive_id_checkChargeFeeTransport']
                    = 'checkChargeFeeTransport:' . $receiverId;
                $checkRules['receive_id_checkChargeFeeDelivery']
                    = 'checkChargeFeeDelivery:' . $receiverId;
                $checkRules['receive_id_checkChargeFeeCulDelivery']
                    = 'checkChargeFeeCulDelivery:' . $receiverId;
        }

        $subValidator  = Validator::make($value, $checkRules, $this->message);
        $isRecalculate = 0;
        $isConfirmed   = $mstOrder->is_confirmed;
        $isError       = $mstOrder->order_sub_status === ORDER_SUB_STATUS['ERROR'] ?  false : true;
        $checkTel      = false;
        $checkNotPaid  = false;
        if ($subValidator->fails()) {
            $errorVal       = $subValidator->errors();
            $message = [];
            $arrKey = [
                'receive_id_checkChargeFeeTransport',
                'receive_id_checkChargeFeeDelivery',
                'receive_id_checkChargeFeeCulDelivery',
                'receive_id_checkPaymentDeliveryFee',
            ];
            $arrKeyConfirm = [
                'receive_id_checkLimitRequestPrice',
                'receive_id_checkBlackList',
            ];
            $arrKeyTel = [
                'receive_id_checkTelDigits1'
            ];
            $arrKeyPaidNot = [
                'receive_id_checkPaidNotByPoint'
            ];
            $orderSubStatus = ORDER_SUB_STATUS['ERROR'];
            foreach ($errorVal->toArray() as $key => $error) {
                $message[] = $error[0];
                if (in_array($key, $arrKey)) {
                    if ($key === 'receive_id_checkChargeFeeTransport') {
                        $orderSubStatus = ORDER_SUB_STATUS['ERROR_FEE_TRANSPORT'];
                    }
                    $isRecalculate = 1;
                }
                if (in_array($key, $arrKeyConfirm)) {
                    $isConfirmed = 1;
                }
                if (in_array($key, $arrKeyTel)) {
                    $checkTel = true;
                }
                if (in_array($key, $arrKeyPaidNot)) {
                    $checkNotPaid = true;
                }
            }
            $message = implode("\n", $message);
            $isError = true;
        } else {
            $message        = '';
            if ($mstOrder->order_status >= 7) {
                $orderSubStatus = $mstOrder->order_sub_status;
            } else {
                $orderSubStatus = 0;
            }
            if ($checkStatus && $mstOrder->order_sub_status !== ORDER_SUB_STATUS['ERROR'] && count($productStatus) !== 0) {
                $flgCheckStatus = true;
                if (in_array($mstOrder->order_sub_status, [
                        ORDER_SUB_STATUS['OUT_OF_STOCK'],
                        ORDER_SUB_STATUS['STOP_SALE'],
                        ORDER_SUB_STATUS['DELAY'],
                        ORDER_SUB_STATUS['EXPIRES']
                    ])) {
                    foreach ($productStatus as $item) {
                        if (in_array($item->product_status, [
                                PRODUCT_STATUS['OUT_OF_STOCK'],
                                PRODUCT_STATUS['SALE_STOP'],
                                PRODUCT_STATUS['DELAY'],
                                PRODUCT_STATUS['EXPIRATION']
                            ])) {
                            $flgCheckStatus = false;
                        }
                    }
                } else {
                    $flgCheckStatus = false;
                }
                if (!$flgCheckStatus) {
                    $orderSubStatus = $mstOrder->order_sub_status;
                }
            }
        }
        try {
            $mstOrder->order_sub_status = $orderSubStatus;
            $mstOrder->err_text         = $message;
            $mstOrder->is_recalculate   = $isRecalculate;
            $mstOrder->is_confirmed     = $isConfirmed;
            if ($checkNotPaid) {
                $mstOrder->order_sub_status = ORDER_SUB_STATUS['DONE'];
                $mstOrder->order_status     = ORDER_SUB_STATUS['VERIFY_DATA'];
                $mstOrder->err_text         = '';
                $mstOrder->payment_method   = 10;
            }
            if ($checkTel) {
                $mstOrder->order_sub_status    = 0;
                $mstOrder->err_text            = '';
                $mstOrder->urgent_mail_id      = 814;
                $mstOrder->is_send_mail_urgent = 1;
                $dataCustomer->tel_num         = '0667151162';
                $dataCustomer->save();
            }
            $mstOrder->save();
        } catch (\Exception $ex) {
            return $ex;
        }
        return $isError;
    }

    /**
     * check product status after save
     *
     * @param  object $mstOrder
     * @return void
     */
    // private function checkProductStatusAfterSave($mstOrder, $productStatus)
    // {
    //     $flg = true;
    //     foreach ($productStatus as $item) {
    //         if ($item->product_status !== PRODUCT_STATUS['CANCEL']) {
    //             $flg = false;
    //         }
    //     }
    //     if ($flg) {
    //         try {
    //             $mstOrder->order_status        = ORDER_STATUS['CANCEL'];
    //             $mstOrder->order_sub_status    = ORDER_SUB_STATUS['DONE'];
    //             $mstOrder->ship_charge         = 0;
    //             $mstOrder->total_price         = 0;
    //             $mstOrder->pay_charge_discount = 0;
    //             $mstOrder->pay_charge          = 0;
    //             $mstOrder->pay_after_charge    = 0;
    //             $mstOrder->used_point          = 0;
    //             $mstOrder->used_coupon         = 0;
    //             $mstOrder->request_price       = 0;
    //             $mstOrder->goods_price         = 0;
    //             $mstOrder->goods_tax           = 0;
    //             $mstOrder->discount            = 0;
    //             $mstOrder->save();
    //         } catch (\Exception $ex) {
    //             return $ex;
    //         }
    //     }
    //     return $flg;
    // }

    /**
     * Check order edi
     *
     * @param  object  $productStatus
     * @return mixed
     */
    private function checkOrderEdi($receiveId, $receiverId, $products, $check)
    {
        $modelOTS     = new DtOrderToSupplier();
        $modelOPD     = new DtOrderProductDetail();
        $modelSS      = new MstStockStatus();
        $productType3 = array();
        $productType2 = array();
        $productType1 = array();
        $arrUpQuan    = array();
        $arrProduct   = array();
        $isChange     = false;
        $isCanel      = false;
        $cols = [
            'dt_order_product_detail.product_status',
            'dt_order_product_detail.product_code',
            'dt_order_product_detail.child_product_code',
            'dt_order_product_detail.order_code'
        ];
        $dataProduct = $modelOPD->getDataByReceiverId($receiveId, $receiverId, $cols);
        foreach ($products as $item) {
            foreach ($dataProduct as $v) {
                if ($v->child_product_code === $item['product_code']
                    || $v->product_code === $item['product_code']
                ) {
                    $dataOld = $v;
                    break;
                }
            }
            if ($dataOld->product_status !== PRODUCT_STATUS['CANCEL']
                && (int)$item['product_status'] === PRODUCT_STATUS['CANCEL']
            ) {
                $isCanel = true;
                if ($item['delivery_type'] === 3) {
                    $productType3[] = $item;
                } elseif ($item['delivery_type'] === 2) {
                    $productType2[] = $item;
                } elseif ($item['delivery_type'] === 1) {
                    $productType1[] = $item;
                }
            }
            if (isset($item['new_quantity']) && (int)$item['quantity'] !== (int)$item['new_quantity'] && (int)$item['product_status'] !== PRODUCT_STATUS['CANCEL']) {
                $isChange = true;
                $arrUpQuan[] = $item;
                if ($item['is_child'] === 1) {
                    $arrProduct[] = $item['parent_product_code'];
                } else {
                    $arrProduct[] = $item['product_code'];
                }
            }
        }
        if (count($arrUpQuan) !== 0) {
            $dataCheckQuan = $modelOTS->getDataCheckChangeQuantity($arrUpQuan);
            foreach ($dataCheckQuan as $check) {
                if ($check->delivery_type === 3) {
                    if ($check->m_order_type_id_detail === 1) {
                        return false;
                    // } elseif ($check->m_order_type_id === 9) {
                    //     $timeCmp = (time() - strtotime($check->order_date))/86400;
                    //     if ($timeCmp <= 5) {
                    //         return false;
                    //     }
                    }
                } elseif ($check->delivery_type === 2) {
                    if ($check->m_order_type_id_direct === 1) {
                        return false;
                    }
                }
            }
        }
        $updateDetail  = array();
        $updateOTS     = array();
        $arrProCheck   = array();
        $updateDeliNum = array();
        $updateZanNum  = array();
        if (count($productType3) > 0) {
            $resType3 = $modelOTS->getOrderDetailEDI($productType3);
            if (count($resType3) > 0) {
                foreach ($resType3 as $item) {
                    $checkUpdate  = true;
                    $keyOTS = $item->order_code . $item->ots_order_code;
                    if ($item->m_order_type_id === 2) {
                        $updateDetail[$item->order_code] = [
                            'm_order_type_id' => 13,
                            'cancel_flg'      => 1
                        ];
                    } elseif ($item->m_order_type_id === 9) {
                        // $timeCmp = (time() - strtotime($item->order_date))/86400;
                        // if ($timeCmp > 5) {
                            $updateDetail[$item->order_code] = [
                                'm_order_type_id' => 14,
                                'cancel_flg'      => 1
                            ];
                        // } else {
                        //     $arrProCheck[$item->product_code] = $item;
                        //     $checkUpdate  = false;
                        // }
                    } elseif ($item->m_order_type_id === 1) {
                        $arrProCheck[$item->product_code] = $item;
                        $checkUpdate  = false;
                    } elseif ($item->order_type === 3) {
                        $arrProCheck[$item->product_code] = $item;
                        $checkUpdate  = false;
                    } else {
                        $updateDetail[$item->order_code] = [
                            'cancel_flg' => 1
                        ];
                    }
                    if ((int)$item->product_status === PRODUCT_STATUS['CANCEL'] && $checkUpdate) {
                        $updateOTS[$keyOTS] = [
                            'key' => [
                                'order_code'     => $item->ots_order_code,
                                'edi_order_code' => $item->edi_order_code,
                            ],
                            'value' => ['is_cancel' => 1],
                        ];
                    }
                }
            }
            $resType3    = $modelOPD->getDataUpdateOrderringNum($productType3);
            foreach ($resType3 as $item) {
                $productCode = $item->product_code;
                if (!empty($item->child_product_code)) {
                    $productCode = $item->child_product_code;
                }
                if ($item->product_status === PRODUCT_STATUS['WAIT_TO_SHIP'] && $item->order_type !== 3) {
                    $updateDeliNum[$productCode] = $item->received_order_num;
                }
                $updateZanNum[$productCode] = $item->received_order_num;
            }
        }
        $updateDirect = array();
        if (count($productType2) > 0) {
            $resType2 = $modelOTS->getOrderDirectEDI($productType2);
            if (count($resType2) > 0) {
                foreach ($resType2 as $item) {
                    $keyOTS = $item->order_code . $item->ots_order_code;
                    $checkUpdate  = true;
                    if (in_array($item->m_order_type_id, [2, 9])) {
                        $updateDirect[$item->order_code] = [
                            'm_order_type_id' => 15,
                            'cancel_flg'      => 1,
                        ];
                    } elseif ($item->m_order_type_id === 1) {
                        $checkUpdate  = false;
                        $arrProCheck[$item->product_code] = $item;
                    } elseif ($item->order_type === 3) {
                        $checkUpdate  = false;
                        $arrProCheck[$item->product_code] = $item;
                    } else {
                        $updateDirect[$item->order_code] = [
                            'cancel_flg' => 1
                        ];
                    }
                    if ((int)$item->product_status === PRODUCT_STATUS['CANCEL'] && $checkUpdate) {
                        $updateOTS[$keyOTS] = [
                            'key' => [
                                'order_code'     => $item->ots_order_code,
                                'edi_order_code' => $item->edi_order_code,
                            ],
                            'value' => ['is_cancel' => 1],
                        ];
                    }
                }
            }
        }
        if (count($productType1) > 0) {
            $resType1    = $modelOPD->getDataUpdateOrderringNum($productType1);
            $resTypeEdi1 = $modelOTS->getOrderDetailEDI($productType1);
            foreach ($resType1 as $item) {
                $productCode = $item->product_code;
                if (!empty($item->child_product_code)) {
                    $productCode = $item->child_product_code;
                }
                if ($item->product_status === PRODUCT_STATUS['WAIT_TO_SHIP']  && $item->order_type !== 3) {
                    $updateDeliNum[$productCode] = $item->received_order_num;
                }
                $updateZanNum[$productCode] = $item->received_order_num;
            }
            foreach ($resTypeEdi1 as $itemEdi) {
                $keyOTS = $item->order_code . $item->ots_order_code;
                $arrProCheck[$item->product_code] = $item;
                // $updateOTS[$keyOTS] = [
                //     'key' => [
                //         'order_code' => $itemEdi->ots_order_code,
                //     ],
                //     'value' => ['is_cancel' => 1],
                // ];
            }
        }

        if (count($arrProCheck) !== 0 && !$check) {
            return [
                'not_cancel' => $arrProCheck
            ];
        }

        $arrChange = array();
        $upStatus = array();
        $mstPSet = new MstProductSet();
        if (count($arrUpQuan) !== 0) {
            foreach ($arrUpQuan as $item) {
                $key = strtoupper($item['product_code']);
                $cal = $item['new_quantity'] - $item['quantity'];
                $new = $item['new_quantity'];

                if ($item['is_child'] === 0) {
                    $arrStock = $modelSS->getDataByKey($item['product_code']);
                } else {
                    $arrStock = $modelSS->getDataByKey($item['parent_product_code'], $item['product_code']);
                    if (!empty($arrStock) && !empty($arrStock->component_num)) {
                        $cal = $cal * $arrStock->component_num;
                        $new = $new * $arrStock->component_num;
                    }
                }
                $temp = [
                    'key' => [
                        'receive_id'      => $item['receive_id'],
                        'detail_line_num' => $item['detail_line_num'],
                        'sub_line_num'    => $item['sub_line_num'],
                    ],
                    'update' => [
                        'received_order_num' => $new,
                    ]
                ];
                if ($item['delivery_type'] === 1) {
                    if ($item['product_status'] === PRODUCT_STATUS['WAIT_TO_SHIP'] && $cal > 0) {
                        $calStock = $arrStock->nanko_num - $arrStock->order_zan_num;

                        if ($calStock > $cal) {
                            $arrChange[$key]['wait_delivery_num'] = $arrStock->wait_delivery_num + $cal;
                        } else {
                            $arrChange[$key]['wait_delivery_num'] = $arrStock->wait_delivery_num -
                                                                    $item['quantity'];
                            $temp['update']['product_status'] = PRODUCT_STATUS['WAIT_TO_ORDER'];
                            if (!empty($item['order_code'])) {
                                $temp['update']['order_code'] = null;
                            }
                        }
                        $arrChange[$key]['order_zan_num'] = $arrStock->order_zan_num + $cal;
                    } elseif ($item['product_status'] === PRODUCT_STATUS['WAIT_TO_SHIP'] && $cal < 0) {
                        $arrChange[$key]['wait_delivery_num'] = $arrStock->wait_delivery_num + $cal;
                        $arrChange[$key]['order_zan_num']     = $arrStock->order_zan_num + $cal;
                    } else {
                        $arrChange[$key]['order_zan_num'] = $arrStock->order_zan_num + $cal;
                        if (!empty($item['order_code'])) {
                            $updateOTS[] = [
                                'key' => [
                                    'order_code'     => $item['order_code'],
                                ],
                                'value' => ['order_remain_num' => DB::raw("order_remain_num - {$item['quantity']}")],
                            ];
                        }
                        $temp['update']['order_code'] = null;
                        $temp['update']['product_status'] = PRODUCT_STATUS['WAIT_TO_ORDER'];
                    }
                } elseif (in_array($item['delivery_type'], [2, 3])) {
                    $temp['update']['product_status'] = PRODUCT_STATUS['WAIT_TO_ORDER'];
                    if ($item['delivery_type'] === 3) {
                        $arrChange[$key]['order_zan_num'] = $arrStock->order_zan_num + $cal;
                    }
                }
                $arrChange['dt_order_product_detail'][] = $temp;
            }
        }
        return [
            'detail'         => $updateDetail,
            'direct'         => $updateDirect,
            'deli'           => $updateDeliNum,
            'zan'            => $updateZanNum,
            'change'         => $arrChange,
            'is_change'      => $isChange,
            'is_cancel'      => $isCanel,
            'order_supplier' => $updateOTS,
        ];
    }
    /**
     * Process add new product have error
     *
     * @param  array  $newProducts
     * @param  object $mstOrder
     * @param  object $mstCustomer
     * @return void
     */
    private function processAddNewProductError($newProducts, $mstOrder, $mstCustomer, $receiverId)
    {
        $mstOrderDetail      = new MstOrderDetail();
        $maxOD           = $mstOrderDetail->getMaxItem($mstOrder->receive_id, $receiverId);
        $maxDLN          = $mstOrderDetail->where('receive_id', $mstOrder->receive_id)->max('detail_line_num');
        $dataOrderDetail = array();
        foreach ($newProducts as $product) {
            $maxDLN++;
            $dataOrderDetail[$mstOrder->receive_id . '_' . $maxDLN] = [
                'receive_id'          => $mstOrder->receive_id,
                'detail_line_num'     => ($maxDLN),
                'product_code'        => $product['product_code'],
                'product_name'        => $product['product_name'],
                'price'               => $product['price'],
                'quantity'            => $product['quantity'],
                'receiver_id'         => $maxOD->receiver_id,
                'ship_date'           => $maxOD->ship_date,
                'delivery_person_id'  => $maxOD->delivery_person_id,
                'package_ship_number' => $maxOD->package_ship_number,
                'time_wish_ship'      => $maxOD->time_wish_ship,
                'in_ope_cd'           => Auth::user()->tantou_code,
                'up_ope_cd'           => Auth::user()->tantou_code
            ];
        }
        $mstOrderDetail->insert($dataOrderDetail);
    }
    /**
    * Update flag mall update.
    *
    * @param Request $request
    * @return json    $object
    */
    public function updateFlgMall(Request $request)
    {
        if ($request->ajax() === true) {
            $modelO = new MstOrder();
            $receiveId = $request->input('receive_id', null);
            $dataOrder = $modelO->find($receiveId);
            $arrUpdate = [];
            if ($dataOrder->is_mall_cancel === 0
                && $dataOrder->order_status === ORDER_STATUS['CANCEL']
                && in_array($dataOrder->mall_id, [4, 5])
            ) {
                $arrUpdate = [
                    'is_mall_update' => 0,
                    'is_mall_cancel' => 1
                ];
            } else {
                $arrUpdate = [
                    'is_mall_update' => 0,
                ];
            }
            $arrUpdate['up_ope_cd'] = Auth::user()->tantou_code;
            $arrUpdate['up_date']   = now();
            $arrKey = ['receive_id' => $receiveId];
            $modelO->updateData($arrKey, $arrUpdate);
            return response()->json([
                'status'  => 1
            ]);
        }
    }
    /**
    * Update status order.
    *
    * @param Request $request
    * @return json    $object
    */
    public function updateCancelOrder(Request $request)
    {
        if ($request->ajax() === true) {
            $modelO                 = new MstOrder();
            $dtOrderProductDetail   = new DtOrderProductDetail();
            $modelCancelReason      = new MstCancelReason();
            $dtOrderUpdateLog       = new DtOrderUpdateLog();
            $common                 = new Common();
            $reasonId = $request->input('reason_id', 0);
            $receiveId = $request->input('receive_id', null);
            try {
                DB::beginTransaction();
                $dataOrder = $modelO->sharedLock()->find($receiveId);
                $dataCancelReason = $modelCancelReason->where($reasonId)
                        ->first(['reason_content']);
                $dataOrderProductDetail = $dtOrderProductDetail->where(['receive_id' => (int)$receiveId])
                        ->get(['product_status']);
                $reasonContent = '';
                if (count($dataCancelReason) > 0) {
                    $reasonContent = $dataCancelReason->reason_content;
                }
                $arrKey = ['receive_id' => $receiveId];
                $common->cancelOrderEdi($receiveId);
                $dataProcess = $dtOrderProductDetail->getDataUpdateStock($receiveId);
                $this->processUpdateZan($dataProcess);
                $arrUpdate = [
                    'is_confirmed'          => 2,
                    'ship_charge'           => 0,
                    'pay_after_charge'      => 0,
                    'total_price'           => 0,
                    'pay_charge_discount'   => 0,
                    'used_point'            => 0,
                    'used_coupon'           => 0,
                    'pay_charge'            => 0,
                    'pay_charge_discount'   => 0,
                    'request_price'         => 0,
                    'goods_price'           => 0,
                    'pay_after_charge'      => 0,
                    'order_status'          => ORDER_STATUS['CANCEL'],
                    'order_sub_status'      => ORDER_SUB_STATUS['DONE'],
                    'cancel_reason'         => $reasonId['reason_id'],
                    'is_mall_update'        => 0,
                    'is_mail_sent'          => 1,
                    'in_ope_cd'             => Auth::user()->tantou_code,
                    'err_text'              => '',
                ];

                if (!empty($receiveId) && !empty($dataCancelReason)) {
                    $modelO->updateData($arrKey, $arrUpdate);
                    $modelOD = new MstOrderDetail();
                    $modelOD->updateDataByKey(
                        ['receive_id' => (int)$receiveId],
                        [
                            'price'    => 0,
                            'quantity' => 0,
                        ]
                    );
                    if (in_array($dataOrder->payment_method, [2, 6, 7])) {
                        if ($dataOrder->payment_status === 1) {
                            $isAddCode = 0;
                            if (in_array($reasonId['reason_id'], [1, 3, 8])) {
                                if ((int)$request->input('check_show_repay_cancel_popup', 0) === 1) {
                                    DB::rollback();
                                    return response()->json([
                                        'status' => 5
                                    ]);
                                }
                                $repayType = (int)$request->input('repay_type', 1);
                                if ($repayType === 1) {
                                    $isAddCode = 1;
                                }
                            } elseif (in_array($reasonId['reason_id'], [4, 5, 6, 7, 9, 10, 11])) {
                                $isAddCode = 1;
                            }
                            $this->processRePaymentCancel($dataOrder, $dataOrder->request_price, $isAddCode);
                        }
                    }
                    if ((int)$request->input('check_show_repay_cancel_popup', 0) === 1) {
                        DB::rollback();
                        return response()->json([
                            'status' => 6
                        ]);
                    }
                    if (count($dataOrderProductDetail) > 0) {
                        $dtOrderProductDetail->updateData(
                            ['receive_id' => (int)$receiveId],
                            ['product_status' => PRODUCT_STATUS['CANCEL']]
                        );
                    }

                    $btnName = __('messages.btn_cancel_memo');
                    $dataLog[] = [
                        'receive_id' => $receiveId,
                        'log_title'  => __('messages.change_order_info'),
                        'log_contents' =>
                            "【Button {$btnName}】 → Clicked (Cancel reason)",
                        'log_status'   => 1,
                        'in_ope_cd'    => Auth::user()->tantou_code,
                        'up_ope_cd'    => Auth::user()->tantou_code
                    ];

                    $columnName      = __('messages.reason_content');
                    $dataLog[] = [
                        'receive_id' => $receiveId,
                        'log_title'  => __('messages.change_order_info'),
                        'log_contents' =>
                            "【{$columnName}】 → {$reasonContent}",
                        'log_status'   => 1,
                        'in_ope_cd'    => Auth::user()->tantou_code,
                        'up_ope_cd'    => Auth::user()->tantou_code
                    ];
                    $dtOrderUpdateLog->insert($dataLog);
                    DB::commit();
                    return response()->json([
                        'status'  => 1
                    ]);
                }
            } catch (\Exception $ex) {
                DB::rollback();
                return response()->json([
                    'status'  => -1
                ]);
            }
        }
    }
    /**
    * Update delivery state.
    *
    * @param Request $request
    * @return json    $object
    */
    public function updateDelivery(Request $request)
    {
        if ($request->ajax() === true) {
            $dtOrderProductDetail = new DtOrderProductDetail();
            $mstStockStatus       = new MstStockStatus();
            $mstOrderDetail       = new MstOrderDetail();
            $tOrder               = new TOrder();
            $tOrderDetail         = new TOrderDetail();
            $tOrderDirect         = new TOrderDirect();
            $mstOrder             = new MstOrder();
            $modelOTS             = new DtOrderToSupplier();
            $params               = $request->all();
            if ($params['name'] === 'new_detail') {
                $mstOrderDetail->updateData(
                    $params['receive_id'],
                    $params['detail_line_num'],
                    ['specify_deli_type' => 2]
                );
            } elseif ($params['name'] === 'new_direct') {
                $mstOrderDetail->updateData(
                    $params['receive_id'],
                    $params['detail_line_num'],
                    ['specify_deli_type' => 3]
                );
            } elseif ($params['name'] === 'detail') {
                $arrKey = [
                    'receive_id'        => $params['receive_id'],
                    'detail_line_num'   => $params['detail_line_num']
                ];
                $arrUpdate = [
                    'delivery_type'         => 2,
                    'stock_status'          => 0,
                    'order_code'            => null,
                    'order_num'             => 0,
                    'product_status'        => PRODUCT_STATUS['WAIT_TO_ORDER']
                ];
                $dtOrderProductDetail->updateData($arrKey, $arrUpdate);
                $deliveryType = $params['delivery_type'];
                $dataOrder    = $mstOrder->find($params['receive_id']);
                // var_dump($params);die;
                if (!empty($params['child_data'])) {
                    foreach ($params['child_data'] as $value) {
                        if (in_array($deliveryType, [1, 3])) {
                            $mstStockStatus->where(['product_code'  => $value['child_product_code']])
                                           ->decrement('order_zan_num', $value['received_order_num']);
                        }
                        if (!is_null($value['child_edi_order_code'])) {
                            $arrKeyO = ['order_code' => $value['child_edi_order_code']];
                            $arrUpO  = [
                                'cancel_flg' => 1,
                                'm_order_type_id' => DB::raw(
                                    'CASE WHEN m_order_type_id = 2 THEN 13 ' .
                                    'WHEN m_order_type_id = 9 THEN 14 ' .
                                    'ELSE m_order_type_id END'
                                ),
                            ];
                            $tOrder->updateData($arrKeyO, ['cancel_flg' => 1]);
                            $tOrderDetail->updateData($arrKeyO, $arrUpO);
                            $modelOTS->updateData(
                                [
                                    'order_code'     => $value['child_order_code'],
                                    'edi_order_code' => $value['child_edi_order_code']
                                ],
                                ['is_cancel' => 1]
                            );
                        }
                    }
                } else {
                    if (in_array($deliveryType, [1, 3])) {
                        $mstStockStatus->where(['product_code'  => $params['product_code']])
                                       ->decrement('order_zan_num', $params['received_order_num']);
                    }
                    if (!is_null($params['edi_order_code'])) {
                        $arrKeyO = ['order_code' => $params['edi_order_code']];
                        $arrUpO  = [
                            'cancel_flg' => 1,
                            'm_order_type_id' => DB::raw(
                                'CASE WHEN m_order_type_id = 2 THEN 13 ' .
                                'WHEN m_order_type_id = 9 THEN 14 ' .
                                'ELSE m_order_type_id END'
                            ),
                        ];
                        $tOrder->updateData($arrKeyO, ['cancel_flg' => 1]);
                        $tOrderDetail->updateData($arrKeyO, $arrUpO);
                        $modelOTS->updateData(
                            [
                                'order_code'     => $params['order_code'],
                                'edi_order_code' => $params['edi_order_code']
                            ],
                            ['is_cancel' => 1]
                        );
                    }
                }
                if ($dataOrder->order_status === 5) {
                    $mstOrder->updateData(
                        ['receive_id' => $params['receive_id']],
                        ['order_sub_status' => 0]
                    );
                }
            } elseif ($params['name'] === 'direct') {
                $arrKey = [
                    'receive_id'        => $params['receive_id'],
                    'detail_line_num'   => $params['detail_line_num']
                ];
                $arrUpdate = [
                    'delivery_type'         => 3,
                    'stock_status'          => 0,
                    'order_code'            => null,
                    'order_num'             => 0,
                    'product_status'        => PRODUCT_STATUS['WAIT_TO_ORDER']
                ];
                $dtOrderProductDetail->updateData($arrKey, $arrUpdate);
                if (!empty($params['child_data'])) {
                    foreach ($params['child_data'] as $value) {
                        if (!is_null($value['child_edi_order_code'])) {
                            $arrKeyO = ['order_code' => $value['child_edi_order_code']];
                            $arrUpO  = [
                                'cancel_flg' => 1,
                                'm_order_type_id' => DB::raw(
                                    'CASE WHEN m_order_type_id = 2 THEN 15 ' .
                                    'WHEN m_order_type_id = 9 THEN 16 ' .
                                    'ELSE m_order_type_id END'
                                ),
                            ];
                            $tOrderDirect->updateData($arrKeyO, $arrUpO);
                            $modelOTS->updateData(
                                [
                                    'order_code'     => $value['child_order_code'],
                                    'edi_order_code' => $value['child_edi_order_code']
                                ],
                                ['is_cancel' => 1]
                            );
                        }
                    }
                } else {
                    if (!is_null($params['edi_order_code'])) {
                        $arrKeyO = ['order_code' => $params['edi_order_code']];
                        $arrUpO  = [
                            'cancel_flg' => 1,
                            'm_order_type_id' => DB::raw(
                                'CASE WHEN m_order_type_id = 2 THEN 15 ' .
                                'WHEN m_order_type_id = 9 THEN 16 ' .
                                'ELSE m_order_type_id END'
                            ),
                        ];
                        $tOrderDirect->updateData($arrKeyO, $arrUpO);
                        $modelOTS->updateData(
                            [
                                'order_code'     => $params['order_code'],
                                'edi_order_code' => $params['edi_order_code']
                            ],
                            ['is_cancel' => 1]
                        );
                    }
                }
            }
            return response()->json([
                'status'  => 1
            ]);
        }
    }
    /**
    * Re-calculate price order.
    *
    * @param Request $request
    * @return json    $object
    */
    public function reCalculate(Request $request)
    {
        if ($request->ajax() === true) {
            $dtOrderUpdateLog = new DtOrderUpdateLog();
            $modelO         = new MstOrder();
            $modelOD        = new MstOrderDetail();
            $receiveId      = $request->input('receive_id', null);
            $dataOrders     = $modelO->getDataReCalculate($receiveId);
            if ($dataOrders->count() === 0) {
                return response()->json([
                    'status'  => 0,
                    'message' => 'Data not found'
                ]);
            }
            $arrGPD = [];
            $dataNewOrders = [];
            foreach ($dataOrders as $data) {
                if (!isset($arrGPD[$data->receiver_id])) {
                    $arrGPD[$data->receiver_id] = 0;
                }
                $arrGPD[$data->receiver_id] += $data->price * $data->quantity;
                $dataNewOrders[$data->receiver_id] = $data;
            }
            $arrSD = [];
            foreach ($dataNewOrders as $data) {
                if ($data->add_ship_charge === 1 && $data->maill_id !== 3) {
                    $arrSD[$data->receiver_id] = 5400;
                } elseif ($arrGPD[$data->receiver_id] >= $data->below_limit_price && $data->mall_id !== 3) {
                    $arrSD[$data->receiver_id] = 0;
                } elseif ($arrGPD[$data->receiver_id] < $data->below_limit_price && $data->mall_id !== 3) {
                    $arrSD[$data->receiver_id] = $data->charge_price;
                }
            }

            $dataOrder      = $dataOrders[0];
            $shipCharge     = 0;
            $payAfterCharge = 0;
            foreach ($arrSD as $sd) {
                $shipCharge += $sd;
            }
            if ($dataOrder->payment_method === 3) {
                $codFree = $modelO->getCodFee($receiveId);
                if (count($codFree) !== 0) {
                    $payAfterCharge = $codFree->cod_fee;
                }
            }
            $totalPrice = $dataOrder->goods_price + $shipCharge + $payAfterCharge + $dataOrder->pay_charge;
            $requestPrice = $totalPrice - $dataOrder->pay_charge_discount -
                            $dataOrder->discount - $dataOrder->used_point - $dataOrder->used_coupon;
            try {
                DB::beginTransaction();
                $data = $modelO->sharedLock()->find($receiveId);
                $data->ship_charge      = $shipCharge;
                $data->total_price      = $totalPrice;
                $data->pay_after_charge = $payAfterCharge;
                $data->request_price    = $requestPrice;
                $data->is_mall_update   = 1;
                $data->is_recalculate   = 0;
                $data->save();
                foreach ($arrGPD as $receiverId => $price) {
                    $modelOD->updateDataByKey(
                        [
                            'receive_id'  => $receiveId,
                            'receiver_id' => $receiverId
                        ],
                        ['goods_price_detail' => $price]
                    );
                }
                foreach ($arrSD as $receiverId => $price) {
                    $modelOD->updateDataByKey(
                        [
                            'receive_id'  => $receiveId,
                            'receiver_id' => $receiverId
                        ],
                        ['ship_charge_detail' => $price]
                    );
                }
                if ($data->is_ignore_error === 0) {
                    $dataReceivers = $modelOD->getListReceiver($receiveId);
                    foreach ($dataReceivers as $value) {
                        $this->checkInfoAfterSave($this->checkRules, $data, $value->receiver_id);
                    }
                }
                $btnName = __('messages.btn_re_calculate');
                $dataLog[] = [
                    'receive_id' => $receiveId,
                    'log_title'  => __('messages.change_order_info'),
                    'log_contents' =>
                        "【Button {$btnName}】 → Clicked",
                    'log_status'   => 1,
                    'in_ope_cd'    => Auth::user()->tantou_code,
                    'up_ope_cd'    => Auth::user()->tantou_code
                ];
                $dtOrderUpdateLog->insert($dataLog);
                DB::commit();
                return response()->json([
                    'status'  => 1
                ]);
            } catch (\Exception $e) {
                DB::rollback();
                return response()->json([
                    'status'  => -1
                ]);
            }
        }
    }
    /**
    * Re-calculate price order.
    *
    * @param object $dataOrder
    * @param int $newRequestPrice
    * @param int $oldRequestPrice
    * @return void
    */
    public function processRePayment($dataOrder, $newRequestPrice, $oldRequestPrice, $returnCP = null, $repayType = 1)
    {
        $modelRP = new DtRePayment();
        $modelRP->repay_price = $oldRequestPrice - $newRequestPrice;
        if ($dataOrder->payment_method === 1 && $dataOrder->mall_id !== 3) {
            $modelRP->repay_bank_code        = null;
            $modelRP->repay_bank_name        = 'クレカ返金';
            $modelRP->repay_bank_branch_code = null;
            $modelRP->repay_bank_branch_name = 'クレカ返金';
            $modelRP->repay_name             = 'クレカ返金';
            $modelRP->repay_bank_account     = 'クレカ返金';
        } elseif ($dataOrder->mall_id === 3) {
            $modelRP->repay_bank_code        = null;
            $modelRP->repay_bank_name        = 'アマゾン';
            $modelRP->repay_bank_branch_code = null;
            $modelRP->repay_bank_branch_name = 'アマゾン';
            $modelRP->repay_name             = 'アマゾン';
            $modelRP->repay_bank_account     = 'アマゾン';
        }
        $modelRP->receive_id       = $dataOrder->receive_id;
        $modelRP->return_no        = null;
        $modelRP->repayment_time   = 0;
        $modelRP->payment_id       = null;
        $modelRP->repayment_status = 0;
        $modelRP->repayment_type   = ($repayType === 1)?1:0;
        $modelRP->deposit_transfer_commission = 0;
        if ($returnCP !== null) {
            $modelRP->return_coupon      = $returnCP['return_coupon'];
            $modelRP->return_point       = $returnCP['return_point'];
        }
        if ($repayType === 1) {
            $modelRP->return_fee = 300;
            $modelRP->total_return_price = $modelRP->repay_price - $modelRP->return_fee;
        } else {
            $modelRP->return_fee = 0;
            $modelRP->total_return_price = $modelRP->repay_price + $modelRP->deposit_transfer_commission;
        }
        $modelRP->is_return        = 2;
        $modelRP->repayment_date   = null;
        $modelRP->in_ope_cd        = Auth::user()->tantou_code;
        $modelRP->in_date          = now();
        $modelRP->up_ope_cd        = Auth::user()->tantou_code;
        $modelRP->up_date          = now();
        $modelRP->save();
    }
    /**
     * Get form data for order editing
     *
     * @param  Request $request
     * @return json
     */
    public function getFormNewOrderData(Request $request)
    {
        $params = $request->all();
        if ($request->ajax() === true) {
            if (isset($params['receive_id'])) {
                $rules = [
                    'receive_id'    => 'required|numeric|exists:horunba.mst_order,receive_id'
                ];
                $validator = Validator::make($request->all(), $rules);
                if ($validator->fails()) {
                    return response()->json([
                        'data'      => [],
                        'status'    => 0,
                        'method'    => 'save',
                        'messages'  => $validator->errors()
                    ]);
                }
            }
            $mstOrder            = new MstOrder();
            $mstOrderDetail      = new MstOrderDetail();
            $mstCustomer         = new MstCustomer();
            $mstSettlementManage = new MstSettlementManage();
            $mstShipping         = new MstShippingCompany();
            $mstShippingFee      = new MstShippingFee();
            $data = [];
            $paymentOpt[] = ['key' => '', 'value' => '------'];
            $dataInfoCustomer = $mstCustomer->getInfoCustomer($params);
            $arrAddress1 =[];
            $arrAddress2 =[];
            $newArray = [];
            $isTheSame = false;

            foreach ($mstSettlementManage->get(['payment_code', 'payment_name']) as $item) {
                $paymentOpt[] = ['key' => $item['payment_code'], 'value' => $item['payment_name']];
            }
            $deliveryOpt[] = ['key' => '', 'value' => '------'];
            foreach ($mstShipping->get(['company_id', 'company_name']) as $item) {
                $deliveryOpt[] = ['key' => $item['company_id'], 'value' => $item['company_name']];
            }
            $shipWishDateOpt[] = ['key' => '', 'value' => '------'];
            foreach (Config::get('common.ship_wish_time') as $key => $val) {
                $shipWishDateOpt[] = ['key' => $key, 'value' => $val];
            }
            $orderDep[] = ['key' => '', 'value' => '------'];
            foreach (Config::get('common.order_dep') as $key => $val) {
                $orderDep[] = ['key' => $key, 'value' => $val];
            }
            $dataShippingFee = $mstShippingFee->getItemByMall(7);
            if (isset($params['receive_id'])) {
                $data       = $mstOrder->getDataByReceiveId($params['receive_id']);
                $dataDetail = $mstOrderDetail->getDataByReceiveId($params['receive_id']);
                if (count($dataDetail) > 0) {
                    $data['list_products'] = $dataDetail;
                }
                if (!empty($dataInfoCustomer)) {
                    $arrAddress1 = [
                        'last_name'     => $dataInfoCustomer['last_name'],
                        'tel_num'       => $dataInfoCustomer['tel_num'],
                        'zip_code'      => $dataInfoCustomer['zip_code'],
                        'prefecture'    => $dataInfoCustomer['prefecture'],
                        'city'          => $dataInfoCustomer['city'],
                        'sub_address'   => $dataInfoCustomer['sub_address']
                    ];
                    $arrAddress2 = [
                        'reciever_name' => $dataInfoCustomer['reciever_name'],
                        're_tel'        => $dataInfoCustomer['re_tel'],
                        're_postal'     => $dataInfoCustomer['re_postal'],
                        're_province'   => $dataInfoCustomer['re_province'],
                        're_address_1'  => $dataInfoCustomer['re_address_1'],
                        're_address_2'  => $dataInfoCustomer['re_address_2']
                    ];
                    $newArray = array_diff($arrAddress1, $arrAddress2);
                    $data['company_name']    = $dataInfoCustomer['company_name'];
                    $data['fax_num']         = $dataInfoCustomer['fax_num'];
                    $data['email']           = $dataInfoCustomer['email'];
                    if (count($newArray) === 0) {
                        $isTheSame = true;
                    }
                }
                if ($isTheSame === true) {
                    $arrAddress1['is_the_same']     = true;
                    $data = array_merge($data->toArray(), $arrAddress1);
                } else {
                    $arrAddress1['is_the_same'] = false;
                    $data = array_merge($data->toArray(), $arrAddress1, $arrAddress2);
                }
            }
            return response()->json([
                'data'              => $data,
                'paymentOpt'        => $paymentOpt,
                'deliveryOpt'       => $deliveryOpt,
                'dataShippingFee'   => $dataShippingFee ? $dataShippingFee : [],
                'shipWishDateOpt'   => $shipWishDateOpt,
                'orderDep'          => $orderDep
            ]);
        }
    }

    /**
    * Add new order
    *
    * @param object $dataOrder
    * @return json
    */
    public function newOrder(Request $request)
    {
        $mstOrderDetail         = new MstOrderDetail();
        $mstCustomer = new MstCustomer();
        $mstOrder    = new MstOrder();
        $params  = $request->all();
        if ($request->ajax() === true) {
            $isTheSame     = (int)$request->input('is_the_same', 1);
            $rules = [
                're_first_name'     => 'required',
                're_last_name'      => 'required',
                're_postal'         => 'required',
                're_province'       => 'required',
                're_address_1'      => 'required',
                're_tel'            => 'required',
                'first_name'        => 'required',
                'last_name'         => 'required',
                'zip_code'          => 'required|numeric',
                'prefecture'        => 'required',
                'city'              => 'required',
                'tel_num'           => 'required',
                'email'             => 'required|email',
                'payment_method'    => 'required|numeric',
                'delivery_method'   => 'required|numeric',
                'list_products.*.product_code'   => 'required|exists:horunba.mst_product,product_code',
                'list_products.*.price'          => 'required|numeric|min:0',
                'list_products.*.quantity'       => 'required|numeric|min:1',
            ];
            $typeOrder = (int)$request->input('type_order', 1);
            if ($typeOrder === 2) {
                $rules['deparment'] = 'required|in:' . implode(',', array_keys(Config::get('common.order_dep')));
            }
            if ($isTheSame === 1) {
                unset($rules['re_first_name']);
                unset($rules['re_last_name']);
                unset($rules['re_postal']);
                unset($rules['re_province']);
                unset($rules['re_address_1']);
                unset($rules['re_address_2']);
                unset($rules['re_tel']);
            }
            if (isset($params['receive_id'])) {
                $rules['receive_id'] =   'required|numeric|exists:horunba.mst_order,receive_id';
            }
            $validator = Validator::make($request->all(), $rules);
            if ($validator->fails()) {
                $errors = $validator->errors();
                return response()->json([
                    'data'      => [],
                    'status'    => 0,
                    'method'    => 'save',
                    'messages'  => $validator->errors()
                ]);
            }
            try {
                DB::beginTransaction();
                // Insert & update customer
                $arrShipCus['first_name']      = (string)$request->input('first_name');
                $arrShipCus['last_name']       = (string)$request->input('last_name');
                $arrShipCus['first_name_kana'] = '';
                $arrShipCus['last_name_kana']  = '';
                $arrShipCus['email']           = (string)$request->input('email');
                $arrShipCus['tel_num']         = str_replace('-', '', (string)$request->input('tel_num'));
                $arrShipCus['fax_num']         = (string)$request->input('fax_num');
                $arrShipCus['zip_code']        = str_replace('-', '', (string)$request->input('zip_code'));
                $arrShipCus['prefecture']      = (string)$request->input('prefecture');
                $arrShipCus['city']            = (string)$request->input('city');
                $arrShipCus['sub_address']     = (string)$request->input('sub_address');
                $arrShipCus['sex']             = '';
                $arrShipCus['urgent_tel_num']  = '';
                $arrShipCus['is_black_list']   = 0;
                $arrShipCus['in_ope_cd']       = Auth::user()->tantou_code;
                $arrShipCus['in_date']         = date('y-m-d h:i:s');
                $arrShipCus['up_ope_cd']       = Auth::user()->tantou_code;
                $arrShipCus['up_date']         = date('y-m-d h:i:s');
                $checkCustomer = $mstCustomer->checkCustomerExist($arrShipCus);
                $customerId    = 0;
                $receiverId    = 0;
                // Check & insert & update customer's info
                if (count($checkCustomer) === 0) {
                    $customerId = $mstCustomer->insertGetId($arrShipCus);
                } else {
                    $customerId = $checkCustomer->customer_id;
                    unset($arrShipCus['in_ope_cd']);
                    unset($arrShipCus['in_date']);
                    $mstCustomer->where(['customer_id' => $customerId])->update($arrShipCus);
                };
                // Check & insert & update receiver's info
                if ($isTheSame === 0) {
                    $arrShipReceiver['last_name']       = (string)$request->input('re_last_name');
                    $arrShipReceiver['first_name']       = (string)$request->input('re_first_name');
                    $arrShipReceiver['tel_num']         = str_replace('-', '', (string)$request->input('re_tel'));
                    $arrShipReceiver['zip_code']        = str_replace('-', '', (string)$request->input('re_postal'));
                    $arrShipReceiver['prefecture']      = (string)$request->input('re_province');
                    $arrShipReceiver['city']            = (string)$request->input('re_address_1');
                    $arrShipReceiver['sub_address']     = (string)$request->input('re_address_2');
                    $checkReceiver = $mstCustomer->checkCustomerExist($arrShipReceiver);
                    if (count($checkReceiver) === 0) {
                        $arrShipReceiver['in_ope_cd']       = Auth::user()->tantou_code;
                        $arrShipReceiver['in_date']         = date('y-m-d h:i:s');
                        $arrShipReceiver['up_ope_cd']       = Auth::user()->tantou_code;
                        $arrShipReceiver['up_date']         = date('y-m-d h:i:s');
                        $receiverId = $mstCustomer->insertGetId($arrShipReceiver);
                    } else {
                        $receiverId = $checkReceiver->customer_id;
                        $arrShipReceiver['up_ope_cd']       = Auth::user()->tantou_code;
                        $arrShipReceiver['up_date']         = date('y-m-d h:i:s');
                        $mstCustomer->where(['customer_id' => $receiverId])->update($arrShipReceiver);
                    };
                }
                // Insert & update order
                if ($typeOrder === 2) {
                    $orderMaxId = $mstOrder->getMaxItemForRefundDetail('DEP-' . date('Ymd'));
                    $preTypeOrder = "DEP-";
                } else {
                    $orderMaxId = $mstOrder->getMaxItemForRefundDetail('FAX-' . date('Ymd'));
                    $preTypeOrder = "FAX-";
                }
                if (!empty($orderMaxId)) {
                    $reOrderId  = last(explode('-', $orderMaxId->received_order_id));
                    $reOrderId  = filter_var($reOrderId, FILTER_SANITIZE_NUMBER_INT);
                    $reOrderId  = $preTypeOrder . date('Ymd') . sprintf("-%04d", (int)$reOrderId + 1);
                } else {
                    $reOrderId  = $preTypeOrder . date('Ymd') . "-0001";
                }
                $mallId = 7;
                if ($typeOrder === 2) {
                    $mallId = 11;
                }
                $arrDataOrder = [];
                $arrDataOrder['received_order_id']      = $reOrderId;
                $arrDataOrder['mall_id']                = $mallId;
                $arrDataOrder['customer_id']            = $customerId;
                $arrDataOrder['used_coupon']            = $request->input('used_coupon', 0);
                $arrDataOrder['used_point']             = $request->input('used_point', 0);
                $arrDataOrder['discount']               = (int)$request->input('discount', 0);
                $arrDataOrder['total_price']            = (int)$request->input('total_price', 0);
                $arrDataOrder['request_price']          = (int)$request->input('request_price', 0);
                $arrDataOrder['order_date']             = $request->input('order_date', date('Y:m:d H:i:s'));
                $arrDataOrder['ship_charge']            = (int)$request->input('ship_charge', 0);
                $arrDataOrder['pay_charge']             = (int)$request->input('pay_charge', 0);
                $arrDataOrder['pay_after_charge']       = (int)$request->input('pay_after_charge', 0);
                $arrDataOrder['pay_charge_discount']    = (int)$request->input('pay_charge_discount', 0);
                $arrDataOrder['goods_price']            = (int)$request->input('goods_price', 0);
                $arrDataOrder['goods_tax']              = 0;
                $arrDataOrder['arrive_type']            = (int)$request->input('quantity', 0);
                $arrDataOrder['is_multi_ship_address']  = 0;
                $arrDataOrder['ship_wish_time']         = (string)$request->input('ship_wish_time', null);
                $arrDataOrder['ship_wish_date']         = (string)$request->input('ship_wish_date', null);
                $arrDataOrder['payment_status']         = 0;
                $arrDataOrder['payment_method']         = (int)$request->input('payment_method', null);
                $arrDataOrder['delivery_method']        = (int)$request->input('delivery_method', null);
                $arrDataOrder['payment_request_date']   = null;
                $arrDataOrder['payment_price']          = 0;
                $arrDataOrder['payment_account']        = '';
                $arrDataOrder['payment_date']           = null;
                $arrDataOrder['payment_confirm_date']   = null;
                $arrDataOrder['customer_question']      = $request->input('customer_question', null);
                $arrDataOrder['shop_answer']            = null;
                $arrDataOrder['cancel_reason']          = null;
                $arrDataOrder['company_name']           = $request->input('company_name', null);
                $arrDataOrder['is_delay']               = $request->input('is_delay', 0);
                $arrDataOrder['delay_priod']            = null;
                $arrDataOrder['delay_reason']           = null;
                $arrDataOrder['mail_seri']              = null;
                $arrDataOrder['order_status']           = ORDER_STATUS['NEW'];
                $arrDataOrder['order_sub_status']       = ORDER_SUB_STATUS['NEW'];
                $arrDataOrder['order_dep']              = ($typeOrder === 2)?$request->input('deparment', 0):0;
                $arrDataOrder['request_id']             = Auth::user()->tantou_code;
                $arrDataOrder['is_sourcing_on_demand']  = 0;
                $arrDataOrder['response_due_date']      = null;
                $arrDataOrder['is_mail_sent']           = 0;
                $arrDataOrder['is_mall_update']         = 0;
                $arrDataOrder['is_mall_cancel']         = 0;
                $arrDataOrder['is_recalculate']         = 0;
                $arrDataOrder['message_api']            = null;
                $arrDataOrder['error_code_api']         = null;
                $arrDataOrder['err_text']               = null;
                $arrDataOrder['in_date']                = date('Y:m:d H:i:s');
                $arrDataOrder['in_ope_cd']              = Auth::user()->tantou_code;
                $arrDataOrder['up_date']                = date('Y:m:d H:i:s');
                $arrDataOrder['up_ope_cd']              = Auth::user()->tantou_code;
                if (isset($params['receive_id'])) {
                    unset($arrDataOrder['received_order_id']);
                    unset($arrDataOrder['in_date']);
                    unset($arrDataOrder['in_ope_cd']);
                    $receiveId = (int)$params['receive_id'];
                    $mstOrder->updateData(['receive_id' => $receiveId], $arrDataOrder);
                } else {
                    $receiveId = $mstOrder->insertGetId($arrDataOrder);
                }
                // Insert & update & delete order detail
                if (!empty($params['list_dels'])) {
                    $detaiLineNum = array_column($params['list_dels'], 'detail_line_num');
                    $mstOrderDetail->deleteData(
                        [
                            'detail_line_num' => $detaiLineNum,
                            'receive_id' => (int)$params['receive_id']
                        ]
                    );
                }
                $dataListProducts = $request->input('list_products', null);
                $detailLineNum  = 0;
                $hasNum = 0;
                foreach ($dataListProducts as $key => $value) {
                    $detailLineNum++;
                    $arrOrderDetail['product_code'] = (string) $value['product_code'];
                    $arrOrderDetail['price'] = $value['price'];
                    $arrOrderDetail['receiver_id'] = $isTheSame === 0 ? $receiverId : $customerId;
                    $arrOrderDetail['quantity'] = (int) $value['quantity'];

                    if (!isset($value['new_product'])) {
                        $arrOrderDetail2['up_ope_cd']    = Auth::user()->tantou_code;
                        $arrOrderDetail2['up_date']      = date('Y:m:d H:i:s');
                        $arrOrderDetail2['price']        = $value['price'];
                        $arrOrderDetail2['quantity']     = $value['quantity'];
                        $arrOrderDetail2['product_code'] = $value['product_code'];
                        $mstOrderDetail->updateData(
                            (int)$value['receive_id'],
                            (int)$value['detail_line_num'],
                            $arrOrderDetail2
                        );
                    } else {
                        if ((int)$value['new_product'] === 1) {
                            if (isset($params['receive_id'])) {
                                $dataLineNum = $mstOrderDetail->getMaxItem((int)$params['receive_id']);
                                $detailLineNum = $dataLineNum->detail_line_num + 1;
                            }
                            $arrOrderDetail['detail_line_num'] = (int) $detailLineNum;
                            $arrOrderDetail['product_name'] = (string) $value['product_name'];
                            $arrOrderDetail['receive_id'] = (int) $receiveId;
                            $arrOrderDetail['return_quantity'] = 0;
                            $arrOrderDetail['item_tax'] = 0;
                            $arrOrderDetail['ship_price'] = 0;
                            $arrOrderDetail['ship_tax'] = 0;
                            $arrOrderDetail['ship_date'] = null;
                            $arrOrderDetail['delivery_person_id'] = null;
                            $arrOrderDetail['package_ship_number'] = null;
                            $arrOrderDetail['time_wish_ship'] = null;
                            $arrOrderDetail['in_ope_cd'] = Auth::user()->tantou_code;
                            $arrOrderDetail['in_date'] = date('Y:m:d H:i:s');
                            $arrOrderDetail['up_ope_cd'] = Auth::user()->tantou_code;
                            $arrOrderDetail['up_date'] = date('Y:m:d H:i:s');
                            $mstOrderDetail->insert($arrOrderDetail);
                        }
                    }
                }
                DB::commit();
            } catch (\Exception $e) {
                $this->errors[] = $e->getMessage();
                DB::rollback();
            }
            return response()->json([
                'status'            => 1,
                'method'            => 'save'
            ]);
        }
        return view('order-management.order_add');
    }

    /**
     * Get data mail by receive
     *
     * @param  String receiveOrderId
     * @return json
     */
    public function getDataMailByReceive($receiveOrderId)
    {
        $statusConf     = Config::get('common.receive_mail_status');

        $model          = new DtReceiveMailList();
        $modelS         = new DtSendMailList();
        $data           = $model->getDataByReceive($receiveOrderId);
        $dataSend       = $modelS->getDataByReceivedOrderId($receiveOrderId);
        $newData = [];
        foreach ($data as $item) {
            $tmp = $item->toArray();
            if (isset($statusConf[$tmp['status']])) {
                $tmp['status'] = $statusConf[$tmp['status']];
            }
            $tmp['type'] = 'receive_mail';
            $newData[] = $tmp;
        }
        foreach ($dataSend as $item) {
            $tmp = $item->toArray();
            if (isset($statusConf[$tmp['send_status']])) {
                $tmp['status'] = $statusConf[$tmp['send_status']];
            } else {
                $tmp['status'] = $tmp['send_status'];
            }
            $tmp['receive_date'] = $tmp['in_date'];
            $tmp['type'] = 'send_mail';
            $newData[] = $tmp;
        }
        usort($newData, function ($a, $b) {
            if (strtotime($a['receive_date']) === strtotime($b['receive_date'])) {
                return 0;
            }
            return (strtotime($a['receive_date']) < strtotime($b['receive_date'])) ? -1 : 1;
        });
        return [
            'data' => $newData
        ];
    }

    /**
    * Re-payment cancel.
    *
    * @param object $dataOrder
    * @param int $newRequestPrice
    * @param int $oldRequestPrice
    * @return void
    */
    public function processRePaymentCancel($dataOrder, $newRequestPrice, $isAddCode = 0)
    {
        $modelRP = new DtRePayment();
        $modelRP->repay_price = $newRequestPrice;
        if ($dataOrder->payment_method === 1 && $dataOrder->mall_id !== 3) {
            $modelRP->repay_bank_code        = null;
            $modelRP->repay_bank_name        = 'クレカ返金';
            $modelRP->repay_bank_branch_code = null;
            $modelRP->repay_bank_branch_name = 'クレカ返金';
            $modelRP->repay_name             = 'クレカ返金';
            $modelRP->repay_bank_account     = 'クレカ返金';
        } elseif ($dataOrder->mall_id === 3) {
            $modelRP->repay_bank_code        = null;
            $modelRP->repay_bank_name        = 'アマゾン';
            $modelRP->repay_bank_branch_code = null;
            $modelRP->repay_bank_branch_name = 'アマゾン';
            $modelRP->repay_name             = 'アマゾン';
            $modelRP->repay_bank_account     = 'アマゾン';
        }
        $modelRP->receive_id       = $dataOrder->receive_id;
        $modelRP->return_no        = null;
        $modelRP->repayment_time   = 0;
        $modelRP->payment_id       = null;
        $modelRP->repayment_status = 0;
        $modelRP->repayment_type   = ($isAddCode === 1)?1:0;
        $modelRP->return_coupon      = $dataOrder->used_coupon;
        $modelRP->return_point       = $dataOrder->used_point;
        $modelRP->deposit_transfer_commission = 0;
        if ($isAddCode === 1) {
            $modelRP->return_fee = 300;
            $modelRP->total_return_price = $modelRP->repay_price - $modelRP->return_fee;
        } else {
            $modelRP->return_fee = 0;
            $modelRP->total_return_price = $modelRP->repay_price + $modelRP->deposit_transfer_commission;
        }
        $modelRP->is_return        = 0;
        $modelRP->repayment_date   = null;
        $modelRP->in_ope_cd        = Auth::user()->tantou_code;
        $modelRP->in_date          = now();
        $modelRP->up_ope_cd        = Auth::user()->tantou_code;
        $modelRP->up_date          = now();
        $modelRP->save();
        return true;
    }

    /**
     * check cancel all order.
     *
     * @param object $request
     *
     * @return json
     */
    public function checkCancelAll(Request $request)
    {
        if ($request->ajax() === true) {
            $receiveId = $request->receive_id;
            $receiverId = $request->input('receiver_id', null);
            $modelODP = new DtOrderProductDetail();
            $modelO   = new MstOrder();
            if ($receiverId !== null) {
                if (!$modelODP->checkAllProductCancel($receiveId, $receiverId)) {
                    return response()->json([
                        'status' => 0
                    ]);
                }
            }
            $datas = $modelODP->getDataUpdateCancelEDI($receiveId);
            $dataO = $modelO->find($receiveId);
            $dataNotCancel = [];
            foreach ($datas as $data) {
                if ($data->delivery_type === 1) {
                    $dataNotCancel[$data->product_code] = $data;
                    continue;
                }
                if (!empty($data->m_order_type_id_detail)) {
                    if ($data->m_order_type_id_detail === 1) {
                        $dataNotCancel[$data->product_code] = $data;
                    // } elseif ($data->m_order_type_id_detail === 9) {
                        // $timeCmp = (time() - strtotime($dataO->order_date))/86400;
                        // if ($timeCmp <= 5) {
                            // $dataNotCancel[$data->product_code] = $data;
                        // }
                    }
                } elseif (!empty($data->m_order_type_id_direct)) {
                    if ($data->m_order_type_id_direct === 1) {
                        $dataNotCancel[$data->product_code] = $data;
                    }
                } elseif ($data->order_type === 3) {
                    $dataNotCancel[$data->product_code] = $data;
                }
            }
            return response()->json([
                'status'     => empty($dataNotCancel) ? 1 : -1,
                'not_cancel' => $dataNotCancel,
            ]);
        }
    }

    /**
     * Cancel not enought
     *
     * @param object $request
     *
     * @return json
     */
    public function cancelNotEnought(Request $request)
    {
        if ($request->ajax() === true) {
            $receiveId = $request->receive_id;
            $dtOrderUpdateLog  = new DtOrderUpdateLog();
            $modelODP          = new DtOrderProductDetail();
            $modelOD           = new MstOrderDetail();
            $modelO            = new MstOrder();
            $tOrderDetail      = new TOrderDetail();
            $tOrder            = new TOrder();
            $tOrderDirect      = new TOrderDirect();
            $DtOrderToSupplier = new DtOrderToSupplier();
            try {
                DB::beginTransaction();
                $datas = $modelODP->getDataCancelEDINotEnought($receiveId);
                $dataO = $modelO->sharedLock()->find($receiveId);
                $dataProcess = $modelODP->getDataUpdateStock($receiveId);
                $arrProduct = [];
                foreach ($datas as $data) {
                    $arrUpdate = [
                        'cancel_flg' => 1,
                    ];
                    $arrUpdateOrder = [
                        'cancel_flg' => 1,
                    ];
                    $flg = true;
                    if ($data->order_type === 3) {
                        $flg = false;
                    } elseif (!empty($data->m_order_type_id_detail) && $data->order_type !== 3) {
                        if ($data->delivery_type === 1) {
                            $flg = false;
                        } elseif ($data->m_order_type_id_detail === 1) {
                            $flg = false;
                        } elseif ($data->m_order_type_id_detail === 9) {
                            // $timeCmp = (time() - strtotime($dataO->order_date))/86400;
                            // if ($timeCmp <= 5) {
                                // $flg = false;
                            // } else {
                                $arrUpdate['m_order_type_id'] = 14;
                            // }
                        } elseif ($data->m_order_type_id_detail === 2) {
                            $arrUpdate['m_order_type_id'] = 13;
                        }
                        if ($flg) {
                            $tOrderDetail->updateData(
                                ['order_code' => $data->edi_order_code],
                                $arrUpdate
                            );
                            $tOrder->updateData(
                                ['order_code' => $data->edi_order_code],
                                $arrUpdateOrder
                            );
                        }
                    } elseif (!empty($data->m_order_type_id_direct)) {
                        if ($data->delivery_type === 1) {
                            $flg = false;
                        } elseif ($data->m_order_type_id_direct === 1) {
                            $flg = false;
                        } elseif ($data->m_order_type_id_direct === 2) {
                            $arrUpdate['m_order_type_id'] = 15;
                        } elseif ($data->m_order_type_id_direct === 9) {
                            $arrUpdate['m_order_type_id'] = 16;
                        }
                        if ($flg) {
                            $tOrderDirect->updateData(
                                ['order_code' => $data->edi_order_code],
                                $arrUpdate
                            );
                        }
                    }
                    if (!empty($data->order_code) && $flg) {
                        $DtOrderToSupplier->updateData(
                            [
                                'order_code'     => $data->order_code,
                                'edi_order_code' => $data->edi_order_code
                            ],
                            ['is_cancel' => 1]
                        );
                    }
                    $modelODP->updateData(
                        [
                            'receive_id'      => $receiveId,
                            'detail_line_num' => $data->detail_line_num,
                            'sub_line_num'    => $data->sub_line_num,
                        ],
                        ['product_status' => PRODUCT_STATUS['CANCEL']]
                    );
                    $arrOD = [
                        'price' => 0,
                        'quantity' => 0,
                    ];
                    $modelOD->updateData($receiveId, $data->detail_line_num, $arrOD);
                }
                $this->processUpdateZan($dataProcess);
                // Update price
                $dataUpdate = [
                    'is_confirmed'          => 2,
                    'order_status'          => ORDER_STATUS['CANCEL'],
                    'order_sub_status'      => ORDER_SUB_STATUS['DONE'],
                    'cancel_reason'         => $request->cancel_reason,
                    'ship_charge'           => 0,
                    'pay_charge'            => 0,
                    'pay_after_charge'      => 0,
                    'total_price'           => 0,
                    'pay_charge_discount'   => 0,
                    'discount'              => 0,
                    'used_point'            => 0,
                    'used_coupon'           => 0,
                    'request_price'         => 0,
                    'goods_tax'             => 0,
                    'goods_price'           => 0,
                    'is_mail_sent'          => 1,
                    'err_text'              => '',
                ];
                $dataKey = ['receive_id' => $receiveId];
                $modelO->updateData($dataKey, $dataUpdate);
                $modelOD->updateDataByKey($dataKey, ['price' => 0, 'quantity' => 0]);
                if (in_array($dataO->payment_method, [2, 6, 7])) {
                    if ($dataO->payment_status === 1) {
                        $isAddCode = 0;
                        if (in_array($request->cancel_reason, [1, 3, 8])) {
                            if ((int)$request->input('check_show_repay_cancel_popup', 0) === 1) {
                                DB::rollback();
                                return response()->json([
                                    'status' => 5
                                ]);
                            }
                            $repayType = (int)$request->input('repay_type', 1);
                            if ($repayType === 1) {
                                $isAddCode = 1;
                            }
                        } elseif (in_array($request->cancel_reason, [4, 5, 6, 7, 9, 10, 11])) {
                            $isAddCode = 1;
                        }
                        $this->processRePaymentCancel($dataO, $dataO->request_price, $isAddCode);
                    }
                }
                if ((int)$request->input('check_show_repay_cancel_popup', 0) === 1) {
                    DB::rollback();
                    return response()->json([
                        'status' => 6
                    ]);
                }

                $btnName = __('messages.btn_cancel_memo');
                $dataLog[] = [
                    'receive_id' => $receiveId,
                    'log_title'  => __('messages.change_order_info'),
                    'log_contents' =>
                        "【Button {$btnName}】 → Clicked (Cancel not enough)",
                    'log_status'   => 1,
                    'in_ope_cd'    => Auth::user()->tantou_code,
                    'up_ope_cd'    => Auth::user()->tantou_code
                ];
                $columnName      = __('messages.reason_content');
                $modelCancelReason = new MstCancelReason();
                $dataCancelReason = $modelCancelReason->where('reason_id', $request->cancel_reason)
                        ->first(['reason_content']);
                $reasonContent = '';
                if (count($dataCancelReason) > 0) {
                    $reasonContent = $dataCancelReason->reason_content;
                }
                $dataLog[] = [
                    'receive_id' => $receiveId,
                    'log_title'  => __('messages.change_order_info'),
                    'log_contents' =>
                        "【{$columnName}】 → {$reasonContent}",
                    'log_status'   => 1,
                    'in_ope_cd'    => Auth::user()->tantou_code,
                    'up_ope_cd'    => Auth::user()->tantou_code
                ];
                $dtOrderUpdateLog->insert($dataLog);
                DB::commit();
                return response()->json([
                    'status'     => 1,
                ]);
            } catch (\Exception $e) {
                DB::rollback();
                return response()->json([
                    'status'     => -1,
                ]);
            }
        }
    }

    /**
     * Process update stock
     *
     * @param  int  receiveID
     * @return void
     */
    public function processUpdateZan($dataProcess)
    {
        $modelODP = new DtOrderProductDetail();
        $modelSS  = new MstStockStatus();
        foreach ($dataProcess as $value) {
            $productCode = $value->product_code;
            $receivedOrderNum = $value->received_order_num;
            $arrUpdate = [];
            if (in_array($value->delivery_type, [1, 3])
                && $value->product_status !== PRODUCT_STATUS['CANCEL']
                && $value->order_status >= ORDER_STATUS['ESTIMATION']) {
                if (!empty($value->child_product_code)) {
                    $productCode = $value->child_product_code;
                }
                if ($value->product_status === 1 && $value->order_type !== 3) {
                    $arrUpdate['wait_delivery_num'] = DB::raw("wait_delivery_num - {$receivedOrderNum}");
                }
                $arrUpdate['order_zan_num'] = DB::raw("order_zan_num - {$receivedOrderNum}");
            }
            if (count($arrUpdate) !== 0) {
                $modelSS->updateData(['product_code' => $productCode], $arrUpdate);
            }
        }
    }
    /**
     * Process export data to csv
     *
     * @param   $request  Request
     * @return json
     */
    public function processExportCsv(Request $request)
    {
        $data = $this->dataList($request)->getData();
        $type       = $request->input('type', null);
        $fileName   = "";
        $column     = [];
        $column = [
            'name_jp'                   => __('messages.name_jp'),
            'order_date'                => __('messages.order_date'),
            'received_order_id'         => __('messages.received_order_id'),
            'receive_id'                => __('messages.receive_id'),
            'full_name'                 => __('messages.full_name'),
            'status_name'               => __('messages.order_status'),
            'payment_name'              => __('messages.payment_name'),
            'request_price'             => __('messages.request_price'),
            'delay_priod'               => __('messages.delay_priod_list_order')
        ];
        $fileName   = 'OrderManagement-' . date('YmdHis') . ".csv";
        Storage::disk('local')->put($fileName, '');
        $url = Storage::disk('local')->getDriver()->getAdapter()->getPathPrefix() . $fileName;
        $file = fopen($url, 'a');
        fputs($file, mb_convert_encoding(implode(",", $column), 'Shift-JIS', 'UTF-8'). "\r\n");
        if (isset($data->data)) {
            if (count($data->data) > 0) {
                foreach ($data->data as $key => $value) {
                    $arrTemp = [];
                    //$date = explode(" ", $value->repayment_date);
                    $arrTemp[] = $value->name_jp;
                    $arrTemp[] = $value->order_date !== null ? date('m-d', strtotime($value->order_date)) : '';
                    $arrTemp[] = $value->received_order_id;
                    $arrTemp[] = $value->receive_id;
                    $arrTemp[] = $value->full_name;
                    $arrTemp[] = $value->status_name;
                    $arrTemp[] = $value->payment_name;
                    $arrTemp[] = $value->request_price;
                    $arrTemp[] = $value->delay_priod;
                    $filter     = array("\r\n", "\n", "\r");
                    $contentCsv = implode(",", $arrTemp);
                    $contentCsv = str_replace($filter, '', $contentCsv);
                    fputs($file, mb_convert_encoding($contentCsv, 'Shift-JIS', 'UTF-8'). "\r\n");
                }
            }
        }
        fclose($file);
        return response()->json([
            'file_name' => $fileName,
            'status'     => 1,
        ]);
    }

    /**
     * process import data.
     * Return $object json
     */
    public function import(Request $request)
    {
        $type        = $request->input("type", "");
        $fileName    = $request->input("filename", "");
        $timeRun     = $request->input("timeRun", 0);
        $currPage    = $request->input("currPage", 0);
        $total       = $request->input("total", 0);
        $fileCorrect = $request->input("fileCorrect", "");
        $countErr    = $request->input("count_err", 0);
        $flg      = false;
        $common   = new Common;
        //
        $info = $common->checkFile($fileName);
        if ($info["flg"] === 0) {
            return response()->json($info);
        } else {
            $readers = $info["msg"];
        }
        if ($type === 'new_order') {
            $flg = $this->processNewOrderCsv($readers, $currPage, $total, $fileName, (int)$countErr, $fileCorrect);
        }

        $arrProcess = array(
            "fileName"    => $fileName,
            "timeRun"     => $timeRun,
            "currPage"    => $currPage,
            "total"       => $total,
            "fileCorrect" => $fileCorrect,
        );
        $infoRun = $common->processDataRun($flg, $arrProcess);
        return response()->json($infoRun);
    }

    /**
     * Process data in file csv order new
     * @param   string  $readers file name in storage
     * @return  boolean
     */
    public function processNewOrderCsv($arr, $offset, $length, $fileName, $countErr = 0, $fileCorrect = "")
    {
        $mstOrderDetail = new MstOrderDetail();
        $mstCustomer    = new MstCustomer();
        $mstOrder       = new MstOrder();
        $mstProduct     = new MstProduct();
        $readers = array_slice($arr, $offset*$length, $length);
        if ($readers === null) {
            return false;
        }
        $arrCol      = Config::get('csv.new_order');
        $totalColumn = count($arrCol);
        $arrCorrect  = array();
        $arrProduct  = array();
        $arrError    = array();
        $num         = 1;
        $checkHead   = false;
        $checkFail   = false;
        if ($countErr >= 3) {
            if ($offset === '0') {
                $checkHead = true;
            }
            $checkFail = true;
        }
        $paymentOpt      = MstSettlementManage::get(['payment_code', 'payment_name'])
            ->pluck('payment_name', 'payment_code')->all();
        $deliveryOpt     = MstShippingCompany::get(['company_id', 'company_name'])
            ->pluck('company_name', 'company_id')->all();
        $shipWishTimeOpt = Config::get('common.ship_wish_time');
        $deparmentOpt    = Config::get('common.order_dep');
        $checkOrderInfo  = true;
        $url = Storage::disk('local')->getDriver()->getAdapter()->getPathPrefix() . $fileCorrect;
        $file = fopen($url, 'a');
        $fieldName = [];
        foreach ($readers as $key => $row) {
            $row = trim($row);
            if ($checkFail) {
                $arrError[] = trim($row) . ',-->' . __('messages.request_timeout');
                continue;
            }
            if (empty($row) || ($offset === '0' && $num === 1)) {
                $num++;
                if (!empty($row)) {
                    $column   = str_getcsv($row, ",");
                    if (count($column) === $totalColumn) {
                        $fieldName = array_combine($arrCol, $column);
                    }
                }
                continue;
            }
            $column   = str_getcsv($row, ",");
            if (count($column) !== $totalColumn) {
                $arrError[] = trim($row) . ',-->' . __('messages.number_column_order_not_match');
                continue;
            }

            $arrTemp = array();
            $column  = array_map('trim', $column);
            $arrTemp = array_combine($arrCol, $column);

            $rule = [
                'product_code' => 'required|exists:horunba.mst_product,product_code',
                'price'        => 'numeric|min:0',
                'quantity'     => 'required|numeric|min:1',
            ];

            if ($checkOrderInfo) {
                $rule['type_order']      = 'required|in:FAX注文,備品発注';
                $rule['first_name']      = 'required';
                $rule['last_name']       = 'required';
                $rule['zip_code']        = 'required|numeric';
                $rule['prefecture']      = 'required';
                $rule['city']            = 'required';
                $rule['tel_num']         = 'required';
                $rule['email']           = 'required|email';
                $rule['payment_method']  = 'required|in:' . implode(",", $paymentOpt);
                $rule['delivery_method'] = 'required|in:' . implode(",", $deliveryOpt);
                $rule['ship_wish_time']  = 'in:' . implode(",", $shipWishTimeOpt);
                if (!empty($arrTemp['re_last_name']) || !empty($arrTemp['re_first_name'])
                    || !empty($arrTemp['re_postal']) || !empty($arrTemp['re_province'])
                    || !empty($arrTemp['re_address_1']) || !empty($arrTemp['re_address_2'])
                    || !empty($arrTemp['re_tel'])) {
                    $rule['re_last_name']  = 'required';
                    $rule['re_first_name'] = 'required';
                    $rule['re_postal']     = 'required|numeric';
                    $rule['re_province']   = 'required';
                    $rule['re_address_1']  = 'required';
                    $rule['re_address_2']  = 'required';
                    $rule['re_tel']        = 'required';
                    $arrTemp['is_the_same'] = 0;
                } else {
                    $arrTemp['is_the_same'] = 1;
                }
                if ($arrTemp['type_order'] === '備品発注') {
                    $rule['deparment'] = 'required|in:' . implode(",", $deparmentOpt);
                }
                $checkOrderInfo = false;
            }

            $validator = Validator::make($arrTemp, $rule)
                ->setAttributeNames($fieldName);
            if ($validator->fails()) {
                $arrError[] = trim($row) . ',-->' . implode(" | ", $validator->messages()->all());
            } else {
                $arrCorrect[] = $arrTemp;
                $arrProduct[] = $arrTemp['product_code'];
            }
        }

        if (count($arrError) === 0 && count($arrCorrect) > 0) {
            try {
                DB::beginTransaction();
                $typeOrder = ($arrCorrect[0]['type_order'] === '備品発注') ? 2 : 1;
                $isTheSame = $arrCorrect[0]['is_the_same'];

                $arrShipCus['first_name']      = (string)$arrCorrect[0]['first_name'];
                $arrShipCus['last_name']       = (string)$arrCorrect[0]['last_name'];
                $arrShipCus['first_name_kana'] = '';
                $arrShipCus['last_name_kana']  = '';
                $arrShipCus['email']           = (string)$arrCorrect[0]['email'];
                $arrShipCus['tel_num']         = str_replace('-', '', (string)$arrCorrect[0]['tel_num']);
                $arrShipCus['fax_num']         = (string)$arrCorrect[0]['fax_num'];
                $arrShipCus['zip_code']        = str_replace('-', '', (string)$arrCorrect[0]['zip_code']);
                $arrShipCus['prefecture']      = (string)$arrCorrect[0]['prefecture'];
                $arrShipCus['city']            = (string)$arrCorrect[0]['city'];
                $arrShipCus['sub_address']     = (string)$arrCorrect[0]['sub_address'];
                $arrShipCus['sex']             = '';
                $arrShipCus['urgent_tel_num']  = '';
                $arrShipCus['is_black_list']   = 0;
                $arrShipCus['in_ope_cd']       = Auth::user()->tantou_code;
                $arrShipCus['in_date']         = date('y-m-d h:i:s');
                $arrShipCus['up_ope_cd']       = Auth::user()->tantou_code;
                $arrShipCus['up_date']         = date('y-m-d h:i:s');
                $checkCustomer = $mstCustomer->checkCustomerExist($arrShipCus);
                $customerId    = 0;
                $receiverId    = 0;
                // Check & insert & update customer's info
                if (count($checkCustomer) === 0) {
                    $customerId = $mstCustomer->insertGetId($arrShipCus);
                } else {
                    $customerId = $checkCustomer->customer_id;
                    unset($arrShipCus['in_ope_cd']);
                    unset($arrShipCus['in_date']);
                    $mstCustomer->where(['customer_id' => $customerId])->update($arrShipCus);
                };

                // Check & insert & update receiver's info
                if ($isTheSame === 0) {
                    $arrShipReceiver['last_name']       = (string)$arrCorrect[0]['re_last_name'];
                    $arrShipReceiver['first_name']      = (string)$arrCorrect[0]['re_first_name'];
                    $arrShipReceiver['tel_num']         = str_replace('-', '', (string)$arrCorrect[0]['re_tel']);
                    $arrShipReceiver['zip_code']        = str_replace('-', '', (string)$arrCorrect[0]['re_postal']);
                    $arrShipReceiver['prefecture']      = (string)$arrCorrect[0]['re_province'];
                    $arrShipReceiver['city']            = (string)$arrCorrect[0]['re_address_1'];
                    $arrShipReceiver['sub_address']     = (string)$arrCorrect[0]['re_address_2'];
                    $checkReceiver = $mstCustomer->checkCustomerExist($arrShipReceiver);
                    if (count($checkReceiver) === 0) {
                        $arrShipReceiver['in_ope_cd']       = Auth::user()->tantou_code;
                        $arrShipReceiver['in_date']         = date('y-m-d h:i:s');
                        $arrShipReceiver['up_ope_cd']       = Auth::user()->tantou_code;
                        $arrShipReceiver['up_date']         = date('y-m-d h:i:s');
                        $receiverId = $mstCustomer->insertGetId($arrShipReceiver);
                    } else {
                        $receiverId = $checkReceiver->customer_id;
                        $arrShipReceiver['up_ope_cd']       = Auth::user()->tantou_code;
                        $arrShipReceiver['up_date']         = date('y-m-d h:i:s');
                        $mstCustomer->where(['customer_id' => $receiverId])->update($arrShipReceiver);
                    };
                }
                // Insert & update order
                if ($typeOrder === 2) {
                    $orderMaxId = $mstOrder->getMaxItemForRefundDetail('DEP-' . date('Ymd'));
                    $preTypeOrder = "DEP-";
                } else {
                    $orderMaxId = $mstOrder->getMaxItemForRefundDetail('FAX-' . date('Ymd'));
                    $preTypeOrder = "FAX-";
                }
                if (!empty($orderMaxId)) {
                    $reOrderId  = last(explode('-', $orderMaxId->received_order_id));
                    $reOrderId  = filter_var($reOrderId, FILTER_SANITIZE_NUMBER_INT);
                    $reOrderId  = $preTypeOrder . date('Ymd') . sprintf("-%04d", (int)$reOrderId + 1);
                } else {
                    $reOrderId  = $preTypeOrder . date('Ymd') . "-0001";
                }

                $orderDate = date('Y-m-d H:i:s');
                if (!empty($arrCorrect[0]['order_date'])) {
                    $orderDate = date('Y-m-d H:i:s', strtotime((string)$arrCorrect[0]['order_date']));
                }
                $shipWishTime = null;
                if (!empty($arrCorrect[0]['ship_wish_time'])) {
                    $shipWishTime = array_search((string)$arrCorrect[0]['ship_wish_time'], $shipWishTimeOpt);
                }
                $shipWishDate = null;
                if (!empty($arrCorrect[0]['ship_wish_date'])) {
                    $shipWishDate = date('Y-m-d', strtotime((string)$arrCorrect[0]['ship_wish_date']));
                }
                $paymentMethod = array_search((string)$arrCorrect[0]['payment_method'], $paymentOpt);
                $deliveryMethod = array_search((string)$arrCorrect[0]['delivery_method'], $deliveryOpt);
                $orderDep = 0;
                if ($typeOrder === 2) {
                    $orderDep = array_search((string)$arrCorrect[0]['deparment'], $deparmentOpt);
                }
                $mallId = 7;
                if ($typeOrder === 2) {
                    $mallId = 11;
                }

                $arrProductInfo = $mstProduct->getItemByProductCodes($arrProduct)->keyBy('product_code')->all();

                $totalPrice = 0;
                foreach ($arrCorrect as $data) {
                    $productCode = (string) $data['product_code'];
                    $price       = $data['price'];
                    if ($price === null || $price === '') {
                        $price = $arrProductInfo[$productCode]['price_invoice'] ?? 0;
                    }
                    $totalPrice += ((int)$price * (int)$data['quantity']);
                }

                $arrDataOrder = [];
                $arrDataOrder['received_order_id']      = $reOrderId;
                $arrDataOrder['mall_id']                = $mallId;
                $arrDataOrder['customer_id']            = $customerId;
                $arrDataOrder['used_coupon']            = 0;
                $arrDataOrder['used_point']             = 0;
                $arrDataOrder['discount']               = 0;
                $arrDataOrder['total_price']            = $totalPrice;
                $arrDataOrder['request_price']          = $totalPrice;
                $arrDataOrder['order_date']             = $orderDate;
                $arrDataOrder['ship_charge']            = 0;
                $arrDataOrder['pay_charge']             = 0;
                $arrDataOrder['pay_after_charge']       = 0;
                $arrDataOrder['pay_charge_discount']    = 0;
                $arrDataOrder['goods_price']            = $totalPrice;
                $arrDataOrder['goods_tax']              = 0;
                $arrDataOrder['arrive_type']            = 0;
                $arrDataOrder['is_multi_ship_address']  = 0;
                $arrDataOrder['ship_wish_time']         = $shipWishTime;
                $arrDataOrder['ship_wish_date']         = $shipWishDate;
                $arrDataOrder['payment_status']         = 0;
                $arrDataOrder['payment_method']         = $paymentMethod;
                $arrDataOrder['delivery_method']        = $deliveryMethod;
                $arrDataOrder['payment_request_date']   = null;
                $arrDataOrder['payment_price']          = 0;
                $arrDataOrder['payment_account']        = '';
                $arrDataOrder['payment_date']           = null;
                $arrDataOrder['payment_confirm_date']   = null;
                $arrDataOrder['customer_question']      = null;
                $arrDataOrder['shop_answer']            = null;
                $arrDataOrder['cancel_reason']          = null;
                $arrDataOrder['company_name']           = (string)$arrCorrect[0]['company_name'];
                $arrDataOrder['is_delay']               = 0;
                $arrDataOrder['delay_priod']            = null;
                $arrDataOrder['delay_reason']           = null;
                $arrDataOrder['mail_seri']              = null;
                $arrDataOrder['order_status']           = ORDER_STATUS['NEW'];
                $arrDataOrder['order_sub_status']       = ORDER_SUB_STATUS['NEW'];
                $arrDataOrder['order_dep']              = $orderDep;
                $arrDataOrder['request_id']             = Auth::user()->tantou_code;
                $arrDataOrder['is_sourcing_on_demand']  = 0;
                $arrDataOrder['response_due_date']      = null;
                $arrDataOrder['is_mail_sent']           = 0;
                $arrDataOrder['is_mall_update']         = 0;
                $arrDataOrder['is_mall_cancel']         = 0;
                $arrDataOrder['is_recalculate']         = 0;
                $arrDataOrder['message_api']            = null;
                $arrDataOrder['error_code_api']         = null;
                $arrDataOrder['err_text']               = null;
                $arrDataOrder['in_date']                = date('Y:m:d H:i:s');
                $arrDataOrder['in_ope_cd']              = Auth::user()->tantou_code;
                $arrDataOrder['up_date']                = date('Y:m:d H:i:s');
                $arrDataOrder['up_ope_cd']              = Auth::user()->tantou_code;
                $receiveId = $mstOrder->insertGetId($arrDataOrder);

                $detailLineNum  = 0;
                foreach ($arrCorrect as $data) {
                    $productCode = (string) $data['product_code'];
                    $price       = $data['price'];
                    $productName = $arrProductInfo[$productCode]['product_name_long'] ?? '';
                    if ($price === null || $price === '') {
                        $price = $arrProductInfo[$productCode]['price_invoice'] ?? 0;
                    }
                    $detailLineNum++;
                    $arrOrderDetail['product_code']        = $productCode;
                    $arrOrderDetail['price']               = $price;
                    $arrOrderDetail['receiver_id']         = $isTheSame === 0 ? $receiverId : $customerId;
                    $arrOrderDetail['quantity']            = (int) $data['quantity'];
                    $arrOrderDetail['detail_line_num']     = (int) $detailLineNum;
                    $arrOrderDetail['product_name']        = $productName;
                    $arrOrderDetail['receive_id']          = (int) $receiveId;
                    $arrOrderDetail['return_quantity']     = 0;
                    $arrOrderDetail['item_tax']            = 0;
                    $arrOrderDetail['ship_price']          = 0;
                    $arrOrderDetail['ship_tax']            = 0;
                    $arrOrderDetail['ship_date']           = null;
                    $arrOrderDetail['delivery_person_id']  = null;
                    $arrOrderDetail['package_ship_number'] = null;
                    $arrOrderDetail['time_wish_ship']      = null;
                    $arrOrderDetail['in_ope_cd']           = Auth::user()->tantou_code;
                    $arrOrderDetail['in_date']             = date('Y:m:d H:i:s');
                    $arrOrderDetail['up_ope_cd']           = Auth::user()->tantou_code;
                    $arrOrderDetail['up_date']             = date('Y:m:d H:i:s');
                    $mstOrderDetail->insert($arrOrderDetail);
                }
                DB::commit();
            } catch (\Exception $e) {
                DB::rollback();
                return "Error [{$e->getCode()}]: {$e->getMessage()}";
            }
        }

        fclose($file);
        $Common = new Common;
        $nameCached  = explode(".", $fileName);
        $nameCached  = $nameCached[0];
        if (count($arrError) > 0) {
            if (!Cache::has($nameCached) && !$checkHead) {
                $header = array_slice($arr, 0, 1);
                $arrError = array_merge($header, $arrError);
            }
            mb_convert_variables('SJIS', 'UTF-8', $arrError);
            $Common->updateCached($nameCached, implode("\r\n", $arrError));
        } elseif (Cache::has($nameCached)) {
            $Common->updateCached($nameCached, '');
        }
        return true;
    }

    /**
     * Process change back list
     * @param   object $request
     * @return  json
     */
    public function changeBackList(Request $request)
    {
        if ($request->ajax() === true) {
            $isBackList = $request->input('is_black_list', '');
            $receiveId  = $request->input('receive_id', '');
            $modelO = new MstOrder();
            $dataO = $modelO->find($receiveId);
            $modelC = new MstCustomer();

            $modelC->updateData(['customer_id' => $dataO->customer_id], ['is_black_list' => $isBackList]);
            return response()->json([
                'status' => 1
            ]);
        }
    }

    /**
     * Process change is claim
     * @param   object $request
     * @return  json
     */
    public function changeisClaim(Request $request)
    {
        if ($request->ajax() === true) {
            $receiveId  = $request->input('receive_id', '');
            $modelO = new MstOrder();
            $dataO = $modelO->find($receiveId);
            if ($dataO->is_claim === 0) {
                $dataO->is_claim = 1;
            } else {
                $dataO->is_claim = 0;
            }
            $dataO->save();
            return response()->json([
                'status' => 1
            ]);
        }
    }

    /**
     * Process change is claim for mst_order_detail
     * @param   object $request
     * @return  json
     */
    public function changeDetailClaim(Request $request)
    {
        if ($request->ajax() === true) {
            $receiveId     = $request->input('receive_id', '');
            $claimId       = $request->input('claim_id', '');
            $detailLineNum = $request->input('detail_line_num', '');
            $modelOD       = new MstOrderDetail();
            $modelOD->updateData(
                $receiveId,
                $detailLineNum,
                ['claim_id' => $claimId]
            );
            return response()->json([
                'status' => 1
            ]);
        }
    }
    /**
     * Process change is claim for mst_order_detail
     * @param   object $request
     * @return  json
     */
    public function changeAddClaim(Request $request)
    {
        if ($request->ajax() === true) {
            $claimContent = $request->input('claim_content', '');
            $modelC       = new MstClaim();
            $modelC->insert(
                [
                    'claim_content' => $claimContent,
                    'in_ope_cd'     => Auth::user()->tantou_code,
                    'up_ope_cd'     => Auth::user()->tantou_code
                ]
            );
            return response()->json([
                'status' => 1
            ]);
        }
    }
}
