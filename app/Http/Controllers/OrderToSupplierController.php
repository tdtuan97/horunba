<?php
/**
 * Controller for order to supplier management
 *
 * @package    App\Http\Controllers
 * @subpackage OrderToSupplierController
 * @copyright  Copyright (c) 2018 RiverCrane! Corporation. All Rights Reserved.
 * @author     le.hung <le.hung@rivercrane.vn>
 */

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Common;
use App\Http\Controllers\Controller;
use App\Models\Backend\DtRePayment;
use App\Models\Backend\MstOrder;
use App\Models\Backend\DtOrderToSupplier;
use App\Models\Backend\TOrderDirect;
use App\Models\Backend\TOrder;
use App\Models\Backend\TOrderDetail;
use App\Models\Backend\MstTantou;
use App\Models\Backend\MstProduct;
use App\Models\Backend\TAdmin;
use App\Models\Backend\TSeq;
use App\Models\Backend\TCustomer;
use App\Models\Backend\DtReturn;
use App\Models\Backend\TNo;
use App\Models\Backend\TNouHin;
use App\Models\Backend\VNouHin;
use App\Models\Backend\TInsertRequestListEdi;
use App\Models\Backend\TRequestPerformanceModelEdi;
use App\Models\Backend\LRequest;
use App\Models\Backend\DtOrderProductDetail;
use App\Models\Backend\MstStockStatus;
use App\Models\Backend\DtSendFaxData;
use App\Models\Batches\DtOrderToSupplier as BatchesDtOrderToSupplier;
use App\Models\Backend\DtOrderUpdateLog;
use App\Notifications\SlackNotification;
use App\Notification;
use Validator;
use Config;
use Storage;
use Auth;
use DB;
use App;
use PDF;
use Swift_Mailer;
use Swift_SmtpTransport;
use Swift_Message;
use Mail;

class OrderToSupplierController extends Controller
{
    /**
     * Load view.
     */
    public function index()
    {
        return view('order-to-supplier.index');
    }

    /**
     * Get all data.
     * Return $object json
     */
    public static function dataList(Request $request)
    {
        if ($request->ajax() === true) {
            $DtOrderToSupplier = new DtOrderToSupplier();
            $arrSort           = [];
            $type              = $request->input('type', null);
            $page              = $request->input('per_page', 20);
            $arrSearch = [
                'order_date'         => $request->input('order_date', null),
                'order_type'         => $request->input('order_type', null),
                'delivery_type'      => $request->input('delivery_type', null),
                'product_jan'        => $request->input('product_jan', null),
                'product_name'       => $request->input('product_name', null),
                'edi_delivery_date'  => $request->input('edi_delivery_date', null),
                'arrival_date_plan'  => $request->input('arrival_date_plan', null),
                'arrival_date'       => $request->input('arrival_date', null),
                'supplier_name'      => $request->input('supplier_name', null),
                'maker_full_nm'      => $request->input('maker_full_nm', null),
                'ope_status'         => $request->input('ope_status', null),
                'order_code'         => $request->input('order_code', null),
                'product_code'       => $request->input('product_code', null),

                'delivery_delay'     => $request->input('delivery_delay', null),
                'receive_delay'      => $request->input('receive_delay', null),
                'receive_incomplete' => $request->input('receive_incomplete', null),
                'note_check'         => $request->input('note_check', null),
                'not_answer'         => $request->input('not_answer', null),

                'btn_resale_all'         => $request->input('btn_resale_all', null),
                'btn_return_all'         => $request->input('btn_return_all', null),
                'btn_note_all'           => $request->input('btn_note_all', null),
                'ope_tantou_all'         => $request->input('ope_tantou_all', null),
                'ope_status_all'         => $request->input('ope_status_all', null),
                'process_all_flag'       => $request->input('process_all_flag', null),
                'process_name'           => $request->input('process_name', null),
                'process_value'          => $request->input('process_value', null),
            ];
            if (!is_null($type)) {
                $data = $DtOrderToSupplier->getData($arrSearch, $page, $arrSort, $type);
                return $data;
            } else {
                $arrInput = [
                    'page',
                    'per_page',
                    'btn_resale_all',
                    'btn_return_all',
                    'btn_note_all',
                    'ope_tantou_all',
                    'ope_status_all',
                    'process_all_flag',
                    'process_name',
                    'process_value'
                ];
                $flag = true;
                foreach ($arrSearch as $key => $value) {
                    if ($value) {
                        if (!in_array($key, $arrInput)) {
                            $flag = false;
                        }
                    }
                }
                if ($flag) {
                    $pageData   = $DtOrderToSupplier->getDataPage($page);
                    $listdata   = $pageData->toArray();
                    $listdata   = $listdata['data'];
                    $arrayOrder = [];
                    foreach ($listdata as $item) {
                        $arrayOrder[] = $item['order_code'];
                    }
                    $data = $DtOrderToSupplier->getDataAll($arrayOrder);

                } else {
                    $data = $DtOrderToSupplier->getData($arrSearch, $page, $arrSort, null);
                    $pageData = $data;
                }
            }

            $url = '';
            if (App::environment(['local'])) {
                $url = '//edi.local';
            } elseif (App::environment(['test'])) {
                $url = 'https://edi-test.diyfactory.jp';
            } else {
                $url = 'https://edi.diyfactory.jp';
            }
            $default = [['key' => '', 'value' => '-----']];

            $mstTantou         = new MstTantou();
            $tantou = $default;
            foreach ($mstTantou->get(['tantou_last_name']) as $item) {
                $tantou[] = ['key' => $item['tantou_last_name'], 'value' => $item['tantou_last_name']];
            }

            $delayNote = Config::get('common.delay_note');

            $reasonNote = $default;
            foreach (Config::get('common.reason_note') as $key => $item) {
                $reasonNote[] = ['key' => $key, 'value' => $item];
            }

            $opeStatus = $default;
            $opeStatusS = [];
            foreach (Config::get('common.ope_status') as $key => $item) {
                $opeStatus[] = ['key' => $key, 'value' => $item];
                $opeStatusS[] = ['key' => $key, 'value' => $item];
            }

            return response()->json([
                'tantou'     => $tantou,
                'delayNote'  => $delayNote,
                'reasonNote' => $reasonNote,
                'opeStatus'  => $opeStatus,
                'opeStatusS' => $opeStatusS,
                'data'       => $data,
                'pageData'   => $pageData,
                'url'        => $url,
                'processAllFlag' => 0,
            ]);
        }
    }

    /**
     * Filter data for prepare process update all
     * Return $object json
     */
    public function prepareProcessAll(Request $request)
    {
        if ($request->ajax() === true) {
            $DtOrderToSupplier = new DtOrderToSupplier();
            $arrSearch = [
                    'per_page'           => $request->input('per_page', null),

                    'order_date'         => $request->input('order_date', null),
                    'order_type'         => $request->input('order_type', null),
                    'delivery_type'      => $request->input('delivery_type', null),
                    'product_jan'        => $request->input('product_jan', null),
                    'product_name'       => $request->input('product_name', null),
                    'edi_delivery_date'  => $request->input('edi_delivery_date', null),
                    'arrival_date_plan'  => $request->input('arrival_date_plan', null),
                    'arrival_date'       => $request->input('arrival_date', null),
                    'supplier_name'      => $request->input('supplier_name', null),
                    'maker_full_nm'      => $request->input('maker_full_nm', null),
                    'ope_status'         => $request->input('ope_status', null),
                    'order_code'         => $request->input('order_code', null),
                    'product_code'       => $request->input('product_code', null),

                    'delivery_delay'     => $request->input('delivery_delay', null),
                    'receive_delay'      => $request->input('receive_delay', null),
                    'receive_incomplete' => $request->input('receive_incomplete', null),
                    'note_check'         => $request->input('note_check', null),
                    'not_answer'         => $request->input('not_answer', null),

                    'btn_resale_all'         => $request->input('btn_resale_all', null),
                    'btn_return_all'         => $request->input('btn_return_all', null),
                    'btn_note_all'           => $request->input('btn_note_all', null),
                    'ope_tantou_all'         => $request->input('ope_tantou_all', null),
                    'ope_status_all'         => $request->input('ope_status_all', null),
                    'process_all_flag'       => $request->input('process_all_flag', null),
                    'process_name'           => $request->input('process_name', null),
                    'process_value'          => $request->input('process_value', null),
                ];
            $processCheckFlag = (int) $request->input('process_all_flag');
            if ($processCheckFlag === 1) {
                return $this->updateAll($arrSearch);
            }
        }
    }
    /**
     * Process update all
     * Return $object json
     */
    public function updateAll($arraySearch = null)
    {
        $DtOrderToSupplier    = new DtOrderToSupplier();
        $dtOrderUpdateLog     = new DtOrderUpdateLog();
        $dtOrderProductDetail = new DtOrderProductDetail();
        $processFlg           = (int) $arraySearch['process_all_flag'];
        $processName          = $arraySearch['process_name'];
        if ($processFlg === 1) {
            if ($processName === 'btn_resale_all') {
                $cols = [
                    'dt_order_to_supplier.order_code',
                    'dt_order_to_supplier.product_code'
                ];
                $colsData = $DtOrderToSupplier->selectColumns($arraySearch, $cols);
                if (count($colsData) > 0) {
                    $colsDataChunk = array_chunk($colsData, 500);
                    foreach ($colsDataChunk as $keyC => $valueC) {
                        foreach ($valueC as $value) {
                            try {
                                $where = 'order_type = 5 AND ope_status != 2';
                                $orderToSup = DtOrderToSupplier::whereRaw($where)->find($value['order_code']);
                                if (!empty($orderToSup)) {
//                                     $stockStatus = MstStockStatus::find($value['product_code']);
//                                     if (!empty($stockStatus)) {
//                                         $stockStatus->return_num = $stockStatus->return_num - $orderToSup->order_num;
//                                         $stockStatus->wait_delivery_num = $stockStatus->wait_delivery_num - $orderToSup->order_num;
//                                         $stockStatus->save();
//                                     }
                                    $orderToSup->ope_tantou = Auth::user()->tantou_code;
                                    $orderToSup->ope_status = 2;
                                    $orderToSup->save();
                                }
                            } catch (\Exception $ex) {
                                return response()->json([
                                            'status' => 0,
                                            'msg' => $ex->getMessage()
                                ]);
                            }
                        }
                    }
                }
                return response()->json([
                            'status' => 1,
                            'method' => 'save'
                ]);
            } elseif ($processName === 'btn_return_all') {
                $modelR   = new DtReturn();
                $modelP   = new MstProduct();
                $cols = [
                    'dt_order_to_supplier.order_code',
                    'dt_order_to_supplier.product_code',
                    'dt_order_to_supplier.order_num',
                    'dt_order_to_supplier.price_invoice',
                    'dt_order_to_supplier.process_status',
                    'dt_order_to_supplier.edi_order_code',
                ];
                $colsData = $DtOrderToSupplier->selectColumns($arraySearch, $cols);
                if (count($colsData) > 0) {
                    $colsDataChunk = array_chunk($colsData, 500);
                    foreach ($colsDataChunk as $keyC => $valueC) {
                        foreach ($valueC as $value) {
                            $productCode  = $value['product_code'];
                            $orderCode    = $value['order_code'];
                            $orderNum     = $value['order_num'];
                            $priceInvoice = $value['price_invoice'];
                            $receiveInstruction = 0;
                            if ($value['process_status'] == 5) {
                                $receiveInstruction = 1;
                            } elseif ($value['process_status'] == 6) {
                                $receiveInstruction = 2;
                            }
                            try {
                                $where = 'order_type = 5 AND ope_status != 2';
                                $orderToSup = DtOrderToSupplier::whereRaw($where)->find($value['order_code']);
                                if (!empty($orderToSup)) {
                                    $maxNo  = $modelR->getMaxNo();
                                    if (empty($maxNo->maxNo)) {
                                        $returnNo = '9900000';
                                    } else {
                                        $returnNo = (int)$maxNo->maxNo + 1;
                                    }
                                    $arrTemp = [
                                        'return_no'            => $returnNo,
                                        'return_line_no'       => 0,
                                        'return_date'          => now(),
                                        'return_time'          => 1,
                                        'return_address_id'    => 0,
                                        'deje_no'              => '',
                                        'return_type'          => 0,
                                        'return_type_large_id' => 3,
                                        'return_type_mid_id'   => 2,
                                        'status'               => 1,
                                        'receive_instruction'  => $receiveInstruction,
                                        'receive_result'       => -1,
                                        'delivery_instruction' => 0,
                                        'delivery_result'      => -1,
                                        'red_voucher'          => 0,
                                        'parent_product_code'  => $productCode,
                                        'product_code'         => null,
                                        'return_quantity'      => $orderNum,
                                        'return_point'         => 0,
                                        'return_fee'           => 0,
                                        'return_cod_fee'       => 0,
                                        'return_price'         => $priceInvoice,
                                        'return_tanka'         => $priceInvoice,
                                        'supplier_id'          => null,
                                        'edi_order_code'       => $value['edi_order_code'],
                                        'receive_id'           => null,
                                        'receive_plan_date'    => null,
                                        'receive_real_date'    => null,
                                        'receive_real_num'     => 0,
                                        'delivery_plan_date'   => now(),
                                        'delivery_real_date'   => null,
                                        'delivery_real_num'    => 0,
                                        'payment_on_delivery'  => 0,
                                        'note'                 => null,
                                        'receive_count'        => 0,
                                        'delivery_count'       => 0,
                                        'supplier_return_no'   => null,
                                        'supplier_return_line' => null,
                                        'is_mojax'             => 0,
                                        'error_code'           => null,
                                        'error_message'        => null,
                                        'in_ope_cd'            => Auth::user()->tantou_code,
                                        'in_date'              => now(),
                                        'up_ope_cd'            => Auth::user()->tantou_code,
                                        'up_date'              => now(),
                                    ];
                                    $dataProduct = $modelP->getDataSetReturnSuppier($productCode);
                                    $line = 0;
                                    $arrInserts = [];
                                    foreach ($dataProduct as $value) {
                                        $line++;
                                        $arrTemp['return_line_no']    = $line;
                                        $arrTemp['return_address_id'] = (int)$value->return_id;
                                        $arrTemp['product_code']      = $value->child_product_code;
                                        $arrTemp['supplier_id']       = $value->price_supplier_id;
                                        $arrInserts[] = $arrTemp;
                                    }
                                    $modelR->insert($arrInserts);

                                    $orderToSup->ope_tantou = Auth::user()->tantou_code;
                                    $orderToSup->ope_status = 2;
                                    $orderToSup->save();
                                }
                            } catch (\Exception $ex) {
                                return response()->json([
                                    'status' => 0,
                                    'msg' => $ex->getMessage()
                                ]);
                            }
                        }
                    }
                }
            } elseif ($processName === 'btn_note_all') {
                $cols = [
                    'dt_order_to_supplier.order_code',
                    'dt_order_to_supplier.edi_order_code'
                ];
                $colsData = $DtOrderToSupplier->selectColumns($arraySearch, $cols);
                if (count($colsData) > 0) {
                    $colsDataChunk = array_chunk($colsData, 500);
                    foreach ($colsDataChunk as $keyC => $valueC) {
                        foreach ($valueC as $value) {
                            $DtOrderToSupplier->updateData($value, ['remarks' => $arraySearch['process_value']]);

                            //<------INSERT DT_ORDER_UPDATE_LOG------>
                            $receiveId = $dtOrderProductDetail->where(['order_code' => $value['order_code']])->first(['receive_id']);
                            $orderSupplierInfo = $DtOrderToSupplier->where($value)->first();
                            if (!empty($orderSupplierInfo->receive_id)) {
                                $dtOrderUpdateLog->insert(array(
                                    'log_title'    => '要確認事項あり',
                                    'receive_id'   => $orderSupplierInfo->receive_id,
                                    'log_status'   => 1,
                                    'in_date'      => $orderSupplierInfo->up_date,
                                    'log_contents' => $orderSupplierInfo->remarks,
                                    'up_ope_cd'    => Auth::user()->tantou_code,
                                    'in_ope_cd'    => Auth::user()->tantou_code
                                ));
                            }
                        }
                    }
                }
            } elseif ($processName === 'ope_tantou_all') {
                $cols = [
                    'dt_order_to_supplier.order_code',
                    'dt_order_to_supplier.edi_order_code'
                ];
                $colsData = $DtOrderToSupplier->selectColumns($arraySearch, $cols);
                if (count($colsData) > 0) {
                    $colsDataChunk = array_chunk($colsData, 500);
                    foreach ($colsDataChunk as $keyC => $valueC) {
                        foreach ($valueC as $value) {
                            $DtOrderToSupplier->updateData($value, ['ope_tantou' => $arraySearch['process_value']]);
                        }
                    }
                }
            } elseif ($processName === 'ope_status_all') {
                $cols = [
                    'dt_order_to_supplier.order_code',
                    'dt_order_to_supplier.edi_order_code'
                ];
                $colsData = $DtOrderToSupplier->selectColumns($arraySearch, $cols);
                if (count($colsData) > 0) {
                    $colsDataChunk = array_chunk($colsData, 500);
                    foreach ($colsDataChunk as $keyC => $valueC) {
                        foreach ($valueC as $value) {
                            $DtOrderToSupplier->updateDataDiffOT($value, ['ope_status' => (int) $arraySearch['process_value']]);
                        }
                    }
                }
            }
            return response()->json([
                        'status' => 1,
                        'method' => 'save'
            ]);
        }
    }
    /**
     * Get form data
     *
     * @param   $request  Request
     * @return  json
     */
    public function getFormData(Request $request)
    {
        if ($request->ajax() === true) {
            $DtRePayment    = new DtRePayment();
            $MstOrder       = new MstOrder();
            $data = array();
            $dataOrder = array();
            if ($request->get('order_code_from') !== null) {
                $data = $DtRePayment->getRePaymentById($request->get('order_code_from'));
                if (!empty($data)) {
                    $dataOrder = $MstOrder->getItemsForRePayMent($data->toArray());
                }
            }
            return response()->json([
                    'data'    => $data,
                    'dataOrder'    => $dataOrder

            ]);
        }
    }

    /**
     * Submit update more flags
     *
     * @param   $request  Request
     * @return  json
     */
    public function changeStatus(Request $request)
    {
        $DtOrderToSupplier      = new DtOrderToSupplier();
        $TOrderDirect           = new TOrderDirect();
        $modelOrder             = new TOrder();
        $modelOD                = new TOrderDetail();
        $modelOTS               = new DtOrderToSupplier();
        $modelO                 = new MstOrder();
        $modelOPD               = new DtOrderProductDetail();
        $dtOrderUpdateLog       = new DtOrderUpdateLog();

        if ($request->ajax() === true) {
            $params = $request->all();
            // Update is_confirmed
            if (isset($params['ope_tantou'])) {
                $where['order_code']     = $params['order_code'];
                $where['edi_order_code'] = $params['edi_order_code'];
                $data['ope_tantou']      = $params['ope_tantou'];
                $DtOrderToSupplier->updateData($where, $data);
            }
            // Update delay_note
            if (isset($params['reason_note'])) {
                $where['order_code']     = $params['order_code'];
                $where['edi_order_code'] = $params['edi_order_code'];
                $data['reason_note']     = $params['reason_note'];
                $DtOrderToSupplier->updateData($where, $data);
            }
            // Update delay_note
            if (isset($params['ope_status'])) {
                $where['order_code']     = $params['order_code'];
                $where['edi_order_code'] = $params['edi_order_code'];
                $data['ope_status']      = (int)$params['ope_status'];
                $DtOrderToSupplier->updateData($where, $data);
            }
            // Update is_cancel
            if (isset($params['is_cancel'])) {
                if (in_array((int) $params['order_type'], [2])) {
                    $mOrderTypeId = 16;
                    if ((int)$params['m_order_type_id'] === 2) {
                        $mOrderTypeId = 15;
                    }
                    $TOrderDirect->updateData(
                        ['order_code' => $params['edi_order_code']],
                        [
                            'cancel_flg' => 1,
                            'm_order_type_id' => $mOrderTypeId,
                        ]
                    );
                }
                if (in_array((int) $params['order_type'], [1])) {
                    $mOrderTypeId = 14;
                    if ((int)$params['m_order_type_id'] === 2) {
                        $mOrderTypeId = 13;
                    }
                    $TOrderDetail   = new TOrderDetail();
                    $TOrder         = new TOrder();
                    $TOrder->updateData(
                        ['order_code' => $params['edi_order_code']],
                        ['cancel_flg' => 1]
                    );
                    $TOrderDetail->updateData(
                        ['order_code' => $params['edi_order_code']],
                        [
                            'cancel_flg' => 1,
                            'm_order_type_id' => $mOrderTypeId,
                        ]
                    );
                }
                $where['order_code']     = $params['order_code'];
                $where['edi_order_code'] = $params['edi_order_code'];
                $data['is_cancel']       = (int) $params['is_cancel'];
                $DtOrderToSupplier->updateData($where, $data);
            }
            // Update state_flg  (lost)
            // Process process_status  = 5
            if (isset($params['state_flg'])) {
                $modelTI = new TInsertRequestListEdi();
                $modelTR = new TRequestPerformanceModelEdi();
                $whereOS['order_code']     = $params['order_code'];
                $whereOS['edi_order_code'] = $params['edi_order_code'];
                $dataOS['process_status']  = 6;
                $dataOS['is_lost']         = 1;
                $dataOS['ope_status']      = 2;
                $dataOS['ope_tantou']      = Auth::user()->tantou_code;
                $DtOrderToSupplier->updateData($whereOS, $dataOS);
                $dataSupplier = $modelOTS->getDataByKey($params['order_code'], $params['edi_order_code']);
                $modelOrder->updateData(
                        ['order_code' => $params['edi_order_code']],
                        [
                        'stock_quantity'    => DB::raw('`quantity`'),
                        'shortage_quantity' => 0
                        ]
                );
                $modelOD->updateData(
                        ['order_code' => $params['edi_order_code']],
                        [
                        'state_flg'         => 3,
                        'm_order_type_id'   => 19,
                        'shipment_date'     => date('Y-m-d'),
                        'stock_flg'         => 1,
                        'stock_quantity'    => DB::raw('`quantity`'),
                        'shortage_quantity' => 0
                        ]
                );
                $dataInsRequest = $modelTI->getDataProcessRequest($params['edi_order_code']);
                if (!empty($dataInsRequest)) {
                    $arrInsert = [];
                    $arrInsert['DataKb']           = 0;
                    $arrInsert['SupplyNo']         = $dataInsRequest->order_code;
                    $arrInsert['RequestNo']        = $dataInsRequest->RequestNo;
                    $arrInsert['RequestBNo']       = $dataInsRequest->RequestBNo;
                    $arrInsert['ArrivalDate']      = date("Ymd");
                    $arrInsert['PlanQuantity']     = $dataInsRequest->PlanQuantity;
                    $arrInsert['Quantity']         = $dataInsRequest->PlanQuantity;
                    $arrInsert['ItemCd']           = $dataInsRequest->ItemCd;
                    $arrInsert['ItemNm']           = $dataInsRequest->ItemNm;
                    $arrInsert['JanCd']            = $dataInsRequest->JanCd;
                    $arrInsert['SupplierNm']       = $dataInsRequest->SupplierNm;
                    $arrInsert['Note']             = 'ロスト';
                    $arrInsert['edi_update_flg']   = 1;
                    $arrInsert['decsy_update_flg'] = 2;
                    $arrInsert['price']            = $dataSupplier->price_invoice;
                    $arrInsert['slack_flg']        = null;
                    $arrInsert['created_at']       = now();
                    $modelTR->insert($arrInsert);
                }

                if (!in_array($params['order_type'], [3, 4]) && (int)$params['product_status'] !== 15 && (!empty($params['flg_re_order']))) {
                    $this->cloneNewOrder($params);
                } else {
                    $stockStatus = MstStockStatus::find($params['product_code']);
                    if (!empty($stockStatus)) {
                        $stockStatus->rep_orderring_num  =  (int)$stockStatus->rep_orderring_num - ((int)$dataSupplier->order_num - (int)$dataSupplier->arrival_quantity);
                        $stockStatus->save();
                    }
                    if ((int)$params['product_status'] === 7 || (int)$params['product_status'] === 17) {
                        $arrDataOrder = $modelOPD->where(['order_code' => $params['order_code']])
                                           ->groupBy('receive_id')
                                           ->get(['receive_id']);
                        $modelOPD->updateData(
                            ['order_code' => $params['order_code']],
                            [
                                'product_status' => PRODUCT_STATUS['WAIT_TO_ORDER'],
                                'order_code'     => null,
                            ]
                        );
                        if ($arrDataOrder->count() !== 0) {
                            foreach ($arrDataOrder as $value) {
                                $modelO->updateData(
                                    ['receive_id'=> $value->receive_id],
                                    [
                                        'order_status'     => ORDER_STATUS['ORDER'],
                                        'order_sub_status' => ORDER_SUB_STATUS['NEW'],
                                    ]
                                );
                            }
                        }
                    }
                }
            }

            if (isset($params['remarks'])) {
                $where['order_code'] = $params['order_code'];
                $where['edi_order_code'] = $params['edi_order_code'];
                $data['remarks']      = $params['remarks'];
                $DtOrderToSupplier->updateData($where, $data);
                //<------INSERT DT_ORDER_UPDATE_LOG------>
                $orderSupplierInfo = $DtOrderToSupplier->where($where)->first();
                if (!empty($orderSupplierInfo->receive_id)) {
                    $dtOrderUpdateLog->insert(array(
                        'log_title'    => '要確認事項あり',
                        'receive_id'   => $orderSupplierInfo->receive_id,
                        'log_status'   => 1,
                        'in_date'      => $orderSupplierInfo->up_date,
                        'log_contents' => $orderSupplierInfo->remarks,
                        'up_ope_cd'    => Auth::user()->tantou_code,
                        'in_ope_cd'    => Auth::user()->tantou_code
                    ));
                }
            }

            return response()->json([
                'status'            => 1,
                'method'            => 'save'
            ]);
        }
        return ;
    }

    /**
     * Resale function
     *
     * @param  Request $request
     * @return json
     */
    public function reSale(Request $request)
    {
        if ($request->ajax() === true) {
            $orderCode = $request->input('order_code', '');
            if (empty($orderCode)) {
                return response()->json([
                    'status' => 0,
                    'msg' => __('messages.valid_order_code')
                ]);
            }
            $orderToSup = DtOrderToSupplier::find($orderCode);
            if (empty($orderToSup)) {
                return response()->json([
                    'status' => 0,
                    'msg' => __('messages.valid_order_code')
                ]);
            }

            $productCode = $request->input('product_code', '');
            if (empty($productCode)) {
                return response()->json([
                    'status' => 0,
                    'msg' => __('messages.valid_product_code')
                ]);
            }
//             $stockStatus = MstStockStatus::find($productCode);
//             if (empty($stockStatus)) {
//                 return response()->json([
//                     'status' => 0,
//                     'msg' => __('messages.valid_product_code')
//                 ]);
//             }

            try {
//                 $stockStatus->return_num = $stockStatus->return_num - $orderToSup->order_num;
//                 $stockStatus->wait_delivery_num = $stockStatus->wait_delivery_num - $orderToSup->order_num;
//                 $stockStatus->save();

                $orderToSup->ope_tantou = Auth::user()->tantou_code;
                $orderToSup->ope_status = 2;
                $orderToSup->save();

                return response()->json([
                    'status' => 1,
                    'msg' => 'Success'
                ]);
            } catch (\Exception $ex) {
                return response()->json([
                    'status' => 0,
                    'msg' => $ex->getMessage()
                ]);
            }
        }
    }

    /**
     * Clone new order
     *
     * @param   $params  Data
     * @return  json
     */
    public function cloneNewOrder($params)
    {
        $modelOTS    = new DtOrderToSupplier();
        $modelP      = new MstProduct();
        $modelOrder  = new TOrder();
        $TOrderDirect = new TOrderDirect();
        $modelOD   = new TOrderDetail();
        $modelAdmin    = new TAdmin();
        $modelMO    = new MstOrder();
        $modelSeq      = new TSeq();
        $modelCus      = new TCustomer();
        $dataSupplier = $modelOTS->getDataByKey($params['order_code'], $params['edi_order_code']);
        DB::beginTransaction();
        DB::connection('edi')->beginTransaction();
        try {
            //Process insert dt_order_to_supplier
            $arrOrSupplier  = [];
            $arrOrSupplier['order_code']          = $dataSupplier->order_code;
            $arrOrSupplier['order_date']          = now();
            $arrOrSupplier['order_type']          = $dataSupplier->order_type;
            $arrOrSupplier['supplier_id']         = $dataSupplier->supplier_id;
            $arrOrSupplier['product_code']        = $dataSupplier->product_code;
            $arrOrSupplier['product_jan']         = $dataSupplier->product_jan;
            $arrOrSupplier['price_invoice']       = $dataSupplier->price_invoice;
            $arrOrSupplier['order_num']           = (int)$dataSupplier->order_num -
                                                    (int)$dataSupplier->arrival_quantity;
            $arrOrSupplier['arrival_date_plan']   = null;
            $arrOrSupplier['arrival_date']        = null;
            $arrOrSupplier['arrival_quantity']    = 0;
            $arrOrSupplier['incomplete_quantity'] = 0;
            $arrOrSupplier['other']               = null;
            $arrOrSupplier['remarks']             = null;
            $arrOrSupplier['wait_days']           = null;
            $arrOrSupplier['process_status']      = 1;
            $arrOrSupplier['receive_id']          = $dataSupplier->receive_id;
            $arrOrSupplier['detail_line_num']     = $dataSupplier->detail_line_num;
            $arrOrSupplier['sub_line_num']        = $dataSupplier->sub_line_num;
            $arrOrSupplier['times_num']           = $dataSupplier->times_num + 1;
            $arrOrSupplier['edi_order_code']      = $dataSupplier->order_code . $arrOrSupplier['times_num'];
            $arrOrSupplier['in_date']             = now();
            $arrOrSupplier['up_date']             = now();
            $modelOTS->insert($arrOrSupplier);
            //Process insert dt_order_to_supplier
            $dataProduct = $modelP->getDataCloneOrderSupplier($dataSupplier->product_code);
            $dataAdmin = $modelAdmin->getItemBySuppliercd($dataSupplier->supplier_id);
            if (in_array($dataSupplier->order_type, ['1', '3', '4', '5'])) {
                $deliveryPeriod = 0;
                $arrOrder       = [];
                $arrOrderDetail = [];
                switch ($dataProduct->delivery_flg) {
                    case '14営業日':
                        $deliveryPeriod = 14;
                        break;
                    case '３営業日':
                        $deliveryPeriod = 3;
                        break;
                    case '５営業日':
                        $deliveryPeriod = 5;
                        break;
                    case '７営業日':
                        $deliveryPeriod = 7;
                        break;
                    case '即納':
                        $deliveryPeriod = 3;
                        break;
                    default:
                        $deliveryPeriod = 30;
                        break;
                }
                $fax    = '';
                $faxFlg = 0;

                if (!empty($dataAdmin)) {
                    $fax    = $dataAdmin['send_fax_flg'] === '1' ? $dataAdmin['fax'] : '';
                    $faxFlg = $dataAdmin['send_fax_flg'] === '1' ? 1 : 0;
                }
                $supplyKB = '';
                if (in_array($dataSupplier->order_type, [1, 2])) {
                    $supplyKB = '客注';
                } elseif ($dataSupplier->order_type === 3) {
                    $supplyKB = '補充';
                } elseif ($value->order_type === 5) {
                    $supplyKB = '予定外';
                } else {
                    $supplyKB = '臨時';
                }
                // process order
                $arrOrder['suppliercd']     = $dataProduct->price_supplier_id;
                $arrOrder['SerialNo']       = null;
                $arrOrder['order_date']     = now();
                $arrOrder['order_code']     = (string)$arrOrSupplier['edi_order_code'];
                $arrOrder['supplieritemcd'] = (string)$dataProduct->supplier_code;
                $arrOrder['deiiveryperiod'] = $deliveryPeriod;
                $arrOrder['supplykb']       = $supplyKB;
                $arrOrder['item_code']      = (string)$dataProduct->product_code;
                $arrOrder['jan_code']       = (string)$dataProduct->product_jan;
                $arrOrder['maker_name']     = (string)$dataProduct->maker_full_nm;
                $arrOrder['maker_code']     = (string)$dataProduct->product_maker_code;
                $arrOrder['note_code']      = '';
                $arrOrder['name']           = (string)$dataProduct->product_name;
                $arrOrder['delivery']       = '南港';
                $arrOrder['quantity']       = (int)$arrOrSupplier['order_num'];
                $arrOrder['color']          = (string)$dataProduct->product_color;
                $arrOrder['size']           = (string)$dataProduct->product_size;
                $arrOrder['price']          = (int)$dataProduct->price_invoice;
                $arrOrder['other']          = null;
                $arrOrder['itemurl']        = $dataProduct->rak_img_url_1;
                $arrOrder['send_fax_flg']   = 0;
                $arrOrder['cancel_flg']     = 0;
                $arrOrder['del_flg']        = 0;
                $arrOrder['valid_flg']      = 1;
                $arrOrder['created_at']     = now();
                $arrOrder['updated_at']     = now();
                $arrOrder['fax']            = $fax;
                $arrOrder['fax_flg']        = $faxFlg;
                $arrOrder['mail']           = '';
                $arrOrder['mail_flg']       = 0;
                $arrOrder['send_mail_flg']  = 0;
                $arrOrder['send_mail_date'] = null;

                $idInsert = $modelOrder->insertGetId($arrOrder);
                // process order detail
                $tSeqId                               = $modelSeq->insertGetId(['directkb' => 0]);
                $arrOrderDetail['suppliercd']         = $dataProduct->price_supplier_id;
                $arrOrderDetail['t_order_id']         = $idInsert;
                $arrOrderDetail['m_order_type_id']    = 2;
                $arrOrderDetail['t_nouhin_id']        = null;
                $arrOrderDetail['t_seq_id']           = $tSeqId;
                $arrOrderDetail['requestbno']         = null;
                $arrOrderDetail['order_code']         = (string)$arrOrSupplier['edi_order_code'];
                $arrOrderDetail['shipment_date_plan'] = null;
                $arrOrderDetail['shipment_date']      = null;
                $arrOrderDetail['quantity']           = (int)$arrOrSupplier['order_num'];
                $arrOrderDetail['price']              = (int)$dataProduct->price_invoice;
                $arrOrderDetail['other']              = null;
                $arrOrderDetail['memo']               = null;
                $arrOrderDetail['direct_flg']         = 0;
                $arrOrderDetail['pass']               = '';
                $arrOrderDetail['state_flg']          = 1;
                $arrOrderDetail['cancel_flg']         = 0;
                $arrOrderDetail['filmaker_flg']       = 1;
                $arrOrderDetail['new_flg']            = 0;
                $arrOrderDetail['request_flg']        = 0;
                $arrOrderDetail['reminder_flg']       = 0;
                $arrOrderDetail['stock_quantity']     = 0;
                $arrOrderDetail['shortage_quantity']  = 0;
                $arrOrderDetail['stock_other']        = null;
                $arrOrderDetail['stock_flg']          = 0;
                $arrOrderDetail['t_admin_id']         = null;
                $arrOrderDetail['del_flg']            = 0;
                $arrOrderDetail['valid_flg']          = 1;
                $arrOrderDetail['created_at']         = now();
                $arrOrderDetail['updated_at']         = now();
                $modelOD->insert($arrOrderDetail);
            } elseif (in_array($dataSupplier->order_type, ['2'])) {
                $dataCustomer = $modelMO->getDataCustomerByReceive($dataSupplier->receive_id);
                $arrCustomer      = [];
                $arrCustomer['SerialNo']   = 0;
                $arrCustomer['c_nm']       = $dataCustomer->order_lastname . $dataCustomer->order_firstname;
                $arrCustomer['c_postal']   = $dataCustomer->order_zip_code;
                $arrCustomer['c_address']  = $dataCustomer->order_prefecture . $dataCustomer->order_city . $dataCustomer->order_sub_address;
                $arrCustomer['c_tel']      = $dataCustomer->order_tel;
                $arrCustomer['d_nm']       = $dataCustomer->ship_to_last_name . $dataCustomer->ship_to_first_name;
                $arrCustomer['d_postal']   = $dataCustomer->ship_zip_code;
                $arrCustomer['d_address']  = $dataCustomer->ship_prefecture . $dataCustomer->ship_city . $dataCustomer->ship_sub_address;
                $arrCustomer['d_tel']      = $dataCustomer->ship_tel_num;
                $arrCustomer['del_flg']    = 0;
                $arrCustomer['valid_flg']  = 1;
                $arrCustomer['created_at'] = now();
                $arrCustomer['updated_at'] = now();
                $arrCusCheck = $modelCus->checkCustomerExist($arrCustomer);
                $id          = '';
                $check       = false;
                if (empty($arrCusCheck)) {
                    $check = true;
                    $id = $modelCus->insertGetId($arrCustomer);
                } else {
                    $id = $arrCusCheck->t_customer_id;
                }
                $fax    = '';
                $faxFlg = 0;
                if (!empty($dataAdmin)) {
                    $fax    = $dataAdmin['send_fax_flg'] === '1' ? $dataAdmin['fax'] : '';
                    $faxFlg = $dataAdmin['send_fax_flg'] === '1' ? 1 : 0;
                }
                switch ($dataProduct->delivery_flg) {
                    case '14営業日':
                        $deliveryPeriod = 14;
                        break;
                    case '３営業日':
                        $deliveryPeriod = 3;
                        break;
                    case '５営業日':
                        $deliveryPeriod = 5;
                        break;
                    case '７営業日':
                        $deliveryPeriod = 7;
                        break;
                    case '即納':
                        $deliveryPeriod = 3;
                        break;
                    default:
                        $deliveryPeriod = 30;
                        break;
                }

                $supplyKB = '';
                if (in_array($dataSupplier->order_type, [1, 2])) {
                    $supplyKB = '客注';
                } elseif ($dataSupplier->order_type === 3) {
                    $supplyKB = '補充';
                } else {
                    $supplyKB = '臨時';
                }
                $tSeqId = $modelSeq->insertGetId(['directkb' => 1]);
                $arrOrderDirect['suppliercd']           = $dataProduct->price_supplier_id;
                $arrOrderDirect['t_customer_id']        = $id;
                $arrOrderDirect['t_delivery_id']        = null;
                $arrOrderDirect['m_order_type_id']      = 2;
                $arrOrderDirect['SerialNo']             = 0;
                $arrOrderDirect['t_seq_id']             = $tSeqId;
                $arrOrderDirect['order_date']           = now();
                $arrOrderDirect['order_code']           = $arrOrSupplier['edi_order_code'];
                $arrOrderDirect['supplieritemcd']       = (string)$dataProduct->supplier_code;
                $arrOrderDirect['supplykb']             = $supplyKB;
                $arrOrderDirect['deiiveryperiod']       = $check === true ? null : $deliveryPeriod;
                $arrOrderDirect['item_code']            = (string)$dataProduct->product_code;
                $arrOrderDirect['jan_code']             = (string)$dataProduct->product_jan;
                $arrOrderDirect['maker_name']           = (string)$dataProduct->maker_full_nm;
                $arrOrderDirect['maker_code']           = (string)$dataProduct->product_maker_code;
                $arrOrderDirect['note_code']            = '';
                $arrOrderDirect['name']                 = (string)$dataProduct->product_name_long;
                $arrOrderDirect['color']                = (string)$dataProduct->product_color;
                $arrOrderDirect['size']                 = (string)$dataProduct->product_size;
                $arrOrderDirect['daito_price']          = (int)$dataProduct->price_invoice;
                $arrOrderDirect['price']                = (int)$dataProduct->price_invoice;
                $arrOrderDirect['quantity']             = (int)$arrOrSupplier['order_num'];
                $arrOrderDirect['shipment_date_plan']   = null;
                $arrOrderDirect['other']                = '';
                $arrOrderDirect['memo']                 = null;
                $arrOrderDirect['mail']                 = '';
                $arrOrderDirect['send_mail_flg']        = 0;
                $arrOrderDirect['send_mail_date']       = null;
                $arrOrderDirect['fax']                  = $fax;
                $arrOrderDirect['fax_flg']              = $faxFlg;
                $arrOrderDirect['send_fax_flg']         = 0;
                $arrOrderDirect['state_flg']            = 1;
                $arrOrderDirect['cancel_flg']           = 0;
                $arrOrderDirect['send_cancel_fax_flg']  = 0;
                $arrOrderDirect['send_cancel_fax_date'] = null;
                $arrOrderDirect['filmaker_flg']         = 1;
                $arrOrderDirect['del_flg']              = 0;
                $arrOrderDirect['valid_flg']            = 1;
                $arrOrderDirect['created_at']           = now();
                $arrOrderDirect['updated_at']           = now();
                $TOrderDirect->insert($arrOrderDirect);
            }
            DB::commit();
            DB::connection('edi')->commit();
        } catch (\Exception $ex) {
            report($ex);
            DB::rollBack();
            DB::connection('edi')->rollback();
        }
    }
    /**
     * Save data
     *
     * @param   $params  Data
     * @return  json
     */
    public function save(Request $request)
    {
        if ($request->ajax() === true) {
            $params         = $request->all();
            $modelTno       = new TNo();
            $modelTNouHin   = new TNouHin();
            $modelTSeq      = new TSeq();
            $modelTOrder    = new TOrder();
            $modelTOrderDetail = new TOrderDetail();
            $modelTInsertRequestList = new TInsertRequestListEdi();
            $modelLRequest  = new LRequest();
            if (isset($params['product_code'])) {
                $rules = [
                    'product_code'     => 'required|exists:mst_product,product_code'
                ];
                $validator = Validator::make($request->all(), $rules);
                if ($validator->fails()) {
                    return response()->json([
                        'status'  => 0,
                        'method'  => 'save',
                        'message' => $validator->errors()
                    ]);
                }
            }
            if (isset($params['isPost'])) {
                if ($params['isPost'] === true) {
                    try {
                        $rules = [
                            'product_code' => 'required|exists:mst_product,product_code',
                            'order_num'    => 'required|numeric|min:1',
                            'product_jan'  => 'required',
                        ];
                        $validator = Validator::make($request->all(), $rules);
                        if ($validator->fails()) {
                            return response()->json([
                                'status'  => 0,
                                'method'  => 'save',
                                'message' => $validator->errors()
                            ]);
                        } else {
                            $dtOrderToSupplier = new DtOrderToSupplier();
                            $yearMonth     = substr(date('Ym'), 3);
                            $maxOrderCode  = $dtOrderToSupplier->getMaxOrderCodeByDate($yearMonth);
                            $indexOrder    = 0;
                            if (count($maxOrderCode) > 0) {
                                $orderCode = $maxOrderCode->toArray()['order_code'];
                                if (strpos($orderCode, $yearMonth) === 0) {
                                    $indexOrder = (int)substr($orderCode, 3);
                                }
                            }
                            $orderCode      = $yearMonth . sprintf("%06d", $indexOrder+1);
                            $date           = date('Y-m-d H:i:s');
                            $supplierId     = $request->input('supplier_id', 0);
                            $supplierCode   = $request->input('supplier_code', '');
                            $productCode    = $request->input('product_code', null);
                            $productJan     = $request->input('product_jan', null);
                            $priceInvoice   = $request->input('price_invoice', 0);
                            $orderNum       = $request->input('order_num', 0);
                            $makerName      = $request->input('maker_full_nm', null);
                            $makerCode      = $request->input('product_maker_code', null);
                            $productNameLong= $request->input('product_name_long', null);
                            $productColor   = $request->input('product_color', null);
                            $productSize    = $request->input('product_size', null);
                            $itemUrl        = $request->input('rak_img_url_1', null);
                            $fax            = $request->input('fax', null);
                            $sendFaxFlg     = $request->input('send_fax_flg', 0);
                            $deliveryFlg    = $request->input('delivery_flg', 0);
                            $dataInsertDtO2S= [
                                'order_code'        => $orderCode,
                                'order_date'        => $date,
                                'order_type'        => 5,
                                'supplier_id'       => $supplierId,
                                'product_code'      => $productCode,
                                'product_jan'       => $productJan,
                                'price_invoice'     => $priceInvoice,
                                'order_num'         => $orderNum,
                                'arrival_date_plan' => $date,
                                'arrival_date'      => now(),
                                'arrival_quantity'  => 0,
                                'incomplete_quantity'=> 0,
                                'order_note'        => null,
                                'remarks'           => null,
                                'wait_days'         => null,
                                'process_status'    => 5,
                                'receive_id'        => null,
                                'detail_line_num'   => 0,
                                'sub_line_num'      => 0,
                                'times_num'         => 0,
                                'edi_order_code'    => $orderCode . '0',
                                'in_date'           => $date,
                                'up_date'           => $date
                            ];

                            if ($supplierId === 0) {
                                $error  = "------------------------------------------" . PHP_EOL;
                                $error .= basename(__CLASS__) . PHP_EOL;
                                $error .= "Order code $orderCode has supplier_id 0" . PHP_EOL;
                                $error .= PHP_EOL . "------------------------------------------" . PHP_EOL;
                                $slack = new Notification(CHANNEL['horunba']);
                                $slack->notify(new SlackNotification($error));
                            }

                            // Insert into dt_order_to_supplier
                            $idOrderCode = $dtOrderToSupplier->insert($dataInsertDtO2S);

                            if ((int) $idOrderCode === 1) {
                                $dataTOrder= [
                                    'suppliercd'        => $supplierId,
                                    'SerialNo'          => null,
                                    'order_date'        => $date,
                                    'order_code'        => $orderCode . '0',
                                    'supplieritemcd'    => (string)$supplierCode,
                                    'deiiveryperiod'    => null,
                                    'supplykb'          => '予定外',
                                    'item_code'         => (string)$productCode,
                                    'jan_code'          => (string)$productJan,
                                    'maker_name'        => (string)$makerName,
                                    'maker_code'        => (string)$makerCode,
                                    'note_code'         => '',
                                    'name'              => (string)$productNameLong,
                                    'delivery'          => '南港',
                                    'quantity'          => (int)$orderNum,
                                    'color'             => (string)$productColor,
                                    'size'              => (string)$productSize,
                                    'price'             => (int)$priceInvoice,
                                    'itemurl'           => (string)$itemUrl,
                                    'send_fax_flg'      => 1,
                                    'cancel_flg'        => 0,
                                    'del_flg'           => 0,
                                    'valid_flg'         => 1,
                                    'created_at'        => $date,
                                    'updated_at'        => $date,
                                    'fax'               => '',
                                    'fax_flg'           => 0,
                                    'mail'              => '',
                                    'mail_flg'          => 0,
                                    'send_mail_flg'     => 1,
                                    'send_mail_date'    => null
                                ];
                                // Insert into t_order
                                $tOrderId = $modelTOrder->insertGetId($dataTOrder);
                                // Prepare insert for t_order_detail
                                $dataTNouHin['suppliercd']      = $supplierId;
                                $dataTNouHin['no']              = $orderCode . '0';
                                $dataTNouHin['shipment_date']   = date('Y-m-d');
                                $dataTNouHin['shipment_price']  = 0;
                                $dataTNouHin['arrival_date']    = null;
                                //Insert new data to table t nouhin
                                $idTNouHin                       = $modelTNouHin->insertGetId($dataTNouHin);
                                // Get id tsque
                                $idTSeq = $modelTSeq->insertData();
                                $dataTOrderDetail= [
                                    'suppliercd'        => $supplierId,
                                    't_order_id'        => $tOrderId,
                                    'm_order_type_id'   => 1,
                                    't_nouhin_id'       => $idTNouHin,
                                    't_seq_id'          => $idTSeq,
                                    'requestbno'        => '00001',
                                    'order_code'        => $orderCode . '0',
                                    'shipment_date_plan'=> null,
                                    'shipment_date'     => $date,
                                    'quantity'          => $orderNum,
                                    'price'             => $priceInvoice,
                                    'other'             => null,
                                    'memo'              => null,
                                    'direct_flg'        => 0,
                                    'pass'              => null,
                                    'state_flg'         => 3,
                                    'cancel_flg'        => 0,
                                    'filmaker_flg'      => 2,
                                    'new_flg'           => 0,
                                    'request_flg'       => 1,
                                    'reminder_flg'      => 0,
                                    'stock_quantity'    => 0,
                                    'shortage_quantity' => 0,
                                    'stock_other'       => null,
                                    'stock_flg'         => 0,
                                    't_admin_id'        => 0,
                                    'del_flg'           => 0,
                                    'valid_flg'         => 1,
                                    'created_at'        => $date,
                                    'updated_at'        => $date,
                                    'zaiko_checked'     => 1
                                ];
                                // Insert into t_order_detail
                                $modelTOrderDetail->insertGetId($dataTOrderDetail);
                            }
                            $tNouhinData = $modelTOrder->getDataNouhin($orderCode . '0');

                            if (!empty($tNouhinData)) {
                                $dataInsertRequestList = array(
                                    'RequestNo'         => $tNouhinData->no,
                                    'RequestBNo'        => $tNouhinData->requestbno,
                                    'order_code'        => $tNouhinData->order_code,
                                    'ArrivalPlanDate'   => date('H:i') > '16:45' ? date('Ymd', strtotime('+1days')) :date('Ymd'),
                                    'PlanQuantity'      => $tNouhinData->quantity - $tNouhinData->stock_quantity,
                                    'ItemCd'            => $tNouhinData->item_code,
                                    'ItemNm'            => $tNouhinData->name,
                                    'JanCd'             => $tNouhinData->jan_code,
                                    'SupplierNm'        => $tNouhinData->t_admin_name,
                                    'MakerNm'           => $tNouhinData->maker_name,
                                    'MakerNo'           => $tNouhinData->maker_code,
                                    'RequestTypeKb'     => 0,
                                    'process_name'      => '予定外',
                                );

                                $modelTInsertRequestList->insertTInsertRequestList($dataInsertRequestList);
                                // 入荷指示を記録 L_request
                                // Insert new log data into l_request , wirte log
                                $dataLRequest = array(
                                    't_admin_id' => 9999,
                                    'suppliercd' => $tNouhinData->suppliercd,
                                    'order_code' => $tNouhinData->order_code,
                                );
                                $modelLRequest->insertLRequest($dataLRequest);
                            }
                            $url = '';
                            if (App::environment(['local']) || App::environment(['test'])) {
                                $url = 'https://edi-test.diyfactory.jp';
                            } else {
                                $url = 'https://edi.diyfactory.jp';
                            }
                            // Process call api
                            $apiUrl = $url . '/edi/api/exec-curl-feed';
                            $cmd  = "curl H $apiUrl";
                            if (substr(php_uname(), 0, 7) == "Windows") {
                                $cmd .= " >NUL 2>NUL";
                                pclose(popen('start /B cmd /C "' . $cmd . '"', 'r'));
                            } else {
                                exec($cmd . " > /dev/null 2>/dev/null &");
                            }
                            return response()->json([
                                'status'  => 1,
                                'method'  => 'save',
                                'message' => 'Insert data successfully.'
                            ]);
                        }
                        DB::commit();
                        DB::connection('edi')->commit();
                    } catch (\Exception $ex) {
                        DB::rollBack();
                        DB::connection('edi')->rollback();
                    }
                }
            }
            return response()->json([
                'data'            => [],
            ]);
        }
        return view('order-to-supplier.add');
    }
    /**
     * Get info product
     *
     * @param   $params  Data
     * @return  json
     */
    public function getDataProduct(Request $request)
    {
        if ($request->ajax() === true) {
            $colWhere = 'product_code';
            $value = $request->input('product_code', null);
            if (empty($value)) {
                $colWhere = 'product_jan';
                $value = $request->input('product_jan', null);
            }
            $modelP = new MstProduct();
            $data = $modelP->getDataProductSupplier($colWhere, $value);
            return response()->json([
                'info' => $data,
            ]);
        }
    }
    /**
     * Process update return
     *
     * @param   $params  Data
     * @return  json
     */
    public function processReturn(Request $request)
    {
        $modelR   = new DtReturn();
        $modelP   = new MstProduct();
        $modelOTS = new DtOrderToSupplier();
        $productCode  = $request->input('product_code', null);
        $orderCode    = $request->input('order_code', null);
        $ediOrderCode = $request->input('edi_order_code', null);
        $orderNum     = $request->input('order_num', null);
        $priceInvoice = $request->input('price_invoice', null);
        $processStatus = $request->input('process_status', null);
        $maxNo  = $modelR->getMaxNo();
        if (empty($maxNo->maxNo)) {
            $returnNo = '9900000';
        } else {
            $returnNo = (int)$maxNo->maxNo + 1;
        }
        $receiveInstruction = 0;
        if ((int)$processStatus === 5) {
            $receiveInstruction = 1;
        } elseif ((int)$processStatus === 6) {
            $receiveInstruction = 2;
        }
        $arrTemp = [
            'return_no'            => $returnNo,
            'return_line_no'       => 0,
            'return_date'          => now(),
            'return_time'          => 1,
            'return_address_id'    => 0,
            'deje_no'              => '',
            'return_type'          => 0,
            'return_type_large_id' => 3,
            'return_type_mid_id'   => 2,
            'status'               => 1,
            'receive_instruction'  => $receiveInstruction,
            'receive_result'       => -1,
            'delivery_instruction' => 0,
            'delivery_result'      => -1,
            'red_voucher'          => 0,
            'parent_product_code'  => $productCode,
            'product_code'         => null,
            'return_quantity'      => $orderNum,
            'return_point'         => 0,
            'return_fee'           => 0,
            'return_cod_fee'       => 0,
            'return_price'         => $priceInvoice,
            'return_tanka'         => $priceInvoice,
            'supplier_id'          => null,
            'receive_id'           => null,
            'receive_plan_date'    => null,
            'receive_real_date'    => null,
            'receive_real_num'     => 0,
            'delivery_plan_date'   => now(),
            'delivery_real_date'   => null,
            'delivery_real_num'    => 0,
            'payment_on_delivery'  => 0,
            'note'                 => null,
            'receive_count'        => 0,
            'delivery_count'       => 0,
            'supplier_return_no'   => null,
            'supplier_return_line' => null,
            'is_mojax'             => 0,
            'edi_order_code'       => $ediOrderCode,
            'error_code'           => null,
            'error_message'        => null,
            'in_ope_cd'            => Auth::user()->tantou_code,
            'in_date'              => now(),
            'up_ope_cd'            => Auth::user()->tantou_code,
            'up_date'              => now(),
        ];
        $dataProduct = $modelP->getDataSetReturnSuppier($productCode);
        $line = 0;
        $arrInserts = [];
        foreach ($dataProduct as $value) {
            $line++;
            $arrTemp['return_line_no']    = $line;
            $arrTemp['return_address_id'] = (int)$value->return_id;
            $arrTemp['product_code']      = $value->child_product_code;
            $arrTemp['supplier_id']       = $value->price_supplier_id;
            $arrInserts[] = $arrTemp;
        }

        try {
            $modelR->insert($arrInserts);
            $modelOTS->updateData(
                [
                    'order_code'     => $orderCode,
                    'edi_order_code' => $ediOrderCode
                ],
                [
                    'ope_tantou' => Auth::user()->tantou_code,
                    'ope_status' => 2,
                ]
            );
            return response()->json([
                'status' => 1,
                'msg' => 'Success'
            ]);
        } catch (\Exception $ex) {
            return response()->json([
                'status' => 0,
                'msg' => $ex->getMessage()
            ]);
        }
    }

    /**
     * Get fax order
     *
     * @param Request $request
     * @return json
     */
    public function getFaxOrder(Request $request)
    {
        if ($request->ajax() === true) {
            $type = $request->input('type', null);
            $ediOrderCode = $request->input('fax_order_code', null);
            $productCode = $request->input('fax_product_code', null);
            $modelOTS = new DtOrderToSupplier();
            $modelFax = new DtSendFaxData();
            $result = $modelOTS->getFaxOrder($type, $ediOrderCode, $productCode);
            $maxId  = $modelFax->max('index');
            if ($result !== null) {
                $result['login_user'] = Auth::user()->tantou_last_name;
                $result['index']      = $maxId + 1;
                return json_encode([
                    'flg' => 1,
                    'msg' => $result
                ]);
            }
            return json_encode([
                'flg' => 0,
                'msg' => 'No data'
            ]);
        }
    }

    /**
     * Get fax order common
     *
     * @param Request $request
     * @return json
     */
    public function getFaxOrderCommon(Request $request)
    {
        if ($request->ajax() === true) {
            $modelP = new MstProduct();
            $modelFax = new DtSendFaxData();

            $type = $request->input('type', null);
            $productCode = $request->input('fax_product_code', null);

            Validator::extend(
                'checkFaxOrder',
                function ($parameters, $value, $attribute) use ($modelP, $type) {
                    $check = $modelP->getFaxOrder($type, $value);
                    if (!empty($check)) {
                        return true;
                    }
                    return false;
                },
                trans('validation.check_fax_order')
            );

            $rules = [
                'fax_product_code' => 'required|checkFaxOrder'
            ];

            $validator = Validator::make($request->all(), $rules);
            if ($validator->fails()) {
                return json_encode([
                    'flg' => -1,
                    'msg' => $validator->errors()
                ]);
            }

            $result = $modelP->getFaxOrder($type, $productCode);
            $maxId  = $modelFax->max('index');
            if ($result !== null) {
                $result['login_user'] = Auth::user()->tantou_last_name;
                $result['index']      = $maxId + 1;
                return json_encode([
                    'flg' => 1,
                    'msg' => $result
                ]);
            }
            return json_encode([
                'flg' => 0,
                'msg' => 'No data'
            ]);
        }
    }

    /**
     * Process fax order
     *
     * @param Request $request
     * @return json
     */
    public function processFaxOrder(Request $request)
    {
        if ($request->ajax() === true) {
            $modelOTS = new DtOrderToSupplier();
            $modelFax = new DtSendFaxData();
            $rules     = [
                'fax_name'     => 'required',
                'fax'          => 'required',
                'fax_contents' => 'required'
            ];
            $input     = $request->all();
            $validator = Validator::make($input, $rules);
            if ($validator->fails()) {
                return json_encode([
                    'flg' => 0,
                    'msg' => $validator->errors()
                ]);
            }

            $faxNum = $request->input('fax', null);

            $dataInsert = [
                'order_code'    => $request->input('edi_order_code', null),
                'fax_num'       => $faxNum,
                'is_supplier'   => $request->input('is_supplier', null),
                'supplier_code' => $request->input('key_cd', null),
                'fax_contents'  => $request->input('fax_contents', null),
                'send_status'   => 0,
                'send_date'     => date('Y-m-d H:i:s'),
                'error_code'    => null,
                'error_content' => null,
                'in_ope_cd'     => Auth::user()->tantou_code,
                'in_date'       => date('Y-m-d H:i:s')
            ];

            $id = $modelFax->insertGetId($dataInsert);

            $fileName = date('YmdHis') . '_' . $request->input('key_cd', null) . ".pdf";
            $filePath = storage_path("fax/$fileName");
            $data = [
                'fax_name'    => $request->input('fax_name', null),
                'fax_num'     => $faxNum,
                'login_user'  => $request->input('login_user', null),
                'login_user'  => $request->input('login_user', null),
                'index'       => $id,
                'fax_content' => str_replace("\n", "<br/>", $request->input('fax_contents', '')),
                'fax_date'    => date('Y/m/d'),
                'fax_time'    => date('H:i:s')
            ];
            $pdf = PDF::loadView(
                'order-to-supplier.fax.pdf.index',
                [
                    'data' => $data,
                ]
            )->save($filePath);

            $fax = str_replace('-', '', $faxNum);
            $fax = ltrim($fax);
            $fax = substr($fax, 1, strlen($fax) - 1);

            $ftpConf = Config::get('ftp')['edi_real'];
            $faxConf = Config::get('fax')['mail'];
            $mailTo  = '81' . $fax . $faxConf['to'];
            if (App::environment(['local', 'test'])) {
                $ftpConf = Config::get('ftp')['edi_test'];
                $mailTo  = $faxConf['to_test'];
            }

            $arrErr = [];

            $swiftTransport = (new Swift_SmtpTransport(
                $faxConf['host'],
                $faxConf['port']
            ))
            ->setUsername($faxConf['username'])
            ->setPassword($faxConf['password']);
            $mailer = new Swift_Mailer($swiftTransport);
            Mail::setSwiftMailer($mailer);

            $dataMail = [
                'body'      => '',
                'subject'   => '81' . $fax . "_OrderHorunba_" . $fileName,
                'attach'    => $filePath,
                'mail_from' => $faxConf['from'],
                'mail_to'   => $mailTo
            ];

            try {
                Mail::raw($dataMail['body'], function ($message) use ($dataMail) {
                    $message->from($dataMail['mail_from']);
                    $message->to($dataMail['mail_to']);
                    $message->subject($dataMail['subject']);
                    $message->attach($dataMail['attach']);
                });
                $modelFax->where(['index' => $id])->update(['send_status' => 1]);
            } catch (\Exception $e) {
                $modelFax->where(['index' => $id])->update(['send_status' => 2]);
                $arrErr[] = $e->getMessage();
            }

            $connId = @ftp_connect($ftpConf['host'], $ftpConf['port']);
            if (!$connId) {
                $arrErr[] = 'Connect ftp fail';
            } elseif (!@ftp_login($connId, $ftpConf['username'], $ftpConf['password'])) {
                $arrErr[] = 'Login ftp fail';
            } else {
                ftp_pasv($connId, true);
                if (!@ftp_put($connId, $ftpConf['path'] . $fileName, $filePath, FTP_BINARY)) {
                    $arrErr[] = 'Put file ftp fail';
                }
            }

            if (count($arrErr) > 0) {
                return json_encode([
                    'flg' => -1,
                    'msg' => implode("\n", $arrErr)
                ]);
            }

            return json_encode([
                'flg' => 1,
                'msg' => 'Sent success'
            ]);
        }
    }
}
