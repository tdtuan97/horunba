<?php
/**
 * Controller for batch management
 *
 * @package    App\Http\Controllers
 * @subpackage SettlementMappingController
 * @copyright  Copyright (c) 2019 RiverCrane! Corporation. All Rights Reserved.
 * @author     Truong Nghia<truong.van.nghia@rivercrane.vn>
 */
namespace App\Http\Controllers;

use App\Models\Backend\DtUriMapping;
use App\Models\Backend\DtUriRakCoupon;
use App\Models\Backend\DtUriRakPay;
use App\Models\Backend\DtUriYahoo;
use App\Models\Backend\MstMall;
use App\Models\Backend\DtUriPaygent;
use App\Models\Backend\DtUriMfk;
use App\Models\Backend\DtUriSagawa;
use App\Models\Backend\DtUriJppost;
use App\Models\Backend\DtUriSeinou;
use App\Models\Backend\DtUriAmazon;
use App\Models\Backend\DtPaymentList;
use App\Models\Backend\MstSettlementManage;
use App\Models\Backend\MstUriOccurReason;
use App\Models\Backend\MstUriRemarks;
use App\Jobs\ProcessDataCsvUriMapping;
use Maatwebsite\Excel\Facades\Excel;
use App\Exports\UriExport;
use Cache;
use Config;
use Illuminate\Http\Request;
use Storage;
use App;
use Auth;
use App\Custom\Utilities;

class UriMappingController extends Controller
{
    /**
     * Load view.
     */
    public function index()
    {
        $modelMall  = new MstMall();
        $modelMSM   = new MstSettlementManage();
        $mallData   = $modelMall->getData();
        $settData   = $modelMSM->getDataSelectBoxMulti();
        $arrDefault = [['key' => '', 'value' => '-----']];
        return view('uri-mapping.index')->with(
            [
                'mallDatas'     => $mallData,
                'settDatas'     => $settData,
                'diffPriceOpts' => array_merge($arrDefault, Config::get('common.flg')),
            ]
        );
    }

    /**
     * Show data list view
     *
     * @param   $request  Request
     * @return json
     */
    public function dataList(Request $request)
    {
        if ($request->ajax() === true) {
            $arrSearch = array(
                'mall_id'            => $request->input('name_jp', null),
                'received_order_id'  => $request->input('received_order_id', null),
                'order_date_from'    => $request->input('order_date_from', null),
                'order_date_to'      => $request->input('order_date_to', null),
                'delivery_date_from' => $request->input('delivery_date_from', null),
                'delivery_date_to'   => $request->input('delivery_date_to', null),
                'payment_name'       => $request->input('payment_name', null),
                'diff_price_flg'     => $request->input('diff_price_flg', null),
                'per_page'           => $request->input('per_page', null),
                'diff_price'         => $request->input('diff_price', null),
                'payment_method'     => $request->input('payment_method', null),
                'is_corrected'       => $request->input('is_corrected', null),
            );
            $arrSort = [
                'name_jp'            => $request->input('sort_name_jp', null),
                'received_order_id'  => $request->input('sort_received_order_id', null),
                'order_date'         => $request->input('sort_order_date', null),
                'delivery_date'      => $request->input('sort_delivery_date', null),
                'payment_method'     => $request->input('sort_payment_method', null),
                'web_payment_method' => $request->input('sort_web_payment_method', null),
                'request_price'      => $request->input('sort_request_price', null),
                'web_request_price'  => $request->input('sort_web_request_price', null),
                'different_price'    => $request->input('sort_different_price', null),
                'order_status'       => $request->input('sort_order_status', null),
                'web_order_status'   => $request->input('sort_web_order_status', null),
            ];

            $modelMall = new MstMall();
            $mallData  = $modelMall->getData();
            $modelDUM  = new DtUriMapping();
            $arrIgnore = [
                'page',
                'per_page'
            ];
            $arrCheckZero = [
                'is_corrected',
            ];
            $flag = Utilities::checkPerformance($arrSearch, $arrSort, $arrIgnore, $arrCheckZero);
            if (!empty($request->input('flg_update_all'))) {
                $name  = $request->input('name', '');
                $value = $request->input('value', '');
                if ($name === 'occur_reason' || $name === 'remarks') {
                    $arrUpdateAll = [
                        $name => $value
                    ];
                } else {
                    $arrUpdateAll = [
                        'is_corrected' => 1
                    ];
                }
                if ($flag) {
                    $modelDUM->whereRaw("1=1")->update($arrUpdateAll);
                } else {
                    $modelDUM->updateAll($arrSearch, null,  $arrUpdateAll);
                }
                return response()->json([
                    'status' => 1
                ]);
            }
            if ($flag) {
                $pageData   = $modelDUM->getDataPage($arrSearch['per_page']);
                $listdata   = $pageData->toArray();
                $listdata   = $listdata['data'];
                $arrayOrder = [];
                foreach ($listdata as $item) {
                    $arrayOrder[] = $item['received_order_id'];
                }
                $data = $modelDUM->getDataAll($arrayOrder);
            } else {
                $data = $modelDUM->getData($arrSearch, $arrSort);
                $pageData = $data;
            }
            $linkOrder = [
                1 => "https://order.rms.rakuten.co.jp/rms/mall/order/rb/vc?__event=BO02_001_013&order_number=%s",
                2 => "https://pro.store.yahoo.co.jp/pro.diy-tool/order/manage/detail/%s",
                3 => "https://sellercentral.amazon.co.jp/hz/orders/details?_encoding=UTF8&orderId=%s",
                4 => "http://42.127.237.184/admin/order/edit.php?order_id=%s",
                5 => "https://www.diy-tool.com/FutureShop2/AcceptDetailHook.htm?acceptno=%s",
                8 => "https://shop.diyfactory.jp/admin/index.php?route=sale/order/info&order_id=%s",
                9 => "https://order-rp.rms.rakuten.co.jp/order-rb/individual-order-detail-sc/init?orderNumber=%s",
            ];
            if (App::environment(['local', 'test'])) {
                $linkOrder[8] = "http://naruto.daitotest.tk/admin/index.php?route=sale/order/info&order_id=%s";
            }

            $optionRemarksAll     = '';
            $optionOccurReasonAll = '';
            $flgOption = false;
            if (!empty($data)) {
                $modelUOR = new MstUriOccurReason();
                $modelUR  = new MstUriRemarks();
                $dataUR   = $modelUR::all();
                $dataUOR  = $modelUOR::all();
                foreach ($data as $key => $value) {
                    $linkMall = '';
                    foreach ($linkOrder as $keyLink => $val) {
                        if ($keyLink === $value->mall_id) {
                            $receivedOrderId = $value->received_order_id;
                            if ($value->mall_id === 5) {
                                $receivedOrderId = str_replace("diy-", "", $receivedOrderId);
                            }
                            if ($value->mall_id === 8) {
                                $receivedOrderId = str_replace("DIY-", "", $receivedOrderId);
                            }
                            $linkMall = sprintf($val, $receivedOrderId);
                        }
                    }
                    $data[$key]->order_date = date('Y-m-d', strtotime($value->order_date));
                    $data[$key]->link_order_detail = action('OrderManagementController@detail') . '?receive_id_key=' . $value->receive_id;
                    $data[$key]->link_order_web = $linkMall;
                    $optionRemarks        = '';
                    $optionOccurReason    = '';
                    foreach ($dataUR as $value) {
                        if ($value->remarks_name === $data[$key]->remarks) {

                            $optionRemarks .= "<option selected value='{$value->remarks_name}'>{$value->remarks_name}</option>";
                        } else {
                            $optionRemarks .= "<option value='{$value->remarks_name}'>{$value->remarks_name}</option>";
                        }
                        if (!$flgOption) {
                            $optionRemarksAll .= "<option value='{$value->remarks_name}'>{$value->remarks_name}</option>";
                        }
                    }
                    foreach ($dataUOR as $value) {
                        if ($value->occur_reason_name === $data[$key]->occur_reason) {
                            $flg = true;
                            $optionOccurReason .= "<option selected value='{$value->occur_reason_name}'>{$value->occur_reason_name}</option>";
                        } else {
                            $optionOccurReason .= "<option value='{$value->occur_reason_name}'>{$value->occur_reason_name}</option>";
                        }
                        if (!$flgOption) {
                            $optionOccurReasonAll .= "<option value='{$value->occur_reason_name}'>{$value->occur_reason_name}</option>";
                        }
                    }
                    $data[$key]->optionRemarks     = $optionRemarks;
                    $data[$key]->optionOccurReason = $optionOccurReason;
                    $flgOption = true;
                }
            }
            return response()->json([
                'data'                 => $data,
                'optionRemarksAll'     => $optionRemarksAll,
                'optionOccurReasonAll' => $optionOccurReasonAll,
                'pageData'             => $pageData,
                'mallData'             => $mallData->toArray(),
            ]);
        }
    }

    /**
     * process import data.
     * Return $object json
     */
    public function import(Request $request)
    {
        $type        = $request->input("type", "");
        $fileName    = $request->input("filename", "");
        $timeRun     = $request->input("timeRun", 0);
        $currPage    = $request->input("currPage", 0);
        $total       = $request->input("total", 0);
        $countErr    = $request->input("count_err", 0);
        $realFile    = $request->input("realFilename", "");
        $fileCorrect = $request->input("fileCorrect", "");
        $flg         = false;
        $common      = new Common;
        $isUnix      = false;
        $isConvert   = true;
        $arrIsUnix = ['seinou_csv', 'amazon_pay_csv', 'pro_mfk_csv'];
        if (in_array($type, $arrIsUnix)) {
            $isUnix = true;
        }
        $arrNotConvert = ['pro_mfk_csv'];
        if (in_array($type, $arrNotConvert)) {
            $isConvert = false;
        }
        $info        = $common->checkFile($fileName, $isUnix, $isConvert);
        if ($info["flg"] === 0) {
            return response()->json($info);
        } else {
            $readers = $info["msg"];
        }
        $table  = '';
        $arrCol = [];
        if ($type === 'rakuten_pay_csv') {
            $model     = new DtUriRakPay();
            $dataCheck = $model->where('imported_file_name', $realFile)->count();
            if ($dataCheck !== 0) {
                return response()->json([
                    'flg' => 0,
                    'msg' => 'File was imported.',
                ]);
            }
            $flg = $this->processRakutenPayCsv(
                $readers,
                $currPage,
                $total,
                $fileName,
                (int) $countErr,
                $realFile,
                $fileCorrect
            );
            $table    = 'dt_uri_rak_pay';
            $arrCol   = Config::get('common.csv_uri_rakuten_pay');
            $arrCol[] = 'imported_file_name';
            $arrCol[] = 'process_flg';
            $arrCol[] = 'is_delete';
            $arrCol[] = 'created_at';
        } elseif ($type === 'yahoo_pay_csv') {
            $model     = new DtUriYahoo();
            $dataCheck = $model->where('imported_file_name', $realFile)->count();
            if ($dataCheck !== 0) {
                return response()->json([
                    'flg' => 0,
                    'msg' => 'File was imported.',
                ]);
            }
            $flg = $this->processYahooPayCsv(
                $readers,
                $currPage,
                $total,
                $fileName,
                (int) $countErr,
                $realFile,
                $fileCorrect
            );
            $table    = 'dt_uri_yahoo';
            $arrCol   = Config::get('common.csv_uri_yahoo_pay');
            $arrCol[] = 'imported_file_name';
            $arrCol[] = 'process_flg';
            $arrCol[] = 'is_delete';
            $arrCol[] = 'created_at';
        } elseif ($type === 'rakuten_coupon_csv') {
            $model     = new DtUriRakCoupon();
            $dataCheck = $model->where('imported_file_name', $realFile)->count();
            if ($dataCheck !== 0) {
                return response()->json([
                    'flg' => 0,
                    'msg' => 'File was imported.',
                ]);
            }
            $flg = $this->processRakutenCouponCsv(
                $readers,
                $currPage,
                $total,
                $fileName,
                (int) $countErr,
                $realFile,
                $fileCorrect
            );
            $table    = 'dt_uri_rak_coupon';
            $arrCol   = Config::get('common.csv_uri_rak_coupon');
            $arrCol[] = 'imported_file_name';
            $arrCol[] = 'process_flg';
            $arrCol[] = 'is_delete';
            $arrCol[] = 'created_at';
        } elseif ($type === 'amazon_pay_csv') {
            $model     = new DtUriAmazon();
            $dataCheck = $model->where('imported_file_name', $realFile)->count();
            if ($dataCheck !== 0) {
                return response()->json([
                    'flg' => 0,
                    'msg' => 'File was imported.',
                ]);
            }
            $flg = $this->processAmazonPayCsv(
                $readers,
                $currPage,
                $total,
                $fileName,
                (int) $countErr,
                $realFile,
                $fileCorrect
            );
            $table    = 'dt_uri_amazon';
            $arrCol   = Config::get('common.csv_uri_amazon_pay');
            $arrCol[] = 'imported_file_name';
            $arrCol[] = 'process_flg';
            $arrCol[] = 'is_delete';
            $arrCol[] = 'created_at';
            foreach ($arrCol as $key => $value) {
                $arrCol[$key] = "`$value`";
            }
        } elseif ($type === 'pro_paygent_csv') {
            $model     = new DtUriPaygent();
            $dataCheck = $model->where('imported_file_name', $realFile)->count();
            if ($dataCheck !== 0) {
                return response()->json([
                    'flg' => 0,
                    'msg' => 'File was imported.',
                ]);
            }
            $flg = $this->processProPaygentCsv(
                $readers,
                $currPage,
                $total,
                $fileName,
                (int) $countErr,
                $realFile,
                $fileCorrect
            );
            $table    = 'dt_uri_paygent';
            $arrCol   = Config::get('common.csv_uri_pro_paygent');
            $arrCol[] = 'imported_file_name';
            $arrCol[] = 'process_flg';
            $arrCol[] = 'is_delete';
            $arrCol[] = 'created_at';
        } elseif ($type === 'pro_mfk_csv') {
            $model     = new DtUriMfk();
            $dataCheck = $model->where('imported_file_name', $realFile)->count();
            if ($dataCheck !== 0) {
                return response()->json([
                    'flg' => 0,
                    'msg' => 'File was imported.',
                ]);
            }
            $flg = $this->processMfkCsv(
                $readers,
                $currPage,
                $total,
                $fileName,
                (int) $countErr,
                $realFile,
                $fileCorrect
            );
            $table    = 'dt_uri_mfk';
            $arrCol   = Config::get('common.csv_uri_mfk');
            $arrCol[] = 'imported_file_name';
            $arrCol[] = 'process_flg';
            $arrCol[] = 'is_delete';
            $arrCol[] = 'created_at';
        } elseif ($type === 'sagawa_pay_csv') {
            $model     = new DtUriSagawa();
            $dataCheck = $model->where('imported_file_name', $realFile)->count();
            if ($dataCheck !== 0) {
                return response()->json([
                    'flg' => 0,
                    'msg' => 'File was imported.',
                ]);
            }
            $flg = $this->processSagawaCsv(
                $readers,
                $currPage,
                $total,
                $fileName,
                (int) $countErr,
                $realFile,
                $fileCorrect
            );
            $table    = 'dt_uri_sagawa';
            $arrCol   = Config::get('common.csv_uri_sagawa');
            $arrCol[] = 'imported_file_name';
            $arrCol[] = 'process_flg';
            $arrCol[] = 'is_delete';
            $arrCol[] = 'created_at';
        } elseif ($type === 'jp_post_csv') {
            $model     = new DtUriJppost();
            $dataCheck = $model->where('imported_file_name', $realFile)->count();
            if ($dataCheck !== 0) {
                return response()->json([
                    'flg' => 0,
                    'msg' => 'File was imported.',
                ]);
            }
            $flg = $this->processJpPostCsv(
                $readers,
                $currPage,
                $total,
                $fileName,
                (int) $countErr,
                $realFile,
                $fileCorrect
            );
            $table    = 'dt_uri_jppost';
            $arrCol   = Config::get('common.csv_uri_jp_post');
            $arrCol[] = 'imported_file_name';
            $arrCol[] = 'process_flg';
            $arrCol[] = 'is_delete';
            $arrCol[] = 'created_at';
        } elseif ($type === 'seinou_csv') {
            $model     = new DtUriSeinou();
            $dataCheck = $model->where('imported_file_name', $realFile)->count();
            if ($dataCheck !== 0) {
                return response()->json([
                    'flg' => 0,
                    'msg' => 'File was imported.',
                ]);
            }
            $flg = $this->processSeinouCsv(
                $readers,
                $currPage,
                $total,
                $fileName,
                (int) $countErr,
                $realFile,
                $fileCorrect
            );
            $table    = 'dt_uri_seinou';
            $arrCol   = Config::get('common.csv_uri_seinou');
            $arrCol[] = 'imported_file_name';
            $arrCol[] = 'process_flg';
            $arrCol[] = 'is_delete';
            $arrCol[] = 'created_at';
        }
        if ($currPage === $timeRun) {
            $common->processCorrect($fileCorrect, $arrCol, $table);
        }
        $arrProcess = array(
            "fileName"     => $fileName,
            "timeRun"      => $timeRun,
            "currPage"     => $currPage,
            "total"        => $total,
            "realFilename" => $realFile,
            "fileCorrect"  => $fileCorrect,
        );
        $infoRun = $common->processDataRun($flg, $arrProcess);
        return response()->json($infoRun);
    }

    /**
     * Process data in file csv for diy
     * @param   string  $readers file name in storage
     * @return  boolean
     */
    public function processRakutenPayCsv(
        $arr,
        $offset,
        $length,
        $fileName,
        $countErr = 0,
        $realFile = "",
        $fileCorrect = ""
    ) {
        $readers = array_slice($arr, $offset * $length, $length);
        if ($readers === null) {
            return false;
        }
        $arrCol      = Config::get('common.csv_uri_rakuten_pay');
        $totalColumn = 11;
        $arrError    = array();
        $num         = 1;
        $checkHead   = false;
        $checkFail   = false;
        if ($countErr >= 3) {
            if ($offset === '0') {
                $checkHead = true;
            }
            $checkFail = true;
        }
        $url  = Storage::disk('local')->getDriver()->getAdapter()->getPathPrefix() . $fileCorrect;
        $file = fopen($url, 'a');
        foreach ($readers as $key => $row) {
            if ($checkFail) {
                $arrError[] = trim($row) . ",-->" . __('messages.request_timeout');
                continue;
            }
            if (empty($row) || ($offset === '0' && $num === 1)) {
                $num++;
                continue;
            }
            $column = str_getcsv($row, ",");
            if (count($column) !== $totalColumn) {
                $arrError[] = trim($row) . ",-->" . __('messages.number_column_not_match');
                continue;
            }
            $column                        = array_map('trim', $column);
            $arrTemp                       = $column;
            $arrTemp                       = array_combine($arrCol, $column);
            $arrTemp['imported_file_name'] = $realFile;
            $arrTemp['process_flg']        = 0;
            $arrTemp['is_deleted']         = 0;
            $arrTemp['created_at']         = now();
            fputs($file, mb_convert_encoding(implode(",", $arrTemp), 'Shift-JIS', 'UTF-8') . "\r\n");
        }
        fclose($file);
        $Common     = new Common;
        $nameCached = explode(".", $fileName);
        $nameCached = $nameCached[0];
        if (count($arrError) > 0) {
            if (!Cache::has($nameCached) && !$checkHead) {
                $header   = array_slice($arr, 0, 1);
                $arrError = array_merge($header, $arrError);
            }
            mb_convert_variables('SJIS', 'UTF-8', $arrError);
            $Common->updateCached($nameCached, implode("\r\n", $arrError));
        } elseif (Cache::has($nameCached)) {
            $Common->updateCached($nameCached, '');
        }
        return true;
    }

    /**
     * Process data in file csv for diy
     * @param   string  $readers file name in storage
     * @return  boolean
     */
    public function processYahooPayCsv(
        $arr,
        $offset,
        $length,
        $fileName,
        $countErr = 0,
        $realFile = "",
        $fileCorrect = ""
    ) {
        $readers = array_slice($arr, $offset * $length, $length);
        if ($readers === null) {
            return false;
        }
        $arrCol      = Config::get('common.csv_uri_yahoo_pay');
        $totalColumn = 8;
        $arrError    = array();
        $num         = 1;
        $checkHead   = false;
        $checkFail   = false;
        if ($countErr >= 3) {
            if ($offset === '0') {
                $checkHead = true;
            }
            $checkFail = true;
        }
        $url  = Storage::disk('local')->getDriver()->getAdapter()->getPathPrefix() . $fileCorrect;
        $file = fopen($url, 'a');
        foreach ($readers as $key => $row) {
            if ($checkFail) {
                $arrError[] = trim($row) . ",-->" . __('messages.request_timeout');
                continue;
            }
            if (empty($row) || ($offset === '0' && $num === 1)) {
                $num++;
                continue;
            }
            $column = str_getcsv($row, ",");
            if (count($column) !== $totalColumn) {
                $arrError[] = trim($row) . ",-->" . __('messages.number_column_not_match');
                continue;
            }
            $column                        = array_map('trim', $column);
            $arrTemp                       = $column;
            $arrTemp                       = array_combine($arrCol, $column);
            $arrTemp['imported_file_name'] = $realFile;
            $arrTemp['process_flg']        = 0;
            $arrTemp['is_deleted']         = 0;
            $arrTemp['created_at']         = now();
            fputs($file, mb_convert_encoding(implode(",", $arrTemp), 'Shift-JIS', 'UTF-8') . "\r\n");
        }
        fclose($file);
        $Common     = new Common;
        $nameCached = explode(".", $fileName);
        $nameCached = $nameCached[0];
        if (count($arrError) > 0) {
            if (!Cache::has($nameCached) && !$checkHead) {
                $header   = array_slice($arr, 0, 1);
                $arrError = array_merge($header, $arrError);
            }
            mb_convert_variables('SJIS', 'UTF-8', $arrError);
            $Common->updateCached($nameCached, implode("\r\n", $arrError));
        } elseif (Cache::has($nameCached)) {
            $Common->updateCached($nameCached, '');
        }
        return true;
    }

    /**
     * Process data in file csv for diy
     * @param   string  $readers file name in storage
     * @return  boolean
     */
    public function processRakutenCouponCsv(
        $arr,
        $offset,
        $length,
        $fileName,
        $countErr = 0,
        $realFile = "",
        $fileCorrect = ""
    ) {
        $readers = array_slice($arr, $offset * $length, $length);
        if ($readers === null) {
            return false;
        }
        $arrCol      = Config::get('common.csv_uri_rak_coupon');
        $totalColumn = 7;
        $arrError    = array();
        $num         = 1;
        $checkHead   = false;
        $checkFail   = false;
        if ($countErr >= 3) {
            if ($offset === '0') {
                $checkHead = true;
            }
            $checkFail = true;
        }
        $url  = Storage::disk('local')->getDriver()->getAdapter()->getPathPrefix() . $fileCorrect;
        $file = fopen($url, 'a');
        foreach ($readers as $key => $row) {
            if ($checkFail) {
                $arrError[] = trim($row) . ",-->" . __('messages.request_timeout');
                continue;
            }
            if (empty($row) || ($offset === '0' && $num === 1)) {
                $num++;
                continue;
            }
            $column = str_getcsv($row, ",");
            if (count($column) !== $totalColumn) {
                $arrError[] = trim($row) . ",-->" . __('messages.number_column_not_match');
                continue;
            }
            $column                        = array_map('trim', $column);
            $arrTemp                       = $column;
            $arrTemp                       = array_combine($arrCol, $column);
            $arrTemp['coupon_name']        = '"' . $arrTemp['coupon_name'] . '"';
            $arrTemp['imported_file_name'] = $realFile;
            $arrTemp['process_flg']        = 0;
            $arrTemp['is_deleted']         = 0;
            $arrTemp['created_at']         = now();
            fputs($file, mb_convert_encoding(implode(",", $arrTemp), 'Shift-JIS', 'UTF-8') . "\r\n");
        }
        fclose($file);
        $Common     = new Common;
        $nameCached = explode(".", $fileName);
        $nameCached = $nameCached[0];
        if (count($arrError) > 0) {
            if (!Cache::has($nameCached) && !$checkHead) {
                $header   = array_slice($arr, 0, 1);
                $arrError = array_merge($header, $arrError);
            }
            mb_convert_variables('SJIS', 'UTF-8', $arrError);
            $Common->updateCached($nameCached, implode("\r\n", $arrError));
        } elseif (Cache::has($nameCached)) {
            $Common->updateCached($nameCached, '');
        }
        return true;
    }

    /**
     * Process data in file csv for diy
     * @param   string  $readers file name in storage
     * @return  boolean
     */
    public function processAmazonPayCsv(
        $arr,
        $offset,
        $length,
        $fileName,
        $countErr = 0,
        $realFile = "",
        $fileCorrect = ""
    ) {

        $readers = array_slice($arr, $offset * $length, $length);
        if ($readers === null) {
            return false;
        }
        $dataMaster    = $arr[1];
        $arrDataMaster = str_getcsv($dataMaster, "\t");
        $isError       = false;
        $arrError      = array();
        if (is_array($arrDataMaster)) {
            $arrDataMaster = array_slice($arrDataMaster, 1, 5);
            if (count($arrDataMaster) !== 5) {
                return false;
            } else {
                foreach ($arrDataMaster as $key => $value) {
                    $arrDataMaster[$key] = trim($value);
                    if (empty($value)) {
                        return false;
                    }
                }
            }
        } else {
            return false;
        }
        $arrDataMaster = array_combine(
            [
                'settlement-start-date',
                'settlement-end-date',
                'deposit-date',
                'total-amount',
                'currency',
            ],
            $arrDataMaster
        );

        $arrCol      = Config::get('common.csv_uri_amazon_pay');
        $totalColumn = 24;
        $num         = 1;
        $checkHead   = false;
        $checkFail   = false;
        if ($countErr >= 3) {
            if ($offset === '0') {
                $checkHead = true;
            }
            $checkFail = true;
        }
        $url  = Storage::disk('local')->getDriver()->getAdapter()->getPathPrefix() . $fileCorrect;
        $file = fopen($url, 'a');
        foreach ($readers as $key => $row) {
            if ($isError) {
                $arrError[] = trim($row);
                continue;
            }
            if ($checkFail) {
                $arrError[] = trim($row) . ",-->" . __('messages.request_timeout');
                continue;
            }
            if (empty($row) || ($offset === '0' && ($num === 1 || $num === 2))) {
                $num++;
                continue;
            }
            $column = str_getcsv($row, "\t");
            if (count($column) !== $totalColumn) {
                $arrError[] = trim($row) . ",-->" . __('messages.number_column_not_match');
                continue;
            }
            $column                        = array_map('trim', $column);
            $arrTemp                       = $column;
            $arrTemp                       = array_combine($arrCol, $column);
            $arrTemp = array_merge($arrTemp, $arrDataMaster);
            $arrTemp['imported_file_name'] = $realFile;
            $arrTemp['process_flg']        = 0;
            $arrTemp['is_deleted']         = 0;
            $arrTemp['created_at']         = now();
            fputs($file, mb_convert_encoding(implode(",", $arrTemp), 'Shift-JIS', 'UTF-8') . "\r\n");
        }
        fclose($file);
        $Common     = new Common;
        $nameCached = explode(".", $fileName);
        $nameCached = $nameCached[0];
        if (count($arrError) > 0) {
            if (!Cache::has($nameCached) && !$checkHead) {
                $header   = array_slice($arr, 0, 1);
                $arrError = array_merge($header, $arrError);
            }
            mb_convert_variables('SJIS', 'UTF-8', $arrError);
            $Common->updateCached($nameCached, implode("\r\n", $arrError));
        } elseif (Cache::has($nameCached)) {
            $Common->updateCached($nameCached, '');
        }
        return true;
    }
    /**
     * Process data in file csv for diy
     * @param   string  $readers file name in storage
     * @return  boolean
     */
    public function processProPaygentCsv(
        $arr,
        $offset,
        $length,
        $fileName,
        $countErr = 0,
        $realFile = "",
        $fileCorrect = ""
    ) {
        $readers = array_slice($arr, $offset * $length, $length);
        if ($readers === null) {
            return false;
        }
        $arrCol      = Config::get('common.csv_uri_pro_paygent');
        $totalColumn = 10;
        $arrError    = array();
        $num         = 1;
        $checkHead   = false;
        $checkFail   = false;
        if ($countErr >= 3) {
            if ($offset === '0') {
                $checkHead = true;
            }
            $checkFail = true;
        }
        $url  = Storage::disk('local')->getDriver()->getAdapter()->getPathPrefix() . $fileCorrect;
        $file = fopen($url, 'a');
        foreach ($readers as $key => $row) {
            if ($checkFail) {
                $arrError[] = trim($row) . ",-->" . __('messages.request_timeout');
                continue;
            }
            if (empty($row) || ($offset === '0' && $num === 1)) {
                $num++;
                continue;
            }
            $column = str_getcsv($row, ",");
            if (count($column) !== $totalColumn) {
                $arrError[] = trim($row) . ",-->" . __('messages.number_column_not_match');
                continue;
            }
            $column                        = array_map('trim', $column);
            $arrTemp                       = $column;
            $arrTemp                       = array_combine($arrCol, $column);
            if ((int)$arrTemp['sales_type'] === 1) {
                $arrTemp['paid_price'] = -$arrTemp['paid_price'];
            }
            $arrTemp['order_id'] = 'DIY-' . $arrTemp['order_id'];
            $arrTemp['imported_file_name'] = $realFile;
            $arrTemp['process_flg']        = 0;
            $arrTemp['is_deleted']         = 0;
            $arrTemp['created_at']         = now();
            fputs($file, mb_convert_encoding(implode(",", $arrTemp), 'Shift-JIS', 'UTF-8') . "\r\n");
        }
        fclose($file);
        $Common     = new Common;
        $nameCached = explode(".", $fileName);
        $nameCached = $nameCached[0];
        if (count($arrError) > 0) {
            if (!Cache::has($nameCached) && !$checkHead) {
                $header     = array_slice($arr, 0, 1);
                $arrError = array_merge($header, $arrError);
            }
            mb_convert_variables('SJIS', 'UTF-8', $arrError);
            $Common->updateCached($nameCached, implode("\r\n", $arrError));
        } elseif (Cache::has($nameCached)) {
            $Common->updateCached($nameCached, '');
        }
        return true;
    }

    /**
     * Process data in file csv for diy
     * @param   string  $readers file name in storage
     * @return  boolean
     */
    public function processMfkCsv(
        $arr,
        $offset,
        $length,
        $fileName,
        $countErr = 0,
        $realFile = "",
        $fileCorrect = ""
    ) {
        $readers = array_slice($arr, $offset * $length, $length);
        if ($readers === null) {
            return false;
        }
        $arrCol      = Config::get('common.csv_uri_mfk');
        $totalColumn = 7;
        $arrError    = array();
        $num         = 1;
        $checkHead   = false;
        $checkFail   = false;
        if ($countErr >= 3) {
            if ($offset === '0') {
                $checkHead = true;
            }
            $checkFail = true;
        }
        $url  = Storage::disk('local')->getDriver()->getAdapter()->getPathPrefix() . $fileCorrect;
        $file = fopen($url, 'a');
        foreach ($readers as $key => $row) {
            if ($checkFail) {
                $arrError[] = trim($row) . ",-->" . __('messages.request_timeout');
                continue;
            }
            if (empty($row) || ($offset === '0' && $num === 1)) {
                $num++;
                continue;
            }
            $column = str_getcsv($row, ",");
            if (count($column) !== $totalColumn) {
                $arrError[] = trim($row) . ",-->" . __('messages.number_column_not_match');
                continue;
            }
            $column                        = array_map('trim', $column);
            $arrTemp                       = $column;
            $arrTemp                       = array_combine($arrCol, $column);
            $arrTemp['order_id']           = str_replace('DIY_', 'DIY-', $arrTemp['order_id']);
            $arrTemp['imported_file_name'] = $realFile;
            $arrTemp['process_flg']        = 0;
            $arrTemp['is_deleted']         = 0;
            $arrTemp['created_at']         = now();
            fputs($file, mb_convert_encoding(implode(",", $arrTemp), 'Shift-JIS', 'UTF-8') . "\r\n");
        }
        fclose($file);
        $Common     = new Common;
        $nameCached = explode(".", $fileName);
        $nameCached = $nameCached[0];
        if (count($arrError) > 0) {
            if (!Cache::has($nameCached) && !$checkHead) {
                $header     = array_slice($arr, 0, 1);
                $arrError = array_merge($header, $arrError);
            }
            mb_convert_variables('SJIS', 'UTF-8', $arrError);
            $Common->updateCached($nameCached, implode("\r\n", $arrError));
        } elseif (Cache::has($nameCached)) {
            $Common->updateCached($nameCached, '');
        }
        return true;
    }

    /**
     * Process data in file csv for diy
     * @param   string  $readers file name in storage
     * @return  boolean
     */
    public function processSagawaCsv(
        $arr,
        $offset,
        $length,
        $fileName,
        $countErr = 0,
        $realFile = "",
        $fileCorrect = ""
    ) {
        $readers = array_slice($arr, $offset * $length, $length);
        if ($readers === null) {
            return false;
        }
        $arrCol      = Config::get('common.csv_uri_sagawa');
        $totalColumn = 20;
        $arrError    = array();
        $num         = 1;
        $checkHead   = false;
        $checkFail   = false;
        if ($countErr >= 3) {
            if ($offset === '0') {
                $checkHead = true;
            }
            $checkFail = true;
        }
        $url  = Storage::disk('local')->getDriver()->getAdapter()->getPathPrefix() . $fileCorrect;
        $file = fopen($url, 'a');
        foreach ($readers as $key => $row) {
            if ($checkFail) {
                $arrError[] = trim($row) . ",-->" . __('messages.request_timeout');
                continue;
            }
            if (empty($row) || ($offset === '0' && $num === 1)) {
                $num++;
                continue;
            }
            $column = str_getcsv($row, ",");
            if (count($column) !== $totalColumn) {
                $arrError[] = trim($row) . ",-->" . __('messages.number_column_not_match');
                continue;
            }
            $column                        = array_map('trim', $column);
            $arrTemp                       = $column;
            $arrTemp                       = array_combine($arrCol, $column);
            $arrTemp['imported_file_name'] = $realFile;
            $arrTemp['process_flg']        = 0;
            $arrTemp['is_deleted']         = 0;
            if ($arrTemp['deposit_type'] === '振込対象外') {
                $arrTemp['is_deleted'] = 1;
            }
            $arrTemp['created_at']         = now();
            fputs($file, mb_convert_encoding(implode(",", $arrTemp), 'Shift-JIS', 'UTF-8') . "\r\n");
        }
        fclose($file);
        $Common     = new Common;
        $nameCached = explode(".", $fileName);
        $nameCached = $nameCached[0];
        if (count($arrError) > 0) {
            if (!Cache::has($nameCached) && !$checkHead) {
                $header     = array_slice($arr, 0, 1);
                $arrError = array_merge($header, $arrError);
            }
            mb_convert_variables('SJIS', 'UTF-8', $arrError);
            $Common->updateCached($nameCached, implode("\r\n", $arrError));
        } elseif (Cache::has($nameCached)) {
            $Common->updateCached($nameCached, '');
        }
        return true;
    }

    /**
     * Process data in file csv for diy
     * @param   string  $readers file name in storage
     * @return  boolean
     */
    public function processJpPostCsv(
        $arr,
        $offset,
        $length,
        $fileName,
        $countErr = 0,
        $realFile = "",
        $fileCorrect = ""
    ) {
        $readers = array_slice($arr, $offset * $length, $length);
        if ($readers === null) {
            return false;
        }
        $arrCol      = Config::get('common.csv_uri_jp_post');
        $totalColumn = 43;
        $arrError    = array();
        $num         = 1;
        $checkHead   = false;
        $checkFail   = false;
        if ($countErr >= 3) {
            if ($offset === '0') {
                $checkHead = true;
            }
            $checkFail = true;
        }
        $url  = Storage::disk('local')->getDriver()->getAdapter()->getPathPrefix() . $fileCorrect;
        $file = fopen($url, 'a');
        foreach ($readers as $key => $row) {
            if ($checkFail) {
                $arrError[] = trim($row) . ",-->" . __('messages.request_timeout');
                continue;
            }
            if (empty($row) || ($offset === '0' && $num === 1)) {
                $num++;
                continue;
            }
            $column = str_getcsv($row, ",");
            if (count($column) !== $totalColumn) {
                $arrError[] = trim($row) . ",-->" . __('messages.number_column_not_match');
                continue;
            }
            $column                        = array_map('trim', $column);
            $arrTemp                       = $column;
            $arrTemp                       = array_combine($arrCol, $column);
            $arrTemp['imported_file_name'] = $realFile;
            $arrTemp['process_flg']        = 0;
            $arrTemp['is_deleted']         = 0;
            $arrTemp['created_at']         = now();
            fputs($file, mb_convert_encoding(implode(",", $arrTemp), 'Shift-JIS', 'UTF-8') . "\r\n");
        }
        fclose($file);
        $Common     = new Common;
        $nameCached = explode(".", $fileName);
        $nameCached = $nameCached[0];
        if (count($arrError) > 0) {
            if (!Cache::has($nameCached) && !$checkHead) {
                $header     = array_slice($arr, 0, 1);
                $arrError = array_merge($header, $arrError);
            }
            mb_convert_variables('SJIS', 'UTF-8', $arrError);
            $Common->updateCached($nameCached, implode("\r\n", $arrError));
        } elseif (Cache::has($nameCached)) {
            $Common->updateCached($nameCached, '');
        }
        return true;
    }

    /**
     * Process data in file csv for diy
     * @param   string  $readers file name in storage
     * @return  boolean
     */
    public function processSeinouCsv(
        $arr,
        $offset,
        $length,
        $fileName,
        $countErr = 0,
        $realFile = "",
        $fileCorrect = ""
    ) {
        $readers = array_slice($arr, $offset * $length, $length);
        if ($readers === null) {
            return false;
        }
        $arrCol      = Config::get('common.csv_uri_seinou');
        $totalColumn = 23;
        $arrError    = array();
        $num         = 1;
        $checkHead   = false;
        $checkFail   = false;
        if ($countErr >= 3) {
            if ($offset === '0') {
                $checkHead = true;
            }
            $checkFail = true;
        }
        $url  = Storage::disk('local')->getDriver()->getAdapter()->getPathPrefix() . $fileCorrect;
        $file = fopen($url, 'a');
        foreach ($readers as $key => $row) {
            if ($checkFail) {
                $arrError[] = trim($row) . ",-->" . __('messages.request_timeout');
                continue;
            }
            if (empty($row) || ($offset === '0' && $num === 1)) {
                $num++;
                continue;
            }
            $column = str_getcsv($row, ",");
            if (isset($column[23])) {
                unset($column[23]);
            }
            if (count($column) !== $totalColumn) {
                $arrError[] = trim($row) . ",-->" . __('messages.number_column_not_match');
                continue;
            }
            $column                        = array_map('trim', $column);
            $arrTemp                       = $column;
            $arrTemp                       = array_combine($arrCol, $column);
            $arrTemp['imported_file_name'] = $realFile;
            $arrTemp['process_flg']        = 0;
            $arrTemp['is_deleted']         = 0;
            $arrTemp['created_at']         = now();
            fputs($file, mb_convert_encoding(implode(",", $arrTemp), 'Shift-JIS', 'UTF-8') . "\r\n");
        }
        fclose($file);
        $Common     = new Common;
        $nameCached = explode(".", $fileName);
        $nameCached = $nameCached[0];
        if (count($arrError) > 0) {
            if (!Cache::has($nameCached) && !$checkHead) {
                $header     = array_slice($arr, 0, 1);
                $arrError = array_merge($header, $arrError);
            }
            mb_convert_variables('SJIS', 'UTF-8', $arrError);
            $Common->updateCached($nameCached, implode("\r\n", $arrError));
        } elseif (Cache::has($nameCached)) {
            $Common->updateCached($nameCached, '');
        }
        return true;
    }

    /**
     * Process data after import.
     * @return $object json
     */
    public function processDataImport(Request $request)
    {
        if ($request->ajax() === true) {
            $type = $request->input("type", "");
            dispatch(new ProcessDataCsvUriMapping($type));
            return response()->json([
                'status' => 1,
            ]);
        }
    }

    /**
     * Process export excel
     * @return file
     */
    public function processExportExcel(Request $request)
    {
        $mallId   = $request->input('name_jp', null);
        $nameFile = [];
        if (!empty($mallId)) {
            $modelMall  = new MstMall();
            $nameFile[] = $modelMall->getMallName($mallId)->name_jp;
        }
        $orderDateFrom = $request->input('order_date_from', null);
        if (!empty($orderDateFrom)) {
            $nameFile[] = date('Ymd', strtotime($orderDateFrom));
        }
        $orderDateTo   = $request->input('order_date_to', null);
        if (!empty($orderDateTo)) {
            $nameFile[] =  date('Ymd', strtotime($orderDateTo));
        }
        if (empty($nameFile)) {
            $nameFile[] = date('Ymd');
        }
        return Excel::download(new UriExport($request), implode('_', $nameFile) . '.xlsx');
    }

    /**
     * Process save data
     * @param object $request
     * @return json
     */
    public function processSaveData(Request $request)
    {
        $modelDUM = new DtUriMapping();
        $field = $request->input('name', '');
        $receivedOrderId = $request->input('received_order_id', '');
        $arrUpdate = [];
        if (in_array($field, ['occur_reason', 'remarks'])) {
            $value = $request->input('value', '');
            $arrUpdate = [
                $field => $value
            ];
        } elseif ($field === 'match') {
            $arrUpdate['is_corrected']  = 1;
            $arrUpdate['finished_date'] = now();
        } elseif ($field === 'active') {
            $arrUpdate['is_corrected']  = 0;
        }
        $arrUpdate['up_ope_cd'] = Auth::user()->tantou_code;
        $arrUpdate['up_date']   = now();

        $modelDUM->updateData(
            ['received_order_id' => $receivedOrderId],
            $arrUpdate
        );
        return response()->json([
            'status' => 1,
        ]);
    }

    /**
     * Process save data
     * @param object $request
     * @return json
     */
    public function processSaveOption(Request $request)
    {
        $modelUOR = new MstUriOccurReason();
        $modelUR  = new MstUriRemarks();
        $name = $request->input('name');
        $value = $request->input('value-input', '');
        if (empty($value)) {
            return response()->json([
                'message' => 'Please input data.',
                'status'  => 0,
            ]);
        }
        if ($name === 'occur_reason') {
            $modelUOR->insert([
                'occur_reason_name' => $request->input('value-input'),
                'in_ope_cd' => Auth::user()->tantou_code,
                'in_date'   => now(),
                'up_ope_cd' => Auth::user()->tantou_code,
                'up_date'   => now(),
            ]);
        } else {
            $modelUR->insert([
                'remarks_name' => $request->input('value-input'),
                'in_ope_cd' => Auth::user()->tantou_code,
                'in_date'   => now(),
                'up_ope_cd' => Auth::user()->tantou_code,
                'up_date'   => now(),
            ]);
        }
        return response()->json([
            'status' => 1,
        ]);
    }
}