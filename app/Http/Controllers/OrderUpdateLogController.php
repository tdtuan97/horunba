<?php
/**
 * OrderUpdateLog Controller
 *
 * @package     App\Controllers
 * @subpackage  OrderUpdateLogController
 * @copyright   Copyright (c) 2017 RiverCrane! Corporation. All Rights Reserved.
 * @author      le.hung<le.hung.rcvn2012@gmail.com>
 */

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Backend\DtOrderUpdateLog;
use Validator;
use App\User;
use Auth;

class OrderUpdateLogController extends Controller
{

    /**
     * Get all data
     *
     * @param   $request  Request
     * @return json
     */
    public function getInfoLog(Request $request)
    {
        if ($request->ajax() === true) {
            $arraySearch = array(
                'receive_id' => $request->input('receive_id', null),
            );
            $model = new DtOrderUpdateLog();
            $data  = $model->getData($arraySearch);
            return response()->json([
                'data' => $data,
            ]);
        }
    }

    /**
     * Get log item
     *
     * @param  Request $request
     * @return object
     */
    public function getItem(Request $request)
    {
        if ($request->ajax() === true) {
            $model = new DtOrderUpdateLog();
            $logId = $request->input('log_id', '');
            $data  = $model->find($logId);
            return response()->json([
                'data' => $data,
            ]);
        }
    }

    /**
     * Add new memo
     *
     * @param  Request $request
     * @return json
     */
    public function addMemo(Request $request)
    {
        if ($request->ajax() === true) {
            $rules = [
                'receive_id'   => 'exists:mst_order,receive_id',
                'log_title'    => 'required|max:50',
                'log_contents' => 'required',
            ];
            $validator = Validator::make($request->all(), $rules);
            if ($validator->fails()) {
                return response()->json([
                    'status'  => 0,
                    'messages' => $validator->errors()
                ]);
            }

            $logId = $request->input('log_id', '');
            $dtOrderUpdateLog = new DtOrderUpdateLog();
            $dtOrderUpdateLog = $dtOrderUpdateLog->find($logId);
            if (empty($dtOrderUpdateLog)) {
                $dtOrderUpdateLog = new DtOrderUpdateLog();
                $dtOrderUpdateLog->log_status = 0;
                $dtOrderUpdateLog->in_ope_cd  = Auth::user()->tantou_code;
                $dtOrderUpdateLog->in_date    = date('Y-m-d H:i:s');
            }
            $dtOrderUpdateLog->receive_id   = $request->receive_id;
            $dtOrderUpdateLog->log_title    = $request->log_title;
            $dtOrderUpdateLog->log_contents = $request->log_contents;
            $dtOrderUpdateLog->up_ope_cd    = Auth::user()->tantou_code;
            $dtOrderUpdateLog->up_date      = date('Y-m-d H:i:s');

            if ($dtOrderUpdateLog->save()) {
                return response()->json([
                    'status'  => 1
                ]);
            }
            return response()->json([
                'status'  => -1,
                'messages' => 'Insert Error'
            ]);
        }
    }

    /**
     * Update log status
     *
     * @param  Request $request
     * @return json
     */
    public function updateStatus(Request $request)
    {
        $logId  = $request->input('log_id', '');
        $status = $request->input('log_status', '');
        $model  = new DtOrderUpdateLog();
        $log    = $model->find($logId);
        if (!empty($log) && in_array($status, [0, 1])) {
            $log->log_status = $status;
            $log->up_ope_cd    = Auth::user()->tantou_code;
            $log->up_date      = date('Y-m-d H:i:s');
            if ($log->save()) {
                return response()->json([
                    'status'  => 1
                ]);
            }
        }
        return response()->json([
            'status'  => 0,
            'messages' => 'Update log status fail'
        ]);
    }
}
