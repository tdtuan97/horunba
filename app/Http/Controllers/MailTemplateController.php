<?php
/**
 * MailTemplate Controller
 *
 * @package     App\Controllers
 * @subpackage  MailTemplateController
 * @copyright   Copyright (c) 2017 RiverCrane! Corporation. All Rights Reserved.
 * @author      le.hung<le.hung.rcvn2012@gmail.com>
 */

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Backend\MstGenre;
use App\Models\Backend\MstMailParameter;
use App\Models\Backend\MstMailTemplate;
use Validator;
use Storage;
use App\User;
use Auth;

class MailTemplateController extends Controller
{
    /**
     * Show template for append data
     *
     * @param   $request  Request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        return view('mail-template.index');
    }
    /**
     * Show data list view
     *
     * @param   $request  Request
     * @return json
     */
    public function dataList(Request $request)
    {
        if ($request->ajax() === true) {
            $arraySearch    = array(
                'mail_id'           => $request->input('mail_id', null),
                'per_page'          => $request->input('per_page', null),
                'mail_subject'      => $request->input('mail_subject', null),
                'mail_content'      => $request->input('mail_content', null),
                'genre'             => $request->input('genre', null),
                'template_name'     => $request->input('template_name', null),
            );
            $arraySort = [
                'mail_id'            => $request->input('sort_mail_id', null),
                'genre'              => $request->input('sort_genre', null),
                'sequence'              => $request->input('sort_sequence', null),
                'template_name'      => $request->input('sort_template_name', null),
                'mail_subject'       => $request->input('sort_mail_subject', null),
                'mail_content'       => $request->input('sort_mail_content', null),
                'attached_file_path' => $request->input('sort_attached_file_path', null),
                'in_date'            => $request->input('sort_in_date', null),
                'up_date'            => $request->input('sort_up_date', null)
            ];
            $model  = new MstMailTemplate();
            $data   = $model->getData($arraySearch, $arraySort);
            $mstGenre    = new MstGenre();
            foreach ($mstGenre->get() as $item) {
                $genreData[] = ['key' => $item['genre_id'], 'value' => $item['genre_name']];
            }
            return response()->json([
                        'data'      => $data,
                        'sort'      => $arraySort,
                        'sorted'    => ($request->input('sort')) ? true : false,
                        'params'    => $request->all(),
                        'genre'     => $genreData,
            ]);
        }
    }

    /**
     * Show information details
     *
     * @param   $request  Request
     * @return json
     */
    public function detail(Request $request)
    {
        if ($request->ajax() === true) {
            $modelMT  = new MstMailTemplate();
            $modelG   = new MstGenre();
            $modelP   = new MstMailParameter();
            $mailId   = $request->input('mail_id', null);
            if (!empty($mailId)) {
                $data = $modelMT->getItem($mailId);
            } else {
                $data = [];
            }
            $genreRes = $modelG->all();
            $genre    = [
                ['key' => 'none', 'value' => '-----']
            ];
            foreach ($genreRes as $item) {
                $genre[] = ['key' => $item->id, 'value' => $item->name];
            }
            $mailParam = $modelP->get(['para_code'])->pluck('para_code');

            return response()->json([
                        'data'      => $data,
                        'genre'     => $genre,
                        'mailParam' => $mailParam,
                        'params'    => $request->all()
            ]);
        }
        return view('mail-template.detail');
    }

    /**
     * Get information data to process
     *
     * @param   $request  Request
     * @return json
     */
    public function getFormData(Request $request)
    {
        if ($request->ajax() === true) {
            $modelMT  = new MstMailTemplate();
            $modelG   = new MstGenre();
            $modelP   = new MstMailParameter();
            $mailId   = $request->input('mail_id', null);
            $mailIdCopy = $request->input('mail_id_copy', null);
            if (!empty($mailId)) {
                $data = $modelMT->getItem($mailId);
            } elseif (!empty($mailIdCopy)) {
                $data = $modelMT->getItem($mailIdCopy);
                unset($data['mail_id']);
                unset($data['attached_file_path']);
            } else {
                $data = [];
            }
            $genreRes = $modelG->all();
            $genre    = [
                ['key' => 'none', 'value' => '-----']
            ];
            foreach ($genreRes as $item) {
                $genre[] = ['key' => $item->genre_id, 'value' => $item->genre_name];
            }
            $mailParam = $modelP->get(['para_code'])->pluck('para_code');

            return response()->json([
                        'data'      => $data,
                        'genre'     => $genre,
                        'mailParam' => $mailParam,
                        'params'    => $request->all()
            ]);
        }
    }
    
    /**
     * Get max genre
     *
     * @param  Request $request
     * @return json
     */
    public function getMaxSequence(Request $request)
    {
        if ($request->ajax() === true) {
            $mailId = $request->input('mail_id', null);
            $genre  = $request->input('genre', null);
            if (!empty($mailId)) {
                return json_encode([
                    'flg' => 0
                ]);
            }
            $modelMT = new MstMailTemplate();
            $maxSeq  = $modelMT->where('genre', $genre)->max('sequence');
            return json_encode([
                'flg' => 1,
                'data' => $maxSeq
            ]);
        }
    }

    /**
     * Create/Edit mail template
     *
     * @param   $request  Request
     * @return json
     */
    public function save(Request $request)
    {
        $mstMailTemplate = new MstMailTemplate();
        if ($request->ajax() === true) {
            $method = $request->input('method', '');

            Validator::extend(
                'checkUniqueMailTemplate',
                function ($parameters, $value, $attribute) use ($mstMailTemplate) {
                    return $mstMailTemplate->checkUniqueMailTemplate($value);
                },
                trans('validation.check_unique_mail_template')
            );

            $allFields = $request->all();
            $allFields['check_unique_mail_template'] = [
                'mail_id'       => $allFields['mail_id']??null,
                'genre'         => $allFields['genre']??'',
                'template_name' => $allFields['template_name']??'',
                'mail_subject'  => $allFields['mail_subject']??''
            ];

            $rules = [
                'check_unique_mail_template' => 'checkUniqueMailTemplate',
                'mail_id'       => 'numeric|exists:horunba.mst_mail_template,mail_id',
                'genre'         => 'required|numeric|exists:horunba.mst_genre,genre_id',
                'sequence'      => 'required|numeric',
                'template_name' => 'required',
                'mail_subject'  => 'required',
                'mail_content'  => 'required',
                'attached_file' => 'max:2048' //KB
            ];
            $validator = Validator::make($allFields, $rules);
            if ($validator->fails()) {
                return response()->json([
                    'status'  => 0,
                    'message' => $validator->errors(),
                    'data'    => []
                ]);
            }
            $inputFiles = $request->input('attached_file_path', '');
            $mallId     = $request->input('mail_id', '');
            if ($mallId !== '') {
                $mstMailTemplate = $mstMailTemplate->find($mallId);
                if (!empty($mstMailTemplate->attached_file_path) &&
                    ($inputFiles !== $mstMailTemplate->attached_file_path)) {
                    if (Storage::disk('attached_file')->exists($mstMailTemplate->attached_file_path)) {
                        Storage::disk('attached_file')->delete($mstMailTemplate->attached_file_path);
                    }
                    $mstMailTemplate->attached_file_path = null;
                }
            }
            $mstMailTemplate->genre              = $request->input('genre', '');
            $mstMailTemplate->template_name      = $request->input('template_name', '');
            $mstMailTemplate->mail_subject       = $request->input('mail_subject', '');
            $mstMailTemplate->mail_content       = $request->input('mail_content', '');
            $mstMailTemplate->sequence           = $request->input('sequence', '');
            $attachedFiles = [];
            if ($request->file('attached_file')) {
                $destinationPath = storage_path('uploads/attached_file');
                $attachedFile    = $request->file('attached_file');
                if (!empty($attachedFile)) {
                    $extension = $attachedFile->getClientOriginalExtension();
                    $filename = 'attached_file_' . microtime() . '.' . $extension;
                    $attachedFiles[] = $filename;
                    $attachedFile->move($destinationPath, $filename);
                    $mstMailTemplate->attached_file_path = implode("; ", $attachedFiles);
                }
            }

            if ($mallId === '') {
                $mstMailTemplate->in_ope_cd = Auth::user()->tantou_code;
                $mstMailTemplate->up_ope_cd = Auth::user()->tantou_code;
                $mstMailTemplate->in_date   = date('Y-m-d H:i:s');
                $mstMailTemplate->up_date   = date('Y-m-d H:i:s');
            } else {
                $mstMailTemplate->up_ope_cd = Auth::user()->tantou_code;
                $mstMailTemplate->up_date   = date('Y-m-d H:i:s');
            }
            $mstMailTemplate->save();
            return response()->json([
                'status'  => 1,
                'method'  => 'save',
                'mail_id' => $mstMailTemplate->mail_id,
                'message' => 'Save template success'
            ]);
        }
        return view('mail-template.save', $mstMailTemplate);
    }


    /**
     * Load content from db to iframe
     *
     * @param  Request $request
     * @return view
     */
    public function iframe(Request $request)
    {
        \Debugbar::disable();
        $modelMT  = new MstMailTemplate();
        $mailId   = $request->input('mail_id', null);
        $data = [];
        if (!empty($mailId)) {
            $data = $modelMT->getItem($mailId);
        }
        return view('iframe.iframe_mail_receive', ['content' => $data->mail_content]);
    }
}
