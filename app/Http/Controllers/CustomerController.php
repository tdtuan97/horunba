<?php


namespace App\Http\Controllers;


use App\Custom\Utilities;
use App\Models\Backend\DtDejeComment;
use App\Models\Backend\MstCustomer;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Auth;
use Validator;
use Storage;
use DB;
use Barryvdh\Debugbar\Facade as Debugbar;

class CustomerController extends Controller
{
    /**
     * Load view.
     */
    public function index()
    {
        return view('customer.index');
    }

    /**
     * Get all data.
     * Return $object json
     * @param Request $request
     * @return JsonResponse
     */
    public function dataList(Request $request)
    {
        if ($request->ajax() === true) {
            $modelCustomer = new MstCustomer();
            $arrSort = [
                'customer_id' => $request->input('sort_customer_id', null),
                'last_name_kana' => $request->input('sort_last_name_kana', null),
                'email' => $request->input('sort_email', null),
                'tel_num' => $request->input('sort_tel_num', null),
                'prefecture' => $request->input('sort_prefecture', null),
                'city' => $request->input('sort_city', null),
                'sub_address' => $request->input('sort_sub_address', null),
            ];
            $rule = [
                'customer_id' => 'nullable|numeric',
                'tel_num' => 'nullable|numeric',
            ];
            $arrSearch = [
                'per_page' => $request->input('per_page', null),
                'customer_id' => $request->input('customer_id', null),
                'last_name_kana' => $request->input('last_name_kana', null),
                'email' => $request->input('email', null),
                'tel_num' => $request->input('tel_num', null),
                'prefecture' => $request->input('prefecture', null),
                'city' => $request->input('city', null),
                'sub_address' => $request->input('sub_address', null),
            ];
            $validator = Validator::make($arrSearch, $rule);
            if ($validator->fails()) {
                $data = [];
            } else {
                $data = $modelCustomer->getDataCustomer($arrSearch, $arrSort);
            }
            return response()->json([
                'data' => $data,
            ]);
        }
    }

    /**
     * Delete data
     *
     * @param Request $request
     * @return mixed
     */
    public function delete(Request $request)
    {
        if ($request->ajax() === true) {
            $customerId = $request->customer_id;
            $modelCustomer = new MstCustomer();
            Debugbar::info($customerId);
            $modelCustomer->where('customer_id', $customerId)
                ->delete();
            return response()->json([
                'status' => 1,
            ]);
        }
    }

    /**
     * Get form data
     *
     * @param   $request  Request
     * @return  JsonResponse
     */
    public function getFormData(Request $request)
    {
        if ($request->ajax() === true) {
            $modelCustomer = new MstCustomer();
            $data = array();
            if ($request->get('customer_id') !== null) {
                $data = $modelCustomer->getCustomerById($request->get('customer_id'));
            }
            return response()->json([
                'data' => $data,
            ]);
        }
    }

    /**
     * Process display detail
     * @param   $request  Request
     * @return JsonResponse
     */
    public function save(Request $request)
    {
        if ($request->ajax() === true) {
            $modelCustomer = new MstCustomer();
            $accessFlg = $request->input('accessFlg', null);
            $rules = [
                'first_name' => 'required',
                'last_name' => 'required',
                'last_name_kana' => 'required',
                'first_name_kana' => 'required',
                'email' => 'required|email',
                'tel_num' => 'required|numeric',
            ];
            $validator = Validator::make($request->all(), $rules);
            if ($validator->fails()) {
                return response()->json([
                    'status' => 0,
                    'message' => $validator->errors(),
                    'data' => []
                ]);
            }
            if ($accessFlg === null) {
                $data = array(
                    'customer_id' => null,
                    'in_ope_cd' => Auth::user()->tantou_code,
                    'in_date' => date("Y-m-d H:i:s"),
                    'first_name' => $request->input('first_name', null),
                    'last_name' => $request->input('last_name', null),
                    'first_name_kana' => $request->input('first_name_kana', null),
                    'last_name_kana' => $request->input('last_name_kana', null),
                    'email' => $request->input('email', null),
                    'tel_num' => $request->input('tel_num', null),
                    'fax_num' => $request->input('fax_num', null),
                    'zip_code' => $request->input('zip_code', null),
                    'prefecture' => $request->input('prefecture', null),
                    'city' => $request->input('city', null),
                    'sub_address' => $request->input('sub_address', null),
                );
                try {
                    $modelCustomer->insert($data);
                    return response()->json([
                        'status' => 1,
                        'method' => 'save'
                    ]);
                } catch (\Exception $ex) {
                    return response()->json([
                        'status' => -1,
                        'message' => $ex->getMessage()
                    ]);
                }
            } else {
                $modelCustomer = $modelCustomer->find($request->input('customer_id'));
                $modelCustomer->up_ope_cd = Auth::user()->tantou_code;
                $modelCustomer->up_date = date("Y-m-d H:i:s");
                $modelCustomer->first_name = $request->input('first_name', null);
                $modelCustomer->first_name_kana = $request->input('first_name_kana', null);
                $modelCustomer->last_name = $request->input('last_name', null);
                $modelCustomer->last_name_kana = $request->input('last_name', null);
                $modelCustomer->email = $request->input('email', null);
                $modelCustomer->tel_num = $request->input('tel_num', null);
                $modelCustomer->fax_num = $request->input('fax_num', null);
                $modelCustomer->zip_code = $request->input('zip_code', null);
                $modelCustomer->prefecture = $request->input('prefecture', null);
                $modelCustomer->city = $request->input('city', null);
                $modelCustomer->sub_address = $request->input('sub_address', null);
                try {
                    $modelCustomer->save();
                } catch (\Exception $ex) {
                    return response()->json([
                        'status' => -1,
                        'message' => $ex->getMessage()
                    ]);
                }
            }
            $dataComment = null;
            $dataComment = $modelCustomer->getCustomerById(['customer_id' => $request->input('customer_id')]);
            return response()->json([
                'status' => 1,
                'dataComment' => $dataComment
            ]);
        }
        return view('customer.save');
    }

    /**
     * Process export data to csv
     *
     * @param   $request  Request
     * @return JsonResponse
     */
    public function processExportCsv(Request $request)
    {
        $modelCustomer = new MstCustomer();
        $arrSort = [
            'customer_id' => $request->input('sort_customer_id', null),
            'last_name_kana' => $request->input('sort_last_name_kana', null),
            'email' => $request->input('sort_email', null),
            'tel_num' => $request->input('sort_tel_num', null),
            'prefecture' => $request->input('sort_prefecture', null),
            'city' => $request->input('sort_city', null),
            'sub_address' => $request->input('sort_sub_address', null),
        ];
        $arrSearch = [
            'per_page' => $request->input('per_page', null),
            'customer_id' => $request->input('customer_id', null),
            'last_name_kana' => $request->input('last_name_kana', null),
            'email' => $request->input('email', null),
            'tel_num' => $request->input('tel_num', null),
            'prefecture' => $request->input('prefecture', null),
            'city' => $request->input('city', null),
            'sub_address' => $request->input('sub_address', null),
        ];
        $column = [
            'customer_id'            => 'Customer ID',
            'last_name_kana'         => 'Last Name Kana',
            'email'                  => 'Email',
            'tel_num'                => 'Tel Number',
            'prefecture'             => 'Prefecture',
            'city'                   => 'City',
            'sub_address'            => 'Sub Address'
        ];
        $fileName   = 'Customer-' . date('YmdHis') . ".csv";
        Storage::disk('local')->put($fileName, '');
        $url = Storage::disk('local')->getDriver()->getAdapter()->getPathPrefix() . $fileName;
        $file = fopen($url, 'a');
        fputs($file, mb_convert_encoding(implode(",", $column), 'Shift-JIS', 'UTF-8'). "\r\n");

        $query = $modelCustomer->select('*');
        if (count($arrSearch) > 0) {
            $query->where(function ($query) use ($arrSearch) {
                if (isset($arrSearch['customer_id'])) {
                    $query->where('customer_id', $arrSearch['customer_id']);
                }
                if (isset($arrSearch['last_name_kana'])) {
                    $query->where('last_name_kana', $arrSearch['last_name_kana']);
                }
                if (isset($arrSearch['email'])) {
                    $query->where('email', $arrSearch['email']);
                }
                if (isset($arrSearch['tel_num'])) {
                    $query->where('tel_num', $arrSearch['tel_num']);
                }
                if (isset($arrSearch['prefecture'])) {
                    $query->where('prefecture', $arrSearch['prefecture']);
                }
                if (isset($arrSearch['city'])) {
                    $query->where('city', $arrSearch['city']);
                }
                if (isset($arrSearch['sub_address'])) {
                    $query->where('sub_address', $arrSearch['sub_address']);
                }
            });
        }
        $check = false;
        if ($arrSort !== null && count($arrSort) > 0) {
            foreach ($arrSort as $column => $sort) {
                if (in_array($sort, ['asc', 'desc'])) {
                    $query->orderBy($column, $sort);
                    $check = true;
                }
            }
        }
        if (!$check) {
            $query->orderBy('customer_id', 'asc');
        }
        $total = $query->count();
        $perPage = 10000;
        $totalPage = ceil(1.0 * $total / $perPage);
        for ($i = 0; $i < $totalPage; $i++ )
        {
            $offset = $query->skip($i * $perPage)->take($perPage)->get();
            foreach ($offset as $key => $value) {
                $arrTemp = [];
                $arrTemp[] = $value->customer_id;
                $arrTemp[] = $value->last_name_kana;
                $arrTemp[] = $value->email;
                $arrTemp[] = $value->tel_num;
                $arrTemp[] = $value->prefecture;
                $arrTemp[] = $value->city;
                $arrTemp[] = $value->sub_address;

                $filter     = array("\r\n", "\n", "\r");
                $contentCsv = implode(",", $arrTemp);
                $contentCsv = str_replace($filter, '', $contentCsv);
                fputs($file, mb_convert_encoding($contentCsv, 'Shift-JIS', 'UTF-8'). "\r\n");
            }
        }
        fclose($file);
        return response()->json([
            'file_name' => $fileName,
            'status'     => 1,
        ]);
    }
}