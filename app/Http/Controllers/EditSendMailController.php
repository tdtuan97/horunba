<?php
/**
 * Controller for edit send mail
 *
 * @package    App\Http\Controllers
 * @subpackage EditSendMailController
 * @copyright  Copyright (c) 2018 RiverCrane! Corporation. All Rights Reserved.
 * @author     Van Qui<van.qui@rivercrane.vn>
 */

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Backend\MstGenre;
use App\Models\Backend\MstMailTemplate;
use App\Models\Backend\DtSendMailList;
use App\Models\Backend\MstOrder;
use App\Models\Backend\MstCustomer;
use App\Models\Backend\MstSettlementManage;
use App\Models\Backend\MstPaymentAccount;
use App\Models\Backend\DtReceiveMailList;
use App\Models\Backend\MstSignature;
use App\Console\Commands\ProcessMailQueue;
use App\Models\Backend\MstSupplier;
use Validator;
use Auth;
use Config;

class EditSendMailController extends Controller
{
    /**
     * Get form data
     *
     * @param  Request $request
     * @return ajax
     */
    public function getFormData(Request $request)
    {
        if ($request->ajax() === true) {
            $mstGenre        = new MstGenre();
            $dtReceiveML     = new DtReceiveMailList();
            $mstMailTemplate = new MstMailTemplate();
            $mstOrder        = new MstOrder();
            $mstSignature    = new MstSignature();

            $recMailIndex   = $request->input('rec_mail_index', null);
            $receiveOrderId = $request->input('receive_order_id', null);

            $resReceiveML = null;
            $resOrder     = $mstOrder->getDataByReceivedOrderId($receiveOrderId);

            $resReceiveML = $dtReceiveML->find($recMailIndex);
            if (empty($resOrder) && !empty($resReceiveML)) {
                $resOrder     = $mstOrder->getDataByReceivedOrderId($resReceiveML->receive_order_id);
            }
            $mailFrom  = Auth::user()->tantou_mail;
            if (!empty($resReceiveML)) {
                $mailTo    = $resReceiveML->mail_from;
                if (!empty($resOrder)) {
                    if ($resOrder->mall_id === 1) {
                        $mailFrom  = 'rakuten@diy-tool.com';
                    } else {
                        $mailFrom  = 'online@diyfactory.jp';
                    }
                }
                $mailToCc  = Config::get('mail')['default_mail']['cc'];
                $mailToBcc = Config::get('mail')['default_mail']['bcc'];
            } else {
                if (!empty($receiveOrderId)) {
                    $mstCustomer = new MstCustomer();
                    $resCustomer = $mstCustomer->find($resOrder->customer_id);
                    if (!empty($resCustomer) || empty($mailTo)) {
                        $mailTo = $resCustomer->email;
                    }
                    if (!empty($resOrder)) {
                        if ($resOrder->mall_id === 1) {
                            $mailFrom  = 'rakuten@diy-tool.com';
                        } else {
                            $mailFrom  = 'online@diyfactory.jp';
                        }
                    }
                } else {
                    $mailTo    = '';
                }
                $mailToCc  = Config::get('mail')['default_mail']['cc'];
                $mailToBcc =  Config::get('mail')['default_mail']['bcc'];
            }
            $genre = $request->input('genre', '');

            $genreData[] = ['key' => '', 'value' => '-----'];
            foreach ($mstGenre->get() as $item) {
                $genreData[] = ['key' => $item['genre_id'], 'value' => $item['genre_name']];
            }

            $mailId = $request->input('mail_id', '');

            $templateData[] = ['key' => '', 'value' => '-----'];
            foreach ($mstMailTemplate->getTemplateNameByGenre($genre) as $item) {
                $templateData[] = ['key' => $item['mail_id'], 'value' => $item['template_name']];
            }

            $data = $mstMailTemplate->find($mailId);
            $mailId           = '';
            $mailSubject      = '';
            $mailContent      = '';
            $reMailContent    = !empty($resReceiveML) ? $resReceiveML->mail_content : '';
            $receiveDate    = !empty($resReceiveML) ? $resReceiveML->receive_date : '';
            $arrUpdate        = [];
            $attachedFilePath = '';
            if (!empty($data)) {
                if (!empty($resOrder)) {
                    $dataSignature = $mstSignature->find($resOrder->mall_id);
                    $mailSubject = ProcessMailQueue::processContent(
                        $data->mail_subject,
                        $resOrder->payment_method,
                        $resOrder->received_order_id,
                        false
                    );

                    list($mailContent, $arrUpdate) = ProcessMailQueue::processContent(
                        $data->mail_content,
                        $resOrder->payment_method,
                        $resOrder->received_order_id,
                        true
                    );
                    if (!empty($dataSignature->signature_content)) {
                        $mailContent = $mailContent . "\r\n" . $dataSignature->signature_content;
                    }
                } else {
                    $mailSubject = $data->mail_subject;
                    $mailContent = $data->mail_content;
                }
                $attachedFilePath = $data->attached_file_path;
                $mailId = $data->mail_id;
            }

            if (!empty($recMailIndex) && $mailId === '') {
                $date  = date("Y-m-d H:m:s") . " $mailTo<br>";
                $strHr = "---------------------------------------------------------<br>";
                $mailContent = $reMailContent;
                if (strip_tags($reMailContent) === $reMailContent) {
                    $mailContent = str_replace(["\n", "\r\n"], "<br>", $reMailContent);
                }
                $mailContent = '<br><blockquote class="gmail_quote" style="margin:0 0 0 .8ex;'
                    . 'border-left:1px #ccc solid;padding-left:1ex"><u></u>'
                    . $date
                    . $strHr
                    . $mailContent
                    . '</blockquote>';
            } else {
                if (strip_tags($mailContent) === $mailContent) {
                    $mailContent = str_replace(["\n", "\r\n"], "<br>", $mailContent);
                }
            }

            return response()->json([
                'genreData'             => $genreData,
                'templateData'          => $templateData,
                'mail_id'               => $mailId,
                'mail_subject'          => $mailSubject,
                'mail_content'          => $mailContent,
                'receive_mail_content'  => $reMailContent,
                'attached_file_path'    => $attachedFilePath,
                'arrUpdate'             => $arrUpdate,
                'mail_to'               => $mailTo,
                'mail_from'             => $mailFrom,
                'mail_to_cc'            => $mailToCc,
                'mail_to_bcc'           => $mailToBcc,
            ]);
        }
    }

    /**
     * Process save receive input
     *
     * @param  object $request
     * @return json
     */
    public function save(Request $request)
    {
        if ($request->ajax() === true) {
            $receiveOrderId   = $request->input('receive_order_id', null);
            $receiveMailIndex = $request->input('rec_mail_index', null);
            $mailId           = $request->input('mail_id', '');
            $mailSubject      = $request->input('mail_subject', '');
            $mailContent      = $request->input('mail_content', '');
            $reMailContent    = $request->input('receive_mail_content', '');
            $mailTo           = $request->input('mail_to', '');
            $mailFrom         = $request->input('mail_from', '');
            $mailToCc         = $request->input('mail_to_cc', '');
            $mailToBcc        = $request->input('mail_to_bcc', '');
            $arrUpdate        = $request->input('arrUpdate', '');
            $chkShien        = $request->input('chk_shien', null);
            //First , we will process file input
            $inputFiles = $request->input('attached_file_path', '');
            $attachedFiles = [];
            if ($request->file('attached_file')) {
                $destinationPath = storage_path('uploads/attached_file');
                $attachedFile    = $request->file('attached_file');
                $rules = [
                    'attached_file'    => 'max:20480' //KB
                ];
                $validator = Validator::make($request->all(), $rules);
                if ($validator->fails()) {
                    return response()->json([
                        'status'  => 0,
                        'message' => $validator->errors(),
                        'data'    => []
                    ]);
                }
                if (!empty($attachedFile)) {
                    $extension = $attachedFile->getClientOriginalExtension();
                    $filename = 'attached_file_' . microtime() . '.' . $extension;
                    $attachedFiles[] = $filename;
                    $attachedFile->move($destinationPath, $filename);
                }
            } elseif ($inputFiles !== '' && $inputFiles !== 'null') {
                $attachedFiles[] = $inputFiles;
            }
            $mailContent = view('edit-send-mail.wrap_mail', ['content' => $mailContent])->render();
            // Add new if have not id
            if (empty($receiveOrderId) && empty($receiveMailIndex)) {
                $rules = [
                    'mail_subject' => 'required',
                    'mail_content' => 'required',
                ];
                if (empty($chkShien)) {
                    $rules['mail_to'] = 'required';
                }
                $validator = Validator::make($request->all(), $rules);
                if ($validator->fails()) {
                    return response()->json([
                        'status'  => 0,
                        'message' => $validator->errors(),
                        'data'    => []
                    ]);
                }
                try {
                    $dataAddress = [[
                        'mail_address_se_to' => $mailTo,
                        'mail_address_se_cc' => $mailToCc,
                    ]];
                    if (!empty($chkShien)) {
                        $modelMS = new MstSupplier();
                        $dataSendTo = $modelMS->getDataSendMail();
                        if ($dataSendTo->count() !== 0) {
                            $dataAddress = $dataSendTo->toArray();
                        }
                    }
                    foreach ($dataAddress as $value) {
                        $dtSendMailList = new DtSendMailList();
                        $maxIndex       = (int)$dtSendMailList->getMaxSendIndex([
                            'receive_order_id'      => 'ope_send_mail',
                        ]) + 1;
                        $dtSendMailList->receive_order_id    = 'ope_send_mail';
                        $dtSendMailList->order_status_id     = ORDER_STATUS['NEW'];
                        $dtSendMailList->order_sub_status_id = ORDER_SUB_STATUS['NEW'];
                        $dtSendMailList->operater_send_index = $maxIndex;
                        $dtSendMailList->mail_id             = $mailId;
                        $dtSendMailList->mail_subject        = $mailSubject;
                        $dtSendMailList->mail_content        = $mailContent;
                        $dtSendMailList->attached_file_path  = implode("; ", $attachedFiles);
                        $dtSendMailList->mail_to             = $value['mail_address_se_to'];
                        $dtSendMailList->mail_from           = $mailFrom;
                        $dtSendMailList->mail_cc             = $value['mail_address_se_cc'];
                        $dtSendMailList->mail_bcc            = $mailToBcc;
                        $dtSendMailList->send_status         = 0;
                        $dtSendMailList->send_type           = 2;
                        $dtSendMailList->in_date             = date('Y-m-d H:i:s');
                        $dtSendMailList->in_ope_cd           = Auth::user()->tantou_code;
                        $dtSendMailList->up_date             = date('Y-m-d H:i:s');
                        $dtSendMailList->up_ope_cd           = Auth::user()->tantou_code;
                        $dtSendMailList->save();
                    }
                } catch (\Exception $ex) {
                    return response()->json([
                        'status'  => -1,
                        'message' => $ex->getMessage()
                    ]);
                }
                return response()->json([
                    'status'  => 1,
                ]);
            }
            //Process Validation
            if (!empty($receiveOrderId)) {
                $rules = [
                    'receive_order_id' => 'exists:horunba.mst_order,received_order_id',
                    'mail_subject'     => 'required',
                    'mail_content'     => 'required'
                ];
            }
            if (!empty($receiveMailIndex)) {
                $rules = [
                    'rec_mail_index'   => 'exists:horunba.dt_receive_mail_list,rec_mail_index',
                    'mail_subject'     => 'required',
                    'mail_content'     => 'required'
                ];
            }
            $validator = Validator::make($request->all(), $rules);
            if ($validator->fails()) {
                return response()->json([
                    'status'  => 0,
                    'message' => $validator->errors(),
                    'data'    => []
                ]);
            }
            //Then If receive_order_id is exist
            $mstOrder    = new MstOrder();
            $modelPA     = new MstPaymentAccount();
            $mstCustomer = new MstCustomer();
            $resOrder    = $mstOrder->getDataByReceivedOrderId($receiveOrderId);
            if (!empty($resOrder)) {
                if (!empty($arrUpdate)) {
                    $arrUpdate = json_decode($arrUpdate, true);
                }
                try {
                    $dtSendMailList = new DtSendMailList();
                    $maxIndex       = (int)$dtSendMailList->getMaxSendIndex([
                        'order_status_id'       => (int)$resOrder->order_status,
                        'order_sub_status_id'   => (int)$resOrder->order_sub_status,
                        'receive_order_id'      => $receiveOrderId,
                    ]) + 1;
                    $receiveMailIndex = $request->input('rec_mail_index', null);
                    $dtReceiveMail    = new DtReceiveMailList();
                    $mailInfo         = $dtReceiveMail->getDataByMailIndex($receiveMailIndex);
                    if (!empty($mailInfo)) {
                        $mailInfo->is_reply = 1;
                        $mailInfo->save();
                        $dtSendMailList->uid_mail = $mailInfo->uid_mail;
                    }
                    $dtSendMailList->receive_order_id    = $receiveOrderId;
                    $dtSendMailList->order_status_id     = $resOrder->order_status;
                    $dtSendMailList->order_sub_status_id = $resOrder->order_sub_status;
                    $dtSendMailList->payment_method      = $resOrder->payment_method;
                    $dtSendMailList->operater_send_index = $maxIndex;
                    $dtSendMailList->mall_id             = $resOrder->mall_id;
                    $dtSendMailList->mail_id             = $mailId;
                    $dtSendMailList->mail_subject        = $mailSubject;
                    $dtSendMailList->mail_content        = $mailContent;
                    $dtSendMailList->attached_file_path  = implode("; ", $attachedFiles);
                    $dtSendMailList->mail_to             = $mailTo;
                    $dtSendMailList->mail_from           = $mailFrom;
                    $dtSendMailList->mail_cc             = $mailToCc;
                    $dtSendMailList->mail_bcc            = $mailToBcc;
                    $dtSendMailList->send_status         = 0;
                    $dtSendMailList->send_type           = 2;
                    $dtSendMailList->in_date             = date('Y-m-d H:i:s');
                    $dtSendMailList->in_ope_cd           = Auth::user()->tantou_code;
                    $dtSendMailList->up_date             = date('Y-m-d H:i:s');
                    $dtSendMailList->up_ope_cd           = Auth::user()->tantou_code;
                    $dtSendMailList->save();
                    if (count($arrUpdate) !== 0) {
                        if ($arrUpdate['flg_payment']) {
                            $modelPA->updateData(
                                ['name' => $arrUpdate['name']],
                                ['current_account' => $arrUpdate['current_account']]
                            );
                        }
                        $mstOrder->updateData(
                            ['receive_id' => $arrUpdate['receive_id']],
                            [
                                'payment_account'      => $arrUpdate['payment_account'],
                                'payment_request_date' => $arrUpdate['payment_request_date'],
                            ]
                        );
                    }

                } catch (\Exception $ex) {
                    return response()->json([
                        'status'  => -1,
                        'message' => $ex->getMessage()
                    ]);
                }
            }
            //If rec_mail_index is exist
            $receiveMailIndex = $request->input('rec_mail_index', null);
            $dtReceiveMail    = new DtReceiveMailList();
            $mailInfo         = $dtReceiveMail->getDataByMailIndex($receiveMailIndex);
            if (!empty($mailInfo) && empty($receiveOrderId)) {
                try {
                    $dtSendMailList = new DtSendMailList();
                    $maxIndex       = (int)$dtSendMailList->getMaxSendIndex([
                        'receive_order_id'      => 'ope_send_mail',
                    ]) + 1;
                    $dtSendMailList->receive_order_id    = 'ope_send_mail';
                    $dtSendMailList->order_status_id     = ORDER_STATUS['NEW'];
                    $dtSendMailList->order_sub_status_id = ORDER_SUB_STATUS['NEW'];
                    $dtSendMailList->operater_send_index = $maxIndex;
                    $dtSendMailList->mail_id             = $mailId;
                    $dtSendMailList->mail_subject        = $mailSubject;
                    $dtSendMailList->mail_content        = $mailContent;
                    $dtSendMailList->attached_file_path  = implode("; ", $attachedFiles);
                    $dtSendMailList->mail_to             = $mailTo;
                    $dtSendMailList->mail_from           = $mailFrom;
                    $dtSendMailList->mail_cc             = $mailToCc;
                    $dtSendMailList->mail_bcc            = $mailToBcc;
                    $dtSendMailList->send_status         = 0;
                    $dtSendMailList->send_type           = 2;
                    $dtSendMailList->in_date             = date('Y-m-d H:i:s');
                    $dtSendMailList->in_ope_cd           = Auth::user()->tantou_code;
                    $dtSendMailList->up_date             = date('Y-m-d H:i:s');
                    $dtSendMailList->up_ope_cd           = Auth::user()->tantou_code;
                    $dtSendMailList->uid_mail            = $mailInfo->uid_mail;
                    $dtSendMailList->save();
                    // Update mail reply
                    $mailInfo->is_reply                  = 1;
                    $mailInfo->save();
                } catch (\Exception $ex) {
                    return response()->json([
                        'status'  => -1,
                        'message' => $ex->getMessage()
                    ]);
                }
            }
            return response()->json([
                'status' => 1,
            ]);
        }
        return view('edit-send-mail.save');
    }

    // /**
    //  * Process content email.
    //  * @param $mailContent string content can process
    //  * @param $paymentCode int payment menthod code
    //  * @param $receivedOrderId string order id
    //  * @return list array
    //  */
    // public function processContent($mailContent, $paymentCode, $receivedOrderId, $check)
    // {
    //     $modelO   = new MstOrder();
    //     $modelSM  = new MstSettlementManage();
    //     $modelPA  = new MstPaymentAccount();
    //     $arrParas = [];
    //     preg_match_all("/\[###[^\]]*\]/", $mailContent, $arrParas);
    //     $arrReplace     = [];
    //     $datas          = $modelO->getInfoOrderMail($receivedOrderId)->toArray();
    //     $arrUpdate = [];
    //     foreach ($arrParas[0] as $para) {
    //         $subContent = '';
    //         if ($para === '[###決済方法別テンプレ]') {
    //             $dataCase = $modelSM->getDataByPaymentCode($paymentCode);
    //             if ($dataCase->have_para === 1) {
    //                 $arrSubParas = [];
    //                 preg_match_all("/\[###[^\]]*\]/", $dataCase->add_2_mail_text, $arrSubParas);
    //                 $arrSubReplace = [];
    //                 foreach ($arrSubParas[0] as $arrSubPara) {
    //                     if ($paymentCode === 6) {
    //                         $arrUpdate = [
    //                             'flg_payment'          => false,
    //                             'receive_id'           => $datas['receive_id'],
    //                             'payment_account'      => '1339284',
    //                             'payment_request_date' => date("Y/m/d"),
    //                         ];
    //                         $datas['payment_account'] = '1339284';
    //                     } elseif ($arrSubPara === '[###入金口座]') {
    //                         if (!empty($datas['payment_account'])) {
    //                             $paymentAccount = $datas['payment_account'];
    //                         } else {
    //                             $paymentAccount = 0;
    //                             $arrAcc = $modelPA->first();
    //                             if ($arrAcc->current_account >= $arrAcc->min_number_account &&
    //                                 $arrAcc->current_account < $arrAcc->max_number_account) {
    //                                 $paymentAccount = $arrAcc->current_account + 1;
    //                             } elseif ($arrAcc->current_account === $arrAcc->max_number_account) {
    //                                 $paymentAccount = $arrAcc->min_number_account;
    //                             }
    //                             $arrUpdate = [
    //                                 'flg_payment'          => true,
    //                                 'name'                 => $arrAcc->name,
    //                                 'current_account'      => $paymentAccount,
    //                                 'receive_id'           => $datas['receive_id'],
    //                                 'payment_account'      => $paymentAccount,
    //                                 'payment_request_date' => date("Y/m/d"),
    //                             ];
    //                         }
    //                         $datas['payment_account'] = $paymentAccount;
    //                     }
    //                     $arrSubReplace[] = view('template-mail/template', [
    //                                         'param' => $arrSubPara,
    //                                         'datas' => $datas
    //                                     ])->render();
    //                 }
    //                 $subContent = str_replace($arrSubParas[0], $arrSubReplace, $dataCase->add_2_mail_text);
    //             } else {
    //                 $subContent = $dataCase->add_2_mail_text;
    //             }
    //         } elseif ($para === '[###送付先情報]') {
    //             $dataDetail  = $modelO->getInfoOrderDetail($receivedOrderId)->toArray();
    //             $contentTemp = '';
    //             $countDt = count($dataDetail);
    //             $newdData = [];
    //             foreach ($dataDetail as $key => $value) {
    //                 $deliveryType = $value['delivery_type'];
    //                 $suplierId    = $value['suplier_id'];
    //                 if (in_array($deliveryType, [1, 3])) {
    //                     $newdData[1]['ship_last_name']     = $value['ship_last_name'];
    //                     $newdData[1]['ship_first_name']    = $value['ship_first_name'];
    //                     $newdData[1]['ship_zip_code']      = $value['ship_zip_code'];
    //                     $newdData[1]['ship_prefecture']    = $value['ship_prefecture'];
    //                     $newdData[1]['ship_city']          = $value['ship_city'];
    //                     $newdData[1]['ship_sub_address']   = $value['ship_sub_address'];
    //                     $newdData[1]['ship_tel_num']       = $value['ship_tel_num'];
    //                     $newdData[1]['child'][] = [
    //                         'product_code'  => $value['product_code'],
    //                         'product_name'  => $value['product_name'],
    //                         'price'         => $value['price'],
    //                         'quantity'      => $value['quantity'],
    //                         'delivery_type' => $value['delivery_type']
    //                     ];
    //                 } else {
    //                     $newdData[$suplierId.$deliveryType]['ship_last_name']     = $value['ship_last_name'];
    //                     $newdData[$suplierId.$deliveryType]['ship_first_name']    = $value['ship_first_name'];
    //                     $newdData[$suplierId.$deliveryType]['ship_zip_code']      = $value['ship_zip_code'];
    //                     $newdData[$suplierId.$deliveryType]['ship_prefecture']    = $value['ship_prefecture'];
    //                     $newdData[$suplierId.$deliveryType]['ship_city']          = $value['ship_city'];
    //                     $newdData[$suplierId.$deliveryType]['ship_sub_address']   = $value['ship_sub_address'];
    //                     $newdData[$suplierId.$deliveryType]['ship_tel_num']       = $value['ship_tel_num'];
    //                     $newdData[$suplierId.$deliveryType]['child'][] = [
    //                         'product_code'  => $value['product_code'],
    //                         'product_name'  => $value['product_name'],
    //                         'price'         => $value['price'],
    //                         'quantity'      => $value['quantity'],
    //                         'delivery_type' => $value['delivery_type']
    //                     ];
    //                 }
    //             }
    //             $j = 0;
    //             if (count($newdData) > 0) {
    //                 $newdData1 = [];
    //                 if (isset($newdData[1])) {
    //                     $newdData1[] = $newdData[1];
    //                     unset($newdData[1]);
    //                 }

    //                 array_multisort(array_map('count', $newdData), SORT_ASC, $newdData);
    //                 krsort($newdData);
    //                 $finalArray = array_merge($newdData1, $newdData);
    //                 foreach ($finalArray as $key => $value) {
    //                     $j++;
    //                     $contentTemp .= view('template-mail/template', [
    //                         'param' => $para,
    //                         'datas' => $value,
    //                         'index' => $j,
    //                     ])->render();
    //                 }
    //             }
    //             $subContent = $contentTemp;
    //         // Process order not in dt_order_detail incase Mail thanks
    //         } elseif ($para === '[###送付先情報感謝]') {
    //             $dataDetail  = $modelO->getInfoNotInOrderDetail($receivedOrderId)->toArray();
    //             $newdData = [];
    //             foreach ($dataDetail as $key => $value) {
    //                 $newdData[1]['ship_last_name']     = $value['ship_last_name'];
    //                 $newdData[1]['ship_first_name']    = $value['ship_first_name'];
    //                 $newdData[1]['ship_zip_code']      = $value['ship_zip_code'];
    //                 $newdData[1]['ship_prefecture']    = $value['ship_prefecture'];
    //                 $newdData[1]['ship_city']          = $value['ship_city'];
    //                 $newdData[1]['ship_sub_address']   = $value['ship_sub_address'];
    //                 $newdData[1]['ship_tel_num']       = $value['ship_tel_num'];
    //                 $newdData[1]['child'][] = [
    //                     'product_code'  => $value['product_code'],
    //                     'product_name'  => $value['product_name'],
    //                     'price'         => $value['price'],
    //                     'quantity'      => $value['quantity']
    //                 ];
    //             }
    //             if (count($newdData) > 0) {
    //                 $subContent = view('template-mail/template', [
    //                     'param' => $para,
    //                     'datas' => $newdData[1]
    //                 ])->render();
    //             }
    //         } elseif ($para === '[###送付先情報欠品・廃番]') {
    //             $dataDetail   = $modelO->getOrderDetailMailQueue($receivedOrderId, $para)->toArray();
    //             $subContent = view('template-mail/template', [
    //                                 'param' => $para,
    //                                 'datas' => $dataDetail
    //                             ])->render();
    //         } elseif ($para === '[###送付先情報発送日案内]') {
    //             $dataDetails   = $modelO->getOrderDetailMailQueue($receivedOrderId, $para)->toArray();
    //             $dataOrders = [];
    //             foreach ($dataDetails as $dataDetail) {
    //                 if ($dataDetail['delivery_type'] === 1 || $dataDetail['delivery_type'] === 3) {
    //                     $key = '13';
    //                     if (!isset($dataOrders[$key]['delivery_date']) ||
    //                     strtotime($dataOrders[$key]['delivery_date']) < strtotime($dataDetail['delivery_date'])) {
    //                         $dataOrders[$key]['delivery_date'] = $dataDetail['delivery_date'];
    //                     }
    //                 } else {
    //                     $key = date("Ymd", strtotime($dataDetail['delivery_date']));
    //                 }
    //                 $dataOrders[$key]['ship_last_name']   = $dataDetail['ship_last_name'];
    //                 $dataOrders[$key]['ship_first_name']  = $dataDetail['ship_first_name'];
    //                 $dataOrders[$key]['ship_zip_code']    = $dataDetail['ship_zip_code'];
    //                 $dataOrders[$key]['ship_prefecture']  = $dataDetail['ship_prefecture'];
    //                 $dataOrders[$key]['ship_city']        = $dataDetail['ship_city'];
    //                 $dataOrders[$key]['ship_sub_address'] = $dataDetail['ship_sub_address'];
    //                 $dataOrders[$key]['ship_tel_num']     = $dataDetail['ship_tel_num'];
    //                 $dataOrders[$key]['delivery_date']    = $dataDetail['delivery_date'];
    //                 $productCode = $dataDetail['product_code'];
    //                 $dataOrders[$key]['product'][$productCode]['product_code'] = $productCode;
    //                 $dataOrders[$key]['product'][$productCode]['product_name'] = $dataDetail['product_name'];
    //                 $dataOrders[$key]['product'][$productCode]['price']        = $dataDetail['price'];
    //                 $dataOrders[$key]['product'][$productCode]['quantity']     = $dataDetail['quantity'];
    //             }
    //             $subContent = view('template-mail/template', [
    //                                 'param' => $para,
    //                                 'datas' => $dataOrders
    //                             ])->render();
    //         } else {
    //             $subContent = view('template-mail/template', [
    //                                     'param' => $para,
    //                                     'datas' => $datas
    //                                 ])->render();
    //         }

    //         $arrCheckSubParas = [];
    //         preg_match_all("/\[###[^\]]*\]/", $subContent, $arrCheckSubParas);
    //         if (count($arrCheckSubParas[0]) !== 0) {
    //             $arrSubReplace = [];
    //             foreach ($arrCheckSubParas[0] as $subPara) {
    //                 $arrSubReplace = view('template-mail/template', [
    //                     'param' => $subPara,
    //                     'datas' => $datas
    //                 ])->render();
    //             }
    //             $subContent = str_replace($arrCheckSubParas[0], $arrSubReplace, $subContent);
    //         }
    //         $arrReplace[] = $subContent;
    //     }
    //     if ($check) {
    //         return [str_replace($arrParas[0], $arrReplace, $mailContent), $arrUpdate];
    //     } else {
    //         return str_replace($arrParas[0], $arrReplace, $mailContent);
    //     }
    // }
}
