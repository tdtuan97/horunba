<?php
/**
 * Settlement Controller
 *
 * @package     App\Controllers
 * @subpackage  SettlementController
 * @copyright   Copyright (c) 2017 RiverCrane! Corporation. All Rights Reserved.
 * @author      lam.vinh<lam.vinh.rcvn2012@gmail.com>
 */

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Backend\MstSettlementManage;
use Validator;
use App\User;
use Auth;

class SettlementController extends Controller
{
    /**
     * Show the application settlement.
     *
     * @param   $request  Request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        return view('settlement.index');
    }

    /**
     * Show the application settlement.
     *
     * @param   $request  Request
     * @return \Illuminate\Http\Response
     */
    public function dataList(Request $request)
    {
        if ($request->ajax() === true) {
            $arraySearch    = array(
                'payment_request' => $request->input('payment_check', null),
                'per_page'        => $request->input('per_page', null)
            );
            $model  = new MstSettlementManage();
            $data   = $model->getData($arraySearch);
            return response()->json([
                'data'  => $data
            ]);
        }
    }

    /**
     * Change status function
     *
     * @param   $request  Request
     * @return json
     */

    public function changeStatus(Request $request)
    {
        if ($request->ajax() === true) {
            $params  = $request->all();
            $model = new MstSettlementManage();
            $keys['payment_code']  = $params['payment_code'];
            $data['up_ope_cd']     = Auth::user()->tantou_code;
            $data[$params['type']] = $params['payment_checkbox'];
            $process = $model->changeStatus($keys, $data);
            return response()->json([
                'status'  => $process
            ]);
        }
    }

    /**
     * Get information data to process
     *
     * @param   $request  Request
     * @return json
     */
    public function getFormData(Request $request)
    {
        if ($request->ajax() === true) {
            $mstSettlement      = new MstSettlementManage();
            $paymentCode = $request->input('payment_code', null);
            if (!empty($paymentCode)) {
                $data = $mstSettlement->getItem($paymentCode);
            } else {
                $data = [];
            }
            return response()->json([
                'data'      => $data
            ]);
        }
    }

    /**
     * Add or edit settlement
     *
     * @param   $request  Request
     * @return mixed
     */
    public function save(Request $request)
    {
        if ($request->ajax() === true) {
//            echo '<pre style="color:red">';
//            print_r($request->all());
//            echo '</pre>';
//            die();
            $mstSettlement = new MstSettlementManage();
            $rules = [
                'payment_code'       => 'numeric|exists:horunba.mst_settlement_manage,payment_code',
                'payment_name'       => 'required',
                'payment_request'    => 'required|numeric',
                'add_2_mail_text'    => 'required',
                'have_para'          => 'required|numeric'
            ];
            $validator = Validator::make($request->all(), $rules);
            if ($validator->fails()) {
                return response()->json([
                    'status'  => false,
                    'method'  => 'save',
                    'message' => $validator->errors()
                ]);
            }
            $paymentCode = $request->input('payment_code', '');
            if ($paymentCode !== '') {
                $mstSettlement            = $mstSettlement->find($paymentCode);
                $mstSettlement->up_ope_cd = Auth::user()->tantou_code;
                $mstSettlement->up_date   = date('Y-m-d H:i:s');
            } else {
                $mstSettlement->in_ope_cd = Auth::user()->tantou_code;
                $mstSettlement->in_date   = date('Y-m-d H:i:s');
                $mstSettlement->up_ope_cd = Auth::user()->tantou_code;
                $mstSettlement->up_date   = date('Y-m-d H:i:s');
            }
            $mstSettlement->payment_name       = $request->input('payment_name', '');
            $mstSettlement->payment_request    = (int)$request->input('payment_request', 0);
            $mstSettlement->add_2_mail_text    = $request->input('add_2_mail_text', '');
            if ($request->input('add_2_receipt_text', '') !== 'null') {
                 $mstSettlement->add_2_receipt_text = $request->input('add_2_receipt_text', '');
            }
            $mstSettlement->have_para          = (int)$request->input('have_para', 0);
            $result = $mstSettlement->save();
            return response()->json([
                'status'  => $result,
                'method'  => 'save'
            ]);
        }
        return view('settlement.save');
    }
}
