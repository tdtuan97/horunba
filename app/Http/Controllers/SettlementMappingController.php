<?php
/**
 * Controller for batch management
 *
 * @package    App\Http\Controllers
 * @subpackage SettlementMappingController
 * @copyright  Copyright (c) 2018 RiverCrane! Corporation. All Rights Reserved.
 * @author     Truong Nghia<truong.van.nghia@rivercrane.vn>
 */
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Backend\MstOrder;
use App\Models\Backend\DtPaymentMapping;
use App\Models\Backend\MstMall;
use App\Models\Backend\DtWebRakData;
use App\Models\Backend\MstPaymentMethod;
use App\Models\Backend\DtPaymentDiyFregiData;
use App\Models\Backend\DtPaymentBizFregiData;
use App\Models\Backend\MstSettlementManage;
use App\Models\Backend\MstOrderStatus;
use App\Models\Backend\DtPaymentProMfkData;
use App\Models\Backend\DtPaymentProPaygentData;
use App\Http\Controllers\Common;
use App\Helpers\Helper;
use Config;
use Storage;
use Cache;
use DB;
use Auth;
use Validator;
use App;

class SettlementMappingController extends Controller
{
    /**
     * The limit of step process.
     *
     * @var int
     */
    protected $limit = 50;

    /**
     * Load view.
     */
    public function index()
    {
        return view('settlement-mapping.index');
    }

    /**
     * Show data list view
     *
     * @param   $request  Request
     * @return json
     */
    public function dataList(Request $request)
    {
        if ($request->ajax() === true) {
            $arrSearch    = array(
                'mall_id'         => $request->input('name_jp', null),
                'received_order_id' => $request->input('received_order_id', null),
                'order_date_from' => $request->input('order_date_from', null),
                'order_date_to'   => $request->input('order_date_to', null),
                'sold_date_from'  => $request->input('sold_date_from', null),
                'sold_date_to'    => $request->input('sold_date_to', null),
                'diferent_type'   => $request->input('diferent_type', null),
                'process_content' => $request->input('process_content', null),
                'per_page'        => $request->input('per_page', null),

                'setlement_hrnb'     => $request->input('setlement_hrnb', null),
                'setlement_web'      => $request->input('setlement_web', null),
                'request_price_hrnb' => $request->input('request_price_hrnb', null),
                'request_price_web'  => $request->input('request_price_web', null),
                'different_price'    => $request->input('different_price', null),
                'status_hrnb'        => $request->input('status_hrnb', null),
                'status_web'         => $request->input('status_web', null),
                'is_correct'         => $request->input('is_correct', null),
            );
            $arrSort = [
                'name_jp'            => $request->input('sort_name_jp', null),
                'received_order_id'  => $request->input('sort_received_order_id', null),
                'order_date'         => $request->input('sort_order_date', null),
                'delivery_date'      => $request->input('sort_delivery_date', null),
                'payment_method'     => $request->input('sort_payment_method', null),
                'web_payment_method' => $request->input('sort_web_payment_method', null),
                'request_price'      => $request->input('sort_request_price', null),
                'web_request_price'  => $request->input('sort_web_request_price', null),
                'different_price'    => $request->input('sort_different_price', null),
                'order_status'       => $request->input('sort_order_status', null),
                'web_order_status'   => $request->input('sort_web_order_status', null),
            ];

            $modelMall = new MstMall();
            $mallData  = $modelMall->getData();

            $modelSM = new MstSettlementManage();
            $smData  = [];
            foreach ($modelSM->get(['payment_code', 'payment_name']) as $item) {
                $smData[] = ['key' => $item['payment_code'], 'value' => $item['payment_name']];
            }

            $modelOS = new MstOrderStatus();
            $osData  = [];
            foreach ($modelOS->get(['order_status_id', 'status_name']) as $item) {
                $osData[] = ['key' => $item['order_status_id'], 'value' => $item['status_name']];
            }

            $rules = [
                'request_price_hrnb' => 'numeric',
                'request_price_web'  => 'numeric',
                'different_price'    => 'numeric'
            ];
            $validator = Validator::make($request->all(), $rules);
            if ($validator->fails()) {
                return response()->json([
                    'data'          => [],
                    'mallData'      => $mallData->toArray(),
                    'differentData' => Config::get('common.diferent_type'),
                    'processData'   => Config::get('common.process_content'),
                    'occurredData'  => Config::get('common.occurred_reason'),
                    'smData'        => $smData,
                    'osData'        => $osData,
                    'message' => $validator->errors()
                ]);
            }

            $model = new DtPaymentMapping();
            if (!empty($request->input('is_corrected_all'))) {
                $model->updateAll($arrSearch, null, ['is_corrected' => 1]);
                return response()->json([
                    'status' => 1
                ]);
            }

            if (!empty($request->input('occorred_reason_all'))) {
                $occorredReason = (int) $request->input('occorred_reason_all');
                $model->updateAll($arrSearch, null, ['occorred_reason' => $occorredReason]);
                return response()->json([
                    'status' => 1
                ]);
            }

            $processAll = $request->input('process_all', null);
            if (!empty($processAll)) {
                $model->updateAll($arrSearch, null, ['process_content' => $processAll]);
                return response()->json([
                    'status' => 1
                ]);
            }
            $type       = $request->input('type_export', null);
            if (!is_null($type)) {
                $data = $model->getData($arrSearch, $arrSort, ['type' => $type]);
            } else {
                $data = $model->getData($arrSearch, $arrSort);
            }
            
            $diyLink = "https://shop.diyfactory.jp/admin/index.php?route=sale/order/info&order_id=%s";
            if (App::environment(['local', 'test'])) {
                $diyLink = "http://naruto.daitotest.tk/admin/index.php?route=sale/order/info&order_id=%s";
            }
            $detailLink = action('OrderManagementController@detail') . "?receive_id_key=%s";
            
            foreach ($data as $item) {
                $link = sprintf($detailLink, $item->receive_id);
                $linkWeb = '#';
                if ($item->mall_id === 8) {
                    $receivedOrderId = str_replace("DIY-", "", $item->received_order_id);
                    $linkWeb = sprintf($diyLink, $receivedOrderId);
                }
                $item['link'] = $link;
                $item['link_web'] = $linkWeb;
            }
            
            return response()->json([
                'data'            => $data,
                'mallData'        => $mallData->toArray(),
                'differentData'   => Config::get('common.diferent_type'),
                'processData'     => Config::get('common.process_content'),
                'occurredData'    => Config::get('common.occurred_reason'),
                'isCorrectedData' => Config::get('common.flg'),
                'smData'          => $smData,
                'osData'          => $osData,
            ]);
        }
    }

    /**
     * Show data list view
     *
     * @param   $request  Request
     * @return json
     */
    public function processUpdateAll(Request $request)
    {
        if ($request->ajax() === true) {
            $arrSearch    = array(
                'mall_id'         => $request->input('name_jp', null),
                'received_order_id' => $request->input('received_order_id', null),
                'order_date_from' => $request->input('order_date_from', null),
                'order_date_to'   => $request->input('order_date_to', null),
                'sold_date_from'  => $request->input('sold_date_from', null),
                'sold_date_to'    => $request->input('sold_date_to', null),
                'diferent_type'   => $request->input('diferent_type', null),
                'process_content' => $request->input('process_content', null),
                'per_page'        => $request->input('per_page', null),

                'setlement_hrnb'     => $request->input('setlement_hrnb', null),
                'setlement_web'      => $request->input('setlement_web', null),
                'request_price_hrnb' => $request->input('request_price_hrnb', null),
                'request_price_web'  => $request->input('request_price_web', null),
                'different_price'    => $request->input('different_price', null),
                'status_hrnb'        => $request->input('status_hrnb', null),
                'status_web'         => $request->input('status_web', null),
                'is_correct'         => $request->input('is_correct', null),
            );
            $arrSort = [
                'name_jp'            => $request->input('sort_name_jp', null),
                'received_order_id'  => $request->input('sort_received_order_id', null),
                'order_date'         => $request->input('sort_order_date', null),
                'delivery_date'      => $request->input('sort_delivery_date', null),
                'payment_method'     => $request->input('sort_payment_method', null),
                'web_payment_method' => $request->input('sort_web_payment_method', null),
                'request_price'      => $request->input('sort_request_price', null),
                'web_request_price'  => $request->input('sort_web_request_price', null),
                'different_price'    => $request->input('sort_different_price', null),
                'order_status'       => $request->input('sort_order_status', null),
                'web_order_status'   => $request->input('sort_web_order_status', null),
            ];

            $modelMall = new MstMall();
            $mallData  = $modelMall->getData();

            $modelSM = new MstSettlementManage();
            $smData  = [];
            foreach ($modelSM->get(['payment_code', 'payment_name']) as $item) {
                $smData[] = ['key' => $item['payment_code'], 'value' => $item['payment_name']];
            }

            $modelOS = new MstOrderStatus();
            $osData  = [];
            foreach ($modelOS->get(['order_status_id', 'status_name']) as $item) {
                $osData[] = ['key' => $item['order_status_id'], 'value' => $item['status_name']];
            }

            $rules = [
                'request_price_hrnb' => 'numeric',
                'request_price_web'  => 'numeric',
                'different_price'    => 'numeric'
            ];
            $validator = Validator::make($request->all(), $rules);
            if ($validator->fails()) {
                return response()->json([
                    'data'          => [],
                    'mallData'      => $mallData->toArray(),
                    'differentData' => Config::get('common.diferent_type'),
                    'processData'   => Config::get('common.process_content'),
                    'occurredData'  => Config::get('common.occurred_reason'),
                    'smData'        => $smData,
                    'osData'        => $osData,
                    'message' => $validator->errors()
                ]);
            }

            $model = new DtPaymentMapping();
            if (!empty($request->input('is_corrected_all'))) {
                $model->updateAll($arrSearch, null, ['is_corrected' => 1]);
                return response()->json([
                    'status' => 1
                ]);
            }

            if (!empty($request->input('occorred_reason_all'))) {
                $occorredReason = (int) $request->input('occorred_reason_all');
                $model->updateAll($arrSearch, null, ['occorred_reason' => $occorredReason]);
                return response()->json([
                    'status' => 1
                ]);
            }

            $processAll = $request->input('process_all', null);
            if (!empty($processAll)) {
                $model->updateAll($arrSearch, null, ['process_content' => $processAll]);
                return response()->json([
                    'status' => 1
                ]);
            }
        }
    }

    /**
     * Update is_corrected and is_deleted
     *
     * @param   $request  Request
     * @return json
     */
    public function updateStatus(Request $request)
    {
        $name = $request->input('name');
        $value = $request->input('value', null);
        $model = new DtPaymentMapping();
        $arrUpdate = [];
        if ($name === 'all') {
            $arrUpdate['is_deleted'] = 0;
            $arrUpdate['is_corrected'] = 0;
        } elseif ($name === 'occorred_reason' || $name === 'process_content') {
            $arrUpdate[$name] = $value;
        } else {
            $arrUpdate[$name] = $value === 1 ? 0 : 1;
        }
        if ($name === 'is_corrected') {
            $arrUpdate['finished_date'] = now();
        }
        $arrWhere['received_order_id'] = $request->input('id');
        try {
            DB::beginTransaction();
            $model->where($arrWhere)->update($arrUpdate);
            DB::commit();
            return response()->json([
                'status' => 1
            ]);
        } catch (\Exception $e) {
            DB::rollback();
            $message = $e->getPrevious()->getMessage();
            return response()->json([
                'error' => "ERROR!!!"
            ]);
        }
    }

    public function processApi(Request $request)
    {
        $modelO   = new MstOrder();
        $modelRak = new DtWebRakData();
        $modelPay = new MstPaymentMethod();
        $modelMap = new DtPaymentMapping();
        $name     = $request->input('name');
        $count    = 0;
        $infoRun  = [];
        if ($name === 'rakuten_bank') {
            $rakAuth   = Config::get('common.rak_auth_key');
            $rakUrl    = "https://api.rms.rakuten.co.jp/es/1.0/order/ws";
            $rakHeader = array(
                "Content-Type: text/xml;charset=UTF-8",
            );
            $dataOrder = $modelO->getOrderAPI(ORDER_STATUS['SETTLEMENT'], ORDER_SUB_STATUS['DOING'], 1, null, 7);
            if ($dataOrder->count() > 0) {
                $dataOrder = $dataOrder->chunk(50);
                foreach ($dataOrder as $orders) {
                    try {
                        $startDate  = date("Y-m-d", strtotime($orders->min('order_date') . ' -1 day'));
                        $endDate    = date("Y-m-d", strtotime($orders->max('order_date') . ' +1 day'));
                        $requestXml = view('mapping.xml.web_rakuten', [
                            'authKey'   => $rakAuth,
                            'shopUrl'   => 'tuzukiya',
                            'userName'  => 'tuzukiya',
                            'startDate' => $startDate,
                            'endDate'   => $endDate,
                            'orders'    => $orders
                        ])->render();
                        $ch = curl_init($rakUrl);
                        curl_setopt($ch, CURLOPT_POST, true);
                        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                        curl_setopt($ch, CURLOPT_HTTPHEADER, $rakHeader);
                        curl_setopt($ch, CURLOPT_POSTFIELDS, $requestXml);
                        $responseXml = curl_exec($ch);
                        curl_close($ch);

                        $dataProcess      = $this->processRakutenXml($responseXml, $orders);
                        $receivedOrderIds = $orders->pluck('received_order_id');
                        if ($dataProcess['error_code'] === 'E10-001') {
                            $modelO->updateDataByReceivedOrderId(
                                $receivedOrderIds,
                                [
                                    'error_code_api'   => $dataProcess['error_code'],
                                    'message_api'      => $dataProcess['message'],
                                    'up_ope_cd'        => Auth::user()->tantou_code,
                                    'up_date'          => date('Y-m-d H:i:s')
                                ]
                            );
                        } else {
                            $dataInsert = $dataProcess['data_insert'];
                            if (count($dataInsert) > 0) {
                                $modelRak->insert($dataInsert);
                            }
                            $dataError = $dataProcess['data_error'];
                            if (count($dataError) > 0) {
                                $cancelOrder = [];
                                foreach ($dataError as $error) {
                                    if ($error['error_code'] === 'E10-303') {
                                        $cancelOrder[] = [
                                            'error_code'      => $error['error_code'],
                                            'message'         => $error['message'],
                                            'order_number'    => $error['order_key'],
                                            'settlement_name' => 11,
                                            'request_price'   => 0,
                                            'status'          => 10
                                        ];
                                    }
                                    $modelO->updateDataByReceivedOrderId(
                                        [$error['order_key']],
                                        [
                                            'error_code_api'   => $error['error_code'],
                                            'message_api'      => $error['message'],
                                            'up_ope_cd'        => Auth::user()->tantou_code,
                                            'up_date'          => date('Y-m-d H:i:s')
                                        ]
                                    );
                                }
                                if (count($cancelOrder) > 0) {
                                    $cancelOrder = array_chunk($cancelOrder, 500);
                                    foreach ($cancelOrder as $co) {
                                        $modelRak->insert($co);
                                    }
                                }
                            }
                        }
                    } catch (\Exception $ex) {
                        return response()->json([
                            'flg'     => -1,
                            'message' => $ex->getMessage()
                        ]);
                    }
                }
            }

            $dataRak = $modelRak->getDataProcessWebRakuten();
            $dataPayment = $modelPay->getDataByMall(1);
            if (count($dataRak) === 0) {
                $infoRun['flg'] = 1;
                $infoRun['message_success'] = '';
                return response()->json($infoRun);
            } else {
                foreach ($dataRak as $data) {
                    $arrInsert = [];
                    try {
                        $paymentMethod = 0;
                        foreach ($dataPayment as $pay) {
                            if ($data->settlement_name === $pay->payment_name) {
                                $paymentMethod = $pay->payment_code;
                                break;
                            }
                        }
                        $arrInsert['received_order_id']  = $data->order_number;
                        $arrInsert['mall_id']            = 1;
                        $arrInsert['web_payment_method'] = $paymentMethod;
                        switch ($data->status) {
                            case '6':
                                $arrInsert['web_order_status'] = 8;
                                break;
                            case '5':
                                $arrInsert['web_order_status'] = 4;
                                break;
                            case '8':
                                $arrInsert['web_order_status'] = 10;
                                break;
                            default:
                                $arrInsert['web_order_status'] = 1;
                                break;
                        }
                        $arrInsert['web_request_price'] = $data->request_price;
                        $differentType = [];
                        if ($data->payment_method !== $paymentMethod) {
                            $differentType[] = '支払方法';
                        }
                        if ($data->order_request_price !== $data->request_price) {
                            $differentType[] = 'WEB金額';
                        }
                        if ($data->order_status !== $arrInsert['web_order_status']) {
                            $differentType[] = 'ステータス';
                        }
                        $arrInsert['different_type']  = implode("\n", $differentType);
                        $arrInsert['different_price'] = $data->order_request_price - $data->request_price;
                        $arrInsert['occorred_reason'] = 0;
                        $arrInsert['process_content'] = 0;
                        $arrInsert['finished_date']   = empty($arrInsert['different_type'])
                            ? date('Y-m-d H:i:s') : null;
                        $arrInsert['is_corrected']    = empty($arrInsert['different_type']) ? 1 : 0;
                        $arrInsert['is_deleted']      = 0;
                        $arrInsert['receive_id']      = 0;
                        $arrInsert['in_ope_cd']       = Auth::user()->tantou_code;
                        $arrInsert['in_date']         = date('Y-m-d H:i:s');
                        $arrInsert['up_ope_cd']       = Auth::user()->tantou_code;
                        $arrInsert['up_date']         = date('Y-m-d H:i:s');
                        $modelMap->insertIgnore($arrInsert);
                        $modelRak->where('order_number', $data->order_number)
                            ->update(['process_flg' => 1]);
                        $count++;
                    } catch (Exception $e) {
                        $infoRun['flg'] = 1;
                        $infoRun['message_success'] = 'ERROR!!!';
                        return response()->json($infoRun);
                    }
                }
                $infoRun['flg'] = 1;
                $infoRun['message_success'] = "Insert success: $count record";
                return response()->json($infoRun);
            }
        }
    }

    /**
     * process import data.
     * Return $object json
     */
    public function import(Request $request)
    {
        $type      = $request->input("type", "");
        $fileName  = $request->input("filename", "");
        $timeRun   = $request->input("timeRun", 0);
        $currPage  = $request->input("currPage", 0);
        $total     = $request->input("total", 0);
        $countErr  = $request->input("count_err", 0);
        $realFile  = $request->input("realFilename", "");
        $fileCorrect = $request->input("fileCorrect", "");
        $flg       = false;
        $common    = new Common;
        //
        $isUnix = false;
        if ($type === 'import_pro_mfk') {
            $isUnix = true;
        }
        $info = $common->checkFile($fileName, $isUnix);
        if ($info["flg"] === 0) {
            return response()->json($info);
        } else {
            $readers = $info["msg"];
        }
        if ($type === 'import_diy_fregi_csv') {
            $flg = $this->processDiyFregiCsv(
                $readers,
                $currPage,
                $total,
                $fileName,
                (int)$countErr,
                $realFile,
                $fileCorrect
            );
            $table = 'dt_payment_diy_fregi_data';
            $arrCol = Config::get('common.csv_diy_fregi_data');
            $arrCol[] = 'csv_file_name';
            $arrCol[] = 'process_flg';
            $arrCol[] = 'is_deleted';
            $arrCol[] = 'in_date';
        } elseif ($type === 'import_biz_fregi_csv') {
            $flg = $this->processBizFregiCsv(
                $readers,
                $currPage,
                $total,
                $fileName,
                (int)$countErr,
                $realFile,
                $fileCorrect
            );
            $table = 'dt_payment_biz_fregi_data';
            $arrCol = Config::get('common.csv_biz_fregi_data');
        } elseif ($type === 'import_pro_mfk') {
            $flg = $this->processProMFKCsv(
                $readers,
                $currPage,
                $total,
                $fileName,
                (int)$countErr,
                $realFile,
                $fileCorrect
            );
            $table = 'dt_payment_pro_mfk_data';
            $arrCol = Config::get('common.csv_pro_mfk_data');
            $arrCol[] = 'payment_method';
            $arrCol[] = 'status';
            $arrCol[] = 'process_flg';
            $arrCol[] = 'is_delete';
            $arrCol[] = 'created_at';
        } elseif ($type === 'import_pro_paygent') {
            $flg = $this->processProPaygentCsv(
                $readers,
                $currPage,
                $total,
                $fileName,
                (int)$countErr,
                $realFile,
                $fileCorrect
            );
            $table = 'dt_payment_pro_paygent_data';
            $arrCol = Config::get('common.csv_pro_paygent_data');
        }
        if ($currPage === $timeRun) {
            $common->processCorrect($fileCorrect, $arrCol, $table);
        }
        $arrProcess = array(
            "fileName"     => $fileName,
            "timeRun"      => $timeRun,
            "currPage"     => $currPage,
            "total"        => $total,
            "realFilename" => $realFile,
            "fileCorrect"  => $fileCorrect,
        );
        $infoRun = $common->processDataRun($flg, $arrProcess);
        return response()->json($infoRun);
    }

    /**
     * Process data in file csv for diy
     * @param   string  $readers file name in storage
     * @return  boolean
     */
    public function processDiyFregiCsv(
        $arr,
        $offset,
        $length,
        $fileName,
        $countErr = 0,
        $realFile = "",
        $fileCorrect = ""
    ) {
        $readers = array_slice($arr, $offset*$length, $length);
        if ($readers === null) {
            return false;
        }
        $arrCol = Config::get('common.csv_diy_fregi_data');
        $totalColumn = 21;
        $arrError = array();
        $num = 1;
        $checkHead = false;
        $checkFail = false;
        if ($countErr >= 3) {
            if ($offset === '0') {
                $checkHead = true;
            }
            $checkFail = true;
        }
        $url = Storage::disk('local')->getDriver()->getAdapter()->getPathPrefix() . $fileCorrect;
        $file = fopen($url, 'a');

        foreach ($readers as $key => $row) {
            if ($checkFail) {
                $arrError[] = trim($row) . ",-->" . __('messages.request_timeout');
                continue;
            }
            if (empty($row) || ($offset === '0' && $num === 1)) {
                $num++;
                continue;
            }
            $column   = str_getcsv($row, ",");
            if (count($column) !== $totalColumn) {
                $arrError[] = trim($row) . ",-->" . __('messages.number_column_not_match');
                continue;
            }
            $column = array_map('trim', $column);
            $arrTemp = $column;
            $arrTemp = array_combine($arrCol, $column);
            $arrTemp['csv_file_name'] = $realFile;
            $arrTemp['process_flg']   = 0;
            $arrTemp['is_deleted']    = $arrTemp['amount_of_money'] === '1' ? 1 : 0;
            $arrTemp['in_date']       = now();
            $arrTemp['receipt_num']   = 'diy-' . $arrTemp['receipt_num'];
            fputs($file, mb_convert_encoding(implode(",", $arrTemp), 'Shift-JIS', 'UTF-8') . "\r\n");
        }
        fclose($file);
        $Common = new Common;
        $nameCached  = explode(".", $fileName);
        $nameCached  = $nameCached[0];
        if (count($arrError) > 0) {
            if (!Cache::has($nameCached) && !$checkHead) {
                $header = array_slice($arr, 0, 1);
                $arrError = array_merge($header, $arrError);
            }
            mb_convert_variables('SJIS', 'UTF-8', $arrError);
            $Common->updateCached($nameCached, implode("\r\n", $arrError));
        } elseif (Cache::has($nameCached)) {
            $Common->updateCached($nameCached, '');
        }
        return true;
    }

    /**
     * Process data after import
     * @param   object  $request
     * @return  json
     */
    public function processAfterImport(Request $request)
    {
        $Common = new Common;
        $modelDiy = new DtPaymentDiyFregiData();
        $modelBiz = new DtPaymentBizFregiData();
        $modelMap = new DtPaymentMapping();
        $modelO   = new MstOrder();
        $type     = $request->input('type');
        $error    = (int)$request->input('error');
        $fileName = $request->input('filename');
        $flg      = (int)$request->input('flg');
        $count    = 0;
        $nameCached  = explode(".", $fileName);
        $nameCached  = $nameCached[0];
        $infoRun  = [
            'type'     => $type,
            'error'    => $error,
            'filename' => $fileName,
            'flg'      => $flg,
        ];
        if ($type === 'import_diy_fregi_csv') {
            $dataDiy = $modelDiy->getDataProcessDiyFregi();
            $dataDiy = $dataDiy->keyBy('receipt_num');
            $dataOrder = $modelO->getDataMapping($dataDiy->keys());
            if ($dataDiy->count() === 0 || $dataOrder->count() === 0) {
                $infoRun['message_success'] = '';
                if ($dataDiy->count() !== 0) {
                    $modelDiy->whereIn('receipt_num', $dataDiy->keys())
                             ->update(['process_flg' => 1]);
                }
                return response()->json($infoRun);
            } else {
                $arrInsert = [];
                $arrUpdate = [];
                try {
                    $dataOrder = $dataOrder->keyBy('received_order_id');
                    foreach ($dataDiy as $key => $data) {
                        $arrUpdate[]  = $data->receipt_num;
                        if (isset($dataOrder[$data->receipt_num])) {
                            $arrCheck = $dataOrder[$data->receipt_num];
                        } else {
                            continue;
                        }
                        $arrInsert[$key]['received_order_id']  = $data->receipt_num;
                        $arrInsert[$key]['mall_id']            = 5;
                        $arrInsert[$key]['web_payment_method'] = 1;
                        $webOrderStatus = 0;
                        if (in_array($data->status, ['売上取消', '売上済取消用'])) {
                            $webOrderStatus = 10;
                        } elseif ($data->status === '売上済') {
                            $webOrderStatus = 8;
                        }
                        $arrInsert[$key]['web_order_status'] = $webOrderStatus;
                        $arrInsert[$key]['web_request_price'] = $data->amount_of_money;
                        $differentType = [];
                        if ($arrCheck->payment_method !== $arrInsert[$key]['web_payment_method']) {
                            $differentType[] = '支払方法';
                        }
                        if ($arrCheck->order_request_price !== $data->amount_of_money) {
                            $differentType[] = 'WEB金額';
                        }
                        if ($arrCheck->order_status !== $arrInsert[$key]['web_order_status']) {
                            $differentType[] = 'ステータス';
                        }
                        $arrInsert[$key]['different_type']  = implode("\n", $differentType);
                        $arrInsert[$key]['different_price'] = $arrCheck->order_request_price - $data->amount_of_money;
                        $arrInsert[$key]['occorred_reason'] = 0;
                        $arrInsert[$key]['process_content'] = 0;
                        $arrInsert[$key]['finished_date']   = empty($arrInsert[$key]['different_type']) ? now() : null;
                        $arrInsert[$key]['is_corrected']    = empty($arrInsert[$key]['different_type']) ? 1 : 0;
                        $arrInsert[$key]['is_deleted']      = 0;
                        $arrInsert[$key]['receive_id']      = 0;
                        $arrInsert[$key]['in_ope_cd']       = 'OPE99999';
                        $arrInsert[$key]['in_date']         = now();
                        $arrInsert[$key]['up_ope_cd']       = 'OPE99999';
                        $arrInsert[$key]['up_date']         = now();
                    }
                    $count = count($arrInsert);
                    if ($count !== 0 || count($arrUpdate) !== 0) {
                        $arrSteps = array_chunk($arrInsert, $this->limit);
                        foreach ($arrSteps as $step) {
                            if (Cache::has($nameCached)) {
                                $Common->updateCached($nameCached, '');
                            }
                            $modelMap->insertIgnore($step);
                        }
                        $modelDiy->whereIn('receipt_num', $arrUpdate)
                            ->update(['process_flg' => 1]);
                    }
                } catch (Exception $e) {
                    $infoRun['message_success'] = 'ERROR PROCESS MAPPING!!!';
                    return response()->json($infoRun);
                }
                $infoRun['message_success'] = "Insert success: $count record";
                return response()->json($infoRun);
            }
        } elseif ($type === 'import_biz_fregi_csv') {
            $dataBiz = $modelBiz->getDataProcessBizFregi();
            $dataBiz = $dataBiz->keyBy('receipt_num');
            $dataOrder = $modelO->getDataMapping($dataBiz->keys());
            if (count($dataBiz) === 0 || $dataOrder->count() === 0) {
                $infoRun['message_success'] = '';
                if ($dataBiz->count() !== 0) {
                    $modelBiz->whereIn('receipt_num', $dataBiz->keys())
                        ->update(['process_flg' => 1]);
                }
                return response()->json($infoRun);
            } else {
                $arrInsert = [];
                $arrUpdate = [];
                try {
                    $dataOrder  = $dataOrder->keyBy('received_order_id');
                    foreach ($dataBiz as $key => $data) {
                        $arrUpdate[]  = $data->receipt_num;
                        if (isset($dataOrder[$data->receipt_num])) {
                            $arrCheck = $dataOrder[$data->receipt_num];
                        } else {
                            continue;
                        }
                        $arrInsert[$key]['received_order_id']  = $data->receipt_num;
                        $arrInsert[$key]['mall_id']            = 4;
                        $arrInsert[$key]['web_payment_method'] = 1;
                        if (in_array($data->status, ['売上取消', '売上済取消用'])) {
                            $webOrderStatus = 10;
                        } elseif ($data->status === '売上済') {
                            $webOrderStatus = 8;
                        }
                        $arrInsert[$key]['web_order_status'] = $webOrderStatus;
                        $arrInsert[$key]['web_request_price'] = $data->amount_of_money;
                        $differentType = [];
                        if ($arrCheck->payment_method !== $arrInsert[$key]['web_payment_method']) {
                            $differentType[] = '支払方法';
                        }
                        if ($arrCheck->order_request_price !== $data->amount_of_money) {
                            $differentType[] = 'WEB金額';
                        }
                        if ($arrCheck->order_status !== $arrInsert[$key]['web_order_status']) {
                            $differentType[] = 'ステータス';
                        }
                        $arrInsert[$key]['different_type']  = implode("\n", $differentType);
                        $arrInsert[$key]['different_price'] = $arrCheck->order_request_price - $data->amount_of_money;
                        $arrInsert[$key]['occorred_reason'] = 0;
                        $arrInsert[$key]['process_content'] = 0;
                        $arrInsert[$key]['finished_date']   = empty($arrInsert[$key]['different_type']) ? now() : null;
                        $arrInsert[$key]['is_corrected']    = empty($arrInsert[$key]['different_type']) ? 1 : 0;
                        $arrInsert[$key]['is_deleted']      = 0;
                        $arrInsert[$key]['receive_id']      = 0;
                        $arrInsert[$key]['in_ope_cd']       = 'OPE99999';
                        $arrInsert[$key]['in_date']         = now();
                        $arrInsert[$key]['up_ope_cd']       = 'OPE99999';
                        $arrInsert[$key]['up_date']         = now();
                    }
                    $count = count($arrInsert);
                    if ($count !== 0 || count($arrUpdate) !== 0) {
                        $arrSteps = array_chunk($arrInsert, $this->limit);
                        foreach ($arrSteps as $step) {
                            if (Cache::has($nameCached)) {
                                $Common->updateCached($nameCached, '');
                            }
                            $modelMap->insertIgnore($step);
                        }
                        $modelBiz->whereIn('receipt_num', $arrUpdate)
                                 ->update(['process_flg' => 1]);
                    }
                } catch (Exception $e) {
                    $infoRun['message_success'] = 'ERROR!!!';
                    return response()->json($infoRun);
                }

                $infoRun['message_success'] = "Insert success: $count record";
                return response()->json($infoRun);
            }
        } elseif ($type === 'import_pro_mfk') {
            $modelPF = new DtPaymentProMfkData();
            $dataMFK = $modelPF->getDataProcessProMFK();
            $dataMFK = $dataMFK->keyBy('order_id');
            $dataOrder = $modelO->getDataMapping($dataMFK->keys());
            if (count($dataMFK) === 0 || $dataOrder->count() === 0) {
                $infoRun['message_success'] = '';
                if ($dataMFK->count() !== 0) {
                    $modelPF->whereIn('order_id', $dataMFK->keys())
                        ->update(['process_flg' => 1]);
                }
                return response()->json($infoRun);
            } else {
                $arrInsert = [];
                $arrUpdate = [];
                try {
                    $dataOrder  = $dataOrder->keyBy('received_order_id');
                    foreach ($dataMFK as $key => $data) {
                        $arrUpdate[]  = $data->order_id;
                        if (isset($dataOrder[$data->order_id])) {
                            $arrCheck = $dataOrder[$data->order_id];
                        } else {
                            continue;
                        }
                        $arrInsert[$key]['received_order_id']  = $data->order_id;
                        $arrInsert[$key]['mall_id']            = 8;
                        $arrInsert[$key]['web_payment_method'] = 12;
                        $arrInsert[$key]['web_order_status']   = $data->status;
                        $arrInsert[$key]['web_request_price']  = $data->request_price;
                        $differentType = [];
                        if ($arrCheck->payment_method !== $arrInsert[$key]['web_payment_method']) {
                            $differentType[] = '支払方法';
                        }
                        if ($arrCheck->order_request_price !== $arrInsert[$key]['web_request_price']) {
                            $differentType[] = 'WEB金額';
                        }
                        if ($arrCheck->order_status !== $arrInsert[$key]['web_order_status']) {
                            $differentType[] = 'ステータス';
                        }
                        $arrInsert[$key]['different_type']  = implode("\n", $differentType);
                        $arrInsert[$key]['different_price'] = $arrCheck->order_request_price - $arrInsert[$key]['web_request_price'];
                        $arrInsert[$key]['occorred_reason'] = 0;
                        $arrInsert[$key]['process_content'] = 0;
                        $arrInsert[$key]['finished_date']   = empty($arrInsert[$key]['different_type']) ? now() : null;
                        $arrInsert[$key]['is_corrected']    = empty($arrInsert[$key]['different_type']) ? 1 : 0;
                        $arrInsert[$key]['is_deleted']      = 0;
                        $arrInsert[$key]['receive_id']      = 0;
                        $arrInsert[$key]['in_ope_cd']       = 'OPE99999';
                        $arrInsert[$key]['in_date']         = now();
                        $arrInsert[$key]['up_ope_cd']       = 'OPE99999';
                        $arrInsert[$key]['up_date']         = now();
                    }
                    $count = count($arrInsert);
                    if ($count !== 0 || count($arrUpdate) !== 0) {
                        $arrSteps = array_chunk($arrInsert, $this->limit);
                        foreach ($arrSteps as $step) {
                            if (Cache::has($nameCached)) {
                                $Common->updateCached($nameCached, '');
                            }
                            $modelMap->insertIgnore($step);
                        }
                        $modelPF->whereIn('order_id', $arrUpdate)
                                 ->update(['process_flg' => 1]);
                    }
                } catch (Exception $e) {
                    $infoRun['message_success'] = 'ERROR!!!';
                    return response()->json($infoRun);
                }

                $infoRun['message_success'] = "Insert success: $count record";
                return response()->json($infoRun);
            }
        } elseif ($type === 'import_pro_paygent') {
            $modelPP = new DtPaymentProPaygentData();
            $dataMFK = $modelPP->getDataProcessProPaygent();
            $dataMFK = $dataMFK->keyBy('order_id');
            $dataOrder = $modelO->getDataMapping($dataMFK->keys());
            if (count($dataMFK) === 0 || $dataOrder->count() === 0) {
                $infoRun['message_success'] = '';
                if ($dataMFK->count() !== 0) {
                    $modelPP->whereIn('order_id', $dataMFK->keys())
                        ->update(['process_flg' => 1]);
                }
                return response()->json($infoRun);
            } else {
                $arrInsert = [];
                $arrUpdate = [];
                try {
                    $dataOrder  = $dataOrder->keyBy('received_order_id');
                    foreach ($dataMFK as $key => $data) {
                        $arrUpdate[]  = $data->order_id;
                        if (isset($dataOrder[$data->order_id])) {
                            $arrCheck = $dataOrder[$data->order_id];
                        } else {
                            continue;
                        }
                        $arrInsert[$key]['received_order_id']  = $data->order_id;
                        $arrInsert[$key]['mall_id']            = 8;
                        $arrInsert[$key]['web_payment_method'] = 1;
                        $arrInsert[$key]['web_order_status']   = $data->status;
                        $arrInsert[$key]['web_request_price']  = $data->payment_price;
                        $differentType = [];
                        if ($arrCheck->payment_method !== $arrInsert[$key]['web_payment_method']) {
                            $differentType[] = '支払方法';
                        }
                        if ($arrCheck->order_request_price !== $arrInsert[$key]['web_request_price']) {
                            $differentType[] = 'WEB金額';
                        }
                        if ($arrCheck->order_status !== $arrInsert[$key]['web_order_status']) {
                            $differentType[] = 'ステータス';
                        }
                        $arrInsert[$key]['different_type']  = implode("\n", $differentType);
                        $arrInsert[$key]['different_price'] = $arrCheck->order_request_price - $arrInsert[$key]['web_request_price'];
                        $arrInsert[$key]['occorred_reason'] = 0;
                        $arrInsert[$key]['process_content'] = 0;
                        $arrInsert[$key]['finished_date']   = empty($arrInsert[$key]['different_type']) ? now() : null;
                        $arrInsert[$key]['is_corrected']    = empty($arrInsert[$key]['different_type']) ? 1 : 0;
                        $arrInsert[$key]['is_deleted']      = 0;
                        $arrInsert[$key]['receive_id']      = 0;
                        $arrInsert[$key]['in_ope_cd']       = 'OPE99999';
                        $arrInsert[$key]['in_date']         = now();
                        $arrInsert[$key]['up_ope_cd']       = 'OPE99999';
                        $arrInsert[$key]['up_date']         = now();
                    }
                    $count = count($arrInsert);
                    if ($count !== 0 || count($arrUpdate) !== 0) {
                        $arrSteps = array_chunk($arrInsert, $this->limit);
                        foreach ($arrSteps as $step) {
                            if (Cache::has($nameCached)) {
                                $Common->updateCached($nameCached, '');
                            }
                            $modelMap->insertIgnore($step);
                        }
                        $modelPP->whereIn('order_id', $arrUpdate)
                                 ->update(['process_flg' => 1]);
                    }
                } catch (Exception $e) {
                    $infoRun['message_success'] = 'ERROR!!!';
                    return response()->json($infoRun);
                }

                $infoRun['message_success'] = "Insert success: $count record";
                return response()->json($infoRun);
            }
        }
    }


        /**
     * Process data in file csv for diy
     * @param   string  $readers file name in storage
     * @return  boolean
     */
    public function processBizFregiCsv(
        $arr,
        $offset,
        $length,
        $fileName,
        $countErr = 0,
        $realFile = "",
        $fileCorrect = ""
    ) {
        $readers = array_slice($arr, $offset*$length, $length);
        if ($readers === null) {
            return false;
        }
        $totalColumn = 21;
        $arrError = array();
        $num = 1;
        $checkHead = false;
        $checkFail = false;
        if ($countErr >= 3) {
            if ($offset === '0') {
                $checkHead = true;
            }
            $checkFail = true;
        }
        $url = Storage::disk('local')->getDriver()->getAdapter()->getPathPrefix() . $fileCorrect;
        $file = fopen($url, 'a');
        foreach ($readers as $key => $row) {
            if ($checkFail) {
                $arrError[] = trim($row) . ',-->' . __('messages.request_timeout');
                continue;
            }
            if (empty($row) || ($offset === '0' && $num === 1)) {
                $num++;
                continue;
            }
            $column   = str_getcsv($row, ",");
            if (count($column) !== $totalColumn) {
                $arrError[] = trim($row) . ',-->' . __('messages.number_column_not_match');
                continue;
            }
            $column = array_map('trim', $column);
            $arrTemp = $column;
            $arrTemp[] = $realFile;
            $arrTemp[] = 0;
            $arrTemp[] = 0;
            $arrTemp[] = now();
            fputs($file, mb_convert_encoding(implode(",", $arrTemp), 'Shift-JIS', 'UTF-8') . "\r\n");
        }
        fclose($file);
        $Common = new Common;
        $nameCached  = explode(".", $fileName);
        $nameCached  = $nameCached[0];
        if (count($arrError) > 0) {
            if (!Cache::has($nameCached) && !$checkHead) {
                $header = array_slice($arr, 0, 1);
                $arrError = array_merge($header, $arrError);
            }
            mb_convert_variables('SJIS', 'UTF-8', $arrError);
            $Common->updateCached($nameCached, implode("\r\n", $arrError));
        } elseif (Cache::has($nameCached)) {
            $Common->updateCached($nameCached, '');
        }
        return true;
    }

    /**
     * Process Rakuten xml
     *
     * @param string $responseXml
     * @param array  $orders
     */
    private function processRakutenXml($responseXml, $orders)
    {
        $columns     = [
            "error_code","message","unit_error_code","unit_error_message","unit_error_order_key",
            "asuraku_flg","canenclosure","card_status","carrier_code","child_order_model",
            "coupon_all_total_price","coupon_all_total_unit","coupon_capital","coupon_code",
            "coupon_name","coupon_price","coupon_summary","coupon_total_price","coupon_unit",
            "coupon_usage","discount_type","coupon_expiry_date","coupon_fee_flag","coupon_item_id",
            "coupon_order_number","coupon_other_price","coupon_other_total_unit","coupon_shop_price",
            "coupon_shop_total_unit","deal","delivery_class","delivery_name","delivery_price",
            "drug_category","email_carrier_code","enc_coupon_price","enc_delivery_price",
            "enc_goods_price","enc_goods_tax","enc_id","enc_point_price","enc_postage_price",
            "enc_bank_transfer_commission","enc_request_price","enc_status","enc_total_price",
            "first_amount","bid_id","comment","goods_price","goods_tax","history_model","is_black_user",
            "is_gift","is_gift_check","is_rak_member","is_tax_recalc","mail_plug_sentence","membership",
            "memo","modify","detailid","reserve_date_time","reserve_number","reserve_type","operator",
            "option","order_date","order_number","order_type","birth_day","birth_month","birth_year",
            "city","email_address","family_name","family_name_kana","first_name","first_name_kana",
            "nick_name","phone_number1","phone_number2","phone_number3","prefecture","sex","sub_address",
            "zip_code1","zip_code2","pac_model_basket_id","delete_flg","delivery_company_id","area_code",
            "cvs_bikou","cvs_close_time","cvs_code","cvs_open_time","depo","store_address","store_code",
            "store_genrecode","store_name","store_prefecture","store_zip","pack_model_deli_price",
            "pack_model_goods_price","pack_model_goods_tax","item_model_basket_id","delete_item_flg",
            "current_amount","gbuy_bid_inv_model_bid_units","gbuy_bid_inv_model_gchoice_id",
            "gbuy_gchoice_model_gchoice_id","gbuy_gchoice_model_gchoice_inv_try",
            "gbuy_gchoice_model_gchoice_max_units","gbuy_gchoice_model_gchoice_name",
            "gbuy_gchoice_model_item_id","gbuy_gchoice_model_order_by","gbuy_gchoice_model_sold_flag",
            "gbuy_gchoice_model_sum_amount","is_shift_status","shift_date","unit_text",
            "is_included_cash_on_deli_postage","is_included_postage","is_included_tax","item_model_item_id",
            "item_name","item_number","deli_date_info","inventory_type","page_url","point_rate","point_type",
            "price","restore_inv_flag","sa_item_model","selected_choice","units","noshi","pack_model_postage_price",
            "sender_model_birth_day","sender_model_birth_month","sender_model_birth_year","sender_model_city",
            "sender_model_email_address","sender_model_family_name","sender_model_family_name_kana",
            "sender_model_first_name","sender_model_first_name_kana","sender_model_nickname",
            "sender_model_phone_num1","sender_model_phone_num2","sender_model_phone_num3",
            "sender_model_prefecture","sender_model_sex","sender_model_subaddress","sender_model_zip_code1",
            "sender_model_zip_code2","shipping_number","payment_date","pay_status_model_access_point",
            "pay_status_model_error_code","pay_status_model_operator","pay_status_model_order_number",
            "pay_statusmodel_pay_type","pay_status_model_price","pay_status_model_shop_id","pay_status_model_status",
            "pay_status_model_update_time","point_model_point_usage","point_model_status","point_model_used_point",
            "postage_price","rbank_model_order_number","rbank_model_commission_payer","rbank_model_rbank_status",
            "rbank_model_shop_id","rbank_model_transfer_commission","request_price","rmid","sa_order_model_bid_id",
            "sa_order_model_comment","sa_order_model_reg_date","seq_id","brand_name","card_no","expiration_date",
            "installment_desc","owner_name","pay_type","settlement_name","shipping_date","shipping_term",
            "status","total_price","wish_deli_date","wrap_model1_del_wrap_flg","wrap_model1_is_included_tax",
            "wrap_model1_name","wrap_model1_price","wrap_model1_title","wrap_model2_del_wrap_flg",
            "wrap_model2_is_included_tax","wrap_model2_name","wrap_model2_price","wrap_model2_title",
            "accept_change_flg","accept_change_request_id","mail_serial","form_cancel_flg","process_flg","is_delete",
            "created_at"
        ];

        $responseXml = str_ireplace(['S:', 'ns2:'], '', $responseXml);
        $responData  = simplexml_load_string($responseXml);
        $returnData  = $responData->Body->getOrderResponse->return;
        $errorCode   = Helper::val($returnData->errorCode);
        $message     = Helper::val($returnData->message);

        if ($errorCode === "E10-001") {
            return [
                'error_code' => $errorCode,
                'message'    => $message
            ];
        } else {
            $dataError  = array();
            $dataInsert = array();
            foreach ($returnData->unitError as $unitError) {
                $unitErrorCode = Helper::val($unitError->errorCode);
                $unitMessage   = Helper::val($unitError->message);
                $orderKey      = Helper::val($unitError->orderKey);
                $dataError[]   = [
                    'error_code' => $unitErrorCode,
                    'message'    => $unitMessage,
                    'order_key'  => $orderKey
                ];
            }
            foreach ($returnData->orderModel as $orderModel) {
                foreach ($orderModel->packageModel->itemModel as $itemModel) {
                    $tmp = [
                        'error_code'                           => Helper::val($returnData->errorCode),
                        'message'                              => Helper::val($returnData->message),
                        'asuraku_flg'                          => Helper::val($orderModel->asurakuFlg),
                        'canenclosure'                         => Helper::val($orderModel->canEnclosure),
                        'card_status'                          => Helper::val($orderModel->cardStatus),
                        'carrier_code'                         => Helper::val($orderModel->carrierCode),
                        'child_order_model'                    => Helper::val($orderModel->childOrderModel),
                        'coupon_all_total_price'               => Helper::val($orderModel->couponAllTotalPrice),
                        'coupon_all_total_unit'                => Helper::val($orderModel->couponAllTotalUnit),
                        'coupon_capital'                       => Helper::val($orderModel->couponModel->couponCapital),
                        'coupon_code'                          => Helper::val($orderModel->couponModel->couponCode),
                        'coupon_name'                          => Helper::val($orderModel->couponModel->couponName),
                        'coupon_price'                         => Helper::val($orderModel->couponModel->couponPrice),
                        'coupon_summary'                       => Helper::val($orderModel->couponModel->couponSummary),
                        'coupon_total_price'
                        => Helper::val($orderModel->couponModel->couponTotalPrice),
                        'coupon_unit'                          => Helper::val($orderModel->couponModel->couponUnit),
                        'coupon_usage'                         => Helper::val($orderModel->couponModel->couponUsage),
                        'discount_type'                        => Helper::val($orderModel->couponModel->discountType),
                        'coupon_expiry_date'                   => Helper::val($orderModel->couponModel->expiryDate),
                        'coupon_fee_flag'                      => Helper::val($orderModel->couponModel->feeFlag),
                        'coupon_item_id'                       => Helper::val($orderModel->couponModel->itemId),
                        'coupon_order_number'                  => Helper::val($orderModel->couponModel->orderNumber),
                        'coupon_other_price'                   => Helper::val($orderModel->couponOtherPrice),
                        'coupon_other_total_unit'              => Helper::val($orderModel->couponOtherTotalUnit),
                        'coupon_shop_price'                    => Helper::val($orderModel->couponShopPrice),
                        'coupon_shop_total_unit'               => Helper::val($orderModel->couponShopTotalUnit),
                        'deal'                                 => Helper::val($orderModel->deal),
                        'delivery_class'
                        => Helper::val($orderModel->deliveryModel->deliveryClass),
                        'delivery_name'                        => Helper::val($orderModel->deliveryModel->deliveryName),
                        'delivery_price'                       => Helper::val($orderModel->deliveryPrice),
                        'drug_category'                        => Helper::val($orderModel->drugCategory),
                        'email_carrier_code'                   => Helper::val($orderModel->emailCarrierCode),
                        'enc_coupon_price'                     => Helper::val($orderModel->enclosureCouponPrice),
                        'enc_delivery_price'                   => Helper::val($orderModel->enclosureDeliveryPrice),
                        'enc_goods_price'                      => Helper::val($orderModel->enclosureGoodsPrice),
                        'enc_goods_tax'                        => Helper::val($orderModel->enclosureGoodsTax),
                        'enc_id'                               => Helper::val($orderModel->enclosureId),
                        'enc_point_price'                      => Helper::val($orderModel->enclosurePointPrice),
                        'enc_postage_price'                    => Helper::val($orderModel->enclosurePostagePrice),
                        'enc_bank_transfer_commission'
                        => Helper::val($orderModel->enclosureRBankTransferCommission),
                        'enc_request_price'                    => Helper::val($orderModel->enclosureRequestPrice),
                        'enc_status'                           => Helper::val($orderModel->enclosureStatus),
                        'enc_total_price'                      => Helper::val($orderModel->enclosureTotalPrice),
                        'first_amount'                         => Helper::val($orderModel->firstAmount),
                        'bid_id'                               => Helper::val($orderModel->gbuyOrderModel->bidId),
                        'comment'                              => Helper::val($orderModel->gbuyOrderModel->comment),
                        'goods_price'                          => Helper::val($orderModel->goodsPrice),
                        'goods_tax'                            => Helper::val($orderModel->goodsTax),
                        'history_model'                        => Helper::val($orderModel->historyModel),
                        'is_black_user'                        => Helper::val($orderModel->isBlackUser),
                        'is_gift'                              => Helper::val($orderModel->isGift),
                        'is_gift_check'                        => Helper::val($orderModel->isGiftCheck),
                        'is_rak_member'                        => Helper::val($orderModel->isRakutenMember),
                        'is_tax_recalc'                        => Helper::val($orderModel->isTaxRecalc),
                        'mail_plug_sentence'                   => Helper::val($orderModel->mailPlugSentence),
                        'membership'                           => Helper::val($orderModel->membership),
                        'memo'                                 => Helper::val($orderModel->memo),
                        'modify'                               => Helper::val($orderModel->modify),
                        'detailid'                             => Helper::val($orderModel->normalOrderModel->detailId),
                        'reserve_date_time'
                        => Helper::val($orderModel->normalOrderModel->reserveDatetime),
                        'reserve_number'
                        => Helper::val($orderModel->normalOrderModel->reserveNumber),
                        'reserve_type'
                        => Helper::val($orderModel->normalOrderModel->reserveType),
                        'operator'                             => Helper::val($orderModel->operator),
                        'option'                               => Helper::val($orderModel->option),
                        'order_date'                           => Helper::val($orderModel->orderDate),
                        'order_number'                         => Helper::val($orderModel->orderNumber),
                        'order_type'                           => Helper::val($orderModel->orderType),
                        'birth_day'                            => Helper::val($orderModel->ordererModel->birthDay),
                        'birth_month'                          => Helper::val($orderModel->ordererModel->birthMonth),
                        'birth_year'                           => Helper::val($orderModel->ordererModel->birthYear),
                        'city'                                 => Helper::val($orderModel->ordererModel->city),
                        'email_address'                        => Helper::val($orderModel->ordererModel->emailAddress),
                        'family_name'                          => Helper::val($orderModel->ordererModel->familyName),
                        'family_name_kana'
                        => Helper::val($orderModel->ordererModel->familyNameKana),
                        'first_name'                           => Helper::val($orderModel->ordererModel->firstName),
                        'first_name_kana'                      => Helper::val($orderModel->ordererModel->firstNameKana),
                        'nick_name'                            => Helper::val($orderModel->ordererModel->nickname),
                        'phone_number1'                        => Helper::val($orderModel->ordererModel->phoneNumber1),
                        'phone_number2'                        => Helper::val($orderModel->ordererModel->phoneNumber2),
                        'phone_number3'                        => Helper::val($orderModel->ordererModel->phoneNumber3),
                        'prefecture'                           => Helper::val($orderModel->ordererModel->prefecture),
                        'sex'                                  => Helper::val($orderModel->ordererModel->sex),
                        'sub_address'                          => Helper::val($orderModel->ordererModel->subAddress),
                        'zip_code1'                            => Helper::val($orderModel->ordererModel->zipCode1),
                        'zip_code2'                            => Helper::val($orderModel->ordererModel->zipCode2),
                        'pac_model_basket_id'                  => Helper::val($orderModel->packageModel->basketId),
                        'delete_flg'                           => Helper::val($orderModel->packageModel->deleteFlg),
                        'delivery_company_id'
                        => Helper::val($orderModel->packageModel->deliveryCompanyId),
                        'area_code'
                                => Helper::val($orderModel->packageModel->deliveryCvsModel->areaCode),
                        'cvs_bikou'
                                => Helper::val($orderModel->packageModel->deliveryCvsModel->cvsBikou),
                        'cvs_close_time'
                           => Helper::val($orderModel->packageModel->deliveryCvsModel->cvsCloseTime),
                        'cvs_code'
                                 => Helper::val($orderModel->packageModel->deliveryCvsModel->cvsCode),
                        'cvs_open_time'
                            => Helper::val($orderModel->packageModel->deliveryCvsModel->cvsOpenTime),
                        'depo'
                                     => Helper::val($orderModel->packageModel->deliveryCvsModel->depo),
                        'store_address'
                            => Helper::val($orderModel->packageModel->deliveryCvsModel->storeAddress),
                        'store_code'
                               => Helper::val($orderModel->packageModel->deliveryCvsModel->storeCode),
                        'store_genrecode'
                          => Helper::val($orderModel->packageModel->deliveryCvsModel->storeGenreCode),
                        'store_name'
                               => Helper::val($orderModel->packageModel->deliveryCvsModel->storeName),
                        'store_prefecture'
                         => Helper::val($orderModel->packageModel->deliveryCvsModel->storePrefecture),
                        'store_zip'
                                => Helper::val($orderModel->packageModel->deliveryCvsModel->storeZip),
                        'pack_model_deli_price'                => Helper::val($orderModel->packageModel->deliveryPrice),
                        'pack_model_goods_price'               => Helper::val($orderModel->packageModel->goodsPrice),
                        'pack_model_goods_tax'                 => Helper::val($orderModel->packageModel->goodsTax),
                        'item_model_basket_id'                 => Helper::val($itemModel->basketId),
                        'delete_item_flg'                      => Helper::val($itemModel->deleteItemFlg),
                        'current_amount'
                        => Helper::val($itemModel->gbuyItemModel->currentSumAmount),
                        'gbuy_bid_inv_model_bid_units'
                        => isset($itemModel->gbuyItemModel->gbuyBidInventoryModel->bidUnits)?
                        (string)$itemModel->gbuyItemModel->gbuyBidInventoryModel->bidUnits:null,
                        'gbuy_bid_inv_model_gchoice_id'
                        => isset($itemModel->gbuyItemModel->gbuyBidInventoryModel->gchoiceId)?
                        (string)$itemModel->gbuyItemModel->gbuyBidInventoryModel->gchoiceId:null,
                        'gbuy_gchoice_model_gchoice_id'
                        => isset($itemModel->gbuyItemModel->gbuyGchoiceModel->gchoiceId)?
                        (string)$itemModel->gbuyItemModel->gbuyGchoiceModel->gchoiceId:null,
                        'gbuy_gchoice_model_gchoice_inv_try'
                        => isset($itemModel->gbuyItemModel->gbuyGchoiceModel->gchoiceInvtry)?
                        (string)$itemModel->gbuyItemModel->gbuyGchoiceModel->gchoiceInvtry:null,
                        'gbuy_gchoice_model_gchoice_max_units'
                        => isset($itemModel->gbuyItemModel->gbuyGchoiceModel->gchoiceMaxUnits)?
                        (string)$itemModel->gbuyItemModel->gbuyGchoiceModel->gchoiceMaxUnits:null,
                        'gbuy_gchoice_model_gchoice_name'
                        => isset($itemModel->gbuyItemModel->gbuyGchoiceModel->gchoiceName)?
                        (string)$itemModel->gbuyItemModel->gbuyGchoiceModel->gchoiceName:null,
                        'gbuy_gchoice_model_item_id'
                        => isset($itemModel->gbuyItemModel->gbuyGchoiceModel->itemId)?
                        (string)$itemModel->gbuyItemModel->gbuyGchoiceModel->itemId:null,
                        'gbuy_gchoice_model_order_by'
                        => isset($itemModel->gbuyItemModel->gbuyGchoiceModel->orderby)?
                        (string)$itemModel->gbuyItemModel->gbuyGchoiceModel->orderby:null,
                        'gbuy_gchoice_model_sold_flag'
                        => isset($itemModel->gbuyItemModel->gbuyGchoiceModel->soldFlag)?
                        (string)$itemModel->gbuyItemModel->gbuyGchoiceModel->soldFlag:null,
                        'gbuy_gchoice_model_sum_amount'
                        => isset($itemModel->gbuyItemModel->gbuyGchoiceModel->sumAmount)?
                        (string)$itemModel->gbuyItemModel->gbuyGchoiceModel->sumAmount:null,
                        'is_shift_status'                      => Helper::val($itemModel->gbuyItemModel->isShiftStatus),
                        'shift_date'                           => Helper::val($itemModel->gbuyItemModel->shiftDate),
                        'unit_text'                            => Helper::val($itemModel->gbuyItemModel->unitText),
                        'is_included_cash_on_deli_postage'
                        => Helper::val($itemModel->isIncludedCashOnDeliveryPostage),
                        'is_included_postage'                  => Helper::val($itemModel->isIncludedPostage),
                        'is_included_tax'                      => Helper::val($itemModel->isIncludedTax),
                        'item_model_item_id'                   => Helper::val($itemModel->itemId),
                        'item_name'                            => Helper::val($itemModel->itemName),
                        'item_number'                          => Helper::val($itemModel->itemNumber),
                        'deli_date_info'
                        => Helper::val($itemModel->normalItemModel->delvdateInfo),
                        'inventory_type'
                        => Helper::val($itemModel->normalItemModel->inventoryType),
                        'page_url'                             => Helper::val($itemModel->pageURL),
                        'point_rate'                           => Helper::val($itemModel->pointRate),
                        'point_type'                           => Helper::val($itemModel->pointType),
                        'price'                                => Helper::val($itemModel->price),
                        'restore_inv_flag'                     => Helper::val($itemModel->restoreInventoryFlag),
                        'sa_item_model'                        => Helper::val($itemModel->saItemModel),
                        'selected_choice'                      => Helper::val($itemModel->selectedChoice),
                        'units'                                => Helper::val($itemModel->units),
                        'noshi'                                => Helper::val($orderModel->packageModel->noshi),
                        'pack_model_postage_price'             => Helper::val($orderModel->packageModel->postagePrice),
                        'sender_model_birth_day'
                        => Helper::val($orderModel->packageModel->senderModel->birthDay),
                        'sender_model_birth_month'
                        => Helper::val($orderModel->packageModel->senderModel->birthMonth),
                        'sender_model_birth_year'
                        => Helper::val($orderModel->packageModel->senderModel->birthYear),
                        'sender_model_city'
                        => Helper::val($orderModel->packageModel->senderModel->city),
                        'sender_model_email_address'
                        => Helper::val($orderModel->packageModel->senderModel->emailAddress),
                        'sender_model_family_name'
                        => Helper::val($orderModel->packageModel->senderModel->familyName),
                        'sender_model_family_name_kana'
                        => Helper::val($orderModel->packageModel->senderModel->familyNameKana),
                        'sender_model_first_name'
                        => Helper::val($orderModel->packageModel->senderModel->firstName),
                        'sender_model_first_name_kana'
                        => Helper::val($orderModel->packageModel->senderModel->firstNameKana),
                        'sender_model_nickname'
                        => Helper::val($orderModel->packageModel->senderModel->nickname),
                        'sender_model_phone_num1'
                        => Helper::val($orderModel->packageModel->senderModel->phoneNumber1),
                        'sender_model_phone_num2'
                        => Helper::val($orderModel->packageModel->senderModel->phoneNumber2),
                        'sender_model_phone_num3'
                        => Helper::val($orderModel->packageModel->senderModel->phoneNumber3),
                        'sender_model_prefecture'
                        => Helper::val($orderModel->packageModel->senderModel->prefecture),
                        'sender_model_sex'
                        => Helper::val($orderModel->packageModel->senderModel->sex),
                        'sender_model_subaddress'
                        => Helper::val($orderModel->packageModel->senderModel->subAddress),
                        'sender_model_zip_code1'
                        => Helper::val($orderModel->packageModel->senderModel->zipCode1),
                        'sender_model_zip_code2'
                        => Helper::val($orderModel->packageModel->senderModel->zipCode2),
                        'shipping_number'
                        => Helper::val($orderModel->packageModel->shippingNumber),
                        'payment_date'                         => Helper::val($orderModel->paymentDate),
                        'pay_status_model_access_point'
                        => Helper::val($orderModel->paymentStatusModel->accessPoint),
                        'pay_status_model_error_code'
                        => Helper::val($orderModel->paymentStatusModel->errorCode),
                        'pay_status_model_operator'
                        => Helper::val($orderModel->paymentStatusModel->operator),
                        'pay_status_model_order_number'
                        => Helper::val($orderModel->paymentStatusModel->orderNumber),
                        'pay_statusmodel_pay_type'
                        => Helper::val($orderModel->paymentStatusModel->paymentType),
                        'pay_status_model_price'               => Helper::val($orderModel->paymentStatusModel->price),
                        'pay_status_model_shop_id'             => Helper::val($orderModel->paymentStatusModel->shopId),
                        'pay_status_model_status'              => Helper::val($orderModel->paymentStatusModel->status),
                        'pay_status_model_update_time'
                        => Helper::val($orderModel->paymentStatusModel->updateTime),
                        'point_model_point_usage'              => Helper::val($orderModel->pointModel->pointUsage),
                        'point_model_status'                   => Helper::val($orderModel->pointModel->status),
                        'point_model_used_point'               => Helper::val($orderModel->pointModel->usedPoint),
                        'postage_price'                        => Helper::val($orderModel->postagePrice),
                        'rbank_model_order_number'             => Helper::val($orderModel->RBankModel->orderNumber),
                        'rbank_model_commission_payer'
                        => Helper::val($orderModel->RBankModel->rbCommissionPayer),
                        'rbank_model_rbank_status'             => Helper::val($orderModel->RBankModel->rbankStatus),
                        'rbank_model_shop_id'                  => Helper::val($orderModel->RBankModel->shopId),
                        'rbank_model_transfer_commission'
                        => Helper::val($orderModel->RBankModel->transferCommission),
                        'request_price'                        => Helper::val($orderModel->requestPrice),
                        'rmid'                                 => Helper::val($orderModel->rmId),
                        'sa_order_model_bid_id'                => Helper::val($orderModel->saOrderModel->bidId),
                        'sa_order_model_comment'               => Helper::val($orderModel->saOrderModel->comment),
                        'sa_order_model_reg_date'              => Helper::val($orderModel->saOrderModel->regDate),
                        'seq_id'                               => Helper::val($orderModel->seqId),
                        'brand_name'
                               => Helper::val($orderModel->settlementModel->cardModel->brandName),
                        'card_no'
                        => Helper::val($orderModel->settlementModel->cardModel->cardNo),
                        'expiration_date'
                        => Helper::val($orderModel->settlementModel->cardModel->expYM),
                        'installment_desc'
                         => Helper::val($orderModel->settlementModel->cardModel->installmentDesc),
                        'owner_name'
                               => Helper::val($orderModel->settlementModel->cardModel->ownerName),
                        'pay_type'
                        => Helper::val($orderModel->settlementModel->cardModel->payType),
                        'settlement_name'
                        => Helper::val($orderModel->settlementModel->settlementName),
                        'shipping_date'                        => Helper::val($orderModel->shippingDate),
                        'shipping_term'                        => Helper::val($orderModel->shippingTerm),
                        'status'                               => Helper::val($orderModel->status),
                        'total_price'                          => Helper::val($orderModel->totalPrice),
                        'wish_deli_date'                       => Helper::val($orderModel->wishDeliveryDate),
                        'wrap_model1_del_wrap_flg'
                        => Helper::val($orderModel->wrappingModel1->deleteWrappingFlg),
                        'wrap_model1_is_included_tax'
                        => Helper::val($orderModel->wrappingModel1->isIncludedTax),
                        'wrap_model1_name'                     => Helper::val($orderModel->wrappingModel1->name),
                        'wrap_model1_price'                    => Helper::val($orderModel->wrappingModel1->price),
                        'wrap_model1_title'                    => Helper::val($orderModel->wrappingModel1->title),
                        'wrap_model2_del_wrap_flg'
                        => Helper::val($orderModel->wrappingModel2->deleteWrappingFlg),
                        'wrap_model2_is_included_tax'
                        => Helper::val($orderModel->wrappingModel2->isIncludedTax),
                        'wrap_model2_name'                     => Helper::val($orderModel->wrappingModel2->name),
                        'wrap_model2_price'                    => Helper::val($orderModel->wrappingModel2->price),
                        'wrap_model2_title'                    => Helper::val($orderModel->wrappingModel2->title),
                        'mail_serial'                          => md5((string)$orderModel->orderNumber),
                        'form_cancel_flg'                      => 0,
                        'process_flg'                          => 0,
                        'is_delete'                            => 0,
                        'created_at'                           => date('Y-m-d H:i:s')
                    ];
                    $tmp = Helper::combineColumn($columns, $tmp);
                    $dataInsert[] = $tmp;
                }
            }

            return [
                'error_code'  => $errorCode,
                'message'     => $message,
                'data_insert' => $dataInsert,
                'data_error'  => $dataError
            ];
        }
    }
    /**
     * Process export data to csv
     *
     * @param   $request  Request
     * @return json
     */
    public function processExportCsv(Request $request)
    {
        $data = $this->dataList($request)->getData();
        $type       = $request->input('type', null);
        $fileName   = "";
        $column     = [];
        $column = [
            'name_jp'                   => __('messages.mapping_mall'),
            'received_order_id'         => __('messages.received_order_id'),
            'order_date'                => __('messages.payment_order_date'),
            'delivery_date'             => __('messages.sold_date'),
            'different_type'            => __('messages.different_type'),
            'hrnb_payment_name'         => __('messages.search_setlement_hrnb'),
            'web_payment_name'          => __('messages.search_setlement_web'),
            'search_request_price_hrnb' => __('messages.search_request_price_hrnb'),
            'search_request_price_web'  => __('messages.search_request_price_web'),
            'disparity_of_price'        => __('messages.disparity_of_price'),
            'hrnb_order_status_name'    => __('messages.search_status_hrnb'),
            'web_order_status_name'     => __('messages.search_status_web'),
            'occurred_reason'           => __('messages.occurred_reason'),
            'process_content'           => __('messages.process_content'),
            'check'                     => __('messages.bulk'),
            'finished_date'             => __('messages.finished_date'),
        ];
        $fileName   = 'SettlementMapping-' . date('YmdHis') . ".csv";
        Storage::disk('local')->put($fileName, '');
        $url = Storage::disk('local')->getDriver()->getAdapter()->getPathPrefix() . $fileName;
        $file = fopen($url, 'a');
        fputs($file, mb_convert_encoding(implode(",", $column), 'Shift-JIS', 'UTF-8'). "\r\n");
        $processContent = Config::get('common.process_content');
        $occurredReason = Config::get('common.occurred_reason');
        $check = false;
        if (isset($data->data)) {
            if (count($data->data) > 0) {
                foreach ($data->data as $key => $value) {
                    $check = ((int)$value->is_corrected === 1 || (int)$value->is_deleted === 1) ? true  : false;
                    $arrTemp = [];
                    $arrTemp[] = $value->name_jp;
                    $arrTemp[] = $value->received_order_id;
                    $arrTemp[] = $value->order_date !== null ? date('Y-m-d', strtotime($value->order_date)) : '';
                    $arrTemp[] = $value->delivery_date !== null ? date('Y-m-d', strtotime($value->delivery_date)) : '';
                    $arrTemp[] = $value->different_type;
                    $arrTemp[] = $value->hrnb_payment_name;
                    $arrTemp[] = $value->web_payment_name;
                    $arrTemp[] = $value->request_price;
                    $arrTemp[] = $value->web_request_price;
                    $arrTemp[] = (int)$value->request_price - (int)$value->web_request_price;
                    $arrTemp[] = $value->hrnb_order_status_name;
                    $arrTemp[] = $value->web_order_status_name;
                    $arrTemp[] = (int)$value->occorred_reason !== 0 ? $occurredReason[$value->occorred_reason] : '';
                    $arrTemp[] = (int)$value->process_content !== 0 ? $processContent[$value->process_content] : '';
                    $arrTemp[] = !$check ? __('messages.match') : __('messages.mapping_active');
                    $arrTemp[] = $value->finished_date !== null ? date('Y-m-d', strtotime($value->finished_date)) : '';
                    $filter     = array("\r\n", "\n", "\r");
                    $contentCsv = implode(",", $arrTemp);
                    $contentCsv = str_replace($filter, '', $contentCsv);
                    fputs($file, mb_convert_encoding($contentCsv, 'Shift-JIS', 'UTF-8'). "\r\n");
                }
            }
        }
        fclose($file);
        return response()->json([
            'file_name' => $fileName,
            'status'     => 1,
        ]);
    }

    /**
     * Process data in file csv for pro mfk
     * @param   string  $readers file name in storage
     * @return  boolean
     */
    public function processProMFKCsv(
        $arr,
        $offset,
        $length,
        $fileName,
        $countErr = 0,
        $realFile = "",
        $fileCorrect = ""
    ) {
        $readers = array_slice($arr, $offset*$length, $length);
        if ($readers === null) {
            return false;
        }
        $arrCol = Config::get('common.csv_pro_mfk_data');
        $totalColumn = 7;
        $arrError = array();
        $num = 1;
        $checkHead = false;
        $checkFail = false;
        if ($countErr >= 3) {
            if ($offset === '0') {
                $checkHead = true;
            }
            $checkFail = true;
        }
        $url = Storage::disk('local')->getDriver()->getAdapter()->getPathPrefix() . $fileCorrect;
        $file = fopen($url, 'a');

        foreach ($readers as $key => $row) {
            if ($checkFail) {
                $arrError[] = trim($row) . ",-->" . __('messages.request_timeout');
                continue;
            }
            if (empty($row) || ($offset === '0' && $num === 1)) {
                $num++;
                continue;
            }
            $column   = str_getcsv($row, ",");
            if (count($column) !== $totalColumn) {
                $arrError[] = trim($row) . ",-->" . __('messages.number_column_not_match');
                continue;
            }
            $column  = array_map('trim', $column);
            $arrTemp = $column;
            $arrTemp = array_combine($arrCol, $column);
            $arrTemp['order_id']       = str_replace('_', '-', $arrTemp['order_id']);
            $arrTemp['payment_method'] = 12;
            $arrTemp['status']         = 8;
            $arrTemp['process_flg']    = 0;
            $arrTemp['is_delete']      = 0;
            $arrTemp['created_at']     = date('Y-m-d H:i:s');
            fputs($file, mb_convert_encoding(implode(",", $arrTemp), 'Shift-JIS', 'UTF-8') . "\r\n");
        }
        fclose($file);
        $Common = new Common;
        $nameCached  = explode(".", $fileName);
        $nameCached  = $nameCached[0];
        if (count($arrError) > 0) {
            if (!Cache::has($nameCached) && !$checkHead) {
                $header = array_slice($arr, 0, 1);
                $arrError = array_merge($header, $arrError);
            }
            mb_convert_variables('SJIS', 'UTF-8', $arrError);
            $Common->updateCached($nameCached, implode("\r\n", $arrError));
        } elseif (Cache::has($nameCached)) {
            $Common->updateCached($nameCached, '');
        }
        return true;
    }

    /**
     * Process data in file csv for pro paygent
     * @param   string  $readers file name in storage
     * @return  boolean
     */
    public function processProPaygentCsv(
        $arr,
        $offset,
        $length,
        $fileName,
        $countErr = 0,
        $realFile = "",
        $fileCorrect = ""
    ) {
        $readers = array_slice($arr, $offset*$length, $length);
        if ($readers === null) {
            return false;
        }
        $arrCol = [
            'payment_id',
            'is_deficit',
            'payment_price',
            'order_id',
            'temp_1',
            'temp_2',
            'order_date',
            'payment_date',
            'cancel_date',
        ];
        $totalColumn = 9;
        $arrError = array();
        $num = 1;
        $checkHead = false;
        $checkFail = false;
        if ($countErr >= 3) {
            if ($offset === '0') {
                $checkHead = true;
            }
            $checkFail = true;
        }
        $url = Storage::disk('local')->getDriver()->getAdapter()->getPathPrefix() . $fileCorrect;
        $file = fopen($url, 'a');

        foreach ($readers as $key => $row) {
            if ($checkFail) {
                $arrError[] = trim($row) . ",-->" . __('messages.request_timeout');
                continue;
            }
            if (empty($row) || ($offset === '0' && $num === 1)) {
                $num++;
                continue;
            }
            $column   = str_getcsv($row, ",");
            if (count($column) !== $totalColumn) {
                $arrError[] = trim($row) . ",-->" . __('messages.number_column_not_match');
                continue;
            }
            $column  = array_map('trim', $column);
            $arrTemp = $column;
            $arrTemp = array_combine($arrCol, $column);
            unset($arrTemp['temp_1']);
            unset($arrTemp['temp_2']);
            $arrTemp['order_id']       = 'DIY-' . $arrTemp['order_id'];
            $arrTemp['cancel_date']    = empty($arrTemp['cancel_date']) ? '\N' : $arrTemp['cancel_date'];
            $arrTemp['payment_method'] = 1;
            $arrTemp['status']         = $arrTemp['is_deficit'] == 1 ? 10 : 8;
            $arrTemp['process_flg']    = 0;
            $arrTemp['is_delete']      = 0;
            $arrTemp['created_at']     = date('Y-m-d H:i:s');
            fputs($file, mb_convert_encoding(implode(",", $arrTemp), 'Shift-JIS', 'UTF-8') . "\r\n");
        }
        fclose($file);
        $Common = new Common;
        $nameCached  = explode(".", $fileName);
        $nameCached  = $nameCached[0];
        if (count($arrError) > 0) {
            if (!Cache::has($nameCached) && !$checkHead) {
                $header = array_slice($arr, 0, 1);
                $arrError = array_merge($header, $arrError);
            }
            mb_convert_variables('SJIS', 'UTF-8', $arrError);
            $Common->updateCached($nameCached, implode("\r\n", $arrError));
        } elseif (Cache::has($nameCached)) {
            $Common->updateCached($nameCached, '');
        }
        return true;
    }
}
