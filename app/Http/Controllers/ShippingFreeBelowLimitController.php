<?php
/**
 * Shipping Free Below Limit Controller
 *
 * @package     App\Controllers
 * @subpackage  ShippingFreeBelowLimitController
 * @copyright   Copyright (c) 2017 RiverCrane! Corporation. All Rights Reserved.
 * @author      van.qui<van.qui.rcvn2012@gmail.com>
 */

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Validator;
use App\User;
use Auth;
use Config;

use App\Models\Backend\MstShippingFee;
use App\Models\Backend\MstMall;

class ShippingFreeBelowLimitController extends Controller
{
    /**
     * Show template for append data
     *
     * @param   $request  Request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        return view('shipping-free-below-limit.index');
    }

    /**
     * Show data list view
     *
     * @param   $request  Request
     * @return json
     */
    public function dataList(Request $request)
    {
        if ($request->ajax() === true) {
            $arraySearch    = array(
                'mall_id'           => $request->input('mall_id', null),
                'charge_price_from' => $request->input('charge_price_from', null),
                'charge_price_to'   => $request->input('charge_price_to', null),
                'start_date_from'   => $request->input('start_date_from', null),
                'start_date_to'     => $request->input('start_date_to', null),
                'is_delete'         => $request->input('is_delete', null),
                'per_page'          => $request->input('per_page', null)
            );
            $arraySort = [
                'index'             => $request->input('sort_index', null),
                'name_jp'           => $request->input('sort_mall_name', null),
                'charge_price'      => $request->input('sort_charge_price', null),
                'below_limit_price' => $request->input('sort_below_limit_price', null),
                'start_date'        => $request->input('sort_start_date', null),
                'end_date'          => $request->input('sort_end_date', null)
            ];

            $model   = new MstShippingFee();
            $mstMall = new MstMall();

            $mall    = $mstMall->getDataSelectBox()->map(function ($item, $key) {
                return ['key' => $item['id'], 'value' => $item['name_jp']];
            });

            $del = [
                ['key' => 0, 'value' => '無'],
                ['key' => 1, 'value' => '有']
            ];

            $rules = [
                'charge_price_from' => 'numeric',
                'charge_price_to'   => 'numeric',
                'start_date_from'   => 'date',
                'start_date_to'     => 'date'
            ];
            $validator = Validator::make($request->all(), $rules);
            if ($validator->fails()) {
                return response()->json([
                    'data'    => [],
                    'mallOpt' => $mall,
                    'delOpt'  => $del
                ]);
            }

            $data = $model->getData($arraySearch, array_filter($arraySort));

            return response()->json([
                'data'    => $data,
                'mallOpt' => $mall,
                'delOpt'  => $del
            ]);
        }
    }

    /**
     * Change is delete function
     *
     * @param   $request  Request
     * @return  json
     */
    public function changeDelete(Request $request)
    {
        if ($request->ajax() === true) {
            $params = $request->all();
            $model  = new MstShippingFee();

            $index    = $params['index'];
            $data     = [
                'is_delete' => $params['is_delete'],
                'up_ope_cd' => Auth::user()->tantou_code,
                'up_date'   => now()
            ];
            $status = $model->changeDelete($index, $data);
            return response()->json([
                'status' => $status
            ]);
        }
    }

    /**
     * Get data for save screen
     *
     * @param   $request  Request
     * @return json
     */
    public function getFormData(Request $request)
    {
        if ($request->ajax() === true) {
            $model  = new MstShippingFee();
            $index = $request->input('index', null);
            if (!empty($index)) {
                $data = $model->getItem($index);
            } else {
                $data = [];
            }

            $mstMall = new MstMall();
            $mall    = $mstMall->getDataSelectBox()->map(function ($item, $key) {
                return ['key' => $item['id'], 'value' => $item['name_jp']];
            });
            $mall->prepend(['key' => '', 'value' => '------']);

            $del = [
                ['key' => '', 'value' => '------'],
                ['key' => 0, 'value' => '無'],
                ['key' => 1, 'value' => '有']
            ];

            return response()->json([
                'data'    => $data,
                'mallOpt' => $mall,
                'delOpt'  => $del
            ]);
        }
    }

    /**
     * Create/Edit item
     *
     * @param   $request  Request
     * @return json
     */
    public function save(Request $request)
    {
        if ($request->ajax() === true) {
            $rules = [
                'index'             => 'exists:horunba.mst_shipping_fee,index',
                'mall_id'           => 'required|exists:horunba.mst_mall,id',
                'charge_price'      => 'required|numeric',
                'below_limit_price' => 'required|numeric',
                'start_date'        => 'required|date|before_or_equal:end_date',
                'end_date'          => 'required|date|after_or_equal:start_date',
                'is_delete'         => 'required|in:0,1'
            ];
            $validator = Validator::make($request->all(), $rules);
            if ($validator->fails()) {
                return response()->json([
                    'status'  => 0,
                    'method'  => 'save',
                    'message' => $validator->errors()
                ]);
            }

            $index          = $request->input('index', '');
            $mstShippingFee = new MstShippingFee();

            if ($index !== '') {
                $mstShippingFee            = $mstShippingFee->find($index);
            } else {
                $mstShippingFee->in_ope_cd = Auth::user()->tantou_code;
                $mstShippingFee->in_date   = now();
            }
            $mstShippingFee->up_ope_cd = Auth::user()->tantou_code;
            $mstShippingFee->up_date   = now();

            $mstShippingFee->mall_id           = $request->input('mall_id', '');
            $mstShippingFee->charge_price      = $request->input('charge_price', '');
            $mstShippingFee->below_limit_price = $request->input('below_limit_price', '');
            $mstShippingFee->start_date        = $request->input('start_date', '');
            $mstShippingFee->end_date          = $request->input('end_date', '');
            $mstShippingFee->is_delete         = $request->input('is_delete', '');

            $mstShippingFee->save();
            return response()->json([
                'status' => 1,
                'method' => 'save'
            ]);
        }

        return view('shipping-free-below-limit.save');
    }
}
