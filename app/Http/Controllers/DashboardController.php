<?php
/**
 * Dashboard Controller
 *
 * @package     App\Controllers
 * @subpackage  DashboardController
 * @copyright   Copyright (c) 2017 RiverCrane! Corporation. All Rights Reserved.
 * @author      van.qui<van.qui.rcvn2012@gmail.com>
 */

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\Models\Backend\TmpCheckEdit;

class DashboardController extends Controller
{

    /**
     * Show the application dashboard.
     *
     * @param   $request  Request
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return redirect()->action(
            'OrderManagementController@index'
        );
    }
    /**
     * Check item editing
     *
     * $params $request  Request
     * @return json
     */
    public function checkItemEdit(Request $request)
    {
        \Debugbar::disable();
        if ($request->ajax() === true && !empty($request->all())) {
            $data   = $request->all();
            // Check current item has existed
            $tmpCheckEdit = new TmpCheckEdit();
            $flg = $tmpCheckEdit->getItem($data['query']);
            if (!empty($data['key'])) {
                if (!empty($flg)) {
                    // Begin time update
                    $beginTimeUpdate        = strtotime($flg->in_date);
                    // current time update
                    $currentTime            = strtotime(date('Y-m-d H:i:s'));
                    // End session time update
                    $lastTimeUpdate         = strtotime($flg->up_date);
                    $totalTimesUpdate       = abs($lastTimeUpdate - $beginTimeUpdate);
                    $limitTime              = abs($currentTime - $beginTimeUpdate);
                    if (($currentTime - $lastTimeUpdate >= 10)) {
                        $flg->in_date          = date('Y-m-d H:i:s');
                        $flg->up_date          = date('Y-m-d H:i:s');
                        $flg->ope_cd           = Auth::user()->tantou_code;
                        $flg->tantou_name      = Auth::user()->tantou_last_name;
                        $flg->save();
                        return [
                            'accessFlg' => true,
                            'method' => 'save'
                        ];
                    }
                    // Check who is edit , if current session login diff row's information
                    if (Auth::user()->tantou_code !== $flg->ope_cd && $flg->is_avaliable === 1) {
                        return [
                            'accessFlg' => false,
                            'method'    => 'save',
                            'message'   => trans('messages.error_dupplicate_edit', ['name' => $flg->tantou_name])
                        ];
                    } elseif (Auth::user()->tantou_code === $flg->ope_cd) {
                        $message                = '';
                        $accessFlg              = true;
                        $limit = env('TIMEDURING', 600);
                        if ($flg->page_key === 'RECEIVE-MAIL-SAVE') {
                            $limit = 1200;
                        }
                        // Check limit time , if user does not process
                        if ($limitTime > $limit
                            && date("Y-m-d", strtotime(date("Y-m-d", $beginTimeUpdate))) ===
                            date("Y-m-d", strtotime(date('Y-m-d')))) {
                            $message    = trans('messages.error_during_time', ['time_edit' => $limit]);
                            $flg->delete();
                            $accessFlg          = false;
                        // Check t time , if user does not process
                        } else {
                            $flg->up_date       = date('Y-m-d H:i:s');
                            $flg->save();
                        }
                        return [
                            'accessFlg'     => $accessFlg,
                            'method'        => 'save',
                            'times'         => $totalTimesUpdate,
                            'message'       => $message
                        ];
                    }
                } else {
                    // No item => insert
                    if (empty($flg)) {
                        $tmpCheckEdit->page_key         = $data['query']['page_key'];
                        $tmpCheckEdit->primary_key      = $data['query']['primary_key'];
                        $tmpCheckEdit->ope_cd           = Auth::user()->tantou_code;
                        $tmpCheckEdit->tantou_name      = Auth::user()->tantou_last_name;
                        $tmpCheckEdit->in_date          = date('Y-m-d H:i:s');
                        $tmpCheckEdit->up_date          = date('Y-m-d H:i:s');
                        $tmpCheckEdit->is_avaliable     = 1;
                        $tmpCheckEdit->save();
                    }
                    return [
                        'accessFlg'     => true,
                        'method'        => 'save'
                    ];
                }
            }
        }
    }
}
