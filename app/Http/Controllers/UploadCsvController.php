<?php
/**
 * Controller for upload csv to folder
 *
 * @package    App\Http\Controllers
 * @subpackage UploadCsvController
 * @copyright  Copyright (c) 2018 RiverCrane! Corporation. All Rights Reserved.
 * @author     Le Hung<le.hung.rcvn2012@gmail.com>
 */

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator;
use File;
use Storage;
use Log;

class UploadCsvController extends Controller
{
    /**
     * Process save receive input
     *
     * @param  object $request
     * @return json
     */
    public function save(Request $request)
    {
        if ($request->ajax() === true) {
            $mallId = $request->input('mall');
            if ($mallId === 'biz') {
                $env  = env('CSVBIZ', 75);
            } elseif ($mallId === 'diy') {
                $env  = env('CSVDIY', 95);
            } else {
                $env  = env('CSVAMAZON', 50);
            }
            $fileUpload     = $request->file('attached_file_csv');
            $input['attached_file_csv_path']    = $fileUpload;
            $input['mall']                  = $request->input('mall');

            Validator::extend('mineCSV', function ($parameters, $value, $attribute) {
                $ext = $value->getClientOriginalExtension();
                if ($ext === "csv" || $ext === "tab" || $ext === "txt") {
                    return true;
                } else {
                    return false;
                }
            });
            Validator::extend('isSijs', function ($parameters, $value, $attribute) {
                $encode = mb_detect_encoding(File::get($value), mb_list_encodings(), true);
                if ($encode === "SJIS" || $encode === "SJIS-win") {
                    return true;
                } else {
                    return false;
                }
            });
            // setting up rules
            $rules['attached_file_csv_path']    = 'required|mineCSV|isSijs|max:10000';
            $rules['mall']                  = 'required';
            // doing the validation, passing post data, rules and the messages
            $validator = Validator::make($input, $rules);
            if ($validator->fails()) {
                Log::error($validator->errors());
                return array(
                    "process" => 'uploadCsv',
                    "flg"     => 0,
                    "message"     => $validator->errors(),
                );
            }
            $fileTmpPath    = $request->file('attached_file_csv')->getPathName();
            $fileName       = $request->file('attached_file_csv')->getClientOriginalName();

            $extension = $fileUpload->getClientOriginalExtension();
            $fh = fopen($fileTmpPath, 'r+');
            $lines = array();
            $i = -1;
            $j = 1;
            $rowFalse = [];
            $datas = [];
            $delimiter = ",";
            if ($extension === "tab") {
                $delimiter = "\t";
            }
            if ($extension === "csv" || $extension === "tab") {
                while (($row = fgetcsv($fh, 0, $delimiter)) !== false) {
                    $i++;
                    $datas[] = $row;
                    $count      = count($row);
                    if ((int)$count !== (int)$env) {
                        $rowFalse[$i] = trans('messages.alert_csv_message_error', ['name' => $i]);
                        if ($j >= 10) {
                            continue;
                        }
                        $j++;
                    }
                }
                if (count($rowFalse) > 0) {
                    return array(
                        "process"     => 'uploadCsv',
                        "flg"         => 0,
                        "flgCsv"      => 0,
                        "message"     => implode("<br/>", $rowFalse)
                    );
                }
            }
            if ($fileUpload->isValid() && count($rowFalse) === 0) {
                if ($mallId === 'biz') {
                    $filePath = Storage::disk('csv_biz')->path($fileName);
                    if (file_exists($filePath)) {
                        Storage::disk('csv_biz')::delete($filePath);
                    }
                    Storage::disk('csv_biz')->put($fileName, File::get($fileUpload));

                } elseif ($mallId === 'diy') {
                    $filePath = Storage::disk('csv_diy')->path($fileName);
                    if (file_exists($filePath)) {
                        Storage::delete($filePath);
                    }
                    Storage::disk('csv_diy')->put($fileName, File::get($fileUpload));
                } else {
                    $filePath = Storage::disk('csv_amazon')->path($fileName);
                    if (file_exists($filePath)) {
                        Storage::delete($filePath);
                    }
                    Storage::disk('csv_amazon')->put($fileName, File::get($fileUpload));
                }
                chmod("$filePath", 0777);
            }
            return array(
                    "flg"       => 1,
                    "flgCsv"    => 1,
                    "mall"      => null,
                    "attached_file_csv" => null,
                    "attached_file_csv_path" => null,
                    "message" => trans('messages.alert_csv_message_success', ['name' => $fileName])
                );
        }
        return view('upload-csv.save');
    }
}
