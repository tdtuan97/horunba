<?php
/**
 * CancelReason Controller
 *
 * @package     App\Controllers
 * @subpackage  CancelReasonController
 * @copyright   Copyright (c) 2017 RiverCrane! Corporation. All Rights Reserved.
 * @author      van.qui<van.qui.rcvn2012@gmail.com>
 */

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use App\User;
use Auth;

use App\Models\Backend\MstCancelReason;

class CancelReasonController extends Controller
{
    /**
     * Show template for append data
     *
     * @param   $request  Request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        return view('cancel-reason.index');
    }
    /**
     * Show data list view
     *
     * @param   $request  Request
     * @return json
     */
    public function dataList(Request $request)
    {
        if ($request->ajax() === true) {
            $arraySearch    = array(
                'type'           => $request->input('type', null),
                'reason_content' => $request->input('reason_content', null),
                'per_page'       => $request->input('per_page', null),
            );
            $arraySort = [
                'reason_id'       => $request->input('sort_reason_id', null),
                'type'            => $request->input('sort_type', null),
                'reason_content'  => $request->input('sort_reason_content', null),
                'add_2_mail_text' => $request->input('sort_add_2_mail_text', null)
            ];

            $model  = new MstCancelReason();
            $data   = $model->getData($arraySearch, $arraySort);
            return response()->json([
                'data'      => $data,
                'sort'      => $arraySort,
                'params'    => $request->all()
            ]);
        }
    }

    /**
     * Get data for save screen
     *
     * @param   $request  Request
     * @return json
     */
    public function getFormData(Request $request)
    {
        if ($request->ajax() === true) {
            $mstCancelReason = new MstCancelReason();
            $reasonId        = $request->input('reason_id', null);
            if (!empty($reasonId)) {
                $data = $mstCancelReason->getItem($reasonId);
            } else {
                $data = [];
            }

            return response()->json([
                'data'      => $data
            ]);
        }
    }

    /**
     * Create/Edit cancel reason
     *
     * @param   $request  Request
     * @return json
     */
    public function save(Request $request)
    {
        $mstCancelReason = new MstCancelReason();
        if ($request->ajax() === true) {
            $rules = [
                'reason_id'       => 'numeric|exists:horunba.mst_cancel_reason,reason_id',
                'type'            => 'required',
                'reason_content'  => 'required',
                'add_2_mail_text' => 'required'
            ];
            $validator = Validator::make($request->all(), $rules);
            if ($validator->fails()) {
                return response()->json([
                    'status'  => 0,
                    'method'  => 'save',
                    'message' => $validator->errors()
                ]);
            }

            $reasonId = $request->input('reason_id', '');

            if ($reasonId !== '') {
                $mstCancelReason            = $mstCancelReason->find($reasonId);
                $mstCancelReason->up_ope_cd = Auth::user()->tantou_code;
                $mstCancelReason->up_date   = date('Y-m-d H:i:s');
            } else {
                $mstCancelReason->in_ope_cd = Auth::user()->tantou_code;
                $mstCancelReason->up_ope_cd = Auth::user()->tantou_code;
                $mstCancelReason->in_date   = date('Y-m-d H:i:s');
                $mstCancelReason->up_date   = date('Y-m-d H:i:s');
            }
            $mstCancelReason->type            = $request->input('type', '');
            $mstCancelReason->reason_content  = $request->input('reason_content', '');
            $mstCancelReason->add_2_mail_text = $request->input('add_2_mail_text', '');

            $mstCancelReason->save();
            return response()->json([
                'status'  => 1,
                'method'  => 'save'
            ]);
        }
        return view('cancel-reason.save');
    }
}
