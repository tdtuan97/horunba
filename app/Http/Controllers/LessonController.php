<?php
/**
 * Controller for lesson
 *
 * @package    App\Http\Controllers
 * @subpackage LessonController
 * @copyright  Copyright (c) 2019 RiverCrane! Corporation. All Rights Reserved.
 * @author     Van Qui<van.qui@rivercrane.vn>
 */

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Backend\MstLesson;
use App\Models\Backend\MstTantou;
use Validator;
use Auth;
use Config;
use DB;

class LessonController extends Controller
{
    /**
     * Load view.
     */
    public function index()
    {
        return view('lesson.index');
    }

    /**
     * Get all data.
     * Return $object json
     */
    public function dataList(Request $request)
    {
        if ($request->ajax() === true) {
            $modelLesson = new MstLesson();

            $arrSort = [
                'lesson_id'       => $request->input('sort_lesson_id', null),
                'category_name'   => $request->input('sort_category_name', null),
                'lesson_name'     => $request->input('sort_lesson_name', null),
                'require_point'   => $request->input('sort_require_point', null),
                'priod'           => $request->input('sort_priod', null),
                'incharge_person' => $request->input('sort_incharge_person', null)
            ];

            $arrSearch = [
                'lesson_id'       => $request->input('lesson_id', null),
                'category_name'   => $request->input('category_name', null),
                'lesson_name'     => $request->input('lesson_name', null),
                'incharge_person' => $request->input('incharge_person', null),
                'per_page'        => $request->input('per_page', null)
            ];
            $data = $modelLesson->getData($arrSearch, $arrSort);

            return response()->json([
                'data'      => $data
            ]);
        }
    }
    
    /**
     * Get detail member.
     * @param request
     * Return $object json
     */
    public function detail(Request $request)
    {
        if ($request->ajax() === true) {
            $lessonId = $request->input('lesson_id', null);
            $data = [];
            $modelLesson = new MstLesson();
            $data['action'] = [
                'value'    => 'add',
                'is_error' => false,
                'message'  => '',
            ];
            if (!empty($lessonId)) {
                $dataLesson = $modelLesson->find($lessonId);
                if (!empty($dataLesson)) {
                    $data['action'] = [
                        'value'    => 'edit',
                        'is_error' => false,
                        'message'  => '',
                    ];
                    foreach ($dataLesson->toArray() as $key => $value) {
                        if ($key === 'lesson_id') {
                            $data[$key] = [
                                'value'    => $value,
                                'is_error' => false,
                                'message'  => '',
                                'readonly' => true
                            ];
                        } else {
                            $data[$key] = [
                                'value'    => $value,
                                'is_error' => false,
                                'message'  => '',
                            ];
                        }
                    }
                } else {
                    return response()->json([
                        'status'        => 0,
                        'link_redirect' => action('LessonController@index'),
                    ]);
                }
            }
            
            $optTantou = [['key' => '', 'value' => '------']];
            foreach (MstTantou::get(['tantou_code', 'tantou_last_name']) as $item) {
                $optTantou[] = ['key' => $item->tantou_code, 'value' =>  $item->tantou_last_name];
            }
            return response()->json([
                'data'      => $data,
                'optTantou' => $optTantou
            ]);
        }
    }
    
    /**
     * Save
     *
     * @return view
     */
    public function save()
    {
        return view('lesson.save');
    }

    /**
     * Submit form
     *
     * @param Request $request
     * @return json
     */
    public function submitForm(Request $request)
    {
        $rules = [
            'lesson_id'       => 'required',
            'category_name'   => 'required',
            'lesson_name'     => 'required',
            'require_point'   => 'required|numeric',
            'priod'           => 'required|int',
            'incharge_person' => 'required'
        ];
        
        $arrInput = [
            'lesson_id'       => $request->input('lesson_id', null),
            'category_name'   => $request->input('category_name', null),
            'lesson_name'     => $request->input('lesson_name', null),
            'require_point'   => $request->input('require_point', null),
            'priod'           => $request->input('priod', null),
            'incharge_person' => $request->input('incharge_person', null),
            'action'          => $request->input('action', null),
        ];
        
        $action = $arrInput['action'];
        if ($action === 'add') {
            $rules['lesson_id'] = 'required|unique:mst_lesson,lesson_id';
        }
        
        $optTantou = [['key' => '', 'value' => '------']];
        foreach (MstTantou::get(['tantou_code', 'tantou_last_name']) as $item) {
            $optTantou[] = ['key' => $item->tantou_code, 'value' =>  $item->tantou_last_name];
        }
        
        $validator = Validator::make($arrInput, $rules);
        if ($validator->fails()) {
            $arrResponse = [];
            $error = $validator->errors()->toArray();
            foreach ($arrInput as $key => $value) {
                $arrResponse[$key] = [
                    'value'    => is_null($value) ? '' : $value,
                    'is_error' => false,
                    'message'  => '',
                ];
                if (isset($error[$key])) {
                    $arrResponse[$key]['is_error'] = true;
                    $arrResponse[$key]['message']  = $error[$key][0];
                }
            }
            if ($action === 'edit') {
                $arrResponse['lesson_id']['readonly'] = true;
            }
            return response()->json([
                'data'     => $arrResponse,
                'status'   => 0,
                'optTantou' => $optTantou
            ]);
        } else {
            $modelL = MstLesson::find($arrInput['lesson_id']);
            if (empty($modelL)) {
                $modelL = new MstLesson();
                $modelL->in_ope_cd = \Auth::user()->tantou_code;
                $modelL->in_date   = date('Y-m-d');
            }
            $modelL->lesson_id       = $arrInput['lesson_id'];
            $modelL->category_name   = $arrInput['category_name'];
            $modelL->lesson_name     = $arrInput['lesson_name'];
            $modelL->require_point   = $arrInput['require_point'];
            $modelL->priod           = $arrInput['priod'];
            $modelL->incharge_person = $arrInput['incharge_person'];
            $modelL->up_ope_cd       = \Auth::user()->tantou_code;
            $modelL->up_date         = date('Y-m-d');
            $modelL->save();
        }
        return response()->json([
            'status'        => 1,
            'link_redirect' => action('LessonController@index'),
        ]);
    }
}
