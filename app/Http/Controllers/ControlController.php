<?php
/**
 * Controller for Controller for not permission
 *
 * @package    App\Http\Controllers
 * @subpackage ControlController
 * @copyright  Copyright (c) 2019 RiverCrane! Corporation. All Rights Reserved.
 * @author     Truong Nghia<truong.van.nghia@rivercrane.vn>
 */

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use Auth;
use App\Models\Backend\MstPermission;
use App\Models\Backend\MstController;

class ControlController extends Controller
{
    /**
     * Load view.
     */
    public function index()
    {
        return view('controller.index');
    }

    /**
     * Get all data.
     *
     * @param $request Request
     * @return json
     */
    public function dataList(Request $request)
    {
        if ($request->ajax() === true) {
            $modelC    = new MstController();
            $arrSearch = [
                'per_page' => $request->input('per_page', null),
            ];
            $data        = $modelC->getData($arrSearch);
            return response()->json([
                'data'       => $data,
            ]);
        }
    }

    /**
     * Save controller
     *
     * @param $request Request
     * @return json
     */
    public function saveController(Request $request)
    {
        if ($request->ajax() === true) {
            $rules = [
                'screen_class' => 'required',
            ];
            $validator = Validator::make($request->all(), $rules);
            if ($validator->fails()) {
                return response()->json([
                    'status'  => 0,
                    'message' => $validator->errors(),
                ]);
            }
            $model = new MstController();
            if (!empty($request->input('id', null))) {
                $model = $model->find($request->input('id', null));
            }
            $model->screen_name   = $request->input('screen_name', null);
            $model->screen_class  = $request->input('screen_class', null);
            $model->action_list   = $request->input('action_list', null);
            $model->action_access = $request->input('action_access', null);
            $model->no_list       = $request->input('no_list', null);
            $model->flg_access    = $request->input('flg_access', null);
            $model->save();
            return response()->json([
                'status'  => 1,
            ]);
        }
    }

    /**
     * Process update controller.
     *
     * @param $request Request
     * @return json
     */
    public function updateController(Request $request)
    {
        if ($request->ajax() === true) {
            $id = $request->input('key', null);
            $name = $request->input('name', null);
            $value = $request->input('value', null);
            $modelCtrl = new MstController();
            $modelCtrl->updateData(
                [
                    'id' => $id,
                ],
                [
                    $name => $value
                ]
            );
            return response()->json([
                'status' => 1
            ]);
        }
    }

    /**
     * Process update controller.
     *
     * @param $request Request
     * @return json
     */
    public function deleteController(Request $request)
    {
        if ($request->ajax() === true) {
            $id = $request->input('key', null);
            $modelCtrl = new MstController();
            $modelCtrl->find($id)->delete();
            return response()->json([
                'status' => 1
            ]);
        }
    }
}
