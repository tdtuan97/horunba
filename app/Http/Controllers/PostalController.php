<?php
/**
 * Postal Controller
 *
 * @package     App\Controllers
 * @subpackage  PostalController
 * @copyright   Copyright (c) 2018 RiverCrane! Corporation. All Rights Reserved.
 * @author      le.hung<le.hung.rcvn2012@gmail.com>
 */

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use Auth;
use Config;
use App\Models\Backend\MstPostal;
use App\Models\Backend\TmpCheckEdit;

class PostalController extends Controller
{
    /**
     * Show template for append data
     *
     * @param   $request  Request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        return view('postal.index');
    }
    /**
     * Show data list view
     *
     * @param   $request  Request
     * @return json
     */
    public function dataList(Request $request)
    {
        if ($request->ajax() === true) {
            $arraySearch    = array(
                'postal_code'       => $request->input('postal_code', null),
                'prefecture'        => $request->input('prefecture', null),
                'city'              => $request->input('city', null),
                'one_area_n_pos'    => $request->input('one_area_n_pos', null),
                'one_pos_n_area'    => $request->input('one_pos_n_area', null),
                'can_not_daihiki'   => $request->input('can_not_daihiki', null),
                'can_not_order_time'=> $request->input('can_not_order_time', null),
                'deli_days'         => $request->input('deli_days', null),
                'add_ship_charge'   => $request->input('add_ship_charge', null),
                'per_page'          => $request->input('per_page', null),
            );
            $arraySort = [
                'index'             => $request->input('sort_index', null),
                'postal_code'       => $request->input('sort_postal_code', null),
                'prefecture'        => $request->input('sort_prefecture', null),
                'sub_address'        => $request->input('sort_sub_address', null),
                'city'              => $request->input('sort_city', null),
                'one_area_n_pos'    => $request->input('sort_one_area_n_pos', null),
                'one_pos_n_area'    => $request->input('sort_one_pos_n_area', null),
                'can_not_daihiki'   => $request->input('sort_can_not_daihiki', null),
                'can_not_order_time'=> $request->input('sort_can_not_order_time', null),
                'deli_days'         => $request->input('sort_deli_days', null),
                'add_ship_charge'   => $request->input('sort_add_ship_charge', null),
                'full_address'   => $request->input('sort_full_address', null),
            ];

            $model             = new MstPostal();
            $data              = $model->getData($arraySearch, array_filter($arraySort));
            $dataAddShipCharge = $model->getShipCharge();
            return response()->json([
                'data'                      => $data,
                'data_add_ship_charge'      => $dataAddShipCharge,
                'sort'                      => $arraySort,
                'params'                    => $request->all(),
                'option'                    => Config::get('common.flg'),
            ]);
        }
    }

    /**
     * Get data for save screen
     *
     * @param   $request  Request
     * @return json
     */
    public function getFormData(Request $request)
    {
        if ($request->ajax() === true) {
            $mstPostal      = new MstPostal();
            $index          = $request->input('index', null);
            $itemCheck      = '';
            if (!empty($index)) {
                $data       = $mstPostal->getItem($index);
                // Check current item has existed
            } else {
                $data = [];
            }
            return response()->json([
                'data'          => $data
            ]);
        }
    }

    /**
     * Create/Edit cancel postal
     *
     * @param   $request  Request
     * @return json
     */
    public function changeFlag(Request $request)
    {
        $mstPostal = new MstPostal();
        if ($request->ajax() === true) {
            $params                 = $request->all();
            $params['data']['up_ope_cd']    = Auth::user()->tantou_code;
            $params['data']['up_date']      = now();
            $dataWhere              = $params['where'];
            $dataUpdate             = $params['data'];
            $mstPostal->updateData($dataWhere, $dataUpdate);
        }
    }
    /**
     * Create/Edit cancel postal
     *
     * @param   $request  Request
     * @return json
     */
    public function save(Request $request)
    {
        $mstPostal = new MstPostal();
        if ($request->ajax() === true) {
            $rules = [
                'postal_code'       => 'required',
                'sub_address'       => 'required',
                'full_address'       => 'required',
                'city'              => 'required',
                'prefecture'        => 'required',
                'one_area_n_pos'    => 'required|numeric',
                'one_pos_n_area'    => 'required|numeric',
                'can_not_daihiki'   => 'required|numeric',
                'can_not_order_time'=> 'required|numeric',
                'deli_days'         => 'required|numeric',
                'add_ship_charge'   => 'required|numeric'
            ];
            $validator = Validator::make($request->all(), $rules);
            if ($validator->fails()) {
                return response()->json([
                    'status'  => 0,
                    'method'  => 'save',
                    'message' => $validator->errors()
                ]);
            }

            $index = $request->input('index', '');
            $currentDataEdit = '';
            if ($index !== '') {
                $mstPostal            = $mstPostal->find($index);
                TmpCheckEdit::where('primary_key', 'index:' . $index)->delete();
            } else {
                $mstPostal->in_ope_cd = Auth::user()->tantou_code;
                $mstPostal->in_date   = now();
            }
            $mstPostal->up_ope_cd = Auth::user()->tantou_code;
            $mstPostal->up_date   = now();

            $mstPostal->postal_code             = $request->input('postal_code', '');
            $mstPostal->prefecture              = $request->input('prefecture', '');
            $mstPostal->city                    = $request->input('city', '');
            $mstPostal->sub_address             = $request->input('sub_address', '');
            $mstPostal->full_address            = $request->input('full_address', '');
            $mstPostal->one_area_n_pos          = $request->input('one_area_n_pos', '');
            $mstPostal->one_pos_n_area          = $request->input('one_pos_n_area', '');
            $mstPostal->can_not_daihiki         = $request->input('can_not_daihiki', '');
            $mstPostal->can_not_order_time      = $request->input('can_not_order_time', '');
            $mstPostal->deli_days               = $request->input('deli_days', '');
            $mstPostal->add_ship_charge         = $request->input('add_ship_charge', '');

            $mstPostal->save();
            return response()->json([
                'status'            => 1,
                'method'            => 'save',
                'currentDataEdit'   => $currentDataEdit
            ]);
        }
        return view('postal.save');
    }
}
