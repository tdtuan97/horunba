<?php
/**
 * Controller for batch management
 *
 * @package    App\Http\Controllers
 * @subpackage ReceiptController
 * @copyright  Copyright (c) 2018 RiverCrane! Corporation. All Rights Reserved.
 * @author     Truong Nghia<truong.van.nghia@rivercrane.vn>
 */

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Backend\DtReceiptLedger;
use App\Models\Backend\MstProductBase;
use Validator;
use Config;
use Storage;
use App\Http\Controllers\Common;
use Auth;

class ReceiptController extends Controller
{
    /**
     * Load view.
     */
    public function index()
    {
        return view('receipt.index');
    }

    /**
     * Show data list view
     *
     * @param   $request  Request
     * @return json
     */
    public function dataList(Request $request, $isExport = false, $limitEx = 5000, $offsetEx = 0)
    {
        if ($request->ajax() === true) {
            $arraySearch    = array(
                'stock_type'         => $request->input('stock_type', null),
                'stock_detail_type'  => $request->input('stock_detail_type', null),
                'recognite_key'      => $request->input('recognite_key', null),
                'product_code'       => $request->input('product_code', null),
                'up_date_from'       => $request->input('up_date_from', null),
                'up_date_to'         => $request->input('up_date_to', null),
                'per_page'           => $request->input('per_page', null),
            );
            $arraySort = [
                'index'             => $request->input('sort_index', null),
                'stock_type'        => $request->input('sort_stock_type', null),
                'stock_detail_type' => $request->input('sort_stock_detail_type', null),
                'recognite_key'     => $request->input('sort_recognite_key', null),
                'product_code'      => $request->input('sort_product_code', null),
                'product_name'      => $request->input('sort_product_name', null),
                'stock_num'         => $request->input('sort_stock_num', null),
            ];
            $model   = new DtReceiptLedger();
            $modePB  = new MstProductBase();
            $rules = [
                'up_date_from' => 'date',
                'up_date_to'   => 'date'
            ];
            $validator = Validator::make($request->all(), $rules);
            if ($validator->fails()) {
                if ($isExport) {
                    return [];
                }
                return response()->json([
                    'data'    => []
                ]);
            }
            $data = $model->getData($arraySearch, array_filter($arraySort), $isExport, $limitEx, $offsetEx)->toArray();
            if ($isExport) {
                $data['data'] = $data;
            }
            $arrInfoProduct = $modePB->getDataProduct(
                ['product_code', 'product_name'],
                array_column($data['data'], 'product_code')
            );
            if ($arrInfoProduct->count() !== 0) {
                $arrInfoProduct = $arrInfoProduct->keyBy('product_code')->toArray();
            } else {
                $arrInfoProduct = [];
            }
            $arrTemp = [];
            $arrDefault = ['product_name' => ''];
            if (count($data['data']) !== 0) {
                foreach ($data['data'] as $key => $value) {
                    if (isset($arrInfoProduct[$value['product_code']])) {
                        $arrTemp[] = $value + $arrInfoProduct[$value['product_code']];
                    } else {
                        $arrTemp[] = $value + $arrDefault;
                    }
                    // $dataDetail = $model->getDataDetail($value['index'], $value['product_code']);
                    // foreach ($dataDetail as $key => $item) {
                    //     if ($key === 0) {
                    //         $temp = $item->toArray();
                    //         $temp['count'] = $dataDetail->count();
                    //         $arrTemp[] = $value + $temp;
                    //     } else {
                    //         $arrTemp[] = $item;
                    //     }
                    // }
                }
                $data['data'] = $arrTemp;
            }
            if ($isExport) {
                return $data['data'];
            }
            $stockType = Config::get('common.stock_type');
            $stockDetailType = Config::get('common.stock_detail_type');
            return response()->json([
                'data'    => $data,
                'stockType' => $stockType,
                'stockDetailType' => $stockDetailType,
            ]);
        }
    }
    /**
     * Export csv
     *
     * @param   $request  Request
     * @return json
     */
    public function exportCsv(Request $request)
    {
        $csvHeader = [
           '区分',
           '分詳細',
           '対象ID',
           '商品コード',
           '商品名',
           '商品単価',
           '在庫数',
           '変動数',
           '更新日時'
        ];
        if ($request->ajax() === true) {
            $limit  = 100;
            $offset = 0;
            $index  = 0;
            
            $fileName = Auth::user()->tantou_code . '-' . time() . ".csv";
            $header   = mb_convert_encoding(implode(",", $csvHeader), 'Shift-JIS', 'UTF-8'). "\r\n";
            Storage::disk('local')->put($fileName, $header);
            $url  = Storage::disk('local')->getDriver()->getAdapter()->getPathPrefix() . $fileName;
            $file = fopen($url, 'a');
            do {
                $data = $this->dataList($request, true, $limit, $offset);
                if (count($data) === 0) {
                    break;
                }
                $row = '';
                foreach ($data as $key => $value) {
                    $val = [
                        $value['stock_type'],
                        $value['stock_detail_type'],
                        $value['recognite_key'],
                        $value['product_code'],
                        $value['product_name'],
                        $value['stock_num'],
                        $value['price_invoice'],
                        $value['change_num'],
                        $value['up_date']
                    ];
                    $row .= mb_convert_encoding(implode(",", $val), 'Shift-JIS', 'UTF-8') . "\r\n";
                }
                fputs($file, $row);
                $index++;
                $offset = $index * $limit;
            } while (count($data) >= $limit);
            fclose($file);
            return response()->json([
                'fileNameDownload' => $fileName
            ]);
        }
    }
    /**
     * Download csv
     *
     * @param   $request  Request
     * @return file download
     */
    public function downloadCsv(Request $request)
    {
        $params = $request->all();
        register_shutdown_function('unlink', storage_path('app/' . $params['fileName']));
        $headers = array(
            'Content-Encoding: Shift-JIS',
            'Content-Type' => 'text/csv',
        );
        $path = storage_path('app/' . $params['fileName']);
        return response()->download($path, "Export_".$params['fileName'], $headers);
    }
}
