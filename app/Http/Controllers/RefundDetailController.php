<?php
/**
 * Controller for refund management
 *
 * @package    App\Http\Controllers
 * @subpackage RefundDetailController
 * @copyright  Copyright (c) 2018 RiverCrane! Corporation. All Rights Reserved.
 * @author     le.hung<le.hung2012@gmail.com>
 */

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Backend\MstOrder;
use App\Models\Backend\MstOrderDetail;
use App\Models\Backend\DtReorder;
use App\Models\Backend\DtOrderProductDetail;
use App\Http\Controllers\Controller;
use Validator;
use Auth;
use DB;

class RefundDetailController extends Controller
{
    /**
     * Load view.
     */
    public function index()
    {
        return view('refund-detail.index');
    }
    /**
     * message error
     * @var array
     */
    private $errors = null;
    /**
     * Get form data
     *
     * @param   $request  Request
     * @return  json
     */
    public function getFormData(Request $request)
    {
        if ($request->ajax() === true) {
            return response()->json([
                    'data'          => array()
            ]);
        }
    }
    /**
     * Get info data
     *
     * @param   $request  Request
     * @return  json
     */
    public function getInfoOrderData(Request $request)
    {

        if ($request->ajax() === true) {
            $mstOrder       = new MstOrder();
            $dataOrder = array();
            $data = array();
            $dataCustomer = array();
            if ($request->get('origin_receive_order_id') !== null) {
                $data = $mstOrder->getItemForRefund($request->all());
                $dataCustomer = $mstOrder->getCusInfoByRecevieOrderId($request->get('origin_receive_order_id'));
            }
            return response()->json([
                    'dataOrder'    => $data,
                    'dataCustomer'  => $dataCustomer
            ]);
        }
    }
    /**
     * Submit save form data
     *
     * @param   $request  Request
     * @return  json
     */
    public function save(Request $request)
    {
        $mstOrder               = new MstOrder();
        $mstOrderDetail         = new MstOrderDetail();
        $dtReorder              = new DtReorder();
        $dtOrderProductDetail   = new DtOrderProductDetail();
        if ($request->ajax() === true && !empty($request->get('newDataOrder'))) {
            $params         = $request->all();
            $type           = $params['typeSubmit'];
            $dataReorder    = $dtReorder->getMaxItem();
            $reOrderId      = '0000001' ;
            $totalPrice     = 0;
            if (!empty($params['newDataOrder'])) {
                foreach ($params['newDataOrder'] as $key => $value) {
                    $totalPrice += $value['quantity'] * $value['price'];
                }
            }
            $checkPrice = false;
            if ($type === 'btnReturn') {
                $prefix        = 'returns';
                $productStatus = 16;
                $totalPrice    = -$totalPrice;
            } elseif ($type === 'btnResend' || $type === 'btnNotEnough') {
                $totalPrice    = 0;
                $prefix        = 'resends';
                $checkPrice    = true;
                $productStatus = 18;
            } elseif ($type === 'btnReorder') {
                $prefix        = 'reorder';
                $productStatus = 18;
            }
            if (!empty($dataReorder)) {
                $reOrderId  = filter_var($dataReorder->reorder_id, FILTER_SANITIZE_NUMBER_INT);
                $reOrderId  = $prefix . sprintf("%07d", (int)$reOrderId + 1);
            } else {
                $reOrderId  = $prefix . $reOrderId;
            }
            $rules = [
                'status'                  => 'required|integer|min:1|digits_between: 1,8',
                'origin_receive_order_id' => 'required'
            ];
            $validator = Validator::make($request->all(), $rules);
            if ($validator->fails()) {
                return response()->json([
                    'status'  => 0,
                    'method'  => 'save',
                    'message' => $validator->errors()
                ]);
            }
            try {
                DB::beginTransaction();
                // Insert dt_reorder
                $dataInsertReoder = [
                    'reorder_id'            => $reOrderId,
                    'received_order_id'     => $request->get('origin_receive_order_id', null),
                    're_order_type'         => $request->get('status', null),
                    'reorder_id'            => $reOrderId,
                    'in_ope_cd'             => Auth::user()->tantou_code,
                    'in_date'               => date('Y-m-d H:i:s'),
                    'up_ope_cd'             => Auth::user()->tantou_code,
                    'up_date'               => date('Y-m-d H:i:s')
                ];
                $dtReorder->insert($dataInsertReoder);
                $mallId = isset($params['dataCustomer']['mall_id']) ? $params['dataCustomer']['mall_id'] : 0;
                // Insert mst_order
                $companyName = isset($params['dataOrder'][0]) ? $params['dataOrder'][0]['company_name']  : '';
                $dataInsertOrder = [
                    'received_order_id'     => $reOrderId,
                    'mall_id'               => $mallId,
                    'customer_id'           => $request->get("dataCustomer", null)['customer_id'],
                    'used_coupon'           => 0,
                    'used_point'            => 0,
                    'discount'              => 0,
                    'total_price'           => $totalPrice,
                    'request_price'         => $totalPrice,
                    'order_date'            => date('Y-m-d H:i:s'),
                    'ship_charge'           => 0,
                    'pay_charge'            => 0,
                    'pay_after_charge'      => 0,
                    'pay_charge_discount'   => 0,
                    'goods_price'           => $totalPrice,
                    'goods_tax'             => 0,
                    'comment'               => null,
                    'arrive_type'           => 0,
                    'is_multi_ship_address' => 0,
                    'ship_wish_time'        => '',
                    'ship_wish_date'        => '',
                    'payment_status'        => 0,
                    'payment_method'        => 11,
                    'payment_request_date'  => null,
                    'payment_price'         => 0,
                    'payment_account'       => '',
                    'payment_date'          => null,
                    'payment_confirm_date'  => null,
                    'delivery_instrustions' => '【厳重梱包】',
                    'customer_question'     => '',
                    'shop_answer'           => null,
                    'cancel_reason'         => null,
                    'company_name'          => $companyName,
                    'is_delay'              => 1,
                    'delay_priod'           => date("Y/m/d", strtotime("+7 days")),
                    'delay_reason'          => null,
                    'mail_seri'             => null,
                    'order_status'          => ORDER_STATUS['NEW'],
                    'order_sub_status'      => ORDER_SUB_STATUS['NEW'],
                    'is_mail_sent'          => 0,
                    'is_ignore_error'       => 1,
                    'err_text'              => null,
                    'in_ope_cd'             => Auth::user()->tantou_code,
                    'in_date'               => date('Y-m-d H:i:s'),
                    'up_ope_cd'             => Auth::user()->tantou_code,
                    'up_date'               => date('Y-m-d H:i:s'),
                ];
                if ($type === 'btnReorder') {
                    $dataOldOrder = current($params['dataOrder']);
                    $dataInsertOrder['used_coupon']         = $dataOldOrder['used_coupon'];
                    $dataInsertOrder['used_point']          = $dataOldOrder['used_point'];
                    $dataInsertOrder['discount']            = $dataOldOrder['discount'];
                    $dataInsertOrder['total_price']         = $dataOldOrder['total_price'];
                    $dataInsertOrder['request_price']       = $dataOldOrder['request_price'];
                    $dataInsertOrder['ship_charge']         = $dataOldOrder['ship_charge'];
                    $dataInsertOrder['pay_charge']          = $dataOldOrder['pay_charge'];
                    $dataInsertOrder['pay_after_charge']    = $dataOldOrder['pay_after_charge'];
                    $dataInsertOrder['pay_charge_discount'] = $dataOldOrder['pay_charge_discount'];
                    $dataInsertOrder['goods_price']         = $dataOldOrder['goods_price'];
                    $dataInsertOrder['goods_tax']           = $dataOldOrder['goods_tax'];
                    $dataInsertOrder['payment_method']      = $dataOldOrder['payment_method'];
                }
                $returnReceiveId = $mstOrder->insertGetId($dataInsertOrder);
                // Insert mst_order_detail
                if (!empty($params['newDataOrder'])) {
                    $i = 0;
                    foreach ($params['newDataOrder'] as $key => $value) {
                        $i++;
                        $dataInsertOrderDetail = [
                            'receive_id'            => $returnReceiveId,
                            'detail_line_num'       => $i,
                            'product_code'          => $value['product_code'],
                            'product_name'          => $value['product_name'],
                            'price'                 => $checkPrice ? 0 : $value['price'],
                            'quantity'              => $value['quantity'],
                            'item_tax'              => 0,
                            'ship_price'            => 0,
                            'ship_tax'              => 0,
                            'receiver_id'           => $value['receiver_id'],
                            'in_ope_cd'             => Auth::user()->tantou_code,
                            'in_date'               => date('Y-m-d H:i:s'),
                            'up_ope_cd'             => Auth::user()->tantou_code,
                            'up_date'               => date('Y-m-d H:i:s')
                        ];
                        $mstOrderDetail->insert($dataInsertOrderDetail);
                        $dtOrderProductDetail->updateData(
                            [
                                'receive_id'        => $value['receive_id'],
                                'detail_line_num'   => $value['detail_line_num'],
                                'sub_line_num'      => $value['sub_line_num']
                            ],
                            [
                                'product_status'=> $productStatus
                            ]
                        );
                    }
                }
                DB::commit();
            } catch (\Exception $e) {
                $this->errors[] = $e->getMessage();
                DB::rollback();
            }
            return response()->json([
                'status'            => 1,
                'method'            => 'save',
                'receive_id'        => $returnReceiveId
            ]);
        }
        return view('refund-detail.save');
    }
}
