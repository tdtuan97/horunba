<?php
/**
 * UsersManagement Controller
 *
 * @package     App\Controllers
 * @subpackage  UsersManagementController
 * @copyright   Copyright (c) 2017 RiverCrane! Corporation. All Rights Reserved.
 * @author      van.qui<van.qui.rcvn2012@gmail.com>
 */

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

use Validator;
use App\User;
use Auth;
use Config;

use App\Models\Backend\MstTantou;
use App\Models\Backend\MstPermission;

class UsersManagementController extends Controller
{
    /**
     * Show template for append data
     *
     * @param   $request  Request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        return view('users-management.index');
    }
    /**
     * Show data list view
     *
     * @param   $request  Request
     * @return json
     */
    public function dataList(Request $request)
    {
        if ($request->ajax() === true) {
            $arraySearch    = array(
                'tantou_code'       => $request->input('tantou_code', null),
                'tantou_last_name'  => $request->input('tantou_last_name', null),
                'tantou_first_name' => $request->input('tantou_first_name', null),
                'tantou_department' => $request->input('tantou_department', null),
                'tantou_role'       => $request->input('tantou_role', null),
                'per_page'          => $request->input('per_page', null),
            );
            $arraySort = [
                'tantou_code'       => $request->input('sort_tantou_code', null),
                'tantou_last_name'  => $request->input('sort_tantou_last_name', null),
                'tantou_first_name' => $request->input('sort_tantou_first_name', null),
                'tantou_department' => $request->input('sort_tantou_department', null),
                'tantou_tel'        => $request->input('sort_tantou_tel', null),
                'tantou_mail'       => $request->input('sort_tantou_mail', null),
                'tantou_role'       => $request->input('sort_tantou_role', null)
            ];

            $model      = new MstTantou();
            $modelMP    = new MstPermission();
            $data       = $model->getData($arraySearch, array_filter($arraySort));
            $department = $modelMP->getDepartOption();

            $roleOpt = [];
            foreach (Config::get('common.tantou_role') as $key => $val) {
                $roleOpt[] = ['key' => $key, 'value' => $val];
            }

            return response()->json([
                'data'          => $data,
                'departmentOpt' => $department,
                'roleOpt'       => $roleOpt,
                'tantouRole'    => Config::get('common.tantou_role')
            ]);
        }
    }

    /**
     * Change active function
     *
     * @param   $request  Request
     * @return json
     */
    public function changeActive(Request $request)
    {
        if ($request->ajax() === true) {
            $params = $request->all();
            $model  = new MstTantou();

            $tantouCode = $params['tantou_code'];
            $isActive   = $params['is_active'];

            $status = $model->changeActive($tantouCode, $isActive);
            return response()->json([
                'status' => $status
            ]);
        }
    }

    /**
     * Get data for save screen
     *
     * @param   $request  Request
     * @return json
     */
    public function getFormData(Request $request)
    {
        if ($request->ajax() === true) {
            $mstTantou  = new MstTantou();
            $tantouCode = $request->input('tantou_code', null);
            if (!empty($tantouCode)) {
                $data = $mstTantou->getItem($tantouCode);
                $isAdmin = false;
                if ($tantouCode !== 'OPE99999' && $data->tantou_department != '999') {
                    $isAdmin = true;
                }
            } else {
                $data = [];
                $isAdmin = true;
            }

            $roleOpt[] = ['key' => '', 'value' => '------'];
            foreach (Config::get('common.tantou_role') as $key => $val) {
                $roleOpt[] = ['key' => $key, 'value' => $val];
            }
            $mstPermission  = new MstPermission();

            $departOpt = $mstPermission->getDepartOption($isAdmin);

            return response()->json([
                'data'      => $data,
                'roleOpt'   => $roleOpt,
                'departOpt' => array_merge([['key' => '' , 'value' => '-----']], $departOpt->toArray())
            ]);
        }
    }

    /**
     * Create/Edit cancel reason
     *
     * @param   $request  Request
     * @return json
     */
    public function save(Request $request)
    {
        $roleConfig = Config::get('common.tantou_role');
        $mstTantou  = new MstTantou();
        if ($request->ajax() === true) {
            $roleKeys      = array_keys($roleConfig);
            $rules = [
                'tantou_code'       => 'exists:horunba.mst_tantou,tantou_code',
                'tantou_last_name'  => 'required',
                'tantou_first_name' => 'required',
                'tantou_department' => 'required|exists:horunba.mst_permission,depart_id',
                'tantou_role'       => 'required|in:' . implode(',', $roleKeys),
                'tantou_tel'        => 'required|phone_number',
                'tantou_mail'       => 'required|email',
                'tantou_password'   => 'required_if:tantou_code,""|min:6,""'
                    . '|regex:/^.*(?=.{1,})(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[\d\X]).*$/',
                'is_active'         => 'required:in:0,1'
            ];
            $message = ['tantou_password.min' => 'パスワードの桁数が6桁以上ご設定ください。'];
            $validator = Validator::make($request->all(), $rules, $message);
            if ($validator->fails()) {
                return response()->json([
                    'status'  => 0,
                    'method'  => 'save',
                    'message' => $validator->errors()
                ]);
            }

            $tantouCode = $request->input('tantou_code', '');

            if ($tantouCode !== '') {
                $mstTantou            = $mstTantou->find($tantouCode);
                $mstTantou->up_ope_cd = Auth::user()->tantou_code;
            } else {
                $maxTantouCode          = $mstTantou->getMaxTantouCode();
                $tmpTantouCode          = (((int)str_replace('OPE', '', $maxTantouCode)) + 1);
                $tantouCode             = 'OPE' . sprintf("%05d", $tmpTantouCode);

                $mstTantou->tantou_code = $tantouCode;
                $mstTantou->in_ope_cd   = Auth::user()->tantou_code;
                $mstTantou->up_ope_cd   = Auth::user()->tantou_code;
            }

            $mstTantou->tantou_last_name  = $request->input('tantou_last_name', '');
            $mstTantou->tantou_first_name = $request->input('tantou_first_name', '');
            $mstTantou->tantou_department = $request->input('tantou_department', '');
            $mstTantou->tantou_role       = $request->input('tantou_role', '');
            $mstTantou->tantou_tel        = $request->input('tantou_tel', '');
            $mstTantou->tantou_mail       = $request->input('tantou_mail', '');
            $mstTantou->is_active         = $request->input('is_active', '');
            $tantouPass                   = $request->input('tantou_password', '');
            if ($tantouPass !== '') {
                $mstTantou->tantou_password   = Hash::make($request->input('tantou_password', ''));
                $mstTantou->last_renew_pass = date('Y-m-d H:i:s');
            }

            $mstTantou->save();
            return response()->json([
                'status' => 1,
                'method' => 'save'
            ]);
        }

        return view('users-management.save');
    }
}
