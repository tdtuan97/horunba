<?php
/**
 * SendMailTemplate Controller
 *
 * @package     App\Controllers
 * @subpackage  Send MailTemplateController
 * @copyright   Copyright (c) 2017 RiverCrane! Corporation. All Rights Reserved.
 * @author      le.hung<le.hung.rcvn2012@gmail.com>
 */

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Backend\DtSendMailList;
use Validator;
use App\User;
use Auth;

class SendMailController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @param   $request  Request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        return view('send-mail.index');
    }
    /**
     * Get all data
     *
     * @param   $request  Request
     * @return json
     */
    public function dataList(Request $request)
    {
        if ($request->ajax() === true) {
            $arraySearch    = array(
                'mail_id'           => $request->input('mail_id', null),
                'per_page'          => $request->input('per_page', null),
                'mail_subject'      => $request->input('mail_subject', null),
                'mail_contents'     => $request->input('mail_contents', null),
                'send_status'       => $request->input('send_status', null),
                'genre'             => $request->input('genre', null),
                'template_name'     => $request->input('template_name', null),
            );
            $arraySort = $request->input('sort', ['in_date' => 'desc']);
            $model  = new DtSendMailList();
            $data   = $model->getData($arraySearch, $arraySort);
            $links  = (string)$data->links();
            return response()->json([
                        'data'      => $data,
                        'sort'      => $arraySort,
                        'sorted'    => ($request->input('sort')) ? true : false,
                        'params'    => $request->all()

            ]);
        }
    }

    /**
     * Show more information detail
     *
     * @param   $request  Request
     * @return json
     */
    public function detail(Request $request)
    {
        if ($request->ajax() === true) {
            $model  = new DtSendMailList();
            $params = $request->all();
            if (!isset($params['receive_order_id']) ||
                !isset($params['order_status_id']) ||
                !isset($params['operater_send_index']) ||
                !isset($params['order_sub_status_id'])
            ) {
                $data = [];
            } else {
                $data = $model->getItem($request->all());
            }
            return response()->json([
                        'data'      => $data,
                        'params'    => $request->all()
            ]);
        }
        return view('send-mail.detail');
    }
    /**
     * Show more information detail
     *
     * @param   $request  Request
     * @return json
     */
    public function getItems(Request $request)
    {
        if ($request->ajax() === true) {
            $model  = new DtSendMailList();
            $data     = $model->getItems($request->all());
            return response()->json([
                        'data'      => $data,
                        'params'    => $request->all()
            ]);
        }
    }

    /**
     * Load content from db to iframe
     *
     * @param  Request $request
     * @return view
     */
    public function iframe(Request $request)
    {
        \Debugbar::disable();
        $model  = new DtSendMailList();
        $data = $model->getItem($request->all());
        return view('iframe.iframe_mail_receive', ['content' => $data->mail_content]);
    }
}
