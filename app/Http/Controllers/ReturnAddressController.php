<?php
/**
 * Return Management Controller
 *
 * @package     App\Controllers
 * @subpackage  ReturnAddressController
 * @copyright   Copyright (c) 2018 CriverCrane! Corporation. All Rights Reserved.
 * @author      le.hung<le.hung@rivercrance.com.vn>
 */

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Validator;
use App\User;
use Auth;
use Config;
use DB;
use App\Models\Backend\MstReturnAddress;
use App\Models\Backend\MstSupplier;

class ReturnAddressController extends Controller
{
    /**
     * Show template for append data
     *
     * @param   $request  Request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        return view('return-address.index');
    }
    /**
     * Show data list view
     *
     * @param   $request  Request
     * @return json
     */
    public function dataList(Request $request)
    {
        if ($request->ajax() === true) {
            $arraySearch    = array(
                'supplier_nm'       => $request->input('supplier_nm', null),
                'return_postal'     => $request->input('return_postal', null),
                'return_pref'       => $request->input('return_pref', null),
                'return_address'    => $request->input('return_address', null),
                'return_tel'        => $request->input('return_tel', null),
                'per_page'          => $request->input('per_page', null)
            );
            $arraySort = [
                'supplier_cd'       => $request->input('sort_supplier_cd', null),
                'return_nm'         => $request->input('sort_return_nm', null),
                'return_postal'     => $request->input('sort_return_postal', null),
                'return_pref'       => $request->input('sort_return_pref', null),
                'return_address'    => $request->input('sort_return_address', null),
                'return_tel'        => $request->input('sort_return_tel', null)
            ];
            $model   = new MstReturnAddress();
            $data = $model->getDataList($arraySearch, $arraySort)->toArray();
            return response()->json([
                'data' => $data
            ]);
        }
    }
    /**
     * Get info address
     *
     * @param   $request  Request
     * @return json
     */
    public function getFormData(Request $request)
    {
        if ($request->ajax() === true) {
            $supplierCd = $request->input('supplier_cd', null);
            $mstReturnAddress = new MstReturnAddress();
            $dataReturnAddress = $mstReturnAddress->getData($supplierCd);
            return response()->json([
                'time'   => time(),
                'dataReturnAddress'    => $dataReturnAddress,

            ]);
        }
    }
    /**
     * Get info address
     *
     * @param   $request  Request
     * @return json
     */
    public function getFormDataAddress(Request $request)
    {
        if ($request->ajax() === true) {
            $supplierCd = $request->input('supplier_cd', null);
            $mstReturnAddress = new MstReturnAddress();
            $where = ['mst_return_address.supplier_cd' => $supplierCd];
            $dataReturnAddress      = $mstReturnAddress->getDataReturnAddressSave($where)->first();
            $dataReturnAddressList  = $mstReturnAddress->getDataReturnAddressSave($where)->get();
            return response()->json([
                'time'   => time(),
                'status' => 1,
                'dataReturnAddress'         => $dataReturnAddress,
                'dataReturnAddressList'     => $dataReturnAddressList,

            ]);
        }
    }
    /**
     * Save address and return new address data
     *
     * @param   $request  Request
     * @return json
     */
    public function saveAddress(Request $request)
    {
        if ($request->ajax() === true) {
            $params = $request->all();
            $newValue = null;
            $messages = null;
            $arrIsSelected = [];
            $arrUpdate = [];
            $arrWhere = [];
            $arrTemp = [];
            $supplierCd = '';
            foreach ($params['dataReturnAddress'] as $key => $value) {
                $supplierCd = $value['supplier_cd'];
                $arrTemp['supplier_cd'] = $value['supplier_cd'];
                if (!isset($value['return_id'])) {
                    unset($value['new_item']);
                    $value['in_ope_cd'] = Auth::user()->tantou_code;
                    $value['in_date']   = date('Y-m-d H:i:s');
                    $value['up_ope_cd'] = Auth::user()->tantou_code;
                    $value['up_date']   = date('Y-m-d H:i:s');
                    $newValue[] = $value;
                    $rules = [
                        'supplier_cd'           => 'required|numeric|exists:mst_supplier,supplier_cd',
                        'return_nm'             => 'required|max:255',
                        'return_postal'         => 'required|max:8',
                        'return_address'        => 'required|max:255',
                        'return_pref'           => 'required|max:50',
                        'return_tel'            => 'required|max:15',
                        'is_selected'           => 'numeric',
                    ];
                    $validator = Validator::make($value, $rules);
                    if ($validator->fails()) {
                        $messages[$key] = $validator->errors();
                    }
                } else {
                    if ($value['is_selected'] === 1) {
                        $arrUpdate['is_selected'] = 1;
                        $arrUpdate['up_ope_cd']   = Auth::user()->tantou_code;
                        $arrUpdate['up_date']     = date('Y-m-d H:i:s');
                        $arrWhere['supplier_cd']  = $value['supplier_cd'];
                        $arrWhere['return_id']    = $value['return_id'];
                    }
                }
                $arrIsSelected[] = $value['is_selected'];
            }
            if (!empty($messages)) {
                return response()->json([
                    'status' => 0,
                    'messages' => $messages,
                    'data' => []
                ]);
            } elseif (!in_array(1, $arrIsSelected)) {
                return response()->json([
                    'status' => 0,
                    'messages' => __('messages.message_choose_address_return'),
                    'data' => []
                ]);
            }
            $mstReturnAddress = new MstReturnAddress();
            try {
                DB::beginTransaction();
                $mstReturnAddress->where($arrTemp)->update(['is_selected' => 0]);
                if (count($arrUpdate) !== 0) {
                    $mstReturnAddress->where($arrWhere)->update($arrUpdate);
                }
                if (!empty($newValue)) {
                    $mstReturnAddress->insert($newValue);
                }
                DB::commit();
            } catch (Exception $e) {
                DB::rollback();
            }
            $datas = $mstReturnAddress->getDataIsChoose($supplierCd);
            $dataResult = [];
            if (count($datas) !== 0) {
                $dataResult['address_name'] = '品住所変更';
                $dataResult['return_id']    = $datas->return_id;
            }
            return response()->json([
                'status' => 1,
                'data'   => $dataResult,
                'time'   => time(),
            ]);
        }
    }
    /**
     * Save address and return new address data
     *
     * @param   $request  Request
     * @return json
     */
    public function save(Request $request)
    {
        if ($request->ajax() === true) {
            $rules = [
                'supplier_cd' => 'required|numeric|exists:mst_supplier,supplier_cd',
                'return_nm' => 'required|max:255',
                'return_postal' => 'required|max:8',
                'return_address' => 'required|max:255',
                'return_pref' => 'required|max:50',
                'return_tel' => 'required|max:15'
            ];
            $validator = Validator::make($request->all(), $rules);
            if ($validator->fails()) {
                return response()->json([
                    'status'  => 0,
                    'message' => $validator->errors(),
                    'data'    => []
                ]);
            }
            $mstReturnAddress = new MstReturnAddress();
            $arrData['return_nm']        = $request->input('return_nm');
            $arrData['return_postal']    = $request->input('return_postal');
            $arrData['return_address']   = $request->input('return_address');
            $arrData['return_pref']      = $request->input('return_pref');
            $arrData['return_tel']       = $request->input('return_tel');
            try {
                DB::beginTransaction();
                if (!empty($request->input('supplier_cd')) && (int)$request->input('save')  === 1
                    && !empty($request->input('return_id'))) {
                    $arrData['up_ope_cd']       = Auth::user()->tantou_code;
                    $arrData['up_date']         = date('Y-m-d H:i:s');
                    $arrWhere['supplier_cd']    = $request->input('supplier_cd');
                    $arrWhere['return_id']      = $request->input('return_id');
                    $mstReturnAddress->updateData($arrWhere, $arrData);
                } else {
                    $mstReturnAddress->supplier_cd      = $request->input('supplier_cd');
                    $mstReturnAddress->return_nm        = $request->input('return_nm');
                    $mstReturnAddress->return_postal    = $request->input('return_postal');
                    $mstReturnAddress->return_address   = $request->input('return_address');
                    $mstReturnAddress->return_pref      = $request->input('return_pref');
                    $mstReturnAddress->return_tel       = $request->input('return_tel');
                    $mstReturnAddress->in_ope_cd        = Auth::user()->tantou_code;
                    $mstReturnAddress->in_date          = date('Y-m-d H:i:s');
                    $mstReturnAddress->save();
                }
                DB::commit();
            } catch (Exception $e) {
                DB::rollback();
            }
            return response()->json([
                'status'  => 1,
                'message' => [],
                'data'    => []
            ]);
        }
        return view('return-address.save');
    }
    
    /**
     * Get info supplier
     *
     * @param   $request  Request
     * @return json
     */
    public function getDataSupplier(Request $request)
    {
        if ($request->ajax() === true) {
            $rules = [
                'supplier_cd' => 'required|numeric|exists:mst_supplier,supplier_cd'
            ];
            
            $validator = Validator::make($request->all(), $rules);
            if ($validator->fails()) {
                return response()->json([
                    'status'  => 0,
                    'message' => $validator->errors(),
                    'dataInfo'    => []
                ]);
            }
            $mstSupplier        = new MstSupplier();
            $mstReturnAddress   = new MstReturnAddress();
            $cols = ['mst_supplier.supplier_nm', 'mst_return_address.return_nm'];
            $dataInfoAddress = $mstReturnAddress->select($cols)
                    ->join('mst_supplier', 'mst_supplier.supplier_cd', '=', 'mst_return_address.supplier_cd')
                    ->where(['mst_supplier.supplier_cd' => (int) $request->input('supplier_cd')])->first();
            
            $dataInfoSupplier = $mstSupplier->select(['supplier_nm'])
                    ->where(['supplier_cd' => (int) $request->input('supplier_cd')])->first();
            if (count($dataInfoAddress) > 0) {
                $dataInfo = $dataInfoAddress;
            } else {
                $dataInfo = $dataInfoSupplier;
            }

            $where = ['mst_return_address.supplier_cd' => (int) $request->input('supplier_cd')];
            $dataReturnAddressList  = $mstReturnAddress->getDataReturnAddressSave($where)->get();
            
            return response()->json([
                'status'  => 1,
                'message' => [],
                'dataInfo'=> $dataInfo,
                'dataReturnAddressList' => $dataReturnAddressList
            ]);
        }
    }
}
