<?php
/**
 * Controller for order to supplier by monthly
 *
 * @package    App\Http\Controllers
 * @subpackage OrderToSupplierMonthlyController
 * @copyright  Copyright (c) 2018 RiverCrane! Corporation. All Rights Reserved.
 * @author     Truong Nghia<truong.van.nghia@rivercrane.vn>
 */

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Notifications\SlackNotification;
use App\Notification;
use App\Models\Backend\MstProduct;
use App\Models\Backend\MstSupplier;
use App\Models\Backend\DtExtraOrderList;
use App\Models\Backend\DtOrderToSupplier;
use App\Models\Backend\MstStockStatus;
use App\Http\Controllers\Common;
use Validator;
use Storage;
use Config;
use Cache;
use Auth;
use DB;

class OrderToSupplierMonthlyController extends Controller
{
    /**
     * Load view.
     */
    public function index()
    {
        return view('ordertosup-monthly.index');
    }

        /**
     * Show data list view
     *
     * @param   $request  Request
     * @return json
     */
    public function dataList(Request $request)
    {
        if ($request->ajax() === true) {
            $arrDefault = array(array('key' => '_none', 'value' => '-----'));
            $arraySearch    = array(
                'product_code'  => $request->input('product_code', null),
                'supplier_id'   => $request->input('supplier_id', null),
                'is_ordered'    => $request->input('is_ordered', null),
                'is_cancel'     => $request->input('is_cancel', null),
                'per_page'      => $request->input('per_page', null),
            );
            $arraySort = [
                'product_code'  => $request->input('sort_product_code', null),
                'product_name'  => $request->input('sort_product_name', null),
                'supplier_id'   => $request->input('sort_supplier_id', null),
                'supplier_nm'   => $request->input('sort_supplier_name', null),
                'order_num'     => $request->input('sort_order_num', null),
                'price_invoice' => $request->input('sort_price_invoice', null),
                'order_price'   => $request->input('sort_order_price', null),
            ];
            $model   = new DtExtraOrderList();
            $data = $model->getData($arraySearch, array_filter($arraySort));

            return response()->json([
                'data'    => $data,
                'sort'    => $arraySort,
                'option'  => array_merge($arrDefault, Config::get('common.flg')),
            ]);
        }
    }

    /**
     * Process add and edit return order
     *
     * @param   $request  Request
     * @return json
     */
    public function save(Request $request)
    {
        if ($request->ajax() === true) {
            $modelEx       = new DtExtraOrderList();
            $modelP        = new MstProduct();
            $index         = $request->input('index', null);
            if ($request->status === 'add' || $request->status === 'edit') {
                goto end;
            }
            $rules = [
                'order_date'   => 'nullable|date',
                'order_num'    => 'required|numeric|min:1',
                'order_price'  => 'required|numeric|min:1',
                'product_code' => 'required|checkProductCodeExists',
            ];
            $validator = Validator::make($request->all(), $rules);
            if ($validator->fails()) {
                return response()->json([
                    'status'  => 0,
                    'message' => $validator->errors(),
                    'data'    => []
                ]);
            }
            if (!empty($index)) {
                $modelEx = $modelEx->find($index);
            }
            $arrFieldDate = ['order_date'];
            foreach ($request->all() as $key => $value) {
                if ($value === 'null' || $value === 'undefined') {
                    $value = in_array($key, $arrFieldDate) ? null : '';
                }
                $modelEx->{$key} = $value;
            }
            if (empty($index)) {
                $modelEx->in_ope_cd         = Auth::user()->tantou_code;
                $modelEx->in_date           = date('Y-m-d H:i:s');
            }

            $modelEx->up_ope_cd = Auth::user()->tantou_code;
            $modelEx->up_date   = date('Y-m-d H:i:s');
            $modelEx->save();
            return response()->json([
                'status'    => 1,
                'method'    => 'save',
                'return_no' => $modelEx->return_no,
                'message'   => 'Save success',
                'data'      => [],
            ]);
            end :
            $data = $modelEx->getDataDetail($index);
            return response()->json([
                'status'      => (!$data && !empty($index)) ? 999 : 0,
                'data'        => $data ? $data : null,
            ]);
        }
        return view('ordertosup-monthly.save');
    }

    /**
     * Get data product
     *
     * @param   $request  Request
     * @return json
     */
    public function getDataProduct(Request $request)
    {
        $productCode = $request->input('product_code', null);
        $model = new MstProduct();
        $info = $model->getInfproductAndSupplier($productCode);
        return response()->json([
            'dataInfo'    => $info,
        ]);
    }

    /**
     * Get data supplier
     *
     * @param   $request  Request
     * @return json
     */
    public function getDataSupplier(Request $request)
    {
        $supplierCode = $request->input('supplier_id', null);
        $model = new MstSupplier();
        $info = $model->getInfo('supplier_nm AS supplier_name', ['supplier_cd' => $supplierCode], false);
        return response()->json([
            'dataSupplier'    => $info,
        ]);
    }

    /**
     * Update field is_cancel and is_ordered
     *
     * @param   $request  Request
     * @return json
     */
    public function updateByCheckBox(Request $request)
    {
        $name  = $request->input('name', null);
        $value = $request->input('value', null);
        $index = $request->input('index', null);
        $modelEx = new DtExtraOrderList();
        $dataUp = [$name => $value ===0 ? 1 : 0];
        $dataUp['up_ope_cd'] = Auth::user()->tantou_code;
        $dataUp['up_date']   = date('Y-m-d H:i:s');
        $modelEx->where('index', $index)->update($dataUp);
        return response()->json([
            'status' => 1
        ]);
    }

    /**
     * process import data.
     * Return $object json
     */
    public function import(Request $request)
    {
        $type        = $request->input("type", "");
        $fileName    = $request->input("filename", "");
        $timeRun     = $request->input("timeRun", 0);
        $currPage    = $request->input("currPage", 0);
        $total       = $request->input("total", 0);
        $fileCorrect = $request->input("fileCorrect", "");
        $countErr    = $request->input("count_err", 0);
        $flg      = false;
        $common   = new Common;
        //
        $info = $common->checkFile($fileName);
        if ($info["flg"] === 0) {
            return response()->json($info);
        } else {
            $readers = $info["msg"];
        }
        if ($type === 'monthly_csv') {
            $flg = $this->processMonthLyCsv($readers, $currPage, $total, $fileName, (int)$countErr, $fileCorrect);
        }
        if ($currPage === $timeRun) {
            $arrCol = Config::get('common.csv_monthly');
            $common->processCorrect($fileCorrect, $arrCol, 'dt_extra_order_list');
        }
        $arrProcess = array(
            "fileName"    => $fileName,
            "timeRun"     => $timeRun,
            "currPage"    => $currPage,
            "total"       => $total,
            "fileCorrect" => $fileCorrect,
        );
        $infoRun = $common->processDataRun($flg, $arrProcess);
        return response()->json($infoRun);
    }

    /**
     * Process data in file csv for monthly
     * @param   string  $readers file name in storage
     * @return  boolean
     */
    public function processMonthLyCsv($arr, $offset, $length, $fileName, $countErr = 0, $fileCorrect = "")
    {
        $modelP = new MstProduct();
        $readers = array_slice($arr, $offset*$length, $length);
        if ($readers === null) {
            return false;
        }
        $arrCol = array(
            'product_code',
            'supplier_id',
            'quantiy',
            'order_price',
        );
        $rule = [
            'quantiy'      => 'required|numeric|min:1',
            'order_price'  => 'required|numeric|min:1',
        ];
        $totalColumn = 4;
        $arrCorrect = array();
        $arrError = array();
        $num = 1;
        $checkHead = false;
        $checkFail = false;
        if ($countErr >= 3) {
            if ($offset === '0') {
                $checkHead = true;
            }
            $checkFail = true;
        }
        $arrProduct = [];
        $url = Storage::disk('local')->getDriver()->getAdapter()->getPathPrefix() . $fileCorrect;
        $file = fopen($url, 'a');
        foreach ($readers as $key => $row) {
            if ($checkFail) {
                $arrError[] = trim($row) . ',-->' . __('messages.request_timeout');
                continue;
            }
            if (empty($row) || ($offset === '0' && $num === 1)) {
                $num++;
                continue;
            }
            $column   = str_getcsv($row, ",");
            if (count($column) !== $totalColumn) {
                $arrError[] = trim($row) . ',-->' . __('messages.number_column_not_match');
                continue;
            }

            $arrTemp = array();
            $column  = array_map('trim', $column);
            $arrTemp = array_combine($arrCol, $column);
            $arrTemp['supplier_id'] = (int)$arrTemp['supplier_id'];
            $arrTemp['quantiy']     = (int)$arrTemp['quantiy'];
            $arrTemp['order_price'] = (int)$arrTemp['order_price'];
            $validator = Validator::make($arrTemp, $rule);
            if ($validator->fails()) {
                $arrTemp['error_code'] = 'E';
                $arrTemp['error_message'] = __('messages.order_price_incorrect');
            } else {
                $arrTemp['error_code'] = '';
                $arrTemp['error_message'] = '';
            }
            $arrCorrect[] = $arrTemp;
            $arrProduct[] = $arrTemp['product_code'];
        }
        $dataProducts = $modelP->getDataCsvMonthly($arrProduct);
        foreach ($arrCorrect as $value) {
            $arrInserts['product_code']  = $value['product_code'];
            $arrInserts['supplier_id']   = $value['supplier_id'];
            $arrInserts['order_num']     = $value['quantiy'];
            $arrInserts['order_price']   = $value['order_price'];
            if (empty($dataProducts[$value['product_code']])) {
                $arrInserts['error_code'] = 'E';
                $arrInserts['error_message'] = __('messages.order_price_incorrect');
            } else {
                $dataProduct = $dataProducts[$value['product_code']];
                if ($value['order_price'] <= ($dataProduct->price_invoice * 0.5) ||
                    $value['order_price'] >= ($dataProduct->price_invoice * 1.5)) {
                    $arrInserts['error_code'] = 'E';
                    $arrInserts['error_message'] = __('messages.order_price_incorrect');
                } elseif ($value['supplier_id'] !== $dataProduct->price_supplier_id) {
                    $arrInserts['error_code'] = 'E';
                    $arrInserts['error_message'] = __('messages.supplier_id_incorrect');
                } else {
                    $arrInserts['error_code'] = $value['error_code'];
                    $arrInserts['error_message'] = $value['error_message'];
                }
            }
            $arrInserts['in_ope_cd']     = Auth::user()->tantou_code;
            $arrInserts['in_date']       = date('Y-m-d H:i:s');
            $arrInserts['up_ope_cd']     = Auth::user()->tantou_code;
            $arrInserts['up_date']       = date('Y-m-d H:i:s');
            fputs($file, mb_convert_encoding(implode(",", $arrInserts), 'Shift-JIS', 'UTF-8') . "\r\n");
        }
        fclose($file);
        $Common = new Common;
        $nameCached  = explode(".", $fileName);
        $nameCached  = $nameCached[0];
        if (count($arrError) > 0) {
            if (!Cache::has($nameCached) && !$checkHead) {
                $header = array_slice($arr, 0, 1);
                $arrError = array_merge($header, $arrError);
            }
            mb_convert_variables('SJIS', 'UTF-8', $arrError);
            $Common->updateCached($nameCached, implode("\r\n", $arrError));
        } elseif (Cache::has($nameCached)) {
            $Common->updateCached($nameCached, '');
        }
        return true;
    }
    /**
     * Process order to supplier
     * @param   string  $readers file name in storage
     * @return  boolean
     */
    public function processOrderToSupplier()
    {
        $modelEx  = new DtExtraOrderList();
        $modelOTS = new DtOrderToSupplier();
        $modelSS  = new MstStockStatus();
        $datas = $modelEx->getDataProcessOrder2Supplier();
        if (count($datas) === 0) {
            $infoRun = [];
            $infoRun['flg'] = 1;
            $infoRun['message_success'] = '';
            return response()->json($infoRun);
        } else {
            $index = 0;
            $yearMonth     = substr(date('Ym'), 3);
            $maxOrderCode  = $modelOTS->getMaxOrderCodeByDate($yearMonth);
            if (count($maxOrderCode) > 0) {
                $orderCode = $maxOrderCode->toArray()['order_code'];
                if (strpos($orderCode, $yearMonth) === 0) {
                    $index = (int)substr($orderCode, 3);
                }
            }
            $succ = 0;
            $infoRun = [];
            $infoRun['flg'] = 1;
            foreach ($datas as $value) {
                $arrUpdate = [];
                try {
                    if ($value->stock_lower_limit <= 0) {
                        $arrUpdate['error_code'] = 'E';
                        $arrUpdate['error_message'] = '南港の商品ではないです。';
                    } elseif ($value->rod_unit !== 0 && $value->order_num%$value->rod_unit !== 0) {
                        $arrUpdate['error_code'] = 'E';
                        $arrUpdate['error_message'] = '発注個数が良くないです。';
                    }
                    if ($value->cheetah_status === 4) {
                        $arrUpdate['error_code'] = 'E';
                        $arrUpdate['error_message'] = '廃番の商品です。';
                    }
                    if ($value->cheetah_status === 3) {
                        $arrUpdate['error_code'] = 'E';
                        $arrUpdate['error_message'] = 'この商品は現在、欠品です。';
                    }
                    if (count($arrUpdate) === 0) {
                        $arrInsert = [];
                        $index     += 1;
                        $orderCode  = $yearMonth . sprintf("%06d", $index);
                        $arrInsert['order_code']          = $orderCode;
                        $arrInsert['order_date']          = now();
                        $arrInsert['order_type']          = 4;
                        $arrInsert['supplier_id']         = $value->supplier_id;
                        $arrInsert['product_code']        = $value->product_code;
                        $arrInsert['product_jan']         = $value->product_jan;
                        $arrInsert['price_invoice']       = $value->order_price;
                        $arrInsert['order_num']           = $value->order_num;
                        $arrInsert['arrival_date_plan']   = null;
                        $arrInsert['arrival_date']        = null;
                        $arrInsert['arrival_quantity']    = 0;
                        $arrInsert['incomplete_quantity'] = 0;
                        $arrInsert['order_note']          = null;
                        $arrInsert['remarks']             = null;
                        $arrInsert['wait_days']           = 0;
                        $arrInsert['replenish_num']       = 0;
                        $arrInsert['times_num']           = 0;
                        $arrInsert['edi_order_code']      = $orderCode . $arrInsert['times_num'];
                        $arrInsert['in_date']             = now();
                        $arrInsert['up_date']             = now();

                        if ($value->supplier_id === 0) {
                            $error  = "------------------------------------------" . PHP_EOL;
                            $error .= basename(__CLASS__) . PHP_EOL;
                            $error .= "Order code $orderCode has supplier_id 0" . PHP_EOL;
                            $error .= PHP_EOL . "------------------------------------------" . PHP_EOL;
                            $slack = new Notification(CHANNEL['horunba']);
                            $slack->notify(new SlackNotification($error));
                        }

                        // Insert data table dt_order_to_supplier
                        $modelOTS->insert($arrInsert);
                        // Update table mst_stock_status
                        $modelSS->where('product_code', $value->product_code)
                            ->increment('rep_orderring_num', $value->order_num, ['up_date' => date('Y-m-d H:i:s')]);
                        $arrUpdate['is_ordered'] = 1;
                        $arrUpdate['order_code'] = $orderCode;
                    }
                    $arrUpdate['up_ope_cd'] = Auth::user()->tantou_code;
                    $arrUpdate['up_date']   = date('Y-m-d H:i:s');
                    // Update table dt_extr_order_list
                    $modelEx->updateData([$value->index], $arrUpdate);
                    $succ++;
                } catch (\Exception $e) {
                    $message = $e->getPrevious()->getMessage();
                    $infoRun['message_success'] = "ERROR: $message.";
                    return response()->json($infoRun);
                }
            }
            $infoRun['message_success'] = "Process success $succ records";
            return response()->json($infoRun);
        }
    }
}
