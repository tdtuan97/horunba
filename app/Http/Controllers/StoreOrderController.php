<?php
/**
 * Controller for store order
 *
 * @package    App\Http\Controllers
 * @subpackage StoreOrderController
 * @copyright  Copyright (c) 2018 RiverCrane! Corporation. All Rights Reserved.
 * @author     Van Qui<van.qui@rivercrane.vn>
 */

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Backend\DtStoreOrderInfo;
use App\Models\Backend\DtStoreOrderDetail;
use App\Models\Backend\MstStoreInfo;
use App\Models\Backend\MstStoreProduct;
use App\Models\Backend\MstProductControl;
use Validator;
use Auth;
use Config;
use DB;

class StoreOrderController extends Controller
{
    /**
     * Load view.
     */
    public function index()
    {
        return view('store-order.index');
    }

    /**
     * Get all data.
     * Return $object json
     */
    public function dataList(Request $request)
    {
        if ($request->ajax() === true) {
            $modelSOI = new DtStoreOrderInfo();
            $modelSI  = new MstStoreInfo();
            
            $statusConf = Config::get('common.store_order_status');
            $statusOpt  = array();
            foreach ($statusConf as $key => $value) {
                $statusOpt[] = ['key' => $key, 'value' => $value];
            }
            
            $storeOpt = array();
            foreach ($modelSI->get(['store_id', 'store_name']) as $item) {
                $storeOpt[] = ['key' => $item['store_id'], 'value' => $item['store_name']];
            }
            
            $arrSort = [
                'order_id'           => $request->input('sort_order_id', null),
                'order_date'         => $request->input('sort_order_date', null),
                'delivery_plan_date' => $request->input('sort_delivery_plan_date', null),
                'delivery_date'      => $request->input('sort_delivery_date', null),
                'received_date'      => $request->input('sort_received_date', null)
            ];
            $rule = [
                'order_id_from'           => 'nullable|numeric',
                'order_id_to'             => 'nullable|numeric',
                'delivery_plan_date_from' => 'nullable|date',
                'delivery_plan_date_to'   => 'nullable|date'
            ];
            $arrSearch = [
                'order_id_from'           => $request->input('order_id_from', null),
                'order_id_to'             => $request->input('order_id_to', null),
                'storage_store_id'        => $request->input('storage_store_id', null),
                'status'                  => $request->input('status', null),
                'delivery_plan_date_from' => $request->input('delivery_plan_date_from', null),
                'delivery_plan_date_to'   => $request->input('delivery_plan_date_to', null),
                'per_page'                => $request->input('per_page', null)
            ];
            $validator = Validator::make($arrSearch, $rule);
            if ($validator->fails()) {
                $data = [];
            } else {
                $data = $modelSOI->getData($arrSearch, $arrSort);
            }
            
            return response()->json([
                'data'      => $data,
                'statusOpt' => $statusOpt,
                'storeOpt'  => $storeOpt
            ]);
        }
    }
    
    /**
     * Get data detail
     *
     * @param Request $request
     * @return json
     */
    public function getDetail(Request $request)
    {
        if ($request->ajax() === true) {
            $storeOrderDetail = new DtStoreOrderDetail();
            $data = $storeOrderDetail->getDetail($request->input('order_id', null));
            return response()->json([
                'data' => $data
            ]);
        }
    }
    
    /**
     * Detail screen
     *
     * @param  Request $request
     * @return view
     */
    public function detail(Request $request)
    {
        $storeOrderInfo = DtStoreOrderInfo::find($request->input('order_id', null));
        $storeOrderStatus = Config::get('common.store_order_status');
        $storeOrderProcessStatus = Config::get('common.store_order_process_status');
        if (empty($storeOrderInfo)) {
            return redirect()->action('StoreOrderController@index');
        }
        return view(
            'store-order.detail',
            [
                'storeOrderInfo'          => $storeOrderInfo,
                'storeOrderStatus'        => $storeOrderStatus,
                'storeOrderProcessStatus' => $storeOrderProcessStatus
            ]
        );
    }
    
    /**
     * Set to order
     *
     * @param  Request $request
     * @return json
     */
    public function setToOrder(Request $request)
    {
        if ($request->ajax() === true) {
            $orderId     = $request->input('order_id', null);
            $productCode = $request->input('product_code', null);
            try {
                DB::beginTransaction();
                MstStoreProduct::where('product_code', $productCode)->update(['df_handling' => 1]);
                MstProductControl::where('product_code', $productCode)->update(['df_handling' => 1]);
                DtStoreOrderDetail::where('order_id', $orderId)
                    ->where('product_code', $productCode)
                    ->update([
                        'process_status' => 0,
                        'error_code'     => null,
                        'error_message'  => null
                    ]);
                DB::commit();
                return json_encode([
                    'flg' => 1,
                    'msg' => 'Update success'
                ]);
            } catch (\Exception $ex) {
                DB::rollback();
                return json_encode([
                    'flg' => 0,
                    'msg' => $ex->getMessage()
                ]);
            }
        }
    }
    
    /**
     * Re order
     *
     * @param  Request $request
     * @return json
     */
    public function reOrder(Request $request)
    {
        if ($request->ajax() === true) {
            $orderId     = $request->input('order_id', null);
            try {
                DB::beginTransaction();
                DtStoreOrderInfo::where('order_id', $orderId)->update(['status' => 2]);
                DB::commit();
                return json_encode([
                    'flg' => 1,
                    'msg' => 'Update success'
                ]);
            } catch (\Exception $ex) {
                DB::rollback();
                return json_encode([
                    'flg' => 0,
                    'msg' => $ex->getMessage()
                ]);
            }
        }
    }
}
