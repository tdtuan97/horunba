<?php
/**
 * Controller for payment management
 *
 * @package    App\Http\Controllers
 * @subpackage PaymentController
 * @copyright  Copyright (c) 2018 RiverCrane! Corporation. All Rights Reserved.
 * @author     Truong Nghia<truong.van.nghia@rivercrane.vn>
 */

namespace App\Http\Controllers;

use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use App\Models\Backend\MstMall;
use App\Http\Controllers\Common;
use App\Models\Backend\MstOrder;
use App\Http\Controllers\Controller;
use App\Models\Backend\DtPaymentList;
use App\Models\Backend\MstOrderStatus;
use App\Models\Backend\MstJapaneseYear;
use Validator;
use Storage;
use Cache;
use Auth;
use DB;

class PaymentController extends Controller
{
    /**
     * Load view.
     */
    public function index()
    {
        return view('payment.index');
    }

    /**
     * Get all data.
     * Return $object json
     */
    public function dataList(Request $request)
    {
        if ($request->ajax() === true) {
            $modelMall      = new MstMall();
            $modelOrderStatus = new MstOrderStatus();
            $modelOrder     = new MstOrder();
            $mallData       = $modelMall->getData();
            $orderStatusData= $modelOrderStatus->getDataSelect();
            $arrSort = [
                'name_jp'              => $request->input('sort_name_jp', null),
                'order_date'           => $request->input('sort_order_date', null),
                'received_order_id'    => $request->input('sort_received_order_id', null),
                'full_name'            => $request->input('sort_full_name', null),
                'order_status'         => $request->input('sort_order_status', null),
                'request_price'        => $request->input('sort_request_price', null),
                'request_payment_date' => $request->input('sort_request_payment_date', null),
                'pay_price'            => $request->input('sort_pay_price', null),
                'payment_date'         => $request->input('sort_payment_date', null),
                'remain_amount'        => $request->input('sort_remain_amount', null),
            ];
            $rule = [
                'order_date_from'           => 'nullable|date',
                'order_date_to'             => 'nullable|date',
                'request_payment_date_from' => 'nullable|date',
                'request_payment_date_to'   => 'nullable|date',
                'remain_amount'             => 'nullable|numeric',
            ];
            $arrSearch = [
                'name_jp'            => $request->input('name_jp', null),
                'per_page'           => $request->input('per_page', null),
                'order_date_from'    => $request->input('order_date_from', null),
                'order_date_to'      => $request->input('order_date_to', null),
                'request_price_from' => $request->input('request_price_from', null),
                'request_price_to'   => $request->input('request_price_to', null),
                'order_status'       => $request->input('order_status', null),
                'remain_amount'      => $request->input('remain_amount', null),
                'full_name'          => $request->input('full_name', null),
                'payment_account'    => $request->input('payment_account', null),
                'receive_id'         => $request->input('receive_id', null),
            ];
            $validator = Validator::make($arrSearch, $rule);
            if ($validator->fails()) {
                $data = [];
            } else {
                $data = $modelOrder->getDataPatment($arrSearch, $arrSort);
            }
            return response()->json([
                'data'          => $data,
                'mallData'      => $mallData->toArray(),
                'orderStatusData' => $orderStatusData->toArray(),
            ]);
        }
    }

    /**
     * Update data
     * Return $object json
     * @param Request $request
     * @return JsonResponse
     */
    public function updateReceive(Request $request)
    {
        if ($request->ajax() === true && $request->input('index') !== '') {
            $arrWhere['index'] = $request->input('index');
            $arrUpdate['receive_id'] = null;
            $arrUpdate['up_ope_cd']  = Auth::user()->tantou_code;
            $arrUpdate['up_date']    = date('Y-m-d H:i:s');
            $modelDPL = new DtPaymentList();
            try {
                DB::beginTransaction();
                $modelDPL->where($arrWhere)->update($arrUpdate);
                DB::commit();
                return response()->json([
                    'status' => 1
                ]);
            } catch (\Exception $e) {
                DB::rollback();
                $message = $e->getPrevious()->getMessage();
                return response()->json([
                    'error' => "ERROR: $message at row $index."
                ]);
            }
        }
    }
    /**
     * Delete data
     * Return $object json
     */
    public function deleteItem(Request $request)
    {
        if ($request->ajax() === true && $request->input('index_key') !== '') {
            $index = $request->input('index_key');
            $modelDPL = new DtPaymentList();
            try {
                DB::beginTransaction();
                $modelDPL->where('index', $index)->delete();
                DB::commit();
                return response()->json([
                    'status' => 1
                ]);
            } catch (\Exception $e) {
                DB::rollback();
                $message = $e->getPrevious()->getMessage();
                return response()->json([
                    'error' => "ERROR: $message at row $index."
                ]);
            }
        }
    }

    /**
     * process import data.
     * Return $object json
     */
    public function import(Request $request)
    {
        $type        = $request->input("type", "");
        $fileName    = $request->input("filename", "");
        $timeRun     = $request->input("timeRun", 0);
        $currPage    = $request->input("currPage", 0);
        $total       = $request->input("total", 0);
        $fileCorrect = $request->input("fileCorrect", "");
        $realFile    = $request->input("realFilename", "");
        $checkAfter  = (int)$request->input("checkAfter");
        $flg      = false;
        $common   = new Common;
        $modelDtPaymentList = new DtPaymentList();
        $info = $common->checkFile($fileName);
        if ($info["flg"] === 0) {
            return response()->json($info);
        } else {
            $readers = $info["msg"];
        }
        if ($type === 'mitsu_bank_csv') {
            array_shift($readers);
            array_pop($readers);
            array_pop($readers);
        }
        if ($checkAfter === 0) {
            $check    = $modelDtPaymentList->where('csv_file_name', $realFile)->first();
            if ($type === 'mitsu_bank_csv') {
                $env  = env('CSVMISTUBANK', 20);
            } else {
                $env  = env('CSVRAKUTENBANK', 8);
            }
            if (!empty($check)) {
                return array(
                    "flg" => 0,
                    "msg" => __('messages.message_choose_file_exist'),
                );
            }
            foreach ($readers as $keyM => $valueM) {
                $csvValues = str_getcsv($valueM, ",");
                $countValues = count($csvValues);
                if ($countValues !== $env) {
                    return array(
                        "flg" => 0,
                        "msg" => __('messages.message_choose_file_format_error'),
                    );
                }
            }
            $checkAfter = 1;
        }

        if ($type === 'mitsu_bank_csv') {
            $flg = $this->processMitsuBank($readers, $currPage, $total, $fileName, $realFile);
        } elseif ($type === 'rakuten_bank_csv') {
            $flg = $this->processRakutenBank($readers, $currPage, $total, $fileName, $realFile);
        }
        $arrProcess = array(
            "fileName"  => $fileName,
            "timeRun"   => $timeRun,
            "currPage"  => $currPage,
            "total"     => $total,
            "realFilename" => $realFile,
            "fileCorrect"  => $fileCorrect,
            "checkAfter"   => $checkAfter,
        );
        $infoRun = $common->processDataRun($flg, $arrProcess);
        return response()->json($infoRun);
    }

    /**
     * Process data in file csv for Mitsu Bank
     *
     * @param   array   array contains data csv
     * @param   int     $offset positon get data row
     * @param   int     $length total item to get
     * @param   string  $fileName file name in storage
     * @return  boolean
     */
    public function processMitsuBank($arr, $offset, $length, $fileName, $realFile)
    {
        $modelDPL   = new DtPaymentList();
        $modelOrder = new MstOrder();
        $modelJY    = new MstJapaneseYear();
        $dataJY     = $modelJY->get()->keyBy('ja_year');
        $readers    = array_slice($arr, $offset*$length, $length);
        if ($readers === null) {
            return false;
        }
        $arrCol = array(
            'data_type',
            'inquiry_num',
            'payment_date',
            'relative_date',
            'temp_1',
            'temp_2',
            'payment_price',
            'temp_3',
            'temp_4',
            'temp_5',
            'temp_6',
            'temp_7',
            'temp_8',
            'transfer_requester_code',
            'payment_name',
            'from_bank_name',
            'from_bank_brand_name',
            'temp_9',
            'temp_10',
            'temp_11',
        );
        $rule = [
            'transfer_requester_code' => 'required',
            'payment_name'            => 'required',
            'payment_price'           => 'required',
            'from_bank_name'          => 'required',
            'from_bank_brand_name'    => 'required',
            'payment_date'            => 'required',
            'relative_date'           => 'required',
            'inquiry_num'             => 'required',
        ];
        $totalColumn = 20;
        $arrCorrect  = array();
        $message     = '';
        $arrError    = [];
        foreach ($readers as $row) {
            if (empty($row)) {
                continue;
            }
            $column   = str_getcsv($row, ",");
            if (count($column) !== $totalColumn && (int)$column[0] === 2) {
                $arrError[] = trim($row) . ",-->" . __('messages.number_column_not_match');
                continue;
            }
            if ((int)$column[0] !== 2) {
                $arrError[] = trim($row) . ",-->" . "データ区分 diffrent 2.";
                continue;
            }
            $arrTemp = array();
            for ($i = 0; $i < $totalColumn; $i++) {
                $arrTemp[$arrCol[$i]] =  isset($column[$i]) ? trim($column[$i]) : "";
            }
            $validator = Validator::make($arrTemp, $rule);
            if ($validator->fails()) {
                $errorVal   = $validator->errors();
                $arrError[] = trim($row) . ',-->' . implode("|", array_column($errorVal->toArray(), 0));
                continue;
            }
            $arrCorrect[] = $arrTemp;
            try {
                DB::beginTransaction();
                $arrInserts   = [];
                $paymentDate  = '';
                $relativeDate = '';
                $receiveAccountNum = substr($arrTemp['transfer_requester_code'], 3, 7);
                if (!empty($arrTemp['payment_date'])) {
                    $year = substr($arrTemp['payment_date'], 0, 2);
                    if (isset($dataJY[$year])) {
                        $paymentDate = date(
                            'Y-m-d',
                            strtotime($dataJY[$year]->eu_year . substr($arrTemp['payment_date'], 2))
                        );
                    }
                }
                if (!empty($arrTemp['relative_date'])) {
                    $year = substr($arrTemp['relative_date'], 0, 2);
                    if (isset($dataJY[$year])) {
                        $relativeDate = date(
                            'Y-m-d',
                            strtotime($dataJY[$year]->eu_year . substr($arrTemp['relative_date'], 2))
                        );
                    }
                }
                $arrInserts['payment_code']         = 2;
                $arrInserts['payment_name']         = $arrTemp['payment_name'];
                $arrInserts['payment_price']        = $arrTemp['payment_price'];
                $arrInserts['inquiry_num']          = $arrTemp['inquiry_num'];
                $arrInserts['from_bank_name']       = $arrTemp['from_bank_name'];
                $arrInserts['from_bank_brand_name'] = $arrTemp['from_bank_brand_name'];
                $arrInserts['receive_account_num']  = $receiveAccountNum;
                $arrInserts['csv_file_name']        = $realFile;
                $arrInserts['payment_date']         = $paymentDate;
                $arrInserts['relative_date']        = $relativeDate;
                $arrInserts['in_ope_cd']            = Auth::user()->tantou_code;
                $arrInserts['in_date']              = now();
                $arrInserts['up_ope_cd']            = Auth::user()->tantou_code;
                $arrInserts['up_date']              = now();
                $check = $modelDPL->checkInsertMitsuBank($arrInserts);
                if ($check === 0) {
                    //$arrError[] = trim($row) . ',--> Row is exists.';
                    //continue;
                    $id           = $modelDPL->insertGetId($arrInserts);
                    $checkAccount = $modelOrder->getDataCheckAccount($receiveAccountNum, $arrTemp['payment_price']);
                    if ($checkAccount->count() !== 0 && $checkAccount->count_num === 1) {
                        $modelDPL->where(['index' => $id])
                                 ->update(['receive_id' => $checkAccount->receive_id]);

                        $arrUpdate = [];
                        if ($checkAccount->payment_status === 0) {
                            $arrUpdate['payment_status']        = 1;
                            $arrUpdate['payment_price']         = $arrTemp['payment_price'];
                            $arrUpdate['payment_date']          = $paymentDate;
                            $arrUpdate['payment_confirm_date']  = now();
                            $arrUpdate['up_ope_cd']             = Auth::user()->tantou_code;
                            $arrUpdate['up_date']               = now();
                            $modelOrder->updateData(['receive_id' => $checkAccount->receive_id], $arrUpdate);
                        }
                    }
                }
                DB::commit();
            } catch (\Exception $e) {
                DB::rollback();
                $arrError[] = trim($row) . ',-->' . $e->getMessage();
            }
        }
        if (count($arrError) > 0) {
            $Common = new Common;
            $nameCached  = explode(".", $fileName);
            $nameCached  = $nameCached[0];
            mb_convert_variables('SJIS', 'UTF-8', $arrError);
            $Common->updateCached($nameCached, implode("\r\n", $arrError));
        }
        return true;
    }

    /**
     * Process data in file csv for Rakuten Bank
     *
     * @param   array   array contains data csv
     * @param   int     $offset positon get data row
     * @param   int     $length total item to get
     * @param   string  $fileName file name in storage
     * @return  boolean
     */
    public function processRakutenBank($arr, $offset, $length, $fileName, $realFile)
    {
        $modelDPL = new DtPaymentList();
        $modelOrder     = new MstOrder();
        $readers = array_slice($arr, $offset*$length, $length);
        if ($readers === null) {
            return false;
        }
        $arrCol = array(
            'relative_date',
            'relative_time',
            'payment_date',
            'payment_name',
            'payment_price',
            'receive_order_id',
            'payment_status',
            'temp_1',
        );
        $totalColumn = 8;
        $rule = [
            'payment_name'     => 'required',
            'payment_price'    => 'required',
            'payment_status'   => 'required',
            'payment_date'     => 'required',
            'relative_date'    => 'required',
            'relative_time'    => 'required',
        ];
        $num     = 1;
        $message = '';
        $arrError = array();
        foreach ($readers as $key => $row) {
            if (empty($row) || ($offset === '0' && $num === 1)) {
                $num++;
                continue;
            }
            $column   = str_getcsv($row, ",");
            if (count($column) !== $totalColumn) {
                $arrError[] = trim($row) . ",-->" . __('messages.number_column_not_match');
                continue;
            }
            $arrTemp = array();
            for ($i = 0; $i < $totalColumn; $i++) {
                $arrTemp[$arrCol[$i]] =  isset($column[$i]) ? trim($column[$i]) : "";
            }
            $validator = Validator::make($arrTemp, $rule);
            if ($validator->fails()) {
                $errorVal  = $validator->errors();
                $arrError[] = trim($row) . ',-->' . implode("|", array_column($errorVal->toArray(), 0));
                continue;
            }
            try {
                DB::beginTransaction();
                $arrInserts = [];
                $arrInserts['payment_code']     = 6;
                $arrInserts['payment_name']     = str_replace([' ', '　'], ['', ''], $arrTemp['payment_name']);
                $arrInserts['payment_price']    = $arrTemp['payment_price'];
                $arrInserts['receive_order_id'] = $arrTemp['receive_order_id'];
                $arrInserts['payment_status']   = $arrTemp['payment_status'];
                $arrInserts['payment_date']     = $arrTemp['payment_date'];
                $arrInserts['relative_date']    = $arrTemp['relative_date'];
                $arrInserts['relative_time']    = $arrTemp['relative_time'];
                $arrInserts['csv_file_name']    = $realFile;
                $arrInserts['in_ope_cd']        = Auth::user()->tantou_code;
                $arrInserts['in_date']          = now();
                $arrInserts['up_ope_cd']        = Auth::user()->tantou_code;
                $arrInserts['up_date']          = now();
                $check = $modelDPL->checkInsertRakutenBank($arrInserts);
                if ($check !== 0) {
                    //$arrError[] = trim($row) . ',--> Row is exists.';
                    continue;
                }
                $id = $modelDPL->insertGetId($arrInserts);
                $dataCheck = $modelOrder->getDataCheckPayRakuten(
                    $arrTemp['receive_order_id'],
                    $arrTemp['payment_price']
                );
                if ($dataCheck->count() !== 0 && $dataCheck->count_num === 1) {
                    $modelDPL->where([
                        'index'  => $id,
                    ])->update(['receive_id' => $dataCheck->receive_id]);
                    if ($dataCheck->payment_status === 0) {
                        $arrUpdate['payment_status']       = 1;
                        $arrUpdate['payment_price']        = $arrTemp['payment_price'];
                        $arrUpdate['payment_date']         = $arrTemp['payment_date'];
                        $arrUpdate['payment_confirm_date'] = now();
                        $arrUpdate['up_ope_cd']            = Auth::user()->tantou_code;
                        $arrUpdate['up_date']              = now();
                        $modelOrder->updateData(['receive_id' => $dataCheck->receive_id], $arrUpdate);
                    }
                }
                DB::commit();
            } catch (\Exception $e) {
                DB::rollback();
                $arrError[] = trim($row) . ',-->' . $e->getMessage();
            }
        }
        if (count($arrError) > 0) {
            $Common = new Common;
            $nameCached  = explode(".", $fileName);
            $nameCached  = $nameCached[0];
            if (!Cache::has($nameCached)) {
                $header = array_slice($arr, 0, 1);
                $arrError = array_merge($header, $arrError);
            }
            mb_convert_variables('SJIS', 'UTF-8', $arrError);
            $Common->updateCached($nameCached, implode("\r\n", $arrError));
        }
        return true;
    }

    /**
     * Load view unknown list.
     */
    public function unknownList()
    {
        return view('payment.unknown');
    }

    /**
     * Process get data list payment unknown
     *
     * @param  object $request
     * @return json
     */
    public function dataListUnknown(Request $request)
    {
        if ($request->ajax() === true) {
            $modelPL     = new DtPaymentList();
            $arrSort = [
                'payment_name'        => $request->input('sort_payment_name', null),
                'payment_price'       => $request->input('sort_payment_price', null),
                'payment_date'        => $request->input('sort_payment_date', null),
                'relative_time'       => $request->input('sort_relative_time', null),
                'payment_code'        => $request->input('sort_payment_code', null),
                'receive_account_num' => $request->input('sort_receive_account_num', null),
            ];
            $rule = [
                'payment_price_from' => 'nullable|numeric',
                'payment_price_to'   => 'nullable|numeric',
                'account_num_from'   => 'nullable|numeric',
                'account_num_to'     => 'nullable|numeric',
            ];
            $arrSearch = [
                'payment_date_from'  => $request->input('payment_date_from', null),
                'payment_date_to'    => $request->input('payment_date_to', null),
                'per_page'           => $request->input('per_page', null),
                'payment_price_from' => $request->input('payment_price_from', null),
                'payment_price_to'   => $request->input('payment_price_to', null),
                'account_num_from'   => $request->input('account_num_from', null),
                'account_num_to'     => $request->input('account_num_to', null),
            ];
            $validator = Validator::make($arrSearch, $rule);
            if ($validator->fails()) {
                $data = [];
            } else {
                $data = $modelPL->getDataPatmentUnkown($arrSearch, $arrSort);
            }

            return response()->json([
                'data' => $data,
            ]);
        }
    }

    /**
     * Process get order by receive_id
     *
     * @param  object $request
     * @return json
     */
    public function getOrderByReceive(Request $request)
    {
        if ($request->ajax() === true) {
            $modelOrder     = new MstOrder();
            $modelPL     = new DtPaymentList();
            foreach ($request->all() as $key => $value) {
                $pos = strpos($key, 'receive_id_');
                if ($pos !== false) {
                    $index = str_replace('receive_id_', '', $key);
                    $paymentInfo = $modelPL->getPaymentById($index);
                    $data = [];
                    $data = $modelOrder->getOrderInput($value, $index, $paymentInfo->payment_price);
                    return response()->json([
                        'info' => count($data) !== 0 ? $data->toArray() : [],
                        'index' => $index
                    ]);
                }
            }
        }
    }

    /**
     * Process save receive input
     *
     * @param  object $request
     * @return json
     */
    public function saveReceive(Request $request)
    {
        if ($request->ajax() === true) {
            $modelPL      = new DtPaymentList();
            $modelOrder   = new MstOrder();
            $index        = $request->input('index', null);
            $receiveId    = $request->input('receive_id', null);
            $paymentCode  = $request->input('payment_code', null);
            $paymentPrice = $request->input('payment_price', null);
            $dataCheck   = $modelOrder->getDataByReceivedOrderId($receiveId);
            $flg = false;
            if ($dataCheck->payment_status === 1) {
                if ($dataCheck->request_price > $dataCheck->payment_price) {
                    $flg = true;
                    $cal = $dataCheck->request_price - ($paymentPrice + $dataCheck->payment_price);
                    $modelPL->updateData(
                        ['index' => $index],
                        [
                            'receive_id' => $dataCheck->receive_id,
                            'need_repayment' => $cal
                        ]
                    );
                } else {
                    return response()->json([
                        'status'  => 1,
                        'flg' => 1,
                        'messages' => '支払済でした。',
                    ]);
                }

            } elseif ($paymentCode !== $dataCheck->payment_method) {
                return response()->json([
                    'status'  => 1,
                    'flg' => 2,
                    'messages' => '支払方法が違っています。ご確認ください。',
                ]);
            } else {
                $flg = true;
                $cal = $dataCheck->request_price - $paymentPrice;
                $modelPL->updateData(
                    ['index' => $index],
                    [
                        'receive_id' => $dataCheck->receive_id,
                        'need_repayment' => $cal
                    ]
                );
            }
            if ($flg) {
                $cal = $dataCheck->request_price - ($paymentPrice + $dataCheck->payment_price);
                $dataPayment    = $modelPL->find($index);
                $paymentPriceUp = $paymentPrice + $dataCheck->payment_price;
                $arrUpdate      = [
                    'payment_status'       => 0,
                    'payment_date'         => null,
                    'payment_confirm_date' => null,
                    'payment_price'        => $paymentPriceUp,
                ];
                if ($cal <= 100) {
                    $arrUpdate   = [
                        'payment_status'       => 1,
                        'payment_date'         => $dataPayment->payment_date,
                        'payment_confirm_date' => now(),
                        'payment_price'        => $paymentPriceUp,
                    ];
                }
                $modelOrder->updateData(
                    ['receive_id' => $dataCheck->receive_id],
                    $arrUpdate
                );
            }
            return response()->json([
                'status' => 1,
                'flg'    => 0,
            ]);
        }
    }
}
