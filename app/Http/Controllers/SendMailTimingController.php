<?php
/**
 * SendMailTiming Controller
 *
 * @package     App\Controllers
 * @subpackage  SendMailTimingController
 * @copyright   Copyright (c) 2017 RiverCrane! Corporation. All Rights Reserved.
 * @author      le.hung<le.hung.rcvn2012@gmail.com>
 */

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Backend\MstSendMailTiming;
use App\Models\Backend\MstGenre;
use App\Models\Backend\MstMailParameter;
use App\Models\Backend\MstOrderStatus;
use App\Models\Backend\MstOrderSubStatus;
use App\Models\Backend\MstMailTemplate;
use Validator;
use App\User;
use Auth;

class SendMailTimingController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @param   $request  Request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        return view('send-mail-timing.index');
    }

    /**
     * Show the application dashboard.
     *
     * @param   $request  Request
     * @return \Illuminate\Http\Response
     */
    public function dataList(Request $request)
    {
        if ($request->ajax() === true) {
            $arraySearch    = array(
                'mail_id'           => $request->input('mail_id', null),
                'per_page'          => $request->input('per_page', null),
                'status_name'       => $request->input('status_name', null),
                'sub_status_name'   => $request->input('sub_status_name', null),
                'template_name'     => $request->input('template_name', null),
            );
            $arraySort['mst_send_mail_timing.in_date'] = 'desc';
            $model  = new MstSendMailTiming();
            $data   = $model->getData($arraySearch, $arraySort);
            $links  = (string)$data->links();
            return response()->json([
                        'data'  => $data,
                        'params' => $request->except(['order_status_id', 'order_sub_status_id', 'is_available'])
            ]);
        }
    }

    /**
     * Change status function
     *
     * @param   $request  Request
     * @return json
     */
    public function changeStatus(Request $request)
    {
        if ($request->ajax() === true) {
            $params  = $request->all();
            $modelMT = new MstSendMailTiming();

            $keys['order_sub_status_id'] = $params['order_sub_status_id'];
            $keys['order_status_id']     = $params['order_status_id'];
            $data['is_available']        = $params['is_available'];

            $modelMT->changeStatus($keys, $data);
            return response()->json([
                'status'  => 1,
                'message' => 'change success'
            ]);
        }
    }

    /**
     * Get form data
     *
     * @param   $request  Request
     * @return  json
     */
    public function getFormData(Request $request)
    {
        if ($request->ajax() === true) {
            $mstOrderStatus    = new MstOrderStatus();
            $mstOrderSubStatus = new MstOrderSubStatus();
            $mstGenre          = new MstGenre();
            $mstMailTemplate   = new MstMailTemplate();
            $mstSendMailTiming = new MstSendMailTiming();
            $resOrderStatus    = $mstOrderStatus->get(['order_status_id', 'status_name']);
            $arrDefaultOption  = array(array('key' => '_none', 'value' => '-----'));
            $orderStatus       = $arrDefaultOption;
            if ($resOrderStatus->count() > 0) {
                foreach ($resOrderStatus as $item) {
                    $orderStatus[] = [
                        'key'   => $item->order_status_id,
                        'value' => $item->status_name
                    ];
                }
            }
            $resOrderSubStatus = $mstOrderSubStatus->get(['order_sub_status_id', 'sub_status_name']);
            $orderSubStatus = $arrDefaultOption;
            if ($resOrderSubStatus->count() > 0) {
                foreach ($resOrderSubStatus as $item) {
                    $orderSubStatus[] = [
                        'key'   => $item->order_sub_status_id,
                        'value' => $item->sub_status_name
                    ];
                }
            }
            $orderStatusId    = $request->input('order_status_id', null);
            $orderSubStatusId = $request->input('order_sub_status_id', null);
            $itemMailTiming   = $mstSendMailTiming->getItem($orderStatusId, $orderSubStatusId);
            $mailId = null;
            $genreId = null;
            if (!empty($itemMailTiming)) {
                $itemMailTemplate = $mstMailTemplate->find($itemMailTiming->mail_id);
                if (!empty($itemMailTemplate)) {
                    $mailId  = $itemMailTemplate->mail_id;
                    $genreId = $itemMailTemplate->genre;
                }
            } else {
                $orderStatusId    = null;
                $orderSubStatusId = null;
            }

            $resGenre = $mstGenre->get(['genre_id', 'genre_name']);
            $genre    = $arrDefaultOption;
            if ($resGenre->count() > 0) {
                foreach ($resGenre as $item) {
                    $genre[] = [
                        'key'   => $item->genre_id,
                        'value' => $item->genre_name
                    ];
                }
            }
            $mailTemplate = $arrDefaultOption;
            if (count($genre) > 0) {
                if ($genreId === null) {
                    $genreId = $genre[0]['key'];
                }
                $resMailTemplate = $mstMailTemplate->getTemplateNameByGenre($genreId);
                if ($resMailTemplate->count() > 0) {
                    foreach ($resMailTemplate as $item) {
                        $mailTemplate[] = [
                            'key'   => $item->mail_id,
                            'value' => $item->template_name
                        ];
                    }
                }
            }
            return response()->json([
                    'orderStatusOption'    => $orderStatus,
                    'orderSubStatusOption' => $orderSubStatus,
                    'genreOption'          => $genre,
                    'mailTemplateOption'   => $mailTemplate,
                    'order_status_id'      => $orderStatusId,
                    'order_sub_status_id'  => $orderSubStatusId,
                    'mail_id'              => $mailId,
                    'genre_id'             => $genreId,
            ]);
        }
    }

    /**
     * Get template name by genre
     *
     * @param   Request $request
     * @return  json
     */
    public function getTemplateName(Request $request)
    {
        if ($request->ajax() === true) {
            $mstMailTemplate   = new MstMailTemplate();
            $arrDefaultOption  = array(array('key' => '_none', 'value' => '-----'));
            $genre = $request->input('genre', '');
            $mailTemplate = [];
            if (count($genre) > 0) {
                $resMailTemplate = $mstMailTemplate->getTemplateNameByGenre($genre);
                if ($resMailTemplate->count() > 0) {
                    foreach ($resMailTemplate as $item) {
                        $mailTemplate[] = [
                            'key'   => $item->mail_id,
                            'value' => $item->template_name
                        ];
                    }
                }
            }
            return response()->json([
                'mailTemplateOption' => array_merge($arrDefaultOption, $mailTemplate),
            ]);
        }
    }
   /**
     * Get template name by id
     *
     * @param   Request $request
     * @return  json
     */

    public function getTemplateId(Request $request)
    {
        if ($request->ajax() === true) {
            $mstMailTemplate   = new MstMailTemplate();
            $genre = $request->input('genre', '');
            $mailTemplate = [];
            if (count($genre) > 0) {
                $resMailTemplate = $mstMailTemplate->getTemplateNameByGenre($genre);
                if ($resMailTemplate->count() > 0) {
                    foreach ($resMailTemplate as $item) {
                        $mailTemplate[$item->mail_id] = $item->template_name;
                    }
                }
            }
            return response()->json([
                'mailTemplateOption' => $mailTemplate
            ]);
        }
    }

    /**
     * Add send mail timing
     *
     * @return json
     */
    public function save(Request $request)
    {
        if ($request->ajax() === true) {
            $rules = [
                'order_status_id' => 'required|numeric|exists:horunba.mst_order_status,order_status_id',
                'order_sub_status_id' => 'required|numeric|exists:horunba.mst_order_sub_status,order_sub_status_id',
                'mail_id' => 'required|numeric|exists:horunba.mst_mail_template,mail_id',
            ];
            $mailTemplate = array(array('key' => '_none', 'value' => '-----'));
            if ($request->genre !== '') {
                $mstMailTemplate   = new MstMailTemplate();
                $resMailTemplate = $mstMailTemplate->getTemplateNameByGenre($request->genre);
                if ($resMailTemplate->count() > 0) {
                    foreach ($resMailTemplate as $item) {
                        $mailTemplate[] = [
                            'key'   => $item->mail_id,
                            'value' => $item->template_name
                        ];
                    }
                }
            }
            $validator = Validator::make($request->all(), $rules);
            if ($validator->fails()) {
                return response()->json([
                    'status'  => 0,
                    'method'  => 'save',
                    'message' => $validator->errors(),
                    'mailTemplateOption' => $mailTemplate
                ]);
            }
            $orderStatusId    = $request->input('order_status_id');
            $orderSubStatusId = $request->input('order_sub_status_id');
            $mailId           = $request->input('mail_id');
            $dataMailTiming = [
                'order_status_id'     => $orderStatusId,
                'order_sub_status_id' => $orderSubStatusId,
                'mail_id'             => $mailId
            ];

            $sendMailTiming = new MstSendMailTiming();
            if ($sendMailTiming->saveData($dataMailTiming)) {
                return response()->json([
                    'status'  => 1,
                    'method'  => 'save',
                    'message' => 'Save mail timing success'
                ]);
            }
            return response()->json([
                'status'  => 0,
                'method'  => 'save',
                'message' => 'Save mail timing fail',
                'mailTemplateOption' => $mailTemplate
            ]);
        }
        return view('send-mail-timing.add');
    }
}
