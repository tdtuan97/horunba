<?php
/**
 * Controller for stock confirm
 *
 * @package    App\Http\Controllers
 * @subpackage StockConfirmController
 * @copyright  Copyright (c) 2018 RiverCrane! Corporation. All Rights Reserved.
 * @author     Le Hung<le.hung.rcvn2012@gmail.com>
 */

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Backend\DtEstimation;
use App\Models\Backend\MstProductStatus;
use Validator;
use Config;
use App;

class StockConfirmController extends Controller
{
    /**
     * Load view.
     */
    public function index()
    {
        return view('stock-confirm.index');
    }

    /**
     * Get all data.
     * Return $object json
     */
    public static function dataList(Request $request)
    {
        if ($request->ajax() === true) {
            $dtEstimation = new DtEstimation();
            $mstProductStatus  = new MstProductStatus();
            $mOrderType        = Config::get('common.m_order_type_id');
            foreach ($mOrderType as $key => $value) {
                $status[] = ['key' => $key, 'value' => $value];
            }
            $type = $request->input('type', null);
            $arrSort = [
                'dt_estimation.estimate_date'  => $request->input('sort_estimate_date', null),
                'dt_estimation.estimate_code'  => $request->input('sort_estimate_code', null),
                'p.product_code'               => $request->input('sort_product_code', null),
                'z.jan_code'        => $request->input('sort_jan_code', null),
                'p.product_name'    => $request->input('sort_product_name', null),
                'z.quantity'        => $request->input('sort_quantity', null),
                'z.m_order_type_id' => $request->input('sort_m_order_type_id', null),
                's.supplier_nm'     => $request->input('sort_supplier_name', null),
                'z.updated_at'      => $request->input('sort_edi_answer_date', null),
            ];
            $rule = [
                'estimate_code_from' => 'nullable',
                'estimate_code_to'   => 'nullable',
                'estimate_date_from' => 'nullable|date',
                'estimate_date_to'   => 'nullable|date'
            ];
            $arrSearch = [
                'estimate_code_from'    => $request->input('estimate_code_from', null),
                'product_code'          => $request->input('product_code', null),
                'estimate_code_to'      => $request->input('estimate_code_to', null),
                'm_order_type_id'       => $request->input('m_order_type_id', null),
                'supplier_name'         => $request->input('supplier_name', null),
                'estimate_date_from'    => $request->input('estimate_date_from', null),
                'estimate_date_to'      => $request->input('estimate_date_to', null),
                'not_answer'            => $request->input('not_answer', null),
                'per_page'              => $request->input('per_page', null)
            ];
            $validator = Validator::make($arrSearch, $rule);
            if ($validator->fails()) {
                $data = [];
            } else {
                if (!is_null($type)) {
                    $data = $dtEstimation->getData($arrSearch, array_filter($arrSort), $type);

                    return $data;
                } else {
                    $data = $dtEstimation->getData($arrSearch, array_filter($arrSort));
                }
            }
            $ediZaikoDetailUrl = '';
            $env = App::environment();
            if ($env === 'local') {
                $ediZaikoDetailUrl = "http://edi.local/edi/zaiko";
            } elseif ($env === 'test') {
                $ediZaikoDetailUrl = "https://edi-test.diyfactory.jp/edi/zaiko";
            } elseif ($env === 'real') {
                $ediZaikoDetailUrl = "https://edi.diyfactory.jp/edi/zaiko";
            }
            return response()->json([
                'data'              => $data,
                'statusOptions'     => $status,
                'ediZaikoUrl'       => $ediZaikoDetailUrl
            ]);
        }
    }
}
