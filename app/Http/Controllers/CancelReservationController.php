<?php
/**
 * Cance Reservation Controller
 *
 * @package     App\Controllers
 * @subpackage  CancelReservationController
 * @copyright   Copyright (c) 2018 RiverCrane! Corporation. All Rights Reserved.
 * @author      le.hung<le.hung.rcvn2012@gmail.com>
 */

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use Auth;
use App\Models\Backend\MstCancelReservation;
use App\Models\Backend\MstMall;

class CancelReservationController extends Controller
{
    /**
     * Show template for append data
     *
     * @param   $request  Request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        return view('cancel-reservation.index');
    }

    /**
     * Show data list view
     *
     * @param  array $request
     * @return json
     */
    public function dataList(Request $request)
    {
        if ($request->ajax() === true) {
            $arraySearch    = array(
                'mall_id'           => $request->input('mall_id', null),
                'status'            => $request->input('status', null),
                'per_page'          => $request->input('per_page', null)
            );
            $arraySort = [
                'name_jp'              => $request->input('sort_name_jp', null),
                'status'               => $request->input('sort_status', null),
                'note'                 => $request->input('sort_note', null),
                'receive_order_id'     => $request->input('sort_receive_order_id', null),
                'is_deleted'           => $request->input('sort_is_deleted', null),
                'in_date'              => $request->input('sort_in_date', null),
                'up_date'              => $request->input('sort_up_date', null),
            ];
            $model   = new MstCancelReservation();
            $mall    = new MstMall();
            $newDataMal = array();
            foreach ($mall->getDataSelectBox() as $key => $value) {
                $newDataMal[] = ['key' => $value['id'] , 'value' => $value['name_jp']];
            }
            $data = $model->getData($arraySearch, array_filter($arraySort));
            return response()->json([
                'data'          => $data,
                'dataMall'      => $newDataMal,
            ]);
        }
    }
    /**
     * Update flag
     *
     * @param   $request  Request
     * @return json
     */
    public function changeFlag(Request $request)
    {
        $model   = new MstCancelReservation();
        if ($request->ajax() === true) {
            $params                 = $request->all();
            $params['data']['up_ope_cd']    = Auth::user()->tantou_code;
            $dataWhere              = ['mall_id' => $params['mall_id'] ,
                                        'receive_order_id' =>  $params['receive_order_id']];
            $dataUpdate             = ['is_deleted' =>  $params['is_deleted']];
            $model->updateData($dataWhere, $dataUpdate);
            return ;
        }
    }
    /**
     * Process add new to database
     *
     * @param   $request  Request
     * @return json
     */
    public function save(Request $request)
    {
        if ($request->ajax() === true) {
            $params  =  $request->only(
                [
                    'note',
                    'status',
                    'mall_id',
                    'up_date',
                    'in_date',
                    'up_ope_cd',
                    'in_ope_cd',
                    'is_deleted',
                    'receive_order_id',
                ]
            );
            $model    = new MstCancelReservation();

            $returnNo = $request->input('return_no', null);
            $action   = $request->input('action');
            $rules    = [
                'mall_id'               => 'required',
                'receive_order_id'      => 'required',
                'note'                  => 'required'
            ];
            $validator = Validator::make($request->all(), $rules);
            if ($validator->fails()) {
                return response()->json([
                    'status'  => 0,
                    'message' => $validator->errors(),
                    'data'    => []
                ]);
            }
            if ($action === 'add') {
                $dataCheck = $model->getItem(
                    [
                    'mall_id'          => $params['mall_id'],
                    'receive_order_id' => $params['receive_order_id']
                    ]
                );
                if (!empty($dataCheck)) {
                    return response()->json([
                        'status'  => 0,
                        'message' => [
                            'mall_id'          => __('messages.cancel_reservation_exists_message'),
                            'receive_order_id' => __('messages.cancel_reservation_exists_message')
                        ],
                        'data'    => []
                    ]);
                }
                $params['in_ope_cd'] = Auth::user()->tantou_code;
                $params['up_ope_cd'] = Auth::user()->tantou_code;
                $params['in_date']   = date('Y-m-d H:i:s');
                $params['up_date']   = date('Y-m-d H:i:s');
                $model->insert($params);
            } else {
                $params['up_ope_cd']           = Auth::user()->tantou_code;
                $params['up_date']             = date('Y-m-d H:i:s');
                $model->where(
                    [
                    'mall_id' => $params['mall_id'],
                    'receive_order_id' => $params['receive_order_id']
                    ]
                )->update($params);
            }
            return response()->json([
                'status'            => 1,
                'method'            => 'save'
            ]);
        }
        return view('cancel-reservation.save');
    }

    /**
     * Get info address
     *
     * @param   $request  Request
     * @return json
     */
    public function getFormData(Request $request)
    {
        if ($request->ajax() === true) {
            $dataWhere['mall_id'] = $request->input('mall_id', null);
            $dataWhere['receive_order_id'] = $request->input('receive_order_id', null);
            $model      = new MstCancelReservation();
            $mall       = new MstMall();
            $data       = $model->getItem($dataWhere);
            $dataMall   = $mall->getDataSelectBox();
            $newDataMal = null;
            $newDataMal[] = ['key' => '_none', 'value' => '-----'];
            foreach ($dataMall as $key => $value) {
                $newDataMal[] = ['key' => $value['id'] , 'value' => $value['name_jp']];
            }
            return response()->json([
                'data'          => $data,
                'dataMall'      => $newDataMal
            ]);
        }
    }
}
