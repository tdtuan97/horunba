<?php
/**
 * Middleware for set params
 *
 * @package    App\Http\Middleware
 * @subpackage CheckRole
 * @copyright  Copyright (c) 2018 RiverCrane! Corporation. All Rights Reserved.
 * @author     Le Hung<le.hung.rcvn2012@gmail.com>
 */

namespace App\Http\Middleware;

use Closure;

class SetParams
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $perPage = $request->input('per_page', 1);
        if ($perPage > 100) {
            $request->merge(array("per_page" => 100));
        } elseif (!is_numeric($perPage)) {
            $request->merge(array("per_page" => 20));
        }
        return $next($request);
    }
}
