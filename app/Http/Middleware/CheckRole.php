<?php
/**
 * Middleware for access control list
 *
 * @package    App\Http\Middleware
 * @subpackage CheckRole
 * @copyright  Copyright (c) 2018 RiverCrane! Corporation. All Rights Reserved.
 * @author     Truong Nghia<truong.van.nghia@rivercrane.vn>
 */

namespace App\Http\Middleware;

use Closure;
use App\Models\Backend\MstController;
use App\Models\Backend\MstTantou;

class CheckRole
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $routeName = \Request::route()->getName();
        $routeUri  = \Request::route()->uri;
        if ($routeName !== 'change-password' && $routeName !== 'logout'
            && $routeUri !== 'reset-password' && !$request->ajax()) {
            $lassRenewPass = \Auth::user()->last_renew_pass;
            if (empty($lassRenewPass)) {
                return redirect()->route('change-password')->with('exp_mess', __('messages.pass_expire_date'));
            } else {
                if (strtotime("+3 months", strtotime($lassRenewPass)) <= strtotime(date('Y-m-d H:i:s'))) {
                    return redirect()->route('change-password')->with('exp_mess', __('messages.pass_expire_date'));
                }
            }
        }
        
        $className = class_basename(\Request::route()->getActionName());
        list($controller, $action) = explode('@', $className);
        $modelTantou = new MstTantou();
        $dataCheck = $modelTantou->getDataCheckPermissionByKey(
            [
                'mst_controller.screen_class' => $controller,
                'mst_tantou.tantou_code' => \Auth::user()->tantou_code
            ]
        );

        if (!empty($dataCheck)) {
            if (empty($dataCheck->action_list) && $dataCheck->no_list === 0) {
                return $next($request);
            } else {
                switch ($dataCheck->permission) {
                    case 0:
                        $link = $modelTantou->getDataRedirect(\Auth::user()->tantou_code);
                        if (!$link) {
                            return redirect()->route('not-permission.page');
                        } else {
                            return redirect($link)->with(['not_permission'=> true, 'message' => 'access ' . $dataCheck->screen_name]);
                        }
                        break;

                    case 1:
                        if ($dataCheck->permission === 1 && $dataCheck->no_list === 0 &&
                            (strpos($dataCheck->action_list, $action) !== false ||
                                strpos($dataCheck->action_access, $action) !== false
                        )) {
                            return $next($request);
                        } else {
                            if ($request->ajax() === true) {
                                return response()->json([
                                    'status'         => 1,
                                    'not_permission' => 1,
                                ]);
                            } else {
                                if (strpos(url()->previous(), \Request::url()) === false) {
                                    return redirect(url()->previous())->with(['not_permission'=> true, 'message' => 'edit ' . $controller]);
                                } else {
                                    $link = $modelTantou->getDataRedirect(\Auth::user()->tantou_code);
                                    if (!$link) {
                                        return redirect()->route('not-permission.page');
                                    } else {
                                        return redirect($link)->with(['not_permission'=> true, 'message' => $controller]);
                                    }
                                }
                            }
                        }
                        break;

                    case 2:
                        if (empty($dataCheck->action_denied) ||
                            (!empty($dataCheck->action_denied) && strpos($dataCheck->action_denied, $action) === false))
                        {
                            return $next($request);
                        } else {
                            if ($request->ajax() === true) {
                                return response()->json([
                                    'status'         => 1,
                                    'not_permission' => 1,
                                ]);
                            } else {
                                if (strpos(url()->previous(), \Request::url()) === false) {
                                    return redirect(url()->previous())->with(['not_permission'=> true, 'message' => $controller . '@' . $action]);
                                } else {
                                    $link = $modelTantou->getDataRedirect(\Auth::user()->tantou_code);
                                    if (!$link) {
                                        return redirect()->route('not-permission.page');
                                    } else {
                                        return redirect($link)->with(['not_permission'=> true, 'message' => $$controller . '@' . $action]);
                                    }
                                }
                            }
                        }
                        break;

                    default:
                        break;
                }
            }
        } else {
            $modelC = new MstController();
            if (!empty($modelC->getDataCheckAccess($controller))) {
                return $next($request);
            } else {
                if (strpos(url()->previous(), \Request::url()) === false) {
                    return redirect(url()->previous())->with(['not_permission'=> true, 'message' => $controller]);
                } else {
                    $link = $modelTantou->getDataRedirect(\Auth::user()->tantou_code);
                    if (!$link) {
                        return redirect()->route('not-permission.page');
                    } else {
                        return redirect($link)->with(['not_permission'=> true, 'message' => 'access ' . $controller]);
                    }
                }
            }
        }
    }
}
