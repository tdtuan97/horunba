<?php
/**
 * Enable logging of database queries.
 */
namespace App\Http\Middleware;

use Closure;
use DB;
use Log;
use Illuminate\Routing\Router;
use App\Models\Backend\ScreenAllLog;

class LogDatabaseQueries
{
    protected $router;
    /**
     * Set router request
     *
     * @param  \Illuminate\Routing\Router $router
     * @return mixed
     */
    public function __construct(Router $router)
    {
        $this->router = $router;
    }

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        if (!env('DB_LOG', false)) {
            return $next($request);
        }
        DB::enableQueryLog();
        $response = $next($request);
        $actionName = $this->router->getRoutes()->match($request)->getActionName();

        foreach (DB::getQueryLog() as $log) {
            try {
                $query    = $log['query'];
                $tmpQuery = substr($query, 0, 100);
                $bindList = $log['bindings'];
                $time = $log['time'];
                $string = "/^update ((?!tmp_check_edit)(?!log_update_status_order)(?!log_update_status_product)(?!dt_order_update_log).)*$|^insert ((?!tmp_check_edit)(?!log_update_status_order)(?!log_update_status_product)(?!dt_order_update_log).)*$/";
                if (preg_match($string, $tmpQuery)) {
                    foreach ($bindList as $binding) {
                        $value = is_numeric($binding) ? $binding : "'".$binding."'";
                        $query = preg_replace('/\?/', $value, $query, 1);
                    }
                    ScreenAllLog::insert(array('class_action' => $actionName, 'query_string' => $query));
                }
            } catch (\Exception $ex) {
                report($ex);
            }
        }
        return $response;
    }
}
