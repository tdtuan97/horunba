<?php
/**
 * Make new view composer for horunba
 *
 * @package App\Http\ViewComposers
 * @subpackage GlobalComposer
 * @copyright Copyright (c) 2018 CriverCrane! Corporation. All Rights Reserved.
 * @author Le Hung <le.hung.rcvn2012@gmail.com>
 */
namespace App\Http\ViewComposers;

use Illuminate\View\View;
use App\Models\Backend\DtOrderUpdateLog;
use App\Models\Backend\MstRakutenKey;
use Auth;
use Config;

class GlobalComposer
{

    public $global = [];
    /**
     * Bind data to the view.
     *
     * @param  View  $view
     * @return void
     */
    public function compose(View $view)
    {

        $model           = new DtOrderUpdateLog();
        $modelRakutenKey = new MstRakutenKey();
        $dataNotify           = $model->getDataNotify(Auth::user()->tantou_code);
        $expirDate = $modelRakutenKey->getExpirDate();
        $global = [
            'setMenu'         => !empty(Config::get('app.skin')) ? Config::get('app.skin') : 'red',
            'dataNotify'      => $dataNotify,
            'countDataNotify' => count($dataNotify),
            'expirDate'       => $expirDate
        ];
        $view->with('globalVar', $global);
    }
}
