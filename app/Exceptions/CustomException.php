<?php
/**
 * Custom report exception
 *
 * @package    App\Exceptions
 * @subpackage ProcessDivideProduct
 * @copyright  Copyright (c) 2018 RiverCrane! Corporation. All Rights Reserved.
 * @author     Lam Vinh<lam.vinh.rcvn2012@rivercrane.vn>
 */
namespace App\Exceptions;

use Exception;
use App\Events\Command as eCommand;
use Event;
use App\Notifications\SlackNotification;
use App\Notification;

class CustomException extends Exception
{
    /**
     * Report or log an exception and then notify to slack
     *
     *
     * @return void
     */
    public function report()
    {
        $traceString = $this->getTraceAsString();

        $arrayTrace = explode("\n", $traceString);
        $index = 0;
        $flag  = 0;
        foreach ($arrayTrace as $key => $value) {
            if (strpos($value, 'internal function]') !== false) {
                //Get index current
                $index = $key - 1;
                break;
            }
        }
        $trace     = $this->getTrace();

        $file      = $trace[$index]['file'];
        $tempName  = explode("\\", $file);
        $className = str_replace('.php', '', $tempName[count($tempName) - 1]);
        $fileError = $arrayTrace[$index];

        $error  = "--------------------------" . PHP_EOL;
        $error .= $className . PHP_EOL;
        $error .= $fileError . PHP_EOL;
        //debug preve
        if ($index > 0) {
             $error .= $arrayTrace[$index - 1] . PHP_EOL;
        }
        $error .= $this->getMessage() . PHP_EOL;
        $error .= "--------------------------" . PHP_EOL;
        //Notify to slack
        $slack  = new Notification(CHANNEL['horunba']);
        $slack->notify(new SlackNotification($error));
        //Update to table status batch
        if (strpos($fileError, 'Console\Commands') !== false) {
            Event::fire(new eCommand($className, array('stop' => true, 'error' => $error)));
        }
    }
}
