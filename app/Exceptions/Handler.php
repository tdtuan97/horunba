<?php

namespace App\Exceptions;

use Exception;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use App\Events\Command as eCommand;
use Event;
use App\Notifications\SlackNotification;
use App\Notification;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
        \Illuminate\Auth\AuthenticationException::class,
        \Illuminate\Auth\Access\AuthorizationException::class,
        \Illuminate\Validation\ValidationException::class,
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'password',
        'password_confirmation',
    ];

    /**
     * Report or log an exception.
     *
     * This is a great spot to send exceptions to Sentry, Bugsnag, etc.
     *
     * @param  \Exception  $exception
     * @return void
     */
    public function report(Exception $exception)
    {
        if ($this->shouldReport($exception)) {
            $traceString = $exception->getTraceAsString();
            $arrayTrace = explode("\n", $traceString);
            $index = 0;
            $flag  = 0;
            foreach ($arrayTrace as $key => $value) {
                if (strpos($value, 'internal function]') !== false) {
                    //Get index current
                    $index = $key - 1;
                    break;
                }
            }
            if ($index === -1) {
                $index = 1;
            };
            $trace     = $exception->getTrace();
            $file      = $trace[$index]['file'];
            $tempName  = explode(DIRECTORY_SEPARATOR, $file);
            $className = str_replace('.php', '', $tempName[count($tempName) - 1]);
            $fileError = $arrayTrace[$index];

            $error  = "--------------------------" . PHP_EOL;
            $error .= $className . PHP_EOL;
            $error .= $fileError . PHP_EOL;
            //debug preve
            if ($index > 0) {
                $error .= $arrayTrace[$index - 1] . PHP_EOL;
            } else {
                $error .= $arrayTrace[$index] . PHP_EOL;
            }
            $error .= $exception->getMessage() . PHP_EOL;
            $error .= "--------------------------" . PHP_EOL;
            //Notify to slack
            $slack  = new Notification(CHANNEL['horunba']);
            $slack->notify(new SlackNotification($error));
            //Update to table status batch
            if (strpos($fileError, 'Commands') !== false) {
                Event::fire(new eCommand($className, array('stop' => true, 'error' => $error)));
            }
            parent::report($exception);
        }
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Exception  $exception
     * @return \Illuminate\Http\Response
     */
    public function render($request, Exception $exception)
    {
        return parent::render($request, $exception);
    }
}
