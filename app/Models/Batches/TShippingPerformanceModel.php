<?php

/**
 * Model for tShippingPerformanceModel table.
 *
 * @package    App\Models\Batches
 * @subpackage BatchModel
 * @copyright  Copyright (c) 2017 RiverCrane! Corporation. All Rights Reserved.
 * @author     Truong Nghia<truong.van.nghia@rivercrane.vn>
 */

namespace App\Models\Batches;

use Illuminate\Database\Eloquent\Model;

class TShippingPerformanceModel extends Model
{
    public $timestamps = false;

    /**
    * The database table used by the model.
    * @var string
    */
    protected $table = 'tShippingPerformanceModel';

    /**
    * The database is used by the model.
    * @var string
    */
    protected $connection = 'glsc';

    /**
     * Get data insert delivery result
     *
     * @return object
     */
    public function getDataInsertDeliveryResult()
    {
        $col = [
            'tShippingPerformanceModel_autono AS id',
            'DataKb',
            'OrderNo',
            'OrderBNo',
            'OrderLNo',
            'DeliDate',
            'InquiryNo',
            'ItemCd',
            'JanCd',
            'Quantity',
            'Note',
            'ShipNum',
            'mp.price_supplier_id',
            'mp.price_invoice',
        ];
        $result = $this->select($col)
                       ->join(
                           'hrnb.mst_product AS mp',
                           'tShippingPerformanceModel.ItemCd',
                           '=',
                           'mp.product_code'
                       )
                       ->where('DataKb', '=', 5)
                       ->where('NodisplayFlg', '=', 0)
                       ->get();
        return $result;
    }

    /**
    * The update infomation.
    * @param array $key    condition
    * @param array $value  data update
    * @return object $result
    */
    public function updateData($key, $value)
    {
        return $this->where($key)->update($value);
    }
}
