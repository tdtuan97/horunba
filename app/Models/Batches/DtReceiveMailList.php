<?php
/**
 * Model for dt_receive_mail_list table.
 *
 * @package    App\Models\Batches
 * @subpackage DtReceiveMailList
 * @copyright  Copyright (c) 2018 RiverCrane! Corporation. All Rights Reserved.
 * @author     van.qui<van.qui@rivercrane.vn>
 */

namespace App\Models\Batches;

use Illuminate\Database\Eloquent\Model;

class DtReceiveMailList extends Model
{
    public $timestamps = false;

    /**
    * The database table used by the model.
    * @var string
    */
    protected $table = 'dt_receive_mail_list';

    /**
    * The database is used by the model.
    * @var string
    */
    protected $connection = 'horunba';

    /**
     * get new email sakura
     *
     * @return object
     */
    public function getDataReceiveMailSakura()
    {
        $col = ['mst_order.received_order_id', 'rec_mail_index'];
        $result = $this->select($col)
                       ->join('mst_customer', 'dt_receive_mail_list.mail_from', '=', 'mst_customer.email')
                       ->join('mst_order', 'mst_customer.customer_id', '=', 'mst_order.customer_id')
                       ->whereRaw('mst_order.in_date > (NOW() - INTERVAL 14 DAY)')
                       ->whereNull('receive_order_id')
                       ->get();
        return $result;
    }

    /**
     * Update data by key
     *
     * @param  int     $recMailIndex
     * @param  array   $arrUpdate
     * @return boolean
     */
    public function updateDataByKey($recMailIndex, $arrUpdate)
    {
        return $this->where('rec_mail_index', $recMailIndex)->update($arrUpdate);
    }

    /**
     * Get data filter
     *
     * @return object
     */
    public function getDataFilter()
    {
        $data = $this->select(
            'rec_mail_index',
            'mail_from',
            'mail_to',
            'mail_subject',
            'mail_content',
            'receive_order_id'
        )
        ->where('is_filtered', 0)
        ->get();
        return $data;
    }

    /**
     * Get data filter
     *
     * @return object
     */
    public function getDataProcessMailFilter($limit)
    {
        $result = $this->select('rec_mail_index', 'mail_subject', 'mail_from', 'inquiry_code')
                       ->where('filter_checked', 0)
                       ->limit($limit)
                       ->get();
        return $result;
    }

    /**
     * Update data by key
     *
     * @param  int     $arrKey
     * @param  array   $arrUpdate
     * @return boolean
     */
    public function updateData($arrKey, $arrUpdate)
    {
        return $this->whereIn('rec_mail_index', $arrKey)->update($arrUpdate);
    }
    
    /**
     * Get empty content mail
     *
     * @return object
     */
    public function getEmptyContentMail()
    {
        $data = $this->select('uid_mail')
            ->where('uid_mail', '<>', 0)->where('mail_content', '')
            ->groupBy('uid_mail')->get();
        return $data;
    }
    
    /**
     * Update data by uid
     *
     * @param  int     $uid
     * @param  array   $data
     * @return boolean
     */
    public function updateDataByUID($uid, $data)
    {
        return $this->where('uid_mail', $uid)->update($data);
    }
    
    /**
     * Get exist uid
     *
     * @param  array $udis
     * @return object
     */
    public function getExistUid($udis)
    {
        return $this->select('uid_mail')->whereIn('uid_mail', $udis)->get();
    }
}
