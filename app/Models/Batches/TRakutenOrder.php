<?php

/**
 * Model for t_rakuten_order table.
 *
 * @package    App\Models\Batches
 * @subpackage TRakutenOrder
 * @copyright  Copyright (c) 2017 RiverCrane! Corporation. All Rights Reserved.
 * @author     Truong Nghia<truong.van.nghia@rivercrane.vn>
 */

namespace App\Models\Batches;

use Illuminate\Database\Eloquent\Model;
use DB;

class TRakutenOrder extends Model
{
    public $timestamps = false;

    /**
    * The database table used by the model.
    * @var string
    */
    protected $table = 't_rakuten_order';

    /**
    * The database is used by the model.
    * @var string
    */
    protected $connection = 'horunba';

    /**
     * Get data check same data
     * @param int $limit limit get datas
     * @return object $result
     */
    public function getDataCheck($limit)
    {
        $objId = $this->select('order_number')
                      ->where('process_flg', 0)
                      ->groupBy('order_number')
                      ->limit($limit)
                      ->get();
        $arrId = array_column($objId->toArray(), 'order_number');
        $result = $this->select(['rakuten_order_key', 'order_number', 'item_number', 'is_gift'])
                       ->whereIn('order_number', $arrId)
                       ->groupBy(['order_number', 'item_number'])
                       ->get();
        return $result;
    }

    /**
     * Get data of tabe t_rakuten_order by date
     * @param int $limit limit get datas
     * @return object $result
     */
    public function getData($arrId)
    {
        $result = $this->whereIn('order_number', $arrId)
                       ->where('is_delete', 0)
                       ->where('process_flg', '=', '0')
                       ->whereNull('delete_flg')
                       ->orderBy('order_number')
                       ->orderBy('rakuten_order_key')
                       ->get();
        return $result;
    }

   /**
     * Get order detail before insert order.
     * @param  array  $receiveId value receive_id for condition where
     * @return object $result
     */
    public function getInfoOrderDetail($receiveId)
    {
        $result = $this->join('mst_order', function ($join) use ($receiveId) {
                            $join->on('order_number', '=', 'mst_order.received_order_id')
                                 ->where('receive_id', '=', $receiveId)
                                 ->where('is_delete', 0);
        })
                       ->get();
        return $result;
    }

   /**
     * Update process flag before get information order.
     *
     * @param  array  $arrKey list rakuten_order_key need update
     * @return object $result
     */
    public function updateProcessFlg($arrKey)
    {
        $result = $this->whereIn('rakuten_order_key', $arrKey)
             ->update(['process_flg' => 1]);
        return $result;
    }

    /**
     * Check order exist
     * @param string $orderNumber order number
     * @return int $resule
     */
    public function checkOrder($orderNumber)
    {
        $result = $this->where('order_number', "=", $orderNumber)
                       ->count();
        return $result;
    }
}
