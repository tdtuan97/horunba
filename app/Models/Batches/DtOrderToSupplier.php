<?php

/**
 * Model for dt_order_to_supplier table.
 *
 * @package    App\Models\Batches
 * @subpackage DtOrderToSupplier
 * @copyright  Copyright (c) 2017 RiverCrane! Corporation. All Rights Reserved.
 * @author     Truong Nghia<truong.van.nghia@rivercrane.vn>
 */

namespace App\Models\Batches;

use Illuminate\Database\Eloquent\Model;

class DtOrderToSupplier extends Model
{
    public $timestamps = false;

    /**
    * The database table used by the model.
    * @var string
    */
    protected $table = 'dt_order_to_supplier';

    /**
    * The database is used by the model.
    * @var string
    */
    protected $connection = 'horunba';

    /**
     * Get last order code tabe dt_order_to_supplier
     * @return object $result
     */
    public function getMaxOrderCode($prefix)
    {
        $result = $this->select('order_code')
                       ->where('order_code', 'like', $prefix . '%')
                       ->orderBy('order_code', 'DESC')
                       ->first();
        return $result;
    }

    /**
     * Get last order code tabe dt_order_to_supplier
     * @return object $result
     */
    public function getMaxOrderCodeByDate($date)
    {
        $result = $this->select('order_code')
                       ->where('order_code', 'like', $date . '%')
                       ->orderBy('order_code', 'DESC')
                       ->first();
        return $result;
    }

    /**
     * Update table follow order_code
     * @param string $orderCode  value order_code will updated
     * @param array  $arrData    data will update
     * @return boolean $check or string $error
     */
    public function updateData($orderCode, $arrData, $ediOrderCode = null)
    {
        $update = $this->where('order_code', $orderCode);
        if (!empty($ediOrderCode)) {
            $update->where('edi_order_code', $ediOrderCode);
        }
        return $update->update($arrData);
    }

    /**
    * The get data process order edi result step 1.
    * @return object $result
    */
    public function getDataEdiOrderResult1()
    {
        $col = [
            'dt_order_to_supplier.order_code',
            'dt_order_to_supplier.order_date',
        ];
        $result = $this->select($col)
                       ->where('process_status', '<', 3)
                       ->where('wait_days', '<', 7)
                       ->get();
        return $result;
    }

    /**
    * The get data process Inventory reservation step 2
    * @param string $productCode.
    * @return object $result
    */
    public function getDataInventory2($productCode)
    {
        $col = [
            'order_code',
            'edi_order_code',
            'order_remain_num',
            'order_num',
            'replenish_num',
			'estimate_delivery_date',
        ];
        $result = $this->select($col)
                       ->where('product_code', $productCode)
                       ->whereRaw('order_num - order_remain_num > 0')
                       ->whereIn('order_type', [3,4])
                       ->whereIn('process_status', [1,2])
                       ->where('is_cancel', '=', 0)
                       ->orderBy('order_code')
                       ->get();
        return $result;
    }

    /**
    * Get data process supplier EDI
    * @return object $result
    */
    public function getDataProcessEDI2()
    {
        $col = [
            'dt_order_to_supplier.order_code',
            'dt_order_to_supplier.product_code',
            'dt_order_to_supplier.order_num',
            'dt_order_to_supplier.order_type',
            'dt_order_to_supplier.supplier_id',
            'dt_order_to_supplier.edi_order_code',
            'dt_order_to_supplier.is_close_dok',
            'dt_order_to_supplier.sku_num',
            'mst_product.supplier_code',
            'mst_product.product_jan',
            'mst_product.maker_full_nm',
            'mst_product.product_maker_code',
            'mst_product.product_name',
            'mst_product.product_color',
            'mst_product.product_size',
            'mst_product.rak_img_url_1',
            'mst_product.price_invoice',
            'mst_product.product_name_long'
            //'emp.delivery_flg',
        ];
        $result = $this->select($col)
                       ->join('mst_product', 'dt_order_to_supplier.product_code', '=', 'mst_product.product_code')
                       ->join('edi.mst_product as emp', function ($join) {
                            $join->on('dt_order_to_supplier.product_code', '=', 'emp.product_code')
                            ->where('emp.is_live', '=', 1);
                       })
                       ->where('dt_order_to_supplier.process_status', 0)
                       ->where('dt_order_to_supplier.order_type', '<>', 2)
                       ->get();
        return $result;
    }
    /**
    * Get data process supplier EDI direct
    * @return object $result
    */
    public function getDataProcessEDIDirect2()
    {
        $col = [
            'dt_order_to_supplier.order_code',
            'dt_order_to_supplier.product_code',
            'dt_order_to_supplier.order_num',
            'dt_order_to_supplier.order_type',
            'dt_order_to_supplier.supplier_id',
            'dt_order_to_supplier.edi_order_code',
            'dt_order_to_supplier.sku_num',
            'mst_product.supplier_code',
            'mst_product.product_jan',
            'mst_product.maker_full_nm',
            'mst_product.product_maker_code',
            'mst_product.product_name',
            'mst_product.product_color',
            'mst_product.product_size',
            'mst_product.rak_img_url_1',
            'mst_product.price_invoice',
            'mst_product.product_name_long',
            'o.company_name as company_name',
            'mco.first_name as order_firstname',
            'mco.last_name as order_lastname',
            'mco.tel_num as order_tel',
            'mco.zip_code as order_zip_code',
            'mco.prefecture as order_prefecture',
            'mco.city as order_city',
            'mco.sub_address as order_sub_address',
            'mcod.first_name as ship_to_first_name',
            'mcod.last_name as ship_to_last_name',
            'mcod.tel_num as ship_tel_num',
            'mcod.zip_code as ship_zip_code',
            'mcod.prefecture as ship_prefecture',
            'mcod.city as ship_city',
            'mcod.sub_address as ship_sub_address',
        ];
        $result = $this->select($col)
                       ->join('mst_product', 'dt_order_to_supplier.product_code', '=', 'mst_product.product_code')
                       ->join('edi.mst_product as emp', function ($join) {
                            $join->on('dt_order_to_supplier.product_code', '=', 'emp.product_code')
                                 ->where('emp.is_live', '=', 1);
                       })
                       ->join('mst_order as o', 'dt_order_to_supplier.receive_id', '=', 'o.receive_id')
                       ->join('mst_order_detail as od', function ($join) {
                            $join->on('dt_order_to_supplier.receive_id', '=', 'od.receive_id')
                                 ->on('dt_order_to_supplier.detail_line_num', '=', 'od.detail_line_num');
                       })
                       ->join('mst_customer as mco', 'o.customer_id', '=', 'mco.customer_id')
                       ->join('mst_customer as mcod', 'od.receiver_id', '=', 'mcod.customer_id')
                       ->where('dt_order_to_supplier.process_status', 0)
                       ->where('dt_order_to_supplier.order_type', 2)
                       ->where('o.order_sub_status', '<>', ORDER_SUB_STATUS['ERROR'])
                       ->where('o.is_delay', 0)
                       ->get();
        return $result;
    }

    /**
    * Get data process receive result step 2
    * @return object $result
    */
    public function getDataReceiveResult2()
    {
        $col = [
            'tr.tRequestPerformanceModel_autono',
            'tr.SupplyNo',
            'tr.PlanQuantity',
            'tr.Quantity',
            'tr.price',
            'dt_order_to_supplier.order_code',
            'dt_order_to_supplier.edi_order_code',
            'dt_order_to_supplier.product_code',
            'dt_order_to_supplier.price_invoice',
            'dt_order_to_supplier.order_num',
            'dt_order_to_supplier.arrival_quantity',
            'dt_order_to_supplier.supplier_id',
            'dt_order_to_supplier.order_type',
            'dt_order_to_supplier.supplier_id',
            'dt_order_to_supplier.process_status',
            'opd.receive_id',
            'opd.detail_line_num',
            'opd.sub_line_num',
            'opd.received_order_num',
            'opd.order_num AS opd_order_num',
            'opd.order_code AS opd_order_code',
            'opd.delivery_type',
            'opd.delivery_date',
            'opd.product_status',
            'mst_order.ship_wish_date',
            'mst_order.received_order_id',
            'dt_return.return_no',
            'dt_return.return_line_no',
            'dt_return.return_time',
        ];
        $sub = '(select deli_days from mst_postal where postal_code = mst_customer.zip_code limit 1) AS deli_days';
        $result = $this->select($col)
                       ->selectRaw($sub)
                       ->join(
                           'edi.tRequestPerformanceModel as tr',
                           'dt_order_to_supplier.edi_order_code',
                           '=',
                           'tr.SupplyNo'
                       )
                       ->leftjoin(
                           'dt_order_product_detail as opd',
                           'dt_order_to_supplier.order_code',
                           '=',
                           'opd.order_code'
                       )
                       ->leftjoin('mst_order_detail', function ($join) {
                                $join->on('opd.receive_id', '=', 'mst_order_detail.receive_id')
                                     ->on('opd.detail_line_num', '=', 'mst_order_detail.detail_line_num');
                       })
                       ->leftjoin('mst_order', 'mst_order.receive_id', '=', 'mst_order_detail.receive_id')
                       ->leftjoin('dt_return', 'dt_return.edi_order_code', '=', 'dt_order_to_supplier.edi_order_code')
                       ->leftjoin('mst_customer', 'mst_customer.customer_id', '=', 'mst_order_detail.receiver_id')
                       ->whereIn('tr.decsy_update_flg', [0,1])
                       ->where('dt_order_to_supplier.process_status', '=', 5)
                       ->where('tr.DataKb', '0')
                       ->where('tr.Quantity', '>', 0)
                       ->whereDate('tr.ArrivalDate', date('Ymd'))
                       ->orderby('dt_order_to_supplier.order_code')
                       ->orderby('tr.DataKb')
                       ->orderby('dt_order_to_supplier.receive_id')
                       ->get();
        return $result;
    }

        /**
    * Get data process receive result step 3
    * @return object $result
    */
    public function getDataReceiveResult3()
    {
        $col = [
            'tr.tRequestPerformanceModel_autono',
            'tr.SupplyNo',
            'tr.PlanQuantity',
            'tr.Quantity',
            'tr.price',
            'dt_order_to_supplier.order_code',
            'dt_order_to_supplier.edi_order_code',
            'dt_order_to_supplier.product_code',
            'dt_order_to_supplier.price_invoice',
            'dt_order_to_supplier.order_num',
            'dt_order_to_supplier.arrival_quantity',
            'dt_order_to_supplier.order_type',
            'dt_order_to_supplier.supplier_id',
            'dt_order_to_supplier.process_status',
            'opd.receive_id',
            'opd.detail_line_num',
            'opd.sub_line_num',
            'opd.received_order_num',
            'opd.product_status',
            'opd.delivery_type',
            'opd.order_code AS opd_order_code',
            'mo.order_status',
            'dt_return.return_no',
            'dt_return.return_line_no',
            'dt_return.return_time',
        ];
        $result = $this->select($col)
                       ->join(
                           'edi.tRequestPerformanceModel as tr',
                           'dt_order_to_supplier.edi_order_code',
                           '=',
                           'tr.SupplyNo'
                       )
                       ->leftjoin(
                           'dt_order_product_detail as opd',
                           'dt_order_to_supplier.order_code',
                           '=',
                           'opd.order_code'
                       )
                       ->leftjoin(
                           'mst_order as mo',
                           'mo.receive_id',
                           '=',
                           'opd.receive_id'
                       )
                       ->leftjoin('dt_return', 'dt_return.edi_order_code', '=', 'dt_order_to_supplier.edi_order_code')
                       ->whereIn('tr.decsy_update_flg', [0,1])
                       ->where('tr.DataKb', '2')
                       ->whereDate('tr.ArrivalDate', date('Ymd'))
                       ->orderby('dt_order_to_supplier.order_code')
                       ->orderby('tr.DataKb')
                       ->orderby('dt_order_to_supplier.receive_id')
                       ->where(function ($sub) {
                           $sub->whereNull('mo.is_delay')
                               ->orWhere('mo.is_delay', '<>', 1);
                       })
                       ->get();
        return $result;
    }
}
