<?php

/**
 * Model for tRequestPerformanceModel table.
 *
 * @package    App\Models\Batches
 * @subpackage BatchModel
 * @copyright  Copyright (c) 2017 RiverCrane! Corporation. All Rights Reserved.
 * @author     Truong Nghia<truong.van.nghia@rivercrane.vn>
 */

namespace App\Models\Batches;

use Illuminate\Database\Eloquent\Model;

class TRequestPerformanceModelEdi extends Model
{
    public $timestamps = false;

    /**
    * The database table used by the model.
    * @var string
    */
    protected $table = 'tRequestPerformanceModel';

    /**
    * The database is used by the model.
    * @var string
    */
    protected $connection = 'edi';

    /**
     * Get data insert return result
     *
     * @return object
     */
    public function getDateInsertReturnResult()
    {
        $col = [
            'tRequestPerformanceModel.tRequestPerformanceModel_autono',
            'tRequestPerformanceModel.RequestNo',
            'tRequestPerformanceModel.RequestBNo',
            'tRequestPerformanceModel.ArrivalDate',
            'tRequestPerformanceModel.PlanQuantity',
            'tRequestPerformanceModel.Quantity',
            'tRequestPerformanceModel.ItemCd',
            'tRequestPerformanceModel.Note',
            'tRequestPerformanceModel.SupplyNo',
            'tRequestPerformanceModel.price',
            'mp.price_supplier_id',
            'opd.delivery_type',
        ];
        $result = $this->select($col)
                       ->join('hrnb.mst_product AS mp', 'tRequestPerformanceModel.ItemCd', '=', 'mp.product_code')
                       ->leftjoin(
                           'hrnb.dt_order_product_detail AS opd',
                           'tRequestPerformanceModel.SupplyNo',
                           '=',
                           'opd.order_code'
                       )
                       ->where('tRequestPerformanceModel.DataKb', '=', 4)
                       ->where('tRequestPerformanceModel.decsy_update_flg', '=', 0)
                       ->get();
        return $result;
    }

    /**
    * Update information
    * @param string $key      condition
    * @param array $arrUpdate data update
    * @return object boolean
    */
    public function updateData($key, $arrUpdate)
    {
        return $this->where($key)
                    ->update($arrUpdate);
    }
}
