<?php

/**
 * Model for dt_store_order_info table.
 *
 * @package    App\Models\Batches
 * @subpackage DtOrderStoreOrderInfo
 * @copyright  Copyright (c) 2017 RiverCrane! Corporation. All Rights Reserved.
 * @author     Le Hung<le.hung.rcvn2012@gmail.com>
 */

namespace App\Models\Batches;

use Illuminate\Database\Eloquent\Model;

class DtOrderStoreOrderInfo extends Model
{
    public $timestamps = false;

    /**
    * The database table used by the model.
    * @var string
    */
    protected $table = 'dt_store_order_info';

    /**
    * The database is used by the model.
    * @var string
    */
    protected $connection = 'horunba';

    /**
     * Get data order detail info
     * @return object $result
     */
    public function getDataOrdersDetail()
    {
        $result = $this->select('order_id')
                       ->where('process_status', '=', 0)
                       ->orderBy('order_id', 'DESC')
                       ->limit('5000')
                       ->get();
        return $result;
    }
    /**
     * Update data
     *
     * @param  array $dataWhere for condition
     * @param  array $dataUpdate for update
     * @return object
     */
    public function updateData($dataWhere, $dataUpdate)
    {
        $data = $this->where($dataWhere)->update($dataUpdate);
        return $data;
    }

    /**
     * Update data
     *
     * @param  array $dataWhere for condition
     * @param  array $dataUpdate for update
     * @return object
     */
    public function getDataUpdatingApi()
    {
        $result = $this->select('dt_store_order_info.order_id')
            ->addSelect('dt_store_order_info.storage_store_id')
            ->addSelect('dt_store_order_detail.product_id')
            ->addSelect('dt_store_order_detail.quantity')
            ->addSelect('dt_store_order_detail.price')
            ->addSelect('dt_store_order_info.delivery_plan_date')
            ->addSelect('dt_store_order_info.status')
            ->where('dt_store_order_info.process_status', '=', 1)
            ->where('dt_store_order_info.status', '=', 3)
            ->whereNotNull('delivery_plan_date')
            ->leftJoin(
                'dt_store_order_detail',
                'dt_store_order_info.order_id',
                '=',
                'dt_store_order_detail.order_id'
            )
            ->orderBy('order_id', 'desc')
            ->orderBy('delivery_plan_date', 'asc')
            ->get();
        return $result;
    }
}
