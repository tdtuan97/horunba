<?php

/**
 * Model for t_no table.
 *
 * @package    App\Models\Batches
 * @subpackage BatchModel
 * @copyright  Copyright (c) 2018 RiverCrane! Corporation. All Rights Reserved.
 * @author     Truong Nghia<truong.van.nghia@rivercrane.vn>
 */

namespace App\Models\Batches;

use Illuminate\Database\Eloquent\Model;

class TNo extends Model
{
    public $timestamps = false;

    /**
    * The database table used by the model.
    * @var string
    */
    protected $table = 't_no';

    /**
    * The database is used by the model.
    * @var string
    */
    protected $connection = 'edi';

    /**
    * The primary key.
    * @var string
    */
    protected $primaryKey = 't_no_id';
}
