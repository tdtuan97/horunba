<?php

/**
 * Model for t_hoten_order_api table.
 *
 * @package    App\Models\Batches
 * @subpackage TRakutenOrder
 * @copyright  Copyright (c) 2018 RiverCrane! Corporation. All Rights Reserved.
 * @author     Truong Nghia<truong.van.nghia@rivercrane.vn>
 */

namespace App\Models\Batches;

use Illuminate\Database\Eloquent\Model;
use DB;

class THontenOrderApi extends Model
{
    public $timestamps = false;

    /**
    * The database table used by the model.
    * @var string
    */
    protected $table = 't_honten_order_api';

    /**
    * The database is used by the model.
    * @var string
    */
    protected $connection = 'horunba';

    /**
     * Get data check same data
     * @param int $limit limit get datas
     * @return object $result
     */
    public function getDataCheck($limit)
    {
        $objId = $this->select('order_id')
                      ->where('process_flg', 0)
                      ->groupBy('order_id')
                      ->limit($limit)
                      ->get();
        $arrId = array_column($objId->toArray(), 'order_id');
        $result = $this->select(['id', 'order_id', 'item_number'])
                       ->whereIn('order_id', $arrId)
                       ->groupBy(['order_id', 'item_number'])
                       ->get();
        return $result;
    }

    /**
     * Get data of tabe t_hoten_order_api
     * @param int $limit limit get datas
     * @return object $result
     */
    public function getData($arrId)
    {
        $result = $this->whereIn('order_id', $arrId)
                       ->where('is_delete', 0)
                       ->where('process_flg', '0')
                       ->orderBy('order_id')
                       ->orderBy('id')
                       ->get();
        return $result;
    }

   /**
     * Get order detail before insert order.
     * @param  array  $receiveId value receive_id for condition where
     * @return object $result
     */
    public function getInfoOrderDetail($receiveId)
    {
        $result = $this->join('mst_order', function ($join) use ($receiveId) {
                            $join->on('order_id', '=', 'mst_order.received_order_id')
                                 ->where('receive_id', '=', $receiveId)
                                 ->where('is_delete', 0);
        })
                       ->get();
        return $result;
    }

   /**
     * Update process flag before get information order.
     *
     * @param  array  $arrKey list rakuten_order_key need update
     * @return object $result
     */
    public function updateProcessFlg($arrKey)
    {
        $result = $this->whereIn('id', $arrKey)
             ->update(['process_flg' => 1]);
        return $result;
    }
}
