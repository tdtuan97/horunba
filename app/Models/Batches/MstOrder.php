<?php
/**
 * Model for mst_order table.
 *
 * @package    App\Models\Batches
 * @subpackage MstOrder
 * @copyright  Copyright (c) 2017 RiverCrane! Corporation. All Rights Reserved.
 * @author     Truong Nghia<truong.van.nghia@rivercrane.vn>
 */

namespace App\Models\Batches;

use Illuminate\Database\Eloquent\Model;
use App\Models\Batches\LogUpdateStatusOrder;
use DB;

class MstOrder extends Model
{
    public $timestamps = false;

    /**
    * The database table used by the model.
    * @var string
    */
    protected $table = 'mst_order';

    /**
    * The database is used by the model.
    * @var string
    */
    protected $connection = 'horunba';

    /**
    * The primary key.
    * @var string
    */
    protected $primaryKey = 'receive_id';

    /**
     * Get list data to do update product status
     * @return object $result
     */
    public function getDataUpdateStatus()
    {
        $col = [
            'mst_order.receive_id',
            'mst_order.is_ignore_error',
            'mco.first_name as order_firstname',
            'mco.last_name as order_lastname',
            'mco.tel_num as order_tel',
            'mco.zip_code as order_zip_code',
            'mco.prefecture as order_prefecture',
            'mco.city as order_city',
            'mco.sub_address as order_sub_address',
            'total_price',
            'order_date',
            'request_price',
            'payment_method',
            'emp.product_code',
            'price',
            'quantity',
            'mall_id',
            'mcod.first_name as ship_to_first_name',
            'mcod.last_name as ship_to_last_name',
            'mcod.tel_num as ship_tel_num',
            'mcod.zip_code as ship_zip_code',
            'mcod.prefecture as ship_prefecture',
            'mcod.city as ship_city',
            'mcod.sub_address as ship_sub_address',
            'od.receiver_id'
        ];
        $result = $this->select($col)
                       ->join('mst_order_detail as od', 'mst_order.receive_id', '=', 'od.receive_id')
                       ->join('mst_customer as mco', 'mst_order.customer_id', '=', 'mco.customer_id')
                       ->join('mst_customer as mcod', 'od.receiver_id', '=', 'mcod.customer_id')
                       ->join('edi.mst_product as emp', function ($join) {
                            $join->on('od.product_code', '=', 'emp.product_code')
                                 ->where('emp.is_live', '=', 1);
                       })
                       ->where('mst_order.order_status', '=', ORDER_STATUS['NEW'])
                       ->where('mst_order.order_sub_status', '=', ORDER_SUB_STATUS['NEW'])
                       ->where('mst_order.is_delay', 0)
                       ->orderBy('mst_order.receive_id')
                       ->get();
        return $result->toArray();
    }

    /**
     * Get list data to do process divide
     * @return object $result
     */
    public function getDataProcessDivide()
    {
        $arrSelect = [
            'mst_order.receive_id',
            'ps.parent_product_code',
            'ps.child_product_code',
            'ps.component_num',
            'ps.set_type',
            'mp.price_supplier_id',
            'mp.price_invoice',
            'od.detail_line_num',
            'od.product_code',
            'od.quantity',
            'od.price',
        ];
        $result = $this->select($arrSelect)
                       ->join('mst_order_detail as od', 'mst_order.receive_id', '=', 'od.receive_id')
                       ->leftJoin('mst_product_set as ps', 'od.product_code', '=', 'ps.parent_product_code')
                       ->join('mst_product as mp', 'od.product_code', '=', 'mp.product_code')
                       ->leftjoin('dt_order_product_detail as opd', 'od.receive_id', '=', 'opd.receive_id')
                       ->where('mst_order.order_status', '=', ORDER_STATUS['VERIFY_DATA'])
                       ->where('mst_order.order_sub_status', '=', ORDER_SUB_STATUS['DONE'])
                       ->whereNull('opd.receive_id')
                       ->where('mst_order.is_delay', 0)
                       ->orderBy('mst_order.receive_id')
                       ->orderBy('od.detail_line_num')
                       ->orderBy('ps.parent_product_code')
                       ->get();
        return $result;
    }

    /**
    * Get data process supplier EDI direct
    * @param string $type use when is product set
    * @return object $result
    */
    public function getDataProcessEDIDirect($type)
    {
        $col = [
            'mst_order.*',
            'od.*',
            'opd.*',
            'mp.*',
            'tc.*',
        ];
        $result = $this->join('mst_order_detail as od', 'mst_order.receive_id', '=', 'od.receive_id')
                        ->join('dt_order_product_detail as opd', function ($join) {
                            $join->on('od.receive_id', '=', 'opd.receive_id')
                                 ->on('od.detail_line_num', '=', 'opd.detail_line_num');
                        })
                      ->join('mst_product as mp', "opd.$type", '=', 'mp.product_code')
		                    ->join('edi.mst_product as tc', function($join) {
                            $join->on('mp.product_code', '=', 'tc.product_code');
                            $join->where('tc.is_live', '=', 1);
                        })
                       //->join('api.cheetah as tc', 'mp.product_code', '=', 'tc.product_code')
                       ->where('product_status', '=', PRODUCT_STATUS['WAIT_TO_ORDER'])
                       ->where('delivery_type', 2)
                       ->where('mst_order.order_status', ORDER_STATUS['ORDER'])
                       ->where('mst_order.order_sub_status', '<>', ORDER_SUB_STATUS['ERROR'])
                       ->where('mst_order.is_delay', 0)
                       ->orderBy('od.receive_id')
                       ->orderBy('od.detail_line_num')
                       ->get();
        return $result;
    }

    /**
    * Get data process repare to request GLSC to delivery
    * @return object $result
    */
    public function getDataProcessReGLSC()
    {
        $col = [
            'opd.receive_id',
            'opd.detail_line_num',
            'opd.sub_line_num',
            'product_status',
        ];
        $result = $this->select($col)
                       ->join('mst_order_detail as od', 'mst_order.receive_id', '=', 'od.receive_id')
                       ->join('dt_order_product_detail as opd', 'od.receive_id', '=', 'opd.receive_id')
                       ->where('opd.sub_process_status', '=', 2)
                       ->where('mst_order.order_sub_status', '<>', ORDER_SUB_STATUS['ERROR'])
                       ->orderBy('opd.receive_id')
                       ->orderBy('opd.detail_line_num')
                       ->orderBy('opd.sub_line_num')
                       ->get();
        return $result;
    }

    /**
    * Get data process request GLSC to delivery
    * @return object $result
    */
    public function getDataProcessGLSC()
    {
        $col = [
            'mp.product_jan',
            'mp.maker_full_nm',
            'mp.product_maker_code',
            'mp.product_name_long',
            'mp.product_oogata',
            'mp.rak_img_url_1',
            'mst_order.receive_id',
            'mst_order.mall_id',
            'mst_order.received_order_id',
            'mst_order.delivery_method',
            'mst_order.delivery_count',
            'mst_order.total_price',
            'mst_order.request_price',
            'mst_order.ship_wish_date',
            'mst_order.ship_wish_time',
            'mst_order.payment_method',
            'mst_order.delivery_instrustions',
            'mst_order.arrive_type',
            'mst_order.order_date',
            'od.quantity',
            'od.receiver_id',
            'mst_order.company_name',
            'mst_order.shipment_date',
            'mcod.zip_code as d_postal',
            'mcod.prefecture',
            'mcod.city',
            'mcod.sub_address',
            'mcod.tel_num',
            'od.detail_line_num',
//           'od.product_code',
            'opd.received_order_num',
//            'opd.child_product_code',
            'opd.sub_line_num',
        ];
        $sub = '(select deli_days from mst_postal where postal_code = mcod.zip_code limit 1) AS deli_days';
        $subCode = '(select delivery_code from mst_postal where postal_code = mcod.zip_code limit 1) AS delivery_code';
        $sub_pro = 'IFNULL(opd.child_product_code, opd.product_code) AS product_code';
        $result = $this->select($col)
                       ->selectRaw($sub)
                       ->selectRaw($subCode)
                       ->selectRaw($sub_pro)
                       ->selectRaw('CONCAT(IFNULL(mco.last_name, ""), IFNULL(mco.first_name, "")) as c_nm')
                       ->selectRaw('CONCAT(IFNULL(mcod.last_name, ""), IFNULL(mcod.first_name, "")) as d_nm')
                       ->join('mst_order_detail as od', 'mst_order.receive_id', '=', 'od.receive_id')
                       ->join('mst_customer as mco', 'mst_order.customer_id', '=', 'mco.customer_id')
                       ->join('mst_customer as mcod', 'od.receiver_id', '=', 'mcod.customer_id')
                       ->join('dt_order_product_detail as opd', function ($join) {
                            $join->on('od.receive_id', '=', 'opd.receive_id')
                                 ->on('od.detail_line_num', '=', 'opd.detail_line_num');
                       })
                       ->join(
                           'mst_product as mp',
                           DB::raw('IFNULL(opd.child_product_code, opd.product_code)'),
                           '=',
                           'mp.product_code'
                       )
                       ->where('mst_order.order_status', '=', ORDER_STATUS['DELIVERY'])
                       ->whereIn('delivery_type', [1, 3])
                       ->where('product_status', '=', PRODUCT_STATUS['WAIT_TO_SHIP'])
                       ->where('mst_order.order_sub_status', '=', ORDER_SUB_STATUS['NEW'])
                       ->where('mst_order.is_delay', 0)
                       ->orderBy('od.receive_id')
                       ->orderBy('od.detail_line_num')
                       ->get();
        return $result;
    }

    /**
    * The get data process payment.
    * @return object $result
    */
    public function getDataProcessPayment()
    {
        $col = [
            'opd.receive_id',
            'detail_line_num',
            'sub_line_num',
            'payment_method',
            'product_status',
            'mall_id',
            'opd.stock_status',
        ];
        $result = $this->select($col)
                       ->join('dt_order_product_detail as opd', 'mst_order.receive_id', '=', 'opd.receive_id')
                       ->where('sub_process_status', '=', 2)
                       ->where('order_status', '<', 3)
                       ->where('mst_order.order_sub_status', '<>', ORDER_SUB_STATUS['ERROR'])
                       ->where('mst_order.is_delay', 0)
                       ->get();
        return $result;
    }

    /**
    * The update infomation.
    * @param string $arrReceiveId array receive_id will updated
    * @param string $arrUpdate    array data want update
    * @return object $result
    */
    public function updateData($arrReceiveId, $arrUpdate)
    {
        if (array_key_exists('order_status', $arrUpdate) ||
          array_key_exists('order_sub_status', $arrUpdate)) {
            $className  = debug_backtrace(false, 2)[1]['class'];
            $modelLog = new LogUpdateStatusOrder();
            $modelLog->insertLog($arrReceiveId, $arrUpdate, $className);
        }
        $result = $this->whereIn('receive_id', $arrReceiveId)
                      ->update($arrUpdate);
        return $result;
    }

        /**
    * The update infomation.
    * @param string $arrReceiveId array receive_id will updated
    * @param string $arrUpdate    array data want update
    * @return object $result
    */
    public function updateDataByReceiveOrder($arrReceiveOrder, $arrUpdate)
    {
        if (array_key_exists('order_status', $arrUpdate) ||
          array_key_exists('order_sub_status', $arrUpdate)) {
            $className  = debug_backtrace(false, 2)[1]['class'];
            $modelLog = new LogUpdateStatusOrder();
            $modelLog->insertLog($arrReceiveOrder, $arrUpdate, $className, 'received_order_id');
        }
        $result = $this->whereIn('received_order_id', $arrReceiveOrder)
                      ->update($arrUpdate);
        return $result;
    }

    /**
    * The get data process payment confirm step 1.
    * @return object $result
    */
    public function getDataPaymentConfirm1()
    {
        $col1 = [
            'opd.receive_id',
            'payment_method',
            'payment_status',
            'order_status',
            'mall_id',
            'order_sub_status',
            'is_mail_sent',
            'smt.is_available'
        ];
        $step1 = $this->select($col1)
                      ->join('dt_order_product_detail as opd', 'mst_order.receive_id', '=', 'opd.receive_id')
                      ->leftjoin('mst_send_mail_timing as smt', function ($join) {
                          $join->on('mst_order.order_status', '=', 'smt.order_status_id')
                              ->on('mst_order.order_sub_status', '=', 'smt.order_sub_status_id');
                      })
                      ->where('order_status', '=', ORDER_STATUS['ESTIMATION'])
                      ->where('order_sub_status', '=', ORDER_SUB_STATUS['DONE'])
                      ->where('is_delay', '=', 0)
                      ->orderBy('mst_order.receive_id')
                      ->get();
        return $step1;
    }

    /**
    * The get data process payment confirm step 2.
    * @return object $result
    */
    public function getDataPaymentComfirm2()
    {
        $col2 = [
            'receive_id',
            'os.delay_priod',
            'os.expiration_priod',
            'payment_request_date',
            'payment_status',
            'mst_order.payment_method',
            'mst_order.received_order_id',
            'mst_order.mall_id',
            'mc.email',
            'mst_signature.signature_content',
        ];

        $step2 = $this->select($col2)
                      ->join('mst_order_status as os', 'mst_order.order_status', '=', 'os.order_status_id')
                      ->leftjoin('mst_send_mail_timing as smt', function ($join) {
                          $join->on('mst_order.order_status', '=', 'smt.order_status_id')
                               ->on('mst_order.order_sub_status', '=', 'smt.order_sub_status_id');
                      })
                      ->leftJoin('mst_signature', 'mst_order.mall_id', '=', 'mst_signature.mall_id')
                      ->join('mst_customer as mc', 'mst_order.customer_id', '=', 'mc.customer_id')
                      ->where(function ($query) {
                            $query->where(function ($sub) {
                                $sub->where('order_status', ORDER_STATUS['PAYMENT_CONFIRM'])
                                    ->where('order_sub_status', '<', ORDER_SUB_STATUS['DONE'])
                                    ->whereRaw('is_mail_sent = (case when smt.is_available is null then 0 else is_available end)');
                            })
                            ->orWhere(function ($sub) {
                                $sub->where('order_sub_status', ORDER_SUB_STATUS['DELAY'])
                                    ->whereRaw('DATEDIFF(NOW(), payment_request_date) > 6');
                            })
                            ->orWhere(function ($sub) {
                                $sub->where('order_sub_status', ORDER_SUB_STATUS['DELAY'])
                                    ->where('payment_status', 1);
                            });
                      })
                      ->where('order_sub_status', '<>', ORDER_SUB_STATUS['ERROR'])
                      ->where('mst_order.is_delay', 0)
                      ->orderBy('receive_id')
                      ->get();

        return $step2;
    }

    /**
    * The get data process confirm status.
    * @return object $result
    */
    public function getDataConfirmStatus()
    {
        $col = [
            'mst_order.receive_id',
            'mst_order.is_mail_sent',
            'smt.is_available',
        ];
        $result = $this->select($col)
                       ->leftjoin('mst_send_mail_timing as smt', function ($join) {
                          $join->on('mst_order.order_status', '=', 'smt.order_status_id')
                               ->on('mst_order.order_sub_status', '=', 'smt.order_sub_status_id');
                       })
                       ->where('order_status', '=', ORDER_STATUS['PAYMENT_CONFIRM'])
                       ->where('order_sub_status', '=', ORDER_SUB_STATUS['DONE'])
                       ->where('is_delay', 0)
                       ->get();
        return $result;
    }

    /**
    * The get data process order zaiko step 3.
    * @return object $result
    */
    public function getDataZaikoResult3()
    {
        $col = [
            'mst_order.receive_id',
            'mst_order.mall_id',
            'product_status',
            'payment_method',
            'is_sourcing_on_demand',
        ];
        $result = $this->select($col)
                       ->join('dt_order_product_detail as opd', 'mst_order.receive_id', '=', 'opd.receive_id')
                       ->where('order_status', '=', ORDER_STATUS['ESTIMATION'])
                       ->where('order_sub_status', '<', ORDER_SUB_STATUS['DONE'])
                       ->where('order_sub_status', '<>', ORDER_SUB_STATUS['ERROR'])
                       ->where('mst_order.is_delay', 0)
                       ->orderBy('mst_order.receive_id')
                       ->get();
        return $result;
    }

    /**
    * The get data glsc check status.
    * @return object $result
    */
    public function getDataGlscCheckStatus()
    {
        $col = [
            'mst_order.receive_id',
            'mst_order.is_mail_sent',
            'mst_order.mall_id',
            'mst_order.payment_method',
            'smt.is_available',
        ];
        $result = $this->select($col)
                       ->leftjoin('mst_send_mail_timing as smt', function ($join) {
                          $join->on('mst_order.order_status', '=', 'smt.order_status_id')
                               ->on('mst_order.order_sub_status', '=', 'smt.order_sub_status_id');
                       })
                       ->where('mst_order.order_status', '=', ORDER_STATUS['ARRIVAL'])
                       ->where('order_sub_status', '=', ORDER_SUB_STATUS['DONE'])
                       ->where('is_delay', '=', 0)
                       ->get();
        return $result;
    }

    /**
    * The get data process edi result step 3.
    * @return object $result
    */
    public function getDataEdiOrderResult3()
    {
        $col = [
            'mst_order.receive_id',
            'product_status'
        ];
        $result = $this->select($col)
                       ->join('dt_order_product_detail as opd', 'mst_order.receive_id', '=', 'opd.receive_id')
                       ->where('order_status', '=', ORDER_STATUS['ORDER'])
                       ->where('order_sub_status', ORDER_SUB_STATUS['DOING'])
                       ->where('is_mail_sent', 1)
                       ->where('mst_order.is_delay', 0)
                       ->orderBy('mst_order.receive_id')
                       ->get();
        return $result;
    }

    /**
    * The get data process delivery result step 7.
    * @return object $result
    */
    public function getDataDeliveryResult7()
    {
        $col = [
            'mst_order.receive_id',
            'mst_order.shipment_date',
            'opd.detail_line_num',
            'opd.sub_line_num',
            'opd.product_status',
        ];
        $result = $this->select($col)
                       ->join('dt_order_product_detail as opd', 'opd.receive_id', '=', 'mst_order.receive_id')
                       ->where('mst_order.order_status', '=', ORDER_STATUS['DELIVERY'])
                       ->whereIn('mst_order.order_sub_status', [ORDER_SUB_STATUS['NEW'], ORDER_SUB_STATUS['DOING']])
                       ->where('mst_order.is_delay', 0)
                       ->orderBy('mst_order.receive_id')
                       ->get();
        return $result;
    }

    /**
    * The get data process mail queue.
    * @return object $result
    */
    public function getDataMailQueue()
    {
        $col = [
            'mst_order.receive_id',
            'mst_order.mall_id',
            'mst_order.payment_method',
            'mst_order.delivery_plan_date',
            'mst_order.order_status',
            'mst_order.order_sub_status',
            'mst_order.is_sourcing_on_demand',
            'received_order_id',
            'order_status_id',
            'order_sub_status_id',
            'smt.mail_id',
            'mt.mail_subject',
            'mt.mail_content',
            'mt.attached_file_path',
            'mc.email',
            'mst_signature.signature_content',
        ];
        $subQueryStock = '(select stock_status from dt_order_product_detail ';
        $subQueryStock .= 'where dt_order_product_detail.receive_id = mst_order.receive_id and stock_status = 2 ';
        $subQueryStock .= 'group by stock_status) as stock_status';
        $subQueryDelivery = '(select delivery_type from dt_order_product_detail ';
        $subQueryDelivery .= 'where dt_order_product_detail.receive_id = mst_order.receive_id and delivery_type = 3 ';
        $subQueryDelivery .= 'group by delivery_type) as delivery_type';
        $result = $this->select($col)
                       ->selectRaw($subQueryStock)
                       ->selectRaw($subQueryDelivery)
                       ->join('mst_send_mail_timing as smt', function ($join) {
                          $join->on('mst_order.order_status', '=', 'smt.order_status_id')
                              ->on('mst_order.order_sub_status', '=', 'smt.order_sub_status_id');
                       })
                       ->join('mst_mail_template as mt', 'smt.mail_id', '=', 'mt.mail_id')
                       ->join('mst_customer as mc', 'mst_order.customer_id', '=', 'mc.customer_id')
                       ->leftJoin('mst_signature', 'mst_order.mall_id', '=', 'mst_signature.mall_id')
                       ->where('mst_order.is_mail_sent', '=', 0)
                       ->where('mst_order.is_delay', 0)
                       ->where('smt.is_available', '=', 1)
                       ->get();
        return $result;
    }

    /**
    * The get information process content mail.
    * @param string $receiveOrderId order_id of order want sent mail
    * @return object $result
    */
    public function getInfoOrderMail($receivedOrderId)
    {
        $col = [
            'mst_order.receive_id',
            'mst_order.received_order_id',
            'mst_order.order_date',
            'mst_order.shipment_date',
            'mst_order.total_price',
            'mst_order.goods_price',
            'mst_order.ship_charge',
            'mst_order.pay_charge',
            'mst_order.discount',
            'mst_order.used_point',
            'mst_order.used_coupon',
            'mst_order.company_name',
            'mst_order.mall_id',
            'mst_order.pay_after_charge',
            'mst_order.pay_charge_discount',
            'mst_order.payment_price',
            'mst_order.payment_confirm_date',
            'mst_order.payment_account',
            'mst_order.payment_method',
            'mst_order.customer_question',
            'mst_order.mail_seri',
            'mst_order.shop_answer',
            'mst_cancel_reason.add_2_mail_text as cancel_reason',
            'mst_order.request_price',
            'mst_order.order_status',
            'dt_payment_list.payment_date',
            'mco.last_name as order_last_name',
            'mco.first_name as order_first_name',
            'mco.zip_code as order_zip_code',
            'mco.prefecture as order_prefecture',
            'mco.sub_address as order_sub_address',
            'mco.city as order_city',
            'mco.tel_num as order_tel_num',
            'sm.payment_name',
            'mcod.last_name as ship_last_name',
            'mcod.first_name as ship_first_name',
            'mcod.zip_code as ship_zip_code',
            'mcod.prefecture as ship_prefecture',
            'mcod.sub_address as ship_sub_address',
            'mcod.city as ship_city',
            'mcod.tel_num as ship_tel_num',
            'mst_order.delivery_plan_date',
            'dd.delivery_date',
            'dd.inquiry_no',
            'msc.company_name AS ship_company_name'
        ];
        $result = $this->select($col)
                       ->join('mst_order_detail as od', 'mst_order.receive_id', '=', 'od.receive_id')
                       ->leftjoin('mst_settlement_manage as sm', 'mst_order.payment_method', '=', 'sm.payment_code')
                       ->join('mst_customer as mco', 'mst_order.customer_id', '=', 'mco.customer_id')
                       ->join('mst_customer as mcod', 'od.receiver_id', '=', 'mcod.customer_id')
                       ->leftjoin('dt_delivery as dd', 'mst_order.received_order_id', '=', 'dd.received_order_id')
                       ->leftjoin('mst_shipping_company as msc', 'dd.delivery_code', '=', 'msc.company_id')
                       ->leftjoin('dt_payment_list', 'dt_payment_list.receive_id', '=', 'mst_order.receive_id')
                       ->leftjoin('mst_cancel_reason', 'mst_cancel_reason.reason_id', '=', 'mst_order.cancel_reason')
                       ->where('mst_order.received_order_id', '=', $receivedOrderId)
                       ->first();
        return $result;
    }
    /**
    * The get information order detail process content mail.
    * @param string $receiveOrderId order_id of order want sent mail
    * @return object $result
    */
    public function getInfoOrderDetail($receivedOrderId)
    {
        $col = [
            'mst_order.received_order_id',
            'mcod.last_name as ship_last_name',
            'mcod.first_name as ship_first_name',
            'mcod.zip_code as ship_zip_code',
            'mcod.prefecture as ship_prefecture',
            'mcod.city as ship_city',
            'mcod.sub_address as ship_sub_address',
            'mcod.tel_num as ship_tel_num',
            'od.product_code',
            'od.product_name',
            'od.receiver_id',
            // 'od.price',
            // 'od.quantity',
            'dto.delivery_type',
            'dto.suplier_id'
        ];
        $subPrice = 'CASE WHEN dto.product_status = 15 THEN 0 ELSE od.price END as price';
        $subQuantity = 'CASE WHEN dto.product_status = 15 THEN 0 ELSE od.quantity END as quantity';
        $result = $this->select($col)
                       ->selectRaw($subPrice)
                       ->selectRaw($subQuantity)
                       ->join('mst_order_detail as od', 'mst_order.receive_id', '=', 'od.receive_id')
                       ->join('mst_customer as mcod', 'od.receiver_id', '=', 'mcod.customer_id')
                       ->leftJoin('dt_order_product_detail as dto', function ($join) {
                          $join->on('dto.receive_id', '=', 'od.receive_id')
                              ->on('dto.detail_line_num', '=', 'od.detail_line_num');
                       })
                       ->where('mst_order.received_order_id', '=', $receivedOrderId)
                       ->groupBy('od.detail_line_num')
                       ->get();
        return $result;
    }
    /**
    * The get information order detail process content mail in case product not exist in dt_order_product_detail.
    * @param string $receiveOrderId order_id of order want sent mail
    * @return object $result
    */
    public function getInfoNotInOrderDetail($receivedOrderId)
    {
        $col = [
            'mst_order.received_order_id',
            'mcod.last_name as ship_last_name',
            'mcod.first_name as ship_first_name',
            'mcod.zip_code as ship_zip_code',
            'mcod.prefecture as ship_prefecture',
            'mcod.city as ship_city',
            'mcod.sub_address as ship_sub_address',
            'mcod.tel_num as ship_tel_num',
            'od.product_code',
            'od.product_name',
            // 'od.price',
            // 'od.quantity'
        ];
        $subPrice = 'CASE WHEN dto.product_status = 15 THEN 0 ELSE od.price END as price';
        $subQuantity = 'CASE WHEN dto.product_status = 15 THEN 0 ELSE od.quantity END as quantity';
        $result = $this->select($col)
                       ->selectRaw($subPrice)
                       ->selectRaw($subQuantity)
                       ->join('mst_order_detail as od', 'mst_order.receive_id', '=', 'od.receive_id')
                       ->join('mst_customer as mcod', 'od.receiver_id', '=', 'mcod.customer_id')
                       ->leftJoin('dt_order_product_detail as dto', function ($join) {
                          $join->on('dto.receive_id', '=', 'od.receive_id')
                              ->on('dto.detail_line_num', '=', 'od.detail_line_num');
                       })
                       ->where('mst_order.received_order_id', '=', $receivedOrderId)
                       ->groupBy('od.detail_line_num')
                       ->get();
        return $result;
    }
    /**
     * Get data delivery result step 5
     *
     * @return object
     */
    public function getDataDeliveryResult5()
    {
        $col = [
            'od.receive_id AS od_receive_id',
            'od.detail_line_num AS od_detail_line_num',
            'od.sub_line_num AS od_sub_line_num',
            'od.product_code',
            'od.price_invoice',
            'ddd.received_order_id',
            'ddd.subdivision_num',
            'ddd.detail_line_num',
            'ddd.delivery_num',
            'ddd.delivery_real_date',
        ];
        $result = $this->select($col)
                       ->join('dt_order_product_detail as od', 'mst_order.receive_id', '=', 'od.receive_id')
                       ->join('dt_delivery_detail as ddd', function ($join) {
                          $join->on('mst_order.received_order_id', '=', 'ddd.received_order_id')
                               ->on('od.sub_line_num', '=', 'ddd.detail_line_num');
                       })
                       ->join('dt_delivery as dd', function ($join) {
                          $join->on('mst_order.received_order_id', '=', 'dd.received_order_id')
                          ->on('ddd.subdivision_num', '=', 'dd.subdivision_num');
                       })
                       ->where('dd.delivery_status', '=', 2)
                       ->where('mst_order.is_delay', 0)
                       ->where('mst_order.order_sub_status', '<>', ORDER_SUB_STATUS['ERROR'])
                       ->get();
        return $result;
    }

    /**
    * Get data by order id.
    * @param string $orderId
    * @return object
    */
    public function getDataByOrderId($orderId)
    {
        $result = $this->join('mst_order_detail as od', 'mst_order.receive_id', '=', 'od.receive_id')
                       ->where('received_order_id', $orderId)
                       ->orderBy('od.detail_line_num', 'DESC')
                       ->first();
        return $result;
    }

    /**
    * Get data process receive result step 1
    * @return object $result
    */
    public function getDataReceiveResult1()
    {
        $col = [
            'receive_id',
            'is_mail_sent',
            'is_available',
        ];
        $result = $this->select($col)
                       ->leftjoin('mst_send_mail_timing', function ($join) {
                          $join->on('mst_order.order_status', '=', 'mst_send_mail_timing.order_status_id')
                               ->on('mst_order.order_sub_status', '=', 'mst_send_mail_timing.order_sub_status_id');
                       })
                       ->where('is_mail_sent', 0)
                       ->where('order_status', ORDER_STATUS['ORDER'])
                       ->where('order_sub_status', ORDER_SUB_STATUS['DONE'])
                       ->where('mst_order.is_delay', 0)
                       ->get();
        return $result;
    }

    /**
    * Get data process receive result step 4
    * @return object $result
    */
    public function getDataReceiveResult4()
    {
        $col = [
            'od.product_status',
            'mst_order.receive_id',
        ];
        $result = $this->select($col)
                       ->join('dt_order_product_detail as od', 'mst_order.receive_id', '=', 'od.receive_id')
                       ->where('order_status', ORDER_STATUS['ARRIVAL'])
                       ->where('order_sub_status', '<', ORDER_SUB_STATUS['DONE'])
                       ->where('mst_order.is_delay', 0)
                       ->orderBy('mst_order.receive_id')
                       ->get();
        return $result;
    }

    /**
     * Get order api
     *
     * @param  int    $orderStatus
     * @param  int    $orderSubStatus
     * @param  int    $mailId
     * @param  int    $limit
     * @return object
     */
    public function getOrderAPI($orderStatus, $orderSubStatus, $mailId, $limit = null)
    {
        $data = $this->select('mst_order.received_order_id', 'mst_order.order_date');
        if ($mailId === 1) {
            $data->leftJoin('dt_web_rak_data', 'dt_web_rak_data.order_number', '=', 'mst_order.received_order_id')
                ->whereNull('dt_web_rak_data.order_number');
        } elseif ($mailId === 2) {
            $data->leftJoin('dt_web_yah_data', 'dt_web_yah_data.order_id', '=', 'mst_order.received_order_id')
                ->whereNull('dt_web_yah_data.order_id');
        }
        $data->where('mst_order.order_status', $orderStatus)
            ->where('mst_order.order_sub_status', $orderSubStatus)
            ->where('mst_order.mall_id', $mailId);

        if ($limit !== null) {
            $data->limit($limit);
        }
        return $data->get();
    }

    /**
     * Get data cancel reservation
     * @return object
     */
    public function getDataCancelReservation()
    {
        $col = [
            'mst_order.mall_id',
            'mst_order.received_order_id',
        ];
        $result = $this->select()
                       ->join('mst_cancel_reservation AS cr', function ($join) {
                            $join->on('mst_order.mall_id', '=', 'cr.mall_id')
                                  ->on('mst_order.received_order_id', '=', 'cr.receive_order_id');
                       })
                       ->where('order_status', ORDER_STATUS['NEW'])
                       ->where('is_deleted', 0)
                       ->where('mst_order.is_delay', 0)
                       ->where('status', 0)
                       ->get();
        return $result;
    }

    /**
     * Get cancel order
     *
     * @return array
     */
    public function getCancelOrderData()
    {
        $cancelStatus = PRODUCT_STATUS['CANCEL'];
        $data = $this->select(
            'mor.receive_id',
            'ordt.product_code',
            'total_price',
            'ship_charge',
            'request_price',
            'used_point',
            DB::raw('SUM(ordt.price*ordt.quantity) AS cancel_price'),
            'sf.below_limit_price',
            'sf.charge_price'
        )
        ->from('hrnb.mst_order AS mor')
        ->join('hrnb.mst_order_detail AS ordt', 'mor.receive_id', '=', 'ordt.receive_id')
        ->join(
            DB::raw("
            (SELECT
                det.receive_id,
                det.product_code,
                det.product_status
            FROM
                hrnb.dt_order_product_detail AS det
            WHERE
                det.product_status = $cancelStatus
            GROUP BY det.product_code) AS temp"),
            function ($join) {
                $join->on('ordt.receive_id', '=', 'temp.receive_id')
                    ->on('ordt.product_code', '=', 'temp.product_code');
            }
        )
        ->leftJoin('hrnb.mst_shipping_fee AS sf', function ($join) {
            $join->on('sf.mall_id', '=', 'mor.mall_id')
                ->where('sf.is_delete', 0);
        })
        ->where('mor.order_status', '<>', ORDER_STATUS['CANCEL'])
        ->where('is_mall_update', 0)
        ->groupBy('mor.receive_id')
        ->get();
        return $data;
    }

    /**
     * Get cancel order api
     *
     * @return type
     */
    public function getCancelOrderAPI()
    {
        $data = $this->select(
            'mst_order.receive_id',
            'mst_order.received_order_id',
            'mst_order_detail.order_item_code',
            'mst_order.mall_id',
            'mst_order.payment_method',
            'mst_order.payment_status',
            'mst_cancel_reason.rakuten_reason',
            'mst_cancel_reason.amazon_reason',
            'mst_cancel_reason.yahoo_reason',
            'mst_cancel_reason.wowma_reason',
            'mst_cancel_reason.wowma_reason_comment',
            'mst_cancel_reason.yahoo_reason_comment',
            'mst_order.up_date'
        )
        ->join('mst_order_detail', 'mst_order_detail.receive_id', '=', 'mst_order.receive_id')
        ->leftJoin('mst_cancel_reason', 'mst_cancel_reason.reason_id', '=', 'mst_order.cancel_reason')
        ->where('order_status', ORDER_STATUS['CANCEL'])
        ->where('is_mall_cancel', 0)
        ->get();
        return $data;
    }

    /**
     * Get data update status batch finish order
     * @return object
     */
    public function getDataFinishUpdate()
    {
        return $this->select('receive_id')
                    ->where('order_status', ORDER_STATUS['DELIVERY'])
                    ->where('order_sub_status', ORDER_SUB_STATUS['DONE'])
                    ->where('is_mail_sent', 1)
                    ->get();
    }

    /**
     * Get data process finish order
     * @return object
     */
    public function getDataFinishOrderAPI()
    {
        $col = [
            'mst_order.receive_id',
            'mst_order.received_order_id',
            'mst_order.payment_method',
            'mst_order.request_price',
            'mst_order.shipment_date',
            'mst_order.ship_wish_date',
            'mst_order.ship_id_rakuten',
            'inquiry_no',
            'msc.company_name',
            'mall_id',
            'dt_delivery.delivery_plan_date',
            'dt_delivery.delivery_code',
            'dt_delivery.delivery_real_date',
        ];
        $result = $this->select($col)
                       ->selectRaw("CASE WHEN mst_order.shipment_date < NOW() THEN NOW() ELSE mst_order.shipment_date END as shipment_date")
                       ->selectRaw('IFNULL(dt_delivery.inquiry_no, "") as inquiry_no')
                       ->selectRaw('MAX(odp.delivery_date) as max_delivery_date')
                       ->leftJoin('dt_delivery', 'mst_order.received_order_id', '=', 'dt_delivery.received_order_id')
                       ->leftJoin('mst_shipping_company AS msc', 'dt_delivery.delivery_code', '=', 'msc.company_id')
                       ->leftJoin('mst_order_detail as od', 'mst_order.receive_id', '=', 'od.receive_id')
                       ->leftJoin('dt_order_product_detail AS odp', function ($query) {
                           $query->on('odp.detail_line_num', '=', 'od.detail_line_num')
                                 ->on('odp.receive_id', '=', 'mst_order.receive_id');
                       })
                       ->where('mst_order.order_status', ORDER_STATUS['SETTLEMENT'])
                       ->where('mst_order.order_sub_status', ORDER_SUB_STATUS['NEW'])
                       ->whereIn('mall_id', [1, 2, 3, 4, 5, 8, 9, 10])
                       ->where('shipment_date', '<=', NOW())
                       ->groupBy('mst_order.received_order_id')
                       ->get();
        return $result;
    }

    /**
     * Get data Toriyose order cancel directly
     *
     * @param   string  $type
     * @param   int     $limit
     * @return  object
     */
    public function getDataToriyoseOrderCancel($type, $limit)
    {
        $data = $this->select(
            'mst_order.receive_id',
            'mst_order.received_order_id',
            'mst_order_detail.order_item_code',
            'dt_order_product_detail.detail_line_num',
            'dt_order_product_detail.sub_line_num',
            'dt_order_product_detail.product_code',
            'dt_order_product_detail.order_code',
            'dt_order_product_detail.received_order_num'
        );

        $data->join('mst_order_detail', 'mst_order_detail.receive_id', '=', 'mst_order.receive_id')
        ->join('dt_order_product_detail', function ($join) {
            $join->on('dt_order_product_detail.receive_id', '=', 'mst_order_detail.receive_id')
                ->on('dt_order_product_detail.detail_line_num', '=', 'mst_order_detail.detail_line_num');
        });

        if ($type === 'direct' || $type === 'detail') {
            $data->join(
                'dt_order_to_supplier',
                'dt_order_to_supplier.order_code',
                '=',
                'dt_order_product_detail.order_code'
            );
            if ($type === 'direct') {
                $data->join(
                    'edi.t_order_direct',
                    't_order_direct.order_code',
                    '=',
                    'dt_order_to_supplier.edi_order_code'
                );
            } elseif ($type === 'detail') {
                $data->join('edi.t_order', 't_order.order_code', '=', 'dt_order_to_supplier.edi_order_code')
                    ->join(
                        'edi.t_order_detail',
                        't_order_detail.order_code',
                        '=',
                        'dt_order_to_supplier.edi_order_code'
                    );
            }
        } elseif ($type === 'stock') {
            $data->join(
                'mst_stock_status',
                'mst_stock_status.product_code',
                '=',
                'dt_order_product_detail.product_code'
            );
        }

        $data->where('mst_order.is_sourcing_on_demand', 1)
        ->whereDate('mst_order.response_due_date', date('Y-m-d'))
        ->where('mst_order.mall_id', 3);

        if ($type === 'direct' || $type === 'detail') {
            $data->where('dt_order_to_supplier.process_status', 1);
            if ($type === 'direct') {
                $data->where('t_order_direct.m_order_type_id', 2);
            } elseif ($type === 'detail') {
                $data->where('t_order_detail.m_order_type_id', 2);
            }
        } elseif ($type === 'stock') {
            $data->whereIn('dt_order_product_detail.product_status', [
                PRODUCT_STATUS['WAIT_TO_ARRIVAL'],
                PRODUCT_STATUS['OUT_OF_STOCK'],
                PRODUCT_STATUS['SALE_STOP']
            ]);
        }

        $data->limit($limit);
        return $data->get();
    }

    /**
     * Get data Toriyose order cancel directly
     *
     * @param   int     $limit
     * @return  object
     */
    public function getDataToriyoseOrderDelivery($limit)
    {
        $data = $this->select(
            'mst_order.receive_id',
            'mst_order.received_order_id',
            'mst_order.order_status',
            'mst_order.order_sub_status',
            'mst_order.delivery_plan_date',
            'mst_order_detail.product_code'
        );

        $data->join('mst_order_detail', 'mst_order_detail.receive_id', '=', 'mst_order.receive_id');
/*        $data->join('dt_order_product_detail', function ($join) {
            $join->on('dt_order_product_detail.receive_id', '=', 'mst_order_detail.receive_id')
                ->on('dt_order_product_detail.detail_line_num', '=', 'mst_order_detail.detail_line_num');
        });
        $data->join(
            'dt_order_to_supplier',
            'dt_order_to_supplier.order_code',
            '=',
            'dt_order_product_detail.order_code'
        );
        $data->join(
            'edi.t_order_direct',
            't_order_direct.order_code',
            '=',
            'dt_order_to_supplier.edi_order_code'
        );*/
        $data->where('mst_order.order_status', ORDER_STATUS['ORDER'])
            ->where('mst_order.order_sub_status', ORDER_SUB_STATUS['DOING'])
            ->where('mst_order.is_mail_sent', 0)
            ->where('mst_order.is_sourcing_on_demand', '<>', 0)
            ->where('mst_order.mall_id', 3);

        $data->limit($limit);
        return $data->get();
    }


    /**
     * Get data check order exists
     * @param   array     $arrReceived
     * @return  object
     */
    public function getDataCheckOrder($arrReceived)
    {
        return $this->select('received_order_id')
                   ->whereIn('received_order_id', $arrReceived)
                   ->get();
    }

    /**
     * Get data rakuten banking
     *
     * @return object
     */
    public function getDataRakutenBanking()
    {
        $data = $this->select('receive_id', 'received_order_id', 'order_date', 'payment_request_date')
            ->where('mall_id', 1)
            ->where('order_status', ORDER_STATUS['PAYMENT_CONFIRM'])
            ->where('payment_method', 7)
            ->where('payment_status', 0)
            ->whereNotIn('order_sub_status', [ORDER_SUB_STATUS['DONE'], ORDER_SUB_STATUS['EXPIRES']])
            ->get();
        return $data;
    }
    /**
     * Get data process cancel order
     * @return object
     */
    public function getDataCancel()
    {
        $col = [
            'mst_order.receive_id',
            'mst_order.received_order_id',
            'mst_order.payment_method',
            'mst_order.mall_id',
            'mst_order.payment_status',
            'mst_order.order_status',
            'mst_order.order_sub_status',
            'mst_order.used_coupon',
            'mst_order.used_point',
            'mst_order.request_price',
            'mst_customer.email',
            'mst_signature.signature_content',
        ];
        $result = $this->select($col)
                        ->leftJoin(
                            'mst_order_detail',
                            'mst_order_detail.receive_id',
                            '=',
                            'mst_order.receive_id'
                        )
                        ->leftJoin(
                            'mst_customer',
                            'mst_customer.customer_id',
                            '=',
                            'mst_order.customer_id'
                        )
                       ->leftJoin('mst_signature', 'mst_order.mall_id', '=', 'mst_signature.mall_id')
                       ->whereIn('mst_order.order_sub_status', [ORDER_SUB_STATUS['OUT_OF_STOCK'],
                           ORDER_SUB_STATUS['STOP_SALE']])
                       ->groupBy('mst_order.receive_id')
                       ->get();
        return $result;
    }
    /**
     * Get order detail bat mail queue
     * @return string $receivedOrderId
     * @return string $para
     */
    public function getOrderDetailMailQueue($receivedOrderId, $para = '')
    {
        $col = [
            'mst_order.received_order_id',
            'mcod.last_name as ship_last_name',
            'mcod.first_name as ship_first_name',
            'mcod.zip_code as ship_zip_code',
            'mcod.prefecture as ship_prefecture',
            'mcod.city as ship_city',
            'mcod.sub_address as ship_sub_address',
            'mcod.tel_num as ship_tel_num',
            'od.product_code',
            'od.product_name',
            'od.receiver_id',
            // 'od.price',
            // 'od.quantity',
            'dto.delivery_type',
            'dto.suplier_id'
        ];
        $subPrice = 'CASE WHEN dto.product_status = 15 THEN 0 ELSE od.price END as price';
        $subQuantity = 'CASE WHEN dto.product_status = 15 THEN 0 ELSE od.quantity END as quantity';
        $query = $this->select($col)
                       ->selectRaw($subPrice)
                       ->selectRaw($subQuantity)
                       ->join('mst_order_detail as od', 'mst_order.receive_id', '=', 'od.receive_id')
                       ->join('mst_customer as mcod', 'od.receiver_id', '=', 'mcod.customer_id')
                       ->leftjoin('dt_order_product_detail as dto', function ($join) {
                          $join->on('dto.receive_id', '=', 'od.receive_id')
                              ->on('dto.detail_line_num', '=', 'od.detail_line_num');
                       })
                       ->where('mst_order.received_order_id', '=', $receivedOrderId);
        if ($para === '[###送付先情報欠品・廃番]') {
            $query->addSelect('mst_product_status.status_name')
                   ->leftJoin('mst_product_status', 'dto.product_status', '=', 'mst_product_status.product_status_id')
                   ->groupBy('od.detail_line_num');
        }
        if ($para === '[###送付先情報発送日案内]') {
            $query->addSelect('mst_order.delivery_plan_date')
                ->addSelect('opd.delivery_date')
                  ->leftjoin('dt_delivery', function ($join) {
                      $join->on('mst_order.received_order_id', '=', 'dt_delivery.received_order_id')
                          ->on('dto.sub_line_num', '=', 'dt_delivery.detail_line_num');
                  })
                  ->join('dt_order_product_detail as opd', function ($join) {
                     $join->on('opd.receive_id', '=', 'od.receive_id')
                         ->on('opd.detail_line_num', '=', 'od.detail_line_num');
                  })
                  ->orderBy('opd.delivery_date')
                  ->groupBy('od.detail_line_num');
        }
        $result = $query->get();
        return $result;
    }

    /**
    * Get data process receive result step 5
    * @return object $result
    */
    public function getDataReceiveResult5()
    {
        $col = [
            'opd.delivery_date',
            'opd.detail_line_num',
            'opd.sub_line_num',
            'mst_order.receive_id',
            'mst_order.ship_wish_date',
        ];
        $sub = '(select deli_days from mst_postal where postal_code = mst_customer.zip_code limit 1) AS deli_days';
        $result = $this->select($col)
                       ->selectRaw($sub)
                       ->join('mst_order_detail', 'mst_order.receive_id', '=', 'mst_order_detail.receive_id')
                       ->join('dt_order_product_detail AS opd', function ($join) {
                                $join->on('opd.receive_id', '=', 'mst_order_detail.receive_id')
                                     ->on('opd.detail_line_num', '=', 'mst_order_detail.detail_line_num');
                       })
                       ->join('mst_customer', 'mst_customer.customer_id', '=', 'mst_order_detail.receiver_id')
                       ->where('mst_order.order_status', ORDER_STATUS['ARRIVAL'])
                       ->where('mst_order.order_sub_status', '<', ORDER_SUB_STATUS['DONE'])
                       ->where('opd.stock_status', 1)
                       ->where('opd.product_status', PRODUCT_STATUS['WAIT_TO_SHIP'])
                       ->where('mst_order.is_delay', 0)
                       ->whereNull('opd.delivery_date')
                       ->orderBy('mst_order.receive_id')
                       ->get();
        return $result;
    }

    /**
    * Calculate deliveru date
    * @param array $param
    * @return object $result
    */
    public function calculateDeliveryDate($param)
    {
        $deliveryDate = null;
        $plus         = 0;
        if (!in_array($param->supplier_id, [32, 41, 62, 138, 171, 282, 367, 395])) {
            $plus = 1;
        }
        if (!empty($param->ship_wish_date)) {
            $deliDays = (int)$param->deli_days;
            $cal = strtotime($param->ship_wish_date . '-' . $deliDays . ' days');
            $now = strtotime('+' . $plus . ' days');
            if ($cal >= $now) {
                $deliveryDate = date('Y-m-d', $cal);
            } else {
                $shopAnswer = '▼お届け日のご希望をお伺いしておりましたが、'.
                '本メールに記載しております発送日が最短でございます。'.
                '恐れ入りますが商品到着までもうしばらくお待ちくださいませ。';
                $this->updateData([$param->receive_id], ['shop_answer' => $shopAnswer]);
                $deliveryDate =  date('Y-m-d', strtotime('+' . $plus . ' days'));
            }
        } else {
            $hour = date('H:i');
            if ($hour < '15:00' && $plus === 0) {
                $deliveryDate = date('Y-m-d');
            } else {
                $deliveryDate = date('Y-m-d', strtotime("+1 days"));
            }
        }
        return $deliveryDate;
    }

    /**
     * The get data process order edi result step 2.
     *
     * @return object $result
     */
    public function getDataEdiOrderResult2()
    {
        $col = [
            'mst_order.receive_id',
            'opd.product_status',
        ];
        $result = $this->select($col)
                      ->join('dt_order_product_detail as opd', 'opd.receive_id', '=', 'mst_order.receive_id')
                      ->where('order_status', '=', ORDER_STATUS['ORDER'])
                      ->where('order_sub_status', '<', ORDER_SUB_STATUS['DONE'])
                      ->where('mst_order.is_delay', 0)
                      ->whereIn(
                          'opd.product_status',
                          [
                              PRODUCT_STATUS['OUT_OF_STOCK'],
                              PRODUCT_STATUS['SALE_STOP']
                          ]
                      )
                      ->get();
        return $result;
    }
    /**
     * Get data process delivery result content 4
     *
     * @return object $result
     */
    public function getDataDeliveryResult4()
    {
        $col = [
            'ots.order_code',
            'ots.edi_order_code',
            'opd.product_code',
            'opd.price_invoice',
            'opd.received_order_num',
            'mst_order.received_order_id',
        ];
        $result = $this->select($col)
                       ->join('dt_order_product_detail AS opd', 'mst_order.receive_id', '=', 'opd.receive_id')
                       ->join('dt_order_to_supplier AS ots', 'opd.order_code', '=', 'ots.order_code')
                       ->whereIn('mst_order.order_status', [ORDER_STATUS['DELIVERY'], ORDER_STATUS['SETTLEMENT']])
                       ->where('mst_order.is_delay', 0)
                       ->where('opd.delivery_type', 2)
                       ->whereIn('ots.process_status', [2, 5])
                       ->where('ots.is_cancel', 0)
                       ->whereIn('opd.product_status', [PRODUCT_STATUS['DIRECTLY_SHIPPING'], PRODUCT_STATUS['SHIPPED']])
                       ->whereDate('opd.delivery_date', '<=', date('Y-m-d'))
                       ->get();
        return $result;
    }

    /**
     * Get data process after edi result
     * @return object $result
     */
    public function getDataAfterEdiResult()
    {
        $col = [
            'mst_order.receive_id',
            'product_status',
        ];
        return $this->select($col)
                    ->join('dt_order_product_detail AS opd', 'mst_order.receive_id', '=', 'opd.receive_id')
                    ->where('order_status', ORDER_STATUS['ORDER'])
                    ->where('order_sub_status', ORDER_SUB_STATUS['NEW'])
                    ->get();
    }

    /**
     * Get data process mail urgent
     * @return object $result
     */
    public function getDataMailUrgent()
    {
        $col = [
            'mst_order.receive_id',
            'mst_order.mall_id',
            'mst_order.payment_method',
            'mst_order.received_order_id',
            'mst_order.urgent_mail_id',
            'mt.mail_subject',
            'mt.mail_content',
            'mt.attached_file_path',
            'mc.email',
            'mst_signature.signature_content',
        ];
        $result = $this->select($col)
                       ->join('mst_mail_template as mt', 'mst_order.urgent_mail_id', '=', 'mt.mail_id')
                       ->join('mst_customer as mc', 'mst_order.customer_id', '=', 'mc.customer_id')
                       ->leftJoin('mst_signature', 'mst_order.mall_id', '=', 'mst_signature.mall_id')
                       ->where(function ($query) {
                         $query->where(function ($sub) {
                            $sub->where('mst_order.is_send_mail_urgent', 1)
                                ->where('mst_order.is_delay', 0);
                         })
                           ->orWhere(function ($sub) {
                               $sub->where('mst_order.is_send_mail_urgent', 1)
                               ->where('mst_order.urgent_mail_id', 38);
                           });
                       })
                       ->get();
        return $result;
    }

    /**
     * Get data process mail urgent
     * @return object $result
     */
    public function getDataMailUrgentEdiResult()
    {
        $col = [
            'mst_order.receive_id',
            'mst_order.delivery_plan_date',
            'mst_order.mall_id',
        ];
        $result = $this->select($col)
                       ->selectRaw('MAX(delivery_date) AS max_delivery_date')
                       ->join('dt_order_product_detail', 'mst_order.receive_id', '=', 'dt_order_product_detail.receive_id')
                       ->where('mst_order.order_status', ORDER_STATUS['ORDER'])
                       ->where('mst_order.order_sub_status', ORDER_SUB_STATUS['DOING'])
                       ->groupBy('mst_order.receive_id')
                       ->get();
        return $result;
    }

    /**
     * Get data process update shipment date
     * @return object $result
     */
    public function getDataReceiveResult6()
    {
        $col = [
            'mst_order.receive_id',
            'mst_order.ship_wish_date'
        ];
        $result = $this->select($col)
                       ->selectRaw('MAX(delivery_date) AS max_delivery_date')
                       ->join('dt_order_product_detail', 'mst_order.receive_id', '=', 'dt_order_product_detail.receive_id')
                       ->where('mst_order.order_status', ORDER_STATUS['ARRIVAL'])
                       ->where('mst_order.order_sub_status', ORDER_SUB_STATUS['DONE'])
                       ->groupBy('mst_order.receive_id')
                       ->get();
        return $result;
    }

    /**
     * Get data process is can delivery check
     * @return object $result
     */
    public function getDataProcessIsCanDeliveryCheck()
    {
        $col = [
            'mst_order.receive_id',
        ];
        $result = $this->select($col)
                       ->where('mall_id', '<>', 3)
                       ->where('is_send_mail_urgent', 0)
                       ->where(function ($query) {
                            $query->whereNotNull('delivery_plan_date')
                            ->whereDate('delivery_plan_date', date('Y-m-d'));
                       })
                      ->where(function ($query) {
                         $query->where(function ($sub) {
                               $sub->whereIn('order_status', [ORDER_STATUS['ORDER'], ORDER_STATUS['ARRIVAL']])
                                   ->whereIn('order_sub_status', [ORDER_SUB_STATUS['NEW'], ORDER_SUB_STATUS['DOING'], ORDER_SUB_STATUS['DONE']]);
                         })
                           ->orWhere(function ($sub) {
                               $sub->where('order_status', ORDER_STATUS['DELIVERY'])
                                   ->whereIn('order_sub_status', [ORDER_SUB_STATUS['NEW'], ORDER_SUB_STATUS['DOING']])
                                   ->whereRaw('DATE(delivery_plan_date) < DATE(shipment_date)');
                           });
                      })
                      ->get();
        return $result;
    }

    /**
     * Get data process delivery result content 6
     *
     * @return object $result
     */
    public function getDataDeliveryResult6()
    {
        $col = [
            'mst_order.received_order_id',
            'dt_order_to_supplier.order_code',
            'dt_order_to_supplier.edi_order_code',
            'dt_order_to_supplier.supplier_id',
            //'dt_order_product_detail.product_code',
            'dt_order_product_detail.received_order_num',
            't_order_direct.price',
        ];
        $sub_pro = 'IFNULL(dt_order_product_detail.child_product_code, dt_order_product_detail.product_code) AS product_code';
        $result = $this->select($col)
                       ->selectRaw($sub_pro)
                       ->join('dt_order_product_detail', 'dt_order_product_detail.receive_id', '=', 'mst_order.receive_id')
                       ->join('dt_order_to_supplier', 'dt_order_to_supplier.order_code', '=', 'dt_order_product_detail.order_code')
                       ->join('edi.t_order_direct', 'dt_order_to_supplier.edi_order_code', '=', 't_order_direct.order_code')
                       ->whereIn('mst_order.order_status', [ORDER_STATUS['DELIVERY'], ORDER_STATUS['SETTLEMENT']])
                       ->where('mst_order.is_delay', 0)
                       ->where('dt_order_product_detail.delivery_type', 2)
                       ->whereIn('dt_order_to_supplier.process_status', [2, 5])
                       ->where('dt_order_to_supplier.is_cancel', 0)
                       ->whereIn('dt_order_product_detail.product_status', [PRODUCT_STATUS['DIRECTLY_SHIPPING'], PRODUCT_STATUS['SHIPPED']])
                       ->whereDate('dt_order_product_detail.delivery_date', '<=', date('Y-m-d'))
                       ->get();
        return $result;
    }

    /**
     * Get data process call API confirm order for rakuten pay
     *
     * @return object $result
     */
    public function getDataProcessConfirmRakPay()
    {
        $result = $this->select(['receive_id', 'received_order_id', 'order_sub_status'])
                       ->where('order_status', ORDER_STATUS['ESTIMATION'])
                       ->whereIn('order_sub_status', [ORDER_SUB_STATUS['DOING'], ORDER_SUB_STATUS['RAKUTEN_PAY']])
                       ->where('mall_id', 9)
                       ->get();
        return $result;
    }

    /**
     * Get data process check status Rakuten Pay
     *
     * @return object $result
     */
    public function getDataCheckStatusRaKPay()
    {
        $result = $this->select(['received_order_id'])
                       ->where('order_status', ORDER_STATUS['DELIVERY'])
                       ->where('order_sub_status', ORDER_SUB_STATUS['RAKUTEN_PAY'])
                       ->where('is_delay', 0)
                       ->where('mall_id', 9)
                       ->get();
        return $result;
    }

    /**
     * Get data process check status Rakuten Pay(batch payment confirm)
     *
     * @return object $result
     */
    public function getDataPaymentComfirm3()
    {
        $result = $this->select(['received_order_id'])
                       ->where('order_status', ORDER_STATUS['PAYMENT_CONFIRM'])
                       ->whereIn('order_sub_status', [ORDER_SUB_STATUS['NEW'], ORDER_SUB_STATUS['DELAY']])
                       ->where('is_delay', 0)
                       ->where('mall_id', 9)
                       ->get();
        return $result;
    }

    /**
     * Get data process check status Rakuten Pay(batch payment confirm)
     *
     * @return object $result
     */
    public function getPaymentMethod($receiveOrderId, $mallId = null)
    {
        $result = $this->select(['index', 'payment.payment_name', 'payment.payment_code'])
                       ->join('mst_payment_method as payment', function ($join) {
                            $join->on('mst_order.payment_method', '=', 'payment.payment_code')
                                 ->on('mst_order.mall_id', '=', 'payment.mall_id');
                       })
                       ->where('mst_order.received_order_id', '=', $receiveOrderId)
                       ->where('mst_order.mall_id', $mallId)
                       ->first();
        return $result;
    }

    /**
     * Get order api
     *
     * @param  int    $mallId
     * @param  int    $limit
     * @return object
     */
    public function getOrderAPIWebMapping($mallId, $limit = null, $paymentMethod = null)
    {
        $data = $this->select('mst_order.received_order_id', 'mst_order.order_date');
        if ($mallId === 9) {
            $data->leftJoin('dt_web_rak_pay_data', 'dt_web_rak_pay_data.order_number', '=', 'mst_order.received_order_id')
                ->whereNull('dt_web_rak_pay_data.order_number');
        } elseif ($mallId === 1) {
            $data->leftJoin('dt_web_rak_data', 'dt_web_rak_data.order_number', '=', 'mst_order.received_order_id')
                ->whereNull('dt_web_rak_data.order_number');
        } elseif ($mallId === 2) {
            $data->leftJoin('dt_web_yah_data', 'dt_web_yah_data.order_id', '=', 'mst_order.received_order_id')
                ->whereNull('dt_web_yah_data.order_id');
        } elseif ($mallId === 3) {
            $data->leftJoin('dt_web_amazon_data', 'dt_web_amazon_data.order_number', '=', 'mst_order.received_order_id')
                ->whereNull('dt_web_amazon_data.order_number');
        } elseif ($mallId === 10) {
            $data->leftJoin('dt_web_wow_data', 'dt_web_wow_data.order_number', '=', 'mst_order.received_order_id')
                ->whereNull('dt_web_wow_data.order_number');
        }
        $data->where(function ($sub) {
                $sub->where(function ($sub1) {
                        $sub1->where('mst_order.order_status', ORDER_STATUS['SETTLEMENT'])
                            ->where('mst_order.order_sub_status', ORDER_SUB_STATUS['DOING'])
                            ->whereRaw("DATE_ADD(mst_order.shipment_date, INTERVAL 1 DAY) = CURDATE()");
                })
                ->orWhere(function ($sub2) {
                    $sub2->where('mst_order.order_status', ORDER_STATUS['CANCEL'])
                        ->where('mst_order.order_sub_status', ORDER_SUB_STATUS['DONE']);
                });
        })
        ->where('mst_order.mall_id', $mallId);
        if ($paymentMethod !== null) {
            $data->where('mst_order.payment_method', $paymentMethod);
        } else {
            $data->where('mst_order.payment_method', '<>', 7);
        }
        $data->where('mst_order.received_order_id', 'not regexp', 're');
        if ($limit !== null) {
            $data->limit($limit);
        }
        return $data->get();
    }
     /**
     * Get order api rakuten pay
     *
     * @param  int $mallId
     * @return object
     */
    public function getOrderPayCorrect($mallId)
    {
        $column = [
          'mst_order.received_order_id',
          'mst_order.order_date',
          'mst_order.order_status',
          'dt_web_mapping.web_order_status'
        ];
        $data = $this->select($column)
                      ->join('dt_web_mapping', function ($join) {
                          $join->on('dt_web_mapping.received_order_id', '=', 'mst_order.received_order_id')
                              ->where(function ($subWhere) {
                                     $subWhere->where('dt_web_mapping.web_order_status', '<>', 'mst_order.order_status')
                                     ->orWhere('dt_web_mapping.web_request_price', '<>', 'mst_order.request_price');
                              });
                      })
                      ->where('dt_web_mapping.is_corrected', '=', 0)
                      ->where('dt_web_mapping.mall_id', '=', $mallId)
                      ->where(function ($subWhere) {
                        $subWhere->where('dt_web_mapping.different_type', '=', 'ステータス')
                                 ->orWhere('mst_order.order_status', '=', 10);
                      })
                      ->get();
        return $data;
    }

     /**
     * count order
     *
     * @param  string $orderNumber
     * @param  int    $orderStatus
     * @return object
     */
    public function checkOrderMapping($orderNumber, $orderStatus)
    {
        $count = $this->where('received_order_id', $orderNumber)
             ->where('order_status', $orderStatus)
             ->count();
        return $count;
    }
    /**
     * Get order api for amazon
     *
     * @param  int    $mallId
     * @param  int    $limit
     * @param  int    $payment method
     * @return object
     */
    public function getOrderAPIWebMappingAmazon($mallId, $limit = null, $paymentMethod = null)
    {
        $data = $this->select('mst_order.received_order_id', 'mst_order.order_date')
                     ->leftJoin('dt_web_amazon_data', 'dt_web_amazon_data.order_number', '=', 'mst_order.received_order_id')
                     ->whereNull('dt_web_amazon_data.order_number');
        $data->where(function ($sub) {
                $sub->where(function ($sub1) {
                        $sub1->where('mst_order.order_status', ORDER_STATUS['SETTLEMENT'])
                             ->where('mst_order.order_sub_status', ORDER_SUB_STATUS['DOING'])
                             ->whereRaw("DATE_ADD(mst_order.shipment_date, INTERVAL 1 DAY) = CURDATE()");
                })
                ->orWhere(function ($sub2) {
                    $sub2->where('mst_order.order_status', ORDER_STATUS['CANCEL'])
                         ->where('mst_order.order_sub_status', ORDER_SUB_STATUS['DONE'])
                         ->whereRaw("DATE_ADD(DATE_FORMAT(mst_order.up_date, '%Y-%m-%d'), INTERVAL 5 DAY) = CURDATE()");
                });
        })
        ->where('mst_order.mall_id', $mallId);
        if ($paymentMethod !== null) {
            $data->where('mst_order.payment_method', $paymentMethod);
        } else {
            $data->where('mst_order.payment_method', '<>', 7);
        }
        $data->where('mst_order.received_order_id', 'not regexp', 're');
        if ($limit !== null) {
            $data->limit($limit);
        }
        return $data->get();
    }

    /**
     * Get data process change wowma status
     *
     * @return object
     */
    public function getDataProcessChangeWowmaStatus()
    {
        $cols = [
            'receive_id', 'received_order_id', 'order_status', 'order_sub_status', 'payment_status', 'payment_method'
        ];
        $data = $this->select($cols)
            ->where('order_status', ORDER_STATUS['ESTIMATION'])
            ->whereIn('order_sub_status', [ORDER_SUB_STATUS['DOING'], ORDER_SUB_STATUS['WOWMA_PAY']])
            ->where('mall_id', 10)
            ->get();
        return $data;
    }

    /**
     * Get data payment confirm wowma
     *
     * @return object
     */
    public function getDataPaymentComfirmWowma()
    {
        $data = $this->select('receive_id', 'received_order_id', 'order_status', 'order_sub_status', 'payment_status')
            ->where('order_status', ORDER_STATUS['PAYMENT_CONFIRM'])
            ->whereIn('order_sub_status', [ORDER_SUB_STATUS['NEW'], ORDER_SUB_STATUS['DELAY']])
            ->where('is_delay', 0)
            ->where('mall_id', 10)
            ->where('payment_status', 0)
            ->get();
        return $data;
    }

    /**
     * Get data process Wowma glsc
     *
     * @return object
     */
    public function getDataWowmaGlsc()
    {
        $cols = [
            'receive_id', 'received_order_id', 'order_status', 'order_sub_status', 'payment_status', 'payment_method'
        ];
        $data = $this->select($cols)
            ->where('order_status', ORDER_STATUS['DELIVERY'])
            ->where('order_sub_status', ORDER_SUB_STATUS['WOWMA_PAY'])
            ->where('is_delay', 0)
            ->where('mall_id', 10)
            ->get();
        return $data;
    }

    /**
     * Get data process Delay by is_on_hold
     *
     * @return object
     */
    public function getDataDelayByHold()
    {
        $result = $this->select('mst_order.receive_id')
                       ->join('mst_order_detail as od', 'mst_order.receive_id', '=', 'od.receive_id')
                       ->join('mst_product as mp', 'od.product_code', '=', 'mp.product_code')
                       ->where('mst_order.order_status', ORDER_STATUS['NEW'])
                       ->where('mst_order.order_sub_status', '<>', ORDER_SUB_STATUS['ERROR'])
                       ->where('mst_order.is_on_hold', 1)
                       ->groupBy('mst_order.receive_id')
                       ->get();
        return $result;
    }

    /**
     * Get data process delivery_instrustions
     *
     * @return object
     */
    public function getDataInstrustions()
    {
        $result = $this->select([
                         'mst_order.receive_id',
                         'mp.ship_intrustion',
                         'mst_order.delivery_instrustions'
                       ])
                       ->join('mst_order_detail as od', 'mst_order.receive_id', '=', 'od.receive_id')
                       ->join('mst_product as mp', 'od.product_code', '=', 'mp.product_code')
                       ->where('mst_order.order_status', ORDER_STATUS['NEW'])
                       ->where('mst_order.order_sub_status', '<>', ORDER_SUB_STATUS['ERROR'])
                       ->whereNotNull('mp.ship_intrustion')
                       ->get();
        return $result;
    }

    /**
     * Get repare data uri mapping for first content
     *
     * @return collection
     */
    public function getRepareDataUriMappingFinishNew()
    {
        $data = $this->select('mst_order.receive_id')
            ->join('dt_web_mapping', 'dt_web_mapping.received_order_id', '=', 'mst_order.received_order_id')
            ->where('mst_order.order_status', ORDER_STATUS['SETTLEMENT'])
            ->where('mst_order.order_sub_status', ORDER_SUB_STATUS['DOING'])
            ->where('dt_web_mapping.is_corrected', 1)
            ->whereRaw("DATE_FORMAT(mst_order.shipment_date, '%Y-%m-%d') >= '2019-01-01'")
            ->get();
        return $data;
    }

    /**
     * Get repare data uri mapping for second content
     *
     * @return collection
     */
    public function getRepareDataUriMappingFinishDoing()
    {
        $data = $this->select(
            'mst_order.receive_id',
            'mst_order.received_order_id',
            'mst_order.order_date',
            'mst_order.order_status',
            'mst_order.mall_id',
            'mst_order.payment_method',
            'mst_order.used_coupon',
            'mst_order.used_point',
            'mst_order.goods_price',
            'mst_order.discount',
            'mst_order.pay_after_charge',
            'mst_order.request_price',
            'mst_order.ship_charge',
            'mst_order.pay_charge',
            'mst_order.pay_charge_discount',
            'mst_order.shipment_date',
            'dt_delivery.inquiry_no',
            'dt_delivery.delivery_code'
        );
        $data = $data->addSelect(DB::raw(
            '(CASE WHEN dt_delivery_detail.delivery_real_date IS NULL THEN mst_order.shipment_date '
            . 'ELSE DATE(MAX(dt_delivery_detail.delivery_real_date)) END) AS delivery_date'
        ))
        ->addSelect(DB::raw('SUM(IFNULL(dt_repayment.return_coupon, 0)) AS total_return_coupon'))
        ->addSelect(DB::raw('SUM(IFNULL(dt_repayment.return_point, 0)) AS total_return_point'))
        ->addSelect(DB::raw('SUM(IFNULL(dt_repayment.total_return_price, 0)) AS total_return_price'))
        ->leftJoin('dt_repayment', 'dt_repayment.receive_id', '=', 'mst_order.receive_id')
        ->leftJoin('dt_delivery', 'dt_delivery.received_order_id', '=', 'mst_order.received_order_id')
        ->leftJoin('dt_delivery_detail', function ($join) {
            $join->on('dt_delivery_detail.received_order_id', '=', 'dt_delivery.received_order_id')
                ->on('dt_delivery_detail.subdivision_num', '=', 'dt_delivery.subdivision_num');
        })
        ->where('mst_order.order_status', ORDER_STATUS['FINISHED'])
        ->where('mst_order.order_sub_status', ORDER_SUB_STATUS['NEW'])
        ->whereNotNull('mst_order.shipment_date')
        ->groupBy('mst_order.received_order_id')
        ->get();
        return $data;
    }
    /**
     * update data to process mail urgent
     *
     * @return void
     */
    public function updateReviewMailUrgent()
    {
        $data = $this
            ->whereRaw("DATE_ADD(shipment_date, INTERVAL 7 DAY) = CURDATE()")
            ->whereIn('mall_id', [2,9])
            ->where('is_claim', 0)
            ->whereIn('order_status', [ORDER_STATUS['DELIVERY'], ORDER_STATUS['SETTLEMENT'], ORDER_STATUS['FINISHED']])
            ->update(['is_send_mail_urgent' => 1, 'urgent_mail_id' => 851]);
        return $data;
    }

    /**
     * Get dât process validate
     *
     * @return void
     */
    public function getDataProcessValidate()
    {
        $col = [
            'mst_order.mall_id',
            'mst_order.payment_method',
            'mst_order.receive_id',
        ];
        $result = $this->select($col)
                       ->where('is_recalculate', 1)
                       ->where('order_sub_status', ORDER_SUB_STATUS['ERROR_FEE_TRANSPORT'])
                       ->get();
        return $result;
    }
}
