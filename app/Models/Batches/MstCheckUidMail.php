<?php
/**
 * Model for mst_check_uid_mail table.
 *
 * @package    App\Models\Batches
 * @subpackage MstCheckUidMail
 * @copyright  Copyright (c) 2018 RiverCrane! Corporation. All Rights Reserved.
 * @author     Van Qui<van.qui@rivercrane.vn>
 */

namespace App\Models\Batches;

use Illuminate\Database\Eloquent\Model;

class MstCheckUidMail extends Model
{
    public $timestamps = false;

    /**
    * The database table used by the model.
    * @var string
    */
    protected $table = 'mst_check_uid_mail';

    /**
    * The database is used by the model.
    * @var string
    */
    protected $connection = 'horunba';
}
