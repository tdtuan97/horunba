<?php
/**
 * Model for dt_delivery table.
 *
 * @package    App\Models\Batches
 * @subpackage DtDeliveryDetail
 * @copyright  Copyright (c) 2017 RiverCrane! Corporation. All Rights Reserved.
 * @author     Truong Nghia<truong.van.nghia@rivercrane.vn>
 */

namespace App\Models\Batches;

use Illuminate\Database\Eloquent\Model;
use DB;
use App\Custom\DBExtend;

class DtDeliveryDetail extends Model
{
    use DBExtend;
    public $timestamps = false;

    /**
    * The database table used by the model.
    * @var string
    */
    protected $table = 'dt_delivery_detail';

    /**
    * The database is used by the model.
    * @var string
    */
    protected $connection = 'horunba';

    /**
    * The update infomation.
    * @param array $key    condition
    * @param array $value  data update
    * @return object $result
    */
    public function updateData($key, $value)
    {
        return $this->where($key)->update($value);
    }

    /**
     * Count data check detail.
     * @param  string $receivedOrderId
     * @param  string $subdivisionNum
     * @return object
     */
    public function countDetailByKey($receivedOrderId, $subdivisionNum)
    {
        return $this->where('received_order_id', $receivedOrderId)
                    ->where('subdivision_num', $subdivisionNum)
                    ->count();
    }
}
