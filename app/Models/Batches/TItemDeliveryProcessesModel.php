<?php

/**
 * Model for tItemDeliveryProcessesModel table.
 *
 * @package    App\Models\Batches
 * @subpackage TItemDeliveryProcessesModel
 * @copyright  Copyright (c) 2018 RiverCrane! Corporation. All Rights Reserved.
 * @author     Van Qui<van.qui@rivercrane.vn>
 */

namespace App\Models\Batches;

use Illuminate\Database\Eloquent\Model;
use DB;

class TItemDeliveryProcessesModel extends Model
{
    public $timestamps = false;

    /**
    * The database table used by the model.
    * @var string
    */
    protected $table = 'tItemDeliveryProcessesModel';

    /**
    * The database is used by the model.
    * @var string
    */
    protected $connection = 'glsc';

    /**
     * Get data Nanko
     *
     * @param  int    $limit
     * @return object
     */
    public function getDataGlsc($limit = null)
    {
        $strLimit = "";
        if ($limit !== null) {
            $strLimit = "LIMIT $limit";
        }
        $sql = "
            SELECT
                stock_status.product_code,
                stock_status.glsc_num,
                IFNULL(tdelivery.Stock, 0) AS new_glsc_num
            FROM
                glsc.tItemDeliveryProcessesModel AS tdelivery
            RIGHT JOIN
                hrnb.mst_stock_status AS stock_status
            ON
                tdelivery.ItemCd = stock_status.product_code
            WHERE
                stock_status.glsc_num <> IFNULL(tdelivery.Stock, 0)
            $strLimit
        ";

        $data = DB::select(DB::raw($sql));
        return $data;
    }
}
