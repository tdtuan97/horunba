<?php

/**
 * Model for mst_order_detail table.
 *
 * @package    App\Models\Batches
 * @subpackage MstOrderDetail
 * @copyright  Copyright (c) 2017 RiverCrane! Corporation. All Rights Reserved.
 * @author     Truong Nghia<truong.van.nghia@rivercrane.vn>
 */

namespace App\Models\Batches;

use Illuminate\Database\Eloquent\Model;

class MstOrderDetail extends Model
{
    public $timestamps = false;

    /**
    * The database table used by the model.
    * @var string
    */
    protected $table = 'mst_order_detail';

    /**
    * The database is used by the model.
    * @var string
    */
    protected $connection = 'horunba';
    protected $primaryKey = 'receive_id';
    /**
    * Get product code by receive id.
    * @param string $receiveId
    * @return object
    */
    public function getProductCodeByReceiveId($receiveId)
    {
        $result = $this->select('product_code')
                       ->where('receive_id', $receiveId)
                       ->get();
        return $result;
    }

    /**
     * Update information
     *
     * @param string $key       condition
     * @param array $arrUpdate  data update
     *
     * @return object boolean
     */
    public function updateData($key, $arrUpdate)
    {
        return $this->where($key)
                    ->update($arrUpdate);
    }

    /**
     * Update data by array receive id
     *
     * @param  array   $arrReceiveId
     * @param  array   $arrUpdate
     * @return boolean
     */
    public function updateDataByReceiveIds($arrReceiveId, $arrUpdate)
    {
        return $this->whereIn('receive_id', $arrReceiveId)
                    ->update($arrUpdate);
    }

    /**
    * Get data check validate
    * @param string $receiveId
    * @return object $result
    */
    public function getListReceiver($receiveId)
    {
        return $this->select('receiver_id')
                    ->where('receive_id', $receiveId)
                    ->distinct()
                    ->get();
    }
}
