<?php

/**
 * Exe sql for hrnb.dt_order_update_log
 *
 * @package    App\Models
 * @subpackage DtOrderUpdateLog
 * @copyright  Copyright (c) 2018 RiverCrane! Corporation. All Rights Reserved.
 * @author     van qui<van.qui.rcvn2012@gmail.com>
 */

namespace App\Models\Batches;

use Illuminate\Database\Eloquent\Model;
use DB;

class DtOrderUpdateLog extends Model
{
    public $timestamps = false;
    /**
    * The database table used by the model.
    * @var string
    */
    protected $table = 'dt_order_update_log';
    /**
    * The database is used by the model.
    * @var string
    */
    protected $connection = 'horunba';

    /**
    * The primary key.
    * @var string
    */
    protected $primaryKey = 'log_id';
}
