<?php

/**
 * Model for dt_repayment table.
 *
 * @package    App\Models\Batches
 * @subpackage DtRePayment
 * @copyright  Copyright (c) 2017 RiverCrane! Corporation. All Rights Reserved.
 * @author     Truong Nghia<truong.van.nghia@rivercrane.vn>
 */

namespace App\Models\Batches;

use Illuminate\Database\Eloquent\Model;

class DtRePayment extends Model
{
    public $timestamps = false;

    /**
    * The database table used by the model.
    * @var string
    */
    protected $table = 'dt_repayment';

    /**
    * The database is used by the model.
    * @var string
    */
    protected $connection = 'horunba';
}
