<?php

/**
 * Model for mst_product table.
 *
 * @package    App\Models\Batches
 * @subpackage MstProduct
 * @copyright  Copyright (c) 2017 RiverCrane! Corporation. All Rights Reserved.
 * @author     Truong Nghia<truong.van.nghia@rivercrane.vn>
 */

namespace App\Models\Batches;

use Illuminate\Database\Eloquent\Model;

class MstProduct extends Model
{
    public $timestamps = false;

    /**
    * The database table used by the model.
    * @var string
    */
    protected $table = 'mst_product';

    /**
    * The database is used by the model.
    * @var string
    */
    protected $connection = 'horunba';

    /**
    * The get product by product code.
    * @param string $productCode product code
    * @return object $result
    */
    public function getDataByProductCode($productCode)
    {
        $result = $this->where('product_code', '=', $productCode)
                       ->first();
        return $result;
    }

    /**
    * The get data process stock order step 1.
    * @param array $productCode array product code want get
    * @return object $result
    */
    public function getDataStockOrder1()
    {
        $col = [
            'ss.product_code',
            'ss.nanko_num',
            'ss.order_zan_num',
            'ss.rep_orderring_num',
            'ss.return_num',
            'mst_product.stock_default',
            'mst_product.price_supplier_id',
            'mst_product.product_jan',
            'mst_product.price_invoice',
            'mst_product.rod_unit',
            'mst_product.stock_lower_limit',
        ];
        $result = $this->select($col)
                       ->join('mst_stock_status as ss', function ($join) {
                            $join->on('mst_product.product_code', '=', 'ss.product_code')
                                 ->where('product_service', '<>', 1)
                                 ->where('stock_lower_limit', '>', 0)
                                 ->where('product_daihiki', 0)
                                 ->whereRaw('(ss.nanko_num - ss.order_zan_num - ss.return_num + ss.rep_orderring_num) < mst_product.stock_lower_limit');
                       })
                       ->leftJoin('api.product_special', function ($join) {
                            $join->on('product_special.product_code', '=', 'ss.product_code')
                                 ->where('never_sale_again_flg', '<>', 1)
                                 ->where('sales_stop', '<>', 1)
                                 ->where('only_stock', '<>', 1);
                       })
                       ->join('edi.mst_product as che', function ($join) {
                            $join->on('che.product_code', '=', 'ss.product_code')
                                 ->where('che.is_live', '=', 1)
                                 ->whereNotIn('che.cheetah_status', [3, 4]);
                       })
                       ->get();
        return $result;
    }

    /**
    * The get data process stock order step 2.
    * @param array $productCode array product code want get
    * @return object $result
    */
    public function getDataStockOrder2()
    {
        $col = [
            'ss.product_code',
            'ss.replenish_num',
            'mst_product.price_supplier_id',
            'mst_product.product_jan',
            'mst_product.price_invoice',
        ];
        $result = $this->select($col)
                       ->join('mst_stock_status as ss', function ($join) {
                            $join->on('mst_product.product_code', '=', 'ss.product_code')
                                 ->where('stock_lower_limit', '>', 0);
                       })
                       ->join('edi.mst_product as emp', function ($join) {
                            $join->on('mst_product.product_code', '=', 'emp.product_code')
                                 ->where('emp.is_live', '=', 1);
                       })
                       ->where('ss.replenish_num', '>', 0)
                       ->get();
        return $result;
    }

    /**
    * Get data sync product
    * @return object
    */
    public function getDataSyncProduct()
    {
        $result = $this->select('mst_product.product_code')
                       ->leftJoin('mst_stock_status as ss', function ($join) {
                            $join->on('mst_product.product_code', '=', 'ss.product_code');
                       })
                       ->whereNull('ss.product_code')
                       ->get();
        return $result;
    }
}
