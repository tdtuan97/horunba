<?php

/**
 * Exe sql for hrnb.dt_web_pro_data
 *
 * @package    App\Models
 * @subpackage DtProWebData
 * @copyright  Copyright (c) 2018 RiverCrane! Corporation. All Rights Reserved.
 * @author     Truong Nghia<truong.van.nghia@rivercrane.vn>
 */

namespace App\Models\Batches;

use Illuminate\Database\Eloquent\Model;

class DtWebProData extends Model
{
    public $timestamps = false;
    /**
    * The database table used by the model.
    * @var string
    */
    protected $table = 'dt_web_pro_data';
    /**
    * The database is used by the model.
    * @var string
    */
    protected $connection = 'horunba';

    public function getDataCheck($arrId)
    {
        return $this->select('order_id')
                    ->whereIn('order_id', $arrId)
                    ->get();
    }

    /**
     * Get data process web B-dash Pro
     * @return object
     */
    public function getDataProcessWebBDashPro()
    {
        $col = [
            'dt_web_pro_data.order_id',
            'dt_web_pro_data.settlement',
            'dt_web_pro_data.process_status',
            'dt_web_pro_data.requestprice',
            'mst_order.mall_id',
            'mst_order.order_status',
            'mst_order.payment_method',
            'mst_order.request_price',
            'mst_order.order_status'
        ];
        $result = $this->select($col)
                       ->leftjoin('mst_order', 'dt_web_pro_data.order_id', '=', 'mst_order.received_order_id')
                       ->where('process_flg', 0)
                       ->where('is_delete', 0)
                       ->groupBy('dt_web_pro_data.order_id')
                       ->get();
        return $result;
    }
}
