<?php

/**
 * Model for dt_order_product_detail table.
 *
 * @package    App\Models\Batches
 * @subpackage DtOrderProductDetail
 * @copyright  Copyright (c) 2017 RiverCrane! Corporation. All Rights Reserved.
 * @author     Truong Nghia<truong.van.nghia@rivercrane.vn>
 */

namespace App\Models\Batches;

use App\Models\Batches\MstStockStatus;
use Illuminate\Database\Eloquent\Model;
use App\Models\Batches\LogUpdateStatusProduct;
use DB;

class DtOrderProductDetail extends Model
{
    public $timestamps = false;

    /**
    * The database table used by the model.
    * @var string
    */
    protected $table = 'dt_order_product_detail';

    /**
    * The database is used by the model.
    * @var string
    */
    protected $connection = 'horunba';

    /**
    * Get data to classify
    * @param string $type     field for operator join
    * @return object $result
    */
    public function getDataClassify($type)
    {
        $col = [
            'dt_order_product_detail.receive_id',
            'dt_order_product_detail.detail_line_num',
            'sub_line_num',
            'product_daihiki',
            'stock_default',
            'child_product_code',
            'stock_lower_limit',
            'od.specify_deli_type',
            'mst_order.ship_wish_time',
            'mst_order.ship_wish_date',
            'mst_order.shop_answer',
        ];
        $result = $this->select($col)
                       ->join('mst_product as mp', "dt_order_product_detail.$type", '=', 'mp.product_code')
                       ->join('mst_order_detail as od', function ($join) {
                            $join->on('od.receive_id', '=', 'dt_order_product_detail.receive_id')
                                 ->on('od.detail_line_num', '=', 'dt_order_product_detail.detail_line_num');
                       })
                       ->join('mst_order', 'mst_order.receive_id', '=', 'dt_order_product_detail.receive_id')
                       ->where('sub_process_status', '=', 0)
                       ->orderBy('receive_id')
                       ->orderBy('detail_line_num')
                       ->get();
        return $result;
    }

    /**
    * Update delivery_type and sub_process_status
    * @param string $receiveId   value receive_id need update
    * @param string $detailLine  value detail_line_num need update
    * @param string $subLineNum  value sub_line_num need update
    * @param string $type        value delivery_type need update
    * @return boolean $check or string $error
    */
    // public function updateDeliType($receiveId, $detailLine, $subLineNum, $type)
    // {
    //     $check = '';
    //     try {
    //         $check = $this->where('receive_id', '=', $receiveId)
    //                       ->where('detail_line_num', '=', $detailLine)
    //                       ->where('sub_line_num', '=', $subLineNum)
    //                       ->update(['delivery_type' => $type]);
    //     } catch (\Exception $e) {
    //         $error      = "------------------------------------------" . PHP_EOL;
    //         $className  = debug_backtrace(false, 2)[1]['class'];
    //         $arrClass   = explode('\\', $className);
    //         $error     .= end($arrClass) . PHP_EOL;
    //         $error     .= $e->getPrevious()->getMessage();
    //         $error     .= PHP_EOL . "------------------------------------------" . PHP_EOL;
    //         return $error;
    //     }
    //     return $check;
    // }

    /**
    * Get data process inventory
    * @param string $type      field for operator join
    * @return boolean $check or string $error
    */
    public function getDataProcessInventory()
    {
        $col = [
            'dt_order_product_detail.receive_id',
            'delivery_type',
            'detail_line_num',
            'sub_line_num',
            'received_order_num',
            'order_zan_num',
            'child_product_code',
            'nanko_num',
            'stock_num',
            'order_zan_num',
            //'mc.delivery_haiban',
            //'mc.delivery_keppin',
            'mss.return_num',
        ];
        $sub = 'IFNULL(dt_order_product_detail.child_product_code, dt_order_product_detail.product_code) AS product_code';
        $result = $this->select($col)
                       ->selectRaw($sub)
                       ->leftjoin(
                           'mst_stock_status as mss',
                           DB::raw('IFNULL(dt_order_product_detail.child_product_code, dt_order_product_detail.product_code)'),
                           '=',
                           'mss.product_code'
                       )
                       ->join('edi.mst_product as mc', function ($join) {
                            $join->on(DB::raw('IFNULL(dt_order_product_detail.child_product_code, dt_order_product_detail.product_code)'), '=', 'mc.product_code');
                            $join->where('mc.is_live', '=', 1);
                        })
                       ->join('mst_order', 'mst_order.receive_id', '=', 'dt_order_product_detail.receive_id')
                       ->where('sub_process_status', '=', 1)
                       ->whereIn('delivery_type', [1, 3])
                       ->where('product_status', 0)
                       ->where('order_sub_status', '<', 2)
                       ->orderBy('product_code')
                       ->orderBy('dt_order_product_detail.in_date')
                       ->get();
        return $result;
    }

    /**
    * Update information after process reservation
    * @param string $receiveId   value condition field receive_id
    * @param string $detailLine  value condition field detail_line_num
    * @param string $subLineNum  value condition field sub_line_num
    * @param array $dataUpdate
    * @return object $result
    */
    public function updateData($receiveId, $detailLine, $subLineNum, $dataUpdate)
    {
        if (array_key_exists('product_status', $dataUpdate)) {
            $modelLog  = new LogUpdateStatusProduct();
            $className = debug_backtrace(false, 2)[1]['class'];
            $modelLog->insertLog(
                [
                    'receive_id' => $receiveId,
                    'detail_line_num' => $detailLine,
                    'sub_line_num' => $subLineNum,
                ],
                $dataUpdate,
                $className
            );
        }
        $check = $this->where('receive_id', '=', $receiveId)
                      ->where('detail_line_num', '=', $detailLine)
                      ->where('sub_line_num', '=', $subLineNum)
                      ->update($dataUpdate);
        return $check;
    }

    /**
    * Get data process delivery
    * @return object $result
    */
    public function getDataProcessDelivery()
    {
        $col = [
            'mst_order.receive_id',
            'delivery_type',
            'stock_status',
            'suplier_id',
        ];
        $result = $this->select($col)
                        ->leftjoin('mst_order_detail', function ($join) {
                            $join->on('dt_order_product_detail.receive_id', '=', 'mst_order_detail.receive_id')
                            ->on('dt_order_product_detail.detail_line_num', '=', 'mst_order_detail.detail_line_num');
                        })
                       ->leftjoin('mst_order', 'mst_order.receive_id', '=', 'mst_order_detail.receive_id')
                       ->where('sub_process_status', '=', 1)
                       ->where('mst_order.order_sub_status', '<', ORDER_SUB_STATUS['DONE'])
                       ->orderBy('mst_order.receive_id')
                       ->orderBy('dt_order_product_detail.detail_line_num')
                       ->orderBy('delivery_type')
                       ->get();
        return $result;
    }

    /**
    * Update product status
    * @param string $receiveId   value condition field receive_id
    * @param int $productStatus  value need update field product_status
    * @return boolean
    */
    public function updateProductStatusByDeliType($receiveId, $productStatus)
    {
        $modelLog  = new LogUpdateStatusProduct();
        $className = debug_backtrace(false, 2)[1]['class'];
        $modelLog->insertLog(
            ['receive_id' => $receiveId],
            ['product_status' => $productStatus],
            $className . '(delivery_type <> 1)'
        );
        return $this->where('receive_id', '=', $receiveId)
                    ->where('delivery_type', '<>', 1)
                    ->whereIn('product_status', [0, 3])
                    ->update(['product_status' => $productStatus]);
    }

    /**
    * Update sub product status
    * @param array $arrUpdate  array data will update
    * @return boolean $check or string $error
    */
    public function updateDataByReceive($arrReceive, $arrUpdate)
    {
        if (array_key_exists('product_status', $arrUpdate)) {
            $modelLog  = new LogUpdateStatusProduct();
            $className = debug_backtrace(false, 2)[1]['class'];
            $modelLog->insertLog(
                ['receive_id' => $arrReceive],
                $arrUpdate,
                $className
            );
        }
        return $this->whereIn('receive_id', $arrReceive)->update($arrUpdate);
    }

    /**
    * Get data process order zaiko
    * @return object $result
    */
    public function getDataProcessZaiko()
    {
        $col = [
            'dt_order_product_detail.receive_id',
            'detail_line_num',
            'sub_line_num',
            'suplier_id',
            'dt_order_product_detail.product_code',
            'child_product_code',
            'product_jan',
            'maker_full_nm',
            'product_maker_code',
            'product_name',
            'product_name_long',
            'product_size',
            'received_order_num',
            'mc.cheetah_status',
//            'mc.delivery_keppin',
        ];
        $result = $this->select($col)
                       ->join('mst_product as mp', DB::raw('IFNULL(dt_order_product_detail.child_product_code, dt_order_product_detail.product_code)'), '=', 'mp.product_code')
                       ->join('edi.mst_product as mc', function($join){
                                $join->on(DB::raw('IFNULL(dt_order_product_detail.child_product_code, dt_order_product_detail.product_code)'), '=', 'mc.product_code');
                                $join->where('mc.is_live', '=', 1);
                        })
						//->join('edi.mst_product as mc', 'mp.product_code', '=', 'mc.product_code')
                       ->join('mst_order', 'mst_order.receive_id', '=', 'dt_order_product_detail.receive_id')
                       ->where('product_status', '=', PRODUCT_STATUS['WAIT_TO_ESTIMATE'])
                       ->where('mst_order.is_mail_sent', 1)
					   //->where('mc.is_live', 1)
                       ->orderBy('dt_order_product_detail.receive_id')
                       ->orderBy('detail_line_num')
                       ->get();
        return $result;
    }

    /**
    * Update product status after process batch order
    * @param string $receiveId   value condition field receive_id
    * @param string $detailLine  value condition field detail_line_num
    * @param string $subLineNum  value condition field sub_line_num
    * @param string $productStatus value need update
    * @return boolean $check or string $error
    */
    // public function updateOrderProStatus($receiveId, $detailLine, $subLineNum, $productStatus)
    // {
    //     $check = '';
    //     try {
    //         $check = $this->where('receive_id', '=', $receiveId)
    //                       ->where('detail_line_num', '=', $detailLine)
    //                       ->where('sub_line_num', '=', $subLineNum)
    //                       ->update(['product_status' => $productStatus]);
    //     } catch (\Exception $e) {
    //         $error      = "------------------------------------------" . PHP_EOL;
    //         $className  = debug_backtrace(false, 2)[1]['class'];
    //         $arrClass   = explode('\\', $className);
    //         $error     .= end($arrClass) . PHP_EOL;
    //         $error     .= $e->getPrevious()->getMessage();
    //         $error     .= PHP_EOL . "------------------------------------------" . PHP_EOL;
    //         return $error;
    //     }
    //     return $check;
    // }

    /**
    * Get data process supplier EDI
    * @param string $type use when is product set
    * @return object $result
    */
    public function getDataProcessEDI()
    {
        $col = [
            'dt_order_product_detail.receive_id',
            'dt_order_product_detail.detail_line_num',
            'dt_order_product_detail.sub_line_num',
            'dt_order_product_detail.suplier_id',
            DB::Raw('IFNULL(dt_order_product_detail.child_product_code,dt_order_product_detail.product_code) as product_code'),
            'dt_order_product_detail.price_invoice',
            'dt_order_product_detail.received_order_num',
            'dt_order_product_detail.sku_num',
            'dt_order_product_detail.is_close_dok',
            'mp.product_jan',
            'mc.cheetah_status',
//            'mc.delivery_keppin',
        ];
        $result = $this->select($col)
                        ->join('mst_product as mp', DB::raw('IFNULL(dt_order_product_detail.child_product_code, dt_order_product_detail.product_code)'), '=', 'mp.product_code')
						->join('edi.mst_product as mc', function($join){
                                $join->on(DB::raw('IFNULL(dt_order_product_detail.child_product_code, dt_order_product_detail.product_code)'), '=', 'mc.product_code');
                                $join->where('mc.is_live', '=', 1);
                        })
                       // ->join('edi.mst_product as mc', 'mp.product_code', '=', 'mc.product_code')
                       ->join('mst_order as o', "dt_order_product_detail.receive_id", '=', 'o.receive_id')
                       ->where('order_status', '=', ORDER_STATUS['ORDER'])
                       ->where('product_status', '=', PRODUCT_STATUS['WAIT_TO_ORDER'])
                       ->where('o.order_sub_status', '<>', ORDER_SUB_STATUS['ERROR'])
                       ->whereIn('delivery_type', [3])
                       ->where('o.is_delay', 0)
					   // ->where('mc.is_live', 1)
                       ->orderBy('o.receive_id')
                       ->orderBy('detail_line_num')
                       ->get();
        return $result;
    }

    /**
    * Update information table dt_order_product_detail by order_code
    * @param string $orderCode   value condition field order_code
    * @param array $dataUpdate
    * @return boolean $check or string $error
    */
    public function updateDataByOrderCode($arrOrderCode, $dataUpdate)
    {
        if (array_key_exists('product_status', $dataUpdate)) {
            $modelLog  = new LogUpdateStatusProduct();
            $className = debug_backtrace(false, 2)[1]['class'];
            $modelLog->insertLog(
                ['received_order_id' => implode(',', $arrOrderCode)],
                $dataUpdate,
                $className,
                'received_order_id'
            );
        }
        return $this->whereIn('order_code', $arrOrderCode)
                    ->update($dataUpdate);
    }

    /**
    * The get data process Inventory reservation step 2
    * @return object $result
    */
    public function getDataProcessInventory2()
    {
        $col = [
            'product_code',
            'child_product_code',
            'received_order_num',
            'order_num',
            'dt_order_product_detail.receive_id',
            'detail_line_num',
            'sub_line_num',
        ];
        $result = $this->select($col)
                       ->join('mst_order', 'dt_order_product_detail.receive_id', '=', 'mst_order.receive_id')
                       ->where('product_status', PRODUCT_STATUS['WAIT_TO_ORDER'])
                       ->whereNull('order_code')
                       ->where('stock_status', 2)
                       ->where('order_status', ORDER_STATUS['ORDER'])
                       ->orderBy('product_code')
                       ->get();
        return $result;
    }

    /**
    * Update information by condition
    * @param array $key           condition
    * @param array $dataUpdate    data update
    * @return object $result
    */
    public function updateDataByCondition($key, $dataUpdate)
    {
        if (array_key_exists('product_status', $dataUpdate)) {
            $modelLog  = new LogUpdateStatusProduct();
            $className = debug_backtrace(false, 2)[1]['class'];
            $modelLog->insertLog($key, $dataUpdate, $className);
        }
        return $this->where($key)
                    ->update($dataUpdate);
    }

    /**
     * Get max delivery date
     *
     * @param  array   $arrReceiveId
     * @return object
     */
    public function getMaxDeliveryDate($arrReceiveId)
    {
        $data = $this->select('receive_id', DB::raw('MAX(delivery_date) AS max_delivery_date'))
                ->whereIn('receive_id', $arrReceiveId)
                ->groupBy('receive_id')
                ->get();
        return $data;
    }

    /**
    * Get data process inventory 3
    * @return object $result
    */
    public function getDataProcessInventory3()
    {
        $col = [
            'dt_order_product_detail.receive_id',
            'dt_order_product_detail.detail_line_num',
            'dt_order_product_detail.sub_line_num',
            'dt_order_product_detail.order_num',
            DB::Raw('IFNULL(dt_order_product_detail.child_product_code,dt_order_product_detail.product_code) as product_code'),
            'received_order_num',
            'order_num',
            'order_zan_num',
            'wait_delivery_num',
            'nanko_num',
            'mc.cheetah_status',
            'dt_order_product_detail.suplier_id AS supplier_id',
            'mst_order.ship_wish_date',
        ];
        $sub = '(select deli_days from mst_postal where postal_code = mst_customer.zip_code limit 1) AS deli_days';
        $result = $this->select($col)
                       ->selectRaw($sub)
                       ->join('mst_stock_status as mss', DB::raw('IFNULL(dt_order_product_detail.child_product_code, dt_order_product_detail.product_code)'), '=', 'mss.product_code')
                       ->join('edi.mst_product as mc', function($join){
                                $join->on(DB::raw('IFNULL(dt_order_product_detail.child_product_code, dt_order_product_detail.product_code)'), '=', 'mc.product_code');
                                $join->where('mc.is_live', '=', 1);
                        })
                       ->leftjoin('mst_order_detail', function ($join) {
                            $join->on('dt_order_product_detail.receive_id', '=', 'mst_order_detail.receive_id')
                            ->on('dt_order_product_detail.detail_line_num', '=', 'mst_order_detail.detail_line_num');
                       })
                       ->leftjoin('mst_order', 'mst_order.receive_id', '=', 'mst_order_detail.receive_id')
                       ->leftjoin('mst_customer', 'mst_customer.customer_id', '=', 'mst_order_detail.receiver_id')
                       ->where('product_status', PRODUCT_STATUS['WAIT_TO_ORDER'])
                       ->where('order_status', ORDER_STATUS['ORDER'])
                       ->where('stock_status', 2)
                       ->whereNull('order_code')
                       ->orderBy('product_code')
                       ->orderBy('dt_order_product_detail.in_date')
                       ->get();
        return $result;
    }

    /**
     * The get data process order zaiko result step 2.
     *
     * @return object $result
     */
    public function getDataZaikoResult2()
    {
        $col = [
            'dt_order_product_detail.receive_id',
            'dt_order_product_detail.detail_line_num',
            'dt_order_product_detail.sub_line_num',
            'product_status'
        ];
        $result = $this->select($col)
                       ->join('mst_order as o', 'dt_order_product_detail.receive_id', '=', 'o.receive_id')
                       ->where(function ($query) {
                            $query->where(function ($sub1) {
                                $sub1->where('o.order_status', '=', ORDER_STATUS['ESTIMATION'])
                                    ->where('o.order_sub_status', '<', ORDER_SUB_STATUS['DONE'])
                                    ->whereIn(
                                        'dt_order_product_detail.product_status',
                                        [
                                            PRODUCT_STATUS['OUT_OF_STOCK'],
                                            PRODUCT_STATUS['SALE_STOP']
                                        ]
                                    );
                            });
                       })
                       ->where('o.is_delay', 0)
                       ->orderby('dt_order_product_detail.receive_id')
                       ->get();
        return $result;
    }

    /**
     * Update cancel stock
     *
     * @param  mixed $receiveId
     * @return void
     */
    public function updateCancelStock($receiveId)
    {
        $dataOPD = $this->select(
            [
                DB::Raw('IFNULL(dt_order_product_detail.child_product_code,dt_order_product_detail.product_code) as product_code'),
                'dt_order_product_detail.received_order_num',
                'dt_order_product_detail.delivery_type',
                'dt_order_product_detail.product_status',
                'dt_order_product_detail.order_num',
                'o.order_status'
            ]
        )->join('mst_order as o', 'dt_order_product_detail.receive_id', '=', 'o.receive_id');
        if (is_array($receiveId)) {
            $dataOPD->whereIn('dt_order_product_detail.receive_id', $receiveId);
        } else {
            $dataOPD->where('dt_order_product_detail.receive_id', $receiveId);
        }

        $dataOPD = $dataOPD->get();
        $modelSS = new MstStockStatus();
        foreach ($dataOPD as $item) {
            $dataSS = $modelSS->find($item->product_code);
            if (!empty($dataSS)) {
                if (in_array($item->delivery_type, [1, 3]) &&
                    $item->product_status > 0 &&
                    $item->product_status !== PRODUCT_STATUS['CANCEL'] &&
                    $item->order_status >= ORDER_STATUS['ESTIMATION']) {
                    $dataSS->order_zan_num = $dataSS->order_zan_num - $item->received_order_num;
                    if ($item->product_status === 1) {
                        $dataSS->wait_delivery_num =
                            $dataSS->wait_delivery_num - $item->received_order_num;
                    } elseif ($item->delivery_type !== 3) {
                        $dataSS->wait_delivery_num = $dataSS->wait_delivery_num - ($item->received_order_num - $item->order_num);
                    }
                    $dataSS->save();
                }
            }
        }
    }
    /**
    * The get data process close ok
    * @return object $result
    */
    public function getDataProcessCloseOk()
    {
        $result = $this->select('receive_id')
                       ->where('sku_num', 0)
                       ->where('product_status', '<>', 1)
                       ->groupBy('receive_id')
                       ->havingRaw('COUNT(sub_line_num) = ?', [1])
                       ->get();
        return $result;
    }
    /**
    * The get data process sku num
    * @return object $result
    */
    public function getDataProcessSkuNum()
    {
        $result = $this->select('receive_id')
                       ->addSelect(DB::raw("COUNT(sub_line_num) AS sku_num_up"))
                       ->where('sku_num', 0)
                       ->groupBy('receive_id')
                       ->get();
        return $result;
    }
}
