<?php

/**
 * Model for t_order_direct table.
 *
 * @package    App\Models\Batches
 * @subpackage TOrderDirect
 * @copyright  Copyright (c) 2017 RiverCrane! Corporation. All Rights Reserved.
 * @author     Truong Nghia<truong.van.nghia@rivercrane.vn>
 */

namespace App\Models\Batches;

use Illuminate\Database\Eloquent\Model;

class TOrderDirect extends Model
{
    public $timestamps = false;

    /**
    * The database table used by the model.
    * @var string
    */
    protected $table = 't_order_direct';

    /**
    * The database is used by the model.
    * @var string
    */
    protected $connection = 'edi';

    /**
     * Get list data process EDI result (direct)
     * @return object $result
     */
    public function getDataDirect()
    {
        $col = [
            't_order_direct.m_order_type_id',
            't_order_direct.shipment_date_plan',
            't_order_direct.m_order_type_id',
            't_order_direct.other',
            'td.shipment_date',
            'ots.order_code',
            'ots.edi_order_code',
            'ots.receive_id',
            'ots.detail_line_num',
            'ots.sub_line_num',
            'received_order_id',
            'opd.product_status'
        ];
        $result = $this->select($col)
                       ->leftjoin('t_delivery as td', 't_order_direct.t_delivery_id', '=', 'td.t_delivery_id')
                       ->join('hrnb.dt_order_to_supplier as ots', 'ots.edi_order_code', '=', 't_order_direct.order_code')
                       ->join('hrnb.mst_order as o', 'o.receive_id', '=', 'ots.receive_id')
                       ->leftJoin('hrnb.dt_order_product_detail AS opd', 'opd.order_code', '=', 'ots.order_code')
                       ->where(function ($subWhere) {
                            $subWhere->where(function ($subW) {
                                $subW->whereIn('t_order_direct.filmaker_flg', [0,1])
                                    ->where('t_order_direct.del_flg', '=', 0)
                                    ->where('t_order_direct.valid_flg', '=', 1)
                                    ->where('ots.process_status', '<', '3');
                            })
                            ->orWhere(function ($subW) {
                                $subW->where('ots.process_status', 2)
                                    ->where('t_order_direct.m_order_type_id', 1);
                            });
                       })
                       ->where('ots.edi_order_code', '<>', '')
                       ->get();
        return $result;
    }

    /**
     * Update data
     *
     * @param  array   $key
     * @param  array   $data
     * @return boolean
     */
    public function updateData($key, $data)
    {
        return $this->where($key)->update($data);
    }
}
