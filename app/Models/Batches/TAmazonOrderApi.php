<?php
/**
 * Model for t_amazon_order_api table.
 *
 * @package    App\Models\Batches
 * @subpackage TAmazonOrderApi
 * @copyright  Copyright (c) 2018 RiverCrane! Corporation. All Rights Reserved.
 * @author     Truong Nghia<truong.van.nghia@rivercrane.vn>
 */

namespace App\Models\Batches;

use Illuminate\Database\Eloquent\Model;
use DB;

use App\Custom\DBExtend;

class TAmazonOrderApi extends Model
{
    use DBExtend;

    public $timestamps = false;
    /**
    * The database table used by the model.
    * @var string
    */
    protected $table = 't_amazon_order_api';
    /**
    * The database is used by the model.
    * @var string
    */
    protected $connection = 'horunba';

    /**
     * Get data of tabe t_amazon_order_api
     *
     * @return object $result
     */
    public function getData()
    {
        return $this->selectRaw('t_amazon_order_api.*, COD_fee_amount, COD_fee_discount_amount')
            ->selectRaw('SUM(shipping_price_amount) AS ship_charge')
            ->selectRaw('SUM(item_price_amount) AS goods_price')
            ->selectRaw('SUM(item_tax_amount) AS goods_tax')
            ->join(
                't_amazon_order_detail',
                't_amazon_order_api.amazon_order_id',
                '=',
                't_amazon_order_detail.amazon_order_id'
            )
            ->where('process_flg', 0)
            ->where('is_deleted', 0)
            ->where('item_price_amount', '<>', 0)
            ->groupBy('t_amazon_order_api.amazon_order_id')
            ->get();
    }

    /**
     * Update process flag.
     *
     * @param  string  $key
     * @return object $result
     */
    public function updateProcessFlg($key)
    {
        $result = $this->where('amazon_order_id', $key)
             ->update(['process_flg' => 1]);
        return $result;
    }

    /**
     * Get orders exist
     *
     * @param  array   $orders
     * @return object
     */
    public function getOrdersExist($orders)
    {
        $data = $this->select('amazon_order_id')->whereIn('amazon_order_id', $orders)->get();
        return $data;
    }
    /**
     * Get order by status
     *
     * @param  array  $arrStatus
     * @return object
     */
    public function getOrdersByStatus($arrStatus)
    {
        $result = $this->select('amazon_order_id', 'order_status')->whereIn('order_status', $arrStatus)->get();
        return $result;
    }

    /**
     * Update data
     *
     * @param  array   $key
     * @param  array   $data
     * @return boolean
     */
    public function updateData($key, $data)
    {
        return $this->where($key)->update($data);
    }
}
