<?php

/**
 * Model for t_zaiko table.
 *
 * @package    App\Models\Batches
 * @subpackage BatchModel
 * @copyright  Copyright (c) 2017 RiverCrane! Corporation. All Rights Reserved.
 * @author     Truong Nghia<truong.van.nghia@rivercrane.vn>
 */

namespace App\Models\Batches;

use Illuminate\Database\Eloquent\Model;

class TZaiko extends Model
{
    public $timestamps = false;

    /**
    * The database table used by the model.
    * @var string
    */
    protected $table = 't_zaiko';

    /**
    * The database is used by the model.
    * @var string
    */
    protected $connection = 'edi';

    /**
     * Get last order code in table t_zaiko
     * @return object $result
     */
    public function getMaxOrderCode()
    {
        $result = $this->select('order_code')
                       ->where('order_code', 'like', 'HE%')
                       ->orderBy('t_zaiko_id', 'DESC')
                       ->first();
        return $result;
    }

    /**
     * Get list data process zaiko result
     * @return object $result
     */
    public function getDataZaikoResult()
    {
        $col = [
            't_zaiko_id',
            'ds.receive_id',
            'ds.detail_line_num',
            'ds.sub_line_num',
            'm_order_type_id',
            'ds.estimate_code',
            't_zaiko.other',
            't_zaiko.memo',
        ];
        $result = $this->select($col)
                       ->join('hrnb.dt_estimation as ds', 't_zaiko.order_code', '=', 'ds.estimate_code')
                       ->join('hrnb.dt_order_product_detail as dopd', function ($join) {
                            $join->on('ds.receive_id', '=', 'dopd.receive_id')
                                 ->on('ds.detail_line_num', '=', 'dopd.detail_line_num')
                                 ->on('ds.sub_line_num', '=', 'dopd.sub_line_num');
                       })
                       ->whereIn('t_zaiko.filmaker_flg', [0,1])
                       ->where('t_zaiko.del_flg', '=', 0)
                       ->where('t_zaiko.valid_flg', '=', 1)
                       ->where('dopd.product_status', '=', 6)
                       ->get();
        return $result;
    }
}
