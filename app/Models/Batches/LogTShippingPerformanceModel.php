<?php
/**
 * Model for log_tShippingPerformanceModel table.
 *
 * @package    App\Models\Batches
 * @subpackage LogTShippingPerformanceModel
 * @copyright  Copyright (c) 2018 RiverCrane! Corporation. All Rights Reserved.
 * @author     Truong Nghia<truong.van.nghia@rivercrane.vn>
 */

namespace App\Models\Batches;

use Illuminate\Database\Eloquent\Model;
use DB;
use App\Custom\DBExtend;

class LogTShippingPerformanceModel extends Model
{
    public $timestamps = false;

    /**
    * The database table used by the model.
    * @var string
    */
    protected $table = 'log_tShippingPerformanceModel';

    /**
    * The database is used by the model.
    * @var string
    */
    protected $connection = 'horunba';
}
