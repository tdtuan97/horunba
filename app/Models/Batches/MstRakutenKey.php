<?php

/**
 * Exe sql for hrnb.mst_rakuten_key
 *
 * @package    App\Models
 * @subpackage MstMall
 * @copyright  Copyright (c) 2019 RiverCrane! Corporation. All Rights Reserved.
 * @author     lam.vinh<lam.vinh.rcvn2012@gmail.com>
 */

namespace App\Models\Batches;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class MstRakutenKey extends Model
{
    public $timestamps = false;
    /**
    * The database table used by the model.
    * @var string
    */
    protected $table = 'mst_rakuten_key';

    /**
    * The database is used by the model.
    * @var string
    */
    protected $connection = 'horunba';

    /**
     * Get data of table mst_rakuten_key
     *
     * @return object
     */
    public function getKeyRakuten()
    {
        $data = $this->first();
        $key = '';
        if (!empty($data)) {
            $key = 'ESA ' . base64_encode($data->service_secret.':'.$data->license_key);
        }
        return $key;
    }
}
