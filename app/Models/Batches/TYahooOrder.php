<?php

/**
 * Model for t_yahoo_order table.
 *
 * @package    App\Models\Batches
 * @subpackage BatchModel
 * @copyright  Copyright (c) 2017 RiverCrane! Corporation. All Rights Reserved.
 * @author     Truong Nghia<truong.van.nghia@rivercrane.vn>
 */

namespace App\Models\Batches;

use Illuminate\Database\Eloquent\Model;
use DB;

class TYahooOrder extends Model
{
    public $timestamps = false;

    /**
    * The database table used by the model.
    * @var string
    */
    protected $table = 't_yahoo_order';

    /**
    * The database is used by the model.
    * @var string
    */
    protected $connection = 'horunba';


    /**
     * Get data check same data
     * @param int $limit limit get datas
     * @return object $result
     */
    public function getDataCheck($limit)
    {
        $objId = $this->select('order_id')
                      ->where('process_flg', 0)
                      ->groupBy('order_id')
                      ->limit($limit)
                      ->get();
        $arrId = array_column($objId->toArray(), 'order_id');
        $result = $this->select(['yahoo_order_list_key', 'order_id', 'item_id', 'order_status', 'settlestatus'])
                       ->whereIn('order_id', $arrId)
                       ->groupBy(['order_id', 'item_id'])
                       ->get();
        return $result;
    }

    /**
     * Get data of tabe t_yahoo_order by id
     * @param int $limit limit get datas
     * @return object $result
     */
    public function getData($arrId)
    {
        $result = $this->whereIn('order_id', $arrId)
                       ->where('is_delete', 0)
                       ->where('process_flg', '=', '0')
                       ->orderBy('order_id')
                       ->orderBy('yahoo_order_list_key')
                       ->get();
        return $result;
    }

   /**
     * Get order detail before insert order.
     *
     * @param  array  $arrOrderId  list order id for where condition
     * @return object $result
     */
    public function getInfoOrderDetail($receiveId)
    {
        $result = $this->join('mst_order', function ($join) use ($receiveId) {
                            $join->on('order_id', '=', 'mst_order.received_order_id')
                                 ->where('receive_id', '=', $receiveId)
                                 ->where('is_delete', 0);
        })
                       ->get();
        return $result;
    }

   /**
     * Update process flag before get information order.
     *
     * @param  array  $arrKey list id need update
     * @return object $result
     */
    public function updateProcessFlg($arrKey)
    {
        $result = $this->whereIn('yahoo_order_list_key', $arrKey)
             ->update(['process_flg' => 1]);
        return $result;
    }

    /**
     * Calculate price.
     *
     * @return array $arrResult
     */
    public function calculatePrice()
    {
        $col = ['order_id'];
        $goodsPrice = "SUM(unit_price*quantity) as goods_price";
        $result = $this->select($col)
                       ->selectRaw($goodsPrice)
                       ->where('process_flg', '=', '0')
                       ->where('is_delete', 0)
                       ->groupBy('order_id')
                       ->get();
        $arrResult = [];
        foreach ($result as $value) {
            $arrResult[$value->order_id]['goods_price'] = (int)$value->goods_price;
        }
        return $arrResult;
    }

    /**
     * Get order by order ids
     *
     * @param  array  $orderIds
     * @return object
     */
    public function getOrderByOrderId($orderId)
    {
        $data = $this->select('order_id')->where('order_id', $orderId)->first();
        return $data;
    }
}
