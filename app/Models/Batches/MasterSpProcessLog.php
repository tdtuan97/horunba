<?php
/**
 * Model for master_sp_process_log table
 *
 * @package    App\Models
 * @subpackage MasterSpProcessLog
 * @copyright  Copyright (c) 2018 RiverCrane! Corporation. All Rights Reserved.
 * @author     Truong Nghia<truong.van.nghia@rivercrane.vn>
 */

namespace App\Models\Batches;

use Illuminate\Database\Eloquent\Model;
use DB;

class MasterSpProcessLog extends Model
{
    public $timestamps = false;
    /**
    * The database table used by the model.
    * @var string
    */
    protected $table = 'master_sp_process_log';
    /**
    * The database is used by the model.
    * @var string
    */
    protected $connection = 'logs_total';

    /**
     * log data
     *
     * @param  type  $processName
     * @return bool
     */
    public function countCheckProcessLog($processName)
    {
        $result = $this->select('process_step')
                       ->where('process_name', $processName)
                       ->whereRaw("DATE(process_time) = DATE(NOW())")
                       ->where('process_step', '<>', function ($sub_1) use ($processName) {
                            $sub_1->select('a.process_step')
                                  ->from('master_sp_process_log AS a')
                                  ->where('a.process_name', $processName)
                                  ->whereRaw("DATE(a.process_time) = DATE(NOW())")
                                  ->whereRaw("a.process_step = CONCAT(master_sp_process_log.process_step,' sucess')")
                                  ->where('master_sp_process_log.finish_rows_num', '<>', function ($sub_2) use ($processName) {
                                        $sub_2->select('a.finish_rows_num')
                                              ->from('master_sp_process_log AS a')
                                              ->where('a.process_name', $processName)
                                              ->whereRaw("DATE(a.process_time) = DATE(NOW())")
                                              ->whereRaw("a.process_step = CONCAT(master_sp_process_log.process_step,' sucess')");
                                  });
                       })
                       ->get();
        return $result;
    }
}
