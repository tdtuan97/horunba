<?php

/**
 * Model for t_customer table.
 *
 * @package    App\Models\Batches
 * @subpackage TCustomer
 * @copyright  Copyright (c) 2017 RiverCrane! Corporation. All Rights Reserved.
 * @author     Truong Nghia<truong.van.nghia@rivercrane.vn>
 */

namespace App\Models\Batches;

use Illuminate\Database\Eloquent\Model;

class TCustomer extends Model
{
    public $timestamps = false;

    /**
    * The database table used by the model.
    * @var string
    */
    protected $table = 't_customer';

    /**
    * The database is used by the model.
    * @var string
    */
    protected $connection = 'edi';

    /**
    * Check exixts customer.
    * @param array $arrDatas Data for condition where
    * @return object $result
    */
    public function checkCustomerExist($arrDatas)
    {
        $result = $this->whereRaw("c_nm = BINARY ifnull(?,'')", [$arrDatas['c_nm']])
                       ->whereRaw("c_postal = BINARY ifnull(?,'')", [$arrDatas['c_postal']])
                       ->whereRaw("c_address = BINARY ifnull(?,'')", [$arrDatas['c_address']])
                       ->whereRaw("c_tel = BINARY ifnull(?,'')", [$arrDatas['c_tel']])
                       ->whereRaw("d_nm = BINARY ifnull(?,'')", [$arrDatas['d_nm']])
                       ->whereRaw("d_postal = BINARY ifnull(?,'')", [$arrDatas['d_postal']])
                       ->whereRaw("d_address = BINARY ifnull(?,'')", [$arrDatas['d_address']])
                       ->whereRaw("d_tel = BINARY ifnull(?,'')", [$arrDatas['d_tel']])
                       ->whereRaw("del_flg = ?", [$arrDatas['del_flg']])
                       ->whereRaw("valid_flg = ?", [$arrDatas['valid_flg']])
                       ->first();
        return $result;
    }
}
