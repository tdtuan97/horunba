<?php
/**
 * Model for log_tShippingDetailModel table.
 *
 * @package    App\Models\Batches
 * @subpackage DtDelivery
 * @copyright  Copyright (c) 2018 RiverCrane! Corporation. All Rights Reserved.
 * @author     Truong Nghia<truong.van.nghia@rivercrane.vn>
 */

namespace App\Models\Batches;

use Illuminate\Database\Eloquent\Model;

class LogTShippingDetailModel extends Model
{
    public $timestamps = false;

    /**
    * The database table used by the model.
    * @var string
    */
    protected $table = 'log_tShippingDetailModel';

    /**
    * The database is used by the model.
    * @var string
    */
    protected $connection = 'horunba';
}
