<?php

/**
 * Model for mst_status_batch table.
 *
 * @package    App\Models\Batches
 * @subpackage MstStatusBatch
 * @copyright  Copyright (c) 2017 RiverCrane! Corporation. All Rights Reserved.
 * @author     Truong Nghia<truong.van.nghia@rivercrane.vn>
 */

namespace App\Models\Batches;

use Illuminate\Database\Eloquent\Model;

class MstStatusBatch extends Model
{
    public $timestamps = false;

    /**
    * The database table used by the model.
    * @var string
    */
    protected $table = 'mst_status_batch';

    /**
    * The database is used by the model.
    * @var string
    */
    protected $connection = 'horunba';

    /**
     * Get data by signature
     * @param  String $signature
     * @return object
     */
    public function getDataBySignature($signature)
    {
        $resutl = $this->where('command', '=', $signature)
                       ->first();
        return $resutl;
    }

    /**
     * Check process order
     * @return int
     */
    public function checkProcessOrder()
    {
        $arrayCommand = ['process:get-order-rakuten',
                        'process:get-order-amazon',
                        'process:get-order-diy',
                        'process:get-order-yahoo',
                        'process:get-order-biz',
                        'process:smaregi-api',
                        'process:order-customer',
                        'process:divide-delivery',
                        ];
        $resutl = $this->whereIn('command', $arrayCommand)
                       ->where('status_flag', '=', 1)
                       ->count();
        return $resutl;
    }
}
