<?php
/**
 * Model for dt_send_mail_list table.
 *
 * @package    App\Models\Batches
 * @subpackage DtSendMailList
 * @copyright  Copyright (c) 2017 RiverCrane! Corporation. All Rights Reserved.
 * @author     Truong Nghia<truong.van.nghia@rivercrane.vn>
 */

namespace App\Models\Batches;

use Illuminate\Database\Eloquent\Model;
use App\Custom\DBExtend;

class DtSendMailList extends Model
{
    use DBExtend;

    public $timestamps = false;

    /**
     * The primary key for the model.
     *
     * @var array
     */
    protected $primaryKey = [
        'receive_order_id',
        'order_status_id',
        'order_sub_status_id',
    ];

    /**
     * Indicates if the IDs are auto-incrementing.
     *
     * @var bool
     */
    public $incrementing = false;

    /**
     * The fillable for the model.
     *
     * @var array
     */
    protected $fillable = [
        'receive_order_id',
        'order_status_id',
        'order_sub_status_id',
        'payment_method',
        'mall_id',
        'mail_subject',
        'mail_content',
        'attached_file_path',
        'send_status',
        'mail_to',
        'mail_cc',
        'mail_bcc',
        'mail_from',
        'error_code',
        'error_message',
        'in_date',
        'in_ope_cd',
        'up_date',
        'up_ope_cd',
    ];
    /**
    * The database table used by the model.
    * @var string
    */
    protected $table = 'dt_send_mail_list';

    /**
    * The database is used by the model.
    * @var string
    */
    protected $connection = 'horunba';

    /**
    * The get data process send mail.
    * @return object $result
    */
    public function getDataMailSend($limit)
    {
        $col = [
            'receive_order_id',
            'order_status_id',
            'order_sub_status_id',
            'mall_id',
            'mail_subject',
            'mail_content',
            'attached_file_path',
            'mail_to',
            'mail_cc',
            'mail_bcc',
            'mail_from',
            'send_status',
            'operater_send_index',
        ];
        $result = $this->select($col)
                       ->where('send_status', '=', 0)
                       ->limit($limit)
                       ->get();
        return $result;
    }

    /**
    * Update information
    * @param string $key      condition
    * @param array $arrUpdate data update
    * @return object boolean
    */
    public function updateData($key, $arrUpdate)
    {
        return $this->where($key)
                    ->update($arrUpdate);
    }
}
