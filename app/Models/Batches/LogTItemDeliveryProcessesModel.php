<?php
/**
 * Model for log_tItemDeliveryProcessesModel table.
 *
 * @package    App\Models\Batches
 * @subpackage LogTItemDeliveryProcessesModel
 * @copyright  Copyright (c) 2018 RiverCrane! Corporation. All Rights Reserved.
 * @author     Le Hung<le.hung@rivercrane.vn>
 */

namespace App\Models\Batches;

use Illuminate\Database\Eloquent\Model;
use DB;
use App\Custom\DBExtend;

class LogTItemDeliveryProcessesModel extends Model
{
    public $timestamps = false;

    /**
    * The database table used by the model.
    * @var string
    */
    protected $table = 'log_tItemDeliveryProcessesModel';

    /**
    * The database is used by the model.
    * @var string
    */
    protected $connection = 'horunba';
    /**
    * The primaryKey is used by the model.
    * @var string
    */
    protected $primaryKey = 'ItemCd';
}
