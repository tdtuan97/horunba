<?php

/**
 * Model for t_biz_order table.
 *
 * @package    App\Models\Batches
 * @subpackage TBizOrder
 * @copyright  Copyright (c) 2017 RiverCrane! Corporation. All Rights Reserved.
 * @author     Truong Nghia<truong.van.nghia@rivercrane.vn>
 */

namespace App\Models\Batches;

use Illuminate\Database\Eloquent\Model;
use DB;

class TBizOrder extends Model
{
    public $timestamps = false;

    /**
    * The database table used by the model.
    * @var string
    */
    protected $table = 't_biz_order';

    /**
    * The database is used by the model.
    * @var string
    */
    protected $connection = 'horunba';

        /**
     * Get data check same data
     * @param int $limit limit get datas
     * @return object $result
     */
    public function getDataCheck($limit)
    {
        $objId = $this->select('order_id')
                      ->where('process_flg', 0)
                      ->groupBy('order_id')
                      ->limit($limit)
                      ->get();
        $arrId = array_column($objId->toArray(), 'order_id');
        $result = $this->select(['id', 'order_id', 'item_code'])
                       ->whereIn('order_id', $arrId)
                       ->groupBy(['order_id', 'item_code'])
                       ->get();
        return $result;
    }

    /**
     * Get data of tabe t_yahoo_order by id
     * @param int $limit limit get datas
     * @return object $result
     */
    public function getData($arrId)
    {
        $this->where('settlementname', 'like', '%請求書締払い%')
             ->where('credit', '未与信')
             ->whereIn('order_id', $arrId)
             ->update([
                 'is_delete'   => 1,
                 'process_flg' => 1,
             ]);
        $result = $this->whereIn('order_id', $arrId)
                       ->where('is_delete', 0)
                       ->where('process_flg', '=', '0')
                       ->orderBy('order_id')
                       ->orderBy('id')
                       ->get();
        return $result;
    }

   /**
     * Get order detail before insert order.
     * @param  array  $receiveId value receive_id for condition where
     * @return object $result
     */
    public function getInfoOrderDetail($receiveId)
    {
        $result = $this->join('mst_order', function ($join) use ($receiveId) {
                            $join->on('order_id', '=', 'mst_order.received_order_id')
                                 ->where('receive_id', '=', $receiveId)
                                 ->where('is_delete', 0);
        })
                       ->get();
        return $result;
    }

   /**
     * Update process flag before get information order.
     *
     * @param  array  $arrKey list id need update
     * @return object $result
     */
    public function updateProcessFlg($arrKey)
    {
        $result = $this->whereIn('id', $arrKey)
             ->update(['process_flg' => 1]);
        return $result;
    }

   /**
     * Get list good price
     * @return array $arrResult
     */
    public function getGoodsprice()
    {
        $col = 'order_id';
        $colGoodsPrice = "SUM(CEIL(item_price * 1.08) * item_units) as goods_price";
        $result = $this->select($col)
                       ->selectRaw($colGoodsPrice)
                       ->where('process_flg', '=', '0')
                       ->where('is_delete', 0)
                       ->groupBy('order_id')
                       ->get();
        $arrResult = [];
        foreach ($result as $value) {
            $arrResult[$value->order_id] = $value->goods_price;
        }
        return $arrResult;
    }

   /**
     * Count check order exists.
     * @param  array  $arrKey
     * @return object $result
     */
    public function countCheckExists($arrKey)
    {
        $result = $this->where($arrKey)
                       ->count();
        return $result;
    }

}
