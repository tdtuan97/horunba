<?php
/**
 * Model for dt_receipt_ledger table.
 *
 * @package    App\Models\Batches
 * @subpackage class DtReceiptLedger extends Model

 * @copyright  Copyright (c) 2018 RiverCrane! Corporation. All Rights Reserved.
 * @author     Truong Nghia<truong.van.nghia@rivercrane.vn>
 */

namespace App\Models\Batches;

use Illuminate\Database\Eloquent\Model;
use App\Models\Batches\MstProduct;

class DtReceiptLedger extends Model
{
    public $timestamps = false;

    /**
    * The database table used by the model.
    * @var string
    */
    protected $table = 'dt_receipt_ledger';

    /**
    * The database is used by the model.
    * @var string
    */
    protected $connection = 'horunba';

    /**
     * Get Max index of product
     * @param int productCode
     * @return object
     */
    public function getMaxIndex($productCode)
    {
        $result = $this->selectRaw('MAX(`index`) AS `index`')
                       ->where('product_code', $productCode)
                       ->groupBy('product_code')
                       ->first();
        return $result;
    }

    /**
     * Get data stock of product
     * @param int productCode
     * @return object
     */
    public function getDataProcessStock($productCode)
    {
        $col = [
            'index',
            'line_index',
            'product_code',
            'recognite_key',
            'price_invoice',
            'stock_num',
            'stock_detail_num',
            'edi_order_code',
            'supplier_id',
            'in_date',
        ];
        $result = $this->select($col)
                       ->where('product_code', $productCode)
                       ->where('index', function ($query) use ($productCode) {
                           $query->selectRaw('MAX(`index`)')
                                 ->from('dt_receipt_ledger')
                                 ->where('product_code', $productCode)
                                 ->groupBy('product_code');
                       })
                       ->orderBy('line_index')
                       ->get();
        return $result;
    }

    /**
     * Process out stock follow FIFO
     * @param array arrData
     * @return void
     */
    public function add2ReceiptLedgerOutStock($arrData)
    {
        $datas = $this->getDataProcessStock($arrData['pa_product_code']);
        $count = count($datas);
        if ($count === 0) {
            $index = $this->getMaxIndex($arrData['pa_product_code']);
            $index = !empty($index->index) ? $index->index : 1;
            $arrInsert = [
                'index'             => $index,
                'line_index'        => 1,
                'stock_type'        => $arrData['pa_stock_type'],
                'stock_detail_type' => $arrData['pa_stock_detail_type'],
                'recognite_key'     => $arrData['pa_order_code'],
                'product_code'      => $arrData['pa_product_code'],
                'price_invoice'     => 0,
                'stock_num'         => -($arrData['pa_delivery_num']),
                'stock_detail_num'  => 0,
                'change_num'        => -($arrData['pa_delivery_num']),
                'edi_order_code'    => '',
                'supplier_id'       => $arrData['pa_supplier_id'],
                'in_date'           => now(),
                'up_date'           => now(),
            ];
            $this->insert($arrInsert);
        } else {
            $index = 0;
            $lineIndex = 1;
            $tempDeliveryNum = $arrData['pa_delivery_num'];
            $arrInserts = [];
            $i = 0;
            foreach ($datas as $data) {
                $i++;
                if ($index === 0) {
                    $index = $data->index + 1;
                }
                if ($data->stock_detail_num >= $tempDeliveryNum) {
                    $arrInsert = [
                        'index'             => $index,
                        'line_index'        => $lineIndex,
                        'stock_type'        => $arrData['pa_stock_type'],
                        'stock_detail_type' => $arrData['pa_stock_detail_type'],
                        'recognite_key'     => $arrData['pa_order_code'],
                        'product_code'      => $data->product_code,
                        'price_invoice'     => $data->price_invoice,
                        'stock_num'         => $data->stock_num - $arrData['pa_delivery_num'],
                        'stock_detail_num'  => $data->stock_detail_num - $tempDeliveryNum,
                        'change_num'        => -$tempDeliveryNum,
                        'edi_order_code'    => $data->edi_order_code,
                        'supplier_id'       => !empty($data->supplier_id) ? $data->supplier_id : $arrData['pa_supplier_id'],
                        'in_date'           => $data->in_date, //now(),
                        'up_date'           => now(),
                    ];
                    $arrInserts[] = $arrInsert;
                    $tempDeliveryNum = 0;
                } elseif ($data->stock_detail_num > 0 && $tempDeliveryNum > 0) {
                    $arrInsert = [
                        'index'             => $index,
                        'line_index'        => $lineIndex,
                        'stock_type'        => $arrData['pa_stock_type'],
                        'stock_detail_type' => $arrData['pa_stock_detail_type'],
                        'recognite_key'     => $arrData['pa_order_code'],
                        'product_code'      => $data->product_code,
                        'price_invoice'     => $data->price_invoice,
                        'stock_num'         => $data->stock_num - $arrData['pa_delivery_num'],
                        'stock_detail_num'  => 0,
                        'change_num'        => -$data->stock_detail_num,
                        'edi_order_code'    => $data->edi_order_code,
                        'supplier_id'       => !empty($data->supplier_id) ? $data->supplier_id : $arrData['pa_supplier_id'],
                        'in_date'           => $data->in_date,
                        'up_date'           => now(),
                    ];
                    $tempDeliveryNum -= $data->stock_detail_num;
                    $arrInserts[] = $arrInsert;
                } elseif ($data->stock_detail_num > 0) {
                    // $check = false;
                    // if ($i === $count && ($data->stock_num - $tempDeliveryNum) < 0) {
                    //     $check = true;
                    // }
                    $arrInsert = [
                        'index'             => $index,
                        'line_index'        => $lineIndex,
                        'stock_type'        => $arrData['pa_stock_type'],
                        'stock_detail_type' => $arrData['pa_stock_detail_type'],
                        'recognite_key'     => $arrData['pa_order_code'],
                        'product_code'      => $data->product_code,
                        'price_invoice'     => $data->price_invoice,
                        'stock_num'         => $data->stock_num,
                        'stock_detail_num'  => $data->stock_detail_num,
                        'change_num'        => 0,
                        'edi_order_code'    => $data->edi_order_code,
                        'supplier_id'       => !empty($data->supplier_id) ? $data->supplier_id : $arrData['pa_supplier_id'],
                        'in_date'           => $data->in_date,
                        'up_date'           => now(),
                    ];
                    $tempDeliveryNum -= $data->stock_detail_num;
                    $arrInserts[] = $arrInsert;
                    // if ($check) {
                    //     $arrInsert = [
                    //         'index'             => $index,
                    //         'line_index'        => $lineIndex + 1,
                    //         'stock_type'        => $arrData['pa_stock_type'],
                    //         'stock_detail_type' => $arrData['pa_stock_detail_type'],
                    //         'recognite_key'     => $arrData['pa_order_code'],
                    //         'product_code'      => $data->product_code,
                    //         'price_invoice'     => 0,
                    //         'stock_num'         => -$tempDeliveryNum,
                    //         'stock_detail_num'  => 0,
                    //         'change_num'        => -$tempDeliveryNum,
                    //         'in_date'           => $data->in_date,
                    //         'up_date'           => now(),
                    //     ];
                    //     $arrInserts[] = $arrInsert;
                    // }
                } elseif ($data->stock_detail_num <= 0 && $i === $count) {
                    $arrInsert = [
                        'index'             => $index,
                        'line_index'        => $lineIndex,
                        'stock_type'        => $arrData['pa_stock_type'],
                        'stock_detail_type' => $arrData['pa_stock_detail_type'],
                        'recognite_key'     => $arrData['pa_order_code'],
                        'product_code'      => $data->product_code,
                        'price_invoice'     => $data->price_invoice,
                        'stock_num'         => $data->stock_num - $arrData['pa_delivery_num'],
                        'stock_detail_num'  => 0,
                        'change_num'        => - $arrData['pa_delivery_num'],
                        'edi_order_code'    => $data->edi_order_code,
                        'supplier_id'       => !empty($data->supplier_id) ? $data->supplier_id : $arrData['pa_supplier_id'],
                        'in_date'           => $data->in_date,
                        'up_date'           => now(),
                    ];
                    $tempDeliveryNum = 0;
                    $arrInserts[] = $arrInsert;
                }
                if ($data->stock_detail_num > 0) {
                    $lineIndex++;
                }
            }
            if (count($arrInserts) !== 0) {
                $this->insert($arrInserts);
            }
        }
    }

    /**
     * Process in stock
     * @param array arrData
     * @return void
     */
    public function add2ReceiptLedgerInStock($arrData)
    {
        $datas = $this->getDataProcessStock($arrData['pa_product_code']);
        $count = count($datas);
        if ($count === 0) {
            $index = $this->getMaxIndex($arrData['pa_product_code']);
            $index = !empty($index->index) ? $index->index : 1;
            $arrInsert = [
                'index'             => $index,
                'line_index'        => 1,
                'stock_type'        => $arrData['pa_stock_type'],
                'stock_detail_type' => $arrData['pa_stock_detail_type'],
                'recognite_key'     => $arrData['pa_order_code'],
                'product_code'      => $arrData['pa_product_code'],
                'price_invoice'     => $arrData['pa_price_invoice'],
                'stock_num'         => $arrData['pa_arrival_num'],
                'stock_detail_num'  => $arrData['pa_arrival_num'],
                'change_num'        => $arrData['pa_arrival_num'],
                'edi_order_code'    => $arrData['pa_order_code'],
                'supplier_id'       => $arrData['pa_supplier_id'],
                'in_date'           => now(),
                'up_date'           => now(),
            ];
            $this->insert($arrInsert);
        } else {
            $index = 0;
            $lineIndex = 1;
            $arrInserts = [];
            $i = 0;
            foreach ($datas as $data) {
                $i++;
                if ($index === 0) {
                    $index = $data->index + 1;
                }
                $arrInsert = [
                    'index'             => $index,
                    'line_index'        => $lineIndex,
                    'stock_type'        => $arrData['pa_stock_type'],
                    'stock_detail_type' => $arrData['pa_stock_detail_type'],
                    'recognite_key'     => 0,
                    'product_code'      => 0,
                    'price_invoice'     => 0,
                    'stock_num'         => 0,
                    'stock_detail_num'  => 0,
                    'supplier_id'       => 0,
                    'change_num'        => 0,
                    'edi_order_code'    => $data->edi_order_code,
                    'in_date'           => now(),
                    'up_date'           => now(),
                ];
                if ($data->line_index === $count && $data->price_invoice === $arrData['pa_price_invoice']
                    && $data->supplier_id === $arrData['pa_supplier_id']) {
                    $arrTemp = $arrInsert;
                    $arrTemp['recognite_key']    = $arrData['pa_order_code'];
                    $arrTemp['product_code']     = $data->product_code;
                    $arrTemp['price_invoice']    = $data->price_invoice;
                    $arrTemp['stock_num']        = $data->stock_num + $arrData['pa_arrival_num'];
                    $arrTemp['stock_detail_num'] = $data->stock_detail_num + $arrData['pa_arrival_num'];
                    $arrTemp['change_num']       = $arrData['pa_arrival_num'];
                    $arrTemp['edi_order_code']   = $arrData['pa_order_code'];
                    $arrTemp['supplier_id']      = !empty($data->supplier_id) ? $data->supplier_id : $arrData['pa_supplier_id'];
                    //$arrTemp['in_date']          = $data->in_date;
                    $arrInserts[] = $arrTemp;
                } elseif ($data->line_index === $count && (($data->price_invoice !== $arrData['pa_price_invoice'] )
                    || ($data->supplier_id !== $arrData['pa_supplier_id']))) {
                    if ($count === $i && $data->stock_num <= 0) {
                        $check = false;
                        if ($data->stock_num + $arrData['pa_arrival_num'] > 0) {
                            $check = true;
                        }
                        $arrTemp = $arrInsert;
                        $arrTemp['recognite_key']    = $arrData['pa_order_code'];
                        $arrTemp['product_code']     = $data->product_code;
                        $arrTemp['price_invoice']    = $check ? $arrData['pa_price_invoice'] : 0;
                        $arrTemp['stock_num']        = $data->stock_num + $arrData['pa_arrival_num'];
                        $arrTemp['stock_detail_num'] = $check ? $data->stock_num + $arrData['pa_arrival_num'] : 0;
                        $arrTemp['change_num']       = $arrData['pa_arrival_num'];
                        $arrTemp['edi_order_code']   = $arrData['pa_order_code'];
                        $arrTemp['supplier_id']      = $arrData['pa_supplier_id'];
                        //$arrTemp['in_date']          = $data->in_date;
                        $arrInserts[] = $arrTemp;
                    } else {
                        $arrTemp = $arrInsert;
                        $arrTemp['recognite_key']    = $arrData['pa_order_code'];
                        $arrTemp['product_code']     = $data->product_code;
                        $arrTemp['price_invoice']    = $data->price_invoice;
                        $arrTemp['stock_num']        = $data->stock_num + $arrData['pa_arrival_num'];
                        $arrTemp['stock_detail_num'] = $data->stock_detail_num;
                        $arrTemp['supplier_id']      = !empty($data->supplier_id) ? $data->supplier_id : $arrData['pa_supplier_id'];
                        $arrTemp['in_date']          = $data->in_date;
                        $arrInserts[] = $arrTemp;
                        $arrTemp = $arrInsert;
                        $arrTemp['line_index']       = $arrTemp['line_index'] + 1;
                        $arrTemp['recognite_key']    = $arrData['pa_order_code'];
                        $arrTemp['product_code']     = $data->product_code;
                        $arrTemp['price_invoice']    = $arrData['pa_price_invoice'];
                        $arrTemp['stock_num']        = $data->stock_num + $arrData['pa_arrival_num'];
                        $arrTemp['stock_detail_num'] = $arrData['pa_arrival_num'];
                        $arrTemp['change_num']       = $arrData['pa_arrival_num'];
                        $arrTemp['edi_order_code']   = $arrData['pa_order_code'];
                        $arrTemp['supplier_id']      = $arrData['pa_supplier_id'];
                        $arrInserts[] = $arrTemp;
                    }
                } elseif ($data->stock_detail_num > 0) {
                    $arrTemp = $arrInsert;
                    $arrTemp['recognite_key']    = $arrData['pa_order_code'];
                    $arrTemp['product_code']     = $data->product_code;
                    $arrTemp['price_invoice']    = $data->price_invoice;
                    $arrTemp['stock_num']        = $data->stock_num + $arrData['pa_arrival_num'];
                    $arrTemp['stock_detail_num'] = $data->stock_detail_num;
                    $arrTemp['change_num']       = 0;
                    $arrTemp['supplier_id']      = !empty($data->supplier_id) ? $data->supplier_id : $arrData['pa_supplier_id'];
                    $arrTemp['in_date']          = $data->in_date;
                    $arrInserts[] = $arrTemp;
                }
                if ($data->stock_detail_num > 0) {
                    $lineIndex++;
                }
            }
            if (count($arrInserts) !== 0) {
                $this->insert($arrInserts);
            }
        }
    }

    /**
     * Get recognite_key by product code
     * @param int productCode
     * @return object
     */
    public function getOrderCodeByProduct($productCode)
    {
        $result = $this->select('recognite_key')
                       ->where('index', function ($query) use ($productCode) {
                            $query->selectRaw('max(`index`)')
                                  ->where('product_code', $productCode)
                                  ->from('dt_receipt_ledger')
                                  ->groupBy('product_code')
                                  ->first();
                       })
                       ->where('product_code', $productCode)
                       ->first();
        return $result;
    }

    /**
     * Get data stock of product
     * @param int productCode
     * @return object
     */
    public function getDataProcessStockRIR($productCode)
    {
        $col = [
            'dt_receipt_ledger.index',
            'dt_receipt_ledger.line_index',
            'dt_receipt_ledger.product_code',
            'dt_receipt_ledger.recognite_key',
            'dt_receipt_ledger.stock_num',
            'dt_receipt_ledger.supplier_id',
            'dt_receipt_ledger.stock_detail_num',
            'dt_receipt_ledger.edi_order_code',
            'dt_receipt_ledger.in_date',
            'dt_receipt_ledger.price_invoice',
        ];
        $result = $this->select($col)
                       ->where('dt_receipt_ledger.product_code', $productCode)
                       ->where('index', function ($query) use ($productCode) {
                           $query->selectRaw('MAX(`index`)')
                                 ->from('dt_receipt_ledger')
                                 ->where('dt_receipt_ledger.product_code', $productCode)
                                 ->groupBy('dt_receipt_ledger.product_code');
                       })
                       ->orderBy('dt_receipt_ledger.line_index')
                       ->get();
        return $result;
    }
    /**
     * Process in stock
     * @param array arrData
     * @return void
     */
    public function add2ReceiptLedgerInStockRIR($arrData)
    {
        $modelP = new MstProduct();
        $datas = $this->getDataProcessStockRIR($arrData['pa_product_code']);
        $arrProduct = $modelP->getDataByProductCode($arrData['pa_product_code']);
        $count = count($datas);
        if ($count === 0) {
            $index = $this->getMaxIndex($arrData['pa_product_code']);
            $index = !empty($index->index) ? $index->index : 1;
            $arrInsert = [
                'index'             => $index,
                'line_index'        => 1,
                'stock_type'        => $arrData['pa_stock_type'],
                'stock_detail_type' => $arrData['pa_stock_detail_type'],
                'recognite_key'     => $arrData['pa_order_code'],
                'product_code'      => $arrData['pa_product_code'],
                'price_invoice'     => $arrProduct->price_invoice,
                'stock_num'         => $arrData['pa_arrival_num'],
                'stock_detail_num'  => $arrData['pa_arrival_num'],
                'change_num'        => $arrData['pa_arrival_num'],
                'edi_order_code'    => $arrData['pa_order_code'],
                'supplier_id'       => $arrData['pa_supplier_id'],
                'in_date'           => now(),
                'up_date'           => now(),
            ];
            $this->insert($arrInsert);
        } else {
            $index = 0;
            $lineIndex = 1;
            $arrInserts = [];
            $i = 0;
            foreach ($datas as $data) {
                $i++;
                if ($index === 0) {
                    $index = $data->index + 1;
                }
                $arrInsert = [
                    'index'             => $index,
                    'line_index'        => $lineIndex,
                    'stock_type'        => $arrData['pa_stock_type'],
                    'stock_detail_type' => $arrData['pa_stock_detail_type'],
                    'recognite_key'     => 0,
                    'product_code'      => 0,
                    'price_invoice'     => 0,
                    'stock_num'         => 0,
                    'stock_detail_num'  => 0,
                    'change_num'        => 0,
                    'edi_order_code'    => $data->edi_order_code,
                    'supplier_id'       => 0,
                    'in_date'           => now(),
                    'up_date'           => now(),
                ];
                if ($data->line_index === $count) {
                    $arrTemp = $arrInsert;
                    $arrTemp['recognite_key']    = !empty($arrData['pa_order_code']) ? $arrData['pa_order_code'] : 'errRIR';
                    $arrTemp['product_code']     = $data->product_code;
                    $arrTemp['price_invoice']    = $data->price_invoice;
                    $arrTemp['stock_num']        = $data->stock_num + $arrData['pa_arrival_num'];
                    $arrTemp['stock_detail_num'] = $data->stock_detail_num + $arrData['pa_arrival_num'];
                    $arrTemp['change_num']       = $arrData['pa_arrival_num'];
                    $arrTemp['supplier_id']      = !empty($data->supplier_id) ? $data->supplier_id : $arrData['pa_supplier_id'];
                    $arrTemp['in_date']          = $data->in_date;
                    $arrInserts[] = $arrTemp;
                } elseif ($data->stock_detail_num > 0) {
                    $arrTemp = $arrInsert;
                    $arrTemp['recognite_key']    = $arrData['pa_order_code'];
                    $arrTemp['product_code']     = $data->product_code;
                    $arrTemp['price_invoice']    = $data->price_invoice;
                    $arrTemp['stock_num']        = $data->stock_num + $arrData['pa_arrival_num'];
                    $arrTemp['stock_detail_num'] = $data->stock_detail_num;
                    $arrTemp['supplier_id']      = !empty($data->supplier_id) ? $data->supplier_id : $arrData['pa_supplier_id'];
                    $arrTemp['in_date']          = $data->in_date;
                    $arrInserts[] = $arrTemp;
                }
                if ($data->stock_detail_num > 0) {
                    $lineIndex++;
                }
            }
            if (count($arrInserts) !== 0) {
                $this->insert($arrInserts);
            }
        }
    }
}
