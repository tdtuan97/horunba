<?php

/**
 * Model for mst_estimate_status table.
 *
 * @package    App\Models\Batches
 * @subpackage MstEstimateStatus
 * @copyright  Copyright (c) 2017 RiverCrane! Corporation. All Rights Reserved.
 * @author     Truong Nghia<truong.van.nghia@rivercrane.vn>
 */

namespace App\Models\Batches;

use Illuminate\Database\Eloquent\Model;

class MstEstimateStatus extends Model
{
    public $timestamps = false;

    /**
    * The database table used by the model.
    * @var string
    */
    protected $table = 'mst_estimate_status';

    /**
    * The database is used by the model.
    * @var string
    */
    protected $connection = 'horunba';

    /**
     * Get data of tabe mst_product_status by date
     * @return object $result
     */
    public function getdata()
    {
        $result = $this->all();
        return $result;
    }
}
