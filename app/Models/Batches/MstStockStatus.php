<?php
/**
 * Model for mst_stock_status table.
 *
 * @package    App\Models\Batches
 * @subpackage MstStockStatus
 * @copyright  Copyright (c) 2017 RiverCrane! Corporation. All Rights Reserved.
 * @author     Truong Nghia<truong.van.nghia@rivercrane.vn>
 */

namespace App\Models\Batches;

use Illuminate\Database\Eloquent\Model;
use DB;
use App\Custom\DBExtend;

class MstStockStatus extends Model
{
    use DBExtend;

    public $timestamps = false;

    /**
    * The database table used by the model.
    * @var string
    */
    protected $table = 'mst_stock_status';

    /**
    * The database is used by the model.
    * @var string
    */
    protected $connection = 'horunba';
    
    /**
    * The primaryKey is used by the model.
    * @var string
    */
    protected $primaryKey = 'product_code';

    /**
    * Get list product code follow number particular
    * @param string $limit       limit to get
    * @param string $offset      start position
    * @return object $result
    */
    public function getProductCode($limit, $offset)
    {
        $result = $this->select('product_code')
                       ->whereNotNull('product_code')
                       ->offset($offset)
                       ->limit($limit)
                       ->get();
        return $result->pluck('product_code')->toArray();
    }

    /**
    * The get data process stock order step 2.
    * @return object $result
    */
    public function getDataStockResult2()
    {
        $col = [
            'mst_stock_status.product_code',
            'ro.arrival_num',
            'ro.order_code',
        ];
        $result = $this->select($col)
                       ->join('dt_replenish_order as ro', 'mst_stock_status.product_code', '=', 'ro.product_code')
                       ->where('replenish_status', '=', 9)
                       ->get();
        return $result;
    }

    /**
    * Update stock when out stock.
    * @param string $paProductCode
    * @param int $paDeliveryNum
    * @return boolean
    */
    public function updateStockOutStock($paProductCode, $paDeliveryNum)
    {
        $arrUpdate = [
            'nanko_num' => DB::raw("nanko_num - {$paDeliveryNum}"),
        ];
        $arrUpdate['order_zan_num']     = DB::raw("order_zan_num - {$paDeliveryNum}");
        $arrUpdate['wait_delivery_num'] = DB::raw("wait_delivery_num - {$paDeliveryNum}");
        return $this->where('product_code', $paProductCode)
                    ->update($arrUpdate);
    }

    /**
    * Update stock when in stock.
    * @param string $paProductCode
    * @param int $paDeliveryNum
    * @return boolean
    */
    public function updateStockInStock($paProductCode, $paArrivalNum, $deliveryType)
    {
        $arrUpdate = [
            'nanko_num' => DB::raw("nanko_num + {$paArrivalNum}"),
        ];
        $deliveryType = (int)$deliveryType;
        if ($deliveryType !== 3) {
            $arrUpdate['rep_orderring_num'] = DB::raw("rep_orderring_num - {$paArrivalNum}");
        }
        return $this->where('product_code', $paProductCode)
                    ->update($arrUpdate);
    }

    /**
    * Update stock when in stock R.
    * @param string $paProductCode
    * @param int $paDeliveryNum
    * @return boolean
    */
    public function updateStockInStockR($paProductCode, $paArrivalNum, $flgWait = false)
    {
        $arrUpdate = [
            'nanko_num' => DB::raw("nanko_num + {$paArrivalNum}"),
        ];
        if ($flgWait) {
            $arrUpdate['wait_delivery_num'] = DB::raw("wait_delivery_num + {$paArrivalNum}");
            $arrUpdate['return_num'] = DB::raw("return_num + {$paArrivalNum}");
        }
        return $this->where('product_code', $paProductCode)
                    ->update($arrUpdate);
    }

    /**
    * Update stock when out stock R.
    * @param string $paProductCode
    * @param int $paDeliveryNum
    * @return boolean
    */
    public function updateStockOutStockR($paProductCode, $paDeliveryNum, $flgWait = false)
    {
        $arrUpdate = [
            'nanko_num' => DB::raw("nanko_num - {$paDeliveryNum}"),
        ];
        if ($flgWait) {
            $arrUpdate['wait_delivery_num'] = DB::raw("wait_delivery_num - {$paDeliveryNum}");
            $arrUpdate['return_num'] = DB::raw("return_num - {$paDeliveryNum}");
        }
        return $this->where('product_code', $paProductCode)
                    ->update($arrUpdate);
    }

    /**
     * Update order zan num
     *
     * @param  string $productCode
     * @param  int    $quantity
     * @param  string $type
     * @return boolean
     */
    public function updateOrderZanNum($productCode, $quantity, $type = null)
    {
        $data = $this->where('product_code', $productCode);
        if ($type === 'increment') {
            return $data->increment('order_zan_num', $quantity);
        } elseif ($type === 'decrement') {
            return $data->decrement('order_zan_num', $quantity);
        } else {
            return $data->update(['order_zan_num' => $quantity]);
        }
    }

    /**
    * Update information stock
    * @param string $productCode
    * @param array $arrUpdate
    * @return object boolean
    */
    public function updateData($productCode, $arrUpdate)
    {
        return $this->where('product_code', $productCode)
                    ->update($arrUpdate);
    }
}
