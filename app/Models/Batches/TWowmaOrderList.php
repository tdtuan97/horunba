<?php

/**
 * Exe sql for hrnb.t_wowma_order_list
 *
 * @package    App\Models
 * @subpackage TWowmaOrderList
 * @copyright  Copyright (c) 2019 RiverCrane! Corporation. All Rights Reserved.
 * @author     Van Qui<van.qui@rivercrane.vn>
 */

namespace App\Models\Batches;

use Illuminate\Database\Eloquent\Model;

class TWowmaOrderList extends Model
{
    public $timestamps = false;
    /**
    * The database table used by the model.
    * @var string
    */
    protected $table = 't_wowma_order_list';
    /**
    * The database is used by the model.
    * @var string
    */
    protected $connection = 'horunba';
    
    /**
     * Get order by order id
     *
     * @param  array  $orderIds
     * @return object
     */
    public function getOrderByOrderId($orderIds)
    {
        $data = $this->select('order_id')->whereIn('order_id', $orderIds)->get();
        return $data;
    }
    
    /**
     * Get order new
     *
     * @return object
     */
    public function getOrderNew()
    {
        $data = $this->select('order_id')->where('is_got', 0)->get();
        return $data;
    }
    
    /**
     * Update data by condition
     *
     * @param  array   $cond
     * @param  array   $data
     * @return boolean
     */
    public function updateData($cond, $data)
    {
        return $this->where($cond)->update($data);
    }
}
