<?php
/**
 * Exe sql for hrnb.t_wowma_order
 *
 * @package    App\Models
 * @subpackage TWowmaOrder
 * @copyright  Copyright (c) 2019 RiverCrane! Corporation. All Rights Reserved.
 * @author     Van Qui<van.qui@rivercrane.vn>
 */

namespace App\Models\Batches;

use Illuminate\Database\Eloquent\Model;
use App\Custom\DBExtend;

class TWowmaOrder extends Model
{
    use DBExtend;
    public $timestamps = false;
    /**
    * The database table used by the model.
    * @var string
    */
    protected $table = 't_wowma_order';
    /**
    * The database is used by the model.
    * @var string
    */
    protected $connection = 'horunba';

    /**
     * get data
     *
     * @return object
     */
    public function getData()
    {
        $cols = [
            'order_id', 'coupon_total_price', 'use_point', 'total_price', 'request_price',
            'order_date', 'postage_price', 'total_sale_price', 'delivery_request_time',
            'settle_status', 'settlement_name', 'user_comment', 'charge_price',
            'orderer_name', 'orderer_kana', 'mail_address', 'orderer_phone_number1',
            'orderer_phone_number2', 'orderer_zip_code', 'orderer_address', 'sender_name',
            'sender_kana', 'sender_phone_number1', 'sender_phone_number2', 'sender_zip_code',
            'sender_address', 'use_au_point_price', 'delivery_request_day','use_point','card_jadgement'
        ];
        $data = $this->select($cols)
            ->where('process_flg', 0)
            ->where('is_deleted', 0)
            ->get();
        return $data;
    }

    /**
     * Update process flag.
     *
     * @param  string  $key
     * @return boolean $result
     */
    public function updateProcessFlg($key)
    {
        $result = $this->where('order_id', $key)
             ->update(['process_flg' => 1]);
        return $result;
    }
}
