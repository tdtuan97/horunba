<?php
/**
 * Model for dt_web_mapping table.
 *
 * @package    App\Models\Batches
 * @subpackage DtWebMapping
 * @copyright  Copyright (c) 2018 RiverCrane! Corporation. All Rights Reserved.
 * @author     Van Qui<van.qui@rivercrane.vn>
 */

namespace App\Models\Batches;

use Illuminate\Database\Eloquent\Model;
use Config;
use DB;
use App\Custom\DBExtend;

class DtWebMapping extends Model
{
    use DBExtend;
    public $timestamps = false;

    /**
    * The database table used by the model.
    * @var string
    */
    protected $table = 'dt_web_mapping';

    /**
    * The database is used by the model.
    * @var string
    */
    protected $connection = 'horunba';

    /**
    * Update data
    * @param string $orderNumber
    * @param int $webOrderStatus
    * @var string
    */
    public function updateData($orderNumber, $webOrderStatus)
    {
        $this->where('received_order_id', $orderNumber)
            ->update([
                'web_order_status' => $webOrderStatus,
                'is_corrected'     => 1,
                'different_type'   => '',
                'finished_date'    => date('Y-m-d H:i:s'),
                'up_date'          => date('Y-m-d H:i:s')
            ]);
    }
}
