<?php

/**
 * Model for t_order table.
 *
 * @package    App\Models\Batches
 * @subpackage TOrder
 * @copyright  Copyright (c) 2017 RiverCrane! Corporation. All Rights Reserved.
 * @author     Truong Nghia<truong.van.nghia@rivercrane.vn>
 */

namespace App\Models\Batches;

use Illuminate\Database\Eloquent\Model;

class TOrder extends Model
{
    public $timestamps = false;

    /**
    * The database table used by the model.
    * @var string
    */
    protected $table = 't_order';

    /**
    * The database is used by the model.
    * @var string
    */
    protected $connection = 'edi';

    /**
     * Get list data process EDI result
     * @return object $result
     */
    public function getDataEdiResult()
    {
        $col = [
            'od.t_order_detail_id',
            'od.m_order_type_id',
            'od.shipment_date_plan',
            'tn.shipment_date',
            'tn.arrival_date',
            'od.stock_quantity',
            'od.shortage_quantity',
            'od.other',
            'ots.order_code',
            'ots.edi_order_code',
            'ots.product_code',
            'ots.order_num',
            'opd.receive_id',
            'opd.detail_line_num',
            'opd.sub_line_num',
            'opd.product_status',
            'opd.suplier_id',
            'opd.delivery_date',
            'opd.delivery_type',
            'mst_order.ship_wish_date',
            'mst_order.arrive_type',
            'mst_supplier.delivery_days',
        ];
        $sub = '(select deli_days from hrnb.mst_postal where postal_code = mst_customer.zip_code limit 1) AS deli_days';
        $result = $this->select($col)
                       ->selectRaw($sub)
                       ->join('t_order_detail as od', 't_order.order_code', '=', 'od.order_code')
                       ->join('hrnb.dt_order_to_supplier as ots', 'ots.edi_order_code', '=', 'od.order_code')
                       ->leftJoin('edi.t_nouhin AS tn', 'tn.t_nouhin_id', '=', 'od.t_nouhin_id')
                       ->leftJoin('hrnb.dt_order_product_detail AS opd', 'opd.order_code', '=', 'ots.order_code')
                       ->leftJoin('hrnb.mst_order', 'mst_order.receive_id', '=', 'opd.receive_id')
                       ->leftJoin('hrnb.mst_order_detail', function ($sub) {
                            $sub->on('mst_order_detail.receive_id', '=', 'opd.receive_id')
                                ->on('mst_order_detail.detail_line_num', '=', 'opd.detail_line_num');
                       })
                       ->join('hrnb.mst_supplier', 'mst_supplier.supplier_cd', '=', 'ots.supplier_id')
                       ->leftJoin('hrnb.mst_customer', 'mst_order_detail.receiver_id', '=', 'mst_customer.customer_id')
                       ->where(function ($subWhere) {
                            $subWhere->where(function ($subW) {
                                $subW->whereIn('od.filmaker_flg', [0,1])
                                    ->where('od.del_flg', 0)
                                    ->where('od.valid_flg', 1)
                                    ->where('ots.process_status', '<', '3');
                            })
                            ->orWhere(function ($subW) {
                                $subW->where('ots.process_status', 2)
                                    ->where('od.m_order_type_id', 1);
                            });
                       })
                       ->where('ots.edi_order_code', '<>', '')
                       ->get();
        return $result;
    }

    /**
     * Get list data process stock result step 1
     * @return object $result
     */
    public function getDataStockResult1()
    {
        $col = [
            'od.t_order_detail_id',
            'od.m_order_type_id',
            'od.other',
            'od.shipment_date_plan',
            'od.shipment_date',
            'od.stock_quantity',
            'od.shortage_quantity',
            'od.order_code',
        ];
        $result = $this->select($col)
                       ->join('t_order_detail as od', 't_order.t_order_id', '=', 'od.t_order_id')
                       ->whereIn('od.filmaker_flg', [0,1])
                       ->where('od.del_flg', '=', 0)
                       ->where('od.valid_flg', '=', 1)
                       ->get();
        return $result;
    }

    /**
     * Update data
     *
     * @param  array   $key
     * @param  array   $data
     * @return boolean
     */
    public function updateData($key, $data)
    {
        return $this->where($key)->update($data);
    }
    /**
     * Get order id of table t_order
     * @author lam.vinh
     * @param  string $orderCode
     * @return int order id
     */
    public function getOrderId($orderCode)
    {
        $result = $this->select('t_order_id')->where('order_code', $orderCode)->first();
        return $result;
    }
}
