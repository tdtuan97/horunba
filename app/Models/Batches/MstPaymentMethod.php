<?php

/**
 * Model for mst_payment_method table.
 *
 * @package    App\Models\Batches
 * @subpackage MstPaymentMethod
 * @copyright  Copyright (c) 2017 RiverCrane! Corporation. All Rights Reserved.
 * @author     Truong Nghia<truong.van.nghia@rivercrane.vn>
 */

namespace App\Models\Batches;

use Illuminate\Database\Eloquent\Model;

class MstPaymentMethod extends Model
{
    public $timestamps = false;

    /**
    * The database table used by the model.
    * @var string
    */
    protected $table = 'mst_payment_method';

    /**
    * The database is used by the model.
    * @var string
    */
    protected $connection = 'horunba';

    /**
    * Get data payment method by mall id
    * @param type int $mall
    * @return obj data method
    */
    public function getDataByMall($mall)
    {
        $result = $this->where('mall_id', '=', $mall)
                       ->get();
        return $result;
    }

    /**
    * Get data payment method
    * @param type int $mallId
    * @param type int $paymentMethod
    * @return obj data
    */
    public function getPaymentMethod($mallId, $paymenName)
    {
        $result = $this->where('mall_id', '=', $mallId)
                       ->where('payment_name', '=', $paymenName)
                       ->first();
        return $result;
    }
}
