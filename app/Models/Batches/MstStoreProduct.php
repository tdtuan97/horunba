<?php

/**
 * Model for mst_store_product table.
 *
 * @package    App\Models\Batches
 * @subpackage MstStoreProduct
 * @copyright  Copyright (c) 2018 RiverCrane! Corporation. All Rights Reserved.
 * @author     Le Hung<le.hung.rcvn2012@gmail.com>
 */

namespace App\Models\Batches;

use Illuminate\Database\Eloquent\Model;

class MstStoreProduct extends Model
{
    public $timestamps = false;

    /**
    * The database table used by the model.
    * @var string
    */
    protected $table = 'mst_store_product';

    /**
    * The database is used by the model.
    * @var string
    */
    protected $connection = 'horunba';

    /**
    * The primary key of table.
    * @var string
    */
    protected $primaryKey = 'product_code';
    /**
    * Set type of primary key.
    * @var boolean
    */
    public $incrementing = false;
    /**
    * Get max store product id
    * @return object
    */
    public function getMaxStoreProductId()
    {
        $result = $this->selectRaw('MAX(store_product_id) AS store_product_id')
                       ->whereRaw("store_product_id like '20180%'")
                       ->first();
        return $result;
    }
    /**
     * Update data
     *
     * @param  array $dataWhere for condition
     * @param  array $dataUpdate for update
     * @return object
     */
    public function updateData($dataWhere, $dataUpdate)
    {
        $data = $this->where($dataWhere)->update($dataUpdate);
        return $data;
    }
    /**
     * Update data in
     *
     * @param  array $dataWhere for condition
     * @param  array $dataUpdate for update
     * @return object
     */
    public function updateDataIn($dataWhere, $dataUpdate)
    {
        $data = $this->whereIn('product_code', $dataWhere)->update($dataUpdate);
        return $data;
    }
    /**
     * Get data
     *
     *
     * @return object $result
     */
    public function getDataSyncContent2($limit = 5000)
    {
        $data = $this->select('mst_product.df_handling')
        ->addSelect('mst_product.product_name')
        ->addSelect('mst_store_product.product_code')
        ->leftJoin('mst_product', 'mst_product.product_code', '=', 'mst_store_product.product_code')
        ->whereRaw('mst_product.df_handling != mst_store_product.df_handling')
        ->limit($limit)
        ->get();
        return $data;
    }
    /**
     * Get data
     *
     *
     * @return object $result
     */
    public function getDataSyncContent3($limit = 5000)
    {
        $data = $this->select('mst_store_product.product_code')
        ->join('edi.mst_product as emp', function ($join) {
            $join->on('mst_store_product.product_code', '=', 'emp.product_code')
            ->where('emp.is_live', '=', 1);
        })
        ->where('emp.cheetah_status', '=', 3)
        ->orWhere('emp.cheetah_status', '=', 4)
        ->limit($limit)
        ->get();
        return $data;
    }
    /**
     * Get data order detail info
     *
     *
     * @return object $result
     */
    public function getDataSyncContent4($limit = 5000)
    {
        $data = $this->select('mst_product.product_jan')
        ->addSelect('mst_store_product.product_code')
        ->leftJoin('mst_product', 'mst_product.product_code', '=', 'mst_store_product.product_code')
        ->whereRaw('mst_product.product_jan != mst_store_product.product_jan')
        ->limit($limit)
        ->get();
        return $data;
    }
    /**
     * Get data order detail info
     *
     *
     * @return object $result
     */
    public function getDataSyncContent5($limit)
    {
        $data = $this->select('mst_product.product_code')
        ->addSelect('mst_product.product_name')
        ->addSelect('mst_product.product_jan')
        ->addSelect('mst_product.df_handling')
        ->addSelect('mst_product.price_invoice')
        ->addSelect('mst_product.product_size')
        ->addSelect('mst_product.product_color')
        ->addSelect('mst_product.maker_cd')
        ->addSelect('mst_product.product_genre')
        ->addSelect('mst_product.product_maker_code')
        ->addSelect('dt_price_mall.price_calc_diy_komi')
        ->rightJoin('mst_product', 'mst_product.product_code', '=', 'mst_store_product.product_code')
        ->leftJoin('master_plus.dt_price_mall', 'mst_product.product_code', '=', 'dt_price_mall.product_code')
        ->where('mst_product.df_handling', '=', 1)
        ->whereNull('mst_store_product.product_code')
        ->limit($limit)
        ->get();
        return $data;
    }
    /**
     * Get data order detail info
     *
     *
     * @return object $result
     */
    public function getDataSyncContent5IsUpdate($limit)
    {
        $col = [
            'store_product_id',
            'store_product_category_id',
            'df_handling',
            'product_jan',
            'product_price',
            'product_cost',
            'product_size',
            'product_color',
            'product_tag',
            'product_url',
            'product_code',
        ];
        $data = $this->select($col)
        ->selectRaw("REPLACE(store_product_name,'★','') as store_product_name")
        ->where('is_updated', '=', 1)
        ->limit($limit)
        ->get();
        return $data;
    }
    /**
     * Get data for send mail
     *
     *
     * @return object $result
     */
    public function getDataForSendMail($limit)
    {
        $data = $this->select('store_product_id')
        ->addSelect('product_jan')
        ->addSelect('product_jan_new')
        ->addSelect('store_product_name')
        ->addSelect('product_code')
        ->where('is_send_mail', '=', 1)
        ->limit($limit)
        ->get();
        return $data;
    }

    /**
     * Get data smaregi del product
     *
     * @param type $limit
     */
    public function getSmaregiDelProduct($limit)
    {
        $data = $this->select('product_code', 'store_product_id')->where('is_updated', 2)->get();
        return $data;
    }
}
