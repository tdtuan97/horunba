<?php

/**
 * Model for dt_estimation table.
 *
 * @package    App\Models\Batches
 * @subpackage DtEstimation
 * @copyright  Copyright (c) 2017 RiverCrane! Corporation. All Rights Reserved.
 * @author     Truong Nghia<truong.van.nghia@rivercrane.vn>
 */

namespace App\Models\Batches;

use Illuminate\Database\Eloquent\Model;

class DtEstimation extends Model
{
    public $timestamps = false;

    /**
    * The database table used by the model.
    * @var string
    */
    protected $table = 'dt_estimation';

    /**
    * The database is used by the model.
    * @var string
    */
    protected $connection = 'horunba';

    /**
    * The get data process order zaiko result step 1.
    * @return object $result
    */
    public function getDataZaikoResult1()
    {
        $col = [
            'estimate_code',
            'estimate_date',
        ];
        $result = $this->select($col)
                       ->join('hrnb.dt_order_product_detail as dopd', function ($join) {
                            $join->on('dt_estimation.receive_id', '=', 'dopd.receive_id')
                                 ->on('dt_estimation.detail_line_num', '=', 'dopd.detail_line_num')
                                 ->on('dt_estimation.sub_line_num', '=', 'dopd.sub_line_num');
                       })
                       ->where('product_status', '=', PRODUCT_STATUS['ESTIMATING'])
                       ->where('wait_days', '<', 7)
                       ->get();
        return $result;
    }

    /**
    * The update infomation.
    * @param string $arrEstimateCode
    * @param string $arrUpdate
    * @return boolean
    */
    public function updateData($arrEstimateCode, $arrUpdate)
    {
        $result = $this->whereIn('estimate_code', $arrEstimateCode)
                      ->update($arrUpdate);
        return $result;
    }
}
