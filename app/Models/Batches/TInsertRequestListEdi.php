<?php

/**
 * Model for tinsertRequestList table.
 *
 * @package    App\Models\Batches
 * @subpackage TInsertRequestList
 * @copyright  Copyright (c) 2018 RiverCrane! Corporation. All Rights Reserved.
 * @author     Truong Nghia<truong.van.nghia@rivercrane.vn>
 */

namespace App\Models\Batches;

use Illuminate\Database\Eloquent\Model;

class TInsertRequestListEdi extends Model
{
    public $timestamps = false;

    /**
    * The database table used by the model.
    * @var string
    */
    protected $table = 't_insertRequestList';

    /**
    * The database is used by the model.
    * @var string
    */
    protected $connection = 'edi';
}
