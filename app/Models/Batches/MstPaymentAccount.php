<?php

/**
 * Model for mst_payment_account table.
 *
 * @package    App\Models\Batches
 * @subpackage MstPaymentAccount
 * @copyright  Copyright (c) 2017 RiverCrane! Corporation. All Rights Reserved.
 * @author     Truong Nghia<truong.van.nghia@rivercrane.vn>
 */

namespace App\Models\Batches;

use Illuminate\Database\Eloquent\Model;

class MstPaymentAccount extends Model
{
    public $timestamps = false;

    /**
    * The database table used by the model.
    * @var string
    */
    protected $table = 'mst_payment_account';

    /**
    * The database is used by the model.
    * @var string
    */
    protected $connection = 'horunba';
}
