<?php

/**
 * Model for t_insert_shipping table.
 *
 * @package    App\Models\Batches
 * @subpackage TInsertShippingHrnb
 * @copyright  Copyright (c) 2017 RiverCrane! Corporation. All Rights Reserved.
 * @author     Truong Nghia<truong.van.nghia@rivercrane.vn>
 */

namespace App\Models\Batches;

use Illuminate\Database\Eloquent\Model;

class TInsertShippingHrnb extends Model
{
    public $timestamps = false;

    /**
    * The database table used by the model.
    * @var string
    */
    protected $table = 't_insert_shipping';

    /**
    * The database is used by the model.
    * @var string
    */
    protected $connection = 'horunba';
}
