<?php

/**
 * Exe sql for hrnb.t_wowma_order_detail
 *
 * @package    App\Models
 * @subpackage TWowmaOrderDetail
 * @copyright  Copyright (c) 2019 RiverCrane! Corporation. All Rights Reserved.
 * @author     Van Qui<van.qui@rivercrane.vn>
 */

namespace App\Models\Batches;

use Illuminate\Database\Eloquent\Model;
use App\Custom\DBExtend;

class TWowmaOrderDetail extends Model
{
    use DBExtend;
    public $timestamps = false;
    /**
    * The database table used by the model.
    * @var string
    */
    protected $table = 't_wowma_order_detail';
    /**
    * The database is used by the model.
    * @var string
    */
    protected $connection = 'horunba';
    
    /**
     * Get order detail before insert order.
     * @param  array  $receiveId value receive_id for condition where
     * @return object $result
     */
    public function getInfoOrderDetail($receiveId)
    {
        $result = $this->join('mst_order', function ($join) use ($receiveId) {
            $join->on('order_id', '=', 'mst_order.received_order_id')
                 ->where('receive_id', '=', $receiveId);
        })->get();
        return $result;
    }
}
