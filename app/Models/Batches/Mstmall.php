<?php
/**
 * Model for mst_mall table.
 *
 * @package    App\Models\Batches
 * @subpackage Mstmall
 * @copyright  Copyright (c) 2017 RiverCrane! Corporation. All Rights Reserved.
 * @author     Truong Nghia<truong.van.nghia@rivercrane.vn>
 */

namespace App\Models\Batches;

use Illuminate\Database\Eloquent\Model;

class Mstmall extends Model
{
    public $timestamps = false;

    /**
    * The database table used by the model.
    * @var string
    */
    protected $table = 'mst_mall';

    /**
    * The database is used by the model.
    * @var string
    */
    protected $connection = 'horunba';

    /**
     * Get data of tabe t_hoten_csv by date
     *
     * @param string $name  value name_en for condition where
     * @return object $result
     */
    public function getInfoMallByName($name)
    {
        $result = $this->where('name_en', '=', $name)->first();
        return $result;
    }
}
