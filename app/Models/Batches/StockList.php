<?php
/**
 * Model for stock_list table
 *
 * @package    App\Models
 * @subpackage StockList
 * @copyright  Copyright (c) 2018 RiverCrane! Corporation. All Rights Reserved.
 * @author     van.qui<van.qui.rcvn2012@gmail.com>
 */

namespace App\Models\Batches;

use Illuminate\Database\Eloquent\Model;
use DB;
use App\Custom\DBExtend;

class StockList extends Model
{
    use DBExtend;
    
    public $timestamps = false;
    /**
    * The database table used by the model.
    * @var string
    */
    protected $table = 'stock_list';
    /**
    * The database is used by the model.
    * @var string
    */
    protected $connection = 'api';
    
    /**
     * Update data
     *
     * @param  array  $cond
     * @param  array  $data
     * @return bool
     */
    public function updateData($cond, $data)
    {
        return $this->where($cond)->update($data);
    }
}
