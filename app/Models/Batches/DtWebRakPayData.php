<?php

/**
 * Model for dt_web_rak_pay_data table.
 *
 * @package    App\Models\Batches
 * @subpackage DtWebRakPayData
 * @copyright  Copyright (c) 2017 RiverCrane! Corporation. All Rights Reserved.
 * @author     Truong Nghia<truong.van.nghia@rivercrane.vn>
 */

namespace App\Models\Batches;

use Illuminate\Database\Eloquent\Model;

class DtWebRakPayData extends Model
{
    public $timestamps = false;

    /**
    * The database table used by the model.
    * @var string
    */
    protected $table = 'dt_web_rak_pay_data';

    /**
    * The database is used by the model.
    * @var string
    */
    protected $connection = 'horunba';

    /**
     * Get data process web rakuten
     * @return object
     */
    public function getDataProcessWebRakuten()
    {
        $col = [
            'dt_web_rak_pay_data.order_number',
            'dt_web_rak_pay_data.settlement_name',
            'dt_web_rak_pay_data.request_price',
            'dt_web_rak_pay_data.status',
            'mst_order.mall_id',
            'mst_order.order_status',
            'mst_order.payment_method',
            'mst_order.request_price AS order_request_price',
        ];
        $result = $this->select($col)
                       ->leftjoin('mst_order', 'dt_web_rak_pay_data.order_number', '=', 'mst_order.received_order_id')
                       ->where('process_flg', 0)
                       ->where('is_delete', 0)
                       ->groupBy('dt_web_rak_pay_data.order_number')
                       ->get();
        return $result;
    }
}
