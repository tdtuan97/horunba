<?php
/**
 * Model for log_update_status_order table.
 *
 * @package    App\Models\Batches
 * @subpackage LogUpdateStatusOrder
 * @copyright  Copyright (c) 2018 RiverCrane! Corporation. All Rights Reserved.
 * @author     Truong Nghia<truong.van.nghia@rivercrane.vn>
 */

namespace App\Models\Batches;

use Illuminate\Database\Eloquent\Model;

class LogUpdateStatusOrder extends Model
{
    public $timestamps = false;

    /**
    * The database table used by the model.
    * @var string
    */
    protected $table = 'log_update_status_order';

    /**
    * The database is used by the model.
    * @var string
    */
    protected $connection = 'horunba';

    public function insertLog($arrkey, $arrUpdate, $className, $key = 'receive_id')
    {
        $arrInsert = [];
        foreach ($arrkey as $k => $id) {
            $arrInsert[$k][$key]       = $id;
            $arrInsert[$k]['order_status']     = isset($arrUpdate['order_status']) ?
                                                    $arrUpdate['order_status'] : null;
            $arrInsert[$k]['order_sub_status'] = isset($arrUpdate['order_sub_status']) ?
                                                    $arrUpdate['order_sub_status'] : null;
            $arrInsert[$k]['class_name_call']  = $className;
        }
        return $this->insert($arrInsert);
    }
}
