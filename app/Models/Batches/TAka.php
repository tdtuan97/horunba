<?php

/**
 * Model for t_aka table.
 *
 * @package    App\Models\Batches
 * @subpackage TAka
 * @copyright  Copyright (c) 2017 RiverCrane! Corporation. All Rights Reserved.
 * @author     Truong Nghia<truong.van.nghia@rivercrane.vn>
 */

namespace App\Models\Batches;

use Illuminate\Database\Eloquent\Model;

class TAka extends Model
{
    public $timestamps = false;

    /**
    * The database table used by the model.
    * @var string
    */
    protected $table = 't_aka';

    /**
    * The database is used by the model.
    * @var string
    */
    protected $connection = 'edi';

    /**
    * Get max return_code
    * @return object boolean
    */
    public function getMaxReturn($supplierId)
    {
        return $this->select('return_code')
                    ->where('return_code', 'like', 'r-' . $supplierId . '-4%')
                    ->orderBy('t_aka_id', 'DESC')
                    ->first();
    }
}
