<?php
/**
 * Model for dt_replenish_order table.
 *
 * @package    App\Models\Batches
 * @subpackage DtRelenishOrder
 * @copyright  Copyright (c) 2017 RiverCrane! Corporation. All Rights Reserved.
 * @author     Truong Nghia<truong.van.nghia@rivercrane.vn>
 */

namespace App\Models\Batches;

use Illuminate\Database\Eloquent\Model;

class DtRelenishOrder extends Model
{
    public $timestamps = false;

    /**
    * The database table used by the model.
    * @var string
    */
    protected $table = 'dt_replenish_order';

    /**
    * The database is used by the model.
    * @var string
    */
    protected $connection = 'horunba';

    /**
    * The get max order code.
    * @return object $result
    */
    public function getMaxOrderCode()
    {
        $result = $this->select('order_code')
                       ->where('order_code', 'like', 'HR%')
                       ->orderBy('order_code', 'DESC')
                       ->first();
        return $result;
    }
}
