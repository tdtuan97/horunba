<?php

/**
 * Exe sql for hrnb.dt_web_yah_data
 *
 * @package    App\Models
 * @subpackage DtWebYahData
 * @copyright  Copyright (c) 2018 RiverCrane! Corporation. All Rights Reserved.
 * @author     Van Qui<van.qui.nghia@rivercrane.vn>
 */

namespace App\Models\Batches;

use Illuminate\Database\Eloquent\Model;

class DtWebYahData extends Model
{
    public $timestamps = false;
    /**
    * The database table used by the model.
    * @var string
    */
    protected $table = 'dt_web_yah_data';
    /**
    * The database is used by the model.
    * @var string
    */
    protected $connection = 'horunba';
    /**
     * Get data process web rakuten
     * @return object
     */
    public function getDataProcessWebYahoo()
    {
        $col = [
            'dt_web_yah_data.order_id',
            'dt_web_yah_data.paymethod_name',
            'dt_web_yah_data.settle_amount',
            'dt_web_yah_data.order_status',
            'mst_order.mall_id',
            'mst_order.order_status AS order_order_status',
            'mst_order.payment_method',
            'mst_order.request_price',
        ];
        $result = $this->select($col)
                       ->leftjoin('mst_order', 'dt_web_yah_data.order_id', '=', 'mst_order.received_order_id')
                       ->where('process_flg', 0)
                       ->where('is_delete', 0)
                       ->groupBy('dt_web_yah_data.order_id')
                       ->get();
        return $result;
    }
}
