<?php
/**
 * Model for dt_uri_mapping table.
 *
 * @package    App\Models\Batches
 * @subpackage DtUriMapping
 * @copyright  Copyright (c) 2019 RiverCrane! Corporation. All Rights Reserved.
 * @author     Truong Nghia<truong.van.nghia@crivercrane.vn>
 */

namespace App\Models\Batches;

use Illuminate\Database\Eloquent\Model;
use DB;
use App\Custom\DBExtend;

class DtUriMapping extends Model
{
    use DBExtend;

    public $timestamps = false;

    /**
    * The database table used by the model.
    * @var string
    */
    protected $table = 'dt_uri_mapping';

    /**
    * The database is used by the model.
    * @var string
    */
    protected $connection = 'horunba';

    /**
     * Update data
     *
     * @param  array    $conds
     * @param  array    $data
     * @return boolean
     */
    public function updateData($conds, $data)
    {
        return $this->where($conds)->update($data);
    }
}
