<?php

/**
 * Model for t_amazon_order table.
 *
 * @package    App\Models\Batches
 * @subpackage TAmazonOrder
 * @copyright  Copyright (c) 2017 RiverCrane! Corporation. All Rights Reserved.
 * @author     Truong Nghia<truong.van.nghia@rivercrane.vn>
 */

namespace App\Models\Batches;

use Illuminate\Database\Eloquent\Model;
use DB;

class TAmazonOrder extends Model
{
    public $timestamps = false;

    /**
    * The database table used by the model.
    * @var string
    */
    protected $table = 't_amazon_order';

    /**
    * The database is used by the model.
    * @var string
    */
    protected $connection = 'horunba';

    /**
     * Get data check same data
     * @param int $limit limit get datas
     * @return object $result
     */
    public function getDataCheck($limit)
    {
        $objId = $this->select('order_id')
                      ->where('process_flg', '=', '0')
                      ->groupBy('order_id')
                      ->limit($limit)
                      ->get();
        $arrId = array_column($objId->toArray(), 'order_id');
        $result = $this->select(['id', 'order_id', 'sku'])
                       ->whereIn('order_id', $arrId)
                       ->groupBy(['order_id', 'sku'])
                       ->get();
        return $result;
    }

    /**
     * Get data of tabe t_amazon_order by date
     * @param int $limit limit get datas
     * @return object $result
     */
    public function getData($arrId)
    {
        $result = $this->whereIn('order_id', $arrId)
                       ->where('is_delete', 0)
                       ->where('process_flg', '=', '0')
                       ->orderBy('order_id')
                       ->orderBy('id')
                       ->get();
        return $result;
    }

   /**
     * Get order detail before insert order.
     * @param  array  $receiveId value receive_id for condition where
     * @return object $result
     */
    public function getInfoOrderDetail($receiveId)
    {
        $result = $this->join('mst_order', function ($join) use ($receiveId) {
                            $join->on('order_id', '=', 'mst_order.received_order_id')
                                 ->where('receive_id', '=', $receiveId)
                                 ->where('is_delete', 0);
        })
                       ->get();
        return $result;
    }

    /**
     * Calculate total amount .
     *
     * @return array $arrResult
     */
    public function getAllTypePrice()
    {
        $col = ['order_id', 'payment_method_fee', 'already_paid'];
        $totalPrice = "(SUM(item_price) + payment_method_fee) as total_price";
        $shipCharge = "SUM(shipping_price) as ship_charge";
        $goodsPrice = "SUM(item_price) as goods_price";
        $goodsTax   = "SUM(item_tax) as goods_tax";
        $result = $this->select($col)
                       ->selectRaw($totalPrice)
                       ->selectRaw($shipCharge)
                       ->selectRaw($goodsPrice)
                       ->selectRaw($goodsTax)
                       ->where('process_flg', '=', '0')
                       ->where('is_delete', 0)
                       ->groupBy(['order_id', 'payment_method_fee', 'already_paid'])
                       ->get();
        $arrResult = [];
        foreach ($result as $value) {
            $arrResult[$value->order_id]['total_price'] = (int)$value->total_price + (int)$value->ship_charge;
            $arrResult[$value->order_id]['ship_charge'] = (int)$value->ship_charge;
            $arrResult[$value->order_id]['goods_price'] = (int)$value->goods_price;
            $arrResult[$value->order_id]['goods_tax']   = (int)$value->goods_tax;
            $arrResult[$value->order_id]['request_price'] = (int)$value->total_price +
                                                            (int)$value->ship_charge -
                                                            (int)$value->already_paid;
        }
        return $arrResult;
    }

   /**
     * Update process flag before get information order.
     *
     * @param  array  $arrKey list id need update
     * @return object $result
     */
    public function updateProcessFlg($arrKey)
    {
        $result = $this->whereIn('id', $arrKey)
             ->update(['process_flg' => 1]);
        return $result;
    }
   /**
     * Check existed row.
     *
     * @param  array  $dataWhere condition for updating
     * @return object $result
     */
    public function checkExists($dataWhere)
    {
        $result = $this->select('id')->where($dataWhere)->first();
        return $result;
    }
}
