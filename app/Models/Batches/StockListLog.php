<?php
/**
 * Model for stock_list_log table
 *
 * @package    App\Models
 * @subpackage StockListLog
 * @copyright  Copyright (c) 2018 RiverCrane! Corporation. All Rights Reserved.
 * @author     van.qui<van.qui.rcvn2012@gmail.com>
 */

namespace App\Models\Batches;

use Illuminate\Database\Eloquent\Model;
use DB;

class StockListLog extends Model
{
    public $timestamps = false;
    /**
    * The database table used by the model.
    * @var string
    */
    protected $table = 'stock_list_log';
    /**
    * The database is used by the model.
    * @var string
    */
    protected $connection = 'logs';
    
    /**
     * log data
     *
     * @param  type  $productCodes
     * @return bool
     */
    public function logData($productCodes)
    {
        if (!is_array($productCodes) || count($productCodes) === 0) {
            return false;
        }
        $sql = "INSERT INTO
                    logs.stock_list_log (
                        product_code, num_nanko, glsc_num, order_zan_num, num_trusco, num_yamazen,
                        num_sold_product, is_change_stock_a,
                        is_change_stock_y, is_change_stock_r
                    )
                SELECT
                    product_code, num_nanko_a, glsc_num, order_zan_num, num_trusco, num_yamazen,
                    num_sold_product, is_change_stock_a,
                    is_change_stock_y, is_change_stock_r
                FROM
                    api.stock_list
                WHERE
                    product_code IN ('" . implode("','", $productCodes) . "')";
        return DB::insert($sql);
    }
}
