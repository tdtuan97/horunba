<?php

/**
 * Exe sql for hrnb.yahoo_token
 *
 * @package    App\Models
 * @subpackage YahooToken
 * @copyright  Copyright (c) 2018 RiverCrane! Corporation. All Rights Reserved.
 * @author     van.qui<van.qui.rcvn2012@gmail.com>
 */

namespace App\Models\Batches;

use Illuminate\Database\Eloquent\Model;

class YahooToken extends Model
{
    public $timestamps = false;
    /**
    * The database table used by the model.
    * @var string
    */
    protected $table = 'yahoo_token';
    /**
    * The database is used by the model.
    * @var string
    */
    protected $connection = 'api';
    /**
    * The primary key.
    * @var string
    */
    protected $primaryKey = 'ID';
    /**
    * The type of primary key.
    * @var string
    */
    protected $keyType = 'string';
}
