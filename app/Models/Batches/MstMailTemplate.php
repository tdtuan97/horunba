<?php

/**
 * Exe sql for hrnb.mst_mail_template
 *
 * @package    App\Models
 * @subpackage MstMailTemplate
 * @copyright  Copyright (c) 2017 RiverCrane! Corporation. All Rights Reserved.
 * @author     le.hung<le.hung.rcvn2012@gmail.com>
 */

namespace App\Models\Batches;

use Illuminate\Database\Eloquent\Model;

class MstMailTemplate extends Model
{
    public $timestamps = false;
    /**
    * The database table used by the model.
    * @var string
    */
    protected $table = 'mst_mail_template';
    /**
    * The database is used by the model.
    * @var string
    */
    protected $connection = 'horunba';
    /**
    * The primary key.
    * @var string
    */
    protected $primaryKey = 'mail_id';

    public function getDataCancelOrder($id)
    {
        $cols = ['mail_id', 'template_name', 'mail_subject', 'mail_content'];
        return $this->select()
                ->where('mail_id', $id)->first();
    }

    /**
     * Update data
     *
     * @param  array $keys
     * @param  array $data
     * @return boolean
     */
    public function updateData($keys, $data)
    {
        $result = $this->where('receive_id', '=', $keys['receive_id'])
                    ->where('detail_line_num', '=', $keys['detail_line_num'])
                    ->where('sub_line_num', '=', $keys['sub_line_num'])
                    ->update($data);
        return true;
    }
    /**
     * Get data template by id
     *
     * @param  array $id
     * @return object
     */
    public function getDataById($id)
    {
        return $this->where('mail_id', $id)->first();
    }
}
