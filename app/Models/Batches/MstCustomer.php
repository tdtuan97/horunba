<?php
/**
 * Model for mst_customer table.
 *
 * @package    App\Models\Batches
 * @subpackage MstCustomer
 * @copyright  Copyright (c) 2017 RiverCrane! Corporation. All Rights Reserved.
 * @author     Truong Nghia<truong.van.nghia@rivercrane.vn>
 */

namespace App\Models\Batches;

use Illuminate\Database\Eloquent\Model;

class MstCustomer extends Model
{
    public $timestamps = false;

    /**
    * The database table used by the model.
    * @var string
    */
    protected $table = 'mst_customer';

    /**
    * The database is used by the model.
    * @var string
    */
    protected $connection = 'horunba';

    /**
    * Check exixts customer.
    * @param array $arrDatas Data for condition where
    * @return object $result
    */
    public function checkCustomerExist($arrDatas, $flgEmail = false)
    {
        $result = $this->whereRaw("first_name = BINARY ifnull(?,'')", [$arrDatas['first_name']])
                       ->whereRaw("last_name = BINARY ifnull(?,'')", [$arrDatas['last_name']])
                       ->whereRaw("tel_num = BINARY ifnull(?,'')", [$arrDatas['tel_num']])
                       ->whereRaw("zip_code = BINARY ifnull(?,'')", [$arrDatas['zip_code']])
                       ->whereRaw("prefecture = BINARY ifnull(?,'')", [$arrDatas['prefecture']])
                       ->whereRaw("city = BINARY ifnull(?,'')", [$arrDatas['city']])
                       ->whereRaw("sub_address = BINARY ifnull(?,'')", [$arrDatas['sub_address']]);
        if ($flgEmail) {
            $result->whereRaw("email = BINARY ifnull(?,'')", [$arrDatas['email']]);
        }
        return  $result->first();
    }
}
