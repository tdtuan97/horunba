<?php
/**
 * Model for t_amazon_order_detail table.
 *
 * @package    App\Models\Batches
 * @subpackage TAmazonOrderDetail
 * @copyright  Copyright (c) 2018 RiverCrane! Corporation. All Rights Reserved.
 * @author     van.qui<van.qui.rvcn2012@gmail.com>
 * @author     Truong Nghia<truong.van.nghia@rivercrane.vn>
 */

namespace App\Models\Batches;

use Illuminate\Database\Eloquent\Model;
use DB;
use App\Custom\DBExtend;

class TAmazonOrderDetail extends Model
{
    use DBExtend;

    public $timestamps = false;
    /**
    * The database table used by the model.
    * @var string
    */
    protected $table = 't_amazon_order_detail';
    /**
    * The database is used by the model.
    * @var string
    */
    protected $connection = 'horunba';
    /**
     * Get data of tabe t_amazon_order_detail
     *
     * @return object $result
     */
    public function getDataDetail($id)
    {
        return $this->selectRaw('t_amazon_order_detail.*,receive_id')
            ->join('mst_order', 'mst_order.received_order_id', '=', 't_amazon_order_detail.amazon_order_id')
            ->where('receive_id', $id)
            ->where('item_price_amount', '<>', 0)
            ->get();
    }
}
