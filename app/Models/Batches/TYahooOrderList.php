<?php

/**
 * Exe sql for hrnb.t_yahoo_order_list
 *
 * @package    App\Models
 * @subpackage TYahooOrderList
 * @copyright  Copyright (c) 2018 RiverCrane! Corporation. All Rights Reserved.
 * @author     Van Qui<van.qui@rivercrane.vn>
 */

namespace App\Models\Batches;

use Illuminate\Database\Eloquent\Model;

class TYahooOrderList extends Model
{
    public $timestamps = false;
    /**
    * The database table used by the model.
    * @var string
    */
    protected $table = 't_yahoo_order_list';
    /**
    * The database is used by the model.
    * @var string
    */
    protected $connection = 'horunba';
    
    /**
     * Get order by order id
     *
     * @param  array  $orderIds
     * @return object
     */
    public function getOrderByOrderId($orderIds)
    {
        $data = $this->select('order_list_orderid')->whereIn('order_list_orderid', $orderIds)->get();
        return $data;
    }
    
    /**
     * Get new order
     *
     * @return object
     */
    public function getNewOrder()
    {
        $data = $this->select('yahoo_order_info_key', 'order_list_orderid')->where('status', 0)->get();
        return $data;
    }
    
    /**
     * Update data by key
     *
     * @param  array   $key
     * @param  array   $data
     * @return boolean
     */
    public function updateData($key, $data)
    {
        return $this->where($key)->update($data);
    }
}
