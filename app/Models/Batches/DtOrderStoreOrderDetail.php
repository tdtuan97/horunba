<?php

/**
 * Model for dt_store_order_detail table.
 *
 * @package    App\Models\Batches
 * @subpackage DtOrderStoreOrderDetail
 * @copyright  Copyright (c) 2017 RiverCrane! Corporation. All Rights Reserved.
 * @author     Le Hung<le.hung.rcvn2012@gmail.com>
 */

namespace App\Models\Batches;

use Illuminate\Database\Eloquent\Model;
use DB;

class DtOrderStoreOrderDetail extends Model
{
    public $timestamps = false;

    /**
    * The database table used by the model.
    * @var string
    */
    protected $table = 'dt_store_order_detail';

    /**
    * The database is used by the model.
    * @var string
    */
    protected $connection = 'horunba';

    /**
     * Get data order detail info
     * @return object $result
     */
    public function getDataOrdersDetail()
    {
        $result = $this->select('order_id')
                       ->where('process_status', '=', 0)
                       ->orderBy('order_id', 'ASC')
                       ->limit('5000')
                       ->get();
        return $result;
    }
    /**
     * Update data
     *
     * @param  array $dataWhere for condition
     * @param  array $dataUpdate for update
     * @return object
     */
    public function updateData($dataWhere, $dataUpdate)
    {
        $data = $this->where($dataWhere)->update($dataUpdate);
        return $data;
    }
    /**
     * Get data order detail info
     *
     *
     * @return object $result
     */
    public function getDataOrderDetailForUpdate($case = null)
    {

        $query = $this->select(
            'order_id',
            'product_id'
        )->leftJoin('mst_product', 'mst_product.product_code', '=', 'dt_store_order_detail.product_code');
        if ($case === null) {
            $query->whereNull('mst_product.product_code')
            ->where('dt_store_order_detail.process_status', '=', 0);
        } elseif ($case === 'E03') {
            $query->where('mst_product.df_handling', '!=', 1)
            ->where('dt_store_order_detail.process_status', '=', 0);
        }
        $data = $query->limit(5000)->get();
        return $data;
    }

    /**
     * Get data order detail info
     *
     *
     * @return object $result
     */
    public function getDataOrderDetailCloneData($case = null)
    {
        $data = $this->select(
            DB::raw(
                "
                (CASE WHEN mst_store_info.store_id = 1
                 THEN
                     CONCAT('DFO-', dt_store_order_detail.order_id)
                 ELSE
                     CONCAT('DFF-', dt_store_order_detail.order_id)
                 END) AS store_id
                 "
            ),
            'mst_store_info.customer_id',
            'dt_store_order_info.order_date',
            'dt_store_order_detail.order_id',
            'dt_store_order_detail.product_code',
            'dt_store_order_detail.product_name',
            'dt_store_order_detail.quantity',
            'dt_store_order_detail.product_id'
        )
        ->leftJoin('mst_product', 'mst_product.product_code', '=', 'dt_store_order_detail.product_code')
        ->leftJoin('dt_store_order_info', 'dt_store_order_info.order_id', '=', 'dt_store_order_detail.order_id')
        ->leftJoin('mst_store_info', 'dt_store_order_info.storage_store_id', '=', 'mst_store_info.store_id')
        ->where('dt_store_order_info.status', '<>', 6)
        ->where('dt_store_order_detail.process_status', '=', 0)
        ->limit('5000')
        ->get();
        return $data;
    }

    /**
     * Get data for update delivery
     *
     *
     * @return object $result
     */
    public function getDataUpdateDelivery()
    {
        $data = $this->select(
            'mst_order.delivery_plan_date',
            'dt_store_order_info.storage_store_id',
            'dt_store_order_info.order_id',
            'dt_store_order_detail.product_id',
            'mst_order.received_order_id'
        )
        ->leftJoin('dt_store_order_info', 'dt_store_order_info.order_id', '=', 'dt_store_order_detail.order_id')
        ->leftJoin('mst_order', 'dt_store_order_detail.hrnb_order_id', '=', 'mst_order.received_order_id')
        ->where('dt_store_order_detail.process_status', '=', 1)
        ->where('mst_order.order_status', '=', ORDER_STATUS['ORDER'])
        ->where('mst_order.order_sub_status', '=', ORDER_SUB_STATUS['DOING'])
        ->limit('5000')
        ->groupBy('dt_store_order_detail.order_id')
        ->get();
        return $data;
    }

    /**
     * Get data for update delivery
     *
     *
     * @return object $result
     */
    public function getDataUpdateDelivery2()
    {
        $data = $this->select(
            'dt_order_to_supplier.arrival_date_plan',
            'dt_store_order_info.order_id',
            'dt_store_order_info.storage_store_id',
            'dt_order_to_supplier.process_status'
        )
        ->leftJoin('dt_store_order_info', 'dt_store_order_info.order_id', '=', 'dt_store_order_detail.order_id')
        ->leftJoin('mst_order', 'dt_store_order_detail.hrnb_order_id', '=', 'mst_order.received_order_id')
        ->leftJoin('dt_order_to_supplier', 'dt_order_to_supplier.receive_id', '=', 'mst_order.receive_id')
        ->leftJoin(
            'dt_order_product_detail',
            'dt_order_product_detail.receive_id',
            '=',
            'dt_order_to_supplier.receive_id'
        )
        ->where('dt_store_order_detail.process_status', '=', 1)
        ->whereIn('dt_order_to_supplier.process_status', [2,5])
        ->limit('5000')
        ->groupBy('dt_store_order_info.order_id')
        ->get();
        return $data;
    }
    /**
     * Get data for update result
     *
     *
     * @return object $result
     */
    public function getDataUpdateResult()
    {
        $data = $this->select(
            'dt_store_order_info.order_id',
            'dt_store_order_info.storage_store_id',
            'dt_store_order_detail.product_id',
            'dt_store_order_detail.quantity'
        )
        ->leftJoin('dt_store_order_info', 'dt_store_order_info.order_id', '=', 'dt_store_order_detail.order_id')
        ->where('dt_store_order_detail.process_status', '=', 2)
        ->where('dt_store_order_info.process_status', '=', 5)
        ->limit('5000')
        ->groupBy('dt_store_order_info.order_id')
        ->get();
        return $data;
    }
}
