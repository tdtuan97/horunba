<?php

/**
 * Model for t_order_detail table.
 *
 * @package    App\Models\Batches
 * @subpackage TOrderDetail
 * @copyright  Copyright (c) 2017 RiverCrane! Corporation. All Rights Reserved.
 * @author     Truong Nghia<truong.van.nghia@rivercrane.vn>
 */

namespace App\Models\Batches;

use Illuminate\Database\Eloquent\Model;

class TOrderDetail extends Model
{
    public $timestamps = false;

    /**
    * The database table used by the model.
    * @var string
    */
    protected $table = 't_order_detail';

    /**
    * The database is used by the model.
    * @var string
    */
    protected $connection = 'edi';
    
    /**
     * Update data
     *
     * @param  array   $key
     * @param  array   $data
     * @return boolean
     */
    public function updateData($key, $data)
    {
        return $this->where($key)->update($data);
    }
}
