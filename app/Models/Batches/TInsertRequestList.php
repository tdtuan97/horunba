<?php

/**
 * Model for tinsertRequestList table.
 *
 * @package    App\Models\Batches
 * @subpackage TInsertRequestList
 * @copyright  Copyright (c) 2017 RiverCrane! Corporation. All Rights Reserved.
 * @author     Truong Nghia<truong.van.nghia@rivercrane.vn>
 */

namespace App\Models\Batches;

use Illuminate\Database\Eloquent\Model;

class TInsertRequestList extends Model
{
    public $timestamps = false;

    /**
    * The database table used by the model.
    * @var string
    */
    protected $table = 'tinsertRequestList';

    /**
    * The database is used by the model.
    * @var string
    */
    protected $connection = 'glsc';
}
