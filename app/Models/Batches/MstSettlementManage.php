<?php

/**
 * Model for mst_settlement_manage table.
 *
 * @package    App\Models\Batches
 * @subpackage MstSettlementManage
 * @copyright  Copyright (c) 2017 RiverCrane! Corporation. All Rights Reserved.
 * @author     Truong Nghia<truong.van.nghia@rivercrane.vn>
 */

namespace App\Models\Batches;

use Illuminate\Database\Eloquent\Model;

class MstSettlementManage extends Model
{
    public $timestamps = false;

    /**
    * The database table used by the model.
    * @var string
    */
    protected $table = 'mst_settlement_manage';

    /**
    * The database is used by the model.
    * @var string
    */
    protected $connection = 'horunba';

    /**
     * Get data by payment code
     * @param  String $paymentCode
     * @return object
     */
    public function getDataByPaymentCode($paymentCode)
    {
        $result = $this->where('payment_code', '=', $paymentCode)->first();
        return $result;
    }
}
