<?php

/**
 * Model for dt_return table.
 *
 * @package    App\Models\Batches
 * @subpackage DtReturn
 * @copyright  Copyright (c) 2017 RiverCrane! Corporation. All Rights Reserved.
 * @author     Truong Nghia<truong.van.nghia@rivercrane.vn>
 */

namespace App\Models\Batches;

use Illuminate\Database\Eloquent\Model;
use DB;
use App\Custom\DBExtend;

class DtReturn extends Model
{
    use DBExtend;

    public $timestamps = false;

    /**
    * The database table used by the model.
    * @var string
    */
    protected $table = 'dt_return';

    /**
    * The database is used by the model.
    * @var string
    */
    protected $connection = 'horunba';

    /**
    * The primary key.
    * @var string
    */
    protected $primaryKey = 'return_no';

    /**
    * The type of primary key.
    * @var string
    */
    protected $keyType = 'string';

    /**
     * Get data process request glsc
     *
     * @return object
     */
    public function getDataRequestGLSC()
    {
        $col = [
            'dt_return.return_no',
            'dt_return.return_line_no',
            'dt_return.return_time',
            'dt_return.receive_plan_date',
            'dt_return.return_quantity',
//            'dt_return.parent_product_code',
//            'dt_return.product_code',
            'dt_return.receive_count',
            'dt_return.receive_instruction',
            'dt_return.receive_real_num',
            'mpP.product_name',
            'mpP.product_jan',
            'mpP.maker_full_nm',
            'mpP.product_maker_code',
            'mpP.rak_img_url_1',
            'msP.supplier_nm',
/*            'mpC.product_name AS child_product_name',
            'mpC.product_jan AS child_product_jan',
            'mpC.maker_full_nm AS child_maker_full_nm',
            'mpC.product_maker_code AS child_product_maker_code',
            'mpC.rak_img_url_1 AS child_rak_img_url_1',
            'msC.supplier_nm AS child_supplier_nm',*/
        ];
        $sub_pro = 'IFNULL(dt_return.product_code, dt_return.parent_product_code) AS product_code';
        $result = $this->select($col)
                        ->selectRaw($sub_pro)
                        ->join(
                            'hrnb.mst_product AS mpP',
                            'mpP.product_code',
                            '=',
                            DB::raw('IFNULL(dt_return.product_code, dt_return.parent_product_code)')
                        )
                       ->leftjoin('mst_supplier AS msP', 'msP.supplier_cd', '=', 'mpP.price_supplier_id')

//                       ->leftjoin('hrnb.mst_product AS mpC', 'mpC.product_code', '=', 'dt_return.product_code')
//                       ->leftjoin('mst_supplier AS msC', 'msC.supplier_cd', '=', 'mpC.price_supplier_id')
                       ->whereIn('dt_return.receive_instruction', [0,8])
                       ->get();
        return $result;
    }

    /**
     * Get data update message return order
     *
     * @return object
     */
    public function getDataUpdateMessage()
    {
        $col = [
            'return_no',
            'receive_count',
            'er_ErrorCd',
            'er_Message',
        ];
        $result = $this->select($col)
                       ->join('edi.t_insertRequestList AS ti', function ($join) {
                            $join->on(DB::Raw('concat(return_no,return_time,return_line_no)'), '=', 'ti.order_code')
                                 ->on('dt_return.receive_count', '=', 'ti.RequestBNo');
                       })
                       ->where('ti.del_flg', '=', 2)
                       ->where('dt_return.receive_instruction', '=', 1)
                       ->get();
        return $result;
    }

    /**
     * Get data update return result
     *
     * @return object
     */
    public function getDateUpdateReturnResult()
    {
        $col = [
            'tr.tRequestPerformanceModel_autono',
            'tr.RequestNo',
            'tr.RequestBNo',
            'tr.ArrivalDate',
            'tr.PlanQuantity',
            'tr.Quantity',
            'tr.Note',
            'tr.ItemCd',
            'tr.SupplyNo',
            'tr.price',
            'dt_return.return_no',
            'dt_return.return_line_no',
            'dt_return.delivery_instruction',
            'dt_return.supplier_id',
            'dt_return.return_line_no',
            'dt_return.return_time',
        ];
        $result = $this->select($col)
                       ->join('edi.tRequestPerformanceModel AS tr', function ($join) {
                            $join->on(DB::Raw('concat(return_no,return_time,return_line_no)'), '=', 'tr.SupplyNo')
                                 ->on('dt_return.receive_count', '=', 'tr.RequestBNo');
                       })
                       ->whereIn('tr.decsy_update_flg', [0, 1])
                       ->where('dt_return.receive_instruction', '=', 1)
                       ->get();
        return $result;
    }

    /**
     * Get data delivery return
     *
     * @return object
     */
    public function getDataDeliveryReturn()
    {
        $col = [
            'mst_product.product_name',
            'mst_product.product_jan',
            'mst_product.maker_full_nm',
            'mst_product.product_maker_code',
            'mst_product.product_name_long',
            'mst_product.product_oogata',
            'mst_product.rak_img_url_1',
            'mst_return_address.return_nm',
            'mst_return_address.return_postal',
            'mst_return_address.return_pref',
            'mst_return_address.return_tel',
            'mst_return_address.return_address',
            'dt_return.receive_instruction',
            'dt_return.return_quantity',
            'dt_return.return_no',
            'dt_return.return_line_no',
            'dt_return.return_time',
            'dt_return.supplier_id',
            'dt_return.return_address_id',
            'dt_return.note',
            'dt_return.delivery_plan_date',
            'dt_return.receive_id',
            'dt_return.receive_instruction',
            // 'dt_return.parent_product_code',
            'dt_return.return_quantity',
            'dt_return.delivery_count',
            'dt_return.delivery_instruction',
            'mst_stock_status.nanko_num',
            'mst_stock_status.order_zan_num',
            'mst_stock_status.return_num',
            'mst_stock_status.wait_delivery_num',
            'mst_stock_status.product_code'
        ];
        $subSelect = "IFNULL(dt_return.product_code, dt_return.parent_product_code) AS product_code";
        $result = $this->select($col)
                       ->selectRaw($subSelect)
                       ->join(
                           'mst_product',
                           DB::raw('IFNULL(dt_return.product_code, dt_return.parent_product_code)'),
                           '=',
                           'mst_product.product_code'
                       )
                       ->join(
                           'mst_stock_status',
                           DB::raw('IFNULL(dt_return.product_code, dt_return.parent_product_code)'),
                           '=',
                           'mst_stock_status.product_code'
                       )
                       ->leftjoin('mst_return_address', function ($join) {
                           $join->on('mst_return_address.return_id', '=', 'dt_return.return_address_id')
                                ->on('mst_return_address.supplier_cd', '=', 'dt_return.supplier_id');
                       })
                       ->whereIn('delivery_instruction', [0, 8])
                       ->whereIn('dt_return.receive_instruction', [-1, 2])
                       ->where('dt_return.is_mojax', 0)
                       ->where(function ($sub) {
                           $sub->whereDate('dt_return.delivery_plan_date', '<=', date('Y-m-d'))
                               ->orWhereNull('dt_return.delivery_plan_date');
                       })
                       ->where(function ($sub) {
                            $sub->whereNull('error_code')
                                ->orWhere('error_code', '');
                       })
                       ->orderBy('receive_id')
                       ->orderBy('dt_return.supplier_id')
                       ->orderBy('dt_return.return_address_id')
                       ->get();
        return $result;
    }

    /**
     * Get data error result delivery return
     *
     * @return object
     */
    public function getDataErrDeliveryResult()
    {
        $col = [
            'dt_return.return_no',
            'dt_return.return_line_no',
            'dt_return.return_time',
            'tInsertShipping.er_ErrorCd',
            'tInsertShipping.er_Message',
        ];
        $result = $this->select($col)
                       ->join('glsc.tInsertShipping', function ($join) {
                            $join->on('dt_return.supplier_return_no', '=', 'tInsertShipping.OrderNo')
                                 ->on('dt_return.supplier_return_line', '=', 'tInsertShipping.OrderLNo')
                                 ->on('dt_return.delivery_count', '=', 'tInsertShipping.OrderBNo');
                       })
                       ->where('tInsertShipping.ProcessFlg', '=', 0)
                       ->where('dt_return.delivery_instruction', '=', 1)
                       ->get();
        return $result;
    }

    /**
     * Get data result delivery update data folow dataKb
     * @param int dataKb
     * @return object
     */
    public function getDataUpdateFollowKb($dataKb)
    {
        $col = [
            'ts.tShippingPerformanceModel_autono AS id',
            'ts.DataKb',
            'ts.OrderNo',
            'ts.OrderBNo',
            'ts.OrderLNo',
            'ts.DeliDate',
            'ts.Note',
            'ts.PlanQuantity',
            'ts.Quantity',
            'ts.ShipNum',
            'ts.ItemCd',
            'mp.price_invoice',
            'mp.price_supplier_id',
            'dt_return.receive_instruction',
            'dt_return.return_no',
            'dt_return.return_line_no',
            'dt_return.return_time',
            'dt_return.is_mojax',
        ];
        $result = $this->select($col)
                       ->join('glsc.tShippingPerformanceModel AS ts', function ($join) {
                            $join->on('dt_return.supplier_return_no', '=', 'ts.OrderNo')
                                 ->on('dt_return.supplier_return_line', '=', 'ts.OrderLNo')
                                 ->on('dt_return.delivery_count', '=', 'ts.OrderBNo');
                       })
                       ->join(
                           'mst_product AS mp',
                           'ts.ItemCd',
                           '=',
                           'mp.product_code'
                       )
                       ->where('ts.DataKb', '=', $dataKb)
                       ->whereIn('ts.NodisplayFlg', [0, 1])
                       ->where('dt_return.delivery_instruction', '=', 1)
                       ->where('ts.DeliDate', '=', date('Ymd'))
                       ->get();
        return $result;
    }

    /**
    * Update information
    * @param string $key      condition
    * @param array $arrUpdate data update
    * @return object boolean
    */
    public function updateData($key, $arrUpdate)
    {
        return $this->where($key)
                    ->update($arrUpdate);
    }

    /**
    * Update information multi row
    * @param array $arrId      condition
    * @param array $arrUpdate data update
    * @return object boolean
    */
    public function updateDataMulti($arrId, $arrUpdate)
    {
        return $this->whereIn('return_no', $arrId)
                    ->update($arrUpdate);
    }
    /**
     * Get data return delivery return content 2
     *
     * @return object
     */
    public function getDataDeliveryReturn2()
    {
        $col = [
            'dt_return.supplier_id',
            'dt_return.return_date',
            'dt_return.supplier_return_no',
            'dt_return.return_type_mid_id',
            'dt_return.return_quantity',
            'dt_return.return_no',
            'dt_return.return_line_no',
            'dt_return.return_time',
            'dt_return.delivery_instruction',
            'dt_return.red_voucher',
            'ms.supplier_nm',
            'ms.supplier_fax',
            'ms.mail_address',
            'mp.product_jan',
            'mp.maker_full_nm',
            'mp.product_maker_code',
            'mp.product_name_long',
            'mp.price_invoice',
        ];
        $result = $this->select($col)
            ->selectRaw('IFNULL(dt_return.product_code, dt_return.parent_product_code) AS product_code')
            ->selectRaw("IFNULL(mp.product_color, '') AS product_color")
            ->selectRaw("IFNULL(mp.product_size, '') AS product_size")
            ->selectRaw("IF(dt_return.return_tanka = 0, mp.price_invoice, dt_return.return_tanka) AS return_tanka")
            ->selectRaw("IF(dt_return.return_price = 0, mp.price_invoice, dt_return.return_price) AS return_price")
            ->join(
                'mst_product AS mp',
                DB::raw('IFNULL(dt_return.product_code, dt_return.parent_product_code)'),
                '=',
                'mp.product_code'
            )
            ->join('mst_supplier AS ms', 'dt_return.supplier_id', '=', 'ms.supplier_cd')
            ->where('dt_return.red_voucher', 0)
            ->whereIn('dt_return.delivery_instruction', [-1, 2])
            ->get();
        return $result;
    }

    /**
     * Get data process delivery result new
     *
     * @return object
     */
    public function getDataProcessDelivetyResultNew()
    {
        $col = [
            'dd.received_order_id',
            'dd.subdivision_num',
            'ddd.detail_line_num',
            'ddd.delivery_real_num',
            'ddd.delivery_real_date',
            'dt_return.delivery_instruction',
            'dt_return.return_no',
            'dt_return.return_line_no',
            'dt_return.return_time',
        ];
        $result = $this->select($col)
                       ->join('dt_delivery AS dd', function ($join) {
                            $join->on('dt_return.supplier_return_no', '=', 'dd.received_order_id')
                                 ->where('dd.delivery_status', 2);
                       })
                       ->join('dt_delivery_detail AS ddd', function ($join) {
                            $join->on('ddd.detail_line_num', '=', 'dt_return.supplier_return_line')
                                 ->on('dd.received_order_id', '=', 'ddd.received_order_id')
                                 ->on('dd.subdivision_num', '=', 'ddd.subdivision_num');
                       })
                       ->where('dt_return.delivery_instruction', 1)
                       ->get();
        return $result;
    }
}
