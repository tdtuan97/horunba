<?php

/**
 * Model for t_admin table.
 *
 * @package    App\Models\Batches
 * @subpackage TAdmin
 * @copyright  Copyright (c) 2017 RiverCrane! Corporation. All Rights Reserved.
 * @author     Truong Nghia<truong.van.nghia@rivercrane.vn>
 */

namespace App\Models\Batches;

use Illuminate\Database\Eloquent\Model;

class TAdmin extends Model
{
    public $timestamps = false;

    /**
    * The database table used by the model.
    * @var string
    */
    protected $table = 't_admin';

    /**
    * The database is used by the model.
    * @var string
    */
    protected $connection = 'edi';

    /**
     * Get data of tabe t_admin for order zaiko
     * @return object $result
     */
    public function getDataZaiko()
    {
        $result = $this->select('send_zaiko_fax_flg', 'suppliercd')->get()->keyBy('suppliercd');
        return $result;
    }

    /**
     * Get data of tabe t_admin for order EDI
     * @return object $result
     */
    public function getDataEdi()
    {
        $result = $this->select('send_fax_flg', 'fax', 'suppliercd')->get()->keyBy('suppliercd');
        return $result;
    }
}
