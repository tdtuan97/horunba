<?php
/**
 * Model for mst_mail_filter table.
 *
 * @package    App\Models\Batches
 * @subpackage DtMailFilter
 * @copyright  Copyright (c) 2018 RiverCrane! Corporation. All Rights Reserved.
 * @author     Truong Nghia<truong.van.nghia@rivercrane.vn>
 */

namespace App\Models\Batches;

use Illuminate\Database\Eloquent\Model;

class DtMailFilter extends Model
{
    public $timestamps = false;

    /**
    * The database table used by the model.
    * @var string
    */
    protected $table = 'mst_mail_filter';

    /**
    * The database is used by the model.
    * @var string
    */
    protected $connection = 'horunba';

}
