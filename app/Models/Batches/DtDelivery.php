<?php
/**
 * Model for dt_delivery table.
 *
 * @package    App\Models\Batches
 * @subpackage DtDelivery
 * @copyright  Copyright (c) 2017 RiverCrane! Corporation. All Rights Reserved.
 * @author     Truong Nghia<truong.van.nghia@rivercrane.vn>
 */

namespace App\Models\Batches;

use Illuminate\Database\Eloquent\Model;
use DB;
use App\Custom\DBExtend;

class DtDelivery extends Model
{
    use DBExtend;
    public $timestamps = false;

    /**
    * The database table used by the model.
    * @var string
    */
    protected $table = 'dt_delivery';

    /**
    * The database is used by the model.
    * @var string
    */
    protected $connection = 'horunba';

    /**
    * The get data.
    * @return object $result
    */
    public function getData()
    {
        $result = $this
            ->whereIn('delivery_status', [0, 7, 8])
            ->where('delivery_plan_date', date('Ymd'))
            ->get();
        return $result;
    }

    /**
    * The get check delivery error.
    * @return object $result
    */
    public function getDataCheckError()
    {
        $col = [
            'received_order_id',
            'subdivision_num',
            'detail_line_num',
            'er_ErrorCd',
            'er_Message'
        ];
        $result = $this->select($col)
                       ->join('glsc.tInsertShipping as tis', function ($join) {
                            $join->on('dt_delivery.received_order_id', '=', 'tis.OrderNo')
                                 ->on('dt_delivery.subdivision_num', '=', 'tis.OrderBNo')
                                 ->on('dt_delivery.detail_line_num', '=', 'tis.OrderLNo');
                       })
                       ->where('tis.ProcessFlg', '=', 0)
                       ->where('dt_delivery.delivery_status', '=', 1)
                       ->whereNotNull('er_ErrorCd')
                       ->get();
        return $result;
    }

    /**
    * The get data process delivery.
    * @param int $dataKb delivery status
    * @return object $result
    */
    public function getDataDeliveryResult($dataKb)
    {
        $col = [
            'dt_delivery.received_order_id',
            'dt_delivery.subdivision_num',
            'dt_delivery.detail_line_num',
            'tsp.tShippingPerformanceModel_autono',
            'tsp.DataKb',
            'tsp.ShipNum',
            'tsp.Note',
            'tsp.InquiryNo',
            'tsp.Quantity',
            'tsp.PlanQuantity',
            'tsp.DeliDate',
            'tsp.DeliveryCd',
            'mst_order.receive_id',
            'dt_delivery.delivery_num',
            'dt_delivery.delivery_real_num',
            'dt_delivery.product_code',
            'mst_product.price_invoice',
            'opd.delivery_type',
        ];
        $result = $this->select($col)
                       ->join('glsc.tShippingPerformanceModel as tsp', function ($join) use ($dataKb) {
                            $join->on('dt_delivery.received_order_id', '=', 'tsp.OrderNo')
                                 ->on('dt_delivery.subdivision_num', '=', 'tsp.OrderBNo')
                                 ->on('dt_delivery.detail_line_num', '=', 'tsp.OrderLNo')
                                 ->where('DataKb', '=', $dataKb);
                       })
                       ->leftjoin('mst_order', 'mst_order.received_order_id', '=', 'dt_delivery.received_order_id')
                       ->leftjoin('mst_product', 'mst_product.product_code', '=', 'dt_delivery.product_code')
                       ->leftjoin('dt_order_product_detail as opd', function ($join) {
                            $join->on('mst_order.receive_id', '=', 'opd.receive_id')
                                 ->on('dt_delivery.detail_line_num', '=', 'opd.sub_line_num');
                       })
                       ->where('dt_delivery.delivery_status', '=', 2)
                       ->where('mst_order.is_delay', '<>', 1)
                       ->whereDate('tsp.DeliDate', date('Ymd'))
                       ->whereIn('tsp.NodisplayFlg', [0,1])
                       ->get();
        return $result;
    }

    /**
    * The update infomation.
    * @param array $key    condition
    * @param array $value  data update
    * @return object $result
    */
    public function updateData($key, $value)
    {
        return $this->where($key)->update($value);
    }

    /**
    * The get data process delivery 1.
    * @param int $dataKb delivery status
    * @return object $result
    */
    public function getDataDeliveryResult2()
    {
        $col = [
            'dt_delivery.received_order_id',
            'dt_delivery.subdivision_num',
            'dt_delivery.detail_line_num',
            'tsp.tShippingPerformanceModel_autono',
            'tsp.DeliveryCd',
            'tsp.InquiryNo'
        ];
        $result = $this->select($col)
                       ->join('glsc.tShippingPerformanceModel as tsp', function ($join) {
                            $join->on('dt_delivery.received_order_id', '=', 'tsp.OrderNo')
                                ->on('dt_delivery.subdivision_num', '=', 'tsp.OrderBNo')
                                ->on('dt_delivery.detail_line_num', '=', 'tsp.OrderLNo')
                                ->where('DataKb', '=', 7);
                       })
                       ->leftjoin('mst_order', 'mst_order.received_order_id', '=', 'dt_delivery.received_order_id')
                       ->where('mst_order.is_delay', '<>', 1)
                       ->whereDate('tsp.DeliDate', date('Ymd'))
                       ->whereIn('tsp.NodisplayFlg', [0,1])
                       ->get();
        return $result;
    }

    /**
    * Get data process call API insert shipping list.
    * @return object $result
    */
    public function getDataProcessInsertShipping($limit)
    {
        $col = [
            'dt_delivery.received_order_id',
        ];
        $result = $this->select($col)
                    ->join('dt_delivery_detail AS ddd', function ($sub) {
                        $sub->on('ddd.received_order_id', '=', 'dt_delivery.received_order_id')
                            ->on('ddd.subdivision_num', '=', 'dt_delivery.subdivision_num');
                    })
                    ->where(function ($query) {
                        $query->where('delivery_status', 0)
                            ->orWhere('delivery_status', 7)
                            ->orWhere('delivery_status', 8);
                    })
                    ->whereDate('delivery_plan_date', date('Y-m-d'))
                    ->groupBy(["dt_delivery.received_order_id"])
                    ->orderBy('group_key')
                    ->limit($limit)
                    ->get();
        return $result;
    }

    /**
    * Get data process call API insert shipping list.
    * @return object $result
    */
    public function getDataProcessInsertShippingByKey($arrKey)
    {
        $col = [
            'dt_delivery.received_order_id',
            'dt_delivery.subdivision_num',
            'dt_delivery.delivery_date',
            'dt_delivery.delivery_time',
            'dt_delivery.delivery_instrustions',
            'dt_delivery.account',
            'dt_delivery.delivery_plan_date',
            'dt_delivery.total_price',
            'dt_delivery.shipping_instructions',
            'dt_delivery.customer_name',
            'dt_delivery.delivery_name',
            'dt_delivery.delivery_postal',
            'dt_delivery.delivery_perfecture',
            'dt_delivery.delivery_city',
            'dt_delivery.delivery_sub_address',
            'dt_delivery.delivery_tel',
            'dt_delivery.group_key',
            'dt_delivery.store_code',
            'dt_delivery.invoice_remarks',
            'dt_delivery.delivery_code',
            'dt_delivery.delivery_status',
            'ddd.detail_line_num',
            'ddd.product_code',
            'ddd.product_name',
            'ddd.product_jan',
            'ddd.maker_name',
            'ddd.maker_code',
            'ddd.product_image_url',
            'ddd.delivery_num',
        ];
        $result = $this->select($col)
                    ->join('dt_delivery_detail AS ddd', function ($sub) {
                        $sub->on('ddd.received_order_id', '=', 'dt_delivery.received_order_id')
                            ->on('ddd.subdivision_num', '=', 'dt_delivery.subdivision_num');
                    })
                    ->where(function ($query) {
                        $query->where('delivery_status', 0)
                            ->orWhere('delivery_status', 7)
                            ->orWhere('delivery_status', 8);
                    })
                    ->whereDate('delivery_plan_date', date('Y-m-d'))
                    ->whereIn("dt_delivery.received_order_id", $arrKey)
                    ->groupBy(["dt_delivery.received_order_id", "dt_delivery.subdivision_num", "ddd.detail_line_num"])
                    ->orderBy('group_key')
                    ->get();
        return $result;
    }

    /**
    * The get data process delivery 3.
    * @param int $dataKb delivery status
    * @return object $result
    */
    public function getDataDeliveryResult3()
    {
        $col = [
            'dt_delivery.received_order_id',
            'dt_delivery.subdivision_num',
            'dt_delivery.detail_line_num',
            'dt_delivery.product_code',
            'tsp.tShippingPerformanceModel_autono',
            'tsp.DeliveryCd',
            'tsp.InquiryNo',
            'tsp.Quantity',
            'tsp.DeliDate',
            'opd.price_invoice',
        ];
        $result = $this->select($col)
                       ->join('glsc.tShippingPerformanceModel as tsp', function ($join) {
                            $join->on('dt_delivery.received_order_id', '=', 'tsp.OrderNo')
                                ->on('dt_delivery.subdivision_num', '=', 'tsp.OrderBNo')
                                ->on('dt_delivery.detail_line_num', '=', 'tsp.OrderLNo')
                                ->where('DataKb', '=', 3);
                       })
                       ->leftjoin('mst_order', 'mst_order.received_order_id', '=', 'dt_delivery.received_order_id')
                       ->leftjoin('dt_order_product_detail as opd', function ($join) {
                            $join->on('mst_order.receive_id', '=', 'opd.receive_id')
                                 ->on('dt_delivery.detail_line_num', '=', 'opd.sub_line_num');
                       })
                       ->where('mst_order.is_delay', '<>', 1)
                       ->whereDate('tsp.DeliDate', date('Ymd'))
                       ->whereIn('tsp.NodisplayFlg', [0,1])
                       ->get();
        return $result;
    }

    /**
    * Get data process call API update group key.
    * @return object $result
    */
    public function getDataProcessUpdateGroupKey()
    {
        $col = [
            'received_order_id',
            'subdivision_num',
            'group_key'
        ];
        $result = $this->select($col)
                       ->where('delivery_status', 4)
                       ->groupBy('group_key')
                       ->get();
        return $result;
    }

    /**
    * Get data check delivery plan date is past.
    * @return object $result
    */
    public function getDataCheckDateIsPast()
    {
        $col = [
            'received_order_id',
            'subdivision_num',
        ];
        return $this->select($col)
                    ->whereDate('delivery_plan_date', '<', date('Y-m-d'))
                    ->whereIn('delivery_status', [0, 7])
                    ->get();
    }

    /**
    * Get data check digits of JAN code is not equal 13.
    * @return object $result
    */
    public function getDataCheckDigits()
    {
        $col = [
            'dt_delivery.received_order_id',
            'dt_delivery.subdivision_num',
        ];
        return $this->select($col)
                    ->join('dt_delivery_detail AS ddd', function ($sub) {
                        $sub->on('ddd.received_order_id', '=', 'dt_delivery.received_order_id')
                            ->on('ddd.subdivision_num', '=', 'dt_delivery.subdivision_num');
                    })
                    ->whereRaw('LENGTH(ddd.product_jan) <> 13')
                    ->whereIn('delivery_status', [0, 7])
                    ->groupBy(['dt_delivery.received_order_id', 'dt_delivery.subdivision_num'])
                    ->get();
    }

    /**
    * Get data check total equal 0.
    * @return object $result
    */
    public function getDataCheckTotal()
    {
        $col = [
            'dt_delivery.received_order_id',
            'dt_delivery.subdivision_num',
        ];
        return $this->select($col)
                    ->where('account', '代引')
                    ->where('total_price', 0)
                    ->whereIn('delivery_status', [0, 7])
                    ->get();
    }

    /**
    * Get data check delivery code.
    * @return object $result
    */
    public function getDataCheckDeliveryCode()
    {
        $col = [
            'dt_delivery.received_order_id',
            'dt_delivery.subdivision_num',
        ];
        return $this->select($col)
                    ->where(function ($sub) {
                        $sub->whereNull('delivery_code')
                            ->orwhereNotIn('delivery_code', [3, 4]);
                    })
                    ->whereIn('delivery_status', [0, 7])
                    ->get();
    }

    /**
    * Get data check total delivery city.
    * @return object $result
    */
    public function getDataCheckDeliveryCity()
    {
        $col = [
            'dt_delivery.received_order_id',
            'dt_delivery.subdivision_num',
        ];
        return $this->select($col)
                    ->whereNull('delivery_city')
                    ->whereIn('delivery_status', [0, 7])
                    ->get();
    }

    /**
    * Get data by key.
    * @return object $result
    */
    public function getDataDeliveryByKey($arrKey)
    {
        $result = $this->select('price_invoice')
                        ->join('dt_delivery_detail AS ddd', function ($sub) use ($arrKey) {
                            $sub->on('ddd.received_order_id', '=', 'dt_delivery.received_order_id')
                                ->on('ddd.subdivision_num', '=', 'dt_delivery.subdivision_num')
                                ->where($arrKey);
                        })
        ->join('mst_product', 'mst_product.product_code', '=', 'ddd.product_code')
        ->first();
        return $result;
    }

    /**
    * Get max subdivision num.
    * @param string $receivedOrderId
    * @return object
    */
    public function getMaxSubdivisionNum($receivedOrderId)
    {
        return $this->selectRaw("MAX(subdivision_num) AS subdivision_num")
                    ->where('received_order_id', $receivedOrderId)
                    ->groupBy('received_order_id')
                    ->first();
    }

    /**
    * Get data check delivery num.
    * @return object
    */
    public function getDataCheckDeliveryNum()
    {
        $col = [
            'dt_delivery.received_order_id',
            'dt_delivery.subdivision_num',
        ];
        return $this->select($col)
                    ->join('dt_delivery_detail AS ddd', function ($sub) {
                        $sub->on('ddd.received_order_id', '=', 'dt_delivery.received_order_id')
                            ->on('ddd.subdivision_num', '=', 'dt_delivery.subdivision_num');
                    })
                    ->where(function ($sub) {
                        $sub->whereNull('ddd.delivery_num')
                            ->orwhere('ddd.delivery_num', '');
                    })
                    ->whereIn('delivery_status', [0, 7])
                    ->groupBy(['ddd.received_order_id', 'ddd.subdivision_num'])
                    ->get();
    }

    /**
    * Get max OrderNo
    * @return object boolean
    */
    public function getMaxOrderReturn()
    {
        return $this->select('received_order_id')
                    ->where('received_order_id', 'like', 'r-%-4%')
                    ->orderBy('in_date', 'DESC')
                    ->first();
    }

    /**
    * Get data check delivery detail.
    * @return object
    */
    public function getDataCheckDeliveryDetail()
    {
        $col = [
            'dt_delivery.received_order_id',
            'dt_delivery.subdivision_num',
        ];

        return $this->select($col)
                    ->leftJoin('dt_delivery_detail AS ddd', function ($sub) {
                        $sub->on('ddd.received_order_id', '=', 'dt_delivery.received_order_id')
                            ->on('ddd.subdivision_num', '=', 'dt_delivery.subdivision_num');
                    })
                    ->whereNull('ddd.received_order_id')
                    ->whereIn('delivery_status', [0, 7])
                    ->groupBy(['dt_delivery.received_order_id', 'dt_delivery.subdivision_num'])
                    ->get();
    }
    /**
    * Get data check API update status.
    * @return object
    */
    public function getDataCheckUpdated()
    {
        $col = [
            'dt_delivery.received_order_id',
            'dt_delivery.subdivision_num',
        ];
        return $this->select($col)
                    ->selectRaw("CONCAT(dt_delivery.received_order_id, dt_delivery.subdivision_num) AS key_check")
                    ->whereIn('delivery_status', [1, 2])
                    ->whereDate('delivery_plan_date', date('Y-m-d'))
                    ->get();
    }

    /**
     * Get data check update delivery result.
     * @param  string $receivedOrderId
     * @param  string $subdivision
     * @return object
     */
    public function getDataCheckUpdateDeliResult($receivedOrderId, $subdivision)
    {
        return $this->whereIn('delivery_status', [1, 2, 8])
                    ->where('received_order_id', $receivedOrderId)
                    ->where('subdivision_num', $subdivision)
                    ->first();
    }
}
