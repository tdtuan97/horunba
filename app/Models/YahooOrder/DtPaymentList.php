<?php
/**
 * Model for dt_payment_list table.
 *
 * @package    App\Models\YahooOrder
 * @subpackage DtPaymentList
 * @copyright  Copyright (c) 2018 RiverCrane! Corporation. All Rights Reserved.
 * @author     van.qui<van.qui@rivercrane.vn>
 */

namespace App\Models\YahooOrder;

use Illuminate\Database\Eloquent\Model;

class DtPaymentList extends Model
{
    public $timestamps = false;

    /**
    * The database table used by the model.
    * @var string
    */
    protected $table = 'dt_payment_list';

    /**
    * The database is used by the model.
    * @var string
    */
    protected $connection = 'horunba_yahoo_order';
    
    /**
     * Update data
     *
     * @param  array    $conds
     * @param  array    $data
     * @return boolean
     */
    public function updateData($conds, $data)
    {
        return $this->where($conds)->update($data);
    }
}
