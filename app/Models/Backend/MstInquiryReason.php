<?php

/**
 * Exe sql for hrnb.mst_inquiry_reason
 *
 * @package    App\Models
 * @subpackage MstOrderSubStatus
 * @copyright  Copyright (c) 2017 RiverCrane! Corporation. All Rights Reserved.
 * @author     van.qui<van.qui.rcvn2012@gmail.com>
 */

namespace App\Models\Backend;

use Illuminate\Database\Eloquent\Model;

class MstInquiryReason extends Model
{
    public $timestamps = false;
    /**
    * The database table used by the model.
    * @var string
    */
    protected $table = 'mst_inquiry_reason';
    /**
    * The database is used by the model.
    * @var string
    */
    protected $connection = 'horunba';
    /**
     * Get data of tabe mst_mall
     *
     * @return object
     */
    public function getData()
    {
        $data = $this->select('inquiry_code AS key', 'inquiry_content AS value')->get();
        return $data;
    }
}
