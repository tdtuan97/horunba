<?php

/**
 * Exe sql for hrnb.mst_order_sub_status
 *
 * @package    App\Models
 * @subpackage MstOrderSubStatus
 * @copyright  Copyright (c) 2017 RiverCrane! Corporation. All Rights Reserved.
 * @author     van.qui<van.qui.rcvn2012@gmail.com>
 */

namespace App\Models\Backend;

use Illuminate\Database\Eloquent\Model;

class MstOrderSubStatus extends Model
{
    public $timestamps = false;
    /**
    * The database table used by the model.
    * @var string
    */
    protected $table = 'mst_order_sub_status';
    /**
    * The database is used by the model.
    * @var string
    */
    protected $connection = 'horunba';
    /**
     * Get data of tabe mst_mall
     *
     * @return object
     */
    public function getData()
    {
        $data = $this->select('order_sub_status_id AS key', 'sub_status_name AS value')->get();
        return $data;
    }
}
