<?php

/**
 * Exe sql for hrnb.mst_csv_limit
 *
 * @package    App\Models
 * @subpackage MstCsvLimit
 * @copyright  Copyright (c) 2018 RiverCrane! Corporation. All Rights Reserved.
 * @author     Truong Nghia<truong.van.nghia@rivercrane.vn>
 */

namespace App\Models\Backend;

use Illuminate\Database\Eloquent\Model;

class MstCsvLimit extends Model
{
    public $timestamps = false;
    /**
    * The database table used by the model.
    * @var string
    */
    protected $table = 'mst_csv_limit';
    /**
    * The database is used by the model.
    * @var string
    */
    protected $connection = 'horunba';
    /**
     * Get data process web rakuten
     * @return object
     */
    public function getLimitByName($name)
    {
        $result = $this->select('limit')
                       ->where('name_csv', $name)
                       ->first();
        return $result;
    }
}
