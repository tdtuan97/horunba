<?php

/**
 * Exe sql for hrnb.mst_send_mail_timing
 *
 * @package    App\Models
 * @subpackage MstSendMailTiming
 * @copyright  Copyright (c) 2017 RiverCrane! Corporation. All Rights Reserved.
 * @author     le.hung<le.hung.rcvn2012@gmail.com>
 */

namespace App\Models\Backend;

use Illuminate\Database\Eloquent\Model;
use Auth;

class MstSendMailTiming extends Model
{
    public $timestamps = false;
    /**
    * The database table used by the model.
    * @var string
    */
    protected $table = 'mst_send_mail_timing';
    /**
    * The database is used by the model.
    * @var string
    */
    protected $connection = 'horunba';


    /**
     * Get data of tabe mst_send_mail_timing
     * @param  array $arraySearch
     * @param  array $arraySort
     * @return object
     */
    public function getData($arraySearch = null, $arraySort = null)
    {
        $data = $this->select('mst_send_mail_timing.is_available')
                ->addSelect('mst_send_mail_timing.order_status_id')
                ->addSelect('mst_send_mail_timing.order_sub_status_id')
                ->addSelect('mst_order_status.status_name')
                ->addSelect('mst_order_sub_status.sub_status_name')
                ->addSelect('mst_mail_template.template_name')
                ->leftJoin('mst_order_status', function ($join) {
                    $join->on('mst_order_status.order_status_id', '=', 'mst_send_mail_timing.order_status_id');
                })
                ->leftJoin('mst_order_sub_status', function ($join) {
                    $join->on(
                        'mst_order_sub_status.order_sub_status_id',
                        '=',
                        'mst_send_mail_timing.order_sub_status_id'
                    );
                })
                ->leftJoin('mst_mail_template', function ($join) {
                    $join->on('mst_send_mail_timing.mail_id', '=', 'mst_mail_template.mail_id');
                })
                ->groupBy('mst_send_mail_timing.order_status_id', 'mst_send_mail_timing.order_sub_status_id');

        if (count($arraySearch) > 0) {
            $data->where(function ($query) use ($arraySearch) {
                if (isset($arraySearch['mail_id'])) {
                    $query->where('mail_id', "{$arraySearch['mail_id']}");
                }
                if (isset($arraySearch['status_name'])) {
                    $query->where('mst_order_status.status_name', 'LIKE', "%{$arraySearch['status_name']}%");
                }
                if (isset($arraySearch['sub_status_name'])) {
                    $query->where(
                        'mst_order_sub_status.sub_status_name',
                        'LIKE',
                        "%{$arraySearch['sub_status_name']}%"
                    );
                }
                if (isset($arraySearch['template_name'])) {
                    $query->where('mst_mail_template.template_name', 'LIKE', "%{$arraySearch['template_name']}%");
                }
            });
        }

        if ($arraySort !== null && count($arraySort) > 0) {
            if (!is_array($arraySort)) {
                $arraySort = json_decode($arraySort);
            }
            foreach ($arraySort as $column => $sort) {
                $data->orderBy($column, $sort);
            }
        }

        $perPage = ($arraySearch['per_page']) ? $arraySearch['per_page'] : 20;
        $data = $data->paginate($perPage);
        return $data;
    }

    /**
     * Update data
     *
     * @param  array $keys
     * @param  array $data
     * @return boolean
     */
    public function changeStatus($keys, $data)
    {
        $result = $this->where('order_status_id', '=', $keys['order_status_id'])
                    ->where('order_sub_status_id', '=', $keys['order_sub_status_id'])
                    ->update($data);
        return true;
    }

    /**
     * Save data
     *
     * @param   array    $data
     * @return  boolean
     */
    public function saveData($data)
    {
        $sendMailTiming = $this->where('order_status_id', $data['order_status_id'])
                                ->where('order_sub_status_id', $data['order_sub_status_id'])
                                ->first();
        if (empty($sendMailTiming)) {
            return $this->insert([
                'order_status_id'     => $data['order_status_id'],
                'order_sub_status_id' => $data['order_sub_status_id'],
                'mail_id'             => $data['mail_id'],
                'in_ope_cd'           => Auth::user()->tantou_code,
                'up_ope_cd'           => Auth::user()->tantou_code,
                'in_date'             => date('Y-m-d H:i:s'),
                'up_date'             => date('Y-m-d H:i:s')
            ]);
        } else {
            return $this->where('order_status_id', $data['order_status_id'])
                ->where('order_sub_status_id', $data['order_sub_status_id'])
                ->update([
                    'mail_id'   => $data['mail_id'],
                    'up_ope_cd' => Auth::user()->tantou_code,
                    'up_date'   => date('Y-m-d H:i:s')
                ]);
        }
    }

    /*
     * Get Item by primary key
     *
     * @param   int   $orderStatusId    order_status_id
     * @param   int   $orderSubStatusId order_sub_status_id
     * @return  object
     */
    public function getItem($orderStatusId, $orderSubStatusId)
    {
        $data = $this->where('order_status_id', $orderStatusId)
                    ->where('order_sub_status_id', $orderSubStatusId)
                    ->first();
        return $data;
    }
}
