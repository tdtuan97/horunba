<?php

/**
 * Exe sql for hrnb.mst_genre
 *
 * @package    App\Models
 * @subpackage MstGenre
 * @copyright  Copyright (c) 2017 RiverCrane! Corporation. All Rights Reserved.
 * @author     van.qui<van.qui.rcvn2012@gmail.com>
 */

namespace App\Models\Backend;

use Illuminate\Database\Eloquent\Model;

class MstGenre extends Model
{
    public $timestamps = false;
    /**
    * The database table used by the model.
    * @var string
    */
    protected $table = 'mst_genre';
    /**
    * The database is used by the model.
    * @var string
    */
    protected $connection = 'horunba';
    
    /**
    * The primary key.
    * @var int
    */
    protected $primaryKey = 'genre_id';
    
    /**
     * Get data of table mst_genre
     *
     * @params  array    $arraySearch array data search
     * @params  array    $arraySort   array data sort
     * @return object
     */
    public function getData($arraySearch = null, $arraySort = null)
    {
        $data = $this->select('genre_id', 'genre_name', 'remarks', 'del_flg');
        if (count($arraySearch) > 0) {
            $data->where(function ($query) use ($arraySearch) {
                if (isset($arraySearch['genre_name'])) {
                    $query->where('genre_name', 'LIKE', "%{$arraySearch['genre_name']}%");
                }
                if (isset($arraySearch['remarks'])) {
                    $query->where('remarks', 'LIKE', "%{$arraySearch['remarks']}%");
                }
            });
        }
        if ($arraySort !== null && count($arraySort) > 0) {
            foreach ($arraySort as $column => $sort) {
                if ($sort !== null && in_array($sort, ['asc', 'desc'])) {
                    $data->orderBy($column, $sort);
                }
            }
        }
        $data->orderBy('genre_id', 'desc');
        $perPage = ($arraySearch['per_page']) ? $arraySearch['per_page'] : 20;
        $data = $data->paginate($perPage);
        return $data;
    }
}
