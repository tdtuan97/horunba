<?php
/**
 * Exe sql for hrnb.mst_cancel_reservation
 *
 * @package    App\Models
 * @subpackage MstCancelReservation
 * @copyright  Copyright (c) 2018 RiverCrane! Corporation. All Rights Reserved.
 * @author     le.hung<le.hung.rcvn2012@gmail.com>
 */

namespace App\Models\Backend;

use Illuminate\Database\Eloquent\Model;

class MstCancelReservation extends Model
{
    public $timestamps = false;

    /**
    * The database table used by the model.
    * @var string
    */
    protected $table = 'mst_cancel_reservation';
    /**
    * The database is used by the model.
    * @var string
    */
    protected $connection = 'horunba';

    /**
     * Get data of table
     *
     * @param array $arraySearch , array $arraySort
     * @return object
     */
    public function getData($arraySearch = null, $arraySort = null)
    {
        $data = $this->select('mall_id', 'receive_order_id', 'status', 'note')
                ->addSelect('mst_mall.name_jp')
                ->addSelect('mst_cancel_reservation.status')
                ->addSelect('mst_cancel_reservation.is_deleted')
                ->addSelect('mst_cancel_reservation.in_date')
                ->addSelect('mst_cancel_reservation.up_date')
                ->join('mst_mall', 'mst_mall.id', '=', 'mst_cancel_reservation.mall_id');
        if (count($arraySearch) > 0) {
            $data->where(function ($query) use ($arraySearch) {
                if (isset($arraySearch['mall_id'])) {
                    if (is_array($arraySearch['mall_id'])) {
                        $query->whereIn('mst_cancel_reservation.mall_id', $arraySearch['mall_id']);
                    } else {
                        $query->where('mst_cancel_reservation.mall_id', $arraySearch['mall_id']);
                    }
                }
                if (isset($arraySearch['status'])) {
                    if (is_array($arraySearch['status'])) {
                        $query->whereIn('mst_cancel_reservation.status', $arraySearch['status']);
                    } else {
                        $query->where('mst_cancel_reservation.status', $arraySearch['status']);
                    }

                }
            });
        }
        if ($arraySort !== null && count($arraySort) > 0) {
            foreach ($arraySort as $column => $sort) {
                if ($sort !== null && in_array($sort, ['asc', 'desc'])) {
                    $data->orderBy($column, $sort);
                }
            }
        } else {
            $data->orderBy('up_date', 'desc');
        }
        $perPage = ($arraySearch['per_page']) ? $arraySearch['per_page'] : 20;
        $data = $data->paginate($perPage);
        return $data;
    }
    /**
     * Update data
     *
     * @param  array $dataWhere for condition
     * @param  array $dataUpdate for update
     * @return object
     */
    public function updateData($dataWhere, $dataUpdate)
    {
        $data = $this->where($dataWhere)->update($dataUpdate);
        return $data;
    }
    /**
     * Get item data
     *
     * @param  array  mall_id & receive_order_id
     * @return object
     */
    public function getItem($dataWhere)
    {
        $data = $this->select('*')
                    ->where($dataWhere)
                    ->first();
        return $data;
    }
}
