<?php

/**
 * Exe sql for hrnb.dt_send_mail_list
 *
 * @package    App\Models
 * @subpackage DtSendMailList
 * @copyright  Copyright (c) 2017 RiverCrane! Corporation. All Rights Reserved.
 * @author     le.hung<le.hung.rcvn2012@gmail.com>
 */

namespace App\Models\Backend;

use Illuminate\Database\Eloquent\Model;

class DtSendMailList extends Model
{
    public $timestamps = false;
    /**
    * The database table used by the model.
    * @var string
    */
    protected $table = 'dt_send_mail_list';

    /**
    * The database is used by the model.
    * @var string
    */
    protected $connection = 'horunba';

    /**
     * Get data of tabe dt_send_mail_list
     *
     * @return object
     */
    public function getData($arraySearch = null, $arraySort = null)
    {
        $data = $this->select('dt_send_mail_list.receive_order_id')
                ->addSelect('dt_send_mail_list.order_status_id')
                ->addSelect('dt_send_mail_list.order_sub_status_id')
                ->addSelect('dt_send_mail_list.mail_subject')
                ->addSelect('dt_send_mail_list.send_status')
                ->addSelect('dt_send_mail_list.in_date')
                ->join('mst_order_status', function ($join) {
                    $join->on('dt_send_mail_list.order_status_id', '=', 'mst_order_status.order_status_id');
                })
                ->join('mst_order_sub_status', function ($join) {
                    $join->on('dt_send_mail_list.order_sub_status_id', '=', 'mst_order_sub_status.order_sub_status_id');
                });
        if (count($arraySearch) > 0) {
            $data->where(function ($query) use ($arraySearch) {
                if (isset($arraySearch['receive_order_id'])) {
                    $query->where('dt_send_mail_list.receive_order_id', 'LIKE', "{$arraySearch['receive_order_id']}%");
                }
                if (isset($arraySearch['order_sub_status_id'])) {
                    $query->where(
                        'dt_send_mail_list.order_sub_status_id',
                        'LIKE',
                        "{$arraySearch['order_sub_status_id']}%"
                    );
                }
                if (isset($arraySearch['order_status_id'])) {
                    $query->where('dt_send_mail_list.order_status_id', 'LIKE', "{$arraySearch['order_status_id']}%");
                }
                if (isset($arraySearch['mail_subject'])) {
                    $query->where('dt_send_mail_list.mail_subject', 'LIKE', "{$arraySearch['mail_subject']}%");
                }
                if (isset($arraySearch['send_status'])
                        &&
                        $arraySearch['send_status'] !=='' ) {
                    $query->where('dt_send_mail_list.send_status', "{$arraySearch['send_status']}");
                }
            });
        }
        if ($arraySort !== null && count($arraySort) > 0) {
            if (!is_array($arraySort)) {
                $arraySort = json_decode($arraySort);
            }
            foreach ($arraySort as $column => $sort) {
                $data->orderBy($column, $sort);
            }
        }
        $perPage = ($arraySearch['per_page']) ? $arraySearch['per_page'] : 20;
        $data = $data->paginate($perPage);
        return $data;
    }
    /**
     * Get data of tabe dt_send_mail_list
     *
     * @return object
     */
    public function getItem($arrayQuery = null)
    {
        return $this->select('dt_send_mail_list.receive_order_id')
                ->addSelect('dt_send_mail_list.order_status_id')
                ->addSelect('dt_send_mail_list.operater_send_index')
                ->addSelect('dt_send_mail_list.order_sub_status_id')
                ->addSelect('dt_send_mail_list.mail_subject')
                ->addSelect('dt_send_mail_list.send_status')
                ->addSelect('dt_send_mail_list.in_date')
                ->addSelect('dt_send_mail_list.attached_file_path')
                ->addSelect('dt_send_mail_list.mail_content')
                ->addSelect('mst_order_sub_status.sub_status_name')
                ->addSelect('mst_order_status.status_name')
                ->addSelect('mst_order.receive_id')
                ->join('mst_order_status', function ($join) {
                    $join->on('dt_send_mail_list.order_status_id', '=', 'mst_order_status.order_status_id');
                })
                ->join('mst_order', function ($join) {
                    $join->on('dt_send_mail_list.receive_order_id', '=', 'mst_order.received_order_id');
                })
                ->join('mst_order_sub_status', function ($join) {
                    $join->on('dt_send_mail_list.order_sub_status_id', '=', 'mst_order_sub_status.order_sub_status_id');
                })
                ->where('dt_send_mail_list.receive_order_id', $arrayQuery['receive_order_id'])
                ->where('dt_send_mail_list.order_status_id', $arrayQuery['order_status_id'])
                ->where('dt_send_mail_list.order_sub_status_id', $arrayQuery['order_sub_status_id'])
                ->where('dt_send_mail_list.operater_send_index', $arrayQuery['operater_send_index'])
                ->first();
    }

    /**
     * Get data of tabe dt_send_mail_list
     * @param array $arrayQuery
     * @return object
     */
    public function getItems($arrayQuery = null)
    {
        return $this->select('dt_send_mail_list.receive_order_id')
                ->addSelect('dt_send_mail_list.order_status_id')
                ->addSelect('dt_send_mail_list.order_sub_status_id')
                ->addSelect('dt_send_mail_list.mail_subject')
                ->addSelect('dt_send_mail_list.send_status')
                ->addSelect('dt_send_mail_list.send_type')
                ->addSelect('dt_send_mail_list.in_date')
                ->addSelect('dt_send_mail_list.attached_file_path')
                ->addSelect('dt_send_mail_list.mail_content')
                ->join('mst_order_status', function ($join) {
                    $join->on('dt_send_mail_list.order_status_id', '=', 'mst_order_status.order_status_id');
                })
                ->addSelect('mst_order_status.status_name')
                ->join('mst_order_sub_status', function ($join) {
                    $join->on('dt_send_mail_list.order_sub_status_id', '=', 'mst_order_sub_status.order_sub_status_id');
                })
                ->addSelect('mst_order_sub_status.sub_status_name')
                ->where('dt_send_mail_list.receive_order_id', $arrayQuery['receive_order_id'])->get();
    }
    /**
     * Update data
     *
     * @param  array $keys
     * @param  array $data
     * @return boolean
     */
    public function updateData($keys, $data)
    {
        return $this->where($keys)->update($data);
    }
    /**
     * Check status order
     *
     * @param  array $data
     * @return object
     */
    public function checkOrder($data)
    {
        $data = $this->where('receive_order_id', '=', $data['receive_order_id'])
                ->where('order_status_id', '=', $data['order_status_id'])
                ->where('order_sub_status_id', '=', $data['order_sub_status_id'])
                ->count();
        return $data;
    }
    /**
     * Get mail by received_order_id
     *
     * @param  string $receivedOrderId
     * @return object
     */
    public function getDataByReceivedOrderId($receivedOrderId = null)
    {
        $result = $this->select([
                            'receive_order_id',
                            'order_status_id',
                            'order_sub_status_id',
                            'operater_send_index',
                            'mail_subject',
                            'send_status',
                            'in_date',
                       ])
                       ->where('receive_order_id', $receivedOrderId)
                       ->get();
        return $result;
    }
    /**
     * Get max operater_send_index for case has order
     *
     * @param  string $receivedOrderId
     * @return object
     */
    public function getMaxSendIndex($dataWhere)
    {
        $result = $this->select(['operater_send_index'])->where($dataWhere)
                       ->max('operater_send_index');
        return $result;
    }

    /**
     * Get mail reply by uid_mail
     *
     * @param  int $uidMail
     * @return object
     */
    public function getMailReply($uidMail)
    {
        $result = $this->where('uid_mail', $uidMail)
                       ->get();
        return $result;
    }
}
