<?php

/**
 * Exe sql for hrnb.dt_purchased_point
 *
 * @package    App\Models
 * @subpackage DtWebYahData
 * @copyright  Copyright (c) 2019 RiverCrane! Corporation. All Rights Reserved.
 * @author     Truong Nghia<truong.van.nghia@rivercrane.vn>
 */

namespace App\Models\Backend;

use Illuminate\Database\Eloquent\Model;
use DB;
use App\Custom\DBExtend;

class DtPurchasedPoint extends Model
{
    use DBExtend;
    public $timestamps = false;

    /**
    * The database table used by the model.
    * @var string
    */
    protected $table = 'dt_purchased_point';
    /**
    * The database is used by the model.
    * @var string
    */
    protected $connection = 'horunba';

    /**
     * Update data
     * @param  array condition $where
     * @param  array update $data
     * @return boolean
     */
    public function updateData($where, $data)
    {
        $result = $this->where($where)
                    ->update($data);
        return $result;
    }
    /**
     * Get data by member_id
     * @param  string $memberId
     * @return object
     */
    public function getDataByMemberId($memberId)
    {
        $result = $this->select()
                       ->where('member_id', $memberId)
                       ->get();
        return $result;
    }
    /**
     * Get data by member_id
     * @param  string $memberId
     * @return object
     */
    public function getDataDetail($memberId, $detailNum)
    {
        $result = $this->select()
                       ->where('member_id', $memberId)
                       ->where('detail_num', $detailNum)
                       ->first();
        return $result;
    }

    /**
     * Get detail num by member_id
     * @param  string $memberId
     * @return object
     */
    public function getDataDetailNum($memberId)
    {
        $result = $this->selectRaw('MAX(detail_num) as detail_num')
                       ->where('member_id', $memberId)
                       ->first();
        return $result;
    }
}
