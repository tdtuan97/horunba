<?php

/**
 * Exe sql for hrnb.mst_return_address
 *
 * @package    App\Models\Backend
 * @subpackage MstReturnAddress
 * @copyright  Copyright (c) 2018 RiverCrane! Corporation. All Rights Reserved.
 * @author     le.hung<le.hung@rivercrane.com.vn>
 */

namespace App\Models\Backend;

use Illuminate\Database\Eloquent\Model;

class MstReturnAddress extends Model
{
    public $timestamps = false;
    /**
    * The database table used by the model.
    * @var string
    */
    protected $table = 'mst_return_address';
    /**
    * The primary key.
    * @var string
    */
    protected $primaryKey = 'return_id';
    /**
    * The database is used by the model.
    * @var string
    */
    protected $connection = 'horunba';

    /**
    * Get data by supplier_cd
    * @param string $supplier_cd
    * @return object
    */
    public function getData($supplier_cd)
    {
        $col = [
                'supplier_cd',
                'return_id',
                'return_nm',
                'return_postal',
                'return_address',
                'return_pref',
                'return_tel',
                'is_selected'
            ];
        $result = $this->select($col)
                ->where('supplier_cd', '=', $supplier_cd)
                ->get();
        return $result;
    }
    /**
    * Get data by supplier_cd
    * @param string $supplier_cd
    * @return object
    */
    public function getDataReturnAddressSave($arrayWhere = null)
    {
        $col = [
                'mst_return_address.supplier_cd',
                'mst_supplier.supplier_nm',
                'mst_return_address.return_id',
                'mst_return_address.return_nm',
                'mst_return_address.return_postal',
                'mst_return_address.return_address',
                'mst_return_address.return_pref',
                'mst_return_address.return_tel',
                'is_selected'
            ];
        $result = $this->select($col)
                ->join('mst_supplier', 'mst_supplier.supplier_cd', '=', 'mst_return_address.supplier_cd')
                ->where($arrayWhere);
        return $result;
    }
    /**
    * Get data by supplier_cd
    * @param string $supplier_cd
    * @return object
    */
    public function getDataList($arraySearch = null, $arraySort = null)
    {
        $col = [
                'mst_supplier.supplier_nm',
                'mst_return_address.supplier_cd',
                'mst_return_address.return_id',
                'mst_return_address.return_nm',
                'mst_return_address.return_postal',
                'mst_return_address.return_address',
                'mst_return_address.return_pref',
                'mst_return_address.return_tel',
                'mst_return_address.is_selected'
            ];
        $data = $this->select($col)
                ->join('mst_supplier', function ($join) {
                    $join->on('mst_supplier.supplier_cd', '=', 'mst_return_address.supplier_cd');
                });
        if (count($arraySearch) > 0) {
            $data->where(function ($query) use ($arraySearch) {
                if (isset($arraySearch['supplier_nm'])) {
                    $query->where('mst_supplier.supplier_nm', 'LIKE', "%{$arraySearch['supplier_nm']}%");
                }
                if (isset($arraySearch['return_postal'])) {
                    $query->where('mst_return_address.return_postal', 'LIKE', "%{$arraySearch['return_postal']}%");
                }
                if (isset($arraySearch['return_pref'])) {
                    $query->where('mst_return_address.return_pref', 'LIKE', "%{$arraySearch['return_pref']}%");
                }
                if (isset($arraySearch['return_tel'])) {
                    $query->where('mst_return_address.return_tel', 'LIKE', "%{$arraySearch['return_tel']}%");
                }
                if (isset($arraySearch['return_address'])) {
                    $query->where('mst_return_address.return_address', 'LIKE', "%{$arraySearch['return_address']}%");
                }
            });
        }
        $data = $data->groupBy('mst_return_address.supplier_cd');
        $check = false;
        if ($arraySort !== null && count($arraySort) > 0) {
            foreach ($arraySort as $column => $sort) {
                if ($sort !== null && in_array($sort, ['asc', 'desc'])) {
                    $data->orderBy($column, $sort);
                    $check = true;
                }
            }
        }
        if (!$check) {
            $data->orderBy('return_id', 'desc');
        }
        $perPage = ($arraySearch['per_page']) ? $arraySearch['per_page'] : 20;
        $data = $data->paginate($perPage);
        return $data;
    }

    /**
    * Get data by supplier_cd
    * @param string $supplier_cd
    * @return object
    */
    public function getDataIsChoose($supplierCd)
    {
        $result = $this->select('supplier_cd', 'return_id', 'return_'
                . 'nm', 'return_postal', 'return_address', 'return_pref', 'return_tel', 'is_selected')
                ->where('supplier_cd', '=', $supplierCd)
                ->where('is_selected', 1)
                ->first();
        return $result;
    }
    /**
     * Update data
     *
     * @param  array $dataWhere for condition
     * @param  array $dataUpdate for update
     * @return object
     */
    public function updateData($dataWhere, $dataUpdate)
    {
        $data = $this->where($dataWhere)->update($dataUpdate);
        return $data;
    }
}
