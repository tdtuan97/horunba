<?php

/**
 * Exe sql for hrnb.mst_mall
 *
 * @package    App\Models
 * @subpackage MstMall
 * @copyright  Copyright (c) 2017 RiverCrane! Corporation. All Rights Reserved.
 * @author     le.hung<le.hung.rcvn2012@gmail.com>
 */

namespace App\Models\Backend;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class MstMall extends Model
{
    public $timestamps = false;
    /**
    * The database table used by the model.
    * @var string
    */
    protected $table = 'mst_mall';

    /**
    * The database is used by the model.
    * @var string
    */
    protected $connection = 'horunba';

    /**
     * Get data of tabe mst_mall
     *
     * @return object
     */
    public function getDataSelectBox()
    {
        $data = $this->select('id', 'name_jp')->get();
        return $data;
    }

    /**
     * Get data of tabe mst_mall
     *
     * @return object
     */
    public function getData()
    {
        $data = $this->select('id AS key', 'name_jp AS value')->get();
        return $data;
    }

    /**
     * Get data of tabe mst_mall
     *
     * @return object
     */
    public function getMallName($mallId)
    {
        $data = $this->select('name_jp')->where('id', $mallId)->first();
        return $data;
    }
}
