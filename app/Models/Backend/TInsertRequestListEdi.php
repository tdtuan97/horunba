<?php

/**
 * Model for tinsertRequestList table.
 *
 * @package    App\Models\Batches
 * @subpackage TInsertRequestList
 * @copyright  Copyright (c) 2018 RiverCrane! Corporation. All Rights Reserved.
 * @author     Le Hung<le.hung@rivercrane.vn>
 */

namespace App\Models\Backend;

use Illuminate\Database\Eloquent\Model;

class TInsertRequestListEdi extends Model
{
    public $timestamps = false;

    /**
    * The database table used by the model.
    * @var string
    */
    protected $table = 't_insertRequestList';

    /**
    * The database is used by the model.
    * @var string
    */
    protected $connection = 'edi';

    /**
     * Get request list
     * @param array $data use for get data follow conditions
     * @return array
     */
    public function getTInsertRequestList($data)
    {
        $data = $this->where('RequestNo', "=", $data['no'])
                     ->max('RequestBNo');
        return $data;
    }

    /**
     * Insert data into t_insertRequestList
     * @param array $data use for insert new data
     * @return array
     */
    public function insertTInsertRequestList($data)
    {
        $id = $this->insertGetId($data);
        return $id;
    }

    /**
     * Get data process insert request performence model
     * @param string $orderCode
     * @return array
     */
    public function getDataProcessRequest($orderCode)
    {
        $result = $this->where('order_code', $orderCode)
                       ->whereRaw("date(ArrivalPlanDate) = date(NOW())")
                       ->first();
        return $result;
    }
}
