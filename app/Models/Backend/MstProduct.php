<?php

/**
 * Exe sql for hrnb.mst_product
 *
 * @package    App\Models\Backend
 * @subpackage MstProduct
 * @copyright  Copyright (c) 2018 RiverCrane! Corporation. All Rights Reserved.
 * @author     van.qui<van.qui.rcvn2012@gmail.com>
 */

namespace App\Models\Backend;

use DB;
use Illuminate\Database\Eloquent\Model;

class MstProduct extends Model
{
    public $timestamps = false;
    /**
    * The database table used by the model.
    * @var string
    */
    protected $table = 'mst_product';

    /**
    * The database is used by the model.
    * @var string
    */
    protected $connection = 'horunba';

    /**
    * Get info product by product code process return order.
    * @param string $productCode
    * @return object
    */
    public function getInfoByProdCodeReturn($productCode)
    {
        $col = [
            'price_invoice',
            'product_name'
        ];
        $result = $this->select($col)->where('product_code', '=', $productCode)->first();
        return $result;
    }

    /**
    * Get info supplier by product code process return order.
    * @param string $productCode
    * @return object
    */
    public function getDataProcessReturn($productCode)
    {
        $col = [
            'return_postal AS supplier_return_postal',
            'return_pref AS supplier_return_pref',
            'return_address AS supplier_return_address',
            'return_nm AS supplier_return_nm',
            'return_tel AS supplier_return_tel',
            'ms.supplier_cd AS supplier_id',
        ];
        $result = $this->select($col)
                       ->join('mst_supplier as ms', function ($join) use ($productCode) {
                            $join->on('mst_product.price_supplier_id', '=', 'ms.supplier_cd')
                                 ->where('mst_product.product_code', '=', $productCode);
                       })
                       ->first();
        return $result;
    }

    /**
     * Get item by product_code
     *
     * @param  string $productCode
     * @return object
     */
    public function getItemByProductCode($productCode)
    {
        $data = $this->select(
            'product_code',
            'product_name',
            'product_name_long',
            'price_invoice AS price',
            'price_invoice',
            'product_service',
            'price_supplier_id',
            'supplier_code',
            'product_jan',
            'maker_full_nm',
            'product_maker_code',
            'product_color',
            'product_size',
            'rak_img_url_1'
        )
        ->where('product_code', $productCode)
        ->first();
        return $data;
    }

    /**
     * Get item by product_codes
     *
     * @param  array $productCodes
     * @return object
     */
    public function getItemByProductCodes($productCodes)
    {
        $data = $this->select(
            'product_code',
            'product_name_long',
            'product_jan',
            'price_invoice'
        )
        ->whereIn('product_code', $productCodes)
        ->get();
        return $data;
    }

    /**
     * Get item detail by product_code
     *
     * @param  string $productCode
     * @return object
     */
    public function getItemDetailByProductCode($productCode)
    {
        $data = $this->select(
            'mst_product.product_code',
            'mst_product.product_name_long as product_name',
            'mst_product.price_invoice AS price',
            'mst_product_set.child_product_code',
            'child.product_name_long as child_product_name',
            'component_num',
            'mst_price_mall.price_calc_rak_komi',
            'mst_price_mall.price_calc_yah_komi',
            'mst_price_mall.price_calc_ama_komi',
            'mst_price_mall.price_calc_biz_nuki',
            'mst_price_mall.price_calc_diy_nuki'
        )
        ->leftJoin('mst_price_mall', 'mst_product.product_code', '=', 'mst_price_mall.product_code')
        ->leftJoin('mst_product_set', 'mst_product_set.parent_product_code', '=', 'mst_product.product_code')
        ->leftJOin('mst_product as child', 'child.product_code', '=', 'mst_product_set.child_product_code')
        ->where('mst_product.product_code', $productCode)
        ->get();
        return $data;
    }
    /**
     * Get name by product_code
     *
     * @param  string $productCode
     * @return object
     */
    public function getNameByProductCode($productCode)
    {
        $data = $this->select(
            'mst_product.product_code',
            'mst_product.product_name'
        )
        ->addSelect(DB::raw("ceil(mst_price_mall.price_calc_diy_nuki * 1.08) AS price_invoice"))
        ->join('mst_price_mall', 'mst_product.product_code', '=', 'mst_price_mall.product_code')
        ->where('mst_product.product_code', $productCode)
        ->get();
        return $data;
    }

    /**
     * Get product set
     *
     * @param  string $productCode
     * @return object
     */
    public function getProductSet($productCode)
    {
        $data = $this->select(
            'mst_product_set.parent_product_code AS product_code',
            'mst_product.product_code AS child_product_code',
            'mst_product_set.component_num',
            'mst_product.price_supplier_id AS supplier_id',
            'mst_product.price_invoice AS price_invoice',
            'mst_product.product_daihiki',
            //'mst_product.stock_default',
            'mst_product.stock_lower_limit',
            'mst_stock_status.nanko_num',
            'mst_stock_status.order_zan_num',
            'mst_stock_status.return_num'
        )
        ->join('mst_product_set', 'mst_product_set.child_product_code', '=', 'mst_product.product_code')
        ->join('mst_stock_status', 'mst_stock_status.product_code', '=', 'mst_product_set.child_product_code')
        ->where('mst_product_set.parent_product_code', $productCode)
        ->get();
        return $data;
    }

    /**
     * Get no product set
     *
     * @param  string $productCode
     * @return object
     */
    public function getNoProductSet($productCode)
    {
        $data = $this->select(
            'mst_product.product_code',
            'mst_product.product_daihiki AS product_daihiki',
            'mst_product.price_supplier_id AS supplier_id',
            'mst_product.price_invoice AS price_invoice',
            //'mst_product.stock_default',
            'mst_product.stock_lower_limit',
            'mst_stock_status.nanko_num',
            'mst_stock_status.order_zan_num',
            'mst_stock_status.return_num'
        )
        ->join('mst_stock_status', 'mst_stock_status.product_code', '=', 'mst_product.product_code')
        ->where('mst_product.product_code', $productCode)
        ->get();
        return $data;
    }

    /**
     * Get infomation product and supplier by product code
     *
     * @param  string $productCode
     * @return object
     */
    public function getInfproductAndSupplier($productCode)
    {
        $col = [
            'product_name',
            'supplier_nm AS supplier_name',
            'supplier_cd AS supplier_code',
        ];
        $result = $this->select($col)
                       ->join('mst_supplier as ms', function ($join) use ($productCode) {
                            $join->on('mst_product.price_supplier_id', '=', 'ms.supplier_cd')
                                 ->where('mst_product.product_code', '=', $productCode);
                       })
                       ->first();
        return $result;
    }

    /**
     * Check product exists
     *
     * @param  string $productCode
     * @return object
     */
    public function checkProductCodeExists($productCode)
    {
        $result = $this->where('product_code', $productCode)
                       ->count();
        return $result;
    }

    /**
     * Get infomation by product code
     *
     * @param  string $productCode
     * @return object
     */
    public function getDataByProductCode($productCode)
    {
        $result = $this->where('product_code', $productCode)
                       ->first();
        return $result;
    }

    /**
     * Get data csv monthly
     *
     * @param  string $productCode
     * @return object
     */
    public function getDataCsvMonthly($arrProductCode)
    {
        $col = [
            'price_invoice',
            'price_supplier_id',
        ];
        $result = $this->whereIn('product_code', $arrProductCode)
                       ->get()->keyBy('product_code');
        return $result;
    }

    /**
     * Get data csv monthly
     *
     * @param  string $productCode
     * @return object
     */
    public function getDataReturnProduct($productCode)
    {
        $col = [
            'mst_product.product_code',
            'mst_product.product_name',
            'mst_product.price_supplier_id',
            'ra.return_id',
            'dt_receipt_ledger.edi_order_code',
            'dt_receipt_ledger.price_invoice',
        ];
        $result = $this->select($col)
                       ->selectRaw("CASE when ra.return_address is null then '返品住所ない' else '品住所変更' end as address")
                       ->leftjoin('mst_return_address AS ra', function ($join) {
                            $join->on('mst_product.price_supplier_id', '=', 'ra.supplier_cd')
                            ->where('ra.is_selected', 1);
                       })
                       ->join('dt_receipt_ledger', function ($join) use ($productCode) {
                            $join->on('dt_receipt_ledger.product_code', '=', 'mst_product.product_code')
                            ->whereRaw("dt_receipt_ledger.index = (SELECT MAX(dt_receipt_ledger.index) FROM dt_receipt_ledger WHERE product_code = '{$productCode}')")
                            ->where('line_index', 1);
                       })
                       ->where('mst_product.product_code', $productCode)
                       ->get();
        return $result;
    }
    /**
     * Get data clone order to supplier
     *
     * @param  string $productCode
     * @return object
     */
    public function getDataCloneOrderSupplier($productCode)
    {
        $result = $this->join('edi.mst_product as emp', function ($join) {
            $join->on('mst_product.product_code', '=', 'emp.product_code')
                ->where('emp.is_live', '=', 1);
        })
           ->where('mst_product.product_code', $productCode)
           ->first();
        return $result;
    }
    /**
     * Get data clone order to supplier
     *
     * @param  string $productCode
     * @return object
     */
    public function getDataProcessMojax()
    {
        $col = [
            'mst_product.product_jan',
            'mst_product.maker_full_nm',
            'mst_product.product_maker_code',
            'mst_product.product_name_long',
            'mst_product.product_oogata',
            'mst_product.product_name',
            'mst_product.rak_img_url_1',
            'dt_return.return_no',
            'dt_return.return_line_no',
            'dt_return.return_time',
            'dt_return.note',
            //'dt_return.parent_product_code',
            //'dt_return.product_code',
            'dt_return.return_quantity',
            'dt_return.delivery_count',
            'dt_return.delivery_instruction',
            'dt_return.supplier_return_no',
            'dt_return.supplier_return_line',
        ];
        $sub = '(CASE WHEN  product_oogata >= 1 THEN 3 ELSE 4 END) AS delivery_code';
        $sub_pro = 'IFNULL(dt_return.product_code, dt_return.parent_product_code) AS product_code';
        $result = $this->select($col)
                       ->selectRaw($sub)
                       ->selectRaw($sub_pro)
                       ->join(
                           'dt_return',
                           'mst_product.product_code',
                           '=',
                           DB::raw('IFNULL(dt_return.product_code, dt_return.parent_product_code)')
                       )
                       ->join('mst_supplier', 'mst_product.price_supplier_id', '=', 'mst_supplier.supplier_cd')
                       ->whereIn('dt_return.delivery_instruction', [0, 8])
                       ->whereIn('dt_return.receive_instruction', [-1, 2])
                       ->where('dt_return.is_mojax', 1)
                       ->orderBy('delivery_count')
                       ->get();
        return $result;
    }

    /**
     * Get data address
     *
     * @param  array $arrProductCode
     * @return object
     */
    public function getDataAdressProduct($arrProductCode)
    {
        $col = [
            'mst_product.product_code',
            'mst_product.price_supplier_id',
            'ra.return_id',
        ];
        $result = $this->select($col)
                       ->selectRaw("CASE when ra.return_address is null then '返品住所ない' else '品住所変更' end as address")
                       ->leftjoin('mst_return_address AS ra', function ($join) {
                            $join->on('mst_product.price_supplier_id', '=', 'ra.supplier_cd')
                            ->where('ra.is_selected', 1);
                       })
                       ->whereIn('mst_product.product_code', $arrProductCode)
                       ->get();
        return $result;
    }

    /**
     * Get data product supplier
     *
     * @param  string $value
     * @param  string $colWhere
     * @return object
     */
    public function getDataProductSupplier($colWhere, $value)
    {
        $col = [
            'mst_product.product_code',
            'mst_product.product_name',
            'mst_product.product_jan',
            'mst_product.price_invoice',
            'mst_product.price_supplier_id AS supplier_id',
            'mst_supplier.supplier_nm',
            'mst_product.supplier_code',
            // 'dt_order_to_supplier.order_num',
            'emp.cheetah_status',
            'mst_product.maker_full_nm',
            'mst_product.product_maker_code',
            'mst_product.product_name_long',
            'mst_product.product_color',
            'mst_product.product_size',
            'mst_product.rak_img_url_1',
            't_admin.fax',
            't_admin.send_fax_flg'
        ];
        $result = $this->select($col)
                       ->join('mst_supplier', 'mst_product.price_supplier_id', '=', 'mst_supplier.supplier_cd')
                       ->join('edi.mst_product as emp', function ($join) {
                            $join->on('mst_product.product_code', '=', 'emp.product_code')
                                 ->where('emp.is_live', '=', 1);
                       })
                       // ->join('dt_order_to_supplier', 'dt_order_to_supplier.product_code', '=', 'mst_product.product_code')
                       ->leftjoin('edi.t_admin', 't_admin.suppliercd', '=', 'mst_product.price_supplier_id')
                       ->where("mst_product.{$colWhere}", $value)
                       ->first();
        return $result;
    }
    /**
     * Get data product set to return supplier
     *
     * @param  array $productCode
     * @return object
     */
    public function getDataSetReturnSuppier($productCode)
    {
        $col = [
            'mst_product_set.child_product_code',
            'mst_product.price_supplier_id',
            'ra.return_id',
        ];
        $result = $this->select($col)
                       ->leftJoin('mst_return_address AS ra', function ($join) {
                            $join->on('mst_product.price_supplier_id', '=', 'ra.supplier_cd')
                            ->where('ra.is_selected', 1);
                       })
                       ->leftJoin('mst_product_set', 'mst_product_set.child_product_code', '=', 'mst_product.product_code')
                       ->where('product_code', $productCode)
                       ->get();
        return $result;
    }

    /**
     * Get fax order
     * @param string $type
     * @param string $productCode
     * @return collection
     */
    public function getFaxOrder($type, $productCode)
    {
        $result = $this->select(
            'mst_product.product_name_long'
        )
        ->where('mst_product.product_code', $productCode);
        if ($type === 'maker') {
            $result->addSelect('mst_product.maker_full_nm', 'mst_maker.maker_fax', 'mst_maker.maker_cd')
                ->join('master_plus.mst_maker', 'mst_product.maker_cd', '=', 'mst_maker.maker_cd');
        } elseif ($type === 'supplier') {
            $result->addSelect('mst_supplier.supplier_nm', 'mst_supplier.supplier_fax', 'mst_supplier.supplier_cd')
                ->join('master_plus.mst_supplier', 'mst_product.price_supplier_id', '=', 'mst_supplier.supplier_cd');
        }
        return $result->first();
    }

    /**
     * Get product price
     *
     * @param  string  $productCode
     * @return object
     */
    public function getProduct($productCode)
    {
        if (is_array($productCode)) {
            return $this->whereIn('product_code', $productCode)->get();
        } else {
            return $this->where('product_code', $productCode)->first();
        }
    }

    /**
     * Get product add new smaregi
     *
     * @param  array  $condition
     * @return object
     */
    public function getDataProductAddSmaregi($condition)
    {
        $col = [
            'product_code',
            'product_name',
            'product_name_long',
            'product_jan',
            'product_maker_code',
        ];
        $result = $this->select($col);
        $check  = false;
        if (!empty($condition['product_code'])) {
            $result = $result->where('product_code', $condition['product_code']);
            $check  = true;
        }
        // if (!empty($condition['product_name'])) {
        //     $result = $result->where('product_name', 'LIKE', "%{$condition['product_name']}%");
        //     $check  = true;
        // }
        if (!empty($condition['product_jan'])) {
            $result = $result->where('product_jan', $condition['product_jan']);
            $check  = true;
        }
        // if (!empty($condition['product_maker_code'])) {
        //     $result = $result->where('product_maker_code', 'LIKE', "%{$condition['product_maker_code']}%");
        //     $check  = true;
        // }
        if ($check) {
            return $result->first();
        } else {
            return [];
        }
    }

    /**
     * Get item by product_codes
     *
     * @param  array $productCodes
     * @return object
     */
    public function getDataParseContent14($productCodes)
    {
        $data = $this->select(
            'product_code',
            'product_name_long',
            'product_jan',
            'price_invoice'
        )
        ->where(function ($query) use ($productCodes) {
            $query->whereIn('product_code', $productCodes)
                  ->orWhereIn('product_jan', $productCodes);
        })
        ->get();
        return $data;
    }
}
