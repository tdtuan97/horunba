<?php
/**
 * Exe sql for hrnb.mst_customer
 *
 * @package    App\Models
 * @subpackage MstCustomer
 * @copyright  Copyright (c) 2018 RiverCrane! Corporation. All Rights Reserved.
 * @author     van.qui<van.qui.rcvn2012@gmail.com>
 */

namespace App\Models\Backend;

use Illuminate\Database\Eloquent\Model;

class MstCustomer extends Model
{
    public $timestamps = false;

    /**
    * The database table used by the model.
    * @var string
    */
    protected $table = 'mst_customer';

    /**
    * The database is used by the model.
    * @var string
    */
    protected $connection = 'horunba';

    /**
    * The primary key.
    * @var string
    */
    protected $primaryKey = 'customer_id';
    
    /**
    * Check exixts customer.
    * @param array $arrDatas Data for condition where
    * @return object $result
    */
    public function checkCustomerExist($arrDatas)
    {
        $result = $this->whereRaw("last_name = BINARY ifnull(?,'')", [$arrDatas['last_name']])
                       ->whereRaw("tel_num = BINARY ifnull(?,'')", [$arrDatas['tel_num']])
                       ->whereRaw("zip_code = BINARY ifnull(?,'')", [$arrDatas['zip_code']])
                       ->whereRaw("prefecture = BINARY ifnull(?,'')", [$arrDatas['prefecture']])
                       ->whereRaw("city = BINARY ifnull(?,'')", [$arrDatas['city']])
                       ->whereRaw("sub_address = BINARY ifnull(?,'')", [$arrDatas['sub_address']])
                       ->first();
        return $result;
    }
    /**
    * Get info customer
    * @param array $arrData Data for condition where
    * @return object $result
    */
    public function getInfoCustomer($arrData)
    {
        if (isset($arrData['receive_id'])) {
            $cols = [
                'mst_order.company_name',
                'mst_customer.customer_id',
                'mst_customer.last_name',
                'mst_customer.email',
                'mst_customer.tel_num',
                'mst_customer.fax_num',
                'mst_customer.zip_code',
                'mst_customer.prefecture',
                'mst_customer.city',
                'mst_customer.sub_address',
                'receiver.customer_id as reciever_id',
                'receiver.last_name as reciever_name',
                'receiver.tel_num as re_tel',
                'receiver.zip_code as re_postal',
                'receiver.prefecture as re_province',
                'receiver.city as re_address_1',
                'receiver.sub_address as re_address_2'
            ];

            $result = $this->select($cols)
                        ->join('mst_order', function ($join) {
                            $join->on('mst_order.customer_id', '=', 'mst_customer.customer_id');
                        })
                        ->join('mst_order_detail', function ($join) {
                            $join->on('mst_order_detail.receive_id', '=', 'mst_order.receive_id');
                        })
                        ->join('mst_customer AS receiver', function ($join) {
                            $join->on('mst_order_detail.receiver_id', '=', 'receiver.customer_id');
                        })
                        ->where('mst_order.receive_id', (int)$arrData['receive_id'])
                        ->first();
            return $result;
        }
        return [];
    }
    
    /**
     * Update data by key
     *
     * @param  array   $keys
     * @param  array   $data
     * @return boolean
     */
    public function updateData($keys, $data)
    {
        return $this->where($keys)->update($data);
    }

    public function getDataCustomer($arrSearch = null, $arrSort = null)
    {
        $query = $this->select('*');
        if (count($arrSearch) > 0) {
            $query->where(function ($query) use ($arrSearch) {
                if (isset($arrSearch['customer_id'])) {
                    $query->where('customer_id', $arrSearch['customer_id']);
                }
                if (isset($arrSearch['last_name_kana'])) {
                    $query->where('last_name_kana', $arrSearch['last_name_kana']);
                }
                if (isset($arrSearch['email'])) {
                    $query->where('email', $arrSearch['email']);
                }
                if (isset($arrSearch['tel_num'])) {
                    $query->where('tel_num', $arrSearch['tel_num']);
                }
                if (isset($arrSearch['prefecture'])) {
                    $query->where('prefecture', $arrSearch['prefecture']);
                }
                if (isset($arrSearch['city'])) {
                    $query->where('city', $arrSearch['city']);
                }
                if (isset($arrSearch['sub_address'])) {
                    $query->where('sub_address', $arrSearch['sub_address']);
                }
            });
        }
        $check = false;
        if ($arrSort !== null && count($arrSort) > 0) {
            foreach ($arrSort as $column => $sort) {
                if (in_array($sort, ['asc', 'desc'])) {
                    $query->orderBy($column, $sort);
                    $check = true;
                }
            }
        }
        if (!$check) {
            $query->orderBy('customer_id', 'asc');
        }
        $perPage = ($arrSearch['per_page']) ? $arrSearch['per_page'] : 20;
        $data = $query->paginate($perPage);
        return $data;
    }

    public function getCustomerById($customerId)
    {
        $data = $this->where('customer_id', '=', $customerId)->first();
        return $data;
    }
}
