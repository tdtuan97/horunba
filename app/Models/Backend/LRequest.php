<?php
/**
 * Exe sql for edi.l_request
 *
 * @package App\Models\Backend
 * @subpackage EdiModel
 * @copyright Copyright (c) 2018 CriverCrane! Corporation. All Rights Reserved.
 * @author Le Hung<le.hung.rcvn2012@crivercrane.vn>
 */
namespace App\Models\Backend;

use Illuminate\Database\Eloquent\Model;

class LRequest extends Model
{

    public $timestamps = false;
    /**
     * The database table used by the model.
     * @var string
     */
    protected $table = 'l_request';
    /**
     * The database is used by the model.
     * @var string
     */
    protected $connection = 'edi';

    /**
     * Insert data into l_request
     * @param array $data use for insert new data
     * @return array
     */
    public function insertLRequest($data)
    {
        $id = $this->insertGetId($data);
        return $id;
    }
}
