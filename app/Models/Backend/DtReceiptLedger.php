<?php
/**
 * Model for dt_receipt_ledger table.
 *
 * @package    App\Models\Backend
 * @subpackage DtReceiptLedger
 * @copyright  Copyright (c) 2018 RiverCrane! Corporation. All Rights Reserved.
 * @author     Truong Nghia<truong.van.nghia@crivercrane.vn>
 */

namespace App\Models\Backend;

use Illuminate\Database\Eloquent\Model;

class DtReceiptLedger extends Model
{
    public $timestamps = false;

    /**
    * The database table used by the model.
    * @var string
    */
    protected $table = 'dt_receipt_ledger';

    /**
    * The database is used by the model.
    * @var string
    */
    protected $connection = 'horunba';

    /**
     * Get data list
     *
     * @return object
     */
    public function getData($arraySearch = null, $arraySort = null, $isExport = false, $limitEx = 5000, $offsetEx = 0)
    {
        $col = [
            'dt_receipt_ledger.index',
            'dt_receipt_ledger.stock_type',
            'dt_receipt_ledger.stock_detail_type',
            'dt_receipt_ledger.recognite_key',
            'dt_receipt_ledger.product_code',
            // 'mst_product_base.product_name',
            'dt_receipt_ledger.stock_num',
            'dt_receipt_ledger.price_invoice',
            'dt_receipt_ledger.stock_detail_num',
            'dt_receipt_ledger.change_num',
            'dt_receipt_ledger.up_date',
        ];
        $data = $this->select($col)
            ->selectRaw('1 AS count');
            // ->join('mst_product_base', 'mst_product_base.product_code', '=', 'dt_receipt_ledger.product_code');
        if (count($arraySearch) > 0) {
            $data->where(function ($query) use ($arraySearch) {
                if (isset($arraySearch['stock_type'])) {
                    if (is_array($arraySearch['stock_type'])) {
                        $query->whereIn('dt_receipt_ledger.stock_type', $arraySearch['stock_type']);
                    } else {
                        $query->where('dt_receipt_ledger.stock_type', $arraySearch['stock_type']);
                    }
                }
                if (isset($arraySearch['stock_detail_type'])) {
                    if (is_array($arraySearch['stock_detail_type'])) {
                        $query->whereIn('dt_receipt_ledger.stock_detail_type', $arraySearch['stock_detail_type']);
                    } else {
                        $query->where('dt_receipt_ledger.stock_detail_type', $arraySearch['stock_detail_type']);
                    }
                }
                if (isset($arraySearch['recognite_key'])) {
                    $query->where('dt_receipt_ledger.recognite_key', 'LIKE', "%{$arraySearch['recognite_key']}%");
                }
                if (isset($arraySearch['product_code'])) {
                    $query->where('dt_receipt_ledger.product_code', 'LIKE', "%{$arraySearch['product_code']}%");
                }
                if (isset($arraySearch['up_date_from'])) {
                    $dateFrom = date("Y-m-d 00:00:00", strtotime($arraySearch['up_date_from']));
                    $query->where(
                        'dt_receipt_ledger.up_date',
                        '>=',
                        $dateFrom
                    );
                }
                if (isset($arraySearch['up_date_to'])) {
                    $dateTo = date("Y-m-d 23:59:59", strtotime($arraySearch['up_date_to']));
                    $query->where(
                        'dt_receipt_ledger.up_date',
                        '<=',
                        $dateTo
                    );
                }
            });
        }
        if ($arraySort !== null && count($arraySort) > 0) {
            foreach ($arraySort as $column => $sort) {
                if ($sort !== null && in_array($sort, ['asc', 'desc'])) {
                    $data->orderBy($column, $sort);
                }
            }
        } else {
            $data->orderBy('dt_receipt_ledger.product_code', 'ASC');
            $data->orderBy('dt_receipt_ledger.index', 'ASC');
            $data->orderBy('dt_receipt_ledger.line_index', 'ASC');
        }
        if ($isExport) {
            $data->limit($limitEx)
                ->offset($offsetEx);
            return $data->get();
        }
        $perPage = ($arraySearch['per_page']) ? $arraySearch['per_page'] : 20;
        $data = $data->paginate($perPage);
        return $data;
    }
    /**
     * Get data list
     *
     * @return object
     */
    public function getDataDetail($index = null, $productCode = null)
    {
        $col = [
            'dt_receipt_ledger.price_invoice',
            'dt_receipt_ledger.stock_detail_num',
            'dt_receipt_ledger.change_num',
            'dt_receipt_ledger.up_date',
        ];
        $data = $this->select($col)
                     ->where('index', $index)
                     ->where('product_code', $productCode)
                     ->orderBy('dt_receipt_ledger.line_index')
                     ->get();
        return $data;
    }
    /**
     * Get price_invoice
     *
     * @return object
     */
    public function getPriceInvoice($recogniteKey, $productCode)
    {
        $col = [
            'dt_receipt_ledger.price_invoice',
        ];
        $data = $this->select($col)
                     ->where('recognite_key', $recogniteKey)
                     ->where('product_code', $productCode)
                     ->orderBy('dt_receipt_ledger.index', 'DESC')
                     ->orderBy('dt_receipt_ledger.line_index', 'DESC')
                     ->first();
        return $data;
    }

    /**
     * Get data price_invoice, edi_order_code
     * @param  string $orderCode
     * @return object
     */
    public function getDataReturnProuct($receivedOrderId, $productCode)
    {
        return $this->select(['dt_receipt_ledger.price_invoice', 'dt_receipt_ledger.edi_order_code'])
                    ->where('dt_receipt_ledger.recognite_key', $receivedOrderId)
                    ->where('dt_receipt_ledger.product_code', $productCode)
                    ->where('dt_receipt_ledger.line_index', 1)
                    ->orderBy('dt_receipt_ledger.index', 'DESC')
                    ->first();
    }
}
