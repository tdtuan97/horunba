<?php
/**
 * Exe sql for edi.t_nouhin
 *
 * @package App\Models\Edi
 * @subpackage EdiModel
 * @copyright Copyright (c) 2017 CriverCrane! Corporation. All Rights Reserved.
 * @author lam.vinh<lam.vinh@crivercrane.vn>
 * @clone  le.hung<le.hung.rcvn@gmail.com
 */
namespace App\Models\Backend;

use Illuminate\Database\Eloquent\Model;

class TNouHin extends Model
{

    public $timestamps = false;
    /**
     * The database table used by the model.
     * @var string
     */
    protected $table = 't_nouhin';
    /**
     * The database is used by the model.
     * @var string
     */
    protected $connection = 'edi';

    /**
     * Update order from wait to finish
     * @param array $data array data to update
     * @return int
     */
    public function updateShipmentPrice($data)
    {
        $check = $this->where("suppliercd", "=", $data['suppliercd'])
            ->where("t_nouhin_id", "=", $data['t_nouhin_id'])
            ->where("del_flg", "=", 0)
            ->where("valid_flg", "=", 1)
            ->update(array(
                'shipment_price' => $data['shipment_price'],
            ));
        return $check;
    }

    /**
     * Get data nouhin id by no
     * @param int $no number no
     * @param int $suppliercd number supplier code
     * @return obj
     */
    public function getNouhin($no, $suppliercd)
    {
        $result = $this->where("suppliercd", "=", $suppliercd)
            ->where("no", "=", $no)
            ->first();
        return $result;
    }

    /**
     * Insert new data to table t_nohin
     * @return int
     */
    public function insertDataGetId($data)
    {
        if (!empty($data)) {
            $id = $this->insertGetId($data);
            return $id;
        }
        return null;
    }
}
