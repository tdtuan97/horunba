<?php

/**
 * Exe sql for hrnb.dt_order_to_supplier
 *
 * @package    App\Models
 * @subpackage DtOrderToSupplier
 * @copyright  Copyright (c) 2018 RiverCrane! Corporation. All Rights Reserved.
 * @author     van.qui<van.qui.rcvn2012@gmail.com>
 * @author     le.hung<le.hung.rcvn2012@gmail.com>
 */

namespace App\Models\Backend;

use Illuminate\Database\Eloquent\Model;
use App\Models\Backend\MstStockStatus;
use Illuminate\Pagination\Paginator;
use DB;

class DtOrderToSupplier extends Model
{
    public $timestamps = false;
    /**
    * The database table used by the model.
    * @var string
    */
    protected $table = 'dt_order_to_supplier';
    /**
    * The database is used by the model.
    * @var string
    */
    protected $connection = 'horunba';

    /**
    * Set default key type.
    * @var string
    */
    protected $keyType = 'string';

    /**
    * Set default primary key.
    * @var string
    */
    protected $primaryKey = 'order_code';
    /**
    * Set none auto incrementing.
    * @var string
    */
    public $incrementing = false;
    /**
     * Get max item by order code
     *
     * @param  string    $prefix
     * @return object
     */

    public function getMaxItemByOrderCode($prefix = "HO")
    {
        $currentMonth = date('Ym');
        $data = $this->where(
            'order_code',
            'like',
            $prefix . $currentMonth . "%"
        )
        ->orderBy('order_code', 'desc')->first();
        return $data;
    }

    /**
     * Get last order code tabe dt_order_to_supplier
     * @return object $result
     */
    public function getMaxOrderCodeByDate($date)
    {
        $result = $this->select('order_code')
                       ->where('order_code', 'like', $date . '%')
                       ->orderBy('order_code', 'DESC')
                       ->first();
        return $result;
    }

    /**
     * Get max item by order code
     *
     * @param  string  $prefix
     * @param  string  $orderCode
     * @return object
     */
    public function getMaxItemByOrderCode2($prefix = "HO", $orderCode = null)
    {
        $code = str_replace($prefix, '', $orderCode);
        $currentMonth = substr($code, 0, 6);
        $data = $this->where(
            'order_code',
            'like',
            $prefix . $currentMonth . "%"
        )
        ->orderBy('order_code', 'desc')->first();
        return $data;
    }
    /**
     * Get data of table
     *
     * @param  array  $arraySearch
     * @param  array  $arraySort
     * @return object
     */
    public function getData($arraySearch = null, $numpage = 1, $arraySort = null, $type = null)
    {
        $cols = [
            'dt_order_to_supplier.order_date',
            'dt_order_to_supplier.order_code',
            'dt_order_to_supplier.order_type',
            'dt_order_product_detail.delivery_type',
            'dt_order_product_detail.receive_id',
            'dt_order_product_detail.product_status',
            'dt_order_to_supplier.product_code',
            'dt_order_to_supplier.product_jan',
            'mst_product_base.product_name',
            'dt_order_to_supplier.price_invoice',
            'dt_order_to_supplier.order_num',
            'dt_order_to_supplier.arrival_date_plan',
            'dt_order_to_supplier.arrival_date',
            'dt_order_to_supplier.supplier_id',
            'mst_product_base.maker_cd',
            'mst_product_base.maker_full_nm',
            'dt_order_to_supplier.process_status',
            'dt_order_to_supplier.delay_note',
            'dt_order_to_supplier.order_note',
            'dt_order_to_supplier.is_confirmed',
            'mst_product_base.product_maker_code',
            'mst_supplier.supplier_nm',
            'dt_order_to_supplier.remarks',
            'dt_order_to_supplier.ope_tantou',
            'dt_order_to_supplier.ope_status',
            'dt_order_to_supplier.reason_note',
            'dt_order_to_supplier.is_cancel',
            'dt_order_to_supplier.incomplete_quantity',
            'dt_order_to_supplier.edi_order_code',
            'dt_order_to_supplier.edi_delivery_date',
            't_order_detail.m_order_type_id AS type_detail',
            't_order_direct.m_order_type_id AS type_direct',
            'dt_order_to_supplier.is_lost'
        ];
        $data = $this->select($cols);
        $data = $this->conditionQuery($data, $arraySearch, $arraySort);

        if (!is_null($type)) {
            return $data->get();
        }
        $data = $data->paginate($numpage);
        return $data;
    }

    public function getDataAll($arrayOrder)
    {
        $cols = [
            'dt_order_to_supplier.order_date',
            'dt_order_to_supplier.order_code',
            'dt_order_to_supplier.order_type',
            'dt_order_product_detail.delivery_type',
            'dt_order_product_detail.receive_id',
            'dt_order_product_detail.product_status',
            'dt_order_to_supplier.product_code',
            'dt_order_to_supplier.product_jan',
            'mst_product_base.product_name',
            'dt_order_to_supplier.price_invoice',
            'dt_order_to_supplier.order_num',
            'dt_order_to_supplier.arrival_date_plan',
            'dt_order_to_supplier.arrival_date',
            'dt_order_to_supplier.supplier_id',
            'mst_product_base.maker_cd',
            'mst_product_base.maker_full_nm',
            'dt_order_to_supplier.process_status',
            'dt_order_to_supplier.delay_note',
            'dt_order_to_supplier.order_note',
            'dt_order_to_supplier.is_confirmed',
            'mst_product_base.product_maker_code',
            'mst_supplier.supplier_nm',
            'dt_order_to_supplier.remarks',
            'dt_order_to_supplier.ope_tantou',
            'dt_order_to_supplier.ope_status',
            'dt_order_to_supplier.reason_note',
            'dt_order_to_supplier.is_cancel',
            'dt_order_to_supplier.incomplete_quantity',
            'dt_order_to_supplier.edi_order_code',
            'dt_order_to_supplier.edi_delivery_date',
            't_order_detail.m_order_type_id AS type_detail',
            't_order_direct.m_order_type_id AS type_direct',
            'dt_order_to_supplier.is_lost'
        ];
        $data = $this->select($cols)
                    ->leftJoin('mst_product_base', 'mst_product_base.product_code', '=', 'dt_order_to_supplier.product_code')
                    ->leftJoin(
                        'dt_order_product_detail',
                        'dt_order_product_detail.order_code',
                        '=',
                        'dt_order_to_supplier.order_code'
                    )
                    ->leftJoin('mst_supplier', 'mst_supplier.supplier_cd', '=', 'dt_order_to_supplier.supplier_id')
                    ->leftJoin('edi.t_order_detail', 't_order_detail.order_code', '=', 'dt_order_to_supplier.edi_order_code')
                    ->leftJoin('edi.t_order_direct', 't_order_direct.order_code', '=', 'dt_order_to_supplier.edi_order_code')
                    ->whereIn('dt_order_to_supplier.order_code', $arrayOrder)
                    ->orderBy('dt_order_to_supplier.order_date', 'desc');
        $currentPage = 1;
        Paginator::currentPageResolver(function () use ($currentPage) {
            return $currentPage;
        });
        return $data->paginate(count($arrayOrder));
    }

    public function getDataPage($page)
    {
        $data = $this->select(['dt_order_to_supplier.order_code'])
                     ->orderBy('dt_order_to_supplier.order_date', 'desc');
        $data = $data->paginate($page);
        return $data;
    }
    /**
     * Condition query
     *
     * @param  object  $data
     * @param  array   $arraySearch
     * @param  array   $arraySort
     * @return object
     */
    public function conditionQuery($data, $arraySearch = null, $arraySort = null)
    {
        $data->leftJoin('mst_product_base', 'mst_product_base.product_code', '=', 'dt_order_to_supplier.product_code')
            ->leftJoin(
                'dt_order_product_detail',
                'dt_order_product_detail.order_code',
                '=',
                'dt_order_to_supplier.order_code'
            )
            ->leftJoin('mst_supplier', 'mst_supplier.supplier_cd', '=', 'dt_order_to_supplier.supplier_id')
            ->leftJoin('edi.t_order_detail', 't_order_detail.order_code', '=', 'dt_order_to_supplier.edi_order_code')
            ->leftJoin('edi.t_order_direct', 't_order_direct.order_code', '=', 'dt_order_to_supplier.edi_order_code');
        if (count($arraySearch) > 0) {
            $data->where(function ($query) use ($arraySearch) {
                if (isset($arraySearch['order_date'])) {
                    $query->whereDate('dt_order_to_supplier.order_date', "{$arraySearch['order_date']}");
                }
                if (isset($arraySearch['order_type'])) {
                    if (is_array($arraySearch['order_type'])) {
                        if (count($arraySearch['order_type']) > 0) {
                            $query->whereIn('order_type', $arraySearch['order_type']);
                        }
                    } else {
                        $query->where('order_type', $arraySearch['order_type']);
                    }
                }
                if (isset($arraySearch['delivery_type'])) {
                    if (is_array($arraySearch['delivery_type'])) {
                        if (count($arraySearch['delivery_type']) > 0) {
                            $query->whereIn('delivery_type', $arraySearch['delivery_type']);
                        }
                    } else {
                        $query->where('delivery_type', $arraySearch['delivery_type']);
                    }
                }
                if (isset($arraySearch['product_jan'])) {
                    $query->where(
                        'dt_order_to_supplier.product_jan',
                        'LIKE',
                        "%{$arraySearch['product_jan']}%"
                    );
                }
                if (isset($arraySearch['product_name'])) {
                    $query->where(
                        'mst_product_base.product_name',
                        'LIKE',
                        "%{$arraySearch['product_name']}%"
                    );
                }
                if (isset($arraySearch['edi_delivery_date'])) {
                    $query->whereDate('edi_delivery_date', "{$arraySearch['edi_delivery_date']}");
                }
                if (isset($arraySearch['arrival_date_plan'])) {
                    $query->whereDate('dt_order_to_supplier.arrival_date_plan', "{$arraySearch['arrival_date_plan']}");
                }
                if (isset($arraySearch['arrival_date'])) {
                    $query->whereDate('dt_order_to_supplier.arrival_date', "{$arraySearch['arrival_date']}");
                }
                if (isset($arraySearch['supplier_name'])) {
                    $query->where('mst_supplier.supplier_nm', 'LIKE', "%{$arraySearch['supplier_name']}%");
                }
                if (isset($arraySearch['maker_full_nm'])) {
                    $query->where('mst_product_base.maker_full_nm', 'LIKE', "%{$arraySearch['maker_full_nm']}%");
                }
                if (isset($arraySearch['ope_status'])) {
                    if (is_array($arraySearch['ope_status'])) {
                        if (count($arraySearch['ope_status']) > 0) {
                            $query->whereIn('dt_order_to_supplier.ope_status', $arraySearch['ope_status']);
                        }
                    } else {
                        $query->where('dt_order_to_supplier.ope_status', $arraySearch['ope_status']);
                    }
                }
                if (isset($arraySearch['order_code'])) {
                    $query->where('dt_order_to_supplier.order_code', $arraySearch['order_code']);
                }
                if (isset($arraySearch['product_code'])) {
                    $query->where('dt_order_to_supplier.product_code', $arraySearch['product_code']);
                }
                if (isset($arraySearch['delivery_delay'])) {
                    $query->whereRaw('dt_order_to_supplier.edi_delivery_date <= NOW() - INTERVAL 1 DAY');
                    $query->where('dt_order_to_supplier.process_status', 2);
                    $query->where('dt_order_to_supplier.is_cancel', 0);
                }
                if (isset($arraySearch['receive_delay'])) {
                    $query->whereRaw('dt_order_to_supplier.arrival_date <= NOW() - INTERVAL 1 DAY');
                    $query->where('dt_order_to_supplier.process_status', 5);
                    $query->where('dt_order_to_supplier.incomplete_quantity', 0);
                    $query->where('dt_order_to_supplier.is_cancel', 0);
                    $query->whereRaw('dt_order_to_supplier.order_type != 2');
                }
                if (isset($arraySearch['receive_incomplete'])) {
                    $query->where('dt_order_to_supplier.incomplete_quantity', '>', 0);
                    $query->where('dt_order_to_supplier.process_status', 5);
                    $query->where('dt_order_to_supplier.is_cancel', 0);
                }
                if (isset($arraySearch['note_check'])) {
                    $query->whereNotNull('dt_order_to_supplier.order_note');
                    $query->where('dt_order_to_supplier.order_note', '<>', '');
                    $query->where('dt_order_to_supplier.process_status', '<', 3);
                    $query->where('dt_order_to_supplier.is_cancel', 0);
                    $query->whereRaw('(LENGTH(dt_order_to_supplier.order_note) <> 75) OR (dt_order_to_supplier.order_note NOT LIKE "%トラスコメーカー手配中%" and LENGTH(dt_order_to_supplier.order_note) = 75)');
                }
                if (isset($arraySearch['not_answer'])) {
                    $query->where('dt_order_to_supplier.process_status', 1);
                    $query->whereRaw('dt_order_to_supplier.order_date <= NOW() - INTERVAL 1 DAY');
                    $query->where('dt_order_to_supplier.is_cancel', 0);
                }
            });
        }
        $check = false;
        if ($arraySort !== null && count($arraySort) > 0) {
            foreach ($arraySort as $column => $sort) {
                if (in_array($sort, ['asc', 'desc'])) {
                    $data->orderBy($column, $sort);
                    $check = true;
                }
            }
        }
        if (!$check) {
            $data->orderBy('dt_order_to_supplier.order_date', 'desc');
        }
        return $data;
    }
    /**
     * Update data
     *
     * @param  array condition $where
     * @param  array update $data
     * @return boolean
     */
    public function updateData($where, $data)
    {
        $result = $this->where($where)
                    ->update($data);
        return $result;
    }
    /**
     * Update data different order_type
     *
     * @param  array condition $where
     * @param  array update $data
     * @return boolean
     */
    public function updateDataDiffOT($where, $data)
    {
        $result = $this->where($where)
                    ->where('order_type', '!=', 5)
                    ->update($data);
        return $result;
    }

    /**
     * Get order detail from edi
     *
     * @param  array   $conds
     * @return object
     */
    public function getOrderDetailEDI($conds)
    {
        $data = $this->select(
            "dt_order_to_supplier.receive_id",
            "dt_order_to_supplier.detail_line_num",
            "dt_order_to_supplier.sub_line_num",
            "dt_order_to_supplier.order_date",
            "dt_order_to_supplier.order_type",
            "dt_order_to_supplier.order_code AS ots_order_code",
            "dt_order_to_supplier.edi_order_code",
            "t_order_detail.order_code",
            "t_order_detail.m_order_type_id",
            'pb.product_code',
            'pb.product_name'
        )
        ->join("edi.t_order_detail", "t_order_detail.order_code", "=", "dt_order_to_supplier.edi_order_code")
        ->leftjoin('mst_product_base AS pb', 'pb.product_code', '=', 'dt_order_to_supplier.product_code');
        $case = '';
        foreach ($conds as $item) {
            if (!empty($item['order_code'])) {
                $case .= "WHEN dt_order_to_supplier.order_code = '" . $item['order_code'] . "' THEN " . $item['product_status'] . " ";
                $data->orWhere(function ($subWhere) use ($item) {
                    $subWhere->where("dt_order_to_supplier.order_code", $item['order_code']);
                });
            }
        }
        if (empty($case)) {
            return [];
        }
        return $data->selectRaw('(CASE ' . $case . "END) AS product_status")
                    ->get();
    }

    /**
     * Get order direct from edi
     *
     * @param  array   $conds
     * @return object
     */
    public function getOrderDirectEDI($conds)
    {
        $data = $this->select(
            "dt_order_to_supplier.receive_id",
            "dt_order_to_supplier.detail_line_num",
            "dt_order_to_supplier.sub_line_num",
            "dt_order_to_supplier.order_code AS ots_order_code",
            "dt_order_to_supplier.edi_order_code",
            "dt_order_to_supplier.order_type",
            "t_order_direct.order_code",
            "t_order_direct.m_order_type_id",
            'pb.product_code',
            'pb.product_name'
        )
        ->join("edi.t_order_direct", "t_order_direct.order_code", "=", "dt_order_to_supplier.edi_order_code")
        ->leftjoin('mst_product_base AS pb', 'pb.product_code', '=', 'dt_order_to_supplier.product_code');
        $case = '';
        foreach ($conds as $item) {
            if (!empty($item['order_code'])) {
                $case .= "WHEN dt_order_to_supplier.order_code = '" . $item['order_code'] . "' THEN " . $item['product_status'] . " ";
                $data->orWhere(function ($subWhere) use ($item) {
                    $subWhere->where("dt_order_to_supplier.order_code", $item['order_code']);
                });
            }
        }
        if (empty($case)) {
            return [];
        }
        return $data->selectRaw('(CASE ' . $case . "END) AS product_status")
                    ->get();
    }
    /**
     * Get data by condition
     *
     * @param  string   $orderCode
     * @param  string   $ediOrderCode
     * @return object
     */
    public function getDataByKey($orderCode, $ediOrderCode)
    {
        $result = $this->where('order_code', $orderCode)
                       ->where('edi_order_code', $ediOrderCode)
                       ->first();
        return $result;
    }

    /**
     * Get order detail from edi by order code
     *
     * @param  array   $conds
     * @return object
     */
    public function getDataCheckChangeQuantity($conds)
    {
        $data = $this->select(
            "t_order_detail.m_order_type_id AS m_order_type_id_detail",
            "t_order_direct.m_order_type_id AS m_order_type_id_direct",
            'dt_order_product_detail.delivery_type',
            "dt_order_to_supplier.order_date"
        )
        ->leftjoin(
            "edi.t_order_detail",
            "t_order_detail.order_code",
            "=",
            "dt_order_to_supplier.edi_order_code"
        )
        ->leftjoin(
            "edi.t_order_direct",
            "t_order_direct.order_code",
            "=",
            "dt_order_to_supplier.edi_order_code"
        )
        ->leftJoin(
            'dt_order_product_detail',
            'dt_order_product_detail.order_code',
            '=',
            'dt_order_to_supplier.order_code'
        );
        $flg = false;
        foreach ($conds as $item) {
            if (!empty($item['order_code'])) {
                $flg = true;
                $data->orWhere(function ($subWhere) use ($item) {
                    $subWhere->where("dt_order_to_supplier.order_code", $item['order_code']);
                });
            }
        }
        if (!$flg) {
            return [];
        }
        return $data->get();
    }
    /**
     * select all record by condition
     *
     * @return object
     */
    public function selectColumns($arraySearch, $arrayCols = null)
    {
        if (count($arrayCols) > 0) {
            $sql = $this->conditionQuery($this->select($arrayCols), $arraySearch, null)->get()->toArray();
        }
        return $sql;
    }

    /**
     * Get fax order
     * @param string $type
     * @param string $ediOrderCode
     * @param string $productCode
     * @return collection
     */
    public function getFaxOrder($type, $ediOrderCode, $productCode)
    {
        $result = $this->select(
            'dt_order_to_supplier.edi_order_code',
            'mst_product.product_name_long'
        )
            ->join('hrnb.mst_product', 'dt_order_to_supplier.product_code', '=', 'mst_product.product_code')
            ->where('dt_order_to_supplier.edi_order_code', $ediOrderCode)
            ->where('dt_order_to_supplier.product_code', $productCode);
        if ($type === 'maker') {
            $result->addSelect('mst_product.maker_full_nm', 'mst_maker.maker_fax', 'mst_maker.maker_cd')
                ->join('master_plus.mst_maker', 'mst_product.maker_cd', '=', 'mst_maker.maker_cd');
        } elseif ($type === 'supplier') {
            $result->addSelect('mst_supplier.supplier_nm', 'mst_supplier.supplier_fax', 'mst_supplier.supplier_cd')
                ->join('master_plus.mst_supplier', 'dt_order_to_supplier.supplier_id', '=', 'mst_supplier.supplier_cd');
        }
        return $result->first();
    }

    /**
     * Get data parse deje content
     * @param  string $orderCode
     * @param  string $productCode
     * @return object
     */
    public function getDataParseContent1($orderCode, $productCode, $businessStype)
    {
        $col = [
            'dt_order_to_supplier.order_code',
            'dt_order_to_supplier.order_num',
            'dt_order_to_supplier.product_code',
            'dt_order_to_supplier.product_jan',
            't_insertRequestList.RequestNo',
            't_insertRequestList.ItemNm',
        ];
        $result = $this->select($col)
                       ->leftJoin('edi.t_insertRequestList', 't_insertRequestList.order_code', '=', 'dt_order_to_supplier.edi_order_code');
        if (!empty($orderCode)) {
            $result->whereIn('dt_order_to_supplier.order_code', $orderCode);
        }
        if (!empty($productCode) && in_array($businessStype, ['1', '2'])) {
            $result->where(function ($query) use ($productCode) {
                $query->whereIn('dt_order_to_supplier.product_code', $productCode)
                      ->orWhereIn('dt_order_to_supplier.product_jan', $productCode);
            });
        }

        return $result->get();
    }

    /**
     * Get data parse deje content
     * @param  string $orderCode
     * @param  string $productCode
     * @return object
     */
    public function getDataParseContent4($orderCode, $productCode)
    {
        $col = [
            'dt_order_to_supplier.order_code',
            'dt_order_to_supplier.order_num',
            'dt_order_to_supplier.product_code',
            'dt_order_to_supplier.product_jan',
            't_insertRequestList.RequestNo',
            't_insertRequestList.ItemNm',
        ];
        $result = $this->select($col)
                       ->join('edi.t_insertRequestList', 't_insertRequestList.order_code', '=', 'dt_order_to_supplier.edi_order_code');
        if (!empty($orderCode)) {
            $result->whereIn('dt_order_to_supplier.order_code', $orderCode);
        }
        if (!empty($productCode)) {
            $result->where(function ($query) use ($productCode) {
                $query->whereIn('dt_order_to_supplier.product_code', $productCode)
                      ->orWhereIn('dt_order_to_supplier.product_jan', $productCode);
            });
        }

        return $result->get();
    }

    /**
     * Get data parse deje content
     * @param  string $orderCode
     * @param  string $productCode
     * @return object
     */
    public function getDataParseContent7($orderCode, $productCode)
    {
        $col = [
            'dt_order_to_supplier.order_code',
            'dt_order_to_supplier.order_num',
            'dt_order_to_supplier.product_code',
            'dt_order_to_supplier.product_jan',
            't_insertRequestList.RequestNo',
            't_insertRequestList.ItemNm',
            't_insertRequestList.SupplierNm',
            'mst_order.received_order_id',
        ];
        $result = $this->select($col)
                       ->leftJoin('edi.t_insertRequestList', 't_insertRequestList.order_code', '=', 'dt_order_to_supplier.edi_order_code')
                       ->leftJoin('dt_order_product_detail', 'dt_order_product_detail.order_code', '=', 'dt_order_to_supplier.order_code')
                       ->leftJoin('mst_order', 'dt_order_product_detail.receive_id', '=', 'mst_order.receive_id');
        if (!empty($orderCode)) {
            $result->whereIn('dt_order_to_supplier.order_code', $orderCode);
        }
        if (!empty($productCode)) {
            $result->where(function ($query) use ($productCode) {
                $query->whereIn('dt_order_to_supplier.product_code', $productCode)
                      ->orWhereIn('dt_order_to_supplier.product_jan', $productCode);
            });
        }
        return $result->get();
    }

    /**
     * Get data price_invoice, edi_order_code
     * @param  string $orderCode
     * @return object
     */
    public function getDataReturnProuct($orderCode)
    {
        return $this->select(['dt_receipt_ledger.price_invoice', 'dt_order_to_supplier.edi_order_code'])
                    ->join('dt_receipt_ledger', 'dt_receipt_ledger.recognite_key', '=', 'dt_order_to_supplier.edi_order_code')
                    ->where('dt_order_to_supplier.order_code', $orderCode)
                    ->orderBy('dt_receipt_ledger.index', 'DESC')
                    ->first();
    }
}
