<?php

/**
 * Exe sql for hrnb.mst_settlement_manage
 *
 * @package    App\Models
 * @subpackage MstSettlementManage
 * @copyright  Copyright (c) 2017 RiverCrane! Corporation. All Rights Reserved.
 * @author     lam.vinh<lam.vinh.rcvn2012@gmail.com>
 */

namespace App\Models\Backend;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class MstSettlementManage extends Model
{
    public $timestamps = false;
    /**
    * The database table used by the model.
    * @var string
    */
    protected $table = 'mst_settlement_manage';

    /**
    * The primary key.
    * @var string
    */
    protected $primaryKey = 'payment_code';

    /**
    * The database is used by the model.
    * @var string
    */
    protected $connection = 'horunba';

    /**
     * Get data of tabe mst_settlement_manage
     *
     * @return object
     */
    public function getDataSelectBox()
    {
        $data = $this->select('payment_code', 'payment_name')->get();
        return $data;
    }

    /**
     * Get data of tabe mst_settlement_manage
     *
     * @return object
     */
    public function getDataSelectBoxMulti()
    {
        $data = $this->select('payment_code AS key', 'payment_name AS value')->get();
        return $data;
    }

    /**
     * Get data of tabe mst_settlement_manage
     * @param  array $arraySearch
     * @param  array $arraySort
     * @return object
     */
    public function getData($arraySearch = null, $arraySort = null)
    {
        $data = $this->select('payment_code')
                ->addSelect('payment_name')
                ->addSelect('credit_limit_check')
                ->addSelect('payment_request')
                ->addSelect('add_2_mail_text')
                ->addSelect('add_2_receipt_text')
                ->addSelect('have_para');
        if (count($arraySearch) > 0) {
            $data->where(function ($query) use ($arraySearch) {
                if (isset($arraySearch['payment_request'])) {
                    if (is_array($arraySearch['payment_request'])) {
                        $query->whereIn('payment_request', $arraySearch['payment_request']);
                    } else {
                        $query->where('payment_request', $arraySearch['payment_request']);
                    }
                }
            });
        }

        //Sort default
        $data->orderBy('in_date', 'desc');
        //Sort by maunual
        if ($arraySort !== null && count($arraySort) > 0) {
            if (!is_array($arraySort)) {
                $arraySort = json_decode($arraySort);
            }
            foreach ($arraySort as $column => $sort) {
                $data->orderBy($column, $sort);
            }
        }

        $perPage = ($arraySearch['per_page']) ? $arraySearch['per_page'] : 20;
        $data = $data->paginate($perPage);
        return $data;
    }
    /**
     * Update data payment_request or have_param
     *
     * @param  array $keys
     * @param  array $data
     * @return boolean
     */
    public function changeStatus($keys, $data)
    {
        $result = $this->where('payment_code', '=', $keys['payment_code'])
                       ->update($data);
        return $result;
    }

    /**
     * Get item data
     *
     * @param  int  paymentCode
     * @return object
     */
    public function getItem($paymentCode)
    {
        $data = $this->select('payment_code')
                    ->addSelect('payment_name')
                    ->addSelect('payment_request')
                    ->addSelect('add_2_mail_text')
                    ->addSelect('add_2_receipt_text')
                    ->addSelect('have_para')
                    ->where('payment_code', $paymentCode)
                    ->first();
        return $data;
    }

    /**
     * Get data by payment code
     *
     * @param  int    $paymentCode
     * @return object
     */
    public function getDataByPaymentCode($paymentCode)
    {
        $result = $this->where('payment_code', '=', $paymentCode)->first();
        return $result;
    }
}
