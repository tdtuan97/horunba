<?php

/**
 * Exe sql for hrnb.mst_business_style
 *
 * @package    App\Models\Backend
 * @subpackage MstBusinessStyle
 * @copyright  Copyright (c) 2018 RiverCrane! Corporation. All Rights Reserved.
 * @author     le.hung<le.hung.rcvn2012@gmail.com>
 */

namespace App\Models\Backend;

use Illuminate\Database\Eloquent\Model;

class MstBusinessStyle extends Model
{
    public $timestamps = false;
    /**
    * The database table used by the model.
    * @var string
    */
    protected $table = 'mst_business_style';

    /**
    * The database is used by the model.
    * @var string
    */
    protected $connection = 'horunba';
}
