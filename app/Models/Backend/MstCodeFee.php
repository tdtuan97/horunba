<?php

/**
 * Exe sql for hrnb.mst_cod_fee
 *
 * @package    App\Models\Backend
 * @subpackage MstCodFee
 * @copyright  Copyright (c) 2018 RiverCrane! Corporation. All Rights Reserved.
 * @author     Le Hung<le.hung.rcvn2012@gmail.com>
 */

namespace App\Models\Backend;

use Illuminate\Database\Eloquent\Model;

class MstCodeFee extends Model
{
    public $timestamps = false;
    /**
    * The database table used by the model.
    * @var string
    */
    protected $table = 'mst_cod_fee';

    /**
    * The database is used by the model.
    * @var string
    */
    protected $connection = 'horunba';
    
    /**
     * Get code fee
     *
     * @param  string $productCode
     * @return object
     */
    public function getCodeFeeByIndex($codIndex)
    {
        $data = $this->select('cod_fee')
        ->where('cod_index', $codIndex)
        ->first();
        return $data;
    }
}
