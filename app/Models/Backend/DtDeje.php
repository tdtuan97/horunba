<?php

/**
 * Exe sql for hrnb.dt_deje_data
 *
 * @package    App\Models
 * @subpackage DtDeje
 * @copyright  Copyright (c) 2017 RiverCrane! Corporation. All Rights Reserved.
 * @author     le.hung<le.hung.rcvn2012@gmail.com>
 */

namespace App\Models\Backend;

use Illuminate\Database\Eloquent\Model;
use DB;
use Config;

class DtDeje extends Model
{
    public $timestamps = false;
    /**
    * The database table used by the model.
    * @var string
    */
    protected $table = 'dt_deje_data';
    /**
    * The database is used by the model.
    * @var string
    */
    protected $connection = 'horunba';
    /**
    * The primary key.
    * @var string
    */
    protected $primaryKey = 'deje_id';

    /**
     * Get data of table dt_deje_data
     *
     * @params  array    $arraySearch array data search
     * @params  array    $arraySort   array data sort
     * @return object
     */
    public function getData($arraySearch = null, $arraySort = null)
    {
        $data = $this->select('dt_deje_data.*')
                 ->join('mst_tantou', 'mst_tantou.tantou_code', '=', 'dt_deje_data.in_ope_cd')
                ->addSelect(DB::raw("DATE_FORMAT(dt_deje_data.in_date,'%Y/%m/%d %H:%i:%s') AS in_date"))
                ->addSelect("mst_tantou.tantou_last_name AS tantou_last_name")
                ->addSelect(DB::raw("DATE_FORMAT(dt_deje_data.up_date,'%Y/%m/%d %H:%i:%s') AS up_date"));
        $data->leftJoin('mst_tantou AS mst_tantou_dai', 'mst_tantou_dai.tantou_code', '=', 'dt_deje_data.dai_tantou')
            ->leftJoin('mst_business_style', 'mst_business_style.style_id', '=', 'dt_deje_data.business_stype')
            ->leftJoin('dt_deje_comment_data', 'dt_deje_comment_data.deje_id', '=', 'dt_deje_data.deje_id');
        if (count($arraySearch) > 0) {
            $data->where(function ($query) use ($arraySearch) {
                $check = true;
                if (!empty($arraySearch['deje_id'])) {
                    $check = false;
                    $query->where('dt_deje_data.deje_id', "{$arraySearch['deje_id']}");
                }
                if (!empty($arraySearch['subject'])) {
                    $check = false;
                    $query->where('dt_deje_data.subject', 'LIKE', "%{$arraySearch['subject']}%");
                }
                if (!empty($arraySearch['contents'])) {
                    $check = false;
                    $query->where('dt_deje_data.contents', 'LIKE', "%{$arraySearch['contents']}%");
                }
                if (!empty($arraySearch['object_type'])) {
                    $check = false;
                    if (is_array($arraySearch['object_type'])) {
                        $query->whereIn('dt_deje_data.object_type', $arraySearch['object_type']);
                    } else {
                        $query->where('dt_deje_data.object_type', $arraySearch['object_type']);
                    }
                }
                if (!empty($arraySearch['dai_tantou'])) {
                    $check = false;
                    if (is_array($arraySearch['dai_tantou'])) {
                        $query->whereIn('dt_deje_data.dai_tantou', $arraySearch['dai_tantou']);
                    } else {
                        $query->where('dt_deje_data.dai_tantou', $arraySearch['dai_tantou']);
                    }
                }
                if (!empty($arraySearch['process_status'])) {
                    $check = false;
                    if (is_array($arraySearch['process_status'])) {
                        $query->whereIn('dt_deje_data.process_status', $arraySearch['process_status']);
                    } else {
                        $query->where('dt_deje_data.process_status', $arraySearch['process_status']);
                    }
                }
                if (!empty($arraySearch['incharge_person'])) {
                    $check = false;
                    if (is_array($arraySearch['incharge_person'])) {
                        $query->whereIn('dt_deje_data.incharge_person', $arraySearch['incharge_person']);
                    } else {
                        $query->where('dt_deje_data.incharge_person', $arraySearch['incharge_person']);
                    }
                }
                if (!empty($arraySearch['business_stype'])) {
                    $check = false;
                    if (is_array($arraySearch['business_stype'])) {
                        $query->whereIn('dt_deje_data.business_stype', $arraySearch['business_stype']);
                    } else {
                        $query->where('dt_deje_data.business_stype', $arraySearch['business_stype']);
                    }
                }
                if (!empty($arraySearch['process_method'])) {
                    $check = false;
                    if (is_array($arraySearch['process_method'])) {
                        $query->whereIn('process_method', $arraySearch['process_method']);
                    } else {
                        $query->where('process_method', $arraySearch['process_method']);
                    }
                }
                if ($arraySearch['search_all'] !== null) {
                    $check = false;
                    $query->where(function ($whereSub) use ($arraySearch) {
                        $whereSub->where('dt_deje_data.deje_id', $arraySearch['search_all'])
                            ->orWhere('dt_deje_data.subject', 'LIKE', "%{$arraySearch['search_all']}%")
                            ->orWhere('dt_deje_data.contents', 'LIKE', "%{$arraySearch['search_all']}%")
                            ->orWhere('dt_deje_comment_data.contents', 'LIKE', "%{$arraySearch['search_all']}%");

                        $indexObjType = [];
                        foreach (Config::get('common.cmb_object_type') as $key => $val) {
                            if (strpos($val, $arraySearch['search_all']) !== false) {
                                $indexObjType[] = $key;
                            }
                        }
                        if (count($indexObjType) > 0) {
                            $whereSub->orWhereIn('dt_deje_data.object_type', $indexObjType);
                        }

                        $whereSub->orWhere('mst_tantou_dai.tantou_last_name', 'LIKE', "%{$arraySearch['search_all']}%");

                        $indexPS = [];
                        foreach (Config::get('common.process_status') as $key => $val) {
                            if (strpos($val, $arraySearch['search_all']) !== false) {
                                $indexPS[] = $key;
                            }
                        }
                        if (count($indexPS) > 0) {
                            $whereSub->orWhereIn('dt_deje_data.process_status', $indexPS);
                        }

                        $whereSub->orWhere('mst_business_style.style_name', 'LIKE', "%{$arraySearch['search_all']}%");

                        $indexPM = [];
                        foreach (Config::get('common.process_method') as $key => $val) {
                            if (strpos($val, $arraySearch['search_all']) !== false) {
                                $indexPM[] = $key;
                            }
                        }
                        if (count($indexPM) > 0) {
                            $whereSub->orWhereIn('dt_deje_data.process_method', $indexPM);
                        }

                        $whereSub->orWhere('mst_tantou.tantou_last_name', 'LIKE', "%{$arraySearch['search_all']}%");

                        $formatDate = [
                            'Y-m-d H:i:s',
                            'Y/m/d H:i:s',
                            'Y-m-d H:i',
                            'Y/m/d H:i',
                            'Y-m-d H',
                            'Y/m/d H',
                            'Y-m-d',
                            'Y/m/d',
                            'Y-m',
                            'Y/m'
                        ];
                        $startDateSearch = null;
                        $endDateSearch   = null;
                        foreach ($formatDate as $key => $format) {
                            if (\DateTime::createFromFormat($format, $arraySearch['search_all']) !== false) {
                                switch ($key) {
                                    case 0:
                                    case 1:
                                        $searchTime = strtotime($arraySearch['search_all']);
                                        $startDateSearch = date('Y-m-d H:i:s', $searchTime);
                                        $endDateSearch   = date('Y-m-d H:i:s', $searchTime);
                                        break;
                                    case 2:
                                    case 3:
                                        $searchTime = strtotime($arraySearch['search_all'] . ':00');
                                        $startDateSearch = date('Y-m-d H:i:00', $searchTime);
                                        $endDateSearch   = date('Y-m-d H:i:59', $searchTime);
                                        break;
                                    case 4:
                                    case 5:
                                        $searchTime = strtotime($arraySearch['search_all'] . ':00:00');
                                        $startDateSearch = date('Y-m-d H:00:00', $searchTime);
                                        $endDateSearch   = date('Y-m-d H:59:59', $searchTime);
                                        break;
                                    case 6:
                                    case 7:
                                        $searchTime = strtotime($arraySearch['search_all'] . ' 00:00:00');
                                        $startDateSearch = date('Y-m-d 00:00:00', $searchTime);
                                        $endDateSearch   = date('Y-m-d 23:59:59', $searchTime);
                                        break;
                                    case 8:
                                        $searchTime = strtotime($arraySearch['search_all'] . '-01 00:00:00');
                                        $startDateSearch = date('Y-m-01 00:00:00', $searchTime);
                                        $endDateSearch   = date('Y-m-t 23:59:59', $searchTime);
                                        break;
                                    case 9:
                                        $searchTime = strtotime($arraySearch['search_all'] . '/01 00:00:00');
                                        $startDateSearch = date('Y-m-01 00:00:00', $searchTime);
                                        $endDateSearch   = date('Y-m-t 23:59:59', $searchTime);
                                        break;
                                }
                            }
                        }
                        if ($startDateSearch !== null && $endDateSearch !== null) {
                            $whereSub->orWhere(function ($whereSubDate) use ($startDateSearch, $endDateSearch) {
                                $whereSubDate->where('dt_deje_data.in_date', '>=', $startDateSearch)
                                    ->where('dt_deje_data.in_date', '<=', $endDateSearch);
                            });
                            $whereSub->orWhere(function ($whereSubDate) use ($startDateSearch, $endDateSearch) {
                                $whereSubDate->where('dt_deje_data.up_date', '>=', $startDateSearch)
                                    ->where('dt_deje_data.up_date', '<=', $endDateSearch);
                            });
                        }
                    });
                }
                if ($check) {
                    $query->where('dt_deje_data.process_status', '<>', 7);
                }
            });
        }
        $check = false;
        if ($arraySort !== null && count($arraySort) > 0) {
            foreach ($arraySort as $column => $sort) {
                if ($sort !== null && in_array($sort, ['asc', 'desc'])) {
                    $data->orderBy($column, $sort);
                    $check = true;
                }
            }
        }
        if (!$check) {
            $data->orderBy('up_date', 'desc');
        }
        $data->groupBy('dt_deje_data.deje_id');
        $perPage = ($arraySearch['per_page']) ? $arraySearch['per_page'] : 20;
        $data = $data->paginate($perPage);
        return $data;
    }
    /**
     * @param  array condition $where
     * @param  array update $data
     * @return boolean
     */
    public function updateData($where, $data)
    {
        $result = $this->where($where)
                    ->update($data);
        return $result;
    }
    
    /**
     * Get data to print
     *
     * @param int $dejeId
     * @return object
     */
    public function getDataPdf($dejeId)
    {
        $data = $this->select(
            'dt_deje_data.*',
            'mst_tantou_in.tantou_last_name AS last_name_in',
            'mst_tantou_in.tantou_first_name AS first_name_in',
            'mst_tantou_up.tantou_last_name AS last_name_up',
            'mst_tantou_up.tantou_first_name AS first_name_up',
            'mst_tantou_dai.tantou_last_name AS last_name_dai',
            'mst_tantou_dai.tantou_first_name AS first_name_dai',
            'mst_business_style.style_name'
        )
        ->leftJoin('mst_tantou AS mst_tantou_in', 'mst_tantou_in.tantou_code', '=', 'dt_deje_data.in_ope_cd')
        ->leftJoin('mst_tantou AS mst_tantou_up', 'mst_tantou_up.tantou_code', '=', 'dt_deje_data.up_ope_cd')
            ->leftJoin('mst_tantou AS mst_tantou_dai', 'mst_tantou_dai.tantou_code', '=', 'dt_deje_data.dai_tantou')
        ->leftJoin('mst_business_style', 'mst_business_style.style_id', '=', 'dt_deje_data.business_stype')
        ->where('dt_deje_data.deje_id', $dejeId)
        ->first();
        return $data;
    }
}
