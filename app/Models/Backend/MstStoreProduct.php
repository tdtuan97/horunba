<?php
/**
 * Exe sql for hrnb.mst_store_product
 *
 * @package    App\Models
 * @subpackage MstStoreProduct
 * @copyright  Copyright (c) 2018 RiverCrane! Corporation. All Rights Reserved.
 * @author     van.qui<van.qui.rcvn2012@gmail.com>
 */

namespace App\Models\Backend;

use Illuminate\Database\Eloquent\Model;
use App\Custom\DBExtend;

class MstStoreProduct extends Model
{
    use DBExtend;
    public $timestamps = false;
    /**
    * The database table used by the model.
    * @var string
    */
    protected $table = 'mst_store_product';
    /**
    * The database is used by the model.
    * @var string
    */
    protected $connection = 'horunba';

    /**
    * The primary key.
    * @var string
    */
    protected $primaryKey = 'product_code';

    /**
    * The type of primary key.
    * @var string
    */
    protected $keyType = 'string';

    /**
     * Get data of tabe
     *
     * @param  array  $arraySearch
     * @param  array  $arraySort
     * @return object
     */
    public function getData($arraySearch = null, $arraySort = null)
    {
        $data = $this->select('product_code', 'store_product_id', 'store_product_name', 'df_handling', 'product_jan')
        ->addSelect('product_price', 'product_cost', 'product_size', 'product_color', 'product_jan_new', 'is_updated');
        if (count($arraySearch) > 0) {
            $data->where(function ($query) use ($arraySearch) {
                if (isset($arraySearch['product_code_from'])) {
                    $query->where('product_code', '>=', $arraySearch['product_code_from']);
                }
                if (isset($arraySearch['product_code_to'])) {
                    $query->where('product_code', '<=', $arraySearch['product_code_to']);
                }
                if (isset($arraySearch['store_product_id_from'])) {
                    $query->where('store_product_id', '>=', $arraySearch['store_product_id_from']);
                }
                if (isset($arraySearch['store_product_id_to'])) {
                    $query->where('store_product_id', '<=', $arraySearch['store_product_id_to']);
                }
                if (isset($arraySearch['product_jan_new'])) {
                    if ((int)$arraySearch['product_jan_new'] === 0) {
                        $query->where(function ($subQuery) {
                            $subQuery->whereNull('product_jan_new');
                            $subQuery->orWhere('product_jan_new', '');
                        });
                    } else {
                        $query->where(function ($subQuery) {
                            $subQuery->whereNotNull('product_jan_new');
                            $subQuery->where('product_jan_new', '<>', '');
                        });
                    }
                }
                if (isset($arraySearch['df_handling'])) {
                    if ((int)$arraySearch['df_handling'] === 0) {
                        $query->where('df_handling', 0);
                    } else {
                        $query->where('df_handling', 1);
                    }
                }
            });
        }
        if ($arraySort !== null && count($arraySort) > 0) {
            foreach ($arraySort as $column => $sort) {
                if ($sort !== null && in_array($sort, ['asc', 'desc'])) {
                    $data->orderBy($column, $sort);
                }
            }
        }
        $data->orderBy('up_date', 'asc');
        $data->orderBy('product_jan_new', 'desc');
        $perPage = ($arraySearch['per_page']) ? $arraySearch['per_page'] : 20;
        $data = $data->paginate($perPage);
        return $data;
    }

    /**
     * Update data by condition
     *
     * @param  array   $productCode
     * @param  array   $data
     * @return boolean
     */
    public function updateByProductCodes($productCode, $data)
    {
        return $this->whereIn('product_code', $productCode)->update($data);
    }
}
