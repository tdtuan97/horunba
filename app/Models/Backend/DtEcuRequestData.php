<?php

/**
 * Exe sql for hrnb.dt_ecu_request_data
 *
 * @package    App\Models
 * @subpackage DtEcuRequestData
 * @copyright  Copyright (c) 2018 RiverCrane! Corporation. All Rights Reserved.
 * @author     van.qui<van.qui.rcvn2012@gmail.com>
 */

namespace App\Models\Backend;

use Illuminate\Database\Eloquent\Model;

class DtEcuRequestData extends Model
{
    public $timestamps = false;
    /**
    * The database table used by the model.
    * @var string
    */
    protected $table = 'dt_ecu_request_data';
    /**
    * The database is used by the model.
    * @var string
    */
    protected $connection = 'horunba';
    
    /**
     * Get data by deje_id
     *
     * @param  int     $dejeId
     * @return object
     */
    public function getDataByDejeId($dejeId)
    {
        $data = $this->where('deje_id', $dejeId)->get();
        return $data;
    }
    
    /**
     * Get data export issue
     *
     * @param  int $dejeId
     * @return collection
     */
    public function getDataIssue($dejeId)
    {
        $data = $this->select(
            'dt_ecu_request_data.deje_id',
            'dt_ecu_request_data.line_num',
            'dt_ecu_request_data.product_code',
            'mst_product.product_name_long',
            'dt_ecu_request_data.quantity',
            'dt_ecu_request_data.price_komi'
        )
        ->join('mst_product', 'mst_product.product_code', '=', 'dt_ecu_request_data.product_code')
        ->where('dt_ecu_request_data.deje_id', $dejeId)
        ->get();
        return $data;
    }
}
