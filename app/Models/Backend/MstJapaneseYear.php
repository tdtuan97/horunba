<?php

/**
 * Exe sql for hrnb.mst_japanese_year
 *
 * @package    App\Models
 * @subpackage MstGenre
 * @copyright  Copyright (c) 2018 RiverCrane! Corporation. All Rights Reserved.
 * @author     Truong Nghia<truong.van.nghia@rivercrane.vn>
 */

namespace App\Models\Backend;

use Illuminate\Database\Eloquent\Model;

class MstJapaneseYear extends Model
{
    public $timestamps = false;
    /**
    * The database table used by the model.
    * @var string
    */
    protected $table = 'mst_japanese_year';
    /**
    * The database is used by the model.
    * @var string
    */
    protected $connection = 'horunba';
}
