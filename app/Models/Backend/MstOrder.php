<?php

/**
 * Exe sql for hrnb.mst_order
 *
 * @package    App\Models
 * @subpackage MstOrder
 * @copyright  Copyright (c) 2017 RiverCrane! Corporation. All Rights Reserved.
 * @author     le.hung<le.hung.rcvn2012@gmail.com>
 * @author     van.qui<van.qui.rcvn2012@gmail.com>
 * @author     truong.nghia<truong.nghia.rcvn2012@gmail.com>
 */

namespace App\Models\Backend;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Auth;
use App\Events\MstOrderUpdated;
use Illuminate\Pagination\Paginator;

class MstOrder extends Model
{
    public $timestamps = false;
    /**
    * The database table used by the model.
    * @var string
    */
    protected $table = 'mst_order';

    /**
    * The database is used by the model.
    * @var string
    */
    protected $connection = 'horunba';

    /**
    * The primary key.
    * @var string
    */
    protected $primaryKey = 'receive_id';

    protected $dispatchesEvents = [
        'updated' => MstOrderUpdated::class,
    ];

    /**
     * Get all data of tabe mst_order
     *
     * @return object
     */
    public function getData($arraySearch = null, $arraySort = null, $arrayOptions = null)
    {
        $fullName = 'CONCAT(IFNULL(mst_customer.last_name, "")," ",IFNULL(mst_customer.first_name, "")) AS full_name';
        $data = $this->select('mst_order.received_order_id')
                ->addSelect('mst_order.order_date')
                ->addSelect('mst_order.receive_id')
                ->addSelect('mst_order.order_status')
                ->addSelect('mst_order.request_price')
                ->addSelect('mst_settlement_manage.payment_name')
                ->addSelect('mst_order_status.status_name')
                ->addSelect('mst_mall.name_jp')
                ->addSelect('mst_order.delay_priod')
                ->addSelect(DB::raw($fullName))
                ->leftJoin('mst_settlement_manage', function ($join) {
                    $join->on('mst_settlement_manage.payment_code', '=', 'mst_order.payment_method');
                })
                ->leftJoin('mst_order_status', function ($join) {
                    $join->on('mst_order.order_status', '=', 'mst_order_status.order_status_id');
                })
                ->leftJoin('mst_customer', function ($join) {
                    $join->on('mst_order.customer_id', '=', 'mst_customer.customer_id');
                })
                ->leftJoin('mst_mall', function ($join) {
                    $join->on('mst_mall.id', '=', 'mst_order.mall_id');
                });
        if (count($arraySearch) > 0) {
            $data->where(function ($query) use ($arraySearch) {
                if (isset($arraySearch['receive_id'])) {
                    $query->where('mst_order.receive_id', '=', $arraySearch['receive_id']);
                }
                if (isset($arraySearch['product_code'])) {
                    $query->where(function ($subWhere) use ($arraySearch) {
                        $subWhere->whereRaw("EXISTS (select 1 from"
                                . " mst_order_detail where mst_order_detail.receive_id=mst_order.receive_id"
                                . " and product_code LIKE '%{$arraySearch['product_code']}%' limit 1)")
                          ->orWhereRaw(" exists ("
                                  . " SELECT 1 FROM dt_order_product_detail, mst_order_detail"
                                  . " where dt_order_product_detail.receive_id = mst_order_detail.receive_id  "
                                  . " and mst_order.receive_id = dt_order_product_detail.receive_id "
                                  . " and dt_order_product_detail.detail_line_num =  mst_order_detail.detail_line_num "
                                  . " and  child_product_code LIKE '%{$arraySearch['product_code']}%' limit 1)");
                    });
                }
                if (isset($arraySearch['full_name'])) {
                    $query->where(function ($subWhere) use ($arraySearch) {
                        $fullName = $arraySearch['full_name'];
                        $multi  = str_replace('　', ' ', $fullName);
                        $single = str_replace(' ', '　', $fullName);
                        $searchFullName = 'CONCAT(IFNULL(mst_customer.last_name, "")," ",IFNULL(mst_customer.first_name, ""))';
                        $subWhere->whereRaw("{$searchFullName} LIKE '%{$multi}%'")
                                ->orWhereRaw("{$searchFullName} LIKE '%{$single}%'");
                    });
                }
                if (isset($arraySearch['address'])) {
                    $query->where(function ($subWhere) use ($arraySearch) {
                        $fullAddress = $arraySearch['address'];
                        $multi  = str_replace('　', ' ', $fullAddress);
                        $single = str_replace(' ', '　', $fullAddress);
                        $searchFullAdd = 'CONCAT(IFNULL(mst_customer.prefecture, ""),IFNULL(mst_customer.city, ""),IFNULL(mst_customer.sub_address, ""))';
                        $strQuery = "EXISTS (select 1 from mst_order_detail";
                        $strQuery .= " JOIN mst_customer ON mst_customer.customer_id = mst_order_detail.receiver_id";
                        $strQuery .= " WHERE mst_order_detail.receive_id=mst_order.receive_id";
                        $strQuery .= " AND (({$searchFullAdd} LIKE '%{$multi}%') OR ({$searchFullAdd} LIKE '%{$single}%')))";
                        $subWhere->whereRaw($strQuery);
                    });
                }
                if (isset($arraySearch['name_kana'])) {
                    $query->where(function ($subWhere) use ($arraySearch) {
                        $nameKana = $arraySearch['name_kana'];
                        $searchNameKana = 'CONCAT(IFNULL(mst_customer.last_name_kana, ""),IFNULL(mst_customer.first_name_kana, ""))';
                        $subWhere->whereRaw("{$searchNameKana} LIKE '%{$nameKana}%'");
                    });
                }
                if (isset($arraySearch['tel_num'])) {
                    $query->where(function ($subWhere) use ($arraySearch) {
                        $telNum = $arraySearch['tel_num'];
                        $subWhere->whereRaw("mst_customer.tel_num LIKE '%{$telNum}%'");
                    });
                }
                if (isset($arraySearch['order_date_from'])) {
                    $dateFrom = date("Y-m-d 00:00:00", strtotime($arraySearch['order_date_from']));
                    $query->where('mst_order.order_date', '>=', $dateFrom);
                }
                if (isset($arraySearch['order_date_to'])) {
                    $dateTo = date("Y-m-d 23:59:59", strtotime($arraySearch['order_date_to']));
                    $query->where('mst_order.order_date', '<=', $dateTo);
                }
                if (isset($arraySearch['name_jp'])) {
                    if (is_array($arraySearch['name_jp'])) {
                        $query->whereIn('mst_mall.id', $arraySearch['name_jp']);
                    } else {
                        $query->where('mst_mall.id', $arraySearch['name_jp']);
                    }
                }
                if (isset($arraySearch['order_status'])) {
                    if (is_array($arraySearch['order_status'])) {
                        $query->whereIn('mst_order.order_status', $arraySearch['order_status']);
                    } else {
                        $query->where('mst_order.order_status', $arraySearch['order_status']);
                    }
                }
                if (isset($arraySearch['order_sub_status'])) {
                    if (is_array($arraySearch['order_sub_status'])) {
                        $query->whereIn('mst_order.order_sub_status', $arraySearch['order_sub_status']);
                    } else {
                        $query->where('mst_order.order_sub_status', $arraySearch['order_sub_status']);
                    }
                }
                if (isset($arraySearch['payment_method'])) {
                    if (is_array($arraySearch['payment_method'])) {
                        $query->whereIn('mst_order.payment_method', $arraySearch['payment_method']);
                    } else {
                        $query->where('mst_order.payment_method', $arraySearch['payment_method']);
                    }
                }
                if (isset($arraySearch['order_sub_status'])) {
                    if (is_array($arraySearch['order_sub_status'])) {
                        $query->whereIn('mst_order.order_sub_status', $arraySearch['order_sub_status']);
                    } else {
                        $query->where('mst_order.order_sub_status', $arraySearch['order_sub_status']);
                    }
                }
                if (isset($arraySearch['another_order_status'])) {
                    $query->where('mst_order.order_status', '<>', $arraySearch['another_order_status']);
                }
                if (isset($arraySearch['is_mall_cancel'])) {
                    $query->where('mst_order.is_mall_cancel', $arraySearch['is_mall_cancel']);
                }
                if (isset($arraySearch['is_mall_update'])) {
                    $query->where('mst_order.is_mall_update', $arraySearch['is_mall_update']);
                }
                if (isset($arraySearch['received_order_id'])) {
                    $query->where('mst_order.received_order_id', $arraySearch['received_order_id']);
                }
                if (isset($arraySearch['is_delay'])) {
                    $query->where(
                        'mst_order.is_delay',
                        ($arraySearch['is_delay'] === '1' ? '=' : '<>'),
                        1
                    );
                }

                if (isset($arraySearch['delay_priod_from'])) {
                    $dateFrom = date("Y-m-d 00:00:00", strtotime($arraySearch['delay_priod_from']));
                    $query->where('mst_order.delay_priod', '>=', $dateFrom);
                }
                if (isset($arraySearch['delay_priod_to'])) {
                    $dateTo = date("Y-m-d 23:59:59", strtotime($arraySearch['delay_priod_to']));
                    $query->where('mst_order.delay_priod', '<=', $dateTo);
                }

                if (isset($arraySearch['request_price_from'])) {
                    $query->where('mst_order.request_price', '>=', $arraySearch['request_price_from']);
                }
                if (isset($arraySearch['request_price_to'])) {
                    $query->where('mst_order.request_price', '<=', $arraySearch['request_price_to']);
                }
            });
        }
        if ($arraySort !== null && count($arraySort) > 0) {
            foreach ($arraySort as $column => $sort) {
                if (in_array($sort, ['asc', 'desc'])) {
                    $data->orderBy($column, $sort);
                }
            }
        }
        $data->orderBy('receive_id', 'desc');
        $perPage = ($arraySearch['per_page']) ? $arraySearch['per_page'] : 20;
        if (isset($arrayOptions['type'])) {
            $data = $data->get();
        } else {
            $data = $data->paginate($perPage);
        }
        return $data;
    }

    /**
     * Get data all
     * @array $arrayOrder
     * @return object
     */
    public function getDataAll($arrayOrder)
    {
        $fullName = 'CONCAT(IFNULL(mst_customer.last_name, "")," ",IFNULL(mst_customer.first_name, "")) AS full_name';
        $data = $this->select('mst_order.received_order_id')
                ->addSelect('mst_order.order_date')
                ->addSelect('mst_order.receive_id')
                ->addSelect('mst_order.order_status')
                ->addSelect('mst_order.request_price')
                ->addSelect('mst_settlement_manage.payment_name')
                ->addSelect('mst_order_status.status_name')
                ->addSelect('mst_mall.name_jp')
                ->addSelect('mst_order.delay_priod')
                ->addSelect(DB::raw($fullName))
                ->leftJoin('mst_settlement_manage', function ($join) {
                    $join->on('mst_settlement_manage.payment_code', '=', 'mst_order.payment_method');
                })
                ->leftJoin('mst_order_status', function ($join) {
                    $join->on('mst_order.order_status', '=', 'mst_order_status.order_status_id');
                })
                ->leftJoin('mst_customer', function ($join) {
                    $join->on('mst_order.customer_id', '=', 'mst_customer.customer_id');
                })
                ->leftJoin('mst_mall', function ($join) {
                    $join->on('mst_mall.id', '=', 'mst_order.mall_id');
                })
                ->whereIn('mst_order.receive_id', $arrayOrder)
                ->orderBy('mst_order.receive_id', 'desc');
        $currentPage = 1;
        Paginator::currentPageResolver(function () use ($currentPage) {
            return $currentPage;
        });
        return $data->paginate(count($arrayOrder));
    }

    /**
     * Get data page
     * @array $page
     * @return object
     */
    public function getDataPage($page)
    {
        $data = $this->select(['mst_order.receive_id'])
                     ->orderBy('mst_order.receive_id', 'desc');
        $data = $data->paginate($page);
        return $data;
    }
    /**
     * Get a item data of tabe mst_order
     * @array $arrayQuery
     * @return object
     */
    public function getItem($arrayQuery = null)
    {
        $data = $this->select('mst_order.*')
                ->addSelect('mst_order.delivery_instrustions AS delivery_instrustions_col')
                ->leftJoin('mst_settlement_manage', function ($join) {
                    $join->on('mst_settlement_manage.payment_code', '=', 'mst_order.payment_method');
                })
                ->addSelect('mst_settlement_manage.payment_name')
                ->addSelect('order_cus.is_black_list')
                ->leftJoin('mst_order_status', function ($join) {
                    $join->on('mst_order.order_status', '=', 'mst_order_status.order_status_id');
                })
                ->leftJoin('mst_order_detail', 'mst_order_detail.receive_id', '=', 'mst_order.receive_id')
                ->leftJoin('mst_customer AS order_cus', 'mst_order.customer_id', '=', 'order_cus.customer_id')
                ->leftJoin('mst_customer AS receiver_cus', 'mst_order_detail.receiver_id', '=', 'receiver_cus.customer_id')
                ->leftJoin('mst_backlist_comment AS bc', 'bc.ID', '=', 'order_cus.is_black_list')
                ->leftJoin('dt_delivery', function ($join) {
                    $join->on('dt_delivery.received_order_id', '=', 'mst_order.received_order_id');
                })
                ->addSelect('mst_order_status.status_name')
                ->addSelect('mst_settlement_manage.payment_name')
                ->addSelect(DB::raw('CONCAT(IFNULL(order_cus.last_name,"")," ",IFNULL(order_cus.first_name,"")) AS full_name'))
                ->addSelect('order_cus.prefecture')
                ->addSelect('order_cus.city')
                ->addSelect('order_cus.sub_address')
                ->addSelect('order_cus.zip_code')
                ->addSelect('order_cus.tel_num')
                ->addSelect('order_cus.fax_num')
                ->addSelect('order_cus.urgent_tel_num')
                ->addSelect('order_cus.email')

                ->addSelect(DB::raw('CONCAT(IFNULL(receiver_cus.last_name, "")," ",IFNULL(receiver_cus.first_name, "")) AS receiver_full_name'))
                ->addSelect('receiver_cus.prefecture AS receiver_prefecture')
                ->addSelect('receiver_cus.city AS receiver_city')
                ->addSelect('receiver_cus.sub_address AS receiver_sub_address')
                ->addSelect('receiver_cus.zip_code AS receiver_zip_code')
                ->addSelect('receiver_cus.tel_num AS receiver_tel_num')

                ->addSelect('dt_delivery.inquiry_no')
                ->addSelect('dt_delivery.delivery_instrustions')
                ->addSelect('dt_delivery.delivery_plan_date')
                ->addSelect('dt_delivery.delivery_date')
                ->addSelect('dt_delivery.delivery_time')
                ->addSelect('bc.backlist_comment')
                ->addSelect('bc.backlist_rank')
                ->leftJoin('mst_order_sub_status', function ($join) {
                    $join->on('mst_order.order_sub_status', '=', 'mst_order_sub_status.order_sub_status_id');
                })
                ->addSelect('mst_order_sub_status.sub_status_name')

                ->addSelect('mst_mall.name_jp')
                ->join('mst_mall', 'mst_mall.id', '=', 'mst_order.mall_id')

                ->where('mst_order.receive_id', "{$arrayQuery['receive_id']}");
        $data = $data->first();
        return $data;
    }
    /**
     * Get items data of tabe mst_order joins with dt_order_product_detail
     * @array $arrayQuery
     * @return object
     */
    public function getItemsForRePayMent($arrayQuery = null, $check = false)
    {
        $data = $this->select('mst_order.order_status')
                ->addSelect('mst_order.receive_id')
                ->addSelect('dt_order_product_detail.detail_line_num')
                ->addSelect('dt_order_product_detail.sub_line_num')
                ->addSelect('dt_order_product_detail.product_code')
                ->addSelect('dt_order_product_detail.sale_price')
                ->addSelect('dt_order_product_detail.received_order_num AS quantity')
                ->addSelect('dt_order_product_detail.product_status')
                ->addSelect('dt_order_product_detail.received_order_num')
                ->addSelect("mst_order.goods_price AS sub_total")
                ->join('dt_order_product_detail', 'dt_order_product_detail.receive_id', '=', 'mst_order.receive_id')
                ->where('mst_order.order_status', '!=', ORDER_STATUS['ON_HOLD'])
                ->where('dt_order_product_detail.product_status', PRODUCT_STATUS['RETURN'])
                ->where('mst_order.received_order_id', "{$arrayQuery['received_order_id']}");
        if ($check) {
            $data->addSelect('dt_return.return_quantity')
            ->join('dt_return', 'dt_order_product_detail.receive_id', '=', 'dt_return.receive_id')
            ->join('dt_repayment', function ($sub) {
                  $sub->on('dt_return.return_no', '=', 'dt_repayment.return_no')
                      ->on('dt_return.return_time', '=', 'dt_repayment.repayment_time');
            })
            ->where('dt_repayment.repay_no', $arrayQuery['repay_no'])
            ->groupBy('product_code');
        }
        $data = $data->get();
        return $data;
    }
    /**
     * Get info repayment by receive_id
     *
     * @param  string $index
     * @return object
     */
    public function getInfoForRepaymentById($receiveOrderId)
    {
        $data = $this->select(
            'receive_id'
        )
        ->addSelect('mst_cod_fee.cod_fee')
        ->addSelect('order_status')
        ->addSelect('total_price')
        ->addSelect('request_price')
        ->addSelect('pay_charge_discount')
        ->addSelect('used_point')
        ->addSelect('pay_after_charge')
        ->addSelect('ship_charge')
        ->addSelect('discount')
        ->addSelect('discount')
        ->addSelect('pay_charge')
        ->addSelect('used_coupon')
        ->addSelect('mst_settlement_manage.payment_name')
        ->addSelect(DB::raw("(mst_order.goods_price + mst_order.goods_price) AS sub_total"))
        ->addSelect(DB::raw("CONCAT(IFNULL(mst_customer.first_name, ''),' ',IFNULL(mst_customer.last_name, '')) AS full_name"))
        ->leftJoin('mst_customer', 'mst_customer.customer_id', '=', 'mst_order.customer_id')
        ->leftJoin('mst_settlement_manage', 'mst_settlement_manage.payment_code', '=', 'mst_order.payment_method')
        ->leftJoin('mst_cod_fee', 'mst_cod_fee.mall_id', '=', 'mst_order.mall_id')
        ->where('received_order_id', '=', $receiveOrderId)->first();
        return $data;
    }
    /**
     * Get issue estimate data
     *
     * @param  int    $receiveId
     * @return object
     */
    public function getIssue($receiveId)
    {
        $data = $this->select('mst_order.receive_id', 'mst_order.received_order_id', 'mst_order.goods_price')
                    ->addSelect('mst_order.goods_tax', 'mst_order.ship_charge', 'mst_order.pay_charge')
                    ->addSelect('mst_order.pay_after_charge')
                    ->addSelect('mst_order.used_point', 'mst_order.used_coupon', 'mst_order.pay_charge')
                    ->addSelect('mst_order.request_price', 'mst_customer.first_name', 'mst_customer.last_name')
                    ->addSelect('mst_customer.zip_code', 'mst_customer.prefecture', 'mst_customer.city')
                    ->addSelect('mst_customer.sub_address', 'mst_customer.tel_num', 'mst_order_detail.detail_line_num')
                    ->addSelect('mst_order_detail.product_name', 'mst_order_detail.price', 'mst_order_detail.quantity')
                    ->join('mst_order_detail', 'mst_order_detail.receive_id', '=', 'mst_order.receive_id')
                    ->join('mst_customer', 'mst_order_detail.receiver_id', '=', 'mst_customer.customer_id')
                    ->leftjoin('dt_order_product_detail', function ($join) {
                        $join->on('mst_order_detail.receive_id', '=', 'dt_order_product_detail.receive_id')
                            ->on('mst_order_detail.detail_line_num', '=', 'dt_order_product_detail.detail_line_num')
                            ->where('dt_order_product_detail.product_status', '<>', PRODUCT_STATUS['CANCEL']);
                    })
                    ->where('mst_order.receive_id', '=', $receiveId)
                    ->groupBy('mst_order.receive_id', 'mst_order_detail.detail_line_num')
                    ->get();
        return $data;
    }
    /**
     * Update data
     *
     * @param  array $where
     * @param  array $data
     * @return boolean
     */
    public function changeDelay($data)
    {
        $flg = ($data['is_delay'] === 0) ? 1 : 0;
        $dataUpdate['is_delay'] = $flg;
        if ($flg === 1) {
            $dataUpdate['delay_reason'] = $data['reason'];
            $dataUpdate['delay_priod']  = date("Y-m-d", strtotime("+7 day"));
            $dataUpdate['delay_by']     = Auth::user()->tantou_last_name;
        } else {
            $dataUpdate['delay_priod']  = null;
            $dataUpdate['delay_reason'] = null;
            $dataUpdate['delay_by']     = null;
        }
        $dataUpdate['up_date']      = date('Y-m-d H:i:s');
        $dataUpdate['up_ope_cd']   = Auth::user()->tantou_code;
        $result = $this->where('receive_id', '=', $data['receive_id'])
                        ->update($dataUpdate);
        return true;
    }

      /**
     * Get data payment list
     *
     * @param  array $arrSearch List value search
     * @param  array $arrSort List sort
     * @return object
     */
    public function getDataPatment($arrSearch = '', $arrSort = '')
    {
        $col = [
            'index',
            'mm.name_jp',
            'mst_order.order_date',
            'mst_order.received_order_id',
            'mos.status_name as order_status',
            'mst_order.request_price',
            'mst_order.payment_request_date',
            'pl.payment_price',
            'pl.payment_date',
            'pl.up_date',
        ];
        $query = $this->select($col)
                      ->selectRaw('CONCAT(IFNULL(mc.last_name, ""), IFNULL(mc.first_name, "")) as full_name')
                      ->selectRaw('(mst_order.request_price - pl.payment_price) as remain_amount')
                      ->join('mst_mall as mm', 'mst_order.mall_id', '=', 'mm.id')
                      ->join('mst_customer as mc', 'mst_order.customer_id', '=', 'mc.customer_id')
                      ->join(
                          'mst_order_status as mos',
                          'mst_order.order_status',
                          '=',
                          'mos.order_status_id'
                      )
                      ->join('dt_payment_list as pl', 'mst_order.receive_id', '=', 'pl.receive_id');
        if (count($arrSearch) > 0) {
            $query->where(function ($query) use ($arrSearch) {
                if (isset($arrSearch['name_jp'])) {
                    if (is_array($arrSearch['name_jp'])) {
                        $query->whereIn('mst_order.mall_id', $arrSearch['name_jp']);
                    } else {
                        $query->where('mst_order.mall_id', $arrSearch['name_jp']);
                    }
                }
                if (isset($arrSearch['order_date_from'])) {
                    $dateFrom = date("Y-m-d 00:00:00", strtotime($arrSearch['order_date_from']));
                    $query->where(
                        'mst_order.order_date',
                        '>=',
                        $dateFrom
                    );
                }
                if (isset($arrSearch['order_date_to'])) {
                    $dateTo = date("Y-m-d 23:59:59", strtotime($arrSearch['order_date_to']));
                    $query->where(
                        'mst_order.order_date',
                        '<=',
                        $dateTo
                    );
                }
                if (isset($arrSearch['request_price_from'])) {
                    $query->where(
                        'mst_order.request_price',
                        '>=',
                        "{$arrSearch['request_price_from']}"
                    );
                }
                if (isset($arrSearch['request_price_to'])) {
                    $query->where(
                        'mst_order.request_price',
                        '<=',
                        "{$arrSearch['request_price_to']}"
                    );
                }
                if (isset($arrSearch['order_status'])) {
                    if (is_array($arrSearch['order_status'])) {
                        $query->whereIn('mst_order.order_status', $arrSearch['order_status']);
                    } else {
                        $query->where('mst_order.order_status', $arrSearch['order_status']);
                    }
                }
                if (isset($arrSearch['remain_amount'])) {
                    $query->whereRaw(
                        '(mst_order.request_price - pl.payment_price) = ?',
                        "{$arrSearch['remain_amount']}"
                    );
                }
                if (isset($arrSearch['full_name'])) {
                    $query->whereRaw(
                        'CONCAT(IFNULL(mc.last_name, ""), IFNULL(mc.first_name, "")) '.
                        'like '.
                        "'%{$arrSearch['full_name']}%'"
                    );
                }
                if (isset($arrSearch['payment_account'])) {
                    $query->where('payment_account', $arrSearch['payment_account']);
                }
                if (isset($arrSearch['receive_id'])) {
                    $query->where('mst_order.receive_id', $arrSearch['receive_id']);
                }
            });
        }
        $check = false;
        if ($arrSort !== null && count($arrSort) > 0) {
            foreach ($arrSort as $column => $sort) {
                if (in_array($sort, ['asc', 'desc'])) {
                    $query->orderBy($column, $sort);
                    $check = true;
                }
            }
        }
        if (!$check) {
            $query->orderBy('pl.in_date', 'asc');
        }
        $perPage = ($arrSearch['per_page']) ? $arrSearch['per_page'] : 20;
        $data = $query->paginate($perPage);
        return $data;
    }

      /**
     * Get data check payment account
     *
     * @param  string $paymentAccount
     * @return object
     */
    public function getDataCheckAccount($paymentAccount, $requestPrice)
    {
        $result = $this->select(DB::raw('count(*) as count_num, receive_id, payment_status'))
                       ->whereRaw('order_date > (NOW() - INTERVAL 14 DAY)')
                       ->where('request_price', $requestPrice)
                       ->where('payment_account', $paymentAccount)
                       ->first();
        return $result;
    }

      /**
     * Get data process payment by index
     *
     * @param  int $id
     * @return object
     */
    public function getDataProPayById($id)
    {
        $result = $this->selectRaw('request_price - dpl.payment_price AS cal')
                       ->join('dt_payment_list AS dpl', 'mst_order.receive_id', '=', 'dpl.receive_id')
                       ->where('order_status', '=', ORDER_STATUS['PAYMENT_CONFIRM'])
                       ->where('order_sub_status', '=', ORDER_SUB_STATUS['NEW'])
                       ->where('index', '=', $id)
                       ->first();
        return $result;
    }
      /**
     * Get data check payment rakuten
     *
     * @param  string $receiveOrderId
     * @param  int $paymentPrice
     * @return object
     */
    public function getDataCheckPayRakuten($receivedOrderId, $paymentPrice)
    {
        $result = $this->select(DB::raw('count(*) as count_num, receive_id, payment_status'))
                       ->where('received_order_id', $receivedOrderId)
                       ->where('request_price', $paymentPrice)
                       ->first();
        return $result;
    }
      /**
     * Get data check update rakuten payment
     *
     * @param  string $receiveId
     * @return object
     */
    public function getDataRakuPayUpdate($receiveId)
    {
        $result = $this->join('dt_payment_list AS dpl', 'mst_order.receive_id', '=', 'dpl.receive_id')
                       ->where('order_status', '=', ORDER_STATUS['PAYMENT_CONFIRM'])
                       ->where('order_sub_status', '=', ORDER_SUB_STATUS['NEW'])
                       ->where('payment_code', '=', 6)
                       ->where('mst_order.receive_id', '=', $receiveId)
                       ->count();
        return $result;
    }

    /**
     * Get info order by value input
     *
     * @param  string $receiveOrderId
     * @param  string $index
     * @param  string $paymentPrice
     * @return object
     */
    public function getOrderInput($receiveOrderId, $index, $paymentPrice)
    {
        $col = [
            'mst_order.request_price AS request_price_' . $index,
            'mst_order.order_date AS order_date_' . $index,
        ];

        $rerult = $this->select($col)
                       ->selectRaw("(mst_order.request_price - $paymentPrice) AS price_" . $index)
                       ->selectRaw('CONCAT(last_name_kana, first_name_kana) AS full_name_' . $index)
                       ->join('hrnb.mst_customer as cus', 'mst_order.customer_id', '=', 'cus.customer_id')
                       ->where('received_order_id', '=', $receiveOrderId)
                       ->first();
        return $rerult;
    }

    /**
     * Get info order mitsu
     *
     * @param  string $receiveOrderId
     * @return int
     */
    // public function getOrderMitsu($receiveOrderId)
    // {
    //     $rerult = $this->where('received_order_id', $receiveOrderId)
    //                    ->where('is_mail_sent', 1)
    //                    ->whereNotNull('payment_account')
    //                    ->count();
    //     return $rerult;
    // }

    /**
     * Count order Rakuten
     *
     * @param  int    $receiveOrderId
     * @param  int    $paymentPrice
     * @param  string $paymentName
     * @return int
     */

    public function getOrderRakuten($receiveOrderId, $paymentPrice, $paymentName)
    {
        $subCol = [
            'mst_order.request_price',
            'mst_order.order_date',
        ];
        $case = "case when cus.last_name_kana = 'カブシキガイシャ' then CONCAT('カ）',cus.first_name_kana)"
                ." when cus.first_name_kana =  'カブシキガイシャ' then CONCAT(cus.last_name_kana, '（カ')"
                ." else CONCAT(cus.last_name_kana, ' ', cus.first_name_kana) end  as full_kana";
        $sub = $this->select($subCol)
                    ->selectRaw($case)
                    ->join('hrnb.mst_customer as cus', 'mst_order.customer_id', '=', 'cus.customer_id')
                    ->where('received_order_id', '=', $receiveOrderId)
                    ->where('mst_order.order_status', '=', ORDER_STATUS['PAYMENT_CONFIRM'])
                    ->where('mst_order.order_sub_status', '=', ORDER_SUB_STATUS['NEW'])
                    ->where('mst_order.mall_id', '=', 1)
                    ->where('mst_order.payment_method', '=', 6);

        $result = DB::table(DB::raw("({$sub->toSql()}) as temp_order"))
                  ->mergeBindings($sub->getQuery())
                  ->where('temp_order.full_kana', '=', $paymentName)
                  ->where('temp_order.request_price', '=', $paymentPrice)
                  ->count();
        return $result;
    }

    /**
    * Get data check validate postal and address.
    * @param string $receiveId
    * @return object $result
    */
    public function getDataCheckValidatePostal($receiveId)
    {
        $result = $this->join('mst_customer AS cus', 'cus.customer_id', '=', 'mst_order.customer_id')
                       ->join('mst_postal AS pos', 'cus.zip_code', '=', 'pos.postal_code')
                       ->where('mst_order.receive_id', '=', $receiveId)
                       ->whereRaw(" LOCATE(Concat(ifnull(pos.prefecture,''),ifnull(pos.city,'')), "
                           . "Concat(ifnull(cus.prefecture,''), ifnull(cus.city,''), ifnull(cus.sub_address,''))) > 0")
                       ->count();
        return $result;
    }

    /**
    * Get data by receive_id.
    * @param string $receiveId
    * @return object $result
    */
    public function getDataByReceiveId($receiveId)
    {
        $result = $this->where('mst_order.receive_id', '=', $receiveId)
                       ->first();
        return $result;
    }

    /**
    * Get data check validate direct delivery and payment.
    * @param string $receiveId
    * @return object $result
    */
    public function getDataCheckDirectAndPayment($receiveId)
    {
        $result = $this->join('dt_order_product_detail as do', 'mst_order.receive_id', '=', 'do.receive_id')
                       ->where('mst_order.receive_id', '=', $receiveId)
                       ->where('mst_order.payment_method', '=', 3)
                       ->where('do.delivery_type', '=', 2)
                       ->count();
        return $result;
    }

    /**
    * Get data check validate black list.
    * @param string $receiveId
    * @return object $result
    */
    public function getDataCheckBackList($receiveId)
    {
        $result = $this->join('mst_customer AS cus', 'cus.customer_id', '=', 'mst_order.customer_id')
                       ->where('mst_order.receive_id', '=', $receiveId)
                       ->where('cus.is_black_list', '>', 0)
                       ->where('mst_order.is_confirmed', '=', 0)
                       ->count();
        return $result;
    }

    /**
    * Get data check validate fee transport.
    * @param string $receiveId
    * @param string $receiverId
    * @return object $result
    */
    public function getDataCheckFeeTransport($receiveId, $receiverId = null)
    {
        $result = $this->join('mst_order_detail', 'mst_order_detail.receive_id', '=', 'mst_order.receive_id')
                       ->join('mst_customer AS cus', 'cus.customer_id', '=', 'mst_order_detail.receiver_id')
                       ->join('mst_postal AS pos', 'cus.zip_code', '=', 'pos.postal_code')
                       ->where('mst_order.receive_id', '=', $receiveId);
        if ($receiverId !== null) {
            $result->where('mst_order_detail.receiver_id', $receiverId);
        }
        $result = $result->where('pos.add_ship_charge', '=', 1)
                       ->where('mst_order_detail.ship_charge_detail', '<>', 5400)
                       ->whereNotIn('mst_order.mall_id', [3, 7, 8])
                       ->count();
        return $result;
    }

    /**
    * Get data check validate fee delivery.
    * @param string $receiveId
    * @param string $receiverId
    * @return object $result
    */
    public function getDataCheckFeeDelivery($receiveId, $receiverId = null)
    {
        $result = $this->join('mst_order_detail', 'mst_order_detail.receive_id', '=', 'mst_order.receive_id')
                       ->join('mst_customer AS cus', 'cus.customer_id', '=', 'mst_order_detail.receiver_id')
                       ->join('mst_postal AS pos', 'cus.zip_code', '=', 'pos.postal_code')
                       ->join('mst_shipping_fee AS fee', 'mst_order.mall_id', '=', 'fee.mall_id')
                       ->where('mst_order.receive_id', '=', $receiveId);
        $checkSum = "SELECT SUM(price * quantity) FROM mst_order_detail WHERE mst_order_detail.receive_id = $receiveId";
        if ($receiverId !== null) {
            $result->where('mst_order_detail.receiver_id', $receiverId);
            $checkSum .= " AND mst_order_detail.receiver_id = $receiverId";
        }
        $checkSum = "(" . $checkSum . ")";
        $result = $result->whereRaw('DATE(mst_order.order_date) > DATE(fee.start_date)')
                       ->whereRaw('DATE(mst_order.order_date) < DATE(fee.end_date)')
                       ->where('fee.is_delete', '=', 0)
                       ->whereRaw("$checkSum >= fee.below_limit_price")
                       ->where('pos.add_ship_charge', '<>', 1)
                       ->where('mst_order_detail.ship_charge_detail', '>', 0)
                       ->whereNotIn('mst_order.mall_id', [3, 8])
                       ->count();
        return $result;
    }

    /**
    * Get data check validate calculate delivery
    * @param string $receiveId
    * @param string $receiverId
    * @return object $result
    */
    public function getDataCheckFeeCulDelivery($receiveId, $receiverId = null)
    {
        $result = $this->join('mst_order_detail', 'mst_order_detail.receive_id', '=', 'mst_order.receive_id')
                       ->join('mst_customer AS cus', 'cus.customer_id', '=', 'mst_order_detail.receiver_id')
                       ->join('mst_postal AS pos', 'cus.zip_code', '=', 'pos.postal_code')
                       ->join('mst_shipping_fee AS fee', 'mst_order.mall_id', '=', 'fee.mall_id')
                       ->where('mst_order.receive_id', '=', $receiveId);
        $checkSum = "SELECT SUM(price * quantity) FROM mst_order_detail WHERE mst_order_detail.receive_id = $receiveId";
        if ($receiverId !== null) {
            $result->where('mst_order_detail.receiver_id', $receiverId);
            $checkSum .= " AND mst_order_detail.receiver_id = $receiverId";
        }
        $checkSum = "(" . $checkSum . ")";
        $result = $result->where('fee.is_delete', '=', 0)
                       ->whereRaw('DATE(mst_order.order_date) > DATE(fee.start_date)')
                       ->whereRaw('DATE(mst_order.order_date) < DATE(fee.end_date)')
                       ->whereRaw("$checkSum < fee.below_limit_price")
                       ->where('pos.add_ship_charge', '<>', 1)
                       ->where('mst_order.payment_method', '<>', 11)
                       ->whereRaw('mst_order_detail.ship_charge_detail <> fee.charge_price')
                       ->whereNotIn('mst_order.mall_id', [3, 8])
                       ->count();
        return $result;
    }
    /**
    * Get data check validate payment delivery fee
    * @param string $receiveId
    * @return object $result
    */
    public function getDataCheckPaymentDeliveryFee($receiveId)
    {
        $result = $this->join('mst_cod_fee AS cod', 'cod.mall_id', '=', 'mst_order.mall_id')
                       ->where('mst_order.receive_id', '=', $receiveId)
                       ->where('cod.is_delete', '=', 0)
                       ->whereRaw('mst_order.goods_price > cod.min_amount')
                       ->whereRaw('mst_order.goods_price <= cod.max_amount')
                       ->whereRaw('mst_order.order_date >= start_date')
                       ->whereRaw('mst_order.order_date <= end_date')
                       ->whereRaw("(( mst_order.pay_after_charge <> cod.cod_fee
                            and mst_order.payment_method = '3')
                            or (mst_order.payment_method <> '3')
                            and mst_order.pay_after_charge <> 0 )")
                       ->count();
        return $result;
    }

    /**
    * Get data check validate fee delivery.
    * @param string $receiveId
    * @return object $result
    */
    public function getDataCheckPaidByPoint($receiveId)
    {
        $result = $this->where('mst_order.receive_id', '=', $receiveId)
                       ->whereIn('mst_order.payment_method', [10, 11])
                       ->where('mst_order.request_price', '>', 0)
                       ->count();
        return $result;
    }

    /**
    * Get data check validate fee delivery.
    * @param string $receiveId
    * @return object $result
    */
    public function getDataCheckPaidNotByPoint($receiveId)
    {
        $result = $this->where('mst_order.receive_id', '=', $receiveId)
                       ->whereNotIn('mst_order.payment_method', [10, 11])
                       ->where('mst_order.request_price', '=', 0)
                       ->count();
        return $result;
    }
    /**
     * Get order editing data
     *
     * @param  int  $receiveId
     * @return json
     */
    public function getOrderEditing($receiveId)
    {
        $data = $this->select('received_order_id', 'receive_id', 'order_status', 'order_date', 'mst_order.in_date')
                    ->addSelect('order_sub_status')
                    ->addSelect('company_name', 'goods_price', 'goods_tax', 'pay_charge_discount')
                    ->addSelect('ship_charge', 'discount', 'pay_after_charge', 'used_point', 'pay_charge')
                    ->addSelect('used_coupon', 'total_price', 'request_price', 'customer_question')
                    ->addSelect('shop_answer', 'delivery_instrustions', 'last_name', 'first_name')
                    ->addSelect('zip_code', 'prefecture', 'city', 'sub_address', 'tel_num', 'fax_num')
                    ->addSelect('urgent_tel_num', 'email', 'payment_method', 'payment_name', 'bc.backlist_rank', 'is_black_list')
                    ->join('mst_customer', 'mst_customer.customer_id', '=', 'mst_order.customer_id')
                    ->leftJoin('mst_backlist_comment AS bc', 'bc.ID', '=', 'mst_customer.is_black_list')
                    ->leftJoin(
                        'mst_settlement_manage',
                        'mst_settlement_manage.payment_code',
                        '=',
                        'mst_order.payment_method'
                    )
                    ->where('receive_id', $receiveId)
                    ->first();
        return $data;
    }

    /**
     * Get order detail editing data
     * @param  int $receiveId
     * @return json
     */
    public function getOrderDetailEditing($receiveId, $receiverId = null)
    {
        $data = $this->select(
            'mst_order.receive_id',
            'mst_order.order_status',
            'mst_order.order_sub_status',
            'mst_order.ship_wish_date',
            'mst_order.ship_wish_time',
            'mst_order.ship_charge',
            'mst_order.pay_after_charge',
            'mst_order.pay_charge',
            'mst_order.total_price',
            'mst_order.pay_charge_discount',
            'mst_order.discount',
            'mst_order.used_point',
            'mst_order.used_coupon',
            'mst_order.request_price',
            'mst_order.goods_price',
            'mst_order.delivery_method as delivery_code',
            'mst_order.mall_id',
            'mst_customer.last_name',
            'mst_customer.first_name',
            'mst_customer.tel_num',
            'mst_customer.zip_code',
            'mst_customer.prefecture',
            'mst_customer.city',
            'mst_customer.sub_address',
            'mst_order_detail.detail_line_num',
            'mst_order_detail.product_code',
            'mst_order_detail.product_name',
            'mst_order_detail.price',
            'mst_order_detail.quantity',
            'dt_order_product_detail.sub_line_num',
            'dt_order_product_detail.child_product_code',
            'dt_order_product_detail.received_order_num',
            'dt_order_product_detail.price_invoice',
            'dt_order_product_detail.delivery_type',
            'dt_order_product_detail.product_status',
            'dt_order_product_detail.order_code',
            'mst_product.product_name_long AS child_product_name',
            'dt_delivery.delivery_plan_date',
            'mst_price_mall.price_calc_rak_komi',
            'mst_price_mall.price_calc_yah_komi',
            'mst_price_mall.price_calc_ama_komi',
            'mst_price_mall.price_calc_biz_nuki',
            'mst_price_mall.price_calc_diy_nuki',
            'mst_product_set.component_num'
        )

        ->join('mst_order_detail', 'mst_order_detail.receive_id', '=', 'mst_order.receive_id')
        ->join('mst_customer', 'mst_customer.customer_id', '=', 'mst_order_detail.receiver_id')
        ->leftjoin('dt_order_product_detail', function ($join) {
            $join->on('dt_order_product_detail.receive_id', '=', 'mst_order_detail.receive_id')
                ->on('dt_order_product_detail.detail_line_num', '=', 'mst_order_detail.detail_line_num');
        })
        ->leftJoin(
            'mst_product',
            'mst_product.product_code',
            '=',
            'dt_order_product_detail.child_product_code'
        )
        ->leftJoin('mst_price_mall', 'mst_order_detail.product_code', '=', 'mst_price_mall.product_code')
        ->leftJoin('dt_delivery_detail', function ($join) {
            $join->on('dt_delivery_detail.detail_line_num', '=', 'dt_order_product_detail.sub_line_num')
                ->on('dt_delivery_detail.received_order_id', '=', 'mst_order.received_order_id');
        })
        ->leftJoin('dt_delivery', function ($join) {
            $join->on('dt_delivery.subdivision_num', '=', 'dt_delivery_detail.subdivision_num')
                ->on('dt_delivery.received_order_id', '=', 'dt_delivery_detail.received_order_id');
        })
        ->leftJoin('mst_product_set', function ($join) {
            $join->on('dt_order_product_detail.product_code', '=', 'mst_product_set.parent_product_code')
                ->on('dt_order_product_detail.child_product_code', '=', 'mst_product_set.child_product_code');
        })
        ->where('mst_order.receive_id', $receiveId)
        ->whereRaw('IF(dt_delivery.detail_line_num <> 0, dt_delivery.detail_line_num = dt_delivery_detail.detail_line_num , 1 = 1)');
        if ($receiverId !== null) {
            $data->where('mst_order_detail.receiver_id', $receiverId);
        }
        $data = $data->get();

        return $data;
    }

    /**
     * Get delivery_code by receive_id
     *
     * @param  int      $receiveId
     * @param  int      $receiverId
     * @return object
     */
    public function getDeliveryCode($receiveId, $receiverId)
    {
        $data = $this->select(
            'dt_delivery.received_order_id',
            'dt_delivery.detail_line_num',
            'dt_delivery.delivery_code',
            'dt_delivery.received_order_id AS key_delivery'
        )
        ->join('mst_order_detail', 'mst_order_detail.receive_id', '=', 'mst_order.receive_id')
        ->join('dt_order_product_detail', function ($join) {
            $join->on('dt_order_product_detail.receive_id', '=', 'mst_order_detail.receive_id')
                ->on('dt_order_product_detail.detail_line_num', '=', 'mst_order_detail.detail_line_num');
        })
        ->leftJoin('dt_delivery_detail', function ($join) {
            $join->on('dt_delivery_detail.detail_line_num', '=', 'dt_order_product_detail.sub_line_num')
                ->on('dt_delivery_detail.received_order_id', '=', 'mst_order.received_order_id');
        })
        ->leftJoin('dt_delivery', function ($join) {
            $join->on('dt_delivery.subdivision_num', '=', 'dt_delivery_detail.subdivision_num')
                ->on('dt_delivery.received_order_id', '=', 'dt_delivery_detail.received_order_id');
        })
        ->where('mst_order.receive_id', $receiveId)
        ->whereRaw('IF(dt_delivery.detail_line_num <> 0, dt_delivery.detail_line_num = dt_delivery_detail.detail_line_num , 1 = 1)')
        ->where('mst_order_detail.receiver_id', $receiverId)
        ->get();
        return $data;
    }

    /**
     * Update data by key
     *
     * @param  array   $keys
     * @param  array   $data
     * @return boolean
     */
    public function updateData($keys, $data)
    {
        if (array_key_exists('order_status', $data) ||
          array_key_exists('order_sub_status', $data)) {
            $backtrace = debug_backtrace(false, 2)[1];
            $className = $backtrace['class'];
            $function  = $backtrace['function'];
            $action    = $className . '@' . $function;
            $modelLog  = new LogUpdateStatusOrder();
            $modelLog->insertLog($keys, $data, $action, key($keys));
        }
        return $this->where($keys)->update($data);
    }

    /**
     * Update data by received_order_id
     *
     * @param  array   $keys
     * @param  array   $data
     * @return boolean
     */
    public function updateDataByReceivedOrderId($keys, $data)
    {
        if (array_key_exists('order_status', $data) ||
          array_key_exists('order_sub_status', $data)) {
            $backtrace = debug_backtrace(false, 2)[1];
            $className = $backtrace['class'];
            $function  = $backtrace['function'];
            $action    = $className . '@' . $function;
            $modelLog  = new LogUpdateStatusOrder();
            $modelLog->insertLog($keys, $data, $action, 'received_order_id');
        }
        return $this->whereIn('received_order_id', $keys)->update($data);
    }

    /**
     * Get data by received_order_id
     *
     * @param  string $receivedOrderId
     * @return object
     */
    public function getDataByReceivedOrderId($receivedOrderId)
    {
        $data = $this->where('received_order_id', $receivedOrderId)->first();
        return $data;
    }

    /**
    * The get information process content mail.
    * @param string $receiveOrderId order_id of order want sent mail
    * @return object $result
    */
    public function getInfoOrderMail($receivedOrderId)
    {
        $col = [
            'mst_order.receive_id',
            'mst_order.received_order_id',
            'mst_order.order_date',
            'mst_order.shipment_date',
            'mst_order.total_price',
            'mst_order.goods_price',
            'mst_order.ship_charge',
            'mst_order.pay_charge',
            'mst_order.discount',
            'mst_order.used_point',
            'mst_order.used_coupon',
            'mst_order.company_name',
            'mst_order.mall_id',
            'mst_order.pay_after_charge',
            'mst_order.pay_charge_discount',
            'mst_order.payment_price',
            'mst_order.payment_confirm_date',
            'mst_order.payment_account',
            'mst_order.payment_method',
            'mst_order.customer_question',
            'mst_order.mail_seri',
            'mst_order.shop_answer',
            'mst_cancel_reason.add_2_mail_text as cancel_reason',
            'mst_order.request_price',
            'dt_payment_list.payment_date',
            'mco.last_name as order_last_name',
            'mco.first_name as order_first_name',
            'mco.zip_code as order_zip_code',
            'mco.prefecture as order_prefecture',
            'mco.sub_address as order_sub_address',
            'mco.city as order_city',
            'mco.tel_num as order_tel_num',
            'pm.payment_name',
            'mcod.last_name as ship_last_name',
            'mcod.first_name as ship_first_name',
            'mcod.zip_code as ship_zip_code',
            'mcod.prefecture as ship_prefecture',
            'mcod.sub_address as ship_sub_address',
            'mcod.city as ship_city',
            'mcod.tel_num as ship_tel_num',
            'dd.delivery_plan_date',
            'dd.delivery_date',
            'dd.inquiry_no',
            'msc.company_name AS ship_company_name'
        ];
        $result = $this->select($col)
                       ->join('mst_order_detail as od', 'mst_order.receive_id', '=', 'od.receive_id')
                       ->leftjoin('mst_payment_method as pm', function ($join) {
                          $join->on('mst_order.mall_id', '=', 'pm.mall_id')
                              ->on('mst_order.payment_method', '=', 'pm.payment_code');
                       })
                       ->join('mst_customer as mco', 'mst_order.customer_id', '=', 'mco.customer_id')
                       ->join('mst_customer as mcod', 'od.receiver_id', '=', 'mcod.customer_id')
                       ->leftjoin('dt_delivery as dd', 'mst_order.received_order_id', '=', 'dd.received_order_id')
                       ->leftjoin('mst_shipping_company as msc', 'dd.delivery_code', '=', 'msc.company_id')
                       ->leftjoin('dt_payment_list', 'dt_payment_list.receive_id', '=', 'mst_order.receive_id')
                       ->leftjoin('mst_cancel_reason', 'mst_cancel_reason.reason_id', '=', 'mst_order.cancel_reason')
                       ->where('mst_order.received_order_id', '=', $receivedOrderId)
                       ->first();
        return $result;
    }

    /**
    * The get information order detail process content mail.
    * @param string $receiveOrderId order_id of order want sent mail
    * @return object $result
    */
    public function getInfoOrderDetail($receivedOrderId)
    {
        $col = [
            'mst_order.received_order_id',
            'mcod.last_name as ship_last_name',
            'mcod.first_name as ship_first_name',
            'mcod.zip_code as ship_zip_code',
            'mcod.prefecture as ship_prefecture',
            'mcod.city as ship_city',
            'mcod.sub_address as ship_sub_address',
            'mcod.tel_num as ship_tel_num',
            'od.product_code',
            'od.product_name',
            // 'od.price',
            // 'od.quantity',
            'dto.delivery_type',
            'dto.suplier_id'
        ];
        $subPrice = 'CASE WHEN dto.product_status = 15 THEN 0 ELSE od.price END as price';
        $subQuantity = 'CASE WHEN dto.product_status = 15 THEN 0 ELSE od.quantity END as quantity';
        $result = $this->select($col)
                       ->selectRaw($subPrice)
                       ->selectRaw($subQuantity)
                       ->join('mst_order_detail as od', 'mst_order.receive_id', '=', 'od.receive_id')
                       ->join('mst_customer as mcod', 'od.receiver_id', '=', 'mcod.customer_id')
                       ->leftjoin('dt_order_product_detail as dto', function ($join) {
                          $join->on('dto.receive_id', '=', 'od.receive_id')
                              ->on('dto.detail_line_num', '=', 'od.detail_line_num');
                       })
                       ->where('mst_order.received_order_id', '=', $receivedOrderId)
                       ->groupBy('dto.detail_line_num')
                       ->get();
        return $result;
    }
    /**
     * Get data of table for refund detail
     *
     * @return object
     * @author le.hung
     */
    public function getItemForRefund($arrayQuery = null, $arraySearch = null, $arraySort = null)
    {
        $data = $this->select('*')
                ->addSelect('dt_opd.product_status')
                ->addSelect('mst_order_detail.product_code as product_code')
                ->selectRaw("IFNULL(err_text, '') AS err_text")
                ->leftJoin('mst_order_detail', 'mst_order.receive_id', '=', 'mst_order_detail.receive_id')
                ->leftJoin('dt_order_product_detail AS dt_opd', function ($join) {
                    $join->on('mst_order.receive_id', '=', 'dt_opd.receive_id')
                        ->on('mst_order_detail.detail_line_num', '=', 'dt_opd.detail_line_num');
                })
                ->where('mst_order.received_order_id', '=', $arrayQuery['origin_receive_order_id'])
                ->groupBy(
                    'mst_order_detail.receive_id',
                    'mst_order_detail.detail_line_num',
                    'mst_order_detail.product_code'
                )
                ->get();

        return $data;
    }
    /**
     * Get customer info by received_order_id
     *
     * @param  int  $receiveOrderId
     * @return object
     */

    public function getCusInfoByRecevieOrderId($receivedOrderId = null)
    {
        $data = array();
        if (!empty($receivedOrderId)) {
            $data = $this->select('receive_id', 'received_order_id', 'mst_customer.last_name', 'mst_order.customer_id', 'mall_id')
                    ->where('received_order_id', '=', "$receivedOrderId")
                    ->leftJoin(
                        'mst_customer',
                        'mst_customer.customer_id',
                        '=',
                        'mst_order.customer_id'
                    )
                    ->first();
        }
        return $data;
    }
    /**
     * Get max item for refund detail
     *
     * @param  int  $receiveOrderId
     * @return object
     */

    public function getMaxItemForRefundDetail($prefix = 'RETURN')
    {
        $data = $this->select('received_order_id')
                ->where('received_order_id', 'like', "$prefix%")->orderBy('receive_id', 'desc')
                ->first();
        return $data;
    }

    /**
     * Get position row
     *
     * @param  int    $receiveIdKey
     * @param  array  $arraySearch
     * @param  array  $arraySort
     * @return object
     */
    public function getPositionRow($receiveIdKey, $arraySearch = null, $arraySort = null)
    {
        $query = $this->select('mst_order.receive_id')
        ->from('mst_order')
        ->leftJoin('mst_settlement_manage', 'mst_settlement_manage.payment_code', '=', 'mst_order.payment_method')
        ->leftJoin('mst_order_status', 'mst_order.order_status', '=', 'mst_order_status.order_status_id')
        ->leftJoin('mst_customer', 'mst_order.customer_id', '=', 'mst_customer.customer_id')
        ->leftJoin('mst_mall', 'mst_mall.id', '=', 'mst_order.mall_id');
        if (count($arraySearch) > 0) {
            if (isset($arraySearch['receive_id'])) {
                $query->where('mst_order.receive_id', '=', $arraySearch['receive_id']);
            }
            if (isset($arraySearch['full_name'])) {
                $query->where(function ($subWhere) use ($arraySearch) {
                    $fullName = $arraySearch['full_name'];
                    $multi  = str_replace('　', ' ', $fullName);
                    $single = str_replace(' ', '　', $fullName);
                    $searchFullName = 'CONCAT(IFNULL(mst_customer.last_name, "")," ",IFNULL(mst_customer.first_name, ""))';
                    $subWhere->whereRaw("{$searchFullName} LIKE '%{$multi}%'")
                                ->orWhereRaw("{$searchFullName} LIKE '%{$single}%'");
                });
            }
            if (isset($arraySearch['product_code'])) {
                $query->where(function ($subWhere) use ($arraySearch) {
                    $subWhere->whereRaw("EXISTS (select 1 from"
                            . " mst_order_detail where mst_order_detail.receive_id=mst_order.receive_id"
                            . " and product_code LIKE '%{$arraySearch['product_code']}%' limit 1)")
                      ->orWhereRaw(" exists ("
                              . " SELECT 1 FROM dt_order_product_detail, mst_order_detail"
                              . " where dt_order_product_detail.receive_id = mst_order_detail.receive_id  "
                              . " and mst_order.receive_id = dt_order_product_detail.receive_id "
                              . " and dt_order_product_detail.detail_line_num =  mst_order_detail.detail_line_num "
                              . " and  child_product_code LIKE '%{$arraySearch['product_code']}%' limit 1)");
                });
            }
            if (isset($arraySearch['order_date_from'])) {
                $dateFrom = date("Y-m-d 00:00:00", strtotime($arraySearch['order_date_from']));
                $query->where('mst_order.order_date', '>=', $dateFrom);
            }
            if (isset($arraySearch['order_date_to'])) {
                $dateTo = date("Y-m-d 23:59:59", strtotime($arraySearch['order_date_to']));
                $query->where('mst_order.order_date', '<=', $dateTo);
            }
            if (isset($arraySearch['name_jp'])) {
                if (is_array($arraySearch['name_jp'])) {
                    $query->whereIn('mst_mall.id', $arraySearch['name_jp']);
                } else {
                    $query->where('mst_mall.id', $arraySearch['name_jp']);
                }
            }
            if (isset($arraySearch['order_status'])) {
                if (is_array($arraySearch['order_status'])) {
                    $query->whereIn('mst_order.order_status', $arraySearch['order_status']);
                } else {
                    $query->where('mst_order.order_status', $arraySearch['order_status']);
                }
            }
            if (isset($arraySearch['payment_method'])) {
                if (is_array($arraySearch['payment_method'])) {
                    $query->whereIn('mst_order.payment_method', $arraySearch['payment_method']);
                } else {
                    $query->where('mst_order.payment_method', $arraySearch['payment_method']);
                }
            }
            if (isset($arraySearch['order_sub_status'])) {
                if (is_array($arraySearch['order_sub_status'])) {
                    $query->whereIn('mst_order.order_sub_status', $arraySearch['order_sub_status']);
                } else {
                    $query->where('mst_order.order_sub_status', $arraySearch['order_sub_status']);
                }
            }
            if (isset($arraySearch['is_mall_cancel'])) {
                $query->where('mst_order.is_mall_cancel', $arraySearch['is_mall_cancel']);
            }
            if (isset($arraySearch['is_mall_update'])) {
                $query->where('mst_order.is_mall_update', $arraySearch['is_mall_update']);
            }
            if (isset($arraySearch['received_order_id'])) {
                $query->where('mst_order.received_order_id', $arraySearch['received_order_id']);
            }
            if (isset($arraySearch['another_order_status'])) {
                $query->where('mst_order.order_status', '<>', $arraySearch['another_order_status']);
            }
            if (isset($arraySearch['is_delay'])) {
                $query->where(
                    'mst_order.is_delay',
                    ($arraySearch['is_delay'] === '1' ? '=' : '<>'),
                    1
                );
            }
            if (isset($arraySearch['delay_priod_from'])) {
                $dateFrom = date("Y-m-d 00:00:00", strtotime($arraySearch['delay_priod_from']));
                $query->where('mst_order.delay_priod', '>=', $dateFrom);
            }
            if (isset($arraySearch['delay_priod_to'])) {
                $dateTo = date("Y-m-d 23:59:59", strtotime($arraySearch['delay_priod_to']));
                $query->where('mst_order.delay_priod', '<=', $dateTo);
            }
            if (isset($arraySearch['address'])) {
                $query->where(function ($subWhere) use ($arraySearch) {

                    $fullAddress = $arraySearch['address'];
                    $multi  = str_replace('　', ' ', $fullAddress);
                    $single = str_replace(' ', '　', $fullAddress);
                    $searchFullAdd = 'CONCAT(IFNULL(mst_customer.prefecture, ""),IFNULL(mst_customer.city, ""),IFNULL(mst_customer.sub_address, ""))';
                    $strQuery = "EXISTS (select 1 from mst_order_detail";
                    $strQuery .= " JOIN mst_customer ON mst_customer.customer_id = mst_order_detail.receiver_id";
                    $strQuery .= " WHERE mst_order_detail.receive_id=mst_order.receive_id";
                    $strQuery .= " AND (({$searchFullAdd} LIKE '%{$multi}%') OR ({$searchFullAdd} LIKE '%{$single}%')))";
                    $subWhere->whereRaw($strQuery);
                });
            }

            if (isset($arraySearch['request_price_from'])) {
                $query->where('mst_order.request_price', '>=', $arraySearch['request_price_from']);
            }
            if (isset($arraySearch['request_price_to'])) {
                $query->where('mst_order.request_price', '<=', $arraySearch['request_price_to']);
            }
        }
        if ($arraySort !== null && count($arraySort) > 0) {
            foreach ($arraySort as $column => $sort) {
                if (in_array($sort, ['asc', 'desc'])) {
                    $query->orderBy($column, $sort);
                }
            }
        }
        $query->orderBy('receive_id', 'desc');
        //trick limit to keep data ordered
        $query->limit(2147483647);

        $queryTotal = $this->selectRaw('receive_id, @rownum := @rownum + 1 AS position')
            ->from(DB::raw("({$query->toSql()}) as q"))
            ->mergeBindings($query->getQuery())
            ->crossJoin(DB::raw('(SELECT @rownum := 0) r'));

        $data = $this->selectRaw('receive_id, position')
            ->from(DB::raw("({$queryTotal->toSql()}) as qt"))
            ->mergeBindings($queryTotal->getQuery())
            ->where('receive_id', $receiveIdKey);
        return $data->first();
    }

    /**
     * Get item by position
     *
     * @param  int    $position
     * @param  array  $arraySearch
     * @param  array  $arraySort
     * @return object
     */
    public function getItemByPosition($position, $arraySearch = null, $arraySort = null)
    {
        $data = $this->select('mst_order.receive_id')
        ->leftjoin('mst_settlement_manage', 'mst_settlement_manage.payment_code', '=', 'mst_order.payment_method')
        ->leftJoin('mst_order_status', 'mst_order.order_status', '=', 'mst_order_status.order_status_id')
        ->leftJoin('mst_customer', 'mst_order.customer_id', '=', 'mst_customer.customer_id')
        ->leftJoin('mst_mall', 'mst_mall.id', '=', 'mst_order.mall_id');
        if (count($arraySearch) > 0) {
            if (isset($arraySearch['receive_id'])) {
                $data->where('mst_order.receive_id', '=', $arraySearch['receive_id']);
            }
            if (isset($arraySearch['product_code'])) {
                $data->where(function ($subWhere) use ($arraySearch) {
                    $subWhere->whereRaw("EXISTS (select 1 from"
                            . " mst_order_detail where mst_order_detail.receive_id=mst_order.receive_id"
                            . " and product_code LIKE '%{$arraySearch['product_code']}%' limit 1)")
                      ->orWhereRaw(" exists ("
                              . " SELECT 1 FROM dt_order_product_detail, mst_order_detail"
                              . " where dt_order_product_detail.receive_id = mst_order_detail.receive_id  "
                              . " and mst_order.receive_id = dt_order_product_detail.receive_id "
                              . " and dt_order_product_detail.detail_line_num =  mst_order_detail.detail_line_num "
                              . " and  child_product_code LIKE '%{$arraySearch['product_code']}%' limit 1)");
                });
            }
            if (isset($arraySearch['full_name'])) {
                $data->where(function ($subWhere) use ($arraySearch) {
                    $fullName = $arraySearch['full_name'];
                    $multi  = str_replace('　', ' ', $fullName);
                    $single = str_replace(' ', '　', $fullName);
                    $searchFullName = 'CONCAT(IFNULL(mst_customer.last_name, "")," ",IFNULL(mst_customer.first_name, ""))';
                    $subWhere->whereRaw("{$searchFullName} LIKE '%{$multi}%'")
                                ->orWhereRaw("{$searchFullName} LIKE '%{$single}%'");
                });
            }
            if (isset($arraySearch['order_date_from'])) {
                $dateFrom = date("Y-m-d 00:00:00", strtotime($arraySearch['order_date_from']));
                $data->where('mst_order.order_date', '>=', $dateFrom);
            }
            if (isset($arraySearch['order_date_to'])) {
                $dateTo = date("Y-m-d 23:59:59", strtotime($arraySearch['order_date_to']));
                $data->where('mst_order.order_date', '<=', $dateTo);
            }
            if (isset($arraySearch['name_jp'])) {
                if (is_array($arraySearch['name_jp'])) {
                    $data->whereIn('mst_mall.id', $arraySearch['name_jp']);
                } else {
                    $data->where('mst_mall.id', $arraySearch['name_jp']);
                }
            }
            if (isset($arraySearch['order_status'])) {
                if (is_array($arraySearch['order_status'])) {
                    $data->whereIn('mst_order.order_status', $arraySearch['order_status']);
                } else {
                    $data->where('mst_order.order_status', $arraySearch['order_status']);
                }
            }
            if (isset($arraySearch['payment_method'])) {
                if (is_array($arraySearch['payment_method'])) {
                    $data->whereIn('mst_order.payment_method', $arraySearch['payment_method']);
                } else {
                    $data->where('mst_order.payment_method', $arraySearch['payment_method']);
                }
            }
            if (isset($arraySearch['order_sub_status'])) {
                if (is_array($arraySearch['order_sub_status'])) {
                    $data->whereIn('mst_order.order_sub_status', $arraySearch['order_sub_status']);
                } else {
                    $data->where('mst_order.order_sub_status', $arraySearch['order_sub_status']);
                }
            }
            if (isset($arraySearch['is_mall_cancel'])) {
                $data->where('mst_order.is_mall_cancel', $arraySearch['is_mall_cancel']);
            }
            if (isset($arraySearch['another_order_status'])) {
                $data->where('mst_order.order_status', '<>', $arraySearch['another_order_status']);
            }
            if (isset($arraySearch['is_mall_update'])) {
                $data->where('mst_order.is_mall_update', $arraySearch['is_mall_update']);
            }
            if (isset($arraySearch['received_order_id'])) {
                $data->where('mst_order.received_order_id', $arraySearch['received_order_id']);
            }
            if (isset($arraySearch['is_delay'])) {
                $data->where(
                    'mst_order.is_delay',
                    ($arraySearch['is_delay'] === '1' ? '=' : '<>'),
                    1
                );
            }
            if (isset($arraySearch['delay_priod_from'])) {
                $dateFrom = date("Y-m-d 00:00:00", strtotime($arraySearch['delay_priod_from']));
                $data->where('mst_order.delay_priod', '>=', $dateFrom);
            }
            if (isset($arraySearch['delay_priod_to'])) {
                $dateTo = date("Y-m-d 23:59:59", strtotime($arraySearch['delay_priod_to']));
                $data->where('mst_order.delay_priod', '<=', $dateTo);
            }
            if (isset($arraySearch['address'])) {
                $data->where(function ($subWhere) use ($arraySearch) {
                    $fullAddress = $arraySearch['address'];
                    $multi  = str_replace('　', ' ', $fullAddress);
                    $single = str_replace(' ', '　', $fullAddress);
                    $searchFullAdd = 'CONCAT(IFNULL(mst_customer.prefecture, ""),IFNULL(mst_customer.city, ""),IFNULL(mst_customer.sub_address, ""))';
                    $strQuery = "EXISTS (select 1 from mst_order_detail";
                    $strQuery .= " JOIN mst_customer ON mst_customer.customer_id = mst_order_detail.receiver_id";
                    $strQuery .= " WHERE mst_order_detail.receive_id=mst_order.receive_id";
                    $strQuery .= " AND (({$searchFullAdd} LIKE '%{$multi}%') OR ({$searchFullAdd} LIKE '%{$single}%')))";
                    $subWhere->whereRaw($strQuery);
                });
            }

            if (isset($arraySearch['request_price_from'])) {
                $data->where('mst_order.request_price', '>=', $arraySearch['request_price_from']);
            }
            if (isset($arraySearch['request_price_to'])) {
                $data->where('mst_order.request_price', '<=', $arraySearch['request_price_to']);
            }
        }
        if ($arraySort !== null && count($arraySort) > 0) {
            foreach ($arraySort as $column => $sort) {
                if (in_array($sort, ['asc', 'desc'])) {
                    $data->orderBy($column, $sort);
                }
            }
        }
        $data->orderBy('receive_id', 'desc')
            ->offset($position)
            ->limit(3);
        return $data->get();
    }
    /**
     * Get item by position
     *
     * @param  int    $position
     * @param  array  $arraySearch
     * @param  array  $arraySort
     * @return object
     */
    public function countItemsByPosition($arraySearch = null, $arraySort = null)
    {
        $query = $this->select('mst_order.receive_id')
        ->from('mst_order')
        ->leftJoin('mst_settlement_manage', 'mst_settlement_manage.payment_code', '=', 'mst_order.payment_method')
        ->leftJoin('mst_order_status', 'mst_order.order_status', '=', 'mst_order_status.order_status_id')
        ->leftJoin('mst_customer', 'mst_order.customer_id', '=', 'mst_customer.customer_id')
        ->leftJoin('mst_mall', 'mst_mall.id', '=', 'mst_order.mall_id');
        if (count($arraySearch) > 0) {
            if (isset($arraySearch['receive_id'])) {
                $query->where('mst_order.receive_id', '=', $arraySearch['receive_id']);
            }
            if (isset($arraySearch['product_code'])) {
                $query->where(function ($subWhere) use ($arraySearch) {
                    $subWhere->whereRaw("EXISTS (select 1 from"
                            . " mst_order_detail where mst_order_detail.receive_id=mst_order.receive_id"
                            . " and product_code LIKE '%{$arraySearch['product_code']}%' limit 1)")
                      ->orWhereRaw(" exists ("
                              . " SELECT 1 FROM dt_order_product_detail, mst_order_detail"
                              . " where dt_order_product_detail.receive_id = mst_order_detail.receive_id  "
                              . " and mst_order.receive_id = dt_order_product_detail.receive_id "
                              . " and dt_order_product_detail.detail_line_num =  mst_order_detail.detail_line_num "
                              . " and  child_product_code LIKE '%{$arraySearch['product_code']}%' limit 1)");
                });
            }
            if (isset($arraySearch['full_name'])) {
                $query->where(function ($subWhere) use ($arraySearch) {
                    $fullName = $arraySearch['full_name'];
                    $multi  = str_replace('　', ' ', $fullName);
                    $single = str_replace(' ', '　', $fullName);
                    $searchFullName = 'CONCAT(IFNULL(mst_customer.last_name, "")," ",IFNULL(mst_customer.first_name, ""))';
                    $subWhere->whereRaw("{$searchFullName} LIKE '%{$multi}%'")
                                ->orWhereRaw("{$searchFullName} LIKE '%{$single}%'");
                });
            }
            if (isset($arraySearch['order_date_from'])) {
                $dateFrom = date("Y-m-d 00:00:00", strtotime($arraySearch['order_date_from']));
                $query->where('mst_order.order_date', '>=', $dateFrom);
            }
            if (isset($arraySearch['order_date_to'])) {
                $dateTo = date("Y-m-d 23:59:59", strtotime($arraySearch['order_date_to']));
                $query->where('mst_order.order_date', '<=', $dateTo);
            }
            if (isset($arraySearch['name_jp'])) {
                if (is_array($arraySearch['name_jp'])) {
                    $query->whereIn('mst_mall.id', $arraySearch['name_jp']);
                } else {
                    $query->where('mst_mall.id', $arraySearch['name_jp']);
                }
            }
            if (isset($arraySearch['order_status'])) {
                if (is_array($arraySearch['order_status'])) {
                    $query->whereIn('mst_order.order_status', $arraySearch['order_status']);
                } else {
                    $query->where('mst_order.order_status', $arraySearch['order_status']);
                }
            }
            if (isset($arraySearch['payment_method'])) {
                if (is_array($arraySearch['payment_method'])) {
                    $query->whereIn('mst_order.payment_method', $arraySearch['payment_method']);
                } else {
                    $query->where('mst_order.payment_method', $arraySearch['payment_method']);
                }
            }
            if (isset($arraySearch['order_sub_status'])) {
                if (is_array($arraySearch['order_sub_status'])) {
                    $query->whereIn('mst_order.order_sub_status', $arraySearch['order_sub_status']);
                } else {
                    $query->where('mst_order.order_sub_status', $arraySearch['order_sub_status']);
                }
            }
            if (isset($arraySearch['is_mall_cancel'])) {
                $query->where('mst_order.is_mall_cancel', $arraySearch['is_mall_cancel']);
            }
            if (isset($arraySearch['is_mall_update'])) {
                $query->where('mst_order.is_mall_update', $arraySearch['is_mall_update']);
            }
            if (isset($arraySearch['received_order_id'])) {
                $query->where('mst_order.received_order_id', $arraySearch['received_order_id']);
            }
            if (isset($arraySearch['is_delay'])) {
                $query->where(
                    'mst_order.is_delay',
                    ($arraySearch['is_delay'] === '1' ? '=' : '<>'),
                    1
                );
            }
            if (isset($arraySearch['delay_priod_from'])) {
                $dateFrom = date("Y-m-d 00:00:00", strtotime($arraySearch['delay_priod_from']));
                $query->where('mst_order.delay_priod', '>=', $dateFrom);
            }
            if (isset($arraySearch['delay_priod_to'])) {
                $dateTo = date("Y-m-d 23:59:59", strtotime($arraySearch['delay_priod_to']));
                $query->where('mst_order.delay_priod', '<=', $dateTo);
            }
            if (isset($arraySearch['another_order_status'])) {
                $query->where('mst_order.order_status', '<>', $arraySearch['another_order_status']);
            }
            if (isset($arraySearch['address'])) {
                $query->where(function ($subWhere) use ($arraySearch) {
                    $fullAddress = $arraySearch['address'];
                    $multi  = str_replace('　', ' ', $fullAddress);
                    $single = str_replace(' ', '　', $fullAddress);
                    $searchFullAdd = 'CONCAT(IFNULL(mst_customer.prefecture, ""),IFNULL(mst_customer.city, ""),IFNULL(mst_customer.sub_address, ""))';
                    $strQuery = "EXISTS (select 1 from mst_order_detail";
                    $strQuery .= " JOIN mst_customer ON mst_customer.customer_id = mst_order_detail.receiver_id";
                    $strQuery .= " WHERE mst_order_detail.receive_id=mst_order.receive_id";
                    $strQuery .= " AND (({$searchFullAdd} LIKE '%{$multi}%') OR ({$searchFullAdd} LIKE '%{$single}%')))";
                    $subWhere->whereRaw($strQuery);
                });
            }

            if (isset($arraySearch['request_price_from'])) {
                $query->where('mst_order.request_price', '>=', $arraySearch['request_price_from']);
            }
            if (isset($arraySearch['request_price_to'])) {
                $query->where('mst_order.request_price', '<=', $arraySearch['request_price_to']);
            }
        }
        $data = $query;
        return $data->count();
    }
    /**
     * count item order
     *
     * @return init
     */
    public function countItemsByPositionAll()
    {
        $count = $this->count();
        return $count;
    }

    /**
     * Get order api
     *
     * @param  int    $orderStatus
     * @param  int    $orderSubStatus
     * @param  int    $mallId
     * @param  int    $limit
     * @return object
     */
    public function getOrderAPI($orderStatus, $orderSubStatus, $mallId, $limit = null, $paymentMethod = null)
    {
        $data = $this->select('mst_order.received_order_id', 'mst_order.order_date');
        if ($mallId === 1) {
            $data->leftJoin('dt_web_rak_data', 'dt_web_rak_data.order_number', '=', 'mst_order.received_order_id')
                ->whereNull('dt_web_rak_data.order_number');
        } elseif ($mallId === 2) {
            $data->leftJoin('dt_web_yah_data', 'dt_web_yah_data.order_id', '=', 'mst_order.received_order_id')
                ->whereNull('dt_web_yah_data.order_id');
        } elseif ($mallId === 8) {
            $data->leftJoin('dt_web_pro_data', 'dt_web_pro_data.order_id', '=', 'mst_order.received_order_id')
                ->whereNull('dt_web_pro_data.order_id');
        } elseif ($mallId === 3) {
            $data->leftJoin('dt_web_amazon_data', 'dt_web_amazon_data.order_number', '=', 'mst_order.received_order_id')
                ->whereNull('dt_web_amazon_data.order_number');
        }
        $data->where('mst_order.order_status', $orderStatus)
            ->where('mst_order.order_sub_status', $orderSubStatus)
            ->where('mst_order.mall_id', $mallId);
        if ($paymentMethod !== null) {
            $data->where('mst_order.payment_method', $paymentMethod);
        } else {
            $data->where('mst_order.payment_method', '<>', 7);
        }
        if ($limit !== null) {
            $data->limit($limit);
        }
        return $data->get();
    }
    /**
     * Get order by receive id
     *
     * @param  int    $receiveId
     * @return object
     */
    public function getOrderByReceive($receiveId)
    {
        return $this->join('mst_order_detail', 'mst_order.receive_id', '=', 'mst_order_detail.receive_id')
                    ->where('mst_order.receive_id', $receiveId)
                    ->get();
    }

    public function getReturnCheck($receiveId)
    {
        return $this->join('mst_order_detail', 'mst_order.receive_id', '=', 'mst_order_detail.receive_id')
                    ->join('dt_order_product_detail', function ($join) {
                        $join->on('dt_order_product_detail.receive_id', '=', 'mst_order_detail.receive_id')
                            ->on('dt_order_product_detail.detail_line_num', '=', 'mst_order_detail.detail_line_num');
                    })
                    ->where('mst_order.receive_id', $receiveId)
                    ->groupBy('mst_order_detail.detail_line_num')
                    ->get();
    }

        /**
     * Get data by receive_id , for show detail order tab
     *
     * @param  array $arrQuery
     * @return object
     */
    public function getOrderDetail($arrQuery)
    {
        $subWhere = 'dt_order_to_supplier.times_num =
                    (select MAX(times_num) from dt_order_to_supplier AS sub
                        where sub.order_code = dt_order_to_supplier.order_code
                        group by sub.order_code
                    )';
        $data = $this->select('dt_order_product_detail.delivery_type')
        ->addSelect('dt_delivery.inquiry_no')
        ->addSelect('dt_delivery.delivery_instrustions')
        ->addSelect('dt_delivery.delivery_plan_date')
        ->addSelect('mst_order_detail.receive_id')
        ->addSelect('mst_order_detail.price')
        ->addSelect('mst_order_detail.quantity')
        ->addSelect('mst_order_detail.specify_deli_type')
        ->addSelect('dt_order_product_detail.product_status')
        ->addSelect('dt_order_product_detail.suplier_id')
        ->addSelect('mst_order.ship_wish_date')
        ->addSelect('mst_order.ship_wish_time')
        ->addSelect('dt_delivery.delivery_code')
        ->addSelect('mst_shipping_company.company_name')
        ->addSelect(DB::raw('(mst_order_detail.price * mst_order_detail.quantity) AS sub_total'))
        ->addSelect('mst_order_detail.product_code')
        ->addSelect('mst_order_detail.product_name')
        ->addSelect('mst_order_detail.receiver_id')
        ->addSelect('mst_order_detail.claim_id')
        ->addSelect(
            DB::raw(
                'CONCAT(IFNULL(receiver_cus.last_name, "")," ",IFNULL(receiver_cus.first_name, ""))
                AS receiver_full_name'
            )
        )
        ->addSelect('receiver_cus.prefecture AS receiver_prefecture')
        ->addSelect('receiver_cus.city AS receiver_city')
        ->addSelect('receiver_cus.sub_address AS receiver_sub_address')
        ->addSelect('receiver_cus.zip_code AS receiver_zip_code')
        ->addSelect('receiver_cus.tel_num AS receiver_tel_num')
        ->addSelect('dt_order_product_detail.child_product_code')
        ->addSelect('dt_order_product_detail.received_order_num')
        ->addSelect('mst_order_detail.detail_line_num')
        ->addSelect('child_pro.product_name_long AS child_product_name')
        ->addSelect('parrent.price_supplier_id AS parrent_supplier_id')
        ->addSelect('child.price_supplier_id AS child_supplier_id')
        ->addSelect('dt_order_product_detail.sub_line_num')
        ->addSelect('dt_return.return_no')
        ->addSelect('dt_order_to_supplier.edi_order_code')
        ->addSelect('dt_order_to_supplier.order_code')
        ->addSelect('dt_order_to_supplier.order_type')
        ->join('mst_order_detail', function ($join) {
            $join->on('mst_order.receive_id', '=', 'mst_order_detail.receive_id');
        })
        ->join('mst_customer AS receiver_cus', function ($join) {
            $join->on('mst_order_detail.receiver_id', '=', 'receiver_cus.customer_id');
        })
        ->leftjoin('dt_order_product_detail', function ($join) {
            $join->on('mst_order_detail.receive_id', '=', 'dt_order_product_detail.receive_id')
                ->on('mst_order_detail.detail_line_num', '=', 'dt_order_product_detail.detail_line_num');
        })
        ->leftJoin('mst_product_supplier AS parrent', function ($join) {
            $join->on('mst_order_detail.product_code', '=', 'parrent.product_code');
        })
        ->leftJoin('mst_product_supplier AS child', function ($join) {
            $join->on('dt_order_product_detail.child_product_code', '=', 'child.product_code');
        })
        ->leftJoin('mst_product AS child_pro', function ($join) {
            $join->on('dt_order_product_detail.child_product_code', '=', 'child_pro.product_code');
        })
        ->leftJoin('dt_delivery_detail', function ($join) {
            $join->on('dt_delivery_detail.detail_line_num', '=', 'dt_order_product_detail.sub_line_num')
                ->on('dt_delivery_detail.received_order_id', '=', 'mst_order.received_order_id');
        })
        ->leftJoin('dt_delivery', function ($join) {
            $join->on('dt_delivery.subdivision_num', '=', 'dt_delivery_detail.subdivision_num')
                ->on('dt_delivery.received_order_id', '=', 'dt_delivery_detail.received_order_id');
        })
        ->leftJoin(
            'mst_shipping_company',
            'mst_shipping_company.company_id',
            '=',
            'dt_delivery.delivery_code'
        )
        ->leftJoin('dt_return', function ($join) {
            $join->on('dt_return.receive_id', '=', 'dt_order_product_detail.receive_id')
                ->on('dt_return.product_code', '=', 'dt_order_product_detail.product_code');
        })
        ->leftJoin('dt_order_to_supplier', function ($join) use ($subWhere) {
            $join->on('dt_order_product_detail.order_code', '=', 'dt_order_to_supplier.order_code')
            ->whereRaw($subWhere);
        })
        ->where('mst_order.receive_id', '=', $arrQuery['receive_id'])
        ->whereRaw('IF(dt_delivery.detail_line_num <> 0, dt_delivery.detail_line_num = dt_delivery_detail.detail_line_num , 1 = 1)')
        ->orderBy('mst_order_detail.detail_line_num', 'asc');
        return $data->get();
    }
    /**
     * Get data re-calculate price
     *
     * @param  int $receiveId
     * @return object
     */
    public function getDataReCalculate($receiveId)
    {
        $col = [
            'msf.below_limit_price',
            'msf.charge_price',
            'msf.end_date',
            'msf.start_date',
            'pos.add_ship_charge',
            'mst_order.pay_charge_discount',
            'mst_order.pay_after_charge',
            'mst_order.pay_charge',
            'mst_order.used_point',
            'mst_order.used_coupon',
            'mst_order.discount',
            'mst_order.mall_id',
            'mst_order.goods_price',
            'mst_order.payment_method',
            'mst_order.order_date',
            'mst_order.total_price',
            'cod.max_amount',
            'cod.min_amount',
            'cod.cod_fee',
            'mod.receiver_id',
            'mod.price',
            'mod.quantity',
            'mod.ship_charge_detail'
        ];
        return $this->select($col)
                    ->join('mst_order_detail AS mod', 'mod.receive_id', '=', 'mst_order.receive_id')
                    ->join('mst_customer AS cus', 'cus.customer_id', '=', 'mod.receiver_id')
                    ->join('mst_postal AS pos', 'cus.zip_code', '=', 'pos.postal_code')
                    ->join('mst_shipping_fee AS msf', 'mst_order.mall_id', '=', 'msf.mall_id')
                    ->join('mst_cod_fee AS cod', 'mst_order.mall_id', '=', 'cod.mall_id')
                    ->where('mst_order.receive_id', '=', $receiveId)
                    ->whereRaw('DATE(mst_order.order_date) >= DATE(msf.start_date)')
                    ->whereRaw('DATE(mst_order.order_date) <= DATE(msf.end_date)')
                    ->whereColumn('mst_order.goods_price', '>=', 'cod.min_amount')
                    ->whereColumn('mst_order.goods_price', '<=', 'cod.max_amount')
                    ->where('msf.is_delete', 0)
                    ->where('cod.is_delete', 0)
                    ->groupBY('mod.receive_id', 'mod.detail_line_num')
                    ->orderBy('msf.start_date')
                    ->get();
    }
    /**
     * Get total price order
     *
     * @param  int $receiveId
     * @return object
     */
    public function getTotalPriceOrder($receiveId)
    {
        $total = 0;
        $result = $this->select(['mst_order_detail.detail_line_num'])
                        ->selectRaw('SUM(mst_order_detail.price * mst_order_detail.quantity) AS price_product')
                       ->join('mst_order_detail', 'mst_order_detail.receive_id', '=', 'mst_order.receive_id')
                       ->leftjoin('dt_order_product_detail', function ($join) {
                          $join->on('dt_order_product_detail.receive_id', '=', 'mst_order_detail.receive_id')
                              ->on('dt_order_product_detail.detail_line_num', '=', 'mst_order_detail.detail_line_num');
                       })
                       ->where('dt_order_product_detail.product_status', '<>', PRODUCT_STATUS['CANCEL'])
                       ->where('mst_order.receive_id', '=', $receiveId)
                       ->groupBy('dt_order_product_detail.sub_line_num')
                       ->distinct()
                       ->get();
        if (count($result) !== 0) {
            foreach ($result as $item) {
                $total += $item->price_product;
            }
        } else {
            return false;
        }
        return $total;
    }
    /**
     * Get cod fee by receive_id
     *
     * @param  int $receiveId
     * @return object
     */
    public function getCodFee($receiveId)
    {
        $result = $this->select('cod_fee')
                       ->join('mst_cod_fee', 'mst_order.mall_id', '=', 'mst_cod_fee.mall_id')
                       ->whereRaw('mst_order.goods_price BETWEEN mst_cod_fee.min_amount and mst_cod_fee.max_amount')
                       ->whereRaw('mst_order.order_date BETWEEN mst_cod_fee.start_date and mst_cod_fee.end_date')
                       ->where('mst_order.receive_id', $receiveId)
                       ->first();
        return $result;
    }
    /**
     * Get info customer of order
     *
     * @param  int $receiveId
     * @return object
     */
    public function getDataCustomerByReceive($receiveId)
    {
        $col = [
            'mco.first_name as order_firstname',
            'mco.last_name as order_lastname',
            'mco.tel_num as order_tel',
            'mco.zip_code as order_zip_code',
            'mco.prefecture as order_prefecture',
            'mco.city as order_city',
            'mco.sub_address as order_sub_address',
            'mcod.first_name as ship_to_first_name',
            'mcod.last_name as ship_to_last_name',
            'mcod.tel_num as ship_tel_num',
            'mcod.zip_code as ship_zip_code',
            'mcod.prefecture as ship_prefecture',
            'mcod.city as ship_city',
            'mcod.sub_address as ship_sub_address',
        ];
        $result = $this->select($col)
                       ->join('mst_order_detail as od', 'mst_order.receive_id', '=', 'od.receive_id')
                       ->join('mst_customer as mco', 'mst_order.customer_id', '=', 'mco.customer_id')
                       ->join('mst_customer as mcod', 'od.receiver_id', '=', 'mcod.customer_id')
                       ->where('mst_order.receive_id', $receiveId)
                       ->first();
        return $result;
    }

    /**
     * Get receipt by mail seri
     *
     * @param  string $mailSerial
     * @return object
     */
    public function getReceipt($mailSerial)
    {
        $data = $this->select(
            'o.receive_id',
            'o.received_order_id',
            'o.order_date',
            'o.shipment_date',
            'o.goods_price',
            'o.ship_charge',
            'o.pay_charge',
            'o.total_price',
            'o.pay_charge_discount',
            'o.used_coupon',
            'o.used_point',
            'o.discount',
            'o.request_price',
            'o.pdf_status',
            'o.receipt_date',
            'c.customer_id',
            'c.last_name',
            'c.first_name',
            'c.full_name',
            'o.proviso',
            'od.detail_line_num',
            'od.product_code',
            'od.product_name',
            'od.price',
            'od.quantity'
        )
        ->from('mst_order AS o')
        ->join('mst_customer AS c', 'o.customer_id', '=', 'c.customer_id')
        ->join('mst_order_detail AS od', 'o.receive_id', '=', 'od.receive_id')
        ->join('dt_order_product_detail AS opd', function ($join) {
            $join->on('od.receive_id', '=', 'opd.receive_id')
                ->on('od.detail_line_num', '=', 'opd.detail_line_num')
                ->where('opd.product_status', '<>', PRODUCT_STATUS['CANCEL']);
        })
        ->where('mail_seri', $mailSerial)
        ->groupBy('o.receive_id', 'od.detail_line_num')
        ->get();
        return $data;
    }

    /**
     * Get order detail bat mail queue
     * @return string $receivedOrderId
     * @return string $para
     */
    public function getOrderDetailMailQueue($receivedOrderId, $para = '')
    {
        $col = [
            'mst_order.received_order_id',
            'mcod.last_name as ship_last_name',
            'mcod.first_name as ship_first_name',
            'mcod.zip_code as ship_zip_code',
            'mcod.prefecture as ship_prefecture',
            'mcod.city as ship_city',
            'mcod.sub_address as ship_sub_address',
            'mcod.tel_num as ship_tel_num',
            'od.product_code',
            'od.product_name',
            // 'od.price',
            // 'od.quantity',
            'dto.delivery_type',
            'dto.suplier_id'
        ];
        $subPrice = 'CASE WHEN dto.product_status = 15 THEN 0 ELSE od.price END as price';
        $subQuantity = 'CASE WHEN dto.product_status = 15 THEN 0 ELSE od.quantity END as quantity';
        $query = $this->select($col)
                       ->selectRaw($subPrice)
                       ->selectRaw($subQuantity)
                       ->join('mst_order_detail as od', 'mst_order.receive_id', '=', 'od.receive_id')
                       ->join('mst_customer as mcod', 'od.receiver_id', '=', 'mcod.customer_id')
                       ->leftjoin('dt_order_product_detail as dto', function ($join) {
                          $join->on('dto.receive_id', '=', 'od.receive_id')
                              ->on('dto.detail_line_num', '=', 'od.detail_line_num');
                       })
                       ->where('mst_order.received_order_id', '=', $receivedOrderId);
        if ($para === '[###送付先情報欠品・廃番]') {
            $query->addSelect('mst_product_status.status_name')
                   ->leftJoin('mst_product_status', 'dto.product_status', '=', 'mst_product_status.product_status_id')
                   ->groupBy('od.detail_line_num');
        }
        if ($para === '[###送付先情報発送日案内]') {
            $query->addSelect('opd.delivery_date')
                  ->leftjoin('dt_delivery', function ($join) {
                      $join->on('mst_order.received_order_id', '=', 'dt_delivery.received_order_id')
                          ->on('dto.sub_line_num', '=', 'dt_delivery.detail_line_num');
                  })
                  ->join('dt_order_product_detail as opd', function ($join) {
                     $join->on('opd.receive_id', '=', 'od.receive_id')
                         ->on('opd.detail_line_num', '=', 'od.detail_line_num');
                  })
                  ->orderBy('opd.delivery_date')
                  ->groupBy('od.detail_line_num');
        }
        $result = $query->get();
        return $result;
    }

    /**
    * The get information order detail process content mail in case product not exist in dt_order_product_detail.
    * @param string $receiveOrderId order_id of order want sent mail
    * @return object $result
    */
    public function getInfoNotInOrderDetail($receivedOrderId)
    {
        $col = [
            'mst_order.received_order_id',
            'mcod.last_name as ship_last_name',
            'mcod.first_name as ship_first_name',
            'mcod.zip_code as ship_zip_code',
            'mcod.prefecture as ship_prefecture',
            'mcod.city as ship_city',
            'mcod.sub_address as ship_sub_address',
            'mcod.tel_num as ship_tel_num',
            'od.product_code',
            'od.product_name',
            // 'od.price',
            // 'od.quantity'
        ];
        $subPrice = 'CASE WHEN dto.product_status = 15 THEN 0 ELSE od.price END as price';
        $subQuantity = 'CASE WHEN dto.product_status = 15 THEN 0 ELSE od.quantity END as quantity';
        $result = $this->select($col)
                       ->selectRaw($subPrice)
                       ->selectRaw($subQuantity)
                       ->join('mst_order_detail as od', 'mst_order.receive_id', '=', 'od.receive_id')
                       ->join('mst_customer as mcod', 'od.receiver_id', '=', 'mcod.customer_id')
                       ->leftJoin('dt_order_product_detail as dto', function ($join) {
                          $join->on('dto.receive_id', '=', 'od.receive_id')
                              ->on('dto.detail_line_num', '=', 'od.detail_line_num');
                       })
                       ->where('mst_order.received_order_id', '=', $receivedOrderId)
                       ->groupBy('od.detail_line_num')
                       ->get();
        return $result;
    }

    /**
     * Get data not payment list
     *
     * @param  array   $arraySearch
     * @param  array   $arraySort
     * @return object
     */
    public function getDataNotPaymentList($arraySearch = null, $arraySort = null)
    {
        $fullName     = 'CONCAT(IFNULL(c.last_name, ""),"　",IFNULL(c.first_name, ""))';
        $fullNameKana = 'CONCAT(IFNULL(c.last_name_kana, ""),"　",IFNULL(c.first_name_kana, ""))';
        $data = $this->select(
            'o.order_date',
            'o.received_order_id',
            'o.payment_account',
            'o.request_price',
            'o.mall_id',
            'm.name_jp',
            'sm.payment_name',
            DB::raw($fullName . ' AS full_name'),
            DB::raw($fullNameKana . ' AS full_name_kana')
        )
        ->from('mst_order AS o')
        ->join('mst_customer AS c', 'o.customer_id', '=', 'c.customer_id')
        ->join('mst_mall AS m', 'o.mall_id', '=', 'm.id')
        ->join('mst_settlement_manage AS sm', 'o.payment_method', '=', 'sm.payment_code')
        ->whereIn('o.payment_method', [2, 6])
        ->where('o.payment_status', 0)
        ->where('o.payment_account', '<>', '');

        if (count($arraySearch) > 0) {
            if (isset($arraySearch['mall_id'])) {
                if (is_array($arraySearch['mall_id'])) {
                    $data->whereIn('o.mall_id', $arraySearch['mall_id']);
                } else {
                    $data->where('o.mall_id', $arraySearch['mall_id']);
                }
            }
            if (isset($arraySearch['order_date_from'])) {
                $data->where('o.order_date', '>=', date('Y-m-d 00:00:00', strtotime($arraySearch['order_date_from'])));
            }
            if (isset($arraySearch['order_date_to'])) {
                $data->where('o.order_date', '<=', date('Y-m-d 23:59:59', strtotime($arraySearch['order_date_to'])));
            }
            if (isset($arraySearch['received_order_id'])) {
                $data->where('o.received_order_id', 'LIKE', "%{$arraySearch['received_order_id']}%");
            }
            if (isset($arraySearch['full_name'])) {
                $data->where(DB::raw($fullName), 'LIKE', "%{$arraySearch['full_name']}%");
            }
            if (isset($arraySearch['full_name_kana'])) {
                $data->where(DB::raw($fullNameKana), 'LIKE', "%{$arraySearch['full_name_kana']}%");
            }
            if (isset($arraySearch['payment_account'])) {
                $data->where('o.payment_account', $arraySearch['payment_account']);
            }
            if (isset($arraySearch['request_price'])) {
                $data->where('o.request_price', $arraySearch['request_price']);
            }
        }
        if ($arraySort !== null && count($arraySort) > 0) {
            foreach ($arraySort as $column => $sort) {
                if (in_array($sort, ['asc', 'desc'])) {
                    $data->orderBy($column, $sort);
                }
            }
        }
        $data->orderBy('o.order_date', 'desc');

        $perPage = ($arraySearch['per_page']) ? $arraySearch['per_page'] : 20;
        $data = $data->paginate($perPage);
        return $data;
    }
    /**
     * Get data mapping
     *
     * @param  array   $arrKey
     * @return object
     */
    public function getDataMapping($arrKey)
    {
        $col = [
            'mst_order.received_order_id',
            'mst_order.mall_id',
            'mst_order.order_status',
            'mst_order.payment_method',
            'mst_order.request_price AS order_request_price',
        ];
        $result = $this->select($col)
                       ->whereIn('mst_order.received_order_id', $arrKey)
                       ->groupBy('mst_order.received_order_id')
                       ->get();
        return $result;
    }

    /**
     * Get data check error
     * @return object $result
     */
    public function getDataCheckError($receiveId, $receiverId = null)
    {
        $col = [
            'mst_order.receive_id',
            'mst_order.is_ignore_error',
            'mco.first_name as order_firstname',
            'mco.last_name as order_lastname',
            'mco.tel_num as order_tel',
            'mco.zip_code as order_zip_code',
            'mco.prefecture as order_prefecture',
            'mco.city as order_city',
            'mco.sub_address as order_sub_address',
            'total_price',
            'order_date',
            'request_price',
            'payment_method',
            'emp.product_code',
            'price',
            'quantity',
            'mall_id',
            'mcod.first_name as ship_to_first_name',
            'mcod.last_name as ship_to_last_name',
            'mcod.tel_num as ship_tel_num',
            'mcod.zip_code as ship_zip_code',
            'mcod.prefecture as ship_prefecture',
            'mcod.city as ship_city',
            'mcod.sub_address as ship_sub_address',
            'dto.delivery_type',
        ];
        $result = $this->select($col)
                       ->join('mst_order_detail as od', 'mst_order.receive_id', '=', 'od.receive_id')
                       ->join('mst_customer as mco', 'mst_order.customer_id', '=', 'mco.customer_id')
                       ->join('mst_customer as mcod', 'od.receiver_id', '=', 'mcod.customer_id')
                       ->leftjoin('edi.mst_product as emp', function ($join) {
                            $join->on('od.product_code', '=', 'emp.product_code')
                            ->where('emp.is_live', '=', 1);
                        })
                       ->leftJoin('dt_order_product_detail as dto', function ($join) {
                          $join->on('dto.receive_id', '=', 'od.receive_id')
                              ->on('dto.detail_line_num', '=', 'od.detail_line_num');
                       })
                       ->where('mst_order.receive_id', $receiveId);
        if ($receiverId !== null) {
            $result->where('od.receiver_id', $receiverId);
        }
        $result = $result->where(function ($query) {
             $query->whereNull('product_status')
                   ->orWhere('product_status', '<>', PRODUCT_STATUS['CANCEL']);
        })
        ->groupBy('od.detail_line_num')
        ->first();
        return $result;
    }

    /**
     * Get deli days
     * @return object $result
     */
    public function getDeliDays($where)
    {
        $result = $this->select('deli_days')
                       ->join('mst_order_detail', 'mst_order_detail.receive_id', '=', 'mst_order.receive_id')
                       ->join('mst_customer', 'mst_customer.customer_id', '=', 'mst_order_detail.receiver_id')
                       ->join('mst_postal', 'mst_customer.zip_code', '=', 'mst_postal.postal_code')
                       ->where($where)
                       ->first();
        return $result;
    }

    /**
     * Get data process mapping b-dash
     * @return object $result
     */
    public function getDataProMapping($limit)
    {
        $result = $this->select('received_order_id')
                       ->where('order_status', ORDER_STATUS['SETTLEMENT'])
                       ->where('order_sub_status', ORDER_SUB_STATUS['DOING'])
                       ->where('mall_id', 8)
                       ->limit($limit)
                       ->get();
        return $result;
    }

    /**
     * Get data check delivery
     *
     * @return object $result
     */
    public function getCheckDeliveryExists($receiveId, $receiverId)
    {
        $result = $this->select(['dd.received_order_id', 'dd.subdivision_num', 'dd.delivery_code'])
                       ->join('mst_order_detail AS mod', 'mst_order.receive_id', '=', 'mod.receive_id')
                       ->join('dt_order_product_detail AS opd', function ($sub) {
                            $sub->on('opd.receive_id', '=', 'mod.receive_id')
                                ->on('opd.detail_line_num', '=', 'mod.detail_line_num');
                       })
                       ->join('dt_delivery_detail AS ddd', function ($sub) {
                            $sub->on('ddd.received_order_id', '=', 'mst_order.received_order_id')
                                ->on('opd.sub_line_num', '=', 'ddd.detail_line_num');
                       })
                       ->join('dt_delivery AS dd', function ($sub) {
                            $sub->on('ddd.received_order_id', '=', 'dd.received_order_id')
                                ->on('dd.subdivision_num', '=', 'ddd.subdivision_num');
                       })
                       ->where('mod.receiver_id', $receiverId)
                       ->where('mst_order.receive_id', $receiveId)
                       ->groupBy('dd.received_order_id', 'dd.subdivision_num')
                       ->first();
        return $result;
    }

    /**
     * Get data parse deje content
     * @param  string $orderCode
     * @param  string $productCode
     * @return object
     */
    public function getDataParseContent5($receivedOrderId, $productCode)
    {
        $col = [
            'mst_order.received_order_id',
            'dt_order_product_detail.received_order_num',
            'mst_product.product_jan',
            'mst_product.product_name_long',
            'dd.delivery_name',
        ];
        $pro = "IFNULL(dt_order_product_detail.child_product_code, dt_order_product_detail.product_code) AS product_code";
        $result = $this->select($col)
                       ->selectRaw($pro)
                       ->leftJoin('mst_order_detail', 'mst_order_detail.receive_id', '=', 'mst_order.receive_id')
                       ->leftJoin('dt_order_product_detail', function ($join) {
                            $join->on('mst_order_detail.receive_id', '=', 'dt_order_product_detail.receive_id')
                                ->on('mst_order_detail.detail_line_num', '=', 'dt_order_product_detail.detail_line_num');
                       })
                       ->leftJoin('dt_delivery AS dd', function ($sub) {
                            $sub->on('dd.received_order_id', '=', 'mst_order.received_order_id');
                       })
                       ->leftJoin('mst_product', DB::raw('IFNULL(dt_order_product_detail.child_product_code, dt_order_product_detail.product_code)'), '=', 'mst_product.product_code');

        if (!empty($receivedOrderId)) {
            $result->whereIn('mst_order.received_order_id', $receivedOrderId);
        }
        if (!empty($productCode)) {
            $result->where(function ($query) use ($productCode) {
                $query->whereIn('dt_order_product_detail.product_code', $productCode)
                      ->orWhereIn('mst_product.product_jan', $productCode);
            });
        }
        return $result->get();
    }

    /**
     * Get data parse deje content
     * @param  string $orderCode
     * @param  string $productCode
     * @return object
     */
    public function getDataParseContent6($param, $receivedOrderId, $productCode)
    {
        $col = [
            'mst_order.received_order_id',
            'dt_delivery.inquiry_no',
            'dt_delivery.delivery_name',
            'dt_delivery.delivery_tel',
            'dt_delivery_detail.delivery_real_date',
            'dt_delivery_detail.product_code',
            'dt_delivery_detail.product_name',
            'dt_delivery_detail.delivery_num',
        ];
        $fullAddress = 'CONCAT("〒",IFNULL(dt_delivery.delivery_postal, ""),';
        $fullAddress .= 'IFNULL(dt_delivery.delivery_perfecture, ""), ';
        $fullAddress .= 'IFNULL(dt_delivery.delivery_city, ""), ';
        $fullAddress .= 'IFNULL(dt_delivery.delivery_sub_address, "")) AS full_address';
        $result = $this->select($col)
                       ->selectRaw($fullAddress)
                       ->leftJoin('dt_delivery', function ($join) {
                            $join->on('dt_delivery.received_order_id', '=', 'mst_order.received_order_id');
                       })
                       ->leftJoin('dt_delivery_detail', function ($join) {
                            $join->on('dt_delivery_detail.received_order_id', '=', 'dt_delivery.received_order_id')
                                ->on('dt_delivery_detail.subdivision_num', '=', 'dt_delivery.subdivision_num');
                       });
        if (!empty($receivedOrderId)) {
            $result->whereIn('mst_order.received_order_id', $receivedOrderId);
        }
        if ($param === '12' && !empty($productCode)) {
            $result->where(function ($query) use ($productCode) {
                $query->whereIn('dt_delivery_detail.product_code', $productCode)
                      ->orWhereIn('dt_delivery_detail.product_jan', $productCode);
            });
        }
        return $result->get();
    }


    /**
     * Get data parse deje content
     * @param  string $orderCode
     * @param  string $productCode
     * @return object
     */
    public function getDataParseContent9($param, $receivedOrderId, $productCode)
    {
        $col = [
            'mst_order.received_order_id',
            'dt_delivery.inquiry_no',
            'dt_delivery.delivery_name',
            'dt_delivery.delivery_tel',
            'dt_delivery_detail.delivery_real_date',
            'dt_delivery_detail.product_code',
            'dt_delivery_detail.product_name',
            'dt_delivery_detail.delivery_num',
        ];
        $fullAddress = 'CONCAT("〒",IFNULL(dt_delivery.delivery_postal, ""),';
        $fullAddress .= 'IFNULL(dt_delivery.delivery_perfecture, ""), ';
        $fullAddress .= 'IFNULL(dt_delivery.delivery_city, ""), ';
        $fullAddress .= 'IFNULL(dt_delivery.delivery_sub_address, "")) AS full_address';
        $result = $this->select($col)
                       ->selectRaw($fullAddress)
                       ->join('dt_delivery', function ($join) {
                            $join->on('dt_delivery.received_order_id', '=', 'mst_order.received_order_id');
                       })
                       ->join('dt_delivery_detail', function ($join) {
                            $join->on('dt_delivery_detail.received_order_id', '=', 'dt_delivery.received_order_id')
                                ->on('dt_delivery_detail.subdivision_num', '=', 'dt_delivery.subdivision_num');
                       });
        if (!empty($receivedOrderId)) {
            $result->whereIn('mst_order.received_order_id', $receivedOrderId);
        }
        return $result->get();
    }

    /**
     * Get data parse deje content
     * @param  string $orderCode
     * @param  string $productCode
     * @return object
     */
    public function getDataParseContent8($receivedOrderId, $productCode)
    {
        $col = [
            'mst_order.received_order_id',
            'dt_order_product_detail.received_order_num',
            'mst_product.product_jan',
            'mst_product.product_name_long',
        ];
        $fullName = 'CONCAT(IFNULL(mst_customer.last_name, ""),IFNULL(mst_customer.first_name, "")) AS full_name';
        $pro = "IFNULL(dt_order_product_detail.child_product_code, dt_order_product_detail.product_code) AS product_code";
        $result = $this->select($col)
                       ->selectRaw($fullName)
                       ->selectRaw($pro)
                       ->leftJoin('mst_order_detail', 'mst_order_detail.receive_id', '=', 'mst_order.receive_id')
                       ->leftJoin('dt_order_product_detail', function ($join) {
                            $join->on('mst_order_detail.receive_id', '=', 'dt_order_product_detail.receive_id')
                                ->on('mst_order_detail.detail_line_num', '=', 'dt_order_product_detail.detail_line_num');
                       })
                       ->leftJoin('mst_product', DB::raw('IFNULL(dt_order_product_detail.child_product_code, dt_order_product_detail.product_code)'), '=', 'mst_product.product_code')
                       ->leftJoin('mst_customer', function ($join) {
                            $join->on('mst_order.customer_id', '=', 'mst_customer.customer_id');
                       });
        if (!empty($receivedOrderId)) {
            $result->whereIn('mst_order.received_order_id', $receivedOrderId);
        }
        if (!empty($productCode)) {
            $result->whereIn('dt_order_product_detail.product_code', $productCode);
        }
        return $result->get();
    }

    /**
     * Get position row not condition
     *
     * @param  int    $receiveIdKey
     * @param  array  $arraySearch
     * @param  array  $arraySort
     * @return object
     */
    public function getPositionRowNotCodition($receiveIdKey, $arraySearch = null, $arraySort = null)
    {
        $query = $this->selectRaw('COUNT(*)+1 AS position')
            ->from('mst_order')
            ->leftJoin('mst_settlement_manage', 'mst_settlement_manage.payment_code', '=', 'mst_order.payment_method')
            ->leftJoin('mst_order_status', 'mst_order.order_status', '=', 'mst_order_status.order_status_id')
            ->leftJoin('mst_customer', 'mst_order.customer_id', '=', 'mst_customer.customer_id')
            ->leftJoin('mst_mall', 'mst_mall.id', '=', 'mst_order.mall_id')
            ->where('receive_id', '>', $receiveIdKey);
        if (count($arraySearch) > 0) {
            if (isset($arraySearch['receive_id'])) {
                $query->where('mst_order.receive_id', '=', $arraySearch['receive_id']);
            }
            if (isset($arraySearch['full_name'])) {
                $query->where(function ($subWhere) use ($arraySearch) {
                    $fullName = $arraySearch['full_name'];
                    $multi  = str_replace('　', ' ', $fullName);
                    $single = str_replace(' ', '　', $fullName);
                    $searchFullName = 'CONCAT(IFNULL(mst_customer.last_name, "")," ",IFNULL(mst_customer.first_name, ""))';
                    $subWhere->whereRaw("{$searchFullName} LIKE '%{$multi}%'")
                                ->orWhereRaw("{$searchFullName} LIKE '%{$single}%'");
                });
            }
            if (isset($arraySearch['product_code'])) {
                $query->where(function ($subWhere) use ($arraySearch) {
                    $subWhere->whereRaw("EXISTS (select 1 from"
                            . " mst_order_detail where mst_order_detail.receive_id=mst_order.receive_id"
                            . " and product_code LIKE '%{$arraySearch['product_code']}%' limit 1)")
                      ->orWhereRaw(" exists ("
                              . " SELECT 1 FROM dt_order_product_detail, mst_order_detail"
                              . " where dt_order_product_detail.receive_id = mst_order_detail.receive_id  "
                              . " and mst_order.receive_id = dt_order_product_detail.receive_id "
                              . " and dt_order_product_detail.detail_line_num =  mst_order_detail.detail_line_num "
                              . " and  child_product_code LIKE '%{$arraySearch['product_code']}%' limit 1)");
                });
            }
            if (isset($arraySearch['order_date_from'])) {
                $dateFrom = date("Y-m-d 00:00:00", strtotime($arraySearch['order_date_from']));
                $query->where('mst_order.order_date', '>=', $dateFrom);
            }
            if (isset($arraySearch['order_date_to'])) {
                $dateTo = date("Y-m-d 23:59:59", strtotime($arraySearch['order_date_to']));
                $query->where('mst_order.order_date', '<=', $dateTo);
            }
            if (isset($arraySearch['name_jp'])) {
                if (is_array($arraySearch['name_jp'])) {
                    $query->whereIn('mst_mall.id', $arraySearch['name_jp']);
                } else {
                    $query->where('mst_mall.id', $arraySearch['name_jp']);
                }
            }
            if (isset($arraySearch['order_status'])) {
                if (is_array($arraySearch['order_status'])) {
                    $query->whereIn('mst_order.order_status', $arraySearch['order_status']);
                } else {
                    $query->where('mst_order.order_status', $arraySearch['order_status']);
                }
            }
            if (isset($arraySearch['payment_method'])) {
                if (is_array($arraySearch['payment_method'])) {
                    $query->whereIn('mst_order.payment_method', $arraySearch['payment_method']);
                } else {
                    $query->where('mst_order.payment_method', $arraySearch['payment_method']);
                }
            }
            if (isset($arraySearch['order_sub_status'])) {
                if (is_array($arraySearch['order_sub_status'])) {
                    $query->whereIn('mst_order.order_sub_status', $arraySearch['order_sub_status']);
                } else {
                    $query->where('mst_order.order_sub_status', $arraySearch['order_sub_status']);
                }
            }
            if (isset($arraySearch['is_mall_cancel'])) {
                $query->where('mst_order.is_mall_cancel', $arraySearch['is_mall_cancel']);
            }
            if (isset($arraySearch['is_mall_update'])) {
                $query->where('mst_order.is_mall_update', $arraySearch['is_mall_update']);
            }
            if (isset($arraySearch['received_order_id'])) {
                $query->where('mst_order.received_order_id', $arraySearch['received_order_id']);
            }
            if (isset($arraySearch['another_order_status'])) {
                $query->where('mst_order.order_status', '<>', $arraySearch['another_order_status']);
            }
            if (isset($arraySearch['is_delay'])) {
                $query->where(
                    'mst_order.is_delay',
                    ($arraySearch['is_delay'] === '1' ? '=' : '<>'),
                    1
                );
            }
            if (isset($arraySearch['delay_priod_from'])) {
                $dateFrom = date("Y-m-d 00:00:00", strtotime($arraySearch['delay_priod_from']));
                $query->where('mst_order.delay_priod', '>=', $dateFrom);
            }
            if (isset($arraySearch['delay_priod_to'])) {
                $dateTo = date("Y-m-d 23:59:59", strtotime($arraySearch['delay_priod_to']));
                $query->where('mst_order.delay_priod', '<=', $dateTo);
            }
            if (isset($arraySearch['address'])) {
                $query->where(function ($subWhere) use ($arraySearch) {

                    $fullAddress = $arraySearch['address'];
                    $multi  = str_replace('　', ' ', $fullAddress);
                    $single = str_replace(' ', '　', $fullAddress);
                    $searchFullAdd = 'CONCAT(IFNULL(mst_customer.prefecture, ""),IFNULL(mst_customer.city, ""),IFNULL(mst_customer.sub_address, ""))';
                    $strQuery = "EXISTS (select 1 from mst_order_detail";
                    $strQuery .= " JOIN mst_customer ON mst_customer.customer_id = mst_order_detail.receiver_id";
                    $strQuery .= " WHERE mst_order_detail.receive_id=mst_order.receive_id";
                    $strQuery .= " AND (({$searchFullAdd} LIKE '%{$multi}%') OR ({$searchFullAdd} LIKE '%{$single}%')))";
                    $subWhere->whereRaw($strQuery);
                });
            }

            if (isset($arraySearch['request_price_from'])) {
                $query->where('mst_order.request_price', '>=', $arraySearch['request_price_from']);
            }
            if (isset($arraySearch['request_price_to'])) {
                $query->where('mst_order.request_price', '<=', $arraySearch['request_price_to']);
            }
        }
        $query->orderBy('receive_id', 'desc');
        return $query->first();
    }

}
