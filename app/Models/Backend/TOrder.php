<?php

/**
 * Model for t_order table.
 *
 * @package    App\Models\Backend
 * @subpackage TOrder
 * @copyright  Copyright (c) 2018 RiverCrane! Corporation. All Rights Reserved.
 * @author     van.qui<van.qui.rcvn2012@gmail.com>
 * @author     le.hung<le.hung.rcvn2012@gmail.com>
 */

namespace App\Models\Backend;

use Illuminate\Database\Eloquent\Model;

class TOrder extends Model
{
    public $timestamps = false;

    /**
    * The database table used by the model.
    * @var string
    */
    protected $table = 't_order';

    /**
    * Set default primary key.
    * @var string
    */
    protected $primaryKey = 't_order_id';

    /**
    * The database is used by the model.
    * @var string
    */
    protected $connection = 'edi';

    /**
     * Update data
     * @author le.hung
     * @param  array condition $where
     * @param  array update $data
     * @return boolean
     */
    public function updateData($where, $data)
    {
        $result = $this->where($where)
                    ->update($data);
        return $result;
    }

    /**
     * Get data
     * @author lam.vinh
     * @param  string orderCode
     * @return object
     */
    public function getDataNouhin($orderCode)
    {
        $col = [
            't_order.order_code',
            't_order.item_code',
            't_order.maker_name',
            't_order.suppliercd',
            't_order.jan_code',
            't_order.maker_code',
            't_order.name',
            't_order_detail.requestbno',
            't_order_detail.quantity',
            't_order_detail.stock_quantity',
            't_nouhin.no',
            't_admin.name as t_admin_name'
        ];
        $result = $this->select($col)
                    ->join('t_order_detail', 't_order.t_order_id', '=', 't_order_detail.t_order_id')
                    ->join('t_nouhin', 't_order_detail.t_nouhin_id', '=', 't_nouhin.t_nouhin_id')
                    ->join('t_admin', 't_order.suppliercd', '=', 't_admin.suppliercd')
                    ->where('t_order.order_code', '=', $orderCode)
                    ->first();
        return $result;
    }
}
