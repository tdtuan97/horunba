<?php
/**
 * Exe sql for hrnb.mst_shipping_company
 *
 * @package    App\Models
 * @subpackage MstShippingCompany
 * @copyright  Copyright (c) 2018 RiverCrane! Corporation. All Rights Reserved.
 * @author     van.qui<van.qui.rcvn2012@gmail.com>
 */

namespace App\Models\Backend;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Auth;

class MstShippingCompany extends Model
{
    public $timestamps = false;
    /**
    * The database table used by the model.
    * @var string
    */
    protected $table = 'mst_shipping_company';

    /**
    * The database is used by the model.
    * @var string
    */
    protected $connection = 'horunba';

    /**
    * The primary key.
    * @var string
    */
    protected $primaryKey = 'company_id';

    /**
     * Get item by delivery_type
     *
     * @param  int    $deliveryType
     * @return object
     */
    public function getItemByDeliveryType($deliveryType)
    {
        return $this->where('delivery_type', $deliveryType)->first();
    }
}
