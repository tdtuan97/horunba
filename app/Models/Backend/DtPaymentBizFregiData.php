<?php

/**
 * Exe sql for hrnb.dt_payment_biz_fregi_data
 *
 * @package    App\Models
 * @subpackage DtPaymentBizFregiData
 * @copyright  Copyright (c) 2018 RiverCrane! Corporation. All Rights Reserved.
 * @author     Truong Nghia<truong.van.nghia@rivercrane.vn>
 */

namespace App\Models\Backend;

use Illuminate\Database\Eloquent\Model;

class DtPaymentBizFregiData extends Model
{
    public $timestamps = false;
    /**
    * The database table used by the model.
    * @var string
    */
    protected $table = 'dt_payment_biz_fregi_data';
    /**
    * The database is used by the model.
    * @var string
    */
    protected $connection = 'horunba';
    /**
     * Get data process payment fregi
     * @return object
     */
    public function getDataProcessBizFregi()
    {
        $col = [
            'dt_payment_biz_fregi_data.receipt_num',
            'dt_payment_biz_fregi_data.amount_of_money',
            'dt_payment_biz_fregi_data.status',
            // 'dt_payment_mapping.received_order_id',
            // 'mst_order.mall_id',
            // 'mst_order.order_status',
            // 'mst_order.payment_method',
            // 'mst_order.request_price AS order_request_price',
        ];
        $result = $this->select($col)
                       // ->leftjoin(
                       //     'mst_order',
                       //     'dt_payment_biz_fregi_data.receipt_num',
                       //     '=',
                       //     'mst_order.received_order_id'
                       // )
                       // ->leftjoin(
                       //     'dt_payment_mapping',
                       //     'dt_payment_biz_fregi_data.receipt_num',
                       //     '=',
                       //     'dt_payment_mapping.received_order_id'
                       // )
                       ->where('dt_payment_biz_fregi_data.process_flg', 0)
                       ->where('dt_payment_biz_fregi_data.is_deleted', 0)
                       ->groupBy('dt_payment_biz_fregi_data.receipt_num')
                       ->get();
        return $result;
    }
}
