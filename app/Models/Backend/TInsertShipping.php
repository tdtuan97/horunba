<?php

/**
 * Model for tinsertshipping table.
 *
 * @package    App\Models\Backend
 * @subpackage TInsertShipping
 * @copyright  Copyright (c) 2017 RiverCrane! Corporation. All Rights Reserved.
 * @author     Truong Nghia<truong.van.nghia@rivercrane.vn>
 */

namespace App\Models\Backend;

use Illuminate\Database\Eloquent\Model;

class TInsertShipping extends Model
{
    public $timestamps = false;

    /**
    * The database table used by the model.
    * @var string
    */
    protected $table = 'tInsertShipping';

    /**
    * The database is used by the model.
    * @var string
    */
    protected $connection = 'glsc';

    /**
    * Update information
    * @param string $key      condition
    * @param array $arrUpdate data update
    * @return object boolean
    */
    public function updateData($key, $arrUpdate)
    {
        return $this->where($key)
                    ->update($arrUpdate);
    }
}
