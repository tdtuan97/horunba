<?php

/**
 * Exe sql for hrnb.mst_lesson
 *
 * @package    App\Models
 * @subpackage DtWebYahData
 * @copyright  Copyright (c) 2019 RiverCrane! Corporation. All Rights Reserved.
 * @author     Truong Nghia<truong.van.nghia@rivercrane.vn>
 */

namespace App\Models\Backend;

use Illuminate\Database\Eloquent\Model;
use DB;
use App\Custom\DBExtend;

class MstLesson extends Model
{
    use DBExtend;
    public $timestamps = false;

    /**
    * The database table used by the model.
    * @var string
    */
    protected $table = 'mst_lesson';
    /**
    * The database is used by the model.
    * @var string
    */
    protected $connection = 'horunba';
    
    /**
    * The primary key.
    * @var string
    */
    protected $primaryKey = 'lesson_id';

    /**
    * The type of primary key.
    * @var string
    */
    protected $keyType = 'string';

    /**
     * @param  array condition $where
     * @param  array update $data
     * @return boolean
     */
    public function updateData($where, $data)
    {
        $result = $this->where($where)
                    ->update($data);
        return $result;
    }
    
    /**
     * Get data of tabe
     *
     * @return object
     */
    public function getData($arraySearch = null, $arraySort = null)
    {
        $col = [
            'mst_lesson.lesson_id',
            'mst_lesson.category_name',
            'mst_lesson.lesson_name',
            'mst_lesson.require_point',
            'mst_lesson.priod',
            'mst_lesson.incharge_person',
            'mst_tantou.tantou_last_name'
        ];
        $data = $this->select($col)
            ->leftJoin('mst_tantou', 'mst_tantou.tantou_code', '=', 'mst_lesson.incharge_person');
        if (count($arraySearch) > 0) {
            $data->where(function ($query) use ($arraySearch) {
                if (isset($arraySearch['lesson_id'])) {
                    $query->where('mst_lesson.lesson_id', $arraySearch['lesson_id']);
                }
                if (isset($arraySearch['category_name'])) {
                    $query->where('mst_lesson.category_name', 'LIKE', "%{$arraySearch['category_name']}%");
                }
                if (isset($arraySearch['lesson_name'])) {
                    $query->where('mst_lesson.lesson_name', 'LIKE', "%{$arraySearch['lesson_name']}%");
                }
                if (isset($arraySearch['incharge_person'])) {
                    $query->where('mst_tantou.tantou_last_name', 'LIKE', "%{$arraySearch['incharge_person']}%");
                }
            });
        }
        $check = false;
        if ($arraySort !== null && count($arraySort) > 0) {
            foreach ($arraySort as $column => $sort) {
                if ($sort !== null && in_array($sort, ['asc', 'desc'])) {
                    $data->orderBy($column, $sort);
                    $check = true;
                }
            }
        }
        if (!$check) {
            $data->orderBy('mst_lesson.in_date', 'desc');
        }
        $perPage = ($arraySearch['per_page']) ? $arraySearch['per_page'] : 20;
        $data = $data->paginate($perPage);
        return $data;
    }
}
