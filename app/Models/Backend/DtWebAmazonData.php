<?php

/**
 * Exe sql for hrnb.dt_web_amazon_data
 *
 * @package    App\Models
 * @subpackage DtWebAmazonData
 * @copyright  Copyright (c) 2018 RiverCrane! Corporation. All Rights Reserved.
 * @author     Van Qui<van.qui@rivercrane.vn>
 */

namespace App\Models\Backend;

use Illuminate\Database\Eloquent\Model;

class DtWebAmazonData extends Model
{
    public $timestamps = false;
    
    /**
    * The database table used by the model.
    * @var string
    */
    protected $table = 'dt_web_amazon_data';
    
    /**
    * The database is used by the model.
    * @var string
    */
    protected $connection = 'horunba';
    
    /**
     * Get data process web amazon
     * @return object
     */
    public function getDataProcessWebAmazon()
    {
        $col = [
            'dt_web_amazon_data.order_number',
            'dt_web_amazon_data.payment_method',
            'dt_web_amazon_data.request_price',
            'dt_web_amazon_data.order_status',
            'mst_order.mall_id',
            'mst_order.order_status AS order_order_status',
            'mst_order.payment_method AS order_payment_method',
            'mst_order.request_price AS order_request_price',
        ];
        $result = $this->select($col)
                       ->leftjoin('mst_order', 'dt_web_amazon_data.order_number', '=', 'mst_order.received_order_id')
                       ->where('process_flg', 0)
                       ->where('is_delete', 0)
                       ->groupBy('dt_web_amazon_data.order_number')
                       ->get();
        return $result;
    }
}
