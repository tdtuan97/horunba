<?php

/**
 * Model for mst_payment_method table.
 *
 * @package    App\Models\Backend
 * @subpackage MstPaymentMethod
 * @copyright  Copyright (c) 2018 RiverCrane! Corporation. All Rights Reserved.
 * @author     Truong Nghia<truong.van.nghia@rivercrane.vn>
 */

namespace App\Models\Backend;

use Illuminate\Database\Eloquent\Model;

class MstPaymentMethod extends Model
{
    public $timestamps = false;

    /**
    * The database table used by the model.
    * @var string
    */
    protected $table = 'mst_payment_method';

    /**
    * The database is used by the model.
    * @var string
    */
    protected $connection = 'horunba';

    public function getDataByMall($mall)
    {
        $result = $this->where('mall_id', '=', $mall)
                       ->get();
        return $result;
    }
}
