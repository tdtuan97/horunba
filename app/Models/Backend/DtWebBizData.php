<?php

/**
 * Exe sql for hrnb.dt_web_biz_data
 *
 * @package    App\Models
 * @subpackage DtWebBizData
 * @copyright  Copyright (c) 2018 RiverCrane! Corporation. All Rights Reserved.
 * @author     Truong Nghia<truong.van.nghia@rivercrane.vn>
 */

namespace App\Models\Backend;

use Illuminate\Database\Eloquent\Model;

class DtWebBizData extends Model
{
    public $timestamps = false;
    /**
    * The database table used by the model.
    * @var string
    */
    protected $table = 'dt_web_biz_data';
    /**
    * The database is used by the model.
    * @var string
    */
    protected $connection = 'horunba';
    /**
     * Get data process web biz
     * @return object
     */
    public function getDataProcessWebBiz()
    {
        $col = [
            'dt_web_biz_data.order_id',
            'dt_web_biz_data.settlement_name',
            'dt_web_biz_data.request_price',
            'dt_web_biz_data.process_status',

            // 'dt_web_mapping.received_order_id',
            // 'mst_order.mall_id',
            // 'mst_order.order_status',
            // 'mst_order.payment_method',
            // 'mst_order.request_price AS order_request_price',
        ];
        $result = $this->select($col)
                       // ->leftjoin('mst_order', 'dt_web_biz_data.order_id', '=', 'mst_order.received_order_id')
                       // ->leftjoin('dt_web_mapping', 'dt_web_biz_data.order_id', '=', 'dt_web_mapping.received_order_id')
                       ->where('dt_web_biz_data.process_flg', 0)
                       ->where('dt_web_biz_data.is_delete', 0)
                       ->groupBy('dt_web_biz_data.order_id')
                       ->get();
        return $result;
    }
}
