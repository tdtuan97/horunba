<?php

/**
 * Exe sql for hrnb.dt_reorder
 *
 * @package    App\Models
 * @subpackage DtReorder
 * @copyright  Copyright (c) 2018 RiverCrane! Corporation. All Rights Reserved.
 * @author     le.hung<le.hung.rcvn2012@gmail.com>
 */

namespace App\Models\Backend;

use Illuminate\Database\Eloquent\Model;

class DtReorder extends Model
{
    public $timestamps = false;
    /**
    * The database table used by the model.
    * @var string
    */
    protected $table = 'dt_reorder';
    /**
    * The database is used by the model.
    * @var string
    */
    protected $connection = 'horunba';

        /**
     * Get max item by order code
     *
     * @param  int    $suppliercd
     * @return object
     */

    public function getMaxItem()
    {
        $data = $this->select('reorder_id')->orderBy('in_date', 'desc')->first();
        return $data;
    }
    
    /**
     * Get data by received order id
     *
     * @return object
     */
    public function getDataByReceivedOrderId($receivedOrderId)
    {
        $data = $this->select(
            'dt_reorder.reorder_id',
            'dt_reorder.in_date',
            'dt_reorder.re_order_type',
            'mst_tantou.tantou_last_name'
        )
        ->join('mst_tantou', 'tantou_code', '=', 'dt_reorder.in_ope_cd')
        ->where('received_order_id', $receivedOrderId)
        ->get();
        return $data;
    }
}
