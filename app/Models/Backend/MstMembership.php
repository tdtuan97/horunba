<?php

/**
 * Exe sql for hrnb.mst_membership
 *
 * @package    App\Models
 * @subpackage DtWebYahData
 * @copyright  Copyright (c) 2019 RiverCrane! Corporation. All Rights Reserved.
 * @author     Truong Nghia<truong.van.nghia@rivercrane.vn>
 */

namespace App\Models\Backend;

use Illuminate\Database\Eloquent\Model;
use DB;
use App\Custom\DBExtend;

class MstMembership extends Model
{
    use DBExtend;
    public $timestamps = false;

    /**
    * The database table used by the model.
    * @var string
    */
    protected $table = 'mst_membership';
    /**
    * The database is used by the model.
    * @var string
    */
    protected $connection = 'horunba';

    /**
    * The primary key.
    * @var string
    */
    protected $primaryKey = 'member_id';

    /**
    * The type of primary key.
    * @var string
    */
    protected $keyType = 'string';

    /**
     * @param  array condition $where
     * @param  array update $data
     * @return boolean
     */
    public function updateData($where, $data)
    {
        $result = $this->where($where)
                    ->update($data);
        return $result;
    }


    /**
     * Get data of tabe
     *
     * @return object
     */
    public function getData($arraySearch = null, $arraySort = null, $type = null)
    {
        $col = [
            'mst_membership.*',
        ];
        $case = "case when sex = 0 then '女性' when sex = 1 then '男性' else '不明' end  as sex";
        $data = $this->select($col)
                     ->selectRaw('CONCAT(IFNULL(last_name, ""), IFNULL(first_name, "")) as full_name')
                     ->selectRaw($case);
        if (!is_null($type)) {
            $data = $data->addSelect('T.*')
                         ->leftJoin(DB::raw('(SELECT point_num, paid_price, paid_date, unit_price, member_id  FROM dt_purchased_point WHERE dt_purchased_point.detail_num = (SELECT MAX(detail_num) FROM dt_purchased_point DPP WHERE DPP.member_id = dt_purchased_point.member_id)
                        ) as T'), function ($join) {
                            $join->on ( 'T.member_id', '=', 'mst_membership.member_id' );
                        });
        }
        if (count($arraySearch) > 0) {
            $data->where(function ($query) use ($arraySearch) {
                if (isset($arraySearch['member_id'])) {
                    $query->where('mst_membership.member_id', $arraySearch['member_id']);
                }
                if (isset($arraySearch['full_name'])) {
                    $query->where(function ($subWhere) use ($arraySearch) {
                        $fullName = $arraySearch['full_name'];
                        $multi  = str_replace('　', ' ', $fullName);
                        $single = str_replace(' ', '　', $fullName);
                        $searchFullName = 'CONCAT(IFNULL(last_name, ""), IFNULL(first_name, ""))';
                        $subWhere->whereRaw("{$searchFullName} LIKE '%{$multi}%'")
                                ->orWhereRaw("{$searchFullName} LIKE '%{$single}%'");
                    });
                }
                if (isset($arraySearch['birthday'])) {
                    $birthday = date("Y-m-d", strtotime($arraySearch['birthday']));
                    $query->whereDate('birthday', $birthday);
                }
                if (isset($arraySearch['tel_no'])) {
                    $query->whereRaw("tel_no LIKE '%{$arraySearch['tel_no']}%'");
                }
                if (isset($arraySearch['address'])) {
                    $query->whereRaw("address LIKE '%{$arraySearch['address']}%'");
                }
                if (isset($arraySearch['mail_address'])) {
                    $query->whereRaw("mail_address LIKE '%{$arraySearch['mail_address']}%'");
                }
            });
        }
        $check = false;
        if ($arraySort !== null && count($arraySort) > 0) {
            foreach ($arraySort as $column => $sort) {
                if ($sort !== null && in_array($sort, ['asc', 'desc'])) {
                    $data->orderBy($column, $sort);
                    $check = true;
                }
            }
        }
        if (!$check) {
            $data->orderBy('in_date', 'desc');
        }
        if (!is_null($type)) {
            return $data->get();
        }
        $perPage = ($arraySearch['per_page']) ? $arraySearch['per_page'] : 20;
        $data = $data->paginate($perPage);
        return $data;
    }
}
