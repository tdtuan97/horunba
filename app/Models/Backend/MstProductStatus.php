<?php
/**
 * Exe sql for hrnb.mst_product_status
 *
 * @package    App\Models
 * @subpackage MstProductStatus
 * @copyright  Copyright (c) 2018 RiverCrane! Corporation. All Rights Reserved.
 * @author     van.qui<van.qui.rcvn2012@gmail.com>
 */

namespace App\Models\Backend;

use Illuminate\Database\Eloquent\Model;

class MstProductStatus extends Model
{
    public $timestamps = false;
    /**
    * The database table used by the model.
    * @var string
    */
    protected $table = 'mst_product_status';
    /**
    * The database is used by the model.
    * @var string
    */
    protected $connection = 'horunba';
}
