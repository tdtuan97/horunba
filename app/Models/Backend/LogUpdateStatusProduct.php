<?php
/**
 * Model for log_update_status_product table.
 *
 * @package    App\Models\Backend
 * @subpackage LogUpdateStatusProduct
 * @copyright  Copyright (c) 2018 RiverCrane! Corporation. All Rights Reserved.
 * @author     Van Qui<van.qui@rivercrane.vn>
 */

namespace App\Models\Backend;

use Illuminate\Database\Eloquent\Model;

class LogUpdateStatusProduct extends Model
{
    public $timestamps = false;

    /**
    * The database table used by the model.
    * @var string
    */
    protected $table = 'log_update_status_product';

    /**
    * The database is used by the model.
    * @var string
    */
    protected $connection = 'horunba';

    /**
    * Insert log update product status
    * @return boolean
    */
    public function insertLog($arrkey, $arrUpdate, $className, $key = 'receive_id')
    {
        $arrInsert = [];
        if (is_array($arrkey[$key])) {
            foreach ($arrkey[$key] as $k => $id) {
                $arrInsert[$k][$key]              = $id;
                $arrInsert[$k]['product_status']  = $arrUpdate['product_status'];
                $arrInsert[$k]['class_name_call'] = $className;
            }
        } else {
            $arrInsert[$key]              = $arrkey[$key];
            $arrInsert['product_status']  = $arrUpdate['product_status'];
            $arrInsert['class_name_call'] = $className;
            if (isset($arrkey['detail_line_num'])) {
                $arrInsert['detail_line_num']  = $arrkey['detail_line_num'];
            }
            if (isset($arrkey['sub_line_num'])) {
                $arrInsert['sub_line_num']  = $arrkey['sub_line_num'];
            }
        }

        return $this->insert($arrInsert);
    }
}
