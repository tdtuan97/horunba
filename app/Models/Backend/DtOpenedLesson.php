<?php

/**
 * Exe sql for hrnb.dt_opened_lesson
 *
 * @package    App\Models
 * @subpackage DtWebYahData
 * @copyright  Copyright (c) 2019 RiverCrane! Corporation. All Rights Reserved.
 * @author     Truong Nghia<truong.van.nghia@rivercrane.vn>
 */

namespace App\Models\Backend;

use Illuminate\Database\Eloquent\Model;
use DB;
use App\Custom\DBExtend;

class DtOpenedLesson extends Model
{
    use DBExtend;
    public $timestamps = false;

    /**
    * The database table used by the model.
    * @var string
    */
    protected $table = 'dt_opened_lesson';
    /**
    * The database is used by the model.
    * @var string
    */
    protected $connection = 'horunba';

    /**
     * @param  array condition $where
     * @param  array update $data
     * @return boolean
     */
    public function updateData($where, $data)
    {
        $result = $this->where($where)
                    ->update($data);
        return $result;
    }

    /**
     * Get data opended lesson of member
     * @param  int memberId
     * @return object
     */
    public function getDataOpenedLessonOfMember($memberId)
    {
        $col = [
            'dt_opened_lesson.open_date',
            'dt_opened_lesson.used_point',
            'dt_opened_lesson.member_id',
            'mst_lesson.lesson_name',
            'mst_lesson.lesson_id',
            'mst_lesson.incharge_person',
        ];
        $result = $this->select($col)
                       ->join('mst_lesson', 'mst_lesson.lesson_id', 'dt_opened_lesson.lesson_id')
                       ->where('dt_opened_lesson.member_id', $memberId)
                       ->orderBy('dt_opened_lesson.up_ope_cd')
                       ->get();
        return $result;
    }

    /**
     * Get data
     * @param  array $arrCondition
     * @return object
     */
    public function getData($arrCondition)
    {
        $col = [
            'dt_opened_lesson.open_date',
            'dt_opened_lesson.used_point',
            'dt_opened_lesson.member_id',
        ];
        $result = $this->select($col)
                       ->where($arrCondition)
                       ->first();
        return $result;
    }

    /**
     * Get data by member_id
     * @param  string $memberId
     * @return object
     */
    public function getDataDetail($memberId, $lessonId, $openDate)
    {
        $result = $this->where('member_id', $memberId)
                       ->where('lesson_id', $lessonId)
                       ->where('open_date', $openDate)
                       ->first();
        return $result;
    }
}
