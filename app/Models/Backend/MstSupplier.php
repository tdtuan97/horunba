<?php

/**
 * Exe sql for hrnb.mst_supplier
 *
 * @package    App\Models\Backend
 * @subpackage MstProduct
 * @copyright  Copyright (c) 2018 RiverCrane! Corporation. All Rights Reserved.
 * @author     van.qui<van.qui.rcvn2012@gmail.com>
 */

namespace App\Models\Backend;

use Illuminate\Database\Eloquent\Model;

class MstSupplier extends Model
{
    public $timestamps = false;
    /**
    * The database table used by the model.
    * @var string
    */
    protected $table = 'mst_supplier';

    /**
    * The database is used by the model.
    * @var string
    */
    protected $connection = 'horunba';

    /**
     * Get infomation supplier
     *
     * @param  array $col field
     * @param  array $arrWhere condition
     * @param  boolean $multi get one or multi record
     * @return object
     */
    public function getInfo($col, $arrWhere, $multi = true)
    {
        if (!empty($col)) {
            $result = $this->select($col);
        } else {
            $result = $this;
        }
        if (is_array($arrWhere)) {
            $result->where($arrWhere);
        }
        if (!$multi) {
            return $result->first();
        }
        return $result->get();
    }

    /**
     * Get data by key
     *
     * @param  int $supplierCd
     *
     * @return object
     */
    public function checkSupplierIdExists($supplierCd)
    {
        return $this->where('supplier_cd', $supplierCd)->first();
    }

    public function getDataSendMail()
    {
        return $this->select(['mail_address_se_to', 'mail_address_se_cc'])
                    ->where('is_send', 1)
                    ->get();
    }
}
