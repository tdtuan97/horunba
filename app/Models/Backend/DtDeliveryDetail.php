<?php

/**
 * Exe sql for hrnb.dt_delivery_detail
 *
 * @package    App\Models
 * @subpackage DtDelivery
 * @copyright  Copyright (c) 2018 RiverCrane! Corporation. All Rights Reserved.
 * @author     le.hung<le.hung.rcvn2012@gmail.com>
 */

namespace App\Models\Backend;

use Illuminate\Database\Eloquent\Model;
use DB;
use App\Custom\DBExtend;

class DtDeliveryDetail extends Model
{
    use DBExtend;
    public $timestamps = false;

    /**
    * The database table used by the model.
    * @var string
    */
    protected $table = 'dt_delivery_detail';
    /**
    * The database is used by the model.
    * @var string
    */
    protected $connection = 'horunba';

    /**
     * @param  array condition $where
     * @param  array update $data
     * @return boolean
     */
    public function updateData($where, $data)
    {
        $result = $this->where($where)
                    ->update($data);
        return $result;
    }
}
