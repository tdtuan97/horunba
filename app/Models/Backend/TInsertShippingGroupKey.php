<?php

/**
 * Model for mst_groupkey table.
 *
 * @package    App\Models\Backend
 * @subpackage TInsertShippingGroupKey
 * @copyright  Copyright (c) 2017 RiverCrane! Corporation. All Rights Reserved.
 * @author     Truong Nghia<truong.van.nghia@rivercrane.vn>
 */

namespace App\Models\Backend;

use Illuminate\Database\Eloquent\Model;

class TInsertShippingGroupKey extends Model
{
    public $timestamps = false;

    /**
    * The database table used by the model.
    * @var string
    */
    protected $table = 'mst_groupkey';

    /**
    * The database is used by the model.
    * @var string
    */
    protected $connection = 'horunba';
}
