<?php

/**
 * Model for t_seq table.
 *
 * @package    App\Models\Backend
 * @subpackage TSeq
 * @copyright  Copyright (c) 2018 RiverCrane! Corporation. All Rights Reserved.
 * @author     van.qui<van.qui.rcvn2012@gmail.com>
 */

namespace App\Models\Backend;

use Illuminate\Database\Eloquent\Model;

class TSeq extends Model
{
    public $timestamps = false;
    /**
     * The database table used by the model.
     * @var string
     */
    protected $table = 't_seq';
    /**
     * The database is used by the model.
     * @var string
     */
    protected $connection = 'edi';

    /**
     * Update order from wait to finish
     * @param array $data array data to update
     * @return int
     */
    public function insertData()
    {
        $insertdata['directkb'] = "0";
        $id =  $this->insertGetId($insertdata);
        return $id;
    }
}
