<?php
/**
 * Exe sql for edi.v_nouhin (table view)
 *
 * @package App\Models\Edi
 * @subpackage EdiModel
 * @copyright Copyright (c) 2017 CriverCrane! Corporation. All Rights Reserved.
 * @author hung.le<hung.le@crivercrane.vn>
 */
namespace App\Models\Backend;

use Illuminate\Database\Eloquent\Model;
use DB;
use Config;

class VNouHin extends Model
{

    public $timestamps = false;
    /**
     * The database table used by the model.
     * @var string
     */
    protected $table = 'v_nouhin';
    /**
     * The database is used by the model.
     * @var string
     */
    protected $connection = 'edi';

    /**
     * Get list v_nouhin
     * @param string $arrayQuery    information data query
     * @param string $arrayOptions contain conditions for query
     *
     * @return object
     */
    public function getListVNouhinWhere($arrayQuery, $arrayOptions = null)
    {
        if ($arrayOptions['task'] === 'get_by_t_nouhin_id') {
            $data = $this->where('del_flg', "=", 0)
                ->where('valid_flg', "=", 1)
                ->whereIn('t_nouhin_id', $arrayQuery['t_nouhin_id'])
                ->get();
        }
        return $data;
    }
}
