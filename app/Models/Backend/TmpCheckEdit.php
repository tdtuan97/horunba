<?php
/**
 * Exe sql for hrnb.tmp_check_edit
 *
 * @package    App\Models
 * @subpackage TmpCheckEdit
 * @copyright  Copyright (c) 2018 RiverCrane! Corporation. All Rights Reserved.
 * @author     le.hung<le.hung.rcvn2012@gmail.com>
 */

namespace App\Models\Backend;

use Illuminate\Database\Eloquent\Model;

class TmpCheckEdit extends Model
{
    public $timestamps = false;

    /**
    * The database table used by the model.
    * @var string
    */
    protected $table = 'tmp_check_edit';

    /**
    * The primary key.
    * @var string
    */
    protected $primaryKey = 'id';
    /**
    * The type of primary key.
    * @var int
    */
    protected $keyType = 'int';
    /**
    * The database is used by the model.
    * @var string
    */
    protected $connection = 'horunba';


    /**
     * Get item data
     *
     * @param  int  index
     * @return object
     */
    public function getItem($dataWhere)
    {
        $data = $this->select('*')
                ->where($dataWhere)
                ->whereIn('is_avaliable', [0, 1])
                ->first();
        return $data;
    }
}
