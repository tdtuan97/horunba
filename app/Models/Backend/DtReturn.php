<?php
/**
 * Exe sql for hrnb.dt_return
 *
 * @package    App\Models
 * @subpackage DtReturn
 * @copyright  Copyright (c) 2018 RiverCrane! Corporation. All Rights Reserved.
 * @author     van.qui<van.qui.rcvn2012@gmail.com>
 */

namespace App\Models\Backend;

use Illuminate\Database\Eloquent\Model;

class DtReturn extends Model
{
    public $timestamps = false;

    /**
    * The database table used by the model.
    * @var string
    */
    protected $table = 'dt_return';

    /**
    * The database is used by the model.
    * @var string
    */
    protected $connection = 'horunba';

    /**
    * The primary key.
    * @var string
    */
    protected $primaryKey = 'return_no';

    /**
    * The type of primary key.
    * @var string
    */
    protected $keyType = 'string';
    /**
     * Get data list
     *
     * @return object
     */
    public function getData($arraySearch = null, $arraySort = null)
    {
        $col = [
            'dt_return.return_no',
            'dt_return.return_line_no',
            'dt_return.supplier_return_no',
            'dt_return.return_date',
            'dt_return.return_time',
            'dt_return.deje_no',
            'type_large_name AS return_type_large',
            'type_mid_name AS return_type_mid',
            'dt_return.status',
            'dt_return.receive_instruction',
            'dt_return.receive_result',
            'dt_return.delivery_instruction',
            'dt_return.delivery_result',
            'dt_return.red_voucher',
            'dt_return.product_code',
            'dt_return.parent_product_code',
            'mst_product_base.product_name',
            'mst_product_base.product_jan',
            'dt_return.return_quantity',
            'mst_supplier.supplier_nm',
            'mst_product_base.maker_full_nm',
            'dt_return.return_tanka',
            'dt_return.receive_id',
            'dt_return.error_code',
            'dt_return.in_date',
        ];
        $data = $this->select($col)
                     ->selectRaw('concat(return_no,return_time,return_line_no) AS glsc_receive')
                     ->selectRaw('case when date(dt_return.in_date) > DATE("2019-04-01") then 1 ELSE 0 end  AS deje_check')
                     ->join('mst_return_type_large', 'type_large_id', '=', 'return_type_large_id')
                     ->join('mst_return_type_mid', 'type_mid_id', '=', 'return_type_mid_id')
                     ->join('mst_product_base', 'mst_product_base.product_code', '=', 'dt_return.parent_product_code')
                     ->join('mst_supplier', 'mst_supplier.supplier_cd', '=', 'dt_return.supplier_id');
        if (count($arraySearch) > 0) {
            $data->where(function ($query) use ($arraySearch) {
                if (isset($arraySearch['return_no'])) {
                    $query->where('return_no', 'LIKE', "%{$arraySearch['return_no']}%");
                }
                if (isset($arraySearch['return_date_from'])) {
                    $query->where('return_date', '>=', "{$arraySearch['return_date_from']}");
                }
                if (isset($arraySearch['return_date_to'])) {
                    $query->where('return_date', '<=', "{$arraySearch['return_date_to']}");
                }
                if (isset($arraySearch['deje_no'])) {
                    $query->where('deje_no', 'LIKE', "%{$arraySearch['deje_no']}%");
                }
                if (isset($arraySearch['product_code_from'])) {
                    $query->where('dt_return.parent_product_code', '>=', "{$arraySearch['product_code_from']}");
                }
                if (isset($arraySearch['product_code_to'])) {
                    $query->where('dt_return.parent_product_code', '<=', "{$arraySearch['product_code_to']}");
                }

                if (isset($arraySearch['supplier_nm'])) {
                    $query->where('supplier_nm', 'LIKE', "%{$arraySearch['supplier_nm']}%");
                }

                if (isset($arraySearch['return_type_large_id'])) {
                    if (is_array($arraySearch['return_type_large_id'])) {
                        $query->whereIn('dt_return.return_type_large_id', $arraySearch['return_type_large_id']);
                    } else {
                        $query->where('dt_return.return_type_large_id', $arraySearch['return_type_large_id']);
                    }
                }
                if (isset($arraySearch['return_type_mid_id'])) {
                    if (is_array($arraySearch['return_type_mid_id'])) {
                        $query->whereIn('dt_return.return_type_mid_id', $arraySearch['return_type_mid_id']);
                    } else {
                        $query->where('dt_return.return_type_mid_id', $arraySearch['return_type_mid_id']);
                    }
                }
                if (isset($arraySearch['status'])) {
                    if (is_array($arraySearch['status'])) {
                        $query->whereIn('dt_return.status', $arraySearch['status']);
                    } else {
                        $query->where('dt_return.status', $arraySearch['status']);
                    }
                }
                if (isset($arraySearch['receive_instruction'])) {
                    if (is_array($arraySearch['receive_instruction'])) {
                        $query->whereIn('dt_return.receive_instruction', $arraySearch['receive_instruction']);
                    } else {
                        $query->where('dt_return.receive_instruction', $arraySearch['receive_instruction']);
                    }
                }
                if (isset($arraySearch['delivery_instruction'])) {
                    if (is_array($arraySearch['delivery_instruction'])) {
                        $query->whereIn('dt_return.delivery_instruction', $arraySearch['delivery_instruction']);
                    } else {
                        $query->where('dt_return.delivery_instruction', $arraySearch['delivery_instruction']);
                    }
                }
                if (isset($arraySearch['red_voucher'])) {
                    if (is_array($arraySearch['red_voucher'])) {
                        $query->whereIn('dt_return.red_voucher', $arraySearch['red_voucher']);
                    } else {
                        $query->where('dt_return.red_voucher', $arraySearch['red_voucher']);
                    }
                }
            });
        }
        $check = false;
        if ($arraySort !== null && count($arraySort) > 0) {
            foreach ($arraySort as $column => $sort) {
                if ($sort !== null && in_array($sort, ['asc', 'desc'])) {
                    $data->orderBy($column, $sort);
                    $check = true;
                }
            }
        }
        if (!$check) {
            $data->orderBy('dt_return.in_date', 'desc');
        }
        $perPage = ($arraySearch['per_page']) ? $arraySearch['per_page'] : 20;
        $data = $data->paginate($perPage);
        return $data;
    }

    /**
    * Get max primary key
    * @return object
    */
    public function getMaxNo()
    {
        $result = $this->selectRaw('MAX(return_no) AS maxNo')
                       ->whereRaw('length(return_no) = 7')
                       ->whereRaw("return_no like '99%'")
                       ->first();
        return $result;
    }
    /**
     * Get list data by receive_id
     *
     * @param   int     $receiveId
     * @return  object
     */
    public function getDataByReceiveId($receiveId)
    {
        $data = $this->select(
            'return_no',
            'return_time',
            'return_line_no',
            'return_date',
            'note',
            'dt_return.in_ope_cd',
            'tantou_last_name'
        )
        ->join('mst_tantou', 'tantou_code', '=', 'dt_return.in_ope_cd')
        ->where('receive_id', $receiveId)
        ->get();
        return $data;
    }

    /**
     * Get data check info return
     *
     * @param   int     $receiveId
     * @param   string  $productCode
     * @return  object
     */
    public function checkInfoReturn($receiveId, $productCode)
    {
        $result = $this->select('return_no')
                       ->where('receive_id', $receiveId)
                       ->where('parent_product_code', $productCode)
                       ->first();
        return $result;
    }
    /**
     * Get data check order exist
     *
     * @param   int     $receiveId
     * @return  object
     */
    public function checkOrderExists($receiveId)
    {
        return $this->where('dt_return.receive_id', $receiveId)
                    ->whereNotNull('dt_return.parent_product_code')
                    ->orderBy('return_time')
                    ->orderBy('return_line_no')
                    ->get();
    }
    /**
     * Get max return_line_no by receive_id
     *
     * @param   int     $receiveId
     * @return  object
     */
    public function getMaxDataByReceive($receiveId)
    {
        return $this->where('receive_id', $receiveId)
                    ->orderBy('return_time', 'DESC')
                    ->first();
    }

    /**
     * Get detail return save
     * @param   int     $returnNo
     * @return  object
     */
    public function getOrderByReturnNo($returnNo)
    {
        return $this->where('dt_return.return_no', $returnNo)
                    ->first();
    }

    /**
     * Get detail return edit
     * @param   int     $returnNo
     * @param   string  $productCode
     * @param   int     $returnTime
     * @return  object
     */
    public function getReturnEditByReturnNo($returnNo, $productCode, $returnTime)
    {
        return $this->leftjoin('mst_return_address', 'dt_return.return_address_id', '=', 'mst_return_address.return_id')
                    ->where('dt_return.return_no', $returnNo)
                    ->where('dt_return.parent_product_code', $productCode)
                    ->where('dt_return.return_time', $returnTime)
                    ->orderBy('return_line_no')
                    ->first();
    }


    /**
     * Count data check order exist
     * @param   int     $returnNo
     * @param   string  $productCode
     * @return  object
     */
    public function countOrderByReturnNo($returnNo, $productCode)
    {
        return $this->select('parent_product_code')
                    ->selectRaw('count(parent_product_code) AS count')
                    ->where('dt_return.return_no', $returnNo)
                    ->where('dt_return.parent_product_code', $productCode)
                    ->first();
    }
    /**
     * Get data
     * @param   int     $returnNo
     * @return  object
     */
    public function getDataByReturnNo($returnNo)
    {
        $col = [
            'dt_return.return_no',
            'dt_return.return_address_id',
            'dt_return.deje_no',
            'dt_return.return_type_large_id',
            'dt_return.return_type_mid_id',
            'dt_return.parent_product_code',
            'dt_return.product_code',
            'dt_return.return_quantity',
            'dt_return.return_price',
            'dt_return.return_tanka',
            'dt_return.receive_plan_date',
            'dt_return.delivery_plan_date',
            'dt_return.payment_on_delivery',
            'dt_return.return_line_no',
            'dt_return.return_time',
            'dt_return.note',
            'dt_return.error_code',
            'dt_return.return_date',
            'dt_return.edi_order_code',
            'parent.product_name AS parent_product_name',
            'parent.price_invoice AS parent_price_invoice',
            'parent.price_supplier_id',
            'child.product_name AS child_product_name',
            'child.price_invoice AS child_price_invoice',
            'ra.return_id',
            'mst_product_set.component_num',
        ];
        $result = $this->select($col)
                       ->selectRaw("CASE when ra.return_address is null then '返品住所ない' else '品住所変更' end as address")
                       ->leftjoin('mst_product_supplier AS parent', 'parent.product_code', 'dt_return.parent_product_code')
                       ->leftjoin('mst_return_address AS ra', function ($join) {
                            $join->on('parent.price_supplier_id', '=', 'ra.supplier_cd')
                            ->where('ra.is_selected', 1);
                       })
                       ->leftjoin('mst_product_supplier AS child', 'child.product_code', 'dt_return.product_code')
                       ->leftjoin('mst_product_set', 'child.product_code', '=', 'mst_product_set.child_product_code')
                       ->where('return_no', $returnNo)
                       ->orderBy('return_line_no')
                       ->get();
        return $result;
    }

    /**
     * Update data by key
     *
     * @param  array   $keys
     * @param  array   $data
     * @return boolean
     */
    public function updateData($keys, $data)
    {
        return $this->where($keys)->update($data);
    }
    /**
     * Get data
     * @param   int     $returnNo
     * @param   int     $returnLineNo
     * @param   int     $returnTime
     * @return  object
     */
    public function getDataProductByKey($returnNo, $returnTime)
    {
        return $this->where('return_no', $returnNo)
                    ->where('return_time', $returnTime)
                    ->get();
    }

    public function getDataCurrentByCondition($returnNo, $arrReturnLineNo)
    {
        $result = $this->leftJoin('mst_product_set', function ($sub) {
            $sub->on('dt_return.parent_product_code', '=', 'mst_product_set.parent_product_code')
               ->on('dt_return.product_code', '=', 'mst_product_set.child_product_code');
        })
        ->where('return_no', $returnNo)
        ->whereIn('return_line_no', $arrReturnLineNo)
        ->get();
        return $result;
    }

    public function getDataCheckStockEdit($returnNo, $arrProductCheck)
    {
        $result = $this->select(
            'dt_return.parent_product_code',
            'dt_return.product_code',
            'dt_return.return_quantity',
            'dt_return.return_line_no'
        )
        ->where('return_no', $returnNo)
        ->where(function ($sub) use ($arrProductCheck) {
            foreach ($arrProductCheck as $item) {
                $sub->orWhere(function ($where) use ($item) {
                    $where->whereRaw(
                        'IFNULL(dt_return.product_code, dt_return.parent_product_code) = ?',
                        [$item['product_code']]
                    )
                    ->where('return_line_no', '<>', $item['return_line_no']);
                });
            }
        });
        return $result->get();
    }
}
