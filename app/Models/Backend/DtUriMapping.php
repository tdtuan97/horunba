<?php
/**
 * Model for dt_uri_mapping table.
 *
 * @package    App\Models\Backend
 * @subpackage DtUriMapping
 * @copyright  Copyright (c) 2019 RiverCrane! Corporation. All Rights Reserved.
 * @author     Truong Nghia<truong.van.nghia@crivercrane.vn>
 */

namespace App\Models\Backend;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Pagination\Paginator;

class DtUriMapping extends Model
{
    public $timestamps = false;

    /**
     * The database table used by the model.
     * @var string
     */
    protected $table = 'dt_uri_mapping';

    /**
     * The database is used by the model.
     * @var string
     */
    protected $connection = 'horunba';

    /**
     * Get data list
     *
     * @return object
     */
    public function getData($arraySearch = null, $arraySort = null, $arrayOptions = null)
    {
        $col = [
            'dt_uri_mapping.received_order_id',
            'dt_uri_mapping.order_date',
            'dt_uri_mapping.inquiry_no',
            'dt_uri_mapping.delivery_date',
            'dt_uri_mapping.payment_name',
            'pm.payment_name AS payment_method',
            'mst_order_status.status_name AS order_status',
            'dt_uri_mapping.payment_plan_date',
            'dt_uri_mapping.payment_date',
            'dt_uri_mapping.total_payment_plan',
            'dt_uri_mapping.ship_charge',
            'dt_uri_mapping.pay_charge_discount',
            'dt_uri_mapping.pay_charge',
            'dt_uri_mapping.pay_after_charge',
            'dt_uri_mapping.used_point',
            'dt_uri_mapping.paid_point',
            'dt_uri_mapping.used_coupon',
            'dt_uri_mapping.paid_coupon',
            'dt_uri_mapping.request_price',
            'dt_uri_mapping.return_price',
            'dt_uri_mapping.paid_request_price',
            'dt_uri_mapping.total_paid_price',
            'dt_uri_mapping.diff_price',
            'dt_uri_mapping.remarks',
            'dt_uri_mapping.paid_point_plan_date',
            'dt_uri_mapping.point_paid_date',
            'dt_uri_mapping.coupon_paid_plan_date',
            'dt_uri_mapping.coupon_paid_date',
            'dt_uri_mapping.goods_price',
            'dt_uri_mapping.discount',
            'dt_uri_mapping.occur_reason',
            'dt_uri_mapping.finished_date',
            'dt_uri_mapping.is_corrected',
            'mst_mall.name_jp',
            'mst_order.receive_id',
            'mst_order.mall_id',
        ];
        $data = $this->select($col);

        $data    = $this->conditionQuery($data, $arraySearch, $arraySort);
        $perPage = ($arraySearch['per_page']) ? $arraySearch['per_page'] : 20;
        if (isset($arrayOptions['type'])) {
            $data = $data->get();
        } else {
            $data = $data->paginate($perPage);
        }
        return $data;
    }

    /**
     * Condition query
     *
     * @param  object  $data
     * @param  array   $arraySearch
     * @param  array   $arraySort
     * @return object
     */
    public function conditionQuery($data, $arraySearch = null, $arraySort = null)
    {
        $data->join('mst_mall', 'mst_mall.id', '=', 'dt_uri_mapping.mall_id')
            ->leftjoin('mst_settlement_manage as pm', 'dt_uri_mapping.payment_method', '=', 'pm.payment_code')
            ->leftjoin('mst_order', 'dt_uri_mapping.received_order_id', '=', 'mst_order.received_order_id')
            ->join('mst_order_status', 'mst_order_status.order_status_id', '=', 'dt_uri_mapping.order_status');
        if (count($arraySearch) > 0) {
            $data->where(function ($query) use ($arraySearch) {
                if (isset($arraySearch['mall_id'])) {
                    if (is_array($arraySearch['mall_id'])) {
                        $query->whereIn('dt_uri_mapping.mall_id', $arraySearch['mall_id']);
                    } else {
                        $query->where('dt_uri_mapping.mall_id', $arraySearch['mall_id']);
                    }
                }
                if (isset($arraySearch['payment_method'])) {
                    if (is_array($arraySearch['payment_method'])) {
                        $query->whereIn('dt_uri_mapping.payment_method', $arraySearch['payment_method']);
                    } else {
                        $query->where('dt_uri_mapping.payment_method', $arraySearch['payment_method']);
                    }
                }
                if (isset($arraySearch['order_date_from'])) {
                    $dateFrom = date("Y-m-d 00:00:00", strtotime($arraySearch['order_date_from']));
                    $query->where(
                        'dt_uri_mapping.order_date',
                        '>=',
                        $dateFrom
                    );
                }
                if (isset($arraySearch['order_date_to'])) {
                    $dateTo = date("Y-m-d 23:59:59", strtotime($arraySearch['order_date_to']));
                    $query->where(
                        'dt_uri_mapping.order_date',
                        '<=',
                        $dateTo
                    );
                }
                if (isset($arraySearch['delivery_date_from'])) {
                    $dateFrom = date("Y-m-d 00:00:00", strtotime($arraySearch['delivery_date_from']));
                    $query->where(
                        'dt_uri_mapping.delivery_date',
                        '>=',
                        $dateFrom
                    );
                }
                if (isset($arraySearch['delivery_date_to'])) {
                    $dateTo = date("Y-m-d 23:59:59", strtotime($arraySearch['delivery_date_to']));
                    $query->where(
                        'dt_uri_mapping.delivery_date',
                        '<=',
                        $dateTo
                    );
                }
                if (isset($arraySearch['received_order_id'])) {
                    $query->where('dt_uri_mapping.received_order_id', 'like', "%{$arraySearch['received_order_id']}%");
                }

                if (isset($arraySearch['payment_name'])) {
                    $query->where('dt_uri_mapping.payment_name', 'like', "%{$arraySearch['payment_name']}%");
                }
                if (isset($arraySearch['is_corrected'])) {
                    $query->where('dt_uri_mapping.is_corrected', $arraySearch['is_corrected']);
                }

                if (isset($arraySearch['diff_price_flg'])) {
                    if ($arraySearch['diff_price_flg'] == 0) {
                        $query->where('dt_uri_mapping.diff_price', 0);
                    } else {
                        $query->where('dt_uri_mapping.diff_price', '<>', 0);
                    }
                }
                if (isset($arraySearch['diff_price'])) {
                    $query->where('dt_uri_mapping.diff_price', $arraySearch['diff_price']);
                }
            });
        }
        if ($arraySort !== null && count($arraySort) > 0) {
            foreach ($arraySort as $column => $sort) {
                if ($sort !== null && in_array($sort, ['asc', 'desc'])) {
                    $data->orderBy($column, $sort);
                }
            }
        } else {
            $data->orderBy('dt_uri_mapping.up_date', 'desc');
        }
        return $data;
    }

    /**
     * Get data all
     * @array $arrayOrder
     * @return object
     */
    public function getDataAll($arrayOrder)
    {
        $col = [
            'dt_uri_mapping.received_order_id',
            'dt_uri_mapping.order_date',
            'dt_uri_mapping.inquiry_no',
            'dt_uri_mapping.delivery_date',
            'dt_uri_mapping.payment_name',
            'pm.payment_name AS payment_method',
            'mst_order_status.status_name AS order_status',
            'dt_uri_mapping.payment_plan_date',
            'dt_uri_mapping.payment_date',
            'dt_uri_mapping.total_payment_plan',
            'dt_uri_mapping.ship_charge',
            'dt_uri_mapping.pay_charge_discount',
            'dt_uri_mapping.pay_charge',
            'dt_uri_mapping.pay_after_charge',
            'dt_uri_mapping.used_point',
            'dt_uri_mapping.paid_point',
            'dt_uri_mapping.used_coupon',
            'dt_uri_mapping.paid_coupon',
            'dt_uri_mapping.request_price',
            'dt_uri_mapping.return_price',
            'dt_uri_mapping.paid_request_price',
            'dt_uri_mapping.total_paid_price',
            'dt_uri_mapping.diff_price',
            'dt_uri_mapping.remarks',
            'dt_uri_mapping.paid_point_plan_date',
            'dt_uri_mapping.point_paid_date',
            'dt_uri_mapping.coupon_paid_plan_date',
            'dt_uri_mapping.coupon_paid_date',
            'dt_uri_mapping.goods_price',
            'dt_uri_mapping.discount',
            'dt_uri_mapping.occur_reason',
            'dt_uri_mapping.finished_date',
            'dt_uri_mapping.is_corrected',
            'mst_mall.name_jp',
            'mst_order.receive_id',
            'mst_order.mall_id',
        ];
        $data = $this->select($col)
            ->join('mst_mall', 'mst_mall.id', '=', 'dt_uri_mapping.mall_id')
            ->leftjoin('mst_settlement_manage as pm', 'dt_uri_mapping.payment_method', '=', 'pm.payment_code')
            ->leftjoin('mst_order', 'dt_uri_mapping.received_order_id', '=', 'mst_order.received_order_id')
            ->join('mst_order_status', 'mst_order_status.order_status_id', '=', 'dt_uri_mapping.order_status')
            ->whereIn('dt_uri_mapping.received_order_id', $arrayOrder)
            ->orderBy('dt_uri_mapping.received_order_id', 'desc');
        $currentPage = 1;
        Paginator::currentPageResolver(function () use ($currentPage) {
            return $currentPage;
        });
        return $data->paginate(count($arrayOrder));
    }

    /**
     * Get data page
     * @array $page
     * @return object
     */
    public function getDataPage($page)
    {
        $data = $this->select(['received_order_id'])
                     ->orderBy('up_date', 'desc');
        $data = $data->paginate($page);
        return $data;
    }

    /**
     * Update data by key
     *
     * @param  array   $keys
     * @param  array   $data
     * @return boolean
     */
    public function updateData($keys, $data)
    {
        return $this->where($keys)->update($data);
    }

    /**
     * Get data process rakuten pay calculate point
     *
     * @return object
     */
    public function getDataProcessRakPayPoint()
    {
        $col = [
            'dt_uri_rak_pay.id',
            'dt_uri_rak_pay.index_in_file',
            'dt_uri_rak_pay.paid_price',
            'dt_uri_rak_pay.paid_date',
            'dt_uri_mapping.received_order_id',
            'dt_uri_mapping.paid_point',
        ];
        $result = $this->select($col)
            ->selectRaw("SUM(IFNULL(dt_uri_rak_pay.paid_price,0)) as total_paid_price")
            ->join('dt_uri_rak_pay', 'dt_uri_mapping.received_order_id', '=', 'dt_uri_rak_pay.receive_order_num')
            ->where('dt_uri_rak_pay.paid_type', '楽天スーパーポイント')
            ->where('dt_uri_rak_pay.process_flg', 0)
            ->where('dt_uri_rak_pay.is_delete', 0)
            ->groupBy('dt_uri_mapping.received_order_id')
            ->get();
        return $result;
    }

    /**
     * Get data process rakuten pay calculate point
     *
     * @return object
     */
    public function getDataProcessRakPayRequestPrice()
    {
        $col = [
            'dt_uri_rak_pay.id',
            'dt_uri_rak_pay.index_in_file',
            'dt_uri_rak_pay.paid_date',
            'dt_uri_mapping.paid_request_price',
            'dt_uri_mapping.received_order_id',
        ];
        $result = $this->select($col)
            ->selectRaw("SUM(IFNULL(dt_uri_rak_pay.paid_price,0)) as total_paid_price")
            ->join('dt_uri_rak_pay', 'dt_uri_mapping.received_order_id', '=', 'dt_uri_rak_pay.receive_order_num')
            ->where('dt_uri_rak_pay.paid_type', '<>', '楽天スーパーポイント')
            ->whereNotIn('dt_uri_rak_pay.summary', ['印紙税相当額（郵便局）', '印紙税'])
            ->where('dt_uri_rak_pay.process_flg', 0)
            ->where('dt_uri_rak_pay.is_delete', 0)
            ->groupBy('dt_uri_mapping.received_order_id')
            ->get();
        return $result;
    }

    /**
     * Get data process rakuten pay calculate point
     *
     * @return object
     */
    public function getDataProcessRakCouponUpdate()
    {
        $col = [
            'dt_uri_rak_coupon.id',
            'dt_uri_rak_coupon.index_in_file',
            'dt_uri_rak_coupon.coupon_paid_price',
            'dt_uri_rak_coupon.paid_date',
            'dt_uri_mapping.received_order_id',
            'dt_uri_mapping.paid_coupon',
        ];
        $result = $this->select($col)
            ->join('dt_uri_rak_coupon', 'dt_uri_mapping.received_order_id', '=', 'dt_uri_rak_coupon.receive_order_num')
            ->where('dt_uri_rak_coupon.process_flg', 0)
            ->where('dt_uri_rak_coupon.is_delete', 0)
            ->get();
        return $result;
    }

    /**
     * Get data yahoo process coupon 1
     *
     * @return object
     */
    public function getDataProcessYahCoupon1()
    {
        $col = [
            'dt_uri_yahoo.tax_price',
            'dt_uri_yahoo.paid_date',
            'dt_uri_yahoo.id',
            'dt_uri_mapping.received_order_id',
        ];
        $result = $this->select($col)
            ->join('dt_uri_yahoo', 'dt_uri_mapping.received_order_id', '=', 'dt_uri_yahoo.receive_order_num')
            ->where('dt_uri_yahoo.used_item', 'ポイント利用料')
            ->where('dt_uri_yahoo.process_flg', 0)
            ->where('dt_uri_yahoo.is_delete', 0)
            ->get();
        return $result;
    }


    /**
     * Get data yahoo process coupon 2
     *
     * @return object
     */
    public function getDataProcessYahCoupon2()
    {
        $col = [
            'dt_uri_yahoo.tax_price',
            'dt_uri_yahoo.paid_date',
            'dt_uri_yahoo.id',
            'dt_uri_mapping.received_order_id',
        ];
        $result = $this->select($col)
            ->join('dt_uri_yahoo', 'dt_uri_mapping.received_order_id', '=', 'dt_uri_yahoo.receive_order_num')
            ->where('dt_uri_yahoo.used_item', 'モールクーポン利用料')
            ->where('dt_uri_yahoo.process_flg', 0)
            ->where('dt_uri_yahoo.is_delete', 0)
            ->get();
        return $result;
    }


    /**
     * Get data yahoo process coupon 3
     *
     * @return object
     */
    public function getDataProcessYahCoupon3()
    {
        $col = [
            'dt_uri_mapping.received_order_id',
            'dt_uri_yahoo.id',
        ];
        $result = $this->select($col)
            ->selectRaw('SUM(IFNULL(dt_uri_yahoo.tax_price,0)) AS paid_request_price')
            ->selectRaw('MAX(dt_uri_yahoo.paid_date) AS payment_date')
            ->join('dt_uri_yahoo', 'dt_uri_mapping.received_order_id', '=', 'dt_uri_yahoo.receive_order_num')
            ->whereNotIn('dt_uri_yahoo.used_item', ['モールクーポン利用料', 'ポイント利用料'])
            ->where('dt_uri_yahoo.used_item', 'NOT LIKE', '%キャンセル%')
            ->where('dt_uri_yahoo.process_flg', 0)
            ->where('dt_uri_yahoo.is_delete', 0)
            ->groupBy('dt_uri_mapping.received_order_id')
            ->get();
        return $result;
    }

    /**
     * Get data yahoo process coupon 4
     *
     * @return object
     */
    public function getDataProcessYahCoupon4()
    {
        $col = [
            'dt_uri_mapping.received_order_id',
            'dt_payment_list.payment_price',
            'dt_payment_list.payment_date',
            'dt_payment_list.payment_code',
            'dt_payment_list.index',
        ];
        $result = $this->select($col)
            ->join('mst_order', 'dt_uri_mapping.received_order_id', '=', 'mst_order.received_order_id')
            ->join('dt_payment_list', 'dt_payment_list.receive_id', '=', 'mst_order.receive_id')
            ->where('dt_payment_list.payment_code', 2)
            ->get();
        return $result;
    }

    /**
     * Get data paygent calculate
     *
     * @return object
     */
    public function getDataProcessProPaygentCalculate()
    {
        $col = [
            'dt_uri_paygent.id',
            'dt_uri_paygent.order_id',
            'dt_uri_paygent.paid_date',
            'dt_uri_mapping.received_order_id',
        ];
        $result = $this->select($col)
            ->selectRaw("SUM(IFNULL(dt_uri_paygent.paid_price,0)) as total_paid_price")
            ->join('dt_uri_paygent', 'dt_uri_mapping.received_order_id', '=', 'dt_uri_paygent.order_id')
            ->where('dt_uri_paygent.process_flg', 0)
            ->where('dt_uri_paygent.is_delete', 0)
            ->groupBy('dt_uri_paygent.order_id')
            ->get();
        return $result;
    }

    /**
     * Get data MFK calculate
     *
     * @return object
     */
    public function getDataProcessMfkCalculate()
    {
        $col = [
            'dt_uri_mfk.id',
            'dt_uri_mfk.order_id',
            'dt_uri_mfk.paid_date',
            'dt_uri_mapping.received_order_id',
        ];
        $result = $this->select($col)
            ->selectRaw("SUM(IFNULL(dt_uri_mfk.paid_price,0)) as total_paid_price")
            ->join('dt_uri_mfk', 'dt_uri_mapping.received_order_id', '=', 'dt_uri_mfk.order_id')
            ->where('dt_uri_mfk.process_flg', 0)
            ->where('dt_uri_mfk.is_delete', 0)
            ->groupBy('dt_uri_mfk.order_id')
            ->get();
        return $result;
    }

    /**
     * Get data Sagawa calculate
     *
     * @return object
     */
    public function getDataProcessSagawaCalculate()
    {
        $col = [
            'dt_uri_sagawa.id',
            'dt_uri_sagawa.inquiry_no',
            'dt_uri_sagawa.paid_price',
            'dt_uri_mapping.received_order_id',
        ];
        $result = $this->select($col)
            ->selectRaw("CONCAT(dt_uri_sagawa.deposit_year,'/',dt_uri_sagawa.deposit_month, '/',dt_uri_sagawa.deposit_day) AS payment_date")
            ->join('dt_uri_sagawa', 'dt_uri_mapping.inquiry_no', '=', 'dt_uri_sagawa.inquiry_no')
            ->where('dt_uri_sagawa.process_flg', 0)
            ->where('dt_uri_sagawa.is_delete', 0)
            ->where('dt_uri_mapping.delivery_code', 3)
            ->groupBy('dt_uri_sagawa.inquiry_no')
            ->get();
        return $result;
    }

    /**
     * Get data JP post calculate
     *
     * @return object
     */
    public function getDataProcessJpPostCalculate()
    {
        $col = [
            'dt_uri_jppost.id',
            'dt_uri_jppost.inquiry_no',
            'dt_uri_jppost.deposit_date',
            'dt_uri_jppost.settlement_price',
            'dt_uri_mapping.received_order_id',
        ];
        $result = $this->select($col)
            ->join('dt_uri_jppost', 'dt_uri_mapping.inquiry_no', '=', 'dt_uri_jppost.inquiry_no')
            ->where('dt_uri_jppost.process_flg', 0)
            ->where('dt_uri_jppost.is_delete', 0)
            ->where('dt_uri_jppost.summary_type', '<>', 3)
            ->where('dt_uri_mapping.delivery_code', 4)
            ->get();
        return $result;
    }

    /**
     * Get data Seinou calculate
     *
     * @return object
     */
    public function getDataProcessSeinouCalculate()
    {
        $col = [
            'dt_uri_seinou.id',
            'dt_uri_seinou.inquiry_no',
            'dt_uri_seinou.deposit_date',
            'dt_uri_seinou.cod_price',
            'dt_uri_mapping.received_order_id',
        ];
        $result = $this->select($col)
            ->join('dt_uri_seinou', 'dt_uri_mapping.inquiry_no', '=', 'dt_uri_seinou.inquiry_no')
            ->where('dt_uri_seinou.process_flg', 0)
            ->where('dt_uri_seinou.is_delete', 0)
            ->where('dt_uri_mapping.delivery_code', 999)
            ->get();
        return $result;
    }

    /**
     * Get data process amazon uri mapping
     *
     * @return object
     */
    public function getDataProcessAmazonCalculate()
    {
        $col = [
            'dt_uri_amazon.id',
            'dt_uri_mapping.received_order_id',
            'dt_uri_mapping.used_point',
            'dt_uri_mapping.used_coupon',
            'dt_uri_mapping.payment_method',
            'dt_uri_mapping.paid_request_price',
        ];
        $result = $this->select($col)
            ->selectRaw('SUM(IFNULL(dt_uri_amazon.amount, 0)) AS sum_amount')
            ->selectRaw('MAX(dt_uri_amazon.`deposit-date`) AS max_deposit_date')
            ->join('dt_uri_amazon', 'dt_uri_mapping.received_order_id', '=', 'dt_uri_amazon.order-id')
            ->where('dt_uri_amazon.process_flg', 0)
            ->where('dt_uri_amazon.is_delete', 0)
            ->whereIn('dt_uri_amazon.amount-description', ['Tax', 'Principal', 'COD', 'COD Tax', 'Shipping', 'ShippingTax', 'CollectOnDeliveryRevenue'])
            ->groupBy('dt_uri_mapping.received_order_id')
            ->get();
        return $result;
    }

    /**
     * Update all record by condition
     *
     * @return object
     */
    public function updateAll($arraySearch, $arraySort = null, $arrayUpdate = null)
    {
        $data = $this->conditionQuery($this->select('dt_uri_mapping.received_order_id'), $arraySearch, $arraySort);
        $processData = $data->pluck('received_order_id');
        if (count($processData) > 0) {
            $chunkData = array_chunk($processData->toArray(), 500);
            foreach ($chunkData as $value) {
                $this->whereIn('received_order_id', $value)->update($arrayUpdate);
            }
            $chunkData = null;
        }
    }
}
