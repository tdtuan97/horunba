<?php
/**
 * Exe sql for hrnb.mst_cancel_reason
 *
 * @package    App\Models
 * @subpackage MstCancelReason
 * @copyright  Copyright (c) 2017 RiverCrane! Corporation. All Rights Reserved.
 * @author     van.qui<van.qui.rcvn2012@gmail.com>
 */

namespace App\Models\Backend;

use Illuminate\Database\Eloquent\Model;

class MstCancelReason extends Model
{
    public $timestamps = false;

    /**
    * The database table used by the model.
    * @var string
    */
    protected $table = 'mst_cancel_reason';

    /**
    * The primary key.
    * @var string
    */
    protected $primaryKey = 'reason_id';

    /**
    * The database is used by the model.
    * @var string
    */
    protected $connection = 'horunba';

    /**
     * Get data of tabe
     *
     * @return object
     */
    public function getData($arraySearch = null, $arraySort = null)
    {
        $data = $this->select('reason_id', 'type', 'reason_content', 'add_2_mail_text');
        if (count($arraySearch) > 0) {
            $data->where(function ($query) use ($arraySearch) {
                if (isset($arraySearch['type'])) {
                    $query->where('type', 'LIKE', "%{$arraySearch['type']}%");
                }
                if (isset($arraySearch['reason_content'])) {
                    $query->where('reason_content', 'LIKE', "%{$arraySearch['reason_content']}%");
                }
            });
        }
        $check = false;
        if ($arraySort !== null && count($arraySort) > 0) {
            foreach ($arraySort as $column => $sort) {
                if ($sort !== null && in_array($sort, ['asc', 'desc'])) {
                    $data->orderBy($column, $sort);
                    $check = true;
                }
            }
        }
        if (!$check) {
            $data->orderBy('in_date', 'desc');
        }
        $perPage = ($arraySearch['per_page']) ? $arraySearch['per_page'] : 20;
        $data = $data->paginate($perPage);
        return $data;
    }

    /**
     * Get item data
     *
     * @param  int  reason_id
     * @return object
     */
    public function getItem($reasonId)
    {
        $data = $this->select('reason_id', 'type', 'reason_content', 'add_2_mail_text')
                    ->where('reason_id', $reasonId)
                    ->first();
        return $data;
    }
}
