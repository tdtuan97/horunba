<?php

/**
 * Exe sql for hrnb.mst_uri_occur_reason
 *
 * @package    App\Models
 * @subpackage MstUriOccurReason
 * @copyright  Copyright (c) 2019 RiverCrane! Corporation. All Rights Reserved.
 * @author     Truong Nghia<truong.van.nghia@rivercrane.vn>
 */

namespace App\Models\Backend;

use Illuminate\Database\Eloquent\Model;

class MstUriOccurReason extends Model
{
    public $timestamps = false;
    /**
    * The database table used by the model.
    * @var string
    */
    protected $table = 'mst_uri_occur_reason';

    /**
    * The database is used by the model.
    * @var string
    */
    protected $connection = 'horunba';
}
