<?php

/**
 * Model for cheetah table.
 *
 * @package    App\Models\Backend
 * @subpackage Cheetah
 * @copyright  Copyright (c) 2018 RiverCrane! Corporation. All Rights Reserved.
 * @author     van.qui<van.qui.rcvn2012@gmail.com>
 */

namespace App\Models\Backend;

use Illuminate\Database\Eloquent\Model;

class Cheetah extends Model
{
    public $timestamps = false;

    /**
    * The database table used by the model.
    * @var string
    */
    protected $table = 'cheetah';

    /**
    * The database is used by the model.
    * @var string
    */
    protected $connection = 'api';

    /**
    * The primary key.
    * @var string
    */
    protected $primaryKey = 'product_code';

    /**
    * The type of primary key.
    * @var string
    */
    protected $keyType = 'string';
}
