<?php
/**
 * Model for scree table.
 *
 * @package    App\Models\Backend
 * @subpackage ScreenAllLog
 * @copyright  Copyright (c) 2018 RiverCrane! Corporation. All Rights Reserved.
 * @author     Lam Vinh<lam.vinh@rivercrane.vn>
 */

namespace App\Models\Backend;

use Illuminate\Database\Eloquent\Model;

class ScreenAllLog extends Model
{
    public $timestamps = false;

    /**
    * The database table used by the model.
    * @var string
    */
    protected $table = 'screen_all_log';

    /**
    * The database is used by the model.
    * @var string
    */
    protected $connection = 'horunba';
}
