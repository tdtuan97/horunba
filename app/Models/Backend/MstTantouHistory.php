<?php
/**
 * Exe sql for hrnb.mst_tantou_history
 *
 * @package    App\Models
 * @subpackage MstTantouHistory
 * @copyright  Copyright (c) 2018 RiverCrane! Corporation. All Rights Reserved.
 * @author     van.qui<van.qui.rcvn2012@gmail.com>
 */

namespace App\Models\Backend;

use Illuminate\Database\Eloquent\Model;

class MstTantouHistory extends Model
{
    public $timestamps = false;
    /**
    * The database table used by the model.
    * @var string
    */
    protected $table = 'mst_tantou_history';
    /**
    * The database is used by the model.
    * @var string
    */
    protected $connection = 'horunba';

    /**
    * The type of primary key.
    * @var string
    */
    protected $keyType = 'string';

    /**
    * The name of primary key.
    * @var string
    */
    protected $primaryKey = 'token_id';
}
