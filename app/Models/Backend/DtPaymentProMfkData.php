<?php

/**
 * Exe sql for hrnb.dt_payment_pro_mfk_data
 *
 * @package    App\Models
 * @subpackage DtPaymentProMfkData
 * @copyright  Copyright (c) 2018 RiverCrane! Corporation. All Rights Reserved.
 * @author     Truong Nghia<truong.van.nghia@rivercrane.vn>
 */

namespace App\Models\Backend;

use Illuminate\Database\Eloquent\Model;

class DtPaymentProMfkData extends Model
{
    public $timestamps = false;
    /**
    * The database table used by the model.
    * @var string
    */
    protected $table = 'dt_payment_pro_mfk_data';
    /**
    * The database is used by the model.
    * @var string
    */
    protected $connection = 'horunba';
    /**
     * Get data process payment fregi
     * @return object
     */
    public function getDataProcessProMFK()
    {
        $col = [
            'dt_payment_pro_mfk_data.order_id',
            'dt_payment_pro_mfk_data.request_price',
            'dt_payment_pro_mfk_data.status',
        ];
        $result = $this->select($col)
                       ->where('dt_payment_pro_mfk_data.process_flg', 0)
                       ->where('dt_payment_pro_mfk_data.is_delete', 0)
                       ->groupBy('dt_payment_pro_mfk_data.order_id')
                       ->get();
        return $result;
    }
}
