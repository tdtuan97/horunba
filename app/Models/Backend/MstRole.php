<?php

/**
 * Exe sql for hrnb.mst_role
 *
 * @package    App\Models\Backend
 * @subpackage MstRole
 * @copyright  Copyright (c) 2018 RiverCrane! Corporation. All Rights Reserved.
 * @author     Truong Nghia<truong.van.nghia@rivercrane.vn>
 */

namespace App\Models\Backend;

use Illuminate\Database\Eloquent\Model;

class MstRole extends Model
{
    public $timestamps = false;
    /**
    * The database table used by the model.
    * @var string
    */
    protected $table = 'mst_role';

    /**
    * The database is used by the model.
    * @var string
    */
    protected $connection = 'horunba';

    /**
    * Get role by condition.
    * @param string $role
    * @param string $controller
    * @param string $action
    * @return object
    */
    public function getRole($role, $controller, $action)
    {
        $result = $this->where('role', 'like', '%' . $role . '%')
                       ->where('controller', '=', $controller)
                       ->where('action', '=', $action)
                       ->first();
        return $result;
    }

    /**
    * Get role check.
    * @param string $role
    * @return object
    */
    public function getRoleCheck($role)
    {
        $result = $this->where('role', 'like', '%' . $role . '%')
                       ->where('access', 1)
                       ->count();
        return $result;
    }
}
