<?php

/**
 * Model for mst_mall table.
 *
 * @package    App\Models\Batches
 * @subpackage MstStatusBatch
 * @copyright  Copyright (c) 2017 RiverCrane! Corporation. All Rights Reserved.
 * @author     Truong Nghia<truong.van.nghia@rivercrane.vn>
 */

namespace App\Models\Backend;

use Illuminate\Database\Eloquent\Model;

class MstStatusBatch extends Model
{
    public $timestamps = false;

    /**
    * The database table used by the model.
    * @var string
    */
    protected $table = 'mst_status_batch';

    /**
    * The database is used by the model.
    * @var string
    */
    protected $connection = 'horunba';
}
