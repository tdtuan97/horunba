<?php
/**
 * Exe sql for hrnb.dt_store_order_detail
 *
 * @package    App\Models
 * @subpackage DtStoreOrderDetail
 * @copyright  Copyright (c) 2019 RiverCrane! Corporation. All Rights Reserved.
 * @author     van.qui<van.qui.rcvn2012@gmail.com>
 */

namespace App\Models\Backend;

use Illuminate\Database\Eloquent\Model;

class DtStoreOrderDetail extends Model
{
    public $timestamps = false;
    /**
    * The database table used by the model.
    * @var string
    */
    protected $table = 'dt_store_order_detail';
    /**
    * The database is used by the model.
    * @var string
    */
    protected $connection = 'horunba';


    /**
     * Get data of tabe
     *
     * @param  array  $arraySearch
     * @param  array  $arraySort
     * @return object
     */
    public function getDetail($orderId)
    {
        $data = $this->select(
            'order_id',
            'product_code',
            'product_name',
            'quantity',
            'process_status',
            'error_code',
            'error_message'
        )->where('order_id', $orderId)->paginate(20);
        return $data;
    }
}
