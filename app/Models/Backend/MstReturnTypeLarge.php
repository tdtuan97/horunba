<?php

/**
 * Exe sql for hrnb.mst_return_type_large
 *
 * @package    App\Models\Backend
 * @subpackage MstReturnTypeLarge
 * @copyright  Copyright (c) 2018 RiverCrane! Corporation. All Rights Reserved.
 * @author     Truong Nghia<truong.van.nghia@rivercrane.vn>
 */

namespace App\Models\Backend;

use Illuminate\Database\Eloquent\Model;

class MstReturnTypeLarge extends Model
{
    public $timestamps = false;
    /**
    * The database table used by the model.
    * @var string
    */
    protected $table = 'mst_return_type_large';

    /**
    * The database is used by the model.
    * @var string
    */
    protected $connection = 'horunba';

    /**
    * Get all data.
    * @return object
    */
    public function getData()
    {
        $result = $this->select('type_large_id AS key', 'type_large_name AS value')->get();
        return $result;
    }
}
