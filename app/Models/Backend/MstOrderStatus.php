<?php

/**
 * Exe sql for hrnb.mst_order_status
 *
 * @package    App\Models
 * @subpackage MstOrderStatus
 * @copyright  Copyright (c) 2017 RiverCrane! Corporation. All Rights Reserved.
 * @author     le.hung<le.hung.rcvn2012@gmail.com>
 */

namespace App\Models\Backend;

use Illuminate\Database\Eloquent\Model;

class MstOrderStatus extends Model
{
    public $timestamps = false;
    /**
    * The database table used by the model.
    * @var string
    */
    protected $table = 'mst_order_status';
    /**
    * The database is used by the model.
    * @var string
    */
    protected $connection = 'horunba';

    /**
     * Get data of tabe mst_order_status
     *
     * @return object
     */
    public function getData($arraySearch = null, $arraySort = null)
    {
        $data = $this->select('*');
        if (count($arraySearch) > 0) {
            $data->where(function ($query) use ($arraySearch) {
                if (isset($arraySearch['mail_id'])) {
                    $query->where('mail_id', "{$arraySearch['mail_id']}");
                }
                if (isset($arraySearch['mail_subject'])) {
                    $query->where('mail_subject', 'LIKE', "{$arraySearch['mail_subject']}%");
                }
                if (isset($arraySearch['mail_contents'])) {
                    $query->where('mail_contents', 'LIKE', "{$arraySearch['mail_contents']}%");
                }
                if (isset($arraySearch['template_name'])) {
                    $query->where('template_name', 'LIKE', "{$arraySearch['template_name']}%");
                }
                if (isset($arraySearch['genre'])) {
                    $query->where('mst_genre.name', 'LIKE', "{$arraySearch['genre']}%");
                }
            });
        }
        if ($arraySort !== null && count($arraySort) > 0) {
            if (!is_array($arraySort)) {
                $arraySort = json_decode($arraySort);
            }
            foreach ($arraySort as $column => $sort) {
                $data->orderBy($column, $sort);
            }
        }
        $perPage = ($arraySearch['per_page']) ? $arraySearch['per_page'] : 20;
        $data = $data->paginate($perPage);
        return $data;
    }
    /**
     * Get data of tabe mst_order_status
     *
     * @return object
     */
        /*
     * Get Item by primary key
     *
     * @param   int   $orderStatusId    order_status_id
     * @param   int   $orderSubStatusId order_sub_status_id
     * @return  object
     */
    public function getItem($where)
    {
        $data = $this->where($where)
                    ->first();
        return $data;
    }


    /**
     * Update data
     *
     * @param  array $keys
     * @param  array $data
     * @return boolean
     */
    public function updateData($keys, $data)
    {
        $result = $this->where($keys)
                        ->update($data);
        return true;
    }
    /**
     * Get data of tabe mst_order_status
     *
     * @return object
     */
    public function getDataSelectBox()
    {
        $data = $this->select('order_status_id', 'status_name')->get();
        return $data;
    }
    /**
     * Get data of tabe mst_order_status
     *
     * @return object
     */
    public function getDataSelect()
    {
        $data = $this->select('order_status_id AS key', 'status_name AS value')->get();
        return $data;
    }
}
