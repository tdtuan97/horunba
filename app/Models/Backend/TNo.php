<?php
/**
 * Exe sql for edi.t_no
 *
 * @package App\Models\Edi
 * @subpackage EdiModel
 * @copyright Copyright (c) 2017 CriverCrane! Corporation. All Rights Reserved.
 * @author lam.vinh<lam.vinh@crivercrane.vn>
 * @clone  le.hung<le.hung.rcvn@gmail.com>
 */
namespace App\Models\Backend;

use Illuminate\Database\Eloquent\Model;

class TNo extends Model
{

    public $timestamps = false;
    /**
    * The database table used by the model.
    * @var string
    */
    protected $table = 't_no';
    /**
    * The database is used by the model.
    * @var string
    */
    protected $connection = 'edi';

    /**
     * Insert new data to table t no
     * @return int
     */
    public function insertData()
    {
        $insertdata['created_at'] = date('Y-m-d H:i:s');
        $id =  $this->insertGetId($insertdata);
        $id = substr(date('Y'), 3, 1).date("md").str_pad($id, 5, 0, STR_PAD_LEFT);
        return $id;
    }
}
