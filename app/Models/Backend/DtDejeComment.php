<?php

/**
 * Exe sql for hrnb.dt_deje_data
 *
 * @package    App\Models
 * @subpackage DtDejeComment
 * @copyright  Copyright (c) 2017 RiverCrane! Corporation. All Rights Reserved.
 * @author     le.hung<le.hung.rcvn2012@gmail.com>
 */

namespace App\Models\Backend;

use Illuminate\Database\Eloquent\Model;
use DB;

class DtDejeComment extends Model
{
    public $timestamps = false;
    /**
    * The database table used by the model.
    * @var string
    */
    protected $table = 'dt_deje_comment_data';
    /**
    * The database is used by the model.
    * @var string
    */
    protected $connection = 'horunba';
    /**
    * The primary key.
    * @var string
    */
    protected $primaryKey = 'comment_id';
    /**
     * Get data of table dt_deje_data_comment
     *
     * @params  array    $arraySearch array data search
     * @params  array    $arraySort   array data sort
     * @return object
     */
    public function getData($arraySearch = null, $arraySort = null)
    {
        $data = $this->select('dt_deje_comment_data.*')
                ->join('mst_tantou', 'mst_tantou.tantou_code', '=', 'dt_deje_comment_data.in_ope_cd')
                ->addSelect(DB::raw("CONCAT(mst_tantou.tantou_first_name,' ',mst_tantou.tantou_last_name) AS full_name"))
                ->where($arraySearch)
                ->orderBy('in_date', 'DESC')
                ->get();
        return $data;
    }
}
