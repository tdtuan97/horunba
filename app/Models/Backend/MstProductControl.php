<?php

/**
 * Exe sql for hrnb.mst_product_control
 *
 * @package    App\Models\Backend
 * @subpackage MstProductControl
 * @copyright  Copyright (c) 2019 RiverCrane! Corporation. All Rights Reserved.
 * @author     Van Qui<van.qui@crivercrane.vn>
 */

namespace App\Models\Backend;

use DB;
use Illuminate\Database\Eloquent\Model;

class MstProductControl extends Model
{
    public $timestamps = false;
    /**
    * The database table used by the model.
    * @var string
    */
    protected $table = 'mst_product_control';

    /**
    * The database is used by the model.
    * @var string
    */
    protected $connection = 'master_plus';

    /**
     * Get data process up smaregi
     *
     * @param  string $productCode
     * @return object
     */
    public function getDataProcessUpSmaregi($productCode)
    {
        $col = [
            'mst_product_control.product_code',
            'mst_product_control.df_handling',
            'mst_product_base.product_jan',
            'dt_product.product_name_long',
            'dt_price_mall.price_calc_diy_nuki',
            'dt_price_supplier.price_hanbai',
            'dt_price_supplier.price_invoice',
            'mst_product_base.product_size',
            'mst_product_base.product_color',
            'mst_store_product.store_product_id',
            'mst_store_product.in_date',
        ];
        $result = $this->select($col)
                       ->join('mst_product_base', 'mst_product_base.product_code', '=', 'mst_product_control.product_code')
                       ->join('dt_product', 'dt_product.product_code', '=', 'mst_product_control.product_code')
                       ->join('dt_price_supplier', 'dt_price_supplier.product_code', '=', 'mst_product_base.product_code')
                       ->leftJoin('dt_price_mall', 'dt_price_mall.product_code', '=', 'mst_product_base.product_code')
                       ->leftJoin('hrnb.mst_store_product', 'mst_store_product.product_code', '=', 'mst_product_control.product_code')
                       ->where('mst_product_control.product_code', $productCode)
                       ->first();
        return $result;
    }
}
