<?php
/**
 * Exe sql for hrnb.mst_postal
 *
 * @package    App\Models
 * @subpackage MstPostal
 * @copyright  Copyright (c) 2018 RiverCrane! Corporation. All Rights Reserved.
 * @author     le.hung<le.hung.rcvn2012@gmail.com>
 */

namespace App\Models\Backend;

use Illuminate\Database\Eloquent\Model;

class MstPostal extends Model
{
    public $timestamps = false;

    /**
    * The database table used by the model.
    * @var string
    */
    protected $table = 'mst_postal';

    /**
    * The primary key.
    * @var string
    */
    protected $primaryKey = 'index';
    /**
    * The type of primary key.
    * @var string
    */
    protected $keyType = 'string';
    /**
    * The database is used by the model.
    * @var string
    */
    protected $connection = 'horunba';

    /**
     * Get data of tabe
     *
     * @return object
     */
    public function getData($arraySearch = null, $arraySort = null)
    {
        $data = $this->select(
            'index',
            'postal_code',
            'prefecture',
            'city',
            'sub_address',
            'full_address',
            'jis_code',
            'no_count',
            'one_area_n_pos',
            'one_pos_n_area',
            'add_ship_charge',
            'can_not_daihiki',
            'can_not_order_time',
            'sagawa_code',
            'dis_pos_code',
            'deli_days',
            'in_ope_cd',
            'in_date',
            'up_ope_cd',
            'up_date'
        );
        if (count($arraySearch) > 0) {
            $data->where(function ($query) use ($arraySearch) {
                if (isset($arraySearch['postal_code'])) {
                    $query->where('postal_code', 'LIKE', "%{$arraySearch['postal_code']}%");
                }
                if (isset($arraySearch['prefecture'])) {
                    $query->where('prefecture', 'LIKE', "%{$arraySearch['prefecture']}%");
                }
                if (isset($arraySearch['city'])) {
                    $query->where('city', 'LIKE', "%{$arraySearch['city']}%");
                }
                if (isset($arraySearch['one_area_n_pos'])) {
                    if (is_array($arraySearch['one_area_n_pos'])) {
                        $query->whereIn('one_area_n_pos', $arraySearch['one_area_n_pos']);
                    } else {
                        $query->where('one_area_n_pos', $arraySearch['one_area_n_pos']);
                    }
                }
                if (isset($arraySearch['one_pos_n_area'])) {
                    if (is_array($arraySearch['one_pos_n_area'])) {
                        $query->whereIn('one_pos_n_area', $arraySearch['one_pos_n_area']);
                    } else {
                        $query->where('one_pos_n_area', $arraySearch['one_pos_n_area']);
                    }
                }
                if (isset($arraySearch['can_not_daihiki'])) {
                    if (is_array($arraySearch['can_not_daihiki'])) {
                        $query->whereIn('can_not_daihiki', $arraySearch['can_not_daihiki']);
                    } else {
                        $query->where('can_not_daihiki', $arraySearch['can_not_daihiki']);
                    }
                }
                if (isset($arraySearch['can_not_order_time'])) {
                    if (is_array($arraySearch['can_not_order_time'])) {
                        $query->whereIn('can_not_order_time', $arraySearch['can_not_order_time']);
                    } else {
                        $query->where('can_not_order_time', $arraySearch['can_not_order_time']);
                    }
                }
                if (isset($arraySearch['deli_days'])) {
                    $query->where('deli_days', '=', "{$arraySearch['deli_days']}");
                }
                if (isset($arraySearch['add_ship_charge'])) {
                    if (is_array($arraySearch['add_ship_charge'])) {
                        $query->whereIn('add_ship_charge', $arraySearch['add_ship_charge']);
                    } else {
                        $query->where('add_ship_charge', $arraySearch['add_ship_charge']);
                    }
                }
            });
        }
        if ($arraySort !== null && count($arraySort) > 0) {
            foreach ($arraySort as $column => $sort) {
                if ($sort !== null && in_array($sort, ['asc', 'desc'])) {
                    $data->orderBy($column, $sort);
                }
            }
        } elseif (empty($arraySort)) {
            $data->orderBy('index', 'desc');
        }

        $perPage = ($arraySearch['per_page']) ? $arraySearch['per_page'] : 20;
        $data = $data->paginate($perPage);
        return $data;
    }

    /**
     * Get add ship charge items
     *
     * @return object
     */
    public function getShipCharge()
    {
        $data = $this->select('add_ship_charge')
                    ->groupBy('add_ship_charge')
                    ->get();
        return $data;
    }
    /**
     * Get item data
     *
     * @param  int  index
     * @return object
     */
    public function getItem($index)
    {
        $data = $this->select('*')
                    ->where('index', $index)
                    ->first();
        return $data;
    }

    /**
     * Update data
     *
     * @param  array $dataWhere for condition
     * @param  array $dataUpdate for update
     * @return object
     */
    public function updateData($dataWhere, $dataUpdate)
    {
        $data = $this->where($dataWhere)->update($dataUpdate);
        return $data;
    }
}
