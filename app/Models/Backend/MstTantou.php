<?php

/**
 * Model for mst_tantou table.
 *
 * @package    App\Models
 * @subpackage MstTantou
 * @copyright  Copyright (c) 2017 RiverCrane! Corporation. All Rights Reserved.
 * @author     Truong Nghia<truong.van.nghia@rivercrane.vn>
 */

namespace App\Models\Backend;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class MstTantou extends Authenticatable
{
    use Notifiable;

    /**
    * The database table used by the model.
    * @var string
    */
    protected $table = 'mst_tantou';

    /**
    * The timestamps.
    * @var boolean
    */
    public $timestamps = false;

    /**
    * The name of primary key.
    * @var string
    */
    protected $primaryKey = 'tantou_code';

    /**
    * The type of primary key.
    * @var string
    */
    protected $keyType = 'string';

    /**
     * Ignore show and modify this admin
     * @var string
     */
    private $ignoreTantouCode = 'OPE99999';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'tantou_code', 'tantou_last_name', 'tantou_first_name', 'tantou_mail', 'tantou_password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'tantou_password', 'remember_token',
    ];

    /**
     * Get the password for the user.
     *
     * @return string
     */
    public function getAuthPassword()
    {
        return $this->tantou_password;
    }

    /**
     * Get data list
     *
     * @return object
     */
    public function getData($arraySearch = null, $arraySort = null)
    {
        $data = $this->select('tantou_code', 'tantou_last_name', 'tantou_first_name', 'tantou_tel')
                    ->addSelect('tantou_mail', 'tantou_role', 'tantou_department', 'is_active', 'depart_nm')
                    ->leftJoin('mst_permission', 'mst_tantou.tantou_department', '=', 'mst_permission.depart_id');
        if (count($arraySearch) > 0) {
            $data->where(function ($query) use ($arraySearch) {
                if (isset($arraySearch['tantou_code'])) {
                    $query->where('tantou_code', 'LIKE', "%{$arraySearch['tantou_code']}%");
                }
                if (isset($arraySearch['tantou_last_name'])) {
                    $query->where('tantou_last_name', 'LIKE', "%{$arraySearch['tantou_last_name']}%");
                }
                if (isset($arraySearch['tantou_first_name'])) {
                    $query->where('tantou_first_name', 'LIKE', "%{$arraySearch['tantou_first_name']}%");
                }
                if (isset($arraySearch['tantou_department'])) {
                    if (is_array($arraySearch['tantou_department'])) {
                        $query->whereIn('tantou_department', $arraySearch['tantou_department']);
                    } else {
                        $query->where('tantou_department', $arraySearch['tantou_department']);
                    }
                }
                if (isset($arraySearch['tantou_role'])) {
                    if (is_array($arraySearch['tantou_role'])) {
                        $query->whereIn('tantou_role', $arraySearch['tantou_role']);
                    } else {
                        $query->where('tantou_role', $arraySearch['tantou_role']);
                    }
                }
            });
        }
        $data->where('tantou_code', '<>', $this->ignoreTantouCode)
             ->groupBy('tantou_code');
        if ($arraySort !== null && count($arraySort) > 0) {
            foreach ($arraySort as $column => $sort) {
                if ($sort !== null && in_array($sort, ['asc', 'desc'])) {
                    $data->orderBy($column, $sort);
                }
            }
        } else {
            $data->orderBy('in_date', 'desc');
        }
        $perPage = ($arraySearch['per_page']) ? $arraySearch['per_page'] : 20;
        $data = $data->paginate($perPage);
        return $data;
    }

    /**
     * Get tantou role
     *
     * @return object
     */
    public function getTantouRole()
    {
        $data = $this->select('tantou_role')->distinct()->get();
        return $data;
    }

    /**
     * Get tantou department
     *
     * @return object
     */
    public function getTantouDepartment()
    {
        $data = $this->select('mst_tantou.tantou_department', 'mst_permission.depart_nm')
                     ->join('mst_permission', 'mst_tantou.tantou_department', '=', 'mst_permission.depart_id')
                     ->distinct()->get();
        return $data;
    }

    /**
     * Change is_active
     *
     * @param  string $tantouCode
     * @param  int    $isActive
     * @return boolean
     */
    public function changeActive($tantouCode, $isActive)
    {
        return $this->where('tantou_code', $tantouCode)
                ->where('tantou_code', '<>', $this->ignoreTantouCode)
                ->update(['is_active' => $isActive]);
    }

    /**
     * Get item by tantou code
     *
     * @param  string $tantouCode
     * @return object
     */
    public function getItem($tantouCode)
    {
        $data = $this->select('tantou_code', 'tantou_last_name', 'tantou_first_name', 'tantou_department')
                    ->addSelect('tantou_role', 'tantou_tel', 'tantou_mail', 'is_active')
                    ->where('tantou_code', $tantouCode)
                    ->where('tantou_code', '<>', $this->ignoreTantouCode)
                    ->first();
        return $data;
    }

    /**
     * Get max item by tantou_code
     *
     * @return object
     */
    public function getMaxTantouCode()
    {
        $data = $this->select('tantou_code')
                    ->where('tantou_code', '<>', $this->ignoreTantouCode)
                    ->orderByRaw('CAST(SUBSTRING(tantou_code, 4) AS UNSIGNED) DESC')
                    ->first();
        if ($data !== null) {
            return $data->tantou_code;
        }
        return null;
    }
    /**
     * Get data of tabe mst tantou
     *
     * @return object
     */
    public function getDataSelectBox()
    {
        $data = $this->select('tantou_code AS key', 'tantou_last_name AS value')->get();
        return $data;
    }

    /**
     * Get data of tabe mst tantou
     * @param array arrKeys
     * @return object
     */
    public function getDataCheckPermissionByKey($arrKeys)
    {
        $permission = "IF(mst_tantou.tantou_role = 0,mst_permission.func_cd,mst_permission.func_cap_cd) as permission";
        $data = $this->select('mst_controller.action_list', 'action_access', 'no_list', 'mst_controller.screen_name', 'mst_permission.action_denied')
                     ->selectRaw($permission)
                     ->join('mst_permission', 'mst_tantou.tantou_department', '=', 'mst_permission.depart_id')
                     ->leftjoin('mst_controller', 'mst_permission.screen_class', '=', 'mst_controller.id')
                     ->where($arrKeys)
                     ->first();
        return $data;
    }

    /**
     * Get data redirect
     * @param string $tantouCode
     * @return object
     */
    public function getDataRedirect($tantouCode)
    {
        $data = $this->select('mst_controller.screen_class', 'mst_controller.action_list')
                     ->join('mst_permission', 'mst_tantou.tantou_department', '=', 'mst_permission.depart_id')
                     ->leftjoin('mst_controller', 'mst_permission.screen_class', '=', 'mst_controller.id')
                     ->where('mst_tantou.tantou_code', $tantouCode)
                     ->whereNotNull('mst_controller.action_list')
                     ->whereRaw('IF(mst_tantou.tantou_role = 0,mst_permission.func_cd,mst_permission.func_cap_cd) IN (1,2)')
                     ->first();
        if (empty($data)) {
            return false;
        } else {
            $arrIndex = explode(',', $data->action_list);
            return action($data->screen_class . '@' . $arrIndex[0]);
        }
    }
}
