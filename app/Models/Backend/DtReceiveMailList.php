<?php
/**
 * Exe sql for hrnb.dt_receive_mail_list
 *
 * @package    App\Models
 * @subpackage DtReceiveMailList
 * @copyright  Copyright (c) 2018 RiverCrane! Corporation. All Rights Reserved.
 * @author     van.qui<van.qui.rcvn2012@gmail.com>
 */

namespace App\Models\Backend;

use Illuminate\Database\Eloquent\Model;
use App\Custom\NextPrevModel;

class DtReceiveMailList extends Model
{
    use NextPrevModel;

    public $timestamps = false;

    /**
    * The database table used by the model.
    * @var string
    */
    protected $table = 'dt_receive_mail_list';

    /**
    * The database is used by the model.
    * @var string
    */
    protected $connection = 'horunba';

    /**
    * The primary key.
    * @var string
    */
    protected $primaryKey = 'rec_mail_index';

    /**
     * Get data list
     *
     * @return object
     */
    public function getData($arraySearch = null, $arraySort = null)
    {
        $data = $this->select(
            'rec_mail_index',
            'status',
            'is_reply',
            'deparment',
            'incharge_person',
            'mail_subject',
            'mail_content',
            'receive_date',
            'mail_from'
        );

        $data = $this->conditionQuery($data, $arraySearch, $arraySort);
        $perPage = ($arraySearch['per_page']) ? $arraySearch['per_page'] : 20;
        $data = $data->paginate($perPage);
        return $data;
    }

    /**
     * Condition query
     *
     * @param  object  $data
     * @param  array   $arraySearch
     * @param  array   $arraySort
     * @return object
     */
    public function conditionQuery($data, $arraySearch = null, $arraySort = null)
    {
        $data = $data;
        if (count($arraySearch) > 0) {
            $data->where(function ($query) use ($arraySearch) {
                if (isset($arraySearch['status'])) {
                    if (is_array($arraySearch['status'])) {
                        $query->whereIn('status', $arraySearch['status']);
                    } else {
                        $query->where('status', $arraySearch['status']);
                    }
                }
                if (isset($arraySearch['is_reply'])) {
                    if (is_array($arraySearch['is_reply'])) {
                        $query->whereIn('is_reply', $arraySearch['is_reply']);
                    } else {
                        $query->where('is_reply', $arraySearch['is_reply']);
                    }
                }

                if (!empty($arraySearch['deparment'])) {
                    if (is_array($arraySearch['deparment'])) {
                        $query->whereIn('deparment', $arraySearch['deparment']);
                    } else {
                        $query->where('deparment', $arraySearch['deparment']);
                    }
                }

                if (!empty($arraySearch['incharge_person'])) {
                    $query->where('incharge_person', 'LIKE', "%{$arraySearch['incharge_person']}%");
                }
                if (!empty($arraySearch['mail_subject'])) {
                    $query->where('mail_subject', 'LIKE', "%{$arraySearch['mail_subject']}%");
                }
                if (!empty($arraySearch['receive_date_from'])) {
                    $dateFrom = date("Y-m-d 00:00:00", strtotime($arraySearch['receive_date_from']));
                    $query->where(
                        'receive_date',
                        '>=',
                        $dateFrom
                    );
                }
                if (!empty($arraySearch['receive_date_to'])) {
                    $dateTo = date("Y-m-d 23:59:59", strtotime($arraySearch['receive_date_to']));
                    $query->where(
                        'receive_date',
                        '<=',
                        $dateTo
                    );
                }

                if (!empty($arraySearch['mail_from'])) {
                    $query->where('mail_from', 'LIKE', "%{$arraySearch['mail_from']}%");
                }
                if (!empty($arraySearch['mail_to'])) {
                    $query->where('mail_to', 'LIKE', "%{$arraySearch['mail_to']}%");
                }
            });
        }
        if ($arraySort !== null && count($arraySort) > 0) {
            foreach ($arraySort as $column => $sort) {
                if ($sort !== null && in_array($sort, ['asc', 'desc'])) {
                    $data =  $data->orderBy($column, $sort);
                }
            }
        } else {
            $data = $data->orderBy('in_date', 'desc');
        }
        $data = $data->orderBy('rec_mail_index', 'desc');
        return $data;
    }
    /**
     * Update all record by condition
     *
     * @return object
     */
    public function updateAll($arraySearch, $arraySort = null, $arrayUpdate = null)
    {
        $data = $this->conditionQuery($this->select('dt_receive_mail_list.rec_mail_index'), $arraySearch, null);
        $processData = $data->pluck('rec_mail_index');
        if (count($processData) > 0) {
            $chunkData = array_chunk($processData->toArray(), 500);
            foreach ($chunkData as $value) {
                $this->whereIn('rec_mail_index', $value)->update($arrayUpdate);
            }
            $chunkData = null;
        }
    }
    /**
     * Get item by primary key
     *
     * @param  int    $recMailIndex
     * @return object
     */
    public function getItem($recMailIndex)
    {
        $data = $this->select(
            'rec_mail_index',
            'mail_from',
            'mail_to',
            'mail_subject',
            'mail_content',
            'attached_file_path1',
            'attached_file_path2',
            'attached_file_path3',
            'receive_order_id',
            'status',
            'deparment',
            'incharge_person',
            'receive_date',
            'inquiry_code',
            'cs_note',
            'mst_order.receive_id',
            'uid_mail'
        )
        ->where('dt_receive_mail_list.rec_mail_index', $recMailIndex)
        ->leftJoin('mst_order', 'mst_order.received_order_id', '=', 'dt_receive_mail_list.receive_order_id')
        ->first();
        return $data;
    }

    /**
     * Get data by receive_order_id
     *
     * @param   string  $receiveOrderId
     * @return  object
     */
    public function getDataByReceive($receiveOrderId)
    {
        $data = $this->select('rec_mail_index', 'receive_date', 'status', 'mail_subject')
            ->where('receive_order_id', $receiveOrderId)
            ->get();
        return $data;
    }
    /**
     * Get data by receive mail index
     *
     * @param  int $mailIndex
     * @return object
     */
    public function getDataByMailIndex($mailIndex)
    {
        $data = $this->where('rec_mail_index', $mailIndex)->first();
        return $data;
    }

    /**
     * Update data by key
     *
     * @param  array   $keys
     * @param  array   $data
     * @return boolean
     */
    public function updateData($keys, $data)
    {
        return $this->where($keys)->update($data);
    }
}
