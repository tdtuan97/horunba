<?php
/**
 * Model for mst_claim table.
 *
 * @package    App\Models\Backend
 * @subpackage MstClaim
 * @copyright  Copyright (c) 2019 RiverCrane! Corporation. All Rights Reserved.
 * @author     Truong Nghia<truong.van.nghia@crivercrane.vn>
 */

namespace App\Models\Backend;

use Illuminate\Database\Eloquent\Model;

class MstClaim extends Model
{
    public $timestamps = false;

    /**
     * The database table used by the model.
     * @var string
     */
    protected $table = 'mst_claim';

    /**
     * The database is used by the model.
     * @var string
     */
    protected $connection = 'horunba';

    /**
     * Update data by key
     *
     * @param  array   $keys
     * @param  array   $data
     * @return boolean
     */
    public function updateData($keys, $data)
    {
        return $this->where($keys)->update($data);
    }
}
