<?php

/**
 * Exe sql for hrnb.dt_repayment
 *
 * @package    App\Models
 * @subpackage DtRepayment
 * @copyright  Copyright (c) 2018 RiverCrane! Corporation. All Rights Reserved.
 * @author     Le Hung<le.hung@rivercrane.vn>
 */

namespace App\Models\Backend;

use Barryvdh\Debugbar\Facade as Debugbar;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Auth;

class DtRePayment extends Model
{
    public $timestamps = false;
    /**
    * The database table used by the model.
    * @var string
    */
    protected $table = 'dt_repayment';
    /**
    * The database is used by the model.
    * @var string
    */
    protected $connection = 'horunba';
    /**
    * Set default primary key.
    * @var int
    */
    protected $primaryKey = 'repay_no';

   /**
     * Get data of tabe
     *
     * @return object
     */
    public function getData($arraySearch = null, $arraySort = null, $arrayOptions = null)
    {
        $data = $this->select(
            'repay_no',
            'repay_name',
            'repay_price',
            'repay_bank_code',
            'repayment_date',
            'repay_bank_name',
            'repay_bank_branch_name',
            'repay_bank_branch_code',
            'repay_bank_account',
            'repayment_status',
            'repayment_type',
            'repayment_date',
            'total_return_price',
            'dt_repayment.receive_id',
            'dt_repayment.in_ope_cd',
            'dt_repayment.in_date',
            'dt_repayment.up_ope_cd',
            'dt_repayment.up_date',
            'is_return',
            'name_jp',
            'mst_order.received_order_id'
        )
        ->addSelect('mst_order.request_price')
        ->addSelect(DB::raw("CONCAT(mst_customer.first_name,' ',mst_customer.last_name) AS full_name"))
        ->leftJoin('mst_order', 'mst_order.receive_id', '=', 'dt_repayment.receive_id')
        ->leftJoin('mst_customer', 'mst_customer.customer_id', '=', 'mst_order.customer_id')
        ->leftJoin('mst_mall', 'mst_mall.id', '=', 'mst_order.mall_id');
        if (count($arraySearch) > 0) {
            $data->where(function ($query) use ($arraySearch) {
                if (isset($arraySearch['repay_name'])) {
                    $query->where('repay_name', 'LIKE', "%{$arraySearch['repay_name']}%");
                }
                if (isset($arraySearch['repay_bank_name'])) {
                    $query->where('repay_bank_name', 'LIKE', "%{$arraySearch['repay_bank_name']}%");
                }
                if (isset($arraySearch['repay_bank_branch_name'])) {
                    $query->where('repay_bank_branch_name', 'LIKE', "%{$arraySearch['repay_bank_branch_name']}%");
                }
                if (isset($arraySearch['repay_bank_account'])) {
                    $query->where('repay_bank_account', 'LIKE', "%{$arraySearch['repay_bank_account']}%");
                }
                if (isset($arraySearch['repayment_status'])) {
                    if (is_array($arraySearch['repayment_status'])) {
                        if (count($arraySearch['repayment_status']) > 0) {
                            $query->whereIn('repayment_status', $arraySearch['repayment_status']);
                        }
                    } else {
                        $query->where('repayment_status', '=', "{$arraySearch['repayment_status']}");
                    }

                }
                if (isset($arraySearch['received_order_id'])) {
                    $query->where('received_order_id', '=', "{$arraySearch['received_order_id']}");
                }
                if (isset($arraySearch['repay_date_from'])) {
                    $dateFrom = date("Y-m-d 00:00:00", strtotime($arraySearch['repay_date_from']));
                    $query->where('repayment_date', '>=', $dateFrom);
                }
                if (isset($arraySearch['repay_date_to'])) {
                    $dateTo = date("Y-m-d 23:59:59", strtotime($arraySearch['repay_date_to']));
                    $query->where('repayment_date', '<=', $dateTo);
                }
            });
        }
        if ($arraySort !== null && count($arraySort) > 0) {
            foreach ($arraySort as $column => $sort) {
                if ($sort !== null && in_array($sort, ['asc', 'desc'])) {
                    $data->orderBy($column, $sort);
                }
            }
        } else {
            $data->orderBy('repay_no', 'desc');
        }
        $perPage = ($arraySearch['per_page']) ? $arraySearch['per_page'] : 20;
        Debugbar::info($data->get());
        if (isset($arrayOptions['type'])) {
            $data = $data->get();
        } else {
            $data = $data->paginate($perPage);
        }
        return $data;
    }

    /**
     * Get info repayment by repay_no
     *
     * @param  string $index
     * @return object
     */
    public function getRePaymentById($repay_no)
    {
        $data = $this->select(
            'repay_no',
            'repay_name',
            'repay_price',
            'repayment_date',
            'repay_bank_name',
            'repay_bank_code',
            'repay_bank_branch_name',
            'repay_bank_branch_code',
            'repay_bank_account',
            'repayment_status',
            'repayment_type',
            'return_fee',
            'dt_repayment.return_coupon',
            'deposit_transfer_commission',
            'other_commission',
            'total_return_price',
            'repayment_date',
            'dt_repayment.return_point',
            'dt_repayment.is_return',
            'dt_repayment.receive_id',
            'dt_repayment.in_ope_cd',
            'dt_repayment.in_date',
            'dt_repayment.up_ope_cd',
            'dt_repayment.up_date',
            'mst_order.received_order_id'
        )
        ->addSelect('mst_cod_fee.cod_fee')
        ->addSelect('mst_order.order_status')
        ->addSelect('mst_order.total_price')
        ->addSelect('mst_order.request_price')
        ->addSelect('mst_order.pay_charge_discount')
        ->addSelect('mst_order.used_point')
        ->addSelect('mst_order.pay_after_charge')
        ->addSelect('mst_order.ship_charge')
        ->addSelect('mst_order.discount')
        ->addSelect('mst_order.pay_charge')
        ->addSelect('mst_order.used_coupon')
        ->addSelect('mst_settlement_manage.payment_name')
        ->addSelect("mst_order.goods_price AS sub_total")
        ->addSelect(DB::raw("CONCAT(mst_customer.first_name,' ',mst_customer.last_name) AS full_name"))
        ->leftJoin('mst_order', 'mst_order.receive_id', '=', 'dt_repayment.receive_id')
        ->leftJoin('mst_customer', 'mst_customer.customer_id', '=', 'mst_order.customer_id')
        ->leftJoin('mst_settlement_manage', 'mst_settlement_manage.payment_code', '=', 'mst_order.payment_method')
        ->leftJoin('mst_cod_fee', 'mst_cod_fee.mall_id', '=', 'mst_order.mall_id')
        ->where('repay_no', '=', $repay_no)->first();
        return $data;
    }
    /**
     * Update data
     *
     * @param  array condition $where
     * @param  array update $data
     * @return boolean
     */
    public function updateData($where, $data)
    {
        $data['up_ope_cd'] = Auth::user()->tantou_code;
        $data['up_date']   = date('Y-m-d H:i:s');
        $result = $this->where($where)
                    ->update($data);
        return $result;
    }
    /**
     * Update data by receive id
     *
     * @param  int receiveId
     * @return object
     */
    public function getDataByReceive($receiveId)
    {
        return $this->where('receive_id', $receiveId)
                    ->get();
    }

    /**
     * check exist by received order id
     *
     * @param   string  $receivedOrderId
     * @return  boolean
     */
    public function checkExistByReceivedOrderId($receivedOrderId)
    {
        $count = $this->join('mst_order', 'dt_repayment.receive_id', '=', 'mst_order.receive_id')
                    ->where('mst_order.received_order_id', $receivedOrderId)
                    ->count();
        if ($count > 0) {
            return true;
        }
        return false;
    }
}
