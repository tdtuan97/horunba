<?php
/**
 * Exe sql for hrnb.dt_send_fax_data
 *
 * @package    App\Models
 * @subpackage DtSendFaxData
 * @copyright  Copyright (c) 2018 RiverCrane! Corporation. All Rights Reserved.
 * @author     Van Qui<van.qui@rivercrane.vn>
 */

namespace App\Models\Backend;

use Illuminate\Database\Eloquent\Model;

class DtSendFaxData extends Model
{
    public $timestamps = false;
    
    /**
    * The database table used by the model.
    * @var string
    */
    protected $table = 'dt_send_fax_data';
    
    /**
    * The database is used by the model.
    * @var string
    */
    protected $connection = 'horunba';
}
