<?php

/**
 * Model for tRequestPerformanceModel table.
 *
 * @package    App\Models\Batches
 * @subpackage TRequestPerformanceModelEdi
 * @copyright  Copyright (c) 2018 RiverCrane! Corporation. All Rights Reserved.
 * @author     Truong Nghia<truong.van.nghia@rivercrane.vn>
 */

namespace App\Models\Backend;

use Illuminate\Database\Eloquent\Model;

class TRequestPerformanceModelEdi extends Model
{
    public $timestamps = false;

    /**
    * The database table used by the model.
    * @var string
    */
    protected $table = 'tRequestPerformanceModel';

    /**
    * The database is used by the model.
    * @var string
    */
    protected $connection = 'edi';
}
