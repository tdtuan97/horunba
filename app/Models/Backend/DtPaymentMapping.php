<?php
/**
 * Model for dt_payment_mapping table.
 *
 * @package    App\Models\Backend
 * @subpackage DtPaymentMapping
 * @copyright  Copyright (c) 2018 RiverCrane! Corporation. All Rights Reserved.
 * @author     Truong Nghia<truong.van.nghia@crivercrane.vn>
 */

namespace App\Models\Backend;

use Illuminate\Database\Eloquent\Model;
use Config;
use DB;

class DtPaymentMapping extends Model
{
    public $timestamps = false;

    /**
    * The database table used by the model.
    * @var string
    */
    protected $table = 'dt_payment_mapping';

    /**
    * The database is used by the model.
    * @var string
    */
    protected $connection = 'horunba';

    /**
     * Get data list
     *
     * @return object
     */
    public function getData($arraySearch = null, $arraySort = null, $arrayOptions = null)
    {
        $col = [
            'mst_mall.name_jp',
            'mst_order.shipment_date AS delivery_date',
            'mst_order.receive_id',
            'mst_order.order_date',
            'mst_order.payment_method',
            'mst_order.request_price',
            'mst_order.order_status',
            'dt_payment_mapping.received_order_id',
            'dt_payment_mapping.mall_id',
            'dt_payment_mapping.different_type',
            'dt_payment_mapping.web_payment_method',
            'dt_payment_mapping.web_request_price',
            'dt_payment_mapping.different_price',
            'dt_payment_mapping.web_order_status',
            'dt_payment_mapping.occorred_reason',
            'dt_payment_mapping.process_content',
            'dt_payment_mapping.is_corrected',
            'dt_payment_mapping.is_deleted',
            'dt_payment_mapping.finished_date',
            'hrnb_pn.payment_name as hrnb_payment_name',
            'web_pn.payment_name as web_payment_name'
        ];
        $data = $this->select($col)
            ->addSelect(DB::raw('(select status_name'
                    . ' from mst_order_status where order_status_id=mst_order.order_status) as hrnb_order_status_name'))
            ->addSelect(DB::raw('(select status_name'
                    . ' from mst_order_status where order_status_id='
                    . 'dt_payment_mapping.web_order_status) as web_order_status_name'));

        $data = $this->conditionQuery($data, $arraySearch, $arraySort);
        $perPage = ($arraySearch['per_page']) ? $arraySearch['per_page'] : 20;
        if (isset($arrayOptions['type'])) {
            $data = $data->get();
        } else {
            $data = $data->paginate($perPage);
        }
        return $data;
    }

    /**
     * Condition query
     *
     * @param  object  $data
     * @param  array   $arraySearch
     * @param  array   $arraySort
     * @return object
     */
    public function conditionQuery($data, $arraySearch = null, $arraySort = null)
    {
        $data->join('mst_mall', 'mst_mall.id', '=', 'dt_payment_mapping.mall_id')
            ->leftjoin('mst_order', 'mst_order.received_order_id', '=', 'dt_payment_mapping.received_order_id')
            ->leftjoin('mst_settlement_manage as hrnb_pn', 'hrnb_pn.payment_code', '=', 'mst_order.payment_method')
            ->leftjoin(
                'mst_settlement_manage as web_pn',
                'web_pn.payment_code',
                '=',
                'dt_payment_mapping.web_payment_method'
            );
        if (count($arraySearch) > 0) {
            $data->where(function ($query) use ($arraySearch) {
                if (isset($arraySearch['mall_id'])) {
                    if (is_array($arraySearch['mall_id'])) {
                        $query->whereIn('dt_payment_mapping.mall_id', $arraySearch['mall_id']);
                    } else {
                        $query->where('dt_payment_mapping.mall_id', $arraySearch['mall_id']);
                    }
                }
                if (isset($arraySearch['diferent_type'])) {
                    if (is_array($arraySearch['diferent_type'])) {
                        $query->whereIn('dt_payment_mapping.different_type', $arraySearch['diferent_type']);
                    } else {
                        $query->where('dt_payment_mapping.different_type', $arraySearch['diferent_type']);
                    }
                }
                if (isset($arraySearch['process_content'])) {
                    $query->where('dt_payment_mapping.process_content', $arraySearch['process_content']);
                }
                if (isset($arraySearch['order_date_from'])) {
                    $dateFrom = date("Y-m-d 00:00:00", strtotime($arraySearch['order_date_from']));
                    $query->where(
                        'mst_order.order_date',
                        '>=',
                        $dateFrom
                    );
                }
                if (isset($arraySearch['order_date_to'])) {
                    $dateTo = date("Y-m-d 23:59:59", strtotime($arraySearch['order_date_to']));
                    $query->where(
                        'mst_order.order_date',
                        '<=',
                        $dateTo
                    );
                }
                if (isset($arraySearch['sold_date_from'])) {
                    $dateFrom = date("Y-m-d", strtotime($arraySearch['sold_date_from']));
                    $query->whereRaw("DATE_FORMAT(mst_order.shipment_date, '%Y-%m-%d') >= '{$dateFrom}'");
                }
                if (isset($arraySearch['sold_date_to'])) {
                    $dateTo = date("Y-m-d", strtotime($arraySearch['sold_date_to']));
                    $query->whereRaw("DATE_FORMAT(mst_order.shipment_date, '%Y-%m-%d') <= '{$dateTo}'");
                }

                if (isset($arraySearch['setlement_hrnb'])) {
                    if (is_array($arraySearch['setlement_hrnb'])) {
                        $query->whereIn('hrnb_pn.payment_code', $arraySearch['setlement_hrnb']);
                    } else {
                        $query->where('hrnb_pn.payment_code', $arraySearch['setlement_hrnb']);
                    }
                }
                if (isset($arraySearch['setlement_web'])) {
                    if (is_array($arraySearch['setlement_web'])) {
                        $query->whereIn('web_pn.payment_code', $arraySearch['setlement_web']);
                    } else {
                        $query->where('web_pn.payment_code', $arraySearch['setlement_web']);
                    }
                }
                if (isset($arraySearch['request_price_hrnb'])) {
                    $query->where('mst_order.request_price', $arraySearch['request_price_hrnb']);
                }
                if (isset($arraySearch['request_price_web'])) {
                    $query->where('dt_payment_mapping.web_request_price', $arraySearch['request_price_web']);
                }
                if (isset($arraySearch['different_price'])) {
                    $query->where('dt_payment_mapping.different_price', $arraySearch['different_price']);
                }
                if (isset($arraySearch['received_order_id'])) {
                    $query->where('mst_order.received_order_id', 'like', "%{$arraySearch['received_order_id']}%");
                }
                if (isset($arraySearch['status_hrnb'])) {
                    if (is_array($arraySearch['status_hrnb'])) {
                        $query->whereIn('mst_order.order_status', $arraySearch['status_hrnb']);
                    } else {
                        $query->where('mst_order.order_status', $arraySearch['status_hrnb']);
                    }
                }
                if (isset($arraySearch['status_web'])) {
                    if (is_array($arraySearch['status_web'])) {
                        $query->whereIn('dt_payment_mapping.web_order_status', $arraySearch['status_web']);
                    } else {
                        $query->where('dt_payment_mapping.web_order_status', $arraySearch['status_web']);
                    }
                }
                if (isset($arraySearch['is_correct'])) {
                    if (is_array($arraySearch['is_correct'])) {
                        $query->whereIn('dt_payment_mapping.is_corrected', $arraySearch['is_correct']);
                    } else {
                        $query->where('dt_payment_mapping.is_corrected', $arraySearch['is_correct']);
                    }
                }
            });
        }
        if ($arraySort !== null && count($arraySort) > 0) {
            foreach ($arraySort as $column => $sort) {
                if ($sort !== null && in_array($sort, ['asc', 'desc'])) {
                    $data->orderBy($column, $sort);
                }
            }
        } else {
            $data->orderBy('dt_payment_mapping.up_date', 'desc');
        }
        return $data;
    }

    /**
     * Insert ignore data
     *
     * @param type $data
     */
    public function insertIgnore($data)
    {
        if (key($data) !== 0) {
            $data = [$data];
        }
        $arrKey    = array_keys($data[0]);
        $strKey    = "`" . implode("`,`", $arrKey) . "`";
        $arrParams = [];
        $arrValue  = [];
        $index     = 0;
        foreach ($data as $item) {
            $tmpKey = [];
            foreach ($item as $key => $val) {
                $curKey          = $key . $index;
                $tmpKey[]        = ':' . $curKey;
                $arrValue[$curKey] = $val;
            }
            $index++;
            $arrParams[] = '(' . implode(',', $tmpKey) . ')';
        }
        $strParam = implode(",", $arrParams);
        return DB::insert("INSERT IGNORE INTO hrnb.dt_payment_mapping ($strKey) VALUES $strParam", $arrValue);
    }

    /**
     * Update all record by condition
     *
     * @return object
     */
    public function updateAll($arraySearch, $arraySort = null, $arrayUpdate = null)
    {
        $data = $this->conditionQuery($this->select('dt_payment_mapping.received_order_id'), $arraySearch, $arraySort);
        $processData = $data->pluck('received_order_id');
        if (count($processData) > 0) {
            $chunkData = array_chunk($processData->toArray(), 500);
            foreach ($chunkData as $value) {
                $this->whereIn('received_order_id', $value)->update($arrayUpdate);
            }
            $chunkData = null;
        }
    }
}
