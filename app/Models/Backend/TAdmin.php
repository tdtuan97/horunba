<?php

/**
 * Model for t_admin table.
 *
 * @package    App\Models\Backend
 * @subpackage TAdmin
 * @copyright  Copyright (c) 2018 RiverCrane! Corporation. All Rights Reserved.
 * @author     van.qui<van.qui.rcvn2012@gmail.com>
 */

namespace App\Models\Backend;

use Illuminate\Database\Eloquent\Model;

class TAdmin extends Model
{
    public $timestamps = false;

    /**
    * The database table used by the model.
    * @var string
    */
    protected $table = 't_admin';

    /**
    * The database is used by the model.
    * @var string
    */
    protected $connection = 'edi';

    /**
     * Get item by suppliercd
     *
     * @param  int    $suppliercd
     * @return object
     */
    public function getItemBySuppliercd($suppliercd)
    {
        $data = $this->where('suppliercd', $suppliercd)->first();
        return $data;
    }
}
