<?php

/**
 * Exe sql for hrnb.mst_product_base
 *
 * @package    App\Models\Backend
 * @subpackage MstProduct
 * @copyright  Copyright (c) 2018 RiverCrane! Corporation. All Rights Reserved.
 * @author     Truong Nghia<truong.van.nghia@crivercrane.vn>
 */

namespace App\Models\Backend;

use DB;
use Illuminate\Database\Eloquent\Model;

class MstProductBase extends Model
{
    public $timestamps = false;
    /**
    * The database table used by the model.
    * @var string
    */
    protected $table = 'mst_product_base';

    /**
    * The database is used by the model.
    * @var string
    */
    protected $connection = 'horunba';

    public function getDataProduct($col, $key)
    {
        return $this->whereIn('product_code', $key)->get($col);
    }
}
