<?php

/**
 * Model for t_order_detail table.
 *
 * @package    App\Models\Backend
 * @subpackage TOrderDetail
 * @copyright  Copyright (c) 2018 RiverCrane! Corporation. All Rights Reserved.
 * @author     van.qui<van.qui.rcvn2012@gmail.com>
 * @author     le.hung<le.hung.rcvn2012@gmail.com>
 */

namespace App\Models\Backend;

use Illuminate\Database\Eloquent\Model;

class TOrderDetail extends Model
{
    public $timestamps = false;

    /**
    * The database table used by the model.
    * @var string
    */
    protected $table = 't_order_detail';

    /**
    * The database is used by the model.
    * @var string
    */
    protected $connection = 'edi';

    /**
     * Update data
     * @author le.hung
     * @param  array condition $where
     * @param  array update $data
     * @return boolean
     */
    public function updateData($where, $data)
    {
        $result = $this->where($where)
                    ->update($data);
        return $result;
    }
}
