<?php

/**
 * Exe sql for hrnb.dt_order_update_log
 *
 * @package    App\Models
 * @subpackage DtOrderUpdateLog
 * @copyright  Copyright (c) 2017 RiverCrane! Corporation. All Rights Reserved.
 * @author     le.hung<le.hung.rcvn2012@gmail.com>
 */

namespace App\Models\Backend;

use Illuminate\Database\Eloquent\Model;
use DB;

class DtOrderUpdateLog extends Model
{
    public $timestamps = false;
    /**
    * The database table used by the model.
    * @var string
    */
    protected $table = 'dt_order_update_log';
    /**
    * The database is used by the model.
    * @var string
    */
    protected $connection = 'horunba';

    /**
    * The primary key.
    * @var string
    */
    protected $primaryKey = 'log_id';

    /**
     * Get data of tabe dt_order_update_log
     *
     * @param   $arraySort, $arraySearch
     * @return object
     */
    public function getData($arraySearch = null, $arraySort = null)
    {
        $data = $this->select(
            'dt_order_update_log.log_id',
            'dt_order_update_log.receive_id',
            'dt_order_update_log.log_title',
            'dt_order_update_log.log_status',
            'dt_order_update_log.in_date',
            'dt_order_update_log.up_date',
            'dt_order_update_log.up_ope_cd'
        )
        ->leftJoin('mst_tantou', function ($join) {
            $join->on('dt_order_update_log.in_ope_cd', '=', 'mst_tantou.tantou_code');
        })
        ->addSelect('mst_tantou.tantou_last_name')
        ->leftJoin('mst_tantou as mst_tantou2', function ($join) {
            $join->on('dt_order_update_log.up_ope_cd', '=', 'mst_tantou2.tantou_code');
        })
        ->addSelect('mst_tantou2.tantou_last_name as up_tantou_last_name');
        if (count($arraySearch) > 0) {
            $data->where('receive_id', "{$arraySearch['receive_id']}");
        }
        $data->orderBy('in_date', 'desc');
        return $data->get();
    }
    /**
     * Get data of tabe dt_order_update_log
     *
     * @param   $arraySort, $arraySearch
     * @return object
     */
    public function getDataLog($arraySearch = null, $arraySort = null, $operatorCd = 0)
    {
        $data = $this->select(
            'mst_mall.name_jp',
            'mst_order.receive_id',
            'mst_order.order_date',
            'mst_order_status.status_name',
            'dt_order_update_log.log_status',
            'dt_order_update_log.log_id',
            'dt_order_update_log.up_ope_cd',
            'dt_order_update_log.in_date',
            'dt_order_update_log.up_date',
            'dt_order_update_log.log_title',
            'dt_order_update_log.log_contents',
            'mst_tantou.tantou_last_name',
            DB::raw("CONCAT(mst_customer.last_name, ' ', mst_customer.first_name) as full_name")
        )
        ->join('mst_order', function ($join) {
            $join->on('mst_order.receive_id', '=', 'dt_order_update_log.receive_id');
        })
        ->join('mst_customer', function ($join) {
            $join->on('mst_customer.customer_id', '=', 'mst_order.customer_id');
        })
        ->join('mst_mall', function ($join) {
            $join->on('mst_mall.id', '=', 'mst_order.mall_id');
        })
        ->join('mst_tantou', function ($join) {
            $join->on('dt_order_update_log.in_ope_cd', '=', 'mst_tantou.tantou_code');
        })
        ->join('mst_tantou as mst_tantou2', function ($join) {
            $join->on('dt_order_update_log.up_ope_cd', '=', 'mst_tantou2.tantou_code');
        })
        ->join('mst_order_status', function ($join) {
            $join->on('mst_order_status.order_status_id', '=', 'mst_order.order_status');
        });
        $checkUserInput = false;
        if (isset($arraySearch['operator'])) {
            if (is_array($arraySearch['name_jp'])) {
                $data->whereIn('mst_tantou.tantou_code', $arraySearch['operator']);
            } else {
                $data->where('mst_tantou.tantou_code', $arraySearch['operator']);
            }
            $checkUserInput = true;
        }
        if (isset($arraySearch['in_date_from'])) {
            $data->where('dt_order_update_log.in_date', '>=', $arraySearch['in_date_from']);
            $checkUserInput = true;
        }

        if (isset($arraySearch['in_date_to'])) {
            $data->where('dt_order_update_log.in_date', '<=', $arraySearch['in_date_to']);
            $checkUserInput = true;
        }
        if (isset($arraySearch['name_jp'])) {
            if (is_array($arraySearch['name_jp'])) {
                $data->whereIn('mst_mall.id', $arraySearch['name_jp']);
            } else {
                $data->where('mst_mall.id', $arraySearch['name_jp']);
            }
            $checkUserInput = true;
        }
        if (isset($arraySearch['search_log_title'])) {
            $data->where('dt_order_update_log.log_title', 'LIKE', "%{$arraySearch['search_log_title']}%");
            $checkUserInput = true;
        }
        if (isset($arraySearch['search_log_content'])) {
            $data->where('dt_order_update_log.log_contents', 'LIKE', "%{$arraySearch['search_log_content']}%");
            $checkUserInput = true;
        }
        if (isset($arraySearch['search_log_status'])) {
            if (is_array($arraySearch['name_jp'])) {
                $data->whereIn('dt_order_update_log.log_status', $arraySearch['search_log_status']);
            } else {
                $data->where('dt_order_update_log.log_status', $arraySearch['search_log_status']);
            }
            $checkUserInput = true;
        }
        if (!$checkUserInput) {
            $data->where('dt_order_update_log.log_status', '=', 0)
                 ->where('dt_order_update_log.up_ope_cd', '=', $operatorCd);
        }
        if ($arraySort !== null && count($arraySort) > 0) {
            foreach ($arraySort as $column => $sort) {
                if (in_array($sort, ['asc', 'desc'])) {
                    $data->orderBy($column, $sort);
                }
            }
        } else {
            $data->orderBy('dt_order_update_log.in_date', 'desc');
        }
        $perPage = isset($arraySearch['per_page']) ? $arraySearch['per_page'] : 20;
        $data = $data->paginate($perPage);
        return $data;
    }
    /**
     * Get data of tabe dt_order_update_log
     *
     * @param   $arraySort, $arraySearch
     * @return object
     */
    public function countDataLog($operatorCd = 0)
    {
        $data = $this
        ->join('mst_order', function ($join) {
            $join->on('mst_order.receive_id', '=', 'dt_order_update_log.receive_id');
        })
        ->join('mst_customer', function ($join) {
            $join->on('mst_customer.customer_id', '=', 'mst_order.customer_id');
        })
        ->join('mst_mall', function ($join) {
            $join->on('mst_mall.id', '=', 'mst_order.mall_id');
        })
        ->join('mst_tantou', function ($join) {
            $join->on('dt_order_update_log.in_ope_cd', '=', 'mst_tantou.tantou_code');
        })
        ->join('mst_tantou as mst_tantou2', function ($join) {
            $join->on('dt_order_update_log.up_ope_cd', '=', 'mst_tantou2.tantou_code');
        })
        ->join('mst_order_status', function ($join) {
            $join->on('mst_order_status.order_status_id', '=', 'mst_order.order_status');
        })
        ->where('dt_order_update_log.log_status', '=', 0)
        ->where('dt_order_update_log.up_ope_cd', '=', $operatorCd)
        ->count();

        return $data;
    }
    /**
     * Get data for notify
     *
     * @param  operatorCd
     * @return object
     */
    public function getDataNotify($operatorCd = 0)
    {
        $data = $this->select(
            'dt_order_update_log.log_id',
            'dt_order_update_log.log_title'
        )
        ->join('mst_order', function ($join) {
            $join->on('mst_order.receive_id', '=', 'dt_order_update_log.receive_id');
        })
        ->join('mst_customer', function ($join) {
            $join->on('mst_customer.customer_id', '=', 'mst_order.customer_id');
        })
        ->join('mst_mall', function ($join) {
            $join->on('mst_mall.id', '=', 'mst_order.mall_id');
        })
        ->join('mst_tantou', function ($join) {
            $join->on('dt_order_update_log.in_ope_cd', '=', 'mst_tantou.tantou_code');
        })
        ->join('mst_tantou as mst_tantou2', function ($join) {
            $join->on('dt_order_update_log.up_ope_cd', '=', 'mst_tantou2.tantou_code');
        })
        ->join('mst_order_status', function ($join) {
            $join->on('mst_order_status.order_status_id', '=', 'mst_order.order_status');
        })
        ->where('dt_order_update_log.log_status', '=', 0)
        ->where('dt_order_update_log.up_ope_cd', '=', $operatorCd)
        ->orderBy('dt_order_update_log.in_date', 'desc')
        ->get();
        return $data;
    }

    /**
     * Insert data
     *
     * @param  array $data
     * @return boolean
     */
    public function insertData($data)
    {
        $result = $this->insertGetId($data);
        return true;
    }


    /**
     * Update data by condition
     *
     * @param  array   $cond
     * @param  array   $data
     * @return boolean
     */
    public function updateData($cond, $data)
    {
        return $this->where($cond)->update($data);
    }
}
