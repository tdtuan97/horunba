<?php
/**
 * Model for mst_backlist_comment table.
 *
 * @package    App\Models\Backend
 * @subpackage MstBacklistComment
 * @copyright  Copyright (c) 2019 RiverCrane! Corporation. All Rights Reserved.
 * @author     Truong Nghia<truong.van.nghia@crivercrane.vn>
 */

namespace App\Models\Backend;

use Illuminate\Database\Eloquent\Model;

class MstBacklistComment extends Model
{
    public $timestamps = false;

    /**
     * The database table used by the model.
     * @var string
     */
    protected $table = 'mst_backlist_comment';

    /**
     * The database is used by the model.
     * @var string
     */
    protected $connection = 'horunba';

    /**
     * Update data by key
     *
     * @param  array   $keys
     * @param  array   $data
     * @return boolean
     */
    public function updateData($keys, $data)
    {
        return $this->where($keys)->update($data);
    }

    /**
     * Get data of tabe mst tantou
     *
     * @return object
     */
    public function getDataSelectBox()
    {
        $data = $this->select('ID AS key', 'backlist_comment AS value')->get();
        return $data;
    }
}
