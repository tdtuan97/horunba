<?php
/**
 * Exe sql for hrnb.mst_store_info
 *
 * @package    App\Models
 * @subpackage MstStoreInfo
 * @copyright  Copyright (c) 2018 RiverCrane! Corporation. All Rights Reserved.
 * @author     van.qui<van.qui.rcvn2012@gmail.com>
 */

namespace App\Models\Backend;

use Illuminate\Database\Eloquent\Model;

class MstStoreInfo extends Model
{
    public $timestamps = false;
    /**
    * The database table used by the model.
    * @var string
    */
    protected $table = 'mst_store_info';
    /**
    * The database is used by the model.
    * @var string
    */
    protected $connection = 'horunba';
    
    /**
    * The primary key.
    * @var string
    */
    protected $primaryKey = 'store_id';
}
