<?php
/**
 * Model for mst_stock_status table.
 *
 * @package    App\Models\Backend
 * @subpackage MstStockStatus
 * @copyright  Copyright (c) 2018 RiverCrane! Corporation. All Rights Reserved.
 * @author     Truong Nghia<truong.van.nghia@rivercrane.vn>
 */

namespace App\Models\Backend;

use Illuminate\Database\Eloquent\Model;
use DB;

class MstStockStatus extends Model
{
    public $timestamps = false;

    /**
    * The database table used by the model.
    * @var string
    */
    protected $table = 'mst_stock_status';

    /**
    * The database is used by the model.
    * @var string
    */
    protected $connection = 'horunba';

    /**
    * Set default key type.
    * @var string
    */
    protected $keyType = 'string';

    /**
    * Set default primary key.
    * @var string
    */

    protected $primaryKey = 'product_code';

    /**
     * Get data by product code
     *
     * @param  string $parentProductCode
     * @param  string $childProductCode
     * @return object
     */
    public function getDataByKey($parentProductCode, $childProductCode = null)
    {
        $data = $this->leftjoin(
            'mst_product_set',
            'mst_product_set.child_product_code',
            '=',
            'mst_stock_status.product_code'
        );

        if ($childProductCode === null) {
            $data->where('mst_stock_status.product_code', $parentProductCode);
        } else {
            $data->where('mst_stock_status.product_code', $childProductCode)
                ->where('mst_product_set.parent_product_code', $parentProductCode);
        }
        return $data->first();
    }

    /**
     * Update data
     *
     * @param  array $keys
     * @param  array $data
     * @return boolean
     */
    public function updateData($keys, $data)
    {
        return $this->where($keys)->update($data);
    }

    public function getData($arrSearch, $arrSort)
    {
        $col = [
            'mst_stock_status.product_code',
            'mst_stock_status.nanko_num',
            'mst_stock_status.glsc_num',
            'mst_stock_status.order_zan_num',
            'mst_stock_status.rep_orderring_num',
            'mst_stock_status.return_num',
            'mst_stock_status.wait_delivery_num',
            'mst_stock_status.up_date',
            'mst_product.cheetah_status'
        ];
        $data = $this->select($col)
            ->leftJoin('edi.mst_product', function ($join) {
                $join->on('mst_stock_status.product_code', '=', 'mst_product.product_code')
                    ->where('mst_product.is_live', 1);
            });
        if (count($arrSearch) > 0) {
            $data->where(function ($query) use ($arrSearch) {
                if ($arrSearch['product_code'] !== null) {
                    $query->where('mst_stock_status.product_code', '=', $arrSearch['product_code']);
                }
                if ($arrSearch['nanko_num'] !== null) {
                    $query->where('mst_stock_status.nanko_num', '=', $arrSearch['nanko_num']);
                }
                if ($arrSearch['glsc_num'] !== null) {
                    $query->where('mst_stock_status.glsc_num', '=', $arrSearch['glsc_num']);
                }
                if ($arrSearch['order_zan_num'] !== null) {
                    $query->where('mst_stock_status.order_zan_num', '=', $arrSearch['order_zan_num']);
                }
                if ($arrSearch['rep_orderring_num'] !== null) {
                    $query->where('mst_stock_status.rep_orderring_num', '=', $arrSearch['rep_orderring_num']);
                }
                if ($arrSearch['return_num'] !== null) {
                    $query->where('mst_stock_status.return_num', '=', $arrSearch['return_num']);
                }
                if ($arrSearch['wait_delivery_num'] !== null) {
                    $query->where('mst_stock_status.wait_delivery_num', '=', $arrSearch['wait_delivery_num']);
                }
                if ($arrSearch['nanko_glsc'] !== null) {
                    $query->whereRaw('mst_stock_status.glsc_num <> mst_stock_status.nanko_num');
                }
                if ($arrSearch['free_zan_num'] !== null) {
                    $freeZanNum = (int)$arrSearch['free_zan_num'];
                    if ($freeZanNum === 0) {
                        $query->whereRaw("(nanko_num - order_zan_num - return_num) <= $freeZanNum");
                    } else {
                        $query->whereRaw("(nanko_num - order_zan_num - return_num) = $freeZanNum");
                    }
                }
                if (isset($arrSearch['cheetah_status'])) {
                    if (is_array($arrSearch['cheetah_status'])) {
                        $query->whereIn('mst_product.cheetah_status', $arrSearch['cheetah_status']);
                    } else {
                        $query->where('mst_product.cheetah_status', $arrSearch['cheetah_status']);
                    }
                }
            });
        }

        if ($arrSort !== null && count($arrSort) > 0) {
            foreach ($arrSort as $column => $sort) {
                if (in_array($sort, ['asc', 'desc'])) {
                    $data->orderBy($column, $sort);
                }
            }
        } else {
            $data->orderBy('up_date', 'desc');
        }
        $perPage = isset($arrSearch['per_page']) ? $arrSearch['per_page'] : 20;
        $data = $data->paginate($perPage);
        return $data;
    }

    /**
     * Get data check stock when return product
     *
     * @param  array $arrProduct
     * @return boolean
     */
    public function getDataStockCheckReturn($arrProduct)
    {
        $col = "(mst_stock_status.nanko_num - mst_stock_status.order_zan_num - mst_stock_status.return_num) AS cal";
        $result = $this->select('product_code')
                       ->selectRaw($col)
                       ->whereIn('product_code', $arrProduct)
                       ->get();
        return $result;
    }
}
