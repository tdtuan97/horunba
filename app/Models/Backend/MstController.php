<?php

/**
 * Model for mst_controller table.
 *
 * @package    App\Models
 * @subpackage MstController
 * @copyright  Copyright (c) 2019 RiverCrane! Corporation. All Rights Reserved.
 * @author     Truong Nghia<truong.van.nghia@rivercrane.vn>
 */

namespace App\Models\Backend;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class MstController extends Authenticatable
{
    use Notifiable;

    /**
    * The database table used by the model.
    * @var string
    */
    protected $table = 'mst_controller';

    /**
    * The timestamps.
    * @var boolean
    */
    public $timestamps = false;

    /**
     * Get data redirect
     * @param string $controller
     * @return object
     */
    public function getDataCheckAccess($controller)
    {
        return $this->where('screen_class', $controller)
                    ->where('flg_access', 1)
                    ->first();
    }

    /**
     * Get list department
     * @return object
     */
    public function getDepartOption()
    {
        $result = $this->where('flg_access' , 0)
                       ->get();
        return $result;
    }

    /**
     * Get data
     *
     * @params  array    $arraySearch array data search
     * @return object
     */
    public function getData($arraySearch = null)
    {
        $data = $this->orderBy('mst_controller.up_date', 'desc');
        $perPage = ($arraySearch['per_page']) ? $arraySearch['per_page'] : 20;
        $data = $data->paginate($perPage);
        return $data;
    }

    /**
     * Update data by key
     *
     * @param  array   $keys
     * @param  array   $data
     * @return boolean
     */
    public function updateData($keys, $data)
    {
        return $this->where($keys)->update($data);
    }
}