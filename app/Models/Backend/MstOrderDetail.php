<?php
/**
 * Exe sql for hrnb.mst_order_detail
 *
 * @package    App\Models
 * @subpackage MstOrderDetail
 * @copyright  Copyright (c) 2018 RiverCrane! Corporation. All Rights Reserved.
 * @author     van.qui<van.qui.rcvn2012@gmail.com>
 */

namespace App\Models\Backend;

use Illuminate\Database\Eloquent\Model;

class MstOrderDetail extends Model
{
    public $timestamps = false;

    /**
    * The database table used by the model.
    * @var string
    */
    protected $table = 'mst_order_detail';

    /**
    * The database is used by the model.
    * @var string
    */
    protected $connection = 'horunba';

    /**
     * Get max item by receive_id
     *
     * @param  int $receiveId
     * @param  int $receiverId
     * @return int
     */
    public function getMaxItem($receiveId, $receiverId = null)
    {
        $data = $this->select(
            'receive_id',
            'detail_line_num',
            'receiver_id',
            'ship_date',
            'delivery_person_id',
            'package_ship_number',
            'time_wish_ship'
        )
        ->where('receive_id', $receiveId);
        if ($receiverId !== null) {
            $data->where('receiver_id', $receiverId);
        }
        $data = $data->orderBy('detail_line_num', 'desc')
        ->first();
        return $data;
    }

    /**
     * Get item
     *
     * @param  int    $receiveId
     * @param  int    $detailLineNum
     * @return object
     */
    public function getItemCheckUpdate($receiveId, $detailLineNum)
    {
        $data = $this->select(
            'mst_order_detail.receive_id',
            'mst_order_detail.detail_line_num',
            'price',
            'quantity',
            'product_status'
        )
        ->join('dt_order_product_detail', function ($join) {
            $join->on('dt_order_product_detail.receive_id', '=', 'mst_order_detail.receive_id')
            ->on('dt_order_product_detail.detail_line_num', '=', 'mst_order_detail.detail_line_num');
        })
        ->where('mst_order_detail.receive_id', $receiveId)
        ->where('mst_order_detail.detail_line_num', $detailLineNum)
        ->get();
        return $data;
    }

    /**
     * Update data
     *
     * @param  int     $receiveId
     * @param  int     $detailLineNum
     * @param  array   $data
     * @return boolean
     */
    public function updateData($receiveId, $detailLineNum, $data)
    {
        return $this->where('receive_id', $receiveId)
                ->where('detail_line_num', $detailLineNum)
                ->update($data);
    }
    /**
     * Get list product code by receive_id
     *
     * @param   int     $receiveId
     * @return  object
     */
    public function getProductCodeByReceiveId($receiveId)
    {
        $result = $this->select(['product_code AS key', 'product_code AS value'])
                       ->where('receive_id', $receiveId)
                       ->whereNotIn('product_code', function ($query) use ($receiveId) {
                            $query->from('dt_return')
                                  ->select('parent_product_code')
                                  ->join('mst_order_detail', function ($query) {
                                        $query->on('mst_order_detail.receive_id', '=', 'dt_return.receive_id')
                                              ->on(
                                                  'mst_order_detail.product_code',
                                                  '=',
                                                  'dt_return.parent_product_code'
                                              );
                                  })
                                  ->where('dt_return.receive_id', $receiveId)
                                  ->where('mst_order_detail.quantity', '<>', 'mst_order_detail.return_quantity')
                                  ->get();
                       })
                       ->get();
        return $result;
    }

    /**
     * Check product child
     *
     * @param   int     $receiveId
     * @return  object
     */
    public function getDataChildByReceive($receiveId, $productCode)
    {
        $result = $this->where('receive_id', $receiveId)
                       ->where('product_code', $productCode)
                       ->get();
        return $result;
    }

    /**
     * Check data by condition
     *
     * @param   array     $arrParam
     * @return  object
     */
    public function getDataByCondition($arrParam)
    {
        $result = $this->where($arrParam)
                       ->get();
        return $result;
    }
    /**
    * Get data by receive id
    * @param array $arrData Data for condition where
    * @return object $result
    */
    public function getDataByReceiveId($receiveId)
    {
        $cols = [
            'receive_id',
            'detail_line_num',
            'product_code',
            'product_name',
            'price',
            'quantity'
        ];
        $result = $this->select($cols)
                    ->where('receive_id', (int)$receiveId)
                    ->get();
        return $result;
    }
    /**
    * Delete item
    * @param array $arrData Data for condition where
    * @return object $result
    */
    public function deleteData($arrData)
    {
        $result = $this->whereIn('detail_line_num', $arrData['detail_line_num'])
                    ->where('receive_id', '=', $arrData['receive_id'])
                    ->delete();
        return $result;
    }

    /**
    * Get data check validate postal and address of receiver.
    * @param string $receiveId
    * @param string $receiverId
    * @return object $result
    */
    public function getDataCheckValidatePostalReceiver($receiveId, $receiverId = null)
    {
        $result = $this->join('mst_customer AS cus', 'cus.customer_id', '=', 'mst_order_detail.receiver_id')
                       ->join('mst_postal AS pos', 'cus.zip_code', '=', 'pos.postal_code')
                       ->where('mst_order_detail.receive_id', '=', $receiveId);
        if ($receiverId !== null) {
            $result->where('mst_order_detail.receiver_id', '=', $receiverId);
        }
        $result = $result->whereRaw(" LOCATE(Concat(ifnull(pos.prefecture,''),ifnull(pos.city,'')), "
            . "Concat(ifnull(cus.prefecture,''), ifnull(cus.city,''), ifnull(cus.sub_address,''))) > 0")
                       ->count();
        return $result;
    }


    /**
    * Get data check validate postal and address of receiver by length.
    * @param string $receiveId
    * @param string $receiverId
    * @return object $result
    */
    public function getDataCheckValidatePostalReceiverLenth($receiveId, $receiverId = null)
    {
        $result = $this->join('mst_customer AS cus', 'cus.customer_id', '=', 'mst_order_detail.receiver_id')
                       ->join('mst_postal AS pos', 'cus.zip_code', '=', 'pos.postal_code')
                       ->where('mst_order_detail.receive_id', '=', $receiveId);
        if ($receiverId !== null) {
            $result->where('mst_order_detail.receiver_id', '=', $receiverId);
        }
        $result = $result->whereRaw(" LOCATE(Concat(ifnull(pos.prefecture,''),ifnull(pos.city,'')), "
            . "Concat(ifnull(cus.prefecture,''), ifnull(cus.city,''), ifnull(cus.sub_address,''))) > 0")
                       ->whereRaw('(LENGTH(pos.full_address) + 2) > LENGTH(CONCAT(cus.prefecture, cus.city, cus.sub_address))')
                       ->count();
        return $result;
    }

    /**
     * Update data
     *
     * @param array  $arrKey
     * @param iarray  $arrData
     *
     * @return boolean
     */
    public function updateDataByKey($arrKey, $arrData)
    {
        return $this->where($arrKey)
                ->update($arrData);
    }

    /**
     * Get data process reutrn product is kippin or haipan
     *
     * @param array  $param
     *
     * @return object
     */
    public function getDataCheckKeppinHaiban($param)
    {
        $data = $this->select('mst_order_detail.price', 'mst_order_detail.quantity')
            ->join('edi.mst_product as emp', function ($join) {
                $join->on('mst_order_detail.product_code', '=', 'emp.product_code')
                ->where('emp.is_live', '=', 1);
            })
            ->where(function ($dataTemp) use ($param) {
                foreach ($param as $item) {
                    $dataTemp->orWhere(function ($subWhere) use ($item) {
                        $subWhere->where("mst_order_detail.detail_line_num", $item['detail_line_num'])
                                 ->where("mst_order_detail.receive_id", $item['receive_id']);
                    });
                }
            });
        $data->where(function ($sub) {
            $sub->where('emp.cheetah_status', '3')
                ->orWhere('emp.cheetah_status', '4');
        });
        return $data->get();
    }


    /**
    * Get data check validate Tel's digits.
    * @param string $receiveId
    * @param string $receiverId
    * @return object $result
    */
    public function getDataCheckTelDigits($receiveId, $receiverId = null)
    {
        $sub = "IF(LEFT(cus.tel_num,3) IN (050,060,070,080,090), LENGTH(replace(cus.tel_num, '-','')) <> 11, LENGTH(replace(cus.tel_num, '-','')) < 10 and LENGTH(replace(cus.tel_num, '-','')) > 11)";
        $result = $this->join('mst_customer AS cus', 'cus.customer_id', '=', 'mst_order_detail.receiver_id')
                       ->join('mst_order AS order', 'order.receive_id', '=', 'mst_order_detail.receive_id')
                       ->where('mst_order_detail.receive_id', '=', $receiveId)
                       ->where('order.mall_id', '<>', 3);
        if ($receiverId !== null) {
            $result->where('mst_order_detail.receiver_id', $receiverId);
        }
        $result = $result->whereRaw($sub)
                       ->count();
        return $result;
    }

    public function getDataCheckTelDigits1($receiveId, $receiverId = null)
    {
        $result = $this->join('mst_customer AS cus', 'cus.customer_id', '=', 'mst_order_detail.receiver_id')
                       ->join('mst_order AS order', 'order.receive_id', '=', 'mst_order_detail.receive_id')
                       ->where('mst_order_detail.receive_id', '=', $receiveId)
                       ->where('order.mall_id', '=', 3)
                       ->whereRaw("((LENGTH(replace(cus.tel_num, '-','')) < 1) OR (LENGTH(replace(cus.tel_num, '-','')) > 11))");
        if ($receiverId !== null) {
            $result->where('mst_order_detail.receiver_id', $receiverId);
        }
        $result = $result->count();
        return $result;
    }

    /**
    * Get data check validate Tel's digits.
    * @param string $receiveId
    * @return object $result
    */
    public function getListReceiver($receiveId)
    {
        return $this->select('receiver_id')
                    ->where('receive_id', $receiveId)
                    ->distinct()
                    ->get();
    }
}
