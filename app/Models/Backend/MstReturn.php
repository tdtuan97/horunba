<?php

/**
 * Exe sql for hrnb.mst_return
 *
 * @package    App\Models\Backend
 * @subpackage MstReturn
 * @copyright  Copyright (c) 2018 RiverCrane! Corporation. All Rights Reserved.
 * @author     Truong Nghia<truong.van.nghia@rivercrane.vn>
 */

namespace App\Models\Backend;

use Illuminate\Database\Eloquent\Model;

class MstReturn extends Model
{
    public $timestamps = false;
    /**
    * The database table used by the model.
    * @var string
    */
    protected $table = 'mst_return';

    /**
    * The database is used by the model.
    * @var string
    */
    protected $connection = 'horunba';

    /**
    * The primary key.
    * @var string
    */
    protected $primaryKey = 'return_id';

    /**
    * Get data option.
    * @return object
    */
    public function getData($returnId = null)
    {
        $result = $this->select('return_id AS key', 'return_name AS value');
        if (isset($returnId)) {
            $result = $result->where('return_id', $returnId);
        }
        return $result->get();
    }
}
