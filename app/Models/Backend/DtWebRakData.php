<?php

/**
 * Exe sql for hrnb.dt_web_rak_data
 *
 * @package    App\Models
 * @subpackage DtWebRakData
 * @copyright  Copyright (c) 2018 RiverCrane! Corporation. All Rights Reserved.
 * @author     Truong Nghia<truong.van.nghia@rivercrane.vn>
 */

namespace App\Models\Backend;

use Illuminate\Database\Eloquent\Model;

class DtWebRakData extends Model
{
    public $timestamps = false;
    /**
    * The database table used by the model.
    * @var string
    */
    protected $table = 'dt_web_rak_data';
    /**
    * The database is used by the model.
    * @var string
    */
    protected $connection = 'horunba';
    /**
     * Get data process web rakuten
     * @return object
     */
    public function getDataProcessWebRakuten()
    {
        $col = [
            'dt_web_rak_data.order_number',
            'dt_web_rak_data.settlement_name',
            'dt_web_rak_data.request_price',
            'dt_web_rak_data.status',
            'mst_order.mall_id',
            'mst_order.order_status',
            'mst_order.payment_method',
            'mst_order.request_price AS order_request_price',
        ];
        $result = $this->select($col)
                       ->leftjoin('mst_order', 'dt_web_rak_data.order_number', '=', 'mst_order.received_order_id')
                       ->where('process_flg', 0)
                       ->where('is_delete', 0)
                       ->where('dt_web_rak_data.error_code', '<>', 'E10-001')
                       ->whereNull('dt_web_rak_data.unit_error_code')
                       ->groupBy('dt_web_rak_data.order_number')
                       ->get();
        return $result;
    }
}
