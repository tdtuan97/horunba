<?php

/**
 * Exe sql for hrnb.dt_order_product_detail
 *
 * @package    App\Models
 * @subpackage DtOrderProductDetail
 * @copyright  Copyright (c) 2017 RiverCrane! Corporation. All Rights Reserved.
 * @author     le.hung<le.hung.rcvn2012@gmail.com>
 */

namespace App\Models\Backend;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use App\Models\Backend\LogUpdateStatusProduct;

class DtOrderProductDetail extends Model
{
    public $timestamps = false;
    /**
    * The database table used by the model.
    * @var string
    */
    protected $table = 'dt_order_product_detail';
    /**
    * The database is used by the model.
    * @var string
    */
    protected $connection = 'horunba';

    /**
     * Get data of tabe dt_order_product_detail
     *
     * @param   $arraySort, $arraySearch
     * @return object
     */
    public function getData($arraySearch = null, $arraySort = null)
    {
        $data = $this->select('*');
        if (count($arraySearch) > 0) {
            $data->where(function ($query) use ($arraySearch) {
                if (isset($arraySearch['order_num'])) {
                    $query->where('order_num', "{$arraySearch['order_num']}");
                }
                if (isset($arraySearch['product_code'])) {
                    $query->where('product_code', 'LIKE', "{$arraySearch['product_code']}%");
                }
                if (isset($arraySearch['receive_id'])) {
                    $query->where('receive_id', "{$arraySearch['receive_id']}");
                }
            });
        }
        if ($arraySort !== null && count($arraySort) > 0) {
            if (!is_array($arraySort)) {
                $arraySort = json_decode($arraySort);
            }
            foreach ($arraySort as $column => $sort) {
                $data->orderBy($column, $sort);
            }
        }

        $data = $data->paginate(20);
        return $data;
    }

    /**
     * Update data
     *
     * @param  array $keys
     * @param  array $data
     * @return boolean
     */
    public function updateData($keys, $data)
    {
        if (array_key_exists('product_status', $data)) {
            $modelLog  = new LogUpdateStatusProduct();
            $backtrace = debug_backtrace(false, 2)[1];
            $className = $backtrace['class'];
            $function  = $backtrace['function'];
            $action    = $className . '@' . $function;
            $arrKeyLog = $keys;
            if (isset($keys['receive_id'])) {
                $key = 'receive_id';
            } else {
                $key = 'received_order_id';
                if (!isset($keys['received_order_id'])) {
                    $arrKeyLog[$key] = $keys[key($keys)];
                }
            }

            $modelLog->insertLog($arrKeyLog, $data, $action, $key);
        }
        return $this->where($keys)->update($data);
    }

    /**
     * Get data by keys
     *
     * @param  array $arrCheck
     * @return object
     */
    public function getDataByKeys($arrCheck)
    {
        $data = $this->select(
            'receive_id',
            'detail_line_num',
            'sub_line_num',
            'product_code',
            'child_product_code',
            'product_status',
            'received_order_num'
        )
                ->where(function ($subQuery) use ($arrCheck) {
                    foreach ($arrCheck as $item) {
                        $subQuery->orWhere(function ($subWhere) use ($item) {
                            $subWhere->where('receive_id', '=', $item['receive_id'])
                            ->where('detail_line_num', '=', $item['detail_line_num'])
                            ->where('sub_line_num', '=', $item['sub_line_num']);
                        });
                    }
                });
        return $data->get();
    }
    /**
     * Get data by receive_id
     *
     * @param  array $arrQuery
     * @return object
     */
    public function getItem($arrQuery)
    {
        $data = $this->select('receive_id', 'delivery_type', 'price_supplier_id')
                    ->join('mst_product', 'mst_product.product_code', '=', 'dt_order_product_detail.product_code')
                    ->where('receive_id', '=', $arrQuery['receive_id'])
                    ->groupBy('delivery_type', 'price_supplier_id');
        return $data->get();
    }

    /**
     * Get max item
     *
     * @param  int    $receiveId
     * @return object
     */
    public function getMaxItem($receiveId)
    {
        $data = $this->select('receive_id', 'detail_line_num', 'sub_line_num')
                ->where('receive_id', $receiveId)
                ->orderBy('sub_line_num', 'desc')
                ->first();
        return $data;
    }

    /**
     * Update order by receive_id and detail line num
     *
     * @param  int     $receiveId
     * @param  int     $detailLineNum
     * @param  array   $data
     * @return boolean
     */
    public function updateGroupProduct($receiveId, $detailLineNum, $data)
    {
        if (array_key_exists('product_status', $data)) {
            $modelLog  = new LogUpdateStatusProduct();
            $backtrace = debug_backtrace(false, 2)[1];
            $className = $backtrace['class'];
            $function  = $backtrace['function'];
            $action    = $className . '@' . $function;
            $modelLog->insertLog([
                'receive_id'      => $receiveId,
                'detail_line_num' => $detailLineNum
            ], $data, $action);
        }
        return $this->where('receive_id', $receiveId)
                ->where('detail_line_num', $detailLineNum)
                ->update($data);
    }

    /**
     * Get product status by primary key
     *
     * @param  array $arrKey
     * @return object
     */
    public function getProductStatusByKey($arrKey)
    {
        $data = $this->select(
            'receive_id',
            'detail_line_num',
            'product_status'
        );
        foreach ($arrKey as $item) {
            $data->orWhere(function ($where) use ($item) {
                $where->where('receive_id', $item['receive_id'])
                    ->where('detail_line_num', $item['detail_line_num']);
            });
        }
        $data->groupBy('receive_id', 'detail_line_num');
        return $data->get();
    }

    /**
     * Get data by condition
     *
     * @param  array  $cols
     * @param  array  $conds
     * @return object
     */
    public function getDataByConds($cols, $conds)
    {
        $data = $this->select($cols)->where($conds)->get();
        return $data;
    }

    /**
     * Get data by condition receier id
     *
     * @param  int    $receiveId
     * @param  int    $receiverId
     * @param  array  $cols
     * @return object
     */
    public function getDataByReceiverId($receiveId, $receiverId, $cols = '*')
    {
        $data = $this->select($cols)
            ->join('mst_order_detail', function ($join) {
                $join->on('mst_order_detail.receive_id', '=', 'dt_order_product_detail.receive_id')
                    ->on('mst_order_detail.detail_line_num', '=', 'dt_order_product_detail.detail_line_num');
            })
            ->where([
                'dt_order_product_detail.receive_id' => $receiveId,
                'mst_order_detail.receiver_id' => $receiverId
            ])->get();
        return $data;
    }

    /**
     * Get data product by receive id
     *
     * @param   int     $receiveId
     * @return  object
     */
    public function getProductReturnByReceive($receiveId)
    {
        $col = [
            'dt_order_product_detail.sub_line_num',
            'dt_order_product_detail.detail_line_num',
            'dt_order_product_detail.product_code',
            'dt_order_product_detail.order_code',
            'od.price',
            'od.return_quantity',
            'mp.product_name',
            'mp.price_invoice',
            'ra.return_address',
            'ra.return_id',
            'mp.price_supplier_id',
            'dt_order_product_detail.child_product_code',
            'od.quantity',
        ];
        $result = $this->select($col)
                       ->selectRaw("CASE when ra.return_address is null then '返品住所ない' else ra.return_nm end as address")
                       ->selectRaw('(od.quantity - od.return_quantity) AS received_order_num')
                       ->leftjoin('mst_product AS mp', 'dt_order_product_detail.product_code', '=', 'mp.product_code')
                       ->leftjoin('mst_order_detail AS od', function ($query) {
                            $query->on('od.receive_id', '=', 'dt_order_product_detail.receive_id')
                                  ->on('od.detail_line_num', '=', 'dt_order_product_detail.detail_line_num');
                       })
                       ->leftjoin('mst_return_address AS ra', function ($join) {
                            $join->on('mp.price_supplier_id', '=', 'ra.supplier_cd')
                            ->where('ra.is_selected', 1);
                       })

                       ->where('dt_order_product_detail.receive_id', $receiveId)
                       ->groupBy('dt_order_product_detail.detail_line_num')
                       ->get();
        return $result;
    }

    /**
     * Get data product by product code
     *
     * @param   int     $productCode
     * @return  object
     */
    public function getChildProReturm($productCode, $receiveId)
    {
        $col = [
            'dt_order_product_detail.detail_line_num',
            'dt_order_product_detail.sub_line_num',
            'dt_order_product_detail.received_order_num',
            'mp.product_name',
            'mp.price_invoice',
            'mp.price_supplier_id',
            'dt_order_product_detail.child_product_code',
            'mps.component_num',
        ];
        $result = $this->select($col)
                       ->leftjoin(
                           'mst_product AS mp',
                           'dt_order_product_detail.child_product_code',
                           '=',
                           'mp.product_code'
                       )
                       ->leftjoin('mst_product_set AS mps', function ($query) {
                           $query->on('mps.child_product_code', '=', 'dt_order_product_detail.child_product_code')
                                 ->on('mps.parent_product_code', '=', 'dt_order_product_detail.product_code');
                       })
                       ->where('dt_order_product_detail.product_code', $productCode)
                       ->where('dt_order_product_detail.receive_id', $receiveId)
                       ->get();
        return $result;
    }

    /**
     * Get data product by receive id and product code
     *
     * @param   int     $receiveId
     * @return  object
     */
    public function getProByReceiveAndPro($receiveId, $productCode)
    {
        $col = [
            'dt_order_product_detail.product_code',
            'dt_order_product_detail.child_product_code',
        ];
        $result = $this->select($col)
                       ->where('dt_order_product_detail.receive_id', $receiveId)
                       ->where('dt_order_product_detail.product_code', $productCode)
                       ->get();
        return $result;
    }

    /**
     * Get data check update cancel edi
     *
     * @param   int     $receiveId
     * @param   string  $productCode
     *
     * @return  object
     */
    public function getDataUpdateCancelEDI($receiveId, $productCode = null)
    {
        $col = [
            't_order_detail.m_order_type_id AS m_order_type_id_detail',
            't_order_direct.m_order_type_id AS m_order_type_id_direct',
            'ots.edi_order_code',
            'ots.order_code',
            'ots.order_type',
            'pb.product_code',
            'pb.product_name',
            'dt_order_product_detail.delivery_type',
        ];
        $query = $this->select($col)
            ->join('dt_order_to_supplier AS ots', 'dt_order_product_detail.order_code', '=', 'ots.order_code')
            ->leftJoin('edi.t_order_detail', 'ots.edi_order_code', '=', 't_order_detail.order_code')
            ->leftJoin('edi.t_order_direct', 'ots.edi_order_code', '=', 't_order_direct.order_code')
            ->leftjoin('mst_product_base AS pb', 'pb.product_code', '=', 'dt_order_product_detail.product_code')
            ->where('dt_order_product_detail.receive_id', $receiveId);
            // ->where('dt_order_product_detail.delivery_type', '<>', 1);
        if (!is_null($productCode)) {
            $query->where('dt_order_product_detail.product_code', $productCode);
        }
        return $query->get();
    }

    /**
     * Get data check cancel all
     *
     * @param   int     $receiveId
     * @param   string  $productCode
     *
     * @return  number
    */
    public function checkReturnAll($receiveId, $productCode)
    {
        $result = $this->where('product_code', '<>', $productCode)
                       ->where('product_status', '<>', PRODUCT_STATUS['CANCEL'])
                       ->where('dt_order_product_detail.receive_id', $receiveId)
                       ->count();
        return $result;
    }

    /**
     * Get data cancel not enought
     *
     * @param   int     $receiveId
     * @param   string  $productCode
     *
     * @return  object
     */
    public function getDataCancelEDINotEnought($receiveId)
    {
        $col = [
            't_order_detail.m_order_type_id AS m_order_type_id_detail',
            't_order_direct.m_order_type_id AS m_order_type_id_direct',
            'ots.edi_order_code',
            'ots.order_code',
            'ots.order_type',
            'dt_order_product_detail.delivery_type',
            'dt_order_product_detail.detail_line_num',
            'dt_order_product_detail.sub_line_num',
        ];
        $result = $this->select($col)
            ->leftJoin('dt_order_to_supplier AS ots', 'dt_order_product_detail.order_code', '=', 'ots.order_code')
            ->leftJoin('edi.t_order_detail', 'ots.edi_order_code', '=', 't_order_detail.order_code')
            ->leftJoin('edi.t_order_direct', 'ots.edi_order_code', '=', 't_order_direct.order_code')
            ->leftjoin('mst_product_base AS pb', 'pb.product_code', '=', 'dt_order_product_detail.product_code')
            ->where('dt_order_product_detail.receive_id', $receiveId)
            ->get();
        return $result;
    }

    /**
     * Get data update orderring num
     *
     * @param  array $arrCheck
     * @return object
     */
    public function getDataUpdateOrderringNum($arrCheck)
    {
        $data = $this->select(
            'dt_order_product_detail.receive_id',
            'dt_order_product_detail.detail_line_num',
            'dt_order_product_detail.sub_line_num',
            'dt_order_product_detail.product_code',
            'dt_order_product_detail.child_product_code',
            'dt_order_product_detail.product_status',
            'dt_order_product_detail.received_order_num',
            'ots.order_type'
        )
        ->leftJoin('dt_order_to_supplier AS ots', 'dt_order_product_detail.order_code', '=', 'ots.order_code');
        $flg = false;
        foreach ($arrCheck as $item) {
            if ((int)$item['product_status'] === PRODUCT_STATUS['CANCEL']) {
                $flg = true;
                $data->orWhere(function ($subWhere) use ($item) {
                    $subWhere->where('dt_order_product_detail.receive_id', '=', $item['receive_id'])
                            ->where('dt_order_product_detail.detail_line_num', '=', $item['detail_line_num'])
                            ->where('dt_order_product_detail.sub_line_num', '=', $item['sub_line_num']);
                });
            }
        }
        if (!$flg) {
            return false;
        }
        return $data->get();
    }

    /**
     * Get data update stock when
     *
     * @param  int $receiveID
     * @return object
     */
    public function getDataUpdateStock($receiveID)
    {
        $col = [
            'dt_order_product_detail.product_code',
            'dt_order_product_detail.child_product_code',
            'dt_order_product_detail.received_order_num',
            'dt_order_product_detail.delivery_type',
            'dt_order_product_detail.product_status',
            'dt_order_to_supplier.order_type',
            'o.order_status',
        ];

        $result = $this->select($col)
                       ->leftJoin('dt_order_to_supplier', 'dt_order_product_detail.order_code', '=', 'dt_order_to_supplier.order_code')
                       ->join('mst_order as o', 'dt_order_product_detail.receive_id', '=', 'o.receive_id')
                       ->where('dt_order_product_detail.receive_id', $receiveID)
                       ->get();
        return $result;
    }

    /**
     * Check all product cancel
     *
     * @param  int $receiveId
     * @param  int $receiverId
     * @return boolean
     */
    public function checkAllProductCancel($receiveId, $receiverId)
    {
        $data = $this->from('dt_order_product_detail AS opd')
                    ->join('mst_order_detail AS od', function ($join) {
                        $join->on('opd.receive_id', '=', 'od.receive_id')
                            ->on('opd.detail_line_num', '=', 'od.detail_line_num');
                    })
                    ->where('od.receive_id', $receiveId)
                    ->where('od.receiver_id', '<>', $receiverId)
                    ->where('opd.product_status', '<>', PRODUCT_STATUS['CANCEL'])
                    ->count();
        if ($data > 0) {
            return false;
        }
        return true;
    }
}
