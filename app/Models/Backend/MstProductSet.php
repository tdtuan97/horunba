<?php

/**
 * Exe sql for hrnb.mst_product_set
 *
 * @package    App\Models\Backend
 * @subpackage MstProduct
 * @copyright  Copyright (c) 2018 RiverCrane! Corporation. All Rights Reserved.
 * @author     Truong Nghia<truong.van.nghia@rivercrane.vn>
 */

namespace App\Models\Backend;

use Illuminate\Database\Eloquent\Model;

class MstProductSet extends Model
{
    public $timestamps = false;
    /**
    * The database table used by the model.
    * @var string
    */
    protected $table = 'mst_product_set';

    /**
    * The database is used by the model.
    * @var string
    */
    protected $connection = 'horunba';

    /**
     * Get Information of product set
     *
     * @param  string $productCode
     * @return object
     */
    public function getInfoProductSet($productCode)
    {
        return $this->select([
                        'mst_product_supplier.product_code',
                        'mst_product_supplier.product_name',
                        'mst_product_supplier.price_invoice',
                        'mst_product_set.component_num',
                    ])
                    ->join('mst_product_supplier', 'mst_product_supplier.product_code', '=', 'mst_product_set.child_product_code')
                    ->where('mst_product_set.parent_product_code', $productCode)
                    ->get();
    }

    /**
     * Check product set
     *
     * @param  string $productCode
     * @return object
     */
    public function checkProductSetValid($productCode)
    {
        $result = $this->select(['mst_product_set.parent_product_code', 'mst_product.product_code'])
                       ->leftjoin('mst_product', 'mst_product.product_code', '=', 'mst_product_set.child_product_code')
                       ->where('mst_product_set.parent_product_code', $productCode)
                       ->get();
        return $result;
    }
    
    /**
     * Get info by product code
     *
     * @param  string  $parentProductCode
     * @param  string  $childProductCode
     * @return object
     */
    public function getItemByKey($parentProductCode, $childProductCode)
    {
        $data = $this->where('parent_product_code', $parentProductCode)
                    ->where('child_product_code', $childProductCode)
                    ->first();
        return $data;
    }
}
