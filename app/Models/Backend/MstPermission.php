<?php

/**
 * Exe sql for hrnb.mst_permission
 *
 * @package    App\Models\Backend
 * @subpackage MstPermission
 * @copyright  Copyright (c) 2019 RiverCrane! Corporation. All Rights Reserved.
 * @author     Truong Nghia<truong.van.nghia@rivercrane.vn>
 */

namespace App\Models\Backend;

use Illuminate\Database\Eloquent\Model;

class MstPermission extends Model
{
    public $timestamps = false;
    /**
    * The database table used by the model.
    * @var string
    */
    protected $table = 'mst_permission';

    /**
    * The database is used by the model.
    * @var string
    */
    protected $connection = 'horunba';

    /**
    * Get role by condition.
    * @param string $role
    * @param string $controller
    * @param string $action
    * @return object
    */
    public function getDepartOption($isAdmin = false)
    {
        $result = $this->select('depart_id AS key', 'depart_nm AS value');
        if ($isAdmin) {
            $result = $result->where('depart_id', '<>', '999');
        }
        return $result->distinct()
                       ->get();
    }

    /**
     * Get data
     *
     * @params  array    $arraySearch array data search
     * @return object
     */
    public function getData($arraySearch = null)
    {
        $col = [
            'mst_permission.depart_id',
            'mst_permission.action_denied',
            'mst_permission.screen_class',
            'mst_permission.depart_nm',
            'mst_permission.func_cap_cd',
            'mst_permission.func_cd',
            'mst_controller.screen_class AS screen_class_name',
            'mst_controller.screen_name',
            'mst_controller.action_list',
            'mst_controller.action_access',
            'mst_controller.no_list',
        ];
        $data = $this->select($col)
                     ->join('mst_controller', 'mst_permission.screen_class', '=', 'mst_controller.id');
        if (count($arraySearch) > 0) {
            $data->where(function ($query) use ($arraySearch) {
                if (isset($arraySearch['depart_id'])) {
                    if (is_array($arraySearch['depart_id'])) {
                        $query->whereIn('mst_permission.depart_id', $arraySearch['depart_id']);
                    } else {
                        $query->where('mst_permission.depart_id', $arraySearch['depart_id']);
                    }
                }
            });
        }
        $data->orderBy('mst_permission.depart_id', 'asc');
        $data->orderBy('mst_permission.screen_class', 'asc');
        $perPage = ($arraySearch['per_page']) ? $arraySearch['per_page'] : 20;
        $data = $data->paginate($perPage);
        return $data;
    }

    /**
     * Update data by key
     *
     * @param  array   $keys
     * @param  array   $data
     * @return boolean
     */
    public function updateData($keys, $data)
    {
        return $this->where($keys)->update($data);
    }

    /**
     * Get data check record is exists
     *
     * @param  string   $departId
     * @param  string   $screenClass
     * @return boolean
     */
    public function getDataCheckExists($departId, $screenClass)
    {
        return $this->where('depart_id', $departId)
                    ->where('screen_class', $screenClass)
                    ->get();
    }
}
