<?php
/**
 * Exe sql for hrnb.dt_store_order_info
 *
 * @package    App\Models
 * @subpackage DtStoreOrderInfo
 * @copyright  Copyright (c) 2018 RiverCrane! Corporation. All Rights Reserved.
 * @author     van.qui<van.qui.rcvn2012@gmail.com>
 */

namespace App\Models\Backend;

use Illuminate\Database\Eloquent\Model;

class DtStoreOrderInfo extends Model
{
    public $timestamps = false;
    /**
    * The database table used by the model.
    * @var string
    */
    protected $table = 'dt_store_order_info';
    /**
    * The database is used by the model.
    * @var string
    */
    protected $connection = 'horunba';

    /**
    * The primary key.
    * @var string
    */
    protected $primaryKey = 'order_id';

    /**
     * Get data of tabe
     *
     * @param  array  $arraySearch
     * @param  array  $arraySort
     * @return object
     */
    public function getData($arraySearch = null, $arraySort = null)
    {
        $data = $this->selectRaw(
            'dt_store_order_info.order_id,
            dt_store_order_info.inquiry_no,
            mst_store_info.store_name,
            dt_store_order_info.order_date,
            dt_store_order_info.status,
            dt_store_order_info.error_message,
            dt_store_order_info.delivery_plan_date,
            dt_store_order_info.delivery_date,
            dt_store_order_info.received_date,
            SUM(dt_store_order_detail.quantity) AS total_quantity,
            SUM(dt_store_order_detail.received_quantity) AS total_received_quantity,
            SUM(dt_store_order_detail.shortage_quantity) AS total_shortage_quantity'
        )
        ->leftJoin('mst_store_info', 'mst_store_info.store_id', '=', 'dt_store_order_info.storage_store_id')
        ->leftJoin('dt_store_order_detail', 'dt_store_order_detail.order_id', '=', 'dt_store_order_info.order_id')
        ->groupBy('dt_store_order_info.order_id');
        if (count($arraySearch) > 0) {
            $data->where(function ($query) use ($arraySearch) {
                if (isset($arraySearch['order_id_from'])) {
                    $query->where('dt_store_order_info.order_id', '>=', $arraySearch['order_id_from']);
                }
                if (isset($arraySearch['order_id_to'])) {
                    $query->where('dt_store_order_info.order_id', '<=', $arraySearch['order_id_to']);
                }
                if (isset($arraySearch['storage_store_id'])) {
                    if (is_array($arraySearch['storage_store_id'])) {
                        $query->whereIn('dt_store_order_info.storage_store_id', $arraySearch['storage_store_id']);
                    } else {
                        $query->where('dt_store_order_info.storage_store_id', $arraySearch['storage_store_id']);
                    }
                }
                if (isset($arraySearch['status'])) {
                    if (is_array($arraySearch['status'])) {
                        $query->whereIn('dt_store_order_info.status', $arraySearch['status']);
                    } else {
                        $query->where('dt_store_order_info.status', $arraySearch['status']);
                    }
                }
                if (isset($arraySearch['delivery_plan_date_from'])) {
                    $query->where(
                        'dt_store_order_info.delivery_plan_date',
                        '>=',
                        $arraySearch['delivery_plan_date_from']
                    );
                }
                if (isset($arraySearch['delivery_plan_date_to'])) {
                    $query->where(
                        'dt_store_order_info.delivery_plan_date',
                        '<=',
                        $arraySearch['delivery_plan_date_to']
                    );
                }
            });
        }
        if ($arraySort !== null && count($arraySort) > 0) {
            foreach ($arraySort as $column => $sort) {
                if ($sort !== null && in_array($sort, ['asc', 'desc'])) {
                    $data->orderBy($column, $sort);
                }
            }
        }
        $perPage = ($arraySearch['per_page']) ? $arraySearch['per_page'] : 20;
        $data = $data->paginate($perPage);
        return $data;
    }
}
