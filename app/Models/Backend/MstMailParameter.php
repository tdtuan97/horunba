<?php

/**
 * Exe sql for hrnb.mst_mail_parameter
 *
 * @package    App\Models
 * @subpackage MstMailParameter
 * @copyright  Copyright (c) 2017 RiverCrane! Corporation. All Rights Reserved.
 * @author     van.qui<van.qui.rcvn2012@gmail.com>
 */

namespace App\Models\Backend;

use Illuminate\Database\Eloquent\Model;

class MstMailParameter extends Model
{
    public $timestamps = false;
    /**
    * The database table used by the model.
    * @var string
    */
    protected $table = 'mst_mail_parameter';
    /**
    * The database is used by the model.
    * @var string
    */
    protected $connection = 'horunba';
}
