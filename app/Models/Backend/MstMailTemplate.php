<?php

/**
 * Exe sql for hrnb.mst_mail_template
 *
 * @package    App\Models
 * @subpackage MstMailTemplate
 * @copyright  Copyright (c) 2017 RiverCrane! Corporation. All Rights Reserved.
 * @author     le.hung<le.hung.rcvn2012@gmail.com>
 */

namespace App\Models\Backend;

use Illuminate\Database\Eloquent\Model;

class MstMailTemplate extends Model
{
    public $timestamps = false;
    /**
    * The database table used by the model.
    * @var string
    */
    protected $table = 'mst_mail_template';
    /**
    * The database is used by the model.
    * @var string
    */
    protected $connection = 'horunba';
    /**
    * The primary key.
    * @var string
    */
    protected $primaryKey = 'mail_id';

    /**
     * Get data of table mst_mail_template
     *
     * @params  array    $arraySearch array data search
     * @params  array    $arraySort   array data sort
     * @return object
     */
    public function getData($arraySearch = null, $arraySort = null)
    {
        $data = $this->select('mst_mail_template.*')
                ->addSelect('mst_genre.genre_name as name_genre')
                ->join('mst_genre', function ($join) {
                    $join->on('mst_mail_template.genre', '=', 'mst_genre.genre_id');
                });
        if (count($arraySearch) > 0) {
            $data->where(function ($query) use ($arraySearch) {
                if (isset($arraySearch['mail_id'])) {
                    $query->where('mail_id', "{$arraySearch['mail_id']}");
                }
                if (isset($arraySearch['mail_subject'])) {
                    $query->where('mail_subject', 'LIKE', "%{$arraySearch['mail_subject']}%");
                }
                if (isset($arraySearch['mail_content'])) {
                    $query->where('mail_content', 'LIKE', "%{$arraySearch['mail_content']}%");
                }
                if (isset($arraySearch['template_name'])) {
                    $query->where('template_name', 'LIKE', "%{$arraySearch['template_name']}%");
                }
                if (isset($arraySearch['genre'])) {
                    if (is_array($arraySearch['genre'])) {
                        $query->whereIn('mst_mail_template.genre', $arraySearch['genre']);
                    } else {
                        $query->where('mst_mail_template.genre', $arraySearch['genre']);
                    }
                }
            });
        }
        $check = false;
        if ($arraySort !== null && count($arraySort) > 0) {
            foreach ($arraySort as $column => $sort) {
                if ($sort !== null && in_array($sort, ['asc', 'desc'])) {
                    $data->orderBy($column, $sort);
                    $check = true;
                }
            }
        }
        if (!$check) {
            $data->orderBy('mst_mail_template.in_date', 'desc');
        }
        $perPage = ($arraySearch['per_page']) ? $arraySearch['per_page'] : 20;
        $data = $data->paginate($perPage);
        return $data;
    }
    /**
     * Get data of tabe dt_order_product_detail
     *
     * @params  int    $mailId
     * @return  object
     */
    public function getItem($mailId)
    {
        return $this->select('mst_mail_template.*')
                ->addSelect('mst_genre.genre_name as name_genre')
                ->join('mst_genre', function ($join) {
                    $join->on('mst_mail_template.genre', '=', 'mst_genre.genre_id');
                })
                ->where('mail_id', $mailId)->first();
    }

    /**
     * Get template name by genre
     *
     * @param   int         $genre
     * @return  collection
     */
    public function getTemplateNameByGenre($genre)
    {
        $data = $this->select('mail_id', 'template_name')->where('genre', '=', $genre)
            ->orderBy('sequence', 'ASC')->get();
        return $data;
    }

    /**
     * Update data
     *
     * @param  array $keys
     * @param  array $data
     * @return boolean
     */
    public function updateData($keys, $data)
    {
        $result = $this->where('receive_id', '=', $keys['receive_id'])
                    ->where('detail_line_num', '=', $keys['detail_line_num'])
                    ->where('sub_line_num', '=', $keys['sub_line_num'])
                    ->update($data);
        return true;
    }

    /**
     * Get data by keys
     *
     * @param  array $arrCheck
     * @return object
     */
    public function getDataByKeys($arrCheck)
    {
        $data = $this->select('receive_id', 'detail_line_num', 'sub_line_num', 'product_code', 'child_product_code')
                ->where(function ($subQuery) use ($arrCheck) {
                    foreach ($arrCheck as $item) {
                        $subQuery->orWhere(function ($subWhere) use ($item) {
                            $subWhere->where('receive_id', '=', $item['receive_id'])
                            ->where('detail_line_num', '=', $item['detail_line_num'])
                            ->where('sub_line_num', '=', $item['sub_line_num']);
                        });
                    }
                });
        return $data->get();
    }

    /**
     * Check unique mail template
     * @param  array      $value
     * @return boolean
     */
    public function checkUniqueMailTemplate($value)
    {
        $data = $this->where('genre', $value['genre'])
                    ->where('template_name', $value['template_name'])
                    ->where('mail_subject', $value['mail_subject']);
        if (!empty($value['mail_id'])) {
            $data->where('mail_id', '<>', $value['mail_id']);
        }
        $data = $data->first();
        if (!empty($data)) {
            return false;
        }
        return true;
    }
}
