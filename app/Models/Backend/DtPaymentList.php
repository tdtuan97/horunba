<?php

/**
 * Exe sql for hrnb.dt_payment_list
 *
 * @package    App\Models
 * @subpackage DtPaymentList
 * @copyright  Copyright (c) 2017 RiverCrane! Corporation. All Rights Reserved.
 * @author     Truong Nghia<truong.van.nghia@rivercrane.vn>
 */

namespace App\Models\Backend;

use Illuminate\Database\Eloquent\Model;

class DtPaymentList extends Model
{
    public $timestamps = false;
    /**
    * The database table used by the model.
    * @var string
    */
    protected $table = 'dt_payment_list';
    /**
    * The database is used by the model.
    * @var string
    */
    protected $connection = 'horunba';
    /**
    * The primary key.
    * @var string
    */
    protected $primaryKey = 'index';

    /**
     * Get list data payment unknow
     *
     * @param  string $arrSearch
     * @param  string $arrSort
     * @return object
     */
    public function getDataPatmentUnkown($arrSearch, $arrSort)
    {
        $col = [
            'index',
            'payment_name',
            'payment_price',
            'payment_date',
            'relative_time',
            'receive_account_num',
            'payment_code AS payment_method'
        ];
        $query = $this->select($col)
                       ->selectRaw("(case when payment_code = 2 then '三井住友銀行' else '楽天銀行' end) as payment_code")
                       ->whereNull('receive_id');
        if (count($arrSearch) > 0) {
            $query->where(function ($query) use ($arrSearch) {
                if (isset($arrSearch['payment_date_from'])) {
                    $query->where(
                        'dt_payment_list.payment_date',
                        '>=',
                        "{$arrSearch['payment_date_from']}"
                    );
                }
                if (isset($arrSearch['payment_date_to'])) {
                    $query->where(
                        'dt_payment_list.payment_date',
                        '<=',
                        "{$arrSearch['payment_date_to']}"
                    );
                }
                if (isset($arrSearch['payment_price_from'])) {
                    $query->where(
                        'dt_payment_list.payment_price',
                        '>=',
                        "{$arrSearch['payment_price_from']}"
                    );
                }
                if (isset($arrSearch['payment_price_to'])) {
                    $query->where(
                        'dt_payment_list.payment_price',
                        '<=',
                        "{$arrSearch['payment_price_to']}"
                    );
                }
                if (isset($arrSearch['account_num_from'])) {
                    $query->where(
                        'dt_payment_list.receive_account_num',
                        '>=',
                        "{$arrSearch['account_num_from']}"
                    );
                }
                if (isset($arrSearch['account_num_to'])) {
                    $query->where(
                        'dt_payment_list.receive_account_num',
                        '<=',
                        "{$arrSearch['account_num_to']}"
                    );
                }
            });
        }
        $check = false;
        if ($arrSort !== null && count($arrSort) > 0) {
            foreach ($arrSort as $column => $sort) {
                if (in_array($sort, ['asc', 'desc'])) {
                    $query->orderBy($column, $sort);
                    $check = true;
                }
            }
        }
        if (!$check) {
            $query->orderBy('dt_payment_list.in_date', 'asc');
        }
        $perPage = ($arrSearch['per_page']) ? $arrSearch['per_page'] : 20;
        $data = $query->paginate($perPage);
        return $data;
    }

    /**
     * Get info payment by index
     *
     * @param  string $index
     * @return object
     */
    public function getPaymentById($index)
    {
        return $this->where('index', '=', $index)->first();
    }
    /**
     * Update data by key
     *
     * @param  array   $keys
     * @param  array   $data
     * @return boolean
     */
    public function updateData($keys, $data)
    {
        return $this->where($keys)->update($data);
    }

    /**
     * Count check data exists when upload csv Mitsu Bank
     *
     * @param  array $param
     * @return number
     */
    public function checkInsertMitsuBank($param)
    {
        return $this->where('payment_name', $param['payment_name'])
                    ->where('payment_price', $param['payment_price'])
                    ->where('receive_account_num', $param['receive_account_num'])
                    ->whereNull('receive_order_id')
                    ->count();
    }
    /**
     * Count check data exists when upload csv Rakuten Bank
     *
     * @param  array $param
     * @return number
     */
    public function checkInsertRakutenBank($param)
    {
        return $this->where('receive_order_id', $param['receive_order_id'])
                    ->where('payment_price', $param['payment_price'])
                    ->where('payment_name', $param['payment_name'])
                    ->where('relative_time', $param['relative_time'])
                    ->count();
    }
}
