<?php
/**
 * Exe sql for hrnb.mst_shipping_fee
 *
 * @package    App\Models
 * @subpackage MstShippingFee
 * @copyright  Copyright (c) 2018 RiverCrane! Corporation. All Rights Reserved.
 * @author     van.qui<van.qui.rcvn2012@gmail.com>
 */

namespace App\Models\Backend;

use Illuminate\Database\Eloquent\Model;

class MstShippingFee extends Model
{
    public $timestamps = false;

    /**
    * The database table used by the model.
    * @var string
    */
    protected $table = 'mst_shipping_fee';

    /**
    * The database is used by the model.
    * @var string
    */
    protected $connection = 'horunba';

    /**
    * The primary key.
    * @var string
    */
    protected $primaryKey = 'index';

    /**
     * Get data list
     *
     * @return object
     */
    public function getData($arraySearch = null, $arraySort = null)
    {
        $data = $this->select('index', 'mall_id', 'charge_price', 'below_limit_price')
                    ->addSelect('start_date', 'end_date', 'is_delete', 'name_jp')
                    ->join('mst_mall', 'mst_mall.id', '=', 'mst_shipping_fee.mall_id');
        if (count($arraySearch) > 0) {
            $data->where(function ($query) use ($arraySearch) {
                if (isset($arraySearch['mall_id'])) {
                    if (is_array($arraySearch['mall_id'])) {
                        $query->whereIn('mall_id', $arraySearch['mall_id']);
                    } else {
                        $query->where('mall_id', $arraySearch['mall_id']);
                    }
                }
                if (isset($arraySearch['charge_price_from'])) {
                    $query->where('charge_price', '>=', "{$arraySearch['charge_price_from']}");
                }
                if (isset($arraySearch['charge_price_to'])) {
                    $query->where('charge_price', '<=', "{$arraySearch['charge_price_to']}");
                }
                if (isset($arraySearch['start_date_from'])) {
                    $query->where('start_date', '>=', "{$arraySearch['start_date_from']}");
                }
                if (isset($arraySearch['start_date_to'])) {
                    $query->where('start_date', '<=', "{$arraySearch['start_date_to']}");
                }
                if (isset($arraySearch['is_delete'])) {
                    if (is_array($arraySearch['is_delete'])) {
                        $query->whereIn('is_delete', $arraySearch['is_delete']);
                    } else {
                        $query->where('is_delete', $arraySearch['is_delete']);
                    }
                }
            });
        }
        if ($arraySort !== null && count($arraySort) > 0) {
            foreach ($arraySort as $column => $sort) {
                if ($sort !== null && in_array($sort, ['asc', 'desc'])) {
                    $data->orderBy($column, $sort);
                }
            }
        } else {
            $data->orderBy('mst_shipping_fee.in_date', 'desc');
        }
        $perPage = ($arraySearch['per_page']) ? $arraySearch['per_page'] : 20;
        $data = $data->paginate($perPage);
        return $data;
    }

    /**
     * Change is_active
     *
     * @param  int    $index
     * @param  array  $data
     * @return boolean
     */
    public function changeDelete($index, $data)
    {
        return $this->where('index', $index)->update($data);
    }

    /**
     * Get item by index
     *
     * @param  int   $index
     * @return object
     */
    public function getItem($index)
    {
        $data = $this->select('index', 'mall_id', 'charge_price', 'below_limit_price')
                    ->addSelect('start_date', 'end_date', 'is_delete')
                    ->where('index', $index)
                    ->first();
        return $data;
    }

    /**
     * Get item by index
     *
     * @param  int   $index
     * @return object
     */
    public function getItemByMall($mallId)
    {
        $data = $this->select('index', 'mall_id', 'charge_price', 'below_limit_price')
                    ->addSelect('start_date', 'end_date', 'is_delete')
                    ->where('mall_id', $mallId)
                    ->whereDate('start_date', '<=', date('Y-m-d'))
                    ->whereDate('end_date', '>=', date('Y-m-d'))
                    ->first();
        return $data;
    }
}
