<?php

/**
 * Exe sql for hrnb.mst_return_detail
 *
 * @package    App\Models\Backend
 * @subpackage MstReturnDetail
 * @copyright  Copyright (c) 2018 RiverCrane! Corporation. All Rights Reserved.
 * @author     Truong Nghia<truong.van.nghia@rivercrane.vn>
 */

namespace App\Models\Backend;

use Illuminate\Database\Eloquent\Model;

class MstReturnDetail extends Model
{
    public $timestamps = false;
    /**
    * The database table used by the model.
    * @var string
    */
    protected $table = 'mst_return_detail';

    /**
    * The database is used by the model.
    * @var string
    */
    protected $connection = 'horunba';

    /**
    * Get data option.
    * @return object
    */
    public function getData($returnId = null)
    {
        $result = $this->select('return_detail_id AS key', 'detail_name AS value');
        if (isset($returnId)) {
            $result = $result->where('return_id', $returnId);
        }
        return $result->get();
    }
}
