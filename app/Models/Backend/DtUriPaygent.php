<?php
/**
 * Model for dt_uri_paygent table.
 *
 * @package    App\Models\Backend
 * @subpackage DtUriPaygent
 * @copyright  Copyright (c) 2019 RiverCrane! Corporation. All Rights Reserved.
 * @author     Truong Nghia<truong.van.nghia@crivercrane.vn>
 */

namespace App\Models\Backend;

use Illuminate\Database\Eloquent\Model;

class DtUriPaygent extends Model
{
    public $timestamps = false;

    /**
     * The database table used by the model.
     * @var string
     */
    protected $table = 'dt_uri_paygent';

    /**
     * The database is used by the model.
     * @var string
     */
    protected $connection = 'horunba';

    /**
     * Update data by key
     *
     * @param  array   $keys
     * @param  array   $data
     * @return boolean
     */
    public function updateData($keys, $data)
    {
        return $this->where($keys)->update($data);
    }
}
