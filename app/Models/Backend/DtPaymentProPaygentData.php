<?php

/**
 * Exe sql for hrnb.dt_payment_pro_paygent_data
 *
 * @package    App\Models
 * @subpackage DtPaymentProPaygentData
 * @copyright  Copyright (c) 2018 RiverCrane! Corporation. All Rights Reserved.
 * @author     Truong Nghia<truong.van.nghia@rivercrane.vn>
 */

namespace App\Models\Backend;

use Illuminate\Database\Eloquent\Model;

class DtPaymentProPaygentData extends Model
{
    public $timestamps = false;
    /**
    * The database table used by the model.
    * @var string
    */
    protected $table = 'dt_payment_pro_paygent_data';
    /**
    * The database is used by the model.
    * @var string
    */
    protected $connection = 'horunba';
    /**
     * Get data process payment fregi
     * @return object
     */
    public function getDataProcessProPaygent()
    {
        $col = [
            'dt_payment_pro_paygent_data.order_id',
            'dt_payment_pro_paygent_data.payment_price',
            'dt_payment_pro_paygent_data.status',
        ];
        $result = $this->select($col)
                       ->where('dt_payment_pro_paygent_data.process_flg', 0)
                       ->where('dt_payment_pro_paygent_data.is_delete', 0)
                       ->groupBy('dt_payment_pro_paygent_data.order_id')
                       ->get();
        return $result;
    }
}
