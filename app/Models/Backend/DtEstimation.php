<?php

/**
 * Model for dt_estimation table.
 *
 * @package    App\Models\Backend
 * @subpackage DtEstimation
 * @copyright  Copyright (c) 2017 RiverCrane! Corporation. All Rights Reserved.
 * @author     Le Hung<le.hung.rcvn2012@gmail.com>
 */

namespace App\Models\Backend;

use Illuminate\Database\Eloquent\Model;

class DtEstimation extends Model
{
    public $timestamps = false;

    /**
    * The database table used by the model.
    * @var string
    */
    protected $table = 'dt_estimation';

    /**
    * The database is used by the model.
    * @var string
    */
    protected $connection = 'horunba';

    /**
    * The get data process.
    * @return object $result
    */
    public function getData($arraySearch = null, $arraySort = null, $type = null)
    {
        $col = [
            'dt_estimation.estimate_date',
            'dt_estimation.estimate_code',
            'dt_estimation.receive_id',
            'z.memo',
            'z.other',
            'z.item_code',
            'z.jan_code',
            'z.m_order_type_id',
            'z.quantity',
            'z.maker_name',
            'z.updated_at',
            'p.product_name',
            'p.product_code',
            'p.product_jan',
            's.supplier_nm'
        ];
        $data = $this->select($col)
                       ->join('edi.t_zaiko as z', 'estimate_code', '=', 'z.order_code')
                       ->join('mst_product_base as p', 'p.product_code', '=', 'z.item_code')
                       ->join('mst_supplier as s', 'z.suppliercd', '=', 's.supplier_cd')
                       ;
        if (count($arraySearch) > 0) {
            $data->where(function ($query) use ($arraySearch) {
                if (isset($arraySearch['not_answer'])) {
                    $query->where('z.m_order_type_id', 10)
                            ->whereRaw("(NOW() - dt_estimation.estimate_date) >= ?", 1);
                }
                if (isset($arraySearch['estimate_date_from'])) {
                    $dateFrom = date("Y-m-d 00:00:00", strtotime($arraySearch['estimate_date_from']));
                    $query->where('dt_estimation.estimate_date', '>=', $dateFrom);
                }
                if (isset($arraySearch['estimate_date_to'])) {
                    $dateTo = date("Y-m-d 23:59:59", strtotime($arraySearch['estimate_date_to']));
                    $query->where('dt_estimation.estimate_date', '<=', $dateTo);
                }
                if (isset($arraySearch['estimate_code_from']) && isset($arraySearch['estimate_code_to'])) {
                    $query->whereBetween(
                        'dt_estimation.estimate_code',
                        [$arraySearch['estimate_code_from'], $arraySearch['estimate_code_to']]
                    );
                } else {
                    if (isset($arraySearch['estimate_code_from'])) {
                        $query->where(
                            'dt_estimation.estimate_code',
                            '>=',
                            $arraySearch['estimate_code_from']
                        );
                    }
                    if (isset($arraySearch['estimate_code_to'])) {
                        $query->where(
                            'dt_estimation.estimate_code',
                            '<=',
                            $arraySearch['estimate_code_to']
                        );
                    }
                }
                if (isset($arraySearch['supplier_name'])) {
                    $query->where('s.supplier_nm', 'LIKE', "%{$arraySearch['supplier_name']}%");
                }
                if (isset($arraySearch['product_code'])) {
                    $query->where('p.product_code', 'LIKE', "%{$arraySearch['product_code']}%");
                }
                if (isset($arraySearch['m_order_type_id'])) {
                    if (is_array($arraySearch['m_order_type_id'])) {
                        $query->whereIn('z.m_order_type_id', $arraySearch['m_order_type_id']);
                    } else {
                        $query->where('z.m_order_type_id', $arraySearch['m_order_type_id']);
                    }
                }
            });
        }
        if (!is_null($type)) {
            return $data->get();
        }
        if ($arraySort !== null && count($arraySort) > 0) {
            foreach ($arraySort as $column => $sort) {
                if ($sort !== null && in_array($sort, ['asc', 'desc'])) {
                    $data->orderBy($column, $sort);
                }
            }
        } else {
            $data = $data->orderby('dt_estimation.estimate_date', 'ASC')
                       ->orderby('dt_estimation.estimate_code', 'DESC');
        }
        $perPage = ($arraySearch['per_page']) ? $arraySearch['per_page'] : 20;
        $data = $data->paginate($perPage);
        return $data;
    }
}
