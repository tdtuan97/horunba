<?php

/**
 * Exe sql for hrnb.dt_extra_order_list
 *
 * @package    App\Models
 * @subpackage DtExtraOrderList
 * @copyright  Copyright (c) 2018 RiverCrane! Corporation. All Rights Reserved.
 * @author     Truong Nghia<truong.van.nghia@crivercrane.vn>
 */

namespace App\Models\Backend;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class DtExtraOrderList extends Model
{
    public $timestamps = false;
    /**
    * The database table used by the model.
    * @var string
    */
    protected $table = 'dt_extra_order_list';

    /**
    * The database is used by the model.
    * @var string
    */
    protected $connection = 'horunba';

    /**
    * The primary key.
    * @var string
    */
    protected $primaryKey = 'index';

    /**
     * Get data list
     * @param array $arraySearch list condition search
     * @param array $arraySort list sort
     * @return object
     */
    public function getData($arraySearch = null, $arraySort = null)
    {
        $col = [
            'dt_extra_order_list.index',
            'dt_extra_order_list.product_code',
            'dt_extra_order_list.supplier_id',
            'dt_extra_order_list.order_num',
            'dt_extra_order_list.order_price',
            'dt_extra_order_list.is_ordered',
            'dt_extra_order_list.is_cancel',
            'dt_extra_order_list.note',
            'dt_extra_order_list.error_code',
            'dt_extra_order_list.error_message',
            'mst_product_supplier.product_name',
            'mst_product_supplier.price_invoice',
            'mst_supplier.supplier_nm',

        ];
        $data = $this->select($col)
            ->join('mst_product_supplier', 'mst_product_supplier.product_code', '=', 'dt_extra_order_list.product_code')
            ->join('mst_supplier', 'mst_supplier.supplier_cd', '=', 'dt_extra_order_list.supplier_id');
        if (count($arraySearch) > 0) {
            $data->where(function ($query) use ($arraySearch) {
                if (isset($arraySearch['product_code'])) {
                    $query->where('dt_extra_order_list.product_code', 'LIKE', "%{$arraySearch['product_code']}%");
                }
                if (isset($arraySearch['supplier_id'])) {
                    $query->where('dt_extra_order_list.supplier_id', '=', "{$arraySearch['supplier_id']}");
                }
                if (isset($arraySearch['is_ordered'])) {
                    $query->where('is_ordered', $arraySearch['is_ordered']);
                }
                if (isset($arraySearch['is_cancel'])) {
                    $query->where('is_cancel', $arraySearch['is_cancel']);
                }
            });
        }
        if ($arraySort !== null && count($arraySort) > 0) {
            foreach ($arraySort as $column => $sort) {
                if ($sort !== null && in_array($sort, ['asc', 'desc'])) {
                    $data->orderBy($column, $sort);
                }
            }
        } else {
            $data->orderBy('dt_extra_order_list.in_date', 'desc');
        }
        $perPage = ($arraySearch['per_page']) ? $arraySearch['per_page'] : 20;
        $data = $data->paginate($perPage);
        return $data;
    }

    /**
     * Get data detail by id
     * @param string $index
     * @return object
     */
    public function getDataDetail($index)
    {
        $col = [
            'mst_product_base.product_name',
            'mst_supplier.supplier_nm AS supplier_name',
            'dt_extra_order_list.order_code',
            'dt_extra_order_list.order_date',
            'dt_extra_order_list.product_code',
            'dt_extra_order_list.supplier_id',
            'dt_extra_order_list.order_num',
            'dt_extra_order_list.order_price',
            'dt_extra_order_list.note',
            'dt_extra_order_list.is_cancel',
            'dt_extra_order_list.is_ordered',
        ];
        $result = $this->select($col)
                       ->leftjoin(
                           'mst_product_base',
                           'mst_product_base.product_code',
                           '=',
                           'dt_extra_order_list.product_code'
                       )
                       ->leftjoin('mst_supplier', 'mst_supplier.supplier_cd', '=', 'dt_extra_order_list.supplier_id')
                       ->where('index', $index)
                       ->first();
        return $result;
    }

    /**
     * Get data process order to supplier
     * @return object
     */
    public function getDataProcessOrder2Supplier()
    {
        $col = [
            'mst_product.product_jan',
            'mst_product.stock_lower_limit',
            'mst_product.stock_default',
            'mst_product.rod_unit',
            'dt_extra_order_list.product_code',
            'dt_extra_order_list.supplier_id',
            'dt_extra_order_list.order_num',
            'dt_extra_order_list.order_price',
            'dt_extra_order_list.index',
            'emp.cheetah_status'
        ];
        $result = $this->select($col)
                       ->join('mst_product', 'mst_product.product_code', '=', 'dt_extra_order_list.product_code')
                       ->join('edi.mst_product as emp', function ($join) {
                            $join->on('dt_extra_order_list.product_code', '=', 'emp.product_code')
                            ->where('emp.is_live', '=', 1);
                        })
                       ->where(function ($subWhere) {
                            $subWhere->where('error_code', '<>', 'E')
                                     ->orWhereNull('error_code');
                       })
                       ->where('is_ordered', 0)
                       ->where('is_cancel', 0)
                       ->get();
        return $result;
    }

    public function updateData($arrIndex, $arrUpdate)
    {
        return $this->whereIn('index', $arrIndex)
                    ->update($arrUpdate);
    }
}
