<?php

/**
 * Exe sql for hrnb.dt_delivery
 *
 * @package    App\Models
 * @subpackage DtDelivery
 * @copyright  Copyright (c) 2018 RiverCrane! Corporation. All Rights Reserved.
 * @author     van.qui<van.qui.rcvn2012@gmail.com>
 */

namespace App\Models\Backend;

use Illuminate\Database\Eloquent\Model;

class DtDelivery extends Model
{
    public $timestamps = false;
    /**
    * The database table used by the model.
    * @var string
    */
    protected $table = 'dt_delivery';
    /**
    * The database is used by the model.
    * @var string
    */
    protected $connection = 'horunba';

    /**
     * Update delivery code
     *
     * @param  int     $receivedOrderId
     * @param  int     $detailLineNum
     * @param  array   $dataUpdate
     * @return boolean
     */
    public function updateDeliveryCode($receivedOrderId, $detailLineNum, $dataUpdate)
    {
        return $this->where('received_order_id', $receivedOrderId)
                    ->where('detail_line_num', $detailLineNum)
                    ->update($dataUpdate);
    }
    /**
     * Get data of tabe
     *
     * @param  array  $arraySearch
     * @param  array  $arraySort
     * @return object
     */
    public function getData($arraySearch = null, $arraySort = null)
    {
        $cols = [
            'dt_delivery.received_order_id',
            'dt_delivery.subdivision_num',
            'dt_delivery.inquiry_no',
            'dt_delivery.delivery_date',
            'dt_delivery.delivery_time',
            'dt_delivery.delivery_instrustions',
            'dt_delivery.shipping_instructions',
            'dt_delivery.detail_line_num',
            'dt_delivery.account',
            'dt_delivery.delivery_plan_date',
            'dt_delivery.total_price',
            'error_message',
            'dt_delivery.delivery_status',
            'mst_mall.name_jp',
            'mst_shipping_company.company_name'
        ];
        $data = $this->select($cols)
                ->leftJoin('mst_order', function ($join) {
                    $join->on('mst_order.received_order_id', '=', 'dt_delivery.received_order_id');
                })
                ->leftJoin('mst_mall', function ($join) {
                    $join->on('mst_mall.id', '=', 'mst_order.mall_id');
                })
                ->leftJoin('mst_shipping_company', function ($join) {
                    $join->on('mst_shipping_company.company_id', '=', 'dt_delivery.delivery_code');
                });
        if (count($arraySearch) > 0) {
            $data->where(function ($query) use ($arraySearch) {
                if (isset($arraySearch['name_jp'])) {
                    if (is_array($arraySearch['name_jp'])) {
                        $query->whereIn('mst_mall.id', $arraySearch['name_jp']);
                    } else {
                        $query->where('mst_mall.id', $arraySearch['name_jp']);
                    }
                }
                if (isset($arraySearch['received_order_id'])) {
                    $query->where('dt_delivery.received_order_id', 'LIKE', "%{$arraySearch['received_order_id']}%");
                }
                if (isset($arraySearch['error_message'])) {
                    $query->where('dt_delivery.error_message', 'LIKE', "%{$arraySearch['error_message']}%");
                }
                if (isset($arraySearch['delivery_status'])) {
                    if (is_array($arraySearch['delivery_status'])) {
                        $query->whereIn('delivery_status', $arraySearch['delivery_status']);
                    } else {
                        $query->where('delivery_status', $arraySearch['delivery_status']);
                    }
                }
                if (isset($arraySearch['delivery_date'])) {
                    $date = str_replace('-', '', $arraySearch['delivery_date']);
                    $query->where(
                        'dt_delivery.delivery_date',
                        '=',
                        $date
                    );
                }
                if (isset($arraySearch['delivery_plan_date_from'])) {
                    $date = str_replace('-', '', $arraySearch['delivery_plan_date_from']);
                    $query->where(
                        'dt_delivery.delivery_plan_date',
                        '>=',
                        $date
                    );
                }
                if (isset($arraySearch['delivery_plan_date_to'])) {
                    $date = str_replace('-', '', $arraySearch['delivery_plan_date_to']);
                    $query->where(
                        'dt_delivery.delivery_plan_date',
                        '<=',
                        $date
                    );
                }
            });
        }
        $data = $data->groupBy('dt_delivery.received_order_id', 'subdivision_num');
        if ($arraySort !== null && count($arraySort) > 0) {
            foreach ($arraySort as $column => $sort) {
                if ($sort !== null && in_array($sort, ['asc', 'desc'])) {
                    $data->orderBy($column, $sort);
                }
            }
        } else {
            $data->orderBy('delivery_plan_date', 'desc');
        }
        $perPage = (isset($arraySearch['per_page'])) ? $arraySearch['per_page'] : 20;
        $data = $data->paginate($perPage);
        return $data;
    }
    /**
     * Get data of tabe
     *
     * @param  array  $arraySearch
     * @param  array  $arraySort
     * @return object
     */
    public function getItem($where)
    {
        $cols = [
            'received_order_id',
            'subdivision_num',
            'inquiry_no',
            'delivery_postal',
            'delivery_instrustions',
            'shipping_instructions',
            'delivery_city',
            'delivery_tel',
            'delivery_sub_address',
            'error_message',
            'delivery_status',
            'delivery_perfecture',
            'delivery_plan_date',
            'delivery_date',
            'delivery_code',
            'delivery_time',
        ];
        return $this->select($cols)->where($where)->lockForUpdate()->first();
    }
    /**
     * Get data of tabe
     *
     * @param  array  $arraySearch
     * @param  array  $arraySort
     * @return object
     */
    public function getItemGroup($where)
    {
        $cols = [
            'dt_delivery_detail.product_code',
            'dt_delivery_detail.product_name',
            'dt_delivery_detail.product_jan',
            'dt_delivery_detail.delivery_num'
        ];
        return $this->select($cols)
                ->leftJoin('dt_delivery_detail', function ($join) {
                    $join->on('dt_delivery_detail.received_order_id', '=', 'dt_delivery.received_order_id')
                         ->on('dt_delivery_detail.subdivision_num', '=', 'dt_delivery.subdivision_num');
                })
                ->where($where)
                ->whereRaw('IF(dt_delivery.detail_line_num <> 0, dt_delivery.detail_line_num = dt_delivery_detail.detail_line_num , 1 = 1)')
                ->groupBy(
                    [
                        'dt_delivery_detail.received_order_id',
                        'dt_delivery_detail.subdivision_num',
                        'dt_delivery_detail.detail_line_num'
                    ]
                )
                ->lockForUpdate()
                ->get();
    }
    /**
     * @param  array condition $where
     * @param  array update $data
     * @return boolean
     */
    public function updateData($where, $data)
    {
        $result = $this->where($where)
                    ->update($data);
        return $result;
    }
}
