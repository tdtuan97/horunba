<?php
/**
 * Listener Login successful
 *
 * @package     App\Listeners
 * @subpackage  LogSuccessfulLogin
 * @copyright   Copyright (c) 2017 RiverCrane! Corporation. All Rights Reserved.
 * @author      van.qui<van.qui.rcvn2012@gmail.com>
 */

namespace App\Listeners;

use Illuminate\Auth\Events\Login;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Http\Request;
use App\Models\Backend\MstTantouHistory;

class LogSuccessfulLogin
{
    /**
     * Create the event listener.
     *
     * @param  Request $request
     * @return void
     */
    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    /**
     * Handle the event.
     *
     * @param  Login  $event
     * @return void
     */
    public function handle(Login $event)
    {
        $user                = $event->user;
        $user->last_login_at = date('Y-m-d H:i:s');
        $user->last_login_ip = $this->request->ip();
        $user->save();

        $tokenId = $this->request->session()->token();
        $modelTH = MstTantouHistory::find($tokenId);
        if (empty($modelTH)) {
            $modelTH = new MstTantouHistory();
            $modelTH->token_id = $tokenId;
        }
        $modelTH->tantou_code   = $user->tantou_code;
        $modelTH->last_login_at = date('Y-m-d H:i:s');
        $modelTH->last_login_ip = $this->request->ip();
        
        $modelTH->save();
    }
}
