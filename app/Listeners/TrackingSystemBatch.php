<?php
/**
 * Listener for event batch
 *
 * @package     App\Listeners
 * @subpackage  UpdateStatusBatch
 * @copyright  Copyright (c) 2017 RiverCrane! Corporation. All Rights Reserved.
 * @author     Truong Nghia<truong.van.nghia@rivercrane.vn>
 */

namespace App\Listeners;

use App\Events\Command;
// use Illuminate\Queue\InteractsWithQueue;
// use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Http\Request;
use App\Models\Batches\MstStatusBatch;
use Illuminate\Support\Facades\Log;
use DB;
use App\Models\Batches\BatchAllLog;

class TrackingSystemBatch
{
    public $request;
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    /**
     * Handle the event.
     *
     * @param  Command  $event
     * @return void
     */
    public function handle(Command $event)
    {
        $modelSB = new MstStatusBatch();
        if (!empty($event->data['start'])) {
            if (env('DB_LOG', false)) {
                DB::enableQueryLog();
                DB::connection('edi')->enableQueryLog();
            }
            $datas = $modelSB->getDataBySignature($event->condition);
            // check active batch
            if (count($datas) !== 0 && $datas->is_active === 0) {
                $arrayReplace = [':', '-'];
                $folder       = str_replace($arrayReplace, '_', $event->condition);
                $folder       = explode(' ', $folder);
                $folder       = $folder[0];
                Log::useDailyFiles(storage_path() . "/logs/$folder/history.log");
                $classCommand = $datas->class_command;
                print_r("Batch $classCommand don't active." . PHP_EOL);
                Log::info("Batch $classCommand don't active." . PHP_EOL);
                exit;
            }
            $modelSB->where('command', '=', $event->condition)
                    ->update([
                        'status_flag'   => 1,
                        'error_message' => '',
                    ]);
        }
        if (!empty($event->data['end'])) {
            $message = '';
            $message = implode('|', $event->data['error']);
            $modelSB->where('command', '=', $event->condition)
                    ->update([
                        'error_message' => $message,
                        'status_flag' => 0
                    ]);
            //Tracking system horunba
            if (env('DB_LOG', false)) {
                foreach (DB::getQueryLog() as $log) {
                    $query    = $log['query'];
                    $tmpQuery = substr($query, 0, 100);
                    $bindList = $log['bindings'];
                    $time     = $log['time'];
                    $string = "/^update ((?!mst_status_batch)(?!t_rakuten_order)(?!t_amazon_order)(?!t_yahoo_order)(?!t_yahoo_order_list)(?!dt_send_mail_list)(?!dt_receive_mail_list).)*$|^insert ((?!t_rakuten_order)(?!t_amazon_order)(?!t_yahoo_order)(?!t_yahoo_order_list)(?!log_update_status_order)(?!log_update_status_product)(?!dt_send_mail_list)(?!dt_receive_mail_list).)*$/";
                    if (preg_match($string, $tmpQuery)) {
                        foreach ($bindList as $binding) {
                            $value = is_numeric($binding) ? $binding : "'".$binding."'";
                            $query = preg_replace('/\?/', $value, $query, 1);
                        }
                        BatchAllLog::insert(array('class_action' => $event->condition, 'query_string' => $query));
                    }
                }
                //Tracking system edi
                foreach (DB::connection('edi')->getQueryLog() as $logEdi) {
                    $query    = $logEdi['query'];
                    $tmpQuery = substr($query, 0, 100);
                    $bindList = $logEdi['bindings'];
                    $time     = $logEdi['time'];
                    $string = "/^update|^insert/";
                    if (preg_match($string, $tmpQuery)) {
                        foreach ($bindList as $binding) {
                            $value = is_numeric($binding) ? $binding : "'".$binding."'";
                            $query = preg_replace('/\?/', $value, $query, 1);
                        }
                        BatchAllLog::insert(array('class_action' => $event->condition, 'query_string' => $query));
                    }
                }
            }
        }
        if (!empty($event->data['stop'])) {
            $modelSB->where('class_command', '=', $event->condition)
                    ->update([
                        'error_message' => $event->data['error'],
                        'status_flag' => 2
                    ]);
        }
    }
}
