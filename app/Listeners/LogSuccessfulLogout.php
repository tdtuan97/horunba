<?php
/**
 * Listener Logout successful
 *
 * @package     App\Listeners
 * @subpackage  LogSuccessfulLogout
 * @copyright   Copyright (c) 2018 RiverCrane! Corporation. All Rights Reserved.
 * @author      van.qui<van.qui.rcvn2012@gmail.com>
 */

namespace App\Listeners;

use Illuminate\Auth\Events\Logout;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Http\Request;
use App\Models\Backend\MstTantouHistory;

class LogSuccessfulLogout
{
    /**
     * Create the event listener.
     *
     * @param  Request $request
     * @return void
     */
    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    /**
     * Handle the event.
     *
     * @param  Logout  $event
     * @return void
     */
    public function handle(Logout $event)
    {
        $tokenId = $this->request->session()->token();
        $modelTH   = MstTantouHistory::find($tokenId);
        if (!empty($modelTH)) {
            $modelTH->last_logout = date('Y-m-d H:i:s');
            $modelTH->save();
        }
    }
}
