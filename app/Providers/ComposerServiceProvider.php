<?php
/**
 * Composer Service Provider
 *
 * @package App\Providers
 * @subpackage ComposerServiceProvider
 * @copyright Copyright (c) 2017 CriverCrane! Corporation. All Rights Reserved.
 * @author lam.vinh<lam.vinh@crivercrane.vn>
 */
namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class ComposerServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        view()->composer(['includes.header','layouts.main'], 'App\Http\ViewComposers\GlobalComposer');
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
