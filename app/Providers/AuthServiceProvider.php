<?php

namespace App\Providers;

use Illuminate\Support\Facades\Gate;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Illuminate\Auth\EloquentUserProvider;
use App\Custom\SecureGuard;
use App\Custom\CustomUserProvider;
use Auth;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        'App\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        //
        Auth::extend(
            'SecureGuard',
            function ($app) {
                $model       = $app['config']['auth.providers.users.model'];
                $provider    = new CustomUserProvider($app['hash'], $model);
                $secureGuard = new SecureGuard('SecureGuard', $provider, \App::make('session.store'), request());
                $secureGuard->setCookieJar(app()['cookie']);
                $secureGuard->setDispatcher(app()['events']);
                return $secureGuard;
            }
        );
    }
}
