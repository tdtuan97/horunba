<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Models\Backend\MstOrder;
use App\Models\Backend\MstOrderDetail;
use App\Models\Backend\MstProduct;
use App\Models\Backend\MstOrderStatus;
use App\Models\Backend\MstOrderSubStatus;
use App\Models\Backend\MstProductStatus;
use App\Models\Backend\MstProductSet;
use App\Models\Backend\MstSupplier;
use Illuminate\Support\Facades\Validator;
use DateTime;
use Cache;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        $modelOrder       = new MstOrder();
        $modelOrderDetail = new MstOrderDetail();
        $modelProduct     = new MstProduct();
        $modelProduct     = new MstProduct();
        $modelSupplier    = new MstSupplier();
        $modelProductSet  = new MstProductSet();
        //Postal code and address are different.
        Validator::extend(
            'checkPostalCodeAndAddress',
            function ($attribute, $value, $parameters, $validator) use ($modelOrder) {
                $check = $modelOrder->getDataCheckValidatePostal($value);
                if ($check > 0) {
                    return true;
                }
                return false;
            }
        );
        //Postal code and address of receiver are different.
        Validator::extend(
            'checkPostalCodeAndAddressReceiver',
            function ($attribute, $value, $parameters, $validator) use ($modelOrderDetail) {
                $receiverId = null;
                if (!empty($parameters[0])) {
                    $receiverId = $parameters[0];
                }
                $check = $modelOrderDetail->getDataCheckValidatePostalReceiver($value, $receiverId);
                if ($check > 0) {
                    return true;
                }
                return false;
            }
        );
        //Postal code and address are different new.
        Validator::extend(
            'checkPostalCodeAndAddressReceiverLength',
            function ($attribute, $value, $parameters, $validator) use ($modelOrderDetail) {
                $receiverId = null;
                if (!empty($parameters[0])) {
                    $receiverId = $parameters[0];
                }
                $check = $modelOrderDetail->getDataCheckValidatePostalReceiverLenth($value, $receiverId);
                if ($check === 0) {
                    return true;
                }
                return false;
            }
        );
        //Use credit card to buy too much so have to confirm to customer.
        Validator::extend(
            'checkLimitRequestPrice',
            function ($attribute, $value, $parameters, $validator) use ($modelOrder) {
                $data = $modelOrder->getDataByReceiveId($value);
                if ((($data->request_price > 120000 && $data->mall_id !== 9) || ($data->request_price > 500000 && $data->mall_id === 9)) &&
                    $data->payment_method === 1 &&
                    $data->is_confirmed < 2) {
                    return false;
                }
                return true;
            }
        );
        //Direct delivery is can not use payment on delivery.
        Validator::extend(
            'checkDeliveryDirectAndPayment',
            function ($attribute, $value, $parameters, $validator) use ($modelOrder) {
                $check = $modelOrder->getDataCheckDirectAndPayment($value);
                if ($check > 0) {
                    return false;
                }
                return true;
            }
        );
        //Check Tel's digits
        Validator::extend(
            'checkTelDigits',
            function ($attribute, $value, $parameters, $validator) use ($modelOrderDetail) {
                $receiverId = null;
                if (!empty($parameters[0])) {
                    $receiverId = $parameters[0];
                }
                $check = $modelOrderDetail->getDataCheckTelDigits($value, $receiverId);
                if ($check > 0) {
                    return false;
                }
                return true;
            }
        );
        //Check Tel's digits 1
        Validator::extend(
            'checkTelDigits1',
            function ($attribute, $value, $parameters, $validator) use ($modelOrderDetail) {
                $receiverId = null;
                if (!empty($parameters[0])) {
                    $receiverId = $parameters[0];
                }
                $check = $modelOrderDetail->getDataCheckTelDigits1($value, $receiverId);
                if ($check > 0) {
                    return false;
                }
                return true;
            }
        );
        //Black list
        Validator::extend(
            'checkBlackList',
            function ($attribute, $value, $parameters, $validator) use ($modelOrder) {
                $check = $modelOrder->getDataCheckBackList($value);
                if ($check > 0) {
                    return false;
                }
                return true;
            }
        );
        //Because transport relay fee occurrent, please re-calculate.
        Validator::extend(
            'checkChargeFeeTransport',
            function ($attribute, $value, $parameters, $validator) use ($modelOrder) {
                $receiverId = null;
                if (!empty($parameters[0])) {
                    $receiverId = $parameters[0];
                }
                $check = $modelOrder->getDataCheckFeeTransport($value, $receiverId);
                if ($check > 0) {
                    return false;
                }
                return true;
            }
        );
        //Delivery free, please re-calculate
        Validator::extend(
            'checkChargeFeeDelivery',
            function ($attribute, $value, $parameters, $validator) use ($modelOrder) {
                $receiverId = null;
                if (!empty($parameters[0])) {
                    $receiverId = $parameters[0];
                }
                $check = $modelOrder->getDataCheckFeeDelivery($value, $receiverId);
                if ($check > 0) {
                    return false;
                }
                return true;
            }
        );
        //Payment on delivery fee is wrong, please re-calculate
        Validator::extend(
            'checkPaymentDeliveryFee',
            function ($attribute, $value, $parameters, $validator) use ($modelOrder) {
                $check = $modelOrder->getDataCheckPaymentDeliveryFee($value);
                if ($check > 0) {
                    return false;
                }
                return true;
            }
        );
        //Delivery fee is wrong, please re-calculate.
        Validator::extend(
            'checkChargeFeeCulDelivery',
            function ($attribute, $value, $parameters, $validator) use ($modelOrder) {
                $receiverId = null;
                if (!empty($parameters[0])) {
                    $receiverId = $parameters[0];
                }
                $check = $modelOrder->getDataCheckFeeCulDelivery($value, $receiverId);
                if ($check > 0) {
                    return false;
                }
                return true;
            }
        );
        //All price has paid by point, but request price is not equal 0
        Validator::extend(
            'checkPaidByPoint',
            function ($attribute, $value, $parameters, $validator) use ($modelOrder) {
                $check = $modelOrder->getDataCheckPaidByPoint($value);
                if ($check > 0) {
                    return false;
                }
                return true;
            }
        );
        //請求額が0円なのに全額ポイントじゃない
        Validator::extend(
            'checkPaidNotByPoint',
            function ($attribute, $value, $parameters, $validator) use ($modelOrder) {
                $check = $modelOrder->getDataCheckPaidNotByPoint($value);
                if ($check > 0) {
                    return false;
                }
                return true;
            }
        );
        Validator::extend('phone_number', function ($attribute, $value, $parameters, $validator) {
            if (!empty($value) && preg_match("/^[0-9]{10,11}$/", $value)) {
                return true;
            }
            return false;
        });
        Validator::extend('zip_code', function ($attribute, $value, $parameters, $validator) {
            if (!empty($value) && preg_match("/^\d{3}([-,\s])?\d{4}$/", $value)) {
                return true;
            }
            return false;
        });
        Validator::extend('fax_number', function ($attribute, $value, $parameters, $validator) {
            if (!empty($value) &&
                preg_match("/^(\+?\d{1,}(\s?|\-?)\d*(\s?|\-?)\(?\d{2,}\)?(\s?|\-?)\d{3,}\s?\d{3,})$/", $value)) {
                return true;
            }
            return false;
        });
        Validator::extend('checkProductCodeIsB340039', function ($attribute, $value, $parameters, $validator) {
            if (!empty($value) && $value !== 'B34-0039') {
                return true;
            }
            return false;
        });
        //Check product code exists
        Validator::extend(
            'checkProductCodeExists',
            function ($attribute, $value, $parameters, $validator) use ($modelProduct) {
                $check = $modelProduct->checkProductCodeExists($value);
                if ($check > 0) {
                    return true;
                }
                return false;
            }
        );
        //Check supplier id exists
        Validator::extend(
            'checkSupplierIdExists',
            function ($attribute, $value, $parameters, $validator) use ($modelSupplier) {
                $check = $modelSupplier->checkSupplierIdExists($value);
                if (!empty($check)) {
                    return true;
                }
                return false;
            }
        );
        //Check product code exists
        Validator::extend(
            'checkProductSetValid',
            function ($attribute, $value, $parameters, $validator) use ($modelProductSet) {
                $check = $modelProductSet->checkProductSetValid($value);
                if (count($check) === 0) {
                    return true;
                } else {
                    foreach ($check as $value) {
                        if (empty($value->product_code)) {
                            return false;
                        }
                    }
                }
                return true;
            }
        );
        //Check date
        Validator::extend('isValidDate', function ($parameters, $value, $attribute) {
            $date = DateTime::createFromFormat("Y-m-d", $value);
            if (!$date) {
                return false;
            } else {
                return true;
            }
        });

        //defind status constants
        $orderStatus = Cache::rememberForever('order_status_cache', function () {
            return MstOrderStatus::get(
                ['order_status_id', 'status_name_en']
            )->pluck('order_status_id', 'status_name_en')->toArray();
        });

        $orderSubStatus = Cache::rememberForever('order_sub_status_cache', function () {
            return MstOrderSubStatus::get(
                ['order_sub_status_id', 'sub_status_name_en']
            )->pluck('order_sub_status_id', 'sub_status_name_en')->toArray();
        });

        $productStatus = Cache::rememberForever('product_status_cache', function () {
            return MstProductStatus::get(
                ['product_status_id', 'status_name_en']
            )->pluck('product_status_id', 'status_name_en')->toArray();
        });

        define('ORDER_STATUS', $orderStatus);
        define('ORDER_SUB_STATUS', $orderSubStatus);
        define('PRODUCT_STATUS', $productStatus);
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
