<?php
namespace App\Exports;

use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use App\Models\Backend\DtUriMapping;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Events\AfterSheet;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;
use Maatwebsite\Excel\Concerns\WithColumnFormatting;

class UriExport implements WithEvents, FromView, ShouldAutoSize, WithColumnFormatting
{

    private $request;

    private $lastNum;

    public function __construct($request)
    {
        $this->request = $request;
    }

    /**
     * @return array
     */
    public function columnFormats(): array
    {
        return [
            'B' => NumberFormat::FORMAT_TEXT,
        ];
    }

    /**
     * @return array
     */
    public function registerEvents(): array
    {
        return [
            AfterSheet::class    => function(AfterSheet $event) {
                $event->sheet->styleCells(
                    "H" . ($this->lastNum + 1),
                    [
                        'font' => [
                            'size' => 16,
                            'bold' => true
                        ]
                    ]
                );
                $event->sheet->styleCells(
                    "S" . ($this->lastNum + 3),
                    [
                        'font' => [
                            'size' => 16,
                            'bold' => true
                        ]
                    ]
                );
                $event->sheet->styleCells(
                    "S" . ($this->lastNum + 4),
                    [
                        'font' => [
                            'size' => 16,
                            'bold' => true
                        ]
                    ]
                );
                $event->sheet->styleCells(
                    "S" . ($this->lastNum + 5),
                    [
                        'font' => [
                            'size' => 16,
                            'bold' => true
                        ]
                    ]
                );
                $event->sheet->styleCells(
                    "V" . ($this->lastNum + 3),
                    [
                        'font' => [
                            'size' => 16,
                            'bold' => true
                        ]
                    ]
                );
                $event->sheet->styleCells(
                    "V" . ($this->lastNum + 4),
                    [
                        'font' => [
                            'size' => 16,
                            'bold' => true
                        ]
                    ]
                );
                $event->sheet->styleCells(
                    "V" . ($this->lastNum + 5),
                    [
                        'font' => [
                            'size' => 16,
                            'bold' => true
                        ]
                    ]
                );
            },
        ];
    }
    /**
     * @return array
     */
    public function view(): View
    {
        $arrSearch = array(
            'mall_id'            => $this->request->input('name_jp', null),
            'received_order_id'  => $this->request->input('received_order_id', null),
            'order_date_from'    => $this->request->input('order_date_from', null),
            'order_date_to'      => $this->request->input('order_date_to', null),
            'delivery_date_from' => $this->request->input('delivery_date_from', null),
            'delivery_date_to'   => $this->request->input('delivery_date_to', null),
            'payment_name'       => $this->request->input('payment_name', null),
            'diff_price_flg'     => $this->request->input('diff_price_flg', null),
            'per_page'           => $this->request->input('per_page', null),
            'diff_price'         => $this->request->input('diff_price', null),
        );
        $arrSort = [
            'name_jp'            => $this->request->input('sort_name_jp', null),
            'received_order_id'  => $this->request->input('sort_received_order_id', null),
            'order_date'         => $this->request->input('sort_order_date', null),
            'delivery_date'      => $this->request->input('sort_delivery_date', null),
            'payment_method'     => $this->request->input('sort_payment_method', null),
            'web_payment_method' => $this->request->input('sort_web_payment_method', null),
            'request_price'      => $this->request->input('sort_request_price', null),
            'web_request_price'  => $this->request->input('sort_web_request_price', null),
            'different_price'    => $this->request->input('sort_different_price', null),
            'order_status'       => $this->request->input('sort_order_status', null),
            'web_order_status'   => $this->request->input('sort_web_order_status', null),
        ];

        $modelDUM  = new DtUriMapping();
        $datas     = $modelDUM->getData($arrSearch, $arrSort, ['type' => 'export-excel']);
        $this->lastNum = $index = $datas->count() + 1;
        $datas[] = [
            'payment_date'       => '小計：',
            'total_payment_plan' => "=Sum(I2:I$index)",
            'goods_price'        => "=Sum(J2:J$index)",
            'used_point'         => "=Sum(R2:R$index)",
            'used_coupon'        => "=Sum(U2:U$index)",
            'paid_point'         => "=Sum(X2:X$index)",
            'paid_coupon'        => "=Sum(Y2:Y$index)",
            'total_paid_price'   => "=Sum(AA2:AA$index)",
        ];
        $datas[] = [];
        $index++;
        $datas[] = [
            'coupon_paid_plan_date' => 'ポイント未入金合計：',
            'coupon_paid_date'      => "=R$index-X$index",
            'colspan'               => 3
        ];
        $datas[] = [
            'coupon_paid_plan_date' => 'クーポン未入金合計：',
            'coupon_paid_date'      => "=U$index-Y$index",
            'colspan'               => 3
        ];
        $datas[] = [
            'coupon_paid_plan_date' => '請求金額未入金：',
            'coupon_paid_date'      => "=I$index-AA$index",
            'colspan'               => 3
        ];
        return view('exports.uri-mapping', [
            'datas' => $datas->toArray()
        ]);
    }
}