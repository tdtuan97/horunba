<?php

/**
 * Notification for slack.
 *
 * @package App\Notifications
 * @subpackage SlackNotification
 * @author Truong Nghia<truong.van.nghia@rivercrane.vn>
 * @copyright Copyright (c) 2017 RiverCrane! Corporation. All Rights Reserved.
 */

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\SlackMessage;

class SlackNotification extends Notification
{
    public $notifiable;
    use Queueable;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($notifiable)
    {
        $this->notifiable = $notifiable;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['slack'];
    }

    /**
     * Get the Slack representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return SlackMessage
     */
    public function toSlack($notifiable)
    {
        return (new SlackMessage)
                ->from('Batch error', ':slack:')
                ->error()
                ->content($this->notifiable);
    }
}
