<?php
/**
 * Process web mapping yahoo
 *
 * @package    App\Console\Commands
 * @subpackage ProcessWebMappingYahoo
 * @copyright  Copyright (c) 2018 RiverCrane! Corporation. All Rights Reserved.
 * @author     Truong Nghia<truong.van.nghia@rivercrane.vn>
 */

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\Batches\YahooToken;
use App\Models\Batches\MstOrder;
use App\Models\Batches\DtWebYahData;
use App\Models\Batches\DtWebMapping;
use App\Models\Batches\MstPaymentMethod;
use Illuminate\Support\Facades\Log;
use App\Events\Command as eCommand;
use Event;
use Config;
use App\Helpers\Helper;

class ProcessWebMappingYahoo extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'process:web-mapping-yahoo';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * The error.
     *
     * @var array
     */
    public $error = [];

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $arrayReplace = [':', '-'];
        $folder       = str_replace($arrayReplace, '_', $this->signature);
        Log::useDailyFiles(storage_path() . "/logs/$folder/history.log");
        //Process data
        Event::fire(new eCommand($this->signature, array('start' => true)));
        $startBatch = microtime(true);
        Log::info('Start batch process web mapping for Yahoo.');
        print_r("Start batch process web mapping for Yahoo." . PHP_EOL);
        $yahooToken     = new YahooToken();
        $modelO         = new MstOrder();
        $modelYah       = new DtWebYahData();
        $modelMap       = new DtWebMapping();
        $modelPay       = new MstPaymentMethod();
        $accessToken    = $yahooToken->find('order')->access_token;
        $apiYahooFields = Config::get('common.api_yahoo_fields');
        $fields      = $this->getApiYahooFields($apiYahooFields, ['Status']);
        $strFields   = implode(",", $fields);
        $yahooUrl    = "https://circus.shopping.yahooapis.jp/ShoppingWebService/V1/orderInfo";
        $yahooHeader = array(
            "Authorization:Bearer " . $accessToken,
        );
        //Process get data api
        $dataOrder = $modelO->getOrderAPIWebMapping(2, 1000);
        $errors    = [];
        $total     = 0;
        foreach ($dataOrder as $order) {
            $retData = $this->processYahoo($yahooUrl, $yahooHeader, $apiYahooFields, $strFields, $order->received_order_id);
            if ($retData['flg'] === 0) {
                continue;
            }
            $data = $retData['data'];
            $modelYah->insert($data);
            $total++;
        }
        $message = "Insert dt_web_yah_data success: $total records.";
        Log::info($message);
        print_r($message . PHP_EOL);
        //Process get data api to check is correct
        $orderCorrect = $modelO->getOrderPayCorrect(2);
        $totalCount = 0;
        foreach ($orderCorrect as $order) {
            $retData = $this->processYahoo($yahooUrl, $yahooHeader, $apiYahooFields, $strFields, $order->received_order_id);
            if ($retData['flg'] === 0) {
                continue;
            }
            $data = $retData['data'];
            switch ($data[0]['order_status']) {
                case '5':
                    $yahooOrderStatus = 8;
                    break;
                case '3':
                    $yahooOrderStatus = 11;
                    break;
                case '4':
                    $yahooOrderStatus = 10;
                    break;
                default:
                    $yahooOrderStatus = 1;
                    break;
            }
            $checkOrder = $modelO->checkOrderMapping($order->received_order_id, $yahooOrderStatus);
            if ($checkOrder === 1) {
                $modelMap->updateData($order->received_order_id, $yahooOrderStatus);
                $totalCount++;
            }
        }
        $message = "Update dt_web_mapping success: $totalCount records.";
        Log::info($message);
        print_r($message . PHP_EOL);
        //end check column is_correct
        $dataYah     = $modelYah->getDataProcessWebYahoo();
        $dataPayment = $modelPay->getDataByMall(2);
        $count       = 0;
        if (count($dataYah) !== 0) {
            foreach ($dataYah as $data) {
                $arrInsert = [];
                try {
                    $paymentMethod = 0;
                    foreach ($dataPayment as $pay) {
                        if ($data->paymethod_name === $pay->payment_name) {
                            $paymentMethod = $pay->payment_code;
                            break;
                        }
                    }
                    $arrInsert['received_order_id']  = $data->order_id;
                    $arrInsert['mall_id']            = 2;
                    $arrInsert['web_payment_method'] = $paymentMethod;
                    switch ($data->order_status) {
                        case '5':
                            $arrInsert['web_order_status'] = 8;
                            break;
                        case '3':
                            $arrInsert['web_order_status'] = 11;
                            break;
                        case '4':
                            $arrInsert['web_order_status'] = 10;
                            break;
                        default:
                            $arrInsert['web_order_status'] = 1;
                            break;
                    }
                    $arrInsert['web_request_price'] = $data->settle_amount;
                    $differentType = [];
                    if ($data->payment_method !== $paymentMethod) {
                        $differentType[] = '支払方法';
                    }
                    if ($arrInsert['web_request_price'] !== $data->request_price && $data->order_order_status <> 10) {
                        $differentType[] = 'WEB金額';
                    }
                    if ($data->order_order_status !== $arrInsert['web_order_status']) {
                        $differentType[] = 'ステータス';
                    }
                    $arrInsert['different_type']  = implode("\n", $differentType);
                    $arrInsert['different_price'] = $data->request_price - $data->settle_amount;
                    $arrInsert['occorred_reason'] = 0;
                    $arrInsert['process_content'] = 0;
                    $arrInsert['finished_date']   = empty($arrInsert['different_type'])
                        ? date('Y-m-d H:i:s') : null;
                    $arrInsert['is_corrected']    = empty($arrInsert['different_type']) ? 1 : 0;
                    $arrInsert['is_deleted']      = 0;
                    $arrInsert['receive_id']      = 0;
                    $arrInsert['in_ope_cd']       = 'OPE99999';
                    $arrInsert['in_date']         = date('Y-m-d H:i:s');
                    $arrInsert['up_ope_cd']       = 'OPE99999';
                    $arrInsert['up_date']         = date('Y-m-d H:i:s');
                    $modelMap->insertIgnore($arrInsert);
                    $modelYah->where('order_id', $data->order_id)
                        ->update(['process_flg' => 1]);
                    $count++;
                } catch (\Exception $e) {
                    report($e);
                }
            }
        }
        $message = "Insert dt_web_mapping success: $count records.";
        Log::info($message);
        print_r($message . PHP_EOL);
        Event::fire(new eCommand($this->signature, array('end' => true, 'error' => $this->error)));
        $totalTime = round(microtime(true) - $startBatch, 2);
        Log::info("End batch process web mapping for Yahoo with total time: $totalTime s.");
        print_r("End batch process web mapping for Yahoo with total time: $totalTime s.");
    }

    /**
     * Get fields api yahoo
     *
     * @param   array  $apiYahooFields
     * @param   array  $ignoreFields
     * @return  array
     */
    private function getApiYahooFields($apiYahooFields, $ignoreFields = [])
    {
        $fields = array();
        foreach ($apiYahooFields as $key => $val) {
            $curVal = $val;
            do {
                if (is_array($curVal)) {
                    $subKey  = key($curVal);
                    $subVal  = $curVal[$subKey];
                    $curVal  = $subVal;

                    if (!is_array($subVal)) {
                        if ($subVal[0] !== '@') {
                            if (!in_array($subVal, $ignoreFields)) {
                                $fields[$key] = $subVal;
                            }
                        }
                        break;
                    }
                } else {
                    if ($curVal[0] !== '@') {
                        if (!in_array($curVal, $ignoreFields)) {
                            $fields[$key] = $curVal;
                        }
                    }
                    break;
                }
            } while (is_array($curVal));
        }
        return $fields;
    }

    /**
     * Process get data yahoo by api
     *
     * @param  string $yahooUrl
     * @param  array  $yahooHeader
     * @param  string $requestXml
     * @param  string $apiYahooFields
     * @param  string $strFields
     * @param  string $orderId
     * @return array
     */
    private function processYahoo($yahooUrl, $yahooHeader, $apiYahooFields, $strFields, $orderId)
    {
        $requestXml = view('mapping.xml.yahoo', [
            'orderId'  => $orderId,
            'field'    => $strFields,
            'sellerId' => 'diy-tool'
        ])->render();
        $ch = curl_init($yahooUrl);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $yahooHeader);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $requestXml);
        $responseXml = curl_exec($ch);
        curl_close($ch);
        $responData = simplexml_load_string($responseXml);
        if ($responData->getName() === "Error") {
            return [
                'flg'     => 0,
                'code'    => Helper::val($responData->Code),
                'message' => Helper::val($responData->Message)
            ];
        }
        $countItem  = count($responData->Result->OrderInfo->Item);
        $retData    = array();
        for ($i = 0; $i < $countItem; $i++) {
            $data = array();
            foreach ($apiYahooFields as $key => $val) {
                $curVal  = $val;
                $resData = $responData;
                do {
                    if (is_array($curVal)) {
                        $subKey  = key($curVal);
                        $subVal  = $curVal[$subKey];
                        $curVal  = $subVal;

                        if ($subKey === 'Item') {
                            $resData = $resData->{$subKey}[$i];
                        } else {
                            $resData = $resData->{$subKey};
                        }
                        if (!is_array($subVal)) {
                            if ($subVal[0] === '@') {
                                $data[$key] = Helper::val($resData->attributes()->{substr($subVal, 1)});
                            } else {
                                $data[$key] = Helper::val($resData->{$subVal});
                            }
                            break;
                        }
                    } else {
                        if ($curVal[0] === '@') {
                            $data[$key] = Helper::val($resData->attributes()->{substr($curVal, 1)});
                        } else {
                            $data[$key] = Helper::val($resData->{$curVal});
                        }
                        break;
                    }
                } while (is_array($curVal));
            }
            $data['yahoo_basket_id']   = (string)$responData->Result->OrderInfo->OrderId . '-'
                . (string)$responData->Result->OrderInfo->Item[$i]->LineId;
            $data['status_change_flg'] = 0;
            $data['form_cancel_flg']   = 0;
            $data['mail_serial']       = md5((string)$responData->Result->OrderInfo->OrderId);
            $data['process_flg']       = 0;
            $data['is_delete']         = 0;
            $data['created_at']        = date('Y-m-d H:i:s');
            $retData[] = $data;
        }
        return [
            'flg' => 1,
            'data' => $retData
        ];
    }
}
