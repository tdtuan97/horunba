<?php
/**
 * Batch process get result glsc
 *
 * @package    App\Console\Commands
 * @subpackage ProcessGetResultGlsc
 * @copyright  Copyright (c) 2018 RiverCrane! Corporation. All Rights Reserved.
 * @author     Truong Nghia<truong.van.nghia@rivercrane.vn>
 */
namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;
use App\Notifications\SlackNotification;
use App\Models\Batches\DtReturn;
use App\Models\Batches\MstStockStatus;
use App\Models\Batches\DtReceiptLedger;
use App\Models\Batches\TRequestPerformanceModelEdi;
use App\Notification;
use App\Custom\Utilities;
use App\Events\Command as eCommand;
use Event;
use DB;

class ProcessGetResultGlsc extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'process:get-result-glsc';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = "Get Receive's result";

    /**
     * The notification.
     *
     * @var object
     */
    protected $slack;

    /**
     * The error.
     *
     * @var array
     */
    public $error = [];

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $arrayReplace = [':', '-'];
        $folder       = str_replace($arrayReplace, '_', $this->signature);
        Log::useDailyFiles(storage_path() . "/logs/$folder/history.log");
        Event::fire(new eCommand($this->signature, array('start' => true)));
        $slack = new Notification(CHANNEL['horunba']);
        $start       = microtime(true);
        Log::info('Start batch process request GLSC.');
        print_r("Start batch process request GLSC." . PHP_EOL);
        $modelDR     = new DtReturn();
        $modelTR     = new TRequestPerformanceModelEdi();
        $modelSS     = new MstStockStatus();
        $modelRL     = new DtReceiptLedger();
        $dataMessage = $modelDR->getDataUpdateMessage();
        Log::info('=== Start process update error message.===');
        print_r("=== Start process update error message.===" . PHP_EOL);
        if (count($dataMessage) === 0) {
            Log::info('Update error message no data');
            print_r("Update error message no data. " . PHP_EOL);
        } else {
            $sucMess  = 0;
            $failMess = 0;
            foreach ($dataMessage as $value) {
                try {
                    $modelDR->updateData(
                        [
                            'return_no'     => $value->return_no,
                            'receive_count' => $value->receive_count,
                        ],
                        [
                            'error_code'          => $value->er_ErrorCd,
                            'error_message'       => $value->er_Message,
                            'receive_instruction' => 9,
                        ]
                    );
                    $sucMess++;
                } catch (\Exception $e) {
                    $this->error[] = Utilities::checkMessageException($e);
                    $error  = "------------------------------------------" . PHP_EOL;
                    $error .= basename(__CLASS__) . PHP_EOL;
                    $error .= Utilities::checkMessageException($e);
                    $error .= PHP_EOL . "------------------------------------------" . PHP_EOL;
                    $slack->notify(new SlackNotification($error));
                    Log::error(Utilities::checkMessageException($e));
                    print_r("$error");
                    $failMess++;
                }
            }
            $message  = "Update table dt_return success: $sucMess records";
            $message .= " and fail : $failMess records";
            Log::info($message);
            print_r($message . PHP_EOL);
        }
        Log::info('=== End process update error message.===');
        print_r("=== End process update error message.===" . PHP_EOL);
        Log::info('=== Start process update data dt_return.===');
        print_r("=== Start process update data dt_return.===" . PHP_EOL);
        $dataUpdate = $modelDR->getDateUpdateReturnResult();
        if (count($dataUpdate) === 0) {
            Log::info('Update table dt_return no data');
            print_r("Update table dt_return no data. " . PHP_EOL);
        } else {
            $sucUp  = 0;
            $failUp = 0;
            foreach ($dataUpdate as $value) {
                try {
                    $arrUpdate = [];
                    $arrUpdate['receive_real_date'] = $value->ArrivalDate;
                    $arrUpdate['note']              = $value->Note;
                    $cal = $value->PlanQuantity - $value->Quantity;
                    if ($cal > 0) {
                        $arrUpdate['receive_instruction'] = 8;
                    } elseif ($cal < 0) {
                        $arrUpdate['receive_instruction'] = 9;
                    } else {
                        $arrUpdate['receive_instruction'] = 2;
                    }
                    $quantity = $value->Quantity;
                    $arrUpdate['receive_real_num'] = DB::raw("receive_real_num + {$quantity}");
                    $modelDR->updateData(
                        [
                            'return_no'      => $value->return_no,
                            'receive_count'  => $value->RequestBNo,
                            'return_line_no' => $value->return_line_no,
                            'return_time'    => $value->return_time,
                        ],
                        $arrUpdate
                    );
                    if ($value->Quantity > 0) {
                        $modelRL->add2ReceiptLedgerInStockRIR([
                            'pa_order_code'        => $value->SupplyNo,
                            'pa_product_code'      => $value->ItemCd,
                            'pa_arrival_num'       => $value->Quantity,
                            'pa_supplier_id'       => $value->supplier_id,
                            'pa_stock_type'        => 4,
                            'pa_stock_detail_type' => 4,
                        ]);
                        $flgWait = false;
                        if ($value->delivery_instruction >= 0) {
                            $flgWait = true;
                        }
                        $modelSS->updateStockInStockR($value->ItemCd, $value->Quantity, $flgWait);
                    }

                    $modelTR->updateData(
                        ['tRequestPerformanceModel_autono' => $value->tRequestPerformanceModel_autono],
                        ['decsy_update_flg' => 2]
                    );
                    $sucUp++;
                } catch (\Exception $e) {
                    $this->error[] = Utilities::checkMessageException($e);
                    $error  = "------------------------------------------" . PHP_EOL;
                    $error .= basename(__CLASS__) . PHP_EOL;
                    $error .= Utilities::checkMessageException($e);
                    $error .= PHP_EOL . "------------------------------------------" . PHP_EOL;
                    $slack->notify(new SlackNotification($error));
                    Log::error(Utilities::checkMessageException($e));
                    print_r("$error");
                    $failUp++;
                }
            }
            $message  = "Update table dt_return success: $sucUp records";
            $message .= " and fail : $failUp records";
            Log::info($message);
            print_r($message . PHP_EOL);
        }
        Log::info('=== End process update data dt_return.===');
        print_r("=== End process update data dt_return.===" . PHP_EOL);
        Log::info('=== Start process insert data dt_return.===');
        print_r("=== Start process insert data dt_return.===" . PHP_EOL);
        $dataInsert = $modelTR->getDateInsertReturnResult();
        if (count($dataInsert) === 0) {
            Log::info('Insert table dt_return no data');
            print_r("Insert table dt_return no data. " . PHP_EOL);
        } else {
            $sucIn  = 0;
            $failIn = 0;
            foreach ($dataInsert as $value) {
                try {
                    $arrInsert = [];
                    $arrInsert['return_no']            = $value->RequestNo;
                    $arrInsert['return_line_no']       = 1;
                    $arrInsert['return_date']          = $value->ArrivalDate;
                    $arrInsert['deje_no']              = 0;
                    $arrInsert['return_type_large_id'] = 5;
                    $arrInsert['return_type_mid_id']   = 1;
                    $arrInsert['status']               = 1;
                    $arrInsert['receive_instruction']  = 2;
                    $arrInsert['receive_result']       = -1;
                    $arrInsert['delivery_instruction'] = -1;
                    $arrInsert['delivery_result']      = -1;
                    $arrInsert['red_voucher']          = -1;
                    $arrInsert['parent_product_code']  = $value->ItemCd;
                    $arrInsert['return_quantity']      = $value->Quantity;
                    $arrInsert['return_tanka']         = 0;
                    $arrInsert['supplier_id']          = $value->price_supplier_id;
                    $arrInsert['receive_plan_date']    = null;
                    $arrInsert['receive_real_date']    = now();
                    $arrInsert['receive_real_num']     = $value->Quantity;
                    $arrInsert['delivery_plan_date']   = null;
                    $arrInsert['delivery_real_date']   = null;
                    $arrInsert['delivery_real_num']    = 0;
                    $arrInsert['receive_id']           = null;
                    $arrInsert['payment_on_delivery']  = '';
                    $arrInsert['note']                 = $value->Note;
                    $arrInsert['receive_count']        = 0;
                    $arrInsert['delivery_count']       = 0;
                    $arrInsert['error_code']           = '';
                    $arrInsert['error_message']        = '';
                    $arrInsert['in_ope_cd']            = 'OPE99999';
                    $arrInsert['in_date']              = now();
                    $arrInsert['up_ope_cd']            = 'OPE99999';
                    $arrInsert['up_date']              = now();
                    $modelDR->insert($arrInsert);
                    $modelTR->updateData(
                        ['tRequestPerformanceModel_autono' => $value->tRequestPerformanceModel_autono],
                        ['decsy_update_flg' => 2]
                    );
                    $modelRL->add2ReceiptLedgerInStockRIR([
                        'pa_order_code'        => $value->RequestNo,
                        'pa_product_code'      => $value->ItemCd,
                        'pa_arrival_num'       => $value->Quantity,
                        'pa_supplier_id'       => $value->price_supplier_id,
                        'pa_stock_type'        => 4,
                        'pa_stock_detail_type' => 5,
                    ]);
                    $modelSS->updateStockInStockR($value->ItemCd, $value->Quantity, false);
                    $sucIn++;
                } catch (\Exception $e) {
                    $this->error[] = Utilities::checkMessageException($e);
                    $error  = "------------------------------------------" . PHP_EOL;
                    $error .= basename(__CLASS__) . PHP_EOL;
                    $error .= Utilities::checkMessageException($e);
                    $error .= PHP_EOL . "------------------------------------------" . PHP_EOL;
                    $slack->notify(new SlackNotification($error));
                    Log::error(Utilities::checkMessageException($e));
                    print_r("$error");
                    $failIn++;
                }
            }
            $message  = "Insert table dt_return success: $sucIn records";
            $message .= " and fail : $failIn records";
            Log::info($message);
            print_r($message . PHP_EOL);
        }
        Log::info('=== End process insert data dt_return.===');
        print_r("=== End process insert data dt_return.===" . PHP_EOL);
        Event::fire(new eCommand($this->signature, array('end' => true, 'error' => $this->error)));
        $totalTime = round(microtime(true) - $start, 2);
        Log::info("End batch process request GLSC with total time: $totalTime.");
        print_r("End batch process request GLSC with total time: $totalTime s." . PHP_EOL);
    }
}
