<?php

namespace App\Console\Commands;

use App\Notifications\SlackNotification;
use App\Models\Batches\MstStockStatus;
use App\Models\Batches\MstProduct;
use Illuminate\Support\Facades\Log;
use App\Events\Command as eCommand;
use Illuminate\Console\Command;
use App\Notification;
use Event;

class ProcessSyncProduct extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'process:sync-product';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * The notification.
     *
     * @var object
     */
    protected $slack;

    /**
     * The error.
     *
     * @var array
     */
    public $error = [];

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $arrayReplace = [':', '-'];
        $folder       = str_replace($arrayReplace, '_', $this->signature);
        Log::useDailyFiles(storage_path() . "/logs/$folder/history.log");
        Event::fire(new eCommand($this->signature, array('start' => true)));
        $slack = new Notification(CHANNEL['horunba']);
        $start = microtime(true);
        Log::info('Start batch process sync product.');
        print_r("Start batch process sync product." . PHP_EOL);
        $modelSS = new MstStockStatus();
        $modelP  = new MstProduct();
        $datas   = $modelP->getDataSyncProduct();
        $count   = $datas->count();
        if ($count === 0) {
            Log::info('No data.');
            print_r("No data." . PHP_EOL);
        } else {
            $arrInsert = [];
            foreach ($datas as $item) {
                $arrTemp = [];
                $arrTemp['product_code']      = $item->product_code;
                $arrTemp['nanko_num']         = 0;
                $arrTemp['glsc_num']          = 0;
                $arrTemp['order_zan_num']     = 0;
                $arrTemp['rep_orderring_num'] = 0;
                $arrTemp['replenish_num']     = 0;
                $arrTemp['return_num']        = 0;
                $arrTemp['wait_delivery_num'] = 0;
                $arrTemp['in_date']           = now();
                $arrTemp['up_date']           = now();
                $arrInsert[]                  = $arrTemp;
            }
            try {
                $arrSteps = array_chunk($arrInsert, 1000);
                foreach ($arrSteps as $step) {
                    $modelSS->insert($step);
                }
            } catch (Exception $e) {
                report($e);
            }
            Log::info("=== Sync success $count product.");
            print_r("=== Sync success $count product." . PHP_EOL);
        }

        Event::fire(new eCommand($this->signature, array('end' => true, 'error' => $this->error)));
        $totalTime = round(microtime(true) - $start, 2);
        Log::info("End batch process sync product with total time: $totalTime.");
        print_r("End batch process sync product with total time: $totalTime s." . PHP_EOL);
    }
}
