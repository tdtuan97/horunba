<?php
/**
 * Batch process Toriyose Order Cancel
 *
 * @package    App\Console\Commands
 * @subpackage ProcessToriyoseOrderCancel
 * @copyright  Copyright (c) 2018 RiverCrane! Corporation. All Rights Reserved.
 * @author     Van Qui<van.qui@rivercrane.vn>
 */

namespace App\Console\Commands;

//include app_path('lib/MarketplaceWebService/autoload.php');

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;
use App\Notifications\SlackNotification;
use App\Custom\Utilities;
use App\Notification;
use App\Events\Command as eCommand;
use Event;
use Config;
use Helper;
use DB;
use App;
use MarketplaceWebService_Client;
use MarketplaceWebService_Model_SubmitFeedRequest;
use MarketplaceWebService_Model_GetFeedSubmissionListRequest;
use MarketplaceWebService_Model_GetFeedSubmissionResultRequest;
use MarketplaceWebService_Exception;
use App\Models\Batches\MstOrder;
use App\Models\Batches\MstOrderDetail;
use App\Models\Batches\DtOrderProductDetail;
use App\Models\Batches\TOrderDirect;
use App\Models\Batches\TOrder;
use App\Models\Batches\TOrderDetail;
use App\Models\Batches\MstStockStatus;
use App\Http\Controllers\Common;

class ProcessToriyoseOrderCancel extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'process:toriyose-order-cancel';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Process Toriyose Order Cancel';

    /**
     * The notification.
     *
     * @var object
     */
    protected $slack;

    /**
     * The error.
     *
     * @var array
     */
    public $error = [];

    /**
     * Limit process run
     *
     * @var int
     */
    public $limit = 1000;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //Create log
        $arrayReplace = [':', '-'];
        $folder       = str_replace($arrayReplace, '_', $this->signature);
        Log::useDailyFiles(storage_path() . "/logs/$folder/history.log");
        //Process data
        Event::fire(new eCommand($this->signature, array('start' => true)));
        Log::info('Start batch process Toriyose order cancel.');
        print_r("Start batch process Toriyose order cancel." . PHP_EOL);
        $start       = microtime(true);
        $this->slack = new Notification(CHANNEL['horunba']);

        $this->processCancelOrder('direct');
        $this->processCancelOrder('detail');
        $this->processCancelOrder('stock');

        Event::fire(new eCommand($this->signature, array('end' => true, 'error' => $this->error)));
        $totalTime = round(microtime(true) - $start, 2);
        Log::info("End batch process Toriyose order cancel with total time: $totalTime s.");
        print_r("End batch process Toriyose order cancel with total time: $totalTime s.");
    }

    /**
     * Process order directly
     *
     * @param  string $type
     * @return void
     */
    private function processCancelOrder($type)
    {
        Log::info("Start process cancel order - $type.");
        print_r("Start process cancel order - $type." . PHP_EOL);

        $isCallAPI = true;
        if (App::environment(['local', 'test'])) {
            $isCallAPI = false;
        }

        $modelO = new MstOrder();
        $data   = $modelO->getDataToriyoseOrderCancel($type, $this->limit)->toArray();
        if (count($data) === 0) {
            Log::info('No data.');
            print_r('No data.' . PHP_EOL);
            goto end;
        }

        $azOrderCancels = array();
        $arrAzTotal     = array();
        $arrCond        = array();
        foreach ($data as $item) {
            if (!in_array($item['received_order_id'], $arrAzTotal)) {
                $arrTmp       = array();
                $arrAzTotal[] = $item['received_order_id'];
            }
            if (!empty($item['order_item_code'])) {
                $arrTmp[] = [
                    'order_item_code' => $item['order_item_code'],
                    'cancel_reason'   => 'NoInventory'
                ];
            }
            $azOrderCancels["".$item['received_order_id']] = [
                'receive_id'        => $item['receive_id'],
                'received_order_id' => $item['received_order_id'],
                'order_item'        => $arrTmp,
                'status_code'       => 'Failure'
            ];
            $arrCond[$item['receive_id']][$item['detail_line_num']][$item['sub_line_num']] = [
                'product_code' => $item['product_code'],
                'order_code'   => $item['order_code'],
                'received_order_num' => $item['received_order_num']
            ];
        }

        $this->cancelAmazon($type, $azOrderCancels, $arrCond, $isCallAPI);

        end:
        Log::info("End process cancel order - $type.");
        print_r("End process cancel order - $type." . PHP_EOL);
    }

    /**
     * Update status
     *
     * @param string $type
     * @param int    $recId
     * @param array  $arrCond
     */
    private function updateStatus($type, $recIds, $arrCond)
    {
        $modelOPD      = new DtOrderProductDetail();
        $modelTOD      = new TOrderDirect();
        $modelTO       = new TOrder();
        $modelTODetail = new TOrderDetail();
        $modelSS       = new MstStockStatus();
        foreach ($recIds as $recId) {
            $arrCondUpdate = isset($arrCond[$recId]) ? $arrCond[$recId] : array();
            foreach ($arrCondUpdate as $dln => $valDLN) {
                foreach ($valDLN as $sln => $val) {
                    $modelOPD->updateData($recId, $dln, $sln, ['product_status' => PRODUCT_STATUS['CANCEL']]);
                    if ($type === 'direct') {
                        $modelTOD->updateData(['order_code' => $val['order_code']], ['cancel_flg' => 1]);
                    } elseif ($type === 'detail') {
                        $modelTO->updateData(['order_code' => $val['order_code']], ['cancel_flg' => 1]);
                        $modelTODetail->updateData(['order_code' => $val['order_code']], ['cancel_flg' => 1]);
                    } elseif ($type === 'stock') {
                        $modelSS->updateOrderZanNum($val['product_code'], $val['received_order_num'], 'decrement');
                    }
                }
            }
        }
    }

    /**
     * Cancel amazon
     *
     * @param  string $type
     * @param  array  $azOrderCancel
     * @param  array  $arrCond
     * @param  bool   $isCallAPI
     * @return void
     */
    private function cancelAmazon($type, $azOrderCancel, $arrCond, $isCallAPI)
    {
        $modelO     = new MstOrder();
        $modelOD    = new MstOrderDetail();
        $receiveIds = array_column($azOrderCancel, 'receive_id');
        $dataUpdate = [
            'ship_charge'           => 0,
            'total_price'           => 0,
            'pay_charge_discount'   => 0,
            'used_point'            => 0,
            'used_coupon'           => 0,
            'request_price'         => 0,
            'pay_charge'            => 0,
            'goods_price'           => 0,
            'order_status'          => ORDER_STATUS['CANCEL'],
            'order_sub_status'      => ORDER_SUB_STATUS['DONE'],
            'is_sourcing_on_demand' => 3,
            'is_mall_update'        => 0,
            'up_ope_cd'             => 'OPE99999',
            'up_date'               => now()
        ];
        $dataODUpdate = ['price' => 0, 'quantity' => 0];
        $common = new Common();
        $common->cancelOrderEdi($receiveIds);
        if (!$isCallAPI) {
            $dataUpdate['message_api']    = '_DONE_';
            $dataUpdate['error_code_api'] = '_DONE_';
            $modelO->updateData($receiveIds, $dataUpdate);
            $modelOD->updateDataByReceiveIds($receiveIds, $dataODUpdate);
            $this->updateStatus($type, $receiveIds, $arrCond);
            echo "Process success" . PHP_EOL;
            Log::info("Process success");
            return true;
        }
        $modelOPD   = new DtOrderProductDetail();
        $feedType   = '_POST_ORDER_ACKNOWLEDGEMENT_DATA_';
        $config     = Config::get('amazonmws');
        $configHost = array(
            'ServiceURL'    => $config['ServiceURL'],
            'ProxyHost'     => $config['ProxyHost'],
            'ProxyPort'     => $config['ProxyPort'],
            'MaxErrorRetry' => $config['MaxErrorRetry'],
        );
        $service    = new MarketplaceWebService_Client(
            $config['AWS_ACCESS_KEY_ID'],
            $config['AWS_SECRET_ACCESS_KEY'],
            $configHost,
            $config['APPLICATION_NAME'],
            $config['APPLICATION_VERSION']
        );

        $params = [
            'data'               => $azOrderCancel,
            'merchantIdentifier' => $config['MERCHANT_ID']
        ];

        $feed = view('api.xml.amazon_cancel_order', $params)->render();

        //***************** SubmitFeed ****************
        $feedSubmissionInfo = $this->submitFeed($feedType, $feed, $config, $service);
        if ($feedSubmissionInfo['status'] === false) {
            $dataUpdate['message_api']    = $feedSubmissionInfo['data'];
            $dataUpdate['error_code_api'] = 'Fail';
            $modelO->updateData($receiveIds, $dataUpdate);
            $modelOD->updateDataByReceiveIds($receiveIds, $dataODUpdate);
            $this->updateStatus($type, $receiveIds, $arrCond);
            print_r($feedSubmissionInfo['data'] . PHP_EOL);
            Log::error($feedSubmissionInfo['data']);
            $this->postSlackAmazon($feedSubmissionInfo['data']);
            return false;
        }

        if (!$feedSubmissionInfo['data']->isSetFeedSubmissionId()) {
            $dataUpdate['message_api']    = 'No FeedSubmissionId';
            $dataUpdate['error_code_api'] = 'Fail';
            $modelO->updateData($receiveIds, $dataUpdate);
            $modelOD->updateDataByReceiveIds($receiveIds, $dataODUpdate);
            $this->updateStatus($type, $receiveIds, $arrCond);
            print_r("No FeedSubmissionId" . PHP_EOL);
            Log::error("No FeedSubmissionId");
            $this->postSlackAmazon("No FeedSubmissionId");
            return false;
        }
        $feedSubmissionId     = $feedSubmissionInfo['data']->getFeedSubmissionId();
        $feedSubmissionIdList = [$feedSubmissionId];
        //***************** GetFeedSubmissionList ****************
        //Do until DONE
        while (true) {
            sleep(80);
            $getFeedSubmissionListResult = $this->getFeedSubmissionList($feedSubmissionIdList, $config, $service);
            if ($getFeedSubmissionListResult['status'] === false) {
                $dataUpdate['message_api']    = $getFeedSubmissionListResult['data'];
                $dataUpdate['error_code_api'] = 'Fail';
                $modelO->updateData($receiveIds, $dataUpdate);
                $modelOD->updateDataByReceiveIds($receiveIds, $dataODUpdate);
                $this->updateStatus($type, $receiveIds, $arrCond);
                print_r($getFeedSubmissionListResult['data'] . PHP_EOL);
                Log::error($getFeedSubmissionListResult['data']);
                $this->postSlackAmazon($getFeedSubmissionListResult['data']);
                return false;
            }
            $feedSubmissionInfoList = $getFeedSubmissionListResult['data']->getFeedSubmissionInfoList();
            $feedProcessingStatus   = false;
            foreach ($feedSubmissionInfoList as $feedSubmissionInfo) {
                if ($feedSubmissionInfo->isSetFeedProcessingStatus()) {
                    $feedProcessingStatus = $feedSubmissionInfo->getFeedProcessingStatus();
                    break;
                }
            }
            if (!$feedProcessingStatus) {
                $dataUpdate['message_api']    = 'No FeedProcessingStatus';
                $dataUpdate['error_code_api'] = 'Fail';
                $modelO->updateData($receiveIds, $dataUpdate);
                $modelOD->updateDataByReceiveIds($receiveIds, $dataODUpdate);
                $this->updateStatus($type, $receiveIds, $arrCond);
                print_r('No FeedProcessingStatus' . PHP_EOL);
                Log::error('No FeedProcessingStatus');
                $this->postSlackAmazon('No FeedProcessingStatus');
                return false;
            }
            $arrStatusFinish = [
                '_DONE_',
                '_AWAITING_ASYNCHRONOUS_REPLY_',
                '_CANCELLED_',
                '_IN_SAFETY_NET_'
            ];

            if (in_array($feedProcessingStatus, $arrStatusFinish)) {
                $dataUpdate['message_api']    = $feedProcessingStatus;
                $dataUpdate['error_code_api'] = $feedProcessingStatus;
                $modelO->updateData($receiveIds, $dataUpdate);
                $modelOD->updateDataByReceiveIds($receiveIds, $dataODUpdate);
                $this->updateStatus($type, $receiveIds, $arrCond);
                break;
            }
        }

        // ***************** GetFeedSubmissionResult ****************
        $getFeedSubmissionResultResult = $this->getFeedSubmissionResult($feedSubmissionId, $config, $service);
        if ($getFeedSubmissionResultResult['status'] === false) {
            $dataUpdate['message_api']    = $getFeedSubmissionResultResult['data'];
            $dataUpdate['error_code_api'] = 'Fail';
            $modelO->updateData($receiveIds, $dataUpdate);
            $modelOD->updateDataByReceiveIds($receiveIds, $dataODUpdate);
            $this->postSlackAmazon($getFeedSubmissionResultResult['data']);
            return false;
        }
        $responseXML = $getFeedSubmissionResultResult['data'];
        if (!empty($responseXML->Message->ProcessingReport->Result)) {
            $errAzMess = [];
            foreach ($responseXML->Message->ProcessingReport->Result as $item) {
                if ((string)$item->ResultCode === 'Error') {
                    $receiveId         = (string)$item->MessageID;
                    $resultDescription = (string)$item->ResultDescription;
                    $resultMessageCode = (string)$item->ResultMessageCode;
                    $dataUpdate['message_api']    = $resultDescription;
                    $dataUpdate['error_code_api'] = $resultMessageCode;
                    $modelO->updateData([$receiveId], $dataUpdate);
                    $modelOD->updateDataByReceiveIds([$receiveId], $dataODUpdate);
                    $errAzMess[] = "Receive ID $receiveId (Code $resultMessageCode): $resultDescription";
                }
            }
            if (count($errAzMess) > 0) {
                $this->postSlackAmazon(implode("\n", $errAzMess));
            }
        }
        echo "Process success" . PHP_EOL;
        Log::info("Process success");
        return true;
    }
    
    /**
     * Post message amazon to slack
     *
     * @param  string $message
     * @return void
     */
    public function postSlackAmazon($message)
    {
        $error  = "------------------------------------------" . PHP_EOL;
        $error .= 'Process Toriyose cancel fail:' . PHP_EOL;
        $error .= $message;
        $error .= PHP_EOL . "------------------------------------------" . PHP_EOL;
        $this->slack->notify(new SlackNotification($error));
    }

    /**
     * Submit feed
     *
     * @param  string   $feedType   Feed type
     * @param  string   $feed       Feed data
     * @param  array    $config     Config service
     * @param  object   $service    Service object
     * @return mixed
     */
    private function submitFeed($feedType, $feed, $config, $service)
    {
        $feedHandle = @fopen('php://temp', 'rw+');
        fwrite($feedHandle, $feed);
        rewind($feedHandle);
        $parameters = [
            'Merchant'        => $config['MERCHANT_ID'],
            'FeedType'        => $feedType,
            'FeedContent'     => $feedHandle,
            'PurgeAndReplace' => false,
            'ContentMd5'      => base64_encode(md5(stream_get_contents($feedHandle), true)),
        ];

        rewind($feedHandle);

        $request    = new MarketplaceWebService_Model_SubmitFeedRequest($parameters);
        $returnData = [
            'status' => false,
            'data'   => 'No reason'
        ];
        try {
            $response = $service->submitFeed($request);
            if ($response->isSetSubmitFeedResult()) {
                $submitFeedResult = $response->getSubmitFeedResult();
                if ($submitFeedResult->isSetFeedSubmissionInfo()) {
                    $returnData = [
                        'status' => true,
                        'data'   => $submitFeedResult->getFeedSubmissionInfo()
                    ];
                } else {
                    $returnData = [
                        'status' => false,
                        'data'   => 'No FeedSubmissionInfo'
                    ];
                }
            } else {
                $returnData = [
                    'status' => false,
                    'data'   => 'No SubmitFeedResult'
                ];
            }
        } catch (MarketplaceWebService_Exception $ex) {
            echo("Caught Exception: " . $ex->getMessage() . "\n");
            echo("Response Status Code: " . $ex->getStatusCode() . "\n");
            echo("Error Code: " . $ex->getErrorCode() . "\n");
            echo("Error Type: " . $ex->getErrorType() . "\n");
            echo("Request ID: " . $ex->getRequestId() . "\n");
            echo("XML: " . $ex->getXML() . "\n");
            echo("ResponseHeaderMetadata: " . $ex->getResponseHeaderMetadata() . "\n");

            Log::error("Caught Exception: " . $ex->getMessage());
            Log::error("Response Status Code: " . $ex->getStatusCode());
            Log::error("Error Code: " . $ex->getErrorCode());
            Log::error("Error Type: " . $ex->getErrorType());
            Log::error("Request ID: " . $ex->getRequestId());
            Log::error("XML: " . $ex->getXML());
            Log::error("ResponseHeaderMetadata: " . $ex->getResponseHeaderMetadata());

            $returnData = [
                'status' => false,
                'data'   => !empty($ex->getErrorCode())?$ex->getErrorCode():$ex->getStatusCode()
            ];
        }
        @fclose($feedHandle);
        return $returnData;
    }

    /**
     * Get feed submission list
     *
     * @param   array   $feedSubmissionIdList
     * @param   array   $config
     * @param   object  $service
     * @return  mixed
     */
    private function getFeedSubmissionList($feedSubmissionIdList, $config, $service)
    {
        $parameters = [
            'Merchant'                 => $config['MERCHANT_ID'],
            'FeedSubmissionIdList'     => ['Id' => $feedSubmissionIdList],
            'FeedProcessingStatusList' => ['Status' => ['_SUBMITTED_']]
        ];

        $request = new MarketplaceWebService_Model_GetFeedSubmissionListRequest($parameters);

        $returnData = [
            'status' => false,
            'data'   => 'No reason'
        ];
        try {
            $response = $service->getFeedSubmissionList($request);
            if ($response->isSetGetFeedSubmissionListResult()) {
                $returnData = [
                    'status' => true,
                    'data'   => $response->getGetFeedSubmissionListResult()
                ];
            } else {
                $returnData = [
                    'status' => false,
                    'data'   => 'No GetFeedSubmissionListResult'
                ];
            }
        } catch (MarketplaceWebService_Exception $ex) {
            echo("Caught Exception: " . $ex->getMessage() . "\n");
            echo("Response Status Code: " . $ex->getStatusCode() . "\n");
            echo("Error Code: " . $ex->getErrorCode() . "\n");
            echo("Error Type: " . $ex->getErrorType() . "\n");
            echo("Request ID: " . $ex->getRequestId() . "\n");
            echo("XML: " . $ex->getXML() . "\n");
            echo("ResponseHeaderMetadata: " . $ex->getResponseHeaderMetadata() . "\n");

            Log::error("Caught Exception: " . $ex->getMessage());
            Log::error("Response Status Code: " . $ex->getStatusCode());
            Log::error("Error Code: " . $ex->getErrorCode());
            Log::error("Error Type: " . $ex->getErrorType());
            Log::error("Request ID: " . $ex->getRequestId());
            Log::error("XML: " . $ex->getXML());
            Log::error("ResponseHeaderMetadata: " . $ex->getResponseHeaderMetadata());

            $returnData = [
                'status' => false,
                'data'   => !empty($ex->getErrorCode())?$ex->getErrorCode():$ex->getStatusCode()
            ];
        }
        return $returnData;
    }

    /**
     * Get feed submission result
     *
     * @param  string  $feedSubmissionId
     * @param  array   $config
     * @param  object  $service
     * @return object
     */
    private function getFeedSubmissionResult($feedSubmissionId, $config, $service)
    {
        $parameters = [
            'Merchant'             => $config['MERCHANT_ID'],
            'FeedSubmissionId'     => $feedSubmissionId,
            'FeedSubmissionResult' => @fopen('php://memory', 'rw+')
        ];

        $request    = new MarketplaceWebService_Model_GetFeedSubmissionResultRequest($parameters);
        $fileHandle = fopen('php://memory', 'rw+');
        $request->setFeedSubmissionResult($fileHandle);

        $returnData = [
            'status' => false,
            'data'   => 'No reason'
        ];
        try {
            $response = $service->getFeedSubmissionResult($request);
            rewind($fileHandle);
            $responseStr = stream_get_contents($fileHandle);
            $responseXML = new \SimpleXMLElement($responseStr);
            $returnData = [
                'status' => true,
                'data'   => $responseXML
            ];
        } catch (MarketplaceWebService_Exception $ex) {
            echo("Caught Exception: " . $ex->getMessage() . "\n");
            echo("Response Status Code: " . $ex->getStatusCode() . "\n");
            echo("Error Code: " . $ex->getErrorCode() . "\n");
            echo("Error Type: " . $ex->getErrorType() . "\n");
            echo("Request ID: " . $ex->getRequestId() . "\n");
            echo("XML: " . $ex->getXML() . "\n");
            echo("ResponseHeaderMetadata: " . $ex->getResponseHeaderMetadata() . "\n");

            Log::error("Caught Exception: " . $ex->getMessage());
            Log::error("Response Status Code: " . $ex->getStatusCode());
            Log::error("Error Code: " . $ex->getErrorCode());
            Log::error("Error Type: " . $ex->getErrorType());
            Log::error("Request ID: " . $ex->getRequestId());
            Log::error("XML: " . $ex->getXML());
            Log::error("ResponseHeaderMetadata: " . $ex->getResponseHeaderMetadata());

            $returnData = [
                'status' => false,
                'data'   => !empty($ex->getErrorCode())?$ex->getErrorCode():$ex->getStatusCode()
            ];
        }
        return $returnData;
    }
}
