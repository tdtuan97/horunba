<?php
/**
 * Batch process order customer from malls
 *
 * @package    App\Console\Commands
 * @subpackage ProcessOrderCustomer
 * @copyright  Copyright (c) 2017 RiverCrane! Corporation. All Rights Reserved.
 * @author     Truong Nghia<truong.van.nghia@rivercrane.vn>
 */

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;
use App\Models\Batches\THontenOrder;
use App\Models\Batches\TAmazonOrder;
use App\Models\Batches\TBizOrder;
use App\Models\Batches\TRakutenOrder;
use App\Models\Batches\TYahooOrder;
use App\Models\Batches\THontenOrderApi;
use App\Models\Batches\MstOrder;
use App\Models\Batches\MstOrderDetail;
use App\Models\Batches\Mstmall;
use App\Models\Batches\MstCustomer;
use App\Models\Batches\MstPaymentMethod;
use App\Models\Batches\MstStatusBatch;
use App\Models\Batches\TAmazonOrderApi;
use App\Models\Batches\TAmazonOrderDetail;
use App\Models\Batches\TRakutenPayOrderDetail;
use App\Models\Batches\TWowmaOrder;
use App\Models\Batches\TWowmaOrderDetail;
use App\Notifications\SlackNotification;
use App\Custom\Utilities;
use App\Notification;
use App\Events\Command as eCommand;
use Event;
use DB;

class ProcessOrderCustomer extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'process:order-customer';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Process order customer from malls';

    /**
     * The notification.
     *
     * @var object
     */
    protected $slack;

    /**
     * The limit of step process.
     *
     * @var int
     */
    protected $limit = 10000;

    /**
     * The error.
     *
     * @var array
     */
    public $error = [];

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //Create log
        $arrayReplace = [':', '-'];
        $folder       = str_replace($arrayReplace, '_', $this->signature);
        Log::useDailyFiles(storage_path() . "/logs/$folder/history.log");
        //Check cron
        $modelStatus = new MstStatusBatch;
        $check = $modelStatus->checkProcessOrder();
        $this->slack = new Notification(CHANNEL['horunba']);
        if ($check > 0) {
            Log::info('Have some batch process order running->Stop this batch');
            print_r("Have some batch process order running->Stop this batch" . PHP_EOL);
            $error  = "------------------------------------------" . PHP_EOL;
            $error .= basename(__CLASS__) . PHP_EOL;
            $error .= 'This batch running->Stop this batch' . PHP_EOL;
            $error .= PHP_EOL . "------------------------------------------" . PHP_EOL;
            $this->slack->notify(new SlackNotification($error));
            return;
        }
        //Process data
        Event::fire(new eCommand($this->signature, array('start' => true)));

        $start       = microtime(true);
        Log::info('Start batch get information of order.');
        print_r("Start batch get information of order." . PHP_EOL);
        Log::info('======Start process mall Yahoo.======');
        print_r("======Start process mall Yahoo.======" . PHP_EOL);
        $this->processMallYahoo();
        Log::info('======End process mall Yahoo.======');
        print_r("======End process mall Yahoo.======" . PHP_EOL);
        Log::info('======Start process mall Honten API.======');
        print_r("======Start process mall Honten API.======" . PHP_EOL);
        $this->processMallHontenAPI();
        Log::info('======End process mall Honten API.======');
        print_r("======End process mall Honten API.======" . PHP_EOL);
        Log::info('======Start process mall Amazon.======');
        print_r("======Start process mall Amazon.======" . PHP_EOL);
        $this->processMallAmazon();
        Log::info('======End process mall Amazon.======');
        print_r("======End process mall Amazon.======" . PHP_EOL);
        Log::info('======Start process mall Amazon API.======');
        print_r("======Start process mall Amazon API.======" . PHP_EOL);
        $this->processMallAmazonAPI();
        Log::info('======End process mall Amazon API.======');
        print_r("======End process mall Amazon API.======" . PHP_EOL);
        Log::info('======Start process mall Rakuten Pay.======');
        print_r("======Start process mall Rakuten Pay.======" . PHP_EOL);
        $this->processMallRakutenPay();
        Log::info('======End process mall Rakuten Pay.======');
        print_r("======End process mall Rakuten Pay.======" . PHP_EOL);
        Log::info('======Start process mall Wowma.======');
        print_r("======Start process mall Wowma.======" . PHP_EOL);
        $this->processMallWowma();
        Log::info('======End process mall Wowma.======');
        print_r("======End process mall Wowma.======" . PHP_EOL);
        Event::fire(new eCommand($this->signature, array('end' => true, 'error' => $this->error)));
        $totalTime = round(microtime(true) - $start, 2);
        Log::info("End batch get information of order with total time: $totalTime.");
        print_r("End batch get information of order with total time: $totalTime s." . PHP_EOL);
    }

    /**
     * Get information order from  mall yahoo.
     * @return void
     */
    public function processMallYahoo()
    {
        $modelYahoo       = new TYahooOrder;
        $modelCustomer    = new MstCustomer;
        $modelOrder       = new MstOrder;
        $modelOrderDetail = new MstOrderDetail;
        $modelMall        = new Mstmall;
        $modelMPM = new MstPaymentMethod();
        $dataCheck = $modelYahoo->getDataCheck($this->limit);
        if (count($dataCheck) == 0) {
            Log::info('Mall Yahoo no data');
            print_r("Mall Yahoo no data. " . PHP_EOL);
            return;
        }
        $arrId         = [];
        $arrYahooCheck = [2,3,5,9];

        foreach ($dataCheck as $value) {
            if ($value->order_status === '4') {
                $modelYahoo->where('yahoo_order_list_key', $value->yahoo_order_list_key)
                    ->update([
                        'is_delete'   => 1,
                        'process_flg' => 1,
                    ]);
                continue;
            }
            if (!in_array($value->settlestatus, $arrYahooCheck)) {
                $modelYahoo->where('yahoo_order_list_key', $value->yahoo_order_list_key)
                    ->update([
                        'is_delete'   => 1,
                        'process_flg' => 1,
                    ]);
                continue;
            }
            if (!in_array($value->order_id, $arrId)) {
                $arrId[] = $value->order_id;
            }
            $modelYahoo->where('yahoo_order_list_key', '<>', $value->yahoo_order_list_key)
                    ->where('order_id', '=', $value->order_id)
                    ->where('item_id', '=', $value->item_id)
                    ->update([
                        'is_delete'   => 1,
                        'process_flg' => 1,
                    ]);
        }
        $dataOrderCheck   = $modelOrder->getDataCheckOrder($arrId);
        if (count($dataOrderCheck) !== 0) {
            $dataOrderCheck = array_column($dataOrderCheck->toArray(), 'received_order_id');
        } else {
            $dataOrderCheck = [];
        }
        $datas      = $modelYahoo->getData($arrId);
        $dataPrice  = $modelYahoo->calculatePrice();
        $arrOrders  = [];
        $mall       = $modelMall->getInfoMallByName('yahoo');
        $arrPayment = $modelMPM->getDataByMall($mall->id);
        $succCus            = 0;
        $failCus            = 0;
        $orderSuccess       = 0;
        $orderFail          = 0;
        $orderDetailSuccess = 0;
        $orderDetailFail    = 0;
        $countDetail        = 0;
        $updatFlag          = 0;
        foreach ($datas as $value) {
            $arrCustomer = [];
            $orderId     = $value->order_id;
            if (in_array($orderId, $arrOrders) || in_array($orderId, $dataOrderCheck)) {
                $modelYahoo->where('order_id', '=', $orderId)
                    ->update([
                        'is_delete'   => 1,
                        'process_flg' => 1,
                    ]);
                continue;
            }
            $countDetail = 0;
            $arrOrders[] = $orderId;
            try {
                DB::beginTransaction();
                $orderSubAddress = $value->bill_address1 . $value->bill_address2;
                $orderDate       = !empty($value->order_time) && $this->isDate($value->order_time) ?
                                (new \DateTime($value->order_time))->format('Y-m-d H:i:s') : now();
                // Get info customer
                $arrCustomer['first_name']      = (string)$value->bill_firstname;
                $arrCustomer['last_name']       = (string)$value->bill_lastname;
                $arrCustomer['first_name_kana'] = (string)$value->bill_firstname_kana;
                $arrCustomer['last_name_kana']  = (string)$value->bill_lastname_kana;
                $arrCustomer['email']           = (string)$value->bill_mail_address;
                $arrCustomer['tel_num']         = $this->removeHyphen($value->bill_phone_number);
                $arrCustomer['fax_num']         = '';
                $arrCustomer['zip_code']        = $this->removeHyphen($value->bill_zipcode);
                $arrCustomer['prefecture']      = (string)$value->bill_prefecture;
                $arrCustomer['city']            = str_replace($value->bill_prefecture, '', $value->bill_city);
                $arrCustomer['sub_address']     = (string)$orderSubAddress;
                $arrCustomer['sex']             = '';
                $arrCustomer['urgent_tel_num']  = (string)$value->bill_emg_phone_number;
                $arrCustomer['is_black_list']   = 0;
                $arrCustomer['in_ope_cd']       = 'OPE99999';
                $arrCustomer['in_date']         = now();
                $arrCustomer['up_ope_cd']       = 'OPE99999';
                $arrCustomer['up_date']         = now();
                $checkCustomer = $modelCustomer->checkCustomerExist($arrCustomer, true);
                $customerId    = 0;
                if (count($checkCustomer) === 0) {
                    $customerId = $modelCustomer->insertGetId($arrCustomer);
                    $succCus++;
                } else {
                    $customerId = $checkCustomer->customer_id;
                }
                // Get order info mall Yahoo
                $paymentMethod = 0;
                foreach ($arrPayment as $pay) {
                    if ($value->paymethodname === $pay->payment_name) {
                        $paymentMethod = $pay->payment_code;
                        break;
                    }
                }
                $totalPrice = $value->total_price +
                              $value->discount +
                              $value->use_point +
                              $value->totalmallcoupondiscount;
                $shipWishTime = '';
                switch ($value->ship_request_time) {
                    case '09:00-12:00':
                        $shipWishTime = '01';
                        break;
                    case '18:00-21:00':
                        $shipWishTime = '04';
                        break;
                    case '12:00-14:00':
                        $shipWishTime = '12';
                        break;
                    case '14:00-16:00':
                        $shipWishTime = '14';
                        break;
                    case '16:00-18:00':
                        $shipWishTime = '16';
                        break;
                    case '18:00-20:00':
                        $shipWishTime = '18';
                        break;
                    case '19:00-21:00':
                        $shipWishTime = '19';
                        break;

                    default:
                        # code...
                        break;
                }
                $arrDataOrders = [];
                $arrDataOrders['received_order_id']     = (string)$orderId;
                $arrDataOrders['mall_id']               = (int)$mall->id;
                $arrDataOrders['customer_id']           = $customerId;
                $arrDataOrders['used_coupon']           = (int)$value->totalmallcoupondiscount;
                $arrDataOrders['used_point']            = (int)$value->use_point;
                $arrDataOrders['total_price']           = $totalPrice;
                $arrDataOrders['request_price']         = (int)$value->settleamount;
                $arrDataOrders['order_date']            = $orderDate;
                $arrDataOrders['ship_charge']           = (int)$value->ship_charge;
                $arrDataOrders['pay_charge']            = (int)$paymentMethod === 4 ? (int)$value->pay_charge : 0;
                $arrDataOrders['goods_price']           = $dataPrice[$orderId]['goods_price'];
                $arrDataOrders['goods_tax']             = 0;
                $arrDataOrders['customer_question']     = (string)$value->ship_notes;
                $arrDataOrders['discount']              = (int)$value->discount;
                $arrDataOrders['arrive_type']           = (int)$value->arrive_type;
                $arrDataOrders['is_multi_ship_address'] = 0;
                $arrDataOrders['ship_wish_time']        = $shipWishTime;
                $arrDataOrders['ship_wish_date']        = (string)$value->ship_request_date;
                $arrDataOrders['payment_status']        = (int)$value->pay_status;
                $arrDataOrders['payment_method']        = (int)$paymentMethod;
                $arrDataOrders['mail_seri']             = (string)$value->mail_serial;
                $arrDataOrders['order_status']          = ORDER_STATUS['NEW'];
                $arrDataOrders['payment_account']       = '';
                $arrDataOrders['shop_answer']           = '';
                $arrDataOrders['company_name']          = '';
                $arrDataOrders['cancel_reason']         = '';
                $arrDataOrders['pay_after_charge']      = (int)$paymentMethod === 3 ? (int)$value->pay_charge : 0;
                $arrDataOrders['pay_charge_discount']   = 0;
                $arrDataOrders['payment_date']          = null;
                $arrDataOrders['payment_confirm_date']  = null;
                $arrDataOrders['in_ope_cd']             = 'OPE99999';
                $arrDataOrders['in_date']               = now();
                $arrDataOrders['up_ope_cd']             = 'OPE99999';
                $arrDataOrders['up_date']               = now();
                //Check payment_method is 全額ポイント
                if ($arrDataOrders['request_price'] <= 0 &&
                    $arrDataOrders['used_point'] === $arrDataOrders['total_price']) {
                    $arrDataOrders['payment_method'] = 10;
                }
                //If payment_method = 0 , we will notify to slack
                if ($arrDataOrders['payment_method'] === 0) {
                    $error  = "---------------------------" . PHP_EOL;
                    $error .= basename(__CLASS__) . PHP_EOL;
                    $error .= "Payment Method is error"  . PHP_EOL;
                    $error .= "Order Id : {$arrDataOrders['received_order_id']}"  . PHP_EOL;
                    $error .= "Mall Id : {$arrDataOrders['mall_id']}"  . PHP_EOL;
                    $error .= "---------------------------" . PHP_EOL;
                    $this->slack->notify(new SlackNotification($error));
                }
                $receiveId      = $modelOrder->insertGetId($arrDataOrders);
                $orderSuccess++;
                $arrDataDetail  = $modelYahoo->getInfoOrderDetail($receiveId);
                $detailLineNum  = 0;
                $arrOrderDetail = [];
                foreach ($arrDataDetail as $value) {
                    $arrShipCus = [];
                    $detailLineNum++;
                    $shipSubAddress = $value->ship_address1 . $value->ship_address2;
                    $id             = $value->yahoo_order_list_key;
                    if (empty($value->ship_request_date)) {
                        $shipDate = null;
                    } else {
                        $shipDate = $value->ship_request_date . ' ' . $value->ship_request_time;
                    }
                    // Get data ship address
                    if (mb_strlen($value->ship_phone_number) !== strlen($value->ship_phone_number)) {
                        $value->ship_phone_number = mb_convert_kana($value->ship_phone_number, 'KVa');
                    }
                    $arrShipCus['first_name']      = (string)$value->ship_first_name;
                    $arrShipCus['last_name']       = (string)$value->ship_last_name;
                    $arrShipCus['first_name_kana'] = (string)$value->ship_firstname_kana;
                    $arrShipCus['last_name_kana']  = (string)$value->ship_lastname_kana;
                    $arrShipCus['email']           = '';
                    $arrShipCus['tel_num']         = $this->removeHyphen($value->ship_phone_number);
                    $arrShipCus['fax_num']         = '';
                    $arrShipCus['zip_code']        = $this->removeHyphen($value->ship_zip_code);
                    $arrShipCus['prefecture']      = $this->removeSpaces((string)$value->ship_prefecture);
                    $arrShipCus['city']            = $this->removeSpaces(str_replace($value->ship_prefecture, '', $value->ship_city));
                    $arrShipCus['sub_address']     = $this->removeSpaces((string)$shipSubAddress);
                    $arrShipCus['urgent_tel_num']  = (string)$value->ship_emg_phone_number;
                    $arrShipCus['sex']             = '';
                    $arrShipCus['in_ope_cd']       = 'OPE99999';
                    $arrShipCus['in_date']         = now();
                    $arrShipCus['up_ope_cd']       = 'OPE99999';
                    $arrShipCus['up_date']         = now();
                    $checkCustomer = $modelCustomer->checkCustomerExist($arrShipCus);
                    $customerId = 0;
                    if (count($checkCustomer) === 0) {
                            $customerId = $modelCustomer->insertGetId($arrShipCus);
                            $succCus++;
                    } else {
                        $customerId = $checkCustomer->customer_id;
                    }
                    $arrOrderDetail[$id]['receive_id']          = (int)$value->receive_id;
                    $arrOrderDetail[$id]['detail_line_num']     = (int)$detailLineNum;
                    $arrOrderDetail[$id]['product_code']        = (string)$value->item_id;
                    $arrOrderDetail[$id]['product_name']        = (string)$value->title;
                    $arrOrderDetail[$id]['price']               = (int)$value->unit_price;
                    $arrOrderDetail[$id]['item_tax']            = 0;
                    $arrOrderDetail[$id]['ship_price']          = 0;
                    $arrOrderDetail[$id]['ship_tax']            = 0;
                    $arrOrderDetail[$id]['quantity']            = (int)$value->quantity;
                    $arrOrderDetail[$id]['receiver_id']         = $customerId;
                    $arrOrderDetail[$id]['ship_date']           = $this->isDate($shipDate) ?
                                                                    date('Y-m-d', strtotime($shipDate)) : null;
                    $arrOrderDetail[$id]['delivery_person_id']  = '';
                    $arrOrderDetail[$id]['package_ship_number'] = '';
                    $arrOrderDetail[$id]['in_ope_cd']           = 'OPE99999';
                    $arrOrderDetail[$id]['in_date']             = now();
                    $arrOrderDetail[$id]['up_ope_cd']           = 'OPE99999';
                    $arrOrderDetail[$id]['up_date']             = now();
                    $arrOrderDetail[$id]['ship_charge_detail']  = (int)$value->ship_charge;
                    $arrOrderDetail[$id]['goods_price_detail']  = (int)$value->goods_price;
                    $arrOrderDetail[$id]['point_rate']          = (int)$value->pointratioseller;
                    $arrOrderDetail[$id]['got_point']           = (int)$value->unitgetpoint;
                }
                $countDetail        = count($arrOrderDetail);
                $modelOrderDetail->insert($arrOrderDetail);
                $orderDetailSuccess += $countDetail;
                $keyIdUpdate        = array_keys($arrOrderDetail);
                $checkPF            = $modelYahoo->updateProcessFlg($keyIdUpdate);
                if ($checkPF) {
                    $updatFlag += count($keyIdUpdate);
                }
                DB::commit();
            } catch (\Exception $e) {
                $this->error[] = Utilities::checkMessageException($e);
                $error  = "------------------------------------------" . PHP_EOL;
                $error .= basename(__CLASS__) . PHP_EOL;
                $error .= Utilities::checkMessageException($e);
                $error .= PHP_EOL . "------------------------------------------" . PHP_EOL;
                $this->slack->notify(new SlackNotification($error));
                Log::error(Utilities::checkMessageException($e));
                print_r("$error");
                $failCus++;
                $orderFail++;
                $orderDetailFail += $countDetail;
                DB::rollback();
                continue;
            }
        }
        $messageC  = "Insert to table mst_customer success: $succCus records";
        $messageC .= "and fail : $failCus records";
        Log::info($messageC);
        print_r($messageC . PHP_EOL);
        $messageO  = "Insert to table mst_order success: $orderSuccess records";
        $messageO .= " and fail : $orderFail records";
        Log::info($messageO);
        print_r($messageO . PHP_EOL);
        $messageOD  = "Insert to mst_order_detail success: $orderDetailSuccess records";
        $messageOD .= " and fail : $orderDetailFail records";
        Log::info($messageOD);
        print_r($messageOD . PHP_EOL);
        Log::info("Update Process flag success : $updatFlag records");
        print_r("Update Process flag success : $updatFlag records" . PHP_EOL);
    }

    /**
     * Get information order from  mall honten API.
     * @return void
     */
    public function processMallHontenAPI()
    {
        $modelHontenApi   = new THontenOrderApi;
        $modelCustomer    = new MstCustomer;
        $modelOrder       = new MstOrder;
        $modelOrderDetail = new MstOrderDetail;
        $modelMPM         = new MstPaymentMethod();
        $dataCheck = $modelHontenApi->getDataCheck($this->limit);
        if (count($dataCheck) == 0) {
            Log::info('Mall Honten no Api data');
            print_r("Mall Honten no Api data. " . PHP_EOL);
            return;
        }
        $arrId = [];
        foreach ($dataCheck as $value) {
            if (!in_array($value->order_id, $arrId)) {
                $arrId[] = $value->order_id;
            }
            $modelHontenApi->where('id', '<>', $value->id)
                    ->where('order_id', '=', $value->order_id)
                        ->where('item_number', '=', $value->item_number)
                    ->update([
                        'is_delete'   => 1,
                        'process_flg' => 1,
                    ]);
        }
        $dataOrderCheck   = $modelOrder->getDataCheckOrder($arrId);
        if (count($dataOrderCheck) !== 0) {
            $dataOrderCheck = array_column($dataOrderCheck->toArray(), 'received_order_id');
        } else {
            $dataOrderCheck = [];
        }
        $datas     = $modelHontenApi->getData($arrId);
        $arrOrders = [];
        $arrPayment = $modelMPM->getDataByMall(8);
        $succCus            = 0;
        $failCus            = 0;
        $orderSuccess       = 0;
        $orderFail          = 0;
        $orderDetailSuccess = 0;
        $orderDetailFail    = 0;
        $countDetail        = 0;
        $updatFlag          = 0;
        foreach ($datas as $value) {
            $arrCustomer = [];
            $orderId     = $value->order_id;
            if (in_array($orderId, $arrOrders)  || in_array($orderId, $dataOrderCheck)) {
                $modelHontenApi->where('order_id', '=', $orderId)
                    ->update([
                        'is_delete'   => 1,
                        'process_flg' => 1,
                    ]);
                continue;
            }
            $countDetail = 0;
            $arrOrders[] = $orderId;
            try {
                DB::beginTransaction();
                $orderZipCode = $value->bill_zip_code1 . $value->bill_zip_code2;
                $orderTel     = $value->bill_phone_number1.
                                $value->bill_phone_number2.
                                $value->bill_phone_number3;
                $orderDate    = !empty($value->orderdatetime) ? $value->orderdatetime : now();
                // Get info customer
                $arrCustomer['first_name']      = (string)$value->bill_first_name;
                $arrCustomer['last_name']       = (string)$value->bill_family_name;
                $arrCustomer['first_name_kana'] = (string)$value->bill_first_name_kana;
                $arrCustomer['last_name_kana']  = (string)$value->bill_family_name_kana;
                $arrCustomer['email']           = (string)$value->bill_email_address;
                $arrCustomer['tel_num']         = str_replace('-', '', $orderTel);
                $arrCustomer['fax_num']         = '';
                $arrCustomer['zip_code']        = str_replace('-', '', $orderZipCode);
                $arrCustomer['prefecture']      = (string)$value->bill_prefecture;
                $arrCustomer['city']            = str_replace($value->bill_prefecture, '', $value->bill_city);
                $arrCustomer['sub_address']     = (string)$value->bill_subaddress;
                $arrCustomer['sex']             = (string)$value->bill_sex;
                $arrCustomer['is_black_list']   = 0;
                $arrCustomer['in_ope_cd']       = 'OPE99999';
                $arrCustomer['in_date']         = now();
                $arrCustomer['up_ope_cd']       = 'OPE99999';
                $arrCustomer['up_date']         = now();
                $checkCustomer = $modelCustomer->checkCustomerExist($arrCustomer, true);
                $customerId    = 0;
                if (count($checkCustomer) === 0) {
                    $customerId = $modelCustomer->insertGetId($arrCustomer);
                    $succCus++;
                } else {
                    $customerId = $checkCustomer->customer_id;
                }
                // Get order info mall honten Api
                $paymentMethod = 0;
                foreach ($arrPayment as $pay) {
                    if ($value->settlement === $pay->payment_name) {
                        $paymentMethod = $pay->payment_code;
                        break;
                    }
                }
                //Check payment_method is 全額ポイント
                if ($value->requestprice === 0 && $value->total_price === $value->used_point) {
                    $paymentMethod = 10;
                }
                $shipWishTime = '';
                switch ($value->wishdeliverytime) {
                    case '午前中':
                        $shipWishTime = '01';
                        break;
                    case '12:00～14:00':
                        $shipWishTime = '12';
                        break;
                    case '14:00～16:00':
                        $shipWishTime = '14';
                        break;
                    case '16:00～18:00':
                        $shipWishTime = '16';
                        break;
                    case '18:00～20:00':
                        $shipWishTime = '18';
                        break;
                    case '19:00～21:00':
                        $shipWishTime = '19';
                        break;
                    default:
                        $shipWishTime = $value->wishdeliverytime;
                        break;
                }
                $arrDataOrders = [];
                $arrDataOrders['received_order_id']     = (string)$orderId;
                $arrDataOrders['mall_id']               = 8;
                $arrDataOrders['customer_id']           = $customerId;
                $arrDataOrders['used_coupon']           = abs($value->coupon);
                $arrDataOrders['used_point']            = abs($value->used_point);
                $arrDataOrders['total_price']           = (int)$value->total_price;
                $arrDataOrders['request_price']         = (int)$value->requestprice;
                $arrDataOrders['order_date']            = $this->isDate($orderDate) ?
                                                            date('Y-m-d H:i:s', strtotime($orderDate)) : now();
                $arrDataOrders['ship_charge']           = (int)$value->postageprice;
                $arrDataOrders['pay_charge']            = 0;
                $arrDataOrders['goods_price']           = $value->goods_price;
                $arrDataOrders['goods_tax']             = (int)$value->goods_tax;
                $arrDataOrders['discount']              = (int)$value->discount_totalpice;
                $arrDataOrders['arrive_type']           = 0;
                $arrDataOrders['is_multi_ship_address'] = 0;
                $arrDataOrders['ship_wish_time']        = $shipWishTime;
                $arrDataOrders['ship_wish_date']        = $value->wishdeliverydate;
                $arrDataOrders['payment_status']        = $value->settlement === 'クレジットカード' ? 1 : 0;
                $arrDataOrders['payment_method']        = (int)$paymentMethod;
                $arrDataOrders['mail_seri']             = MD5($orderId);
                $arrDataOrders['order_status']          = ORDER_STATUS['NEW'];
                $arrDataOrders['payment_price']         = 0;
                $arrDataOrders['payment_account']       = '';
                $arrDataOrders['customer_question']     = 0;
                $arrDataOrders['shop_answer']           = '';
                $arrDataOrders['company_name']          = $value->company;
                $arrDataOrders['cancel_reason']         = '';
                $arrDataOrders['pay_after_charge']      = (int)$value->settlementfee;
                $arrDataOrders['is_sourcing_on_demand'] = 0;
                $arrDataOrders['pay_charge_discount']   = (int)$value->discount_totalShipping;
                $arrDataOrders['payment_date']          = null;
                $arrDataOrders['payment_confirm_date']  = null;
                $arrDataOrders['paygent_id']            = $value->paygent_id;
                $arrDataOrders['in_ope_cd']             = 'OPE99999';
                $arrDataOrders['in_date']               = now();
                $arrDataOrders['up_ope_cd']             = 'OPE99999';
                $arrDataOrders['up_date']               = now();
                //If payment_method = 0 , we will notify to slack
                if ($arrDataOrders['payment_method'] === 0) {
                    $error  = "---------------------------" . PHP_EOL;
                    $error .= basename(__CLASS__) . PHP_EOL;
                    $error .= "Payment Method is error"  . PHP_EOL;
                    $error .= "Order Id : {$arrDataOrders['received_order_id']}"  . PHP_EOL;
                    $error .= "Mall Id : {$arrDataOrders['mall_id']}"  . PHP_EOL;
                    $error .= "---------------------------" . PHP_EOL;
                    $this->slack->notify(new SlackNotification($error));
                }
                $receiveId      = $modelOrder->insertGetId($arrDataOrders);
                $orderSuccess++;
                $arrDataDetail  = $modelHontenApi->getInfoOrderDetail($receiveId);
                $detailLineNum  = 0;
                $arrOrderDetail = [];
                foreach ($arrDataDetail as $value) {
                    $arrShipCus = [];
                    $detailLineNum++;
                    $shipZipCode = $value->sender_zip_code1 . $value->sender_zip_code2;
                    $shipTelNum  = $value->sender_phone_number1.
                                   $value->sender_phone_number2.
                                   $value->sender_phone_number3;
                    $id          = $value->id;
                    $shipDate    = $this->isDate($value->shipment_date) ?
                                    date('Y-m-d', strtotime($value->shipment_date)) : null;
                    // Get data ship address
                    $arrShipCus['first_name']      = (string)$value->sender_first_name;
                    $arrShipCus['last_name']       = (string)$value->sender_family_name;
                    $arrShipCus['first_name_kana'] = (string)$value->senderfirstnamekana;
                    $arrShipCus['last_name_kana']  = (string)$value->senderfamilynamekana;
                    $arrShipCus['email']           = '';
                    $arrShipCus['tel_num']         = $shipTelNum;
                    $arrShipCus['fax_num']         = '';
                    $arrShipCus['zip_code']        = str_replace('-', '', $shipZipCode);
                    $arrShipCus['prefecture']      = $this->removeSpaces((string)$value->sender_prefecture);
                    $arrShipCus['city']            = $this->removeSpaces(str_replace($value->sender_prefecture, '', $value->sender_city));
                    $arrShipCus['sub_address']     = $this->removeSpaces((string)$value->sender_subaddress);
                    $arrShipCus['sex']             = '';
                    $arrShipCus['in_ope_cd']       = 'OPE99999';
                    $arrShipCus['in_date']         = now();
                    $arrShipCus['up_ope_cd']       = 'OPE99999';
                    $arrShipCus['up_date']         = now();
                    $checkCustomer = $modelCustomer->checkCustomerExist($arrShipCus);
                    $customerId = 0;
                    if (count($checkCustomer) === 0) {
                        $customerId = $modelCustomer->insertGetId($arrShipCus);
                        $succCus++;
                    } else {
                        $customerId = $checkCustomer->customer_id;
                    }
                    $arrOrderDetail[$id]['receive_id']          = (int)$value->receive_id;
                    $arrOrderDetail[$id]['detail_line_num']     = (int)$detailLineNum;
                    $arrOrderDetail[$id]['product_code']        = (string)$value->item_number;
                    $arrOrderDetail[$id]['product_name']        = (string)$value->item_name;
                    $arrOrderDetail[$id]['price']               = $value->item_price;
                    $arrOrderDetail[$id]['item_tax']            = 0;
                    $arrOrderDetail[$id]['ship_price']          = 0;
                    $arrOrderDetail[$id]['ship_tax']            = 0;
                    $arrOrderDetail[$id]['quantity']            = (int)$value->item_units;
                    $arrOrderDetail[$id]['receiver_id']         = $customerId;
                    $arrOrderDetail[$id]['ship_date']           = null;
                    $arrOrderDetail[$id]['delivery_person_id']  = '';
                    $arrOrderDetail[$id]['package_ship_number'] = '';
                    $arrOrderDetail[$id]['in_ope_cd']           = 'OPE99999';
                    $arrOrderDetail[$id]['in_date']             = now();
                    $arrOrderDetail[$id]['up_ope_cd']           = 'OPE99999';
                    $arrOrderDetail[$id]['up_date']             = now();
                    $arrOrderDetail[$id]['ship_charge_detail']  = (int)$value->postageprice;
                    $arrOrderDetail[$id]['goods_price_detail']  = $value->goods_price;
                }
                $countDetail = count($arrOrderDetail);
                $modelOrderDetail->insert($arrOrderDetail);
                $orderDetailSuccess += $countDetail;
                $keyIdUpdate = array_keys($arrOrderDetail);
                $checkPF     = $modelHontenApi->updateProcessFlg($keyIdUpdate);
                if ($checkPF) {
                    $updatFlag += count($keyIdUpdate);
                }
                DB::commit();
            } catch (\Exception $e) {
                $this->error[] = Utilities::checkMessageException($e);
                $error  = "------------------------------------------" . PHP_EOL;
                $error .= basename(__CLASS__) . PHP_EOL;
                $error .= Utilities::checkMessageException($e);
                $error .= PHP_EOL . "------------------------------------------" . PHP_EOL;
                $this->slack->notify(new SlackNotification($error));
                Log::error(Utilities::checkMessageException($e));
                print_r("$error");
                $failCus++;
                $orderFail++;
                $orderDetailFail += $countDetail;
                DB::rollback();
                continue;
            }
        }
        $messageC  = "Insert to table mst_customer success: $succCus records";
        $messageC .= "and fail : $failCus records";
        Log::info($messageC);
        print_r($messageC . PHP_EOL);
        $messageO  = "Insert to table mst_order success: $orderSuccess records";
        $messageO .= " and fail : $orderFail records";
        Log::info($messageO);
        print_r($messageO . PHP_EOL);
        $messageOD  = "Insert to mst_order_detail success: $orderDetailSuccess records";
        $messageOD .= " and fail : $orderDetailFail records";
        Log::info($messageOD);
        print_r($messageOD . PHP_EOL);
        Log::info("Update Process flag success : $updatFlag records");
        print_r("Update Process flag success : $updatFlag records" . PHP_EOL);
    }

    public function processMallAmazonAPI()
    {
        $modelAOA         = new TAmazonOrderApi();
        $modelAOD         = new TAmazonOrderDetail();
        $modelOrder       = new MstOrder;
        $modelOrderDetail = new MstOrderDetail;
        $modelCustomer    = new MstCustomer;
        $datas = $modelAOA->getData();
        $total = $datas->count();
        if ($total === 0) {
            $message = 'Mall amazon api no data.';
            Log::info($message);
            print_r($message . PHP_EOL);
        } else {
            $arrId = array_column($datas->toArray(), 'amazon_order_id');
            $dataOrderCheck   = $modelOrder->getDataCheckOrder($arrId);
            if (count($dataOrderCheck) !== 0) {
                $dataOrderCheck = array_column($dataOrderCheck->toArray(), 'received_order_id');
            } else {
                $dataOrderCheck = [];
            }
            $arrOrders        = [];
            $success = 0;
            $fail = 0;
            foreach ($datas as $data) {
                $orderId     = $data->amazon_order_id;
                if (in_array($orderId, $arrOrders) || in_array($orderId, $dataOrderCheck)) {
                    $modelAOA->where('amazon_order_id', $orderId)
                        ->update([
                            'is_deleted'   => 1,
                            'process_flg' => 1,
                        ]);
                    continue;
                }
                $arrOrders[] = $orderId;
                $arrCustomer = [];
                try {
                    DB::beginTransaction();
                    $arrCustomer['first_name']      = '';
                    $arrCustomer['last_name']       = (string)$data->buyer_name;
                    $arrCustomer['first_name_kana'] = '';
                    $arrCustomer['last_name_kana']  = '';
                    $arrCustomer['email']           = (string)$data->buyer_email;
                    $arrCustomer['tel_num']         = '0000000000';
                    $arrCustomer['fax_num']         = '';
                    $arrCustomer['zip_code']        = '0000000';
                    $arrCustomer['prefecture']      = '';
                    $arrCustomer['city']            = '';
                    $arrCustomer['sub_address']     = '';
                    $arrCustomer['sex']             = '';
                    $arrCustomer['is_black_list']   = 0;
                    $arrCustomer['in_ope_cd']       = 'OPE99999';
                    $arrCustomer['in_date']         = now();
                    $arrCustomer['up_ope_cd']       = 'OPE99999';
                    $arrCustomer['up_date']         = now();
                    $checkCustomer = $modelCustomer->checkCustomerExist($arrCustomer, true);
                    $customerId    = 0;
                    if (count($checkCustomer) === 0) {
                        $customerId = $modelCustomer->insertGetId($arrCustomer);
                    } else {
                        $customerId = $checkCustomer->customer_id;
                    }
                    $orderDate = !empty($data->purchase_date) && $this->isDate($data->purchase_date) ?
                                (new \DateTime($data->purchase_date))->format('Y-m-d H:i:s') : now();
                    if ($data->payment_method_detail === 'Invoice') {
                        $paymentStatus = 1;
                        $paymentMethod = 5;
                    } elseif ($data->payment_method_detail === 'Standard') {
                        $paymentStatus = 1;
                        $paymentMethod = 1;
                    } elseif ($data->payment_method_detail === 'CashOnDelivery') {
                        $paymentStatus = 0;
                        $paymentMethod = 3;
                    } else {
                        $paymentStatus = 0;
                        $paymentMethod = 1;
                    }

                    $requestPrice = $data->order_total_amount;
                    if ($paymentMethod === 3) {
                        $requestPrice = $data->payment_COD;
                    }
                    $isSourcing = 0;
                    $responDue  = null;
                    if ($data->order_type === 'SourcingOnDemandOrder') {
                        $isSourcing = 1;
                        $responDue  = $data->promise_response_due_date;
                    }
                    $arrDataOrders = [];
                    $arrDataOrders['received_order_id']     = (string)$orderId;
                    $arrDataOrders['mall_id']               = 3;
                    $arrDataOrders['customer_id']           = $customerId;
                    $arrDataOrders['used_coupon']           = (int)$data->payment_GC;
                    $arrDataOrders['used_point']            = (int)$data->payment_CP;
                    $arrDataOrders['discount']              = 0;
                    $arrDataOrders['total_price']           = (int)$data->order_total_amount;
                    $arrDataOrders['request_price']         = $requestPrice;
                    $arrDataOrders['order_date']            = $orderDate;
                    $arrDataOrders['ship_charge']           = (int)$data->ship_charge;
                    $arrDataOrders['pay_charge']            = 0;
                    $arrDataOrders['goods_price']           = (int)$data->goods_price;
                    $arrDataOrders['goods_tax']             = (int)$data->goods_tax;
                    $arrDataOrders['customer_question']     = '';
                    $arrDataOrders['arrive_type']           = $data->is_prime === 'true' ? 1 : 0;
                    $arrDataOrders['is_multi_ship_address'] = 0;
                    $arrDataOrders['ship_wish_time']        = '';
                    $arrDataOrders['ship_wish_date']        = '';
                    $arrDataOrders['payment_status']        = $paymentStatus;
                    $arrDataOrders['payment_method']        = $paymentMethod;
                    $arrDataOrders['mail_seri']             = MD5($orderId);
                    $arrDataOrders['order_status']          = ORDER_STATUS['NEW'];
                    $arrDataOrders['payment_account']       = '';
                    $arrDataOrders['shop_answer']           = '';
                    $arrDataOrders['company_name']          = '';
                    $arrDataOrders['cancel_reason']         = '';
                    $arrDataOrders['pay_after_charge']      = (int)$data->COD_fee_amount;
                    $arrDataOrders['pay_charge_discount']   = (int)$data->COD_fee_discount_amount;
                    $arrDataOrders['payment_date']          = null;
                    $arrDataOrders['payment_confirm_date']  = null;
                    $arrDataOrders['is_sourcing_on_demand'] = $isSourcing;
                    $arrDataOrders['response_due_date']     = $responDue;
                    $arrDataOrders['in_ope_cd']             = 'OPE99999';
                    $arrDataOrders['in_date']               = now();
                    $arrDataOrders['up_ope_cd']             = 'OPE99999';
                    $arrDataOrders['up_date']               = now();
                    $receiveId = $modelOrder->insertGetId($arrDataOrders);
                    $arrShipCus = [];
                    $arrShipCus['first_name']      = '';
                    $arrShipCus['last_name']       = (string)$data->ship_name;
                    $arrShipCus['first_name_kana'] = '';
                    $arrShipCus['last_name_kana']  = '';
                    $arrShipCus['email']           = '';
                    $arrShipCus['tel_num']         = $this->removeHyphen($data->ship_phone);
                    $arrShipCus['fax_num']         = '';
                    $arrShipCus['zip_code']        = $this->removeHyphen($data->ship_postal_code);
                    $arrShipCus['prefecture']      = $this->removeSpaces((string)$data->ship_state_or_region);
                    $arrShipCus['city']            =
                        $this->removeSpaces(str_replace($data->ship_state_or_region, '', $data->ship_address_line1));
                    $arrShipCus['sub_address']     =
                        $this->removeSpaces((string)$data->ship_address_line2 . (string)$data->ship_address_line3);
                    $arrShipCus['sex']             = '';
                    $arrShipCus['in_ope_cd']       = 'OPE99999';
                    $arrShipCus['in_date']         = now();
                    $arrShipCus['up_ope_cd']       = 'OPE99999';
                    $arrShipCus['up_date']         = now();
                    $checkCustomer = $modelCustomer->checkCustomerExist($arrShipCus);
                    $customerId    = 0;
                    if (count($checkCustomer) === 0) {
                        $customerId = $modelCustomer->insertGetId($arrShipCus);
                    } else {
                        $customerId = $checkCustomer->customer_id;
                    }
                    $dataDetails = $modelAOD->getDataDetail($receiveId);
                    if ($dataDetails->count() === 0) {
                        $message = "Order $orderId is not product.";
                        Log::info($message);
                        print_r($message . PHP_EOL);
                        DB::rollback();
                        continue;
                    } else {
                        $num = 0;
                        $arrOrderDetail = [];
                        foreach ($dataDetails as $item) {
                            $priceItem = 0;
                            if ($item->quantity_ordered > 0) {
                                $priceItem = $item->item_price_amount/$item->quantity_ordered;
                            }
                            $num++;
                            $arrOrderDetail[$num]['receive_id']          = (int)$item->receive_id;
                            $arrOrderDetail[$num]['detail_line_num']     = (int)$num;
                            $arrOrderDetail[$num]['product_code']        = (string)$item->seller_sku;
                            $arrOrderDetail[$num]['product_name']        = (string)$item->title;
                            $arrOrderDetail[$num]['price']               = $priceItem;
                            $arrOrderDetail[$num]['item_tax']            = (int)$item->item_tax_amount;
                            $arrOrderDetail[$num]['ship_price']          = (int)$item->shipping_price_amount;
                            $arrOrderDetail[$num]['ship_tax']            = (int)$item->shipping_tax_amount;
                            $arrOrderDetail[$num]['quantity']            = (int)$item->quantity_ordered;
                            $arrOrderDetail[$num]['receiver_id']         = $customerId;
                            $arrOrderDetail[$num]['ship_date']           = null;
                            $arrOrderDetail[$num]['delivery_person_id']  = '';
                            $arrOrderDetail[$num]['package_ship_number'] = '';
                            $arrOrderDetail[$num]['order_item_code']     = $item->order_item_id;
                            $arrOrderDetail[$num]['in_ope_cd']           = 'OPE99999';
                            $arrOrderDetail[$num]['in_date']             = now();
                            $arrOrderDetail[$num]['up_ope_cd']           = 'OPE99999';
                            $arrOrderDetail[$num]['up_date']             = now();
                            $arrOrderDetail[$num]['ship_charge_detail']  = (int)$item->shipping_price_amount;
                            $arrOrderDetail[$num]['goods_price_detail']  = (int)$data->goods_price;
                        }
                        $modelOrderDetail->insert($arrOrderDetail);
                    }
                    $modelAOA->updateProcessFlg($orderId);
                    $success++;
                    DB::commit();
                } catch (\Exception $e) {
                    report($e);
                    $fail++;
                    DB::rollback();
                    continue;
                }
            }
            $message = "Process $total order: - success: $success, - fail: $fail.";
            Log::info($message);
            print_r($message . PHP_EOL);
        }
    }
    /**
     * Get information order from  mall rakuten pay.
     * @return void
     */
    public function processMallRakutenPay()
    {
        $modelRakuten     = new TRakutenPayOrderDetail;
        $modelCustomer    = new MstCustomer;
        $modelOrder       = new MstOrder;
        $modelOrderDetail = new MstOrderDetail;
        $modelMPM = new MstPaymentMethod();
        $dataCheck = $modelRakuten->getDataCheck($this->limit);
        if (count($dataCheck) == 0) {
            Log::info('Mall Rakuten Pay no data');
            print_r("Mall Rakuten Pay no data. " . PHP_EOL);
            return;
        }
        $arrId = [];
        foreach ($dataCheck as $value) {
            if (!in_array($value->order_number, $arrId)) {
                $arrId[] = $value->order_number;
            }
            if ($value->is_gift === 1) {
                continue;
            }
            $modelRakuten->where('rakuten_order_key', '<>', $value->rakuten_order_key)
                    ->where('order_number', '=', $value->order_number)
                    ->where('item_number', '=', $value->item_number)
                    ->update([
                        'is_delete'   => 1,
                        'process_flg' => 1,
                    ]);
        }
        $dataOrderCheck   = $modelOrder->getDataCheckOrder($arrId);
        if (count($dataOrderCheck) !== 0) {
            $dataOrderCheck = array_column($dataOrderCheck->toArray(), 'received_order_id');
        } else {
            $dataOrderCheck = [];
        }
        $datas      = $modelRakuten->getData($arrId);
        $arrOrders  = [];
        $modelMall  = new Mstmall;
        $arrPayment = $modelMPM->getDataByMall(9);
        $totalOrder = count($datas);
        $orderFail  = 0;
        foreach ($datas as $value) {
            $arrCustomer = [];
            $orderNumber = $value->order_number;
            if (in_array($orderNumber, $arrOrders) || in_array($orderNumber, $dataOrderCheck)) {
                $modelRakuten->where('order_number', '=', $orderNumber)
                    ->update([
                        'is_delete'   => 1,
                        'process_flg' => 1,
                    ]);
                continue;
            }
            $arrOrders[] = $orderNumber;
            try {
                DB::beginTransaction();
                $orderZipCode     = $value->zip_code1 . $value->zip_code2;
                $orderTel         = $value->phone_number1 . $value->phone_number2  . $value->phone_number3;
                $shipWishDate     = '';
                $wishDeliveryDate = $value->delivery_date;
                $shipWishDate     = !empty($wishDeliveryDate) ? date('Y-m-d', strtotime($wishDeliveryDate)) : '';
                // Get info customer
                $arrCustomer['first_name']      = (string)$value->first_name;
                $arrCustomer['last_name']       = (string)$value->family_name;
                $arrCustomer['first_name_kana'] = (string)$value->first_name_kana;
                $arrCustomer['last_name_kana']  = (string)$value->family_name_kana;
                $arrCustomer['email']           = (string)$value->email_address;
                $arrCustomer['tel_num']         = $this->removeHyphen($orderTel);
                $arrCustomer['fax_num']         = '';
                $arrCustomer['zip_code']        = $this->removeHyphen($orderZipCode);
                $arrCustomer['prefecture']      = (string)$value->prefecture;
                $arrCustomer['city']            = str_replace($value->prefecture, '', $value->city);
                $arrCustomer['sub_address']     = (string)$value->sub_address;
                $arrCustomer['sex']             = (string)$value->sex;
                $arrCustomer['is_black_list']   = 0;
                $arrCustomer['in_ope_cd']       = 'OPE99999';
                $arrCustomer['in_date']         = now();
                $arrCustomer['up_ope_cd']       = 'OPE99999';
                $arrCustomer['up_date']         = now();
                $checkCustomer = $modelCustomer->checkCustomerExist($arrCustomer, true);
                $customerId    = 0;
                if (count($checkCustomer) === 0) {
                    $customerId = $modelCustomer->insertGetId($arrCustomer);
                } else {
                    $customerId = $checkCustomer->customer_id;
                }
                // Get order info mall Rakuten
                $paymentStatus = 0;
                // if ($value->settlementname === 'クレジットカード') {
                //     if (in_array($value->card_status, [12, 22, 61])) {
                //         $paymentStatus = 1;
                //     }
                // } elseif ($value->settlementname === '楽天バンク決済') {
                //     if ($value->rbankmodel_rbankstatus === 6) {
                //         $paymentStatus = 1;
                //     }
                // }
                $paymentMethod = 0;
                foreach ($arrPayment as $pay) {
                    if ($value->settlementname === $pay->payment_name) {
                        $paymentMethod = $pay->payment_code;
                        break;
                    }
                }

                $orderDate = !empty($value->order_date) ? date('Y-m-d H:i:s', strtotime($value->order_date)) : now();
                $arrTemp = [];
                preg_match_all("/(?<match>[\d]+時から[\d]+時)|(?<match1>午前)/", $value->option, $arrTemp);
                $shipWishTime = '';
                $strCheck     = '';
                if (!empty($arrTemp) && (!empty($arrTemp['match'][0]) || !empty($arrTemp['match1'][0]))) {
                    if (!empty($arrTemp['match'][0])) {
                        $strCheck = $arrTemp['match'][0];
                    } else {
                        $strCheck = $arrTemp['match1'][0];
                    }
                    switch ($strCheck) {
                        case '午前':
                            $shipWishTime = '01';
                            break;
                        case '12時から14時':
                            $shipWishTime = '12';
                            break;
                        case '14時から16時':
                            $shipWishTime = '14';
                            break;
                        case '16時から18時':
                            $shipWishTime = '16';
                            break;
                        case '18時から20時':
                            $shipWishTime = '18';
                            break;
                        case '19時から21時':
                        case '20時から21時':
                            $shipWishTime = '19';
                            break;
                        default:
                            break;
                    }
                }
                // $customerQuestion = $value->option;
                $arrCusQues = preg_split('/$\R?^/m', $value->option);
                foreach ($arrCusQues as $k => $v) {
                    if (empty(trim($v))) {
                        unset($arrCusQues[$k]);
                    } elseif ($v === '[配送日時指定:]') {
                        unset($arrCusQues[$k]);
                    } elseif ($v === '午後') {
                        unset($arrCusQues[$k]);
                    } elseif (!empty($shipWishDate) && strpos($v, $shipWishDate) !== false) {
                        unset($arrCusQues[$k]);
                    } elseif (!empty($strCheck) && strpos($v, $strCheck) !== false) {
                        unset($arrCusQues[$k]);
                    }
                }
                $customerQuestion = implode('', $arrCusQues);
                $arrDataOrder = [];
                /*$arrSettlementCheck = [
                    'セブンイレブン（前払）',
                    'ローソン、郵便局ATM等（前払）',
                    'Apple Pay'
                ];
                if (in_array($value->settlementname, $arrSettlementCheck)) {
                    $arrDataOrder['is_delay']       = 1;
                    $arrDataOrder['delay_priod']    = date("Y-m-d", strtotime("+7 days"));
                    $arrDataOrder['delay_reason']   = $value->settlementname;
                }*/
                $arriveType = null;
                if ((int)$value->asuraku_flg === 1) {
                    $arriveType = (int)$value->asuraku_flg;
                } elseif (($value->delvdateinfo  ===  '16時迄の注文で翌日出荷(土日祝除)') || ($value->delvdateinfo  ===  '16時までの注文で翌日出荷(土日祝除)')) {
                    $arriveType = 2;
                }
                $arrDataOrder['received_order_id']     = (string)$orderNumber;
                $arrDataOrder['mall_id']               = 9;
                $arrDataOrder['customer_id']           = $customerId;
                $arrDataOrder['used_coupon']           = (int)$value->coupon_all_total_price;
                $arrDataOrder['used_point']            = (int)$value->point_model_used_point;
                $arrDataOrder['total_price']           = (int)$value->total_price + (int)$value->delivery_price;
                $arrDataOrder['request_price']         = (int)$value->request_price;
                $arrDataOrder['order_date']            = $orderDate;
                $arrDataOrder['ship_charge']           = (int)$value->postage_price;
                $arrDataOrder['pay_charge']            = (int)$paymentMethod === 17 ? 250 : 0;
                $arrDataOrder['goods_price']           = $value->goods_price + $value->goods_tax;
                $arrDataOrder['goods_tax']             = (int)$value->goods_tax;
                $arrDataOrder['customer_question']     = $customerQuestion;
                $arrDataOrder['discount']              = 0;
                $arrDataOrder['arrive_type']           = $arriveType;
                $arrDataOrder['is_multi_ship_address'] = $value->is_gift;
                $arrDataOrder['ship_wish_time']        = $shipWishTime;
                $arrDataOrder['ship_wish_date']        = (string)$shipWishDate;
                $arrDataOrder['payment_status']        = $paymentStatus;
                $arrDataOrder['payment_method']        = (int)$paymentMethod;
                $arrDataOrder['mail_seri']             = (string)$value->mail_serial;
                $arrDataOrder['order_status']          = ORDER_STATUS['NEW'];
                $arrDataOrder['payment_account']       = '';
                $arrDataOrder['shop_answer']           = '';
                $arrDataOrder['company_name']          = '';
                $arrDataOrder['cancel_reason']         = '';
                $arrDataOrder['pay_after_charge']      = $value->delivery_price;
                $arrDataOrder['pay_charge_discount']   = 0;
                $arrDataOrder['payment_date']          = null;
                $arrDataOrder['payment_confirm_date']  = null;
                $arrDataOrder['in_date']               = now();
                $arrDataOrder['in_ope_cd']             = 'OPE99999';
                $arrDataOrder['up_date']               = now();
                $arrDataOrder['up_ope_cd']             = 'OPE99999';
                //Check payment_method is 全額ポイント
                if ($arrDataOrder['request_price'] <= 0 &&
                    $arrDataOrder['used_point'] === $arrDataOrder['total_price']) {
                    $arrDataOrder['payment_method'] = 10;
                }
                //If payment_method = 0 , we will notify to slack
                if ($arrDataOrder['payment_method'] === 0) {
                    $error  = "---------------------------" . PHP_EOL;
                    $error .= basename(__CLASS__) . PHP_EOL;
                    $error .= "Payment Method is error"  . PHP_EOL;
                    $error .= "Order Id : {$arrDataOrder['received_order_id']}"  . PHP_EOL;
                    $error .= "Mall Id : {$arrDataOrder['mall_id']}"  . PHP_EOL;
                    $error .= "---------------------------" . PHP_EOL;
                    $this->slack->notify(new SlackNotification($error));
                }

                $receiveId      = $modelOrder->insertGetId($arrDataOrder);
                $arrDataDetail  = $modelRakuten->getInfoOrderDetail($receiveId);
                $detailLineNum  = 0;
                $arrOrderDetail = [];
                foreach ($arrDataDetail as $value) {
                    $arrShipCus = [];
                    $detailLineNum++;
                    $shipZipCode  = $value->sendermodel_zipcode1 . $value->sendermodel_zipcode2;
                    $shipTelNum   = $value->sendermodel_phonenumber1;
                    $shipTelNum  .= $value->sendermodel_phonenumber2;
                    $shipTelNum  .= $value->sendermodel_phonenumber3;
                    $id  = $value->rakuten_order_key;
                    // Get data ship address
                    $arrShipCus['first_name']      = (string)$value->sendermodel_firstname;
                    $arrShipCus['last_name']       = (string)$value->sendermodel_familyname;
                    $arrShipCus['first_name_kana'] = (string)$value->sendermodel_firstnamekana;
                    $arrShipCus['last_name_kana']  = (string)$value->sendermodel_familynamekana;
                    $arrShipCus['email']           = (string)$value->sendermodel_email_address;
                    $arrShipCus['tel_num']         = $this->removeHyphen($shipTelNum);
                    $arrShipCus['fax_num']         = '';
                    $arrShipCus['zip_code']        = $this->removeHyphen($shipZipCode);
                    $arrShipCus['prefecture']      = $this->removeSpaces((string)$value->sendermodel_prefecture);
                    $arrShipCus['city']            = $this->removeSpaces(str_replace($value->sendermodel_prefecture, '', $value->sendermodel_city));
                    $arrShipCus['sub_address']     = $this->removeSpaces((string)$value->sendermodel_subaddress);
                    // $arrShipCus['sex']             = (string)$value->sendermodel_sex;
                    $arrShipCus['in_ope_cd']       = 'OPE99999';
                    $arrShipCus['in_date']         = now();
                    $arrShipCus['up_ope_cd']       = 'OPE99999';
                    $arrShipCus['up_date']         = now();
                    $checkCustomer = $modelCustomer->checkCustomerExist($arrShipCus);
                    $customerId    = 0;
                    if (count($checkCustomer) === 0) {
                        $customerId = $modelCustomer->insertGetId($arrShipCus);
                    } else {
                        $customerId = $checkCustomer->customer_id;
                    }
                    $arrOrderDetail[$id]['receive_id']         = (int)$value->receive_id;
                    $arrOrderDetail[$id]['detail_line_num']    = (int)$detailLineNum;
                    $arrOrderDetail[$id]['product_code']       = (string)$value->item_number;
                    $arrOrderDetail[$id]['product_name']       = (string)$value->item_name;
                    $arrOrderDetail[$id]['price']              = floor($value->price*1.08);
                    $arrOrderDetail[$id]['item_tax']           = 0;
                    $arrOrderDetail[$id]['ship_price']         = 0;
                    $arrOrderDetail[$id]['ship_tax']           = 0;
                    $arrOrderDetail[$id]['quantity']           = (int)$value->units;
                    $arrOrderDetail[$id]['receiver_id']        = $customerId;
                    if (!empty($value->shipping_date)) {
                        $shippingDate = $value->shipping_date;
                        $arrOrderDetail[$id]['ship_date'] = date('Y-m-d', strtotime($shippingDate));
                    } else {
                        $arrOrderDetail[$id]['ship_date'] = null;
                    }
                    $arrOrderDetail[$id]['delivery_person_id']  = '';
                    $arrOrderDetail[$id]['package_ship_number'] = '';
                    $arrOrderDetail[$id]['point_rate']          = (int)$value->point_rate;
                    $arrOrderDetail[$id]['in_ope_cd']           = 'OPE99999';
                    $arrOrderDetail[$id]['in_date']             = now();
                    $arrOrderDetail[$id]['up_ope_cd']           = 'OPE99999';
                    $arrOrderDetail[$id]['up_date']             = now();
                    $arrOrderDetail[$id]['ship_charge_detail']  = (int)$value->packagemodel_postageprice;
                    $arrOrderDetail[$id]['goods_price_detail']  = (int)$value->packagemodel_goodsprice;
                }
                $modelOrderDetail->insert($arrOrderDetail);
                $keyIdUpdate        = array_keys($arrOrderDetail);
                $modelRakuten->updateProcessFlg($keyIdUpdate);
                DB::commit();
            } catch (\Exception $e) {
                print_r($e->getMessage());
                DB::rollback();
                $message = "Can't process order {$value->order_number}";
                print_r($message . PHP_EOL);
                Log::error($message);
                report($e);
                $orderFail++;
                continue;
            }
        }
        $message1 = "Processed orders from mall rakuten : {$totalOrder}";
        print_r($message1 . PHP_EOL);
        Log::info($message1);
        $message2  = "Order Fail : $orderFail";
        print_r($message2 . PHP_EOL);
        Log::info($message2);
    }

    /**
     * Process mall Wowma
     * @return void
     */
    public function processMallWowma()
    {
        $tWowmaOrder       = new TWowmaOrder();
        $tWowmaOrderDetail = new TWowmaOrderDetail();
        $mstOrder          = new MstOrder;
        $mstOrderDetail    = new MstOrderDetail;
        $mstCustomer       = new MstCustomer;
        $mstPaymentMethod  = new MstPaymentMethod();
        $wowmaMallId       = 10;

        $datas = $tWowmaOrder->getData();
        $totalOrder = $datas->count();
        if ($totalOrder === 0) {
            Log::info('Mall Wowma no data');
            print_r("Mall Wowma no data. " . PHP_EOL);
            return;
        }
        $arrId          = $datas->pluck('order_id');
        $dataOrderCheck = $mstOrder->getDataCheckOrder($arrId)->pluck('received_order_id')->toArray();
        $arrPayment     = $mstPaymentMethod->getDataByMall($wowmaMallId);
        $arrOrders      = [];
        $orderFail      = 0;
        foreach ($datas as $data) {
            $orderId = $data->order_id;
            if (in_array($orderId, $arrOrders) || in_array($orderId, $dataOrderCheck)) {
                $tWowmaOrder->where('order_id', $orderId)
                    ->update([
                        'is_deleted'  => 1,
                        'process_flg' => 1,
                    ]);
                continue;
            }
            $arrOrders[] = $orderId;
            $arrCustomer = [];
            try {
                DB::beginTransaction();

                $ordererName = (string)$data->orderer_name;
                $firstName   = '';
                $lastName    = $ordererName;
                if (($posName = mb_strpos($ordererName, ' ')) !== false) {
                    $lastName  = mb_substr($ordererName, 0, $posName + 1);
                    $firstName = mb_substr($ordererName, $posName + 1);
                }

                $ordererAddr = (string)$data->orderer_address;
                $prefecture  = '';
                $city        = '';
                if (($posAddr = mb_strpos($ordererAddr, '都')) !== false) {
                    $prefecture = mb_substr($ordererAddr, 0, $posAddr + 1);
                    $city       = mb_substr($ordererAddr, $posAddr + 1);
                } elseif (($posAddr = mb_strpos($ordererAddr, '府')) !== false) {
                    $prefecture = mb_substr($ordererAddr, 0, $posAddr + 1);
                    $city       = mb_substr($ordererAddr, $posAddr + 1);
                } elseif (($posAddr = mb_strpos($ordererAddr, '県')) !== false) {
                    $prefecture = mb_substr($ordererAddr, 0, $posAddr + 1);
                    $city       = mb_substr($ordererAddr, $posAddr + 1);
                } elseif (($posAddr = mb_strpos($ordererAddr, '道')) !== false) {
                    $prefecture = mb_substr($ordererAddr, 0, $posAddr + 1);
                    $city       = mb_substr($ordererAddr, $posAddr + 1);
                }

                // Get info customer
                $arrCustomer['first_name']      = $firstName;
                $arrCustomer['last_name']       = $lastName;
                $arrCustomer['first_name_kana'] = '';
                $arrCustomer['last_name_kana']  = (string)$data->orderer_kana;
                $arrCustomer['email']           = (string)$data->mail_address;
                $arrCustomer['tel_num']         = str_replace('-', '', (string)$data->orderer_phone_number1);
                $arrCustomer['fax_num']         = str_replace('-', '', (string)$data->orderer_phone_number2);
                $arrCustomer['zip_code']        = str_replace('-', '', (string)$data->orderer_zip_code);
                $arrCustomer['prefecture']      = trim($prefecture);
                $arrCustomer['city']            = trim($city);
                $arrCustomer['sub_address']     = '';
                $arrCustomer['sex']             = '';
                $arrCustomer['is_black_list']   = 0;
                $arrCustomer['urgent_tel_num']  = '';
                $arrCustomer['in_ope_cd']       = 'OPE99999';
                $arrCustomer['in_date']         = now();
                $arrCustomer['up_ope_cd']       = 'OPE99999';
                $arrCustomer['up_date']         = now();
                $checkCustomer = $mstCustomer->checkCustomerExist($arrCustomer, true);
                $customerId    = 0;
                if (count($checkCustomer) === 0) {
                    $customerId = $mstCustomer->insertGetId($arrCustomer);
                } else {
                    $customerId = $checkCustomer->customer_id;
                }

                $orderDate = !empty($data->order_date) ? date('Y-m-d H:i:s', strtotime($data->order_date)) : now();

                $arrTemp = [];
                preg_match_all("/(?<match>[\d]+時から[\d]+時)|(?<match1>午前)/", $data->delivery_request_time, $arrTemp);
                $shipWishTime = '';
                $strCheck     = '';
                if (!empty($arrTemp) && (!empty($arrTemp['match'][0]) || !empty($arrTemp['match1'][0]))) {
                    if (!empty($arrTemp['match'][0])) {
                        $strCheck = $arrTemp['match'][0];
                    } else {
                        $strCheck = $arrTemp['match1'][0];
                    }
                    switch ($strCheck) {
                        case '午前':
                            $shipWishTime = '01';
                            break;
                        case '12時から14時':
                            $shipWishTime = '12';
                            break;
                        case '14時から16時':
                            $shipWishTime = '14';
                            break;
                        case '16時から18時':
                            $shipWishTime = '16';
                            break;
                        case '18時から20時':
                            $shipWishTime = '18';
                            break;
                        case '19時から21時':
                        case '20時から21時':
                            $shipWishTime = '19';
                            break;
                        default:
                            break;
                    }
                }

                $paymentStatus = 0;
                if ($data->settle_status === 'AD') {
                    $paymentStatus = 1;
                }

                $paymentMethod = 0;
                foreach ($arrPayment as $pay) {
                    if ($data->settlement_name === $pay->payment_name) {
                        $paymentMethod = $pay->payment_code;
                        break;
                    }
                }
                if ((int)$data->request_price <= 0 && (int)$data->total_price === (int)$data->use_point) {
                    $paymentMethod = 10;
                }

                $deliReqDay   = (string)$data->delivery_request_day;
                $shipWishDate = !empty($deliReqDay)?date('Y-m-d', strtotime($deliReqDay)):'';

                $arrDataOrder = [];
                $arrDataOrder['received_order_id']     = $orderId;
                $arrDataOrder['mall_id']               = $wowmaMallId;
                $arrDataOrder['customer_id']           = $customerId;
                $arrDataOrder['used_coupon']           = (int)$data->coupon_total_price;
                $arrDataOrder['used_point']            = (int)$data->use_au_point_price + (int)$data->use_point;
                $arrDataOrder['total_price']           = (int)$data->total_price;
                $arrDataOrder['request_price']         = (int)$data->request_price;
                $arrDataOrder['order_date']            = $orderDate;
                $arrDataOrder['ship_charge']           = (int)$data->postage_price;
                $arrDataOrder['pay_charge']            = 0;
                $arrDataOrder['goods_price']           = (int)$data->total_sale_price;
                $arrDataOrder['goods_tax']             = 0;
                $arrDataOrder['discount']              = 0;
                $arrDataOrder['arrive_type']           = 0;
                $arrDataOrder['is_multi_ship_address'] = 0;
                $arrDataOrder['ship_wish_time']        = $shipWishTime;
                $arrDataOrder['ship_wish_date']        = $shipWishDate;
                $arrDataOrder['payment_status']        = $paymentStatus;
                $arrDataOrder['payment_method']        = (int)$paymentMethod;
                $arrDataOrder['mail_seri']             = MD5($orderId);
                $arrDataOrder['order_status']          = ORDER_STATUS['NEW'];
                $arrDataOrder['payment_price']         = 0;
                $arrDataOrder['payment_account']       = '';
                $arrDataOrder['customer_question']     = (string)$data->user_comment;
                $arrDataOrder['shop_answer']           = '';
                $arrDataOrder['company_name']          = null;
                $arrDataOrder['cancel_reason']         = '';
                $arrDataOrder['pay_after_charge']      = $data->charge_price;
                $arrDataOrder['pay_charge_discount']   = 0;
                $arrDataOrder['payment_date']          = null;
                $arrDataOrder['payment_confirm_date']  = null;
                $arrDataOrder['is_delay']              = ($data->card_jadgement > 1) ? 1 : 0;
                $arrDataOrder['delay_priod']           = ($data->card_jadgement > 1) ? now() : null;
                $arrDataOrder['delay_reason']          = ($data->card_jadgement > 1) ? '不正不審の可能性あり(可)' : '';
                $arrDataOrder['is_send_mail_urgent']   = ($data->card_jadgement > 1) ? 1 : 0;
                $arrDataOrder['urgent_mail_id']        = ($data->card_jadgement > 1) ? 38 : 0;
                $arrDataOrder['in_ope_cd']             = 'OPE99999';
                $arrDataOrder['up_date']               = now();
                $arrDataOrder['up_ope_cd']             = 'OPE99999';
                $receiveId = $mstOrder->insertGetId($arrDataOrder);

                $senderName = (string)$data->sender_name;
                $firstName   = '';
                $lastName    = $senderName;
                if (($posName = mb_strpos($senderName, ' ')) !== false) {
                    $lastName  = mb_substr($senderName, 0, $posName + 1);
                    $firstName = mb_substr($senderName, $posName + 1);
                }

                $senderAddr = (string)$data->sender_address;
                $prefecture  = '';
                $city        = '';
                if (($posAddr = mb_strpos($senderAddr, '都')) !== false) {
                    $prefecture = mb_substr($senderAddr, 0, $posAddr + 1);
                    $city       = mb_substr($senderAddr, $posAddr + 1);
                } elseif (($posAddr = mb_strpos($senderAddr, '府')) !== false) {
                    $prefecture = mb_substr($senderAddr, 0, $posAddr + 1);
                    $city       = mb_substr($senderAddr, $posAddr + 1);
                } elseif (($posAddr = mb_strpos($senderAddr, '県')) !== false) {
                    $prefecture = mb_substr($senderAddr, 0, $posAddr + 1);
                    $city       = mb_substr($senderAddr, $posAddr + 1);
                } elseif (($posAddr = mb_strpos($senderAddr, '道')) !== false) {
                    $prefecture = mb_substr($senderAddr, 0, $posAddr + 1);
                    $city       = mb_substr($senderAddr, $posAddr + 1);
                }

                $arrShipCus = [];
                $arrShipCus['first_name']      = $firstName;
                $arrShipCus['last_name']       = $lastName;
                $arrShipCus['first_name_kana'] = '';
                $arrShipCus['last_name_kana']  = (string)$data->sender_kana;
                $arrShipCus['email']           = '';
                $arrShipCus['tel_num']         = str_replace('-', '', (string)$data->sender_phone_number1);
                $arrShipCus['urgent_tel_num']  = str_replace('-', '', (string)$data->sender_phone_number2);
                $arrShipCus['fax_num']         = '';
                $arrShipCus['zip_code']        = str_replace('-', '', (string)$data->sender_zip_code);
                $arrShipCus['prefecture']      = $this->removeSpaces(trim($prefecture));
                $arrShipCus['city']            = $this->removeSpaces(trim($city));
                $arrShipCus['sub_address']     = '';
                $arrShipCus['sex']             = '';
                $arrShipCus['in_ope_cd']       = 'OPE99999';
                $arrShipCus['in_date']         = now();
                $arrShipCus['up_ope_cd']       = 'OPE99999';
                $arrShipCus['up_date']         = now();
                $checkCustomer = $mstCustomer->checkCustomerExist($arrShipCus);
                $customerId    = 0;
                if (count($checkCustomer) === 0) {
                    $customerId = $mstCustomer->insertGetId($arrShipCus);
                } else {
                    $customerId = $checkCustomer->customer_id;
                }
                $arrDataDetail  = $tWowmaOrderDetail->getInfoOrderDetail($receiveId);
                $num            = 0;
                $arrOrderDetail = [];
                foreach ($arrDataDetail as $item) {
                    $num++;
                    $arrOrderDetail[$num]['receive_id']          = (int)$item->receive_id;
                    $arrOrderDetail[$num]['detail_line_num']     = (int)$num;
                    $arrOrderDetail[$num]['product_code']        = (string)$item->item_code;
                    $arrOrderDetail[$num]['product_name']        = (string)$item->item_name;
                    $arrOrderDetail[$num]['price']               = (int)$item->item_price;
                    $arrOrderDetail[$num]['item_tax']            = 0;
                    $arrOrderDetail[$num]['ship_price']          = 0;
                    $arrOrderDetail[$num]['ship_tax']            = 0;
                    $arrOrderDetail[$num]['quantity']            = (int)$item->unit;
                    $arrOrderDetail[$num]['receiver_id']         = $customerId;
                    $arrOrderDetail[$num]['ship_date']           = null;
                    $arrOrderDetail[$num]['delivery_person_id']  = null;
                    $arrOrderDetail[$num]['package_ship_number'] = null;
                    $arrOrderDetail[$num]['order_item_code']     = null;
                    $arrOrderDetail[$num]['ship_charge_detail']  = (int)$data->postage_price;
                    $arrOrderDetail[$num]['goods_price_detail']  = (int)$item->total_item_price;
                    $arrOrderDetail[$num]['in_ope_cd']           = 'OPE99999';
                    $arrOrderDetail[$num]['in_date']             = now();
                    $arrOrderDetail[$num]['up_ope_cd']           = 'OPE99999';
                    $arrOrderDetail[$num]['up_date']             = now();
                }
                $mstOrderDetail->insert($arrOrderDetail);
                $tWowmaOrder->updateProcessFlg($orderId);
                DB::commit();
            } catch (\Exception $e) {
                print_r($e->getMessage());
                DB::rollback();
                $message = "Can't process order {$data->order_id}";
                print_r($message . PHP_EOL);
                Log::error($message);
                report($e);
                $orderFail++;
                continue;
            }
        }
        $message1 = "Processed orders from mall Wowma : {$totalOrder}";
        print_r($message1 . PHP_EOL);
        Log::info($message1);
        $message2  = "Order Fail : $orderFail";
        print_r($message2 . PHP_EOL);
        Log::info($message2);
    }
    /**
     * Get information order from  mall amazon.
     * @return void
     */
    public function processMallAmazon()
    {
        $modelAmazon      = new TAmazonOrder;
        $modelCustomer    = new MstCustomer;
        $modelOrder       = new MstOrder;
        $modelOrderDetail = new MstOrderDetail;
        $modelMPM = new MstPaymentMethod();
        $dataCheck = $modelAmazon->getDataCheck($this->limit);
        if (count($dataCheck) == 0) {
            Log::info('Mall Amazon no data');
            print_r("Mall Amazon no data. " . PHP_EOL);
            return;
        }
        $arrId = [];
        foreach ($dataCheck as $value) {
            if (!in_array($value->order_id, $arrId)) {
                $arrId[] = $value->order_id;
            }
            $modelAmazon->where('id', '<>', $value->id)
                    ->where('order_id', '=', $value->order_id)
                    ->where('sku', '=', $value->sku)
                    ->update([
                        'is_delete'   => 1,
                        'process_flg' => 1,
                    ]);
        }
        $dataOrderCheck   = $modelOrder->getDataCheckOrder($arrId);
        if (count($dataOrderCheck) !== 0) {
            $dataOrderCheck = array_column($dataOrderCheck->toArray(), 'received_order_id');
        } else {
            $dataOrderCheck = [];
        }
        $datas              = $modelAmazon->getData($arrId);
        $arrOrders          = [];
        $modelMall          = new Mstmall;
        $mall               = $modelMall->getInfoMallByName('amazon');
        $arrPayment         = $modelMPM->getDataByMall($mall->id);
        $totalAmount        = $modelAmazon->getAllTypePrice();
        $succCus            = 0;
        $failCus            = 0;
        $orderSuccess       = 0;
        $orderFail          = 0;
        $orderDetailSuccess = 0;
        $orderDetailFail    = 0;
        $countDetail        = 0;
        $updatFlag          = 0;
        foreach ($datas as $value) {
            $arrCustomer = [];
            $orderId     = $value->order_id;
            if (in_array($orderId, $arrOrders) || in_array($orderId, $dataOrderCheck)) {
                $modelAmazon->where('order_id', '=', $orderId)
                    ->update([
                        'is_delete'   => 1,
                        'process_flg' => 1,
                    ]);
                continue;
            }
            $countDetail = 0;
            $arrOrders[] = $orderId;
            try {
                DB::beginTransaction();
                $orderDate       = !empty($value->purchase_date) && $this->isDate($value->purchase_date) ?
                                (new \DateTime($value->purchase_date))->format('Y-m-d H:i:s') : now();
                $orderSubAddress = $value->bill_address_2 . $value->bill_address_3 ;
                // Get info customer
                $city = (string)$value->bill_city . (string)$value->bill_address_1;
                $arrCustomer['first_name']      = '';
                $arrCustomer['last_name']       = (string)$value->buyer_name;
                $arrCustomer['first_name_kana'] = '';
                $arrCustomer['last_name_kana']  = '';
                $arrCustomer['email']           = (string)$value->buyer_email;
                $arrCustomer['tel_num']         = $this->removeHyphen($value->buyer_phone_number);
                $arrCustomer['fax_num']         = '';
                $arrCustomer['zip_code']        = $this->removeHyphen($value->bill_postal_code);
                $arrCustomer['prefecture']      = $this->removeSpaces((string)$value->bill_state);
                $arrCustomer['city']            = $this->removeSpaces(str_replace($value->bill_state, '', $city));
                $arrCustomer['sub_address']     = $this->removeSpaces((string)$orderSubAddress);
                $arrCustomer['sex']             = '';
                $arrCustomer['is_black_list']   = 0;
                $arrCustomer['in_ope_cd']       = 'OPE99999';
                $arrCustomer['in_date']         = now();
                $arrCustomer['up_ope_cd']       = 'OPE99999';
                $arrCustomer['up_date']         = now();
                $checkCustomer = $modelCustomer->checkCustomerExist($arrCustomer, true);
                $customerId    = 0;
                if (count($checkCustomer) === 0) {
                    $customerId = $modelCustomer->insertGetId($arrCustomer);
                    $succCus++;
                } else {
                    $customerId = $checkCustomer->customer_id;
                }
                if ($value->payment_method_details === 'Invoice') {
                    $paymentStatus = 1;
                    $paymentMethod = 5;
                } elseif ($value->payment_method_details === 'Standard') {
                    $paymentStatus = 1;
                    $paymentMethod = 1;
                } elseif ($value->payment_method_details === 'CashOnDelivery') {
                    $paymentStatus = 0;
                    $paymentMethod = 3;
                } else {
                    $paymentStatus = 0;
                    $paymentMethod = 1;
                }
                $arrDataOrders = [];
                $arrDataOrders['received_order_id']     = (string)$orderId;
                $arrDataOrders['mall_id']               = (int)$mall->id;
                $arrDataOrders['customer_id']           = $customerId;
                $arrDataOrders['used_coupon']           = 0;
                $arrDataOrders['used_point']            = (int)$value->already_paid;
                $arrDataOrders['discount']              = 0;
                $arrDataOrders['total_price']           = (int)$totalAmount[$orderId]['total_price'];
                $arrDataOrders['request_price']         = (int)$totalAmount[$orderId]['request_price'];
                $arrDataOrders['order_date']            = $orderDate;
                $arrDataOrders['ship_charge']           = (int)$totalAmount[$orderId]['ship_charge'];
                $arrDataOrders['pay_charge']            = 0;
                $arrDataOrders['goods_price']           = (int)$totalAmount[$orderId]['goods_price'];
                $arrDataOrders['goods_tax']             = (int)$totalAmount[$orderId]['goods_tax'];
                $arrDataOrders['customer_question']     = '';
                $arrDataOrders['arrive_type']           = $value->is_prime === 'true' ? 1 : 0;
                $arrDataOrders['is_multi_ship_address'] = 0;
                $arrDataOrders['ship_wish_time']        = '';
                $arrDataOrders['ship_wish_date']        = '';
                $arrDataOrders['payment_status']        = $paymentStatus;
                $arrDataOrders['payment_method']        = $paymentMethod;
                $arrDataOrders['mail_seri']             = MD5($orderId);
                $arrDataOrders['order_status']          = ORDER_STATUS['NEW'];
                $arrDataOrders['payment_account']       = '';
                $arrDataOrders['shop_answer']           = '';
                $arrDataOrders['company_name']          = '';
                $arrDataOrders['cancel_reason']         = '';
                $arrDataOrders['pay_after_charge']      = (int)$value->payment_method_fee;
                $arrDataOrders['pay_charge_discount']   = 0;
                $arrDataOrders['payment_date']          = null;
                $arrDataOrders['payment_confirm_date']  = null;
                $arrDataOrders['is_sourcing_on_demand'] = ($value->is_sourcing_on_demand === 'false') ? 0 : 1;
                $arrDataOrders['in_ope_cd']             = 'OPE99999';
                $arrDataOrders['in_date']               = now();
                $arrDataOrders['up_ope_cd']             = 'OPE99999';
                $arrDataOrders['up_date']               = now();
                if ($value->is_sourcing_on_demand === 'true') {
                    $arrDataOrders['response_due_date']     = $value->response_due_date;
                }
                //Check payment_method is 全額ポイント
                if ($arrDataOrders['request_price'] <= 0 &&
                    $arrDataOrders['used_point'] === $arrDataOrders['total_price']) {
                    $arrDataOrders['payment_method'] = 10;
                }
                //If payment_method = 0 , we will notify to slack
                if ($arrDataOrders['payment_method'] === 0) {
                    $error  = "---------------------------" . PHP_EOL;
                    $error .= basename(__CLASS__) . PHP_EOL;
                    $error .= "Payment Method is error"  . PHP_EOL;
                    $error .= "Order Id : {$arrDataOrders['received_order_id']}"  . PHP_EOL;
                    $error .= "Mall Id : {$arrDataOrders['mall_id']}"  . PHP_EOL;
                    $error .= "---------------------------" . PHP_EOL;
                    $this->slack->notify(new SlackNotification($error));
                }
                $check = $modelOrder->getDataByOrderId($orderId);
                $receiveId = '';
                $exists = false;
                if (count($check) === 0) {
                    $receiveId = $modelOrder->insertGetId($arrDataOrders);
                    $orderSuccess++;
                } else {
                    $receiveId = $check->receive_id;
                    $exists = true;
                }
                $arrDataDetail  = $modelAmazon->getInfoOrderDetail($receiveId);
                $detailLineNum  = $exists ? $check->detail_line_num : 0;
                $arrOrderDetail = [];
                $arrProductCode = $modelOrderDetail->getProductCodeByReceiveId($receiveId);
                if (count($arrProductCode) !== 0) {
                    $arrProductCode   = array_column($arrProductCode->toArray(), 'product_code');
                } else {
                    $arrProductCode = [];
                }
                foreach ($arrDataDetail as $value) {
                    $arrShipCus = [];
                    $detailLineNum++;
                    $id             = $value->id;
                    $shipSubAddress = $value->ship_address_2 . $value->ship_address_3;
                    // Get data ship address
                    if (mb_strlen($value->ship_phone_number) !== strlen($value->ship_phone_number)) {
                        $value->ship_phone_number = mb_convert_kana($value->ship_phone_number, 'KVa');
                    }
                    $shipCity = (string)$value->ship_city . (string)$value->ship_address_1;
                    $arrShipCus['first_name']      = '';
                    $arrShipCus['last_name']       = (string)$value->recipient_name;
                    $arrShipCus['first_name_kana'] = '';
                    $arrShipCus['last_name_kana']  = '';
                    $arrShipCus['email']           = '';
                    $arrShipCus['tel_num']         = $this->removeHyphen($value->ship_phone_number);
                    $arrShipCus['fax_num']         = '';
                    $arrShipCus['zip_code']        = $this->removeHyphen($value->ship_postal_code);
                    $arrShipCus['prefecture']      = $this->removeSpaces((string)$value->ship_state);
                    $arrShipCus['city']            = $this->removeSpaces(str_replace($value->ship_state, '', $shipCity));
                    $arrShipCus['sub_address']     = $this->removeSpaces((string)$shipSubAddress);
                    $arrShipCus['sex']             = '';
                    $arrShipCus['in_ope_cd']       = 'OPE99999';
                    $arrShipCus['in_date']         = now();
                    $arrShipCus['up_ope_cd']       = 'OPE99999';
                    $arrShipCus['up_date']         = now();
                    $checkCustomer = $modelCustomer->checkCustomerExist($arrShipCus);
                    $customerId    = 0;
                    if (count($checkCustomer) === 0) {
                        $customerId = $modelCustomer->insertGetId($arrShipCus);
                        $succCus++;
                    } else {
                        $customerId = $checkCustomer->customer_id;
                    }
                    if (in_array($value->sku, $arrProductCode)) {
                        $checkPF     = $modelAmazon->updateProcessFlg(array($id));
                        if ($checkPF) {
                            $updatFlag++;
                        }
                        continue;
                    } else {
                        $arrProductCode[] = $value->sku;
                    }
                    $price = (int)$value->item_price/(int)$value->quantity_purchased;
                    $arrOrderDetail[$id]['receive_id']          = (int)$value->receive_id;
                    $arrOrderDetail[$id]['detail_line_num']     = (int)$detailLineNum;
                    $arrOrderDetail[$id]['product_code']        = (string)$value->sku;
                    $arrOrderDetail[$id]['product_name']        = (string)$value->product_name;
                    $arrOrderDetail[$id]['price']               = $price;
                    $arrOrderDetail[$id]['item_tax']            = (int)$value->item_tax;
                    $arrOrderDetail[$id]['ship_price']          = (int)$value->shipping_price;
                    $arrOrderDetail[$id]['ship_tax']            = (int)$value->shipping_tax;
                    $arrOrderDetail[$id]['quantity']            = (int)$value->quantity_purchased;
                    $arrOrderDetail[$id]['receiver_id']         = $customerId;
                    $arrOrderDetail[$id]['ship_date']           = null;
                    $arrOrderDetail[$id]['delivery_person_id']  = '';
                    $arrOrderDetail[$id]['package_ship_number'] = '';
                    $arrOrderDetail[$id]['order_item_code']     = $value->order_item_id;
                    $arrOrderDetail[$id]['in_ope_cd']           = 'OPE99999';
                    $arrOrderDetail[$id]['in_date']             = now();
                    $arrOrderDetail[$id]['up_ope_cd']           = 'OPE99999';
                    $arrOrderDetail[$id]['up_date']             = now();
                    $arrOrderDetail[$id]['ship_charge_detail']  = (int)$totalAmount[$orderId]['ship_charge'];
                    $arrOrderDetail[$id]['goods_price_detail']  = (int)$totalAmount[$orderId]['goods_price'];
                }
                $countDetail = count($arrOrderDetail);
                $modelOrderDetail->insert($arrOrderDetail);
                $orderDetailSuccess += $countDetail;
                $keyIdUpdate = array_keys($arrOrderDetail);
                $checkPF     = $modelAmazon->updateProcessFlg($keyIdUpdate);
                if ($checkPF) {
                    $updatFlag += count($keyIdUpdate);
                }
                DB::commit();
            } catch (\Exception $e) {
                $this->error[] = Utilities::checkMessageException($e);
                $error  = "------------------------------------------" . PHP_EOL;
                $error .= basename(__CLASS__) . PHP_EOL;
                $error .= Utilities::checkMessageException($e);
                $error .= PHP_EOL . "------------------------------------------" . PHP_EOL;
                $this->slack->notify(new SlackNotification($error));
                Log::error(Utilities::checkMessageException($e));
                print_r("$error");
                $failCus++;
                $orderFail++;
                $orderDetailFail += $countDetail;
                DB::rollback();
                continue;
            }
        }
        $messageC  = "Insert to table mst_customer success: $succCus records";
        $messageC .= "and fail : $failCus records";
        Log::info($messageC);
        print_r($messageC . PHP_EOL);
        $messageO  = "Insert to table mst_order success: $orderSuccess records";
        $messageO .= " and fail : $orderFail records";
        Log::info($messageO);
        print_r($messageO . PHP_EOL);
        $messageOD  = "Insert to mst_order_detail success: $orderDetailSuccess records";
        $messageOD .= " and fail : $orderDetailFail records";
        Log::info($messageOD);
        print_r($messageOD . PHP_EOL);
        Log::info("Update Process flag success : $updatFlag records");
        print_r("Update Process flag success : $updatFlag records" . PHP_EOL);
    }
    /**
     * Check is date.
     * @param string $value
     * @return boolean
     */
    public function isDate($value)
    {
        if (!trim($value)) {
            return false;
        }
        try {
            new \DateTime($value);
            return true;
        } catch (\Exception $e) {
            return false;
        }
    }
    /**
     * Remove hyphen of tel and zip
     * @param string $value
     * @return string
     */
    public function removeHyphen($value)
    {
        $str = mb_convert_kana($value, 'KVa');
        return str_replace('-', '', $str);
    }

    /**
     * Remove spaces 1 byte or 2 bytes
     * @param string $string
     * @return string
     */
    public function removeSpaces($string)
    {
        $string = str_replace(" ", "", $string);
        $string = str_replace("　", "", $string);
        $string = mb_convert_kana($string, 'KVa');
        //Because mb_convert_kana can't convert han to zen in some case so
        //we will replace
        $replaceOf = array('ヶ');
        $replaceBy = array('ケ');
        $string = str_replace($replaceOf, $replaceBy, $string);
        return $string;
    }
}
