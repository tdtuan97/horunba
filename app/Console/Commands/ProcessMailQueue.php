<?php
/**
 * Batch process content to mail queue
 *
 * @package    App\Console\Commands
 * @subpackage ProcessMailQueue
 * @copyright  Copyright (c) 2017 RiverCrane! Corporation. All Rights Reserved.
 * @author     Truong Nghia<truong.van.nghia@rivercrane.vn>
 */

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\Batches\MstOrder;
use App\Models\Batches\MstSettlementManage;
use App\Models\Batches\DtSendMailList;
use App\Models\Batches\MstPaymentAccount;
use Illuminate\Support\Facades\Log;
use App\Notifications\SlackNotification;
use App\Custom\Utilities;
use App\Notification;
use App\Events\Command as eCommand;
use Event;

class ProcessMailQueue extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'process:mail-queue';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Add content to mail queue';

    /**
     * The error.
     *
     * @var array
     */
    public $error = [];

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //Create log
        $arrayReplace = [':', '-'];
        $folder       = str_replace($arrayReplace, '_', $this->signature);
        Log::useDailyFiles(storage_path() . "/logs/$folder/history.log");
        //Process data
        Event::fire(new eCommand($this->signature, array('start' => true)));
        Log::info('Start batch process mail queue.');
        print_r("Start batch process mail queue." . PHP_EOL);
        $start    = microtime(true);
        $slack    = new Notification(CHANNEL['horunba']);
        $modelO   = new MstOrder();
        $modelSML = new DtSendMailList();
        $modelPA  = new MstPaymentAccount();
        $datas    = $modelO->getDataMailQueue();
        if (count($datas) !== 0) {
            $arrReceiveId = [];
            $success      = 0;
            $fail         = 0;
            foreach ($datas as $key => $data) {
                try {
                    // We will no send mail in case amazon
                    if ($data->mall_id <> 3) {
                        if ($data->mall_id === 9 &&
                            $data->order_status_id === ORDER_STATUS['PAYMENT_CONFIRM'] &&
                            ($data->order_sub_status_id === ORDER_SUB_STATUS['DONE'] ||
							$data->order_sub_status_id === ORDER_SUB_STATUS['DELAY'])
                        ) {
                            $modelO->updateData(array($data->receive_id), ['is_mail_sent' => 1]);
                            continue;
                        }
                        if ($data->order_status_id === ORDER_STATUS['ORDER'] &&
                            $data->order_sub_status_id === ORDER_SUB_STATUS['DOING'] &&
                            empty($data->delivery_plan_date)) {
                            $error = "Receive Id :". $data->receive_id ." error delivery_plan_date". PHP_EOL;
                            $slack->notify(new SlackNotification($error));
                            continue;
                        }
                        $arrInsert   = [];
                        $arrUpdate   = [];
                        $mailContent = '';
                        $mailSubject = '';
                        list($mailContent, $arrUpdate) = $this->processContent(
                            $data->mail_content,
                            $data->payment_method,
                            $data->received_order_id,
                            true
                        );
                        $mailSubject = $this->processContent(
                            $data->mail_subject,
                            $data->payment_method,
                            $data->received_order_id,
                            false
                        );
                        if (!empty($data->signature_content)) {
                            $mailContent = $mailContent . PHP_EOL . $data->signature_content;
                        }
                        $arrKey = [
                            'receive_order_id'    => $data->received_order_id,
                            'order_status_id'     => $data->order_status_id,
                            'order_sub_status_id' => $data->order_sub_status_id,
                        ];
                        $arrData = [
                            'payment_method'      => $data->payment_method,
                            'mall_id'             => $data->mall_id,
                            'mail_id'             => $data->mail_id,
                            'mail_subject'        => $mailSubject,
                            'mail_content'        => $mailContent,
                            'attached_file_path'  => $data->attached_file_path,
                            'send_status'         => 0,
                            'mail_to'             => $data->email,
                            'mail_cc'             => '',
                            'mail_bcc'            => '',
                            'mail_from'           => $data->mall_id === 1 ? 'rakuten@diy-tool.com' : '',
                            'error_code'          => '',
                            'error_message'       => '',
                            'in_date'             => now(),
                            'in_ope_cd'           => 'OPE99999',
                            'up_date'             => now(),
                            'up_ope_cd'           => 'OPE99999',
                        ];
                        $arrInsert = array_merge($arrKey, $arrData);
                        $modelSML->insertIgnore($arrInsert);
                        if (count($arrUpdate) !== 0) {
                            if ($arrUpdate['flg_payment']) {
                                $modelPA->where(['name' => $arrUpdate['name']])
                                    ->update(['current_account' => $arrUpdate['current_account']]);
                            }
                            $modelO->updateData(
                                [$arrUpdate['receive_id']],
                                [
                                    'payment_account'      => $arrUpdate['payment_account'],
                                    'payment_request_date' => $arrUpdate['payment_request_date'],
                                ]
                            );
                        }
                        $modelO->updateData(array($data->receive_id), ['is_mail_sent' => 1]);
                    } else {
                        if ($data->is_sourcing_on_demand === 0 ||
                            ($data->order_status !== ORDER_STATUS['ORDER']
                            && $data->order_sub_status !==  ORDER_SUB_STATUS['DOING'])) {
                            $modelO->updateData(array($data->receive_id), ['is_mail_sent' => 1]);
                        }
                    }
                    $success++;
                } catch (\Exception $e) {
                    $this->error[] = Utilities::checkMessageException($e);
                    $error  = "------------------------------------------" . PHP_EOL;
                    $error .= basename(__CLASS__) . PHP_EOL;
                    $error .= Utilities::checkMessageException($e);
                    $error .= PHP_EOL . "------------------------------------------" . PHP_EOL;
                    $slack->notify(new SlackNotification($error));
                    Log::error(Utilities::checkMessageException($e));
                    print_r("$error");
                    $fail++;
                }
            }
            Log::info("Process success: $success and fail: $fail records.");
            print_r("Process success: $success and fail: $fail records." . PHP_EOL);
        } else {
            Log::info('No data');
            print_r("No data" . PHP_EOL);
        }
        Event::fire(new eCommand($this->signature, array('end' => true, 'error' => $this->error)));
        $totalTime = round(microtime(true) - $start, 2);
        Log::info("End batch process mail queue with total time: $totalTime s.");
        print_r("End batch process mail queue with total time: $totalTime s.");
    }

    /**
     * Process content email.
     * @param $mailContent string content can process
     * @param $paymentCode int payment menthod code
     * @param $receivedOrderId string order id
     * @return list array
     */
    public static function processContent($mailContent, $paymentCode, $receivedOrderId, $check)
    {
        $modelO   = new MstOrder();
        $modelSM  = new MstSettlementManage();
        $modelPA  = new MstPaymentAccount();
        $arrParas = [];
        preg_match_all("/\[###[^\]]*\]/", $mailContent, $arrParas);
        $arrReplace     = [];
        $datas          = $modelO->getInfoOrderMail($receivedOrderId)->toArray();
        $arrUpdate = [];
        foreach ($arrParas[0] as $para) {
            $subContent = '';
            if ($para === '[###決済方法別テンプレ]') {
                $dataCase = $modelSM->getDataByPaymentCode($paymentCode);
                if ($dataCase->have_para === 1) {
                    $arrSubParas = [];
                    preg_match_all("/\[###[^\]]*\]/", $dataCase->add_2_mail_text, $arrSubParas);
                    $arrSubReplace = [];
                    foreach ($arrSubParas[0] as $arrSubPara) {
                        if ($paymentCode === 6) {
                            $arrUpdate = [
                                'flg_payment'          => false,
                                'receive_id'           => $datas['receive_id'],
                                'payment_account'      => '1339284',
                                'payment_request_date' => date("Y/m/d"),
                            ];
                            $datas['payment_account'] = '1339284';
                        } elseif ($arrSubPara === '[###入金口座]') {
                            if (!empty($datas['payment_account'])) {
                                $paymentAccount = $datas['payment_account'];
                            } else {
                                $paymentAccount = 0;
                                $arrAcc = $modelPA->first();
                                if ($arrAcc->current_account >= $arrAcc->min_number_account &&
                                    $arrAcc->current_account < $arrAcc->max_number_account) {
                                    $paymentAccount = $arrAcc->current_account + 1;
                                } elseif ($arrAcc->current_account === $arrAcc->max_number_account) {
                                    $paymentAccount = $arrAcc->min_number_account;
                                }
                                $arrUpdate = [
                                    'flg_payment'          => true,
                                    'name'                 => $arrAcc->name,
                                    'current_account'      => $paymentAccount,
                                    'receive_id'           => $datas['receive_id'],
                                    'payment_account'      => $paymentAccount,
                                    'payment_request_date' => date("Y/m/d"),
                                ];
                            }
                            $datas['payment_account'] = $paymentAccount;
                        }
                        $arrSubReplace[] = view('template-mail/template', [
                                            'param' => $arrSubPara,
                                            'datas' => $datas
                                        ])->render();
                    }
                    $subContent = str_replace($arrSubParas[0], $arrSubReplace, $dataCase->add_2_mail_text);
                } else {
                    $subContent = $dataCase->add_2_mail_text;
                }
            } elseif ($para === '[###送付先情報]') {
                $dataDetail  = $modelO->getInfoOrderDetail($receivedOrderId)->toArray();
                $contentTemp = '';
                $countDt     = count($dataDetail);
                $newdData    = [];
                $countChild  = 0;
                $countChild2 = 0;
                foreach ($dataDetail as $key => $value) {
                    $deliveryType = $value['delivery_type'];
                    $suplierId    = $value['suplier_id'];
                    if (in_array($deliveryType, [1, 3])) {
                        $newdData[1][$value['receiver_id']]['ship_last_name']     = $value['ship_last_name'];
                        $newdData[1][$value['receiver_id']]['ship_first_name']    = $value['ship_first_name'];
                        $newdData[1][$value['receiver_id']]['ship_zip_code']      = $value['ship_zip_code'];
                        $newdData[1][$value['receiver_id']]['ship_prefecture']    = $value['ship_prefecture'];
                        $newdData[1][$value['receiver_id']]['ship_city']          = $value['ship_city'];
                        $newdData[1][$value['receiver_id']]['ship_sub_address']   = $value['ship_sub_address'];
                        $newdData[1][$value['receiver_id']]['ship_tel_num']       = $value['ship_tel_num'];
                        $newdData[1][$value['receiver_id']]['child'][$countChild] = [
                            'product_code'  => $value['product_code'],
                            'product_name'  => $value['product_name'],
                            'price'         => $value['price'],
                            'quantity'      => $value['quantity'],
                            'delivery_type' => $value['delivery_type']
                        ];
                        $countChild++;
                    } else {
                        $newdData[2][$value['receiver_id']]['ship_last_name']     = $value['ship_last_name'];
                        $newdData[2][$value['receiver_id']]['ship_first_name']    = $value['ship_first_name'];
                        $newdData[2][$value['receiver_id']]['ship_zip_code']      = $value['ship_zip_code'];
                        $newdData[2][$value['receiver_id']]['ship_prefecture']    = $value['ship_prefecture'];
                        $newdData[2][$value['receiver_id']]['ship_city']          = $value['ship_city'];
                        $newdData[2][$value['receiver_id']]['ship_sub_address']   = $value['ship_sub_address'];
                        $newdData[2][$value['receiver_id']]['ship_tel_num']       = $value['ship_tel_num'];
                        $newdData[2][$value['receiver_id']]['child'][$countChild2] = [
                            'product_code'  => $value['product_code'],
                            'product_name'  => $value['product_name'],
                            'price'         => $value['price'],
                            'quantity'      => $value['quantity'],
                            'delivery_type' => $value['delivery_type']
                        ];
                        $countChild2++;
                    }
                }
                $j = 0;
                if (count($newdData) > 0) {
                    $newdData1 = [];
                    if (isset($newdData[1])) {
                        $newdData1[] = $newdData[1];
                        unset($newdData[1]);
                    }

                    array_multisort(array_map('count', $newdData), SORT_ASC, $newdData);
                    krsort($newdData);
                    $finalArray = array_merge($newdData1, $newdData);
                    foreach ($finalArray as $key => $value) {
                        foreach ($value as $item) {
                            $j++;
                            $contentTemp .= view('template-mail/template', [
                                'param' => $para,
                                'datas' => $item,
                                'index' => $j,
                            ])->render();
                        }
                    }
                }
                $subContent = $contentTemp;
            // Process order not in dt_order_detail incase Mail thanks
            } elseif ($para === '[###送付先情報感謝]') {
                $dataDetail  = $modelO->getInfoNotInOrderDetail($receivedOrderId)->toArray();
                $newdData = [];
                foreach ($dataDetail as $key => $value) {
                    $newdData[1]['ship_last_name']     = $value['ship_last_name'];
                    $newdData[1]['ship_first_name']    = $value['ship_first_name'];
                    $newdData[1]['ship_zip_code']      = $value['ship_zip_code'];
                    $newdData[1]['ship_prefecture']    = $value['ship_prefecture'];
                    $newdData[1]['ship_city']          = $value['ship_city'];
                    $newdData[1]['ship_sub_address']   = $value['ship_sub_address'];
                    $newdData[1]['ship_tel_num']       = $value['ship_tel_num'];
                    $newdData[1]['child'][] = [
                        'product_code'  => $value['product_code'],
                        'product_name'  => $value['product_name'],
                        'price'         => $value['price'],
                        'quantity'      => $value['quantity']
                    ];
                }
                if (count($newdData) > 0) {
                    $subContent = view('template-mail/template', [
                        'param' => $para,
                        'datas' => $newdData[1]
                    ])->render();
                }
            } elseif ($para === '[###送付先情報欠品・廃番]') {
                $dataDetail   = $modelO->getOrderDetailMailQueue($receivedOrderId, $para)->toArray();
                $dataDetailNew = [];
                foreach ($dataDetail as $key => $value) {
                    $dataDetailNew[$value['receiver_id']][] = $value;
                }
                $subContent = view('template-mail/template', [
                                    'param' => $para,
                                    'datas' => $dataDetailNew
                                ])->render();
            } elseif ($para === '[###送付先情報発送日案内]') {
                $dataDetails   = $modelO->getOrderDetailMailQueue($receivedOrderId, $para)->toArray();
                $dataOrders = [];
                foreach ($dataDetails as $dataDetail) {
                    if ($dataDetail['delivery_type'] === 1 || $dataDetail['delivery_type'] === 3) {
                        $key = '13';
                    } else {
                        $key = '2';
                    }
                    $dataOrders[$key][$dataDetail['receiver_id']]['ship_last_name']   = $dataDetail['ship_last_name'];
                    $dataOrders[$key][$dataDetail['receiver_id']]['ship_first_name']  = $dataDetail['ship_first_name'];
                    $dataOrders[$key][$dataDetail['receiver_id']]['ship_zip_code']    = $dataDetail['ship_zip_code'];
                    $dataOrders[$key][$dataDetail['receiver_id']]['ship_prefecture']  = $dataDetail['ship_prefecture'];
                    $dataOrders[$key][$dataDetail['receiver_id']]['ship_city']        = $dataDetail['ship_city'];
                    $dataOrders[$key][$dataDetail['receiver_id']]['ship_sub_address'] = $dataDetail['ship_sub_address'];
                    $dataOrders[$key][$dataDetail['receiver_id']]['ship_tel_num']     = $dataDetail['ship_tel_num'];
                    $dataOrders[$key][$dataDetail['receiver_id']]['delivery_plan_date']
                        = $dataDetail['delivery_plan_date'];
                    $dataOrders[$key][$dataDetail['receiver_id']]['delivery_date']
                        = $dataDetail['delivery_date'];
                    $dataOrders[$key][$dataDetail['receiver_id']]['delivery_type']
                        = $dataDetail['delivery_type'];
                    $productCode = $dataDetail['product_code'];
                    $dataOrders[$key][$dataDetail['receiver_id']]['product'][$productCode]['product_code']
                        = $productCode;
                    $dataOrders[$key][$dataDetail['receiver_id']]['product'][$productCode]['product_name']
                        = $dataDetail['product_name'];
                    $dataOrders[$key][$dataDetail['receiver_id']]['product'][$productCode]['price']
                        = $dataDetail['price'];
                    $dataOrders[$key][$dataDetail['receiver_id']]['product'][$productCode]['quantity']
                        = $dataDetail['quantity'];
                }
                $subContent = view('template-mail/template', [
                                    'param' => $para,
                                    'datas' => $dataOrders
                                ])->render();
            } else {
                $subContent = view('template-mail/template', [
                                        'param' => $para,
                                        'datas' => $datas
                                    ])->render();
            }

            $arrCheckSubParas = [];
            preg_match_all("/\[###[^\]]*\]/", $subContent, $arrCheckSubParas);
            if (count($arrCheckSubParas[0]) !== 0) {
                $arrSubReplace = [];
                foreach ($arrCheckSubParas[0] as $subPara) {
                    $arrSubReplace = view('template-mail/template', [
                        'param' => $subPara,
                        'datas' => $datas
                    ])->render();
                }
                $subContent = str_replace($arrCheckSubParas[0], $arrSubReplace, $subContent);
            }
            $arrReplace[] = $subContent;
        }
        if ($check) {
            return [str_replace($arrParas[0], $arrReplace, $mailContent), $arrUpdate];
        } else {
            return str_replace($arrParas[0], $arrReplace, $mailContent);
        }
    }
}
