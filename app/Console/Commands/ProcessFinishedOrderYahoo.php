<?php
/**
 * Call API process finish order
 *
 * @package    App\Console\Commands
 * @subpackage ProcessFinishedOrder
 * @copyright  Copyright (c) 2017 RiverCrane! Corporation. All Rights Reserved.
 * @author     Truong Nghia<truong.van.nghia@rivercrane.vn>
 */
namespace App\Console\Commands;


use Illuminate\Console\Command;
use App\Events\Command as eCommand;
use Illuminate\Support\Facades\Log;
use App\Models\YahooOrder\MstOrder;
use Event;
use App;
use Config;
use Storage;
use App\Helpers\Helper;
use App\Models\Batches\YahooToken;
use App\Notifications\SlackNotification;
use App\Notification;

class ProcessFinishedOrderYahoo extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'process:finish-order-yahoo';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Call API process finish order yahoo';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * The error.
     *
     * @var array
     */
    public $error = [];

    /**
     * The notification.
     *
     * @var object
     */
    protected $slack;

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //Create log
        // $arrayReplace = [':', '-'];
        // $folder       = str_replace($arrayReplace, '_', $this->signature);
        // Log::useDailyFiles(storage_path() . "/logs/$folder/history.log");
        // $this->slack = new Notification(CHANNEL['horunba']);
        // //Process data
        // Event::fire(new eCommand($this->signature, array('start' => true)));
        // Log::info('Start batch finish order yahoo.');
        // print_r("Start batch finish order yahoo." . PHP_EOL);
        // $start       = microtime(true);
        // $modelO = new MstOrder();
        // $yahTotal = 0;
        // $dataProcessIPI = $modelO->getDataFinishOrderAPI();
        // $countApi = count($dataProcessIPI);
        // if ($countApi === 0) {
        //     Log::info('No data process Api.');
        //     print_r("No data process Api." . PHP_EOL);
        // } else {
        //     $yahSucc = 0;
        //     foreach ($dataProcessIPI as $item) {
        //         $yahSucc += $this->changeStatusYahoo($item->received_order_id);
        //     }
        //     Log::info("Processed finish order Yahoo : $yahTotal order and success: $yahSucc order.");
        //     print_r("Processed finish order Yahoo : $yahTotal order and success: $yahSucc order." . PHP_EOL);
        // }
        // Event::fire(new eCommand($this->signature, array('end' => true, 'error' => $this->error)));
        // $totalTime = round(microtime(true) - $start, 2);
        // Log::info("End batch finish order with total time: $totalTime s.");
        // print_r("End batch finish order with total time: $totalTime s.");
    }



    /**
     * Finish order for mall Yahoo
     *
     * @param  array    $orderid list received_order_id
     * @param  boolean  $isCallAPI
     * @return mixed
     */
    public function changeStatusYahoo($orderid)
    {
        $modelO = new MstOrder();
        $yahooToken  = new YahooToken();
        try {
            $accessToken = $yahooToken->find('order')->access_token;
            $yahooHeader = array(
                "Authorization:Bearer " . $accessToken,
            );
            $yahooUrl = 'https://circus.shopping.yahooapis.jp/ShoppingWebService/V1/orderStatusChange';
            $requestXml = view('api.xml.yahoo_finish_order', [
                'orderId'  => $orderid,
                'sellerId' => 'diy-tool',
                'isPointFix' => true,
                'operationUser' => 'OPE99999',
            ])->render();
            $ch = curl_init($yahooUrl);
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_HTTPHEADER, $yahooHeader);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $requestXml);
            $responseXml = curl_exec($ch);
            curl_close($ch);
            $responData  = simplexml_load_string($responseXml);

            if ($responData->getName() === 'Error') {
                $errorCode = Helper::val($responData->Code);
                $message = Helper::val($responData->Message);
                $arrUpdate = [
                    'order_sub_status' => ORDER_SUB_STATUS['DOING'],
                    'error_code_api'   => $errorCode,
                    'message_api'      => $message,
                    'up_ope_cd'        => 'OPE99999',
                    'up_date'          => now(),
                ];
                $modelO->updateDataByReceiveOrder([$orderid], $arrUpdate);
                return 0;
            } else {
                $arrUpdate = [
                    'order_sub_status' => ORDER_SUB_STATUS['DOING'],
                    'error_code_api'   => 'OK',
                    'message_api'      => 'OK',
                    'up_ope_cd'        => 'OPE99999',
                    'up_date'          => now(),
                ];
                $modelO->updateDataByReceiveOrder([$orderid], $arrUpdate);
                return 1;
            }
        } catch (\Exception $e) {
            $message = "Can't update status is finish.";
            print_r($message . PHP_EOL);
            Log::error($message);
            report($e);
            return 0;
        }
    }
}
