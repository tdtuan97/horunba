<?php
/**
 * Batch process cancel order
 *
 * @package    App\Console\Commands
 * @subpackage ProcessCancelOrder
 * @copyright  Copyright (c) 2018 RiverCrane! Corporation. All Rights Reserved.
 * @author     Van Qui<van.qui@rivercrane.vn>
 */

namespace App\Console\Commands;

include app_path('lib/MarketplaceWebService/autoload.php');

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;
use App\Notifications\SlackNotification;
use App\Custom\Utilities;
use App\Notification;
use App\Events\Command as eCommand;
use Event;
use Config;
use Helper;
use DB;
use App;
use MarketplaceWebService_Client;
use MarketplaceWebService_Model_SubmitFeedRequest;
use MarketplaceWebService_Model_GetFeedSubmissionListRequest;
use MarketplaceWebService_Model_GetFeedSubmissionResultRequest;
use MarketplaceWebService_Exception;
use App\Models\Batches\MstOrder;
use App\Models\Batches\DtPaymentList;
use App\Models\Batches\YahooToken;
use App\Models\Batches\MstRakutenKey;

class ProcessCancelOrder extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'process:cancel-order';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Process cancel order';

    /**
     * The key rakuten
     *
     * @var string
     */
    protected $keyRakuten = '';

    /**
     * The notification.
     *
     * @var object
     */
    protected $slack;

    /**
     * The error.
     *
     * @var array
     */
    public $error = [];

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //Create log
        $arrayReplace = [':', '-'];
        $folder       = str_replace($arrayReplace, '_', $this->signature);
        Log::useDailyFiles(storage_path() . "/logs/$folder/history.log");
        //Process data
        Event::fire(new eCommand($this->signature, array('start' => true)));
        Log::info('Start batch process cancel order.');
        print_r("Start batch process cancel order." . PHP_EOL);
        $start       = microtime(true);
        $modelRakutenKey  = new MstRakutenKey();
        $this->keyRakuten = $modelRakutenKey->getKeyRakuten();
        $this->slack = new Notification(CHANNEL['horunba']);

        Log::info('Start process update cancel.');
        print_r("Start process update cancel." . PHP_EOL);
        // $this->processUpdateCancel();
        Log::info('End process update cancel.');
        print_r("End process update cancel." . PHP_EOL);

        Log::info('Start process update cancel api.');
        print_r("Start process update cancel api." . PHP_EOL);
        $this->processUpdateCancelAPI();
        Log::info('End process update cancel api.');
        print_r("End process update cancel api." . PHP_EOL);

        Event::fire(new eCommand($this->signature, array('end' => true, 'error' => $this->error)));
        $totalTime = round(microtime(true) - $start, 2);
        Log::info("End batch process cancel order with total time: $totalTime s.");
        print_r("End batch process cancel order with total time: $totalTime s.");
    }

    /**
     * Process update cancel
     *
     * @return void
     */
    private function processUpdateCancel()
    {
        try {
            $modelO = new MstOrder();
            $result = $modelO->getCancelOrderData();
            if ($result->count() === 0) {
                $message = "No data.";
                Log::info($message);
                print_r($message . PHP_EOL);
                return;
            }
            $totalSuccess = 0;
            foreach ($result as $item) {
                $receiveId    = $item->receive_id;
                $totalPrice   = empty($item->total_price) ? 0 : $item->total_price;
                $shipCharge   = empty($item->ship_charge) ? 0 : $item->ship_charge;
                $requestPrice = empty($item->request_price) ? 0 : $item->request_price;
                $usedPoint    = empty($item->used_point) ? 0 : $item->used_point;
                $cancelPrice  = empty($item->cancel_price) ? 0 : $item->cancel_price;
                $belowLP      = empty($item->below_limit_price) ? 0 : $item->below_limit_price;
                $chargePrice  = empty($item->charge_price) ? 0 : $item->charge_price;

                if ($totalPrice - $cancelPrice < $belowLP) {
                    if ($shipCharge === 0) {
                        $shipCharge = $chargePrice;
                    }
                    if ($cancelPrice - $shipCharge >= $requestPrice) {
                        $requestPrice = 0;
                        $usedPoint    = $usedPoint - ($cancelPrice - $requestPrice - $shipCharge);
                        $totalPrice   = $totalPrice - ($cancelPrice - $shipCharge);
                    } else {
                        $requestPrice = $requestPrice - ($cancelPrice - $shipCharge);
                        $totalPrice   = $totalPrice - ($cancelPrice - $shipCharge);
                    }
                }

                $dataUpdate = [
                    'ship_charge'    => $shipCharge,
                    'request_price'  => $requestPrice,
                    'used_point'     => $usedPoint,
                    'total_price'    => $totalPrice,
                    'is_mall_update' => 1,
                    'up_ope_cd'      => 'OPE99999',
                    'up_date'        => now()
                ];

                if ($modelO->updateData([$receiveId], $dataUpdate)) {
                    $totalSuccess++;
                }
            }
            $message = "Update success $totalSuccess records to mst_order.";
            Log::info($message);
            print_r($message . PHP_EOL);
        } catch (\Exception $e) {
            $message = "Can't process update cancel";
            print_r($message . PHP_EOL);
            Log::error($message);
            report($e);
        }
    }

    /**
     * Process update cancel api
     *
     * @return void
     */
    private function processUpdateCancelAPI()
    {
        $modelO = new MstOrder();
        $result = $modelO->getCancelOrderAPI()->toArray();
        if (count($result) === 0) {
            $message = "No data.";
            Log::info($message);
            print_r($message . PHP_EOL);
            return;
        }
        $isCallAPI = true;
        if (App::environment(['local', 'test'])) {
            $isCallAPI = false;
        }

        $rakOrderCancels = array();
        $azOrderCancels  = array();
        $yahOrderCancels = array();
        $honOrderCancels = array();
        $payOrderCancels = array();
        $wowOrderCancels = array();
        $honOrderUpdate  = array();
        $arrAzTotal      = array();
        $totalYah        = 0;
        $totalRakPay     = 0;
        $totalWowma      = 0;
        $message = "Start process cancel Yahoo.";
        Log::info($message);
        print_r($message . PHP_EOL);
        foreach ($result as $item) {
            if (preg_match('/^re/', $item['received_order_id'])) {
                $modelO->updateDataByReceiveOrder(
                    [$item['received_order_id']],
                    [
                        'error_code_api' => 'OK',
                        'message_api'    => 'OK',
                        'is_mall_cancel' => 1,
                        'up_ope_cd'      => 'OPE99999',
                        'up_date'        => now(),
                    ]
                );
                continue;
            }
            if ($item['mall_id'] === 2) {
                if (!in_array($item['received_order_id'], $yahOrderCancels)) {
                    $this->cancelYahoo($item, $isCallAPI);
                    $totalYah++;
                    $yahOrderCancels[] = $item['received_order_id'];
                }
            } elseif ($item['mall_id'] === 1) {
                $rakOrderCancels["".$item['received_order_id']] = $item;
            } elseif (($item['mall_id'] === 3)
                 && (strtotime($item['up_date']) <= strtotime(date('Ymd')." -4 days"))) {
                if (!in_array($item['received_order_id'], $arrAzTotal)) {
                    $arrTmp       = array();
                    $arrAzTotal[] = $item['received_order_id'];
                }
                if (!empty($item['order_item_code'])) {
                    $arrTmp[] = [
                        'order_item_code' => $item['order_item_code'],
                        'cancel_reason'   => 'NoInventory'
                    ];
                }
                $azOrderCancels["".$item['received_order_id']] = [
                    'receive_id'        => $item['receive_id'],
                    'received_order_id' => $item['received_order_id'],
                    'order_item'        => $arrTmp,
                    'mall_id'           => $item['mall_id'],
                    'payment_method'    => $item['payment_method'],
                    'payment_status'    => $item['payment_status'],
                    'status_code'       => 'Failure'
                ];
            } elseif ($item['mall_id'] === 8) {
                $honOrderCancels[$item['received_order_id']] = [
                    'id' => str_replace('DIY-', '', $item['received_order_id']),
                    'status' => ORDER_STATUS['CANCEL'],
                ];
                $honOrderUpdate[] = $item['received_order_id'];
            } elseif ($item['mall_id'] === 9) {
                if (!in_array($item['received_order_id'], $payOrderCancels)) {
                    $totalRakPay += $this->cancelRakutenPay($item['received_order_id'], $item['rakuten_reason']);
                    $payOrderCancels[] = $item['received_order_id'];
                }
            } elseif ($item['mall_id'] === 10) {
                if (!in_array($item['received_order_id'], $wowOrderCancels)) {
                    $totalWowma += $this->cancelWowma(
                        $item['received_order_id'],
                        $item['wowma_reason'],
                        $item['wowma_reason_comment']
                    );
                    $modelO->updateData([$item['receive_id']], [
                        'is_mall_cancel' => 1,
                        'order_sub_status' => ORDER_SUB_STATUS['DONE']
                    ]);
                    $wowOrderCancels[] = $item['received_order_id'];
                }
            }
        }
        if ($totalYah > 0) {
            $message = "Process $totalYah Yahoo order cancel.";
            Log::info($message);
            print_r($message . PHP_EOL);
        } else {
            $message = "No data.";
            Log::info($message);
            print_r($message . PHP_EOL);
        }
        $message = "End process cancel Yahoo.";
        Log::info($message);
        print_r($message . PHP_EOL);

        //process rakuten
        $message = "Start process cancel Rakuten.";
        Log::info($message);
        print_r($message . PHP_EOL);
        if (count($rakOrderCancels) > 0) {
            $rakOrderCancels = array_chunk($rakOrderCancels, 500);
            $totalRak = 0;
            foreach ($rakOrderCancels as $rakOrderCancel) {
                $requestId = $this->getRequestId($isCallAPI);
                if ($requestId !== false) {
                    $this->cancelRakuten($rakOrderCancel, $requestId, $isCallAPI);
                    $totalRak += count($rakOrderCancel);
                } else {
                    break;
                }
            }
            if ($totalRak > 0) {
                $message = "Process $totalRak Rakuten order cancel.";
                Log::info($message);
                print_r($message . PHP_EOL);
            }
        } else {
            $message = "No data.";
            Log::info($message);
            print_r($message . PHP_EOL);
        }
        $message = "End process cancel Rakuten.";
        Log::info($message);
        print_r($message . PHP_EOL);

        //process amazon
        $message = "Start process cancel Amazon.";
        Log::info($message);
        print_r($message . PHP_EOL);
        if (count($azOrderCancels) > 0) {
            $azOrderCancels = array_chunk($azOrderCancels, 500);
            $totalAz = 0;
            foreach ($azOrderCancels as $azOrderCancel) {
                $this->cancelAmazon($azOrderCancel, $isCallAPI);
                $totalAz += count($azOrderCancel);
            }
            if (count($totalAz) > 0) {
                $message = "Process $totalAz Amazon order cancel.";
                Log::info($message);
                print_r($message . PHP_EOL);
            }
        } else {
            $message = "No data.";
            Log::info($message);
            print_r($message . PHP_EOL);
        }
        $message = "End process cancel Amazon.";
        Log::info($message);
        print_r($message . PHP_EOL);

        //process honten api
        $message = "Start process cancel new Honten.";
        Log::info($message);
        print_r($message . PHP_EOL);
        if (count($honOrderCancels) > 0) {
            $steps = array_chunk($honOrderCancels, 500);
            $stepsUpdate = array_chunk($honOrderUpdate, 500);
            $totalHon = 0;
            foreach ($steps as $k => $step) {
                $this->cancelNewHonten($step, $stepsUpdate[$k], $isCallAPI);
                $totalHon += count($step);
            }
            if (count($totalHon) > 0) {
                $message = "Process $totalHon honten(PRO) order cancel.";
                Log::info($message);
                print_r($message . PHP_EOL);
            }
        } else {
            $message = "No data.";
            Log::info($message);
            print_r($message . PHP_EOL);
        }

        //Process rakuten pay
        $message = "Start process cancel Rakuten pay.";
        Log::info($message);
        print_r($message . PHP_EOL);
        if ($totalRakPay > 0) {
            $message = "Process $totalRakPay Rakuten pay order cancel.";
        } else {
            $message = "No data.";
        }
        Log::info($message);
        print_r($message . PHP_EOL);
        $message = "End process cancel Rakuten pay.";
        Log::info($message);
        print_r($message . PHP_EOL);

        //Process Wowma
        $message = "Start process cancel Wowma.";
        Log::info($message);
        print_r($message . PHP_EOL);
        if ($totalWowma > 0) {
            $message = "Process $totalWowma order cancel.";
        } else {
            $message = "No data.";
        }
        Log::info($message);
        print_r($message . PHP_EOL);
        $message = "End process cancel Wowma.";
        Log::info($message);
        print_r($message . PHP_EOL);
    }

    /**
     * Get request id
     *
     * @param  boolean $isCallAPI
     * @return boolean
     */
    public function getRequestId($isCallAPI)
    {
        try {
            if (!$isCallAPI) {
                return '000000';
            }

            $rakAuth   = Config::get('common.rak_auth_key');
            $rakUrl    = "https://api.rms.rakuten.co.jp/es/1.0/order/ws";
            $rakHeader = array(
                "Content-Type: text/xml;charset=UTF-8",
            );
            $requestXml = view('api.xml.rakuten_get_request', [
                'authKey'   => $rakAuth,
                'shopUrl'   => 'tuzukiya',
                'userName'  => 'tuzukiya',
            ])->render();
            $ch = curl_init($rakUrl);
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_HTTPHEADER, $rakHeader);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $requestXml);
            $responseXml = curl_exec($ch);
            curl_close($ch);
            $responseXml = str_ireplace(['S:', 'ns2:'], '', $responseXml);
            $responData  = simplexml_load_string($responseXml);
            if (!empty($responData) &&
                Helper::val($responData->Body->getRequestIdResponse->return->errorCode) === 'N00-000') {
                return Helper::val($responData->Body->getRequestIdResponse->return->requestId);
            } else {
                return false;
            }
        } catch (\Exception $e) {
            $message = "Can't get request ID";
            print_r($message . PHP_EOL);
            Log::error($message);
            report($e);
            return false;
        }
    }

    /**
     * Call Api Yahoo cancel
     *
     * @param array   $yahOrder
     * @param boolean $isCallAPI
     * @return int
     */
    public function cancelYahoo($yahOrder, $isCallAPI)
    {
        $modelO     = new MstOrder();
        $modelPL    = new DtPaymentList();
        $yahooToken = new YahooToken();
        try {
            if ($isCallAPI) {
                $accessToken = $yahooToken->find('order')->access_token;
                $yahooHeader = array(
                    "Authorization:Bearer " . $accessToken,
                );
                $yahooUrl = 'https://circus.shopping.yahooapis.jp/ShoppingWebService/V1/orderStatusChange';
                $requestXml = view('api.xml.yahoo_cancel_order', [
                    'orderId'            => $yahOrder['received_order_id'],
                    'cancelReason'       => $yahOrder['yahoo_reason'],
                    'cancelReasonDetail' => $yahOrder['yahoo_reason_comment'],
                    'sellerId'           => 'diy-tool',
                    'isPointFix'         => false,
                    'operationUser'      => 'OPE99999',
                ])->render();
                $ch = curl_init($yahooUrl);
                curl_setopt($ch, CURLOPT_POST, true);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($ch, CURLOPT_HTTPHEADER, $yahooHeader);
                curl_setopt($ch, CURLOPT_POSTFIELDS, $requestXml);
                $responseXml = curl_exec($ch);
                curl_close($ch);
                $responData  = simplexml_load_string($responseXml);
            }

            if ($isCallAPI === true && $responData->getName() === 'Error') {
                $errorCode = Helper::val($responData->Code);
                $message = Helper::val($responData->Message);
                $isMallCancel = 1;
                if (strpos($message, 'error="invalid_token"') !== false) {
                    $isMallCancel = 0;
                }
                $modelO->updateDataByReceiveOrder(
                    [$yahOrder['received_order_id']],
                    [
                        'error_code_api' => $errorCode,
                        'message_api'    => $message,
                        'is_mall_cancel' => $isMallCancel,
                        'up_ope_cd'      => 'OPE99999',
                        'up_date'        => now(),
                    ]
                );
            } else {
                $modelO->updateDataByReceiveOrder(
                    [$yahOrder['received_order_id']],
                    [
                        'error_code_api' => 'OK',
                        'message_api'    => 'OK',
                        'is_mall_cancel' => 1,
                        'up_ope_cd'      => 'OPE99999',
                        'up_date'        => now(),
                    ]
                );
            }
            if ($yahOrder['payment_method'] === 2 && $yahOrder['payment_status'] === 1) {
                $modelPL->updateData(
                    ['receive_order_id' => $yahOrder['received_order_id']],
                    [
                        'need_repayment' => 1,
                        'up_ope_cd'      => 'OPE99999',
                        'up_date'        => now()
                    ]
                );
            }
        } catch (\Exception $e) {
            $message = "Can't update status is finish.";
            print_r($message . PHP_EOL);
            Log::error($message);
            report($e);
            return 0;
        }
    }

    /**
     * Cancel rakuten api
     *
     * @param  array   $rakOrderCancel
     * @param  string  $requestId
     * @param  boolean $isCallAPI
     * @return void
     */
    private function cancelRakuten($rakOrderCancel, $requestId, $isCallAPI)
    {
        try {
            $modelPL   = new DtPaymentList();
            $modelO    = new MstOrder();
            if ($isCallAPI) {
                $rakAuth   = Config::get('common.rak_auth_key');
                $rakUrl    = "https://api.rms.rakuten.co.jp/es/1.0/order/ws";
                $rakHeader = array(
                    "Content-Type: text/xml;charset=UTF-8",
                );
                $requestXml = view('api.xml.rakuten_cancel_order', [
                    'authKey'        => $rakAuth,
                    'shopUrl'        => 'tuzukiya',
                    'userName'       => 'tuzukiya',
                    'requestId'      => $requestId,
                    'rakOrderCancel' => $rakOrderCancel
                ])->render();
                $ch = curl_init($rakUrl);
                curl_setopt($ch, CURLOPT_POST, true);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($ch, CURLOPT_HTTPHEADER, $rakHeader);
                curl_setopt($ch, CURLOPT_POSTFIELDS, $requestXml);
                $responseXml = curl_exec($ch);
                curl_close($ch);
                $responseXml = str_ireplace(['S:', 'ns2:'], '', $responseXml);
                $responData  = simplexml_load_string($responseXml);
                $returnData  = $responData->Body->cancelOrderResponse->return;
                $errorCode   = Helper::val($returnData->errorCode);
                $errorMess   = Helper::val($returnData->message);
                $arrError    = array();
                if ($errorCode !== "N00-000") {
                    foreach ($returnData->unitError as $unitError) {
                        $arrError["".$unitError->orderKey] = [
                            'errorCode' => Helper::val($unitError->errorCode),
                            'message'   => Helper::val($unitError->message),
                        ];
                    }
                }
            } else {
                $errorCode = '';
                $errorMess = '';
            }

            foreach ($rakOrderCancel as $order) {
                $codeUpdate = $errorCode;
                $messUpdate = $errorMess;
                if (isset($arrError["".$order['received_order_id']])) {
                    $codeUpdate = $arrError["".$order['received_order_id']]['errorCode'];
                    $messUpdate = $arrError["".$order['received_order_id']]['message'];
                }
                if (in_array($order['payment_method'], [2, 6]) && $order['payment_status'] === 1) {
                    $modelPL->updateData(
                        ['receive_order_id' => $order['received_order_id']],
                        [
                            'need_repayment' => 1,
                            'up_ope_cd'      => 'OPE99999',
                            'up_date'        => now()
                        ]
                    );
                }
                $modelO->updateDataByReceiveOrder(
                    [$order['received_order_id']],
                    [
                        'request_id'     => $requestId,
                        'is_mall_cancel' => 1,
                        'error_code_api' => $codeUpdate,
                        'message_api'    => $messUpdate,
                        'up_ope_cd'      => 'OPE99999',
                        'up_date'        => now()
                    ]
                );
            }
        } catch (\Exception $e) {
            $message = "Call request id $requestId fail";
            print_r($message . PHP_EOL);
            Log::error($message);
            report($e);
        }
    }

    /**
     * Cancel amazon
     *
     * @param  array   $azOrderCancel
     * @param  boolean $isCallAPI
     * @return void
     */
    private function cancelAmazon($azOrderCancel, $isCallAPI)
    {
        $modelO     = new MstOrder();
        $receiveIds = array_column($azOrderCancel, 'receive_id');
        $dataUpdate = [
            'is_mall_cancel' => 1,
            'up_ope_cd'      => 'OPE99999',
            'up_date'        => now()
        ];
        if ($isCallAPI) {
            $feedType   = '_POST_ORDER_ACKNOWLEDGEMENT_DATA_';
            $config     = Config::get('amazonmws');
            $configHost = array(
                'ServiceURL'    => $config['ServiceURL'],
                'ProxyHost'     => $config['ProxyHost'],
                'ProxyPort'     => $config['ProxyPort'],
                'MaxErrorRetry' => $config['MaxErrorRetry'],
            );
            $service    = new MarketplaceWebService_Client(
                $config['AWS_ACCESS_KEY_ID'],
                $config['AWS_SECRET_ACCESS_KEY'],
                $configHost,
                $config['APPLICATION_NAME'],
                $config['APPLICATION_VERSION']
            );

            $params = [
                'data'               => $azOrderCancel,
                'merchantIdentifier' => $config['MERCHANT_ID']
            ];

            $feed = view('api.xml.amazon_cancel_order', $params)->render();

            //***************** SubmitFeed ****************
            $feedSubmissionInfo = $this->submitFeed($feedType, $feed, $config, $service);
            if ($feedSubmissionInfo['status'] === false) {
                $dataUpdate['message_api']    = $feedSubmissionInfo['data'];
                $dataUpdate['error_code_api'] = 'Fail';
                $modelO->updateData($receiveIds, $dataUpdate);
                print_r($feedSubmissionInfo['data'] . PHP_EOL);
                Log::error($feedSubmissionInfo['data']);
                $this->postSlackAmazon($feedSubmissionInfo['data']);
                return false;
            }

            if (!$feedSubmissionInfo['data']->isSetFeedSubmissionId()) {
                $dataUpdate['message_api']    = 'No FeedSubmissionId';
                $dataUpdate['error_code_api'] = 'Fail';
                $modelO->updateData($receiveIds, $dataUpdate);
                print_r("No FeedSubmissionId" . PHP_EOL);
                Log::error("No FeedSubmissionId");
                $this->postSlackAmazon("No FeedSubmissionId");
                return false;
            }
            $feedSubmissionId     = $feedSubmissionInfo['data']->getFeedSubmissionId();
            $feedSubmissionIdList = [$feedSubmissionId];
            //***************** GetFeedSubmissionList ****************
            //Do until DONE
            while (true) {
                sleep(80);
                $getFeedSubmissionListResult = $this->getFeedSubmissionList($feedSubmissionIdList, $config, $service);
                if ($getFeedSubmissionListResult['status'] === false) {
                    $dataUpdate['message_api']    = $getFeedSubmissionListResult['data'];
                    $dataUpdate['error_code_api'] = 'Fail';
                    $modelO->updateData($receiveIds, $dataUpdate);
                    print_r($getFeedSubmissionListResult['data'] . PHP_EOL);
                    Log::error($getFeedSubmissionListResult['data']);
                    $this->postSlackAmazon($getFeedSubmissionListResult['data']);
                    return false;
                }
                $feedSubmissionInfoList = $getFeedSubmissionListResult['data']->getFeedSubmissionInfoList();
                $feedProcessingStatus   = false;
                foreach ($feedSubmissionInfoList as $feedSubmissionInfo) {
                    if ($feedSubmissionInfo->isSetFeedProcessingStatus()) {
                        $feedProcessingStatus = $feedSubmissionInfo->getFeedProcessingStatus();
                        break;
                    }
                }
                if (!$feedProcessingStatus) {
                    $dataUpdate['message_api']    = 'No FeedProcessingStatus';
                    $dataUpdate['error_code_api'] = 'Fail';
                    $modelO->updateData($receiveIds, $dataUpdate);
                    print_r('No FeedProcessingStatus' . PHP_EOL);
                    Log::error('No FeedProcessingStatus');
                    $this->postSlackAmazon('No FeedProcessingStatus');
                    return false;
                }
                $arrStatusFinish = [
                    '_DONE_',
                    '_AWAITING_ASYNCHRONOUS_REPLY_',
                    '_CANCELLED_',
                    '_IN_SAFETY_NET_'
                ];

                if (in_array($feedProcessingStatus, $arrStatusFinish)) {
                    $dataUpdate['message_api']    = $feedProcessingStatus;
                    $dataUpdate['error_code_api'] = $feedProcessingStatus;
                    $modelO->updateData($receiveIds, $dataUpdate);
                    break;
                }
            }

            // ***************** GetFeedSubmissionResult ****************
            $getFeedSubmissionResultResult = $this->getFeedSubmissionResult($feedSubmissionId, $config, $service);
            if ($getFeedSubmissionResultResult['status'] === false) {
                $dataUpdate['message_api']    = $getFeedSubmissionResultResult['data'];
                $dataUpdate['error_code_api'] = 'Fail';
                $modelO->updateData($receiveIds, $dataUpdate);
                $this->postSlackAmazon($getFeedSubmissionResultResult['data']);
                return false;
            }
            $responseXML = $getFeedSubmissionResultResult['data'];
            if (!empty($responseXML->Message->ProcessingReport->Result)) {
                $errAzMess = [];
                foreach ($responseXML->Message->ProcessingReport->Result as $item) {
                    if ((string)$item->ResultCode === 'Error') {
                        $receiveId         = (string)$item->MessageID;
                        $resultDescription = (string)$item->ResultDescription;
                        $resultMessageCode = (string)$item->ResultMessageCode;
                        $dataUpdate['message_api']    = $resultDescription;
                        $dataUpdate['error_code_api'] = $resultMessageCode;
                        $modelO->updateData([$receiveId], $dataUpdate);
                        $errAzMess[] = "Receive ID $receiveId (Code $resultMessageCode): $resultDescription";
                    }
                }
                if (count($errAzMess) > 0) {
                    $this->postSlackAmazon(implode("\n", $errAzMess));
                }
            }
        } else {
            $dataUpdate['message_api']    = '_DONE_';
            $dataUpdate['error_code_api'] = '_DONE_';
            $modelO->updateData($receiveIds, $dataUpdate);
        }
        return true;
    }

    /**
     * Post message amazon to slack
     *
     * @param  string $message
     * @return void
     */
    public function postSlackAmazon($message)
    {
        $error  = "------------------------------------------" . PHP_EOL;
        $error .= 'Process cancel Amazon order fail:' . PHP_EOL;
        $error .= $message;
        $error .= PHP_EOL . "------------------------------------------" . PHP_EOL;
        $this->slack->notify(new SlackNotification($error));
    }

    /**
     * Submit feed
     *
     * @param  string   $feedType   Feed type
     * @param  string   $feed       Feed data
     * @param  array    $config     Config service
     * @param  object   $service    Service object
     * @return mixed
     */
    private function submitFeed($feedType, $feed, $config, $service)
    {
        $feedHandle = @fopen('php://temp', 'rw+');
        fwrite($feedHandle, $feed);
        rewind($feedHandle);
        $parameters = [
            'Merchant'        => $config['MERCHANT_ID'],
            'FeedType'        => $feedType,
            'FeedContent'     => $feedHandle,
            'PurgeAndReplace' => false,
            'ContentMd5'      => base64_encode(md5(stream_get_contents($feedHandle), true)),
        ];

        rewind($feedHandle);

        $request    = new MarketplaceWebService_Model_SubmitFeedRequest($parameters);
        $returnData = [
            'status' => false,
            'data'   => 'No reason'
        ];
        try {
            $response = $service->submitFeed($request);
            if ($response->isSetSubmitFeedResult()) {
                $submitFeedResult = $response->getSubmitFeedResult();
                if ($submitFeedResult->isSetFeedSubmissionInfo()) {
                    $returnData = [
                        'status' => true,
                        'data'   => $submitFeedResult->getFeedSubmissionInfo()
                    ];
                } else {
                    $returnData = [
                        'status' => false,
                        'data'   => 'No FeedSubmissionInfo'
                    ];
                }
            } else {
                $returnData = [
                    'status' => false,
                    'data'   => 'No SubmitFeedResult'
                ];
            }
        } catch (MarketplaceWebService_Exception $ex) {
            echo("Caught Exception: " . $ex->getMessage() . "\n");
            echo("Response Status Code: " . $ex->getStatusCode() . "\n");
            echo("Error Code: " . $ex->getErrorCode() . "\n");
            echo("Error Type: " . $ex->getErrorType() . "\n");
            echo("Request ID: " . $ex->getRequestId() . "\n");
            echo("XML: " . $ex->getXML() . "\n");
            echo("ResponseHeaderMetadata: " . $ex->getResponseHeaderMetadata() . "\n");

            Log::error("Caught Exception: " . $ex->getMessage());
            Log::error("Response Status Code: " . $ex->getStatusCode());
            Log::error("Error Code: " . $ex->getErrorCode());
            Log::error("Error Type: " . $ex->getErrorType());
            Log::error("Request ID: " . $ex->getRequestId());
            Log::error("XML: " . $ex->getXML());
            Log::error("ResponseHeaderMetadata: " . $ex->getResponseHeaderMetadata());

            $returnData = [
                'status' => false,
                'data'   => !empty($ex->getErrorCode())?$ex->getErrorCode():$ex->getStatusCode()
            ];
        }
        @fclose($feedHandle);
        return $returnData;
    }

    /**
     * Get feed submission list
     *
     * @param   array   $feedSubmissionIdList
     * @param   array   $config
     * @param   object  $service
     * @return  mixed
     */
    private function getFeedSubmissionList($feedSubmissionIdList, $config, $service)
    {
        $parameters = [
            'Merchant'                 => $config['MERCHANT_ID'],
            'FeedSubmissionIdList'     => ['Id' => $feedSubmissionIdList],
            'FeedProcessingStatusList' => ['Status' => ['_SUBMITTED_']]
        ];

        $request = new MarketplaceWebService_Model_GetFeedSubmissionListRequest($parameters);

        $returnData = [
            'status' => false,
            'data'   => 'No reason'
        ];
        try {
            $response = $service->getFeedSubmissionList($request);
            if ($response->isSetGetFeedSubmissionListResult()) {
                $returnData = [
                    'status' => true,
                    'data'   => $response->getGetFeedSubmissionListResult()
                ];
            } else {
                $returnData = [
                    'status' => false,
                    'data'   => 'No GetFeedSubmissionListResult'
                ];
            }
        } catch (MarketplaceWebService_Exception $ex) {
            echo("Caught Exception: " . $ex->getMessage() . "\n");
            echo("Response Status Code: " . $ex->getStatusCode() . "\n");
            echo("Error Code: " . $ex->getErrorCode() . "\n");
            echo("Error Type: " . $ex->getErrorType() . "\n");
            echo("Request ID: " . $ex->getRequestId() . "\n");
            echo("XML: " . $ex->getXML() . "\n");
            echo("ResponseHeaderMetadata: " . $ex->getResponseHeaderMetadata() . "\n");

            Log::error("Caught Exception: " . $ex->getMessage());
            Log::error("Response Status Code: " . $ex->getStatusCode());
            Log::error("Error Code: " . $ex->getErrorCode());
            Log::error("Error Type: " . $ex->getErrorType());
            Log::error("Request ID: " . $ex->getRequestId());
            Log::error("XML: " . $ex->getXML());
            Log::error("ResponseHeaderMetadata: " . $ex->getResponseHeaderMetadata());

            $returnData = [
                'status' => false,
                'data'   => !empty($ex->getErrorCode())?$ex->getErrorCode():$ex->getStatusCode()
            ];
        }
        return $returnData;
    }

    /**
     * Get feed submission result
     *
     * @param  string  $feedSubmissionId
     * @param  array   $config
     * @param  object  $service
     * @return object
     */
    private function getFeedSubmissionResult($feedSubmissionId, $config, $service)
    {
        $parameters = [
            'Merchant'             => $config['MERCHANT_ID'],
            'FeedSubmissionId'     => $feedSubmissionId,
            'FeedSubmissionResult' => @fopen('php://memory', 'rw+')
        ];

        $request    = new MarketplaceWebService_Model_GetFeedSubmissionResultRequest($parameters);
        $fileHandle = fopen('php://memory', 'rw+');
        $request->setFeedSubmissionResult($fileHandle);

        $returnData = [
            'status' => false,
            'data'   => 'No reason'
        ];
        try {
            $response = $service->getFeedSubmissionResult($request);
            rewind($fileHandle);
            $responseStr = stream_get_contents($fileHandle);
            $responseXML = new \SimpleXMLElement($responseStr);
            $returnData = [
                'status' => true,
                'data'   => $responseXML
            ];
        } catch (MarketplaceWebService_Exception $ex) {
            echo("Caught Exception: " . $ex->getMessage() . "\n");
            echo("Response Status Code: " . $ex->getStatusCode() . "\n");
            echo("Error Code: " . $ex->getErrorCode() . "\n");
            echo("Error Type: " . $ex->getErrorType() . "\n");
            echo("Request ID: " . $ex->getRequestId() . "\n");
            echo("XML: " . $ex->getXML() . "\n");
            echo("ResponseHeaderMetadata: " . $ex->getResponseHeaderMetadata() . "\n");

            Log::error("Caught Exception: " . $ex->getMessage());
            Log::error("Response Status Code: " . $ex->getStatusCode());
            Log::error("Error Code: " . $ex->getErrorCode());
            Log::error("Error Type: " . $ex->getErrorType());
            Log::error("Request ID: " . $ex->getRequestId());
            Log::error("XML: " . $ex->getXML());
            Log::error("ResponseHeaderMetadata: " . $ex->getResponseHeaderMetadata());

            $returnData = [
                'status' => false,
                'data'   => !empty($ex->getErrorCode())?$ex->getErrorCode():$ex->getStatusCode()
            ];
        }
        return $returnData;
    }



    /**
     * Call Api new Honten cancel
     *
     * @param array   $honOrder
     * @param boolean $isCallAPI
     *
     * @return int
     */
    public function cancelNewHonten($honOrder, $arrKey, $isCallAPI)
    {
        $modelO     = new MstOrder();
        try {
            if ($isCallAPI) {
                $url = 'http://52.199.5.86:9191/api/order';
            } else {
                $url = 'http://api-order.daito.work/api/order';
            }
            $apikey        = Config::get('apiservices')['new_honten']['apikey'];
            $connection    = 'biz_api';
            $fields_string = json_encode($honOrder);
            $ch            = curl_init($url);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "put");
            curl_setopt(
                $ch,
                CURLOPT_HTTPHEADER,
                array(
                    'Content-Type: application/json',
                    'data_apikey: ' . $apikey,
                    'data_connection: ' . $connection,
                    'Content-Length: ' . strlen($fields_string)
                )
            );
            curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 0);
            curl_setopt($ch, CURLOPT_TIMEOUT, 400);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $fields_string);
            $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
            $output   = curl_exec($ch);
            curl_close($ch);

            $result = json_decode($output);
            if (!empty($result)) {
                if ($result->error === 0) {
                    $errorCode = $result->code;
                    $message   = $result->msg;
                    $modelO->updateDataByReceiveOrder(
                        $arrKey,
                        [
                            'error_code_api' => $errorCode,
                            'message_api'    => $message,
                            'is_mall_cancel' => 1,
                            'up_ope_cd'      => 'OPE99999',
                            'up_date'        => now(),
                        ]
                    );
                } else {
                    $detail = $result->detail;
                    $arrSuccess = [];
                    foreach ($detail as $item) {
                        if ($item->error === 0) {
                            $arrSuccess[] = 'DIY-' . $item->id;
                        } else {
                            if (is_object($item->detail)) {
                                $message = $item->detail->detail;
                            } else {
                                $message = $item->detail;
                            }
                            $modelO->updateDataByReceiveOrder(
                                ['DIY-' . $item->id],
                                [
                                    'error_code_api' => 500,
                                    'message_api'    => $message,
                                    'is_mall_cancel' => 1,
                                    'up_ope_cd'      => 'OPE99999',
                                    'up_date'        => now(),
                                ]
                            );
                        }
                    }
                    if (count($arrSuccess) !== 0) {
                        $modelO->updateDataByReceiveOrder(
                            $arrSuccess,
                            [
                                'error_code_api' => 200,
                                'message_api'    => 'OK',
                                'is_mall_cancel' => 1,
                                'up_ope_cd'      => 'OPE99999',
                                'up_date'        => now(),
                            ]
                        );
                    }
                }
            } else {
                $modelO->updateDataByReceiveOrder(
                    $arrKey,
                    [
                        'error_code_api' => 500,
                        'message_api'    => 'ERROR',
                        'up_ope_cd'      => 'OPE99999',
                        'up_date'        => now(),
                    ]
                );
            }
        } catch (\Exception $e) {
            print_r($e->getMessage());
            $message = "Can't update status is finish.";
            print_r($message . PHP_EOL);
            Log::error($message);
            report($e);
            return 0;
        }
    }

    /**
     * Check order rakuten pay
     * @param  string $orderNumber
     * @return int
     */
    public function checkOrderRakutenPay($orderNumber)
    {
        $rakUrl    = "https://api.rms.rakuten.co.jp/es/2.0/order/getOrder/";
        $rakHeader = array(
            "Accept: application/json",
            "Content-type: application/json; charset=UTF-8",
            "Authorization: {$this->keyRakuten}"
        );
        $request = array(
            'orderNumberList' => [$orderNumber],
        );
        $requestJson = json_encode($request);
        $ch = curl_init($rakUrl);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $rakHeader);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $requestJson);
        $responseJson = curl_exec($ch);
        $responseJson = json_decode($responseJson);

        if (isset($responseJson->OrderModelList) && count($responseJson->OrderModelList) !== 0) {
            foreach ($responseJson->OrderModelList as $response) {
                return $response->orderProgress;
            }
        }
        return false;
    }

    /**
     * Call API cancel order rakuten pay
     * @param  string $orderNumber
     * @return int
     */
    public function cancelRakutenPay($orderNumber, $reasonId)
    {
        try {
            $modelO = new MstOrder();
            //Check is return
            $check = $this->checkOrderRakutenPay($orderNumber);
            if (($check && ($check === 900 || $check === 800)) || strpos($orderNumber, 're') === 0) {
                $modelO->updateDataByReceiveOrder(
                    [$orderNumber],
                    [
                        'error_code_api'   => null,
                        'message_api'      => null,
                        'is_mall_cancel'   => 1,
                        'order_sub_status' => ORDER_SUB_STATUS['DONE'],
                        'up_ope_cd'        => 'OPE99999',
                        'up_date'          => now(),
                    ]
                );
                return 1;
            }
            $rakUrl    = "https://api.rms.rakuten.co.jp/es/2.0/order/cancelOrder/";
            $rakHeader = array(
                "Accept: application/json",
                "Content-type: application/json; charset=UTF-8",
                "Authorization: {$this->keyRakuten}"
            );
            $request = array(
                'orderNumber'             => $orderNumber,
                'inventoryRestoreType'    => 2,
                'changeReasonDetailApply' => $reasonId,
            );
            $requestJson = json_encode($request);
            $ch = curl_init($rakUrl);
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_HTTPHEADER, $rakHeader);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $requestJson);
            $responseJson = curl_exec($ch);
            curl_close($ch);
            $responseJson = json_decode($responseJson);
            if (isset($responseJson->MessageModelList) && count($responseJson->MessageModelList) !== 0) {
                foreach ($responseJson->MessageModelList as $response) {
                    if ($response->messageCode === 'ORDER_EXT_API_CANCEL_ORDER_INFO_101' ||
                    $response->messageCode === 'ORDER_EXT_API_CANCEL_ORDER_WARNING_101') {
                        $modelO->updateDataByReceiveOrder(
                            [$orderNumber],
                            [
                                'error_code_api'   => $response->messageCode,
                                'message_api'      => $response->message,
                                'is_mall_cancel'   => 1,
                                'order_sub_status' => ORDER_SUB_STATUS['DONE'],
                                'up_ope_cd'        => 'OPE99999',
                                'up_date'          => now(),
                            ]
                        );
                        return 1;
                    } else {
                        $modelO->updateDataByReceiveOrder(
                            [$orderNumber],
                            [
                                'error_code_api'   => $response->messageCode,
                                'message_api'      => $response->message,
                                'up_ope_cd'        => 'OPE99999',
                                'up_date'          => now(),
                            ]
                        );
                        $error  = "------------------------------------------" . PHP_EOL;
                        $error .= basename(__CLASS__) . PHP_EOL;
                        $error .= 'Process cancel Rakuten pay order fail:' . PHP_EOL;
                        $error .= $orderNumber . ' - ' .$response->message;
                        $error .= PHP_EOL . "------------------------------------------" . PHP_EOL;
                        $this->slack->notify(new SlackNotification($error));
                    }
                }
                return 0;
            }
        } catch (\Exception $e) {
            print_r($e->getMessage());
            $message = "Can't update status is finish.";
            print_r($message . PHP_EOL);
            Log::error($message);
            report($e);
            return 0;
        }
    }

    /**
     * Cancel Wowma
     *
     * @param string $orderId
     * @param int    $reason
     * @param string $reasonComment
     */
    public function cancelWowma($orderId, $reason, $reasonComment)
    {
        $modelO = new MstOrder();
        $environment = 'real';
        if (App::environment(['local', 'test'])) {
            $environment = 'test';
        }
        $shopId    = Config::get("wowma.$environment.shop_id");
        $wowmaAuth = Config::get("wowma.$environment.auth_key");
        $urlApi    = Config::get("wowma.$environment.url");
        $wowmaHeader = array(
            "Content-Type: application/xml; charset=utf-8",
            "Authorization: $wowmaAuth"
        );
        $requestXml = view('api.xml.wowma_cancel_order', [
            'shopId'        => $shopId,
            'orderId'       => $orderId,
            'cancelReason'  => $reason,
            'cancelComment' => $reasonComment
        ])->render();
        $ch = curl_init($urlApi . '/cancelTradeProc');
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $wowmaHeader);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $requestXml);
        $responseXml = curl_exec($ch);
        curl_close($ch);
        $responData = simplexml_load_string($responseXml);
        if ((int)$responData->result->status !== 0) {
            $errorCode = (string)$responData->result->error->code;
            $errorMess = (string)$responData->result->error->message;
            $message = "Cancel order wowma [{$orderId}] has error:" . PHP_EOL;
            $message .= "[$errorCode] $errorMess";
            print_r($message . PHP_EOL);
            Log::error($message);
            $error  = "------------------------------------------" . PHP_EOL;
            $error .= basename(__CLASS__) . PHP_EOL;
            $error .= PHP_EOL . $message;
            $error .= PHP_EOL . "------------------------------------------" . PHP_EOL;
            $this->slack->notify(new SlackNotification($error));
            $modelO->updateDataByReceiveOrder([$orderId], [
                'message_api'    => $errorMess,
                'error_code_api' => $errorCode,
            ]);
        } else {
            $modelO->updateDataByReceiveOrder([$orderId], [
                'message_api'    => 'Cancel_OK',
            ]);
        }
        return 1;
    }
}
