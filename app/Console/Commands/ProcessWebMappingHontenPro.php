<?php
/**
 * Process web mapping honten pro
 *
 * @package    App\Console\Commands
 * @subpackage ProcessWebMappingHontenPro
 * @copyright  Copyright (c) 2018 RiverCrane! Corporation. All Rights Reserved.
 * @author     Truong Nghia<truong.van.nghia@rivercrane.vn>
 */

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\Batches\MstOrder;
use App\Models\Batches\DtWebMapping;
use App\Models\Batches\MstPaymentMethod;
use App\Models\Batches\DtWebProData;
use Illuminate\Support\Facades\Log;
use App\Events\Command as eCommand;
use Event;
use Config;
use App;

class ProcessWebMappingHontenPro extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'process:web-mapping-honten-pro';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * The error.
     *
     * @var array
     */
    public $error = [];

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $arrayReplace = [':', '-'];
        $folder       = str_replace($arrayReplace, '_', $this->signature);
        Log::useDailyFiles(storage_path() . "/logs/$folder/history.log");
        //Process data
        Event::fire(new eCommand($this->signature, array('start' => true)));
        $startBatch = microtime(true);
        Log::info('Start batch process web mapping for Honten Pro.');
        print_r("Start batch process web mapping for Honten Pro." . PHP_EOL);
        $modelTWD       = new DtWebProData();
        $modelO         = new MstOrder();
        $modelPay       = new MstPaymentMethod();
        $modelMap       = new DtWebMapping();
        $dataOrderCheck = $modelO->getOrderAPIWebMapping(8, 1000);
        if ($dataOrderCheck->count() !== 0) {
            $arrOrder = $dataOrderCheck->pluck('received_order_id')->toArray();
            if (!App::environment(['local', 'test'])) {
                $url = 'http://52.199.5.86:9191/api/order?orderID=' . str_replace('DIY-', '', implode(',', $arrOrder));
            } else {
                $url = 'http://api-order.daitotest.tk/api/order?orderID=' . str_replace('DIY-', '', implode(',', $arrOrder));
            }
            $apikey        = Config::get('apiservices')['new_honten']['apikey'];
            $connection    = 'biz_api';
            $ch            = curl_init($url);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "get");
            curl_setopt(
                $ch,
                CURLOPT_HTTPHEADER,
                array(
                    'Content-Type: application/json',
                    'data_apikey: ' . $apikey,
                    'data_connection: ' . $connection
                )
            );
            curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 0);
            curl_setopt($ch, CURLOPT_TIMEOUT, 400);
            $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
            $output   = curl_exec($ch);
            curl_close($ch);
            $result = json_decode($output);
            if (!empty($result)) {
                $total = $this->processDataBDashPro($arrOrder, $result);
            }
            $message = "Insert dt_web_pro_data success: $total records.";
            Log::info($message);
            print_r($message . PHP_EOL);
        }
        $dataPro     = $modelTWD->getDataProcessWebBDashPro();
        $dataPayment = $modelPay->getDataByMall(8);
        $count       = 0;
        if (count($dataPro) !== 0) {
            foreach ($dataPro as $data) {
                $arrInsert = [];
                try {
                    $paymentMethod = 0;
                    foreach ($dataPayment as $pay) {
                        if ($data->settlement === $pay->payment_name) {
                            $paymentMethod = $pay->payment_code;
                            break;
                        }
                    }
                    $arrInsert['received_order_id']  = $data->order_id;
                    $arrInsert['mall_id']            = 8;
                    $arrInsert['web_payment_method'] = $paymentMethod;
                    switch ((int)$data->process_status) {
                        case 3:
                        case 5:
                            $arrInsert['web_order_status'] = 8;
                            break;
                        case 7:
                            $arrInsert['web_order_status'] = 10;
                            break;
                        default:
                            $arrInsert['web_order_status'] = 0;
                            break;
                    }
                    $arrInsert['web_request_price'] = $data->requestprice;
                    $differentType = [];
                    if ($data->payment_method !== $paymentMethod) {
                        $differentType[] = '支払方法';
                    }
                    if ($arrInsert['web_request_price'] !== $data->request_price && ($data->order_status <> 10 || $arrInsert['web_order_status'] <> 10)) {
                        $differentType[] = 'WEB金額';
                    }
                    if ($data->order_status !== $arrInsert['web_order_status']) {
                        $differentType[] = 'ステータス';
                    }
                    $arrInsert['different_type']  = implode("\n", $differentType);
                    $arrInsert['different_price'] = $data->request_price - $data->requestprice;
                    $arrInsert['occorred_reason'] = 0;
                    $arrInsert['process_content'] = 0;
                    $arrInsert['finished_date']   = empty($arrInsert['different_type'])
                        ? date('Y-m-d H:i:s') : null;
                    $arrInsert['is_corrected']    = empty($arrInsert['different_type']) ? 1 : 0;
                    $arrInsert['is_deleted']      = 0;
                    $arrInsert['receive_id']      = 0;
                    $arrInsert['in_ope_cd']       = 'OPE99999';
                    $arrInsert['in_date']         = date('Y-m-d H:i:s');
                    $arrInsert['up_ope_cd']       = 'OPE99999';
                    $arrInsert['up_date']         = date('Y-m-d H:i:s');
                    $modelMap->insertIgnore($arrInsert);
                    $modelTWD->where('order_id', $data->order_id)
                        ->update(['process_flg' => 1]);
                    $count++;
                } catch (\Exception $e) {
                    report($e);
                }
            }
            $message = "Insert dt_web_mapping success: $count records.";
            Log::info($message);
            print_r($message . PHP_EOL);
        }
        Event::fire(new eCommand($this->signature, array('end' => true, 'error' => $this->error)));
        $totalTime = round(microtime(true) - $startBatch, 2);
        Log::info("End batch process web mapping for Honten Pro with total time: $totalTime s.");
        print_r("End batch process web mapping for Honten Pro with total time: $totalTime s.");
    }

    /**
     * Process data b-dash pro
     *
     * @param  array  $arrOrderId
     * @param  array  $datas
     * @return boolean
     */
    public function processDataBDashPro($arrOrderId, $datas)
    {
        $modelTWD  = new DtWebProData();
        $dataCheck = $modelTWD->getDataCheck($arrOrderId);
        $arrCheck  = [];
        if ($dataCheck->count() !== 0) {
            $arrCheck = $dataCheck->pluck('order_id')->toArray();
        }
        $arrInserts = [];
        foreach ($datas as $value) {
            if (!empty($value) && !in_array("DIY-" . $value->order_id, $arrCheck)) {
                $arrTemp = [];
                $arrTemp['order_id']              = "DIY-" . $value->order_id;
                $arrTemp['settlement']            = $value->payment_method;
                $arrTemp['requestprice']          = $value->total;
                $arrTemp['process_status']        = $value->order_status_id;
                $arrTemp['process_flg']           = 0;
                $arrTemp['is_delete']             = 0;
                $arrTemp['created_at']            = date('Y-m-d H:i:s');
                $arrInserts[] = $arrTemp;
            }
        }
        $total = count($arrInserts);
        if ($total !== 0) {
            $modelTWD->insert($arrInserts);
        }
        return $total;
    }
}
