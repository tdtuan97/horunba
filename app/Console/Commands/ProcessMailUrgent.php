<?php
/**
 * Batch process content to mail urgent
 *
 * @package    App\Console\Commands
 * @subpackage ProcessMailUrgent
 * @copyright  Copyright (c) 2018 RiverCrane! Corporation. All Rights Reserved.
 * @author     Truong Nghia<truong.van.nghia@rivercrane.vn>
 */

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\Batches\MstOrder;
use App\Models\Batches\MstSettlementManage;
use App\Models\Batches\DtSendMailList;
use App\Models\Batches\MstPaymentAccount;
use Illuminate\Support\Facades\Log;
use App\Notifications\SlackNotification;
use App\Console\Commands\ProcessMailQueue;
use App\Custom\Utilities;
use App\Notification;
use App\Events\Command as eCommand;
use Event;

class ProcessMailUrgent extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'process:mail-urgent';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Add content to mail urgent';

    /**
     * The error.
     *
     * @var array
     */
    public $error = [];

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //Create log
        $arrayReplace = [':', '-'];
        $folder       = str_replace($arrayReplace, '_', $this->signature);
        Log::useDailyFiles(storage_path() . "/logs/$folder/history.log");
        //Process data
        Event::fire(new eCommand($this->signature, array('start' => true)));
        Log::info('Start batch process mail urgent.');
        print_r("Start batch process mail urgent." . PHP_EOL);
        $start    = microtime(true);
        $slack    = new Notification(CHANNEL['horunba']);
        $modelO   = new MstOrder();
        $modelSML = new DtSendMailList();
        $modelPA  = new MstPaymentAccount();
        $datas    = $modelO->getDataMailUrgent();
        if (count($datas) !== 0) {
            $arrReceiveId = [];
            $success      = 0;
            $fail         = 0;
            foreach ($datas as $key => $data) {
                $flg = true;
                try {
                    $arrInsert   = [];
                    $arrUpdate   = [];
                    $mailContent = '';
                    $mailSubject = '';
                    list($mailContent, $arrUpdate) = ProcessMailQueue::processContent(
                        $data->mail_content,
                        $data->payment_method,
                        $data->received_order_id,
                        true
                    );
                    $mailSubject = ProcessMailQueue::processContent(
                        $data->mail_subject,
                        $data->payment_method,
                        $data->received_order_id,
                        false
                    );
                    if (!empty($data->signature_content)) {
                        $mailContent = $mailContent . PHP_EOL . $data->signature_content;
                    }
                    $arrKey = [
                        'receive_order_id'    => $data->received_order_id,
                        'order_status_id'     => 13,
                        'order_sub_status_id' => 0,
                        'operater_send_index' => $data->urgent_mail_id,
                    ];
                    $arrData = [
                        'payment_method'      => $data->payment_method,
                        'mall_id'             => $data->mall_id,
                        'mail_id'             => $data->urgent_mail_id,
                        'mail_subject'        => $mailSubject,
                        'mail_content'        => $mailContent,
                        'attached_file_path'  => $data->attached_file_path,
                        'send_status'         => 0,
                        'mail_to'             => $data->email,
                        'mail_cc'             => '',
                        'mail_bcc'            => '',
                        'mail_from'           => $data->mall_id === 1 ? 'rakuten@diy-tool.com' : '',
                        'error_code'          => '',
                        'error_message'       => '',
                        'in_date'             => now(),
                        'in_ope_cd'           => 'OPE99999',
                        'up_date'             => now(),
                        'up_ope_cd'           => 'OPE99999',
                    ];
                    $arrInsert = array_merge($arrKey, $arrData);
                    $modelSML->insertIgnore($arrInsert);
                    if (count($arrUpdate) !== 0) {
                        if ($arrUpdate['flg_payment']) {
                            $modelPA->where(['name' => $arrUpdate['name']])
                                ->update(['current_account' => $arrUpdate['current_account']]);
                        }
                        $modelO->updateData(
                            [$arrUpdate['receive_id']],
                            [
                                'payment_account'      => $arrUpdate['payment_account'],
                                'payment_request_date' => $arrUpdate['payment_request_date'],
                            ]
                        );
                    }
                    if ((int)$data->urgent_mail_id === 80) {
                        $modelO->updateData(
                            array($data->receive_id),
                            [
                                'is_send_mail_urgent' => 2,
                                'urgent_mail_id'      => 0,
                            ]
                        );
                    } else {
                        $modelO->updateData(
                            array($data->receive_id),
                            [
                                'is_send_mail_urgent' => 0,
                                'urgent_mail_id'      => 0,
                            ]
                        );
                    }

                    $success++;
                } catch (\Exception $e) {
                    $this->error[] = Utilities::checkMessageException($e);
                    $error  = "------------------------------------------" . PHP_EOL;
                    $error .= basename(__CLASS__) . PHP_EOL;
                    $error .= Utilities::checkMessageException($e);
                    $error .= PHP_EOL . "------------------------------------------" . PHP_EOL;
                    $slack->notify(new SlackNotification($error));
                    Log::error(Utilities::checkMessageException($e));
                    print_r("$error");
                    $fail++;
                }
            }
            Log::info("Process success: $success and fail: $fail records.");
            print_r("Process success: $success and fail: $fail records." . PHP_EOL);
        } else {
            Log::info('No data');
            print_r("No data" . PHP_EOL);
        }
        Event::fire(new eCommand($this->signature, array('end' => true, 'error' => $this->error)));
        $totalTime = round(microtime(true) - $start, 2);
        Log::info("End batch process mail urgent with total time: $totalTime s.");
        print_r("End batch process mail urgent with total time: $totalTime s.");
    }
}
