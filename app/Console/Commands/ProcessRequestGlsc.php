<?php
/**
 * Batch process request glsc
 *
 * @package    App\Console\Commands
 * @subpackage ProcessRequestGlsc
 * @copyright  Copyright (c) 2018 RiverCrane! Corporation. All Rights Reserved.
 * @author     Truong Nghia<truong.van.nghia@rivercrane.vn>
 */

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;
use App\Notifications\SlackNotification;
use App\Models\Batches\DtReturn;
use App\Models\Batches\TInsertRequestListEdi;
use App\Models\Batches\TNo;
use App\Notification;
use App\Events\Command as eCommand;
use App\Custom\Utilities;
use Event;
use App;
use DB;

class ProcessRequestGlsc extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'process:request-glsc';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'When product returned, stock need to receive it. '.
    'So that we have to ordered to GLSC receive that product.';

    /**
     * The notification.
     *
     * @var object
     */
    protected $slack;

    /**
     * The error.
     *
     * @var array
     */
    public $error = [];

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $arrayReplace = [':', '-'];
        $folder       = str_replace($arrayReplace, '_', $this->signature);
        Log::useDailyFiles(storage_path() . "/logs/$folder/history.log");
        Event::fire(new eCommand($this->signature, array('start' => true)));
        $slack = new Notification(CHANNEL['horunba']);
        $start = microtime(true);
        Log::info('Start batch process request GLSC.');
        print_r("Start batch process request GLSC." . PHP_EOL);
        $modelDR = new DtReturn();
        $modelTN = new TNo();
        $modelTI = new TInsertRequestListEdi();
        $data    = $modelDR->getDataRequestGLSC();
        $count   = count($data);
        $success = 0;
        if ($count === 0) {
            Log::info('Process request GLSC no data');
            print_r("Process request GLSC no data. " . PHP_EOL);
        } else {
            $arrInsert = [];
            try {
                DB::beginTransaction();
                foreach ($data as $value) {
                    $arrivalPlanDate = date("Ymd");
                    if (date('H:i') > '16:45' || $value->receive_instruction === 8) {
                        $arrivalPlanDate = date("Ymd", strtotime("+1 days"));
                    }
                    $key = $value->return_no . $value->return_time . $value->return_line_no;
                    // $time = substr(date('Ymd'), 3);
                    // $requestNo = $time . sprintf("%05d", $modelTN->insertGetId(['created_at' => now()]));
                    $arrInsert[$key]['order_code']                   = $key;
                    $arrInsert[$key]['RequestNo']                    = $key;
                    $arrInsert[$key]['RequestBNo']                   = $value->receive_count + 1;
                    $arrInsert[$key]['ArrivalPlanDate']              = $arrivalPlanDate;
                    $arrInsert[$key]['PlanQuantity']                 = $value->return_quantity - $value->receive_real_num;
//                    if (empty($value->product_code)) {
                        $arrInsert[$key]['ItemCd']                       = $value->product_code;
                        $arrInsert[$key]['ItemNm']                       = $value->product_name;
                        $arrInsert[$key]['JanCd']                        = $value->product_jan;
                        $arrInsert[$key]['SupplierNm']                   = $value->supplier_nm;
                        $arrInsert[$key]['MakerNm']                      = $value->maker_full_nm;
                        $arrInsert[$key]['MakerNo']                      = $value->product_maker_code;
                        $arrInsert[$key]['ItemUrl']                      = $value->rak_img_url_1;
/*                    } else {
                        $arrInsert[$key]['ItemCd']                       = $value->product_code;
                        $arrInsert[$key]['ItemNm']                       = $value->child_product_name;
                        $arrInsert[$key]['JanCd']                        = $value->child_product_jan;
                        $arrInsert[$key]['SupplierNm']                   = $value->child_supplier_nm;
                        $arrInsert[$key]['MakerNm']                      = $value->child_maker_full_nm;
                        $arrInsert[$key]['MakerNo']                      = $value->child_product_maker_code;
                        $arrInsert[$key]['ItemUrl']                      = $value->child_rak_img_url_1;
                    }*/
                    $arrInsert[$key]['RecordTag']                    = null;
                    $arrInsert[$key]['GroupKey']                     = null;
                    $arrInsert[$key]['RequestTypeKb']                = 0;
                    // $arrInsert[$key]['created_at']                   = null;
                    $arrInsert[$key]['er_insertRequestListResponse'] = null;
                    $arrInsert[$key]['er_insertRequestListResult']   = null;
                    $arrInsert[$key]['er_tUpdateReplyModel']         = null;
                    $arrInsert[$key]['er_Key']                       = null;
                    $arrInsert[$key]['er_KeyString']                 = null;
                    $arrInsert[$key]['er_ErrInfo']                   = null;
                    $arrInsert[$key]['er_tErrModel']                 = null;
                    $arrInsert[$key]['er_ErrorCd']                   = null;
                    $arrInsert[$key]['er_Message']                   = null;
                    $arrInsert[$key]['er_Kind']                      = null;
                    $arrInsert[$key]['er_StartDate']                 = null;
                    $arrInsert[$key]['er_Count']                     = null;
                    $arrInsert[$key]['er_ResultInfoString']          = null;
                    $arrInsert[$key]['process_name']                 = 'ProcessRequestGlsc';
                    $arrInsert[$key]['del_flg']                      = 0;
                    $arrUpdate = [
                        'receive_count' => DB::raw("receive_count + 1"),
                        'receive_instruction' => 1,
                        'status' => 1,
                    ];
                    $modelDR->updateData(
                        [
                            'return_no'      => $value->return_no,
                            'return_line_no' => $value->return_line_no,
                            'return_time'    => $value->return_time,
                        ],
                        $arrUpdate
                    );
                }
                $success = count($arrInsert);
                if ($success !== 0) {
                    $arrKey = array_keys($arrInsert);
                    $modelTI->insert($arrInsert);
                }
                DB::commit();
            } catch (\Exception $e) {
                $success = 0;
                DB::rollback();
                $this->error[] = Utilities::checkMessageException($e);
                $error  = "------------------------------------------" . PHP_EOL;
                $error .= basename(__CLASS__) . PHP_EOL;
                $error .= Utilities::checkMessageException($e);
                $error .= PHP_EOL . "------------------------------------------" . PHP_EOL;
                $slack->notify(new SlackNotification($error));
                Log::error(Utilities::checkMessageException($e));
                print_r("$error");
            }
            $message  = "Processed : $count records and success: $success records.";
            Log::info($message);
            print_r($message . PHP_EOL);
            // Call API
            if (App::environment(['local']) || App::environment(['test'])) {
                $url = 'https://edi-test.diyfactory.jp';
            } else {
                $url = 'https://edi.diyfactory.jp';
            }
            // Process call api
            $apiUrl = $url . '/edi/api/exec-curl-feed';
            $cmd  = "curl H $apiUrl";
            if (substr(php_uname(), 0, 7) == "Windows") {
                $cmd .= " >NUL 2>NUL";
                pclose(popen('start /B cmd /C "' . $cmd . '"', 'r'));
            } else {
                exec($cmd . " > /dev/null 2>/dev/null &");
            }
        }
        Event::fire(new eCommand($this->signature, array('end' => true, 'error' => $this->error)));
        $totalTime = round(microtime(true) - $start, 2);
        Log::info("End batch process request GLSC with total time: $totalTime.");
        print_r("End batch process request GLSC with total time: $totalTime s." . PHP_EOL);
    }
}
