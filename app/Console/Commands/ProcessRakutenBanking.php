<?php
/**
 * Batch process get rakuten banking from api
 *
 * @package    App\Console\Commands
 * @subpackage ProcessRakutenBanking
 * @copyright  Copyright (c) 2018 RiverCrane! Corporation. All Rights Reserved.
 * @author     Van Qui<van.qui@rivercrane.vn>
 */

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;
use App\Notifications\SlackNotification;
use App\Custom\Utilities;
use App\Notification;
use App\Events\Command as eCommand;
use Event;
use Config;
use Helper;
use App;
use App\Models\Batches\MstOrder;
use App\Models\Batches\MstOrderDetail;
use App\Models\Batches\DtOrderProductDetail;

class ProcessRakutenBanking extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'process:rakuten-banking';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Process Rakuten banking';

    /**
     * The notification.
     *
     * @var object
     */
    protected $slack;

    /**
     * The error.
     *
     * @var array
     */
    public $error = [];

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //Create log
        $arrayReplace = [':', '-'];
        $folder       = str_replace($arrayReplace, '_', $this->signature);
        Log::useDailyFiles(storage_path() . "/logs/$folder/history.log");
        //Process data
        Event::fire(new eCommand($this->signature, array('start' => true)));
        Log::info('Start batch process Rakuten banking.');
        print_r("Start batch process Rakuten banking." . PHP_EOL);
        $start       = microtime(true);
        $this->slack = new Notification(CHANNEL['horunba']);

        $isCallAPI = true;
        if (App::environment(['local', 'test'])) {
            $isCallAPI = false;
        }

        $modelO    = new MstOrder();
        $dataOrder = $modelO->getDataRakutenBanking();
        if ($dataOrder->count() === 0) {
            Log::info('No data.');
            print_r("No data." . PHP_EOL);
        } else {
            $dataOrder = $dataOrder->chunk(1000);
            $total     = 0;
            foreach ($dataOrder as $orders) {
                $this->processRequestPaymentAPI($orders, $isCallAPI);
                $this->processGetInfoAPI($orders, $isCallAPI);
                $total += count($orders);
            }
            Log::info("Process {$total} records.");
            print_r("Process {$total} records." . PHP_EOL);
        }

        Event::fire(new eCommand($this->signature, array('end' => true, 'error' => $this->error)));
        $totalTime = round(microtime(true) - $start, 2);
        Log::info("End batch process Rakuten banking with total time: $totalTime s.");
        print_r("End batch process Rakuten banking with total time: $totalTime s.");
    }

    /**
     * Process get API
     *
     * @param  object $orders
     * @param  bool   $isCallAPI
     * @return void
     */
    private function processGetInfoAPI($orders, $isCallAPI)
    {
        $modelO = new MstOrder();
        foreach ($orders as $item) {
            $listOrder[] = $item->received_order_id;
        }
        try {
            if ($isCallAPI) {
                $rakAuth   = Config::get('common.rak_auth_key');
                $rakUrl    = "https://api.rms.rakuten.co.jp/es/1.0/order/ws";
                $rakHeader = array(
                    "Content-Type: text/xml;charset=UTF-8",
                );
                $startDate  = date("Y-m-d\TH:i:s", strtotime($orders->min('order_date'). "-1 days"));
                $endDate    = date("Y-m-d\TH:i:s", strtotime($orders->max('order_date'). "+1 days"));
                $requestXml = view('mapping.xml.rakuten', [
                    'authKey'   => $rakAuth,
                    'shopUrl'   => 'tuzukiya',
                    'userName'  => 'tuzukiya',
                    'startDate' => $startDate,
                    'endDate'   => $endDate,
                    'orders'    => $listOrder
                ])->render();

                $ch = curl_init($rakUrl);
                curl_setopt($ch, CURLOPT_POST, true);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($ch, CURLOPT_HTTPHEADER, $rakHeader);
                curl_setopt($ch, CURLOPT_POSTFIELDS, $requestXml);
                $responseXml = curl_exec($ch);
                curl_close($ch);
                $dataResponse = $this->processRakutenXml($responseXml);
            }
            $dataUpdate   = array();
            $dataCancel   = array();
            if ($isCallAPI) {
                foreach ($orders as $item) {
                    if (isset($dataResponse[$item->received_order_id])) {
                        if (in_array($dataResponse[$item->received_order_id], [5, 6, 8])) {
                            $dataUpdate[$item->receive_id] = [
                                'payment_status'       => 1,
                                'payment_date'         => now(),
                                'payment_confirm_date' => now(),
                                'up_ope_cd'            => 'OPE99999',
                                'up_date'              => now()
                            ];
                        } elseif ($dataResponse[$item->received_order_id] === 7) {
                            $dataCancel[] = [
                                'receive_id'        => $item->receive_id,
                                'received_order_id' => $item->received_order_id
                            ];
                        }
                    }
                    if (empty($item->payment_request_date)) {
                        $dataUpdate[$item->receive_id] = [
                            'payment_request_date' => now(),
                            'up_ope_cd'      => 'OPE99999',
                            'up_date'        => now()
                        ];
                    }
                }
            } else {
                foreach ($orders as $index => $item) {
                    if ($index % 2 === 0) {
                        $dataCancel[] = [
                            'receive_id'        => $item->receive_id,
                            'received_order_id' => $item->received_order_id
                        ];
                    } else {
                        $dataUpdate[$item->receive_id] = [
                            'payment_status'       => 1,
                            'payment_date'         => now(),
                            'payment_confirm_date' => now(),
                            'up_ope_cd'            => 'OPE99999',
                            'up_date'              => now()
                        ];
                    }
                }
            }
            if (count($dataUpdate) > 0) {
                foreach ($dataUpdate as $key => $val) {
                    $modelO->updateData([$key], $val);
                }
            }
            if (count($dataCancel) > 0) {
                $dataCancel = array_chunk($dataCancel, 500);
                foreach ($dataCancel as $datCancel) {
                    $requestId = $this->getRequestId($isCallAPI);
                    if ($requestId !== false) {
                        $this->cancelRakuten($datCancel, $requestId, $isCallAPI);
                    }
                }
            }
        } catch (\Exception $e) {
            $message = "Can't process get order rakuten api";
            print_r($message . PHP_EOL);
            Log::error($message);
            report($e);
            $this->error[] = $e->getMessage();
        }
    }


    /**
     * Process Rakuten xml
     *
     * @param  string $responseXml
     * @return array
     */
    private function processRakutenXml($responseXml)
    {
        $responseXml = str_ireplace(['S:', 'ns2:'], '', $responseXml);
        $responData  = simplexml_load_string($responseXml);
        $returnData  = $responData->Body->getOrderResponse->return;
        $errorCode   = Helper::val($returnData->errorCode);
        $data = array();
        if ($errorCode !== "E10-001") {
            foreach ($returnData->orderModel as $orderModel) {
                $data[Helper::val($orderModel->orderNumber)] = (int)Helper::val($orderModel->RBankModel->rbankStatus);
            }
        }
        return $data;
    }

    /**
     * Process request payment api
     *
     * @param  array $orders
     * @param  bool  $isCallAPI
     * @return void
     */
    public function processRequestPaymentAPI($orders, $isCallAPI)
    {
        $requestId = $this->getRequestId($isCallAPI);
        if ($requestId !== false) {
            $this->updateBankAccountTransfer($orders, $requestId, $isCallAPI);
        }
    }

     /**
     * Get request id
     *
     * @param  boolean $isCallAPI
     * @return boolean
     */
    public function getRequestId($isCallAPI)
    {
        try {
            if (!$isCallAPI) {
                return '000000';
            }
            $rakAuth   = Config::get('common.rak_auth_key');
            $rakUrl    = "https://api.rms.rakuten.co.jp/es/1.0/order/ws";
            $rakHeader = array(
                "Content-Type: text/xml;charset=UTF-8",
            );
            $requestXml = view('api.xml.rakuten_get_request', [
                'authKey'   => $rakAuth,
                'shopUrl'   => 'tuzukiya',
                'userName'  => 'tuzukiya',
            ])->render();
            $ch = curl_init($rakUrl);
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_HTTPHEADER, $rakHeader);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $requestXml);
            $responseXml = curl_exec($ch);
            curl_close($ch);
            $responseXml = str_ireplace(['S:', 'ns2:'], '', $responseXml);
            $responData  = simplexml_load_string($responseXml);
            if (!empty($responData) &&
                Helper::val($responData->Body->getRequestIdResponse->return->errorCode) === 'N00-000') {
                return Helper::val($responData->Body->getRequestIdResponse->return->requestId);
            } else {
                return false;
            }
        } catch (\Exception $e) {
            $message = "Can't get request ID";
            print_r($message . PHP_EOL);
            Log::error($message);
            report($e);
            return false;
        }
    }

    /**
     * Update rBankAccountTransfer
     *
     * @param  object   $orders
     * @param  string   $requestId
     * @param  boolean  $isCallAPI
     * @return void
     */
    private function updateBankAccountTransfer($orders, $requestId, $isCallAPI)
    {
        try {
            $modelO    = new MstOrder();
            if ($isCallAPI) {
                $rakAuth   = Config::get('common.rak_auth_key');
                $rakUrl    = "https://api.rms.rakuten.co.jp/es/1.0/order/ws";
                $rakHeader = array(
                    "Content-Type: text/xml;charset=UTF-8",
                );
                $requestXml = view('api.xml.rakuten_bank_account_transfer', [
                    'authKey'   => $rakAuth,
                    'shopUrl'   => 'tuzukiya',
                    'userName'  => 'tuzukiya',
                    'requestId' => $requestId,
                    'orders'    => $orders
                    ])->render();



                $ch = curl_init($rakUrl);
                curl_setopt($ch, CURLOPT_POST, true);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($ch, CURLOPT_HTTPHEADER, $rakHeader);
                curl_setopt($ch, CURLOPT_POSTFIELDS, $requestXml);
                $responseXml = curl_exec($ch);
                curl_close($ch);
                $responseXml = str_ireplace(['S:', 'ns2:'], '', $responseXml);
                $responData  = simplexml_load_string($responseXml);
                $returnData  = $responData->Body->rBankAccountTransferResponse->return;
                $errorCode   = Helper::val($returnData->errorCode);
                $errorMess   = Helper::val($returnData->message);
                $arrError    = array();
                if ($errorCode !== "N00-000") {
                    foreach ($returnData->unitError as $unitError) {
                        $arrError["".$unitError->orderKey] = [
                            'errorCode' => Helper::val($unitError->errorCode),
                            'message'   => Helper::val($unitError->message),
                        ];
                    }
                }
            } else {
                $errorCode = '';
                $errorMess = '';
            }


            foreach ($orders as $order) {
                $codeUpdate = $errorCode;
                $messUpdate = $errorMess;
                if (isset($arrError["".$order->received_order_id])) {
                    $codeUpdate = $arrError["".$order->received_order_id]['errorCode'];
                    $messUpdate = $arrError["".$order->received_order_id]['message'];
                }

                $modelO->updateDataByReceiveOrder(
                    ["".$order->received_order_id],
                    [
                        'request_id'     => $requestId,
                        'error_code_api' => $codeUpdate,
                        'message_api'    => $messUpdate,
                        'up_ope_cd'      => 'OPE99999',
                        'up_date'        => now()
                    ]
                );
            }
        } catch (\Exception $e) {
            $message = "Call request id $requestId fail";
            print_r($message . PHP_EOL);
            Log::error($message);
            report($e);
        }
    }

    /**
     * Cancel rakuten api
     *
     * @param  array   $rakOrderCancel
     * @param  string  $requestId
     * @param  boolean $isCallAPI
     * @return void
     */
    private function cancelRakuten($rakOrderCancel, $requestId, $isCallAPI)
    {
        try {
            $modelO    = new MstOrder();
            $modelOPD  = new DtOrderProductDetail();
            if ($isCallAPI) {
                $rakAuth   = Config::get('common.rak_auth_key');
                $rakUrl    = "https://api.rms.rakuten.co.jp/es/1.0/order/ws";
                $rakHeader = array(
                    "Content-Type: text/xml;charset=UTF-8",
                );
                $requestXml = view('api.xml.rakuten_cancel_order', [
                    'authKey'        => $rakAuth,
                    'shopUrl'        => 'tuzukiya',
                    'userName'       => 'tuzukiya',
                    'requestId'      => $requestId,
                    'rakOrderCancel' => $rakOrderCancel
                ])->render();
                $ch = curl_init($rakUrl);
                curl_setopt($ch, CURLOPT_POST, true);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($ch, CURLOPT_HTTPHEADER, $rakHeader);
                curl_setopt($ch, CURLOPT_POSTFIELDS, $requestXml);
                $responseXml = curl_exec($ch);
                curl_close($ch);
                $responseXml = str_ireplace(['S:', 'ns2:'], '', $responseXml);
                $responData  = simplexml_load_string($responseXml);
                $returnData  = $responData->Body->cancelOrderResponse->return;
                $errorCode   = Helper::val($returnData->errorCode);
                $errorMess   = Helper::val($returnData->message);
                $arrError    = array();
                if ($errorCode !== "N00-000") {
                    foreach ($returnData->unitError as $unitError) {
                        $arrError["".$unitError->orderKey] = [
                            'errorCode' => Helper::val($unitError->errorCode),
                            'message'   => Helper::val($unitError->message),
                        ];
                    }
                }
            } else {
                $errorCode = '';
                $errorMess = '';
            }

            foreach ($rakOrderCancel as $order) {
                $codeUpdate = $errorCode;
                $messUpdate = $errorMess;
                if (isset($arrError["".$order['received_order_id']])) {
                    $codeUpdate = $arrError["".$order['received_order_id']]['errorCode'];
                    $messUpdate = $arrError["".$order['received_order_id']]['message'];
                }

                $modelO->updateDataByReceiveOrder(
                    [$order['received_order_id']],
                    [
                        'request_id'          => $requestId,
                        'is_mail_sent'        => 0,
                        'is_mall_cancel'      => 1,
                        'ship_charge'         => 0,
                        'total_price'         => 0,
                        'pay_charge_discount' => 0,
                        'used_point'          => 0,
                        'used_coupon'         => 0,
                        'request_price'       => 0,
                        'pay_charge'          => 0,
                        'goods_price'         => 0,
                        'order_status'        => ORDER_STATUS['CANCEL'],
                        'order_sub_status'    => ORDER_SUB_STATUS['DONE'],
                        'cancel_reason'       => '7',
                        'is_mall_update'      => 0,
                        'error_code_api'      => $codeUpdate,
                        'message_api'         => $messUpdate,
                        'up_ope_cd'           => 'OPE99999',
                        'up_date'             => now()
                    ]
                );
                $modelOPD->updateCancelStock($order['receive_id']);
                $modelOPD->updateDataByCondition(
                    ['receive_id' => $order['receive_id']],
                    ['product_status' => PRODUCT_STATUS['CANCEL']]
                );
                $modelOD = new MstOrderDetail();
                $arrUpdateOrderDetail = [
                    'quantity' => 0,
                    'price'    => 0,
                ];
                $modelOD->updateData(['receive_id' => $order['receive_id']], $arrUpdateOrderDetail);
            }
        } catch (\Exception $e) {
            $message = "Call request id $requestId fail";
            print_r($message . PHP_EOL);
            Log::error($message);
            report($e);
        }
    }
}
