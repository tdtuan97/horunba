<?php
/**
 * Batch process stock result
 *
 * @package    App\Console\Commands
 * @subpackage ProcessStockResult
 * @copyright  Copyright (c) 2017 RiverCrane! Corporation. All Rights Reserved.
 * @author     Truong Nghia<truong.van.nghia@rivercrane.vn>
 */

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;
use App\Notifications\SlackNotification;
use App\Notification;
use App\Models\Batches\TOrder;
use App\Models\Batches\TOrderDetail;
use App\Models\Batches\DtRelenishOrder;
use App\Models\Batches\MstStockStatus;
use DB;
use App\Events\Command as eCommand;
use App\Custom\Utilities;
use Event;

class ProcessStockResult extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'process:stock-result';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * The error.
     *
     * @var array
     */
    public $error = [];

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        // //Create log
        // $arrayReplace = [':', '-'];
        // $folder       = str_replace($arrayReplace, '_', $this->signature);
        // Log::useDailyFiles(storage_path() . "/logs/$folder/history.log");
        //Process data
        // Event::fire(new eCommand($this->signature, array('start' => true)));
        // Log::info('Start batch process stock result.');
        // print_r("Start batch process stock result." . PHP_EOL);
        // $startBatch = microtime(true);
        // $modelTO = new TOrder();
        // $modelTOrder = new TOrderDetail();
        // $modelRO    = new DtRelenishOrder();
        // $modelSS = new MstStockStatus();
        // $datas1 = $modelTO->getDataStockResult1();
        // $flgNoData1 = false;
        // $flgNoData2 = false;
        // if (count($datas1) !== 0) {
        //     $succ1 = 0;
        //     $arrId = [];
        //     foreach ($datas1 as $data) {
        //         $arrUpdate    = [];
        //         $arrId[] = $data->t_order_detail_id;
        //         switch ($data->m_order_type_id) {
        //             case 1:
        //                 if ($data->shortage_quantity > 0) {
        //                     $arrUpdate['replenish_status'] = 8;
        //                 } else {
        //                     $arrUpdate['replenish_status'] = 9;
        //                 }
        //                 $arrUpdate['arrival_date']   = $data->shipment_date;
        //                 $arrUpdate['arrival_num']    = $data->stock_quantity;
        //                 $arrUpdate['incomplete_num'] = $data->shortage_quantity;
        //                 break;
        //             case 3:
        //                 $arrUpdate['replenish_status'] = 3;
        //                 break;
        //             case 4:
        //                 $arrUpdate['replenish_status'] = 4;
        //                 break;
        //             case 9:
        //                 $arrUpdate['replenish_status']  = 2;
        //                 $arrUpdate['arrival_date_plan'] = $data->shipment_date_plan;
        //                 break;
        //             case 13:
        //                 $arrUpdate['replenish_status'] = 5;
        //                 break;
        //             case 14:
        //                 $arrUpdate['replenish_status'] = 6;
        //                 break;
        //             case 18:
        //                 $arrUpdate['replenish_status'] = 7;
        //                 break;
        //             default:
        //                 break;
        //         }
        //         $arrUpdate['order_note'] = $data->other;
        //         if (count($arrUpdate) !== 0) {
        //             $modelRO->where(['order_code' => $data->order_code])->update($arrUpdate);
        //             $succ1++;
        //         }
        //     }
        //     if (count($arrId) !== 0) {
        //         $modelTOrder->whereIn('t_order_detail_id', $arrId)
        //                     ->update(['filmaker_flg' => 1]);
        //     }
        //     Log::info("Update table dt_replenish_order and t_order_detail success: $succ1 records.");
        //     print_r("Update table dt_replenish_order and t_order_detail success: $succ1 records." . PHP_EOL);
        // } else {
        //     $flgNoData1 = true;
        // }

        // $datas2 = $modelSS->getDataStockResult2();
        // if (count($datas2) !== 0) {
        //     $succ2 = 0;
        //     foreach ($datas2 as $data) {
        //         $value = (int)$data->arrival_num;
        //         $modelSS->where(['product_code' => $data->product_code])
        //                 ->increment(
        //                     'nanko_num',
        //                     $value,
        //                     ['rep_orderring_num' => DB::raw("rep_orderring_num - $value")]
        //                 );
        //         $modelRO->where(['order_code' => $data->order_code])
        //                 ->update(['replenish_status' => 10]);
        //         $succ2++;
        //     }
        //     Log::info("Update table mst_stock_status and dt_replenish_order success: $succ2 records.");
        //     print_r("Update table mst_stock_status and dt_replenish_order success: $succ2 records." . PHP_EOL);
        // } else {
        //     $flgNoData2 = true;
        // }
        // if ($flgNoData1 && $flgNoData2) {
        //     Log::info('No data');
        //     print_r("No data" . PHP_EOL);
        // }
        // Event::fire(new eCommand($this->signature, array('end' => true, 'error' => $this->error)));
        // $totalTime = round(microtime(true) - $startBatch, 2);
        // Log::info("End batch process stock result with total time: $totalTime s.");
        // print_r("End batch process stock result with total time: $totalTime s.");
    }
}
