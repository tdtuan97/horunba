<?php
/**
 * Batch process order supplier EDI
 *
 * @package    App\Console\Commands
 * @subpackage ProcessOrderEdi
 * @copyright  Copyright (c) 2017 RiverCrane! Corporation. All Rights Reserved.
 * @author     Truong Nghia<truong.van.nghia@rivercrane.vn>
 */

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\Batches\DtOrderProductDetail;
use Illuminate\Support\Facades\Log;
use App\Notifications\SlackNotification;
use App\Models\Batches\TOrder;
use App\Models\Batches\TAdmin;
use App\Models\Batches\TSeq;
use App\Models\Batches\TOrderDetail;
use App\Models\Batches\DtOrderToSupplier;
use App\Models\Batches\MstOrder;
use App\Models\Batches\TCustomer;
use App\Models\Batches\TOrderDirect;
use App\Custom\Utilities;
use App\Notification;
use App\Events\Command as eCommand;
use Event;
use DB;

class ProcessOrderEdi extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'process:order-edi';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'With direct delivery goods, then order to supplier to delivery to customer.';

    /**
     * The notification.
     *
     * @var object
     */
    protected $slack;

    /**
     * The error.
     *
     * @var array
     */
    public $error = [];

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //Create log
        $arrayReplace = [':', '-'];
        $folder       = str_replace($arrayReplace, '_', $this->signature);
        Log::useDailyFiles(storage_path() . "/logs/$folder/history.log");
        //Process data
        Event::fire(new eCommand($this->signature, array('start' => true)));
        Log::info('Start batch process order EDI.');
        print_r("Start batch process order EDI." . PHP_EOL);
        $start           = microtime(true);
        $this->slack = new Notification(CHANNEL['horunba']);
        Log::info('=== Start batch confirm status order. ===');
        print_r("=== Start batch confirm status order. ===" . PHP_EOL);
        $this->confirmStatusOrder();
        Log::info('=== End batch confirm status order. ===');
        print_r("=== End batch confirm status order. ===" . PHP_EOL);
        Log::info('=== Start batch process order to supplier EDI. ===');
        print_r("=== Start batch process order to supplier EDI. ===" . PHP_EOL);
        $this->orderToEdi();
        Log::info('=== End batch process order to supplier EDI. ===');
        print_r("=== End batch process order to supplier EDI. ===" . PHP_EOL);
        Log::info('=== Start batch process order to supplier EDI direct delivery. ===');
        print_r("=== Start batch process order to supplier EDI direct delivery. ===" . PHP_EOL);
        $this->orderToEdiDirect();
        Log::info('=== End batch process order to supplier EDI direct delivery. ===');
        print_r("=== End batch process order to supplier EDI direct delivery. ===" . PHP_EOL);
        Event::fire(new eCommand($this->signature, array('end' => true, 'error' => $this->error)));
        $totalTime = round(microtime(true) - $start, 2);
        Log::info("End batch process order EDI with total time: $totalTime s.");
        print_r("End batch process order EDI with total time: $totalTime s.");
    }

    /**
    * Process confirm status order
    * @return void
    */
    public function confirmStatusOrder()
    {
        $modelOrder    = new MstOrder();
        $dataConfirm = $modelOrder->getDataConfirmStatus();
        if (count($dataConfirm) !== 0) {
            $arrUpdate = [
                'order_status'     => ORDER_STATUS['ORDER'],
                'order_sub_status' => ORDER_SUB_STATUS['NEW'],
                'is_mail_sent'     => 0,
            ];
            $arrReceive = [];
            foreach ($dataConfirm as $data) {
                if (!in_array($data->receive_id, $arrReceive) &&
                    (is_null($data->is_available) ||
                    (!is_null($data->is_available) && $data->is_available === $data->is_mail_sent))
                ) {
                    $arrReceive[] = $data->receive_id;
                }
            }
            $modelOrder->updateData($arrReceive, $arrUpdate);
            $succ = count($arrReceive);
            Log::info("Update table mst_order success: $succ records.");
            print_r("Update table mst_order success: $succ records." . PHP_EOL);
        } else {
            Log::info('No data');
            print_r("No data" . PHP_EOL);
        }
    }

    /**
    * Process order to supplier EDI
    * @return void
    */
    public function orderToEdi()
    {
        $modelOPD      = new DtOrderProductDetail();
        $modelOrder    = new TOrder();
        $modelAdmin    = new TAdmin();
        $modelSeq      = new TSeq();
        $modelOD       = new TOrderDetail();
        $modelSupplier = new DtOrderToSupplier();
        $modelO        = new MstOrder();
        $index         = 0;
        $yearMonth     = substr(date('Ym'), 3);
        $maxOrderCode  = $modelSupplier->getMaxOrderCodeByDate($yearMonth);
        if (count($maxOrderCode) > 0) {
            $orderCode = $maxOrderCode->toArray()['order_code'];
            if (strpos($orderCode, $yearMonth) === 0) {
                $index = (int)substr($orderCode, 3);
            }
        }
        $datas = $modelOPD->getDataProcessEDI();
        Log::info('Start process insert dt_order_to_supplier.');
        print_r("Start process insert dt_order_to_supplier." . PHP_EOL);
        if (count($datas) === 0) {
            Log::info('No data');
            print_r("No data" . PHP_EOL);
        } else {
            $receiveId      = '';
            $orderCode      = '';
            $inSuccessO     = 0;
            $inFailO        = 0;
            foreach ($datas as $key => $data) {
                if ($data->cheetah_status === 4) {
                    DB::transaction(function () use ($modelO, $modelOPD, $data) {
                        $modelOPD->updateData(
                            $data->receive_id,
                            $data->detail_line_num,
                            $data->sub_line_num,
                            ['product_status' => PRODUCT_STATUS['SALE_STOP']]
                        );
                        $modelO->updateData([$data->receive_id], ['order_sub_status' => ORDER_SUB_STATUS['STOP_SALE']]);
                    }, 5);
                } elseif ($data->cheetah_status === 3) {
                    DB::transaction(function () use ($modelO, $modelOPD, $data) {
                        $modelOPD->updateData(
                            $data->receive_id,
                            $data->detail_line_num,
                            $data->sub_line_num,
                            ['product_status' => PRODUCT_STATUS['OUT_OF_STOCK']]
                        );
                        $modelO->updateData([$data->receive_id], ['order_sub_status' => ORDER_SUB_STATUS['OUT_OF_STOCK']]);
                    }, 5);
                } else {
                    $index     += 1;
                    $orderCode = $yearMonth . sprintf("%06d", $index);
                    $arrOrSupplier  = [];
                    $arrOrSupplier['order_code']          = $orderCode;
                    $arrOrSupplier['order_date']          = now();
                    $arrOrSupplier['order_type']          = 1;
                    $arrOrSupplier['supplier_id']         = $data->suplier_id;
                    $arrOrSupplier['product_code']        = $data->product_code;
                    $arrOrSupplier['product_jan']         = $data->product_jan;
                    $arrOrSupplier['price_invoice']       = $data->price_invoice;
                    $arrOrSupplier['order_num']           = $data->received_order_num;
                    $arrOrSupplier['arrival_date_plan']   = null;
                    $arrOrSupplier['arrival_date']        = null;
                    $arrOrSupplier['arrival_quantity']    = 0;
                    $arrOrSupplier['incomplete_quantity'] = 0;
                    $arrOrSupplier['other']               = null;
                    $arrOrSupplier['remarks']             = null;
                    $arrOrSupplier['wait_days']           = null;
                    $arrOrSupplier['process_status']      = 0;
                    $arrOrSupplier['receive_id']          = $data->receive_id;
                    $arrOrSupplier['detail_line_num']     = $data->detail_line_num;
                    $arrOrSupplier['sub_line_num']        = $data->sub_line_num;
                    $arrOrSupplier['times_num']           = 0;
                    $arrOrSupplier['sku_num']             = $data->sku_num;
                    $arrOrSupplier['is_close_dok']        = $data->is_close_dok;
                    $arrOrSupplier['edi_order_code']      = $orderCode . $arrOrSupplier['times_num'];
                    $arrOrSupplier['in_date']             = now();
                    $arrOrSupplier['up_date']             = now();
                    $arrUpdate = [
                        'product_status' => PRODUCT_STATUS['ORDERING'],
                        'order_code'     => $orderCode,
                    ];
                    if ($data->suplier_id === 0) {
                        $error  = "------------------------------------------" . PHP_EOL;
                        $error .= basename(__CLASS__) . PHP_EOL;
                        $error .= "Order code $orderCode has supplier_id 0" . PHP_EOL;
                        $error .= PHP_EOL . "------------------------------------------" . PHP_EOL;
                        $this->slack->notify(new SlackNotification($error));
                    }
                    DB::transaction(function () use ($modelOPD, $modelSupplier, $arrOrSupplier, $arrUpdate, $data) {
                        $modelSupplier->insert($arrOrSupplier);
                        $modelOPD->updateData(
                            $data->receive_id,
                            $data->detail_line_num,
                            $data->sub_line_num,
                            $arrUpdate
                        );
                    }, 5);
                }
                $inSuccessO++;
            }
            Log::info("Insert to dt_order_to_supplier success: $inSuccessO and fail: $inFailO records.");
            print_r("Insert to dt_order_to_supplier success: $inSuccessO and fail: $inFailO records." . PHP_EOL);
        }
        Log::info('End process insert dt_order_to_supplier.');
        print_r("End process insert dt_order_to_supplier." . PHP_EOL);
        Log::info('Start process insert t_order and t_order_detail.');
        print_r("Start process insert t_order and t_order_detail." . PHP_EOL);
        $datas2 = $modelSupplier->getDataProcessEDI2();
        if (count($datas2) === 0) {
            Log::info('No data');
            print_r("No data" . PHP_EOL);
        } else {
            $dataAdmin    = $modelAdmin->getDataEdi()->toArray();
            $countSuccess = 0;
            foreach ($datas2 as $value) {
                DB::transaction(function () use ($modelOrder, $modelOD, $modelSeq, $modelSupplier, $value, $dataAdmin) {
                    $idInsert       = 0;
                    $deliveryPeriod = 3;
                    $arrOrder       = [];
                    $arrOrderDetail = [];
                    /*
                    switch ($value->delivery_flg) {
                        case '14営業日':
                            $deliveryPeriod = 14;
                            break;
                        case '３営業日':
                            $deliveryPeriod = 3;
                            break;
                        case '５営業日':
                            $deliveryPeriod = 5;
                            break;
                        case '７営業日':
                            $deliveryPeriod = 7;
                            break;
                        case '即納':
                            $deliveryPeriod = 3;
                            break;
                        default:
                            $deliveryPeriod = 30;
                            break;
                    }*/

                    $fax    = '';
                    $faxFlg = 0;
                    $supId  = $value->supplier_id;
                    $flagUpdate = 0;
                    $nowTime = date('H:i');
                    //1 (for Monday) through 7 (for Sunday)
                    $numDay = (int)date('N');
                    if ($supId === 41) {
                        if ($numDay >= 1 && $numDay <= 4) {
                            if ($value->is_close_dok === 1 && ($nowTime > '18:30' || $nowTime < '10:30')) {
                                $flagUpdate = 1;
                            } elseif ($nowTime >= '10:30' && $nowTime <= '18:30') {
                                $flagUpdate = 1;
                            }
                        } elseif ($numDay === 5) {
                            if ($value->is_close_dok === 1 && $nowTime < '10:30') {
                                $flagUpdate = 1;
                            } elseif ($nowTime >= '10:30') {
                                $flagUpdate = 1;
                            }
                        } elseif ($numDay === 6) {
                            if ($value->is_close_dok === 1 && $nowTime > '12:00') {
                                $flagUpdate = 1;
                            } elseif ($nowTime <= '12:00') {
                                $flagUpdate = 1;
                            }
                        } elseif ($numDay === 7) {
                            if ($value->is_close_dok === 1) {
                                $flagUpdate = 1;
                            }
                        }
                    } else {
                        $flagUpdate = 1;
                    }
                    if (array_key_exists($supId, $dataAdmin)) {
                        $fax    = $dataAdmin[$supId]['send_fax_flg'] === '1' ? $dataAdmin[$supId]['fax'] : '';
                        $faxFlg = $dataAdmin[$supId]['send_fax_flg'] === '1' ? 1 : 0;
                    }
                    $supplyKB = '';
                    if (in_array($value->order_type, [1, 2])) {
                        $supplyKB = '客注';
                    } elseif ($value->order_type === 3) {
                        $supplyKB = '補充';
                    } elseif ($value->order_type === 5) {
                        $supplyKB = '予定外';
                    } else {
                        $supplyKB = '臨時';
                    }
                    if ($flagUpdate === 1) {
                        // process order
                        $orderCode                  = (string)$value->edi_order_code;
                        $arrOrder['suppliercd']     = $value->supplier_id;
                        $arrOrder['SerialNo']       = null;
                        $arrOrder['order_date']     = now();
                        $arrOrder['order_code']     = $orderCode;
                        $arrOrder['sku_num']        = $value->sku_num;
                        $arrOrder['supplieritemcd'] = (string)$value->supplier_code;
                        $arrOrder['deiiveryperiod'] = $deliveryPeriod;
                        $arrOrder['supplykb']       = $supplyKB;
                        $arrOrder['item_code']      = (string)$value->product_code;
                        $arrOrder['jan_code']       = (string)$value->product_jan;
                        $arrOrder['maker_name']     = (string)$value->maker_full_nm;
                        $arrOrder['maker_code']     = (string)$value->product_maker_code;
                        $arrOrder['note_code']      = '';
                        $arrOrder['name']           = (string)$value->product_name_long;
                        $arrOrder['delivery']       = '南港';
                        $arrOrder['quantity']       = (int)$value->order_num;
                        $arrOrder['color']          = (string)$value->product_color;
                        $arrOrder['size']           = (string)$value->product_size;
                        $arrOrder['price']          = (int)$value->price_invoice;
                        $arrOrder['other']          = null;
                        $arrOrder['itemurl']        = $value->rak_img_url_1;
                        $arrOrder['send_fax_flg']   = 0;
                        $arrOrder['cancel_flg']     = 0;
                        $arrOrder['del_flg']        = 0;
                        $arrOrder['valid_flg']      = 1;
                        $arrOrder['created_at']     = now();
                        $arrOrder['updated_at']     = now();
                        $arrOrder['fax']            = $fax;
                        $arrOrder['fax_flg']        = $faxFlg;
                        $arrOrder['mail']           = '';
                        $arrOrder['mail_flg']       = 0;
                        $arrOrder['send_mail_flg']  = 0;
                        $arrOrder['send_mail_date'] = null;
                        //Check order code
                        $checkId = $modelOrder->getOrderId($orderCode);
                        if (empty($checkId)) {
                            //Insert new data
                            $idInsert = $modelOrder->insertGetId($arrOrder);
                        } else {
                            $idInsert = $checkId->t_order_id;
                        }
                        // process order detail
                        $tSeqId                               = $modelSeq->insertGetId(['directkb' => 0]);
                        $arrOrderDetail['suppliercd']         = $value->supplier_id;
                        $arrOrderDetail['t_order_id']         = $idInsert;
                        $arrOrderDetail['m_order_type_id']    = 2;
                        $arrOrderDetail['t_nouhin_id']        = 0;
                        $arrOrderDetail['t_seq_id']           = $tSeqId;
                        $arrOrderDetail['requestbno']         = null;
                        $arrOrderDetail['order_code']         = (string)$value->edi_order_code;
                        $arrOrderDetail['shipment_date_plan'] = null;
                        $arrOrderDetail['shipment_date']      = null;
                        $arrOrderDetail['quantity']           = (int)$value->order_num;
                        $arrOrderDetail['price']              = (int)$value->price_invoice;
                        $arrOrderDetail['other']              = null;
                        $arrOrderDetail['memo']               = null;
                        $arrOrderDetail['direct_flg']         = 0;
                        $arrOrderDetail['pass']               = '';
                        $arrOrderDetail['state_flg']          = 1;
                        $arrOrderDetail['cancel_flg']         = 0;
                        $arrOrderDetail['filmaker_flg']       = 1;
                        $arrOrderDetail['new_flg']            = 0;
                        $arrOrderDetail['request_flg']        = 0;
                        $arrOrderDetail['reminder_flg']       = 0;
                        $arrOrderDetail['stock_quantity']     = 0;
                        $arrOrderDetail['shortage_quantity']  = 0;
                        $arrOrderDetail['stock_other']        = null;
                        $arrOrderDetail['stock_flg']          = 0;
                        $arrOrderDetail['t_admin_id']         = null;
                        $arrOrderDetail['del_flg']            = 0;
                        $arrOrderDetail['valid_flg']          = 1;
                        $arrOrderDetail['created_at']         = now();
                        $arrOrderDetail['updated_at']         = now();
                        $modelOD->insert($arrOrderDetail);
                        $modelSupplier->updateData($value->order_code, ['process_status' => 1], $value->edi_order_code);
                    }
                }, 5);
                $countSuccess++;
            }
            Log::info("Insert order to edi: $countSuccess");
            print_r("Insert order to edi: $countSuccess" . PHP_EOL);
        }
        Log::info('End process insert order to edi.');
        print_r("End process insert order to edi." . PHP_EOL);
        //Start copy from batch divide-delivery (From 4. Inventory reservation)
        Log::info('=== Start process inventory reservation. ===');
        print_r("=== Start process inventory reservation. ===" . PHP_EOL);
        $dataInventory2 = $modelOPD->getDataProcessInventory2();
        if (count($dataInventory2) === 0) {
            Log::info('No data process');
            print_r("No data process" . PHP_EOL);
            Log::info('=== End process inventory reservation. ===');
            print_r("=== End process inventory reservation. ===" . PHP_EOL);
        } else {
            $successInv2 = 0;
            $failInv2    = 0;
            foreach ($dataInventory2 as $value) {
                $productCode = !empty($value->child_product_code) ? $value->child_product_code : $value->product_code;
                $dataOTS = $modelSupplier->getDataInventory2($productCode);
                $reserveNum = $value->order_num;
                foreach ($dataOTS as $item) {
                    try {
                        $cal = $item->order_num - $item->order_remain_num - $reserveNum;
                        if ($cal >= 0) {
                            $modelOPD->updateData(
                                $value->receive_id,
                                $value->detail_line_num,
                                $value->sub_line_num,
                                [
                                    'order_code'     => $item->order_code,
									'delivery_date'     => $item->estimate_delivery_date,
                                    'product_status' => PRODUCT_STATUS['ORDERING'],
                                ]
                            );
                            $modelSupplier->where('order_code', $item->order_code)
                                         ->where('edi_order_code', $item->edi_order_code)
                            ->increment(
                                'order_remain_num',
                                $reserveNum
                            );
                            $successInv2++;
                            break;
                        }
                        $successInv2++;
                    } catch (\Exception $e) {
                        $this->error[] = Utilities::checkMessageException($e);
                        $error  = "------------------------------------------" . PHP_EOL;
                        $error .= basename(__CLASS__) . PHP_EOL;
                        $error .= Utilities::checkMessageException($e);
                        $error .= PHP_EOL . "------------------------------------------" . PHP_EOL;
                        $slack->notify(new SlackNotification($error));
                        Log::error(Utilities::checkMessageException($e));
                        print_r("$error");
                        $failInv2++;
                    }
                }
            }
            $message = "End process inventory reservation and".
                       " Update success: $successInv2 record, fail: $failInv2 record.";
            Log::info("=== $message ===");
            print_r("=== $message ===" . PHP_EOL);
        }
        //End copy from batch divide-delivery (From 4. Inventory reservation)
    }

    /**
    * Process order to supplier EDI direct delivery
    * @return void
    */
    public function orderToEdiDirect()
    {
        $modelMstOrder = new MstOrder();
        $modelCus      = new TCustomer();
        $modelSeq      = new TSeq();
        $modelAdmin    = new TAdmin();
        $modelOrderDir = new TOrderDirect();
        $modelSupplier = new DtOrderToSupplier();
        $modelOPD      = new DtOrderProductDetail();
        $datas         = $modelMstOrder->getDataProcessEDIDirect('product_code');
        $datasSet      = $modelMstOrder->getDataProcessEDIDirect('child_product_code')->keyBy('child_product_code');
        $yearMonth     = substr(date('Ym'), 3);
        $maxOrderCode  = $modelSupplier->getMaxOrderCodeByDate($yearMonth);
        $index         = 0;
        if (count($maxOrderCode) > 0) {
            $orderCode = $maxOrderCode->toArray()['order_code'];
            if (strpos($orderCode, $yearMonth) === 0) {
                $index = (int)substr($orderCode, 3);
            }
        }
        Log::info('Start insert dt_order_to_supplier');
        print_r("Start insert dt_order_to_supplier" . PHP_EOL);
        if (count($datas) === 0) {
            Log::info('No data');
            print_r("No data" . PHP_EOL);
        } else {
            $suppSuccess  = 0;
            $suppFail     = 0;
            foreach ($datas as $data) {
                $arrOrderSupplier = [];
                $index            += 1;
                $orderCode = $yearMonth . sprintf("%06d", $index);
                $childProductCode = $data->child_product_code;
                if (!empty($childProductCode)) {
                    if (array_key_exists($childProductCode, $datasSet->toArray())) {
                        $data = $datasSet[$childProductCode];
                    } else {
                        $modelOPD->updateData(
                            $data->receive_id,
                            $data->detail_line_num,
                            $data->sub_line_num,
                            ['product_status' => PRODUCT_STATUS['ERROR']]
                        );
                        continue;
                    }
                }
                if ($data->cheetah_status === 4) {
                    DB::transaction(function () use ($modelMstOrder, $modelOPD, $data) {
                        $modelOPD->updateData(
                            $data->receive_id,
                            $data->detail_line_num,
                            $data->sub_line_num,
                            ['product_status' => PRODUCT_STATUS['SALE_STOP']]
                        );
                        $modelMstOrder->updateData(
                            [$data->receive_id],
                            ['order_sub_status' => ORDER_SUB_STATUS['STOP_SALE']]
                        );
                    }, 5);
                } elseif ($data->cheetah_status === 3) {
                    DB::transaction(function () use ($modelMstOrder, $modelOPD, $data) {
                        $modelOPD->updateData(
                            $data->receive_id,
                            $data->detail_line_num,
                            $data->sub_line_num,
                            ['product_status' => PRODUCT_STATUS['OUT_OF_STOCK']]
                        );
                        $modelMstOrder->updateData(
                            [$data->receive_id],
                            ['order_sub_status' => ORDER_SUB_STATUS['OUT_OF_STOCK']]
                        );
                    }, 5);
                } else {
                    $arrOrderSupplier['order_code']          = $orderCode;
                    $arrOrderSupplier['order_date']          = now();
                    $arrOrderSupplier['order_type']          = 2;
                    $arrOrderSupplier['supplier_id']         = $data->suplier_id;
                    $arrOrderSupplier['product_code']        = $data->product_code;
                    $arrOrderSupplier['product_jan']         = $data->product_jan;
                    $arrOrderSupplier['price_invoice']       = $data->price_invoice;
                    $arrOrderSupplier['order_num']           = $data->received_order_num;
                    $arrOrderSupplier['arrival_date_plan']   = null;
                    $arrOrderSupplier['arrival_date']        = null;
                    $arrOrderSupplier['arrival_quantity']    = 0;
                    $arrOrderSupplier['incomplete_quantity'] = 0;
                    $arrOrderSupplier['other']               = null;
                    $arrOrderSupplier['remarks']             = null;
                    $arrOrderSupplier['wait_days']           = null;
                    $arrOrderSupplier['process_status']      = 0;
                    $arrOrderSupplier['receive_id']          = $data->receive_id;
                    $arrOrderSupplier['detail_line_num']     = $data->detail_line_num;
                    $arrOrderSupplier['sub_line_num']        = $data->sub_line_num;
                    $arrOrderSupplier['times_num']           = 0;
                    $arrOrderSupplier['edi_order_code']      = $orderCode . $arrOrderSupplier['times_num'];
                    $arrOrderSupplier['sku_num']             = $data->sku_num;
                    $arrOrderSupplier['in_date']             = now();
                    $arrOrderSupplier['up_date']             = now();
                    $arrUpdate = [
                        'product_status' => PRODUCT_STATUS['ORDERING'],
                        'order_code'   => $orderCode,
                    ];
                    $arrWhere = [
                        'receive_id'      => $data->receive_id,
                        'detail_line_num' => $data->detail_line_num,
                        'sub_line_num'    => $data->sub_line_num,
                    ];
                    if ($data->suplier_id === 0) {
                        $error  = "------------------------------------------" . PHP_EOL;
                        $error .= basename(__CLASS__) . PHP_EOL;
                        $error .= "Order code $orderCode has supplier_id 0" . PHP_EOL;
                        $error .= PHP_EOL . "------------------------------------------" . PHP_EOL;
                        $this->slack->notify(new SlackNotification($error));
                    }
                    DB::transaction(function () use ($modelOPD, $modelSupplier, $arrOrderSupplier, $arrUpdate, $data) {
                        $modelSupplier->insert($arrOrderSupplier);
                        $modelOPD->updateData(
                            $data->receive_id,
                            $data->detail_line_num,
                            $data->sub_line_num,
                            $arrUpdate
                        );
                    }, 5);
                }
                $suppSuccess++;
            }
            Log::info("Insert to dt_order_to_supplier success: $suppSuccess and fail: $suppFail records.");
            print_r("Insert to dt_order_to_supplier success: $suppSuccess and fail: $suppFail records." . PHP_EOL);
        }
        Log::info('End insert dt_order_to_supplier');
        print_r("End insert dt_order_to_supplier" . PHP_EOL);
        Log::info('Start insert t_order_direct');
        print_r("Start insert t_order_direct" . PHP_EOL);
        $datas2 = $modelSupplier->getDataProcessEDIDirect2();
        if (count($datas2) === 0) {
            Log::info('No data');
            print_r("No data" . PHP_EOL);
        } else {
            $cusSuccess   = 0;
            $cusFail      = 0;
            $orderSuccess = 0;
            $orderFail    = 0;
            foreach ($datas2 as $data) {
                $arrCustomer      = [];
                $arrOrderDirect   = [];
                $arrCustomer['SerialNo']   = 0;
                $arrCustomer['c_nm']       = $data->company_name . $data->order_lastname . $data->order_firstname;
                $arrCustomer['c_postal']   = $data->order_zip_code;
                $arrCustomer['c_address']  = $data->order_prefecture . $data->order_city . $data->order_sub_address;
                $arrCustomer['c_tel']      = $data->order_tel;
                $arrCustomer['d_nm']       = $data->ship_to_last_name . $data->ship_to_first_name;
                $arrCustomer['d_postal']   = $data->ship_zip_code;
                $arrCustomer['d_address']  = $data->ship_prefecture . $data->ship_city . $data->ship_sub_address;
                $arrCustomer['d_tel']      = $data->ship_tel_num;
                $arrCustomer['del_flg']    = 0;
                $arrCustomer['valid_flg']  = 1;
                $arrCustomer['created_at'] = now();
                $arrCustomer['updated_at'] = now();
                $arrCusCheck = $modelCus->checkCustomerExist($arrCustomer);
                $id          = '';
                $check       = false;
                if (count($arrCusCheck) === 0) {
                    try {
                        $id = $modelCus->insertGetId($arrCustomer);
                        $cusSuccess++;
                    } catch (\Exception $e) {
                        $this->error[] = Utilities::checkMessageException($e);
                        $error  = "------------------------------------------" . PHP_EOL;
                        $error .= basename(__CLASS__) . PHP_EOL;
                        $error .= Utilities::checkMessageException($e);
                        $error .= PHP_EOL . "------------------------------------------" . PHP_EOL;
                        $this->slack->notify(new SlackNotification($error));
                        Log::error(Utilities::checkMessageException($e));
                        print_r("$error");
                        $cusFail++;
                        continue;
                    }
                    $check = true;
                } else {
                    $id = $arrCusCheck->t_customer_id;
                }
                $dataAdmin = $modelAdmin->getDataEdi()->toArray();
                $fax    = '';
                $faxFlg = 0;
                $supId  = $data->supplier_id;
                if (array_key_exists($supId, $dataAdmin)) {
                    $fax    = $dataAdmin[$supId]['send_fax_flg'] === '1' ? $dataAdmin[$supId]['fax'] : '';
                    $faxFlg = $dataAdmin[$supId]['send_fax_flg'] === '1' ? 1 : 0;
                }
                $deliveryPeriod = 3;
                /*
                switch ($data->delivery_flg) {
                    case '14営業日':
                        $deliveryPeriod = 14;
                        break;
                    case '３営業日':
                        $deliveryPeriod = 3;
                        break;
                    case '５営業日':
                        $deliveryPeriod = 5;
                        break;
                    case '７営業日':
                        $deliveryPeriod = 7;
                        break;
                    case '即納':
                        $deliveryPeriod = 3;
                        break;
                    default:
                        $deliveryPeriod = 30;
                        break;
                }*/
                $supplyKB = '';
                if (in_array($data->order_type, [1, 2])) {
                    $supplyKB = '客注';
                } elseif ($data->order_type === 3) {
                    $supplyKB = '補充';
                } else {
                    $supplyKB = '臨時';
                }
                $tSeqId = $modelSeq->insertGetId(['directkb' => 1]);
                $arrOrderDirect['suppliercd']           = $data->supplier_id;
                $arrOrderDirect['t_customer_id']        = $id;
                $arrOrderDirect['t_delivery_id']        = null;
                $arrOrderDirect['m_order_type_id']      = 2;
                $arrOrderDirect['SerialNo']             = 0;
                $arrOrderDirect['t_seq_id']             = $tSeqId;
                $arrOrderDirect['order_date']           = now();
                $arrOrderDirect['order_code']           = $data->edi_order_code;
                $arrOrderDirect['sku_num']              = $data->sku_num;
                $arrOrderDirect['supplieritemcd']       = (string)$data->supplier_code;
                $arrOrderDirect['supplykb']             = $supplyKB;
                $arrOrderDirect['deiiveryperiod']       = $check === true ? null : $deliveryPeriod;
                $arrOrderDirect['item_code']            = (string)$data->product_code;
                $arrOrderDirect['jan_code']             = (string)$data->product_jan;
                $arrOrderDirect['maker_name']           = (string)$data->maker_full_nm;
                $arrOrderDirect['maker_code']           = (string)$data->product_maker_code;
                $arrOrderDirect['note_code']            = '';
                $arrOrderDirect['name']                 = (string)$data->product_name_long;
                $arrOrderDirect['color']                = (string)$data->product_color;
                $arrOrderDirect['size']                 = (string)$data->product_size;
                $arrOrderDirect['daito_price']          = (int)$data->price_invoice;
                $arrOrderDirect['price']                = (int)$data->price_invoice;
                $arrOrderDirect['quantity']             = (int)$data->order_num;
                $arrOrderDirect['shipment_date_plan']   = null;
                $arrOrderDirect['other']                = '';
                $arrOrderDirect['memo']                 = null;
                $arrOrderDirect['mail']                 = '';
                $arrOrderDirect['send_mail_flg']        = 0;
                $arrOrderDirect['send_mail_date']       = null;
                $arrOrderDirect['fax']                  = $fax;
                $arrOrderDirect['fax_flg']              = $faxFlg;
                $arrOrderDirect['send_fax_flg']         = 0;
                $arrOrderDirect['state_flg']            = 1;
                $arrOrderDirect['cancel_flg']           = 0;
                $arrOrderDirect['send_cancel_fax_flg']  = 0;
                $arrOrderDirect['send_cancel_fax_date'] = null;
                $arrOrderDirect['filmaker_flg']         = 1;
                $arrOrderDirect['del_flg']              = 0;
                $arrOrderDirect['valid_flg']            = 1;
                $arrOrderDirect['created_at']           = now();
                $arrOrderDirect['updated_at']           = now();
                DB::transaction(function () use ($modelOrderDir, $modelSupplier, $arrOrderDirect, $data) {
                    $modelOrderDir->insert($arrOrderDirect);
                    $modelSupplier->updateData($data->order_code, ['process_status' => 1], $data->edi_order_code);
                }, 5);
                $orderSuccess++;
            }
            Log::info("Insert to t_customer success: $cusSuccess and fail: $cusFail records.");
            print_r("Insert to t_customer success: $cusSuccess and fail: $cusFail records." . PHP_EOL);
            Log::info("Insert to t_order_direct success: $orderSuccess records.");
            print_r("Insert to t_order_direct success: $orderSuccess records." . PHP_EOL);
        }
        Log::info('End insert t_order_direct');
        print_r("End insert t_order_direct" . PHP_EOL);
    }
}
