<?php
/**
 * Batch process update mail urgent
 *
 * @package    App\Console\Commands
 * @subpackage ProcessCreateMailUrgent
 * @copyright  Copyright (c) 2019 RiverCrane! Corporation. All Rights Reserved.
 * @author     Lam vinh<lam.vinha@rivercrane.vn>
 */

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\Batches\MstOrder;
use Illuminate\Support\Facades\Log;
use App\Notifications\SlackNotification;
use App\Custom\Utilities;
use App\Notification;
use App\Events\Command as eCommand;
use Event;

class ProcessCreateMailUrgent extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'process:create-mail-urgent';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update mail to process send mail in case urgent';

    /**
     * The error.
     *
     * @var array
     */
    public $error = [];

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //Create log
        $arrayReplace = [':', '-'];
        $folder       = str_replace($arrayReplace, '_', $this->signature);
        Log::useDailyFiles(storage_path() . "/logs/$folder/history.log");
        //Process data
        Event::fire(new eCommand($this->signature, array('start' => true)));
        Log::info('Start batch process update mail urgent.');
        print_r("Start batch process update mail urgent." . PHP_EOL);
        $start      = microtime(true);
        $slack      = new Notification(CHANNEL['horunba']);
        $modelOrder = new MstOrder();
        $total      = $modelOrder->updateReviewMailUrgent();
        Log::info("Insert success : $total");
        print_r("Insert success : $total" . PHP_EOL);
        Event::fire(new eCommand($this->signature, array('end' => true, 'error' => $this->error)));
        $totalTime = round(microtime(true) - $start, 2);
        Log::info("End batch process update mail urgent with total time: $totalTime s.");
        print_r("End batch process update mail urgent with total time: $totalTime s.");
    }
}
