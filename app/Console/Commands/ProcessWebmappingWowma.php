<?php
/**
 * Process web mapping Wowma
 *
 * @package    App\Console\Commands
 * @subpackage ProcessWebMappingWowma
 * @copyright  Copyright (c) 2019 RiverCrane! Corporation. All Rights Reserved.
 * @author     Van Qui<van.qui@rivercrane.vn>
 */

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\Batches\MstOrder;
use App\Models\Batches\DtWebMapping;
use App\Models\Batches\DtWebWowData;
use App\Models\Batches\MstPaymentMethod;
use Illuminate\Support\Facades\Log;
use App\Events\Command as eCommand;
use Event;
use Config;
use App;
use App\Notifications\SlackNotification;
use App\Notification;

class ProcessWebMappingWowma extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'process:web-mapping-wowma';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';
    /**
     * The notification.
     *
     * @var object
     */
    protected $slack;
    /**
     * The error.
     *
     * @var array
     */
    public $error = [];

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $arrayReplace = [':', '-'];
        $folder       = str_replace($arrayReplace, '_', $this->signature);
        Log::useDailyFiles(storage_path() . "/logs/$folder/history.log");
        //Process data
        Event::fire(new eCommand($this->signature, array('start' => true)));
        $startBatch = microtime(true);
        $this->slack = new Notification(CHANNEL['horunba']);
        Log::info('Start batch process web mapping for Wowma.');
        print_r("Start batch process web mapping for Wowma." . PHP_EOL);

        $modelO   = new MstOrder();
        $modelWow = new DtWebWowData();
        $modelMap = new DtWebMapping();
        $modelPay = new MstPaymentMethod();
        //Get Order to save to dt_web_wow_data
        $dataOrder  = $modelO->getOrderAPIWebMapping(10, 1000);
        $totalStep1 = 0;
        foreach ($dataOrder as $order) {
            $dataInsert = $this->callApiGetOrder($order);
            if (count($dataInsert) > 0) {
                $modelWow->insert($dataInsert);
                $totalStep1++;
            }
        }
        $message = "Insert dt_web_wow_data success: $totalStep1 records.";
        Log::info($message);
        print_r($message . PHP_EOL);
        //End
        //Process get data api to check is correct
        $orderCorrect = $modelO->getOrderPayCorrect(10);
        $totalCount = 0;
        foreach ($orderCorrect as $order) {
            $data = $this->callApiGetOrder($order);
            switch ($data['status']) {
                case '新規受付':
                    $WowOrderStatus = 0;
                    break;
                case '完了':
                    $WowOrderStatus = 8;
                    break;
                case '保留':
                    $WowOrderStatus = 11;
                    break;
                case '発送待ち':
                    $WowOrderStatus = 7;
                    break;
                case '発送前入金待ち':
                case '与信待ち':
                    $WowOrderStatus = 4;
                    break;
                case 'キャンセル':
                    $WowOrderStatus = 10;
                    break;
            }
            $checkOrder = $modelO->checkOrderMapping($data['order_number'], $WowOrderStatus);
            if ($checkOrder === 1) {
                $modelMap->updateData($data['order_number'], $WowOrderStatus);
                $totalCount++;
            }
        }
        $message = "Update dt_web_mapping success: $totalCount records.";
        Log::info($message);
        print_r($message . PHP_EOL);
        //End
        $totalStep2 = 0;
        $dataWow     = $modelWow->getDataProcessWeb();
        $dataPayment = $modelPay->getDataByMall(10);
        foreach ($dataWow as $data) {
            $paymentMethod = 0;
            foreach ($dataPayment as $pay) {
                if ($data->settlement_name === $pay->payment_name) {
                    $paymentMethod = $pay->payment_code;
                    break;
                }
            }
            $webOrderStatus = 1;
            switch ($data->status) {
                case '新規受付':
                    $webOrderStatus = 0;
                    break;
                case '完了':
                    $webOrderStatus = 8;
                    break;
                case '保留':
                    $webOrderStatus = 11;
                    break;
                case '発送待ち':
                    $webOrderStatus = 7;
                    break;
                case '発送前入金待ち':
                case '与信待ち':
                    $webOrderStatus = 4;
                    break;
                case 'キャンセル':
                    $webOrderStatus = 10;
                    break;
            }
            $diffType = [];
            if ($data->payment_method !== $paymentMethod) {
                $diffType[] = '支払方法';
            }
            if ($data->order_request_price !== $data->request_price && $data->order_status !== ORDER_STATUS['CANCEL']) {
                $diffType[] = 'WEB金額';
            }
            if ($data->order_status !== $webOrderStatus) {
                $diffType[] = 'ステータス';
            }
            $diffPrice  = $data->order_request_price - $data->request_price;
            $finishDate = null;
            $isCorrected = 0;
            if (empty($diffType)) {
                $finishDate  = date('Y-m-d H:i:s');
                $isCorrected = 1;
            }
            if ($data->payment_method === 10 && $diffPrice === 0
                && $data->order_status === 8 && $webOrderStatus === 8) {
                $finishDate  = date('Y-m-d H:i:s');
                $isCorrected = 1;
            }

            $arrInsert = [
                'received_order_id'  => $data->order_number,
                'mall_id'            => 10,
                'web_payment_method' => $paymentMethod,
                'web_order_status'   => $webOrderStatus,
                'web_request_price'  => $data->request_price,
                'different_type'     => implode("\n", $diffType),
                'different_price'    => $diffPrice,
                'occorred_reason'    => 0,
                'process_content'    => 0,
                'finished_date'      => $finishDate,
                'is_corrected'       => $isCorrected,
                'is_deleted'         => 0,
                'receive_id'         => 0,
                'in_ope_cd'          => 'OPE99999',
                'in_date'            => date('Y-m-d H:i:s'),
                'up_ope_cd'          => 'OPE99999',
                'up_date'            => date('Y-m-d H:i:s')
            ];
            $modelMap->insertIgnore($arrInsert);
            $modelWow->where('order_number', $data->order_number)
                ->update(['process_flg' => 1]);
            $totalStep2++;
        }
        $message = "Insert dt_web_mapping success: $totalStep2 records.";
        Log::info($message);
        print_r($message . PHP_EOL);
        Event::fire(new eCommand($this->signature, array('end' => true, 'error' => $this->error)));
        $totalTime = round(microtime(true) - $startBatch, 2);
        Log::info("End batch process web mapping for Wowma with total time: $totalTime s.");
        print_r("End batch process web mapping for Wowma with total time: $totalTime s.");
    }

    /**
     * call api get order wowma
     *
     * @param object $order
     * @return array $dataInsert
     */
    private function callApiGetOrder($order)
    {
        $environment = 'real';
        if (App::environment(['local', 'test'])) {
            $environment = 'test';
        }
        $shopId    = Config::get("wowma.$environment.shop_id");
        $wowmaAuth = Config::get("wowma.$environment.auth_key");
        $urlApi    = Config::get("wowma.$environment.url");
        $wowmaHeader = array(
            "Content-Type: application/x-www-form-urlencoded",
            "Authorization: $wowmaAuth"
        );
        $wowmaUrl  = "{$urlApi}/searchTradeInfoProc?shopId={$shopId}&orderId={$order->received_order_id}";
        $ch = curl_init($wowmaUrl);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $wowmaHeader);
        $responseXml = curl_exec($ch);
        curl_close($ch);
        $responData = simplexml_load_string($responseXml);
        if ((int)$responData->result->status !== 0) {
            $errorCode = (string)$responData->result->error->code;
            $errorMess = (string)$responData->result->error->message;
            $message   = "Process webmapping Wowma order [{$order->received_order_id}] has error:"
                . PHP_EOL;
            $message .= "[$errorCode] $errorMess";
            print_r($message . PHP_EOL);
            Log::error($message);
            $error  = "------------------------------------------" . PHP_EOL;
            $error .= basename(__CLASS__) . PHP_EOL;
            $error .= PHP_EOL . $message;
            $error .= PHP_EOL . "------------------------------------------" . PHP_EOL;
            $this->slack->notify(new SlackNotification($error));
            $dataInsert = array();
        } else {
            $dataInsert = [
                'order_number'    => $order->received_order_id,
                'request_price'   => (string)$responData->orderInfo->requestPrice,
                'settlement_name' => (string)$responData->orderInfo->settlementName,
                'status'          => (string)$responData->orderInfo->orderStatus,
                'process_flg'     => 0,
                'is_delete'       => 0,
                'created_at'      => date('Y-m-d H:i:s')
            ];
        }
        return $dataInsert;
    }
}
