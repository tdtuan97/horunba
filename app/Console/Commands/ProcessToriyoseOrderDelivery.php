<?php
/**
 * Batch process Toriyose Order Delivery
 *
 * @package    App\Console\Commands
 * @subpackage ProcessToriyoseOrderDelivery
 * @copyright  Copyright (c) 2018 RiverCrane! Corporation. All Rights Reserved.
 * @author     Van Qui<van.qui@rivercrane.vn>
 */

namespace App\Console\Commands;

include app_path('lib/MarketplaceWebService/autoload.php');

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;
use App\Notifications\SlackNotification;
use App\Custom\Utilities;
use App\Notification;
use App\Events\Command as eCommand;
use Event;
use Config;
use Helper;
use DB;
use App;
use MarketplaceWebService_Client;
use MarketplaceWebService_Model_SubmitFeedRequest;
use MarketplaceWebService_Model_GetFeedSubmissionListRequest;
use MarketplaceWebService_Model_GetFeedSubmissionResultRequest;
use MarketplaceWebService_Exception;
use App\Models\Batches\MstOrder;

class ProcessToriyoseOrderDelivery extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'process:toriyose-order-delivery';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Process Toriyose Order Delivery';

    /**
     * The notification.
     *
     * @var object
     */
    protected $slack;

    /**
     * The error.
     *
     * @var array
     */
    public $error = [];

    /**
     * Limit process run
     *
     * @var int
     */
    public $limit = 1000;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //Create log
        $arrayReplace = [':', '-'];
        $folder       = str_replace($arrayReplace, '_', $this->signature);
        Log::useDailyFiles(storage_path() . "/logs/$folder/history.log");
        //Process data
        Event::fire(new eCommand($this->signature, array('start' => true)));
        Log::info('Start batch process Toriyose order delivery.');
        print_r("Start batch process Toriyose order delivery." . PHP_EOL);
        $start       = microtime(true);
        $this->slack = new Notification(CHANNEL['horunba']);

        $this->processDeliveryOrder();

        Event::fire(new eCommand($this->signature, array('end' => true, 'error' => $this->error)));
        $totalTime = round(microtime(true) - $start, 2);
        Log::info("End batch process Toriyose order delivery with total time: $totalTime s.");
        print_r("End batch process Toriyose order delivery with total time: $totalTime s.");
    }

    /**
     * Process order directly
     *
     * @return void
     */
    private function processDeliveryOrder()
    {
        Log::info("Start process delivery order.");
        print_r("Start process delivery order." . PHP_EOL);

        $isCallAPI = true;
        if (App::environment(['local', 'test'])) {
            $isCallAPI = false;
        }

        $modelO = new MstOrder();
        $data   = $modelO->getDataToriyoseOrderDelivery($this->limit)->toArray();
        if (count($data) === 0) {
            Log::info('No data.');
            print_r('No data.' . PHP_EOL);
            goto end;
        }

        $azOrderDeli = array();
        $arrCond     = array();
        $index       = 1;
        foreach ($data as $item) {
            if (!empty($item['delivery_plan_date'])) {
                $azOrderDeli[] = [
                    'message_id'         => $index,
                    'received_order_id'  => $item['received_order_id'],
                    'product_code'       => $item['product_code'],
                    'shipment_date_plan' => $item['delivery_plan_date'],
                ];
                $arrCond[$index] = $item['receive_id'];
                $index++;
            } else {
                $hour = date('H:i');
                if ($hour < '14:30') {
                    $deliveryDate = date('Ymd');
                } else {
                    $deliveryDate = date('Ymd', strtotime("+1 days"));
                }
                $azOrderDeli[] = [
                    'message_id'         => $index,
                    'received_order_id'  => $item['received_order_id'],
                    'product_code'       => $item['product_code'],
                    'shipment_date_plan' => $deliveryDate,
                ];
                $arrCond[$index] = $item['receive_id'];
                $index++;
/*                $modelO->updateData(
                    [$item['receive_id']],
                    [
                        'message_api'    => 'Delivery Plan Date is required',
                        'error_code_api' => 'Fail'
                    ]
                );*/
            }
        }
        $this->updateAmazon($azOrderDeli, $arrCond, $isCallAPI);

        end:
        Log::info("End process delivery order.");
        print_r("End process delivery order." . PHP_EOL);
    }

    /**
     * Update amazon
     *
     * @param  array   $azOrderDeli
     * @param  array   $arrCond
     * @param  boolean $isCallAPI
     * @return void
     */
    private function updateAmazon($azOrderDeli, $arrCond, $isCallAPI)
    {
        $modelO     = new MstOrder();
        $receiveIds = array_unique($arrCond);
        $dataUpdate = [
            'up_ope_cd'             => 'OPE99999',
            'up_date'               => now()
        ];
        if (!$isCallAPI) {
            $dataUpdate['is_sourcing_on_demand'] = 2;
            $dataUpdate['is_mail_sent']   = 1;
            $dataUpdate['message_api']    = '_DONE_';
            $dataUpdate['error_code_api'] = '_DONE_';
            $modelO->updateData($receiveIds, $dataUpdate);
            echo "Process success" . PHP_EOL;
            Log::info("Process success");
            return true;
        }
        $feedType   = '_POST_EXPECTED_SHIP_DATE_SOD_';
        $config     = Config::get('amazonmws');
        $configHost = array(
            'ServiceURL'    => $config['ServiceURL'],
            'ProxyHost'     => $config['ProxyHost'],
            'ProxyPort'     => $config['ProxyPort'],
            'MaxErrorRetry' => $config['MaxErrorRetry'],
        );
        $service    = new MarketplaceWebService_Client(
            $config['AWS_ACCESS_KEY_ID'],
            $config['AWS_SECRET_ACCESS_KEY'],
            $configHost,
            $config['APPLICATION_NAME'],
            $config['APPLICATION_VERSION']
        );

        $params = [
            'data'               => $azOrderDeli,
            'merchantIdentifier' => $config['MERCHANT_ID']
        ];

        $feed = view('api.xml.amazon_delivery_plan', $params)->render();

        //***************** SubmitFeed ****************
        $feedSubmissionInfo = $this->submitFeed($feedType, $feed, $config, $service);
        if ($feedSubmissionInfo['status'] === false) {
            $dataUpdate['message_api']    = $feedSubmissionInfo['data'];
            $dataUpdate['error_code_api'] = 'Fail';
            $modelO->updateData($receiveIds, $dataUpdate);
            print_r($feedSubmissionInfo['data'] . PHP_EOL);
            Log::error($feedSubmissionInfo['data']);
            $this->postSlackAmazon($feedSubmissionInfo['data']);
            return false;
        }

        if (!$feedSubmissionInfo['data']->isSetFeedSubmissionId()) {
            $dataUpdate['message_api']    = 'No FeedSubmissionId';
            $dataUpdate['error_code_api'] = 'Fail';
            $modelO->updateData($receiveIds, $dataUpdate);
            print_r("No FeedSubmissionId" . PHP_EOL);
            Log::error("No FeedSubmissionId");
            $this->postSlackAmazon("No FeedSubmissionId");
            return false;
        }
        $feedSubmissionId     = $feedSubmissionInfo['data']->getFeedSubmissionId();
        $feedSubmissionIdList = [$feedSubmissionId];
        //***************** GetFeedSubmissionList ****************
        //Do until DONE
        while (true) {
            sleep(80);
            $getFeedSubmissionListResult = $this->getFeedSubmissionList($feedSubmissionIdList, $config, $service);
            if ($getFeedSubmissionListResult['status'] === false) {
                $dataUpdate['message_api']    = $getFeedSubmissionListResult['data'];
                $dataUpdate['error_code_api'] = 'Fail';
                $modelO->updateData($receiveIds, $dataUpdate);
                print_r($getFeedSubmissionListResult['data'] . PHP_EOL);
                Log::error($getFeedSubmissionListResult['data']);
                $this->postSlackAmazon($getFeedSubmissionListResult['data']);
                return false;
            }
            $feedSubmissionInfoList = $getFeedSubmissionListResult['data']->getFeedSubmissionInfoList();
            $feedProcessingStatus   = false;
            foreach ($feedSubmissionInfoList as $feedSubmissionInfo) {
                if ($feedSubmissionInfo->isSetFeedProcessingStatus()) {
                    $feedProcessingStatus = $feedSubmissionInfo->getFeedProcessingStatus();
                    break;
                }
            }
            if (!$feedProcessingStatus) {
                $dataUpdate['message_api']    = 'No FeedProcessingStatus';
                $dataUpdate['error_code_api'] = 'Fail';
                $modelO->updateData($receiveIds, $dataUpdate);
                print_r('No FeedProcessingStatus' . PHP_EOL);
                Log::error('No FeedProcessingStatus');
                $this->postSlackAmazon('No FeedProcessingStatus');
                return false;
            }
            $arrStatusFinish = [
                '_DONE_',
                '_AWAITING_ASYNCHRONOUS_REPLY_',
                '_CANCELLED_',
                '_IN_SAFETY_NET_'
            ];

            if (in_array($feedProcessingStatus, $arrStatusFinish)) {
                $dataUpdate['message_api']    = $feedProcessingStatus;
                $dataUpdate['error_code_api'] = $feedProcessingStatus;
                $modelO->updateData($receiveIds, $dataUpdate);
                break;
            }
        }

        // ***************** GetFeedSubmissionResult ****************
        $getFeedSubmissionResultResult = $this->getFeedSubmissionResult($feedSubmissionId, $config, $service);
        if ($getFeedSubmissionResultResult['status'] === false) {
            $dataUpdate['message_api']    = $getFeedSubmissionResultResult['data'];
            $dataUpdate['error_code_api'] = 'Fail';
            $modelO->updateData($receiveIds, $dataUpdate);
            $this->postSlackAmazon($getFeedSubmissionResultResult['data']);
            return false;
        }
        $responseXML = $getFeedSubmissionResultResult['data'];
        $errorReceiveId = [];
        if (!empty($responseXML->Message->ProcessingReport->Result)) {
            $errAzMess = [];
            foreach ($responseXML->Message->ProcessingReport->Result as $item) {
                if ((string)$item->ResultCode === 'Error') {
                    $messageId         = (string)$item->MessageID;
                    $receiveId         = $arrCond[$messageId];
                    $errorReceiveId[]  = $receiveId;
                    $resultDescription = (string)$item->ResultDescription;
                    $resultMessageCode = (string)$item->ResultMessageCode;
                    $dataUpdate['message_api']    = $resultDescription;
                    $dataUpdate['error_code_api'] = $resultMessageCode;
                    $modelO->updateData([$receiveId], $dataUpdate);
                    $errAzMess[] = "Receive ID $receiveId (Code $resultMessageCode): $resultDescription";
                }
            }
            if (count($errAzMess) > 0) {
                $this->postSlackAmazon(implode("\n", $errAzMess));
            }
        }

        $receiveIds = array_diff($receiveIds, $errorReceiveId);
        if (count($receiveIds) > 0) {
            $dataUpSucc['is_sourcing_on_demand'] = 2;
            $dataUpSucc['is_mail_sent']          = 1;
            $modelO->updateData($receiveIds, $dataUpSucc);
        }

        echo "Process success" . PHP_EOL;
        Log::info("Process success");
        return true;
    }
    
    /**
     * Post message amazon to slack
     *
     * @param  string $message
     * @return void
     */
    public function postSlackAmazon($message)
    {
        $error  = "------------------------------------------" . PHP_EOL;
        $error .= 'Process Toriyose delivery fail:' . PHP_EOL;
        $error .= $message;
        $error .= PHP_EOL . "------------------------------------------" . PHP_EOL;
        $this->slack->notify(new SlackNotification($error));
    }

    /**
     * Submit feed
     *
     * @param  string   $feedType   Feed type
     * @param  string   $feed       Feed data
     * @param  array    $config     Config service
     * @param  object   $service    Service object
     * @return mixed
     */
    private function submitFeed($feedType, $feed, $config, $service)
    {
        $feedHandle = @fopen('php://temp', 'rw+');
        fwrite($feedHandle, $feed);
        rewind($feedHandle);
        $parameters = [
            'Merchant'        => $config['MERCHANT_ID'],
            'FeedType'        => $feedType,
            'FeedContent'     => $feedHandle,
            'PurgeAndReplace' => false,
            'ContentMd5'      => base64_encode(md5(stream_get_contents($feedHandle), true)),
        ];

        rewind($feedHandle);

        $request    = new MarketplaceWebService_Model_SubmitFeedRequest($parameters);
        $returnData = [
            'status' => false,
            'data'   => 'No reason'
        ];
        try {
            $response = $service->submitFeed($request);
            if ($response->isSetSubmitFeedResult()) {
                $submitFeedResult = $response->getSubmitFeedResult();
                if ($submitFeedResult->isSetFeedSubmissionInfo()) {
                    $returnData = [
                        'status' => true,
                        'data'   => $submitFeedResult->getFeedSubmissionInfo()
                    ];
                } else {
                    $returnData = [
                        'status' => false,
                        'data'   => 'No FeedSubmissionInfo'
                    ];
                }
            } else {
                $returnData = [
                    'status' => false,
                    'data'   => 'No SubmitFeedResult'
                ];
            }
        } catch (MarketplaceWebService_Exception $ex) {
            echo("Caught Exception: " . $ex->getMessage() . "\n");
            echo("Response Status Code: " . $ex->getStatusCode() . "\n");
            echo("Error Code: " . $ex->getErrorCode() . "\n");
            echo("Error Type: " . $ex->getErrorType() . "\n");
            echo("Request ID: " . $ex->getRequestId() . "\n");
            echo("XML: " . $ex->getXML() . "\n");
            echo("ResponseHeaderMetadata: " . $ex->getResponseHeaderMetadata() . "\n");

            Log::error("Caught Exception: " . $ex->getMessage());
            Log::error("Response Status Code: " . $ex->getStatusCode());
            Log::error("Error Code: " . $ex->getErrorCode());
            Log::error("Error Type: " . $ex->getErrorType());
            Log::error("Request ID: " . $ex->getRequestId());
            Log::error("XML: " . $ex->getXML());
            Log::error("ResponseHeaderMetadata: " . $ex->getResponseHeaderMetadata());

            $returnData = [
                'status' => false,
                'data'   => !empty($ex->getErrorCode())?$ex->getErrorCode():$ex->getStatusCode()
            ];
        }
        @fclose($feedHandle);
        return $returnData;
    }

    /**
     * Get feed submission list
     *
     * @param   array   $feedSubmissionIdList
     * @param   array   $config
     * @param   object  $service
     * @return  mixed
     */
    private function getFeedSubmissionList($feedSubmissionIdList, $config, $service)
    {
        $parameters = [
            'Merchant'                 => $config['MERCHANT_ID'],
            'FeedSubmissionIdList'     => ['Id' => $feedSubmissionIdList],
            'FeedProcessingStatusList' => ['Status' => ['_SUBMITTED_']]
        ];

        $request = new MarketplaceWebService_Model_GetFeedSubmissionListRequest($parameters);

        $returnData = [
            'status' => false,
            'data'   => 'No reason'
        ];
        try {
            $response = $service->getFeedSubmissionList($request);
            if ($response->isSetGetFeedSubmissionListResult()) {
                $returnData = [
                    'status' => true,
                    'data'   => $response->getGetFeedSubmissionListResult()
                ];
            } else {
                $returnData = [
                    'status' => false,
                    'data'   => 'No GetFeedSubmissionListResult'
                ];
            }
        } catch (MarketplaceWebService_Exception $ex) {
            echo("Caught Exception: " . $ex->getMessage() . "\n");
            echo("Response Status Code: " . $ex->getStatusCode() . "\n");
            echo("Error Code: " . $ex->getErrorCode() . "\n");
            echo("Error Type: " . $ex->getErrorType() . "\n");
            echo("Request ID: " . $ex->getRequestId() . "\n");
            echo("XML: " . $ex->getXML() . "\n");
            echo("ResponseHeaderMetadata: " . $ex->getResponseHeaderMetadata() . "\n");

            Log::error("Caught Exception: " . $ex->getMessage());
            Log::error("Response Status Code: " . $ex->getStatusCode());
            Log::error("Error Code: " . $ex->getErrorCode());
            Log::error("Error Type: " . $ex->getErrorType());
            Log::error("Request ID: " . $ex->getRequestId());
            Log::error("XML: " . $ex->getXML());
            Log::error("ResponseHeaderMetadata: " . $ex->getResponseHeaderMetadata());

            $returnData = [
                'status' => false,
                'data'   => !empty($ex->getErrorCode())?$ex->getErrorCode():$ex->getStatusCode()
            ];
        }
        return $returnData;
    }

    /**
     * Get feed submission result
     *
     * @param  string  $feedSubmissionId
     * @param  array   $config
     * @param  object  $service
     * @return object
     */
    private function getFeedSubmissionResult($feedSubmissionId, $config, $service)
    {
        $parameters = [
            'Merchant'             => $config['MERCHANT_ID'],
            'FeedSubmissionId'     => $feedSubmissionId,
            'FeedSubmissionResult' => @fopen('php://memory', 'rw+')
        ];

        $request    = new MarketplaceWebService_Model_GetFeedSubmissionResultRequest($parameters);
        $fileHandle = fopen('php://memory', 'rw+');
        $request->setFeedSubmissionResult($fileHandle);

        $returnData = [
            'status' => false,
            'data'   => 'No reason'
        ];
        try {
            $response = $service->getFeedSubmissionResult($request);
            rewind($fileHandle);
            $responseStr = stream_get_contents($fileHandle);
            $responseXML = new \SimpleXMLElement($responseStr);
            $returnData = [
                'status' => true,
                'data'   => $responseXML
            ];
        } catch (MarketplaceWebService_Exception $ex) {
            echo("Caught Exception: " . $ex->getMessage() . "\n");
            echo("Response Status Code: " . $ex->getStatusCode() . "\n");
            echo("Error Code: " . $ex->getErrorCode() . "\n");
            echo("Error Type: " . $ex->getErrorType() . "\n");
            echo("Request ID: " . $ex->getRequestId() . "\n");
            echo("XML: " . $ex->getXML() . "\n");
            echo("ResponseHeaderMetadata: " . $ex->getResponseHeaderMetadata() . "\n");

            Log::error("Caught Exception: " . $ex->getMessage());
            Log::error("Response Status Code: " . $ex->getStatusCode());
            Log::error("Error Code: " . $ex->getErrorCode());
            Log::error("Error Type: " . $ex->getErrorType());
            Log::error("Request ID: " . $ex->getRequestId());
            Log::error("XML: " . $ex->getXML());
            Log::error("ResponseHeaderMetadata: " . $ex->getResponseHeaderMetadata());

            $returnData = [
                'status' => false,
                'data'   => !empty($ex->getErrorCode())?$ex->getErrorCode():$ex->getStatusCode()
            ];
        }
        return $returnData;
    }
}
