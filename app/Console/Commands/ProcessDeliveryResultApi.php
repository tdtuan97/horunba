<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Config;
use App;
use DB;
use SoapClient;
use App\Notification;
use App\Notifications\SlackNotification;
use App\Models\Batches\DtReceiptLedger;
use App\Models\Batches\MstProduct;
use App\Models\Batches\MstOrder;
use App\Models\Batches\DtDelivery;
use App\Models\Batches\DtDeliveryDetail;
use App\Models\Batches\MstStockStatus;
use App\Models\Batches\TInsertShippingGroupKey;
use App\Models\Batches\LogTShippingPerformanceModel;
use Illuminate\Support\Facades\Log;
use App\Events\Command as eCommand;
use Event;

class ProcessDeliveryResultApi extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'process:delivery-result-api';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }


    /**
     * The error.
     *
     * @var array
     */
    public $error = [];

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //Create log
        $arrayReplace = [':', '-'];
        $folder       = str_replace($arrayReplace, '_', $this->signature);
        Log::useDailyFiles(storage_path() . "/logs/$folder/history.log");
        //Process data
        Event::fire(new eCommand($this->signature, array('start' => true)));
        Log::info('Start batch process delivery result by API.');
        print_r("Start batch process delivery result by API." . PHP_EOL);
        $start       = microtime(true);
        $this->slack = new Notification(CHANNEL['horunba']);
        $modelDel    = new DtDelivery();
        $modelP      = new MstProduct();
        if (App::environment(['local', 'test'])) {
            $auth = Config::get("apiservices.glsc_test");
        } else {
            $auth = Config::get("apiservices.glsc");
        }
        $glscUrl = "http://gls.ec-union.biz/glsc/glscapi/GlscApi.asmx?WSDL";
        $arrKey  = [[1, 9], [3, 7]];
        $arrKeyUpdated = [];
        $arrDataDelivery = [];
        foreach ($arrKey as $dataKb) {
            try {
                $client  = new SoapClient($glscUrl);
                $params  = [
                    'Auth' => [
                        'LoginID'     => $auth['LoginID'],
                        'Password'    => $auth['Password'],
                        'DeveloperID' => $auth['DeveloperID'],
                    ],
                    'Key' => [
                        'IncrementalOnly' => true,
                        'DeliDateFrom'    => date('Ymd'),
                        'DeliDateTo'      => date('Ymd'),
                        'DeliveryFlg'     => 0,
                        'DataKb'          => $dataKb
                    ],
                    'Shipping' => [
                        'DataKb'       => '',
                        'OrderNo'      => '',
                        'OrderBNo'     => '',
                        'OrderLNo'     => '',
                        'DeliDate'     => '',
                        'InquiryNo'    => '',
                        'CustomerNm'   => '',
                        'DNm'          => '',
                        'DPostal'      => '',
                        'DPref'        => '',
                        'DAddress1'    => '',
                        'DAddress2'    => '',
                        'DTel'         => '',
                        'DConKb'       => '',
                        'ItemCd'       => '',
                        'ItemNm'       => '',
                        'JanCd'        => '',
                        'PlanQuantity' => '',
                        'Quantity'     => '',
                        'Note'         => '',
                        'DeliveryCode' => '',
                        'ShipNum'      => '',
                    ]
                ];
                $temp     = '[' . implode(", ", $dataKb) . ']';
                $response = $client->getShippingPerformanceList($params);
                $errInfo  = $response->getShippingPerformanceListResult->ErrInfo;
                $shipping = $response->Shipping;
                if (isset($errInfo->ErrorCd) && $errInfo->ErrorCd === 0) {
                    if (!empty($shipping->tShippingPerformanceModel)) {
                        $listData = $shipping->tShippingPerformanceModel;
                        $success  = 0;
                        if (is_array($listData)) {
                            foreach ($listData as $item) {
                                $success += $this->processDataResponse($item, $arrKeyUpdated, $arrDataDelivery);
                                $this->insertLogData($item);
                            }
                        } else {
                            $success += $this->processDataResponse($listData, $arrKeyUpdated, $arrDataDelivery);
                            $this->insertLogData($listData);
                        }
                        $message = "Process $temp -> Update success: $success records";
                        Log::info($message);
                        print_r($message . PHP_EOL);
                    } else {
                        $message = "Process $temp -> No data";
                        Log::info($message);
                        print_r($message . PHP_EOL);
                    }
                } else {
                    $message = "Process $temp -> No data";
                    Log::info($message);
                    print_r($message . PHP_EOL);
                }
            } catch (\Exception $e) {
                report($e);
            }
        }
        if (count($arrKeyUpdated) !== 0) {
            $modelDD = new DtDeliveryDetail();
            foreach ($arrKeyUpdated as $value) {
                $numberCheck = $modelDD->countDetailByKey($value['OrderNo'], $value['OrderBNo']);
                if ($value['number'] !== $numberCheck) {
                    $error  = "------------------------------------------" . PHP_EOL;
                    $error .= basename(__CLASS__) . PHP_EOL;
                    $error .= "Delivery {$value['OrderNo']} - {$value['OrderBNo']} have detail not enough";
                    $error .= PHP_EOL . "------------------------------------------" . PHP_EOL;
                    $this->slack->notify(new SlackNotification($error));
                }
            }
        }
        Event::fire(new eCommand($this->signature, array('end' => true, 'error' => $this->error)));
        $totalTime = round(microtime(true) - $start, 2);
        Log::info("End batch process delivery result by API with total time: $totalTime s.");
        print_r("End batch process delivery result by API with total time: $totalTime s.");
    }

    /**
     * Process data response
     *
     * @return void
     */
    public function processDataResponse($item, &$arrKeyUpdated, &$arrDataDelivery)
    {
        $modelDel   = new DtDelivery();
        $modelDD    = new DtDeliveryDetail();
        $modelStock = new MstStockStatus();
        $modelDRL   = new DtReceiptLedger();
        $modelKey   = new TInsertShippingGroupKey();
        $modelP     = new MstProduct();
        $modelOrder = new MstOrder();
        $arrUpD     = [];
        $arrUpOrder = [];
        $arrUpDD    = [];
        $tempKey    = $item->OrderNo . $item->OrderBNo;
        if (!isset($arrKeyUpdated[$tempKey])) {
            $arrKeyUpdated[$tempKey] = [
                'OrderNo'  => $item->OrderNo,
                'OrderBNo' => $item->OrderBNo,
                'OrderLNo' => [$item->OrderLNo],
                'number'   => 1,
            ];
        } else {
            if (!in_array($item->OrderLNo, $arrKeyUpdated[$tempKey]['OrderLNo'])) {
                $arrKeyUpdated[$tempKey]['number']     += 1;
                $arrKeyUpdated[$tempKey]['OrderLNo'][] = $item->OrderLNo;
            }
        }
        $dataP    = $modelP->getDataByProductCode($item->ItemCd);
        if (isset($arrDataDelivery[$tempKey])) {
            $deliInfo = $arrDataDelivery[$tempKey]['info'];
            $flg      = $arrDataDelivery[$tempKey]['flg'];
        } else {
            $deliInfo = $modelDel->getDataCheckUpdateDeliResult($item->OrderNo, $item->OrderBNo);
            $flg      = false;
            $arrDataDelivery[$tempKey]['info'] = $deliInfo;
            $arrDataDelivery[$tempKey]['flg']  = $flg;
        }
        if (empty($dataP) || empty($deliInfo)) {
            Log::info($tempKey . $item->ItemCd);
            $error  = "------------------------------------------" . PHP_EOL;
            $error .= basename(__CLASS__) . PHP_EOL;
            if (empty($dataP)) {
                $error .= "{$item->OrderNo} - {$item->OrderBNo} 出荷実績結果が届けましたが、HRNB.mst_productに「{$item->ItemCd}」品番が存在しない。";
            } else {
                $error .= "{$item->OrderNo} - {$item->OrderBNo} 出荷実績結果が届けましたが、出荷指示テーブルにデータがない。";
            }
            $error .= PHP_EOL . "------------------------------------------" . PHP_EOL;
            $this->slack->notify(new SlackNotification($error));
            return 0;
        }
        if ($deliInfo->delivery_status === 8) {
            $error  = "------------------------------------------" . PHP_EOL;
            $error .= basename(__CLASS__) . PHP_EOL;
            $error .= "{$item->OrderNo} - {$item->OrderBNo} 出荷指示キャンセルしましたが、まだ出荷済みの結果を返ってきます。";
            $error .= PHP_EOL . "------------------------------------------" . PHP_EOL;
            $this->slack->notify(new SlackNotification($error));
        }
        if (in_array($item->DataKb, ['1', '9'])) {
            if ($item->Quantity === $item->PlanQuantity) {
                $arrUpD['delivery_status']     = 2;
                $arrUpD['delivery_code']       = $item->DeliveryCode;
                $arrUpD['inquiry_no']          = $item->InquiryNo;
                $arrUpD['delivery_real_date']  = $item->DeliDate;
                $arrUpDD['delivery_real_date'] = $item->DeliDate;
                $arrUpDD['delivery_real_num']  = $item->Quantity;
                $arrDataDelivery[$tempKey]['flg'] = true;
            } else {
                if ($item->DataKb === '9') {
                    $arrUpD['delivery_status']    = 8;
                    $arrUpD['delivery_plan_date'] = date('Ymd', strtotime("+1 days"));
                    $arrUpOrder['shipment_date']  = date('Ymd', strtotime("+1 days"));
                } else {
                    $arrUpD['delivery_status'] = 9;
                }
            }
            if ($item->DataKb === '1') {
                $modelDRL->add2ReceiptLedgerOutStock([
                    'pa_order_code'        => $item->OrderNo,
                    'pa_product_code'      => $item->ItemCd,
                    'pa_price_invoice'     => $dataP->price_invoice,
                    'pa_delivery_num'      => $item->Quantity,
                    'pa_supplier_id'       => $dataP->price_supplier_id,
                    'pa_stock_type'        => 1,
                    'pa_stock_detail_type' => 1,
                ]);
                if ($deliInfo->in_part_cd === 0) {
                    $modelStock->updateStockOutStock(
                        $item->ItemCd,
                        $item->Quantity
                    );
                } else {
                    $modelStock->updateStockOutStockR(
                        $item->ItemCd,
                        $item->Quantity,
                        true
                    );
                }

            }
        } elseif ($item->DataKb === '7') {
            $arrUpD['inquiry_no']    = $item->InquiryNo;
            $arrUpD['delivery_code'] = $item->DeliveryCode;
        } elseif ($item->DataKb === '3') {
            if ($deliInfo->delivery_status === 1 && !$flg) {
                $error  = "------------------------------------------" . PHP_EOL;
                $error .= basename(__CLASS__) . PHP_EOL;
                $error .= "Delivery {$item->OrderNo} - {$item->OrderBNo} have status <> 2";
                $error .= PHP_EOL . "------------------------------------------" . PHP_EOL;
                $this->slack->notify(new SlackNotification($error));
            } else {
                if ((int)$item->Quantity < 0) {
                    $arrUpD['delivery_status']     = 5;
                    $arrUpDD['delivery_real_date'] = $item->DeliDate;
                    $arrUpDD['delivery_real_num']  = $item->Quantity;

                    $modelDRL->add2ReceiptLedgerOutStock([
                        'pa_order_code'        => $item->OrderNo,
                        'pa_product_code'      => $item->ItemCd,
                        'pa_price_invoice'     => $dataP->price_invoice,
                        'pa_delivery_num'      => $item->Quantity,
                        'pa_supplier_id'       => $dataP->price_supplier_id,
                        'pa_stock_type'        => 1,
                        'pa_stock_detail_type' => 8,
                    ]);
                    if ($deliInfo->in_part_cd === 0) {
                        $modelStock->updateStockOutStock(
                            $item->ItemCd,
                            $item->Quantity
                        );
                    } else {
                        $modelStock->updateStockOutStockR(
                            $item->ItemCd,
                            $item->Quantity,
                            true
                        );
                    }
                }
            }
        }
        if (!empty($arrUpOrder)) {
            $arrUpOrder['up_ope_cd'] = 'OPE99999';
            $arrUpOrder['up_date']   = now();
            $modelOrder->updateData(
                [
                    'received_order_id' => $item->OrderNo,
                ],
                $arrUpOrder
            );
        }
        if (!empty($arrUpD)) {
            $arrUpD['up_ope_cd'] = 'OPE99999';
            $arrUpD['up_date']   = now();
            $modelDel->updateData(
                [
                    'received_order_id' => $item->OrderNo,
                    'subdivision_num'   => $item->OrderBNo,
                ],
                $arrUpD
            );
        }
        if (!empty($arrUpDD)) {
            $arrUpDD['up_ope_cd'] = 'OPE99999';
            $arrUpDD['up_date']   = now();
            $modelDD->updateData(
                [
                    'received_order_id' => $item->OrderNo,
                    'subdivision_num'   => $item->OrderBNo,
                    'detail_line_num'   => $item->OrderLNo,
                ],
                $arrUpDD
            );
        }

        return 1;
    }

    /**
     * Process insert log
     *
     * @return void
     */
    public function insertLogData($item)
    {
        $modelSPM  = new LogTShippingPerformanceModel();
        $arrInsert = [];
        $arrInsert['DataKb']       = $item->DataKb;
        $arrInsert['OrderNo']      = $item->OrderNo;
        $arrInsert['OrderBNo']     = $item->OrderBNo;
        $arrInsert['OrderLNo']     = $item->OrderLNo;
        $arrInsert['DeliDate']     = $item->DeliDate;
        $arrInsert['InquiryNo']    = $item->InquiryNo;
        $arrInsert['CustomerNm']   = $item->CustomerNm;
        $arrInsert['DNm']          = $item->DNm;
        $arrInsert['DPostal']      = $item->DPostal;
        $arrInsert['DPref']        = $item->DPref;
        $arrInsert['DAddress1']    = $item->DAddress1;
        $arrInsert['DAddress2']    = $item->DAddress2;
        $arrInsert['DTel']         = $item->DTel;
        $arrInsert['DConKb']       = $item->DConKb;
        $arrInsert['ItemCd']       = $item->ItemCd;
        $arrInsert['ItemNm']       = $item->ItemNm;
        $arrInsert['JanCd']        = $item->JanCd;
        $arrInsert['PlanQuantity'] = $item->PlanQuantity;
        $arrInsert['Quantity']     = $item->Quantity;
        $arrInsert['Note']         = $item->Note;
        $arrInsert['DeliveryCode'] = $item->DeliveryCode;
        $arrInsert['ShipNum']      = $item->ShipNum;
        $modelSPM->insert($arrInsert);
    }
}
