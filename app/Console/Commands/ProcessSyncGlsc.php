<?php
/**
 * Batch process sync Glsc
 *
 * @package    App\Console\Commands
 * @subpackage ProcessSyncGlsc
 * @copyright  Copyright (c) 2018 RiverCrane! Corporation. All Rights Reserved.
 * @author     Van Qui<van.qui@rivercrane.vn>
 */

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;
use App\Notifications\SlackNotification;
use App\Custom\Utilities;
use App\Notification;
use App\Events\Command as eCommand;
use Event;
use App\Models\Batches\TItemDeliveryProcessesModel;
use App\Models\Batches\MstStockStatus;

class ProcessSyncGlsc extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'process:sync-glsc';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Process sync Glsc';

    /**
     * The error.
     *
     * @var array
     */
    public $error = [];

    private $sizeInsert = 500;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //Create log
        $arrayReplace = [':', '-'];
        $folder       = str_replace($arrayReplace, '_', $this->signature);
        Log::useDailyFiles(storage_path() . "/logs/$folder/history.log");
        //Process data
        Event::fire(new eCommand($this->signature, array('start' => true)));
        Log::info('Start batch process sync Glsc.');
        print_r("Start batch process sync Glsc." . PHP_EOL);
        $start    = microtime(true);
        $slack    = new Notification(CHANNEL['horunba']);

        $modelIDPM = new TItemDeliveryProcessesModel();
        $modelSS   = new MstStockStatus();

        $result = $modelIDPM->getDataGlsc();
        $totalUpdate = count($result);
        if (count($totalUpdate) === 0) {
            Log::info('No data.');
            print_r("No data." . PHP_EOL);
            goto end;
        }
        foreach ($result as $data) {
            $newNumGlsc =  ($data->new_glsc_num > 0) ? $data->new_glsc_num : 0;
            $modelSS->updateData(['product_code' => $data->product_code], ['glsc_num' => $newNumGlsc]);
        }
        Log::info("$totalUpdate records are updated to stock status");
        print_r("$totalUpdate records are updated to stock status" . PHP_EOL);
        end:
        Event::fire(new eCommand($this->signature, array('end' => true, 'error' => $this->error)));
        $totalTime = round(microtime(true) - $start, 2);
        Log::info("End batch process sync Glsc with total time: $totalTime s.");
        print_r("End batch process sync Glsc with total time: $totalTime s.");
    }
}
