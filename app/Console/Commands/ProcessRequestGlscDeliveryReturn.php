<?php
/**
 * Batch process request glsc delivery return
 *
 * @package    App\Console\Commands
 * @subpackage ProcessRequestGlscDeliveryReturn
 * @copyright  Copyright (c) 2018 RiverCrane! Corporation. All Rights Reserved.
 * @author     Truong Nghia<truong.van.nghia@rivercrane.vn>
 */
namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;
use App\Notifications\SlackNotification;
use App\Models\Batches\TInsertShippingGroupKey;
use App\Models\Batches\TInsertShipping;
use App\Models\Batches\DtReturn;
use App\Models\Batches\TAka;
use App\Models\Batches\DtReceiptLedger;
use App\Notification;
use App\Events\Command as eCommand;
use App\Custom\Utilities;
use Event;
use DB;

class ProcessRequestGlscDeliveryReturn extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'process:request-glsc-delivery-return';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'When stock is available, request to delivery to customer.';

    /**
     * The notification.
     *
     * @var object
     */
    protected $slack;

    /**
     * The error.
     *
     * @var array
     */
    public $error = [];

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $arrayReplace = [':', '-'];
        $folder       = str_replace($arrayReplace, '_', $this->signature);
        Log::useDailyFiles(storage_path() . "/logs/$folder/history.log");
        Event::fire(new eCommand($this->signature, array('start' => true)));
        $slack = new Notification(CHANNEL['horunba']);
        $start = microtime(true);
        Log::info('Start batch process GLSC delivery return.');
        print_r("Start batch process GLSC delivery return." . PHP_EOL);
        $modelDR = new DtReturn();
        $modelIS = new TInsertShipping();
        $data    = $modelDR->getDataDeliveryReturn();
        $message = '----Start process content 1----';
        Log::info($message);
        print_r($message . PHP_EOL);
        $count   = count($data);
        if (count($data) === 0) {
            Log::info('No data');
            print_r("No data. " . PHP_EOL);
        } else {
            $modelKey     = new TInsertShippingGroupKey();
            $suc          = 0;
            $fail         = 0;
            $receiveID    = '';
            $groupKey     = '';
            $arrUpAfterIn = [];
            $arrEnd       = [];
            $check        = false;
            $supplierId   = '';
            $orderLNo     = 1;
            $returnAddId  = '';
            $maxNo        = $modelIS->getMaxOrderReturn();
            $index        = 0;
            if (count($maxNo) === 0) {
                $index = 400000;
            } else {
                $arrTemp = explode('-', $maxNo->OrderNo);
                $index = $arrTemp[2];
            }
            try {
                DB::beginTransaction();
                DB::connection('glsc')->beginTransaction();
                foreach ($data as $value) {
                    $arrIns = [];
                    if ($receiveID !== $value->receive_id ||
                        $supplierId !== $value->supplier_id ||
                        $returnAddId !== $value->return_address_id
                    ) {
                        $index++;
                        $receiveID   = $value->receive_id;
                        $supplierId  = $value->supplier_id;
                        $returnAddId = $value->return_address_id;
                        $orderNo     = 'r-' . $value->supplier_id . '-' . $index;
                        $modelKey->where('tShippingG_Key', '=', 1)->increment('GroupKey', 1);
                        $groupKey = $modelKey->where('tShippingG_Key', '=', 1)->first();
                        if (count($arrEnd) !== 0) {
                            $arrEnd['OrderLNo'] = $orderLNo;
                            $arrEnd['ItemCd']   = 'Return-Dmy001';
                            $arrEnd['ItemNm']   = '返品出荷識別用';
                            $arrEnd['JanCd']    = '2016112109003';
                            $arrEnd['Quantity'] = 1;
                            $arrEnd['MakerNm']  = null;
                            $arrEnd['MakerNo']  = null;
                            $arrEnd['ItemUrl']  = null;
                            $modelIS->insert($arrEnd);
                            $arrEnd   = [];
                            $orderLNo = 1;
                        }
                        $check = true;
                    }
                    if ($value->product_oogata >= 1) {
                        $arrUpAfterIn[] = ['OrderNo' => $orderNo, 'GroupKey' => $groupKey->GroupKey];
                    }
                    $DeliPlanDate = date("Ymd");
                    if (date('H:i') > '15:00' || $value->delivery_instruction === 8) {
                        $DeliPlanDate = date("Ymd", strtotime("+1 days"));
                    }
                    $ShippingInstructions = '「無」';
                    if (!empty($value->note)) {
                        $ShippingInstructions = $value->note;
                    }
                    $arrIns['OrderNo']               = $orderNo;
                    $arrIns['OrderBNo']              = $value->delivery_count + 1;
                    $arrIns['OrderLNo']              = $orderLNo;
                    $arrIns['DeliDate']              = '';
                    $arrIns['TimeCd']                = '';
                    $arrIns['DeliveryInstructions']  = $value->note;
                    $arrIns['Account']               = '';
                    $arrIns['DeliPlanDate']          = $DeliPlanDate;
                    $arrIns['TotalAmount']           = 0;
                    $arrIns['RecKb']                 = 1;
                    $arrIns['ReceNm']                = '';
                    $arrIns['Proviso']               = '';
                    $arrIns['ShippingInstructions']  = $ShippingInstructions;
                    $arrIns['StoreCd']               = 1;
                    $arrIns['InvoiceRemarks']        = '';
                    $arrIns['DeliveryCd']            = 4;
                    $arrIns['CustomerNm']            = '株式会社大都';
                    $arrIns['DNm']                   = $value->return_nm;
                    $arrIns['DPostal']               = str_replace('-', '', $value->return_postal);
                    $arrIns['DPref']                 = $value->return_pref;
                    $arrIns['DAddress1']             = $value->return_address;
                    $arrIns['DAddress2']             = '';
                    $arrIns['DTel']                  = $value->return_tel;
                    $arrIns['ItemCd']                = $value->product_code;
                    $arrIns['ItemNm']                = $value->product_name_long;
                    $arrIns['JanCd']                 = $value->product_jan;
                    $arrIns['MakerNm']               = $value->maker_full_nm;
                    $arrIns['MakerNo']               = $value->product_maker_code;
                    $arrIns['ItemUrl']               = $value->rak_img_url_1;
                    $arrIns['Quantity']              = $value->return_quantity;
                    $arrIns['GroupKey']              = $groupKey->GroupKey;
                    $arrIns['One']                   = 1;
                    $modelIS->insert($arrIns);
                    if ($check) {
                        $arrEnd = $arrIns;
                        $check  = false;
                    }
                    $modelDR->updateData(
                        [
                            'return_no'      => $value->return_no,
                            'return_line_no' => $value->return_line_no,
                            'return_time'    => $value->return_time,
                        ],
                        [
                            'delivery_count'       => ($value->delivery_count + 1),
                            'delivery_instruction' => 1,
                            'supplier_return_no'   => $orderNo,
                            'supplier_return_line' => $orderLNo,
                            'status'               => 2,
                        ]
                    );
                    $orderLNo++;
                }
                if (count($arrEnd) !== 0) {
                    $arrEnd['OrderLNo'] = $orderLNo;
                    $arrEnd['ItemCd']   = 'Return-Dmy001';
                    $arrEnd['ItemNm']   = '返品出荷識別用';
                    $arrEnd['JanCd']    = '2016112109003';
                    $arrEnd['Quantity'] = 1;
                    $arrEnd['MakerNm']  = null;
                    $arrEnd['MakerNo']  = null;
                    $arrEnd['ItemUrl']  = null;
                    $modelIS->insert($arrEnd);
                }
                if (count($arrUpAfterIn) !== 0) {
                    $arrKeys = array_map("unserialize", array_unique(array_map("serialize", $arrUpAfterIn)));
                    $arrUpdate = ['DeliveryCd' => 3];
                    foreach ($arrKeys as $item) {
                        $modelIS->updateData($item, $arrUpdate);
                    }
                }
                DB::commit();
                DB::connection('glsc')->commit();
            } catch (\Exception $e) {
                DB::rollback();
                DB::connection('glsc')->rollback();
                $message = "Can't process return: {$value->return_no}-{$value->return_line_no}-{$value->return_time} rollback";
                print_r($message . PHP_EOL);
                Log::error($message);
                report($e);
            }
            $message = "Process success: {$count} record.";
            print_r($message . PHP_EOL);
            Log::error($message);
        }
        $message = '----End process content 1----';
        Log::info($message);
        print_r($message . PHP_EOL);

        $message = '----Start process content 2----';
        Log::info($message);
        print_r($message . PHP_EOL);
        $modelDRL = new DtReceiptLedger();
        $datas2   = $modelDR->getDataDeliveryReturn2();
        $total    = count($datas2);
        if ($total === 0) {
            $message = 'No data.';
            Log::info($message);
            print_r($message . PHP_EOL);
        } else {
            $arrInsert = [];
            try {
                DB::beginTransaction();
                DB::connection('edi')->beginTransaction();
                $modelAT = new TAka();
                foreach ($datas2 as $key => $value) {
                    if (is_null($value->product_maker_code)) {
                        $makerCode = '';
                    } else {
                        $makerCode = $value->product_maker_code;
                    }
                    $returnCode = (string)$value->supplier_return_no;
                    $check = false;
                    if (empty($returnCode)) {
                        $returnCode = 'r-' . $value->supplier_id . '-' . $value->return_no;
                        $check = true;
                    }
                    $arrInsert = [];
                    $arrInsert['suppliercd']             = (int)$value->supplier_id;
                    $arrInsert['SerialNo']               = 0;
                    $arrInsert['return_date']            = empty($value->return_date) ? date('Y-m-d') : $value->return_date;
                    $arrInsert['return_code']            = $returnCode;
                    $arrInsert['item_code']              = (string)$value->product_code;
                    $arrInsert['jan_code']               = (string)$value->product_jan;
                    $arrInsert['supplier_code']          = (string)$value->supplier_id;
                    $arrInsert['supplier_name']          = (string)$value->supplier_nm;
                    $arrInsert['maker_name']             = (string)$value->maker_full_nm;
                    $arrInsert['maker_code']             = $makerCode;
                    $arrInsert['note_code']              = '';
                    $arrInsert['name']                   = (string)$value->product_name_long;
                    $arrInsert['price']                  = (int)$value->price_invoice;
                    $arrInsert['s_price']                = (int)$value->return_price;
                    $arrInsert['quantity']               = (int)$value->return_quantity;
                    $arrInsert['quantity_return']        = (int)$value->return_quantity;
                    $arrInsert['color']                  = (string)$value->product_color;
                    $arrInsert['size']                   = (string)$value->product_size;
                    $arrInsert['fax']                    = (string)$value->supplier_fax;
                    $arrInsert['mail']                   = (string)$value->mail_address;
                    $arrInsert['other']                  = '';
                    $arrInsert['suppliercd_flg']         = 0;
                    $arrInsert['suppliercd_date']        = null;
                    $arrInsert['buyer_flg']              = 0;
                    $arrInsert['buyer_date']             = null;
                    $arrInsert['buyer_flg_t_admin_id']   = null;
                    $arrInsert['captain_flg']            = 0;
                    $arrInsert['captain_date']           = null;
                    $arrInsert['captain_flg_t_admin_id'] = null;
                    $arrInsert['slip_flg']               = 0;
                    $arrInsert['slip_date']              = null;
                    $arrInsert['slip_flg_t_admin_id']    = null;
                    $arrInsert['del_flg']                = 0;
                    $arrInsert['valid_flg']              = 1;
                    $arrInsert['created_at']             = now();
                    $arrInsert['updated_at']             = now();
                    $modelAT->insert($arrInsert);
                    if ($value->red_voucher === 0 && $value->delivery_instruction === -1) {
                        $modelDRL->add2ReceiptLedgerOutStock([
                            'pa_order_code'        => $returnCode,
                            'pa_product_code'      => $value->product_code,
                            'pa_price_invoice'     => $value->price_invoice,
                            'pa_delivery_num'      => $value->return_quantity,
                            'pa_supplier_id'       => $value->supplier_id,
                            'pa_stock_type'        => 3,
                            'pa_stock_detail_type' => 3,
                        ]);
                    }
                    $arrUpdateReturn = [
                        'red_voucher' => 1,
                        'up_ope_cd'   => 'OPE99999',
                        'up_date'     => now(),
                        'status'      => 3
                    ];
                    if ($check) {
                        $arrUpdateReturn['supplier_return_no'] = $returnCode;
                    }
                    $modelDR->updateData(
                        [
                            'return_no'      => $value->return_no,
                            'return_line_no' => $value->return_line_no,
                            'return_time'    => $value->return_time,
                        ],
                        $arrUpdateReturn
                    );
                }
                DB::commit();
                DB::connection('edi')->commit();
                $message = "Process success: {$total} record.";
                print_r($message . PHP_EOL);
                Log::error($message);
            } catch (\Exception $e) {
                DB::rollback();
                DB::connection('edi')->rollback();
                $message = "Can't process insert t_aka";
                print_r($message . PHP_EOL);
                Log::error($message);
                report($e);
            }
        }
        $message = '----End process content 2----';
        Log::info($message);
        print_r($message . PHP_EOL);
        Event::fire(new eCommand($this->signature, array('end' => true, 'error' => $this->error)));
        $totalTime = round(microtime(true) - $start, 2);
        Log::info("End batch process request GLSC with total time: $totalTime.");
        print_r("End batch process request GLSC with total time: $totalTime s." . PHP_EOL);
    }
}
