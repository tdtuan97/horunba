<?php
/**
 * Batch process get info from smaregi api
 *
 * @package    App\Console\Commands
 * @subpackage ProcessSmaregiMasterSyncDbApi
 * @copyright  Copyright (c) 2018 RiverCrane! Corporation. All Rights Reserved.
 * @author     Le Hung<le.hung.rcvn2012@gmail.com>
 */

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;
use Validator;
use Swift_Mailer;
use Swift_SmtpTransport;
use Mail;
use App;
use Config;
use Event;
use App\Events\Command as eCommand;
use App\Models\Batches\MstStoreProduct;
use App\Notification;
use App\Notifications\SlackNotification;

class ProcessSmaregiMasterSyncDbApi extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'process:smaregi-master-sync-api';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Get info from smaregi api';

    /**
     * The error.
     *
     * @var string
     */
    public $error = [];

    /**
     * The folder.
     *
     * @var string
     */
    public $folder = null;

    /**
     * Size insert
     * @var int
     */
    private $sizeInsert = 1000;
    /**
     * Smaregi contract id
     * @var contract_id
     */
    public $contractId = '';

    /**
     * Smaregi access token
     * @var accessToken
     */
    public $accessToken = '';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        $this->contractId   = env('CONTRACT_ID');
        $this->accessToken  = env('ACCESS_TOKEN');
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $start          = microtime(true);
        $model          = new MstStoreProduct();
        $countData      = $model->count();
        $sum            = 0;
        //Create log
        $arrayReplace = [':', '-'];
        $this->folder       = str_replace($arrayReplace, '_', $this->signature);
        Log::useDailyFiles(storage_path() . "/logs/$this->folder/history.log");
        //Process data
        Event::fire(new eCommand($this->signature, array('start' => true)));
        Log::info('Start process smaregi master sync api');
        print_r("Start process smaregi master sync api" . PHP_EOL);

        if ($countData === 0) {
            print_r("-------- PROCESS FIRST --------" . PHP_EOL);
            $sum = $this->processFirst();
        } else {
            print_r("-------- PROCESS STEP 1 --------" . PHP_EOL);
            $sum = $this->processContent1();
        }
        Log::info("Total data insert of all pages : " . $sum);
        print_r("Total data insert of all pages : " . $sum . PHP_EOL);

        // Process Order Detail
        print_r("-------- PROCESS STEP 2 --------" . PHP_EOL);
        $this->processContent2();

        print_r("-------- PROCESS STEP 3 --------" . PHP_EOL);
        //$this->processContent3();

        print_r("-------- PROCESS STEP 4 --------" . PHP_EOL);
        $this->processContent4();

        print_r("-------- PROCESS STEP 5 --------" . PHP_EOL);
        $this->processContent5();

        print_r("-------- PROCESS STEP 6 --------" . PHP_EOL);
        $this->processContent6();

        Event::fire(new eCommand($this->signature, array('end' => true, 'error' => $this->error)));
        $totalTime = round(microtime(true) - $start, 2);
        Log::info("End batch process smaregi  api: $totalTime s.");
        print_r("End batch process smaregi  api: $totalTime s." . PHP_EOL);
    }

    /**
     * Get all of products from api smaregi insert to database
     *
     * @param   array   $response
     * @param   int     $i
     * @return  array
     */
    private function processFirst()
    {
        $startDate      = date("Y-m-d H:i:s");
        $totalCount     = 0;
        $perPage        = 1000;
        $totalPage      = 0;
        $model          = new MstStoreProduct();
        $requestParams['header'] = array(
            "Cache-Control: no-cache",
            "Content-Type: application/x-www-form-urlencoded",
            "X_contract_id: $this->contractId",
            "X_access_token: $this->accessToken",
        );
        $requestParams['fields'] = http_build_query(array(
            'proc_name' => 'product_ref',
            'params' => '{
                "fields":["productId"],
                "table_name":"Product"
            }'
        ));
        $response = $this->curlGet($requestParams);
        if (isset($response->total_count)) {
            $totalCount = $response->total_count;
        }
        $totalPage = (int) ceil($totalCount / $perPage);
        $index  = 0;
        $sum    = 0;
        $arrError           = [];
        for ($i = 1; $i <= $totalPage; $i++) {
            $requestParams['fields'] = http_build_query(array(
                'proc_name' => 'product_ref',
                'params' => '{
                            "page" : '.$i.',
                            "table_name":"Product"
                        }'
            ));
            print_r("--------  PROCESS PAGE $i --------" . PHP_EOL);
            $response = $this->curlGet($requestParams);
            if (!empty($response) && !isset($response->error_code)) {
                if (count($response->result) > 0) {
                    $data       =  (array)$response->result;
                    $arrayInsert        = [];
                    $arrayProductCodes  = [];
                    $index              = 0;
                    foreach ($data as $key => $value) {
                        $value = (array)$value;
                        $input =    [
                            'product_code'              => trim($value['supplierProductNo']),
                            'store_product_id'          => $value['productId'],
                            'df_handling'               => 1,
                            'product_jan'               => $value['productCode'],
                            'store_product_category_id' => $value['categoryId'],
                            'store_product_name'        => $value['productName'],
                            'product_tax_division'      => $value['taxDivision'],
                            'product_price_division'    => $value['productPriceDivision'],
                            'product_price'             => (int)$value['price'],
                            'product_cost'              => $value['cost'],
                            'product_size'              => $value['size'],
                            'product_color'             => $value[ 'color'],
                            'product_tag'               => $value['tag'],
                            'product_group_code'        => $value['groupCode'],
                            'product_url'               => $value['url'],
                            'stock_control_division'    => $value['stockControlDivision'],
                            'display_flag'              => $value['displayFlag'],
                            'point_not_applicable'      => $value['pointNotApplicable'],
                            'is_updated'                => 0,
                            'product_jan_new'           => null,
                            'in_date'                   => date('Y-m-d H:s:i'),
                            'up_date'                   => date('Y-m-d H:s:i')
                        ];
                        $arrayProductCodes[$key] = $value['supplierProductNo'];
                        Validator::extend('checkProductCode', function ($parameters, $value, $attribute) use ($model) {
                            $checkData = $model->find($value);
                            if (count($checkData) === 1) {
                                return false;
                            }
                            return true;
                        }, 'Product code has already exitsed');

                        $validator = Validator::make($input, [
                            'product_code'  => 'required|checkProductCode|unique:mst_store_product,product_code'
                        ]);
                        $mess = '';
                        if ($validator->fails()) {
                            $messageData = $validator->messages()->toArray();
                            foreach ($messageData as $key1 => $value1) {
                                $mess .=  "$key1:".$input['product_code'] . ' > ' . $value1[0] . "\r\n";
                            }
                            $arrError[$key] = $mess;
                            continue;
                        }
                        try {
                            $flgInsert = true;
                            $model->insert($input);
                            $index++;
                        } catch (\Exception $e) {
                            report($e);
                            $flgInsert = false;
                        }
                    }
                    if ($index > 0) {
                        Log::info("Total data insert of page $i : " . $index);
                        print_r("Total data insert of page $i : " . $index . PHP_EOL);
                    } else {
                        Log::info('No data insert of page : '. $i);
                        print_r('No data insert of page : '. $i . PHP_EOL);
                    }
                    $sum += $index;
                }
                if (count($arrError) > 0) {
                    $err = implode("\n", $arrError);
                    Log::info($err);
                }
            }
        }
        return $sum;
    }
    /**
     * Get all of products from api smaregi insert to database
     *
     * @return  array
     */
    private function processContent1()
    {
        $index          = 0;
        $arrError       = [];
        $sum = 0;
        $startDate      = date("Y-m-d 00:00:00");
        $requestParams['header'] = array(
            "Cache-Control: no-cache",
            "Content-Type: application/x-www-form-urlencoded",
            "X_contract_id: $this->contractId",
            "X_access_token: $this->accessToken",
        );
        $requestParams['fields'] = http_build_query(array(
            'proc_name' => 'product_ref',
            'params' => '{
                "conditions":[{"updDateTime >=":"'.$startDate.'"}],
                "table_name":"Product"
            }'
        ));
        $response = $this->curlGet($requestParams);
        if (!empty($response) && !isset($response->error_code)) {
            if (count($response->result) > 0) {
                $data       =  (array)$response->result;
                $model      = new MstStoreProduct();
                $arrayInsert        = [];
                $arrayProductCodes  = [];
                foreach ($data as $key => $value) {
                    $value = (array)$value;
                    $input =    [
                        'product_code'              => trim($value['supplierProductNo']),
                        'store_product_id'          => $value['productId'],
                        'df_handling'               => 1,
                        'product_jan'               => $value['productCode'],
                        'store_product_category_id' => $value['categoryId'],
                        'store_product_name'        => $value['productName'],
                        'product_tax_division'      => $value['taxDivision'],
                        'product_price_division'    => $value['productPriceDivision'],
                        'product_price'             => (int)$value['price'],
                        'product_cost'              => $value['cost'],
                        'product_size'              => $value['size'],
                        'product_color'             => $value[ 'color'],
                        'product_tag'               => $value['tag'],
                        'product_group_code'        => $value['groupCode'],
                        'product_url'               => $value['url'],
                        'stock_control_division'    => $value['stockControlDivision'],
                        'display_flag'              => $value['displayFlag'],
                        'point_not_applicable'      => $value['pointNotApplicable'],
                        'is_updated'                => 0,
                        'product_jan_new'           => null,
                        'in_date'                   => date('Y-m-d H:s:i'),
                        'up_date'                   => date('Y-m-d H:s:i')
                    ];
                    $arrayProductCodes[$key] = $value['supplierProductNo'];
                    Validator::extend('checkProductCode', function ($parameters, $value, $attribute) use ($model) {
                        $checkData = $model->find($value);
                        if (count($checkData) === 1) {
                            return false;
                        }
                        return true;
                    }, 'Product code has already exitsed');

                    $validator = Validator::make($input, [
                        'product_code'  => 'required|checkProductCode|unique:mst_store_product,product_code'
                    ]);
                    $mess = '';
                    if ($validator->fails()) {
                        $messageData = $validator->messages()->toArray();
                        foreach ($messageData as $key1 => $value1) {
                            $mess .=  "$key1:".$input['product_code'] . ' > ' . $value1[0] . "\r\n";
                        }
                        $arrError[$key] = $mess;
                        continue;
                    }
                    try {
                        $flgInsert = true;
                        $model->insert($input);
                        $index++;
                    } catch (\Exception $e) {
                        report($e);
                        $flgInsert = false;
                    }
                }
                $sum += $index;
                if ($index > 0) {
                    Log::info("Total data insert: " . $index);
                    print_r("Total data insert: " . $index . PHP_EOL);
                } else {
                    Log::info('No data insert');
                    print_r('No data insert'. PHP_EOL);
                }
            }
            if (count($arrError) > 0) {
                $err = implode("\n", $arrError);
                Log::info($err);
                print_r('Processing has errors'. PHP_EOL);
            }
        }
        return $sum;
    }
    /**
     * Get products and update name, flag
     *
     * @return  array
     */
    private function processContent2()
    {
        $model      = new MstStoreProduct();
        $data       = $model->getDataSyncContent2($this->sizeInsert);
        $count      = count($data);
        if (count($data) > 0) {
            foreach ($data as $key => $value) {
                $dataWhere = [
                    'product_code' => $value['product_code']
                ];
                $dataUpdate = [
                    'df_handling' => $value['df_handling'],
                    'is_updated'  => 1,
                    'store_product_name' => $value['product_name']
                ];
                $model->updateData($dataWhere, $dataUpdate);
            }
        }
        Log::info("Total records already updated : $count .");
        print_r("Total records already updated : $count ." . PHP_EOL);
    }
    /**
     * Get products and update flag
     *
     * @return  array
     */
    private function processContent3()
    {
        $model      = new MstStoreProduct();
        $data       = $model->getDataSyncContent3($this->sizeInsert);
        $count      = count($data);
        if (count($data) > 0) {
            foreach ($data as $key => $value) {
                $dataWhere = [
                    'product_code'  => $value['product_code']
                ];
                $dataUpdate = [
                    'df_handling'   => 0,
                    'is_updated'    => 1
                ];
                $model->updateData($dataWhere, $dataUpdate);
            }
        }
        Log::info("Total records already updated : $count .");
        print_r("Total records already updated : $count ." . PHP_EOL);
    }
    /**
     * Get products and update flag
     *
     * @return  array
     */
    private function processContent4()
    {
        $model      = new MstStoreProduct();
        $data       = $model->getDataSyncContent4($this->sizeInsert);
        $count      = count($data);
        if (count($data) > 0) {
            foreach ($data as $key => $value) {
                $dataWhere = [
                    'product_code'  => $value['product_code']
                ];
                $dataUpdate = [
                    'product_jan_new'   => $value['product_jan'],
                    'is_updated'        => 2,
                    'is_send_mail'      => 1,
                ];
                $model->updateData($dataWhere, $dataUpdate);
            }
        }
        Log::info("Total records already updated : $count .");
        print_r("Total records already updated : $count ." . PHP_EOL);
    }

    /**
     * Get products and insert
     *
     * @return  array
     */
    private function processContent5()
    {
        $model      = new MstStoreProduct();
        $data       = $model->getDataSyncContent5($this->sizeInsert);
        $count      = count($data);
        $insert     = 0;
        if (count($data) > 0) {
            foreach ($data as $key => $value) {
                $maxId = $model->getMaxStoreProductId();
                if (!empty($maxId->store_product_id)) {
                    $storeProductId = $maxId->store_product_id;
                    $maxId  = filter_var($storeProductId, FILTER_SANITIZE_NUMBER_INT);
                    $maxId  = str_replace('-', '', $maxId);
                    $maxId  = sprintf("%09s", $maxId + 1);
                } else {
                    $maxId = '2018000000001';
                }
                $tag = [$value['maker_cd'], $value['product_genre'], $value['product_maker_code']];
                $tag = array_filter($tag);
                $tag = implode(',', $tag);
                $input = [
                    'product_code'              => $value->product_code,
                    'store_product_id'          => $maxId,
                    'df_handling'               => $value->df_handling,
                    'product_jan'               => $value->product_jan,
                    'store_product_category_id' => 2020,
                    'store_product_name'        => $value->product_name,
                    'product_tax_division'      => null,
                    'product_price_division'    => null,
                    'product_price'             => (int)$value->price_calc_diy_komi,
                    'product_cost'              => $value->price_invoice,
                    'product_size'              => $value->product_size,
                    'product_color'             => $value->product_color,
                    'product_tag'               => $tag,
                    'product_group_code'        => null,
                    'product_url'               => 'http://www.diy-tool.com/fs/diy/'.strtolower($value['product_code']),
                    'stock_control_division'    => null,
                    'display_flag'              => null,
                    'point_not_applicable'      => null,
                    'is_updated'                => 0,
                    'product_jan_new'           => null,
                    'in_date'                   => date('Y-m-d H:s:i'),
                    'up_date'                   => date('Y-m-d H:s:i')
                ];
                Validator::extend('checkProductCode', function ($parameters, $value, $attribute) use ($model) {
                    $checkData = $model->find($value);
                    if (count($checkData) === 1) {
                        return false;
                    }
                    return true;
                }, 'Product code has already exitsed');
                $validator = Validator::make($input, [
                    'product_code'  => 'required|checkProductCode|unique:mst_store_product,product_code'
                ]);
                $mess = '';
                if ($validator->fails()) {
                    $messageData = $validator->messages()->toArray();
                    foreach ($messageData as $key1 => $value1) {
                        $mess .=  "$key1:".$input['product_code'] . ' > ' . $value1[0] . "\r\n";
                    }
                    $arrError[$key] = $mess;
                    continue;
                }
                try {
                    $flgInsert = true;
                    $model->insert($input);
                    $insert++;
                } catch (\Exception $e) {
                    report($e);
                    $flgInsert = false;
                }
            }
        }
        Log::info("Total records already inserted : $insert .");
        print_r("Total records already inserted : $insert ." . PHP_EOL);

        $requestParams['header'] = array(
            "Cache-Control: no-cache",
            "Content-Type: application/x-www-form-urlencoded",
            "X_contract_id: $this->contractId",
            "X_access_token: $this->accessToken",
        );
        $data  = $model->getDataSyncContent5IsUpdate($this->sizeInsert);
        if (count($data) > 0) {
            $tempUpdate = array_chunk($data->toArray(), 100);
            //echo count($tempUpdate);
            foreach ($tempUpdate as $items) {
                foreach ($items as $key => $item) {
                    $productName = $item['df_handling'] === 1 ? $item['store_product_name']
                        : '★' . $item['store_product_name'];
                    $products[] = array(
                        'productId'         => $item['store_product_id'],
                        'categoryId'        => $item['store_product_category_id'],
                        'productCode'       => $item['product_jan'],
                        'productName'       => $productName,
                        'price'             => $item['product_price'],
                        'cost'              => $item['product_cost'],
                        'size'              => $item['product_size'],
                        'color'             => trim($item['product_color']),
                        'tag'               => $item['product_tag'],
                        'url'               => $item['product_url'],
                        'displayFlag'       => 1,
                        'supplierProductNo' => $item['product_code'],
                    );
                    $updateProduct[] =  $item['product_code'];
                    if (App::environment(['local', 'test'])) {
                        $model->updateDataIn(
                            $updateProduct,
                            ['is_updated'   => 0]
                        );
                        continue;
                    }
                }
                $this->processApiAdd($products, $updateProduct, $requestParams, $model);
                //Process update product code

                $products = array();
                $updateProduct = array();
            }
        }
    }

    private function processApiAdd($products, $updateProduct, $requestParams, $model)
    {
        if (count($products) === 0) {
            return true;
        }
        $requestParams['fields'] = http_build_query(array(
            'proc_name' => 'product_upd',
            'params' => '{
                "proc_info": {"proc_division" : "U"},
                "data":[{
                    "table_name":"Product",
                    "rows": '.json_encode($products).'
                }]
            }'
        ));
        $response = $this->curlGet($requestParams);
        if (!empty($response) && isset($response->error)) {
            if ($response->error_code === 99) {
                preg_match_all("/([\d]+)/", $response->error_description, $match);
                if (!empty($match)) {
                    $storeId = $match[0][2];
                    $model->updateData(
                        ['store_product_id'   =>  $storeId],
                        ['is_updated' => 99]
                    );
                }
                return true;
            }
            if ($response->error_code === 41) {
                preg_match("/([\d]+)/", $response->error_description, $match);
                if (empty($match)) {
                    return true;
                } else {
                    $index = (int)$match[0] - 1;
                    $requestParams1['header'] = array(
                        "Cache-Control: no-cache",
                        "Content-Type: application/x-www-form-urlencoded",
                        "X_contract_id: $this->contractId",
                        "X_access_token: $this->accessToken",
                    );
                    $requestParams1['fields'] = http_build_query(array(
                        'proc_name' => 'product_ref',
                        'params' => '{
                            "conditions": [{"productCode":"'.$products[$index]['productCode'].'"}],
                            "table_name":"Product"
                        }'
                    ));
                    $response1 = $this->curlGet($requestParams1);
                    if (isset($response1->total_count)) {
                        if ((int)$response1->total_count > 0) {
                            $checkStore = $response1->result[0]->productId;
                            if ($updateProduct[$index] !== $response1->result[0]->supplierProductNo) {
                                $slack   = new Notification(CHANNEL['horunba']);
                                $slack->notify(new SlackNotification('Process add new for ' . $updateProduct[$index] .' error'));
                                $model->updateData(
                                    ['product_code'   => $updateProduct[$index]],
                                    ['is_updated' => 99]
                                );
                                return true;
                            }
                            if ($products[$index]['productId'] !== $checkStore) {
                                $products[$index]['productId'] = $checkStore;
                                $model->updateData(
                                    ['product_code'   => $updateProduct[$index], 'product_jan' => $products[$index]['productCode']],
                                    ['store_product_id' => $checkStore]
                                );
                            }
                        }
                        if ((int)$response1->total_count === 0) {
                            $model->updateData(
                                ['product_code'   => $updateProduct[$index]],
                                ['is_updated' => 99]
                            );
                            unset($products[$index]);
                        }
                    }
                    return $this->processApiAdd($products, $updateProduct, $requestParams, $model);
                }
            }
        } else {
            $model->updateDataIn(
                $updateProduct,
                ['is_updated'   => 0]
            );
            return true;
        }
    }
    /**
     * Get products for send mail
     *
     * @return  array
     */
    private function processContent6()
    {
        $slack    = new Notification(CHANNEL['horunba']);
        $model    = new MstStoreProduct();
        $data     = $model->getDataForSendMail($this->sizeInsert);
        $view = view('template-mail.sync-order')->with(
            [
                'title'         => 'List Order Codes',
                'datas'         => $data,
            ]
        );
        if (!App::environment(['local', 'test'])) {
            $configTest = Config::get('mail')['mail_test'];
            $swiftTransport = (new Swift_SmtpTransport(
                $configTest['host'],
                $configTest['port'],
                $configTest['encryption']
            ))
            ->setStreamOptions(Config::get('mail')['stream'])
            ->setUsername($configTest['username'])
            ->setPassword($configTest['password']);
            $mailer = new Swift_Mailer($swiftTransport);
            Mail::setSwiftMailer($mailer);
            $mailTo = 'nttien@monotos.biz';
        } else {
            $configTest = Config::get('mail')['mail_test'];
            $swiftTransport = (new Swift_SmtpTransport(
                $configTest['host'],
                $configTest['port'],
                $configTest['encryption']
            ))
            ->setStreamOptions(Config::get('mail')['stream'])
            ->setUsername($configTest['username'])
            ->setPassword($configTest['password']);
            $mailer = new Swift_Mailer($swiftTransport);
            Mail::setSwiftMailer($mailer);
            $mailTo = 'nttien@monotos.biz';
        }
        $mailFrom = env('MAIL_FROM_ADDRESS');
        $body = $view->render();

        if (count($data) !== 0) {
            foreach ($data as $key => $value) {
                $model->updateData(
                    ['product_code' => $value['product_code']],
                    ['is_send_mail'   => 0]
                );
            }
            try {
                Mail::raw([], function ($message) use ($data, $mailTo, $mailFrom, $body) {
                    $message->to($mailTo)
                            ->subject('List products')
                            ->from($mailFrom)
                            ->setBody($body, 'text/html');
                });
            } catch (\Exception $e) {
                $msg = $e->getMessage();
                print_r($msg . PHP_EOL);
                Log::error($msg);
                report($e);
            }
            $count = count($data);
            Log::info("Total $count items has already sent to mail.");
            print_r("Total $count items has already sent to mail." . PHP_EOL);
        }
    }
    /**
     * Request process api
     *
     * @param  array $paramOptions
     * @return json encode
     */
    private function curlGet($paramOptions)
    {
        $slack   = new Notification(CHANNEL['horunba']);
        $smaregiUrl     = "https://webapi.smaregi.jp/access/";
        $ch = curl_init($smaregiUrl);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $paramOptions['header']);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $paramOptions['fields']);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_MAXREDIRS, 10);
        curl_setopt($ch, CURLOPT_TIMEOUT, 30);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
        $response = curl_exec($ch);
        curl_close($ch);
        $response = json_decode($response);
        if (isset($response->error_code)) {
            $arrError = (array) $response;
            $err = implode("\n", $arrError);
            $error  = "------------------------------------------" . PHP_EOL;
            $error .= basename(__CLASS__) . PHP_EOL;
            $error .= $err;
            $error .= PHP_EOL . "------------------------------------------" . PHP_EOL;
            $slack->notify(new SlackNotification($error));
            Log::info($err);
        }
        return $response;
    }
}
