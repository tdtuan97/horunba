<?php
/**
 * Batch process is can delivery check
 *
 * @package    App\Console\Commands
 * @subpackage ProcessIsCanDeliveryCheck
 * @copyright  Copyright (c) 2018 RiverCrane! Corporation. All Rights Reserved.
 * @author     Truong Nghia<truong.van.nghia@rivercrane.vn>
 */
namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;
use App\Notifications\SlackNotification;
use App\Models\Batches\MstOrder;
use App\Custom\Utilities;
use App\Notification;
use App\Events\Command as eCommand;
use Event;

class ProcessIsCanDeliveryCheck extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'process:is-can-delivery-check';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'When delivery plan date was came, but can not delivery then send sorry mail to customer.';

    /**
     * The error.
     *
     * @var array
     */
    public $error = [];

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //Create log
        $arrayReplace = [':', '-'];
        $folder       = str_replace($arrayReplace, '_', $this->signature);
        Log::useDailyFiles(storage_path() . "/logs/$folder/history.log");
        //Process data
        Event::fire(new eCommand($this->signature, array('start' => true)));
        Log::info('Start batch process is can delivery check.');
        print_r("Start batch process is can delivery check." . PHP_EOL);
        $start    = microtime(true);
        $slack    = new Notification(CHANNEL['horunba']);
        $modelO = new MstOrder();
        $datas = $modelO->getDataProcessIsCanDeliveryCheck();
        $total = count($datas);
        if ($total === 0) {
            $message = 'No data';
            Log::info($message);
            print_r($message . PHP_EOL);
        } else {
            $arrReceiveId = [];
            foreach ($datas as $value) {
                $arrReceiveId[] = $value->receive_id;
            }
            $success = count($arrReceiveId);
            if ($success !== 0) {
                $modelO->updateData(
                    $arrReceiveId,
                    [
                        'is_send_mail_urgent' => 1,
                        'urgent_mail_id'      => 80,
                        'up_ope_cd'           => 'OPE99999',
                        'up_date'             => now(),
                    ]
                );
            }
            $message = "Process total: $total and success: $success records.";
            Log::info($message);
            print_r($message . PHP_EOL);
        }
        Event::fire(new eCommand($this->signature, array('end' => true, 'error' => $this->error)));
        $totalTime = round(microtime(true) - $start, 2);
        Log::info("End batch process is can delivery check with total time: $totalTime s.");
        print_r("End batch process is can delivery check with total time: $totalTime s.");
    }
}
