<?php
/**
 * Batch process send mail to customer
 *
 * @package    App\Console\Commands
 * @subpackage ProcessMailSend
 * @copyright  Copyright (c) 2017 RiverCrane! Corporation. All Rights Reserved.
 * @author     Truong Nghia<truong.van.nghia@rivercrane.vn>
 */

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\Batches\DtSendMailList;
use Illuminate\Support\Facades\Log;
use App\Notifications\SlackNotification;
use Egulias\EmailValidator\EmailValidator;
use Egulias\EmailValidator\Validation\RFCValidation;
use App\Custom\Utilities;
use App\Notification;
use Swift_Mailer;
use Swift_SmtpTransport;
use Swift_Message;
use Mail;
use App;
use Config;
use Event;
use App\Events\Command as eCommand;

class ProcessMailSend extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'process:mail-send';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send mail to customer';

    /**
     * The error.
     *
     * @var string
     */
    public $error = [];
    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //Create log
        $arrayReplace = [':', '-'];
        $folder       = str_replace($arrayReplace, '_', $this->signature);
        Log::useDailyFiles(storage_path() . "/logs/$folder/history.log");
        //Process data
        Event::fire(new eCommand($this->signature, array('start' => true)));
        Log::info('Start batch process mail send.');
        print_r("Start batch process mail send." . PHP_EOL);
        $slack    = new Notification(CHANNEL['horunba']);
        $start    = microtime(true);
        $modelSML = new DtSendMailList();
        $limit    = Config::get('mail')['limit_batch_send_mail'];
        $datas = $modelSML->getDataMailSend($limit);
        if (count($datas) !== 0) {
            $success = 0;
            $fail    = 0;
            $arrRakutenError = [];
            foreach ($datas as $data) {
                $arrErrorAddress = [];
                $arrWhere = [
                    'receive_order_id'    => $data->receive_order_id,
                    'order_status_id'     => $data->order_status_id,
                    'order_sub_status_id' => $data->order_sub_status_id,
                    'operater_send_index' => $data->operater_send_index,
                ];
                $mailTo  = '';
                $mailCc  = '';
                $mailBcc = '';
                if (!App::environment(['local', 'test'])) {
                    $config = [];
                    if ($data->mall_id === 1 || $data->mall_id === 9) {
                        $string = '/.rakuten.ne.jp/';
                        if (preg_match($string, $data->mail_to)) {
                            $configRakuten = Config::get('mail')['rakuten'];
                            $swiftTransport = (new Swift_SmtpTransport(
                                $configRakuten['host'],
                                $configRakuten['port'],
                                $configRakuten['encryption']
                            ))
                            ->setStreamOptions(Config::get('mail')['stream'])
                            ->setUsername($configRakuten['username'])
                            ->setPassword($configRakuten['password']);
                        } else {
                            $configSakure = Config::get('mail')['sakura'];
                            $swiftTransport = (new Swift_SmtpTransport(
                                $configSakure['host'],
                                $configSakure['port'],
                                $configSakure['encryption']
                            ))
                            ->setStreamOptions(Config::get('mail')['stream'])
                            ->setUsername($configSakure['username'])
                            ->setPassword($configSakure['password']);
                        }
                    } else {
                        $configSakure = Config::get('mail')['sakura'];
                        $swiftTransport = (new Swift_SmtpTransport(
                            $configSakure['host'],
                            $configSakure['port'],
                            $configSakure['encryption']
                        ))
                        ->setStreamOptions(Config::get('mail')['stream'])
                        ->setUsername($configSakure['username'])
                        ->setPassword($configSakure['password']);
                    }
                    $mailer = new Swift_Mailer($swiftTransport);
                    Mail::setSwiftMailer($mailer);
                    $mailTo  = mb_convert_kana($data->mail_to, "KVa");
                    $mailCc  = mb_convert_kana($data->mail_cc, "KVa");
                    $mailBcc = mb_convert_kana($data->mail_bcc, "KVa");
                } else {
                    if ($data->mall_id === 1 || $data->mall_id === 9) {
                        $configTest = Config::get('mail')['mail_test_rakuten'];
                        $swiftTransport = (new Swift_SmtpTransport(
                            $configTest['host'],
                            $configTest['port'],
                            $configTest['encryption']
                        ))
                        ->setStreamOptions(Config::get('mail')['stream'])
                        ->setUsername($configTest['username'])
                        ->setPassword($configTest['password']);
                        $mailer = new Swift_Mailer($swiftTransport);
                        Mail::setSwiftMailer($mailer);
                        $mailTo  = $configTest['mail_to_test'];
                        $mailCc  = $configTest['mail_cc_test'];
                        $mailBcc = $configTest['mail_bcc_test'];
                    } else {
                        $configTest = Config::get('mail')['mail_test'];
                        $swiftTransport = (new Swift_SmtpTransport(
                            $configTest['host'],
                            $configTest['port'],
                            $configTest['encryption']
                        ))
                        ->setStreamOptions(Config::get('mail')['stream'])
                        ->setUsername($configTest['username'])
                        ->setPassword($configTest['password']);
                        $mailer = new Swift_Mailer($swiftTransport);
                        Mail::setSwiftMailer($mailer);
                        $mailTo  = $configTest['mail_to_test'];
                        $mailCc  = $configTest['mail_cc_test'];
                        $mailBcc = $configTest['mail_bcc_test'];
                    }
                }
                $errorMessage = null;
                $validator = new EmailValidator();
                if (empty($mailTo)) {
                    $arrUpdateFail = [
                        'send_status'   => 2,
                        'error_code'    => 'ERROR',
                        'error_message' => __('messages.mail_to_empty'),
                        'up_date'       => now(),
                        'up_ope_cd'     => 'OPE99999'
                    ];
                    $modelSML->updateData($arrWhere, $arrUpdateFail);
                    $fail++;
                    continue;
                } else {
                    $mailTo = explode(';', $mailTo);
                    $arrMailToErr = [];
                    foreach ($mailTo as $keyTo => $valueTo) {
                        if (!$validator->isValid($valueTo, new RFCValidation())) {
                            unset($mailTo[$keyTo]);
                            $arrMailToErr[] = $valueTo;
                        }
                    }
                    if (count($mailTo) === 0) {
                        $arrUpdateFail = [
                            'send_status'   => 2,
                            'error_code'    => 'ERROR',
                            'error_message' => 'Mail to invalid.',
                            'up_date'       => now(),
                            'up_ope_cd'     => 'OPE99999'
                        ];
                        $modelSML->updateData($arrWhere, $arrUpdateFail);
                        $error  = "------------------------------------------" . PHP_EOL;
                        $error .= basename(__CLASS__) . PHP_EOL;
                        $error .= 'Mail to invalid: ' . implode(',', $arrMailToErr);
                        $error .= PHP_EOL . "------------------------------------------" . PHP_EOL;
                        $slack->notify(new SlackNotification($error));
                        $fail++;
                        continue;
                    }
                    if (count($arrMailToErr)) {
                        $errorMessage = implode(',', $arrMailToErr);
                    }
                }
                try {
                    Mail::raw($data->mail_content, function ($message) use ($data, $mailTo, $mailCc, $mailBcc, &$arrRakutenError, &$arrErrorAddress, $errorMessage) {
                        !empty($data->mail_from) ?
                            $message->from($data->mail_from) :
                            (($data->mall_id === 1 || $data->mall_id === 9) ? 'rakuten@diy-tool.com' : '');
                        $message->to($mailTo);
                        if (!empty($mailCc)) {
                            $arrMailCc = explode(';', $mailCc);
                            if ($data->mall_id === 1 || $data->mall_id === 9) {
                                foreach ($arrMailCc as $keyCc => $valueCc) {
                                    if (strpos($valueCc, 'fw.rakuten.ne.jp') === false) {
                                        $arrRakutenError[] = [
                                            'data'    => $data,
                                            'mail_to' => $valueCc
                                        ];
                                        unset($arrMailCc[$keyCc]);
                                    }
                                }
                            }
                            if (count($arrMailCc) !== 0) {
                                foreach ($arrMailCc as $keyCc => $valueCc) {
                                    $validator = new EmailValidator();
                                    if (!$validator->isValid($valueCc, new RFCValidation())) {
                                        $arrErrorAddress[] = $valueCc;
                                        unset($arrMailCc[$keyCc]);
                                    }
                                }
                                $message->cc($arrMailCc);
                            }
                        }
                        if (!empty($mailBcc)) {
                            $arrMailBcc = explode(';', $mailBcc);
                            if ($data->mall_id === 1 || $data->mall_id === 9) {
                                foreach ($arrMailBcc as $keyBcc => $valueBcc) {
                                    if (strpos($valueBcc, 'fw.rakuten.ne.jp') === false) {
                                        $arrRakutenError[] = [
                                            'data'    => $data,
                                            'mail_to' => $valueBcc
                                        ];
                                        unset($arrMailBcc[$keyBcc]);
                                    }
                                }
                            }
                            if (count($arrMailBcc) !== 0) {
                                foreach ($arrMailBcc as $keyBcc => $valueBcc) {
                                    $validator = new EmailValidator();
                                    if (!$validator->isValid($valueBcc, new RFCValidation())) {
                                        $arrErrorAddress[] = $valueBcc;
                                        unset($arrMailBcc[$keyBcc]);
                                    }
                                }
                                $message->bcc($arrMailBcc);
                            }
                        }
                        !empty($data->mail_subject) ? $message->subject($data->mail_subject) : '';
                        if (!empty($data->attached_file_path) && $data->attached_file_path !== 'null') {
                            $attachedFilePath = storage_path('uploads/attached_file/');
                            if (!empty($data->attached_file_path) && $data->attached_file_path !== 'null') {
                                if (file_exists($attachedFilePath . $data->attached_file_path)) {
                                    $message->attach($attachedFilePath . trim($data->attached_file_path));
                                }
                            }
                        }
                        if ($data->mail_content != strip_tags($data->mail_content)) {
                            $contentEncoder = new \Swift_Mime_ContentEncoder_PlainContentEncoder('8bit');
                            $message->setEncoder($contentEncoder);
                            $message->addPart($data->mail_content, 'text/html');
                        } else {
                            $plainEncoder = new \Swift_Mime_ContentEncoder_PlainContentEncoder('8bit');
                            $message->setEncoder($plainEncoder);
                            $message->addPart($data->mail_content, 'text/plain');
                        }
                    });
                    $success++;
                    $arrUpdateSuccess = [
                        'send_status'   => 1,
                        'error_message' => $errorMessage,
                        'up_date'       => now(),
                        'up_ope_cd'     => 'OPE99999'
                    ];
                    if (count($arrErrorAddress) !== 0) {
                        $arrUpdateSuccess['error_message'] = 'List mail invalid: ' . implode(',', $arrErrorAddress);
                        $error  = "------------------------------------------" . PHP_EOL;
                        $error .= basename(__CLASS__) . PHP_EOL;
                        $error .= 'List mail invalid: ' . implode(',', $arrErrorAddress);
                        $error .= PHP_EOL . "------------------------------------------" . PHP_EOL;
                        $slack->notify(new SlackNotification($error));
                    }
                    $modelSML->updateData($arrWhere, $arrUpdateSuccess);
                } catch (\Exception $e) {
                    $msg = $e->getMessage();
                    $arrUpdateFail = [
                        'send_status'   => 2,
                        'error_code'    => $e->getCode(),
                        'error_message' => $msg,
                        'up_date'       => now(),
                        'up_ope_cd'     => 'OPE99999'
                    ];
                    $modelSML->updateData($arrWhere, $arrUpdateFail);
                    print_r($msg . PHP_EOL);
                    Log::error($msg);
                    report($e);
                    $fail++;
                }
            }
            if (count($arrRakutenError) !== 0) {
                foreach ($arrRakutenError as $dataMail) {
                    if (!App::environment(['local', 'test'])) {
                        $configSakure = Config::get('mail')['sakura'];
                        $swiftTransport = (new Swift_SmtpTransport(
                            $configSakure['host'],
                            $configSakure['port'],
                            $configSakure['encryption']
                        ))
                        ->setStreamOptions(Config::get('mail')['stream'])
                        ->setUsername($configSakure['username'])
                        ->setPassword($configSakure['password']);
                        $mailTo  = $dataMail['mail_to'];
                    } else {
                        $configTest = Config::get('mail')['mail_test'];
                        $swiftTransport = (new Swift_SmtpTransport(
                            $configTest['host'],
                            $configTest['port'],
                            $configTest['encryption']
                        ))
                        ->setStreamOptions(Config::get('mail')['stream'])
                        ->setUsername($configTest['username'])
                        ->setPassword($configTest['password']);
                        $mailer = new Swift_Mailer($swiftTransport);
                        Mail::setSwiftMailer($mailer);
                        $mailTo  = $dataMail['mail_to'];
                    }
                    try {
                        Mail::raw($dataMail['data']->mail_content, function ($message) use ($dataMail, $mailTo) {
                            !empty($dataMail['data']->mail_from) ?
                                $message->from($dataMail['data']->mail_from) : $message->from('rakuten@diy-tool.com');
                            $message->to($mailTo);
                            !empty($dataMail['data']->mail_subject) ? $message->subject($dataMail['data']->mail_subject) : '';
                            if (!empty($dataMail['data']->attached_file_path) && $dataMail['data']->attached_file_path !== 'null') {
                                $attachedFilePath = storage_path('uploads/attached_file/');
                                if (!empty($dataMail['data']->attached_file_path) && $dataMail['data']->attached_file_path !== 'null') {
                                    if (file_exists($attachedFilePath . $dataMail['data']->attached_file_path)) {
                                        $message->attach($attachedFilePath . trim($dataMail['data']->attached_file_path));
                                    }
                                }
                            }
                            if ($dataMail['data']->mail_content != strip_tags($dataMail['data']->mail_content)) {
                                $contentEncoder = new \Swift_Mime_ContentEncoder_PlainContentEncoder('8bit');
                                $message->setEncoder($contentEncoder);
                                $message->addPart($dataMail['data']->mail_content, 'text/html');
                            } else {
                                $plainEncoder = new \Swift_Mime_ContentEncoder_PlainContentEncoder('8bit');
                                $message->setEncoder($plainEncoder);
                                $message->addPart($dataMail['data']->mail_content, 'text/plain');
                            }
                        });
                    } catch (\Exception $e) {
                        $msg = $e->getMessage();
                        print_r($msg . PHP_EOL);
                        Log::error($msg);
                        report($e);
                        $fail++;
                    }
                }
            }
            Log::info("Send mail success: $success and fail: $fail records.");
            print_r("Send mail success: $success and fail: $fail records." . PHP_EOL);
        } else {
            Log::info('No data');
            print_r("No data" . PHP_EOL);
        }
        Event::fire(new eCommand($this->signature, array('end' => true, 'error' => $this->error)));
        $totalTime = round(microtime(true) - $start, 2);
        Log::info("End batch process mail send with total time: $totalTime s.");
        print_r("End batch process mail send with total time: $totalTime s.");
    }
}
