<?php
/**
 * Batch process update status cancel order, insert data send mail
 *
 * @package    App\Console\Commands
 * @subpackage ProcessKeppinHaibanOrderCancel
 * @copyright  Copyright (c) 2017 RiverCrane! Corporation. All Rights Reserved.
 * @author     Le Hung<le.hung.rcvn2012@gmail.com>
 */

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\Batches\MstOrder;
use App\Models\Batches\DtOrderProductDetail;
use App\Models\Batches\MstMailTemplate;
use \App\Models\Batches\DtSendMailList;
use App\Models\Batches\MstSettlementManage;
use App\Models\Batches\MstPaymentAccount;
use App\Models\Batches\MstOrderDetail;
use App\Models\Batches\DtRePayment;
use App\Console\Commands\ProcessMailQueue;
use Illuminate\Support\Facades\Log;
use App\Notification;
use App\Events\Command as eCommand;
use Event;
use DB;

class ProcessKeppinHaibanOrderCancel extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'process:keppin-haiban-order-cancel';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update data cancel order';

    /**
     * count total records updated to databases
     *
     * @var int
     */
    protected $countUpdate = 0;

    /**
     * The error.
     *
     * @var array
     */
    public $error = [];

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //Create log
        $arrayReplace = [':', '-'];
        $folder       = str_replace($arrayReplace, '_', $this->signature);
        Log::useDailyFiles(storage_path() . "/logs/$folder/history.log");
        //Process data
        Event::fire(new eCommand($this->signature, array('start' => true)));
        Log::info('Start batch process update cancel order.');
        print_r("Start batch process update cancel order." . PHP_EOL);
        $start       = microtime(true);
        Log::info('=== Start process cancel order. ===');
        print_r("=== Start process cancel order. ===" . PHP_EOL);
        $this->processCancelOrderResult();
        Log::info('=== End process cancel order. ===');
        print_r("=== End process cancel order. ===" . PHP_EOL);
        Event::fire(new eCommand($this->signature, array('end' => true, 'error' => $this->error)));
        $totalTime = round(microtime(true) - $start, 2);
        Log::info("End batch process update cancel order with total time: $totalTime s.");
        print_r("End batch process update cancel order with total time: $totalTime s.");
    }


    /**
    * Process cancel order result
    * @return void
    */
    public function processCancelOrderResult()
    {
        $modelOPD   = new DtOrderProductDetail();
        $modelO     = new MstOrder();
        $modelOD    = new MstOrderDetail();
        $modelSML   = new DtSendMailList();
        $dataOrder  = $modelO->getDataCancel();
        $modelMT  = new MstMailTemplate();
        $count = count($dataOrder);
        if ($count > 0) {
            Log::info("Total records prepare process: $count records.");
            print_r("Total records prepare process: $count records." . PHP_EOL);
            try {
                foreach ($dataOrder as $key => $value) {
                    // Get row
                    $keppin = 0;
                    $haiban = 0;
                    $normal = 0;
                    $dataAddSendMailList = [];
                    $dataMailTemplate    = [];
                    $dataDtOrderDetail =  $modelOPD->where('receive_id', $value->receive_id)
                            ->get(['product_status']);
                    if (count($dataDtOrderDetail) > 0) {
                        foreach ($dataDtOrderDetail as $keyD => $valueD) {
                            if ($valueD->product_status === 13) {
                                $keppin = 1;
                            } elseif ($valueD->product_status === 14) {
                                $haiban = 1;
                            } else {
                                $normal = 1;
                            }
                        }
                    }
                    $mailId = 0;
                    if ($normal === 0) {
                        DB::transaction(function () use ($modelO, $modelOPD, $modelOD, $modelSML, $modelMT, $value) {
                            $dataUpdateOrder = [];
                            $dataUpdateOrder['ship_charge']            = 0;
                            $dataUpdateOrder['total_price']            = 0;
                            $dataUpdateOrder['pay_charge']             = 0;
                            $dataUpdateOrder['pay_charge_discount']    = 0;
                            $dataUpdateOrder['used_point']             = 0;
                            $dataUpdateOrder['used_coupon']            = 0;
                            $dataUpdateOrder['request_price']          = 0;
                            $dataUpdateOrder['goods_price']            = 0;
                            $dataUpdateOrder['order_status']           = ORDER_STATUS['CANCEL'];
                            $dataUpdateOrder['order_sub_status']       = ORDER_SUB_STATUS['DONE'];
                            $dataUpdateOrder['is_mail_sent']           = 1;
                            $dataUpdateOrder['is_mall_update']         = 0;
                            $dataUpdateOrder['cancel_reason']         = '2';
                            $modelO->updateData([$value->receive_id], $dataUpdateOrder);
                            $modelOPD->updateCancelStock($value->receive_id);
                            if (in_array($value->payment_method, [2, 6, 7])) {
                                if ($value->payment_status === 1) {
                                    $this->processRePaymentCancel($value);
                                }
                            }
                            $arrUpdateOrderDetail = [
                                'quantity' => 0,
                                'price'    => 0,
                            ];
                            $modelOD->updateData(['receive_id' => $value->receive_id], $arrUpdateOrderDetail);
                            if (in_array($value->payment_method, [2, 6])) {
                                if ($value->payment_status === 1) {
                                    $dataMailTemplate = $modelMT->getDataCancelOrder(172);
                                } else {
                                    $dataMailTemplate = $modelMT->getDataCancelOrder(202);
                                }
                            } else {
                                if ($value->mall_id === 9) {
                                    $dataMailTemplate = $modelMT->getDataCancelOrder(779);
                                } else {
                                    $dataMailTemplate = $modelMT->getDataCancelOrder(107);
                                }
                            }

                            $mailContent = '';
                            $mailSubject = '';
                            if (!empty($dataMailTemplate)) {
                                list($mailContent, $arrUpdate) = ProcessMailQueue::processContent(
                                    $dataMailTemplate->mail_content,
                                    $value->payment_method,
                                    $value->received_order_id,
                                    true
                                );
                                $mailSubject = ProcessMailQueue::processContent(
                                    $dataMailTemplate->mail_subject,
                                    $value->payment_method,
                                    $value->received_order_id,
                                    false
                                );
                            }
                            if (!empty($value->signature_content)) {
                                $mailContent = $mailContent . PHP_EOL . $value->signature_content;
                            }
                            $mailId = isset($dataMailTemplate->mail_id) ? $dataMailTemplate->mail_id : '';
                            // Add new record to dt_send_mail_list
                            $dataAddSendMailList['receive_order_id']    = $value->received_order_id;
                            $dataAddSendMailList['order_status_id']     = ORDER_STATUS['CANCEL'];
                            $dataAddSendMailList['order_sub_status_id'] = ORDER_SUB_STATUS['DONE'];
                            $dataAddSendMailList['operater_send_index'] = 0;
                            $dataAddSendMailList['payment_method']      = $value->payment_method;
                            $dataAddSendMailList['mall_id']             = $value->mall_id;
                            $dataAddSendMailList['mail_id']             = $mailId;
                            $dataAddSendMailList['mail_subject']        = $mailSubject;
                            $dataAddSendMailList['mail_content']        = $mailContent;
                            $dataAddSendMailList['attached_file_path']  = null;
                            $dataAddSendMailList['mail_to']             = $value->email;
                            $dataAddSendMailList['mail_cc']             = env('MAIL_CC');
                            $dataAddSendMailList['mail_bcc']            = env('MAIL_BCC');
                            $dataAddSendMailList['mail_from']           = $value->mall_id === 1 ?
                                                                        'rakuten@diy-tool.com' :
                                                                        env('MAIL_FROM_ADDRESS');
                            $dataAddSendMailList['send_status']         = 0;
                            $dataAddSendMailList['send_type']           = 1;
                            $dataAddSendMailList['error_code']          = null;
                            $dataAddSendMailList['error_message']       = null;
                            $dataAddSendMailList['in_ope_cd']           = 'OPE99999';
                            $dataAddSendMailList['in_date']             = now();
                            $dataAddSendMailList['up_date']             = now();
                            $dataAddSendMailList['up_ope_cd']           = 'OPE99999';
                            $modelSML->insert($dataAddSendMailList);
                            //Update product_status
                            $modelOPD->updateDataByReceive(
                                [$value->receive_id],
                                ['product_status' => PRODUCT_STATUS['CANCEL']]
                            );
                            $this->countUpdate++;
                        }, 5);
                    }
                }
            } catch (\Exception $e) {
                $message = "Can't process update cancel";
                print_r($message . PHP_EOL);
                Log::error($message);
                report($e);
            }
            Log::info("Total insert to send mail list : {$this->countUpdate}");
            print_r("Total insert to send mail list : {$this->countUpdate}" . PHP_EOL);

            Log::info("Total records update product status: {$this->countUpdate} records.");
            print_r("Total records update product status:{$this->countUpdate} records.". PHP_EOL);
        } else {
            Log::info('No data');
            print_r("No data" . PHP_EOL);
        }
    }

    /**
    * Re-payment cancel.
    *
    * @param object $dataOrder
    * @param int $newRequestPrice
    * @param int $oldRequestPrice
    * @return void
    */
    public function processRePaymentCancel($dataOrder)
    {
        $modelRP = new DtRePayment();
        $modelRP->repay_price = $dataOrder->request_price;
        if ($dataOrder->payment_method === 1 && $dataOrder->mall_id !== 3) {
            $modelRP->repay_bank_code        = null;
            $modelRP->repay_bank_name        = 'クレカ返金';
            $modelRP->repay_bank_branch_code = null;
            $modelRP->repay_bank_branch_name = 'クレカ返金';
            $modelRP->repay_name             = 'クレカ返金';
            $modelRP->repay_bank_account     = 'クレカ返金';
        } elseif ($dataOrder->mall_id === 3) {
            $modelRP->repay_bank_code        = null;
            $modelRP->repay_bank_name        = 'アマゾン';
            $modelRP->repay_bank_branch_code = null;
            $modelRP->repay_bank_branch_name = 'アマゾン';
            $modelRP->repay_name             = 'アマゾン';
            $modelRP->repay_bank_account     = 'アマゾン';
        }
        $modelRP->receive_id                  = $dataOrder->receive_id;
        $modelRP->return_no                   = null;
        $modelRP->repayment_time              = 0;
        $modelRP->payment_id                  = null;
        $modelRP->repayment_status            = 0;
        $modelRP->repayment_type              = 0;
        $modelRP->return_coupon               = $dataOrder->used_coupon;
        $modelRP->return_point                = $dataOrder->used_point;
        $modelRP->deposit_transfer_commission = 0;
        $modelRP->return_fee                  = 0;
        $modelRP->total_return_price          = $modelRP->repay_price + $modelRP->deposit_transfer_commission;
        $modelRP->is_return                   = 0;
        $modelRP->repayment_date              = null;
        $modelRP->in_ope_cd                   = 'OPE99999';
        $modelRP->in_date                     = now();
        $modelRP->up_ope_cd                   = 'OPE99999';
        $modelRP->up_date                     = now();
        $modelRP->save();
        return true;
    }
}
