<?php
/**
 * Batch process tmp_check_edit table
 *
 * @package    App\Console\Commands
 * @subpackage ProcessSwitchNankoToriyose
 * @copyright  Copyright (c) 2018 RiverCrane! Corporation. All Rights Reserved.
 * @author     Nguyen Tan<nguyen_ta@daitotools.com>
 */

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;
use App\Models\Batches\MasterSpProcessLog;
use App\Notification;
use App\Notifications\SlackNotification;
use DB;
use Event;
use App\Events\Command as eCommand;

class ProcessSwitchNankoToriyose extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'process:switch-nanko-toriyose';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Switch 2 Nanko or Toriyose';

    /**
     * The error.
     *
     * @var array
     */
    public $error = [];

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        Event::fire(new eCommand($this->signature, array('start' => true)));
        // update_stock_list_wait_answer_num
        Log::info('Start batch process stock_list wait answer_num .');
        print_r("Start batch process stock_list wait answer_num." . PHP_EOL);
        $start    = microtime(true);
        DB::connection('horunba')->select('CALL update_stock_list_wait_answer_num()');
        $totalTime = round(microtime(true) - $start, 2);
        Log::info("End batch process stock_list wait answer_num with total time: $totalTime s.");
        print_r("End batch process stock_list wait answer_num with total time: $totalTime s." . PHP_EOL);

        // sp_switch_2_nanko_or_toriyose
        Log::info('Start batch process Switch 2 Nanko or Toriyose.');
        print_r("Start batch process Switch 2 Nanko or Toriyose." . PHP_EOL);
        $start    = microtime(true);
        DB::connection('horunba')->select('CALL sp_switch_2_nanko_or_toriyose()');
        $totalTime = round(microtime(true) - $start, 2);
        Log::info("End batch process Switch 2 Nanko or Toriyose with total time: $totalTime s.");
        print_r("End batch process Switch 2 Nanko or Toriyose with total time: $totalTime s." . PHP_EOL);
        Event::fire(new eCommand($this->signature, array('end' => true, 'error' => $this->error)));
    }
}
