<?php
/**
 * Batch process request glsc delivery return
 *
 * @package    App\Console\Commands
 * @subpackage ProcessRequestGlscDeliveryReturn
 * @copyright  Copyright (c) 2018 RiverCrane! Corporation. All Rights Reserved.
 * @author     Truong Nghia<truong.van.nghia@rivercrane.vn>
 */
namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;
use App\Notifications\SlackNotification;
use App\Models\Batches\TInsertShippingGroupKey;
use App\Models\Batches\TInsertShipping;
use App\Models\Batches\DtReturn;
use App\Models\Batches\TAka;
use App\Models\Batches\DtDelivery;
use App\Models\Batches\DtDeliveryDetail;
use App\Models\Batches\MstStockStatus;
use App\Models\Batches\DtReceiptLedger;
use App\Notification;
use App\Events\Command as eCommand;
use App\Custom\Utilities;
use Event;
use DB;

class ProcessRequestGlscDeliveryReturnNew extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'process:request-glsc-delivery-return-new';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'When stock is available, request to delivery to customer.';

    /**
     * The notification.
     *
     * @var object
     */
    protected $slack;

    /**
     * The error.
     *
     * @var array
     */
    public $error = [];

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $arrayReplace = [':', '-'];
        $folder       = str_replace($arrayReplace, '_', $this->signature);
        Log::useDailyFiles(storage_path() . "/logs/$folder/history.log");
        Event::fire(new eCommand($this->signature, array('start' => true)));
        $this->slack = new Notification(CHANNEL['horunba']);
        $start = microtime(true);
        Log::info('Start batch process GLSC delivery return.');
        print_r("Start batch process GLSC delivery return." . PHP_EOL);
        $modelDR = new DtReturn();
        $modelD = new DtDelivery();
        $modelDD = new DtDeliveryDetail();
        $modelStock = new MstStockStatus();
        $modelRL = new DtReceiptLedger();
        $data    = $modelDR->getDataDeliveryReturn();
        $message = '----Start process content 1----';
        Log::info($message);
        print_r($message . PHP_EOL);
        $count   = count($data);
        if (count($data) === 0) {
            Log::info('No data');
            print_r("No data. " . PHP_EOL);
        } else {
            $modelKey     = new TInsertShippingGroupKey();
            $suc          = 0;
            $fail         = 0;
            $receiveID    = '';
            $groupKey     = '';
            $arrUpAfterIn = [];
            $arrEnd       = [];
            $check        = false;
            $supplierId   = '';
            $orderLNo     = 1;
            $returnAddId  = '';
            $maxNo        = $modelD->getMaxOrderReturn();
            $index        = 0;
            $tempReturnNum = [];
            $tempProductCode = [];
            if (count($maxNo) === 0) {
                $index = 400000;
            } else {
                $arrTemp = explode('-', $maxNo->received_order_id);
                $index = $arrTemp[2];
            }
            try {
                DB::beginTransaction();
                $arrInserted = [];
                $countDetail = 1;
                foreach ($data as $value) {
                    $arrInsD  = [];
                    $arrInsDD = [];
                    if (in_array($value->product_code, $tempProductCode)) {
                        $tempReturnNum[$value->product_code] = $value->return_quantity + $tempReturnNum[$value->product_code];
                    } else {
                        $tempProductCode[] = $value->product_code;
                        $tempReturnNum[$value->product_code] = $value->return_quantity;
                    }

                    $checkStock = ($value->nanko_num - $value->order_zan_num - $value->return_num);

                    if (($value->receive_instruction === -1 && $checkStock < $tempReturnNum[$value->product_code] ) ||
                        ($value->receive_instruction === 2 &&  $value->return_num < $value->return_quantity)) {
                        $error = "Return no : " . $value->return_no."\n";
                        $error .= "Product code : " . $value->product_code."\n";
                        $error .= "Quantity : " . $value->return_quantity."\n";
                        $error .= "Return Num : " . $value->return_num."\n";
                        if ($value->receive_instruction === -1) {
                            $error .= "Nanko Num : " . $value->nanko_num."\n";
                            $error .= "Order Zan Num : " . $value->order_zan_num."\n";
                            $error .= "Return Num Temp: " . $tempReturnNum[$value->product_code]."\n";
                        }
                        $error .= "Message : フリー在庫の不足";
                        $this->slack->notify(new SlackNotification($error));
                        $modelDR->updateData(
                            [
                                'return_no'      => $value->return_no,
                                'return_line_no' => $value->return_line_no,
                                'return_time'    => $value->return_time,
                            ],
                            [
                                'error_code'        => 'E',
                                'error_message'        => 'フリー在庫の不足'
                            ]
                        );
                        $tempReturnNum[$value->product_code] = $tempReturnNum[$value->product_code] - $value->return_quantity;
                        continue;
                    }

                    if ($receiveID !== $value->receive_id ||
                        $supplierId !== $value->supplier_id ||
                        $returnAddId !== $value->return_address_id ||
                        $countDetail > 49
                    ) {
                        $index++;
                        $receiveID   = $value->receive_id;
                        $supplierId  = $value->supplier_id;
                        $returnAddId = $value->return_address_id;
                        $receivedOrderId     = 'r-' . $value->supplier_id . '-' . $index;
                        $modelKey->where('tShippingG_Key', '=', 1)->increment('GroupKey', 1);
                        $groupKey = $modelKey->where('tShippingG_Key', '=', 1)->first();
                        if (count($arrEnd) !== 0) {
                            $arrEnd['detail_line_num']   = $orderLNo;
                            $arrEnd['product_code']      = 'Return-Dmy001';
                            $arrEnd['product_name']      = '返品出荷識別用';
                            $arrEnd['product_jan']       = '2016112109003';
                            $arrEnd['delivery_num']      = 1;
                            $arrEnd['maker_name']        = null;
                            $arrEnd['maker_code']        = null;
                            $arrEnd['product_image_url'] = null;
                            $modelDD->insert($arrEnd);
                            $arrEnd   = [];
                            $orderLNo = 1;
                            $countDetail = 1;
                        }
                        $check = true;
                    }
                    $subdivisionNum = $value->delivery_count + 1;
                    $keyTemp = $receivedOrderId . $subdivisionNum;
                    if ($value->product_oogata >= 1) {
                        $arrUpAfterIn[$keyTemp] = ['received_order_id' => $receivedOrderId, 'subdivision_num' => $subdivisionNum];
                    }
                    if (!in_array($keyTemp, $arrInserted)) {
                        $DeliPlanDate = date("Ymd");
                        if (date('H:i') > '15:00' || $value->delivery_instruction === 8) {
                            $DeliPlanDate = date("Ymd", strtotime("+1 days"));
                        }
                        $ShippingInstructions = $value->note . ' 「無」';

                        $arrInsD['received_order_id']     = $receivedOrderId;
                        $arrInsD['subdivision_num']       = $subdivisionNum;
                        $arrInsD['delivery_date']         = '';
                        $arrInsD['delivery_time']         = '';
                        $arrInsD['delivery_instrustions'] = $value->note;
                        $arrInsD['account']               = '';
                        $arrInsD['delivery_plan_date']    = $DeliPlanDate;
                        $arrInsD['total_price']           = 0;
                        $arrInsD['shipping_instructions'] = $ShippingInstructions;
                        $arrInsD['store_code']            = 1;
                        $arrInsD['invoice_remarks']       = '';
                        $arrInsD['delivery_code']         = 4;
                        $arrInsD['customer_name']         = '株式会社大都';
                        $arrInsD['delivery_name']         = $value->return_nm;
                        $arrInsD['delivery_postal']       = str_replace('-', '', $value->return_postal);
                        $arrInsD['delivery_perfecture']   = $value->return_pref;
                        $arrInsD['delivery_city']         = $value->return_address;
                        $arrInsD['delivery_sub_address']  = '';
                        $arrInsD['delivery_tel']          = $value->return_tel;
                        $arrInsD['group_key']             = $groupKey->GroupKey;
                        $arrInsD['delivery_status']       = 0;
                        $arrInsD['in_part_cd']            = 1;
                        $arrInsD['in_date']               = now();
                        $arrInsD['in_ope_cd']             = 'OPE99999';
                        $arrInsD['up_date']               = now();
                        $arrInsD['up_ope_cd']             = 'OPE99999';
                        $modelD->insert($arrInsD);
                        $arrInserted[] = $keyTemp;
                    }
                    // $checkStock = ($value->nanko_num - $value->order_zan_num - $value->return_num);

                    // if (($value->receive_instruction === -1 && $checkStock < $tempReturnNum[$value->product_code] ) ||
                    //     ($value->receive_instruction === 2 &&  $value->return_num < $value->return_quantity)) {
                    //     $error = "Return no : " . $value->return_no."\n";
                    //     $error .= "Product code : " . $value->product_code."\n";
                    //     $error .= "Quantity : " . $value->return_quantity."\n";
                    //     $error .= "Return Num : " . $value->return_num."\n";
                    //     if ($value->receive_instruction === -1) {
                    //         $error .= "Nanko Num : " . $value->nanko_num."\n";
                    //         $error .= "Order Zan Num : " . $value->order_zan_num."\n";
                    //         $error .= "Return Num Temp: " . $tempReturnNum[$value->product_code]."\n";
                    //     }
                    //     $error .= "Message : フリー在庫の不足";
                    //     $this->slack->notify(new SlackNotification($error));
                    //     $modelDR->updateData(
                    //         [
                    //             'return_no'      => $value->return_no,
                    //             'return_line_no' => $value->return_line_no,
                    //             'return_time'    => $value->return_time,
                    //         ],
                    //         [
                    //             'error_code'        => 'E',
                    //             'error_message'        => 'フリー在庫の不足'
                    //         ]
                    //     );
                    // } else {
                    $arrInsDD['received_order_id'] = $receivedOrderId;
                    $arrInsDD['subdivision_num']   = $value->delivery_count + 1;
                    $arrInsDD['detail_line_num']   = $orderLNo;
                    $arrInsDD['product_code']      = $value->product_code;
                    $arrInsDD['product_name']      = $value->product_name_long;
                    $arrInsDD['product_jan']       = $value->product_jan;
                    $arrInsDD['maker_name']        = $value->maker_full_nm;
                    $arrInsDD['maker_code']        = $value->product_maker_code;
                    $arrInsDD['product_image_url'] = $value->rak_img_url_1;
                    $arrInsDD['delivery_num']      = $value->return_quantity;
                    $arrInsDD['in_date']           = now();
                    $arrInsDD['in_ope_cd']         = 'OPE99999';
                    $arrInsDD['up_date']           = now();
                    $arrInsDD['up_ope_cd']         = 'OPE99999';
                    $countDetail++;
                    $modelDD->insert($arrInsDD);
                    if ($check) {
                        $arrEnd = $arrInsDD;
                        $check  = false;
                    }
                    $modelDR->updateData(
                        [
                            'return_no'      => $value->return_no,
                            'return_line_no' => $value->return_line_no,
                            'return_time'    => $value->return_time,
                        ],
                        [
                            'delivery_count'       => ($value->delivery_count + 1),
                            'delivery_instruction' => 1,
                            'supplier_return_no'   => $receivedOrderId,
                            'supplier_return_line' => $orderLNo,
                            'status'               => 2,
                        ]
                    );
                    if ($value->receive_instruction < 0) {
                        $modelStock->updateData(
                            $value->product_code,
                            [
                                'return_num'        => $value->return_num + $tempReturnNum[$value->product_code],
                                'wait_delivery_num' => $value->wait_delivery_num + $tempReturnNum[$value->product_code]
                            ]
                        );
                    }
                    $orderLNo++;
                    // }
                }
                if (count($arrEnd) !== 0) {
                    $arrEnd['detail_line_num']   = $orderLNo;
                    $arrEnd['product_code']      = 'Return-Dmy001';
                    $arrEnd['product_name']      = '返品出荷識別用';
                    $arrEnd['product_jan']       = '2016112109003';
                    $arrEnd['delivery_num']      = 1;
                    $arrEnd['maker_name']        = null;
                    $arrEnd['maker_code']        = null;
                    $arrEnd['product_image_url'] = null;
                    $modelDD->insert($arrEnd);
                }
                if (count($arrUpAfterIn) !== 0) {
                    // $arrKeys = array_map("unserialize", array_unique(array_map("serialize", $arrUpAfterIn)));
                    $arrUpdate = ['delivery_code' => 3];
                    foreach ($arrUpAfterIn as $item) {
                        $modelD->updateData($item, $arrUpdate);
                    }
                }
                DB::commit();
            } catch (\Exception $e) {
                DB::rollback();
                $message = "Can't process return: {$value->return_no}-{$value->return_line_no}-{$value->return_time} rollback";
                print_r($message . PHP_EOL);
                Log::error($message);
                print_r($e->getMessage());
                report($e);
            }
            $message = "Process success: {$count} record.";
            print_r($message . PHP_EOL);
            Log::error($message);
        }
        $message = '----End process content 1----';
        Log::info($message);
        print_r($message . PHP_EOL);
        $message = '----Start process content 2----';
        Log::info($message);
        print_r($message . PHP_EOL);
        $datas2 = $modelDR->getDataDeliveryReturn2();
        $total = count($datas2);
        if ($total === 0) {
            $message = 'No data.';
            Log::info($message);
            print_r($message . PHP_EOL);
        } else {
            $arrInsert = [];

            try {
                DB::beginTransaction();
                DB::connection('edi')->beginTransaction();
                $modelAT = new TAka();
                foreach ($datas2 as $key => $value) {
                    if (is_null($value->product_maker_code)) {
                        $makerCode = '';
                    } else {
                        $makerCode = $value->product_maker_code;
                    }
                    $returnCode = (string)$value->supplier_return_no;
                    $check = false;
                    if (empty($returnCode)) {
                        $returnCode = 'r-' . $value->supplier_id . '-' . $value->return_no;
                        $check = true;
                    }
                    $arrInsert = [];
                    $arrInsert['suppliercd']             = (int)$value->supplier_id;
                    $arrInsert['SerialNo']               = 0;
                    $arrInsert['return_date']            = empty($value->return_date) ? date('Y-m-d') : $value->return_date;
                    $arrInsert['return_code']            = $returnCode;
                    $arrInsert['item_code']              = (string)$value->product_code;
                    $arrInsert['jan_code']               = (string)$value->product_jan;
                    $arrInsert['supplier_code']          = (string)$value->supplier_id;
                    $arrInsert['supplier_name']          = (string)$value->supplier_nm;
                    $arrInsert['maker_name']             = (string)$value->maker_full_nm;
                    $arrInsert['maker_code']             = $makerCode;
                    $arrInsert['note_code']              = '';
                    $arrInsert['name']                   = (string)$value->product_name_long;
                    $arrInsert['price']                  = (int)$value->price_invoice;
                    $arrInsert['s_price']                = (int)$value->return_tanka;
                    $arrInsert['quantity']               = (int)$value->return_quantity;
                    $arrInsert['quantity_return']        = (int)$value->return_quantity;
                    $arrInsert['color']                  = (string)$value->product_color;
                    $arrInsert['size']                   = (string)$value->product_size;
                    $arrInsert['fax']                    = (string)$value->supplier_fax;
                    $arrInsert['mail']                   = (string)$value->mail_address;
                    $arrInsert['other']                  = '';
                    $arrInsert['suppliercd_flg']         = 0;
                    $arrInsert['suppliercd_date']        = null;
                    $arrInsert['buyer_flg']              = 0;
                    $arrInsert['buyer_date']             = null;
                    $arrInsert['buyer_flg_t_admin_id']   = null;
                    $arrInsert['captain_flg']            = 0;
                    $arrInsert['captain_date']           = null;
                    $arrInsert['captain_flg_t_admin_id'] = null;
                    $arrInsert['slip_flg']               = 0;
                    $arrInsert['slip_date']              = null;
                    $arrInsert['slip_flg_t_admin_id']    = null;
                    $arrInsert['del_flg']                = 0;
                    $arrInsert['valid_flg']              = 1;
                    $arrInsert['created_at']             = now();
                    $arrInsert['updated_at']             = now();
                    $modelAT->insert($arrInsert);
                    $arrUpdateReturn = [
                        'red_voucher' => 1,
                        'up_ope_cd'   => 'OPE99999',
                        'up_date'     => now(),
                        'status'      => 3
                    ];
                    if ($check) {
                        $arrUpdateReturn['supplier_return_no'] = $returnCode;
                    }
                    $modelDR->updateData(
                        [
                            'return_no'      => $value->return_no,
                            'return_line_no' => $value->return_line_no,
                            'return_time'    => $value->return_time,
                        ],
                        $arrUpdateReturn
                    );
					if(($value->delivery_instruction === -1) && ($value->return_type_mid_id === 5)){
	                    $strTemp = $value->return_no . $value->return_line_no .$value->return_time;
	                    $modelRL->add2ReceiptLedgerInStockRIR([
	                        'pa_order_code'        => $strTemp,
	                        'pa_product_code'      => $value->product_code,
	                        'pa_arrival_num'       => $value->return_quantity,
	                        'pa_supplier_id'       => $value->supplier_id,
	                        'pa_stock_type'        => 4,
	                        'pa_stock_detail_type' => 4,
	                    ]);
	                    $modelRL->add2ReceiptLedgerOutStock([
	                        'pa_order_code'        => $returnCode,
	                        'pa_product_code'      => $value->product_code,
	                        'pa_price_invoice'     => $value->price_invoice,
	                        'pa_delivery_num'      => $value->return_quantity,
	                        'pa_supplier_id'       => $value->supplier_id,
	                        'pa_stock_type'        => 3,
	                        'pa_stock_detail_type' => 3,
	                    ]);
					}
                }
                DB::commit();
                DB::connection('edi')->commit();
                $message = "Process success: {$total} record.";
                print_r($message . PHP_EOL);
                Log::error($message);
            } catch (\Exception $e) {
                DB::rollback();
                DB::connection('edi')->rollback();
                $message = "Can't process insert t_aka";
                print_r($message . PHP_EOL);
                Log::error($message);
                report($e);
            }
        }
        $message = '----End process content 2----';
        Log::info($message);
        print_r($message . PHP_EOL);
        Event::fire(new eCommand($this->signature, array('end' => true, 'error' => $this->error)));
        $totalTime = round(microtime(true) - $start, 2);
        Log::info("End batch process request GLSC with total time: $totalTime.");
        print_r("End batch process request GLSC with total time: $totalTime s." . PHP_EOL);
    }
}
