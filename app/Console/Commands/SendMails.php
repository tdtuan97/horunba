<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Notifications\SlackNotification;
use App\Custom\CurlPost;

class SendMails extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'email:send';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send drip e-mails to a user';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $content = "@メッシ大都 " . PHP_EOL;
        $content .= basename(__CLASS__) . PHP_EOL;
        $content .= '198680-20190426-01201808 : 指定された注文番号の注文は存在しませんでした。' . PHP_EOL;

        CurlPost::pushChatGoogle(null, $content);
    }
}
