<?php
/**
 * Batch process smaregi del product
 *
 * @package    App\Console\Commands
 * @subpackage ProcessSmaregiDelProduct
 * @copyright  Copyright (c) 2019 RiverCrane! Corporation. All Rights Reserved.
 * @author     Truong Nghia<van.qui.rcvn2012@gmail.com>
 */

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\Batches\MstStoreProduct;
use App\Notifications\SlackNotification;
use Illuminate\Support\Facades\Log;
use App\Events\Command as eCommand;
use App\Notification;
use App;
use Event;

class ProcessSmaregiDelProduct extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'process:smaregi-del-product';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Smaregi del product';

    /**
     * The error.
     *
     * @var string
     */
    public $error = [];

    /**
     * The folder.
     *
     * @var string
     */
    public $folder = null;

    /**
     * Size insert
     * @var int
     */
    private $sizeInsert = 1000;
    /**
     * Smaregi contract id
     * @var contract_id
     */
    public $contractId = '';

    /**
     * Smaregi access token
     * @var accessToken
     */
    public $accessToken = '';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        $this->contractId   = env('CONTRACT_ID');
        $this->accessToken  = env('ACCESS_TOKEN');
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $start        = microtime(true);
        //Create log
        $arrayReplace = [':', '-'];
        $this->folder = str_replace($arrayReplace, '_', $this->signature);
        Log::useDailyFiles(storage_path() . "/logs/$this->folder/history.log");
        //Process data
        Event::fire(new eCommand($this->signature, array('start' => true)));
        Log::info('Start process smaregi del product');
        print_r("Start process smaregi del product" . PHP_EOL);
        
        $this->processContent();
        
        Event::fire(new eCommand($this->signature, array('end' => true, 'error' => $this->error)));
        $totalTime = round(microtime(true) - $start, 2);
        Log::info("End batch process smaregi del product: $totalTime s.");
        print_r("End batch process smaregi del product: $totalTime s." . PHP_EOL);
    }
    
    /**
     * Process content
     *
     * @return void
     */
    private function processContent()
    {
        $mstStoreProduct = new MstStoreProduct();
        $result = $mstStoreProduct->getSmaregiDelProduct($this->sizeInsert);
        if ($result->count() === 0) {
            Log::info('No data');
            print_r("No data" . PHP_EOL);
        }
        $requestParams['header'] = array(
            "Cache-Control: no-cache",
            "Content-Type: application/x-www-form-urlencoded",
            "X_contract_id: $this->contractId",
            "X_access_token: $this->accessToken",
        );
        foreach ($result as $data) {
            $requestParams['fields'] = http_build_query(array(
                'proc_name' => 'product_upd',
                'params' => '{
                    "proc_info": {"proc_division" : "D"},
                    "data":[
                        {
                            "table_name":"Product",
                            "rows": [{"productId":"'. $data->store_product_id .'"}]
                        }
                    ]
                }'
            ));
            if (!App::environment(['local', 'test'])) {
                $response = $this->curlGet($requestParams);
                if (!empty($response) && !isset($response->error_code)) {
                    $mstStoreProduct->updateData(['product_code' => $data->product_code], ['is_updated' => 4]);
                } else {
                    $mstStoreProduct->updateData(['product_code' => $data->product_code], ['is_updated' => 98]);
                }
            } else {
                $mstStoreProduct->updateData(['product_code' => $data->product_code], ['is_updated' => 4]);
            }
        }
    }
    
    /**
     * Request process api
     *
     * @param  array $paramOptions
     * @return json encode
     */
    private function curlGet($paramOptions)
    {
        $slack      = new Notification(CHANNEL['horunba']);
        $smaregiUrl = "https://webapi.smaregi.jp/access/";
        $ch         = curl_init($smaregiUrl);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $paramOptions['header']);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $paramOptions['fields']);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_MAXREDIRS, 10);
        curl_setopt($ch, CURLOPT_TIMEOUT, 30);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
        $response = curl_exec($ch);
        curl_close($ch);
        $response = json_decode($response);
        if (isset($response->error_code)) {
            $arrError = (array) $response;
            $err      = implode("\n", $arrError);
            $error    = "------------------------------------------" . PHP_EOL;
            $error   .= basename(__CLASS__) . PHP_EOL;
            $error   .= $err;
            $error   .= PHP_EOL . "------------------------------------------" . PHP_EOL;
            $slack->notify(new SlackNotification($error));
            Log::info($err);
        }
        return $response;
    }
}
