<?php
/**
 * Batch process get order yahoo from api
 *
 * @package    App\Console\Commands
 * @subpackage ProcessGetOrderYahoo
 * @copyright  Copyright (c) 2018 RiverCrane! Corporation. All Rights Reserved.
 * @author     Van Qui<van.qui@rivercrane.vn>
 */

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;
use App\Notifications\SlackNotification;
use App\Custom\Utilities;
use App\Notification;
use App\Events\Command as eCommand;
use Event;
use Config;
use Helper;
use DB;
use App\Models\Batches\YahooToken;
use App\Models\Batches\TYahooOrderList;
use App\Models\Batches\TYahooOrder;

class ProcessGetOrderYahoo extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'process:get-order-yahoo {checkOrder=nocheck}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Process get order Yahoo';

    /**
     * The notification.
     *
     * @var object
     */
    protected $slack;

    /**
     * The error.
     *
     * @var array
     */
    public $error = [];

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //Create log
        $arrayReplace = [':', '-'];
        $folder       = str_replace($arrayReplace, '_', $this->signature);
        $folder       = explode(' ', $folder);
        $folder       = $folder[0];
        $checkOrder   = $this->argument('checkOrder');
        $signature    = explode(' ', $this->signature);
        $signature    = $signature[0];
        if ($checkOrder === 'check') {
            $signature = $signature . ' check';
        }

        Log::useDailyFiles(storage_path() . "/logs/$folder/history.log");
        //Process data
        Event::fire(new eCommand($signature, array('start' => true)));
        Log::info('Start batch process get order Yahoo.');
        print_r("Start batch process get order Yahoo." . PHP_EOL);
        $start       = microtime(true);
        $this->slack = new Notification(CHANNEL['horunba']);

        $yahooToken  = new YahooToken();
        $accessToken = $yahooToken->find('order')->access_token;

        Log::info('Start process order list.');
        print_r("Start process order list." . PHP_EOL);
        $check = $this->processOrderList($accessToken);
        Log::info('End process order list.');
        print_r("End process order list." . PHP_EOL);
        if ($check) {
            Log::info('Start process order info.');
            print_r("Start process order info." . PHP_EOL);
            $this->processOrderInfo($accessToken);
            Log::info('End process order info.');
            print_r("End process order info." . PHP_EOL);
        }
        Event::fire(new eCommand($signature, array('end' => true, 'error' => $this->error)));
        $totalTime = round(microtime(true) - $start, 2);
        Log::info("End batch process get order Yahoo with total time: $totalTime s.");
        print_r("End batch process get order Yahoo with total time: $totalTime s.");
    }

    /**
     * Process order list
     *
     * @param  string $accessToken
     * @return boolean
     */
    private function processOrderList($accessToken)
    {
        DB::beginTransaction();
        try {
            $checkOrder    = $this->argument('checkOrder');
            $modelYOL      = new TYahooOrderList();
            if ($checkOrder === 'check') {
                $orderTimeFrom = date("YmdHis", strtotime("-3 days"));
                $orderTimeTo   = date("YmdHis", strtotime("-30 minutes"));
            } else {
                $orderTimeFrom = date("YmdHis", strtotime("-70 minutes"));
                $orderTimeTo   = date("YmdHis", strtotime("-30 minutes"));
            }

            $yahooUrl   = "https://circus.shopping.yahooapis.jp/ShoppingWebService/V1/orderList";
            $requestXml = view('mapping.xml.yahoo-list', [
                'result'        => 2000,
                'start'         => 1,
                'sort'          => '+order_time',
                'orderTimeFrom' => $orderTimeFrom,
                'orderTimeTo'   => $orderTimeTo,
                'field'         => 'Status,TotalCount,OrderId,Version,Index',
                'sellerId'      => 'diy-tool'
            ])->render();
            $yahooHeader = array(
                "Authorization:Bearer " . $accessToken,
            );
            $ch = curl_init($yahooUrl);
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_HTTPHEADER, $yahooHeader);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $requestXml);
            $responseXml = curl_exec($ch);
            curl_close($ch);
            $responData = simplexml_load_string($responseXml);
            if ($responData->getName() === "Error") {
                $message = trim(Helper::val($responData->Message));
                print_r($message . PHP_EOL);
                Log::error($message);
                throw new \Exception($message);
            } else {
                $data       = [];
                $status     = Helper::val($responData->Status);
                $totalCount = Helper::val($responData->Search->TotalCount);
                foreach ($responData->Search->OrderInfo as $orderInfo) {
                    $orderId = Helper::val($orderInfo->OrderId);
                    $data[$orderId] = [
                        'order_list_status'     => $status,
                        'order_list_totalcount' => $totalCount,
                        'order_list_orderid'    => $orderId,
                        'order_list_version'    => Helper::val($orderInfo->Version),
                        'order_list_index'      => Helper::val($orderInfo->Index),
                        'status'                => 0,
                        'create_at'             => date("Y-m-d H:i:s")
                    ];
                }
                if (count($data) > 0) {
                    $totalInsert = 0;
                    $data = array_chunk($data, 50, true);
                    foreach ($data as $item) {
                        $orderIds    = array_column($item, 'order_list_orderid');
                        $orderExists = $modelYOL->getOrderByOrderId($orderIds);
                        foreach ($orderExists as $order) {
                            unset($item[$order->order_list_orderid]);
                        }
                        $countItem = count($item);
                        if ($countItem > 0) {
                            $totalInsert += $countItem;
                            $modelYOL->insert($item);
                        }
                    }
                    $message = "Insert $totalInsert records to t_yahoo_order_list success";
                    print_r($message . PHP_EOL);
                    Log::info($message);
                } else {
                    $message = "No data";
                    print_r($message . PHP_EOL);
                    Log::info($message);
                }
            }
            DB::commit();
            return true;
        } catch (\Exception $e) {
            DB::rollback();
            $message = "Can't process order list";
            print_r($message . PHP_EOL);
            Log::error($message);
            report($e);
            $this->error[] = $e->getMessage();
            return false;
        }
    }

    /**
     * Process order info
     *
     * @param  string $accessToken
     * @return void
     */
    private function processOrderInfo($accessToken)
    {
        DB::beginTransaction();
        try {
            $modelYOL  = new TYahooOrderList();
            $modelYO   = new TYahooOrder();
            $dataOrder = $modelYOL->getNewOrder();
            if ($dataOrder->count() === 0) {
                $message = "No data";
                print_r($message . PHP_EOL);
                Log::info($message);
                DB::rollback();
                return false;
            }

            $apiYahooFields = Config::get('common.batch_api_yahoo_fields');
            $fields         = $this->getApiYahooFields($apiYahooFields, ['Status']);
            $strFields      = implode(",", $fields);
            $yahooUrl       = "https://circus.shopping.yahooapis.jp/ShoppingWebService/V1/orderInfo";
            $yahooHeader    = array(
                "Authorization:Bearer " . $accessToken,
            );
            $totalInsert    = 0;
            foreach ($dataOrder as $order) {
                $requestXml = view('mapping.xml.yahoo', [
                    'orderId'  => $order->order_list_orderid,
                    'field'    => $strFields,
                    'sellerId' => 'diy-tool'
                ])->render();
                $ch = curl_init($yahooUrl);
                curl_setopt($ch, CURLOPT_POST, true);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($ch, CURLOPT_HTTPHEADER, $yahooHeader);
                curl_setopt($ch, CURLOPT_POSTFIELDS, $requestXml);
                $responseXml = curl_exec($ch);
                curl_close($ch);
                $retData = $this->processYahooXml($responseXml, $apiYahooFields);
                if ($retData['flg'] === 0) {
                    print_r($retData['message'] . PHP_EOL);
                    Log::error($retData['message']);
                    throw new \Exception($retData['message']);
                }

                if (empty($modelYO->getOrderByOrderId($order->order_list_orderid))) {
                    $totalInsert += count($retData['data']);
                    $modelYO->insert($retData['data']);
                }
                $modelYOL->updateData(
                    ['yahoo_order_info_key' => $order->yahoo_order_info_key],
                    ['status' => 1]
                );
            }
            $message = "Insert $totalInsert records to t_yahoo_order success";
            print_r($message . PHP_EOL);
            Log::info($message);
            DB::commit();
        } catch (\Exception $e) {
            DB::rollback();
            $message = "Can't process order info";
            print_r($message . PHP_EOL);
            Log::error($message);
            report($e);
            $this->error[] = $e->getMessage();
        }
    }

    /**
     * Get fields api yahoo
     *
     * @param   array  $apiYahooFields
     * @param   array  $ignoreFields
     * @return  array
     */
    private function getApiYahooFields($apiYahooFields, $ignoreFields = [])
    {
        $fields = array();
        foreach ($apiYahooFields as $key => $val) {
            $curVal = $val;
            do {
                if (is_array($curVal)) {
                    $subKey  = key($curVal);
                    $subVal  = $curVal[$subKey];
                    $curVal  = $subVal;

                    if (!is_array($subVal)) {
                        if ($subVal[0] !== '@') {
                            if (!in_array($subVal, $ignoreFields)) {
                                $fields[$key] = $subVal;
                            }
                        }
                        break;
                    }
                } else {
                    if ($curVal[0] !== '@') {
                        if (!in_array($curVal, $ignoreFields)) {
                            $fields[$key] = $curVal;
                        }
                    }
                    break;
                }
            } while (is_array($curVal));
        }
        return $fields;
    }

    /**
     * Process yahoo xml of response api
     *
     * @param  string $xml
     * @param  array  $apiYahooFields
     * @return array
     */
    private function processYahooXml($responseXml, $apiYahooFields)
    {
        $responData = simplexml_load_string($responseXml);
        if ($responData->getName() === "Error") {
            return [
                'flg'     => 0,
                'code'    => Helper::val($responData->Code),
                'message' => trim(Helper::val($responData->Message))
            ];
        }
        $countItem  = count($responData->Result->OrderInfo->Item);
        $retData    = array();
        for ($i = 0; $i < $countItem; $i++) {
            $data = array();
            foreach ($apiYahooFields as $key => $val) {
                $curVal  = $val;
                $resData = $responData;
                do {
                    if (is_array($curVal)) {
                        $subKey  = key($curVal);
                        $subVal  = $curVal[$subKey];
                        $curVal  = $subVal;

                        if ($subKey === 'Item') {
                            $resData = $resData->{$subKey}[$i];
                        } else {
                            $resData = $resData->{$subKey};
                        }
                        if (!is_array($subVal)) {
                            if ($subVal[0] === '@') {
                                $data[$key] = Helper::val($resData->attributes()->{substr($subVal, 1)});
                            } else {
                                $data[$key] = Helper::val($resData->{$subVal});
                            }
                            break;
                        }
                    } else {
                        if ($curVal[0] === '@') {
                            $data[$key] = Helper::val($resData->attributes()->{substr($curVal, 1)});
                        } else {
                            $data[$key] = Helper::val($resData->{$curVal});
                        }
                        break;
                    }
                } while (is_array($curVal));
            }
            $data['yahoobasketid']   = (string)$responData->Result->OrderInfo->OrderId . '-'
                . (string)$responData->Result->OrderInfo->Item[$i]->LineId;
            $data['statuschangeflg'] = 0;
            $data['formcancelflg']   = 0;
            $data['mail_serial']     = md5((string)$responData->Result->OrderInfo->OrderId);
            $data['process_flg']     = 0;
            $data['is_delete']       = 0;
            $data['created_at']      = date('Y-m-d H:i:s');

            $retData[] = $data;
        }
        return [
            'flg'  => 1,
            'data' => $retData
        ];
    }
}
