<?php
/**
 * Batch process result glsc delivery return
 *
 * @package    App\Console\Commands
 * @subpackage ProcessResultGlscDeliveryReturn
 * @copyright  Copyright (c) 2018 RiverCrane! Corporation. All Rights Reserved.
 * @author     Truong Nghia<truong.van.nghia@rivercrane.vn>
 */
namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;
use App\Notifications\SlackNotification;
use App\Models\Batches\DtReturn;
use App\Models\Batches\TShippingPerformanceModel ;
use App\Models\Batches\MstStockStatus;
use App\Models\Batches\DtReceiptLedger;
use App\Models\Batches\DtDelivery;
use App\Models\Batches\DtDeliveryDetail;
use App\Models\Batches\MstProduct;
use App\Models\Batches\LogTShippingPerformanceModel;
use App\Notification;
use App\Events\Command as eCommand;
use App\Custom\Utilities;
use Event;
use DB;
use Config;
use App;
use SoapClient;

class ProcessResultGlscDeliveryReturnNew extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'process:result-glsc-delivery-return-new';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = "Get return delivery' result(Direct delivery) from Glsc.";

    /**
     * The notification.
     *
     * @var object
     */
    protected $slack;

    /**
     * The error.
     *
     * @var array
     */
    public $error = [];

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $arrayReplace = [':', '-'];
        $folder       = str_replace($arrayReplace, '_', $this->signature);
        Log::useDailyFiles(storage_path() . "/logs/$folder/history.log");
        Event::fire(new eCommand($this->signature, array('start' => true)));
        $slack = new Notification(CHANNEL['horunba']);
        $start = microtime(true);
        Log::info('Start batch process result GLSC NEW delivery return.');
        print_r("Start batch process result GLSC NEW delivery return." . PHP_EOL);

        $modelR  = new DtReturn();
        $modelD  = new DtDelivery();
        $modelDD = new DtDeliveryDetail();
        $message = "Start process delivery result update status";
        Log::info($message);
        print_r($message . PHP_EOL);
        $datas1  =  $modelR->getDataProcessDelivetyResultNew();
        $total   = count($datas1);
        if ($total === 0) {
            $message = "No Data";
            Log::info($message);
            print_r($message . PHP_EOL);
        } else {
            $arrUpdated  = [];
            $arrDelivery = [
                'delivery_status' => 3,
                'up_ope_cd'       => 'OPE99999',
                'up_date'         => now(),
            ];
            foreach ($datas1 as $value) {
                $arrKey = [
                    'received_order_id' => $value->received_order_id,
                    'subdivision_num'   => $value->subdivision_num,
                ];
                $temp = $value->received_order_id . $value->subdivision_num;
                if (!in_array($temp, $arrUpdated)) {
                    $modelD->updateData($arrKey, $arrDelivery);
                    $arrUpdated[] = $temp;
                }
                $arrReturn   = [
                    'up_ope_cd' => 'OPE99999',
                    'up_date'   => now(),
                ];
                $arrReturn['delivery_instruction'] = 2;
                $arrReturn['delivery_real_date']   = date('Y-m-d', strtotime($value->delivery_real_date));
                $arrReturn['delivery_real_num']    = $value->delivery_real_num;
                $modelR->updateData(
                    [
                        'return_no'      => $value->return_no,
                        'return_line_no' => $value->return_line_no,
                        'return_time'    => $value->return_time,
                    ],
                    $arrReturn
                );
            }
        }
        $message = "End process delivery result update status with total: {$total} records";
        Log::info($message);
        print_r($message . PHP_EOL);
        $message = "Start process delivery result call API insert Return";
        Log::info($message);
        print_r($message . PHP_EOL);

        //Call API insert return
        $message = "End process delivery result call API insert Return";
        Log::info($message);
        print_r($message . PHP_EOL);

        if (App::environment(['local', 'test'])) {
            $auth = Config::get("apiservices.glsc_test");
        } else {
            $auth = Config::get("apiservices.glsc");
        }
        $glscUrl = "http://gls.ec-union.biz/glsc/glscapi/GlscApi.asmx?WSDL";
        try {
            $client  = new SoapClient($glscUrl);
            $params  = [
                'Auth' => [
                    'LoginID'     => $auth['LoginID'],
                    'Password'    => $auth['Password'],
                    'DeveloperID' => $auth['DeveloperID'],
                ],
                'Key' => [
                    'IncrementalOnly' => true,
                    'DeliDateFrom'    => date('Ymd'),
                    'DeliDateTo'      => date('Ymd'),
                    'DeliveryFlg'     => 0,
                    'DataKb'          => [5]
                ],
                'NotShipping' => [
                    'DataKb'       => '',
                    'OrderNo'      => '',
                    'OrderBNo'     => '',
                    'OrderLNo'     => '',
                    'DeliDate'     => '',
                    'InquiryNo'    => '',
                    'CustomerNm'   => '',
                    'DNm'          => '',
                    'DPostal'      => '',
                    'DPref'        => '',
                    'DAddress1'    => '',
                    'DAddress2'    => '',
                    'DTel'         => '',
                    'DConKb'       => '',
                    'ItemCd'       => '',
                    'ItemNm'       => '',
                    'JanCd'        => '',
                    'PlanQuantity' => '',
                    'Quantity'     => '',
                    'Note'         => '',
                    'DeliveryCode' => '',
                    'ShipNum'      => '',
                ]
            ];
            $response = $client->getShippingPerformanceList($params);
            $errInfo  = $response->getShippingPerformanceListResult->ErrInfo;
            if (isset($errInfo->ErrorCd) && $errInfo->ErrorCd === 0) {
                $shipping = $response->Shipping;
                if (!empty($shipping->tShippingPerformanceModel)) {
                    $listData = $shipping->tShippingPerformanceModel;
                    $success  = 0;
                    if (is_array($listData)) {
                        foreach ($listData as $item) {
                            $success += $this->processDataResponse($item);
                            $this->insertLogData($item);
                        }
                    } else {
                        $success += $this->processDataResponse($listData);
                        $this->insertLogData($listData);
                    }
                    $message = "Process [5] -> Update success: $success records";
                    Log::info($message);
                    print_r($message . PHP_EOL);
                } else {
                    $message = "Process [5] -> No data";
                    Log::info($message);
                    print_r($message . PHP_EOL);
                }
            } else {
                $message = "Process [5] -> No data";
                Log::info($message);
                print_r($message . PHP_EOL);
            }
        } catch (\Exception $e) {
            report($e);
            print_r($e->getMessage());
        }
        Event::fire(new eCommand($this->signature, array('end' => true, 'error' => $this->error)));
        $totalTime = round(microtime(true) - $start, 2);
        Log::info("End batch process result GLSC NEW with total time: $totalTime.");
        print_r("End batch process result GLSC NEW with total time: $totalTime s." . PHP_EOL);
    }

    /**
     * Process insert log
     *
     * @return void
     */
    public function processDataResponse($value)
    {
        $modelDR     = new DtReturn();
        $modelP      = new MstProduct();
        $modelDRL    = new DtReceiptLedger();
        $modelStock  = new MstStockStatus();
        $productInfo = $modelP->getDataByProductCode($value->ItemCd);
        $arrInsert  = [];
        $arrInsert['return_no']            = $value->OrderNo;
        $arrInsert['return_line_no']       = $value->OrderLNo;
        $arrInsert['return_date']          = $value->DeliDate;
        $arrInsert['deje_no']              = 0;
        $arrInsert['return_type_large_id'] = 5;
        $arrInsert['return_type_mid_id']   = 7;
        $arrInsert['status']               = 2;
        $arrInsert['receive_instruction']  = -1;
        $arrInsert['receive_result']       = -1;
        $arrInsert['delivery_instruction'] = 2;
        $arrInsert['delivery_result']      = -1;
        $arrInsert['red_voucher']          = -1;
        $arrInsert['parent_product_code']  = $value->ItemCd;
        $arrInsert['return_quantity']      = $value->Quantity;
        $arrInsert['return_tanka']         = 0;
        $arrInsert['supplier_id']          = $productInfo->price_supplier_id;
        $arrInsert['receive_plan_date']    = null;
        $arrInsert['receive_real_date']    = null;
        $arrInsert['receive_real_num']     = 0;
        $arrInsert['delivery_plan_date']   = null;
        $arrInsert['delivery_real_date']   = date('Y-m-d');
        $arrInsert['delivery_real_num']    = $value->Quantity;
        $arrInsert['receive_id']           = null;
        $arrInsert['payment_on_delivery']  = '';
        $arrInsert['note']                 = $value->Note;
        $arrInsert['receive_count']        = 0;
        $arrInsert['delivery_count']       = 0;
        $arrInsert['error_code']           = '';
        $arrInsert['error_message']        = '';
        $arrInsert['in_ope_cd']            = 'OPE99999';
        $arrInsert['in_date']              = date('Y-m-d H:i:s');
        $arrInsert['up_ope_cd']            = 'OPE99999';
        $arrInsert['up_date']              = date('Y-m-d H:i:s');
        try {
            $modelDR->insertIgnore($arrInsert);
            $modelDRL->add2ReceiptLedgerOutStock([
                    'pa_order_code'        => $value->OrderNo,
                    'pa_product_code'      => $value->ItemCd,
                    'pa_price_invoice'     => $productInfo->price_invoice,
                    'pa_delivery_num'      => $value->Quantity,
                    'pa_supplier_id'       => $productInfo->price_supplier_id,
                    'pa_stock_type'        => 3,
                    'pa_stock_detail_type' => 5,
                ]);
            $modelStock->updateStockOutStockR(
                $value->ItemCd,
                $value->Quantity,
                false //Because of Irregular, so that return_num have be not
            );
            return 1;
        } catch (\Exception $e) {
            report($e);
            print_r($e->getMessage());
            return 0;
        }
    }
    /**
     * Process insert log
     *
     * @return void
     */
    public function insertLogData($item)
    {
        $modelSPM  = new LogTShippingPerformanceModel();
        $arrInsert = [];
        $arrInsert['DataKb']       = $item->DataKb;
        $arrInsert['OrderNo']      = $item->OrderNo;
        $arrInsert['OrderBNo']     = $item->OrderBNo;
        $arrInsert['OrderLNo']     = $item->OrderLNo;
        $arrInsert['DeliDate']     = $item->DeliDate;
        $arrInsert['InquiryNo']    = $item->InquiryNo;
        $arrInsert['CustomerNm']   = $item->CustomerNm;
        $arrInsert['DNm']          = $item->DNm;
        $arrInsert['DPostal']      = $item->DPostal;
        $arrInsert['DPref']        = $item->DPref;
        $arrInsert['DAddress1']    = $item->DAddress1;
        $arrInsert['DAddress2']    = $item->DAddress2;
        $arrInsert['DTel']         = $item->DTel;
        $arrInsert['DConKb']       = $item->DConKb;
        $arrInsert['ItemCd']       = $item->ItemCd;
        $arrInsert['ItemNm']       = $item->ItemNm;
        $arrInsert['JanCd']        = $item->JanCd;
        $arrInsert['PlanQuantity'] = $item->PlanQuantity;
        $arrInsert['Quantity']     = $item->Quantity;
        $arrInsert['Note']         = $item->Note;
        $arrInsert['DeliveryCode'] = $item->DeliveryCode;
        $arrInsert['ShipNum']      = $item->ShipNum;
        $modelSPM->insert($arrInsert);
    }
}
