<?php
/**
 * Batch process tmp_check_edit table
 *
 * @package    App\Console\Commands
 * @subpackage ProcessTruncateCheckUserEditing
 * @copyright  Copyright (c) 2017 RiverCrane! Corporation. All Rights Reserved.
 * @author     Le hung<le.hung.rcvn2012@gmail.com>
 */

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;
use DB;

class ProcessTruncateCheckUserEditing extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'process:truncate-check-user-editing';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Truncate tmp_check_edit table';

    /**
     * The error.
     *
     * @var array
     */
    public $error = [];

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

        Log::info('Start batch process trucate table.');
        print_r("Start batch process trucate table." . PHP_EOL);
        $start    = microtime(true);
        DB::connection('horunba')->table('tmp_check_edit')->truncate();
        $totalTime = round(microtime(true) - $start, 2);
        Log::info("End batch process trucate table with total time: $totalTime s.");
        print_r("End batch process trucate table with total time: $totalTime s.");
    }
}
