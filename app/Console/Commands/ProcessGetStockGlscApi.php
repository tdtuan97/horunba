<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Config;
use App;
use SoapClient;
use App\Notification;
use App\Models\Batches\MstStockStatus;
use App\Models\Batches\LogTItemDeliveryProcessesModel;
use Illuminate\Support\Facades\Log;
use App\Events\Command as eCommand;
use Event;
use App\Notifications\SlackNotification;

class ProcessGetStockGlscApi extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'process:get-stock-glsc-api';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * The error.
     *
     * @var array
     */
    public $error = [];

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //Create log
        $arrayReplace = [':', '-'];
        $folder       = str_replace($arrayReplace, '_', $this->signature);
        Log::useDailyFiles(storage_path() . "/logs/$folder/history.log");
        //Process data
        Event::fire(new eCommand($this->signature, array('start' => true)));
        Log::info('Start batch process stock glsc by API.');
        print_r("Start batch process stock glsc by API." . PHP_EOL);
        $start       = microtime(true);
        $mstStockStatus    = new MstStockStatus();
        if (App::environment(['local', 'test'])) {
            $auth = Config::get("apiservices.glsc_test");
        } else {
            $auth = Config::get("apiservices.glsc");
        }
        $glscUrl = "http://gls.ec-union.biz/glsc/glscapi/GlscApi.asmx?WSDL";
        $countStockStatus = $mstStockStatus->count();
        $now        = strtotime(date('H:i'));
        $time       = strtotime('21:00');
        $time2      = strtotime('08:00');
        $params['Auth'] = [
            'LoginID'     => $auth['LoginID'],
            'Password'    => $auth['Password'],
            'DeveloperID' => $auth['DeveloperID'],
        ];
        $params['Key'] = [
            'ItemCd'            => '',
            'SupplierCd'        => '',
            'MakerCd'           => '',
            'OnlyToday'         => false,
            'OnlyStockShortage' => '',
            'StartLine'         => '',
            'EndLine'           => '',
            'UpdateFrom'        => '',
            'ItemCd'            => '',
            'UpdateTo'          => '',
        ];
        $params['ItemList'] = [
            'ItemCd'                => '',
            'ItemNm'                => '',
            'JanCd'                 => '',
            'SupplierNm'            => '',
            'MakerNm'               => '',
            'MakerNo'               => '',
            'ItemUrl'               => '',
            'Stock'                 => '',
            'Reservation'           => '',
            'AvailableStock'        => '',
            'RequestCnt'            => '',
            'ShippingCnt'           => '',
            'RequestPlanQuantity'   => '',
            'ShippingPlanQuantity'  => '',
            'RackNo'                => ''
        ];
        $client  = new SoapClient($glscUrl, array(
            'trace'              => true,
            'keep_alive'         => true,
            'connection_timeout' => 1000,
            'cache_wsdl'         => WSDL_CACHE_NONE,
            'compression'        => SOAP_COMPRESSION_ACCEPT | SOAP_COMPRESSION_GZIP | SOAP_COMPRESSION_DEFLATE,
        ));
        if ($now < $time && $now > $time2) {
            $params['Key']['OnlyToday'] = true;
            try {
                $response = $client->getItemForDeliveryProcessesList($params);
                $errInfo  = $response->getItemForDeliveryProcessesListResult->ErrInfo;
                $itemList = $response->ItemList;
                if (isset($errInfo->ErrorCd) && $errInfo->ErrorCd === 0) {
                    if (!empty($itemList->tItemDeliveryProcessesModel)) {
                        $timeOn = date('H:i');
                        $this->processDataResponse($itemList->tItemDeliveryProcessesModel, "$timeOn <> 21:00");
                    }
                }
            } catch (Exception $ex) {
                report($e);
            }
        } elseif ($now === $time) {
            if ($countStockStatus > 0) {
                $j = 0;
                for ($i = 1; $i <= $countStockStatus; $i += 2900) {
                    $j += 2900;
                    $params['Key']['StartLine'] = $i;
                    $params['Key']['EndLine']   = $j;
                    try {
                        $response = $client->getItemForDeliveryProcessesList($params);
                        $errInfo  = $response->getItemForDeliveryProcessesListResult->ErrInfo;
                        $itemList = $response->ItemList;
                        if (isset($errInfo->ErrorCd) && $errInfo->ErrorCd === 0) {
                            if (!empty($itemList->tItemDeliveryProcessesModel)) {
                                $this->processDataResponse($itemList->tItemDeliveryProcessesModel, "Process "
                                        . "$i to $j on 21:00");
                            }
                        }
                    } catch (Exception $ex) {
                        report($e);
                    }
                }
            }
        }

        Event::fire(new eCommand($this->signature, array('end' => true, 'error' => $this->error)));
        $totalTime = round(microtime(true) - $start, 2);
        Log::info("End batch process stock glsc result by API with total time: $totalTime s.");
        print_r("End batch process stock glsc result by API with total time: $totalTime s.");
    }

    /**
     * Process data response
     *
     * @return void
     */
    public function processDataResponse($data = null, $message = null)
    {
        if (!empty($data)) {
            Log::info("------$message------\n");
            $mstStockStatus    = new MstStockStatus();
            $slack = new Notification(CHANNEL['horunba']);
            $i = 0;
            $count = count($data);
            $countInsert = 0;
            $countUpdate = 0;
            $countNotify = 1;
            foreach ($data as $key => $value) {
                $item = $mstStockStatus::find($value->ItemCd);
                if (!empty($item)) {
                    if ($value->Stock < 0) {
                        $messageError = "Stock number of $value->ItemCd is : $value->Stock";
                        if ($countNotify <= 10) {
                            $slack->notify(new SlackNotification($messageError));
                            Log::info("$messageError. \n");
                        }
                        $countNotify++;
                    }
                    $item->glsc_num = $value->Stock;
                    $item->save();
                    $i++;
                }
                $log = $this->insertLogData($value);
                if (isset($log['insert'])) {
                    $countInsert += (int)$log['insert'];
                }
                if (isset($log['update'])) {
                    $countUpdate += (int)$log['update'];
                }
            }
            if ($countNotify > 10) {
                $messageError = "Total items have stock number less than 0: $countNotify";
                $slack->notify(new SlackNotification($messageError));
            }
            Log::info("Process stock glsc result by API with update to stock status: $i. \n");
            Log::info("Process stock glsc result by API with update to log: $countUpdate. \n");
            Log::info("Process stock glsc result by API with insert to log: $countInsert. \n");
            
            print_r("------$message------\n");
            print_r("Process stock glsc result by API with update to stock status: $i. \n");
            print_r("Process stock glsc result by API with update to log: $countUpdate. \n");
            print_r("Process stock glsc result by API with insert to log: $countInsert. \n");
            return true;
        }
        return false;
    }

    /**
     * Process insert log
     *
     * @return void
     */
    public function insertLogData($data)
    {
        $modelSPM  = new LogTItemDeliveryProcessesModel();
        $item = $modelSPM::find($data->ItemCd);
        if (!empty($item)) {
            $item->ItemCd        = $data->ItemCd;
            $item->ItemNm        = $data->ItemNm;
            $item->JanCd         = $data->JanCd;
            $item->SupplierNm    = $data->SupplierNm;
            $item->MakerNm       = $data->MakerNm;
            $item->MakerNo       = $data->MakerNo;
            $item->ItemUrl       = $data->ItemUrl;
            $item->Stock         = $data->Stock ? $data->Stock : 0;
            $item->Reservation   = $data->Reservation ? $data->Reservation : 0;
            $item->AvailableStock= $data->AvailableStock ? $data->AvailableStock : 0;
            $item->RequestCnt    = $data->RequestCnt ? $data->RequestCnt : 0;
            $item->ShippingCnt   = $data->ShippingCnt ? $data->ShippingCnt : 0;
            $item->RequestPlanQuantity = $data->RequestPlanQuantity ? $data->RequestPlanQuantity : 0;
            $item->ShippingPlanQuantity= $data->ShippingPlanQuantity ? $data->ShippingPlanQuantity : 0;
            $item->RackNo        = $data->RackNo ? $data->RackNo : 0;
            $item->UpdateAt      = date('Y-m-d H:i:s');
            $item->save();
            return ['update' => 1];
        } else {
            $modelSPM->ItemCd        = $data->ItemCd;
            $modelSPM->ItemNm        = $data->ItemNm;
            $modelSPM->JanCd         = $data->JanCd;
            $modelSPM->SupplierNm    = $data->SupplierNm;
            $modelSPM->MakerNm       = $data->MakerNm;
            $modelSPM->MakerNo       = $data->MakerNo;
            $modelSPM->ItemUrl       = $data->ItemUrl;
            $modelSPM->Stock         = $data->Stock ? $data->Stock : 0;
            $modelSPM->Reservation   = $data->Reservation ? $data->Reservation : 0;
            $modelSPM->AvailableStock= $data->AvailableStock ? $data->AvailableStock : 0;
            $modelSPM->RequestCnt    = $data->RequestCnt ? $data->RequestCnt : 0;
            $modelSPM->ShippingCnt   = $data->ShippingCnt ? $data->ShippingCnt : 0;
            $modelSPM->RequestPlanQuantity = $data->RequestPlanQuantity ? $data->RequestPlanQuantity : 0;
            $modelSPM->ShippingPlanQuantity= $data->ShippingPlanQuantity ? $data->ShippingPlanQuantity : 0;
            $modelSPM->RackNo        = $data->RackNo ? $data->RackNo : 0;
            $modelSPM->UpdateAt      = date('Y-m-d H:i:s');
            $modelSPM->save();
            return ['insert' => 1];
        }
    }
}
