<?php
/**
 * Batch process get mail from server sakura from imap
 *
 * @package    App\Console\Commands
 * @subpackage ProcessImapGetMail
 * @copyright  Copyright (c) 2018 RiverCrane! Corporation. All Rights Reserved.
 * @author     Lam Vinh<lam.vinh@rivercrane.com.vn>
 */

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\Batches\DtSendMailList;
use App\Models\Batches\DtReceiveMailList;
use Illuminate\Support\Facades\Log;
use App\Notifications\SlackNotification;
use App\Custom\Utilities;
use App\Notification;
use App;
use Config;
use Event;
use App\Events\Command as eCommand;
use App\Custom\Imap;

class ProcessImapGetMail extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'process:imap-get-mail';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Get mail from server sakura from imap';

    /**
     * The error.
     *
     * @var string
     */
    public $error = [];

    /**
     * Size insert
     * @var int
     */
    private $sizeInsert = 500;

    /**
     * The notification.
     *
     * @var object
     */
    protected $slack;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //Create log
        $arrayReplace = [':', '-'];
        $folder       = str_replace($arrayReplace, '_', $this->signature);
        Log::useDailyFiles(storage_path() . "/logs/$folder/history.log");
        //Process data
        Event::fire(new eCommand($this->signature, array('start' => true)));
        Log::info('Start process get mail from server sakura from imap');
        print_r("Start process get mail from server sakura from imap" . PHP_EOL);
        $start   = microtime(true);
        $this->slack   = new Notification(CHANNEL['horunba']);

        $this->processGetMail();
        //$this->processFilterMail();
        $this->updateReceivedOrderId();

        Event::fire(new eCommand($this->signature, array('end' => true, 'error' => $this->error)));
        $totalTime = round(microtime(true) - $start, 2);
        Log::info("End batch process get mail from server sakura from imap: $totalTime s.");
        print_r("End batch process get mail from server sakura from imap: $totalTime s.");
        //Execute batch process mail filter imap
        $path = env('PATH_COMMAND', 'artisan');
        system("php {$path} process:mail-filter");
    }

    /**
     * Process get mail
     * @return void
     */
    private function processGetMail()
    {
        Log::info('Start process get mail');
        print_r("Start process get mail" . PHP_EOL);
        $modelRM  = new DtReceiveMailList();
        $imapConf = Config::get('imap')['sakura'];
        if (App::environment(['local', 'test'])) {
            $imapConf = Config::get('imap')['diyfactory'];
        }
        $imap    = new Imap($imapConf);
        $uids    = $imap->searchMail('UNSEEN');
        $existUid = [];
        if (!empty($uids)) {
            $existUid = $modelRM->getExistUid($uids);
            if ($existUid->count() > 0) {
                $existUid = $existUid->pluck('uid_mail', 'uid_mail');
            }
        }
        $mailInf = array();
        $uidFail = array();
        if (!empty($uids)) {
            $flgInsert = true;
            foreach ($uids as $uid) {
                try {
                    $from = '';
                    $to   = '';
                    $header    = $imap->getHeader($uid);
                    if (isset($header->from[0])) {
                        if (!empty($header->from[0])) {
                            $host = isset($header->from[0]->host) ? $header->from[0]->host : '';
                            $from = $header->from[0]->mailbox . "@" . $host;
                        }
                    }
                    if (isset($header->to[0])) {
                        if (!empty($header->to[0])) {
                            $host = isset($header->to[0]->host) ? $header->to[0]->host : '';
                            $to   = $header->to[0]->mailbox . "@" . $host;
                        }
                    }
                    $arrMailCc = isset($header->cc) ? $header->cc : [];
                    $receiveDate = null;
                    if (isset($header->date)) {
                        $tmpDate = str_replace("(GMT+08:00)", "", $header->date);
                        $receiveDate = date("Y/m/d H:i:s", strtotime($tmpDate));
                    }
                    $cc        = [];
                    if (count($arrMailCc) !== 0) {
                        foreach ($arrMailCc as $item) {
                            $itemHost = isset($item->host) ? $item->host : '';
                            $cc[] = $item->mailbox . "@" . $itemHost;
                        }
                    }
                    $cc = implode(',', $cc);
                    if (isset($header->subject)) {
                        $textHeader = $header->subject;
                    } else {
                        $textHeader = '';
                    }
                    $subDec  = imap_mime_header_decode($textHeader);
                    $subject = '';
                    if (!empty($subDec)) {
                        foreach ($subDec as $sub) {
                            $tmpCharset = strtoupper($sub->charset);
                            if ($tmpCharset === 'ISO-2022-JP' || $tmpCharset === 'SHIFT_JIS') {
                                $subject .= mb_convert_encoding($sub->text, 'UTF-8', $tmpCharset);
                            } elseif ($tmpCharset === 'GB2312' || $tmpCharset === 'GB18030') {
                                $subject .= mb_convert_encoding($sub->text, 'UTF-8', 'GB18030');
                            } else {
                                $subject .= $sub->text;
                            }
                        }
                    }
                    $allData = $imap->getAllBody($uid);

                    $contentHtml  = [];
                    $contentText  = [];
                    $contentImage = [];
                    $contentAtts  = [];
                    foreach ($allData as $data) {
                        if ($data['attachment']) {
                            $contentAtts[] = $data;
                        } elseif ($data['type'] === 'TEXT') {
                            if ($data['subtype'] === 'HTML') {
                                $contentHtml[] = $data['data'];
                            } elseif ($data['subtype'] === 'PLAIN') {
                                $contentText[] = $data['data'];
                            } elseif ($data['subtype'] === 'RFC822-HEADERS') {
                                $contentText[] = $data['data'];
                            }
                        } elseif ($data['type'] === 'IMAGE') {
                            $contentImage[] = $data;
                        } elseif ($data['type'] === 'MESSAGE') {
                            $contentText[] = $data['data'];
                        }
                    }
                    $content = '';
                    if (count($contentHtml) > 0) {
                        $content = implode("\n", $contentHtml);
                    }
                    if ($content === '') {
                        $content = implode("\n", $contentText);
                    }

                    if ($content === '') {
                        sleep(3);
                        $imap->reOpen();
                        $allData = $imap->getAllBody($uid);
                        $contentHtml  = [];
                        $contentText  = [];
                        $contentImage = [];
                        $contentAtts  = [];
                        foreach ($allData as $data) {
                            if ($data['attachment']) {
                                $contentAtts[] = $data;
                            } elseif ($data['type'] === 'TEXT') {
                                if ($data['subtype'] === 'HTML') {
                                    $contentHtml[] = $data['data'];
                                } elseif ($data['subtype'] === 'PLAIN') {
                                    $contentText[] = $data['data'];
                                } elseif ($data['subtype'] === 'RFC822-HEADERS') {
                                    $contentText[] = $data['data'];
                                }
                            } elseif ($data['type'] === 'IMAGE') {
                                $contentImage[] = $data;
                            } elseif ($data['type'] === 'MESSAGE') {
                                $contentText[] = $data['data'];
                            }
                        }
                        $content = '';
                        if (count($contentHtml) > 0) {
                            $content = implode("\n", $contentHtml);
                        }
                        if ($content === '') {
                            $content = implode("\n", $contentText);
                        }
                    }


                    $countAtt = 0;
                    $attNames = array();
                    foreach ($contentAtts as $atts) {
                        sleep(1);
                        $countAtt++;
                        $tmpFileName = pathinfo(iconv_mime_decode($atts['name'], 2));
                        $extension   = '';
                        if (isset($tmpFileName['extension'])) {
                            $extension = '.' . $tmpFileName['extension'];
                        }
                        $fileName = time() . $extension;
                        $filePath = storage_path('uploads' . DIRECTORY_SEPARATOR . 'attached_file_imap')
                            . DIRECTORY_SEPARATOR . $fileName;
                        $attNames[] = env('APP_URL_IMAP', '') . $fileName;
                        file_put_contents($filePath, $atts['data']);
                        if ($countAtt >= 3) {
                            break;
                        }
                    }

                    foreach ($contentImage as $image) {
                        sleep(1);
                        $fileName   = time() . '.' . $image['subtype'];
                        $filePath   = storage_path('uploads' . DIRECTORY_SEPARATOR . 'attached_file_imap')
                            . DIRECTORY_SEPARATOR . $fileName;
                        $imageUrl   = env('APP_URL_IMAP', '') . $fileName;
                        if (!empty($image['id'])) {
                            $content = str_replace("cid:" . $image['id'], $imageUrl, $content);
                        } else {
                            $attNames[] = $imageUrl;
                        }
                        file_put_contents($filePath, $image['data']);
                    }
                    if (isset($existUid[$uid])) {
                        $mailInf = array();
                        continue;
                    }
                    $mailInf = [
                        'mail_from'           => $from,
                        'mail_to'             => $to,
                        'mail_to_cc'          => $cc,
                        'mail_subject'        => $subject,
                        'receive_date'        => $receiveDate,
                        'mail_content'        => $content,
                        'attached_file_path1' => isset($attNames[0]) ? $attNames[0] : '',
                        'attached_file_path2' => isset($attNames[1]) ? $attNames[1] : '',
                        'attached_file_path3' => isset($attNames[2]) ? $attNames[2] : '',
                        'status'              => 0,
                        'uid_mail'            => $uid,
                        'in_ope_cd'           => 'OPE99999',
                        'in_date'             => date('Y-m-d H:i:s'),
                        'up_ope_cd'           => 'OPE99999',
                        'up_date'             => date('Y-m-d H:i:s')
                    ];

                    $modelRM->insert($mailInf);
                    $mailInf = array();
                } catch (\Exception $ex) {
                    $imap->reOpen();
                    $tmpMsg = $ex->getMessage();
                    if (strpos($tmpMsg, 'No body information available') !== false) {
                        $uidFail[] = $uid;
                    } else {
                        $this->error[] = Utilities::checkMessageException($ex);
                        $error  = "------------------------------------------" . PHP_EOL;
                        $error .= basename(__CLASS__) . PHP_EOL;
                        $error .= Utilities::checkMessageException($ex);
                        $error .= PHP_EOL . "Mail uid: " . $uid;
                        $error .= PHP_EOL . "------------------------------------------" . PHP_EOL;
                        $this->slack->notify(new SlackNotification($error));
                        Log::error(Utilities::checkMessageException($ex));
                        print_r("$error");
                    }
                    $flgInsert = false;
                }
            }
            $countFail = count($uidFail);
            if ($countFail > 0) {
                $error  = "------------------------------------------" . PHP_EOL;
                $error .= basename(__CLASS__) . PHP_EOL;
                $error .= PHP_EOL . "Mail uid no body information available ($countFail): " . implode(", ", $uidFail);
                $error .= PHP_EOL . "------------------------------------------" . PHP_EOL;
                $this->slack->notify(new SlackNotification($error));
                Log::error($error);
                print_r("$error");
            }
            if ($flgInsert) {
                Log::info('Insert new email success');
                print_r('Insert new email success' . PHP_EOL);
            } else {
                Log::info('Insert new email fail');
                print_r('Insert new email fail' . PHP_EOL);
            }
        } else {
            Log::info('Don\'t have new email');
            print_r('Don\'t have new email' . PHP_EOL);
        }
        Log::info("End process get mail.");
        print_r("End process get mail." . PHP_EOL);
    }

    /**
     * Process filter mail
     * @return void
     */
    private function processFilterMail()
    {
        Log::info("Start process filter mail.");
        print_r("Start process filter mail." . PHP_EOL);

        $modelRM = new DtReceiveMailList();
        $datas   = $modelRM->getDataFilter();
        if ($datas->count() === 0) {
            Log::info('No data');
            print_r("No data. " . PHP_EOL);
        } else {
            foreach ($datas as $data) {
                $dataUpdate = array();
                if (mb_strpos($data->mail_subject, "【楽天市場】問い合わせ内容ご確認（自動配信メール）") !== false
                    && $data->mail_from === "order@rakuten.co.jp") {
                    $dataUpdate['mail_from'] = $data->mail_to;
                    $dataUpdate['mail_to']   = "order@rakuten.co.jp";
                }

                if (mb_strpos($data->mail_subject, "【Yahoo!ショッピング】DIY_FACTORY_ONLINE_SHOPへのお問い合わせ")
                        !== false) {
                    $tempMail = $this->filterString($data->mail_content, "お客様のメールアドレス：", "問い合わせ種別：");
                    if ($tempMail !== false) {
                        $dataUpdate['mail_from'] = $tempMail;
                    }
                }

                if (mb_strpos($data->mail_from, "monotos.co.jp") !== false) {
                    $orderNo = $this->filterString($data->mail_content, "ご注文番号：", "お支払合計：");
                    if ($orderNo !== false) {
                        $dataUpdate['receive_order_id'] = $orderNo;
                    }
                }

                if (mb_strpos($data->mail_subject, "【楽天】注文内容ご確認(携帯)") !== false) {
                    $orderNo = $this->filterString($data->mail_content, "[受注番号]", "※上記の番号に");
                    if ($orderNo !== false) {
                        $dataUpdate['receive_order_id'] = $orderNo;
                    }
                }

                if (mb_strpos($data->mail_subject, "【楽天市場】") !== false) {
                    $orderNo = $this->filterString($data->mail_content, "[受注番号]", "[日時]");
                    if ($orderNo !== false) {
                        $dataUpdate['receive_order_id'] = $orderNo;
                    }
                }
                $dataUpdate['is_filtered'] = 1;
                $dataUpdate['up_ope_cd']   = 'OPE99999';
                $dataUpdate['up_date']     = date('Y-m-d H:i:s');
                $modelRM->updateDataByKey($data->rec_mail_index, $dataUpdate);
            }
            Log::info('Update filter mail success');
            print_r("Update filter mail success" . PHP_EOL);
        }
        Log::info("End process filter mail.");
        print_r("End process filter mail." . PHP_EOL);
    }

    /**
     * Filter string and return required string
     *
     * @param  string  $strSource
     * @param  string  $strStart
     * @param  string  $strEnd
     * @return string
     */
    private function filterString($strSource, $strStart, $strEnd)
    {
        $posStart = mb_strpos($strSource, $strStart);
        $posEnd   = mb_strpos($strSource, $strEnd);
        if ($posStart !== false && $posEnd !== false) {
            $tmp = mb_substr($strSource, $posStart, $posEnd - $posStart);
            $tmp = str_replace("　", "", $tmp);
            $tmp = str_replace(">", "", $tmp);
            $tmp = str_replace(" ", "", $tmp);
            $tmp = str_replace("\r\n", "", $tmp);
            $tmp = str_replace("\n", "", $tmp);
            $tmp = str_replace("\t", "", $tmp);
            $tmp = str_replace($strStart, "", $tmp);
            return $tmp;
        }
        return false;
    }

    /**
     * Update received order id
     * @return void
     */
    private function updateReceivedOrderId()
    {
        Log::info("Start process update received order id.");
        print_r("Start process update received order id." . PHP_EOL);
        $modelRM = new DtReceiveMailList();
        $datas   = $modelRM->getDataReceiveMailSakura();
        if (count($datas) === 0) {
            Log::info('No data');
            print_r("No data. " . PHP_EOL);
        } else {
            $flgUpdate = true;
            foreach ($datas as $data) {
                $arrUpdate = [];
                if (!is_null($data->received_order_id)) {
                    $arrUpdate['receive_order_id'] = $data->received_order_id;
                } else {
                    $arrUpdate['receive_order_id'] = 'Please check Order ID';
                }
                $arrUpdate['up_ope_cd'] = 'OPE99999';
                $arrUpdate['up_date']   = date('Y-m-d H:i:s');
                if (!$modelRM->updateDataByKey($data->rec_mail_index, $arrUpdate)) {
                    $flgUpdate = false;
                }
            }
            if ($flgUpdate) {
                Log::info('Update receive id success');
                print_r("Update receive id success" . PHP_EOL);
            } else {
                Log::info('Update receive id fail');
                print_r("Update receive id fail" . PHP_EOL);
            }
        }
        Log::info("End process update received order id.");
        print_r("End process update received order id." . PHP_EOL);
    }

    /**
     * Process Re get mail
     */
    private function processReGetMail()
    {
        Log::info('Start process get mail');
        print_r("Start process get mail" . PHP_EOL);
        $modelRM  = new DtReceiveMailList();
        $imapConf = Config::get('imap')['sakura'];
        if (App::environment(['local', 'test'])) {
            $imapConf = Config::get('imap')['diyfactory'];
        }
        $imap   = new Imap($imapConf);
        $result = $modelRM->getEmptyContentMail();
        if ($result->count() === 0) {
            Log::info('Don\'t have email');
            print_r('Don\'t have email' . PHP_EOL);
            Log::info("End process get mail.");
            print_r("End process get mail." . PHP_EOL);
            return false;
        }

        $uids    = $result->pluck('uid_mail')->toArray();
        $mailInf = array();
        $uidFail = array();
        if (!empty($uids)) {
            $flgInsert = true;
            foreach ($uids as $uid) {
                try {
                    $from = '';
                    $to   = '';
                    $header    = $imap->getHeader($uid);
                    if (isset($header->from[0])) {
                        if (!empty($header->from[0])) {
                            $host = isset($header->from[0]->host) ? $header->from[0]->host : '';
                            $from = $header->from[0]->mailbox . "@" . $host;
                        }
                    }
                    if (isset($header->to[0])) {
                        if (!empty($header->to[0])) {
                            $host = isset($header->to[0]->host) ? $header->to[0]->host : '';
                            $to   = $header->to[0]->mailbox . "@" . $host;
                        }
                    }
                    $arrMailCc = isset($header->cc) ? $header->cc : [];
                    $receiveDate = null;
                    if (isset($header->date)) {
                        $tmpDate = str_replace("(GMT+08:00)", "", $header->date);
                        $receiveDate = date("Y/m/d H:i:s", strtotime($tmpDate));
                    }
                    $cc        = [];
                    if (count($arrMailCc) !== 0) {
                        foreach ($arrMailCc as $item) {
                            $itemHost = isset($item->host) ? $item->host : '';
                            $cc[] = $item->mailbox . "@" . $itemHost;
                        }
                    }
                    $cc = implode(',', $cc);
                    if (isset($header->subject)) {
                        $textHeader = $header->subject;
                    } else {
                        $textHeader = '';
                    }
                    $subDec  = imap_mime_header_decode($textHeader);
                    $subject = '';
                    if (!empty($subDec)) {
                        foreach ($subDec as $sub) {
                            $tmpCharset = strtoupper($sub->charset);
                            if ($tmpCharset === 'ISO-2022-JP' || $tmpCharset === 'SHIFT_JIS') {
                                $subject .= mb_convert_encoding($sub->text, 'UTF-8', $tmpCharset);
                            } elseif ($tmpCharset === 'GB2312' || $tmpCharset === 'GB18030') {
                                $subject .= mb_convert_encoding($sub->text, 'UTF-8', 'GB18030');
                            } else {
                                $subject .= $sub->text;
                            }
                        }
                    }
                    $allData = $imap->getAllBody($uid);

                    $contentHtml  = [];
                    $contentText  = [];
                    $contentImage = [];
                    $contentAtts  = [];
                    foreach ($allData as $data) {
                        if ($data['attachment']) {
                            $contentAtts[] = $data;
                        } elseif ($data['type'] === 'TEXT') {
                            if ($data['subtype'] === 'HTML') {
                                $contentHtml[] = $data['data'];
                            } elseif ($data['subtype'] === 'PLAIN') {
                                $contentText[] = $data['data'];
                            } elseif ($data['subtype'] === 'RFC822-HEADERS') {
                                $contentText[] = $data['data'];
                            }
                        } elseif ($data['type'] === 'IMAGE') {
                            $contentImage[] = $data;
                        } elseif ($data['type'] === 'MESSAGE') {
                            $contentText[] = $data['data'];
                        }
                    }
                    $content = '';
                    if (count($contentHtml) > 0) {
                        $content = implode("\n", $contentHtml);
                    }
                    if ($content === '') {
                        $content = implode("\n", $contentText);
                    }

                    if ($content === '') {
                        sleep(3);
                        $imap->reOpen();
                        $allData = $imap->getAllBody($uid);
                        $contentHtml  = [];
                        $contentText  = [];
                        $contentImage = [];
                        $contentAtts  = [];
                        foreach ($allData as $data) {
                            if ($data['attachment']) {
                                $contentAtts[] = $data;
                            } elseif ($data['type'] === 'TEXT') {
                                if ($data['subtype'] === 'HTML') {
                                    $contentHtml[] = $data['data'];
                                } elseif ($data['subtype'] === 'PLAIN') {
                                    $contentText[] = $data['data'];
                                } elseif ($data['subtype'] === 'RFC822-HEADERS') {
                                    $contentText[] = $data['data'];
                                }
                            } elseif ($data['type'] === 'IMAGE') {
                                $contentImage[] = $data;
                            } elseif ($data['type'] === 'MESSAGE') {
                                $contentText[] = $data['data'];
                            }
                        }
                        $content = '';
                        if (count($contentHtml) > 0) {
                            $content = implode("\n", $contentHtml);
                        }
                        if ($content === '') {
                            $content = implode("\n", $contentText);
                        }
                    }


                    $countAtt = 0;
                    $attNames = array();
                    foreach ($contentAtts as $atts) {
                        sleep(1);
                        $countAtt++;
                        $tmpFileName = pathinfo(iconv_mime_decode($atts['name'], 2));
                        $extension   = '';
                        if (isset($tmpFileName['extension'])) {
                            $extension = '.' . $tmpFileName['extension'];
                        }
                        $fileName = time() . $extension;
                        $filePath = storage_path('uploads' . DIRECTORY_SEPARATOR . 'attached_file_imap')
                            . DIRECTORY_SEPARATOR . $fileName;
                        $attNames[] = env('APP_URL_IMAP', '') . $fileName;
                        file_put_contents($filePath, $atts['data']);
                        if ($countAtt >= 3) {
                            break;
                        }
                    }

                    foreach ($contentImage as $image) {
                        sleep(1);
                        $fileName   = time() . '.' . $image['subtype'];
                        $filePath   = storage_path('uploads' . DIRECTORY_SEPARATOR . 'attached_file_imap')
                            . DIRECTORY_SEPARATOR . $fileName;
                        $imageUrl   = env('APP_URL_IMAP', '') . $fileName;
                        if (!empty($image['id'])) {
                            $content = str_replace("cid:" . $image['id'], $imageUrl, $content);
                        } else {
                            $attNames[] = $imageUrl;
                        }
                        file_put_contents($filePath, $image['data']);
                    }

                    $mailInf = [
                        'mail_from'           => $from,
                        'mail_to'             => $to,
                        'mail_to_cc'          => $cc,
                        'mail_subject'        => $subject,
                        'receive_date'        => $receiveDate,
                        'mail_content'        => $content,
                        'attached_file_path1' => isset($attNames[0]) ? $attNames[0] : '',
                        'attached_file_path2' => isset($attNames[1]) ? $attNames[1] : '',
                        'attached_file_path3' => isset($attNames[2]) ? $attNames[2] : '',
                        'status'              => 0,
                        'uid_mail'            => $uid,
                        'in_ope_cd'           => 'OPE99999',
                        'in_date'             => date('Y-m-d H:i:s'),
                        'up_ope_cd'           => 'OPE99999',
                        'up_date'             => date('Y-m-d H:i:s')
                    ];

                    $modelRM->updateDataByUID($uid, $mailInf);
                    $mailInf = array();
                } catch (\Exception $ex) {
                    $imap->reOpen();
                    $tmpMsg = $ex->getMessage();
                    if (strpos($tmpMsg, 'No body information available') !== false) {
                        $uidFail[] = $uid;
                    } else {
                        $this->error[] = Utilities::checkMessageException($ex);
                        $error  = "------------------------------------------" . PHP_EOL;
                        $error .= basename(__CLASS__) . PHP_EOL;
                        $error .= Utilities::checkMessageException($ex);
                        $error .= PHP_EOL . "Mail uid: " . $uid;
                        $error .= PHP_EOL . "------------------------------------------" . PHP_EOL;
                        $this->slack->notify(new SlackNotification($error));
                        Log::error(Utilities::checkMessageException($ex));
                        print_r("$error");
                    }
                    $flgInsert = false;
                }
            }
            $countFail = count($uidFail);
            if ($countFail > 0) {
                $error  = "------------------------------------------" . PHP_EOL;
                $error .= basename(__CLASS__) . PHP_EOL;
                $error .= PHP_EOL . "Mail uid no body information available ($countFail): " . implode(", ", $uidFail);
                $error .= PHP_EOL . "------------------------------------------" . PHP_EOL;
                $this->slack->notify(new SlackNotification($error));
                Log::error($error);
                print_r("$error");
            }
            if ($flgInsert) {
                Log::info('Update email success');
                print_r('Update email success' . PHP_EOL);
            } else {
                Log::info('Update email fail');
                print_r('Update email fail' . PHP_EOL);
            }
        } else {
            Log::info('Don\'t have email');
            print_r('Don\'t have email' . PHP_EOL);
        }
        Log::info("End process get mail.");
        print_r("End process get mail." . PHP_EOL);
    }
}
