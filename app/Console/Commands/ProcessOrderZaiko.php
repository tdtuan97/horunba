<?php
/**
 * Batch process order zaiko
 *
 * @package    App\Console\Commands
 * @subpackage ProcessOrderZaiko
 * @copyright  Copyright (c) 2017 RiverCrane! Corporation. All Rights Reserved.
 * @author     Truong Nghia<truong.van.nghia@rivercrane.vn>
 */

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\Batches\DtOrderProductDetail;
use Illuminate\Support\Facades\Log;
use App\Models\Batches\TAdmin;
use App\Models\Batches\TZaiko;
use App\Models\Batches\MstOrder;
use App\Notifications\SlackNotification;
use App\Models\Batches\DtEstimation;
use App\Custom\Utilities;
use App\Notification;
use App\Events\Command as eCommand;
use Event;

class ProcessOrderZaiko extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'process:order-zaiko';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'To confirm stock available, system is issuing a'.
    ' estimating requesion to supplier(EDI System) with product have product_status is WAIT_TO_ESTIMATE';

    /**
     * The limit of step process.
     *
     * @var int
     */
    protected $limit = 500;

    /**
     * The error.
     *
     * @var array
     */
    public $error = [];

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //Create log
        $arrayReplace = [':', '-'];
        $folder       = str_replace($arrayReplace, '_', $this->signature);
        Log::useDailyFiles(storage_path() . "/logs/$folder/history.log");
        //Process data
        Event::fire(new eCommand($this->signature, array('start' => true)));
        Log::info('Start batch process order zaiko.');
        print_r("Start batch process order zaiko." . PHP_EOL);
        $slack           = new Notification(CHANNEL['horunba']);
        $start           = microtime(true);
        $modelOPD        = new DtOrderProductDetail();
        $modelAdmin      = new TAdmin();
        $modelZaiko      = new TZaiko();
        $modelEstimation = new DtEstimation();
        $modelO          = new MstOrder();
        $datas           = $modelOPD->getDataProcessZaiko();
        if (count($datas) === 0) {
            Log::info('No data process');
            print_r("No data process" . PHP_EOL);
            Log::info("End batch process order zaiko.");
            print_r("End batch process order zaiko." . PHP_EOL);
        } else {
            $arrInsert     = [];
            $arrUpdate     = [];
            $arrEstimation = [];
            $index         = 0;
            $dataAdmin     = $modelAdmin->getDataZaiko()->toArray();
            $maxOrderCode  = $modelZaiko->getMaxOrderCode();
            $yearMonth     = date('Ym');
            if (count($maxOrderCode) > 0) {
                $orderCode = $maxOrderCode->toArray()['order_code'];
                if (strpos($orderCode, $yearMonth) === 2) {
                    $index = (int)substr($orderCode, 8);
                }
            }
            foreach ($datas as $data) {
                if ($data->cheetah_status === 4) {
                    $modelOPD->updateData(
                        $data->receive_id,
                        $data->detail_line_num,
                        $data->sub_line_num,
                        ['product_status' => PRODUCT_STATUS['SALE_STOP']]
                    );
                    $modelO->updateData([$data->receive_id], ['order_sub_status' => ORDER_SUB_STATUS['STOP_SALE']]);
                } elseif ($data->cheetah_status === 3) {
                    $modelOPD->updateData(
                        $data->receive_id,
                        $data->detail_line_num,
                        $data->sub_line_num,
                        ['product_status' => PRODUCT_STATUS['OUT_OF_STOCK']]
                    );
                    $modelO->updateData([$data->receive_id], ['order_sub_status' => ORDER_SUB_STATUS['OUT_OF_STOCK']]);
                } else {
                    $key        = $data->receive_id . ',' . $data->detail_line_num . ',' . $data->sub_line_num;
                    $itemCode   = !empty($data->child_product_code) ? $data->child_product_code : $data->product_code;
                    $sendFaxFlg = '';
                    if (array_key_exists($data->suplier_id, $dataAdmin)) {
                        $sendFaxFlg = $dataAdmin[$data->suplier_id]['send_zaiko_fax_flg'] === 1 ? 1 : '';
                    }
                    $index     += 1;
                    $orderCode  = 'HE' . $yearMonth . sprintf("%06d", $index);
                    $arrInsert[$key]['suppliercd']          = $data->suplier_id;
                    $arrInsert[$key]['SerialNo']            = 0;
                    $arrInsert[$key]['item_code']           = $itemCode;
                    $arrInsert[$key]['m_order_type_id']     = 10;
                    $arrInsert[$key]['order_date']          = now();
                    $arrInsert[$key]['order_code']          = $orderCode;
                    $arrInsert[$key]['jan_code']            = (string)$data->product_jan;
                    $arrInsert[$key]['maker_name']          = (string)$data->maker_full_nm;
                    $arrInsert[$key]['maker_code']          = (string)$data->product_maker_code;
                    $arrInsert[$key]['note_code']           = (string)$data->product_name;
                    $arrInsert[$key]['name']                = (string)$data->product_name_long;
                    $arrInsert[$key]['size']                = (string)$data->product_size;
                    $arrInsert[$key]['other']               = '';
                    $arrInsert[$key]['memo']                = '';
                    $arrInsert[$key]['reminder_flg']        = 0;
                    $arrInsert[$key]['filmaker_flg']        = 1;
                    $arrInsert[$key]['cancel_flg']          = 0;
                    $arrInsert[$key]['send_fax_flg']        = $sendFaxFlg;
                    $arrInsert[$key]['send_fax_date']       = $sendFaxFlg === 1 ? now() : '';
                    $arrInsert[$key]['quantity']            = (int)$data->received_order_num;
                    $arrInsert[$key]['del_flg']             = 0;
                    $arrInsert[$key]['valid_flg']           = 1;
                    $arrUpdate[$key]                        = $orderCode;
                    $arrEstimation[$key]['estimate_code']   = $orderCode;
                    $arrEstimation[$key]['estimate_date']   = now();
                    $arrEstimation[$key]['other']           = '';
                    $arrEstimation[$key]['memo']            = '';
                    $arrEstimation[$key]['wait_days']       = 0;
                    $arrEstimation[$key]['is_send_mail']    = 0;
                    $arrEstimation[$key]['is_delete']       = 0;
                    $arrEstimation[$key]['receive_id']      = $data->receive_id;
                    $arrEstimation[$key]['detail_line_num'] = $data->detail_line_num;
                    $arrEstimation[$key]['sub_line_num']    = $data->sub_line_num;
                    $arrEstimation[$key]['in_date']         = now();
                    $arrEstimation[$key]['up_date']         = now();
                }
            }
            if (count($arrInsert) > 0) {
                $arrStepInsert     = array_chunk($arrInsert, $this->limit);
                $arrStepUpdate     = array_chunk($arrUpdate, $this->limit, true);
                $arrStepEstimation = array_chunk($arrEstimation, $this->limit);
                $success   = 0;
                $fail      = 0;
                $upSuccess = 0;
                $upFail    = 0;
                $inSuccess = 0;
                $inFail    = 0;
                foreach ($arrStepInsert as $key => $value) {
                    try {
                        $modelZaiko->insert($value);
                        $success += count($value);
                    } catch (\Exception $e) {
                        $this->error[] = Utilities::checkMessageException($e);
                        $error  = "------------------------------------------" . PHP_EOL;
                        $error .= basename(__CLASS__) . PHP_EOL;
                        $error .= Utilities::checkMessageException($e);
                        $error .= PHP_EOL . "------------------------------------------" . PHP_EOL;
                        $slack->notify(new SlackNotification($error));
                        Log::error(Utilities::checkMessageException($e));
                        print_r("$error");
                        $fail += count($value);
                        continue;
                    }
                    foreach ($arrStepUpdate[$key] as $k => $v) {
                        $arrKey = explode(",", $k);
                        $modelOPD->updateData(
                            $arrKey[0],
                            $arrKey[1],
                            $arrKey[2],
                            ['product_status' => PRODUCT_STATUS['ESTIMATING']]
                        );
                        $upSuccess++;
                    }
                    try {
                        $modelEstimation->insert($arrStepEstimation[$key]);
                        $inSuccess += count($arrStepEstimation[$key]);
                    } catch (\Exception $e) {
                        $this->error[] = Utilities::checkMessageException($e);
                        $error  = "------------------------------------------" . PHP_EOL;
                        $error .= basename(__CLASS__) . PHP_EOL;
                        $error .= Utilities::checkMessageException($e);
                        $error .= PHP_EOL . "------------------------------------------" . PHP_EOL;
                        $slack->notify(new SlackNotification($error));
                        Log::error(Utilities::checkMessageException($e));
                        print_r("$error");
                        $inFail += count($value);
                        continue;
                    }
                }
                $table = 'dt_order_product_detail';
                Log::info("=== Insert into table t_zaiko success: $success and fail: $fail record.");
                print_r("=== Insert into table t_zaiko success: $success and fail: $fail record." . PHP_EOL);
                Log::info("=== Update table $table success: $upSuccess and fail: $upFail record.");
                print_r("=== Update table $table success: $upSuccess and fail: $upFail record." . PHP_EOL);
                Log::info("=== Insert table dt_estimation success: $inSuccess and fail: $inFail record.");
                print_r("=== Insert table dt_estimation success: $inSuccess and fail: $inFail record." . PHP_EOL);
            }
        }
        Event::fire(new eCommand($this->signature, array('end' => true, 'error' => $this->error)));
        $totalTime = round(microtime(true) - $start, 2);
        Log::info("End batch process order zaiko with total time: $totalTime s.");
        print_r("End batch process order zaiko with total time: $totalTime s.");
    }
}
