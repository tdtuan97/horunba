<?php
/**
 * Batch process upload biz order.
 *
 * @package    App\Console\Commands
 * @subpackage ProcessBizCsv
 * @copyright  Copyright (c) 2018 RiverCrane! Corporation. All Rights Reserved.
 * @author     Truong Nghia<truong.van.nghia@rivercrane.vn>
 */
namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;
use App\Http\Controllers\Common;
use App\Models\Batches\TBizOrder;
use Storage;
use File;
use Config;
use DB;

class ProcessGetOrderBiz extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'process:get-order-biz';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Process upload csv biz order.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $arrayReplace = [':', '-'];
        $folder       = str_replace($arrayReplace, '_', $this->signature);
        Log::useDailyFiles(storage_path() . "/logs/$folder/history.log");
        Log::info('Start batch import csv biz.');
        print_r("Start batch import csv biz." . PHP_EOL);
        $start       = microtime(true);
        $modelB      = new TBizOrder();
        $arrFile = File::allFiles(storage_path() . '/csv/biz');
        $common    = new Common;
        foreach ($arrFile as $file) {
            $fileName   = $file->getFilename();
            $extension  = $file->getExtension();
            if (!Storage::disk('csv_biz')->exists($fileName) || $extension !== 'csv') {
                continue;
            }
            Log::info("=== File {$fileName} is processing. ===");
            print_r("=== File {$fileName} is processing. ===" . PHP_EOL);
            $contentCsv = Storage::disk('csv_biz')->get($fileName);
            if ($contentCsv === "") {
                Storage::move(
                    storage_path() . '/csv/biz' . "/{$fileName}",
                    storage_path() . '/csv_bak/biz' . "/{$fileName}"
                );
                Log::info("    File {$fileName} no content and moved to 'csv_bak/biz'.");
                print_r("    File {$fileName} no content and moved to 'csv_bak/biz'." . PHP_EOL);
                continue;
            }
            $contentCsv =  mb_convert_encoding($contentCsv, 'UTF-8', 'Shift-JIS');
            $readers    = str_getcsv($contentCsv, "\r\n");
            $num = 0;
            $arrCol = Config::get('common.t_biz_order');
            $arrProduct = [];
            $arrInsert  = [];
            foreach ($readers as $row) {
                $num++;
                $totalColumn = 75;
                $column   = str_getcsv($row);
                if (empty($row) || $num === 1) {
                    continue;
                }
                if (count($column) !== $totalColumn) {
                    Log::info("    File {$fileName} at Row $num not enought field.");
                    print_r("    File {$fileName} at Row $num not enought field." . PHP_EOL);
                    continue;
                }
                $column = array_map('trim', $column);
                $column[] = $fileName;
                $column[] = 0;
                $column[] = 0;
                $column[] = now();
                $arrCombine = array_combine($arrCol, $column);
                $arrKey = [
                    'order_id'  => $arrCombine['order_id'],
                    'item_code' => $arrCombine['item_code'],
                ];
                if ($modelB->countCheckExists($arrKey) !== 0) {
                    continue;
                } elseif (!isset($arrProduct[$arrCombine['order_id']])) {
                    $arrProduct[$arrCombine['order_id']][] = $arrCombine['item_code'];
                } elseif (!in_array($arrCombine['item_code'], $arrProduct[$arrCombine['order_id']])) {
                    $arrProduct[$arrCombine['order_id']][] = $arrCombine['item_code'];
                } else {
                    continue;
                }
                if (strtotime($arrCombine['order_date']) < strtotime('2018/06/30')) {
                    continue;
                }
                $arrInsert[] = $arrCombine;
            }
            try {
                $num = count($arrInsert);
                if ($num !== 0) {
                    $modelB->insert($arrInsert);
                }
                File::move(storage_path() . "/csv/biz/$fileName", storage_path() . "/csv_bak/biz/$fileName");
            } catch (\Exception $e) {
                DB::rollback();
            }

            Log::info("    File {$fileName} process success: {$num} records.");
            print_r("    File {$fileName} process success: {$num} records." . PHP_EOL);
        }
        $totalTime = round(microtime(true) - $start, 2);
        Log::info("End batch import csv biz with total time: {$totalTime} s.");
        print_r("End batch import csv biz with total time: {$totalTime} s.");
    }
}
