<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;
use App\Models\Batches\MstOrder;
use App\Models\Batches\DtOrderProductDetail;
use App\Models\Batches\MstOrderDetail;
use App\Models\Batches\MstPaymentMethod;
use App\Events\Command as eCommand;
use Config;
use Event;
use DB;
use App\Models\Batches\MstRakutenKey;

class ProcessUpdatePaymentStatusRakutenPay extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'process:update-payment-status-rak-pay';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';
    /**
     * The key rakuten
     *
     * @var string
     */
    protected $keyRakuten = '';
    /**
     * The error.
     *
     * @var array
     */

    public $error = [];
    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //Create log
        $arrayReplace = [':', '-'];
        $folder       = str_replace($arrayReplace, '_', $this->signature);
        Log::useDailyFiles(storage_path() . "/logs/$folder/history.log");
        //Process data
        Event::fire(new eCommand($this->signature, array('start' => true)));
        $start     = microtime(true);
        Log::info('Start batch process update payment status.');
        print_r("Start batch process update payment status." . PHP_EOL);
        $modelO   = new MstOrder();
        $modelOPD = new DtOrderProductDetail();
        $modelOD  = new MstOrderDetail();
        $modelMPM = new MstPaymentMethod();
        $modelRakutenKey  = new MstRakutenKey();
        $this->keyRakuten = $modelRakutenKey->getKeyRakuten();
        $datas  = $modelO->getDataPaymentComfirm3();
        if ($datas->count() === 0) {
            $message = 'No data';
            Log::info($message);
            print_r($message . PHP_EOL);
        } else {
            $arrReOrder = $datas->filter(function ($value, $key) {
                return preg_match('/^re/', $value->received_order_id);
            });
            if (count($arrReOrder) !== 0) {
                $arrOrder = $datas->filter(function ($value, $key) {
                    return !preg_match('/^re/', $value->received_order_id);
                });
                $listReceived = collect($arrOrder->all())->pluck('received_order_id')->toArray();
            } else {
                $listReceived = $datas->pluck('received_order_id')->toArray();
            }
            $arrSteps = array_chunk($listReceived, 100);
            $success = 0;
            foreach ($arrSteps as $listOrder) {
                $rakUrl    = "https://api.rms.rakuten.co.jp/es/2.0/order/getOrder/";
                $rakHeader = array(
                    "Accept: application/json",
                    "Content-type: application/json; charset=UTF-8",
                    "Authorization: {$this->keyRakuten}"
                );
                $request = array(
                    'orderNumberList' => $listOrder,
                );
                $requestJson = json_encode($request);
                $ch = curl_init($rakUrl);
                curl_setopt($ch, CURLOPT_POST, true);
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($ch, CURLOPT_HTTPHEADER, $rakHeader);
                curl_setopt($ch, CURLOPT_POSTFIELDS, $requestJson);
                $responseJson = curl_exec($ch);
                $responseJson = json_decode($responseJson);
                $message     = '';
                $messageCode = '';
                if (isset($responseJson->MessageModelList)) {
                    if (is_array($responseJson->MessageModelList)) {
                        foreach ($responseJson->MessageModelList as $error) {
                            if ($error->messageType === 'ERROR' && isset($error->orderNumber)) {
                                $modelO->updateDataByReceiveOrder(
                                    [$error->orderNumber],
                                    [
                                        'error_code_api'   => $error->messageCode,
                                        'message_api'      => $error->message,
                                        'up_ope_cd'        => 'OPE99999',
                                        'up_date'          => now(),
                                    ]
                                );
                            } elseif ($error->messageType === 'INFO') {
                                $message     = $error->message;
                                $messageCode = $error->messageCode;
                            }
                        }
                    }
                }
                if (isset($responseJson->OrderModelList) && count($responseJson->OrderModelList) !== 0) {
                    foreach ($responseJson->OrderModelList as $response) {
                        if ($response->orderProgress === 300) {
                            $arrUpdate = [
                                    'payment_status'   => 1,
                                    'order_sub_status' => 0,
                                    'is_mail_sent'     => 1,
                                    'message_api'      => $message,
                                    'error_code_api'   => $messageCode,
                                    'up_ope_cd'        => 'OPE99999',
                                    'up_date'          => now(),
                                ];
                            $paymentMethod = $modelO->getPaymentMethod($response->orderNumber, 9);
                            if ($response->SettlementModel->settlementMethod !== $paymentMethod->payment_name) {
                                $arrUpdate['used_coupon']    = $response->couponAllTotalPrice;
                                $arrUpdate['used_point']     = $this->checkIsset($response->PointModel->usedPoint, '');
                                $arrUpdate['total_price']    = $response->totalPrice + $response->deliveryPrice;
                                $arrUpdate['request_price']  = $response->requestPrice;
                                $arrUpdate['ship_charge']    = $response->postagePrice;
                                $arrUpdate['goods_price']    = $response->goodsPrice + $response->goodsTax;
                                $arrUpdate['payment_method'] = $modelMPM->getPaymentMethod(9, $response->SettlementModel->settlementMethod)->payment_code;
                            }
                            $modelO->updateDataByReceiveOrder(
                                [$response->orderNumber],
                                $arrUpdate
                            );
                            $success++;
                        } elseif ($response->orderProgress === 900 || $response->orderProgress === 800) {
                            $arrUpdate = [
                                'order_status'        => ORDER_STATUS['CANCEL'],
                                'order_sub_status'    => ORDER_SUB_STATUS['DONE'],
                                'ship_charge'         => 0,
                                'total_price'         => 0,
                                'pay_charge_discount' => 0,
                                'pay_charge'          => 0,
                                'pay_after_charge'    => 0,
                                'used_point'          => 0,
                                'used_coupon'         => 0,
                                'request_price'       => 0,
                                'goods_price'         => 0,
                                'goods_tax'           => 0,
                                'discount'            => 0,
                                'is_mall_cancel'      => 1,
                                'message_api'         => $message,
                                'error_code_api'      => $messageCode,
                            ];
                            $modelO->updateDataByReceiveOrder(
                                [$response->orderNumber],
                                $arrUpdate
                            );
                            $dataOrder = $modelO->where('received_order_id', $response->orderNumber)->first();
                            if (!empty($dataOrder)) {
                                DB::transaction(function () use ($modelOD, $modelOPD, $dataOrder) {
                                    $modelOPD->updateCancelStock($dataOrder->receive_id);
                                    $modelOPD->updateDataByReceive(
                                        [$dataOrder->receive_id],
                                        ['product_status' => PRODUCT_STATUS['CANCEL']]
                                    );
                                    $modelOD->updateData(
                                        ['receive_id' => $dataOrder->receive_id],
                                        [
                                            'quantity' => 0,
                                            'price'    => 0,
                                        ]
                                    );
                                }, 5);
                            }
                        }
                    }
                }
            }
            if (count($arrReOrder) !== 0) {
                foreach ($arrReOrder as $reOrder) {
                    $modelO->updateDataByReceiveOrder(
                        [$reOrder->received_order_id],
                        [
                            'payment_status'   => 1,
                            'order_sub_status' => 0,
                            'up_ope_cd'        => 'OPE99999',
                            'up_date'          => now(),
                        ]
                    );
                }
            }
            $message = "Call Api and update sub status success: $success records.";
            Log::info($message);
            print_r($message . PHP_EOL);
        }
        Event::fire(new eCommand($this->signature, array('end' => true, 'error' => $this->error)));
        $totalTime = round(microtime(true) - $start, 2);
        Log::info("End batch process payment confirm with total time: $totalTime s.");
        print_r("End batch process payment confirm with total time: $totalTime s.");
    }
    /**
    * Check isset column
    * @return object boolean
    */
    public function checkIsset($data, $value = null)
    {
        return isset($data) ? $data : $value;
    }
}
