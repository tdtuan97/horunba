<?php
/**
 * Batch process diy order honten
 *
 * @package    App\Console\Commands
 * @subpackage ProcessDiyOrderHonten
 * @copyright  Copyright (c) 2018 RiverCrane! Corporation. All Rights Reserved.
 * @author     Le Hung<le.hung.rcvn2012@gmail.com>
 */

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;
use App\Models\Batches\THontenOrder;
use App\Events\Command as eCommand;
use App\Http\Controllers\Common;
use Event;
use Config;
use File;
use Storage;

class ProcessGetOrderDiy extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'process:get-order-diy';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Read csv in horunba/storage/csv/diy path then import to t_order_honten';

    /**
     * The error.
     *
     * @var array
     */
    public $error = [];

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //Create log
        $arrayReplace = [':', '-'];
        $folder       = str_replace($arrayReplace, '_', $this->signature);
        Log::useDailyFiles(storage_path() . "/logs/$folder/history.log");
        //Process data
        Event::fire(new eCommand($this->signature, array('start' => true)));
        Log::info('Start process DIY Acquire Orders batch');
        print_r("Start process DIY Acquire Orders batch import to t_order_honten." . PHP_EOL);
        $start      = microtime(true);
        $this->processBatch();
        $totalTime = round(microtime(true) - $start, 2);
        Log::info("End DIY Acquire Orders batch process with total time: $totalTime s.");
        print_r("End DIY Acquire Orders batch process with total time: $totalTime s.");
        Event::fire(new eCommand($this->signature, array('end' => true, 'error' => $this->error)));
    }
    /**
     * Process csv then insert and write log
     *
     * @return
     */
    public function processBatch()
    {
        $common         = new Common;
        $files          = File::allFiles(storage_path('csv/diy'));
        if (count($files) === 0) {
            print_r("File is not found" . PHP_EOL);
            Log::info("CSV Files process for $this->signature was not found");
            return;
        }
        foreach ($files as $file) {
            $fileName       = last((array)$file);
            $filePath       = storage_path('csv/diy/' . $fileName);
            $filePathBak    = storage_path('csv_bak/diy/' . $fileName);
            $ext = pathinfo($fileName, PATHINFO_EXTENSION);
            if ($ext !== 'csv') {
                print_r("File $fileName is not csv format" . PHP_EOL);
                Log::info("File $fileName is not csv format");
                continue;
            } else {
                 print_r("File $fileName is processing". PHP_EOL);
            }
            if (file_exists($filePath)) {
                $fileContent    = File::get($filePath);
                $fileContentUtf =  mb_convert_encoding($fileContent, 'UTF-8', 'Shift-JIS');
                $readers        = str_getcsv($fileContentUtf, "\r\n");
                $totalColumns   = count(Config::get('common.csv_acquire_order_honten'));
                $arrCol         = Config::get('common.csv_acquire_order_honten');
                $arrCorrect     = array();
                $arrError       = array();
                $arrTemp        = array();
                unset($readers[0]);
                $chunksData = array_chunk($readers, '50');
                $i = 0;
                $model = new THontenOrder();
                foreach ($chunksData as $valueChunk) {
                    $data = $valueChunk;
                    foreach ($data as $key => $row) {
                        $column   = str_getcsv($row, ",");
                        if (count($column) !== $totalColumns) {
                            $arrError[] = trim($row);
                            print_r("Row $i was be errored.". PHP_EOL);
                            Log::info("Row $i was be errored.");
                            continue;
                        }
                        $column      = array_map('trim', $column);
                        $orderNum    = $column[0];
                        $productCode = $column[5];
                        $dataWhere   = ['item_number' => $productCode, 'order_id' => $orderNum];
                        $dataCheck   = $model->checkExists($dataWhere);
                        if (empty($dataCheck)) {
                            $column = array_combine($arrCol, $column);
                            if (strtotime($column['order_date']) < strtotime('2018/06/30')) {
                                continue;
                            }
                            $column['file']        = $fileName;
                            $column['process_flg'] = 0;
                            $column['is_delete']   = 0;
                            $column['created_at']  = date('Y-m-d H:m:i');
                            $i++;
                            $arrCorrect[] = $column;
                        }
                    }
                    if (!empty($arrCorrect)) {
                        $model->insert($arrCorrect);
                    }
                    $arrCorrect = null;
                    $data = null;
                }
                $chunksData = null;
                File::move($filePath, $filePathBak);
                print_r("--------------------------------------------------------------------" . PHP_EOL);
                print_r("File $fileName processed with total inserted: $i items." . PHP_EOL);
                print_r("--------------------------------------------------------------------" . PHP_EOL);
                Log::info("File $fileName processed with total inserted: $i items.");
            }
        }
    }
}
