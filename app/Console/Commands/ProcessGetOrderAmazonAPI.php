<?php
/**
 * Call API get order Amazon
 *
 * @package    App\Console\Commands
 * @subpackage ProcessGetOrderAmazonAPI
 * @copyright  Copyright (c) 2018 RiverCrane! Corporation. All Rights Reserved.
 * @author     Van Qui<van.qui@rivercrane.vn>
 */
namespace App\Console\Commands;

include app_path('lib/MarketplaceWebServiceOrders/autoload.php');

use Illuminate\Console\Command;
use App\Events\Command as eCommand;
use Illuminate\Support\Facades\Log;
use Event;
use App;
use Config;
use Storage;
use App\Notifications\SlackNotification;
use App\Notification;
use App\Models\Batches\TAmazonOrderApi;
use App\Models\Batches\TAmazonOrderDetail;

class ProcessGetOrderAmazonAPI extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'process:get-order-amazon-api {checkOrder=nocheck}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Call API process get order Amazon';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * The error.
     *
     * @var array
     */
    public $error = [];

    /**
     * The notification.
     *
     * @var object
     */
    protected $slack;

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //Create log
        $arrayReplace = [':', '-'];
        $folder       = str_replace($arrayReplace, '_', $this->signature);
        Log::useDailyFiles(storage_path() . "/logs/$folder/history.log");
        $this->slack = new Notification(CHANNEL['horunba']);
        //Process data
        Event::fire(new eCommand($this->signature, array('start' => true)));
        Log::info('Start batch get order Amazon API.');
        print_r("Start batch get order Amazon API." . PHP_EOL);
        $start       = microtime(true);

        $configAll = Config::get('amazonmws');
        $config = array (
            'ServiceURL'    => $configAll['ServiceURLOrder'],
            'ProxyHost'     => $configAll['ProxyHost'],
            'ProxyPort'     => $configAll['ProxyPort'],
            'ProxyUsername' => $configAll['ProxyUsername'],
            'ProxyPassword' => $configAll['ProxyPassword'],
            'MaxErrorRetry' => $configAll['MaxErrorRetry'],
        );

        $service = new \MarketplaceWebServiceOrders_Client(
            $configAll['AWS_ACCESS_KEY_ID'],
            $configAll['AWS_SECRET_ACCESS_KEY'],
            $configAll['APPLICATION_NAME'],
            $configAll['APPLICATION_VERSION'],
            $config
        );
        
        $checkOrder = $this->argument('checkOrder');
        if ($checkOrder === 're_get') {
            Log::info('Start process re get list orders.');
            print_r("Start process re get list orders." . PHP_EOL);
            $amazonOrderIds = $this->processReGetOrders($configAll, $service);
            Log::info('End process re get list orders.');
            print_r("End process re get list orders." . PHP_EOL);
        } else {
            Log::info('Start process get list orders.');
            print_r("Start process get list orders." . PHP_EOL);
            $amazonOrderIds = $this->processGetOrders($configAll, $service);
            Log::info('End process get list orders.');
            print_r("End process get list orders." . PHP_EOL);
        }

        if ($amazonOrderIds !== false) {
            Log::info('Start process get list order items.');
            print_r("Start process get list order items." . PHP_EOL);
            $this->processGetOrderItems($configAll, $service, $amazonOrderIds);
            Log::info('End process get list order items.');
            print_r("End process get list order items." . PHP_EOL);
        }

        Event::fire(new eCommand($this->signature, array('end' => true, 'error' => $this->error)));
        $totalTime = round(microtime(true) - $start, 2);
        Log::info("End batch get order Amazon API with total time: $totalTime s.");
        print_r("End batch get order Amazon API with total time: $totalTime s.");
    }

    /**
     * Process get order list
     *
     * @param  array   $configAll
     * @param  object  $service
     * @return boolean
     */
    private function processGetOrders($configAll, $service)
    {
        $checkOrder = $this->argument('checkOrder');
        if ($checkOrder === 'check') {
            $createdAfter  = gmdate('Y-m-d\TH:i:s\Z', strtotime("-3 days"));
            $createdBefore = gmdate('Y-m-d\TH:i:s\Z', strtotime("-30 minutes"));
        } else {
            $createdAfter  = gmdate('Y-m-d\TH:i:s\Z', strtotime("-60 minutes"));
            $createdBefore = gmdate('Y-m-d\TH:i:s\Z', strtotime("-30 minutes"));
        }
        $request = new \MarketplaceWebServiceOrders_Model_ListOrdersRequest();
        $request->setSellerId($configAll['MERCHANT_ID']);
        $request->setMarketplaceId($configAll['MWS_MARKETPLACE_JP']);
        $request->setCreatedAfter($createdAfter);
        $request->setCreatedBefore($createdBefore);
        $request->setOrderStatus(['PendingAvailability', 'Pending', 'Unshipped', 'PartiallyShipped']);

        $tAmazonOrderApi = new TAmazonOrderApi();
        $amazonOrderIds  = [];
        $xml = $this->executeAPI($service, $request, 'LIST_ORDERS');
        if ($xml === false) {
            return false;
        }
        $resData   = $this->mappingXmlToListOrders($xml, 'LIST_ORDERS');
        if (count($resData['orders']) === 0) {
            Log::info('Don\'t have orders.');
            print_r('Don\'t have orders.' . PHP_EOL);
            return false;
        }
        $currentOrders = array_column($resData['orders'], 'amazon_order_id');
        $existOrders   = $tAmazonOrderApi->getOrdersExist($currentOrders)->pluck('amazon_order_id')->toArray();
        foreach ($resData['orders'] as $key => $order) {
            if (in_array($order['amazon_order_id'], $existOrders)) {
                unset($resData['orders'][$key]);
            }
        }
        if (!$tAmazonOrderApi->insert($resData['orders'])) {
            Log::info('Insert list orders fail.');
            print_r('Insert list orders fail.' . PHP_EOL);
            return false;
        }
        foreach ($resData['orders'] as $order) {
            if (!in_array($order['order_status'], ['PendingAvailability', 'Pending'])) {
                $amazonOrderIds[] = $order['amazon_order_id'];
            }
        }
        $nextToken = $resData['nextToken'];
        while (!empty($nextToken)) {
            sleep(5);
            $request = new \MarketplaceWebServiceOrders_Model_ListOrdersByNextTokenRequest();
            $request->setSellerId($configAll['MERCHANT_ID']);
            $request->setNextToken($nextToken);
            $xml = $this->executeAPI($service, $request, 'LIST_ORDERS_BY_NEXT_TOKEN');
            if ($xml === false) {
                break;
            }
            $resData   = $this->mappingXmlToListOrders($xml, 'LIST_ORDERS_BY_NEXT_TOKEN');
            if (count($resData['orders']) === 0) {
                Log::info('Don\'t have orders next token.');
                print_r('Don\'t have orders next token.' . PHP_EOL);
                break;
            }
            $currentOrders = array_column($resData['orders'], 'amazon_order_id');
            $existOrders   = $tAmazonOrderApi->getOrdersExist($currentOrders)->pluck('amazon_order_id')->toArray();
            foreach ($resData['orders'] as $key => $order) {
                if (in_array($order['amazon_order_id'], $existOrders)) {
                    unset($resData['orders'][$key]);
                }
            }
            if (!$tAmazonOrderApi->insert($resData['orders'])) {
                Log::info('Insert list orders next token fail.');
                print_r('Insert list orders next token fail.' . PHP_EOL);
                break;
            }
            foreach ($resData['orders'] as $order) {
                if (!in_array($order['order_status'], ['PendingAvailability', 'Pending'])) {
                    $amazonOrderIds[] = $order['amazon_order_id'];
                }
            }
            $nextToken = $resData['nextToken'];
        }
        return $amazonOrderIds;
    }
    
    /**
     * Process get order list
     *
     * @param  array   $configAll
     * @param  object  $service
     * @return boolean
     */
    private function processReGetOrders($configAll, $service)
    {
        $request = new \MarketplaceWebServiceOrders_Model_GetOrderRequest();
        $request->setSellerId($configAll['MERCHANT_ID']);
        $modelAO = new TAmazonOrderApi();
        $result  = $modelAO->getOrdersByStatus(['PendingAvailability', 'Pending']);
        if ($result->count() === 0) {
            Log::info('No data.');
            print_r('No data.' . PHP_EOL);
            return [];
        }
        $result = $result->chunk(100);
        $amazonOrderIds = [];
        foreach ($result as $k => $order) {
            if ($k !== 0) {
                sleep(30);
            }
            $oldOrder = $order->pluck('order_status', 'amazon_order_id');
            $checkOrder = $order->pluck('amazon_order_id')->toArray();
            $request->setAmazonOrderId($checkOrder);
            $xml = $this->executeAPI($service, $request, 'GET_ORDER');
            if ($xml === false) {
                break;
            }
            $resData = $this->mappingXmlToListOrders($xml, 'GET_ORDER');
            if (count($resData['orders']) === 0) {
                continue;
            }
            foreach ($resData['orders'] as $newOrder) {
                if ($newOrder['order_status'] !== $oldOrder[$newOrder['amazon_order_id']]) {
                    if ($newOrder['order_status'] === 'Canceled') {
                        $newOrder['process_flg'] = 1;
                    }
                    $modelAO->updateData(
                        ['amazon_order_id' => $newOrder['amazon_order_id']],
                        $newOrder
                    );
                    if (!in_array($newOrder['order_status'], ['PendingAvailability', 'Pending'])) {
                        $amazonOrderIds[] = $newOrder['amazon_order_id'];
                    }
                }
            }
        }
        
        return $amazonOrderIds;
    }

    /**
     * Process get order item
     *
     * @param array $configAll
     * @param object $service
     * @param type $amazonOrderIds
     */
    private function processGetOrderItems($configAll, $service, $amazonOrderIds)
    {
        $tAmazonOrderDetail = new TAmazonOrderDetail();
        $request = new \MarketplaceWebServiceOrders_Model_ListOrderItemsRequest();
        $request->setSellerId($configAll['MERCHANT_ID']);
        foreach ($amazonOrderIds as $amazonOrderId) {
            $request->setAmazonOrderId($amazonOrderId);
            $xml = $this->executeAPI($service, $request, 'LIST_ORDER_ITEMS');
            if ($xml === false) {
                continue;
            }
            $resData = $this->mappingXmlToListOrderItems($xml, 'LIST_ORDER_ITEMS');
            if (count($resData['orderItems']) === 0) {
                Log::info("Order Id $amazonOrderId don't have item.");
                print_r("Order Id $amazonOrderId don't have item." . PHP_EOL);
                continue;
            }

            $orderItems = $resData['orderItems'];
            $nextToken  = $resData['nextToken'];

            while (!empty($nextToken)) {
                sleep(5);
                $request = new \MarketplaceWebServiceOrders_Model_ListOrderItemsByNextTokenRequest();
                $request->setSellerId(MERCHANT_ID);
                $request->setNextToken($nextToken);
                $xml = $this->executeAPI($service, $request, 'LIST_ORDER_ITEMS_BY_NEXT_TOKEN');
                if ($xml === false) {
                    break;
                }
                $resData = $this->mappingXmlToListOrderItems($xml, 'LIST_ORDER_ITEMS_BY_NEXT_TOKEN');
                if (count($resData['orderItems']) === 0) {
                    Log::info("Order Id $amazonOrderId don't have item next token.");
                    print_r("Order Id $amazonOrderId don't have item next token." . PHP_EOL);
                    break;
                }
                $orderItems = array_merge($orderItems, $resData['orderItems']);
                $nextToken  = $resData['nextToken'];
            }

            foreach ($orderItems as $key => &$item) {
                $item['detail_line'] = $key + 1;
            }
            if (!$tAmazonOrderDetail->insertIgnore($orderItems)) {
                Log::info('Insert list order items fail.');
                print_r('Insert list order items fail.' . PHP_EOL);
            }
            sleep(5);
        }
    }

    /**
     * Execute api amazon
     *
     * @param  object   $service
     * @param  object   $request
     * @param  string   $type
     * @return boolean
     */
    private function executeAPI($service, $request, $type = 'LIST_ORDERS')
    {
        try {
            switch ($type) {
                case 'LIST_ORDERS':
                    return $service->ListOrders($request)->toXML();
                case 'LIST_ORDERS_BY_NEXT_TOKEN':
                    return $service->ListOrdersByNextToken($request)->toXML();
                case 'LIST_ORDER_ITEMS':
                    return $service->ListOrderItems($request)->toXML();
                case 'LIST_ORDER_ITEMS_BY_NEXT_TOKEN':
                    return $service->ListOrderItemsByNextToken($request)->toXML();
                case 'GET_ORDER':
                    return $service->GetOrder($request)->toXML();
            }
            return false;
        } catch (\MarketplaceWebServiceOrders_Exception $ex) {
            $message = "Caught Exception: " . $ex->getMessage() . "\n";
            $message .= "Response Status Code: " . $ex->getStatusCode() . "\n";
            $message .= "Error Code: " . $ex->getErrorCode() . "\n";
            $message .= "Error Type: " . $ex->getErrorType() . "\n";
            $message .= "Request ID: " . $ex->getRequestId() . "\n";
            $message .= "XML: " . $ex->getXML() . "\n";
            $message .= "ResponseHeaderMetadata: " . $ex->getResponseHeaderMetadata();

            Log::info($message);
            print_r($message . PHP_EOL);
            return false;
        } catch (\Exception $ex) {
            $message = 'Error ' . $ex->getCode() . ': ' . $ex->getMessage();
            Log::info($message);
            print_r($message . PHP_EOL);
            return false;
        }
    }

    /**
     * Mapping xml to data
     *
     * @param  string $xml
     * @param  string $type
     * @return array
     */
    private function mappingXmlToListOrders($xml, $type = 'LIST_ORDERS')
    {
        $rootNode = 'ListOrdersResult';
        if ($type === 'LIST_ORDERS_BY_NEXT_TOKEN') {
            $rootNode = 'ListOrdersByNextTokenResult';
        } elseif ($type === 'GET_ORDER') {
            $rootNode = 'GetOrderResult';
        }

        $dom = simplexml_load_string($xml);
        $resOrders = [];
        if (isset($dom->{$rootNode})
            && isset($dom->{$rootNode}->Orders)
            && isset($dom->{$rootNode}->Orders->Order)) {
            foreach ($dom->{$rootNode}->Orders->Order as $order) {
                $amountPaymentGC  = 0;
                $amountPaymentCOD = 0;
                $amountPaymentCP  = 0;
                if (isset($order->PaymentExecutionDetail)
                    && isset($order->PaymentExecutionDetail->PaymentExecutionDetailItem)) {
                    foreach ($order->PaymentExecutionDetail->PaymentExecutionDetailItem as $item) {
                        $paymentMethod = (string) $item->PaymentMethod;
                        if ($paymentMethod === 'GC') {
                            $amountPaymentGC = (int) $item->Payment->Amount;
                        }
                        if ($paymentMethod === 'COD') {
                            $amountPaymentCOD = (int) $item->Payment->Amount;
                        }
                        if ($paymentMethod === 'PointsAccount') {
                            $amountPaymentCP = (int) $item->Payment->Amount;
                        }
                    }
                }

                $resOrders[] = [
                    'amazon_order_id'                 => (string) $order->AmazonOrderId,
                    'latest_ship_date'                => (string) $order->LatestShipDate,
                    'order_type'                      => (string) $order->OrderType,
                    'purchase_date'                   => (string) $order->PurchaseDate,
                    'payment_GC'                      => $amountPaymentGC,
                    'payment_COD'                     => $amountPaymentCOD,
                    'payment_CP'                      => $amountPaymentCP,
                    'buyer_email'                     => (string) $order->BuyerEmail,
                    'is_replacement_order'            => (string) $order->IsReplacementOrder,
                    'last_update_date'                => (string) $order->LastUpdateDate,
                    'number_of_items_shipped'         => (int) $order->NumberOfItemsShipped,
                    'ship_service_level'              => (string) $order->ShipServiceLevel,
                    'order_status'                    => (string) $order->OrderStatus,
                    'promise_response_due_date'       => (string) $order->PromiseResponseDueDate,
                    'sales_channel'                   => (string) $order->SalesChannel,
                    'shipped_by_amazon_TFM'           => (string) $order->ShippedByAmazonTFM,
                    'is_business_order'               => (string) $order->IsBusinessOrder,
                    'latest_delivery_date'            => (string) $order->LatestDeliveryDate,
                    'number_of_items_unshipped'       => (int) $order->NumberOfItemsUnshipped,
                    'payment_method_detail'           => (string) $order->PaymentMethodDetails->PaymentMethodDetail,
                    'buyer_name'                      => (string) $order->BuyerName,
                    'earliest_delivery_date'          => (string) $order->EarliestDeliveryDate,
                    'order_total_amount'              => (int) $order->OrderTotal->Amount,
                    'is_premium_order'                => (int) $order->IsPremiumOrder,
                    'earliest_ship_date'              => (string) $order->EarliestShipDate,
                    'market_place_id'                 => (string) $order->MarketplaceId,
                    'fulfillment_channel'             => (string) $order->FulfillmentChannel,
                    'payment_method'                  => (string) $order->PaymentMethod,
                    'ship_postal_code'                => (string) $order->ShippingAddress->PostalCode,
                    'ship_state_or_region'            => (string) $order->ShippingAddress->StateOrRegion,
                    'ship_phone'                      => (string) $order->ShippingAddress->Phone,
                    'ship_country_code'               => (string) $order->ShippingAddress->CountryCode,
                    'ship_name'                       => (string) $order->ShippingAddress->Name,
                    'ship_address_line1'              => (string) $order->ShippingAddress->AddressLine1,
                    'ship_address_line2'              => (string) $order->ShippingAddress->AddressLine2,
                    'ship_address_line3'              => (string) $order->ShippingAddress->AddressLine3,
                    'is_prime'                        => (string) $order->IsPrime,
                    'shipment_service_level_category' => (string) $order->ShipmentServiceLevelCategory,
                    'process_flg'                     => 0,
                    'is_deleted'                      => 0,
                    'created_at'                      => date('Y-m-d H:i:s')
                ];
            }
        }

        $nextToken = '';
        if (isset($dom->{$rootNode}) && isset($dom->{$rootNode}->NextToken)) {
            $nextToken = (string) $dom->{$rootNode}->NextToken;
        }

        return [
            'orders' => $resOrders,
            'nextToken' => $nextToken
        ];
    }

    /**
     * Mapping xml to list order item
     *
     * @param   string   $xml
     * @param   string   $type
     * @return  array
     */
    private function mappingXmlToListOrderItems($xml, $type = 'LIST_ORDER_ITEMS')
    {
        $rootNode = 'ListOrderItemsResult';
        if ($type === 'LIST_ORDER_ITEMS_BY_NEXT_TOKEN') {
            $rootNode = 'ListOrderItemsByNextTokenResult';
        }

        $dom = simplexml_load_string($xml);

        if (!isset($dom->{$rootNode}) || !isset($dom->{$rootNode}->AmazonOrderId)) {
            return [
                'orderItems' => [],
                'nextToken'  => ''
            ];
        }

        $amazonOrderId = (string) $dom->{$rootNode}->AmazonOrderId;

        $resOrderItems = [];
        if (isset($dom->{$rootNode}->OrderItems)
            && isset($dom->{$rootNode}->OrderItems->OrderItem)) {
            foreach ($dom->{$rootNode}->OrderItems->OrderItem as $orderItem) {
                $resOrderItems[] = [
                    'amazon_order_id'           => $amazonOrderId,
                    'detail_line'               => 0,
                    'COD_fee_discount_amount'   => (int) $orderItem->CODFeeDiscount->Amount,
                    'COD_fee_amount'            => (int) $orderItem->CODFee->Amount,
                    'quantity_ordered'          => (int) $orderItem->QuantityOrdered,
                    'title'                     => (string) $orderItem->Title,
                    'shipping_tax_amount'       => (int) $orderItem->ShippingTax->Amount,
                    'promotion_discount_amount' => (int) $orderItem->PromotionDiscount->Amount,
                    'condition_id'              => (string) $orderItem->ConditionId,
                    'is_gift'                   => (string) $orderItem->IsGift,
                    'asin'                      => (string) $orderItem->ASIN,
                    'seller_sku'                => (string) $orderItem->SellerSKU,
                    'order_item_id'             => (string) $orderItem->OrderItemId,
                    'number_of_items'           => (int) $orderItem->ProductInfo->NumberOfItems,
                    'quantity_shipped'          => (int) $orderItem->QuantityShipped,
                    'shipping_price_amount'     => (int) $orderItem->ShippingPrice->Amount,
                    'condition_sub_type_id'     => (int) $orderItem->ConditionSubtypeId,
                    'item_price_amount'         => (int) $orderItem->ItemPrice->Amount,
                    'item_tax_amount'           => (int) $orderItem->ItemTax->Amount,
                    'shipping_discount_amount'  => (int) $orderItem->ShippingDiscount->Amount,
                    'created_at'                => date('Y-m-d H:i:s')
                ];
            }
        }

        $nextToken = '';
        if (isset($dom->{$rootNode}) && isset($dom->{$rootNode}->NextToken)) {
            $nextToken = (string) $dom->{$rootNode}->NextToken;
        }

        return [
            'orderItems' => $resOrderItems,
            'nextToken'  => $nextToken
        ];
    }
}
