<?php
/**
 * Batch process get order rakuten from api
 *
 * @package    App\Console\Commands
 * @subpackage ProcessGetOrderRakuten
 * @copyright  Copyright (c) 2018 RiverCrane! Corporation. All Rights Reserved.
 * @author     Van Qui<van.qui@rivercrane.vn>
 */

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;
use App\Notifications\SlackNotification;
use App\Custom\Utilities;
use App\Notification;
use App\Events\Command as eCommand;
use Event;
use Config;
use Helper;
use DB;
use App;
use App\Models\Batches\TRakutenOrder;

class ProcessGetOrderRakuten extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'process:get-order-rakuten {checkOrder=nocheck}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Process get order Rakuten';

    /**
     * The notification.
     *
     * @var object
     */
    protected $slack;

    /**
     * The error.
     *
     * @var array
     */
    public $error = [];

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //Create log
        //Create log
        $arrayReplace = [':', '-'];
        $folder       = str_replace($arrayReplace, '_', $this->signature);
        $folder       = explode(' ', $folder);
        $folder       = $folder[0];
        $checkOrder   = $this->argument('checkOrder');
        $signature    = explode(' ', $this->signature);
        $signature    = $signature[0];
        if ($checkOrder === 'check') {
            $signature = $signature . ' check';
        }
        Log::useDailyFiles(storage_path() . "/logs/$folder/history.log");
        //Process data
        Event::fire(new eCommand($signature, array('start' => true)));
        Log::info('Start batch process get order Rakuten.');
        print_r("Start batch process get order Rakuten." . PHP_EOL);
        $start       = microtime(true);
        $this->slack = new Notification(CHANNEL['horunba']);

        $this->processGetAPI();

        Event::fire(new eCommand($signature, array('end' => true, 'error' => $this->error)));
        $totalTime = round(microtime(true) - $start, 2);
        Log::info("End batch process get order Rakuten with total time: $totalTime s.");
        print_r("End batch process get order Rakuten with total time: $totalTime s.");
    }

    /**
     * Process get API
     *
     * @return void
     */
    private function processGetAPI()
    {
        DB::beginTransaction();
        try {
            $rakAuth   = Config::get('common.rak_auth_key');
            $rakUrl    = "https://api.rms.rakuten.co.jp/es/1.0/order/ws";
            $rakHeader = array(
                "Content-Type: text/xml;charset=UTF-8",
            );
            $checkOrder = $this->argument('checkOrder');
            if ($checkOrder === 'check') {
                $startDate = date("Y-m-d\TH:i:s", strtotime("-3 days"));
                $endDate   = date("Y-m-d\TH:i:s", strtotime("-60 minutes"));
            } else {
                $time = date('H:i');
                if ($time >= '12:00' && $time <= '15:00') {
                    $startDate = date("Y-m-d\TH:i:s", strtotime("-70 minutes"));
                    $endDate   = date("Y-m-d\TH:i:s", strtotime("-30 minutes"));
                } else {
                    $startDate = date("Y-m-d\TH:i:s", strtotime("-110 minutes"));
                    $endDate   = date("Y-m-d\TH:i:s", strtotime("-60 minutes"));
                }
            }
            $requestXml = view('mapping.xml.rakuten', [
                'authKey'   => $rakAuth,
                'shopUrl'   => 'tuzukiya',
                'userName'  => 'tuzukiya',
                'startDate' => $startDate,
                'endDate'   => $endDate,
                'orders'    => []
            ])->render();
            $ch = curl_init($rakUrl);
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_HTTPHEADER, $rakHeader);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $requestXml);
            $responseXml = curl_exec($ch);
            curl_close($ch);
            list($dataInsert, $dataOrder) = $this->processRakutenXml($responseXml);
            $tRakutenOrder = new TRakutenOrder();
            if (count($dataInsert) > 0) {
                $dataInsert = array_chunk($dataInsert, 50);
                foreach ($dataInsert as $data) {
                    $tRakutenOrder->insert($data);
                }
                Log::info('Insert data success');
                print_r("Insert data success" . PHP_EOL);
                if (!App::environment(['local', 'test'])) {
                    $this->callApiUpdateStatus($dataOrder);
                }
            } else {
                Log::info('No data');
                print_r("No data" . PHP_EOL);
            }
            DB::commit();
        } catch (\Exception $e) {
            DB::rollback();
            $message = "Can't process get order rakuten api";
            print_r($message . PHP_EOL);
            Log::error($message);
            report($e);
            $this->error[] = $e->getMessage();
        }
    }


    /**
     * Process Rakuten xml
     *
     * @param  string $responseXml
     * @return array
     */
    private function processRakutenXml($responseXml)
    {
        $columns     = [
            "errorcode","message","uniterror_errorcode","uniterror_message","uniterror_orderkey",
            "asuraku_flg","canenclosure","card_status","carrier_code","child_order_model",
            "coupon_all_total_price","couponalltotalunit","couponcapital","couponcode","couponname",
            "couponprice","couponsummary","coupontotalprice","couponunit","coupon_usage",
            "coupondiscounttype","couponexpirydate","couponfeeflag","couponitemid","couponordernumber",
            "couponotherprice","couponothertotalunit","couponshopprice","couponshoptotalunit","deal",
            "delivery_class","delivery_name","delivery_price","drugcategory","email_carrier_code",
            "enclosurecouponprice","enclosuredeliveryprice","enclosuregoodsprice","enclosuregoodstax",
            "enclosureid","enclosurepointprice","enclosurepostageprice","enclosurerbanktransfercommission",
            "enclosurerequestprice","enclosurestatus","enclosuretotalprice","firstamount","bidid",
            "comment","goods_price","goods_tax","historymodel","isblackuser","is_gift","isgiftcheck",
            "israkutenmember","istaxrecalc","mailplugsentence","membership","memo","modify","detailid",
            "reservedatetime","reservenumber","reservetype","operator","option","order_date","order_number",
            "order_type","birthday","birthmonth","birthyear","city","email_address","family_name",
            "family_name_kana","first_name","first_name_kana","nick_name","phone_number1","phone_number2",
            "phone_number3","prefecture","sex","sub_address","zip_code1","zip_code2","packagemodel_basketid",
            "delete_flg","delivery_company_id","area_code","cvsbikou","cvsclosetime","cvscode","cvsopentime",
            "depo","store_address","storecode","store_genrecode","store_name","store_prefecture","store_zip",
            "packagemodel_deliveryprice","packagemodel_goodsprice","packagemodel_goodstax",
            "itemmodel_basketid","delete_item_flg","currentsumamount","gbuybidinventorymodel_bidunits",
            "gbuybidinventorymodel_gchoiceid","gbuygchoicemodel_gchoiceid","gbuygchoicemodel_gchoiceinvtry",
            "gbuygchoicemodel_gchoicemaxunits","gbuygchoicemodel_gchoicename","gbuygchoicemodel_itemid",
            "gbuygchoicemodel_orderby","gbuygchoicemodel_soldflag","gbuygchoicemodel_sumamount",
            "is_shift_status","shift_date","unit_text","isincludedcashondeliverypostage","isincludedpostage",
            "isincludedtax","itemmodel_itemid","item_name","item_number","delvdateinfo","inventory_type",
            "page_url","point_rate","point_type","price","restore_inventory_flag","saitemmodel",
            "selected_choice","units","noshi","packagemodel_postageprice","sendermodel_birthday",
            "sendermodel_birthmonth","sendermodel_birthyear","sendermodel_city","sendermodel_email_address",
            "sendermodel_familyname","sendermodel_familynamekana","sendermodel_firstname",
            "sendermodel_firstnamekana","sendermodel_nickname","sendermodel_phonenumber1",
            "sendermodel_phonenumber2","sendermodel_phonenumber3","sendermodel_prefecture",
            "sendermodel_sex","sendermodel_subaddress","sendermodel_zipcode1","sendermodel_zipcode2",
            "shippingnumber","paymentdate","paymentstatusmodel_accesspoint","paymentstatusmodel_errorcode",
            "paymentstatusmodel_operator","paymentstatusmodel_ordernumber","paymentstatusmodel_paymenttype",
            "paymentstatusmodel_price","paymentstatusmodel_shopid","paymentstatusmodel_status",
            "paymentstatusmodel_updatetime","pointmodel_pointusage","pointmodel_status",
            "point_model_used_point","postage_price","rbankmodel_ordernumber","rbankmodel_rbcommissionpayer",
            "rbankmodel_rbankstatus","rbankmodel_shopid","rbank_model_transfer_commission",
            "request_price","rmid","saordermodel_bidid","saordermodel_comment","saordermodel_regdate",
            "seqid","brandname","cardno","expym","installmentdesc","owner_name","pay_type","settlementname",
            "shipping_date","shipping_term","status","total_price","wishdeliverydate",
            "wrappingmodel1_deletewrappingflg","wrappingmodel1_isincludedtax","wrappingmodel1_name",
            "wrappingmodel1_price","wrappingmodel1_title","wrappingmodel2_deletewrappingflg",
            "wrappingmodel2_isincludedtax","wrappingmodel2_name","wrappingmodel2_price",
            "wrappingmodel2_title","acceptingchangeflg","acceptingchange_requestid","mail_serial","form_cancel_flg",
            "process_flg","is_delete","created_at"
        ];

        $responseXml = str_ireplace(['S:', 'ns2:'], '', $responseXml);
        $responData  = simplexml_load_string($responseXml);
        $returnData  = $responData->Body->getOrderResponse->return;
        $dataInsert  = array();
        $dataOrder   = array();
        $errorCode   = Helper::val($returnData->errorCode);
        $tRakutenOrder = new TRakutenOrder();
        if ($errorCode === "E10-001") {
            return [];
        } else {
            foreach ($returnData->orderModel as $orderModel) {
                $count = $tRakutenOrder->checkOrder($orderModel->orderNumber);
                if ($count > 0) {
                    continue;
                }
                if (Helper::val($orderModel->status) === '新規受付') {
                    $dataOrder[Helper::val($orderModel->orderNumber)] = Helper::val($orderModel->orderNumber);
                }
                if (Helper::val($orderModel->isGift) === 'false') {
                    foreach ($orderModel->packageModel->itemModel as $itemModel) {
                        $tmp = [
                            'errorcode'                        => Helper::val($returnData->errorCode),
                            'message'                          => Helper::val($returnData->message),
                            'asuraku_flg'                      => Helper::val($orderModel->asurakuFlg),
                            'canenclosure'                     => Helper::val($orderModel->canEnclosure),
                            'card_status'                      => Helper::val($orderModel->cardStatus),
                            'carrier_code'                     => Helper::val($orderModel->carrierCode),
                            'child_order_model'                => Helper::val($orderModel->childOrderModel),
                            'coupon_all_total_price'           => Helper::val($orderModel->couponAllTotalPrice),
                            'couponalltotalunit'               => Helper::val($orderModel->couponAllTotalUnit),
                            'couponcapital'                    => Helper::val($orderModel->couponModel->couponCapital),
                            'couponcode'                       => Helper::val($orderModel->couponModel->couponCode),
                            'couponname'                       => Helper::val($orderModel->couponModel->couponName),
                            'couponprice'                      => Helper::val($orderModel->couponModel->couponPrice),
                            'couponsummary'                    => Helper::val($orderModel->couponModel->couponSummary),
                            'coupontotalprice'                 => Helper::val($orderModel->couponModel->couponTotalPrice),
                            'couponunit'                       => Helper::val($orderModel->couponModel->couponUnit),
                            'coupon_usage'                     => Helper::val($orderModel->couponModel->couponUsage),
                            'coupondiscounttype'               => Helper::val($orderModel->couponModel->discountType),
                            'couponexpirydate'                 => Helper::val($orderModel->couponModel->expiryDate),
                            'couponfeeflag'                    => Helper::val($orderModel->couponModel->feeFlag),
                            'couponitemid'                     => Helper::val($orderModel->couponModel->itemId),
                            'couponordernumber'                => Helper::val($orderModel->couponModel->orderNumber),
                            'couponotherprice'                 => Helper::val($orderModel->couponOtherPrice),
                            'couponothertotalunit'             => Helper::val($orderModel->couponOtherTotalUnit),
                            'couponshopprice'                  => Helper::val($orderModel->couponShopPrice),
                            'couponshoptotalunit'              => Helper::val($orderModel->couponShopTotalUnit),
                            'deal'                             => Helper::val($orderModel->deal),
                            'delivery_class'                   => Helper::val($orderModel->deliveryModel->deliveryClass),
                            'delivery_name'                    => Helper::val($orderModel->deliveryModel->deliveryName),
                            'delivery_price'                   => Helper::val($orderModel->deliveryPrice),
                            'drugcategory'                     => Helper::val($orderModel->drugCategory),
                            'email_carrier_code'               => Helper::val($orderModel->emailCarrierCode),
                            'enclosurecouponprice'             => Helper::val($orderModel->enclosureCouponPrice),
                            'enclosuredeliveryprice'           => Helper::val($orderModel->enclosureDeliveryPrice),
                            'enclosuregoodsprice'              => Helper::val($orderModel->enclosureGoodsPrice),
                            'enclosuregoodstax'                => Helper::val($orderModel->enclosureGoodsTax),
                            'enclosureid'                      => Helper::val($orderModel->enclosureId),
                            'enclosurepointprice'              => Helper::val($orderModel->enclosurePointPrice),
                            'enclosurepostageprice'            => Helper::val($orderModel->enclosurePostagePrice),
                            'enclosurerbanktransfercommission'
                            => Helper::val($orderModel->enclosureRBankTransferCommission),
                            'enclosurerequestprice'            => Helper::val($orderModel->enclosureRequestPrice),
                            'enclosurestatus'                  => Helper::val($orderModel->enclosureStatus),
                            'enclosuretotalprice'              => Helper::val($orderModel->enclosureTotalPrice),
                            'firstamount'                      => Helper::val($orderModel->firstAmount),
                            'bidid'                            => Helper::val($orderModel->gbuyOrderModel->bidId),
                            'comment'                          => Helper::val($orderModel->gbuyOrderModel->comment),
                            'goods_price'                      => Helper::val($orderModel->goodsPrice),
                            'goods_tax'                        => Helper::val($orderModel->goodsTax),
                            'historymodel'                     => Helper::val($orderModel->historyModel),
                            'isblackuser'                      => Helper::val($orderModel->isBlackUser),
                            'is_gift'                          => Helper::val($orderModel->isGift),
                            'isgiftcheck'                      => Helper::val($orderModel->isGiftCheck),
                            'israkutenmember'                  => Helper::val($orderModel->isRakutenMember),
                            'istaxrecalc'                      => Helper::val($orderModel->isTaxRecalc),
                            'mailplugsentence'                 => Helper::val($orderModel->mailPlugSentence),
                            'membership'                       => Helper::val($orderModel->membership),
                            'memo'                             => Helper::val($orderModel->memo),
                            'modify'                           => Helper::val($orderModel->modify),
                            'detailid'                         => Helper::val($orderModel->normalOrderModel->detailId),
                            'reservedatetime'
                            => Helper::val($orderModel->normalOrderModel->reserveDatetime),
                            'reservenumber'                    => Helper::val($orderModel->normalOrderModel->reserveNumber),
                            'reservetype'                      => Helper::val($orderModel->normalOrderModel->reserveType),
                            'operator'                         => Helper::val($orderModel->operator),
                            'option'                           => Helper::val($orderModel->option),
                            'order_date'                       => Helper::val($orderModel->orderDate),
                            'order_number'                     => Helper::val($orderModel->orderNumber),
                            'order_type'                       => Helper::val($orderModel->orderType),
                            'birthday'                         => Helper::val($orderModel->ordererModel->birthDay),
                            'birthmonth'                       => Helper::val($orderModel->ordererModel->birthMonth),
                            'birthyear'                        => Helper::val($orderModel->ordererModel->birthYear),
                            'city'                             => Helper::val($orderModel->ordererModel->city),
                            'email_address'                    => Helper::val($orderModel->ordererModel->emailAddress),
                            'family_name'                      => Helper::val($orderModel->ordererModel->familyName),
                            'family_name_kana'                 => Helper::val($orderModel->ordererModel->familyNameKana),
                            'first_name'                       => Helper::val($orderModel->ordererModel->firstName),
                            'first_name_kana'                  => Helper::val($orderModel->ordererModel->firstNameKana),
                            'nick_name'                        => Helper::val($orderModel->ordererModel->nickname),
                            'phone_number1'                    => Helper::val($orderModel->ordererModel->phoneNumber1),
                            'phone_number2'                    => Helper::val($orderModel->ordererModel->phoneNumber2),
                            'phone_number3'                    => Helper::val($orderModel->ordererModel->phoneNumber3),
                            'prefecture'                       => Helper::val($orderModel->ordererModel->prefecture),
                            'sex'                              => Helper::val($orderModel->ordererModel->sex),
                            'sub_address'                      => Helper::val($orderModel->ordererModel->subAddress),
                            'zip_code1'                        => Helper::val($orderModel->ordererModel->zipCode1),
                            'zip_code2'                        => Helper::val($orderModel->ordererModel->zipCode2),
                            'packagemodel_basketid'            => Helper::val($orderModel->packageModel->basketId),
                            'delete_flg'                       => Helper::val($orderModel->packageModel->deleteFlg),
                            'delivery_company_id'              => Helper::val($orderModel->packageModel->deliveryCompanyId),
                            'area_code'
                            => Helper::val($orderModel->packageModel->deliveryCvsModel->areaCode),
                            'cvsbikou'
                             => Helper::val($orderModel->packageModel->deliveryCvsModel->cvsBikou),
                            'cvsclosetime'
                            => Helper::val($orderModel->packageModel->deliveryCvsModel->cvsCloseTime),
                            'cvscode'
                              => Helper::val($orderModel->packageModel->deliveryCvsModel->cvsCode),
                            'cvsopentime'
                            => Helper::val($orderModel->packageModel->deliveryCvsModel->cvsOpenTime),
                            'depo'
                                 => Helper::val($orderModel->packageModel->deliveryCvsModel->depo),
                            'store_address'
                            => Helper::val($orderModel->packageModel->deliveryCvsModel->storeAddress),
                            'storecode'
                            => Helper::val($orderModel->packageModel->deliveryCvsModel->storeCode),
                            'store_genrecode'
                            => Helper::val($orderModel->packageModel->deliveryCvsModel->storeGenreCode),
                            'store_name'
                            => Helper::val($orderModel->packageModel->deliveryCvsModel->storeName),
                            'store_prefecture'
                            => Helper::val($orderModel->packageModel->deliveryCvsModel->storePrefecture),
                            'store_zip'
                            => Helper::val($orderModel->packageModel->deliveryCvsModel->storeZip),
                            'packagemodel_deliveryprice'       => Helper::val($orderModel->packageModel->deliveryPrice),
                            'packagemodel_goodsprice'          => Helper::val($orderModel->packageModel->goodsPrice),
                            'packagemodel_goodstax'            => Helper::val($orderModel->packageModel->goodsTax),
                            'itemmodel_basketid'               => Helper::val($itemModel->basketId),
                            'delete_item_flg'                  => Helper::val($itemModel->deleteItemFlg),
                            'currentsumamount'                 => Helper::val($itemModel->gbuyItemModel->currentSumAmount),
                            'gbuybidinventorymodel_bidunits'
                            => (string) isset($itemModel->gbuyItemModel->gbuyBidInventoryModel->bidUnits)?
                                $itemModel->gbuyItemModel->gbuyBidInventoryModel->bidUnits:null,
                            'gbuybidinventorymodel_gchoiceid'
                            => (string) isset($itemModel->gbuyItemModel->gbuyBidInventoryModel->gchoiceId)?
                                $itemModel->gbuyItemModel->gbuyBidInventoryModel->gchoiceId:null,
                            'gbuygchoicemodel_gchoiceid'
                            => (string) isset($itemModel->gbuyItemModel->gbuyGchoiceModel->gchoiceId)?
                                $itemModel->gbuyItemModel->gbuyGchoiceModel->gchoiceId:null,
                            'gbuygchoicemodel_gchoiceinvtry'
                            => (string) isset($itemModel->gbuyItemModel->gbuyGchoiceModel->gchoiceInvtry)?
                                $itemModel->gbuyItemModel->gbuyGchoiceModel->gchoiceInvtry:null,
                            'gbuygchoicemodel_gchoicemaxunits'
                            => (string) isset($itemModel->gbuyItemModel->gbuyGchoiceModel->gchoiceMaxUnits)?
                                $itemModel->gbuyItemModel->gbuyGchoiceModel->gchoiceMaxUnits:null,
                            'gbuygchoicemodel_gchoicename'
                            => (string) isset($itemModel->gbuyItemModel->gbuyGchoiceModel->gchoiceName)?
                                $itemModel->gbuyItemModel->gbuyGchoiceModel->gchoiceName:null,
                            'gbuygchoicemodel_itemid'
                            => (string) isset($itemModel->gbuyItemModel->gbuyGchoiceModel->itemId)?
                                $itemModel->gbuyItemModel->gbuyGchoiceModel->itemId:null,
                            'gbuygchoicemodel_orderby'
                            => (string) isset($itemModel->gbuyItemModel->gbuyGchoiceModel->orderby)?
                                $itemModel->gbuyItemModel->gbuyGchoiceModel->orderby:null,
                            'gbuygchoicemodel_soldflag'
                            => (string) isset($itemModel->gbuyItemModel->gbuyGchoiceModel->soldFlag)?
                                $itemModel->gbuyItemModel->gbuyGchoiceModel->soldFlag:null,
                            'gbuygchoicemodel_sumamount'
                            => (string) isset($itemModel->gbuyItemModel->gbuyGchoiceModel->sumAmount)?
                                $itemModel->gbuyItemModel->gbuyGchoiceModel->sumAmount:null,
                            'is_shift_status'                  => Helper::val($itemModel->gbuyItemModel->isShiftStatus),
                            'shift_date'                       => Helper::val($itemModel->gbuyItemModel->shiftDate),
                            'unit_text'                        => Helper::val($itemModel->gbuyItemModel->unitText),
                            'isincludedcashondeliverypostage'  => Helper::val($itemModel->isIncludedCashOnDeliveryPostage),
                            'isincludedpostage'                => Helper::val($itemModel->isIncludedPostage),
                            'isincludedtax'                    => Helper::val($itemModel->isIncludedTax),
                            'itemmodel_itemid'                 => Helper::val($itemModel->itemId),
                            'item_name'                        => Helper::val($itemModel->itemName),
                            'item_number'                      => Helper::val($itemModel->itemNumber),
                            'delvdateinfo'                     => Helper::val($itemModel->normalItemModel->delvdateInfo),
                            'inventory_type'                   => Helper::val($itemModel->normalItemModel->inventoryType),
                            'page_url'                         => Helper::val($itemModel->pageURL),
                            'point_rate'                       => Helper::val($itemModel->pointRate),
                            'point_type'                       => Helper::val($itemModel->pointType),
                            'price'                            => Helper::val($itemModel->price),
                            'restore_inventory_flag'           => Helper::val($itemModel->restoreInventoryFlag),
                            'saitemmodel'                      => Helper::val($itemModel->saItemModel),
                            'selected_choice'                  => Helper::val($itemModel->selectedChoice),
                            'units'                            => Helper::val($itemModel->units),
                            'noshi'                            => Helper::val($orderModel->packageModel->noshi),
                            'packagemodel_postageprice'        => Helper::val($orderModel->packageModel->postagePrice),
                            'sendermodel_birthday'
                            => Helper::val($orderModel->packageModel->senderModel->birthDay),
                            'sendermodel_birthmonth'
                            => Helper::val($orderModel->packageModel->senderModel->birthMonth),
                            'sendermodel_birthyear'
                            => Helper::val($orderModel->packageModel->senderModel->birthYear),
                            'sendermodel_city'                 => Helper::val($orderModel->packageModel->senderModel->city),
                            'sendermodel_email_address'
                            => Helper::val($orderModel->packageModel->senderModel->emailAddress),
                            'sendermodel_familyname'
                            => Helper::val($orderModel->packageModel->senderModel->familyName),
                            'sendermodel_familynamekana'
                            => Helper::val($orderModel->packageModel->senderModel->familyNameKana),
                            'sendermodel_firstname'
                            => Helper::val($orderModel->packageModel->senderModel->firstName),
                            'sendermodel_firstnamekana'
                            => Helper::val($orderModel->packageModel->senderModel->firstNameKana),
                            'sendermodel_nickname'
                            => Helper::val($orderModel->packageModel->senderModel->nickname),
                            'sendermodel_phonenumber1'
                            => Helper::val($orderModel->packageModel->senderModel->phoneNumber1),
                            'sendermodel_phonenumber2'
                            => Helper::val($orderModel->packageModel->senderModel->phoneNumber2),
                            'sendermodel_phonenumber3'
                            => Helper::val($orderModel->packageModel->senderModel->phoneNumber3),
                            'sendermodel_prefecture'
                            => Helper::val($orderModel->packageModel->senderModel->prefecture),
                            'sendermodel_sex'                  => Helper::val($orderModel->packageModel->senderModel->sex),
                            'sendermodel_subaddress'
                            => Helper::val($orderModel->packageModel->senderModel->subAddress),
                            'sendermodel_zipcode1'
                            => Helper::val($orderModel->packageModel->senderModel->zipCode1),
                            'sendermodel_zipcode2'
                            => Helper::val($orderModel->packageModel->senderModel->zipCode2),
                            'shippingnumber'                   => Helper::val($orderModel->packageModel->shippingNumber),
                            'paymentdate'                      => Helper::val($orderModel->paymentDate),
                            'paymentstatusmodel_accesspoint'   => Helper::val($orderModel->paymentStatusModel->accessPoint),
                            'paymentstatusmodel_errorcode'     => Helper::val($orderModel->paymentStatusModel->errorCode),
                            'paymentstatusmodel_operator'      => Helper::val($orderModel->paymentStatusModel->operator),
                            'paymentstatusmodel_ordernumber'   => Helper::val($orderModel->paymentStatusModel->orderNumber),
                            'paymentstatusmodel_paymenttype'   => Helper::val($orderModel->paymentStatusModel->paymentType),
                            'paymentstatusmodel_price'         => Helper::val($orderModel->paymentStatusModel->price),
                            'paymentstatusmodel_shopid'        => Helper::val($orderModel->paymentStatusModel->shopId),
                            'paymentstatusmodel_status'        => Helper::val($orderModel->paymentStatusModel->status),
                            'paymentstatusmodel_updatetime'    => Helper::val($orderModel->paymentStatusModel->updateTime),
                            'pointmodel_pointusage'            => Helper::val($orderModel->pointModel->pointUsage),
                            'pointmodel_status'                => Helper::val($orderModel->pointModel->status),
                            'point_model_used_point'           => Helper::val($orderModel->pointModel->usedPoint),
                            'postage_price'                    => Helper::val($orderModel->postagePrice),
                            'rbankmodel_ordernumber'           => Helper::val($orderModel->RBankModel->orderNumber),
                            'rbankmodel_rbcommissionpayer'     => Helper::val($orderModel->RBankModel->rbCommissionPayer),
                            'rbankmodel_rbankstatus'           => Helper::val($orderModel->RBankModel->rbankStatus),
                            'rbankmodel_shopid'                => Helper::val($orderModel->RBankModel->shopId),
                            'rbank_model_transfer_commission'  => Helper::val($orderModel->RBankModel->transferCommission),
                            'request_price'                    => Helper::val($orderModel->requestPrice),
                            'rmid'                             => Helper::val($orderModel->rmId),
                            'saordermodel_bidid'               => Helper::val($orderModel->saOrderModel->bidId),
                            'saordermodel_comment'             => Helper::val($orderModel->saOrderModel->comment),
                            'saordermodel_regdate'             => Helper::val($orderModel->saOrderModel->regDate),
                            'seqid'                            => Helper::val($orderModel->seqId),
                            'brandname'
                            => Helper::val($orderModel->settlementModel->cardModel->brandName),
                            'cardno'
                            => Helper::val($orderModel->settlementModel->cardModel->cardNo),
                            'expym'
                            => Helper::val($orderModel->settlementModel->cardModel->expYM),
                            'installmentdesc'
                            => Helper::val($orderModel->settlementModel->cardModel->installmentDesc),
                            'owner_name'
                            => Helper::val($orderModel->settlementModel->cardModel->ownerName),
                            'pay_type'
                            => Helper::val($orderModel->settlementModel->cardModel->payType),
                            'settlementname'                   => Helper::val($orderModel->settlementModel->settlementName),
                            'shipping_date'                    => Helper::val($orderModel->shippingDate),
                            'shipping_term'                    => Helper::val($orderModel->shippingTerm),
                            'status'                           => Helper::val($orderModel->status),
                            'total_price'                      => Helper::val($orderModel->totalPrice),
                            'wishdeliverydate'                 => Helper::val($orderModel->wishDeliveryDate),
                            'wrappingmodel1_deletewrappingflg'
                            => Helper::val($orderModel->wrappingModel1->deleteWrappingFlg),
                            'wrappingmodel1_isincludedtax'     => Helper::val($orderModel->wrappingModel1->isIncludedTax),
                            'wrappingmodel1_name'              => Helper::val($orderModel->wrappingModel1->name),
                            'wrappingmodel1_price'             => Helper::val($orderModel->wrappingModel1->price),
                            'wrappingmodel1_title'             => Helper::val($orderModel->wrappingModel1->title),
                            'wrappingmodel2_deletewrappingflg'
                            => Helper::val($orderModel->wrappingModel2->deleteWrappingFlg),
                            'wrappingmodel2_isincludedtax'     => Helper::val($orderModel->wrappingModel2->isIncludedTax),
                            'wrappingmodel2_name'              => Helper::val($orderModel->wrappingModel2->name),
                            'wrappingmodel2_price'             => Helper::val($orderModel->wrappingModel2->price),
                            'wrappingmodel2_title'             => Helper::val($orderModel->wrappingModel2->title),
                            'acceptingchangeflg'               => 0,
                            'mail_serial'                      => md5((string) $orderModel->orderNumber),
                            'form_cancel_flg'                  => 0,
                            'process_flg'                      => 0,
                            'is_delete'                        => 0,
                            'created_at'                       => date('Y-m-d H:i:s')
                        ];
                        $tmp = Helper::combineColumn($columns, $tmp);
                        $dataInsert[$tmp['order_number'] . $tmp['item_number']] = $tmp;
                    }
                } else {
                    foreach ($orderModel->packageModel as $packageModel) {
                        foreach ($packageModel->itemModel as $itemModel) {
                            $tmp = [
                                'errorcode'                        => Helper::val($returnData->errorCode),
                                'message'                          => Helper::val($returnData->message),
                                'asuraku_flg'                      => Helper::val($orderModel->asurakuFlg),
                                'canenclosure'                     => Helper::val($orderModel->canEnclosure),
                                'card_status'                      => Helper::val($orderModel->cardStatus),
                                'carrier_code'                     => Helper::val($orderModel->carrierCode),
                                'child_order_model'                => Helper::val($orderModel->childOrderModel),
                                'coupon_all_total_price'           => Helper::val($orderModel->couponAllTotalPrice),
                                'couponalltotalunit'               => Helper::val($orderModel->couponAllTotalUnit),
                                'couponcapital'                    => Helper::val($orderModel->couponModel->couponCapital),
                                'couponcode'                       => Helper::val($orderModel->couponModel->couponCode),
                                'couponname'                       => Helper::val($orderModel->couponModel->couponName),
                                'couponprice'                      => Helper::val($orderModel->couponModel->couponPrice),
                                'couponsummary'                    => Helper::val($orderModel->couponModel->couponSummary),
                                'coupontotalprice'                 => Helper::val($orderModel->couponModel->couponTotalPrice),
                                'couponunit'                       => Helper::val($orderModel->couponModel->couponUnit),
                                'coupon_usage'                     => Helper::val($orderModel->couponModel->couponUsage),
                                'coupondiscounttype'               => Helper::val($orderModel->couponModel->discountType),
                                'couponexpirydate'                 => Helper::val($orderModel->couponModel->expiryDate),
                                'couponfeeflag'                    => Helper::val($orderModel->couponModel->feeFlag),
                                'couponitemid'                     => Helper::val($orderModel->couponModel->itemId),
                                'couponordernumber'                => Helper::val($orderModel->couponModel->orderNumber),
                                'couponotherprice'                 => Helper::val($orderModel->couponOtherPrice),
                                'couponothertotalunit'             => Helper::val($orderModel->couponOtherTotalUnit),
                                'couponshopprice'                  => Helper::val($orderModel->couponShopPrice),
                                'couponshoptotalunit'              => Helper::val($orderModel->couponShopTotalUnit),
                                'deal'                             => Helper::val($orderModel->deal),
                                'delivery_class'                   => Helper::val($orderModel->deliveryModel->deliveryClass),
                                'delivery_name'                    => Helper::val($orderModel->deliveryModel->deliveryName),
                                'delivery_price'                   => Helper::val($orderModel->deliveryPrice),
                                'drugcategory'                     => Helper::val($orderModel->drugCategory),
                                'email_carrier_code'               => Helper::val($orderModel->emailCarrierCode),
                                'enclosurecouponprice'             => Helper::val($orderModel->enclosureCouponPrice),
                                'enclosuredeliveryprice'           => Helper::val($orderModel->enclosureDeliveryPrice),
                                'enclosuregoodsprice'              => Helper::val($orderModel->enclosureGoodsPrice),
                                'enclosuregoodstax'                => Helper::val($orderModel->enclosureGoodsTax),
                                'enclosureid'                      => Helper::val($orderModel->enclosureId),
                                'enclosurepointprice'              => Helper::val($orderModel->enclosurePointPrice),
                                'enclosurepostageprice'            => Helper::val($orderModel->enclosurePostagePrice),
                                'enclosurerbanktransfercommission'
                                => Helper::val($orderModel->enclosureRBankTransferCommission),
                                'enclosurerequestprice'            => Helper::val($orderModel->enclosureRequestPrice),
                                'enclosurestatus'                  => Helper::val($orderModel->enclosureStatus),
                                'enclosuretotalprice'              => Helper::val($orderModel->enclosureTotalPrice),
                                'firstamount'                      => Helper::val($orderModel->firstAmount),
                                'bidid'                            => Helper::val($orderModel->gbuyOrderModel->bidId),
                                'comment'                          => Helper::val($orderModel->gbuyOrderModel->comment),
                                'goods_price'                      => Helper::val($orderModel->goodsPrice),
                                'goods_tax'                        => Helper::val($orderModel->goodsTax),
                                'historymodel'                     => Helper::val($orderModel->historyModel),
                                'isblackuser'                      => Helper::val($orderModel->isBlackUser),
                                'is_gift'                          => Helper::val($orderModel->isGift),
                                'isgiftcheck'                      => Helper::val($orderModel->isGiftCheck),
                                'israkutenmember'                  => Helper::val($orderModel->isRakutenMember),
                                'istaxrecalc'                      => Helper::val($orderModel->isTaxRecalc),
                                'mailplugsentence'                 => Helper::val($orderModel->mailPlugSentence),
                                'membership'                       => Helper::val($orderModel->membership),
                                'memo'                             => Helper::val($orderModel->memo),
                                'modify'                           => Helper::val($orderModel->modify),
                                'detailid'                         => Helper::val($orderModel->normalOrderModel->detailId),
                                'reservedatetime'
                                => Helper::val($orderModel->normalOrderModel->reserveDatetime),
                                'reservenumber'                    => Helper::val($orderModel->normalOrderModel->reserveNumber),
                                'reservetype'                      => Helper::val($orderModel->normalOrderModel->reserveType),
                                'operator'                         => Helper::val($orderModel->operator),
                                'option'                           => Helper::val($orderModel->option),
                                'order_date'                       => Helper::val($orderModel->orderDate),
                                'order_number'                     => Helper::val($orderModel->orderNumber),
                                'order_type'                       => Helper::val($orderModel->orderType),
                                'birthday'                         => Helper::val($orderModel->ordererModel->birthDay),
                                'birthmonth'                       => Helper::val($orderModel->ordererModel->birthMonth),
                                'birthyear'                        => Helper::val($orderModel->ordererModel->birthYear),
                                'city'                             => Helper::val($orderModel->ordererModel->city),
                                'email_address'                    => Helper::val($orderModel->ordererModel->emailAddress),
                                'family_name'                      => Helper::val($orderModel->ordererModel->familyName),
                                'family_name_kana'                 => Helper::val($orderModel->ordererModel->familyNameKana),
                                'first_name'                       => Helper::val($orderModel->ordererModel->firstName),
                                'first_name_kana'                  => Helper::val($orderModel->ordererModel->firstNameKana),
                                'nick_name'                        => Helper::val($orderModel->ordererModel->nickname),
                                'phone_number1'                    => Helper::val($orderModel->ordererModel->phoneNumber1),
                                'phone_number2'                    => Helper::val($orderModel->ordererModel->phoneNumber2),
                                'phone_number3'                    => Helper::val($orderModel->ordererModel->phoneNumber3),
                                'prefecture'                       => Helper::val($orderModel->ordererModel->prefecture),
                                'sex'                              => Helper::val($orderModel->ordererModel->sex),
                                'sub_address'                      => Helper::val($orderModel->ordererModel->subAddress),
                                'zip_code1'                        => Helper::val($orderModel->ordererModel->zipCode1),
                                'zip_code2'                        => Helper::val($orderModel->ordererModel->zipCode2),
                                'packagemodel_basketid'            => Helper::val($packageModel->basketId),
                                'delete_flg'                       => Helper::val($packageModel->deleteFlg),
                                'delivery_company_id'              => Helper::val($packageModel->deliveryCompanyId),
                                'area_code'
                                => Helper::val($packageModel->deliveryCvsModel->areaCode),
                                'cvsbikou'
                                 => Helper::val($packageModel->deliveryCvsModel->cvsBikou),
                                'cvsclosetime'
                                => Helper::val($packageModel->deliveryCvsModel->cvsCloseTime),
                                'cvscode'
                                  => Helper::val($packageModel->deliveryCvsModel->cvsCode),
                                'cvsopentime'
                                => Helper::val($packageModel->deliveryCvsModel->cvsOpenTime),
                                'depo'
                                     => Helper::val($packageModel->deliveryCvsModel->depo),
                                'store_address'
                                => Helper::val($packageModel->deliveryCvsModel->storeAddress),
                                'storecode'
                                => Helper::val($packageModel->deliveryCvsModel->storeCode),
                                'store_genrecode'
                                => Helper::val($packageModel->deliveryCvsModel->storeGenreCode),
                                'store_name'
                                => Helper::val($packageModel->deliveryCvsModel->storeName),
                                'store_prefecture'
                                => Helper::val($packageModel->deliveryCvsModel->storePrefecture),
                                'store_zip'
                                => Helper::val($packageModel->deliveryCvsModel->storeZip),
                                'packagemodel_deliveryprice'       => Helper::val($packageModel->deliveryPrice),
                                'packagemodel_goodsprice'          => Helper::val($packageModel->goodsPrice),
                                'packagemodel_goodstax'            => Helper::val($packageModel->goodsTax),
                                'itemmodel_basketid'               => Helper::val($itemModel->basketId),
                                'delete_item_flg'                  => Helper::val($itemModel->deleteItemFlg),
                                'currentsumamount'                 => Helper::val($itemModel->gbuyItemModel->currentSumAmount),
                                'gbuybidinventorymodel_bidunits'
                                => (string) isset($itemModel->gbuyItemModel->gbuyBidInventoryModel->bidUnits)?
                                    $itemModel->gbuyItemModel->gbuyBidInventoryModel->bidUnits:null,
                                'gbuybidinventorymodel_gchoiceid'
                                => (string) isset($itemModel->gbuyItemModel->gbuyBidInventoryModel->gchoiceId)?
                                    $itemModel->gbuyItemModel->gbuyBidInventoryModel->gchoiceId:null,
                                'gbuygchoicemodel_gchoiceid'
                                => (string) isset($itemModel->gbuyItemModel->gbuyGchoiceModel->gchoiceId)?
                                    $itemModel->gbuyItemModel->gbuyGchoiceModel->gchoiceId:null,
                                'gbuygchoicemodel_gchoiceinvtry'
                                => (string) isset($itemModel->gbuyItemModel->gbuyGchoiceModel->gchoiceInvtry)?
                                    $itemModel->gbuyItemModel->gbuyGchoiceModel->gchoiceInvtry:null,
                                'gbuygchoicemodel_gchoicemaxunits'
                                => (string) isset($itemModel->gbuyItemModel->gbuyGchoiceModel->gchoiceMaxUnits)?
                                    $itemModel->gbuyItemModel->gbuyGchoiceModel->gchoiceMaxUnits:null,
                                'gbuygchoicemodel_gchoicename'
                                => (string) isset($itemModel->gbuyItemModel->gbuyGchoiceModel->gchoiceName)?
                                    $itemModel->gbuyItemModel->gbuyGchoiceModel->gchoiceName:null,
                                'gbuygchoicemodel_itemid'
                                => (string) isset($itemModel->gbuyItemModel->gbuyGchoiceModel->itemId)?
                                    $itemModel->gbuyItemModel->gbuyGchoiceModel->itemId:null,
                                'gbuygchoicemodel_orderby'
                                => (string) isset($itemModel->gbuyItemModel->gbuyGchoiceModel->orderby)?
                                    $itemModel->gbuyItemModel->gbuyGchoiceModel->orderby:null,
                                'gbuygchoicemodel_soldflag'
                                => (string) isset($itemModel->gbuyItemModel->gbuyGchoiceModel->soldFlag)?
                                    $itemModel->gbuyItemModel->gbuyGchoiceModel->soldFlag:null,
                                'gbuygchoicemodel_sumamount'
                                => (string) isset($itemModel->gbuyItemModel->gbuyGchoiceModel->sumAmount)?
                                    $itemModel->gbuyItemModel->gbuyGchoiceModel->sumAmount:null,
                                'is_shift_status'                  => Helper::val($itemModel->gbuyItemModel->isShiftStatus),
                                'shift_date'                       => Helper::val($itemModel->gbuyItemModel->shiftDate),
                                'unit_text'                        => Helper::val($itemModel->gbuyItemModel->unitText),
                                'isincludedcashondeliverypostage'  => Helper::val($itemModel->isIncludedCashOnDeliveryPostage),
                                'isincludedpostage'                => Helper::val($itemModel->isIncludedPostage),
                                'isincludedtax'                    => Helper::val($itemModel->isIncludedTax),
                                'itemmodel_itemid'                 => Helper::val($itemModel->itemId),
                                'item_name'                        => Helper::val($itemModel->itemName),
                                'item_number'                      => Helper::val($itemModel->itemNumber),
                                'delvdateinfo'                     => Helper::val($itemModel->normalItemModel->delvdateInfo),
                                'inventory_type'                   => Helper::val($itemModel->normalItemModel->inventoryType),
                                'page_url'                         => Helper::val($itemModel->pageURL),
                                'point_rate'                       => Helper::val($itemModel->pointRate),
                                'point_type'                       => Helper::val($itemModel->pointType),
                                'price'                            => Helper::val($itemModel->price),
                                'restore_inventory_flag'           => Helper::val($itemModel->restoreInventoryFlag),
                                'saitemmodel'                      => Helper::val($itemModel->saItemModel),
                                'selected_choice'                  => Helper::val($itemModel->selectedChoice),
                                'units'                            => Helper::val($itemModel->units),
                                'noshi'                            => Helper::val($packageModel->noshi),
                                'packagemodel_postageprice'        => Helper::val($packageModel->postagePrice),
                                'sendermodel_birthday'
                                => Helper::val($packageModel->senderModel->birthDay),
                                'sendermodel_birthmonth'
                                => Helper::val($packageModel->senderModel->birthMonth),
                                'sendermodel_birthyear'
                                => Helper::val($packageModel->senderModel->birthYear),
                                'sendermodel_city'                 => Helper::val($packageModel->senderModel->city),
                                'sendermodel_email_address'
                                => Helper::val($packageModel->senderModel->emailAddress),
                                'sendermodel_familyname'
                                => Helper::val($packageModel->senderModel->familyName),
                                'sendermodel_familynamekana'
                                => Helper::val($packageModel->senderModel->familyNameKana),
                                'sendermodel_firstname'
                                => Helper::val($packageModel->senderModel->firstName),
                                'sendermodel_firstnamekana'
                                => Helper::val($packageModel->senderModel->firstNameKana),
                                'sendermodel_nickname'
                                => Helper::val($packageModel->senderModel->nickname),
                                'sendermodel_phonenumber1'
                                => Helper::val($packageModel->senderModel->phoneNumber1),
                                'sendermodel_phonenumber2'
                                => Helper::val($packageModel->senderModel->phoneNumber2),
                                'sendermodel_phonenumber3'
                                => Helper::val($packageModel->senderModel->phoneNumber3),
                                'sendermodel_prefecture'
                                => Helper::val($packageModel->senderModel->prefecture),
                                'sendermodel_sex'                  => Helper::val($packageModel->senderModel->sex),
                                'sendermodel_subaddress'
                                => Helper::val($packageModel->senderModel->subAddress),
                                'sendermodel_zipcode1'
                                => Helper::val($packageModel->senderModel->zipCode1),
                                'sendermodel_zipcode2'
                                => Helper::val($packageModel->senderModel->zipCode2),
                                'shippingnumber'                   => Helper::val($orderModel->packageModel->shippingNumber),
                                'paymentdate'                      => Helper::val($orderModel->paymentDate),
                                'paymentstatusmodel_accesspoint'   => Helper::val($orderModel->paymentStatusModel->accessPoint),
                                'paymentstatusmodel_errorcode'     => Helper::val($orderModel->paymentStatusModel->errorCode),
                                'paymentstatusmodel_operator'      => Helper::val($orderModel->paymentStatusModel->operator),
                                'paymentstatusmodel_ordernumber'   => Helper::val($orderModel->paymentStatusModel->orderNumber),
                                'paymentstatusmodel_paymenttype'   => Helper::val($orderModel->paymentStatusModel->paymentType),
                                'paymentstatusmodel_price'         => Helper::val($orderModel->paymentStatusModel->price),
                                'paymentstatusmodel_shopid'        => Helper::val($orderModel->paymentStatusModel->shopId),
                                'paymentstatusmodel_status'        => Helper::val($orderModel->paymentStatusModel->status),
                                'paymentstatusmodel_updatetime'    => Helper::val($orderModel->paymentStatusModel->updateTime),
                                'pointmodel_pointusage'            => Helper::val($orderModel->pointModel->pointUsage),
                                'pointmodel_status'                => Helper::val($orderModel->pointModel->status),
                                'point_model_used_point'           => Helper::val($orderModel->pointModel->usedPoint),
                                'postage_price'                    => Helper::val($orderModel->postagePrice),
                                'rbankmodel_ordernumber'           => Helper::val($orderModel->RBankModel->orderNumber),
                                'rbankmodel_rbcommissionpayer'     => Helper::val($orderModel->RBankModel->rbCommissionPayer),
                                'rbankmodel_rbankstatus'           => Helper::val($orderModel->RBankModel->rbankStatus),
                                'rbankmodel_shopid'                => Helper::val($orderModel->RBankModel->shopId),
                                'rbank_model_transfer_commission'  => Helper::val($orderModel->RBankModel->transferCommission),
                                'request_price'                    => Helper::val($orderModel->requestPrice),
                                'rmid'                             => Helper::val($orderModel->rmId),
                                'saordermodel_bidid'               => Helper::val($orderModel->saOrderModel->bidId),
                                'saordermodel_comment'             => Helper::val($orderModel->saOrderModel->comment),
                                'saordermodel_regdate'             => Helper::val($orderModel->saOrderModel->regDate),
                                'seqid'                            => Helper::val($orderModel->seqId),
                                'brandname'
                                => Helper::val($orderModel->settlementModel->cardModel->brandName),
                                'cardno'
                                => Helper::val($orderModel->settlementModel->cardModel->cardNo),
                                'expym'
                                => Helper::val($orderModel->settlementModel->cardModel->expYM),
                                'installmentdesc'
                                => Helper::val($orderModel->settlementModel->cardModel->installmentDesc),
                                'owner_name'
                                => Helper::val($orderModel->settlementModel->cardModel->ownerName),
                                'pay_type'
                                => Helper::val($orderModel->settlementModel->cardModel->payType),
                                'settlementname'                   => Helper::val($orderModel->settlementModel->settlementName),
                                'shipping_date'                    => Helper::val($orderModel->shippingDate),
                                'shipping_term'                    => Helper::val($orderModel->shippingTerm),
                                'status'                           => Helper::val($orderModel->status),
                                'total_price'                      => Helper::val($orderModel->totalPrice),
                                'wishdeliverydate'                 => Helper::val($orderModel->wishDeliveryDate),
                                'wrappingmodel1_deletewrappingflg'
                                => Helper::val($orderModel->wrappingModel1->deleteWrappingFlg),
                                'wrappingmodel1_isincludedtax'     => Helper::val($orderModel->wrappingModel1->isIncludedTax),
                                'wrappingmodel1_name'              => Helper::val($orderModel->wrappingModel1->name),
                                'wrappingmodel1_price'             => Helper::val($orderModel->wrappingModel1->price),
                                'wrappingmodel1_title'             => Helper::val($orderModel->wrappingModel1->title),
                                'wrappingmodel2_deletewrappingflg'
                                => Helper::val($orderModel->wrappingModel2->deleteWrappingFlg),
                                'wrappingmodel2_isincludedtax'     => Helper::val($orderModel->wrappingModel2->isIncludedTax),
                                'wrappingmodel2_name'              => Helper::val($orderModel->wrappingModel2->name),
                                'wrappingmodel2_price'             => Helper::val($orderModel->wrappingModel2->price),
                                'wrappingmodel2_title'             => Helper::val($orderModel->wrappingModel2->title),
                                'acceptingchangeflg'               => 0,
                                'mail_serial'                      => md5((string) $orderModel->orderNumber),
                                'form_cancel_flg'                  => 0,
                                'process_flg'                      => 0,
                                'is_delete'                        => 0,
                                'created_at'                       => date('Y-m-d H:i:s')
                            ];
                            $tmp = Helper::combineColumn($columns, $tmp);
                            $dataInsert[] = $tmp;
                        }
                    }
                }
            }
        }
        return [$dataInsert, $dataOrder];
    }

    /**
     * Get request id
     * @return mixed
     */
    public function getRequestId()
    {
        try {
            $rakAuth   = Config::get('common.rak_auth_key');
            $rakUrl    = "https://api.rms.rakuten.co.jp/es/1.0/order/ws";
            $rakHeader = array(
                "Content-Type: text/xml;charset=UTF-8",
            );
            $requestXml = view('api.xml.rakuten_get_request', [
                'authKey'   => $rakAuth,
                'shopUrl'   => 'tuzukiya',
                'userName'  => 'tuzukiya',
            ])->render();
            $ch = curl_init($rakUrl);
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_HTTPHEADER, $rakHeader);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $requestXml);
            $responseXml = curl_exec($ch);
            curl_close($ch);
            $responseXml = str_ireplace(['S:', 'ns2:'], '', $responseXml);
            $responData  = simplexml_load_string($responseXml);
            if (!empty($responData) &&
                Helper::val($responData->Body->getRequestIdResponse->return->errorCode) === 'N00-000') {
                return Helper::val($responData->Body->getRequestIdResponse->return->requestId);
            } else {
                return false;
            }
        } catch (\Exception $e) {
            print_r($e->getMessage());
            $message = "Can't get request ID";
            print_r($message . PHP_EOL);
            Log::error($message);
            report($e);
            return false;
        }
    }
    /**
     * Process call API update status
     *
     * @param  array $dataOrder
     * @return void
     */
    public function callApiUpdateStatus($dataOrder)
    {
        try {
            $rakAuth   = Config::get('common.rak_auth_key');
            $rakUrl    = "https://api.rms.rakuten.co.jp/es/1.0/order/ws";
            $rakHeader = array(
                "Content-Type: text/xml;charset=UTF-8",
            );
            $requestId = $this->getRequestId();
            $requestXml = view('api.xml.rakuten_update_status', [
                'authKey'   => $rakAuth,
                'shopUrl'   => 'tuzukiya',
                'userName'  => 'tuzukiya',
                'requestId' => $requestId,
                'status'    => '受付中',
                'orders'    => $dataOrder
            ])->render();
            $ch = curl_init($rakUrl);
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_HTTPHEADER, $rakHeader);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $requestXml);
            $responseXml = curl_exec($ch);
            curl_close($ch);
            $clean_xml = str_ireplace(['S:', 'ns2:'], '', $responseXml);
            $data = simplexml_load_string($clean_xml);
            $data = json_decode(json_encode($data), true);
            $tRakutenOrder = new TRakutenOrder();
            if (isset($data['Body']['changeStatusResponse']['return']['errorCode'])) {
                $return = $data['Body']['changeStatusResponse']['return'];
                if ($return['errorCode'] === 'N00-000') {
                    $tRakutenOrder->whereIn('order_number', $dataOrder)
                                  ->update(
                                      [
                                          'errorcode_updatestatus' => $return['errorCode'],
                                          'message_updatestatus'   => $return['message'],
                                          'request_id'             => $return['requestId'],
                                      ]
                                  );
                } elseif (isset($return['unitError'])) {
                    if (isset($return['unitError']['errorCode'])) {
                        $tRakutenOrder->where('order_number', $return['unitError']['orderKey'])
                                      ->update(
                                          [
                                              'errorcode_updatestatus' => $return['unitError']['errorCode'],
                                              'message_updatestatus'   => $return['unitError']['message'],
                                              'request_id'             => $return['requestId'],
                                          ]
                                      );
                        unset($dataOrder[$return['unitError']['orderKey']]);
                    } else {
                        foreach ($return['unitError'] as $err) {
                            $tRakutenOrder->where('order_number', $err['orderKey'])
                                          ->update(
                                              [
                                                  'errorcode_updatestatus' => $err['errorCode'],
                                                  'message_updatestatus'   => $err['message'],
                                                  'request_id'             => $return['requestId'],
                                              ]
                                          );
                            unset($dataOrder[$err['orderKey']]);
                        }
                    }
                    if (count($dataOrder) !== 0) {
                        $tRakutenOrder->whereIn('order_number', $dataOrder)
                                      ->update(
                                          [
                                              'errorcode_updatestatus' => 'N00-000',
                                              'message_updatestatus'   => '正常終了',
                                              'request_id'             => $return['requestId'],
                                          ]
                                      );
                    }
                }
            }
            $message = "Call API update status is success";
            print_r($message . PHP_EOL);
            Log::error($message);
        } catch (\Exception $e) {
            DB::rollback();
            $message = "Api update status have error.";
            print_r($message . PHP_EOL);
            Log::error($message);
            report($e);
            $this->error[] = $e->getMessage();
        }
    }
}
