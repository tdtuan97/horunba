<?php
/**
 * Call API process finish order
 *
 * @package    App\Console\Commands
 * @subpackage ProcessFinishedOrder
 * @copyright  Copyright (c) 2017 RiverCrane! Corporation. All Rights Reserved.
 * @author     Truong Nghia<truong.van.nghia@rivercrane.vn>
 */
namespace App\Console\Commands;

include app_path('lib/MarketplaceWebService/autoload.php');

use Illuminate\Console\Command;
use App\Events\Command as eCommand;
use Illuminate\Support\Facades\Log;
use App\Models\Batches\MstOrder;
use Event;
use App;
use Config;
use Storage;
use App\Helpers\Helper;
use App\Models\Batches\YahooToken;
use App\Models\Batches\TRakutenPayOrderDetail;
use App\Notifications\SlackNotification;
use App\Notification;
use MarketplaceWebService_Client;
use MarketplaceWebService_Model_SubmitFeedRequest;
use MarketplaceWebService_Model_GetFeedSubmissionListRequest;
use MarketplaceWebService_Model_GetFeedSubmissionResultRequest;
use MarketplaceWebService_Exception;
use App\Models\Batches\MstRakutenKey;

class ProcessFinishedOrder extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'process:finish-order';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Call API process finish order';
    /**
     * The key rakuten
     *
     * @var string
     */
    protected $keyRakuten = '';
    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * The error.
     *
     * @var array
     */
    public $error = [];

    /**
     * The notification.
     *
     * @var object
     */
    protected $slack;

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //Create log
        $arrayReplace = [':', '-'];
        $folder       = str_replace($arrayReplace, '_', $this->signature);
        Log::useDailyFiles(storage_path() . "/logs/$folder/history.log");
        $this->slack = new Notification(CHANNEL['horunba']);
        //Process data
        Event::fire(new eCommand($this->signature, array('start' => true)));
        Log::info('Start batch finish order.');
        print_r("Start batch finish order." . PHP_EOL);
        $start       = microtime(true);
        $modelRakutenKey  = new MstRakutenKey();
        $this->keyRakuten = $modelRakutenKey->getKeyRakuten();
        $modelO = new MstOrder();
        $dataUpdates = $modelO->getDataFinishUpdate();
        $countUp = count($dataUpdates);
        if ($countUp === 0) {
            Log::info('No data update.');
            print_r("No data update." . PHP_EOL);
        } else {
            $arrUpdate = [
                'order_status' => ORDER_STATUS['SETTLEMENT'],
                'order_sub_status' => ORDER_SUB_STATUS['NEW'],
            ];
            $arrReceive = [];
            foreach ($dataUpdates as $item) {
                $arrReceive[] = $item->receive_id;
            }
            $modelO->updateData($arrReceive, $arrUpdate);
            Log::info("Process update success $countUp records.");
            print_r("Process update success $countUp records." . PHP_EOL);
        }
        $yahTotal       = 0;
        $yahSucc        = 0;
        $arrRakId       = [];
        $arrRakPayId    = [];
        $arrWowmaId     = [];
        $arrAmaId       = [];
        $arrNewHonten   = [];
        $arrExportCsv1  = [];
        $arrExportCsv2  = [];
        $arrExportCsv3  = [];
        $honOrderUpdate = [];

        $isCallAPI = true;
        if (App::environment(['local', 'test'])) {
            $isCallAPI = false;
        }

        $dataProcessIPI = $modelO->getDataFinishOrderAPI();
        $countApi = count($dataProcessIPI);
        if ($countApi === 0) {
            Log::info('No data process Api.');
            print_r("No data process Api." . PHP_EOL);
        } else {
            foreach ($dataProcessIPI as $item) {
                if (preg_match('/^re/', $item['received_order_id'])) {
                    $modelO->updateDataByReceiveOrder(
                        [$item['received_order_id']],
                        [
                            'error_code_api'   => 'OK',
                            'message_api'      => 'OK',
                            'order_sub_status' => ORDER_SUB_STATUS['DOING'],
                            'up_ope_cd'        => 'OPE99999',
                            'up_date'          => now(),
                        ]
                    );
                    continue;
                }
                if ($item->mall_id === 1) {
                    $arrRakId[$item->received_order_id]['shipment_date']     = $item->shipment_date;
                    $arrRakId[$item->received_order_id]['delivery_code']     = $item->delivery_code;
                    $arrRakId[$item->received_order_id]['inquiry_no']        = $item->inquiry_no;
                    $arrRakId[$item->received_order_id]['received_order_id'] = $item->received_order_id;
                } elseif ($item->mall_id === 2) {
                    $yahSucc += $this->changeStatusYahoo(
                        $item->received_order_id,
                        $item,
                        $isCallAPI
                    );
                    $yahTotal++;
                } elseif ($item->mall_id === 3) {
                    $arrAmaId[$item->received_order_id]['receive_id']      = $item->receive_id;
                    $arrAmaId[$item->received_order_id]['shipment_date']   = $item->shipment_date;
                    $arrAmaId[$item->received_order_id]['payment_method']  = $item->payment_method;
                    $arrAmaId[$item->received_order_id]['order_id']        = $item->received_order_id;
                    $arrAmaId[$item->received_order_id]['tracking_number'] = $item->inquiry_no;
                    $arrAmaId[$item->received_order_id]['carrier_name']    = $item->company_name;
                } elseif ($item->mall_id === 4) {
                    $arrExportCsv1[$item->received_order_id]['order_id']  = $item->received_order_id;
                    $arrExportCsv1[$item->received_order_id]['status']    = '発送済み';
                    if (isset($item->payment_method)) {
                        if ($item->payment_method === 1) {
                            $arrExportCsv2[$item->received_order_id]['text']           = 14239;
                            $arrExportCsv2[$item->received_order_id]['order_id']       = $item->received_order_id;
                            $arrExportCsv2[$item->received_order_id]['request_price']  = $item->request_price;
                        }
                    }
                } elseif ($item->mall_id === 5) {
                    $orderId = str_replace('diy-', '', $item->received_order_id) . '01';
                    $deliveryPlanDate = '';
                    if (!empty($item->delivery_plan_date)) {
                        $deliveryPlanDate = date('Y/m/d', (strtotime($item->delivery_plan_date)));
                    } elseif (!empty($item->max_delivery_date)) {
                        $deliveryPlanDate = date('Y/m/d', (strtotime($item->max_delivery_date)));
                    }
                    $arrExportCsv3[$item->received_order_id]['order_id']           = $orderId;
                    $arrExportCsv3[$item->received_order_id]['order_type']         = '';
                    $arrExportCsv3[$item->received_order_id]['classification']     = '';
                    $arrExportCsv3[$item->received_order_id]['inquiry_no']         = $item->inquiry_no;
                    $arrExportCsv3[$item->received_order_id]['delivery_plan_date'] = $deliveryPlanDate;
                    $arrExportCsv3[$item->received_order_id]['date']               = '';
                } elseif ($item->mall_id === 8) {
                    $arrNewHonten[] = [
                        'id'     => str_replace('DIY-', '', $item->received_order_id),
                        'status' => ORDER_STATUS['SETTLEMENT'],
                        'info'   => [
                            'shipment_date' => $item->shipment_date,
                        ],
                    ];
                    $honOrderUpdate[] = $item->received_order_id;
                } elseif ($item->mall_id === 9) {
                    $arrRakPayId[$item->received_order_id]['shipment_date']     = $item->shipment_date;
                    $arrRakPayId[$item->received_order_id]['received_order_id'] = $item->received_order_id;
                    $arrRakPayId[$item->received_order_id]['inquiry_no']        = empty($item->inquiry_no) ? '0' : $item->inquiry_no;
                    $arrRakPayId[$item->received_order_id]['delivery_code']     = $item->delivery_code;
                    $arrRakPayId[$item->received_order_id]['ship_id_rakuten']     = $item->ship_id_rakuten;
                } elseif ($item->mall_id === 10) {
                    $arrWowmaId[$item->received_order_id]['received_order_id']  = $item->received_order_id;
                    $arrWowmaId[$item->received_order_id]['delivery_plan_date'] = $item->delivery_plan_date;
                    $arrWowmaId[$item->received_order_id]['delivery_code']      = $item->delivery_code;
                    $arrWowmaId[$item->received_order_id]['inquiry_no']         = $item->inquiry_no;
                    $arrWowmaId[$item->received_order_id]['payment_method']     = $item->payment_method;
                }
            }
            Log::info("Processed finish order Yahoo : $yahTotal order and success: $yahSucc order.");
            print_r("Processed finish order Yahoo : $yahTotal order and success: $yahSucc order." . PHP_EOL);

            if (count($arrRakId) !== 0) {
                $this->changeStatusRakuten($arrRakId, $isCallAPI);
            }

            if (count($arrAmaId) !== 0) {
                $this->changeStatusAmazon($arrAmaId, $isCallAPI);
            }
            $newHontenSucc = count($arrNewHonten);
            if ($newHontenSucc !== 0) {
                $this->changeStatusNewHonten($arrNewHonten, $honOrderUpdate, $isCallAPI);
                Log::info("Processed finish order new Honten success: $newHontenSucc order.");
                print_r("Processed finish order new Honten success: $newHontenSucc order." . PHP_EOL);
            }

            if (count($arrExportCsv1)) {
                $this->exportCsv(1, $arrExportCsv1);
                $this->updateOrderSubStatus($arrExportCsv1);
                $bizTotal = count($arrExportCsv1);
                Log::info("Processed finish order BIZ - Format 1 : "
                    . "$bizTotal order and success: $bizTotal order.");
                print_r("Processed finish order BIZ - Format 1 : "
                    . "$bizTotal order and success: $bizTotal order." . PHP_EOL);
            }
            if (count($arrExportCsv2)) {
                $this->exportCsv(2, $arrExportCsv2);
                $this->updateOrderSubStatus($arrExportCsv2);
                $bizPayTotal = count($arrExportCsv2);
                Log::info("Processed finish order BIZ - Format 2 : "
                    . "$bizPayTotal order and success: $bizPayTotal order.");
                print_r("Processed finish order BIZ - Format 2 : "
                    . "$bizPayTotal order and success: $bizPayTotal order." . PHP_EOL);
            }
            if (count($arrExportCsv3)) {
                $this->exportCsv(3, $arrExportCsv3);
                $this->updateOrderSubStatus($arrExportCsv3);
                $honTotal = count($arrExportCsv3);
                Log::info("Processed finish order HONTEN : $honTotal order and success: $honTotal order.");
                print_r("Processed finish order HONTEN : $honTotal order and success: $honTotal order." . PHP_EOL);
            }

            if (count($arrRakPayId)) {
                $this->changeStatusRakPay($arrRakPayId);
                $rakPayTotal = count($arrRakPayId);
                Log::info("Processed finish order Rakuten Pay : $rakPayTotal order.");
                print_r("Processed finish order Rakuten Pay : $rakPayTotal order." . PHP_EOL);
            }

            if (count($arrWowmaId) > 0) {
                $this->changeStatusWowma($arrWowmaId);
                $wowmaTotal = count($arrWowmaId);
                Log::info("Processed finish order Wowma : $wowmaTotal order.");
                print_r("Processed finish order Wowma : $wowmaTotal order." . PHP_EOL);
            }
        }
        Event::fire(new eCommand($this->signature, array('end' => true, 'error' => $this->error)));
        $totalTime = round(microtime(true) - $start, 2);
        Log::info("End batch finish order with total time: $totalTime s.");
        print_r("End batch finish order with total time: $totalTime s.");
    }

    /**
     * Get request id
     * @return mixed
     */
    public function getRequestId()
    {
        try {
            $rakAuth   = Config::get('common.rak_auth_key');
            $rakUrl    = "https://api.rms.rakuten.co.jp/es/1.0/order/ws";
            $rakHeader = array(
                "Content-Type: text/xml;charset=UTF-8",
            );
            $requestXml = view('api.xml.rakuten_get_request', [
                'authKey'   => $rakAuth,
                'shopUrl'   => 'tuzukiya',
                'userName'  => 'tuzukiya',
            ])->render();
            $ch = curl_init($rakUrl);
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_HTTPHEADER, $rakHeader);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $requestXml);
            $responseXml = curl_exec($ch);
            curl_close($ch);
            $responseXml = str_ireplace(['S:', 'ns2:'], '', $responseXml);
            $responData  = simplexml_load_string($responseXml);
            if (!empty($responData) &&
                Helper::val($responData->Body->getRequestIdResponse->return->errorCode) === 'N00-000') {
                return Helper::val($responData->Body->getRequestIdResponse->return->requestId);
            } else {
                return false;
            }
        } catch (\Exception $e) {
            print_r($e->getMessage());
            $message = "Can't get request ID";
            print_r($message . PHP_EOL);
            Log::error($message);
            report($e);
            return false;
        }
    }
    /**
     * Finish order for mall Rakuten
     *
     * @param  array    $arrRakId Param received_order_id
     * @param  boolean  $isCallAPI
     * @return mixed
     */
    public function changeStatusRakuten($arrRakId, $isCallAPI)
    {
        $modelO = new MstOrder();
        $arrStep = array_chunk($arrRakId, 100);
        $success = 0;
        foreach ($arrStep as $step) {
            try {
                if ($isCallAPI) {
                    $rakAuth   = Config::get('common.rak_auth_key');
                    $rakUrl    = "https://api.rms.rakuten.co.jp/es/1.0/order/ws";
                    $rakHeader = array(
                        "Content-Type: text/xml;charset=UTF-8",
                    );
                    $startDate  = date("Y-m-d\TH:i:s", strtotime("-60 days"));
                    $endDate    = date("Y-m-d\TH:i:s");
                    $requestXml = view('api.xml.rakuten', [
                        'authKey'   => $rakAuth,
                        'shopUrl'   => 'tuzukiya',
                        'userName'  => 'tuzukiya',
                        'startDate' => $startDate,
                        'endDate'   => $endDate,
                        'orders'    => array_column($step, 'received_order_id'),
                    ])->render();
                    $ch = curl_init($rakUrl);
                    curl_setopt($ch, CURLOPT_POST, true);
                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                    curl_setopt($ch, CURLOPT_HTTPHEADER, $rakHeader);
                    curl_setopt($ch, CURLOPT_POSTFIELDS, $requestXml);
                    $responseXml = curl_exec($ch);
                    curl_close($ch);
                    $arrData = $this->processDataRakuten($responseXml, $arrRakId);

                    //Parse xml finish order
                    if (count($arrData) !== 0) {
                        $requestId = $this->getRequestId();
                        if ($requestId === true) {
                            continue;
                        }
                        $rakAuth   = Config::get('common.rak_auth_key');
                        $rakUrl    = "https://api.rms.rakuten.co.jp/es/1.0/order/ws";
                        $rakHeader = array(
                            "Content-Type: text/xml;charset=UTF-8",
                        );
                        $requestXml = view('api.xml.rakuten_finish_order', [
                            'authKey'   => $rakAuth,
                            'shopUrl'   => 'tuzukiya',
                            'userName'  => 'tuzukiya',
                            'requestId' => $requestId,
                            'datas'     => $arrData,
                        ])->render();
                        $ch = curl_init($rakUrl);
                        curl_setopt($ch, CURLOPT_POST, true);
                        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                        curl_setopt($ch, CURLOPT_HTTPHEADER, $rakHeader);
                        curl_setopt($ch, CURLOPT_POSTFIELDS, $requestXml);
                        $responseXml = curl_exec($ch);
                        curl_close($ch);
                        $responseXml = str_ireplace(['S:', 'ns2:'], '', $responseXml);
                        $responData  = simplexml_load_string($responseXml);
                    }
                } else {
                    $totalStep  = count($step);
                    $requestId  = '000000';
                    $responData = simplexml_load_string(
                        "<Result>
                            <Body>
                                <updateOrderResponse>
                                    <return>
                                        <errorCode>N00-000</errorCode>
                                        <count>$totalStep</count>
                                    </return>
                                </updateOrderResponse>
                            </Body>
                        </Result>"
                    );
                }
                if (!empty($responData)) {
                    $dataReturn = $responData->Body->updateOrderResponse->return;
                    if (Helper::val($dataReturn->errorCode) === 'N00-000') {
                        $success += Helper::val($dataReturn->count);
                        $arrUpdate = [
                            'order_sub_status' => ORDER_SUB_STATUS['DOING'],
                            'message_api'      => 'OK',
                            'error_code_api'   => 'OK',
                            'request_id'       => $requestId,
                            'up_ope_cd'        => 'OPE99999',
                            'up_date'          => now(),
                        ];
                        $modelO->updateDataByReceiveOrder(array_column($step, 'received_order_id'), $arrUpdate);
                    } else {
                        foreach ($dataReturn->unitError as $error) {
                            $id = Helper::val($error->orderKey);
                            $errorCode = Helper::val($error->errorCode);
                            $errorMess = Helper::val($error->message);
                            $arrUpdate = [
                                'message_api'    => $errorMess,
                                'error_code_api' => $errorCode,
                            ];
                            $modelO->updateDataByReceiveOrder([$id], $arrUpdate);
                        }
                        $arrUpdate = [
                            'order_sub_status' => ORDER_SUB_STATUS['DOING'],
                            'request_id'       => $requestId,
                            'up_ope_cd'        => 'OPE99999',
                            'up_date'          => now(),
                        ];
                        $modelO->updateDataByReceiveOrder($step, $arrUpdate);
                        $success += Helper::val($dataReturn->count);
                    }
                }
            } catch (\Exception $e) {
                $message = "Can't update status is wait to ship.";
                print_r($message . PHP_EOL);
                Log::error($message);
                report($e);
                continue;
            }
        }
        $count = count($arrRakId);
        Log::info("Processed finish order Rakuten : $count order and success: $success order.");
        print_r("Processed finish order Rakuten : $count order and success: $success order." . PHP_EOL);
    }

    /**
     * Finish order for mall Yahoo
     *
     * @param  array    $orderid list received_order_id
     * @param  boolean  $isCallAPI
     * @return mixed
     */
    public function changeStatusYahoo($orderid, $datas, $isCallAPI)
    {
        $modelO     = new MstOrder();
        $yahooToken = new YahooToken();
        try {
            if ($isCallAPI) {
                $accessToken = $yahooToken->find('order')->access_token;
                $yahooUrl = 'https://circus.shopping.yahooapis.jp/ShoppingWebService/V1/orderShipStatusChange';
                $yahooHeader = array(
                        "Authorization:Bearer " . $accessToken,
                    );
                if (empty($datas->delivery_code)) {
                    $checkDelivery   = false;
                    $shipmentDate    = $datas->shipment_date;
                    $shipMethod      = 'postage1';
                    $shipCompanyCode = 1000;
                } else {
                    $checkDelivery = true;
                    $shipmentDate  = $datas->delivery_real_date;
                    if ($datas->delivery_code === 3) {
                        $shipMethod      = 'postage2';
                        $shipCompanyCode = 1002;
                    } else if ($datas->delivery_code === 4) {
                        $shipMethod      = 'postage3';
                        $shipCompanyCode = 1003;
                    } else if ($datas->delivery_code === 999) {
                        $shipMethod      = 'postage4';
                        $shipCompanyCode = 1004;
                    } else {
                        $shipMethod      = '';
                        $shipCompanyCode = 1000;
                    }
                }
                $requestXml = view('api.xml.yahoo_ship_order', [
                    'orderId'         => $orderid,
                    'sellerId'        => 'diy-tool',
                    'isPointFix'      => true,
                    'operationUser'   => 'OPE99999',
                    'shipmentDate'    => date('Ymd', strtotime($shipmentDate)),
                    'shipMethod'      => $shipMethod,
                    'shipCompanyCode' => $shipCompanyCode,
                    'checkDelivery'   => $checkDelivery,
                    'inquiryNo'       => $datas->inquiry_no
                ])->render();
                $ch = curl_init($yahooUrl);
                curl_setopt($ch, CURLOPT_POST, true);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($ch, CURLOPT_HTTPHEADER, $yahooHeader);
                curl_setopt($ch, CURLOPT_POSTFIELDS, $requestXml);
                $responseXml = curl_exec($ch);
                curl_close($ch);
                $responData  = simplexml_load_string($responseXml);
                if (Helper::val($responData->Result->Status) === 'OK') {
                    $accessToken = $yahooToken->find('order')->access_token;
                    $yahooHeader = array(
                        "Authorization:Bearer " . $accessToken,
                    );
                    $yahooUrl = 'https://circus.shopping.yahooapis.jp/ShoppingWebService/V1/orderStatusChange';
                    $requestXml = view('api.xml.yahoo_finish_order', [
                        'orderId'  => $orderid,
                        'sellerId' => 'diy-tool',
                        'isPointFix' => true,
                        'operationUser' => 'OPE99999',
                    ])->render();
                    $ch = curl_init($yahooUrl);
                    curl_setopt($ch, CURLOPT_POST, true);
                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                    curl_setopt($ch, CURLOPT_HTTPHEADER, $yahooHeader);
                    curl_setopt($ch, CURLOPT_POSTFIELDS, $requestXml);
                    $responseXml = curl_exec($ch);
                    curl_close($ch);
                    $responData  = simplexml_load_string($responseXml);
                    $arrUpdate = [
                        'order_sub_status' => ORDER_SUB_STATUS['DOING'],
                        'error_code_api'   => "OK",
                        'message_api'      => "OK",
                        'up_ope_cd'        => 'OPE99999',
                        'up_date'          => now(),
                    ];
                    $modelO->updateDataByReceiveOrder([$orderid], $arrUpdate);
                    return 1;
                } else {
                    $modelO->updateDataByReceiveOrder([$orderid], [
                        'order_sub_status' => ORDER_SUB_STATUS['ERROR'],
                        'error_code_api'   => Helper::val($responData->Code),
                        'message_api'      => Helper::val($responData->Message),
                        'up_ope_cd'        => 'OPE99999',
                        'up_date'          => now(),
                    ]);
                    return 1;
                }
            } else {
                $arrUpdate = [
                    'order_sub_status' => ORDER_SUB_STATUS['DOING'],
                    'error_code_api'   => 'OK',
                    'message_api'      => 'OK',
                    'up_ope_cd'        => 'OPE99999',
                    'up_date'          => now(),
                ];
                $modelO->updateDataByReceiveOrder([$orderid], $arrUpdate);
                return 1;
            }
        } catch (\Exception $e) {
            $message = "Can't update status is finish.";
            print_r($message . PHP_EOL);
            Log::error($message);
            report($e);
            return 0;
        }
    }
    /**
     * Finish order for mall Amazon
     *
     * @param  array    $arrParam list received_order_id
     * @param  boolean  $isCallAPI
     * @return mixed
     */
    public function changeStatusAmazon($arrParam, $isCallAPI)
    {
        $modelO     = new MstOrder();
        $dataUpdate = [
            'order_sub_status' => ORDER_SUB_STATUS['DOING'],
            'up_ope_cd'        => 'OPE99999',
            'up_date'          => now()
        ];
        if (!$isCallAPI) {
            $dataUpdate['message_api']    = '_DONE_';
            $dataUpdate['error_code_api'] = '_DONE_';
            $modelO->updateDataByReceiveOrder(array_keys($arrParam), $dataUpdate);
            $count = count($arrParam);
            Log::info("Processed finish order Amazon : $count order and success: $count order.");
            print_r("Processed finish order Amazon : $count order and success: $count order." . PHP_EOL);
            return true;
        }
        $feedType = '_POST_ORDER_FULFILLMENT_DATA_';
        $config     = Config::get('amazonmws');
        $configHost = array(
            'ServiceURL'    => $config['ServiceURL'],
            'ProxyHost'     => $config['ProxyHost'],
            'ProxyPort'     => $config['ProxyPort'],
            'MaxErrorRetry' => $config['MaxErrorRetry'],
        );
        $service    = new MarketplaceWebService_Client(
            $config['AWS_ACCESS_KEY_ID'],
            $config['AWS_SECRET_ACCESS_KEY'],
            $configHost,
            $config['APPLICATION_NAME'],
            $config['APPLICATION_VERSION']
        );

        $feed = view(
            'api.xml.amazon_finish_order',
            ['params' => $arrParam, 'merchantIdentifier' => $config['MERCHANT_ID']]
        )->render();
        //***************** SubmitFeed ****************
        $feedSubmissionInfo = $this->submitFeed($feedType, $feed, $config, $service);
        if ($feedSubmissionInfo['status'] === false) {
            $dataUpdate['message_api']    = $feedSubmissionInfo['data'];
            $dataUpdate['error_code_api'] = 'Fail';
            $modelO->updateDataByReceiveOrder(array_keys($arrParam), $dataUpdate);
            print_r($feedSubmissionInfo['data'] . PHP_EOL);
            Log::error($feedSubmissionInfo['data']);
            $error  = "------------------------------------------" . PHP_EOL;
            $error .= 'Update finish order Amazon fail:' . PHP_EOL;
            $error .= $feedSubmissionInfo['data'] . PHP_EOL;
            $error .= PHP_EOL . "------------------------------------------" . PHP_EOL;
            $this->slack->notify(new SlackNotification($error));
            return false;
        }
        if (!$feedSubmissionInfo['data']->isSetFeedSubmissionId()) {
            $dataUpdate['message_api']    = 'No FeedSubmissionId';
            $dataUpdate['error_code_api'] = 'Fail';
            $modelO->updateDataByReceiveOrder(array_keys($arrParam), $dataUpdate);
            print_r("No FeedSubmissionId" . PHP_EOL);
            Log::error("No FeedSubmissionId");
            $error  = "------------------------------------------" . PHP_EOL;
            $error .= 'Update finish order Amazon fail have error:' . PHP_EOL;
            $error .= 'No FeedSubmissionId' . PHP_EOL;
            $error .= PHP_EOL . "------------------------------------------" . PHP_EOL;
            $this->slack->notify(new SlackNotification($error));
            return false;
        }
        $feedSubmissionId     = $feedSubmissionInfo['data']->getFeedSubmissionId();
        $feedSubmissionIdList = [$feedSubmissionId];
        //***************** GetFeedSubmissionList ****************
        //Do until DONE
        while (true) {
            sleep(80);
            $getFeedSubmissionListResult = $this->getFeedSubmissionList($feedSubmissionIdList, $config, $service);
            if ($getFeedSubmissionListResult['status'] === false) {
                $dataUpdate['message_api']    = $getFeedSubmissionListResult['data'];
                $dataUpdate['error_code_api'] = 'Fail';
                $modelO->updateDataByReceiveOrder(array_keys($arrParam), $dataUpdate);
                print_r($getFeedSubmissionListResult['data'] . PHP_EOL);
                Log::error($getFeedSubmissionListResult['data']);
                $error  = "------------------------------------------" . PHP_EOL;
                $error .= 'Update finish order Amazon fail:' . PHP_EOL;
                $error .= $getFeedSubmissionListResult['data'] . PHP_EOL;
                $error .= PHP_EOL . "------------------------------------------" . PHP_EOL;
                $this->slack->notify(new SlackNotification($error));
                return false;
            }
            $feedSubmissionInfoList = $getFeedSubmissionListResult['data']->getFeedSubmissionInfoList();
            $feedProcessingStatus   = false;
            foreach ($feedSubmissionInfoList as $feedSubmissionInfo) {
                if ($feedSubmissionInfo->isSetFeedProcessingStatus()) {
                    $feedProcessingStatus = $feedSubmissionInfo->getFeedProcessingStatus();
                    break;
                }
            }
            if (!$feedProcessingStatus) {
                $dataUpdate['message_api']    = 'No FeedProcessingStatus';
                $dataUpdate['error_code_api'] = 'Fail';
                $modelO->updateDataByReceiveOrder(array_keys($arrParam), $dataUpdate);
                print_r('No FeedProcessingStatus' . PHP_EOL);
                Log::error('No FeedProcessingStatus');
                $error  = "------------------------------------------" . PHP_EOL;
                $error .= 'Update finish order Amazon fail:' . PHP_EOL;
                $error .= $getFeedSubmissionListResult['data'] . PHP_EOL;
                $error .= PHP_EOL . "------------------------------------------" . PHP_EOL;
                $this->slack->notify(new SlackNotification($error));
                return false;
            }
            $arrStatusFinish = [
                '_DONE_',
                '_AWAITING_ASYNCHRONOUS_REPLY_',
                '_CANCELLED_',
                '_IN_SAFETY_NET_'
            ];

            if (in_array($feedProcessingStatus, $arrStatusFinish)) {
                $dataUpdate['message_api']    = $feedProcessingStatus;
                $dataUpdate['error_code_api'] = $feedProcessingStatus;
                $modelO->updateDataByReceiveOrder(array_keys($arrParam), $dataUpdate);
                break;
            }
        }

        // ***************** GetFeedSubmissionResult ****************
        $getFeedSubmissionResultResult = $this->getFeedSubmissionResult($feedSubmissionId, $config, $service);
        if ($getFeedSubmissionResultResult['status'] === false) {
            $dataUpdate['message_api']    = $getFeedSubmissionResultResult['data'];
            $dataUpdate['error_code_api'] = 'Fail';
            $modelO->updateDataByReceiveOrder(array_keys($arrParam), $dataUpdate);
            print_r($getFeedSubmissionResultResult['data'] . PHP_EOL);
            Log::error($getFeedSubmissionResultResult['data']);
            $error  = "------------------------------------------" . PHP_EOL;
            $error .= 'Update finish order Amazon fail:' . PHP_EOL;
            $error .= $getFeedSubmissionResultResult['data'] . PHP_EOL;
            $error .= PHP_EOL . "------------------------------------------" . PHP_EOL;
            $this->slack->notify(new SlackNotification($error));
            return false;
        }
        $responseXML = $getFeedSubmissionResultResult['data'];
        if (!empty($responseXML->Message->ProcessingReport->Result)) {
            $errAzMess = [];
            foreach ($responseXML->Message->ProcessingReport->Result as $item) {
                if ((string)$item->ResultCode === 'Error') {
                    $receiveId         = (string)$item->MessageID;
                    $resultDescription = (string)$item->ResultDescription;
                    $resultMessageCode = (string)$item->ResultMessageCode;
                    $dataUpdate['message_api']    = $resultDescription;
                    $dataUpdate['error_code_api'] = $resultMessageCode;
                    $modelO->updateData([$receiveId], $dataUpdate);
                    $errAzMess[] = "Receive ID $receiveId (Code $resultMessageCode): $resultDescription";
                }
            }
            if (count($errAzMess) > 0) {
                $error  = "------------------------------------------" . PHP_EOL;
                $error .= 'Update finish order Amazon fail:' . PHP_EOL;
                $error .= implode("\n", $errAzMess) . PHP_EOL;
                $error .= PHP_EOL . "------------------------------------------" . PHP_EOL;
                $this->slack->notify(new SlackNotification($error));
            }
        }
        $count = count($arrParam);
        Log::info("Processed finish order Amazon : $count order.");
        print_r("Processed finish order Amazon : $count order." . PHP_EOL);
    }

    /**
     * Submit feed
     *
     * @param  string   $feedType   Feed type
     * @param  string   $feed       Feed data
     * @param  array    $config     Config service
     * @param  object   $service    Service object
     * @return mixed
     */
    private function submitFeed($feedType, $feed, $config, $service)
    {
        $feedHandle = @fopen('php://temp', 'rw+');
        fwrite($feedHandle, $feed);
        rewind($feedHandle);
        $parameters = [
            'Merchant'        => $config['MERCHANT_ID'],
            'FeedType'        => $feedType,
            'FeedContent'     => $feedHandle,
            'PurgeAndReplace' => false,
            'ContentMd5'      => base64_encode(md5(stream_get_contents($feedHandle), true)),
        ];

        rewind($feedHandle);

        $request    = new MarketplaceWebService_Model_SubmitFeedRequest($parameters);
        $returnData = [
            'status' => false,
            'data'   => 'No reason'
        ];
        try {
            $response = $service->submitFeed($request);
            if ($response->isSetSubmitFeedResult()) {
                $submitFeedResult = $response->getSubmitFeedResult();
                if ($submitFeedResult->isSetFeedSubmissionInfo()) {
                    $returnData = [
                        'status' => true,
                        'data'   => $submitFeedResult->getFeedSubmissionInfo()
                    ];
                } else {
                    $returnData = [
                        'status' => false,
                        'data'   => 'No FeedSubmissionInfo'
                    ];
                }
            } else {
                $returnData = [
                    'status' => false,
                    'data'   => 'No SubmitFeedResult'
                ];
            }
        } catch (MarketplaceWebService_Exception $ex) {
            $message  = "Caught Exception: " . $ex->getMessage() . PHP_EOL;
            $message .= "Response Status Code: " . $ex->getStatusCode() . PHP_EOL;
            $message .= "Error Code: " . $ex->getErrorCode() . PHP_EOL;
            $message .= "Error Type: " . $ex->getErrorType() . PHP_EOL;
            $message .= "Request ID: " . $ex->getRequestId() . PHP_EOL;
            $message .= "XML: " . $ex->getXML() . PHP_EOL;
            $message .= "ResponseHeaderMetadata: " . $ex->getResponseHeaderMetadata() . PHP_EOL;
            print_r($message . PHP_EOL);
            Log::error($message);
            report($ex);
            $returnData = [
                'status' => false,
                'data'   => !empty($ex->getErrorCode())?$ex->getErrorCode():$ex->getStatusCode()
            ];
        }
        @fclose($feedHandle);
        return $returnData;
    }

    /**
     * Get feed submission list
     *
     * @param   array   $feedSubmissionIdList
     * @param   array   $config
     * @param   object  $service
     * @return  mixed
     */
    private function getFeedSubmissionList($feedSubmissionIdList, $config, $service)
    {
        $parameters = [
            'Merchant'                 => $config['MERCHANT_ID'],
            'FeedSubmissionIdList'     => ['Id' => $feedSubmissionIdList],
            'FeedProcessingStatusList' => ['Status' => ['_SUBMITTED_']]
        ];

        $request = new MarketplaceWebService_Model_GetFeedSubmissionListRequest($parameters);

        $returnData = [
            'status' => false,
            'data'   => 'No reason'
        ];
        try {
            $response = $service->getFeedSubmissionList($request);
            if ($response->isSetGetFeedSubmissionListResult()) {
                $returnData = [
                    'status' => true,
                    'data'   => $response->getGetFeedSubmissionListResult()
                ];
            } else {
                $returnData = [
                    'status' => false,
                    'data'   => 'No GetFeedSubmissionListResult'
                ];
            }
        } catch (MarketplaceWebService_Exception $ex) {
            $message  = "Caught Exception: " . $ex->getMessage() . PHP_EOL;
            $message .= "Response Status Code: " . $ex->getStatusCode() . PHP_EOL;
            $message .= "Error Code: " . $ex->getErrorCode() . PHP_EOL;
            $message .= "Error Type: " . $ex->getErrorType() . PHP_EOL;
            $message .= "Request ID: " . $ex->getRequestId() . PHP_EOL;
            $message .= "XML: " . $ex->getXML() . PHP_EOL;
            $message .= "ResponseHeaderMetadata: " . $ex->getResponseHeaderMetadata() . PHP_EOL;
            print_r($message . PHP_EOL);
            Log::error($message);
            report($ex);
            $returnData = [
                'status' => false,
                'data'   => !empty($ex->getErrorCode())?$ex->getErrorCode():$ex->getStatusCode()
            ];
        }
        return $returnData;
    }

    /**
     * Get feed submission result
     *
     * @param  string  $feedSubmissionId
     * @param  array   $config
     * @param  object  $service
     * @return object
     */
    private function getFeedSubmissionResult($feedSubmissionId, $config, $service)
    {
        $parameters = [
            'Merchant'             => $config['MERCHANT_ID'],
            'FeedSubmissionId'     => $feedSubmissionId,
            'FeedSubmissionResult' => @fopen('php://memory', 'rw+')
        ];

        $request    = new MarketplaceWebService_Model_GetFeedSubmissionResultRequest($parameters);
        $fileHandle = fopen('php://memory', 'rw+');
        $request->setFeedSubmissionResult($fileHandle);

        $returnData = [
            'status' => false,
            'data'   => 'No reason'
        ];
        try {
            $response = $service->getFeedSubmissionResult($request);
            rewind($fileHandle);
            $responseStr = stream_get_contents($fileHandle);
            $responseXML = new \SimpleXMLElement($responseStr);
            $returnData = [
                'status' => true,
                'data'   => $responseXML
            ];
        } catch (MarketplaceWebService_Exception $ex) {
            $message  = "Caught Exception: " . $ex->getMessage() . PHP_EOL;
            $message .= "Response Status Code: " . $ex->getStatusCode() . PHP_EOL;
            $message .= "Error Code: " . $ex->getErrorCode() . PHP_EOL;
            $message .= "Error Type: " . $ex->getErrorType() . PHP_EOL;
            $message .= "Request ID: " . $ex->getRequestId() . PHP_EOL;
            $message .= "XML: " . $ex->getXML() . PHP_EOL;
            $message .= "ResponseHeaderMetadata: " . $ex->getResponseHeaderMetadata() . PHP_EOL;
            print_r($message . PHP_EOL);
            Log::error($message);
            report($ex);
            $returnData = [
                'status' => false,
                'data'   => !empty($ex->getErrorCode())?$ex->getErrorCode():$ex->getStatusCode()
            ];
        }
        return $returnData;
    }
    /**
     * Export csv
     *
     * @param  int  $type
     * @param  array   $data
     * @return
     */
    public function exportCsv($type = 1, $data = null)
    {
        $csvHeader  = [];
        $fileName   = '';
        if ($type === 1) {
            $fileName = "monotos_st" . date('YmdHi') . ".csv";
            $csvHeader = [
                'モール別受注番号',
                '対応状況'
            ];
        } elseif ($type === 2) {
            $fileName = "monotos_card" . date('YmdHi') . ".csv";
        } elseif ($type === 3) {
            $fileName = "honten" . date('YmdHi') . ".csv";
            $csvHeader = [
                'お客様管理番号',
                '送り状種別',
                'クール区分',
                '伝票番号',
                '出荷予定日',
                'お届け予定（指定）日'
            ];
        }
        if (!empty($csvHeader)) {
            $header = mb_convert_encoding(implode(",", $csvHeader), 'Shift-JIS', 'UTF-8'). "\r\n";
        } else {
            $header = '';
        }
        if (count($data) > 0) {
            Storage::disk('delivery_finished_data')->put($fileName, $header);
            $url = Storage::disk('delivery_finished_data')
                                ->getDriver()->getAdapter()->getPathPrefix() .  $fileName;
            $file = fopen($url, 'a');
            $row = '';
            foreach ($data as $key => $value) {
                $row .= mb_convert_encoding(implode(",", $value), 'Shift-JIS', 'UTF-8') . "\r\n";
            }
            fputs($file, $row);
            fclose($file);
        } else {
            return;
        }
    }
    /**
     * Update order sub status
     *
     * @param  array $arrUpdate
     * @return
     */
    public function updateOrderSubStatus($arrUpdate = [])
    {
        if (count($arrUpdate) > 0) {
            $modelO = new MstOrder();
            $modelO->updateDataByReceiveOrder(
                array_keys($arrUpdate),
                ['order_sub_status' => ORDER_SUB_STATUS['DOING']]
            );
        }
        return;
    }

    /**
     * Process data Rakuten xml
     *
     * @param  string $responseXml
     * @return array
     */
    private function processDataRakuten($responseXml, $arrRakId)
    {

        $responseXml = str_ireplace(['S:', 'ns2:'], '', $responseXml);
        $responData  = simplexml_load_string($responseXml);
        $returnData  = $responData->Body->getOrderResponse->return;
        $dataInsert  = array();
        $errorCode   = Helper::val($returnData->errorCode);

        if ($errorCode === "E10-001") {
            return [];
        } else {
            $arrData = [];
            foreach ($returnData->orderModel as $orderModel) {
                $orderNumber                   = Helper::val($orderModel->orderNumber);
                $arrData[$orderNumber]         = $orderModel;
                $arrData[$orderNumber]->status = '処理済';
                $deliveryCompanyId             = '';
                $shippingNumber                = '';
                $shippingDate                  = date("Y-m-d\TH:i:s+09:00");
                if (isset($arrRakId[$orderNumber])) {
                    if ($arrRakId[$orderNumber]['delivery_code'] === 3) {
                        $deliveryCompanyId = 1002;
                    } elseif ($arrRakId[$orderNumber]['delivery_code'] === 4) {
                        $deliveryCompanyId = 1003;
                    } else {
                        $deliveryCompanyId = 1000;
                    }
                    $shippingNumber = $arrRakId[$orderNumber]['inquiry_no'];
                    $shippingDate   = date("Y-m-d\TH:i:s+09:00", strtotime($arrRakId[$orderNumber]['shipment_date']));
                }
                $arrData[$orderNumber]->packageModel->deliveryCompanyId = $deliveryCompanyId;
                $arrData[$orderNumber]->packageModel->shippingNumber    = $shippingNumber;
                $arrData[$orderNumber]->shippingDate                    = $shippingDate;
            }
            return $arrData;
        }
    }
    /**
     * Call Api new Honten cancel
     *
     * @param array   $honOrder
     * @param boolean $isCallAPI
     *
     * @return int
     */
    public function changeStatusNewHonten($honOrder, $arrKey, $isCallAPI)
    {
        $modelO     = new MstOrder();
        try {
            if ($isCallAPI) {
                $url = 'http://52.199.5.86:9191/api/order';
            } else {
                 $url = 'http://api-order.daitotest.tk/api/order';
            }
            $apikey        = Config::get('apiservices')['new_honten']['apikey'];
            $connection    = 'biz_api';
            $fields_string = json_encode($honOrder);
            $ch            = curl_init($url);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "put");
            curl_setopt(
                $ch,
                CURLOPT_HTTPHEADER,
                array(
                    'Content-Type: application/json',
                    'data_apikey: ' . $apikey,
                    'data_connection: ' . $connection,
                    'Content-Length: ' . strlen($fields_string)
                )
            );

            curl_setopt($ch, CURLOPT_POSTFIELDS, $fields_string);
            curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 0);
            curl_setopt($ch, CURLOPT_TIMEOUT, 400);
            $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
            $output   = curl_exec($ch);
            curl_close($ch);
            $result = json_decode($output);
            if (!empty($result)) {
                if ($result->error === 0) {
                    $errorCode = $result->code;
                    $message   = $result->msg;
                    $modelO->updateDataByReceiveOrder(
                        $arrKey,
                        [
                            'error_code_api'   => $errorCode,
                            'message_api'      => $message,
                            'order_sub_status' => ORDER_SUB_STATUS['DOING'],
                            'up_ope_cd'        => 'OPE99999',
                            'up_date'          => now(),
                        ]
                    );
                } else {
                    $detail = $result->detail;
                    $arrSuccess = [];
                    foreach ($detail as $item) {
                        if ($item->error === 0) {
                            $arrSuccess[] = 'DIY-' . $item->id;
                        } else {
                            if (is_numeric($item->id)) {
                                $receivedOrderId = 'DIY-' . $item->id;
                            } else {
                                $receivedOrderId = $item->id;
                            }
                            $modelO->updateDataByReceiveOrder(
                                [$receivedOrderId],
                                [
                                    'error_code_api'   => 500,
                                    'message_api'      => $item->detail,
                                    'order_sub_status' => ORDER_SUB_STATUS['DOING'],
                                    'up_ope_cd'        => 'OPE99999',
                                    'up_date'          => now(),
                                ]
                            );
                        }
                    }
                    if (count($arrSuccess) !== 0) {
                        $modelO->updateDataByReceiveOrder(
                            $arrSuccess,
                            [
                                'error_code_api'   => 200,
                                'message_api'      => 'OK',
                                'order_sub_status' => ORDER_SUB_STATUS['DOING'],
                                'up_ope_cd'        => 'OPE99999',
                                'up_date'          => now(),
                            ]
                        );
                    }
                }
            } else {
                $modelO->updateDataByReceiveOrder(
                    $arrKey,
                    [
                        'error_code_api'   => 500,
                        'message_api'      => 'ERROR',
                        'up_ope_cd'        => 'OPE99999',
                        'up_date'          => now(),
                    ]
                );
            }
        } catch (\Exception $e) {
            $message = "Can't update status is finish.";
            print_r($message . PHP_EOL);
            Log::error($message);
            report($e);
            return 0;
        }
    }

    /**
     * Process Api rakuten pay finish order
     * @param  array $arrDatas
     * @return object
     */
    public function changeStatusRakPay($arrDatas)
    {
        $modelTRP = new TRakutenPayOrderDetail();
        foreach ($arrDatas as $item) {
            $datas = $modelTRP->getDataFinishOrder($item['received_order_id']);
            if ($datas->count() !== 0) {
                $arrRequest = ['orderNumber' => $item['received_order_id'], 'BasketidModelList' => []];
                foreach ($datas as $data) {
                    $arrTemp = [];
                    $arrTemp['basketId'] = $data->itemmodel_basketid;
                    $deliveryCompanyId = null;
                    if ($item['delivery_code'] === 3) {
                        $deliveryCompanyId = 1002;
                    } elseif ($item['delivery_code'] === 4) {
                        $deliveryCompanyId = 1003;
                    } else {
                        $deliveryCompanyId = 1000;
                    }
                    $arrTemp['ShippingModelList'][0] = [
                        'shippingDate'    => date('Y-m-d', strtotime($item['shipment_date'])),
                        'shippingNumber'  => $item['inquiry_no'],
                        'deliveryCompany' => $deliveryCompanyId,
                    ];
                    if (!empty($item['ship_id_rakuten'])) {
                        $arrTemp['ShippingModelList'][0]['shippingDetailId'] = $item['ship_id_rakuten'];
                    }
                    $arrRequest['BasketidModelList'][] = $arrTemp;
                    if ($data->is_gift === 0) {
                        break;
                    }
                }
                try {
                    $rakUrl    = "https://api.rms.rakuten.co.jp/es/2.0/order/updateOrderShipping/";
                    $rakHeader = array(
                        "Accept: application/json",
                        "Content-type: application/json; charset=UTF-8",
                        "Authorization: {$this->keyRakuten}"
                    );
                    $requestJson = json_encode($arrRequest);
                    $ch = curl_init($rakUrl);
                    curl_setopt($ch, CURLOPT_POST, true);
                    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                    curl_setopt($ch, CURLOPT_HTTPHEADER, $rakHeader);
                    curl_setopt($ch, CURLOPT_POSTFIELDS, $requestJson);
                    $responseJson = curl_exec($ch);
                    curl_close($ch);
                    $responseJson = json_decode($responseJson);
                    if (isset($responseJson->MessageModelList) && count($responseJson->MessageModelList) !== 0) {
                        $modelO = new MstOrder();
                        $arrCodeErr  = [];
                        $arrMessErr  = [];
                        $arrCodeSucc = [];
                        $arrMessSucc = [];
                        foreach ($responseJson->MessageModelList as $response) {
                            if ($response->messageCode === 'ORDER_EXT_API_UPDATE_ORDERSHIPPING_INFO_101' ||
                            $response->messageCode === 'ORDER_EXT_API_UPDATE_ORDERSHIPPING_INFO_102' ||
                            $response->messageCode === 'ORDER_EXT_API_UPDATE_ORDERSHIPPING_INFO_103') {
                                $arrCodeSucc[$response->messageCode] = $response->messageCode;
                                $arrMessSucc[$response->messageCode] = $response->message;
                            } else {
                                $arrCodeErr[$response->messageCode]  = $response->messageCode;
                                $arrMessErr[$response->messageCode]  = $response->message;
                            }
                        }
                        if (count($arrCodeErr)) {
                            $flgSlack  = false;
                            $arrUpdate = [];
                            $message   = '';
                            $strErrorCode = str_replace('ORDER_EXT_API_UPDATE_ORDERSHIPPING_', '', implode(',', $arrCodeErr));
                            if (isset($arrCodeErr['ORDER_EXT_API_UPDATE_ORDERSHIPPING_ERROR_022']) ||
                                isset($arrCodeErr['ORDER_EXT_API_UPDATE_ORDERSHIPPING_INFO_001']) ||
                                isset($arrCodeErr['ORDER_EXT_API_UPDATE_ORDERSHIPPING_WARNING_001'])) {
                                continue;
                            } elseif (isset($arrCodeErr['ORDER_EXT_API_UPDATE_ORDERSHIPPING_ERROR_102'])) {
                                $arrUpdate = [
                                    'error_code_api'   => 'ORDER_EXT_API_UPDATE_ORDERSHIPPING_' . $strErrorCode,
                                    'message_api'      => implode('|', $arrMessErr),
                                    'order_sub_status' => ORDER_SUB_STATUS['ERROR'],
                                    'up_ope_cd'        => 'OPE99999',
                                    'up_date'          => now(),
                                ];
                            } else {
                                $arrUpdate = [
                                    'error_code_api'   => 'ORDER_EXT_API_UPDATE_ORDERSHIPPING_' . $strErrorCode,
                                    'message_api'      => implode('|', $arrMessErr),
                                    'up_ope_cd'        => 'OPE99999',
                                    'up_date'          => now(),
                                ];
                            }
                            $message  = implode('|', $arrMessErr);
                            $error  = "------------------------------------------" . PHP_EOL;
                            $error .= basename(__CLASS__) . PHP_EOL;
                            $error .= $item['received_order_id'] . '-' . $message;
                            $error .= PHP_EOL . "------------------------------------------" . PHP_EOL;
                            $this->slack->notify(new SlackNotification($error));
                            if (!empty($arrUpdate)) {
                                $modelO->updateDataByReceiveOrder([$item['received_order_id']], $arrUpdate);
                            }
                        } else {
                            $strSuccCode = str_replace('ORDER_EXT_API_UPDATE_ORDERSHIPPING_', '', implode(',', $arrCodeSucc));
                            $modelO->updateDataByReceiveOrder(
                                [$item['received_order_id']],
                                [
                                    'error_code_api'   => 'ORDER_EXT_API_UPDATE_ORDERSHIPPING_' . $strSuccCode,
                                    'message_api'      => implode('|', $arrMessSucc),
                                    'order_sub_status' => ORDER_SUB_STATUS['DOING'],
                                    'up_ope_cd'        => 'OPE99999',
                                    'up_date'          => now(),
                                ]
                            );
                        }
                    }
                } catch (\Exception $e) {
                    print_r($e->getMessage());
                    $message = "Can't update status is finish.";
                    print_r($message . PHP_EOL);
                    Log::error($message);
                    report($e);
                }
            } elseif (preg_match('/^re/', $item['received_order_id'])) {
                $modelO->updateDataByReceiveOrder(
                    [$item['received_order_id']],
                    [
                        'error_code_api'   => 'ORDER_EXT_API_UPDATE_ORDERSHIPPING_' . $strSuccCode,
                        'message_api'      => implode('|', $arrMessSucc),
                        'order_sub_status' => ORDER_SUB_STATUS['DOING'],
                        'up_ope_cd'        => 'OPE99999',
                        'up_date'          => now(),

                    ]
                );
            }
        }
    }

    public function changeStatusWowma($arrWowmaId)
    {
        $mstOrder    = new MstOrder();
        $environment = 'real';
        if (App::environment(['local', 'test'])) {
            $environment = 'test';
        }
        $shopId    = Config::get("wowma.$environment.shop_id");
        $wowmaAuth = Config::get("wowma.$environment.auth_key");
        $urlApi    = Config::get("wowma.$environment.url");

        foreach ($arrWowmaId as $data) {
            $wowmaHeader = array(
                "Content-Type: application/xml; charset=utf-8",
                "Authorization: $wowmaAuth"
            );
            $shippingCarrier = '';
            switch ($data['delivery_code']) {
                case 3:
                    $shippingCarrier = 2;
                    break;
                case 4:
                    $shippingCarrier = 6;
                    break;
                case 999:
                    $shippingCarrier = 5;
                    break;
            }
            $requestXml = view('api.xml.wowma_finish_order', [
                'shopId'          => $shopId,
                'orderId'         => $data['received_order_id'],
                'shippingDate'    => $data['delivery_plan_date'],
                'shippingCarrier' => $shippingCarrier,
                'shippingNumber'  => $data['inquiry_no']
            ])->render();
            $ch = curl_init($urlApi . '/updateTradeInfoProc');
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_HTTPHEADER, $wowmaHeader);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $requestXml);
            $responseXml = curl_exec($ch);
            curl_close($ch);
            $responData = simplexml_load_string($responseXml);
            if ((int)$responData->result->status !== 0) {
                $errorCode = (string)$responData->result->error->code;
                $errorMess = (string)$responData->result->error->message;
                $message = "Finish order wowma [{$data['received_order_id']}] has error:" . PHP_EOL;
                $message .= "[$errorCode] $errorMess";
                print_r($message . PHP_EOL);
                Log::error($message);
                $error  = "------------------------------------------" . PHP_EOL;
                $error .= basename(__CLASS__) . PHP_EOL;
                $error .= PHP_EOL . $message;
                $error .= PHP_EOL . "------------------------------------------" . PHP_EOL;
                $this->slack->notify(new SlackNotification($error));
                $mstOrder->updateDataByReceiveOrder(
                    [$data['received_order_id']],
                    [
                        'message_api'    => $errorMess,
                        'error_code_api' => $errorCode,
                    ]
                );
            } else {
                $mstOrder->updateDataByReceiveOrder(
                    [$data['received_order_id']],
                    [
                        'order_sub_status' => ORDER_SUB_STATUS['DOING'],
                        'message_api'      => "Finish_OK",
                        'error_code_api'   => ''
                    ]
                );
            }
        }
    }
}
