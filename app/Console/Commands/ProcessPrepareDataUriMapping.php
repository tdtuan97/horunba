<?php
/**
 * Batch process prepare data for uri mapping
 *
 * @package    App\Console\Commands
 * @subpackage ProcessRepareDataUriMapping
 * @copyright  Copyright (c) 2019 RiverCrane! Corporation. All Rights Reserved.
 * @author     Van Qui<van.qui@rivercrane.vn>
 */

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;
use App\Notifications\SlackNotification;
use App\Custom\Utilities;
use App\Notification;
use App\Events\Command as eCommand;
use Event;
use Config;
use Helper;
use App;
use App\Models\Batches\MstOrder;
use App\Models\Batches\DtUriMapping;

class ProcessPrepareDataUriMapping extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'process:prepare-data-uri-mapping';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Process prepare data for uri mapping';

    /**
     * The notification.
     *
     * @var object
     */
    protected $slack;

    /**
     * The error.
     *
     * @var array
     */
    public $error = [];

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //Create log
        $arrayReplace = [':', '-'];
        $folder       = str_replace($arrayReplace, '_', $this->signature);
        Log::useDailyFiles(storage_path() . "/logs/$folder/history.log");
        //Process data
        Event::fire(new eCommand($this->signature, array('start' => true)));
        Log::info('Start batch process prepare data uri mapping.');
        print_r("Start batch process prepare data uri mapping." . PHP_EOL);
        $start       = microtime(true);
        $this->slack = new Notification(CHANNEL['horunba']);
        $this->processContentFinishNew();
        $this->processContentFinishDoing();
        Event::fire(new eCommand($this->signature, array('end' => true, 'error' => $this->error)));
        $totalTime = round(microtime(true) - $start, 2);
        Log::info("End batch process prepare data uri mapping with total time: $totalTime s.");
        print_r("End batch process prepare data uri mapping with total time: $totalTime s.");
    }

    /**
     * Process content uri mapping finish new
     *
     * @return void
     */
    public function processContentFinishNew()
    {
        Log::info('Start process content 1.');
        print_r("Start process content 1." . PHP_EOL);

        $mstOrder = new MstOrder();
        $data = $mstOrder->getRepareDataUriMappingFinishNew();
        $data = $data->pluck('receive_id')->chunk(500)->toArray();
        $total = 0;
        foreach ($data as $arrReceiveId) {
            $mstOrder->updateData($arrReceiveId, [
                'order_status'     => ORDER_STATUS['FINISHED'],
                'order_sub_status' => ORDER_SUB_STATUS['NEW']
            ]);
            $total += count($arrReceiveId);
        }
        Log::info("Update $total records to table mst_order.");
        print_r("Update $total records to table mst_order." . PHP_EOL);

        Log::info('End process content 1.');
        print_r("End process content 1." . PHP_EOL);
    }

    /**
     * Process content uri mapping finish doing
     *
     * @return void
     */
    public function processContentFinishDoing()
    {
        Log::info('Start process content 2.');
        print_r("Start process content 2." . PHP_EOL);

        $mstOrder   = new MstOrder();
        $dataSteps  = $mstOrder->getRepareDataUriMappingFinishDoing()->chunk(1000);
        $modelDUM   = new DtUriMapping;
        $total      = 0;
        foreach ($dataSteps as $data) {
            $arrInserts   = [];
            $arrReceiveId = [];
            foreach ($data as $item) {
                $paymentPlanDate    = null;
                $paidPointPlanDate  = null;
                $couponPaidPlanDate = null;
                $dayOfMonth  = (int)date('d', strtotime($item->shipment_date));
                $dayOfWeek   = (int)date('w', strtotime($item->shipment_date));
                $monthOfDate = (int)date('m', strtotime($item->shipment_date));
                if ($item->mall_id === 9) {
                    if ($item->payment_method === 3) {
                        if ($dayOfMonth >= 1 && $dayOfMonth <= 10) {
                            $paymentPlanDate = date('Y-m-15', strtotime($item->shipment_date));
                        } elseif ($dayOfMonth >= 11 && $dayOfMonth <= 20) {
                            $paymentPlanDate = date('Y-m-25', strtotime($item->shipment_date));
                        } else {
                            $year  = (int)date('Y', strtotime($item->shipment_date)) + 1;
                            $month = '01';
                            $day   = '05';
                            if ($monthOfDate < 12) {
                                $year  = date('Y', strtotime($item->shipment_date));
                                $month = (int)date('m', strtotime($item->shipment_date)) + 1;
                            }
                            $paymentPlanDate = $year . '-' . $month . '-' . $day;
                        }
                    } else {
                        if ($dayOfMonth >= 26) {
                            $paymentPlanDate = date('Y-m-t', strtotime($item->shipment_date . '+1 month'));
                        } elseif ($dayOfMonth <= 10) {
                            $paymentPlanDate = date('Y-m-t', strtotime($item->shipment_date));
                        } else {
                            $year  = (int)date('Y', strtotime($item->shipment_date)) + 1;
                            $month = '01';
                            $day   = '15';
                            if ($monthOfDate < 12) {
                                $year  = date('Y', strtotime($item->shipment_date));
                                $month = (int)date('m', strtotime($item->shipment_date)) + 1;
                            }
                            $paymentPlanDate = $year . '-' . $month . '-' . $day;
                        }
                    }

                    if ($dayOfMonth >= 26) {
                            $temp = date('Y-m-t', strtotime($item->shipment_date . '+1 month'));
                    } elseif ($dayOfMonth <= 10) {
                        $temp = date('Y-m-t', strtotime($item->shipment_date));
                    } else {
                        $year  = (int)date('Y', strtotime($item->shipment_date)) + 1;
                        $month = '01';
                        $day   = '15';
                        if ($monthOfDate < 12) {
                            $year  = date('Y', strtotime($item->shipment_date));
                            $month = (int)date('m', strtotime($item->shipment_date)) + 1;
                        }
                        $temp = $year . '-' . $month . '-' . $day;
                    }
                    if ($item->used_point > 0) {
                        $paidPointPlanDate = $temp;
                    }
                    if ($item->used_coupon > 0) {
                        $couponPaidPlanDate = $temp;
                    }

                } elseif ($item->mall_id === 2) {
                    if ($item->payment_method === 3) {
                        if ($dayOfMonth >= 1 && $dayOfMonth <= 10) {
                            $paymentPlanDate = date('Y-m-15', strtotime($item->shipment_date));
                        } elseif ($dayOfMonth >= 11 && $dayOfMonth <= 20) {
                            $paymentPlanDate = date('Y-m-25', strtotime($item->shipment_date));
                        } else {
                            $year  = (int)date('Y', strtotime($item->shipment_date)) + 1;
                            $month = '01';
                            $day   = '05';
                            if ($monthOfDate < 12) {
                                $year  = date('Y', strtotime($item->shipment_date));
                                $month = (int)date('m', strtotime($item->shipment_date)) + 1;
                            }
                            $paymentPlanDate = $year . '-' . $month . '-' . $day;
                        }
                    } else {
                        $year  = (int)date('Y', strtotime($item->shipment_date)) + 1;
                        $month = '01';
                        $day   = '28';
                        if ($monthOfDate < 12) {
                            $year  = date('Y', strtotime($item->shipment_date));
                            $month = (int)date('m', strtotime($item->shipment_date)) + 1;
                        }
                        $paymentPlanDate = $year . '-' . $month . '-' . $day;
                    }
                    $year  = (int)date('Y', strtotime($item->shipment_date)) + 1;
                    $month = '01';
                    $day   = '28';
                    if ($monthOfDate < 12) {
                        $year  = date('Y', strtotime($item->shipment_date));
                        $month = (int)date('m', strtotime($item->shipment_date)) + 1;
                    }
                    $temp = $year . '-' . $month . '-' . $day;
                    if ($item->used_point > 0) {
                        $paidPointPlanDate = $temp;
                    }
                    if ($item->used_coupon > 0) {
                        $couponPaidPlanDate = $temp;
                    }

                } elseif ($item->mall_id === 3) {
                    if ($item->payment_method === 3) {
                        if ($dayOfMonth >= 1 && $dayOfMonth <= 10) {
                            $paymentPlanDate = date('Y-m-15', strtotime($item->shipment_date));
                        } elseif ($dayOfMonth >= 11 && $dayOfMonth <= 20) {
                            $paymentPlanDate = date('Y-m-25', strtotime($item->shipment_date));
                        } else {
                            $year  = (int)date('Y', strtotime($item->shipment_date)) + 1;
                            $month = '01';
                            $day   = '05';
                            if ($monthOfDate < 12) {
                                $year  = date('Y', strtotime($item->shipment_date));
                                $month = (int)date('m', strtotime($item->shipment_date)) + 1;
                            }
                            $paymentPlanDate = $year . '-' . $month . '-' . $day;
                        }
                    } else {
                        if ($dayOfWeek === 3) {
                            $paymentPlanDate = date('Y-m-d', strtotime($item->shipment_date . '+9 days'));
                        } elseif ($dayOfWeek === 4) {
                            $paymentPlanDate = date('Y-m-d', strtotime($item->shipment_date . '+8 days'));
                        } elseif ($dayOfWeek === 5) {
                            $paymentPlanDate = date('Y-m-d', strtotime($item->shipment_date . '+7 days'));
                        } elseif ($dayOfWeek === 6) {
                            $paymentPlanDate = date('Y-m-d', strtotime($item->shipment_date . '+6 days'));
                        } elseif ($dayOfWeek === 0) {
                            $paymentPlanDate = date('Y-m-d', strtotime($item->shipment_date . '+5 days'));
                        } elseif ($dayOfWeek === 1) {
                            $paymentPlanDate = date('Y-m-d', strtotime($item->shipment_date . '+4 days'));
                        } elseif ($dayOfWeek === 2) {
                            $paymentPlanDate = date('Y-m-d', strtotime($item->shipment_date . '+3 days'));
                        }
                    }
                    $temp = null;
                    if ($dayOfWeek === 3) {
                        $temp = date('Y-m-d', strtotime($item->shipment_date . '+9 days'));
                    } elseif ($dayOfWeek === 4) {
                        $temp = date('Y-m-d', strtotime($item->shipment_date . '+8 days'));
                    } elseif ($dayOfWeek === 5) {
                        $temp = date('Y-m-d', strtotime($item->shipment_date . '+7 days'));
                    } elseif ($dayOfWeek === 6) {
                        $temp = date('Y-m-d', strtotime($item->shipment_date . '+6 days'));
                    } elseif ($dayOfWeek === 0) {
                        $temp = date('Y-m-d', strtotime($item->shipment_date . '+5 days'));
                    } elseif ($dayOfWeek === 1) {
                        $temp = date('Y-m-d', strtotime($item->shipment_date . '+4 days'));
                    } elseif ($dayOfWeek === 2) {
                        $temp = date('Y-m-d', strtotime($item->shipment_date . '+3 days'));
                    }
                    if ($item->used_point > 0) {
                        $paidPointPlanDate = $temp;
                    }
                    if ($item->used_coupon > 0) {
                        $couponPaidPlanDate = $temp;
                    }
                } elseif ($item->mall_id === 8) {
                    if ($item->payment_method === 3) {
                        if ($dayOfMonth >= 1 && $dayOfMonth <= 10) {
                            $paymentPlanDate = date('Y-m-15', strtotime($item->shipment_date));
                        } elseif ($dayOfMonth >= 11 && $dayOfMonth <= 20) {
                            $paymentPlanDate = date('Y-m-25', strtotime($item->shipment_date));
                        } else {
                            $year  = (int)date('Y', strtotime($item->shipment_date)) + 1;
                            $month = '01';
                            $day   = '05';
                            if ($monthOfDate < 12) {
                                $year  = date('Y', strtotime($item->shipment_date));
                                $month = (int)date('m', strtotime($item->shipment_date)) + 1;
                            }
                            $paymentPlanDate = $year . '-' . $month . '-' . $day;
                        }
                    } elseif ($item->payment_method === 1) {
                        $year  = (int)date('Y', strtotime($item->shipment_date)) + 1;
                        $month = '01';
                        $day   = '15';
                        if ($monthOfDate < 12) {
                            $year  = date('Y', strtotime($item->shipment_date));
                            $month = (int)date('m', strtotime($item->shipment_date)) + 1;
                        }
                        $paymentPlanDate = $year . '-' . $month . '-' . $day;
                    } elseif ($item->payment_method === 12) {
                        $paymentPlanDate = date('Y-m-t', strtotime($item->shipment_date . '+1 month'));
                    }
                }
                $totalPaymentPlan = $item->request_price -
                                    $item->pay_charge - (
                                        $item->total_return_price +
                                        $item->total_return_point +
                                        $item->total_return_coupon
                                    );
                if ($item->mall_id !== 8) {
                    $totalPaymentPlan = $totalPaymentPlan + $item->used_point + $item->used_coupon;
                }
                $arrInserts[] = [
                    'received_order_id'     => $item->received_order_id,
                    'order_date'            => $item->order_date,
                    'delivery_date'         => $item->delivery_date,
                    'mall_id'               => $item->mall_id,
                    'payment_name'          => null,
                    'payment_method'        => $item->payment_method,
                    'order_status'          => $item->order_status,
                    'payment_plan_date'     => $paymentPlanDate,
                    'payment_date'          => null,
                    'total_payment_plan'    => $totalPaymentPlan,
                    'ship_charge'           => $item->ship_charge,
                    'pay_charge_discount'   => $item->pay_charge_discount,
                    'pay_charge'            => $item->pay_charge,
                    'pay_after_charge'      => $item->pay_after_charge,
                    'goods_price'           => $item->goods_price,
                    'discount'              => $item->discount,
                    'paid_point_plan_date'  => $paidPointPlanDate,
                    'point_paid_date'       => null,
                    'used_point'            => $item->used_point,
                    'paid_point'            => 0,
                    'coupon_paid_plan_date' => $couponPaidPlanDate,
                    'coupon_paid_date'      => null,
                    'used_coupon'           => $item->used_coupon,
                    'paid_coupon'           => 0,
                    'request_price'         => $item->request_price,
                    'return_price'          => !empty($item->total_return_price) ? $item->total_return_price : 0,
                    'paid_request_price'    => 0,
                    'total_paid_price'      => 0,
                    'diff_price'            => $totalPaymentPlan,
                    'remarks'               => '未入金',
                    'delivery_code'         => !empty($item->delivery_code) ? $item->delivery_code : 0,
                    'inquiry_no'            => !empty($item->inquiry_no) ? $item->inquiry_no : '',
                    'return_point'          => !empty($item->total_return_point) ? $item->total_return_point : 0,
                    'return_coupon'         => !empty($item->total_return_coupon) ? $item->total_return_coupon : 0,
                    'mall_recal_point'      => 0,
                    'mall_recal_coupon'     => 0,
                    'in_ope_cd'             => 'OPE99999',
                    'in_date'               => now(),
                    'up_ope_cd'             => 'OPE99999',
                    'up_date'               => now(),
                ];
                $arrReceiveId[] = $item->receive_id;
            }
            $countTmp = count($arrInserts);
            if ($countTmp !== 0) {
                try {
                    $modelDUM->replace($arrInserts);
                    $total += $countTmp;
                    $mstOrder->updateData($arrReceiveId, [
                        'order_status'     => ORDER_STATUS['FINISHED'],
                        'order_sub_status' => ORDER_SUB_STATUS['DOING']
                    ]);
                } catch (\Exception $e) {
                    $message = "Can't replace data to table dt_uri_mapping.";
                    print_r($message . PHP_EOL);
                    Log::error($message);
                    report($e);
                    return 0;
                }
            }
        }
        Log::info("Replace $total records to table dt_uri_mapping.");
        print_r("Replace $total records to table dt_uri_mapping." . PHP_EOL);
        Log::info('End process content 2.');
        print_r("End process content 2." . PHP_EOL);
    }
}
