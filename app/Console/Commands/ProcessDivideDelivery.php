<?php
/**
 * Batch process divide product
 *
 * @package    App\Console\Commands
 * @subpackage ProcessDivideProduct
 * @copyright  Copyright (c) 2017 RiverCrane! Corporation. All Rights Reserved.
 * @author     Truong Nghia<truong.van.nghia@rivercrane.vn>
 */
namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;
use App\Models\Batches\MstOrder;
use App\Models\Batches\MstOrderDetail;
use App\Models\Batches\DtOrderUpdateLog;
use App\Models\Batches\MstProduct;
use App\Models\Batches\MstCancelReservation;
use App\Models\Batches\DtOrderProductDetail;
use App\Models\Batches\MstEstimateStatus;
use App\Models\Batches\MstPaymentMethod;
use App\Models\Batches\MstStockStatus;
use App\Models\Batches\MstStatusBatch;
use App\Models\Backend\MstCustomer;
use App\Models\Batches\DtOrderToSupplier;
use App\Notifications\SlackNotification;
use App\Custom\Utilities;
use App\Notification;
use Validator;
use DB;
use App\Events\Command as eCommand;
use Event;

class ProcessDivideDelivery extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'process:divide-delivery';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Move order detail of all mall to mst_order_product_detail';

    /**
     * List rules check require.
     *
     * @var array
     */
    protected $rulesDivide = [
        // 'receive_id_checkPostalCodeAndAddress'         => 'checkPostalCodeAndAddress',
        'receive_id_checkPostalCodeAndAddressReceiver' => 'checkPostalCodeAndAddressReceiver',
        //'receive_id_checkPostalCodeAndAddressReceiverLength' => 'checkPostalCodeAndAddressReceiverLength',
        // 'receive_id_checkLimitRequestPrice'            => 'checkLimitRequestPrice',
        'receive_id_checkTelDigits'                    => 'checkTelDigits',
        'receive_id_checkTelDigits1'                    => 'checkTelDigits1',
        'receive_id_checkBlackList'                    => 'checkBlackList',
        // 'receive_id_checkChargeFeeTransport'           => 'checkChargeFeeTransport',
        'receive_id_checkChargeFeeDelivery'            => 'checkChargeFeeDelivery',
        'receive_id_checkChargeFeeCulDelivery'         => 'checkChargeFeeCulDelivery',
        'receive_id_checkPaymentDeliveryFee'           => 'checkPaymentDeliveryFee',
        'receive_id_checkPaidByPoint'                  => 'checkPaidByPoint',
        'receive_id_checkPaidNotByPoint'               => 'checkPaidNotByPoint',
        'order_firstname'                              => 'required',
        'order_lastname'                               => 'required',
        'order_tel'                                    => 'required',
        'order_zip_code'                               => 'required',
        'total_price'                                  => 'required|not_in:0',
        'order_date'                                   => 'required|date',
        'request_price'                                => 'required',
        'payment_method'                               => 'required|not_in:0',
        'product_code'                                 => 'checkProductCodeExists|checkProductSetValid',
        'price'                                        => 'required|not_in:0',
        'quantity'                                     => 'required|not_in:0',
        'ship_to_last_name'                            => 'required',
        'ship_zip_code'                                => 'required',
        'ship_tel_num'                                 => 'required',
    ];

    /**
     * List rules check require.
     *
     * @var array
     */
    protected $rulesDelivery = [
        'receive_id'     => 'checkDeliveryDirectAndPayment',
        'receive_id_checkLimitRequestPrice'            => 'checkLimitRequestPrice',
    ];

    /**
     * List message result validate.
     *
     * @var array
     */
    protected $message = [
        'total_price.not_in'                  => 'The total_price field must be greater than 0',
        'request_price.not_in'                => 'The request_price field must be greater than 0',
        'payment_method.not_in'               => 'The payment_method field must be greater than 0',
        'price.not_in'                        => 'The price field must be greater than 0',
        'quantity.not_in'                     => 'The quantity field must be greater than 0',
        'product_code.checkProductCodeExists' => 'マスタDBに存在していない商品があります。',
    ];

    /**
     * The error.
     *
     * @var array
     */
    public $error = [];

    /**
     * The limit of step process.
     *
     * @var int
     */
    protected $limit = 500;

    /**
     * The slack
     *
     * @var string
     */
    protected $slack = '';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //Create log
        $arrayReplace = [':', '-'];
        $folder       = str_replace($arrayReplace, '_', $this->signature);
        Log::useDailyFiles(storage_path() . "/logs/$folder/history.log");
        //Check cron
        $modelStatus = new MstStatusBatch;
        $check = $modelStatus->checkProcessOrder();
        $this->slack  = new Notification(CHANNEL['horunba']);
        if ($check > 0) {
            Log::info('Have some batch process order running->Stop this batch');
            print_r("Have some batch process order running->Stop this batch" . PHP_EOL);
            $error  = "------------------------------------------" . PHP_EOL;
            $error .= basename(__CLASS__) . PHP_EOL;
            $error .= 'This batch running->Stop this batch' . PHP_EOL;
            $error .= PHP_EOL . "------------------------------------------" . PHP_EOL;
            $this->slack->notify(new SlackNotification($error));
            return;
        }
        //Process data
        Event::fire(new eCommand($this->signature, array('start' => true)));
        Log::info('Start batch process divide product and delivery estimate.');
        print_r("Start batch process divide product and delivery estimate." . PHP_EOL);
        $start        = microtime(true);
        Log::info('Start batch divide set product to child product.');
        print_r("Start batch divide set product to child product." . PHP_EOL);
        $this->divideProduct();
        Log::info("End batch divide set product to child product.");
        print_r("End batch divide set product to child product." . PHP_EOL);
        Log::info('Start batch process delivery estimate.');
        print_r("Start batch process delivery estimate." . PHP_EOL);
        $this->deliveryEstimate();
        Log::info('End batch process delivery estimate.');
        print_r("End batch process delivery estimate." . PHP_EOL);

        $totalTime = round(microtime(true) - $start, 2);
        Event::fire(new eCommand($this->signature, array('end' => true, 'error' => $this->error)));
        Log::info("End batch divide set product to child product with total time: $totalTime.");
        print_r("End batch divide set product to child product with total time: $totalTime s." . PHP_EOL);
    }

    /**
     * Process divide product
     *
     * @return mixed
     */
    public function divideProduct()
    {
        $modelOrder   = new MstOrder();
        $modelOD      = new MstOrderDetail();
        $modelOPD     = new DtOrderProductDetail();
        $modelOUL     = new DtOrderUpdateLog();
        $modelProduct = new MstProduct();
        $modelCR      = new MstCancelReservation();
        $dataCanRes = $modelOrder->getDataCancelReservation();
        $total = count($dataCanRes);
        if (count($total) === 0) {
            Log::info('No data process');
            print_r("No data process. " . PHP_EOL);
        } else {
            DB::transaction(function () use ($modelOrder, $modelOD, $modelOPD, $modelCR, $modelOUL, $dataCanRes) {
                $arrUpdate['order_sub_status']    = ORDER_SUB_STATUS['DONE'];
                $arrUpdate['order_status']        = ORDER_STATUS['CANCEL'];
                $arrUpdate['cancel_reason']       = 5;
                $arrUpdate['ship_charge']         = 0;
                $arrUpdate['pay_charge_discount'] = 0;
                $arrUpdate['request_price']       = 0;
                $arrUpdate['discount']            = 0;
                $arrUpdate['used_coupon']         = 0;
                $arrUpdate['used_point']          = 0;
                $arrUpdate['pay_charge']          = 0;
                $arrUpdate['goods_price']         = 0;
                $arrUpdate['total_price']         = 0;
                $arrUpdate['err_text']            = '';
                $arrUpdate['is_mail_sent']        = 1;
                $arrUpCR      = ['status' => 1];
                $arrReceive   = [];
                $arrReceiveId = [];
                $arrLogData   = [];
                foreach ($dataCanRes as $dataCanRe) {
                    $arrReceive[]   = $dataCanRe->received_order_id;
                    $arrReceiveId[] = $dataCanRe->receive_id;
                    $arrLogData[]   = [
                        'receive_id'   => $dataCanRe->receive_id,
                        'log_title'    => 'キャンセル予約',
                        'log_contents' => $dataCanRe->note,
                        'log_status'   => 0,
                        'in_ope_cd'    => $dataCanRe->in_ope_cd,
                        'up_ope_cd'    => $dataCanRe->up_ope_cd
                    ];
                    $modelCR->where([
                        'mall_id' => $dataCanRe->mall_id,
                        'receive_order_id' => $dataCanRe->received_order_id,
                    ])->update($arrUpCR);
                }
                $modelOrder->updateDataByReceiveOrder($arrReceive, $arrUpdate);
                $modelOD->updateDataByReceiveIds($arrReceiveId, ['price' => 0, 'quantity' => 0]);
                if (count($arrReceiveId) > 0) {
                    $modelOPD->updateCancelStock($arrReceiveId);
                }
                $modelOUL->insert($arrLogData);
            }, 5);
            Log::info("Check cancel reservation success: $total records.");
            print_r("Check cancel reservation success: $total records." . PHP_EOL);
        }

        //Process update by is_on_hold
        $dataDelay = $modelOrder->getDataDelayByHold();
        if ($dataDelay->count() !== 0) {
            $arrReceive = $dataDelay->pluck('receive_id')->toArray();
            $modelOrder->updateData(
                $arrReceive,
                [
                    'is_delay'     => 1,
                    'delay_reason' => '発注保留'
                ]
            );
            $total = count($arrReceive);
            Log::info("Update order delay success: $total records.");
            print_r("Update order delay success: $total records." . PHP_EOL);
        } else {
            Log::info('No data process');
            print_r("No data process. " . PHP_EOL);
        }

        //Process update delivery_instrustions
        $dataInstustion = $modelOrder->getDataInstrustions();
        if ($dataInstustion->count() !== 0) {
            $arrReceive = [];
            $total      = 0;
            foreach ($dataInstustion as $value) {
                if (!isset($arrReceive[$value->receive_id])) {
                    $arrReceive[$value->receive_id] = $value->delivery_instrustions;
                }
                if (strpos($arrReceive[$value->receive_id], $value->ship_intrustion) === false) {
                    $arrReceive[$value->receive_id] .= $value->ship_intrustion;
                    $modelOrder->updateData(
                        [$value->receive_id],
                        [
                            'delivery_instrustions' => $arrReceive[$value->receive_id],
                        ]
                    );
                    $total++;
                }
            }
            Log::info("Update order delivery_instrustions success: $total records.");
            print_r("Update order delivery_instrustions success: $total records." . PHP_EOL);
        } else {
            Log::info('No data process');
            print_r("No data process. " . PHP_EOL);
        }


        $dataUpStatus = $modelOrder->getDataUpdateStatus();
        if (count($dataUpStatus) !== 0) {
            $idSuccess  = [];
            $idFail     = [];
            $failVal    = 0;
            $successVal = 0;
            foreach ($dataUpStatus as $value) {
                $rule = $this->rulesDivide;

                $rule['receive_id_checkPostalCodeAndAddressReceiver']
                    = 'checkPostalCodeAndAddressReceiver:' . $value['receiver_id'];
                $rule['receive_id_checkTelDigits']
                    = 'checkTelDigits:' . $value['receiver_id'];
                $rule['receive_id_checkTelDigits1']
                    = 'checkTelDigits1:' . $value['receiver_id'];
                // $rule['receive_id_checkChargeFeeTransport']
                //     = 'checkChargeFeeTransport:' . $value['receiver_id'];
                $rule['receive_id_checkChargeFeeDelivery']
                    = 'checkChargeFeeDelivery:' . $value['receiver_id'];
                $rule['receive_id_checkChargeFeeCulDelivery']
                    = 'checkChargeFeeCulDelivery:' . $value['receiver_id'];

                $checkTel     = false;
                $checkNotPaid = false;
                if ($value['is_ignore_error'] === 1) {
                    $idSuccess[] = $value['receive_id'];
                    continue;
                }
                $value['receive_id_checkPostalCodeAndAddress']         = $value['receive_id'];
                $value['receive_id_checkPostalCodeAndAddressReceiver'] = $value['receive_id'];
                // $value['receive_id_checkLimitRequestPrice']            = $value['receive_id'];
                $value['receive_id_checkChargeFeeCulDelivery']         = $value['receive_id'];
                // $value['receive_id_checkChargeFeeTransport']           = $value['receive_id'];
                $value['receive_id_checkChargeFeeDelivery']            = $value['receive_id'];
                $value['receive_id_checkPaymentDeliveryFee']           = $value['receive_id'];
                $value['receive_id_checkBlackList']                    = $value['receive_id'];
                $value['receive_id_checkTelDigits']                    = $value['receive_id'];
                $value['receive_id_checkTelDigits1']                   = $value['receive_id'];
                $value['receive_id_checkPaidByPoint']                  = $value['receive_id'];
                $value['receive_id_checkPaidNotByPoint']               = $value['receive_id'];
                //$value['receive_id_checkPostalCodeAndAddressReceiverLength'] = $value['receive_id'];
                if ($value['mall_id'] === 3) {
                    $value['order_firstname']    = 'AMAZON';
                    $value['ship_to_first_name'] = 'AMAZON';
                }
                if ($value['mall_id'] === 6) {
                    $rule = [];
                }
                if ($value['mall_id'] === 7 && $value['payment_method'] === 11) {
                    unset($rule['total_price']);
                    unset($rule['request_price']);
                    unset($rule['price']);
                    unset($rule['receive_id_checkPaidByPoint']);
                }
                if ($value['mall_id'] === 11) {
                    unset($rule['total_price']);
                    unset($rule['price']);
                }
                if (in_array($value['receive_id'], $idFail)) {
                    continue;
                }
                $arrKey = [
                    // 'receive_id_checkChargeFeeTransport',
                    'receive_id_checkChargeFeeDelivery',
                    'receive_id_checkChargeFeeCulDelivery',
                    'receive_id_checkPaymentDeliveryFee',
                ];
                $arrKeyConfirm = [
                    // 'receive_id_checkLimitRequestPrice',
                    'receive_id_checkBlackList',
                ];
                $arrKeyTel = [
                    'receive_id_checkTelDigits1'
                ];
                $arrKeyPaidNot = [
                    'receive_id_checkPaidNotByPoint'
                ];
                $validator = Validator::make($value, $rule, $this->message);
                if ($validator->fails()) {
                    $idFail[]  = $value['receive_id'];
                    $errorVal  = $validator->errors();
                    $message = [];
                    $arrUpdate = [];
                    foreach ($errorVal->toArray() as $key => $error) {
                        $message[] = $error[0];
                        if (in_array($key, $arrKey)) {
                            $arrUpdate['is_recalculate'] = 1;
                        }
                        if (in_array($key, $arrKeyConfirm)) {
                            $arrUpdate['is_confirmed'] = 1;
                        }
                        if (in_array($key, $arrKeyTel)) {
                            $checkTel = true;
                        }
                        if (in_array($key, $arrKeyPaidNot)) {
                            $checkNotPaid = true;
                        }
                    }
                    $message   = implode("\n", $message);
                    $arrUpdate['order_sub_status'] = ORDER_SUB_STATUS['ERROR'];
                    $arrUpdate['err_text']         = $message;
                    if ($checkNotPaid) {
                        $arrUpdate['order_sub_status']    = ORDER_SUB_STATUS['DONE'];
                        $arrUpdate['order_status']        = ORDER_STATUS['VERIFY_DATA'];
                        $arrUpdate['err_text']            = '';
                        $arrUpdate['payment_method']      = 10;
                    }
                    if ($checkTel) {
                        $arrUpdate['order_sub_status']    = ORDER_SUB_STATUS['DONE'];
                        $arrUpdate['order_status']        = ORDER_STATUS['VERIFY_DATA'];
                        $arrUpdate['err_text']            = '';
                        $arrUpdate['urgent_mail_id']      = 814;
                        $arrUpdate['is_send_mail_urgent'] = 1;
                        $modelCustomer   = new MstCustomer();
                        $dataCustomer = $modelCustomer->find($value['receiver_id']);
                        $dataCustomer->tel_num = '0667151162';
                        $dataCustomer->save();
                    }
                    try {
                        $modelOrder->updateData([$value['receive_id']], $arrUpdate);
                        $key = array_search($value['receive_id'], $idSuccess);
                        if ($key !== false) {
                            unset($idSuccess[$key]);
                        }
                    } catch (\Exception $e) {
                        $this->error[] = Utilities::checkMessageException($e);
                        $error  = "------------------------------------------" . PHP_EOL;
                        $error .= basename(__CLASS__) . PHP_EOL;
                        $error .= Utilities::checkMessageException($e);
                        $error .= PHP_EOL . "------------------------------------------" . PHP_EOL;
                        $this->slack->notify(new SlackNotification($error));
                        Log::error(Utilities::checkMessageException($e));
                        print_r("$error");
                        continue;
                    }
                } else {
                    $idSuccess[] = $value['receive_id'];
                }
            }
            $idSuccess = array_unique(array_diff($idSuccess, $idFail));
            $successVal = count($idSuccess);
            if ($successVal !== 0) {
                $arrUpdate = [
                        'order_sub_status' => ORDER_SUB_STATUS['DONE'],
                        'order_status'     => ORDER_STATUS['VERIFY_DATA'],
                        'err_text'         => ''
                    ];
                $modelOrder->updateData($idSuccess, $arrUpdate);
            }
            $failVal = count(array_diff($idFail, $idSuccess));
            Log::info("Check validation success: $successVal and fails: $failVal records");
            print_r("Check validation success: $successVal and fails: $failVal records" . PHP_EOL);
        } else {
            Log::info('No data process');
            print_r("No data process. " . PHP_EOL);
        }
        $dataProcess = $modelOrder->getDataProcessDivide();
        if (count($dataProcess) === 0) {
            Log::info('No data process');
            print_r("No data process. " . PHP_EOL);
        } else {
            $arrOrderProDetail = [];
            $receiveId         = '';
            $arrReceiveId      = [];
            $arrReceiveIdChild = [];
            $arrTemp           = [];
            foreach ($dataProcess as $key => $value) {
                if (count($arrTemp) === 0) {
                    $arrReceiveIdChild[] = $value->receive_id;
                } elseif (!in_array($value->receive_id, $arrTemp)) {
                    $arrTemp           = [];
                    $arrReceiveIdChild[] = $value->receive_id;
                }
                $realkey             = $key + 1;
                if ($realkey%$this->limit === 0) {
                    $arrReceiveId[]    = $arrReceiveIdChild;
                    $arrTemp = $arrReceiveIdChild;
                    $arrReceiveIdChild = [];
                }
                if ($receiveId === '' || $receiveId !== $value->receive_id) {
                    $subLineNum = 1;
                } else {
                    $subLineNum++;
                }
                $receiveId = $value->receive_id;
                if (!empty($value->parent_product_code)) {
                    $dataChild = $modelProduct->getDataByProductCode($value->child_product_code);
                    $arrOrderProDetail[$key]['received_order_num'] = $value->quantity * $value->component_num;
                    $arrOrderProDetail[$key]['child_product_code'] = $value->child_product_code;
                    $arrOrderProDetail[$key]['suplier_id']         = $dataChild->price_supplier_id;
                    $arrOrderProDetail[$key]['price_invoice']      = $dataChild->price_invoice;
                    if ($value->set_type === 1) {
                        $arrOrderProDetail[$key]['sale_price'] = $value->price/$value->component_num ;
                    } else {
                        $arrOrderProDetail[$key]['sale_price'] = 0;
                    }
                } else {
                    $arrOrderProDetail[$key]['received_order_num'] = $value->quantity;
                    $arrOrderProDetail[$key]['child_product_code'] = null;
                    $arrOrderProDetail[$key]['suplier_id']      = $value->price_supplier_id;
                    $arrOrderProDetail[$key]['price_invoice']   = $value->price_invoice;
                    $arrOrderProDetail[$key]['sale_price']      = $value->price;
                }
                $arrOrderProDetail[$key]['sub_line_num']    = $subLineNum;
                $arrOrderProDetail[$key]['receive_id']      = $value->receive_id;
                $arrOrderProDetail[$key]['detail_line_num'] = $value->detail_line_num;
                $arrOrderProDetail[$key]['product_code']    = $value->product_code;
                $arrOrderProDetail[$key]['delivery_type']   = 0;
                $arrOrderProDetail[$key]['stock_num']       = 0;
                $arrOrderProDetail[$key]['order_num']       = 0;
                $arrOrderProDetail[$key]['stock_status']    = 0;
                $arrOrderProDetail[$key]['product_status']  = 0;
                $arrOrderProDetail[$key]['in_date']         = now();
                $arrOrderProDetail[$key]['up_date']         = now();
            }
            if (count($arrReceiveIdChild) > 0) {
                $arrReceiveId[]    = $arrReceiveIdChild;
            }
            $arrSteps            = array_chunk($arrOrderProDetail, $this->limit);
            $modelOrderProDetail = new DtOrderProductDetail();
            $success   = 0;
            $fail      = 0;
            $updatFlag = 0;
            // process insert data to table dtb_order_product_detail.
            foreach ($arrSteps as $key => $step) {
                try {
                    $modelOrderProDetail->insert($step);
                    $success += count($step);
                } catch (\Exception $e) {
                    $this->error[] = Utilities::checkMessageException($e);
                    $error  = "------------------------------------------" . PHP_EOL;
                    $error .= basename(__CLASS__) . PHP_EOL;
                    $error .= Utilities::checkMessageException($e);
                    $error .= PHP_EOL . "------------------------------------------" . PHP_EOL;
                    $this->slack->notify(new SlackNotification($error));
                    Log::error(Utilities::checkMessageException($e));
                    print_r("$error");
                    $fail += count($step);
                    continue;
                }
                // update process status after insert dtb_order_product_detail success.
                $arrCondition = array_unique($arrReceiveId[$key]);
                $arrUpdate = [
                            'order_sub_status' => ORDER_SUB_STATUS['NEW'],
                            'order_status'     => ORDER_STATUS['EXPANSION'],
                        ];
                $check = $modelOrder->updateData($arrCondition, $arrUpdate);
                if ($check) {
                    $updatFlag += count($arrCondition);
                }
            }
            $message   = "Insert to table dt_order_product_detail success: $success records";
            $message  .= " and fail : $fail records";
            Log::info($message);
            print_r($message . PHP_EOL);
            Log::info("Update order status success : $updatFlag records");
            print_r("Update order status success : $updatFlag records" . PHP_EOL);
        }
    }

    /**
     * Process delivery estimate
     *
     * @return mixed
     */
    public function deliveryEstimate()
    {
        $modelP       = new MstPaymentMethod();
        $modelStock   = new MstStockStatus();
        $modelOPD     = new DtOrderProductDetail();
        $modelO       = new MstOrder();
        $modelOD      = new MstOrderDetail();
        $modelOTS     = new DtOrderToSupplier();
        $modelPS      = new MstEstimateStatus();
        $dataClassify        = $modelOPD->getDataClassify('product_code');
        $dataChildClasify    = $modelOPD->getDataClassify('child_product_code');
        $arrDataChildClasify = [];
        foreach ($dataChildClasify as $value) {
            $arrDataChildClasify[$value->child_product_code]['receive_id']        = $value->receive_id;
            $arrDataChildClasify[$value->child_product_code]['detail_line_num']   = $value->detail_line_num;
            $arrDataChildClasify[$value->child_product_code]['product_daihiki']   = $value->product_daihiki;
            $arrDataChildClasify[$value->child_product_code]['stock_default']     = $value->stock_default;
            $arrDataChildClasify[$value->child_product_code]['specify_deli_type'] = $value->specify_deli_type;
            $arrDataChildClasify[$value->child_product_code]['stock_lower_limit'] = $value->stock_lower_limit;
        }
        // Update delivery type.
        Log::info('=== Start process classify delivery. ===');
        print_r("=== Start process classify delivery. ===" . PHP_EOL);
        $deliverySuccess = 0;
        $deliveryFail    = 0;
        if (count($dataClassify) !== 0) {
            foreach ($dataClassify as $value) {
                $receiveId       = $value->receive_id;
                $detailLine      = $value->detail_line_num;
                $subLineNum      = $value->sub_line_num;
                $productDaihiki  = $value->product_daihiki;
                $stockDefault    = $value->stock_default;
                $stockLowerLimit = $value->stock_lower_limit;
                if (!empty($value->child_product_code)) {
                    $productDaihiki  = $arrDataChildClasify[$value->child_product_code]['product_daihiki'];
                    $stockDefault    = $arrDataChildClasify[$value->child_product_code]['stock_default'];
                    $stockLowerLimit = $arrDataChildClasify[$value->child_product_code]['stock_lower_limit'];
                }
                if ($value->specify_deli_type > 0) {
                    $modelOPD->updateData(
                        $receiveId,
                        $detailLine,
                        $subLineNum,
                        ['delivery_type' => $value->specify_deli_type]
                    );
                } else {
                    if ($productDaihiki > 0) {
                        $modelOPD->updateData(
                            $receiveId,
                            $detailLine,
                            $subLineNum,
                            ['delivery_type' => 2]
                        );
                        if ((!is_null($value->ship_wish_time) && $value->ship_wish_time !== '')
                            || (!is_null($value->ship_wish_date) && $value->ship_wish_date !== '')
                        ) {
                            $arrValue = [];
                            if (!empty($value->shop_answer)) {
                                $arrValue[] = $value->shop_answer;
                            }
                            $arrValue[] = '▼メーカー直送品は、日時指定を承ることができません。何卒ご了承ください。';
                            $modelO->updateData(
                                [$receiveId],
                                ['shop_answer' => implode("\n", $arrValue)]
                            );
                        }
                    } elseif ($stockLowerLimit === 0) {
                        $modelOPD->updateData(
                            $receiveId,
                            $detailLine,
                            $subLineNum,
                            ['delivery_type' => 3]
                        );
                    } else {
                        $modelOPD->updateData(
                            $receiveId,
                            $detailLine,
                            $subLineNum,
                            ['delivery_type' => 1]
                        );
                    }
                }
                $deliverySuccess++;
                $arrReceive = [];
                $arrReceive['receive_id'] = $receiveId;
                $arrReceive['receive_id_checkLimitRequestPrice'] = $receiveId;
                $validator   = Validator::make($arrReceive, $this->rulesDelivery);
                $flgValidate = false;
                $arrUpdate = [
                    'order_sub_status' => ORDER_SUB_STATUS['ERROR'],
                    'err_text'         => '',
                ];
                if ($validator->fails()) {
                    $errorVal    = $validator->errors();
                    $arrUpdate['err_text'] .= implode("\n", array_column($errorVal->toArray(), 0));
                    foreach ($errorVal->toArray() as $key => $error) {
                        if (in_array($key, ['receive_id_checkLimitRequestPrice'])) {
                            $arrUpdate['is_confirmed'] = 1;
                        }
                    }
                    $flgValidate = true;
                }

                $dataReceiver = $modelOD->getListReceiver($receiveId);
                foreach ($dataReceiver as $value) {
                    $rule['receive_id_checkChargeFeeTransport']
                        = 'checkChargeFeeTransport:' . $value['receiver_id'];
                    $dataCheck['receive_id_checkChargeFeeTransport'] = $receiveId;
                    $validateTrans = Validator::make($dataCheck, $rule);
                    if ($validateTrans->fails()) {
                        $arrUpdate['order_sub_status'] = ORDER_SUB_STATUS['ERROR_FEE_TRANSPORT'];
                        $errorVal = $validateTrans->errors();
                        if (!empty($arrUpdate['err_text'])) {
                            $arrUpdate['err_text'] .= "\n";
                        }
                        $arrUpdate['err_text'] .= implode("\n", array_column($errorVal->toArray(), 0));
                        $arrUpdate['is_recalculate'] = 1;
                        $flgValidate = true;
                        break;
                    }
                }

                if ($flgValidate) {
                    try {
                        $modelO->updateData([$receiveId], $arrUpdate);
                    } catch (\Exception $e) {
                        $this->error[] = Utilities::checkMessageException($e);
                        $error  = "------------------------------------------" . PHP_EOL;
                        $error .= basename(__CLASS__) . PHP_EOL;
                        $error .= Utilities::checkMessageException($e);
                        $error .= PHP_EOL . "------------------------------------------" . PHP_EOL;
                        $this->slack->notify(new SlackNotification($error));
                        Log::error(Utilities::checkMessageException($e));
                        print_r("$error");
                    }
                }
                $arrUp = [];
                $arrUp['sub_process_status'] = 1;
                $modelOPD->updateData($receiveId, $detailLine, $subLineNum, $arrUp);
            }
            $message = "End process classify delivery and ".
                       "Update success :$deliverySuccess record, Fail: $deliveryFail record.";
            Log::info("=== $message ===");
            print_r("=== $message ===" . PHP_EOL);
        } else {
            Log::info('No data process');
            print_r("No data process" . PHP_EOL);
            Log::info('=== End process classify delivery. ===');
            print_r("=== End process classify delivery. ===" . PHP_EOL);
        }
        Log::info('=== Start process inventory reservation 1. ===');
        print_r("=== Start process inventory reservation 1. ===" . PHP_EOL);
        $dataInventory = $modelOPD->getDataProcessInventory();
        $successInv = 0;
        $failInv    = 0;
        if (count($dataInventory) !== 0) {
            $productCode = '';
            $orderZanNum = 0;
            foreach ($dataInventory as $value) {
                if ($productCode !== $value->product_code) {
                    $productCode = $value->product_code;
                    $orderZanNum = $value->order_zan_num;
                }
                $receiveId        = $value->receive_id;
                $detailLine       = $value->detail_line_num;
                $subLineNum       = $value->sub_line_num;
                $receivedOrderNum = $value->received_order_num;
                $stockNum         = $value->stock_num;
                $cal = -1;
                if ($value->nanko_num !== ''
                    && $value->order_zan_num !== ''
                    && $value->return_num !== ''
                ) {
                    $cal = $value->nanko_num - $orderZanNum - $value->return_num;
                }
                if ($value->delivery_type === 1) {
                    /*$cal = -1;
                    if ($value->nanko_num !== ''
                        && $value->order_zan_num !== ''
                        && $value->return_num !== ''
                    ) {
                        $cal = $value->nanko_num - $orderZanNum - $value->return_num;
                    }*/
                    $dataUpdate = [];
                    if ($cal >= $receivedOrderNum) {
                        $dataUpdate['stock_num']      = $receivedOrderNum;
                        $dataUpdate['order_num']      = 0;
                        $dataUpdate['stock_status']   = 1;
                        $dataUpdate['product_status'] = PRODUCT_STATUS['WAIT_TO_SHIP'];
                        $modelOPD->updateData($receiveId, $detailLine, $subLineNum, $dataUpdate);
                        $modelStock->updateData(
                            $productCode,
                            [
                                'order_zan_num'     => DB::raw("order_zan_num + {$receivedOrderNum}"),
                                'wait_delivery_num' => DB::raw("wait_delivery_num + {$receivedOrderNum}")
                            ]
                        );
                        $orderZanNum += $receivedOrderNum;
                    } else {
                        if ($cal < 0) {
                            $cal = 0;
                        }
                        $dataUpdate['stock_num']      = 0;
                        $dataUpdate['order_num']      = $receivedOrderNum - $cal;
                        $dataUpdate['stock_status']   = 2;
                        $dataUpdate['product_status'] = PRODUCT_STATUS['WAIT_TO_ORDER'];
                        $modelOPD->updateData($receiveId, $detailLine, $subLineNum, $dataUpdate);
                        $modelStock->updateData(
                            $productCode,
                            [
                                'order_zan_num'     => DB::raw("order_zan_num + {$receivedOrderNum}"),
                                'wait_delivery_num' => DB::raw("wait_delivery_num + {$cal}")
                            ]
                        );
                        $orderZanNum += $receivedOrderNum;
                    }
                } elseif ($value->delivery_type === 3) {
                    if ($cal >= $receivedOrderNum) {
                        $modelStock->updateData(
                            $productCode,
                            [
                                'order_zan_num' => DB::raw("order_zan_num + {$receivedOrderNum}"),
                                'wait_delivery_num' => DB::raw("wait_delivery_num + {$receivedOrderNum}")
                            ]
                        );
                        $modelOPD->updateData(
                            $receiveId,
                            $detailLine,
                            $subLineNum,
                            [
                                'product_status' => PRODUCT_STATUS['WAIT_TO_SHIP'],
                                'order_num'      => 0,
                                'delivery_type'  => 1
                            ]
                        );
                        $orderZanNum += $receivedOrderNum;
                    } else {
                        $modelStock->updateData(
                            $productCode,
                            [
                                'order_zan_num' => DB::raw("order_zan_num + {$receivedOrderNum}"),
                            ]
                        );
                        $modelOPD->updateData(
                            $receiveId,
                            $detailLine,
                            $subLineNum,
                            [
                                'product_status' => PRODUCT_STATUS['WAIT_TO_ESTIMATE'],
                                'order_num' => $receivedOrderNum
                            ]
                        );
                    }
                }
                $successInv++;
            }
            $message = "End process inventory reservation 1 and".
                       " Update success: $successInv record, fail: $failInv record.";
            Log::info("=== $message ===");
            print_r("=== $message ===" . PHP_EOL);
        } else {
            Log::info('No data process');
            print_r("No data process" . PHP_EOL);
            Log::info('=== End process inventory reservation 1. ===');
            print_r("=== End process inventory reservation 1. ===" . PHP_EOL);
        }

        // Log::info('=== Start process inventory reservation 2. ===');
        // print_r("=== Start process inventory reservation 2. ===" . PHP_EOL);
        // $dataInventory2 = $modelOPD->getDataProcessInventory2();
        // if (count($dataInventory2) === 0) {
        //     Log::info('No data process');
        //     print_r("No data process" . PHP_EOL);
        //     Log::info('=== End process inventory reservation 2. ===');
        //     print_r("=== End process inventory reservation 2. ===" . PHP_EOL);
        // } else {
        //     $successInv2 = 0;
        //     $failInv2    = 0;
        //     foreach ($dataInventory2 as $value) {
        //         $productCode = !empty($value->child_product_code) ? $value->child_product_code : $value->product_code;
        //         $dataOTS = $modelOTS->getDataInventory2($productCode);
        //         $reserveNum = $value->received_order_num;
        //         foreach ($dataOTS as $item) {
        //             try {
        //                 $cal = $item->order_num - $item->order_remain_num - $reserveNum;
        //                 if ($cal >= 0) {
        //                     $modelOPD->where([
        //                         'receive_id'   => $value->receive_id,
        //                         'detail_line_num' => $value->detail_line_num,
        //                         'sub_line_num' => $value->sub_line_num,
        //                     ])->update(['order_code' => $item->order_code]);
        //                     $modelOTS->where('order_code', $item->order_code)
        //                     ->increment(
        //                         'order_remain_num',
        //                         $reserveNum
        //                     );
        //                     $successInv2++;
        //                     break;
        //                 } else {
        //                     $modelOTS->updateData($item->order_code, ['order_remain_num' => $item->replenish_num]);
        //                     $reserveNum = $reserveNum - ($item->replenish_num - $item->order_remain_num);
        //                 }
        //                 $successInv2++;
        //             } catch (\Exception $e) {
        //                 $this->error[] = Utilities::checkMessageException($e);
        //                 $error  = "------------------------------------------" . PHP_EOL;
        //                 $error .= basename(__CLASS__) . PHP_EOL;
        //                 $error .= Utilities::checkMessageException($e);
        //                 $error .= PHP_EOL . "------------------------------------------" . PHP_EOL;
        //                 $slack->notify(new SlackNotification($error));
        //                 Log::error(Utilities::checkMessageException($e));
        //                 print_r("$error");
        //                 $failInv2++;
        //             }
        //         }
        //     }
        //     $message = "End process inventory reservation 2 and".
        //                " Update success: $successInv2 record, fail: $failInv2 record.";
        //     Log::info("=== $message ===");
        //     print_r("=== $message ===" . PHP_EOL);
        // }

        // Log::info('=== Start process inventory reservation 3. ===');
        // print_r("=== Start process inventory reservation 3. ===" . PHP_EOL);
        // $dataInventory3 = $modelOPD->getDataProcessInventory3();
        // $successInv3 = 0;
        // if ($dataInventory3->count() === 0) {
        //     Log::info('No data process');
        //     print_r("No data process" . PHP_EOL);
        // } else {
        //     $previousProductCode  = '';
        //     $waitDelivery         = 0;
        //     $isUpdateWaitDelivery = 0;
        //     foreach ($dataInventory3 as $invent3) {
        //         if ($invent3->product_code !== $previousProductCode) {
        //             $waitDelivery         = $invent3->wait_delivery_num;
        //             $isUpdateWaitDelivery = 0;
        //             $previousProductCode  = $invent3->product_code;
        //         }

        //         if ($invent3->nanko_num >= $invent3->order_zan_num &&
        //             ($invent3->nanko_num - $waitDelivery) >= $invent3->received_order_num) {
        //             $arrUp3['product_status'] = PRODUCT_STATUS['WAIT_TO_SHIP'];
        //             $arrUp3['delivery_date']  = $modelO->calculateDeliveryDate($invent3);
        //             $modelOPD->updateData(
        //                 $invent3->receive_id,
        //                 $invent3->detail_line_num,
        //                 $invent3->sub_line_num,
        //                 $arrUp3
        //             );
        //             $waitDelivery += $invent3->received_order_num;
        //             $successInv3++;
        //         } elseif ($invent3->delivery_haiban === 'h') {
        //             $modelOPD->updateData(
        //                 $invent3->receive_id,
        //                 $invent3->detail_line_num,
        //                 $invent3->sub_line_num,
        //                 ['product_status' => PRODUCT_STATUS['SALE_STOP']]
        //             );
        //             $modelO->updateData(
        //                 [$invent3->receive_id],
        //                 ['order_sub_status' => ORDER_SUB_STATUS['STOP_SALE']]
        //             );
        //             $successInv3++;
        //         } elseif ($invent3->delivery_keppin === 'k') {
        //             $modelOPD->updateData(
        //                 $invent3->receive_id,
        //                 $invent3->detail_line_num,
        //                 $invent3->sub_line_num,
        //                 ['product_status' => PRODUCT_STATUS['OUT_OF_STOCK']]
        //             );
        //             $modelO->updateData(
        //                 [$invent3->receive_id],
        //                 ['order_sub_status' => ORDER_SUB_STATUS['OUT_OF_STOCK']]
        //             );
        //             $successInv3++;
        //         } elseif ($isUpdateWaitDelivery === 0) {
        //             $modelStock->updateData(
        //                 $invent3->product_code,
        //                 [
        //                     'wait_delivery_num' => $waitDelivery,
        //                 ]
        //             );
        //             $isUpdateWaitDelivery = 1;
        //             $successInv3++;
        //         }
        //     }
        // }
        // $message = "End process inventory reservation 3 and".
        //                " Update success: $successInv3 record.";
        // Log::info("=== $message ===");
        // print_r("=== $message ===" . PHP_EOL);
        Log::info('=== Start process delivery estimate. ===');
        print_r("=== Start process delivery estimate. ===" . PHP_EOL);
        $dataOrderDetail   = $modelOPD->getDataProcessDelivery();
        $dataProductStatus = $modelPS->getdata();
        $glscDelivery    = $directDelivery = $otherDelivery = $stockFlg =0;
        $supplier        = [];
        $productStatus   = false;
        $receiveId       = '';
        $successEstimate = 0;
        $failEstimate    = 0;
        $arrReceive      = [];
        if (count($dataOrderDetail) !== 0) {
            foreach ($dataOrderDetail as $value) {
                if ($receiveId !== '' && $receiveId !== $value->receive_id) {
                    $productStatus = $this->checkProductStatus(
                        $glscDelivery,
                        $directDelivery,
                        $otherDelivery,
                        $stockFlg,
                        $supplier,
                        $dataProductStatus
                    );
                    if ($productStatus) {
                        $modelOPD->updateProductStatusByDeliType($receiveId, $productStatus);
                        $successEstimate++;
                    }
                    $glscDelivery  = $directDelivery = $otherDelivery = $stockFlg =0;
                    $supplier        = [];
                    $productStatus = false;
                }
                $receiveId = $value->receive_id;
                $arrReceive[] = $receiveId;
                if ($value->delivery_type === 1) {
                    $glscDelivery = 1;
                    if ($value->stock_status === 2) {
                        $stockFlg = 1;
                    }
                } elseif ($value->delivery_type === 2) {
                    $directDelivery = 1;
                    $supplier []    = $value->suplier_id;
                } else {
                    $otherDelivery = 1;
                    $supplier []   = $value->suplier_id;
                }
            }
            $productStatus = $this->checkProductStatus(
                $glscDelivery,
                $directDelivery,
                $otherDelivery,
                $stockFlg,
                $supplier,
                $dataProductStatus
            );
            if ($productStatus) {
                $modelOPD->updateProductStatusByDeliType($receiveId, $productStatus);
                $successEstimate++;
            }

            $modelOPD->updateDataByReceive($arrReceive, ['sub_process_status' => 2]);
            $message = "End process delivery estimate and ".
                       "Update success: $successEstimate record, fail: $failEstimate record.";
            Log::info("=== $message ===");
            print_r("=== $message ===" . PHP_EOL);
        } else {
            Log::info('No data process');
            print_r("No data process" . PHP_EOL);
            Log::info('=== End process delivery estimate. ===');
            print_r("=== End process delivery estimate. ===" . PHP_EOL);
        }
        Log::info('=== Start process payment method. ===');
        print_r("=== Start process payment method. ===" . PHP_EOL);
        $dataProcessPayment = $modelO->getDataProcessPayment();
        if (count($dataProcessPayment) !== 0) {
            $checkUpdatePay = '';
            $success = 0;
            $fail = 0;
            $arrReceive = [];
            foreach ($dataProcessPayment as $data) {
                if (!in_array($data->receive_id, $arrReceive)) {
                    $arrReceive[] = $data->receive_id;
                }
                $arrUpdate = array('sub_process_status' => 3);
                if (($data->payment_method === 2 || $data->payment_method === 6 ||
                    $data->payment_method === 13 || $data->payment_method === 14 || $data->payment_method === 16)
                    && $data->product_status === PRODUCT_STATUS['WAIT_TO_ORDER'] && $data->stock_status === 0
                ) {
                    $arrUpdate['product_status'] = PRODUCT_STATUS['WAIT_TO_ESTIMATE'];
                }
                if ($data->product_status === PRODUCT_STATUS['WAIT_TO_ESTIMATE'] && $data->mall_id === 6) {
                    $arrUpdate['product_status'] = PRODUCT_STATUS['WAIT_TO_ORDER'];
                }
                $modelOPD->updateData(
                    $data->receive_id,
                    $data->detail_line_num,
                    $data->sub_line_num,
                    $arrUpdate
                );
                $success++;
            }
            $modelO->updateData($arrReceive, array('order_status' => ORDER_STATUS['ESTIMATION']));
            $message = "End process payment method and ".
                       "Update success: $success record, fail: $fail record.";
            Log::info("=== $message ===");
            print_r("=== $message ===" . PHP_EOL);
        } else {
            Log::info('No data process');
            print_r("No data process" . PHP_EOL);
            Log::info('=== End process payment method. ===');
            print_r("=== End process payment method. ===" . PHP_EOL);
        }
        Log::info('=== Start process close ok ===');
        print_r("=== Start process close ok ===" . PHP_EOL);
        $dataCloseOk = $modelOPD->getDataProcessCloseOk();
        $total = count($dataCloseOk);
        $chunkCloseOk = $dataCloseOk->chunk(1000);
        foreach ($chunkCloseOk as $arrReceive) {
            $modelOPD->updateDataByReceive($arrReceive, ['is_close_dok' => 1]);
        }
        Log::info("Total : $total updated");
        print_r("Total : $total updated" . PHP_EOL);
        Log::info('=== End process close ok. ===');
        print_r("=== End process close ok. ===" . PHP_EOL);

        Log::info('=== Start process sku num ===');
        print_r("=== Start process sku num ===" . PHP_EOL);
        $dataUpdateSku = $modelOPD->getDataProcessSkuNum();
        $total = count($dataUpdateSku);
        foreach ($dataUpdateSku as $item) {
            $modelOPD->updateDataByReceive([$item->receive_id], ['sku_num' => $item->sku_num_up]);
        }
        Log::info("Total : $total updated");
        print_r("Total : $total updated" . PHP_EOL);
        Log::info('=== End process sku num. ===');
        print_r("=== End process sku num. ===" . PHP_EOL);
    }
    /**
    * Check product status
    * @param int   $glscDelivery      flag for glsc delivery
    * @param int   $directDelivery    flag for direct delivery
    * @param int   $otherDelivery     flag for order delivery
    * @param int   $stockFlg          flag enough or not enough stock
    * @param array $supplier          array supplier with delivery <> 1
    * @param array $dataProductStatus array data product status
    * @return boolean or product status
    */
    public function checkProductStatus(
        $glscDelivery,
        $directDelivery,
        $otherDelivery,
        $stockFlg,
        $supplier,
        $dataProductStatus
    ) {
        $supplierFlg = 0;
        if ($glscDelivery !== 1) {
            $stockFlg = 0;
        }
        if (count($supplier) > 1) {
            $check = $supplier[0];
            foreach ($supplier as $sup) {
                if ($check === $sup) {
                    continue;
                } else {
                    $supplierFlg = 1;
                }
            }
        }
        foreach ($dataProductStatus as $value) {
            if ($glscDelivery   === $value->glsc_delivery &&
                $directDelivery === $value->direct_delivery &&
                $otherDelivery  === $value->other_delivery &&
                $stockFlg       === $value->stock_flg &&
                $supplierFlg    === $value->supplier_flg) {
                return $value->product_status;
            }
        }
        return false;
    }
}
