<?php
/**
 * Batch process check status delivery
 *
 * @package    App\Console\Commands
 * @subpackage ProcessDeliveryResult
 * @copyright  Copyright (c) 2017 RiverCrane! Corporation. All Rights Reserved.
 * @author     Truong Nghia<truong.van.nghia@rivercrane.vn>
 */

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\Batches\DtDelivery;
use App\Models\Batches\MstOrder;
use App\Models\Batches\MstStockStatus;
use App\Models\Batches\DtOrderProductDetail;
use App\Models\Batches\TShippingPerformanceModel;
use App\Models\Batches\DtReceiptLedger;
use App\Models\Batches\DtOrderToSupplier;
use Illuminate\Support\Facades\Log;
use App\Notifications\SlackNotification;
use App\Custom\Utilities;
use App\Notification;
use App\Events\Command as eCommand;
use Event;
use DB;

class ProcessDeliveryResult extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'process:delivery-result';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Check status delivery is okie or not good';

    /**
     * The notification.
     *
     * @var object
     */
    protected $slack;

    /**
     * The error.
     *
     * @var array
     */
    public $error = [];

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //Create log
        $arrayReplace = [':', '-'];
        $folder       = str_replace($arrayReplace, '_', $this->signature);
        Log::useDailyFiles(storage_path() . "/logs/$folder/history.log");
        //Process data
        Event::fire(new eCommand($this->signature, array('start' => true)));
        Log::info('Start batch process check delivery.');
        print_r("Start batch process check delivery." . PHP_EOL);
        $start       = microtime(true);
        $this->slack = new Notification(CHANNEL['horunba']);
        Log::info('=== Start process delivery. ===');
        print_r("=== Start process delivery. ===" . PHP_EOL);
        $this->processDeliveryResult();
        Log::info('=== End process delivery. ===');
        print_r("=== End process delivery. ===" . PHP_EOL);
        Event::fire(new eCommand($this->signature, array('end' => true, 'error' => $this->error)));
        $totalTime = round(microtime(true) - $start, 2);
        Log::info("End batch process check delivery with total time: $totalTime s.");
        print_r("End batch process check delivery with total time: $totalTime s.");
    }

    /**
    * Process delivery result
    * @return void
    */
    public function processDeliveryResult()
    {
        $modelDel   = new DtDelivery();
        $modelOPD   = new DtOrderProductDetail();
        $modelStock = new MstStockStatus();
        $modelDRL   = new DtReceiptLedger();
        $modelO     = new MstOrder();
        $modelTSPM  = new TShippingPerformanceModel();
        $modelOTS   = new DtOrderToSupplier();
        // $arrDataKb = ['1', '9'];
        // $success   = 0;
        // $fail      = 0;
        // $check     = 0;
        // $total     = 0;
        // foreach ($arrDataKb as $dataKb) {
        //     $datas = $modelDel->getDataDeliveryResult($dataKb);
        //     $total = $total + count($datas);
        //     if (count($datas) === 0) {
        //         $check++;
        //         continue;
        //     }
        //     foreach ($datas as $data) {
        //         DB::beginTransaction();
        //         try {
        //             $arrUpdate = [];
        //             $arrWhere  = [];
        //             $arrWhere['received_order_id'] = $data->received_order_id;
        //             $arrWhere['subdivision_num']   = $data->subdivision_num;
        //             $arrWhere['detail_line_num']   = $data->detail_line_num;
        //             $arrUpdate['inquiry_no']       = $data->InquiryNo;
        //             $arrUpdate['delivery_note']    = $data->Note;
        //             $arrUpdate['up_ope_cd']        = 'OPE99999';
        //             $deliveryReal = $data->delivery_real_num;
        //             if ($data->Quantity === $data->PlanQuantity) {
        //                 $arrUpdate['delivery_status']    = 3;
        //                 $arrUpdate['delivery_real_date'] = $data->DeliDate;
        //                 $arrUpdate['delivery_real_num']  = $data->Quantity;
        //                 $arrUpdate['delivery_code']      = $data->DeliveryCd;
        //                 $deliveryReal = $data->Quantity;
        //             } else {
        //                 if ($data->DataKb === '9') {
        //                     unset($arrWhere['subdivision_num']);
        //                     $arrUpdate['delivery_status'] = 8;
        //                     $arrUpdate['subdivision_num'] = $data->subdivision_num + 1;
        //                 } else {
        //                     $arrUpdate['delivery_status'] = 9;
        //                 }
        //             }

        //             $modelDel->updateData($arrWhere, $arrUpdate);
        //             if ($data->DataKb === '1') {
        //                 $modelDRL->add2ReceiptLedgerOutStock([
        //                     'pa_order_code'        => $data->received_order_id,
        //                     'pa_product_code'      => $data->product_code,
        //                     'pa_price_invoice'     => $data->price_invoice,
        //                     'pa_delivery_num'      => $deliveryReal,
        //                     'pa_stock_type'        => 1,
        //                     'pa_stock_detail_type' => 1,
        //                 ]);
        //                 $modelStock->updateStockOutStock(
        //                     $data->product_code,
        //                     (int)$deliveryReal
        //                 );
        //             }
        //             $modelTSPM->updateData(
        //                 ['tShippingPerformanceModel_autono' => $data->tShippingPerformanceModel_autono],
        //                 ['NodisplayFlg' => 2]
        //             );
        //             $success++;
        //             DB::commit();
        //         } catch (\Exception $e) {
        //             DB::rollback();
        //             $message = "Can't process receiveId {$data->received_order_id}";
        //             print_r($message . PHP_EOL);
        //             Log::error($message);
        //             report($e);
        //             $fail++;
        //             continue;
        //         }
        //     }
        // }
        // if ($check === 2) {
        //     Log::info('Step 1 no data');
        //     print_r("Step 1 no data" . PHP_EOL);
        // } else {
        //     $message1 = "Total records processed for step 1 : {$total}";
        //     print_r($message1 . PHP_EOL);
        //     Log::info($message1);
        //     $message2  = "ReceiveId fail : $fail";
        //     print_r($message2 . PHP_EOL);
        //     Log::info($message2);
        // }

        // $datas2 = $modelDel->getDataDeliveryResult2();
        // if (count($datas2) === 0) {
        //     Log::info('Step 2 no data');
        //     print_r("Step 2 no data" . PHP_EOL);
        // } else {
        //     $success   = 0;
        //     $fail      = 0;
        //     foreach ($datas2 as $data2) {
        //         DB::beginTransaction();
        //         try {
        //             $arrUpdate = ['delivery_code' => $data2->DeliveryCd, 'inquiry_no' => $data2->InquiryNo];
        //             $arrWhere  = [];
        //             $arrWhere['received_order_id'] = $data2->received_order_id;
        //             $arrWhere['subdivision_num']   = $data2->subdivision_num;
        //             $arrWhere['detail_line_num']   = $data2->detail_line_num;
        //             $modelDel->updateData($arrWhere, $arrUpdate);
        //             $modelTSPM->updateData(
        //                 ['tShippingPerformanceModel_autono' => $data2->tShippingPerformanceModel_autono],
        //                 ['NodisplayFlg' => 2]
        //             );
        //             $success++;
        //             DB::commit();
        //         } catch (\Exception $e) {
        //             DB::rollback();
        //             $message = "Can't process receiveId {$data2->received_order_id}";
        //             print_r($message . PHP_EOL);
        //             Log::error($message);
        //             report($e);
        //             $fail++;
        //             continue;
        //         }
        //     }
        //     $message = "Process step 2 success: $success record, fail: $fail.";
        //     print_r($message . PHP_EOL);
        //     Log::info($message);
        // }

        // $datas3 = $modelDel->getDataDeliveryResult3();
        // $total = count($datas3);
        // if ($total === 0) {
        //     Log::info('Step 3 no data');
        //     print_r("Step 3 no data" . PHP_EOL);
        // } else {
        //     $success   = 0;
        //     $fail      = 0;
        //     foreach ($datas3 as $value) {
        //         DB::beginTransaction();
        //         try {
        //             if ($value->Quantity < 0) {
        //                 $arrUpdate = [
        //                     'delivery_status'    => 5,
        //                     'delivery_real_date' => $value->DeliDate,
        //                     'delivery_real_num'  => $value->Quantity,
        //                 ];
        //                 $arrWhere  = [];
        //                 $arrWhere['received_order_id'] = $value->received_order_id;
        //                 $arrWhere['subdivision_num']   = $value->subdivision_num;
        //                 $arrWhere['detail_line_num']   = $value->detail_line_num;
        //                 $modelDel->updateData($arrWhere, $arrUpdate);
        //                 $modelDRL->add2ReceiptLedgerOutStock([
        //                     'pa_order_code'        => $value->received_order_id,
        //                     'pa_product_code'      => $value->product_code,
        //                     'pa_price_invoice'     => $value->price_invoice,
        //                     'pa_delivery_num'      => $value->Quantity,
        //                     'pa_stock_type'        => 1,
        //                     'pa_stock_detail_type' => 8,
        //                 ]);
        //                 $modelStock->updateStockOutStock(
        //                     $value->product_code,
        //                     $value->Quantity
        //                 );
        //                 $modelTSPM->updateData(
        //                     ['tShippingPerformanceModel_autono' => $value->tShippingPerformanceModel_autono],
        //                     ['NodisplayFlg' => 2]
        //                 );
        //             }
        //             $success++;
        //             DB::commit();
        //         } catch (\Exception $e) {
        //             DB::rollback();
        //             $message = "Can't process receiveId {$value->received_order_id}";
        //             print_r($message . PHP_EOL);
        //             Log::error($message);
        //             report($e);
        //             $fail++;
        //             continue;
        //         }
        //     }
        //     $message = "Process step 3 success: $success, fail: $fail record.";
        //     print_r($message . PHP_EOL);
        //     Log::info($message);
        // }
        // $datas4 = $modelO->getDataDeliveryResult4();
        // $total = count($datas4);
        // if ($total === 0) {
        //     Log::info('Step 4 no data');
        //     print_r("Step 4 no data" . PHP_EOL);
        // } else {
        //     foreach ($datas4 as $value) {
        //         $modelDRL->add2ReceiptLedgerInStock([
        //             'pa_order_code'        => $value->edi_order_code,
        //             'pa_product_code'      => $value->product_code,
        //             'pa_price_invoice'     => $value->price_invoice,
        //             'pa_arrival_num'       => $value->received_order_num,
        //             'pa_stock_type'        => 2,
        //             'pa_stock_detail_type' => 2,
        //         ]);
        //         $modelDRL->add2ReceiptLedgerOutStock([
        //             'pa_order_code'        => $value->received_order_id,
        //             'pa_product_code'      => $value->product_code,
        //             'pa_price_invoice'     => $value->price_invoice,
        //             'pa_delivery_num'      => $value->received_order_num,
        //             'pa_stock_type'        => 1,
        //             'pa_stock_detail_type' => 2,
        //         ]);
        //         $modelOTS->updateData($value->order_code, ['process_status' => 6]);
        //     }
        //     $message1 = "Total records processed for step 4 : {$total}";
        //     print_r($message1 . PHP_EOL);
        //     Log::info($message1);
        // }
        $datas5 = $modelO->getDataDeliveryResult5();
        if (count($datas5) === 0) {
            Log::info('Step 5 no data');
            print_r("Step 5 no data" . PHP_EOL);
        } else {
            $total = count($datas5);
            $fail2 = 0;
            foreach ($datas5 as $value) {
                DB::beginTransaction();
                try {
                    $arrWhereD = [
                        'received_order_id' => $value->received_order_id,
                        'subdivision_num'   => $value->subdivision_num,
                    ];
                    $modelDel->updateData($arrWhereD, ['delivery_status' => 3]);
                    $modelOPD->updateData(
                        $value->od_receive_id,
                        $value->od_detail_line_num,
                        $value->od_sub_line_num,
                        ['product_status' => PRODUCT_STATUS['SHIPPED'], 'delivery_date' => $value->delivery_real_date]
                    );
                    DB::commit();
                } catch (\Exception $e) {
                    DB::rollback();
                    $message = "Can't process receiveId {$value->received_order_id}";
                    print_r($message . PHP_EOL);
                    Log::error($message);
                    report($e);
                    $fail2++;
                    continue;
                }
            }
            $message1 = "Total records processed for step 5 : {$total}";
            print_r($message1 . PHP_EOL);
            Log::info($message1);
            $message2  = "ReceiveId fail : $fail2";
            print_r($message2 . PHP_EOL);
            Log::info($message2);
        }

        $datas6 = $modelO->getDataDeliveryResult6();
        if (count($datas6) === 0) {
            $message  = "Step 6 no data.";
            print_r($message . PHP_EOL);
            Log::info($message);
        } else {
            foreach ($datas6 as $data) {
                $modelDRL->add2ReceiptLedgerInStock([
                    'pa_order_code'        => $data->edi_order_code,
                    'pa_product_code'      => $data->product_code,
                    'pa_price_invoice'     => $data->price,
                    'pa_arrival_num'       => $data->received_order_num,
                    'pa_supplier_id'       => $data->supplier_id,
                    'pa_stock_type'        => 2,
                    'pa_stock_detail_type' => 2,
                ]);
                $modelDRL->add2ReceiptLedgerOutStock([
                    'pa_order_code'        => $data->received_order_id,
                    'pa_product_code'      => $data->product_code,
                    'pa_price_invoice'     => $data->price,
                    'pa_delivery_num'      => $data->received_order_num,
                    'pa_supplier_id'       => $data->supplier_id,
                    'pa_stock_type'        => 1,
                    'pa_stock_detail_type' => 2,
                ]);
                $modelOTS->updateData($data->order_code, ['process_status' => 6], $data->edi_order_code);
            }
            $total = count($datas6);
            $message1 = "Total records processed for step 6 : {$total}";
            print_r($message1 . PHP_EOL);
            Log::info($message1);
        }
        $datas7 = $modelO->getDataDeliveryResult7();
        if (count($datas7) !== 0) {
            $arrReceiveId = [];
            $receiveId    = '';
            $check        = true;
            $curDate      = date('Y-m-d');
            foreach ($datas7 as $data) {
                if ($receiveId === '' || $receiveId !== $data->receive_id) {
                    if ($receiveId !== '' && $check) {
                        $arrReceiveId[] = $receiveId;
                    }
                    $receiveId = $data->receive_id;
                    $check = true;
                }
                if (empty($data->shipment_date) || (!empty($data->shipment_date) && strtotime($data->shipment_date) <= time())) {
                    $shipmentDate = date('Y-m-d');
                } else {
                    $shipmentDate = date('Y-m-d', strtotime($data->shipment_date));
                }
                if (strtotime($shipmentDate) !== strtotime($curDate)) {
                    $check = false;
                    continue;
                }
                if ($data->product_status !== PRODUCT_STATUS['DIRECTLY_SHIPPING']
                    && $data->product_status !== PRODUCT_STATUS['SHIPPED']
                    && $data->product_status !== PRODUCT_STATUS['CANCEL']) {
                    $check = false;
                }
            }
            if ($receiveId !== '' && $check) {
                $arrReceiveId[] = $receiveId;
            }
            $total = count($arrReceiveId);
            if ($total === 0) {
                Log::info('Step 7 have data but not update');
                print_r("Step 7 have data but not update" . PHP_EOL);
            } else {
                $arrUpdate = [
                    'shipment_date'    => DB::raw("IF(DATE(shipment_date) < NOW(), NOW(), IFNULL(shipment_date, NOW()))"),
                    'order_sub_status' => ORDER_SUB_STATUS['DONE'],
                    'is_mail_sent'     => 0,
                    'up_ope_cd'        => 'OPE99999',
                    'up_date'          => now(),
                ];
                $modelO->updateData($arrReceiveId, $arrUpdate);
                $message = "Step 7 update data to table dt_order_product_detail {$total} records";
                Log::info($message);
                print_r($message . PHP_EOL);
            }
            // if ($total !== 0) {
            //     $resDeli = $modelOPD->getMaxDeliveryDate($arrReceiveId);
            //     $resDeli = $resDeli->pluck('max_delivery_date', 'receive_id');
            //     foreach ($arrReceiveId as $rId) {
            //         if (isset($resDeli[$rId]) && !empty($resDeli[$rId])) {
            //             $arrUpdateO   = [];
            //             $shipmentDate = date('Y-m-d', strtotime($resDeli[$rId]));
            //             $curDate      = date('Y-m-d');
            //             if (strtotime($shipmentDate) < strtotime($curDate)) {
            //                 $shipmentDate = $curDate;
            //             }
            //             $arrUpdateO['shipment_date'] = $shipmentDate . ' 07:00:00' ;
            //             $arrUpdateO['up_ope_cd']     = 'OPE99999';
            //             $arrUpdateO['up_date']       = now();
            //             if ($shipmentDate === $curDate) {
            //                 $arrUpdateO['order_sub_status']  = ORDER_SUB_STATUS['DONE'];
            //                 $arrUpdateO['is_mail_sent']      = 0;
            //             }
            //             $modelO->updateData([$rId], $arrUpdateO);
            //         }
            //     }
            //     $message = "Step 7 update data to table dt_order_product_detail {$total} records";
            //     Log::info($message);
            //     print_r($message . PHP_EOL);
            // } else {
            //     Log::info('Step 7 have data but not update');
            //     print_r("Step 7 have data but not update" . PHP_EOL);
            // }
        } else {
            Log::info('Step 7 no data');
            print_r("Step 7 no data" . PHP_EOL);
        }
    }
}
