<?php
/**
 * Batch process get info from smaregi api
 *
 * @package    App\Console\Commands
 * @subpackage ProcessSmaregiAddProduct
 * @copyright  Copyright (c) 2018 RiverCrane! Corporation. All Rights Reserved.
 * @author     Lam Vinh<lam.vinh.rcvn2012@gmail.com>
 */

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;
use Validator;
use Swift_Mailer;
use Swift_SmtpTransport;
use Mail;
use App;
use Config;
use Event;
use App\Events\Command as eCommand;
use App\Models\Batches\MstStoreProduct;
use App\Notification;
use App\Notifications\SlackNotification;

class ProcessSmaregiAddProduct extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'process:smaregi-add-product';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Get info from smaregi api';

    /**
     * The error.
     *
     * @var string
     */
    public $error = [];

    /**
     * The folder.
     *
     * @var string
     */
    public $folder = null;

    /**
     * Size insert
     * @var int
     */
    private $sizeInsert = 1000;
    /**
     * Smaregi contract id
     * @var contract_id
     */
    public $contractId = '';

    /**
     * Smaregi access token
     * @var accessToken
     */
    public $accessToken = '';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        $this->contractId   = env('CONTRACT_ID');
        $this->accessToken  = env('ACCESS_TOKEN');
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $start          = microtime(true);
        $model          = new MstStoreProduct();
        print_r("Start batch process add new product to smaregi" . PHP_EOL);
        $this->processContent5();
        Event::fire(new eCommand($this->signature, array('end' => true, 'error' => $this->error)));
        $totalTime = round(microtime(true) - $start, 2);
        Log::info("End batch process add new product to smaregi: $totalTime s.");
        print_r("End batch process add new product to smaregi: $totalTime s." . PHP_EOL);
    }

    /**
     * Get products and insert
     *
     * @return  array
     */
    private function processContent5()
    {
        $model      = new MstStoreProduct();
        $requestParams['header'] = array(
            "Cache-Control: no-cache",
            "Content-Type: application/x-www-form-urlencoded",
            "X_contract_id: $this->contractId",
            "X_access_token: $this->accessToken",
        );
        $data  = $model->getDataSyncContent5IsUpdate($this->sizeInsert);
        if (count($data) > 0) {
            $tempUpdate = array_chunk($data->toArray(), 100);
            //echo count($tempUpdate);
            foreach ($tempUpdate as $items) {
                foreach ($items as $key => $item) {
                    $productName = $item['df_handling'] === 1 ? $item['store_product_name']
                        : '★' . $item['store_product_name'];
                    $products[] = array(
                        'productId'         => $item['store_product_id'],
                        'categoryId'        => $item['store_product_category_id'],
                        'productCode'       => $item['product_jan'],
                        'productName'       => $productName,
                        'price'             => $item['product_price'],
                        'cost'              => $item['product_cost'],
                        'size'              => $item['product_size'],
                        'color'             => trim($item['product_color']),
                        'tag'               => $item['product_tag'],
                        'url'               => $item['product_url'],
                        'displayFlag'       => 1,
                        'supplierProductNo' => $item['product_code'],
                    );
                    $updateProduct[] =  $item['product_code'];
                    if (App::environment(['local', 'test'])) {
                        $model->updateDataIn(
                            $updateProduct,
                            ['is_updated'   => 0]
                        );
                        continue;
                    }
                }
                $this->processApiAdd($products, $updateProduct, $requestParams, $model);
                //Process update product code

                $products = array();
                $updateProduct = array();
            }
        }
    }

    private function processApiAdd($products, $updateProduct, $requestParams, $model)
    {
        if (count($products) === 0) {
            return true;
        }
        $requestParams['fields'] = http_build_query(array(
            'proc_name' => 'product_upd',
            'params' => '{
                "proc_info": {"proc_division" : "U"},
                "data":[{
                    "table_name":"Product",
                    "rows": '.json_encode($products).'
                }]
            }'
        ));
        $response = $this->curlGet($requestParams);
        if (!empty($response) && isset($response->error)) {
            if ($response->error_code === 99) {
                preg_match_all("/([\d]+)/", $response->error_description, $match);
                if (!empty($match)) {
                    $storeId = $match[0][2];
                    $model->updateData(
                        ['store_product_id'   =>  $storeId],
                        ['is_updated' => 99]
                    );
                }
                return true;
            }
            if ($response->error_code === 41) {
                preg_match("/([\d]+)/", $response->error_description, $match);
                if (empty($match)) {
                    return true;
                } else {
                    $index = (int)$match[0] - 1;
                    $requestParams1['header'] = array(
                        "Cache-Control: no-cache",
                        "Content-Type: application/x-www-form-urlencoded",
                        "X_contract_id: $this->contractId",
                        "X_access_token: $this->accessToken",
                    );
                    $requestParams1['fields'] = http_build_query(array(
                        'proc_name' => 'product_ref',
                        'params' => '{
                            "conditions": [{"productCode":"'.$products[$index]['productCode'].'"}],
                            "table_name":"Product"
                        }'
                    ));
                    $response1 = $this->curlGet($requestParams1);
                    if (isset($response1->total_count)) {
                        if ((int)$response1->total_count > 0) {
                            $checkStore = $response1->result[0]->productId;
                            if ($updateProduct[$index] !== $response1->result[0]->supplierProductNo) {
                                $slack   = new Notification(CHANNEL['horunba']);
                                $slack->notify(new SlackNotification('Process add new for ' . $updateProduct[$index] .' error'));
                                $model->updateData(
                                    ['product_code'   => $updateProduct[$index]],
                                    ['is_updated' => 99]
                                );
                                return true;
                            }
                            if ($products[$index]['productId'] !== $checkStore) {
                                $products[$index]['productId'] = $checkStore;
                                $model->updateData(
                                    ['product_code'   => $updateProduct[$index], 'product_jan' => $products[$index]['productCode']],
                                    ['store_product_id' => $checkStore]
                                );
                            }
                        }
                        if ((int)$response1->total_count === 0) {
                            $model->updateData(
                                ['product_code'   => $updateProduct[$index]],
                                ['is_updated' => 99]
                            );
                            unset($products[$index]);
                            unset($updateProduct[$index]);
                        }
                    }
                    return $this->processApiAdd($products, $updateProduct, $requestParams, $model);
                }
            }
        } else {
            $model->updateDataIn(
                $updateProduct,
                ['is_updated'   => 0]
            );
            return true;
        }
    }
    private function curlGet($paramOptions)
    {
        $slack   = new Notification(CHANNEL['horunba']);
        $smaregiUrl     = "https://webapi.smaregi.jp/access/";
        $ch = curl_init($smaregiUrl);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $paramOptions['header']);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $paramOptions['fields']);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_MAXREDIRS, 10);
        curl_setopt($ch, CURLOPT_TIMEOUT, 30);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
        $response = curl_exec($ch);
        curl_close($ch);
        $response = json_decode($response);
        if (isset($response->error_code)) {
            $arrError = (array) $response;
            $err = implode("\n", $arrError);
            $error  = "------------------------------------------" . PHP_EOL;
            $error .= basename(__CLASS__) . PHP_EOL;
            $error .= $err;
            $error .= PHP_EOL . "------------------------------------------" . PHP_EOL;
            $slack->notify(new SlackNotification($error));
            Log::info($err);
        }
        return $response;
    }
}
