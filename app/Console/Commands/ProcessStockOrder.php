<?php
/**
 * Batch process Stock order
 *
 * @package    App\Console\Commands
 * @subpackage ProcessStockOrder
 * @copyright  Copyright (c) 2017 RiverCrane! Corporation. All Rights Reserved.
 * @author     Truong Nghia<truong.van.nghia@rivercrane.vn>
 */

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;
use App\Notifications\SlackNotification;
use App\Notification;
use App\Models\Batches\MstStockStatus;
use App\Models\Batches\MstProduct;
use App\Models\Batches\DtOrderToSupplier;
use App\Events\Command as eCommand;
use App\Custom\Utilities;
use Event;
use DB;

class ProcessStockOrder extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'process:stock-order';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Process data stock order and then insert order to edi.';

    /**
     * The limit of step process.
     *
     * @var int
     */
    protected $limit = 50000;

    /**
     * The error.
     *
     * @var array
     */
    public $error = [];

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //Create log
        $arrayReplace = [':', '-'];
        $folder       = str_replace($arrayReplace, '_', $this->signature);
        Log::useDailyFiles(storage_path() . "/logs/$folder/history.log");
        //Process data
        Event::fire(new eCommand($this->signature, array('start' => true)));
        Log::info('Start batch process stock order.');
        print_r("Start batch process stock order." . PHP_EOL);
        $startBatch = microtime(true);
        $slack      = new Notification(CHANNEL['horunba']);
        $modelSS    = new MstStockStatus();
        $modelSupplier = new DtOrderToSupplier();
        $modelP     = new MstProduct();
        $count = $modelSS->count();
        $start = 0;
        $upSucc = 0;
        $upFail = 0;
        $datas1 = $modelP->getDataStockOrder1();
        if (count($datas1) !== 0) {
            foreach ($datas1 as $data) {
                if ($data->stock_lower_limit > 0 &&
                    (($data->nanko_num - $data->order_zan_num - $data->return_num + $data->rep_orderring_num) < $data->stock_lower_limit)
                ) {
                    $replenishmentNum = $data->stock_default -
                        ($data->nanko_num - $data->order_zan_num - $data->return_num + $data->rep_orderring_num);
                    if ($replenishmentNum > 0) {
                        $arrUpdate = [];
                        $rodUnit = (int)$data->rod_unit;
                        if ($replenishmentNum < $rodUnit) {
                            $arrUpdate['replenish_num'] = $rodUnit;
                        } elseif ($rodUnit !== 0) {
                            if (($replenishmentNum%$rodUnit) > 0) {
                                $arrUpdate['replenish_num'] = (floor($replenishmentNum/$rodUnit) + 1) * $rodUnit;
                            } else {
                                $arrUpdate['replenish_num'] = ($replenishmentNum/$rodUnit) * $rodUnit;
                            }
                        }
                        try {
                            if (count($arrUpdate) !== 0) {
                                $modelSS->updateData($data->product_code, $arrUpdate);
                                $upSucc++;
                            }
                        } catch (\Exception $e) {
                            $this->error[] = Utilities::checkMessageException($e);
                            $error  = "------------------------------------------" . PHP_EOL;
                            $error .= basename(__CLASS__) . PHP_EOL;
                            $error .= Utilities::checkMessageException($e);
                            $error .= PHP_EOL . "------------------------------------------" . PHP_EOL;
                            $slack->notify(new SlackNotification($error));
                            Log::error(Utilities::checkMessageException($e));
                            print_r("$error");
                            $upFail++;
                        }
                    }
                }
            }
        }
        $message = "Update mst_stock_status success: $upSucc and fail: $upFail.";
        Log::info($message);
        print_r($message . PHP_EOL);

        $index         = 0;
        $yearMonth     = substr(date('Ym'), 3);
        $maxOrderCode  = $modelSupplier->getMaxOrderCodeByDate($yearMonth);
        if (count($maxOrderCode) > 0) {
            $orderCode = $maxOrderCode->toArray()['order_code'];
            if (strpos($orderCode, $yearMonth) === 0) {
                $index = (int)substr($orderCode, 3);
            }
        }
        $start2 = 0;
        $inSucc = 0;
        $inFail = 0;
        $datas2 = $modelP->getDataStockOrder2();
        if (count($datas2) !== 0) {
            foreach ($datas2 as $key => $data) {
                $index     += 1;
                $orderCode  = $yearMonth . sprintf("%06d", $index);
                $arrInsert  = [];
                $arrInsert['order_code']          = $orderCode;
                $arrInsert['order_date']          = now();
                $arrInsert['order_type']          = 3;
                $arrInsert['supplier_id']         = $data->price_supplier_id;
                $arrInsert['product_code']        = $data->product_code;
                $arrInsert['product_jan']         = $data->product_jan;
                $arrInsert['price_invoice']       = $data->price_invoice;
                $arrInsert['order_num']           = $data->replenish_num;
                $arrInsert['arrival_date_plan']   = null;
                $arrInsert['arrival_date']        = null;
                $arrInsert['arrival_quantity']    = 0;
                $arrInsert['incomplete_quantity'] = 0;
                $arrInsert['other']               = '';
                $arrInsert['remarks']             = '';
                $arrInsert['wait_days']           = 0;
                $arrInsert['process_status']      = 0;
                $arrInsert['receive_id']          = 0;
                $arrInsert['detail_line_num']     = 0;
                $arrInsert['sub_line_num']        = 0;
                $arrInsert['times_num']           = 0;
                $arrInsert['edi_order_code']      = $orderCode . $arrInsert['times_num'];
                $arrInsert['in_date']             = now();
                $arrInsert['up_date']             = now();
                try {
                    if ($data->price_supplier_id === 0) {
                        $error  = "------------------------------------------" . PHP_EOL;
                        $error .= basename(__CLASS__) . PHP_EOL;
                        $error .= "Order code $orderCode has supplier_id 0" . PHP_EOL;
                        $error .= PHP_EOL . "------------------------------------------" . PHP_EOL;
                        $slack->notify(new SlackNotification($error));
                    }
                    $modelSupplier->insert($arrInsert);
                    $replenishNum = $data->replenish_num;
                    $arrUpdate = [
                        'rep_orderring_num' => DB::raw("rep_orderring_num + {$replenishNum}"),
                        'replenish_num' => 0
                    ];
                    $modelSS->updateData($data->product_code, $arrUpdate);
                    $inSucc++;
                } catch (\Exception $e) {
                    $this->error[] = Utilities::checkMessageException($e);
                    $error  = "------------------------------------------" . PHP_EOL;
                    $error .= basename(__CLASS__) . PHP_EOL;
                    $error .= Utilities::checkMessageException($e);
                    $error .= PHP_EOL . "------------------------------------------" . PHP_EOL;
                    $slack->notify(new SlackNotification($error));
                    Log::error(Utilities::checkMessageException($e));
                    print_r("$error");
                    $inFail++;
                }
            }
        }
        Log::info("Insert to dt_order_to_supplier success: $inSucc and fail: $inFail records.");
        print_r("Insert to dt_order_to_supplier success: $inSucc and fail: $inFail records." . PHP_EOL);
        Event::fire(new eCommand($this->signature, array('end' => true, 'error' => $this->error)));
        $totalTime = round(microtime(true) - $startBatch, 2);
        Log::info("End batch process stock order with total time: $totalTime s.");
        print_r("End batch process stock order with total time: $totalTime s.");
    }
}
