<?php
/**
 * Batch process get info from smaregi api
 *
 * @package    App\Console\Commands
 * @subpackage ProcessSmaregiMasterSyncDbApi
 * @copyright  Copyright (c) 2018 RiverCrane! Corporation. All Rights Reserved.
 * @author     Truong Nghia<truong.van.nghia.rcvn2012@gmail.com>
 */

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\Batches\MstStoreProduct;
use App\Notifications\SlackNotification;
use Illuminate\Support\Facades\Log;
use App\Events\Command as eCommand;
use App\Notification;
use App;
use Event;

class ProcessSmaregiSyncProductApi extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'process:smaregi-sync-product-api {startDate=null} {endDate=null}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Get info from smaregi api';

    /**
     * The error.
     *
     * @var string
     */
    public $error = [];

    /**
     * The folder.
     *
     * @var string
     */
    public $folder = null;

    /**
     * Size insert
     * @var int
     */
    private $sizeInsert = 1000;
    /**
     * Smaregi contract id
     * @var contract_id
     */
    public $contractId = '';

    /**
     * Smaregi access token
     * @var accessToken
     */
    public $accessToken = '';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        $this->contractId   = env('CONTRACT_ID');
        $this->accessToken  = env('ACCESS_TOKEN');
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $start        = microtime(true);
        //Create log
        $arrayReplace = [':', '-'];
        $this->folder = str_replace($arrayReplace, '_', $this->signature);
        Log::useDailyFiles(storage_path() . "/logs/$this->folder/history.log");
        //Process data
        Event::fire(new eCommand($this->signature, array('start' => true)));
        Log::info('Start process smaregi master sync api');
        print_r("Start process smaregi master sync api" . PHP_EOL);
        $startDate = $this->argument('startDate');
        $endDate   = $this->argument('endDate');
        if (!preg_match("/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/", $startDate)) {
            Log::info('No start date or start date invalid.');
            print_r("No start date or start date invalid." . PHP_EOL);
        } else {
            if ($endDate !== "null" && (!preg_match("/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/", $endDate) ||
                strtotime($startDate) > strtotime($endDate))) {
                Log::info('End date invalid.');
                print_r("End date invalid." . PHP_EOL);
            } else {
                print_r("-------- Start process sync product --------" . PHP_EOL);
                $this->processContent($startDate, $endDate);
                print_r("-------- End process sync product --------" . PHP_EOL);
            }
        }
        Event::fire(new eCommand($this->signature, array('end' => true, 'error' => $this->error)));
        $totalTime = round(microtime(true) - $start, 2);
        Log::info("End batch process smaregi  api: $totalTime s.");
        print_r("End batch process smaregi  api: $totalTime s." . PHP_EOL);
    }

    /**
     * Get products from api smaregi insert to database
     * @param string $startDate
     * @param string $endDate
     * @return  array
     */
    private function processContent($startDate, $endDate)
    {
        $sum       = 0;
        $startDate = date("Y-m-d 00:00:00", strtotime($startDate));
        if ($endDate !== 'null') {
            $endDate = date("Y-m-d 23:59:59", strtotime($endDate));
        } else {
            $endDate = date("Y-m-d H:s:i");
        }
        $requestParams['header'] = array(
            "Cache-Control: no-cache",
            "Content-Type: application/x-www-form-urlencoded",
            "X_contract_id: $this->contractId",
            "X_access_token: $this->accessToken",
        );
        $requestParams['fields'] = http_build_query(array(
            'proc_name' => 'product_ref',
            'params' => '{
                "fields":["productId"],
                "conditions":[{"updDateTime >=":"'.$startDate.'"}, {"updDateTime <=":"'.$endDate.'"}],
                "table_name":"Product"
            }'
        ));
        $response = $this->curlGet($requestParams);
        $totalCount = '0';
        if (isset($response->total_count)) {
            $totalCount = $response->total_count;
        }
        if ($totalCount === '0') {
            return;
        }
        $perPage        = 1000;
        $totalPage = (int) ceil($totalCount / $perPage);
        for ($i=1; $i <= $totalPage; $i++) {
            $requestParams['fields'] = http_build_query(array(
                'proc_name' => 'product_ref',
                'params'    => '{
                    "conditions":[{"updDateTime >=":"'.$startDate.'"}, {"updDateTime <=":"'.$endDate.'"}],
                    "page" : '.$i.',
                    "table_name":"Product"
                }'
            ));
            $response = $this->curlGet($requestParams);

            if (!empty($response) && !isset($response->error_code)) {
                if (count($response->result) > 0) {
                    $data       =  (array)$response->result;
                    foreach ($data as $key => $value) {
                        $model = new MstStoreProduct();
                        $value = (array)$value;
                        $model = $model->find(trim($value['supplierProductNo']));
                        if (empty($model)) {
                            $model          = new MstStoreProduct();
                            $model->in_date = date('Y-m-d H:s:i');
                        }
                        $model->product_code              = trim($value['supplierProductNo']);
                        $model->store_product_id          = $value['productId'];
                        $model->df_handling               = 1;
                        $model->product_jan               = $value['productCode'];
                        $model->store_product_category_id = $value['categoryId'];
                        $model->store_product_name        = $value['productName'];
                        $model->product_tax_division      = $value['taxDivision'];
                        $model->product_price_division    = $value['productPriceDivision'];
                        $model->product_price             = (int)$value['price'];
                        $model->product_cost              = $value['cost'];
                        $model->product_size              = $value['size'];
                        $model->product_color             = $value[ 'color'];
                        $model->product_tag               = $value['tag'];
                        $model->product_group_code        = $value['groupCode'];
                        $model->product_url               = $value['url'];
                        $model->stock_control_division    = $value['stockControlDivision'];
                        $model->display_flag              = $value['displayFlag'];
                        $model->point_not_applicable      = $value['pointNotApplicable'];
                        $model->is_updated                = 0;
                        $model->product_jan_new           = null;
                        $model->up_date                   = date('Y-m-d H:s:i');
                        $model->save();
                        $sum++;
                    }
                }
            }
        }
        Log::info("Process success : " . $sum . " records");
        print_r("Process success : " . $sum . " records" . PHP_EOL);
    }

    /**
     * Request process api
     *
     * @param  array $paramOptions
     * @return json encode
     */
    private function curlGet($paramOptions)
    {
        $slack      = new Notification(CHANNEL['horunba']);
        $smaregiUrl = "https://webapi.smaregi.jp/access/";
        $ch         = curl_init($smaregiUrl);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $paramOptions['header']);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $paramOptions['fields']);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_MAXREDIRS, 10);
        curl_setopt($ch, CURLOPT_TIMEOUT, 30);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
        $response = curl_exec($ch);
        curl_close($ch);
        $response = json_decode($response);
        if (isset($response->error_code)) {
            $arrError = (array) $response;
            $err      = implode("\n", $arrError);
            $error    = "------------------------------------------" . PHP_EOL;
            $error   .= basename(__CLASS__) . PHP_EOL;
            $error   .= $err;
            $error   .= PHP_EOL . "------------------------------------------" . PHP_EOL;
            $slack->notify(new SlackNotification($error));
            Log::info($err);
        }
        return $response;
    }
}
