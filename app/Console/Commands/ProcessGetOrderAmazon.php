<?php
/**
 * Batch process amazon order
 *
 * @package    App\Console\Commands
 * @subpackage ProcessOrderAmazon
 * @copyright  Copyright (c) 2018 RiverCrane! Corporation. All Rights Reserved.
 * @author     Le Hung<le.hung.rcvn2012@gmail.com>
 */

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;
use App\Models\Batches\TAmazonOrder;
use App\Events\Command as eCommand;
use App\Http\Controllers\Common;
use Event;
use Config;
use File;
use Storage;

class ProcessGetOrderAmazon extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'process:get-order-amazon';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Read csv in horunba/storage/csv/amazon path then import to t_amazon_order';

    /**
     * The error.
     *
     * @var array
     */
    public $error = [];

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //Create log
        $arrayReplace = [':', '-'];
        $folder       = str_replace($arrayReplace, '_', $this->signature);
        Log::useDailyFiles(storage_path() . "/logs/$folder/history.log");
        //Process data
        Event::fire(new eCommand($this->signature, array('start' => true)));
        Log::info('Start process Amazon Acquire Orders batch');
        print_r("Start process Amazon Acquire Orders batch import to t_amazon_order." . PHP_EOL);
        $start      = microtime(true);
        $this->processBatch();
        $totalTime = round(microtime(true) - $start, 2);
        Log::info("End Amazon Acquire Orders batch process with total time: $totalTime s.");
        print_r("End Amazon Acquire Orders batch process with total time: $totalTime s.");
        Event::fire(new eCommand($this->signature, array('end' => true, 'error' => $this->error)));
    }
    /**
     * Process csv then insert and write log
     *
     * @return
     */
    public function processBatch()
    {
        $common         = new Common;
        $model          = new TAmazonOrder();
        $files          = File::allFiles(storage_path('csv/amazon'));
        if (count($files) === 0) {
            print_r("File is not found" . PHP_EOL);
            Log::info("CSV Files process for $this->signature was not found");
            return;
        }
        foreach ($files as $file) {
            $fileName       = last((array)$file);
            $filePath       = storage_path('csv/amazon/' . $fileName);
            $filePathBak    = storage_path('csv_bak/amazon/' . $fileName);
            $ext = pathinfo($fileName, PATHINFO_EXTENSION);
            if ($ext === 'tab' || $ext === 'csv' || $ext === 'txt') {
                print_r("File $fileName is processing". PHP_EOL);
            } else {
                print_r("File $fileName is not csv tab format" . PHP_EOL);
                Log::info("File $fileName is not csv tab format");
                continue;
            }
            $totalColumns   = count(Config::get('common.csv_acquire_order_amazon'));
            $arrCol         = Config::get('common.csv_acquire_order_amazon');
            $arrCorrect     = array();
            $arrError       = array();
            $arrTemp        = array();
            if (file_exists($filePath)) {
                $i = 0;
                if (($handle = fopen("$filePath", "r")) !== false) {
                    while (($column = fgetcsv($handle, 0, "\t")) !== false) {
                        if ($i === 0) {
                            $i++;
                            continue;
                        }
                        if (count($column) !== $totalColumns) {
                            print_r("Row $i was be errored.". PHP_EOL);
                            Log::info("Row $i was be errored.");
                            continue;
                        }
                        $column = implode(';.;', $column);
                        $column = mb_convert_encoding($column, 'UTF-8', 'Shift-JIS');
                        $column = explode(';.;', $column);
                        $column = array_map('trim', $column);
                        $orderNum       = $column[0];
                        $productCode    = $column[1];
                        $dataWhere = ['order_id' => $orderNum, 'order_item_id' => $productCode];
                        $dataCheck = $model->checkExists($dataWhere);
                        if (empty($dataCheck)) {
                            $i++;
                            if (count($arrCol) !== count($column)) {
                                print_r("Row $i was be errored.". PHP_EOL);
                                Log::info("Row $i was be errored.");
                                continue;
                            }
                            $column                     = array_combine($arrCol, $column);
                            if (strtotime($column['purchase_date']) < strtotime('2018/06/30')) {
                                continue;
                            }
                            $column['file']             = $fileName;
                            $column['process_flg']      = 0;
                            $column['is_delete']        = 0;
                            $column['created_at']       = date('Y-m-d H:m:i');
                            $arrCorrect[] = $column;
                        }
                    }
                    if (!empty($arrCorrect)) {
                        $model->insert($arrCorrect);
                    }
                    $arrCorrect = null;
                }
                fclose($handle);
            }
            File::move($filePath, $filePathBak);
            print_r("--------------------------------------------------------------------" . PHP_EOL);
            print_r("File $fileName processed with total inserted: $i items." . PHP_EOL);
            print_r("--------------------------------------------------------------------" . PHP_EOL);
            Log::info("File $fileName processed with total inserted: $i items.");
        }
    }
}
