<?php
/**
 * Batch process order supplier EDI result
 *
 * @package    App\Console\Commands
 * @subpackage ProcessEdiResult
 * @copyright  Copyright (c) 2017 RiverCrane! Corporation. All Rights Reserved.
 * @author     Truong Nghia<truong.van.nghia@rivercrane.vn>
 */

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;
use App\Models\Batches\TOrder;
use App\Models\Batches\DtOrderProductDetail;
use App\Models\Batches\DtOrderToSupplier;
use App\Models\Batches\TOrderDetail;
use App\Models\Batches\TOrderDirect;
use App\Models\Batches\MstOrder;
use App\Models\Batches\MstStatusBatch;
use App\Models\Batches\DtDelivery;
use App\Models\Batches\DtReceiptLedger;
use App\Models\Batches\MstStockStatus;
use App\Models\Batches\TRequestPerformanceModelEdi;
use App\Notifications\SlackNotification;
use App\Notification;
use App\Custom\Utilities;
use App\Events\Command as eCommand;
use Event;
use DB;

class ProcessEdiResult extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'process:edi-result';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Process order result from EDI';

    /**
     * The notification.
     *
     * @var object
     */
    protected $slack;

    /**
     * The error.
     *
     * @var array
     */
    public $error = [];

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //Create log
        $arrayReplace = [':', '-'];
        $folder       = str_replace($arrayReplace, '_', $this->signature);
        Log::useDailyFiles(storage_path() . "/logs/$folder/history.log");
        //Check cron
        $modelStatus = new MstStatusBatch;
        $check = $modelStatus->getDataBySignature($this->signature);
        $this->slack = new Notification(CHANNEL['horunba']);
        if ($check->status_flag === 1) {
            Log::info('This batch running->Stop this batch');
            print_r("This batch running->Stop this batch" . PHP_EOL);
            $error  = "------------------------------------------" . PHP_EOL;
            $error .= basename(__CLASS__) . PHP_EOL;
            $error .= 'This batch running->Stop this batch' . PHP_EOL;
            $error .= PHP_EOL . "------------------------------------------" . PHP_EOL;
            $this->slack->notify(new SlackNotification($error));
            return;
        }
        //Process data
        Event::fire(new eCommand($this->signature, array('start' => true)));
        Log::info('Start batch process EDI result');
        print_r("Start batch process EDI result." . PHP_EOL);
        $start       = microtime(true);
        Log::info('=== Start batch process order to supplier EDI ===');
        print_r("=== Start batch process order to supplier EDI ===" . PHP_EOL);
        $this->processEdi();
        Log::info("=== End batch process order to supplier EDI ===");
        print_r("=== End batch process order to supplier EDI ===" . PHP_EOL);
        Log::info('=== Start batch process EDI result (direct delivery) ===');
        print_r("=== Start batch process EDI result (direct delivery) ===" . PHP_EOL);
        $this->processEdiDirect();
        Log::info("=== End batch process EDI result (direct delivery) ===");
        print_r("=== End batch process EDI result (direct delivery) ===" . PHP_EOL);
        Log::info('=== Start batch process EDI result ===');
        print_r("=== Start batch process EDI result ===" . PHP_EOL);
        $this->processOrderResult();
        Log::info("=== End batch process EDI result ===");
        print_r("=== End batch process EDI result ===" . PHP_EOL);
        Event::fire(new eCommand($this->signature, array('end' => true, 'error' => $this->error)));
        $totalTime = round(microtime(true) - $start, 2);
        Log::info("End batch process EDI result with total time: $totalTime s .");
        print_r("End batch process EDI result with total time: $totalTime s." . PHP_EOL);
    }

    /**
    * Process order to supplier EDI
    * @return void
    */
    public function processEdi()
    {
        $modelOrder  = new TOrder();
        $modelOPD    = new DtOrderProductDetail();
        $modelOTS    = new DtOrderToSupplier();
        $modelTOrder = new TOrderDetail();
        $modelO      = new MstOrder();
        $modelRL     = new DtReceiptLedger();
        $modelSS     = new MstStockStatus();
        $modelTRP    = new TRequestPerformanceModelEdi();
        $datas       = $modelOrder->getDataEdiResult();
        $upOPDSuc    = 0;
        $upOPDFail   = 0;
        $upOTSSuc    = 0;
        $upOTSFail   = 0;
        $message = '---- Start process  content 1 ----';
        Log::info($message);
        print_r($message . PHP_EOL);
        if (count($datas) === 0) {
            Log::info('No data');
            print_r("No data" . PHP_EOL);
        }
        $arrUpSS = [];
        foreach ($datas as $data) {
            $arrUpdate    = [];
            $arrUpdateSup = [];
            $itemUpSS = [
                'order_code'   => $data->order_code,
                'product_code' => $data->product_code
            ];
			$deliveryDate = '';
            switch ($data->m_order_type_id) {
                case 1:
                    if ($data->product_status !== PRODUCT_STATUS['CANCEL']) {
                        $arrUpdate['product_status'] = PRODUCT_STATUS['COMING'];
                        $cal = date(
                            'Y-m-d',
                            strtotime($data->ship_wish_date . '-' . $data->deli_days . ' days')
                        );
                        //$deliveryDate = '';
                        if (in_array($data->suplier_id, [32, 62, 138, 41])) {
                            $arrivalDatePlus = date('Y-m-d', strtotime($data->arrival_date));
                            if (!empty($data->ship_wish_date)) {
                                if (!empty($data->arrival_date)
                                    && strtotime($cal) < strtotime($arrivalDatePlus)
                                ) {
                                    $shopAnswer = '▼お届け日のご希望をお伺いしておりましたが、'.
                                    '本メールに記載しております発送日が最短でございます。'.
                                    '恐れ入りますが商品到着までもうしばらくお待ちくださいませ。';
                                    $temUpdate = [
                                        'shop_answer' => $shopAnswer
                                    ];
                                    /*if (!empty($data->delivery_date)
                                        && strtotime($data->delivery_date) < strtotime($arrivalDatePlus)) {
                                        $temUpdate['is_send_mail_urgent'] = 1;
                                        $temUpdate['urgent_mail_id']      = 80;
                                    }*/
                                    $deliveryDate = $arrivalDatePlus;
                                    $modelO->updateData([$data->receive_id], $temUpdate);
                                } else {
                                    $deliveryDate = $cal;
                                    /*if (!empty($data->delivery_date)
                                        && strtotime($data->delivery_date) < strtotime($cal)) {
                                        $temUpdate = [];
                                        $temUpdate['is_send_mail_urgent'] = 1;
                                        $temUpdate['urgent_mail_id']      = 80;
                                        $modelO->updateData([$data->receive_id], $temUpdate);
                                    }*/
                                }
                            } else {
                                if (!empty($data->arrival_date)) {
                                    $deliveryDate = $arrivalDatePlus;
                                } else {
                                    $deliveryDate = date('Y-m-d');
                                }
                                /*if (!empty($data->delivery_date)
                                    && strtotime($data->delivery_date) < strtotime($arrivalDatePlus)) {
                                    $temUpdate = [];
                                    $temUpdate['is_send_mail_urgent'] = 1;
                                    $temUpdate['urgent_mail_id']      = 80;
                                    $modelO->updateData([$data->receive_id], $temUpdate);
                                }*/
                            }
                        } elseif (in_array($data->suplier_id, [ 367,282,395,171])) {     //アサヒペンの土曜にクロスドックしない(367 , 282, 395, 171)
                            $arrivalDatePlus = date('Y-m-d', strtotime($data->arrival_date));
                            $calday = date('w', strtotime($data->arrival_date));
                            if ($calday === '6') {
                                $arrivalDatePlus = date('Y-m-d', strtotime($data->arrival_date . '+1 days'));
                            }
                            if (!empty($data->ship_wish_date)) {
                                if (!empty($data->arrival_date)
                                    && strtotime($cal) < strtotime($arrivalDatePlus)
                                ) {
                                    $shopAnswer = '▼お届け日のご希望をお伺いしておりましたが、'.
                                    '本メールに記載しております発送日が最短でございます。'.
                                    '恐れ入りますが商品到着までもうしばらくお待ちくださいませ。';
                                    $temUpdate = [
                                        'shop_answer' => $shopAnswer
                                    ];
                                    /*if (!empty($data->delivery_date)
                                        && strtotime($data->delivery_date) < strtotime($arrivalDatePlus)) {
                                        $temUpdate['is_send_mail_urgent'] = 1;
                                        $temUpdate['urgent_mail_id']      = 80;
                                    }*/
                                    $deliveryDate = $arrivalDatePlus;
                                    $modelO->updateData([$data->receive_id], $temUpdate);
                                } else {
                                    $deliveryDate = $cal;
                                    /*if (!empty($data->delivery_date)
                                        && strtotime($data->delivery_date) < strtotime($cal)) {
                                        $temUpdate = [];
                                        $temUpdate['is_send_mail_urgent'] = 1;
                                        $temUpdate['urgent_mail_id']      = 80;
                                        $modelO->updateData([$data->receive_id], $temUpdate);
                                    }*/
                                }
                            } else {
                                if (!empty($data->arrival_date)) {
                                    $deliveryDate = $arrivalDatePlus;
                                } else {
                                    $deliveryDate = date('Y-m-d');
                                }
                                /*if (!empty($data->delivery_date)
                                    && strtotime($data->delivery_date) < strtotime($arrivalDatePlus)) {
                                    $temUpdate = [];
                                    $temUpdate['is_send_mail_urgent'] = 1;
                                    $temUpdate['urgent_mail_id']      = 80;
                                    $modelO->updateData([$data->receive_id], $temUpdate);
                                }*/
                            }
                        } else {
                            $arrivalDatePlus = date('Y-m-d', strtotime($data->arrival_date . '+1 days'));
                            if (!empty($data->ship_wish_date)) {
                                if (!empty($data->arrival_date)
                                    && strtotime($cal) < strtotime($arrivalDatePlus)
                                ) {
                                    $deliveryDate = $arrivalDatePlus;
                                    $shopAnswer = '▼お届け日のご希望をお伺いしておりましたが、'.
                                    '本メールに記載しております発送日が最短でございます。'.
                                    '恐れ入りますが商品到着までもうしばらくお待ちくださいませ。';
                                    $temUpdate = [
                                        'shop_answer' => $shopAnswer
                                    ];
                                    /*if (!empty($data->delivery_date)
                                        && strtotime($data->delivery_date) < strtotime($arrivalDatePlus)) {
                                        $temUpdate['is_send_mail_urgent'] = 1;
                                        $temUpdate['urgent_mail_id']      = 80;
                                    }*/
                                    $modelO->updateData([$data->receive_id], $temUpdate);
                                } else {
                                    $deliveryDate = $cal;
                                }
                            } else {
                                if (!empty($data->arrival_date)) {
                                    $deliveryDate = $arrivalDatePlus;
                                } else {
                                    $deliveryDate = date('Y-m-d');
                                }
                                /*if (!empty($data->delivery_date)
                                    && strtotime($data->delivery_date) < strtotime($arrivalDatePlus)) {
                                    $temUpdate = [];
                                    $temUpdate['is_send_mail_urgent'] = 1;
                                    $temUpdate['urgent_mail_id']      = 80;
                                    $modelO->updateData([$data->receive_id], $temUpdate);
                                }*/
                            }
                        }
                        if (empty($data->delivery_date)) {
                            $arrUpdate['delivery_date'] = $deliveryDate;
                        }
                    }
                    $arrUpdateSup['process_status']      = 5;
                    $arrUpdateSup['arrival_date']        = $data->arrival_date;
                    $arrUpdateSup['edi_delivery_date']   = $data->shipment_date_plan;
                    $arrUpdateSup['arrival_date_plan']   = $data->shipment_date;
                    $arrUpdateSup['arrival_quantity']    = $data->stock_quantity;
                    $arrUpdateSup['incomplete_quantity'] = $data->shortage_quantity;
					$arrUpdateSup['estimate_delivery_date'] = $deliveryDate;
                    break;
                case 3:
                    if ($data->product_status !== PRODUCT_STATUS['CANCEL']) {
                        $arrUpdate['product_status'] = PRODUCT_STATUS['OUT_OF_STOCK'];
                    }
                    $arrUpdateSup['process_status'] = $data->m_order_type_id;
                    if (!$this->checkInArray($itemUpSS, $arrUpSS)) {
                        if (empty($data->delivery_type) || $data->delivery_type === 1) {
                            $modelSS->where('product_code', $data->product_code)->decrement(
                                'rep_orderring_num',
                                (int)$data->order_num
                            );
                            $arrUpSS[] = $itemUpSS;
                        }
                    }
                    break;
                case 4:
                    if ($data->product_status !== PRODUCT_STATUS['CANCEL']) {
                        $arrUpdate['product_status'] = PRODUCT_STATUS['SALE_STOP'];
                    }
                    $arrUpdateSup['process_status'] = $data->m_order_type_id;
                    if (!$this->checkInArray($itemUpSS, $arrUpSS)) {
                        if (empty($data->delivery_type) || $data->delivery_type === 1) {
                            $modelSS->where('product_code', $data->product_code)->decrement(
                                'rep_orderring_num',
                                (int)$data->order_num
                            );
                            $arrUpSS[] = $itemUpSS;
                        }
                    }
                    break;
                case 9:
					$deliveryDate = null;
                    if ($data->product_status !== PRODUCT_STATUS['CANCEL']) {
                        //$deliveryDate = null;
                        if ($data->suplier_id === 41 && $data->arrive_type === 2) {
                            $tempDeliveryDate = $data->shipment_date_plan;
                            if (!empty($data->ship_wish_date)) {
                                $shipWishCal = date(
                                    'Y-m-d',
                                    strtotime($data->ship_wish_date . '-' . $data->deli_days . ' days')
                                );
                                if (strtotime($shipWishCal) < strtotime($tempDeliveryDate)) {
                                    $deliveryDate = $tempDeliveryDate;
                                    $shopAnswer = '▼お届け日のご希望をお伺いしておりましたが、'.
                                    '本メールに記載しております発送日が最短でございます。'.
                                    '恐れ入りますが商品到着までもうしばらくお待ちくださいませ。';
                                    $modelO->updateData([$data->receive_id], ['shop_answer' => $shopAnswer]);
                                } else {
                                    $deliveryDate = $shipWishCal;
                                }
                            } else {
                                $deliveryDate = $tempDeliveryDate;
                            }
                        } else {
                            $cal = date('w', strtotime($data->shipment_date_plan . '+' . $data->delivery_days . 'days'));
                            $tempDeliveryDate = null;
                            $plus = 0;
                            if ($cal === '6') {   //アサヒペンの土曜にクロスドックしない (367 , 282, 395, 171)
                                if (in_array($data->suplier_id, [32, 62, 138, 41])) {
                                    $plus = $data->delivery_days + 2;
                                } else {
                                    $plus = $data->delivery_days + 3;
                                }
                                $tempDeliveryDate = date(
                                    'Y-m-d',
                                    strtotime($data->shipment_date_plan . '+' . $plus . 'days')
                                );
                            } elseif ($cal === '0') {
                                if (in_array($data->suplier_id, [32, 62, 138, 367 , 282, 395, 171, 41])) {
                                    $plus = $data->delivery_days + 2;
                                } else {
                                    $plus = $data->delivery_days + 3;
                                }
                                $tempDeliveryDate = date(
                                    'Y-m-d',
                                    strtotime($data->shipment_date_plan . '+' . $plus . 'days')
                                );
                            } else {
                                if (in_array($data->suplier_id, [32, 62, 138, 367 , 282, 395, 171, 41])) {
                                    $plus = $data->delivery_days;
                                } else {
                                    $plus = $data->delivery_days + 1;
                                }
                                $tempDeliveryDate = date('Y-m-d', strtotime($data->shipment_date_plan . '+' . $plus . 'days'));
                            }
                            if (!empty($data->ship_wish_date)) {
                                $shipWishPlus = date(
                                    'Y-m-d',
                                    strtotime($data->ship_wish_date . '-' . $data->deli_days . ' days')
                                );
                                if (strtotime($shipWishPlus) < strtotime($tempDeliveryDate)) {
                                    $deliveryDate = $tempDeliveryDate;
                                    $shopAnswer = '▼お届け日のご希望をお伺いしておりましたが、'.
                                    '本メールに記載しております発送日が最短でございます。'.
                                    '恐れ入りますが商品到着までもうしばらくお待ちくださいませ。';
                                    $modelO->updateData([$data->receive_id], ['shop_answer' => $shopAnswer]);
                                } else {
                                    $deliveryDate = $shipWishPlus;
                                }
                            } else {
                                $deliveryDate = $tempDeliveryDate;
                            }
                        }
                        $arrUpdate['product_status']    = PRODUCT_STATUS['WAIT_TO_ARRIVAL'];
                        $arrUpdate['delivery_date']     = $deliveryDate;
                        $arrUpdate['arrival_date_plan'] = $data->shipment_date_plan;
                    }
                    $arrUpdateSup['process_status']    = 2;
                    $arrUpdateSup['edi_delivery_date'] = $data->shipment_date_plan;
                    $arrUpdateSup['arrival_date_plan'] = $data->shipment_date_plan;
                    $arrUpdateSup['estimate_delivery_date'] = $deliveryDate;
                    break;
                default:
                    break;
            }
            if (count($arrUpdate) !== 0) {
                if ($data->receive_id === 0) {
                    $modelOPD->updateDataByOrderCode(
                        [$data->order_code],
                        $arrUpdate
                    );
                } elseif (!empty($data->receive_id)) {
                    $modelOPD->updateData(
                        $data->receive_id,
                        $data->detail_line_num,
                        $data->sub_line_num,
                        $arrUpdate
                    );
                }
                $upOPDSuc++;
            }

            $arrUpdateSup['order_note'] = $data->other;
            $resOTS = $modelOTS->updateData($data->order_code, $arrUpdateSup, $data->edi_order_code);
            if (strpos($resOTS, '------') !== false) {
                if (!empty(explode(PHP_EOL, $resOTS)[2])) {
                    $this->error[] = explode(PHP_EOL, $resOTS)[2];
                }
                $this->slack->notify(new SlackNotification($resOTS));
                Log::error($resOTS);
                print_r("$resOTS");
                $upOTSFail++;
                continue;
            }
            $upOTSSuc++;
            $modelTOrder->where('t_order_detail_id', $data->t_order_detail_id)->update(['filmaker_flg' => 2]);
        }
        Log::info("Update table dt_order_product_detail success: $upOPDSuc fail: $upOPDFail record");
        print_r("Update table dt_order_product_detail success: $upOPDSuc fail: $upOPDFail record" . PHP_EOL);
        Log::info("Update table dt_order_to_supplier success: $upOTSSuc fail: $upOTSFail record");
        print_r("Update table dt_order_to_supplier success: $upOTSSuc fail: $upOTSFail record" . PHP_EOL);
        //Start copy from batch divide-delivery(FROM 4. Inventory reservation)
        $message = "Start process inventory reservation content 6";
        Log::info("---- $message ----");
        print_r("---- $message ----" . PHP_EOL);
        $dataInventory3 = $modelOPD->getDataProcessInventory3();
        $successInv3 = 0;
        if ($dataInventory3->count() === 0) {
            Log::info('No data process');
            print_r("No data process" . PHP_EOL);
        } else {
            $previousProductCode = '';
            $waitDelivery        = 0;
            foreach ($dataInventory3 as $invent3) {
                $arrUp3 = [];
                if ($invent3->product_code !== $previousProductCode) {
                    if ($waitDelivery !== 0 && $previousProductCode !== '') {
                        $modelSS->updateData(
                            $previousProductCode,
                            [
                                'wait_delivery_num' => $waitDelivery,
                            ]
                        );
                    }
                    $waitDelivery        = $invent3->wait_delivery_num;
                    $previousProductCode = $invent3->product_code;
                }
                if (($invent3->nanko_num - $waitDelivery) >= $invent3->order_num) {
                    $arrUp3['product_status'] = PRODUCT_STATUS['WAIT_TO_SHIP'];
                    $arrUp3['delivery_date']  = $modelO->calculateDeliveryDate($invent3);
                    $modelOPD->updateData(
                        $invent3->receive_id,
                        $invent3->detail_line_num,
                        $invent3->sub_line_num,
                        $arrUp3
                    );
                    $waitDelivery += $invent3->order_num;
                    $successInv3++;
                } elseif ($invent3->cheetah_status === 4) {
                    $modelOPD->updateData(
                        $invent3->receive_id,
                        $invent3->detail_line_num,
                        $invent3->sub_line_num,
                        ['product_status' => PRODUCT_STATUS['SALE_STOP']]
                    );
                    $modelO->updateData(
                        [$invent3->receive_id],
                        ['order_sub_status' => ORDER_SUB_STATUS['STOP_SALE']]
                    );
                    $successInv3++;
                } elseif ($invent3->cheetah_status === 3) {
                    $modelOPD->updateData(
                        $invent3->receive_id,
                        $invent3->detail_line_num,
                        $invent3->sub_line_num,
                        ['product_status' => PRODUCT_STATUS['OUT_OF_STOCK']]
                    );
                    $modelO->updateData(
                        [$invent3->receive_id],
                        ['order_sub_status' => ORDER_SUB_STATUS['OUT_OF_STOCK']]
                    );
                    $successInv3++;
                }
            }
            if ($waitDelivery !== 0 && $previousProductCode !== '') {
                $modelSS->updateData(
                    $previousProductCode,
                    [
                        'wait_delivery_num' => $waitDelivery,
                    ]
                );
            }
        }
        $message = "End process inventory reservation content 6 and".
                       " Update success: $successInv3 record.";
        Log::info("---- $message ----");
        print_r("---- $message ----" . PHP_EOL);
        //End copy from batch divide-delivery(FROM 4. Inventory reservation)
        /*
        $datas2 = $modelO->getDataAfterEdiResult();
        $total = count($datas2);
        if ($total === 0) {
            $message = "Content 3 no data.";
            Log::info($message);
            print_r($message . PHP_EOL);
        } else {
            $arrUpdate = [
                'order_sub_status' => ORDER_SUB_STATUS['DOING'],
                'is_mail_sent'     => 0
            ];
            $arrKey = [];
            $datas2 = $datas2->groupBy('receive_id')->toArray();
            $arrCheck = [1, 7, 8, 9, 15, 17];
            foreach ($datas2 as $key => $value) {
                $temp = array_column($value, 'product_status');
                $arrCompare = array_diff($temp, $arrCheck);
                if (count($arrCompare) === 0) {
                    $arrKey[] = $key;
                }
            }
            $success = count($arrKey);
            if ($success !== 0) {
                $modelO->updateData($arrKey, $arrUpdate);
            }
            $message = "Content 3 process: $total records and update success: $success records.";
            Log::info($message);
            print_r($message . PHP_EOL);
        }*/
    }

    /**
     * Check in array
     * @param  array $needle
     * @param  array $haystack
     * @return boolean
     */
    private function checkInArray($needle, $haystack)
    {
        foreach ($haystack as $item) {
            if ($item['order_code'] === $needle['order_code'] && $item['product_code'] === $needle['product_code']) {
                return true;
            }
        }
        return false;
    }

    /**
    * Process order to supplier EDI direct
    * @return void
    */
    public function processEdiDirect()
    {
        $modelOD  = new TOrderDirect();
        $modelOPD = new DtOrderProductDetail();
        $modelOTS = new DtOrderToSupplier();
        $modelO   = new MstOrder();
        $datas    = $modelOD->getDataDirect();
        if (count($datas) === 0) {
            Log::info('No data');
            print_r("No data" . PHP_EOL);
        }
        $arrCode     = [];
        $upOPDSuc    = 0;
        $upOPDFail   = 0;
        $upOTSSuc    = 0;
        $upOTSFail   = 0;
        foreach ($datas as $data) {
            $arrCode[]      = $data->order_code;
            $arrUpdate      = [];
            $arrUpdateSup   = [];
            if (empty($data->shipment_date)) {
                $shipmentDate = date('Y-m-d');
            } else {
                $shipmentDate = date('Y-m-d', strtotime($data->shipment_date));
            }
            switch ($data->m_order_type_id) {
                case 1:
                    if ($data->product_status !== PRODUCT_STATUS['CANCEL']) {
                        $arrUpdate['product_status']    = PRODUCT_STATUS['SHIPPED'];
                        $arrUpdate['delivery_date']     = $shipmentDate;
                    }
                    $arrUpdateSup['edi_delivery_date'] = $data->shipment_date_plan;
                    $arrUpdateSup['arrival_date_plan'] = $shipmentDate;
                    $arrUpdateSup['arrival_date']      = $shipmentDate;
                    $arrUpdateSup['process_status'] = 5;
                    break;
                case 3:
                    if ($data->product_status !== PRODUCT_STATUS['CANCEL']) {
                        $arrUpdate['product_status'] = PRODUCT_STATUS['OUT_OF_STOCK'];
                    }
                    $arrUpdateSup['process_status'] = $data->m_order_type_id;
                    break;
                case 4:
                    if ($data->product_status !== PRODUCT_STATUS['CANCEL']) {
                        $arrUpdate['product_status'] = PRODUCT_STATUS['SALE_STOP'];
                    }
                    $arrUpdateSup['process_status'] = $data->m_order_type_id;
                    break;
                case 9:
                    if ($data->product_status !== PRODUCT_STATUS['CANCEL']) {
                        $arrUpdate['product_status'] = PRODUCT_STATUS['DIRECTLY_SHIPPING'];
                        $arrUpdate['delivery_date']  = date('Y-m-d', strtotime($data->shipment_date_plan));
                    }
                    $arrUpdateSup['process_status']    = 2;
                    $arrUpdateSup['edi_delivery_date'] = $data->shipment_date_plan;
                    //$arrUpdateSup['arrival_date_plan'] = $data->shipment_date_plan;
                    break;
                default:
                    break;
            }
            if (count($arrUpdate) !== 0) {
                if ($data->receive_id === 0) {
                    $modelOPD->updateDataByOrderCode(
                        [$data->order_code],
                        $arrUpdate
                    );
                } elseif (!empty($data->receive_id)) {
                    $modelOPD->updateData(
                        $data->receive_id,
                        $data->detail_line_num,
                        $data->sub_line_num,
                        $arrUpdate
                    );
                }
                $upOPDSuc++;
            }

            $arrUpdateSup['order_note'] = $data->other;
            $resOTS = $modelOTS->updateData($data->order_code, $arrUpdateSup, $data->edi_order_code);
            if (strpos($resOTS, '------') !== false) {
                if (!empty(explode(PHP_EOL, $resOTS)[2])) {
                    $this->error[] = explode(PHP_EOL, $resOTS)[2];
                }
                $this->slack->notify(new SlackNotification($resOTS));
                Log::error($resOTS);
                print_r("$resOTS");
                $upOTSFail++;
                continue;
            }
            $modelOD->where('order_code', $data->edi_order_code)->update(['filmaker_flg' => 2]);
            $upOTSSuc++;
        }
        Log::info("Update table dt_order_product_detail success: $upOPDSuc fail: $upOPDFail record");
        print_r("Update table dt_order_product_detail success: $upOPDSuc fail: $upOPDFail record" . PHP_EOL);
        Log::info("Update table dt_order_to_supplier success: $upOTSSuc fail: $upOTSFail record");
        print_r("Update table dt_order_to_supplier success: $upOTSSuc fail: $upOTSFail record" . PHP_EOL);


        $datas2 = $modelO->getDataAfterEdiResult();
        $total = count($datas2);
        if ($total === 0) {
            $message = "Content 2 no data.";
            Log::info($message);
            print_r($message . PHP_EOL);
        } else {
            $arrKey = [];
            $datas2 = $datas2->groupBy('receive_id')->toArray();
            $arrCheck = [1, 7, 8, 9, 15, 17];
            foreach ($datas2 as $key => $value) {
                $temp = array_column($value, 'product_status');
                $arrCompare = array_diff($temp, $arrCheck);
                if (count($arrCompare) === 0) {
                    $arrKey[] = $key;
                }
            }
            $dataDeliveryDate = $modelOPD->getMaxDeliveryDate($arrKey);
            $success = count($arrKey);
            if ($success !== 0 && count($dataDeliveryDate) !== 0) {
                foreach ($dataDeliveryDate as $item) {
                    $arrUpdate = [
                        'order_sub_status'   => ORDER_SUB_STATUS['DOING'],
                        'is_mail_sent'       => 0,
                    ];
                    $modelO->updateData([$item->receive_id], $arrUpdate);
                }
            }
            $message = "Content 2 process: $total records and update success: $success records.";
            Log::info($message);
            print_r($message . PHP_EOL);
        }

        $datas3 = $modelO->getDataMailUrgentEdiResult();
        $total = count($datas3);
        if ($total === 0) {
            $message = "Content 3 no data.";
            Log::info($message);
            print_r($message . PHP_EOL);
        } else {
            $success = 0;
            foreach ($datas3 as $value) {
                $arrUpdate = [
                    'up_ope_cd' => 'OPE99999',
                    'up_date'   => now(),
                ];
                $maxDeliveryDate = date('Y-m-d', strtotime($value->max_delivery_date));
                /*--------Check holiday temp--------*/
                if ($maxDeliveryDate >= '2019-06-27' && $maxDeliveryDate < '2019-06-30') {
                    //$maxDeliveryDate = date('Y-m-d', strtotime($maxDeliveryDate . " +4 days"));
                    $maxDeliveryDate = '2019-07-01';
                }
                /*-----------------------------*/
                if (!empty($value->delivery_plan_date)
                    && date('Y-m-d', strtotime($value->delivery_plan_date)) < $maxDeliveryDate
                    && $value->mall_id !== 3
                ) {
                    $arrUpdate['is_send_mail_urgent'] = 1;
                    $arrUpdate['urgent_mail_id']      = 230;
                    $arrUpdate['delivery_plan_date']  = $maxDeliveryDate;
                } elseif (empty($value->delivery_plan_date)) {
                    if (strtotime($maxDeliveryDate) <= time()) {
                        if (date('H:i') < '14:30') {
                            $arrUpdate['delivery_plan_date']  = date('Y-m-d');
                        } else {
                            $arrUpdate['delivery_plan_date']  = date('Y-m-d', strtotime("+1 days"));
                        }
                    } else {
                        $arrUpdate['delivery_plan_date']  = $maxDeliveryDate;
                    }
                }
                $modelO->updateData([$value->receive_id], $arrUpdate);
                $success++;
            }
            $message = "Content 3 process: $total records and update success: $success records.";
            Log::info($message);
            print_r($message . PHP_EOL);
        }
    }

    /**
    * Process order EDI result
    * @return void
    */
    public function processOrderResult()
    {
        $modelOTS = new DtOrderToSupplier();
        $modelO   = new MstOrder();
        $modelOPD = new DtOrderProductDetail();
        $checkDate = date('H:i');
        // Step 2
        $datas2 = $modelO->getDataEdiOrderResult2();
        if (count($datas2) !== 0) {
            $sucUp = 0;
            $receiveId = '';
            $arrUpdate2 = [];
            $arrUpDetail = [];
            $flgCheck = true;
            foreach ($datas2 as $data) {
                if ($receiveId !== $data->receive_id && !$flgCheck) {
                    $modelO->updateData(array($receiveId), $arrUpdate2);
                    if (count($arrUpDetail) > 0) {
                        $modelOPD->updateDataByReceive([$receiveId], $arrUpDetail);
                    }
                    $sucUp++;
                    $arrUpdate2  = [];
                    $arrUpDetail = [];
                    $flgCheck    = true;
                }
                $receiveId = $data->receive_id;
                if ($data->product_status === PRODUCT_STATUS['OUT_OF_STOCK']) {
                    $arrUpdate2['order_sub_status'] = ORDER_SUB_STATUS['OUT_OF_STOCK'];
                    $flgCheck = false;
                }
                if ($data->product_status === PRODUCT_STATUS['SALE_STOP']) {
                    $arrUpdate2['order_sub_status'] = ORDER_SUB_STATUS['STOP_SALE'];
                    $flgCheck = false;
                }
            }
            if (!$flgCheck && $receiveId !== '') {
                $modelO->updateData(array($receiveId), $arrUpdate2);
                if (count($arrUpDetail) > 0) {
                    $modelOPD->updateDataByReceive([$receiveId], $arrUpDetail);
                }
                $sucUp++;
            }
            $message = "Update status from [New, Processing] to ".
                       "[Out of stock, Stop sale, Delay] success: $sucUp records";
            Log::info($message);
            print_r($message . PHP_EOL);
        }

        // Step 3
        $dataStep3 = $modelO->getDataEdiOrderResult3();
        if (count($dataStep3) !== 0) {
            $flgCheck = true;
            $receiveId = '';
            $arrUpdate3['order_sub_status'] = ORDER_SUB_STATUS['DONE'];
            $arrUpdate3['is_mail_sent']     = 0;
            $succ3 = 0;
            foreach ($dataStep3 as $data) {
                if (!$flgCheck && $receiveId === $data->receive_id) {
                    continue;
                }
                if ($receiveId !== '' && $receiveId !== $data->receive_id) {
                    if ($flgCheck) {
                        $modelO->updateData(array($receiveId), $arrUpdate3);
                        $succ3++;
                    }
                    $flgCheck = true;
                }
                $receiveId = $data->receive_id;
                if ($data->product_status !== PRODUCT_STATUS['WAIT_TO_SHIP']
                    && $data->product_status !== PRODUCT_STATUS['DIRECTLY_SHIPPING']
                    && $data->product_status !== PRODUCT_STATUS['SHIPPED']
                    && $data->product_status !== PRODUCT_STATUS['CANCEL']
                    && $data->product_status !== PRODUCT_STATUS['COMING']
                ) {
                    $flgCheck = false;
                }
            }
            if ($flgCheck && $receiveId !== '') {
                $modelO->updateData(array($receiveId), $arrUpdate3);
                $succ3++;
            }
            Log::info("Update status from [New, Processing] to [Done] success: $succ3 records");
            print_r("Update status from [New, Processing] to [Done] success: $succ3 records" . PHP_EOL);
        }
    }
}
