<?php
/**
 * Batch process update order
 *
 * @package    App\Console\Commands
 * @subpackage ProcessUpdateOrderSmaregiApi
 * @copyright  Copyright (c) 2018 RiverCrane! Corporation. All Rights Reserved.
 * @author     Le Hung<le.hung.rcvn2012@gmail.com>
 */

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;
use App\Notification;
use App\Notifications\SlackNotification;
use App;
use Config;
use Event;
use App\Events\Command as eCommand;
use App\Models\Batches\DtOrderStoreOrderInfo;
use App\Models\Batches\DtOrderStoreOrderDetail;

class ProcessUpdateOrderSmaregiApi extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'process:smaregi-update-order-api';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update order';

    /**
     * The error.
     *
     * @var string
     */
    public $error = [];

    /**
     * The folder.
     *
     * @var string
     */
    public $folder = null;

    /**
     * Size insert
     * @var int
     */
    private $sizeInsert = 500;
    /**
     * Smaregi contract id
     * @var contract_id
     */
    public $contractId = '';

    /**
     * Smaregi access token
     * @var accessToken
     */
    public $accessToken = '';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        $this->contractId   = env('CONTRACT_ID');
        $this->accessToken  = env('ACCESS_TOKEN');
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $start          = microtime(true);
        //Create log
        $arrayReplace = [':', '-'];
        $this->folder       = str_replace($arrayReplace, '_', $this->signature);
        Log::useDailyFiles(storage_path() . "/logs/$this->folder/history.log");
        //Process data
        Event::fire(new eCommand($this->signature, array('start' => true)));
        Log::info('Start process smaregi api');
        print_r("Start process smaregi api" . PHP_EOL);

        // Process get order
        print_r("-------- PROCESS STEP 1 - UPDATE DELIVERY --------" . PHP_EOL);
        $this->processContent1();

        // Process Order Detail
        //print_r("-------- PROCESS STEP 2 - UPDATING INFO & DETAIL --------" . PHP_EOL);
        //$this->processContent2();

        // Process Updating Api
        print_r("-------- PROCESS STEP 2 - UPDATING API --------" . PHP_EOL);
        //$this->processContentKeepStep2();

        print_r("-------- PROCESS STEP 3 - UPDATE STATUS" . PHP_EOL);
        $this->processContent3();

        print_r("-------- PROCESS STEP 4 - UPDATE RESULT" . PHP_EOL);
        $this->processContent4();

        Event::fire(new eCommand($this->signature, array('end' => true, 'error' => $this->error)));
        $totalTime = round(microtime(true) - $start, 2);
        Log::info("End batch process smaregi  api: $totalTime s.");
        print_r("End batch process smaregi  api: $totalTime s." . PHP_EOL);
    }

    /**
     * Update order
     *
     * @param   array  $response
     * @return  array
     */
    private function processContent1()
    {
        $modelDtOrderInfo   = new DtOrderStoreOrderInfo();
        $modelDtOrderDetail = new DtOrderStoreOrderDetail();
        $data = $modelDtOrderDetail->getDataUpdateDelivery();
        if (count($data) > 0) {
            try {
                foreach ($data as $key => $value) {
                    // Update order info
                    $newDate = !empty($value['delivery_plan_date']) ?
                            date('Y-m-d', strtotime($value['delivery_plan_date'])) : null;
                    $modelDtOrderInfo->updateData(
                        ['order_id'                 => $value['order_id']],
                        [
                            'delivery_plan_date'   => $newDate,
                            'status'               => 3
                        ]
                    );
                    // Update order detail belong order info
                    $modelDtOrderDetail->updateData(
                        ['order_id'      => $value['order_id']],
                        ['process_status'   => 2]
                    );
                }
            } catch (\Exception $e) {
                report($e);
            }
            $count = count($data);
            Log::info("Total $count items has already updated.");
            print_r("Total $count items has already updated." . PHP_EOL);
        } else {
            Log::info('No data updated');
            print_r('No data updated' . PHP_EOL);
        }
    }

    /**
     * Update order
     *
     * @return  none
     */
    private function processContent2()
    {
        $slack   = new Notification(CHANNEL['horunba']);
        $modelDtOrderInfo   = new DtOrderStoreOrderInfo();
        $modelDtOrderDetail = new DtOrderStoreOrderDetail();
        $data = $modelDtOrderDetail->getDataUpdateDelivery2();
        if (count($data) > 0) {
            try {
                foreach ($data as $key => $value) {
                    $newDate = !empty($value['arrival_date_plan']) ?
                            date('Y-m-d', strtotime($value['arrival_date_plan'])) : null;
                    $modelDtOrderInfo->updateData(
                        ['order_id'                 => $value['order_id']],
                        [
                            'delivery_plan_date'   => $newDate,
                            'status'               => 3
                        ]
                    );
                    $modelDtOrderDetail->updateData(
                        ['order_id'      => $value['order_id']],
                        ['process_status'   => 2]
                    );
                }
            } catch (\Exception $e) {
                report($e);
            }
            $count = count($data);
            Log::info("Total $count items has already updated.");
            print_r("Total $count items has already updated." . PHP_EOL);
        } else {
            Log::info('No data updated');
            print_r('No data updated' . PHP_EOL);
        }
    }

    /**
     * Update process status
     *
     * @return  none
     */
    private function processContentKeepStep2()
    {
        $modelInfo      = new DtOrderStoreOrderInfo();
        $start          = microtime(true);
        $requestParams['header'] = array(
            "Content-Type: application/x-www-form-urlencoded",
            "X_contract_id: $this->contractId",
            "X_access_token: $this->accessToken",
        );
        $data           = $modelInfo->getDataUpdatingApi();
        $count          = count($data);
        $index          = 0;
        if ($count > 0) {
            if (App::environment(['local', 'test'])) {
                foreach ($data as $key => $value) {
                    $modelInfo->updateData(
                        ['order_id' => $value->order_id],
                        [
                            'process_status' => 2
                        ]
                    );
                }
                $total = count($data);
                Log::info("Total $total items has already updated.");
                print_r("Total $total items has already updated." . PHP_EOL);
                return true;
            }
            $oldOrderId = '';
            foreach ($data as $key => $value) {
                $newDate      = date("Y-m-d", strtotime($value['delivery_plan_date'] . "+1 days"));
                $tokenDate    = date("YmdHis") . "0" . ($key+1);
                if ($oldOrderId === '') {
                    $oldOrderId = $value['order_id'];
                }
                if ($oldOrderId === $value['order_id']) {
                    $productsInfo[] = array(
                        'storageInfoId' => $value['order_id'],
                        'productId'     => $value['product_id'],
                        'cost'          => $value['price']
                    );
                     $productsDelivery[] = array(
                        'storageInfoId' => $value['order_id'],
                        'productId'     => $value['product_id'],
                        'quantity'      => $value['quantity'],
                        'storeId'       => $value['storage_store_id']
                    );
                }
                if ($oldOrderId !== $value['order_id'] || $key === $count - 1) {
                    $requestParams['fields'] = http_build_query(array(
                        'proc_name' => 'storage_info_upd',
                        'params' => '{
                            "proc_info": {"proc_division" : "U"},
                            "data":[
                                {
                                    "table_name":"StorageInfo",
                                    "rows":[{
                                        "storageInfoId":"'.$value['order_id'].'",
                                        "status":"2",
                                        "token" : "'.$tokenDate.'"
                                    }]
                                },
                                {
                                    "table_name":"StorageInfoDelivery",
                                    "rows":[{
                                        "storageInfoId": "'.$value['order_id'].'",
                                        "storageStoreId": "'.$value['storage_store_id'].'",
                                        "storageExpectedDateFrom": "'.$newDate.'",
                                        "storageExpectedDateTo": "'.$newDate.'"
                                    }]
                                },
                                {
                                    "table_name": "StorageInfoProduct",
                                    "rows": '.json_encode($productsInfo).'
                                },
                                {
                                    "table_name":"StorageInfoDeliveryProduct",
                                    "rows": '.json_encode($productsDelivery).'
                                }
                            ]
                        }'
                    ));
                    $response = $this->curlGet($requestParams);
                    if (!empty($response) && !isset($response->error_code)) {
                        if (count($response->result) > 0) {
                            $index++;
                        }
                    }
                    $modelInfo->updateData(
                        ['order_id' => $value['order_id']],
                        [
                            'process_status'    => 2
                        ]
                    );
                    $oldOrderId = $value['order_id'];
                    $productsInfo = array();
                    $productsDelivery = array();
                }
            }
        }
        Log::info("Total $index items has already updated.");
        print_r("Total $index items has already updated." . PHP_EOL);
    }
    /**
     * Update process status
     *
     * @return  none
     */
    private function processContent3()
    {
        $modelInfo      = new DtOrderStoreOrderInfo();
        $start          = microtime(true);
        $index          = 0;
        $requestParams['header'] = array(
            "Content-Type: application/x-www-form-urlencoded",
            "X_contract_id: $this->contractId",
            "X_access_token: $this->accessToken",
        );
        $requestParams['fields'] = http_build_query(array(
            'proc_name' => 'storage_info_ref',
            'params' => '{
                "fields":["storageInfoId","status","modified"],
                "conditions":[{"status":"4"},{"modified >=":"'.date("Y-m-d").'"}],
                "table_name":"StorageInfo"
            }'
        ));
        $response = $this->curlGet($requestParams);
        if (!empty($response) && !isset($response->error_code)) {
            if (count($response->result) > 0) {
                foreach ($response->result as $key => $value) {
                    $modelInfo->updateData(
                        ['order_id' => $value->storageInfoId],
                        [
                            'status'            => 4,
                            'process_status'    => 5,
                            'received_date'     => $value->modified
                        ]
                    );
                    $index++;
                }
            }
        }
        Log::info("Total $index items has already updated.");
        print_r("Total $index items has already updated." . PHP_EOL);
    }
    /**
     * Update process status
     *
     * @return  none
     */
    private function processContent4()
    {
        $index          = 0;
         $requestParams['header'] = array(
            "Content-Type: application/x-www-form-urlencoded",
            "X_contract_id: $this->contractId",
            "X_access_token: $this->accessToken",
        );
        $modelDetail = new DtOrderStoreOrderDetail();
        $data = $modelDetail->getDataUpdateResult();
        if (count($data) > 0) {
            if (App::environment(['local', 'test'])) {
                foreach ($data as $key => $value) {
                    $modelDetail->updateData(
                        [
                            'order_id'      => $value['order_id'],
                            'product_id'    => $value['product_id']
                        ],
                        [
                            'received_quantity'     => (int)$value['quantity'],
                            'shortage_quantity'     => (int)$value['quantity'],
                            'process_status'        => 3
                        ]
                    );
                }
                $total = count($data);
                Log::info("Total $total items has already updated.");
                print_r("Total $total items has already updated." . PHP_EOL);
                return true;
            }

            foreach ($data as $key => $value) {
                $requestParams['fields'] = http_build_query(array(
                    'proc_name' => 'storage_info_ref',
                    'params' => '{
                        "fields":["productCode","quantity"],
                        "conditions":[{"storageInfoId":"'.$value['order_id'].'"},'
                    . '{"storeId":"'.$value['storage_store_id'].'"},{"productId":"'.$value['product_id'].'"}],
                        "table_name":"StorageInfoDeliveryProduct"
                    }'
                ));
                $response = $this->curlGet($requestParams);
                if (!empty($response) && !isset($response->error_code)) {
                    if (count($response->result) > 0) {
                        foreach ($response->result as $key1 => $value1) {
                            $quantity = (int) $value['quantity'] - (int) $value1->quantity;
                            $modelDetail->updateData(
                                [
                                    'order_id'      => $value['order_id'],
                                    'product_id'    => $value['product_id']
                                ],
                                [
                                    'received_quantity'     => $value1->quantity,
                                    'shortage_quantity'     => $quantity,
                                    'process_status'        => 3
                                ]
                            );
                            $index++;
                        }
                    }
                }
            }
        }
        Log::info("Total $index items has already updated.");
        print_r("Total $index items has already updated." . PHP_EOL);
    }
    /**
     * Request process api
     *
     * @param  array $paramOptions
     * @return json encode
     */
    private function curlGet($paramOptions)
    {
        $slack   = new Notification(CHANNEL['horunba']);
        $smaregiUrl     = "https://webapi.smaregi.jp/access/";
        $ch = curl_init($smaregiUrl);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $paramOptions['header']);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $paramOptions['fields']);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $response = curl_exec($ch);
        curl_close($ch);
        $response = json_decode($response);
        if (isset($response->error_code)) {
            $arrError = (array) $response;
            $err = implode("\n", $arrError);
            $error  = "------------------------------------------" . PHP_EOL;
            $error .= basename(__CLASS__) . PHP_EOL;
            $error .= $err;
            $error .= PHP_EOL . "------------------------------------------" . PHP_EOL;
            $slack->notify(new SlackNotification($error));
            Log::info($err);
            print_r($err . PHP_EOL);
        }
        return $response;
    }
}
