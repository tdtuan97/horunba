<?php
/**
 * Batch process tmp_check_edit table
 *
 * @package    App\Console\Commands
 * @subpackage ProcessCloneData
 * @copyright  Copyright (c) 2018 RiverCrane! Corporation. All Rights Reserved.
 * @author     Truong Nghia<truong.van.nghia@rivercrane.vn>
 */

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;
use App\Models\Batches\MasterSpProcessLog;
use App\Notification;
use App\Notifications\SlackNotification;
use DB;
use Event;
use App\Events\Command as eCommand;

class ProcessCloneData extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'process:clone-data';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Clone data';

    /**
     * The error.
     *
     * @var array
     */
    public $error = [];

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        Event::fire(new eCommand($this->signature, array('start' => true)));
        Log::info('Start batch process clone data.');
        print_r("Start batch process clone data." . PHP_EOL);
        $start    = microtime(true);
        DB::connection('horunba_clone')->select('CALL clone_data_for_md()');
        $modelLog = new MasterSpProcessLog();
        $arrError = $modelLog->countCheckProcessLog('clone_data_for_md');
        if ($arrError->count() !== 0) {
            $message = $arrError->pluck('process_step')->toArray();
            $message = implode(', ', $message);
            $message = str_replace('Clone table ', '', $message);
            $slack = new Notification(CHANNEL['horunba']);
            $error  = "------------------------------------------" . PHP_EOL;
            $error .= basename(__CLASS__) . PHP_EOL;
            $error .= "Clone データ($message) のコピーが失敗しました。";
            $error .= PHP_EOL . "------------------------------------------" . PHP_EOL;
            $slack->notify(new SlackNotification($error));
        }
        $totalTime = round(microtime(true) - $start, 2);
        Log::info("End batch process clone data with total time: $totalTime s.");
        print_r("End batch process clone data with total time: $totalTime s." . PHP_EOL);
        Event::fire(new eCommand($this->signature, array('end' => true, 'error' => $this->error)));
    }
}
