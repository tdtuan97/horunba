<?php
/**
 * Get Receive' result from EDI
 *
 * @package    App\Console\Commands
 * @subpackage ProcessEdiReceiveResult
 * @copyright  Copyright (c) 2018 RiverCrane! Corporation. All Rights Reserved.
 * @author     Truong Nghia<truong.van.nghia@rivercrane.vn>
 */
namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;
use App\Notifications\SlackNotification;
use App\Notification;
use App\Models\Batches\MstOrder;
use App\Models\Batches\DtOrderToSupplier;
use App\Models\Batches\DtOrderProductDetail;
use App\Models\Batches\TRequestPerformanceModelEdi;
use App\Models\Batches\DtReceiptLedger;
use App\Models\Batches\MstStockStatus;
use App\Models\Batches\DtDelivery;
use App\Models\Batches\DtReturn;
use App\Custom\Utilities;
use App\Events\Command as eCommand;
use Event;
use DB;

class ProcessEdiReceiveResult extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'process:edi-receive-result';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = "Get Receive' result from EDI ";

    /**
     * The notification.
     *
     * @var object
     */
    protected $slack;

    /**
     * The error.
     *
     * @var array
     */
    public $error = [];
    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $arrayReplace = [':', '-'];
        $folder       = str_replace($arrayReplace, '_', $this->signature);
        Log::useDailyFiles(storage_path() . "/logs/$folder/history.log");
        Event::fire(new eCommand($this->signature, array('start' => true)));
        $slack = new Notification(CHANNEL['horunba']);
        $start = microtime(true);
        Log::info("Start batch process EDI Receive's result.");
        print_r("Start batch process EDI Receive's result." . PHP_EOL);
        $modelO   = new MstOrder();
        $modelDOS = new DtOrderToSupplier();
        $modelOPD = new DtOrderProductDetail();
        $modelTRP = new TRequestPerformanceModelEdi();
        $modelRL  = new DtReceiptLedger();
        $modelSS  = new MstStockStatus();
        $modelDD  = new DtDelivery();
        $modelDR  = new DtReturn();
        $message = "Start process content 1";
        Log::info("---- $message ----");
        print_r("---- $message ----" . PHP_EOL);
        $datas1 = $modelO->getDataReceiveResult1();
        if (count($datas1) === 0) {
            Log::info('No data');
            print_r("No data. " . PHP_EOL);
        } else {
            $upSucc = 0;
            foreach ($datas1 as $data1) {
                if (is_null($data1->is_available) ||
                (!is_null($data1->is_available) && $data1->is_mail_sent === $data1->is_available)) {
                    $arrUpdate = [
                        'order_status'     => ORDER_STATUS['ARRIVAL'],
                        'order_sub_status' => ORDER_SUB_STATUS['NEW'],
                        'is_mail_sent'     => 0,
                    ];
                    $modelO->updateData([$data1->receive_id], $arrUpdate);
                    $upSucc++;
                }
            }
            $message  = "Update table mst_order success: $upSucc records";
            Log::info($message);
            print_r($message . PHP_EOL);
        }
        $message = "End process content 1";
        Log::info("---- $message ----");
        print_r("---- $message ----" . PHP_EOL);
        $message = "Start process content 2";
        Log::info("---- $message ----");
        print_r("---- $message ----" . PHP_EOL);
        $datas2 = $modelDOS->getDataReceiveResult2();
        if (count($datas2) === 0) {
            Log::info('No data');
            print_r("No data. " . PHP_EOL);
        } else {
            $upSucc = 0;
            $preOrderCode = '';
            $tempQuantity = 0;
            $preQuantity = 0;
            $prePlanQuantity = 0;
            $productCode = '';
            $deliveryTpe = 0;
            $caldow = date('w');
            foreach ($datas2 as $data2) {
                try {
                    $quantity = (int)$data2->Quantity;
                    if (!empty($data2->opd_order_code)) {
                        if ($data2->order_code !== $preOrderCode) {
                            $modelRL->add2ReceiptLedgerInStock([
                                'pa_order_code'        => $data2->SupplyNo,
                                'pa_product_code'      => $data2->product_code,
                                'pa_price_invoice'     => $data2->price,
                                'pa_arrival_num'       => $quantity,
                                'pa_supplier_id'       => $data2->supplier_id,
                                'pa_stock_type'        => 2,
                                'pa_stock_detail_type' => 1,
                            ]);
                            if ($data2->order_type === 5) {
                                $modelSS->updateStockInStockR($data2->product_code, $quantity, false);
                            } else {
                                $modelSS->updateStockInStock($data2->product_code, $quantity, $data2->delivery_type);
                            }
                            $processstatus = 5;
                            if ($data2->PlanQuantity === $data2->Quantity) {
                                 $processstatus = 6;
                                if ($data2->order_type === 5 && !empty($data2->return_no)) {
                                    $modelDR->updateData(
                                        [
                                            'return_no'      => $data2->return_no,
                                            'return_line_no' => $data2->return_line_no,
                                            'return_time'    => $data2->return_time,
                                        ],
                                        [
                                            'receive_instruction' => 2
                                        ]
                                    );
                                }
                            }
                            $modelDOS->where('edi_order_code', '=', $data2->edi_order_code)
                                     ->where('order_code', '=', $data2->order_code)
                                     ->increment(
                                         'arrival_quantity',
                                         $quantity,
                                         [
                                             'incomplete_quantity' => ($data2->order_num - $data2->arrival_quantity - $quantity),
                                             'process_status'      => $processstatus,
                                         ]
                                     );
                            $modelTRP->where('tRequestPerformanceModel_autono', $data2->tRequestPerformanceModel_autono)
                                ->update(['decsy_update_flg' => 2]);
                            $preOrderCode = $data2->order_code;
                            $tempQuantity = $quantity;
                        }
                        if (in_array(
                            $data2->product_status,
                            [PRODUCT_STATUS['ARRIVAL_NOT_ENOUGH'], PRODUCT_STATUS['COMING']]
                        )) {
                            $arrUpdate = [];
                            $check = false;
                            $plus  = 0;
                            if (($data2->PlanQuantity === $data2->Quantity)&&
                                ($data2->order_type == 1)) {
                                $check = true;
                                if (!in_array($data2->supplier_id, [32, 41, 62, 138, 171, 282, 367, 395])) {
                                    $plus = 1;
                                }
                                //アサヒペンの土曜にクロスドックしない(367 , 282, 395, 171)
                                if ($caldow === '6' && in_array($data2->supplier_id, [171, 282, 367, 395])) {
                                    $plus = 1;
                                }
                                $receivedOrderNum = $data2->received_order_num;
                                $modelSS->updateData(
                                    $data2->product_code,
                                    ['wait_delivery_num' => DB::raw("wait_delivery_num + {$receivedOrderNum}")]
                                );
                                $arrUpdate = [
                                    'product_status' => PRODUCT_STATUS['WAIT_TO_SHIP'],
                                ];
                            } else {
                                if ($data2->opd_order_num <= $tempQuantity) {
                                    $check     = true;
                                    if (!in_array($data2->supplier_id, [32, 41, 62, 138, 171, 282, 367, 395])) {
                                        $plus = 1;
                                    }
                                    //アサヒペンの土曜にクロスドックしない(367 , 282, 395, 171)
                                    if ($caldow === '6' && in_array($data2->supplier_id, [171, 282, 367, 395])) {
                                        $plus = 1;
                                    }
                                    $receivedOrderNum = $data2->received_order_num;
                                    $modelSS->updateData(
                                        $data2->product_code,
                                        ['wait_delivery_num' => DB::raw("wait_delivery_num + {$receivedOrderNum}")]
                                    );
                                    $arrUpdate = [
                                        'product_status' => PRODUCT_STATUS['WAIT_TO_SHIP'],
                                    ];
                                    $tempQuantity -= $data2->opd_order_num;
                                } else {
                                    $arrUpdate = [
                                        'product_status' => PRODUCT_STATUS['ARRIVAL_NOT_ENOUGH'],
                                        'order_num'      => $data2->opd_order_num - $tempQuantity,
                                    ];
                                    $modelSS->updateData(
                                        $data2->product_code,
                                        ['wait_delivery_num' => DB::raw("wait_delivery_num + {$tempQuantity}")]
                                    );
                                    $tempQuantity = 0;
                                }
                            }
                            if ($check) {
                                $deliveryDate = null;
                                $shopAnswer   = null;
                                if (!empty($data2->ship_wish_date)) {
                                    $deliDays = (int)$data2->deli_days;
                                    $cal = strtotime($data2->ship_wish_date . '-' . $deliDays . ' days');
                                    $now = strtotime('+' . $plus . ' days');
                                    if ($cal >= $now) {
                                        $deliveryDate = date('Ymd', $cal);
                                    } else {
                                        $shopAnswer = '▼お届け日のご希望をお伺いしておりましたが、'.
                                        '本メールに記載しております発送日が最短でございます。'.
                                        '恐れ入りますが商品到着までもうしばらくお待ちくださいませ。';
                                        $modelO->updateData([$data2->receive_id], ['shop_answer' => $shopAnswer]);
                                        $deliveryDate =  date('Ymd', strtotime('+' . $plus . ' days'));
                                    }
                                } else {
                                    $hour = date('H:i');
                                    if ($hour < '15:00' && $plus === 0) {
                                        $deliveryDate = date('Ymd');
                                    } else {
                                        $deliveryDate = date('Ymd', strtotime("+1 days"));
                                    }
                                }
                                $arrUpdate['delivery_date'] = $deliveryDate;
                                /*
                                if (!empty($data2->delivery_date)) {
                                    if (strtotime($data2->delivery_date) < strtotime($deliveryDate)) {
                                        $modelO->updateData([$data2->receive_id], [
                                            'is_send_mail_urgent' => 1,
                                            'urgent_mail_id'      => 80
                                        ]);
                                    }
                                }*/
                            }
                            $modelOPD->updateData(
                                $data2->receive_id,
                                $data2->detail_line_num,
                                $data2->sub_line_num,
                                $arrUpdate
                            );
                        }
                    } else {
                        $modelRL->add2ReceiptLedgerInStock([
                            'pa_order_code'        => $data2->SupplyNo,
                            'pa_product_code'      => $data2->product_code,
                            'pa_price_invoice'     => $data2->price,
                            'pa_arrival_num'       => $quantity,
                            'pa_supplier_id'       => $data2->supplier_id,
                            'pa_stock_type'        => 2,
                            'pa_stock_detail_type' => 1,
                        ]);
                        if ($data2->order_type === 5) {
                            $modelSS->updateStockInStockR($data2->product_code, $quantity, false);
                        } else {
                            $modelSS->updateStockInStock($data2->product_code, $quantity, $data2->delivery_type);
                        }
                        $processstatus = 5;
                        if ($data2->PlanQuantity === $data2->Quantity) {
                            $processstatus = 6;
                            if ($data2->order_type === 5 && !empty($data2->return_no)) {
                                $modelDR->updateData(
                                    [
                                        'return_no'      => $data2->return_no,
                                        'return_line_no' => $data2->return_line_no,
                                        'return_time'    => $data2->return_time,
                                    ],
                                    [
                                        'receive_instruction' => 2
                                    ]
                                );
                            }
                        }
                        $modelDOS->where('edi_order_code', '=', $data2->edi_order_code)
                                 ->where('order_code', '=', $data2->order_code)
                        ->increment(
                            'arrival_quantity',
                            $quantity,
                            [
                                'incomplete_quantity' => ($data2->order_num - $data2->arrival_quantity - $quantity),
                                'process_status'      => $processstatus,
                            ]
                        );
                        $modelTRP->where('tRequestPerformanceModel_autono', $data2->tRequestPerformanceModel_autono)
                                ->update(['decsy_update_flg' => 2]);
                    }
                    $upSucc++;
                } catch (\Exception $e) {
                    $this->error[] = Utilities::checkMessageException($e);
                    $error  = "------------------------------------------" . PHP_EOL;
                    $error .= basename(__CLASS__) . PHP_EOL;
                    $error .= Utilities::checkMessageException($e);
                    $error .= PHP_EOL . "------------------------------------------" . PHP_EOL;
                    continue;
                }
            }
            $message  = "Update table dt_order_product_detail success: $upSucc records";
            Log::info($message);
            print_r($message . PHP_EOL);
        }
        $message = "End process content 2";
        Log::info("---- $message ----");
        print_r("---- $message ----" . PHP_EOL);
        $message = "Start process content 3";
        Log::info("---- $message ----");
        print_r("---- $message ----" . PHP_EOL);
        $datas3 = $modelDOS->getDataReceiveResult3();
        if (count($datas3) === 0) {
            Log::info('No data');
            print_r("No data. " . PHP_EOL);
        } else {
            $upSucc = 0;
            $preOrderCode = '';
            $tempQuantity = 0;
            foreach ($datas3 as $data3) {
                $quantity = (int)$data3->Quantity;
                try {
                    if (!empty($data3->opd_order_code)) {
                        if ($data3->order_code !== $preOrderCode) {
                            $modelRL->add2ReceiptLedgerInStock([
                                'pa_order_code'        => $data3->SupplyNo,
                                'pa_product_code'      => $data3->product_code,
                                'pa_price_invoice'     => $data3->price,
                                'pa_arrival_num'       => $quantity,
                                'pa_supplier_id'       => $data3->supplier_id,
                                'pa_stock_type'        => 2,
                                'pa_stock_detail_type' => 1,
                            ]);
                            if ($data3->order_type === 5) {
                                $modelSS->updateStockInStockR($data3->product_code, $quantity, false);
                            } else {
                                $modelSS->updateStockInStock($data3->product_code, $quantity, $data3->delivery_type);
                            }
                            $modelTRP->where('tRequestPerformanceModel_autono', $data3->tRequestPerformanceModel_autono)
                                ->update(['decsy_update_flg' => 2]);
                            $tempQuantity = $quantity + $data3->arrival_quantity;
                            $modelDOS->where('edi_order_code', '=', $data3->edi_order_code)
                                     ->where('order_code', '=', $data3->order_code)
                                     ->update([
                                        'arrival_quantity' => DB::raw("arrival_quantity + {$quantity}"),
                                        'incomplete_quantity' => DB::raw("incomplete_quantity - {$quantity}"),
                                     ]);
                            // if ($data3->order_type === 5 && $data3->process_status === 6 && !empty($data3->return_no)) {
                            //     $modelDR->updateData(
                            //         [
                            //             'return_no'      => $data3->return_no,
                            //             'return_line_no' => $data3->return_line_no,
                            //             'return_time'    => $data3->return_time,
                            //         ],
                            //         [
                            //             'receive_instruction' => 2
                            //         ]
                            //     );
                            // }
                        }
                        if (in_array(
                            $data3->product_status,
                            [
                                PRODUCT_STATUS['ARRIVAL_NOT_ENOUGH'],
                                PRODUCT_STATUS['COMING'],
                                PRODUCT_STATUS['WAIT_TO_SHIP']
                            ]
                        )) {
                            if ($data3->received_order_num <= $quantity) {
                                $tempQuantity -= $data3->received_order_num;
                            } else {
                                $arrUpdate = [];
                                if ($data3->order_status === ORDER_STATUS['ARRIVAL']
                                    && $data3->product_status === PRODUCT_STATUS['WAIT_TO_SHIP']) {
                                    $arrUpdate = [
                                        'product_status' => PRODUCT_STATUS['ARRIVAL_NOT_ENOUGH'],
                                    ];
                                    $modelOPD->updateData(
                                        $data3->receive_id,
                                        $data3->detail_line_num,
                                        $data3->sub_line_num,
                                        $arrUpdate
                                    );
                                } elseif ($data3->order_status > ORDER_STATUS['ARRIVAL']) {
                                    $arrUpdate = [
                                        'order_sub_status' => ORDER_SUB_STATUS['CORRECT_STOCK'],
                                    ];
                                    $modelO->updateData([$data3->receive_id], $arrUpdate);
                                }
                            }
                        }
                    } else {
                        $modelRL->add2ReceiptLedgerInStock([
                            'pa_order_code'        => $data3->SupplyNo,
                            'pa_product_code'      => $data3->product_code,
                            'pa_price_invoice'     => $data3->price,
                            'pa_arrival_num'       => $quantity,
                            'pa_supplier_id'       => $data3->supplier_id,
                            'pa_stock_type'        => 2,
                            'pa_stock_detail_type' => 1,
                        ]);
                        if ($data3->order_type === 5) {
                            $modelSS->updateStockInStockR($data3->product_code, $quantity, false);
                        } else {
                            $modelSS->updateStockInStock($data3->product_code, $quantity, $data3->delivery_type);
                        }
                        $modelDOS->where('edi_order_code', '=', $data3->edi_order_code)
                                 ->where('order_code', '=', $data3->order_code)
                                    ->update([
                                        'arrival_quantity' => DB::raw("arrival_quantity + {$quantity}"),
                                        'incomplete_quantity' => DB::raw("incomplete_quantity - {$quantity}"),
                                    ]);
                        $modelTRP->where('tRequestPerformanceModel_autono', $data3->tRequestPerformanceModel_autono)
                                ->update(['decsy_update_flg' => 2]);
                        // if ($data3->order_type === 5 && $data3->process_status === 6 && !empty($data3->return_no)) {
                        //     $modelDR->updateData(
                        //         [
                        //             'return_no'      => $data3->return_no,
                        //             'return_line_no' => $data3->return_line_no,
                        //             'return_time'    => $data3->return_time,
                        //         ],
                        //         [
                        //             'receive_instruction' => 2
                        //         ]
                        //     );
                        // }
                    }
                    $upSucc++;
                } catch (\Exception $e) {
                    $this->error[] = Utilities::checkMessageException($e);
                    $error  = "------------------------------------------" . PHP_EOL;
                    $error .= basename(__CLASS__) . PHP_EOL;
                    $error .= Utilities::checkMessageException($e);
                    $error .= PHP_EOL . "------------------------------------------" . PHP_EOL;
                    continue;
                }
            }
            $message  = "Update table dt_order_product_detail success: $upSucc records";
            Log::info($message);
            print_r($message . PHP_EOL);
        }
        $message = "End process content 3";
        Log::info("---- $message ----");
        print_r("---- $message ----" . PHP_EOL);
        $message = "Start process content 4";
        Log::info("---- $message ----");
        print_r("---- $message ----" . PHP_EOL);
        $datas4 = $modelO->getDataReceiveResult4();
        if (count($datas4) === 0) {
            Log::info('No data');
            print_r("No data. " . PHP_EOL);
        } else {
            $arrUpdate = ['order_sub_status' => ORDER_SUB_STATUS['DONE']];
            $check = true;
            $receiveId = '';
            $arrProductStatus = [1, 8, 9, 15];
            $upSucc = 0;
            foreach ($datas4 as $data4) {
                $productStatus = (int)$data4->product_status;
                if ($data4->receive_id !== $receiveId) {
                    if ($receiveId !== '' && $check) {
                        $modelO->updateData([$receiveId], $arrUpdate);
                        $upSucc++;
                    }
                    $check = true;
                    $receiveId = $data4->receive_id;
                } elseif (!$check) {
                    continue;
                }
                if (!in_array($productStatus, $arrProductStatus)) {
                    $check = false;
                }
            }
            if ($check) {
                $modelO->updateData([$receiveId], $arrUpdate);
                $upSucc++;
            }
            $message  = "Update table mst_order success: $upSucc records";
            Log::info($message);
            print_r($message . PHP_EOL);
        }
        $message = "End process content 4";
        Log::info("---- $message ----");
        print_r("---- $message ----" . PHP_EOL);
        $message = "Start process content 5";
        Log::info("---- $message ----");
        print_r("---- $message ----" . PHP_EOL);
        $datas5  = $modelO->getDataReceiveResult5();
        if (count($datas5) === 0) {
            Log::info('No data');
            print_r("No data. " . PHP_EOL);
        } else {
            $success = 0;
            foreach ($datas5 as $data5) {
                $deliveryDate = null;
                if (!empty($data5->ship_wish_date)) {
                    $deliDays = (int)$data5->deli_days;
                    $cal = strtotime($data5->ship_wish_date . '-' . $deliDays . ' days');
                    $now = strtotime(date('Y-m-d'));
                    if ($cal >= $now) {
                        $deliveryDate = date('Ymd', $cal);
                    } else {
                        $shopAnswer = "▼お届け日のご希望をお伺いしておりましたが、".
                        "本メールに記載しております発送日が最短''でございます。".
                        "恐れ入りますが商品到着までもうしばらくお待ちくださいませ。";
                        $modelO->updateData([$data5->receive_id], ['shop_answer' => $shopAnswer]);
                        $deliveryDate =  date('Ymd');
                    }
                } else {
                    $hour = date('H:i');
                    if ($hour < '15:00') {
                        $deliveryDate = date('Ymd');
                    } else {
                        $deliveryDate = date('Ymd', strtotime("+1 days"));
                    }
                }
                $modelOPD->updateData(
                    $data5->receive_id,
                    $data5->detail_line_num,
                    $data5->sub_line_num,
                    ['delivery_date' => $deliveryDate]
                );
                $success++;
            }
            $message  = "Update delivery_date success: $success records";
            Log::info($message);
            print_r($message . PHP_EOL);
        }
        $message = "End process content 5";
        Log::info("---- $message ----");
        print_r("---- $message ----" . PHP_EOL);

        $message = "Start process content 6";
        Log::info("---- $message ----");
        print_r("---- $message ----" . PHP_EOL);
        $datas6  = $modelO->getDataReceiveResult6();
        $total = count($datas6);
        if ($total === 0) {
            Log::info('No data');
            print_r("No data. " . PHP_EOL);
        } else {
            $success = 0;
            foreach ($datas6 as $value) {
                $modelO->updateData(
                    [$value->receive_id],
                    [
                        'shipment_date' => $value->max_delivery_date,
                        'up_ope_cd'     => 'OPE99999',
                        'up_date'       => now(),
                    ]
                );
                $success++;
                //update delivery_plan_date by ship_wish_date
                if (!empty($value->ship_wish_date) && ($value->max_delivery_date < $value->ship_wish_date)) {
                    $modelO->updateData(
                        [$value->receive_id],
                        [
                            'delivery_plan_date' => $value->max_delivery_date,
                            'up_ope_cd'          => 'OPE99999',
                            'up_date'            => now(),
                        ]
                    );
                }
            }
            $message  = "Update shipment_date success: $success records";
            Log::info($message);
            print_r($message . PHP_EOL);
        }
        $message = "End process content 6";
        Log::info("---- $message ----");
        print_r("---- $message ----" . PHP_EOL);
        Event::fire(new eCommand($this->signature, array('end' => true, 'error' => $this->error)));
        $totalTime = round(microtime(true) - $start, 2);
        Log::info("End batch process EDI Receive's result with total time: $totalTime.");
        print_r("End batch process EDI Receive's result with total time: $totalTime s." . PHP_EOL);
    }
}
