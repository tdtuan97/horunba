<?php
/**
 * Batch process order supplier zaiko result
 *
 * @package    App\Console\Commands
 * @subpackage ProcessZaikoResult
 * @copyright  Copyright (c) 2017 RiverCrane! Corporation. All Rights Reserved.
 * @author     Truong Nghia<truong.van.nghia@rivercrane.vn>
 */

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\Batches\TZaiko;
use Illuminate\Support\Facades\Log;
use App\Models\Batches\DtOrderProductDetail;
use App\Models\Batches\DtEstimation;
use App\Models\Batches\MstOrder;
use App\Models\Batches\MstOrderDetail;
use App\Notifications\SlackNotification;
use App\Custom\CurlPost;
use App\Notification;
use App\Events\Command as eCommand;
use App\Custom\Utilities;
use Event;
use Config;
use DB;
use App;
use App\Models\Batches\MstRakutenKey;

class ProcessZaikoResult extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'process:zaiko-result';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';
    /**
     * The key rakuten
     *
     * @var string
     */
    protected $keyRakuten = '';

    /**
     * The error.
     *
     * @var array
     */
    public $error = [];

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //Create log
        $arrayReplace = [':', '-'];
        $folder       = str_replace($arrayReplace, '_', $this->signature);
        Log::useDailyFiles(storage_path() . "/logs/$folder/history.log");
        //Process data
        Event::fire(new eCommand($this->signature, array('start' => true)));
        Log::info('Start batch process zaiko result');
        print_r("Start batch process zaiko result." . PHP_EOL);
        $start      = microtime(true);
        $modelZaiko = new TZaiko();
        $modelOPD   = new DtOrderProductDetail();
        $modelEs    = new DtEstimation();
        $modelO     = new MstOrder();
        $modelOD    = new MstOrderDetail();
        $modelRakutenKey  = new MstRakutenKey();
        $this->keyRakuten = $modelRakutenKey->getKeyRakuten();
        $slack      = new Notification(CHANNEL['horunba']);
        $datas      = $modelZaiko->getDataZaikoResult();
        $upPSSuc    = 0;
        $upPSFail   = 0;
        $upEsSuc    = 0;
        $upEsFail   = 0;
        $arrId      = [];
        foreach ($datas as $data) {
            $productStatus = 0;
            switch ($data->m_order_type_id) {
                case 3:
                    $productStatus = 13;
                    break;
                case 4:
                    $productStatus = 14;
                    break;
                case 11:
                    $productStatus = 2;
                    break;
                default:
                    break;
            }
            if ($productStatus !== 0) {
                $modelOPD->updateData(
                    $data->receive_id,
                    $data->detail_line_num,
                    $data->sub_line_num,
                    ['product_status' => $productStatus]
                );
                $modelZaiko->where('t_zaiko_id', $data->t_zaiko_id)->update(['filmaker_flg' => 2]);
                $upPSSuc++;
            }
            try {
                $modelEs->updateData(
                    [$data->estimate_code],
                    [
                        'other' => $data->other,
                        'memo'  => $data->memo
                    ]
                );
                $upEsSuc++;
            } catch (\Exception $e) {
                $this->error[] = Utilities::checkMessageException($e);
                $error  = "------------------------------------------" . PHP_EOL;
                $error .= basename(__CLASS__) . PHP_EOL;
                $error .= Utilities::checkMessageException($e);
                $error .= PHP_EOL . "------------------------------------------" . PHP_EOL;
                $slack->notify(new SlackNotification($error));
                Log::error(Utilities::checkMessageException($e));
                print_r("$error");
                $upEsFail++;
                continue;
            }
        }
        Log::info("Update table dt_order_product_detail success: $upPSSuc fail: $upPSFail records");
        print_r("Update table dt_order_product_detail success: $upPSSuc fail: $upPSFail records" . PHP_EOL);
        Log::info("Update table dt_estimation success: $upEsSuc fail: $upEsFail records");
        print_r("Update table dt_estimation success: $upEsSuc fail: $upEsFail records" . PHP_EOL);

        $dataStep2 = $modelOPD->getDataZaikoResult2();
        if (count($dataStep2) !== 0) {
            $receiveId     = '';
            $arrUpdate2    = [];
            $flgCheck      = true;
            $sucUp         = 0;
            $arrUpDetail   = [];
            foreach ($dataStep2 as $data) {
                if ($receiveId !== $data->receive_id && !$flgCheck) {
                    $modelO->updateData(array($receiveId), $arrUpdate2);
                    $sucUp++;
                    $arrUpdate2  = [];
                    $arrUpDetail = [];
                    $flgCheck    = true;
                }
                $receiveId = $data->receive_id;
                if ($data->product_status === PRODUCT_STATUS['OUT_OF_STOCK']) {
                    $arrUpdate2['order_sub_status'] = ORDER_SUB_STATUS['OUT_OF_STOCK'];
                    $flgCheck = false;
                }
                if ($data->product_status === PRODUCT_STATUS['SALE_STOP']) {
                    $arrUpdate2['order_sub_status'] = ORDER_SUB_STATUS['STOP_SALE'];
                    $flgCheck = false;
                }
            }
            if (!$flgCheck && $receiveId !== '') {
                $modelO->updateData(array($receiveId), $arrUpdate2);
                $sucUp++;
            }
            $message = "Update status from [New, Processing] to ".
                       "[Out of stock, Stop sale, Delay] success: $sucUp records";
            Log::info($message);
            print_r($message . PHP_EOL);
        }
        $dataStep3 = $modelO->getDataZaikoResult3();
        if (count($dataStep3) !== 0) {
            $flgCheck         = true;
            $flgOne           = true;
            $receiveId        = '';
            $arrUpdate3       = ['order_sub_status' => ORDER_SUB_STATUS['DONE'], 'is_mail_sent' => 0];
            $arrUpdateRakPay3 = ['order_sub_status' => ORDER_SUB_STATUS['DOING'], 'is_mail_sent' => 0];
            $arrUp = [
                'order_status'     => ORDER_STATUS['DELIVERY'],
                'order_sub_status' => ORDER_SUB_STATUS['NEW'],
                'is_mail_sent'     => 0,
            ];
            $arrUpRakPay = [
                'order_status'     => ORDER_STATUS['ESTIMATION'],
                'order_sub_status' => ORDER_SUB_STATUS['RAKUTEN_PAY'],
                'is_mail_sent'     => 0
            ];
            $arrUpWowma = [
                'order_status'     => ORDER_STATUS['ESTIMATION'],
                'order_sub_status' => ORDER_SUB_STATUS['WOWMA_PAY'],
                'is_mail_sent'     => 0
            ];
            $succ3  = 0;
            $arrkey = [1, 2, 7, 15, 17];
            $paymentMethod      = null;
            $isSourcingOnDemand = null;
            $mallId             = null;
            foreach ($dataStep3 as $data) {
                if ($receiveId !== '' && $receiveId !== $data->receive_id) {
                    if ($flgOne && !in_array($paymentMethod, [2, 6, 7]) && $isSourcingOnDemand === 0) {
                        if ($mallId === 9) {
                            $modelO->updateData(array($receiveId), $arrUpRakPay);
                        } elseif ($mallId === 10) {
                            $modelO->updateData(array($receiveId), $arrUpWowma);
                        } else {
                            $modelO->updateData(array($receiveId), $arrUp);
                        }
                        $succ3++;
                    } elseif ($flgCheck) {
                        if ($mallId === 9 || $mallId === 10) {
                            $modelO->updateData(array($receiveId), $arrUpdateRakPay3);
                        } else {
                            $modelO->updateData(array($receiveId), $arrUpdate3);
                        }
                        $succ3++;
                    }
                    $flgCheck       = true;
                    $flgOne         = true;
                    $paymentMethod  = null;
                    $isSourcingOnDemand = null;
                }
                $mallId         = $data->mall_id;
                $paymentMethod  = $data->payment_method;
                $receiveId      = $data->receive_id;
                $isSourcingOnDemand = $data->is_sourcing_on_demand;
                if (!in_array($data->product_status, $arrkey)) {
                    $flgCheck = false;
                }
                if ($data->product_status !== PRODUCT_STATUS['WAIT_TO_SHIP']) {
                    $flgOne = false;
                }
            }
            if (!empty($receiveId)) {
                if ($flgOne && !in_array($paymentMethod, [2, 6, 7]) && $isSourcingOnDemand === 0) {
                    if ($mallId === 9) {
                        $modelO->updateData(array($receiveId), $arrUpRakPay);
                    } elseif ($mallId === 10) {
                        $modelO->updateData(array($receiveId), $arrUpWowma);
                    } else {
                        $modelO->updateData(array($receiveId), $arrUp);
                    }
                    $succ3++;
                } elseif ($flgCheck) {
                    if ($mallId === 9 || $mallId === 10) {
                        $modelO->updateData(array($receiveId), $arrUpdateRakPay3);
                    } else {
                        $modelO->updateData(array($receiveId), $arrUpdate3);
                    }
                    $succ3++;
                }
            }

            Log::info("Update status from [New, Processing] to [Done] success: $succ3 records");
            print_r("Update status from [New, Processing] to [Done] success: $succ3 records" . PHP_EOL);
        }
        $dataStep4 = $modelO->getDataProcessConfirmRakPay();
        if ($dataStep4->count() !== 0) {
            $arrReOrder = $dataStep4->filter(function ($value, $key) {
                return preg_match('/^re/', $value->received_order_id);
            });
            if (count($arrReOrder) !== 0) {
                $arrOrder = $dataStep4->filter(function ($value, $key) {
                    return !preg_match('/^re/', $value->received_order_id);
                });
                $listReceived = collect($arrOrder->all())->pluck('received_order_id')->toArray();
            } else {
                $listReceived = $dataStep4->pluck('received_order_id')->toArray();
            }
            $arrSteps        = array_chunk($listReceived, 100);
            $success         = 0;
            $fail            = 0;
            $dataCheckUpdate = $dataStep4->keyBy('received_order_id');
            $arrReRun        = [];
            foreach ($arrSteps as $arrReceived) {
                $rakUrl    = "https://api.rms.rakuten.co.jp/es/2.0/order/confirmOrder/";
                $rakHeader = array(
                    "Accept: application/json",
                    "Content-type: application/json; charset=UTF-8",
                    "Authorization: {$this->keyRakuten}"
                );
                $request = array(
                    'orderNumberList' => $arrReceived,
                );
                $requestJson = json_encode($request);
                $ch = curl_init($rakUrl);
                curl_setopt($ch, CURLOPT_POST, true);
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($ch, CURLOPT_HTTPHEADER, $rakHeader);
                curl_setopt($ch, CURLOPT_POSTFIELDS, $requestJson);
                $responseJson = curl_exec($ch);
                curl_close($ch);
                $responseJson = json_decode($responseJson);
                if (isset($responseJson->MessageModelList)) {
                    foreach ($responseJson->MessageModelList as $value) {
                        if ((($value->messageType === 'INFO' && $value->messageCode === 'ORDER_EXT_API_CONFIRM_ORDER_INFO_101') ||
                            ($value->messageType === 'WARN' && $value->messageCode === 'ORDER_EXT_API_CONFIRM_ORDER_WARNING_101')) &&
                            isset($dataCheckUpdate[$value->orderNumber])) {
                            if ($dataCheckUpdate[$value->orderNumber]['order_sub_status'] === ORDER_SUB_STATUS['DOING']) {
                                $modelO->updateDataByReceiveOrder(
                                    [$value->orderNumber],
                                    [
                                        'order_sub_status' => ORDER_SUB_STATUS['DONE'],
                                        'err_text'         => ''
                                    ]
                                );
                            } else {
                                $modelO->updateDataByReceiveOrder(
                                    [$value->orderNumber],
                                    [
                                        'order_status'     => ORDER_STATUS['DELIVERY'],
                                        'order_sub_status' => ORDER_SUB_STATUS['RAKUTEN_PAY'],
                                        'err_text'         => '',
                                        'is_mail_sent'     => 0,
                                    ]
                                );
                            }
                            if (($key = array_search($value->orderNumber, $arrReceived)) !== false) {
                                unset($arrReceived[$key]);
                            }
                            $success++;
                        } else {
                            if (isset($value->orderNumber)) {
                                $flgSlack  = false;
                                $flgDetail = false;
                                $arrUpdate = [];
                                if ($value->messageCode === 'ORDER_EXT_API_CONFIRM_ORDER_ERROR_102') {
                                    $arrUpdate = [
                                        'order_sub_status' => ORDER_SUB_STATUS['ERROR'],
                                        'message_api'      => $value->message,
                                        'err_text'         => $value->message,
                                        'error_code_api'   => $value->messageCode,
                                    ];
                                    $flgSlack = true;
                                } elseif ($value->messageCode === 'ORDER_EXT_API_CONFIRM_ORDER_ERROR_103' ||
                                          $value->messageCode === 'ORDER_EXT_API_CONFIRM_ORDER_ERROR_104') {
                                    $rakUrlGetOrder    = "https://api.rms.rakuten.co.jp/es/2.0/order/getOrder/";
                                    $rakHeaderGetOrder = array(
                                        "Accept: application/json",
                                        "Content-type: application/json; charset=UTF-8",
                                        "Authorization: {$rakAuth}"
                                    );
                                    $requestGetOrder = array(
                                        'orderNumberList' => [$value->orderNumber],
                                    );
                                    $requestJson = json_encode($requestGetOrder);
                                    $ch = curl_init($rakUrlGetOrder);
                                    curl_setopt($ch, CURLOPT_POST, true);
                                    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                                    curl_setopt($ch, CURLOPT_HTTPHEADER, $rakHeaderGetOrder);
                                    curl_setopt($ch, CURLOPT_POSTFIELDS, $requestJson);
                                    $responseJsonGetOrder = curl_exec($ch);
                                    $responseJsonGetOrder = json_decode($responseJsonGetOrder);
                                    $message     = '';
                                    $messageCode = '';
                                    if (isset($responseJsonGetOrder->MessageModelList)) {
                                        if (is_array($responseJsonGetOrder->MessageModelList)) {
                                            foreach ($responseJsonGetOrder->MessageModelList as $error) {
                                                $message     = $error->message;
                                                $messageCode = $error->messageCode;
                                            }
                                        }
                                    }

                                    if (isset($responseJsonGetOrder->OrderModelList) && count($responseJsonGetOrder->OrderModelList) !== 0) {
                                        foreach ($responseJsonGetOrder->OrderModelList as $responseGetOrder) {
                                            if ($responseGetOrder->orderProgress === 900 || $responseGetOrder->orderProgress === 800) {
                                                $arrUpdate = [
                                                    'order_status'        => ORDER_STATUS['CANCEL'],
                                                    'order_sub_status'    => ORDER_SUB_STATUS['DONE'],
                                                    'ship_charge'         => 0,
                                                    'total_price'         => 0,
                                                    'pay_charge_discount' => 0,
                                                    'pay_charge'          => 0,
                                                    'pay_after_charge'    => 0,
                                                    'used_point'          => 0,
                                                    'used_coupon'         => 0,
                                                    'request_price'       => 0,
                                                    'goods_price'         => 0,
                                                    'goods_tax'           => 0,
                                                    'discount'            => 0,
                                                    'message_api'         => $message,
                                                    'error_code_api'      => $messageCode,
                                                ];
                                                $flgDetail = true;
                                                $flgSlack  = true;
                                            } elseif ($responseGetOrder->orderProgress === 300 || $responseGetOrder->orderProgress === 200) {
                                                $arrUpdate = [
                                                    'order_sub_status' => ORDER_SUB_STATUS['DONE'],
                                                    'message_api'      => '',
                                                    'error_code_api'   => '',
                                                ];
                                            } else {
                                                $arrUpdate = [
                                                    'order_sub_status' => ORDER_SUB_STATUS['ERROR'],
                                                    'message_api'      => $message,
                                                    'error_code_api'   => $messageCode,
                                                ];
                                                $flgSlack = true;
                                            }
                                        }
                                    }
                                } else {
                                    $arrUpdate = [
                                        'order_sub_status' => ORDER_SUB_STATUS['ERROR'],
                                        'err_text'         => $value->message
                                    ];
                                    $flgSlack = true;
                                }
                                if (count($arrUpdate) !== 0) {
                                    $modelO->updateDataByReceiveOrder(
                                        [$value->orderNumber],
                                        $arrUpdate
                                    );
                                    if (($key = array_search($value->orderNumber, $arrReceived)) !== false) {
                                        unset($arrReceived[$key]);
                                    }
                                }
                                if ($flgDetail && isset($dataCheckUpdate[$value->orderNumber])) {
                                    DB::transaction(function () use ($modelOD, $modelOPD, $dataCheckUpdate) {
                                        $modelOPD->updateCancelStock($dataCheckUpdate[$value->orderNumber]['receive_id']);
                                        $modelOPD->updateDataByReceive(
                                            [$dataCheckUpdate[$value->orderNumber]['receive_id']],
                                            ['product_status' => PRODUCT_STATUS['CANCEL']]
                                        );
                                        $modelOD->updateData(
                                            ['receive_id' => $dataCheckUpdate[$value->orderNumber]['receive_id']],
                                            [
                                                'quantity' => 0,
                                                'price'    => 0,
                                            ]
                                        );
                                    }, 5);
                                }
                                if ($flgSlack) {
                                    $error  = "------------------------------------------" . PHP_EOL;
                                    $error .= basename(__CLASS__) . PHP_EOL;
                                    $error .= $value->orderNumber . '-' . $value->message;
                                    $error .= PHP_EOL . "------------------------------------------" . PHP_EOL;
                                    $slack->notify(new SlackNotification($error));
                                    //Post to google
                                    $content = "@メッシ大都 " . PHP_EOL;
                                    $content .= basename(__CLASS__) . PHP_EOL;
                                    $content .= $value->orderNumber . ' : ' . $value->message . PHP_EOL;
                                    CurlPost::pushChatGoogle(null, $content);
                                }
                                $fail++;
                            } else {
                                $modelO->updateDataByReceiveOrder(
                                    $arrReceived,
                                    [
                                        'message_api'    => $value->message,
                                        'error_code_api' => $value->messageCode,
                                    ]
                                );
                                $fail += count($arrReceived);
                            }
                        }
                    }
                }
            }
            if (count($arrReOrder) !== 0) {
                foreach ($arrReOrder as $item) {
                    if ($item->order_sub_status === ORDER_SUB_STATUS['DOING']) {
                        $modelO->updateDataByReceiveOrder(
                            [$item->received_order_id],
                            ['order_sub_status' => ORDER_SUB_STATUS['DONE']]
                        );
                    } else {
                        $modelO->updateDataByReceiveOrder(
                            [$item->received_order_id],
                            [
                                'order_status'     => ORDER_STATUS['DELIVERY'],
                                'order_sub_status' => ORDER_SUB_STATUS['RAKUTEN_PAY'],
                                'is_mail_sent'     => 0,
                            ]
                        );
                    }
                    $success++;
                }
            }
            $message = "RAKUTEN PAY Call api confirm order success: $success, fail: $fail records.";
            Log::info($message);
            print_r($message . PHP_EOL);
        }
        $dataStep5 = $modelO->getDataProcessChangeWowmaStatus();
        if ($dataStep5->count() > 0) {
            $environment = 'real';
            if (App::environment(['local', 'test'])) {
                $environment = 'test';
            }
            $shopId    = Config::get("wowma.$environment.shop_id");
            $wowmaAuth = Config::get("wowma.$environment.auth_key");
            $urlApi    = Config::get("wowma.$environment.url");
            $wowmaHeader = array(
                "Content-Type: application/xml; charset=utf-8",
                "Authorization: $wowmaAuth"
            );
            $wowmaSuccess = 0;
            $success      = 0;
            $wowmaFail    = 0;
            foreach ($dataStep5 as $data) {
                if ($data->payment_status === 0 && in_array($data->payment_method, [1, 18, 19])) {
                    $requestXml = view('api.xml.wowma_change_status_order', [
                        'shopId'      => $shopId,
                        'orderId'     => $data->received_order_id,
                        'orderStatus' => '与信待ち'
                    ])->render();
                    $ch = curl_init($urlApi . '/updateTradeStsProc');
                    curl_setopt($ch, CURLOPT_POST, true);
                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                    curl_setopt($ch, CURLOPT_HTTPHEADER, $wowmaHeader);
                    curl_setopt($ch, CURLOPT_POSTFIELDS, $requestXml);
                    $responseXml = curl_exec($ch);
                    curl_close($ch);
                    $responData = simplexml_load_string($responseXml);
                    if ((int)$responData->result->status !== 0) {
                        $errorCode = (string)$responData->result->error->code;
                        $errorMess = (string)$responData->result->error->message;
                        $message = "Change status order wowma [{$data->received_order_id}] has error:" . PHP_EOL;
                        $message .= "[$errorCode] $errorMess";
                        print_r($message . PHP_EOL);
                        Log::error($message);
                        $error  = "------------------------------------------" . PHP_EOL;
                        $error .= basename(__CLASS__) . PHP_EOL;
                        $error .= PHP_EOL . $message;
                        $error .= PHP_EOL . "------------------------------------------" . PHP_EOL;
                        $slack->notify(new SlackNotification($error));
                        $wowmaFail++;
                        $modelO->updateData(
                            [$data->receive_id],
                            [
                                'message_api'    => $errorMess,
                                'error_code_api' => $errorCode,
                            ]
                        );
                        continue;
                    } else {
                        $modelO->updateData([$data->receive_id], [
                            'order_sub_status' => ORDER_SUB_STATUS['DONE'],
                            'message_api'      => '与信待ち_OK'
                        ]);
                    }
                    $wowmaSuccess++;
                } else {
                    if ($data->order_sub_status === ORDER_SUB_STATUS['DOING']) {
                        $modelO->updateData([$data->receive_id], [
                            'order_sub_status' => ORDER_SUB_STATUS['DONE']
                        ]);
                    } else {
                        $modelO->updateData([$data->receive_id], [
                            'order_status'     => ORDER_STATUS['DELIVERY'],
                            'order_sub_status' => ORDER_SUB_STATUS['WOWMA_PAY'],
                            'payment_status'   => 1,
                            'is_mail_sent'     => 0
                        ]);
                    }
                    $success++;
                }
            }
            $message = "Wowma Call api change status order success: $wowmaSuccess, fail: $wowmaFail records." . PHP_EOL;
            $message .= "Update status success: $success" . PHP_EOL;
            Log::info($message);
            print_r($message . PHP_EOL);
        }
        Event::fire(new eCommand($this->signature, array('end' => true, 'error' => $this->error)));
        $totalTime = round(microtime(true) - $start, 2);
        Log::info("End batch process zaiko result with total time: $totalTime s .");
        print_r("End batch process zaiko result with total time: $totalTime s." . PHP_EOL);
    }
}
