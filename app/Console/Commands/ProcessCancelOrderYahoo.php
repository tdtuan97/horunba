<?php
/**
 * Batch process cancel order yahoo
 *
 * @package    App\Console\Commands
 * @subpackage ProcessCancelOrderYahoo
 * @copyright  Copyright (c) 2018 RiverCrane! Corporation. All Rights Reserved.
 * @author     Van Qui<van.qui@rivercrane.vn>
 */

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;
use App\Notifications\SlackNotification;
use App\Custom\Utilities;
use App\Notification;
use App\Events\Command as eCommand;
use Event;
use Config;
use Helper;
use DB;
use App;

use App\Models\YahooOrder\MstOrder;
use App\Models\YahooOrder\DtPaymentList;
use App\Models\Batches\YahooToken;

class ProcessCancelOrderYahoo extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'process:cancel-order-yahoo';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Process cancel order - yahoo';

    /**
     * The notification.
     *
     * @var object
     */
    protected $slack;

    /**
     * The error.
     *
     * @var array
     */
    public $error = [];

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //Create log
        // $arrayReplace = [':', '-'];
        // $folder       = str_replace($arrayReplace, '_', $this->signature);
        // Log::useDailyFiles(storage_path() . "/logs/$folder/history.log");
        // //Process data
        // Event::fire(new eCommand($this->signature, array('start' => true)));
        // Log::info('Start batch process cancel order - yahoo.');
        // print_r("Start batch process cancel order - yahoo." . PHP_EOL);
        // $start       = microtime(true);
        // $this->slack = new Notification(CHANNEL['horunba']);

        // $this->processUpdateCancelAPI();

        // Event::fire(new eCommand($this->signature, array('end' => true, 'error' => $this->error)));
        // $totalTime = round(microtime(true) - $start, 2);
        // Log::info("End batch process cancel order - yahoo with total time: $totalTime s.");
        // print_r("End batch process cancel order - yahoo with total time: $totalTime s.");
    }

    /**
     * Process update cancel api
     *
     * @return void
     */
    private function processUpdateCancelAPI()
    {
        $modelO = new MstOrder();
        $result = $modelO->getCancelOrderAPI()->toArray();
        if (count($result) === 0) {
            $message = "No data.";
            Log::info($message);
            print_r($message . PHP_EOL);
            return;
        }
        $isCallAPI       = true;
        $yahOrderCancels = array();
        $totalYah        = 0;
        $message = "Start process cancel Yahoo.";
        Log::info($message);
        print_r($message . PHP_EOL);
        foreach ($result as $item) {
            if (!in_array($item['received_order_id'], $yahOrderCancels)) {
                $this->cancelYahoo($item, $isCallAPI);
                $totalYah++;
                $yahOrderCancels[] = $item['received_order_id'];
            }
        }
        if ($totalYah > 0) {
            $message = "Process $totalYah Yahoo order cancel.";
            Log::info($message);
            print_r($message . PHP_EOL);
        } else {
            $message = "No data.";
            Log::info($message);
            print_r($message . PHP_EOL);
        }
        $message = "End process cancel Yahoo.";
        Log::info($message);
        print_r($message . PHP_EOL);
    }

    /**
     * Call Api Yahoo cancel
     *
     * @param array   $yahOrder
     * @param boolean $isCallAPI
     * @return int
     */
    public function cancelYahoo($yahOrder, $isCallAPI)
    {
        $modelO     = new MstOrder();
        $modelPL    = new DtPaymentList();
        $yahooToken = new YahooToken();
        try {
            if ($isCallAPI) {
                $accessToken = $yahooToken->find('order')->access_token;
                $yahooHeader = array(
                    "Authorization:Bearer " . $accessToken,
                );
                $yahooUrl = 'https://circus.shopping.yahooapis.jp/ShoppingWebService/V1/orderStatusChange';
                $requestXml = view('api.xml.yahoo_cancel_order', [
                    'orderId'       => $yahOrder['received_order_id'],
                    'sellerId'      => 'diy-tool',
                    'isPointFix'    => false,
                    'operationUser' => 'OPE99999',
                ])->render();
                $ch = curl_init($yahooUrl);
                curl_setopt($ch, CURLOPT_POST, true);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($ch, CURLOPT_HTTPHEADER, $yahooHeader);
                curl_setopt($ch, CURLOPT_POSTFIELDS, $requestXml);
                $responseXml = curl_exec($ch);
                curl_close($ch);
                $responData  = simplexml_load_string($responseXml);
            }

            if ($isCallAPI === true && $responData->getName() === 'Error') {
                $errorCode = Helper::val($responData->Code);
                $message = Helper::val($responData->Message);
                $isMallCancel = 1;
                if (strpos($message, 'error="invalid_token"') !== false) {
                    $isMallCancel = 0;
                }
                $modelO->updateDataByReceiveOrder(
                    [$yahOrder['received_order_id']],
                    [
                        'error_code_api' => $errorCode,
                        'message_api'    => $message,
                        'is_mall_cancel' => $isMallCancel,
                        'up_ope_cd'      => 'OPE99999',
                        'up_date'        => now(),
                    ]
                );
            } else {
                $modelO->updateDataByReceiveOrder(
                    [$yahOrder['received_order_id']],
                    [
                        'error_code_api' => 'OK',
                        'message_api'    => 'OK',
                        'is_mall_cancel' => 1,
                        'up_ope_cd'      => 'OPE99999',
                        'up_date'        => now(),
                    ]
                );
            }
            if ($yahOrder['payment_method'] === 2 && $yahOrder['payment_status'] === 1) {
                $modelPL->updateData(
                    ['receive_order_id' => $yahOrder['received_order_id']],
                    [
                        'need_repayment' => 1,
                        'up_ope_cd'      => 'OPE99999',
                        'up_date'        => now()
                    ]
                );
            }
        } catch (\Exception $e) {
            $message = "Can't update status is finish.";
            print_r($message . PHP_EOL);
            Log::error($message);
            report($e);
            return 0;
        }
    }
}
