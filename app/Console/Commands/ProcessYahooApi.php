<?php
/**
 * Batch process get info from yahoo api
 *
 * @package    App\Console\Commands
 * @subpackage ProcessYahooApi
 * @copyright  Copyright (c) 2018 RiverCrane! Corporation. All Rights Reserved.
 * @author     Van Qui<van.qui@rivercrane.com.vn>
 */

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;
use App\Notifications\SlackNotification;
use App\Custom\Utilities;
use App\Notification;
use App;
use Config;
use Event;
use App\Events\Command as eCommand;
use App\Models\Batches\YahooToken;
use App\Models\Batches\MstOrder;
use App\Models\Backend\DtWebYahData;

// use App\Models\YahooOrder\MstOrder;
// use App\Models\YahooOrder\DtWebYahData;

class ProcessYahooApi extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'process:yahoo-api';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Get info from yahoo api';

    /**
     * The error.
     *
     * @var string
     */
    public $error = [];

    /**
     * Size insert
     * @var int
     */
    private $sizeInsert = 500;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //Create log
        $arrayReplace = [':', '-'];
        $folder       = str_replace($arrayReplace, '_', $this->signature);
        Log::useDailyFiles(storage_path() . "/logs/$folder/history.log");
        //Process data
        Event::fire(new eCommand($this->signature, array('start' => true)));
        Log::info('Start process yahoo api');
        print_r("Start process yahoo api" . PHP_EOL);
        $start   = microtime(true);
        $slack   = new Notification(CHANNEL['horunba']);

        $modelO         = new MstOrder();
        $modelYah       = new DtWebYahData();
        $yahooToken     = new YahooToken();
        $accessToken    = $yahooToken->find('order')->access_token;
        $apiYahooFields = Config::get('common.api_yahoo_fields');
        $fields      = $this->getApiYahooFields($apiYahooFields, ['Status']);
        $strFields   = implode(",", $fields);
        $yahooUrl    = "https://circus.shopping.yahooapis.jp/ShoppingWebService/V1/orderInfo";
        $yahooHeader = array(
            "Authorization:Bearer " . $accessToken,
        );
        $dataOrder = $modelO->getOrderAPI(8, 0, 2);
        $xmlErrors = [];
        $arrInsert = [];
        foreach ($dataOrder as $order) {
            $requestXml = view('mapping.xml.yahoo', [
                'orderId'  => $order->received_order_id,
                'field'    => $strFields,
                'sellerId' => 'diy-tool'
            ])->render();
            $ch = curl_init($yahooUrl);
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_HTTPHEADER, $yahooHeader);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $requestXml);
            $responseXml = curl_exec($ch);
            curl_close($ch);
            $retData = $this->processYahooXml($responseXml, $apiYahooFields);
            if ($retData['flg'] === 0) {
                if (strpos($retData['message'], 'error="invalid_token"') !== false) {
                    $xmlErrors[] = "{$retData['message']}";
                    break;
                } else {
                    $xmlErrors[] = "Order {$order->received_order_id} has error {$retData['code']}: " .
                        "{$retData['message']}";
                }
                continue;
            }
            foreach ($retData['data'] as $dat) {
                $arrInsert[] = $dat;
            }
        }

        if (count($xmlErrors) > 0) {
            $err = implode("\n", $xmlErrors);
            Log::info($err);
            print_r($err . PHP_EOL);
        }

        if (count($arrInsert) > 0) {
            $arrInsert = array_chunk($arrInsert, $this->sizeInsert);
            $flgInsert = true;
            try {
                foreach ($arrInsert as $data) {
                    $modelYah->insert($data);
                }
            } catch (\Exception $e) {
                $this->error[] = Utilities::checkMessageException($e);
                $error  = "------------------------------------------" . PHP_EOL;
                $error .= basename(__CLASS__) . PHP_EOL;
                $error .= Utilities::checkMessageException($e);
                $error .= PHP_EOL . "------------------------------------------" . PHP_EOL;
                $slack->notify(new SlackNotification($error));
                Log::error(Utilities::checkMessageException($e));
                print_r("$error");
                $flgInsert = false;
            }
            if ($flgInsert) {
                Log::info('Insert success');
                print_r('Insert success' . PHP_EOL);
            } else {
                Log::info('Insert fail');
                print_r('Insert fail' . PHP_EOL);
            }
        } else {
            Log::info('No data insert');
            print_r('No data insert' . PHP_EOL);
        }

        Event::fire(new eCommand($this->signature, array('end' => true, 'error' => $this->error)));
        $totalTime = round(microtime(true) - $start, 2);
        Log::info("End batch process yahoo api: $totalTime s.");
        print_r("End batch process yahoo api: $totalTime s.");
    }

    /**
     * Get fields api yahoo
     *
     * @param   array  $apiYahooFields
     * @param   array  $ignoreFields
     * @return  array
     */
    private function getApiYahooFields($apiYahooFields, $ignoreFields = [])
    {
        $fields = array();
        foreach ($apiYahooFields as $key => $val) {
            $curVal = $val;
            do {
                if (is_array($curVal)) {
                    $subKey  = key($curVal);
                    $subVal  = $curVal[$subKey];
                    $curVal  = $subVal;

                    if (!is_array($subVal)) {
                        if ($subVal[0] !== '@') {
                            if (!in_array($subVal, $ignoreFields)) {
                                $fields[$key] = $subVal;
                            }
                        }
                        break;
                    }
                } else {
                    if ($curVal[0] !== '@') {
                        if (!in_array($curVal, $ignoreFields)) {
                            $fields[$key] = $curVal;
                        }
                    }
                    break;
                }
            } while (is_array($curVal));
        }
        return $fields;
    }

    /**
     * Process yahoo xml of response api
     *
     * @param  string $xml
     * @param  array  $apiYahooFields
     * @return array
     */
    private function processYahooXml($responseXml, $apiYahooFields)
    {
        $responData = simplexml_load_string($responseXml);
        if ($responData->getName() === "Error") {
            return [
                'flg' => 0,
                'code' => (string)$responData->Code,
                'message' => (string)$responData->Message
            ];
        }
        $countItem  = count($responData->Result->OrderInfo->Item);
        $retData    = array();
        for ($i = 0; $i < $countItem; $i++) {
            $data = array();
            foreach ($apiYahooFields as $key => $val) {
                $curVal  = $val;
                $resData = $responData;
                do {
                    if (is_array($curVal)) {
                        $subKey  = key($curVal);
                        $subVal  = $curVal[$subKey];
                        $curVal  = $subVal;

                        if ($subKey === 'Item') {
                            $resData = $resData->{$subKey}[$i];
                        } else {
                            $resData = $resData->{$subKey};
                        }
                        if (!is_array($subVal)) {
                            if ($subVal[0] === '@') {
                                $data[$key] = (string)$resData->attributes()->{substr($subVal, 1)};
                            } else {
                                $data[$key] = (string)$resData->{$subVal};
                            }
                            break;
                        }
                    } else {
                        if ($curVal[0] === '@') {
                            $data[$key] = (string)$resData->attributes()->{substr($curVal, 1)};
                        } else {
                            $data[$key] = (string)$resData->{$curVal};
                        }
                        break;
                    }
                } while (is_array($curVal));
            }
            $retData[] = $data;
        }
        return [
            'flg' => 1,
            'data' => $retData
        ];
    }
}
