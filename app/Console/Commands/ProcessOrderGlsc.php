<?php
/**
 * Batch process order GLSC
 *
 * @package    App\Console\Commands
 * @subpackage ProcessOrderGlsc
 * @copyright  Copyright (c) 2017 RiverCrane! Corporation. All Rights Reserved.
 * @author     Truong Nghia<truong.van.nghia@rivercrane.vn>
 */

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\Batches\MstOrder;
use App\Models\Batches\DtOrderProductDetail;
use App\Models\Batches\DtDelivery;
use App\Models\Batches\DtDeliveryDetail;
use App\Models\Batches\TInsertShippingGroupKey;
use App\Models\Batches\TInsertShippingHrnb;
use App\Models\Batches\TInsertShipping;
use App\Models\Batches\MstOrderDetail;
use App\Models\Batches\MstProduct;
use App\Models\Batches\MstPaymentMethod;
use Illuminate\Support\Facades\Log;
use App\Notifications\SlackNotification;
use App\Events\Command as eCommand;
use App\Custom\Utilities;
use App\Notification;
use Config;
use Event;
use DB;
use App\Custom\ProcessApiGLSC;
use App\Models\Batches\MstStatusBatch;
use App;
use App\Custom\CurlPost;
use App\Models\Batches\MstRakutenKey;

class ProcessOrderGlsc extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'process:order-glsc';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'System request order to supplier(GLSC) to get the best delivery to customer';
    /**
     * The key rakuten
     *
     * @var string
     */
    protected $keyRakuten = '';
    /**
     * The notification.
     *
     * @var object
     */
    protected $slack;

    /**
     * The limit of step process.
     *
     * @var int
     */
    protected $limit = 500;

    /**
     * The error.
     *
     * @var array
     */
    public $error = [];

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //Create log
        $arrayReplace = [':', '-'];
        $folder       = str_replace($arrayReplace, '_', $this->signature);
        $this->slack  = new Notification(CHANNEL['horunba']);
        Log::useDailyFiles(storage_path() . "/logs/$folder/history.log");
        //Check cron
        $modelStatus = new MstStatusBatch;
        $check = $modelStatus->getDataBySignature($this->signature);
        if ($check->status_flag === 1) {
            Log::info('This batch running->Stop this batch');
            print_r("This batch running->Stop this batch" . PHP_EOL);
            $error  = "------------------------------------------" . PHP_EOL;
            $error .= basename(__CLASS__) . PHP_EOL;
            $error .= 'This batch running->Stop this batch' . PHP_EOL;
            $error .= PHP_EOL . "------------------------------------------" . PHP_EOL;
            $this->slack->notify(new SlackNotification($error));
            return;
        }
        //Process data
        Event::fire(new eCommand($this->signature, array('start' => true)));
        Log::info('Start batch process order GLSC.');
        print_r("Start batch process order GLSC." . PHP_EOL);
        $start           = microtime(true);
        $modelRakutenKey  = new MstRakutenKey();
        $this->keyRakuten = $modelRakutenKey->getKeyRakuten();
        Log::info('=== Start process check status order to delivery. ===');
        print_r("=== Start process check status order to delivery. ===" . PHP_EOL);
        $this->checkStatusOrder();
        Log::info('=== End process check status order to delivery. ===');
        print_r("=== End process check status order to delivery. ===" . PHP_EOL);

        Log::info('=== Start process call Api check status Rakuten pay. ===');
        print_r("=== Start process call Api check status Rakuten pay. ===" . PHP_EOL);
        $this->processCheckStatusRakutenPay();
        Log::info('=== End process call Api check status Rakuten pay. ===');
        print_r("=== End process call Api check status Rakuten pay. ===" . PHP_EOL);

        Log::info('=== Start process call Api change status Wowma. ===');
        print_r("=== Start process call Api change status Wowma. ===" . PHP_EOL);
        $this->processChangeStatusWowma();
        Log::info('=== End process call Api change status Wowma. ===');
        print_r("=== End process call Api change status Wowma. ===" . PHP_EOL);

        Log::info('=== Start process request GLSC to delivery insert into dt_delivery. ===');
        print_r("=== Start process request GLSC to delivery insert into dt_delivery. ===" . PHP_EOL);
        $this->processGlsc();
        Log::info('=== End process request GLSC to delivery insert into dt_delivery. ===');
        print_r("=== End process request GLSC to delivery insert into dt_delivery. ===" . PHP_EOL);
        Log::info('=== Start process insert shipping. ===');
        print_r("=== Start process insert shipping. ===" . PHP_EOL);
        $this->processInsertShipping();
        Log::info('=== End process insert shipping. ===');
        print_r("=== End process insert shipping. ===" . PHP_EOL);
        Event::fire(new eCommand($this->signature, array('end' => true, 'error' => $this->error)));
        $totalTime = round(microtime(true) - $start, 2);
        Log::info("End batch process order GLSC with total time: $totalTime s.");
        print_r("End batch process order GLSC with total time: $totalTime s.");
    }

    /**
    * Process check status order
    * @return void
    */
    public function checkStatusOrder()
    {
        $modelO        = new MstOrder();
        $datas         = $modelO->getDataGlscCheckStatus();
        $arrReceive    = [];
        $arrReceivePay = [];
        $arrWowma      = [];
        if (count($datas) !== 0) {
            $arrUpdate = [
                'order_status'     => ORDER_STATUS['DELIVERY'],
                'order_sub_status' => ORDER_SUB_STATUS['NEW'],
                'is_mail_sent'     => 0,
            ];
            $arrUpdatePay = [
                'order_status'     => ORDER_STATUS['DELIVERY'],
                'order_sub_status' => ORDER_SUB_STATUS['RAKUTEN_PAY'],
                'is_mail_sent'     => 0,
            ];
            $arrUpdateWowma = [
                'order_status'     => ORDER_STATUS['DELIVERY'],
                'order_sub_status' => ORDER_SUB_STATUS['WOWMA_PAY'],
                'is_mail_sent'     => 0,
            ];
            $success = 0;
            foreach ($datas as $data) {
                if (is_null($data->is_available) ||
                    (!is_null($data->is_available) && $data->is_available === $data->is_mail_sent)) {
                    if ($data->mall_id === 9) {
                        $arrReceivePay[] = $data->receive_id;
                    } elseif ($data->mall_id === 10) {
                        $arrWowma[] = $data->receive_id;
                    } else {
                        $arrReceive[] = $data->receive_id;
                    }
                }
            }
            $arrReceive    = array_unique($arrReceive);
            $arrReceivePay = array_unique($arrReceivePay);

            if (count($arrReceive) !== 0) {
                $modelO->updateData($arrReceive, $arrUpdate);
                $success += count($arrReceive);
            }
            if (count($arrReceivePay) !== 0) {
                $modelO->updateData($arrReceivePay, $arrUpdatePay);
                $success += count($arrReceivePay);
            }
            if (count($arrWowma) !== 0) {
                $modelO->updateData($arrWowma, $arrUpdateWowma);
                $success += count($arrWowma);
            }
            Log::info("Update status from [Order] to [Delivery] success: $success records.");
            print_r("Update status from [Order] to [Delivery] success: $success records." . PHP_EOL);
        } else {
            Log::info('No data');
            print_r("No data" . PHP_EOL);
        }
    }

    /**
    * Process call Api check status Rakuten pay
    * @return void
    */
    public function processCheckStatusRakutenPay()
    {
        $modelO   = new MstOrder();
        $modelOPD = new DtOrderProductDetail();
        $modelOD  = new MstOrderDetail();
        $modelMPM = new MstPaymentMethod();
        $datas  = $modelO->getDataCheckStatusRaKPay();
        if ($datas->count() === 0) {
            $message = 'No data';
            Log::info($message);
            print_r($message . PHP_EOL);
        } else {
            $arrPayment = $modelMPM->getDataByMall(9);
            $arrReOrder = $datas->filter(function ($value, $key) {
                return preg_match('/^re/', $value->received_order_id);
            });
            if (count($arrReOrder) !== 0) {
                $arrOrder = $datas->filter(function ($value, $key) {
                    return !preg_match('/^re/', $value->received_order_id);
                });
                $listReceived = collect($arrOrder->all())->pluck('received_order_id')->toArray();
            } else {
                $listReceived = $datas->pluck('received_order_id')->toArray();
            }
            $arrSteps = array_chunk($listReceived, 100);
            $success   = 0;
            foreach ($arrSteps as $listOrder) {
                $rakUrl    = "https://api.rms.rakuten.co.jp/es/2.0/order/getOrder/";
                $rakHeader = array(
                    "Accept: application/json",
                    "Content-type: application/json; charset=UTF-8",
                    "Authorization: {$this->keyRakuten}"
                );
                $request = array(
                    'orderNumberList' => $listOrder,
                );
                $requestJson = json_encode($request);
                $ch = curl_init($rakUrl);
                curl_setopt($ch, CURLOPT_POST, true);
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($ch, CURLOPT_HTTPHEADER, $rakHeader);
                curl_setopt($ch, CURLOPT_POSTFIELDS, $requestJson);
                $responseJson = curl_exec($ch);
                curl_close($ch);
                $responseJson = json_decode($responseJson);
                $message      = '';
                $messageCode  = '';
                if (isset($responseJson->MessageModelList)) {
                    if (is_array($responseJson->MessageModelList)) {
                        foreach ($responseJson->MessageModelList as $error) {
                            if ($error->messageType === 'ERROR' && isset($error->orderNumber)) {
                                $modelO->updateDataByReceiveOrder(
                                    [$error->orderNumber],
                                    [
                                        'error_code_api'   => $error->messageCode,
                                        'message_api'      => $error->message,
                                        'up_ope_cd'        => 'OPE99999',
                                        'up_date'          => now(),
                                    ]
                                );
                                $message = $error->orderNumber . ' - ' . $error->message;
                                $error   = "------------------------------------------" . PHP_EOL;
                                $error  .= basename(__CLASS__) . PHP_EOL;
                                $error  .= 'Get order fail' . PHP_EOL;
                                $error  .= $message;
                                $error  .= PHP_EOL . "------------------------------------------" . PHP_EOL;
                                $this->slack->notify(new SlackNotification($error));
                            } elseif ($error->messageType === 'INFO') {
                                $message     = $error->message;
                                $messageCode = $error->messageCode;
                            }
                        }
                    }
                }
                if (isset($responseJson->OrderModelList) && count($responseJson->OrderModelList) !== 0) {
                    foreach ($responseJson->OrderModelList as $response) {
                        foreach ($response->PackageModelList as $packageModel) {
                            if (!empty($packageModel->ShippingModelList)) {
                                $shippingData = $packageModel->ShippingModelList;
                                $shippingData = $shippingData[0];
                                $shippingDetailId = $shippingData->shippingDetailId;
                                $modelO->updateDataByReceiveOrder(
                                    [$response->orderNumber],
                                    ['ship_id_rakuten' => $shippingDetailId]
                                );
                            }
                        }
                        if ($response->orderProgress === 300) {
                            $arrUpdate = [
                                'order_sub_status' => ORDER_SUB_STATUS['NEW'],
                                'payment_status'   => 1,
                                'error_code_api'   => $messageCode,
                                'message_api'      => $message,
                                'up_ope_cd'        => 'OPE99999',
                                'up_date'          => now(),
                            ];
                            $paymentData = $modelO->getPaymentMethod($response->orderNumber, 9);
                            if (empty($paymentData)) {
                                $paymentMethod = 0;
                                foreach ($arrPayment as $pay) {
                                    if ($response->SettlementModel->settlementMethod === $pay->payment_name) {
                                        $paymentMethod = $pay->payment_code;
                                        break;
                                    }
                                }
                                 $arrUpdate['payment_method'] = $paymentMethod;
                            } else {
                                if ($response->SettlementModel->settlementMethod !== $paymentData->payment_name) {
                                    $arrUpdate['used_coupon']    = $response->couponAllTotalPrice;
                                    $arrUpdate['used_point']     = $this->checkIsset($response->PointModel->usedPoint, '');
                                    $arrUpdate['total_price']    = $response->totalPrice + $response->deliveryPrice;
                                    $arrUpdate['request_price']  = $response->requestPrice;
                                    $arrUpdate['ship_charge']    = $response->postagePrice;
                                    $arrUpdate['goods_price']    = $response->goodsPrice + $response->goodsTax;
                                    $arrUpdate['payment_method'] = $modelMPM->getPaymentMethod(9, $response->SettlementModel->settlementMethod)->payment_code;
                                }
                            }
                            $modelO->updateDataByReceiveOrder(
                                [$response->orderNumber],
                                $arrUpdate
                            );
                            $success++;
                        } elseif ($response->orderProgress === 900 || $response->orderProgress=== 800) {
                            DB::transaction(function () use ($modelO, $modelOD, $modelOPD, $message, $messageCode, $response) {
                                $arrUpdate = [
                                    'order_status'        => ORDER_STATUS['CANCEL'],
                                    'order_sub_status'    => ORDER_SUB_STATUS['DONE'],
                                    'ship_charge'         => 0,
                                    'total_price'         => 0,
                                    'pay_charge_discount' => 0,
                                    'pay_charge'          => 0,
                                    'pay_after_charge'    => 0,
                                    'used_point'          => 0,
                                    'used_coupon'         => 0,
                                    'request_price'       => 0,
                                    'goods_price'         => 0,
                                    'goods_tax'           => 0,
                                    'discount'            => 0,
                                    'is_mall_cancel'      => 1,
                                    'message_api'         => $message,
                                    'error_code_api'      => $messageCode,
                                ];
                                $modelO->updateDataByReceiveOrder(
                                    [$response->orderNumber],
                                    $arrUpdate
                                );
                                $dataOrder = $modelO->where('received_order_id', $response->orderNumber)->first();
                                if (!empty($dataOrder)) {
                                    $modelOPD->updateCancelStock($dataOrder->receive_id);
                                    $modelOPD->updateDataByReceive(
                                        [$dataOrder->receive_id],
                                        ['product_status' => PRODUCT_STATUS['CANCEL']]
                                    );
                                    $modelOD->updateData(
                                        ['receive_id' => $dataOrder->receive_id],
                                        [
                                            'quantity' => 0,
                                            'price'    => 0,
                                        ]
                                    );
                                }
                            }, 5);
                        }
                    }
                }
            }
            if (count($arrReOrder) !== 0) {
                foreach ($arrReOrder as $reOrder) {
                    $modelO->updateDataByReceiveOrder(
                        [$reOrder->received_order_id],
                        [
                            'order_sub_status' => ORDER_SUB_STATUS['NEW'],
                            'up_ope_cd'        => 'OPE99999',
                            'up_date'          => now(),
                        ]
                    );
                }
            }
            $message = "Call Api and update sub status success: $success records.";
            Log::info($message);
            print_r($message . PHP_EOL);
        }
    }

    /**
     * Process change status Wowma
     *
     * @return void
     */
    public function processChangeStatusWowma()
    {
        $modelO = new MstOrder();
        $datas  = $modelO->getDataWowmaGlsc();
        if ($datas->count() === 0) {
            Log::info('No data');
            print_r("No data" . PHP_EOL);
            return;
        }

        $environment = 'real';
        if (App::environment(['local', 'test'])) {
            $environment = 'test';
        }
        $shopId    = Config::get("wowma.$environment.shop_id");
        $wowmaAuth = Config::get("wowma.$environment.auth_key");
        $urlApi    = Config::get("wowma.$environment.url");
        $wowmaHeader = array(
            "Content-Type: application/xml; charset=utf-8",
            "Authorization: $wowmaAuth"
        );
        $wowmaSuccess = 0;
        $wowmaFail    = 0;
        foreach ($datas as $data) {
            if (($data->payment_status === 1)||($data->payment_method === 3)||($data->payment_method === 10)) {
                $requestXml = view('api.xml.wowma_change_status_order', [
                    'shopId'      => $shopId,
                    'orderId'     => $data->received_order_id,
                    'orderStatus' => '完了'
                ])->render();
                $ch = curl_init($urlApi . '/updateTradeStsProc');
                curl_setopt($ch, CURLOPT_POST, true);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($ch, CURLOPT_HTTPHEADER, $wowmaHeader);
                curl_setopt($ch, CURLOPT_POSTFIELDS, $requestXml);
                $responseXml = curl_exec($ch);
                curl_close($ch);
                $responData = simplexml_load_string($responseXml);
                if ((int)$responData->result->status !== 0) {
                    $errorCode = (string)$responData->result->error->code;
                    $errorMess = (string)$responData->result->error->message;
                    $message = "Change status order wowma [{$data->received_order_id}] has error:" . PHP_EOL;
                    $message .= "[$errorCode] $errorMess";
                    print_r($message . PHP_EOL);
                    Log::error($message);
                    $error  = "------------------------------------------" . PHP_EOL;
                    $error .= basename(__CLASS__) . PHP_EOL;
                    $error .= PHP_EOL . $message;
                    $error .= PHP_EOL . "------------------------------------------" . PHP_EOL;
                    $this->slack->notify(new SlackNotification($error));
                    $wowmaFail++;
                    $modelO->updateData([$data->receive_id], [
                        'message_api'    => $errorMess,
                        'error_code_api' => $errorCode,
                    ]);
                    continue;
                }
                $modelO->updateData([$data->receive_id], [
                    'order_sub_status' => ORDER_SUB_STATUS['NEW'],
                    'message_api'      => '完了_OK'
                ]);
                $wowmaSuccess++;
            }
        }
        $message = "Process glsc Wowma success: {$wowmaSuccess}, fail: {$wowmaFail} records.";
        Log::info($message);
        print_r($message . PHP_EOL);
    }

    /**
    * Process request GLSC to delivery insert into dt_delivery
    * @param $arrReceive array List receive check
    * @return void
    */
    public function processGlsc()
    {
        $modelO   = new MstOrder();
        $modelD   = new DtDelivery();
        $modelP   = new MstProduct();
        $modelOPD = new DtOrderProductDetail();
        $modelDD  = new DtDeliveryDetail();
        $modelKey = new TInsertShippingGroupKey();
        $datas    = $modelO->getDataProcessGLSC();
        if (count($datas) === 0) {
            Log::info('No data');
            print_r("No data" . PHP_EOL);
            return;
        }
        $arrDelivery    = [];
        $arrDeliDetail  = [];
        $arrShipping    = [];
        $receiveId      = '';
        $arrCusId       = [];
        $groupKey      = 0;

        foreach ($datas as $key => $data) {
            if ($receiveId === ''|| $receiveId !== $data->receive_id) {
                $modelKey->where('tShippingG_Key', '=', 1)->increment('GroupKey', 1);
                $groupKey = $modelKey->where('tShippingG_Key', '=', 1)->first();
                $receiveId                    = $data->receive_id;
                $arrCusId                     = [];
                $arrCusId[$data->receiver_id] = $data->delivery_count + 1;
            }
            $keyUpdate    = [
                'receive_id' => $data->receive_id,
                'detail_line_num' => $data->detail_line_num,
                'sub_line_num' => $data->sub_line_num
            ];
            $deliveryTime = $data->ship_wish_time;
            $arrayRepTime = ['(:(.+?)-)', '(:(.+?)*)'];
            $account      = '';
            if ($data->payment_method === 3) {
                $account = '代引';
            }
            $subdivisionNum = 0;
            if (array_key_exists($data->receiver_id, $arrCusId)) {
                $subdivisionNum = $arrCusId[$data->receiver_id];
            } else {
                $subdivisionNum = end($arrCusId) + 1;
                $arrCusId[$data->receiver_id] = $subdivisionNum;
            }
            $deliveryDate = null;
            $deliveryPlanDate = null;
            if (!empty($data->ship_wish_date)) {
                $deliDays = (int)$data->deli_days;
                $cal = strtotime($data->ship_wish_date . '-' . $deliDays . ' days');
               //strtotime(date('Y-m-d')." +1 days")
                if ($cal >= strtotime(date('Y-m-d'))) {
                    $deliveryDate = date('Ymd', strtotime($data->ship_wish_date));
                }
            }
            $shipmentDate = date('Ymd', strtotime($data->shipment_date));
            if (strtotime($shipmentDate) <= strtotime(date('Ymd'))) {
                $hour = date('H:i');
                if ($hour < '15:00') {
                    $deliveryPlanDate = date('Ymd');
                } else {
                    $deliveryPlanDate = date('Ymd', strtotime("+1 days"));
                }
            } elseif (strtotime($shipmentDate) > strtotime(date('Ymd')." +1 days")) {
                if (empty($data->ship_wish_date)) {
                    $deliveryPlanDate = date('Ymd', strtotime("+1 days"));
                    $modelO->updateData([$receiveId], ['shipment_date' => date('Y-m-d', strtotime("+1 days"))]);
                } else {
                    $deliveryPlanDate = $shipmentDate;
                }
            } else {
                if (!empty($data->shipment_date)) {
                    $deliveryPlanDate = $shipmentDate;
                }
            }
            //if ($data->arrive_type === 1 &&
            if (date('Ymd', strtotime($data->order_date)) === date('Ymd') &&
                date('H:i', strtotime($data->order_date)) <= '14:00' &&
                date('H:i', strtotime($data->order_date)) > '13:30' &&
                date('H:i') < '15:10'
            ) {
                $deliveryPlanDate = date('Ymd');
            }
            /*--------Check holiday temp--------*/
            if ($deliveryPlanDate === '20181224' || $deliveryPlanDate === '20190101') {
                $deliveryPlanDate = date('Ymd', strtotime($deliveryPlanDate . " +1 days"));
            }
            /*-----------------------------*/
            $shippingInstructions = $data->delivery_instrustions . ' 「無」';

            $keyD = $receiveId . $subdivisionNum;
            $arrDelivery[$keyD]['received_order_id']     = $data->received_order_id;
            $arrDelivery[$keyD]['subdivision_num']       = $subdivisionNum;
            $arrDelivery[$keyD]['detail_line_num']       = 0;
            $arrDelivery[$keyD]['delivery_date']         = $deliveryDate;
            $arrDelivery[$keyD]['delivery_time']         = $deliveryTime;
            $arrDelivery[$keyD]['delivery_instrustions'] = null;
            $arrDelivery[$keyD]['account']               = $account;
            $arrDelivery[$keyD]['delivery_plan_date']    = $deliveryPlanDate;
            $arrDelivery[$keyD]['total_price']           = $data->request_price;
            $arrDelivery[$keyD]['shipping_instructions'] = $shippingInstructions;
            $arrDelivery[$keyD]['store_code']            = 1;
            $arrDelivery[$keyD]['ship_num']              = 0;
            $arrDelivery[$keyD]['invoice_remarks']       = '';
            $arrDelivery[$keyD]['delivery_code']         = $data->delivery_code;
            $arrDelivery[$keyD]['customer_name']         = $data->c_nm;
            $arrDelivery[$keyD]['delivery_name']         = $data->d_nm;
            $arrDelivery[$keyD]['delivery_postal']       = $data->d_postal;
            $arrDelivery[$keyD]['delivery_perfecture']   = $data->prefecture;
            $arrDelivery[$keyD]['delivery_city']         = empty($data->city) ? $data->sub_address : $data->city;
            $arrDelivery[$keyD]['delivery_sub_address']  = $data->sub_address.$data->company_name;
            $arrDelivery[$keyD]['delivery_tel']          = $data->tel_num;
            $arrDelivery[$keyD]['error_code']            = null;
            $arrDelivery[$keyD]['error_message']         = null;
            $arrDelivery[$keyD]['delivery_status']       = 0;
            $arrDelivery[$keyD]['group_key']             = $groupKey->GroupKey;
            $arrDelivery[$keyD]['process_date']          = null;
            $arrDelivery[$keyD]['in_ope_cd']             = 'OPE99999';
            $arrDelivery[$keyD]['in_date']               = now();
            $arrDelivery[$keyD]['up_ope_cd']             = 'OPE99999';
            $arrDelivery[$keyD]['up_date']               = now();
            //Data insert dt_delivery_detail
            $arrTemp = [];
            $arrTemp['received_order_id'] = $data->received_order_id;
            $arrTemp['subdivision_num']   = $subdivisionNum;
            $arrTemp['detail_line_num']   = $data->sub_line_num;
            $arrTemp['product_code']      = $data->product_code;
            $arrTemp['product_name']      = $data->product_name_long;
            $arrTemp['product_jan']       = $data->product_jan;
            $arrTemp['maker_name']        = $data->maker_full_nm;
            $arrTemp['maker_code']        = $data->product_maker_code;
            $arrTemp['product_image_url'] = $data->rak_img_url_1;
            $arrTemp['delivery_num']      = $data->received_order_num;
            $arrTemp['record_tag']        = 0;
            $arrTemp['in_ope_cd']         = 'OPE99999';
            $arrTemp['in_date']           = now();
            $arrTemp['up_ope_cd']         = 'OPE99999';
            $arrTemp['up_date']           = now();
            $arrDelivery[$keyD]['detail'][] = $arrTemp;
            $arrDelivery[$keyD]['update'][] = $keyUpdate;
            if ($data->product_oogata >= 1) {
                $arrDelivery[$keyD]['product_oogata'][] = $data->product_oogata;
            }
        }
        $total = count($arrDelivery);
        if ($total !== 0) {
            $success = 0;
            $fail    = 0;
            foreach ($arrDelivery as $item) {
                $arrDeliveryDetail = $item['detail'];
                unset($item['detail']);
                $arrKeyUpdate = $item['update'];
                unset($item['update']);
                if (isset($item['product_oogata'])) {
                    $item['delivery_code'] = 3;
                    unset($item['product_oogata']);
                }
                try {
                    DB::beginTransaction();
                    $modelD->insert($item);
                    $modelDD->insert($arrDeliveryDetail);
                    foreach ($arrKeyUpdate as $keyUpdate) {
                        $modelOPD->updateDataByCondition(
                            $keyUpdate,
                            ['product_status' => PRODUCT_STATUS['SHIPPING']]
                        );
                        $modelO->updateData(
                            [$keyUpdate['receive_id']],
                            ['order_sub_status' => ORDER_SUB_STATUS['DOING']]
                        );
                    }
                    $success++;
                    DB::commit();
                } catch (\Exception $e) {
                    DB::rollback();
                    report($e);
                    $code = (int)$e->getCode();
                    //Process duplicate for table dt_delivery
                    if ($code === 23000) {
                        $modelO->updateDataByReceiveOrder(
                            [$item['received_order_id']],
                            ['order_sub_status' => ORDER_SUB_STATUS['ERROR_MD']]
                        );
                    }
                    $fail++;
                }
            }
        }
        Log::info("Process success: $success and fail: $fail records.");
        print_r("Process success: $success and fail: $fail records." . PHP_EOL);
    }

    public function processInsertShipping()
    {
        $modelD  = new DtDelivery();
        $arrTemp = [];
        $datas   = $modelD->getDataCheckDeliveryDetail();
        $message = '----- Check delivery detail -----';
        Log::info($message);
        print_r($message . PHP_EOL);
        $total = count($datas);
        if ($total === 0) {
            $message = 'No data';
            Log::info($message);
            print_r($message . PHP_EOL);
        } else {
            $arrUpdate = [
                'error_code'      => '20000',
                'error_message'   => 'No data delivery detail',
                'delivery_status' => 9,
                'up_ope_cd'       => 'OPE99999',
                'up_date'         => now(),
            ];
            $success = 0;
            foreach ($datas as $value) {
                $modelD->updateData(
                    [
                        'received_order_id' => $value->received_order_id,
                        'subdivision_num'   => $value->subdivision_num,
                    ],
                    $arrUpdate
                );
                $arrTemp[] = $value->received_order_id . '-' . $value->subdivision_num;
                $success++;
            }
            $message = "Process $total records and success: $success records.";
            Log::info($message);
            print_r($message . PHP_EOL);
        }
        $message = '----- End check delivery detail -----';
        Log::info($message);
        print_r($message . PHP_EOL);

        $datas1 = $modelD->getDataCheckDateIsPast();
        $message = '----- Check delivery can not is past -----';
        Log::info($message);
        print_r($message . PHP_EOL);
        $total = count($datas1);
        if ($total === 0) {
            $message = 'No data';
            Log::info($message);
            print_r($message . PHP_EOL);
        } else {
            $arrUpdate1 = [
                'error_code'      => '20001',
                'error_message'   => '出荷予定日は本日以降の日付で入力してください',
                'delivery_status' => 9,
                'up_ope_cd'       => 'OPE99999',
                'up_date'         => now(),
            ];
            $success = 0;
            foreach ($datas1 as $value) {
                $modelD->updateData(
                    [
                        'received_order_id' => $value->received_order_id,
                        'subdivision_num'   => $value->subdivision_num,
                    ],
                    $arrUpdate1
                );
                $arrTemp[] = $value->received_order_id . '-' . $value->subdivision_num;
                $success++;
            }
            $message = "Process $total records and success: $success records.";
            Log::info($message);
            print_r($message . PHP_EOL);
        }

        $message = '----- End check delivery can not is past -----';
        Log::info($message);
        print_r($message . PHP_EOL);
        $message = '----- Check digits of JAN code is not equal 13 -----';
        Log::info($message);
        print_r($message . PHP_EOL);
        $datas2 = $modelD->getDataCheckDigits();
        $total = count($datas2);
        if ($total === 0) {
            $message = 'No data';
            Log::info($message);
            print_r($message . PHP_EOL);
        } else {
            $arrUpdate2 = [
                'error_code'      => '20002',
                'error_message'   => 'ＪＡＮコードの桁数は８文字か１３文字で入力してください',
                'delivery_status' => 9,
                'up_ope_cd'       => 'OPE99999',
                'up_date'         => now(),
            ];
            $success = 0;
            foreach ($datas2 as $value) {
                $modelD->updateData(
                    [
                        'received_order_id' => $value->received_order_id,
                        'subdivision_num'   => $value->subdivision_num,
                    ],
                    $arrUpdate2
                );
                $arrTemp[] = $value->received_order_id . '-' . $value->subdivision_num;
                $success++;
            }
            $message = "Process $total records and success: $success records.";
            Log::info($message);
            print_r($message . PHP_EOL);
        }
        $message = '----- End check digits of JAN code is not equal 13 -----';
        Log::info($message);
        print_r($message . PHP_EOL);
        $message = '----- Check total price equal 0 -----';
        Log::info($message);
        print_r($message . PHP_EOL);
        $datas3 = $modelD->getDataCheckTotal();
        $total = count($datas3);
        if ($total === 0) {
            $message = 'No data';
            Log::info($message);
            print_r($message . PHP_EOL);
        } else {
            $arrUpdate3 = [
                'error_code'      => '20003',
                'error_message'   => '代引ですが、金額が０なっています。',
                'delivery_status' => 9,
                'up_ope_cd'       => 'OPE99999',
                'up_date'         => now(),
            ];
            $success = 0;
            foreach ($datas3 as $value) {
                $modelD->updateData(
                    [
                        'received_order_id' => $value->received_order_id,
                        'subdivision_num'   => $value->subdivision_num,
                    ],
                    $arrUpdate3
                );
                $arrTemp[] = $value->received_order_id . '-' . $value->subdivision_num;
                $success++;
            }
            $message = "Process $total records and success: $success records.";
            Log::info($message);
            print_r($message . PHP_EOL);
        }
        $message = '----- End check total price equal 0 -----';
        Log::info($message);
        print_r($message . PHP_EOL);

        $message = '----- Start check delivery code -----';
        Log::info($message);
        print_r($message . PHP_EOL);
        $datas4 = $modelD->getDataCheckDeliveryCode();
        $total = count($datas4);
        if ($total === 0) {
            $message = 'No data';
            Log::info($message);
            print_r($message . PHP_EOL);
        } else {
            $arrUpdate4 = [
                'error_code'      => '20004',
                'error_message'   => 'delivery_codeが違います。',
                'delivery_status' => 9,
                'up_ope_cd'       => 'OPE99999',
                'up_date'         => now(),
            ];
            $success = 0;
            foreach ($datas4 as $value) {
                $modelD->updateData(
                    [
                        'received_order_id' => $value->received_order_id,
                        'subdivision_num'   => $value->subdivision_num,
                    ],
                    $arrUpdate4
                );
                $arrTemp[] = $value->received_order_id . '-' . $value->subdivision_num;
                $success++;
            }
            $message = "Process $total records and success: $success records.";
            Log::info($message);
            print_r($message . PHP_EOL);
        }
        $message = '----- End check delivery code -----';
        Log::info($message);
        print_r($message . PHP_EOL);

        $message = '----- Start check delivery city -----';
        Log::info($message);
        print_r($message . PHP_EOL);
        $datas5 = $modelD->getDataCheckDeliveryCity();
        $total = count($datas5);
        if ($total === 0) {
            $message = 'No data';
            Log::info($message);
            print_r($message . PHP_EOL);
        } else {
            $arrUpdate5 = [
                'error_code'      => '20005',
                'error_message'   => 'delivery_cityに入れてください。',
                'delivery_status' => 9,
                'up_ope_cd'       => 'OPE99999',
                'up_date'         => now(),
            ];
            $success = 0;
            foreach ($datas5 as $value) {
                $modelD->updateData(
                    [
                        'received_order_id' => $value->received_order_id,
                        'subdivision_num'   => $value->subdivision_num,
                    ],
                    $arrUpdate5
                );
                $arrTemp[] = $value->received_order_id . '-' . $value->subdivision_num;
                $success++;
            }
            $message = "Process $total records and success: $success records.";
            Log::info($message);
            print_r($message . PHP_EOL);
        }
        $message = '----- End check delivery city -----';
        Log::info($message);
        print_r($message . PHP_EOL);

        $message = '----- Start check delivery num -----';
        Log::info($message);
        print_r($message . PHP_EOL);
        $datas6 = $modelD->getDataCheckDeliveryNum();
        $total = count($datas6);
        if ($total === 0) {
            $message = 'No data';
            Log::info($message);
            print_r($message . PHP_EOL);
        } else {
            $arrUpdate6 = [
                'error_code'      => '20006',
                'error_message'   => 'delivery_num is null',
                'delivery_status' => 9,
                'up_ope_cd'       => 'OPE99999',
                'up_date'         => now(),
            ];
            $success = 0;
            foreach ($datas6 as $value) {
                $modelD->updateData(
                    [
                        'received_order_id' => $value->received_order_id,
                        'subdivision_num'   => $value->subdivision_num,
                    ],
                    $arrUpdate6
                );
                $arrTemp[] = $value->received_order_id . '-' . $value->subdivision_num;
                $success++;
            }
            $message = "Process $total records and success: $success records.";
            Log::info($message);
            print_r($message . PHP_EOL);
        }
        $message = '----- End check delivery num -----';
        Log::info($message);
        print_r($message . PHP_EOL);
        $message = '----- Call glsc api to send delivery order -----';
        Log::info($message);
        print_r($message . PHP_EOL);
        $arrResults = ProcessApiGLSC::insertShippingList('ProcessOrderGlsc');
        $message = '----- End call glsc api to send delivery order -----';
        Log::info($message);
        print_r($message . PHP_EOL);
        Log::info('Delivery have error.' . PHP_EOL . implode(PHP_EOL, $arrTemp) . PHP_EOL . implode(PHP_EOL, $arrResults));
        $arrTemp = array_unique(array_merge($arrTemp, $arrResults));
        if (count($arrTemp) !== 0) {
            $message = implode(PHP_EOL, $arrTemp);
            $error  = "------------------------------------------" . PHP_EOL;
            $error .= basename(__CLASS__) . PHP_EOL;
            $error .= 'Delivery have error.' . PHP_EOL . $message;
            $error .= PHP_EOL . "------------------------------------------" . PHP_EOL;
            $this->slack->notify(new SlackNotification($error));

            //Post to google chat
            $content = "@文山シンシア, @デビッドnguyen_t \n 出荷指示画面で以下の出荷指示番号
                    $message \n エラーが出ていますので \n ご修正をお願いいたします。";
            CurlPost::pushChatGoogle(null, $content);
        }
    }
    /**
    * Check isset column
    * @return object boolean
    */
    public function checkIsset($data, $value = null)
    {
        return isset($data) ? $data : $value;
    }
}
