<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use DB;
use App\Notification;
use App\Events\Command as eCommand;
use Event;
use Config;
use App;
use Illuminate\Support\Facades\Log;
use App\Notifications\SlackNotification;
use App\Models\Batches\TRakutenPayOrderList;
use App\Models\Batches\TRakutenPayOrderDetail;
use App\Models\Batches\MstRakutenKey;

class ProcessRakutenPayOrderApi extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'process:rakuten-pay-order-api {checkOrder=nocheck}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * The key rakuten
     *
     * @var string
     */
    protected $keyRakuten = '';

    /**
     * The error.
     *
     * @var array
     */
    public $error = [];

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //Create log
        $arrayReplace = [':', '-'];
        $folder       = str_replace($arrayReplace, '_', $this->signature);
        Log::useDailyFiles(storage_path() . "/logs/$folder/history.log");
        $signature    = explode(' ', $this->signature);
        $signature    = $signature[0];
        Event::fire(new eCommand($signature, array('start' => true)));
        Log::info('Start batch rakuten order API.');
        print_r("Start batch rakuten order API." . PHP_EOL);
        $this->slack = new Notification(CHANNEL['horunba']);
        $start       = microtime(true);
        $modelRakutenKey  = new MstRakutenKey();
        $this->keyRakuten = $modelRakutenKey->getKeyRakuten();
        DB::beginTransaction();
        try {
            $rakUrl     =  "https://api.rms.rakuten.co.jp/es/2.0/order/searchOrder/";
            $checkOrder = $this->argument('checkOrder');
            if ($checkOrder === 'check') {
                $startDate = date("Y-m-d\TH:i:s+0900", strtotime("-10 days"));
                $endDate   = date("Y-m-d\TH:i:s+0900", strtotime("-60 minutes"));
            } else {
                $time = date('H:i');
                if ($time >= '12:00' && $time <= '15:00') {
                    $startDate = date("Y-m-d\TH:i:s+0900", strtotime("-70 minutes"));
                    $endDate   = date("Y-m-d\TH:i:s+0900", strtotime("-30 minutes"));
                } else {
                    $startDate = date("Y-m-d\TH:i:s+0900", strtotime("-110 minutes"));
                    $endDate   = date("Y-m-d\TH:i:s+0900", strtotime("-60 minutes"));
                }
            }
            if (App::environment(['local', 'test'])) {
                $startDate = date("2018-11-1\TH:i:s+0900");
                $endDate   = date("2018-11-30\TH:i:s+0900");
            }
            $request = array(
                'dateType'               => 1,
                'startDatetime'          => $startDate,
                'endDatetime'            => $endDate,
                'orderProgressList'      => [100]
            );
            $responseJson = $this->processCurl($request, $rakUrl);
            if (isset($responseJson->orderNumberList) && count($responseJson->orderNumberList) !== 0) {
                if ($responseJson->PaginationResponseModel->totalPages > 1) {
                    for ($i = 1; $i <= $responseJson->PaginationResponseModel->totalPages; $i++) {
                        $request['PaginationRequestModel']['requestRecordsAmount'] = 30;
                        $request['PaginationRequestModel']['requestPage'] = $i;
                        $responseJsonData = $this->processCurl($request, $rakUrl);
                        if (isset($responseJsonData->orderNumberList)
                                && count($responseJsonData->orderNumberList) !== 0) {
                            $this->insertOrderInfo($responseJsonData->orderNumberList);
                        }
                    }
                } else {
                    $this->insertOrderInfo($responseJson->orderNumberList);
                }
            }
            $this->getOrderInfo();
            DB::commit();
        } catch (\Exception $e) {
            DB::rollback();
            $message = $e->getMessage();
            print_r($message . PHP_EOL);
            Log::error($message);
            report($e);
            $this->error[] = $e->getMessage();
        }
        Event::fire(new eCommand($signature, array('end' => true, 'error' => $this->error)));
        $totalTime = round(microtime(true) - $start, 2);
        Log::info("End batch rakuten order API with total time: $totalTime s.");
        print_r("End batch rakuten order API with total time: $totalTime s.");
    }
    //public function process
    /**
    * Process insert replace
    * $parram array
    * @return
    */
    public function insertOrderInfo($orderNumberList)
    {
        foreach ($orderNumberList as $value) {
            DB::insert("INSERT IGNORE INTO hrnb.t_rakuten_pay_order_list (`order_number`) "
                    . "VALUES ('$value')", $orderNumberList);
        }
        $count  = count($orderNumberList);
        Log::info("Total of search order from rakuten api : $count");
        print_r("Total of search order from rakuten api : $count. \n");
    }
    /**
    * Process api
    * @request array value
    * @rakUrl string url
    * @return object json
    */
    public function processCurl($request = null, $rakUrl = null)
    {
        $rakHeader = array(
            "Accept: application/json",
            "Content-type: application/json; charset=UTF-8",
            "Authorization: {$this->keyRakuten}"
        );
        $requestJson = json_encode($request);
        $ch = curl_init($rakUrl);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $rakHeader);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $requestJson);
        $responseJson = curl_exec($ch);
        curl_close($ch);
        return json_decode($responseJson);
    }
    /**
    * Get order info by order number
    * @return object boolean
    */
    public function getOrderInfo()
    {
        $orderNumberList = TRakutenPayOrderList::where('process_flag', 0)->pluck('order_number');
        if (count($orderNumberList) > 0) {
            $rakUrl    = "https://api.rms.rakuten.co.jp/es/2.0/order/getOrder/";
            $orderNumberListChunk = array_chunk($orderNumberList->toArray(), 100);
            foreach ($orderNumberListChunk as $keyChunk => $valueChunk) {
                $request = array(
                    'orderNumberList' => $valueChunk,
                );
                $responseJson = $this->processCurl($request, $rakUrl);
                if (isset($responseJson->MessageModelList)) {
                    $errorMessage = $responseJson->MessageModelList;
                    $error = null;
                    foreach ($errorMessage as $response) {
                        if ($response->messageType === 'ERROR') {
                            $error = "$response->messageCode" . "\n" .$response->message;
                            Log::info("$error");
                            print_r("$error");
                            $this->slack->notify(new SlackNotification($error));
                            return;
                        }
                    }
                }
                if (isset($responseJson->OrderModelList) && count($responseJson->OrderModelList) > 0) {
                    $dataJson = $responseJson->OrderModelList;
                    $dataInsert = [];
                    $i = 0;
                    $arrayOrrderNumber = [];
                    foreach ($dataJson as $key => $value) {
                        if (isset($value->PackageModelList)) {
                            if (count($value->PackageModelList) > 0) {
                                foreach ($value->PackageModelList as $valuePML) {
                                    if (count($valuePML->ItemModelList) > 0) {
                                        foreach ($valuePML->ItemModelList as $valueIML) {
                                            $dataInsert['itemmodel_basketid']       = $this->checkIsset($valuePML->basketId, '');
                                            $dataInsert['packagemodel_postageprice']= $this->checkIsset($valuePML->postagePrice, '');
                                            $dataInsert['packagemodel_goodsprice']= $this->checkIsset($valuePML->goodsPrice, '');
                                            $dataInsert['noshi']                    = $this->checkIsset($valuePML->noshi, '');
                                            $dataInsert['delivery_price']            = $this->checkIsset($valuePML->deliveryPrice, '');
                                            $dataInsert['goods_tax']                 = $this->checkIsset($valuePML->goodsTax, '');
                                            $dataInsert['goods_price']               = $this->checkIsset($valuePML->goodsPrice, '');
                                            $dataInsert['total_price']               = $this->checkIsset($valuePML->totalPrice, '');
                                            $dataInsert['delete_flg']               = $this->checkIsset($valuePML->packageDeleteFlag, '');
                                            $dataInsert['sendermodel_zipcode1']     = $this->checkIsset($valuePML->SenderModel->zipCode1, '');
                                            $dataInsert['sendermodel_zipcode2']     = $this->checkIsset($valuePML->SenderModel->zipCode2, '');
                                            $dataInsert['sendermodel_prefecture']   = $this->checkIsset($valuePML->SenderModel->prefecture, '');
                                            $dataInsert['sendermodel_city']         = $this->checkIsset($valuePML->SenderModel->city, '');
                                            $dataInsert['sendermodel_subaddress']   = $this->checkIsset($valuePML->SenderModel->subAddress, '');
                                            $dataInsert['sendermodel_familyname']   = $this->checkIsset($valuePML->SenderModel->familyName, '');
                                            $dataInsert['sendermodel_firstname']    = $this->checkIsset($valuePML->SenderModel->firstName, '');
                                            $dataInsert['sendermodel_familynamekana']   = $this->checkIsset($valuePML->SenderModel->familyNameKana, '');
                                            $dataInsert['sendermodel_firstnamekana']    = $this->checkIsset($valuePML->SenderModel->firstNameKana, '');
                                            $dataInsert['sendermodel_phonenumber1']     = $this->checkIsset($valuePML->SenderModel->phoneNumber1, 0);
                                            $dataInsert['sendermodel_phonenumber2']     = $this->checkIsset($valuePML->SenderModel->phoneNumber2, 0);
                                            $dataInsert['sendermodel_phonenumber3']     = $this->checkIsset($valuePML->SenderModel->phoneNumber3, 0);
                                            $dataInsert['item_name']                        = $this->checkIsset($valueIML->itemName, '');
                                            $dataInsert['itemmodel_itemid']                 = $this->checkIsset($valueIML->itemId, '');
                                            $dataInsert['item_number']                      = $this->checkIsset($valueIML->itemNumber, '');
                                            $dataInsert['price']                            = $this->checkIsset($valueIML->price, '');
                                            $dataInsert['units']                            = $this->checkIsset($valueIML->units, '');
                                            $dataInsert['isincludedpostage']                = $this->checkIsset($valueIML->includePostageFlag, '');
                                            $dataInsert['isincludedtax']                    = $this->checkIsset($valueIML->includeTaxFlag, '');
                                            $dataInsert['isincludedcashondeliverypostage']  = $this->checkIsset($valueIML->includeCashOnDeliveryPostageFlag, '');
                                            $dataInsert['selected_choice']                  = $this->checkIsset($valueIML->selectedChoice, '');
                                            $dataInsert['point_rate']                       = $this->checkIsset($valueIML->pointRate, '');
                                            $dataInsert['inventory_type']                   = $this->checkIsset($valueIML->inventoryType, '');
                                            $dataInsert['delvdateinfo']                     = $this->checkIsset($valueIML->delvdateInfo, '');
                                            $dataInsert['restore_inventory_flag']           = $this->checkIsset($valueIML->restoreInventoryFlag, '');
                                            $dataInsert['delete_item_flg']                  = $this->checkIsset($valueIML->deleteItemFlag, '');
                                            $dataInsert['item_detail_id']                   = $this->checkIsset($valueIML->itemDetailId, '');
                                            $dataInsert['manage_number']                    = $this->checkIsset($valueIML->manageNumber, '');
                                            if (count($valuePML->ShippingModelList) > 0) {
                                                foreach ($valuePML->ShippingModelList as $valueSML) {
                                                    $dataInsert['delivery_company_id']  = $this->checkIsset($valueSML->deliveryCompany, '');
                                                    $dataInsert['shipping_detail_id']   = $this->checkIsset($valueSML->shippingDetailId, '');
                                                    $dataInsert['shipping_number']      = $this->checkIsset($valueSML->shippingNumber, '');
                                                    $dataInsert['delivery_company_name']= $this->checkIsset($valueSML->deliveryCompanyName, '');
                                                    $dataInsert['shipping_date']        = $this->checkIsset($valueSML->shippingDate, null);
                                                }
                                            }
                                            $dataInsert['cvs_code']             = $this->checkIsset($valuePML->DeliveryCvsModel, '');
                                            $dataInsert['order_number']         = $value->orderNumber;
                                            $dataInsert['order_date']           = $this->checkIsset($value->orderDatetime, null);
                                            $dataInsert['shipping_term']        = $value->shippingTerm;
                                            $dataInsert['carrier_code']         = $value->carrierCode;
                                            $dataInsert['email_carrier_code']   = $value->emailCarrierCode;
                                            $dataInsert['order_type']           = $value->orderType;
                                            $dataInsert['goods_price']          = $value->goodsPrice;
                                            $dataInsert['goods_tax']            = $value->goodsTax;
                                            $dataInsert['postage_price']        = $value->postagePrice;
                                            $dataInsert['delivery_price']       = $value->deliveryPrice;
                                            $dataInsert['total_price']          = $value->totalPrice;
                                            $dataInsert['request_price']        = $value->requestPrice;
                                            $dataInsert['coupon_all_total_price']   = $value->couponAllTotalPrice;
                                            $dataInsert['couponshopprice']      = $value->couponShopPrice;
                                            $dataInsert['couponotherprice']     = $value->couponOtherPrice;
                                            $dataInsert['asuraku_flg']          = $value->asurakuFlag;
                                            $dataInsert['memo']                 = $value->memo;
                                            $dataInsert['operator']             = $value->operator;
                                            $dataInsert['mailplugsentence']     = $value->mailPlugSentence;
                                            $dataInsert['istaxrecalc']          = $value->isTaxRecalc;
                                            $dataInsert['zip_code1']            = $this->checkIsset($value->OrdererModel->zipCode1, 0);
                                            $dataInsert['zip_code2']            = $this->checkIsset($value->OrdererModel->zipCode2, 0);
                                            $dataInsert['prefecture']           = $this->checkIsset($value->OrdererModel->prefecture, '');
                                            $dataInsert['city']                 = $this->checkIsset($value->OrdererModel->city, '');
                                            $dataInsert['sub_address']          = $this->checkIsset($value->OrdererModel->subAddress, '');
                                            $dataInsert['family_name']          = $this->checkIsset($value->OrdererModel->familyName, '');
                                            $dataInsert['first_name']           = $this->checkIsset($value->OrdererModel->firstName, '');
                                            $dataInsert['family_name_kana']     = $this->checkIsset($value->OrdererModel->familyNameKana, '');
                                            $dataInsert['first_name_kana']      = $this->checkIsset($value->OrdererModel->firstNameKana, '');
                                            $dataInsert['phone_number1']        = $this->checkIsset($value->OrdererModel->phoneNumber1, 0);
                                            $dataInsert['phone_number2']        = $this->checkIsset($value->OrdererModel->phoneNumber2, 0);
                                            $dataInsert['phone_number3']        = $this->checkIsset($value->OrdererModel->phoneNumber3, 0);
                                            $dataInsert['email_address']        = $this->checkIsset($value->OrdererModel->emailAddress, '');
                                            $dataInsert['sex']                  = $this->checkIsset($value->OrdererModel->sex, 0);
                                            $dataInsert['birthyear']            = $this->checkIsset($value->OrdererModel->birthYear, null);
                                            $dataInsert['birthmonth']           = $this->checkIsset($value->OrdererModel->birthMonth, null);
                                            $dataInsert['settlementname']       = $this->checkIsset($value->SettlementModel->settlementMethod, '');
                                            $dataInsert['card_name']            = $this->checkIsset($value->SettlementModel->cardName, '');
                                            $dataInsert['card_number']          = $this->checkIsset($value->SettlementModel->cardNumber, '');
                                            $dataInsert['card_owner']           = $this->checkIsset($value->SettlementModel->cardOwner, '');
                                            $dataInsert['card_ym']              = $this->checkIsset($value->SettlementModel->cardYm, null);
                                            $dataInsert['card_pay_type']        = $this->checkIsset($value->SettlementModel->cardPayType, '');
                                            $dataInsert['card_installment_desc']= $this->checkIsset($value->SettlementModel->cardInstallmentDesc, '');
                                            $dataInsert['delivery_name']       = $this->checkIsset($value->DeliveryModel->deliveryName, '');
                                            $dataInsert['delivery_class']       = $this->checkIsset($value->DeliveryModel->deliveryClass, '');
                                            $dataInsert['point_model_used_point']   = $this->checkIsset($value->PointModel->usedPoint, '');
                                            if (count($value->CouponModelList) > 0) {
                                                foreach ($value->CouponModelList as $valueCML) {
                                                    $dataInsert['couponcode']               = $this->checkIsset($valueCML->couponCode, '');
                                                    $dataInsert['couponitemid']             = $this->checkIsset($valueCML->itemId, '');
                                                    $dataInsert['couponname']               = $this->checkIsset($valueCML->couponCode, '');
                                                    $dataInsert['couponsummary']            = $this->checkIsset($valueCML->couponName, '');
                                                    $dataInsert['couponcode']               = $this->checkIsset($valueCML->couponSummary, '');
                                                    $dataInsert['couponcapital']            = $this->checkIsset($valueCML->couponCapital, '');
                                                    $dataInsert['couponexpirydate']         = $this->checkIsset($valueCML->expiryDate, '');
                                                    $dataInsert['couponprice']              = $this->checkIsset($valueCML->couponPrice, '');
                                                    $dataInsert['couponunit']               = $this->checkIsset($valueCML->couponUnit, '');
                                                    $dataInsert['coupontotalprice']         = $this->checkIsset($valueCML->couponTotalPrice, '');
                                                }
                                            }
                                            $dataInsert['mail_serial']              = md5((string)$this->checkIsset($value->orderNumber, ''));
                                            $dataInsert['sub_status_id']            = $this->checkIsset($value->subStatusId, '');
                                            $dataInsert['sub_status_name']          = $this->checkIsset($value->subStatusName, '');
                                            $dataInsert['shop_order_cfm_datetime']  = $this->checkIsset($value->shopOrderCfmDatetime, null);
                                            $dataInsert['order_fix_datetime']       = $this->checkIsset($value->orderFixDatetime, null);
                                            $dataInsert['shipping_inst_datetime']   = $this->checkIsset($value->shippingInstDatetime, null);
                                            $dataInsert['shipping_cmpl_rpt_datetime'] = $this->checkIsset($value->shippingCmplRptDatetime, null);
                                            $dataInsert['cancel_due_date']            = $this->checkIsset($value->cancelDueDate, null);
                                            $dataInsert['delivery_date']              = $this->checkIsset($value->deliveryDate, null);
                                            $dataInsert['option']                   = $this->checkIsset($value->remarks, '');
                                            $dataInsert['gift_check_flag']          = $this->checkIsset($value->giftCheckFlag, '');
                                            $dataInsert['is_gift']                  = $this->checkIsset($value->severalSenderFlag, '');
                                            $dataInsert['equal_sender_flag']        = $this->checkIsset($value->equalSenderFlag, '');
                                            $dataInsert['isolatedIsland_flag']      = $this->checkIsset($value->isolatedIslandFlag, '');
                                            $dataInsert['reserve_number']           = $this->checkIsset($value->reserveNumber, '');
                                            $dataInsert['reserve_delivery_count']   = $this->checkIsset($value->reserveDeliveryCount, '');
                                            $dataInsert['caution_display_type']     = $this->checkIsset($value->cautionDisplayType, '');
                                            $dataInsert['rakuten_confirm_flag']     = $this->checkIsset($value->rakutenConfirmFlag, '');
                                            $dataInsert['drug_flag']                = $this->checkIsset($value->drugFlag, '');
                                            $dataInsert['deal_flag']                = $this->checkIsset($value->dealFlag, '');
                                            $dataInsert['membership_type']          = $this->checkIsset($value->membershipType, '');
                                            $dataInsert['modify_flag']              = $this->checkIsset($value->modifyFlag, '');
                                            $dataInsert['wrapping_model1']                 = $this->checkIsset($value->WrappingModel1, '');
                                            $dataInsert['wrapping_model2']                 = $this->checkIsset($value->WrappingModel2, '');
                                            $dataInsert['created_at']               = now();
                                            if (count($value->ChangeReasonModelList) > 0) {
                                                foreach ($value->ChangeReasonModelList as $valueCRML) {
                                                    $dataInsert['change_id']                = $this->checkIsset($valueCRML->changeId, '');
                                                    $dataInsert['change_type']              = $this->checkIsset($valueCRML->changeType, '');
                                                    $dataInsert['change_type_detail']       = $this->checkIsset($valueCRML->changeTypeDetail, '');
                                                    $dataInsert['change_reason']            = $this->checkIsset($valueCRML->changeReason, '');
                                                    $dataInsert['change_reason_detail']     = $this->checkIsset($valueCRML->changeReasonDetail, '');
                                                    $dataInsert['change_apply_datetime']    = $this->checkIsset($valueCRML->changeApplyDatetime, null);
                                                    $dataInsert['change_fix_datetime']      = $this->checkIsset($valueCRML->changeFixDatetime, null);
                                                    $dataInsert['change_cmpl_datetime']     = $this->checkIsset($valueCRML->changeCmplDatetime, null);
                                                }
                                            }
                                            try {
                                                TRakutenPayOrderDetail::insert($dataInsert);
                                                $i++;
                                            } catch (\Exception $e) {
                                                report($e);
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        $arrayOrrderNumber[] = $value->orderNumber;
                    }
                    if (count($arrayOrrderNumber) > 0) {
                        TRakutenPayOrderList::whereIn('order_number', $arrayOrrderNumber)->update(['process_flag' => 1]);
                    }
                    if ($i > 0) {
                        Log::info("Total of get order from rakuten api : $i");
                        print_r("Total of get order from rakuten api : $i. \n");
                    }
                } else {
                    Log::info("Total of get order from rakuten api : 0");
                    print_r("Total of get order from rakuten api : 0. \n");
                }
            }
        }
        return false;
    }
    /**
    * Check isset column
    * @return object boolean
    */
    public function checkIsset($data, $value = null)
    {
        return isset($data) ? $data : $value;
    }
}
