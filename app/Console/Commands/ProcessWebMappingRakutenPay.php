<?php
/**
 * Process web mapping rakuten pay
 *
 * @package    App\Console\Commands
 * @subpackage ProcessWebMappingRakutenPay
 * @copyright  Copyright (c) 2018 RiverCrane! Corporation. All Rights Reserved.
 * @author     Truong Nghia<truong.van.nghia@rivercrane.vn>
 */

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\Batches\MstOrder;
use App\Models\Batches\DtWebMapping;
use App\Models\Batches\DtWebRakPayData;
use App\Models\Batches\MstPaymentMethod;
use App\Notification;
use App\Notifications\SlackNotification;
use Illuminate\Support\Facades\Log;
use App\Events\Command as eCommand;
use Event;
use Config;
use App\Models\Batches\MstRakutenKey;

class ProcessWebMappingRakutenPay extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'process:web-mapping-rakuten-pay';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';
    /**
     * The key rakuten
     *
     * @var string
     */
    protected $keyRakuten = '';
    /**
     * The error.
     *
     * @var array
     */
    public $error = [];

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $arrayReplace = [':', '-'];
        $folder       = str_replace($arrayReplace, '_', $this->signature);
        Log::useDailyFiles(storage_path() . "/logs/$folder/history.log");
        //Process data
        Event::fire(new eCommand($this->signature, array('start' => true)));
        $startBatch = microtime(true);
        $this->slack = new Notification(CHANNEL['horunba']);
        Log::info('Start batch process web mapping for Rakuten Pay.');
        print_r("Start batch process web mapping for Rakuten Pay." . PHP_EOL);
        $modelO      = new MstOrder();
        $modelRakPay = new DtWebRakPayData();
        $modelMap    = new DtWebMapping();
        $modelPay    = new MstPaymentMethod();
        $modelRakutenKey  = new MstRakutenKey();
        $this->keyRakuten = $modelRakutenKey->getKeyRakuten();
        //Process get first data
        $dataOrder   = $modelO->getOrderAPIWebMapping(9, 1000);
        $totalCount  = 0;
        if ($dataOrder->count() > 0) {
            $dataOrder = $dataOrder->chunk(100);
            foreach ($dataOrder as $orders) {
                try {
                    $arrInserts = $this->processRakuten($orders);
                    $total = count($arrInserts);
                    $totalCount += $total;
                    if ($total !==0) {
                        $modelRakPay->insert($arrInserts);
                    }
                } catch (\Exception $ex) {
                    report($ex);
                    continue;
                }
            }
        }
        $message = "Insert dt_web_rak_pay_data success: $totalCount records.";
        Log::info($message);
        print_r($message . PHP_EOL);
        //Process order in case column is_correct = 0
        $orderCorrect   = $modelO->getOrderPayCorrect(9);
        $totalCount  = 0;
        if ($orderCorrect->count() > 0) {
            $orderCorrect = $orderCorrect->chunk(100);
            foreach ($orderCorrect as $orders) {
                try {
                    $arrUpdate = $this->processRakuten($orders);
                    $total = count($arrUpdate);
                    $totalCount = 0;
                    if ($total !==0) {
                        foreach ($arrUpdate as $item) {
                            $orderNumber = $item['order_number'];
                            $rakutenOrderStatus = 0;
                            switch ($item['status']) {
                                case '100':
                                case '200':
                                case '300':
                                case '400':
                                    $rakutenOrderStatus = 1;
                                    break;
                                case '500':
                                case '600':
                                case '700':
                                    $rakutenOrderStatus = 8;
                                    break;
                                case '800':
                                    $rakutenOrderStatus = 10;
                                    break;
                                case '900':
                                    $rakutenOrderStatus = 10;
                                    break;
                                default:
                                    break;
                            }
                            $checkOrder = $modelO->checkOrderMapping($orderNumber, $rakutenOrderStatus);
                            if ($checkOrder === 1) {
                                $modelMap->updateData($orderNumber, $rakutenOrderStatus);
                                $totalCount++;
                            }
                        }
                    }
                } catch (\Exception $ex) {
                    report($ex);
                    continue;
                }
            }
        }
        $message = "Update dt_web_mapping success: $totalCount records.";
        Log::info($message);
        print_r($message . PHP_EOL);

        //Process new data web mapping
        $count       = 0;
        $dataRak     = $modelRakPay->getDataProcessWebRakuten();
        $dataPayment = $modelPay->getDataByMall(9);
        if (count($dataRak) !== 0) {
            foreach ($dataRak as $data) {
                $arrInsert = [];
                try {
                    $paymentMethod = 0;
                    foreach ($dataPayment as $pay) {
                        if ($data->settlement_name === $pay->payment_name) {
                            $paymentMethod = $pay->payment_code;
                            break;
                        }
                    }
                    $arrInsert['received_order_id']  = $data->order_number;
                    $arrInsert['mall_id']            = 9;
                    $arrInsert['web_payment_method'] = $paymentMethod;
                    switch ($data->status) {
                        case '100':
                        case '200':
                        case '300':
                        case '400':
                            $arrInsert['web_order_status'] = 1;
                            break;
                        case '500':
                        case '600':
                        case '700':
                            $arrInsert['web_order_status'] = 8;
                            break;
                        case '800':
                            $arrInsert['web_order_status'] = 10;
                            break;
                        case '900':
                            $arrInsert['web_order_status'] = 10;
                            break;
                        default:
                            break;
                    }
                    $arrInsert['web_request_price'] = $data->request_price;
                    $differentType = [];
                    if ($data->payment_method !== $paymentMethod && ($data->order_request_price !== $data->request_price || $paymentMethod <> 10)) {
                        $differentType[] = '支払方法';
                    }
                    if ($data->order_request_price !== $data->request_price && ($data->order_status <> 10 || $arrInsert['web_order_status'] <> 10)) {
                        $differentType[] = 'WEB金額';
                    }
                    if ($data->order_status !== $arrInsert['web_order_status']) {
                        $differentType[] = 'ステータス';
                    }
                    $arrInsert['different_type']  = implode("\n", $differentType);
                    $arrInsert['different_price'] = $data->order_request_price - $data->request_price;
                    $arrInsert['occorred_reason'] = 0;
                    $arrInsert['process_content'] = 0;
                    $arrInsert['finished_date']   = empty($arrInsert['different_type'])
                        ? date('Y-m-d H:i:s') : null;
                    $arrInsert['is_corrected']    = empty($arrInsert['different_type']) ? 1 : 0;
                    $arrInsert['is_deleted']      = 0;
                    $arrInsert['receive_id']      = 0;
                    $arrInsert['in_ope_cd']       = 'OPE99999';
                    $arrInsert['in_date']         = date('Y-m-d H:i:s');
                    $arrInsert['up_ope_cd']       = 'OPE99999';
                    $arrInsert['up_date']         = date('Y-m-d H:i:s');
                    $modelMap->insertIgnore($arrInsert);
                    $modelRakPay->where('order_number', $data->order_number)
                        ->update(['process_flg' => 1]);
                    $count++;
                } catch (\Exception $e) {
                    report($e);
                }
            }
        }
        $message = "Insert dt_web_mapping success: $count records.";
        Log::info($message);
        print_r($message . PHP_EOL);
        Event::fire(new eCommand($this->signature, array('end' => true, 'error' => $this->error)));
        $totalTime = round(microtime(true) - $startBatch, 2);
        Log::info("End batch process web mapping for Rakuten Pay with total time: $totalTime s.");
        print_r("End batch process web mapping for Rakuten Pay with total time: $totalTime s.");
    }

    /**
    * Process data rakuten type json
    * @return array
    */
    public function processRakuten($orders)
    {
        $arrOrder  = $orders->pluck('received_order_id');
        $rakUrl    = "https://api.rms.rakuten.co.jp/es/2.0/order/getOrder/";
        $rakHeader = array(
            "Accept: application/json",
            "Content-type: application/json; charset=UTF-8",
            "Authorization: {$this->keyRakuten}"
        );
        $requestJson = json_encode(['orderNumberList' => $arrOrder->toArray()]);
        $ch = curl_init($rakUrl);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $rakHeader);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $requestJson);
        $responseJson = curl_exec($ch);
        curl_close($ch);
        $arrDatas = json_decode($responseJson);
        if (isset($arrDatas->OrderModelList) && count($arrDatas->OrderModelList) > 0) {
            $arrDatas = $arrDatas->OrderModelList;
            $dataInsert = [];
            foreach ($arrDatas as $arrData) {
                $temp = [];
                $temp['order_number']    = $arrData->orderNumber;
                $temp['status']          = $arrData->orderProgress;
                $temp['request_price']   = $arrData->requestPrice;
                $temp['settlement_name'] = $this->checkIsset($arrData->SettlementModel->settlementMethod, '');
                $temp['process_flg']     = 0;
                $temp['is_delete']       = 0;
                $temp['created_at']      = date('Y-m-d H:i:s');
                $dataInsert[] = $temp;
            }
            return $dataInsert;
        }
        if (isset($arrDatas->MessageModelList)) {
            $arrayError = $arrDatas->MessageModelList;
            $arrayError = $arrayError[0];
            if (!empty($arrayError)) {
                $error  = "------------------------------------------" . PHP_EOL;
                $error .= basename(__CLASS__) . PHP_EOL;
                $error .= "{$arrayError->orderNumber} : {$arrayError->messageCode}";
                $error .= PHP_EOL . "------------------------------------------" . PHP_EOL;
                $this->slack->notify(new SlackNotification($error));
            }
            return array();
        }
    }

    /**
    * Check isset column
    * @return object boolean
    */
    public function checkIsset($data, $value = null)
    {
        return isset($data) ? $data : $value;
    }
}
