<?php
/**
 * Batch process order validation
 *
 * @package    App\Console\Commands
 * @subpackage ProcessOrderValidation
 * @copyright  Copyright (c) 2019 RiverCrane! Corporation. All Rights Reserved.
 * @author     Truong Nghia<truong.van.nghia@rivercrane.vn>
 */

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Events\Command as eCommand;
use Illuminate\Support\Facades\Log;
use App\Models\Batches\MstOrder;
use App\Models\Batches\DtOrderProductDetail;
use App\Models\Batches\MstOrderDetail;
use App\Http\Controllers\Common;
use App\Notification;
use Event;

class ProcessOrderValidation extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'process:order-validate';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Process order validation';

    /**
     * The error.
     *
     * @var string
     */
    public $error = [];

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //Create log
        $arrayReplace = [':', '-'];
        $folder       = str_replace($arrayReplace, '_', $this->signature);
        Log::useDailyFiles(storage_path() . "/logs/$folder/history.log");
        //Process data
        Event::fire(new eCommand($this->signature, array('start' => true)));
        Log::info('Start batch process order validate.');
        print_r("Start batch process order validate." . PHP_EOL);
        $start      = microtime(true);
        $slack      = new Notification(CHANNEL['horunba']);
        Log::info('==== Start process transport validation ====');
        print_r("==== Start process transport validation ====" . PHP_EOL);
        $this->processTransportValidation();
        Log::info('==== End process transport validation ====');
        print_r("==== End process transport validation ====" . PHP_EOL);
        Event::fire(new eCommand($this->signature, array('end' => true, 'error' => $this->error)));
        $totalTime = round(microtime(true) - $start, 2);
        Log::info("End batch process order validate with total time: $totalTime s .");
        print_r("End batch process order validate with total time: $totalTime s." . PHP_EOL);
    }

    /**
     * Process transport validation.
     *
     * @return void
     */
    public function processTransportValidation()
    {
        $modelO = new MstOrder();
        $common = new Common();
        $datas  = $modelO->getDataProcessValidate();
        $count  = 0;
        if ($datas->count()) {
            $arrReceiveId = [];
            foreach ($datas as $data) {
                if ($data->mall_id === 10 && $data->payment_method !== 1) {
                    $this->processCancelOrder($data->receive_id);
                    $arrUpdate = [
                        'is_mail_sent'        => 1,
                        'urgent_mail_id'      => 858,
                        'is_send_mail_urgent' => 1,
                    ];
                    $modelO->updateData([$data->receive_id], $arrUpdate);
                } else {
                    $common->reCalculate($data->receive_id);
                    $arrUpdate2 = [
                        'is_delay'            => 1,
                        'delay_reason'        => '離島送料お伺い',
                        'is_mail_sent'        => 1,
                        'urgent_mail_id'      => 49,
                        'is_send_mail_urgent' => 1,
                        'order_sub_status'    => ORDER_SUB_STATUS['NEW']
                    ];
                    $modelO->updateData([$data->receive_id], $arrUpdate2);
                }
                $count++;
            }
            Log::info("Process success: $count records.");
            print_r("Process success: $count records." . PHP_EOL);
        } else {
            Log::info("No data.");
            print_r("No data." . PHP_EOL);
        }
    }

    /**
     * Process cancel order
     * @return void
     */
    public function processCancelOrder($receiveId)
    {
        $common    = new Common();
        $modelODP  = new DtOrderProductDetail();
        $modelO    = new MstOrder();
        $dataOrder = $modelO->find($receiveId);
        $dataODP   = $modelODP->where(['receive_id' => (int)$receiveId])
                ->get(['product_status']);
        $arrKey = [$receiveId];
        $common->cancelOrderEdi($receiveId);
        $common->processUpdateZan($receiveId);
        $arrUpdate = [
            'is_confirmed'          => 2,
            'ship_charge'           => 0,
            'pay_after_charge'      => 0,
            'total_price'           => 0,
            'pay_charge_discount'   => 0,
            'used_point'            => 0,
            'used_coupon'           => 0,
            'pay_charge'            => 0,
            'pay_charge_discount'   => 0,
            'request_price'         => 0,
            'goods_price'           => 0,
            'pay_after_charge'      => 0,
            'order_status'          => ORDER_STATUS['CANCEL'],
            'order_sub_status'      => ORDER_SUB_STATUS['DONE'],
            'cancel_reason'         => 10,
            'is_mall_update'        => 0,
            'is_mail_sent'          => 1,
            'in_ope_cd'             => 'OPE99999',
            'err_text'              => '',
        ];

        $modelO->updateData($arrKey, $arrUpdate);
        $modelOD = new MstOrderDetail();
        $modelOD->updateData(
            ['receive_id' => (int)$receiveId],
            [
                'price'    => 0,
                'quantity' => 0,
            ]
        );
        if (in_array($dataOrder->payment_method, [2, 6, 7])) {
            if ($dataOrder->payment_status === 1) {
                $common->processRePaymentCancel($dataOrder);
            }
        }
        if (count($dataODP) > 0) {
            $modelODP->updateDataByReceive(
                $arrKey,
                ['product_status' => PRODUCT_STATUS['CANCEL']]
            );
        }
    }

}
