<?php
/**
 * Batch process get order wowma from api
 *
 * @package    App\Console\Commands
 * @subpackage ProcessGetOrderWowma
 * @copyright  Copyright (c) 2019 RiverCrane! Corporation. All Rights Reserved.
 * @author     Van Qui<van.qui@rivercrane.vn>
 */

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;
use App\Notifications\SlackNotification;
use App\Custom\Utilities;
use App\Notification;
use App\Events\Command as eCommand;
use Event;
use Config;
use Helper;
use DB;
use App;
use App\Models\Batches\TWowmaOrderList;
use App\Models\Batches\TWowmaOrder;
use App\Models\Batches\TWowmaOrderDetail;

class ProcessGetOrderWowma extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'process:get-order-wowma {checkOrder=nocheck}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Process get order Wowma';

    /**
     * The notification.
     *
     * @var object
     */
    protected $slack;

    /**
     * The error.
     *
     * @var array
     */
    public $error = [];

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //Create log
        //Create log
        $arrayReplace = [':', '-'];
        $folder       = str_replace($arrayReplace, '_', $this->signature);
        $folder       = explode(' ', $folder);
        $folder       = $folder[0];
        $checkOrder   = $this->argument('checkOrder');
        $signature    = explode(' ', $this->signature);
        $signature    = $signature[0];
        if ($checkOrder === 'check') {
            $signature = $signature . ' check';
        }
        Log::useDailyFiles(storage_path() . "/logs/$folder/history.log");
        //Process data
        Event::fire(new eCommand($signature, array('start' => true)));
        Log::info('Start batch process get order Wowma.');
        print_r("Start batch process get order Wowma." . PHP_EOL);
        $start       = microtime(true);
        $this->slack = new Notification(CHANNEL['horunba']);

        $startDate = date('Ymd');
        $endDate   = date('Ymd');
        
        $environment = 'real';
        if (App::environment(['local', 'test'])) {
            $environment = 'test';
        }
        $shopId    = Config::get("wowma.$environment.shop_id");
        $wowmaAuth = Config::get("wowma.$environment.auth_key");
        $urlApi    = Config::get("wowma.$environment.url");

        $checkOrder = $this->argument('checkOrder');
        if ($checkOrder === 'check') {
            $startDate = date('Ymd', strtotime("-3 days"));
        }
        
        $searchNoParams = [
            'shopId'      => $shopId,
            'startDate'   => $startDate,
            'endDate'     => $endDate,
            'orderStatus' => urlencode('新規受付'),
            'totalCount'  => 1000
        ];
        
        $this->processGetListNo($wowmaAuth, $searchNoParams, $urlApi);
        
        $tWowmaOrderList = new TWowmaOrderList();
        $arrOrderId      = $tWowmaOrderList->getOrderNew()->pluck('order_id')->toArray();
        $searchInfoParams['shopId'] = $shopId;
        foreach ($arrOrderId as $orderId) {
            $searchInfoParams['orderId'] = $orderId;
            $this->processGetInfo($wowmaAuth, $searchInfoParams, $urlApi);
        }
        Event::fire(new eCommand($signature, array('end' => true, 'error' => $this->error)));
        $totalTime = round(microtime(true) - $start, 2);
        Log::info("End batch process get order Wowma with total time: $totalTime s.");
        print_r("End batch process get order Wowma with total time: $totalTime s.");
    }
    
    /**
     * Process get list no
     *
     * @param  string $wowmaAuth
     * @param  array  $searchParams
     * @return array
     */
    private function processGetListNo($wowmaAuth, $searchParams, $urlApi)
    {
        try {
            $arrSearch = [];
            foreach ($searchParams as $key => $val) {
                $arrSearch[] = "$key=$val";
            }
            $strSearch = implode("&", $arrSearch);
            $wowmaUrl  = "{$urlApi}/searchTradeNoListProc?" . $strSearch;
            $wowmaHeader = array(
                "Content-Type: application/x-www-form-urlencoded",
                "Authorization: $wowmaAuth"
            );
            $ch = curl_init($wowmaUrl);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_HTTPHEADER, $wowmaHeader);
            $responseXml = curl_exec($ch);
            curl_close($ch);
            
            $responData = simplexml_load_string($responseXml);
            if ((int)$responData->result->status !== 0) {
                $errorCode = (string)$responData->result->error->code;
                $errorMess = (string)$responData->result->error->message;
                $message = "Process get list no error [$errorCode]: $errorMess";
                print_r($message . PHP_EOL);
                Log::error($message);
                $error  = "------------------------------------------" . PHP_EOL;
                $error .= basename(__CLASS__) . PHP_EOL;
                $error .= PHP_EOL . $message;
                $error .= PHP_EOL . "------------------------------------------" . PHP_EOL;
                $this->slack->notify(new SlackNotification($error));
                return false;
            }
            $orderInfos = $responData->orderInfo;
            $orderIds   = [];
            foreach ($orderInfos as $orderInfo) {
                $orderIds[] = (string)$orderInfo->orderId;
            }
            
            $tWowmaOrderList = new TWowmaOrderList();
            $existsOrder  = $tWowmaOrderList->getOrderByOrderId($orderIds)->pluck('order_id')->toArray();
            $insertOrders = array_diff($orderIds, $existsOrder);
            $arrInsert   = [];
            foreach ($insertOrders as $insertOrder) {
                $arrInsert[] = [
                    'order_id'   => $insertOrder,
                    'order_date' => date('Y-m-d'),
                    'is_got'     => 0,
                    'in_date'    => date('Y-m-d'),
                    'up_date'    => date('Y-m-d')
                ];
            }
            $totalInsert = count($arrInsert);
            if ($totalInsert > 0) {
                $arrInsert = array_chunk($arrInsert, 500);
                foreach ($arrInsert as $dataInsert) {
                    $tWowmaOrderList->insert($dataInsert);
                }
            }
            $message = "Insert success $totalInsert records to t_wowma_order_list table";
            print_r($message . PHP_EOL);
            Log::error($message);
            return true;
        } catch (\Exception $e) {
            $message = "Can't process get list no";
            print_r($message . PHP_EOL);
            Log::error($message);
            report($e);
            $this->error[] = $e->getMessage();
            return false;
        }
    }

    /**
     * Process get API
     *
     * @param string $wowmaAuth
     * @param array  $searchParams
     * @return void
     */
    private function processGetInfo($wowmaAuth, $searchParams, $urlApi)
    {
        DB::beginTransaction();
        try {
            $arrSearch = [];
            $orderId = '';
            foreach ($searchParams as $key => $val) {
                $arrSearch[] = "$key=$val";
                if ($key === 'orderId') {
                    $orderId = $val;
                }
            }
            $strSearch = implode("&", $arrSearch);
            $wowmaUrl  = "{$urlApi}/searchTradeInfoProc?" . $strSearch;
            $wowmaHeader = array(
                "Content-Type: application/x-www-form-urlencoded",
                "Authorization: $wowmaAuth"
            );
            $ch = curl_init($wowmaUrl);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_HTTPHEADER, $wowmaHeader);
            $responseXml = curl_exec($ch);
            curl_close($ch);
            list($arrOrderInfo, $arrOrderDetail) = $this->exportWowmaXml($responseXml);
            
            $tWowmaOrderList   = new TWowmaOrderList();
            $tWowmaOrder       = new TWowmaOrder;
            $tWowmaOrderDetail = new TWowmaOrderDetail();
            if (count($arrOrderInfo) > 0) {
                $tWowmaOrder->replace($arrOrderInfo);
            }
            if (count($arrOrderDetail) > 0) {
                $tWowmaOrderDetail->replace($arrOrderDetail);
            }
            $tWowmaOrderList->updateData(['order_id' => $arrOrderInfo['order_id']], ['is_got' => 1]);
            DB::commit();
        } catch (\Exception $e) {
            DB::rollback();
            $message = "Process order id $orderId fail";
            print_r($message . PHP_EOL);
            Log::error($message);
            report($e);
            $this->error[] = $e->getMessage();
        }
    }
    
    /**
     * Export wowma xml
     *
     * @param  string $xml
     * @return array
     */
    public function exportWowmaXml($xml)
    {
        $responData  = simplexml_load_string($xml);
        if ((int)$responData->result->status !== 0) {
            $errorCode = (string)$responData->result->error->code;
            $errorMess = (string)$responData->result->error->message;
            $message = "Process get info error [$errorCode]: $errorMess";
            print_r($message . PHP_EOL);
            Log::error($message);
            $error  = "------------------------------------------" . PHP_EOL;
            $error .= basename(__CLASS__) . PHP_EOL;
            $error .= PHP_EOL . $message;
            $error .= PHP_EOL . "------------------------------------------" . PHP_EOL;
            $this->slack->notify(new SlackNotification($error));
            return [[], []];
        }
        $arrOrderInfo = [
            'order_id'                  => Helper::val($responData->orderInfo->orderId),
            'order_date'                => Helper::val($responData->orderInfo->orderDate),
            'sell_method_segment'       => Helper::val($responData->orderInfo->sellMethodSegment),
            'release_date'              => Helper::val($responData->orderInfo->releaseDate),
            'site_and_device'           => Helper::val($responData->orderInfo->siteAndDevice),
            'mail_address'              => Helper::val($responData->orderInfo->mailAddress),
            'raw_mail_address'          => Helper::val($responData->orderInfo->rawMailAddress),
            'orderer_name'              => Helper::val($responData->orderInfo->ordererName),
            'orderer_kana'              => Helper::val($responData->orderInfo->ordererKana),
            'orderer_zip_code'          => Helper::val($responData->orderInfo->ordererZipCode),
            'orderer_address'           => Helper::val($responData->orderInfo->ordererAddress),
            'orderer_phone_number1'     => Helper::val($responData->orderInfo->ordererPhoneNumber1),
            'orderer_phone_number2'     => Helper::val($responData->orderInfo->ordererPhoneNumber2),
            'nickname'                  => Helper::val($responData->orderInfo->nickname),
            'sender_name'               => Helper::val($responData->orderInfo->senderName),
            'sender_kana'               => Helper::val($responData->orderInfo->senderKana),
            'sender_zip_code'           => Helper::val($responData->orderInfo->senderZipCode),
            'sender_address'            => Helper::val($responData->orderInfo->senderAddress),
            'sender_phone_number1'      => Helper::val($responData->orderInfo->senderPhoneNumber1),
            'sender_phone_number2'      => Helper::val($responData->orderInfo->senderPhoneNumber2),
            'sender_shop_cd'            => Helper::val($responData->orderInfo->senderShopCd),
            'order_option'              => Helper::val($responData->orderInfo->orderOption),
            'settlement_name'           => Helper::val($responData->orderInfo->settlementName),
            'user_comment'              => Helper::val($responData->orderInfo->userComment),
            'memo'                      => Helper::val($responData->orderInfo->memo),
            'order_status'              => Helper::val($responData->orderInfo->orderStatus),
            'contact_status'            => Helper::val($responData->orderInfo->contactStatus),
            'contact_date'              => Helper::val($responData->orderInfo->contactDate),
            'authorization_status'      => Helper::val($responData->orderInfo->authorizationStatus),
            'authorization_date'        => Helper::val($responData->orderInfo->authorizationDate),
            'payment_status'            => Helper::val($responData->orderInfo->paymentStatus),
            'payment_date'              => Helper::val($responData->orderInfo->paymentDate),
            'ship_status'               => Helper::val($responData->orderInfo->shipStatus),
            'ship_date'                 => Helper::val($responData->orderInfo->shipDate),
            'print_status'              => Helper::val($responData->orderInfo->printStatus),
            'print_date'                => Helper::val($responData->orderInfo->printDate),
            'cancel_status'             => Helper::val($responData->orderInfo->cancelStatus),
            'cancel_reason'             => Helper::val($responData->orderInfo->cancelReason),
            'cancel_comment'            => Helper::val($responData->orderInfo->cancelComment),
            'total_sale_price'          => Helper::val($responData->orderInfo->totalSalePrice),
            'total_sale_unit'           => Helper::val($responData->orderInfo->totalSaleUnit),
            'postage_price'             => Helper::val($responData->orderInfo->postagePrice),
            'charge_price'              => Helper::val($responData->orderInfo->chargePrice),
            'total_item_option_price'   => Helper::val($responData->orderInfo->totalItemOptionPrice),
            'total_gift_wrapping_price' => Helper::val($responData->orderInfo->totalGiftWrappingPrice),
            'total_price'               => Helper::val($responData->orderInfo->totalPrice),
            'coupon_total_price'        => Helper::val($responData->orderInfo->couponTotalPrice),
            'use_point'                 => Helper::val($responData->orderInfo->usePoint),
            'use_point_cancel'          => Helper::val($responData->orderInfo->usePointCancel),
            'use_au_point_price'        => Helper::val($responData->orderInfo->useAuPointPrice),
            'use_au_point'              => Helper::val($responData->orderInfo->useAuPoint),
            'use_au_point_cancel'       => Helper::val($responData->orderInfo->useAuPointCancel),
            'request_price'             => Helper::val($responData->orderInfo->requestPrice),
            'point_fixed_date'          => Helper::val($responData->orderInfo->pointFixedDate),
            'point_fixed_status'        => Helper::val($responData->orderInfo->pointFixedStatus),
            'settle_status'             => Helper::val($responData->orderInfo->settleStatus),
            'authori_timelimit_date'    => Helper::val($responData->orderInfo->authoriTimelimitDate),
            'pg_result'                 => Helper::val($responData->orderInfo->pgResult),
            'pg_response_code'          => Helper::val($responData->orderInfo->pgResponseCode),
            'pg_response_detail'        => Helper::val($responData->orderInfo->pgResponseDetail),
            'pg_order_id'               => Helper::val($responData->orderInfo->pgOrderId),
            'pg_request_price'          => Helper::val($responData->orderInfo->pgRequestPrice),
            'coupon_type'               => Helper::val($responData->orderInfo->couponType),
            'coupon_key'                => Helper::val($responData->orderInfo->couponKey),
            'card_jadgement'            => Helper::val($responData->orderInfo->cardJadgement),
            'delivery_name'             => Helper::val($responData->orderInfo->deliveryName),
            'delivery_method_id'        => Helper::val($responData->orderInfo->deliveryMethodId),
            'delivery_id'               => Helper::val($responData->orderInfo->deliveryId),
            'delivery_request_day'      => Helper::val($responData->orderInfo->deliveryRequestDay),
            'delivery_request_time'     => Helper::val($responData->orderInfo->deliveryRequestTime),
            'shipping_date'             => Helper::val($responData->orderInfo->shippingDate),
            'shipping_carrier'          => Helper::val($responData->orderInfo->shippingCarrier),
            'shipping_number'           => Helper::val($responData->orderInfo->shippingNumber),
            'yamato_lnk_mgt_no'         => Helper::val($responData->orderInfo->yamatoLnkMgtNo)
        ];
        
        $arrOrderDetail = [];
        foreach ($responData->orderInfo->detail as $detail) {
            $arrOrderDetail[] = [
                'order_id'                 => Helper::val($responData->orderInfo->orderId),
                'order_detail_id'          => Helper::val($detail->orderDetailId),
                'item_management_id'       => Helper::val($detail->itemManagementId),
                'item_code'                => Helper::val($detail->itemCode),
                'lot_number'               => Helper::val($detail->lotnumber),
                'item_name'                => Helper::val($detail->itemName),
                'item_option'              => Helper::val($detail->itemOption),
                'item_option_commission'   => Helper::val($detail->itemOptionCommission),
                'item_option_price'        => Helper::val($detail->itemOptionPrice),
                'gift_wrapping_type'       => Helper::val($detail->giftWrappingType),
                'gift_wrapping_price'      => Helper::val($detail->giftWrappingPrice),
                'gift_message'             => Helper::val($detail->giftMessage),
                'noshi_type'               => Helper::val($detail->noshiType),
                'noshi_presenter_name1'    => Helper::val($detail->noshiPresenterName1),
                'noshi_presenter_name2'    => Helper::val($detail->noshiPresenterName2),
                'noshi_presenter_name3'    => Helper::val($detail->noshiPresenterName3),
                'item_cancel_status'       => Helper::val($detail->itemCancelStatus),
                'before_discount'          => Helper::val($detail->beforeDiscount),
                'discount'                 => Helper::val($detail->discount),
                'item_price'               => Helper::val($detail->itemPrice),
                'unit'                     => Helper::val($detail->unit),
                'total_item_price'         => Helper::val($detail->totalItemPrice),
                'total_item_charge_price'  => Helper::val($detail->totalItemChargePrice),
                'tax_type'                 => Helper::val($detail->taxType),
                'gift_point'               => Helper::val($detail->giftPoint),
                'shipping_day_disp_text'   => Helper::val($detail->shippingDayDispText),
                'shipping_time_limit_date' => Helper::val($detail->shippingTimeLimitDate)
            ];
        }
        return [$arrOrderInfo, $arrOrderDetail];
    }
}
