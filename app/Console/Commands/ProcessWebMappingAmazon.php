<?php
/**
 * Batch process web mapping Amazon
 *
 * @package    App\Console\Commands
 * @subpackage ProcessWebMappingAmazon
 * @copyright  Copyright (c) 2018 RiverCrane! Corporation. All Rights Reserved.
 * @author     Van Qui<van.qui@rivercrane.vn>
 */

namespace App\Console\Commands;

include app_path('lib/MarketplaceWebService/autoload.php');

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;
use App\Notifications\SlackNotification;
use App\Custom\Utilities;
use App\Notification;
use App\Events\Command as eCommand;
use Event;
use Config;
use Helper;
use DB;
use App;
use App\Models\Batches\MstOrder;
use App\Models\Batches\DtWebAmazonData;
use App\Models\Batches\DtWebMapping;

class ProcessWebMappingAmazon extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'process:web-mapping-amazon';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Process Web Mapping Amazon';

    /**
     * The notification.
     *
     * @var object
     */
    protected $slack;

    /**
     * The error.
     *
     * @var array
     */
    public $error = [];

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //Create log
        $arrayReplace = [':', '-'];
        $folder       = str_replace($arrayReplace, '_', $this->signature);
        Log::useDailyFiles(storage_path() . "/logs/$folder/history.log");
        //Process data
        Event::fire(new eCommand($this->signature, array('start' => true)));
        Log::info('Start batch process web mapping Amazon.');
        print_r("Start batch process web mapping Amazon." . PHP_EOL);
        $start       = microtime(true);
        $this->slack = new Notification(CHANNEL['horunba']);
        $this->getOrderAmazon();
        $this->checkOrderMaping();
        Event::fire(new eCommand($this->signature, array('end' => true, 'error' => $this->error)));
        $totalTime = round(microtime(true) - $start, 2);
        Log::info("End batch process web mapping Amazon with total time: $totalTime s.");
        print_r("End batch process web mapping Amazon with total time: $totalTime s.");
    }

    /**
     * Get order Amazon
     *
     * @return void
     */
    private function getOrderAmazon()
    {
        $amzModel  = new DtWebAmazonData();
        $modelO    = new MstOrder();
        $modelMap  = new DtWebMapping();
        $dataOrder = $modelO->getOrderAPIWebMappingAmazon(3, 50);
        $configAll = Config::get('amazonmws');
        $config = array (
            'ServiceURL'    => $configAll['ServiceURLOrder'],
            'ProxyHost'     => $configAll['ProxyHost'],
            'ProxyPort'     => $configAll['ProxyPort'],
            'ProxyUsername' => $configAll['ProxyUsername'],
            'ProxyPassword' => $configAll['ProxyPassword'],
            'MaxErrorRetry' => $configAll['MaxErrorRetry'],
        );
        $service = new \MarketplaceWebServiceOrders_Client(
            $configAll['AWS_ACCESS_KEY_ID'],
            $configAll['AWS_SECRET_ACCESS_KEY'],
            $configAll['APPLICATION_NAME'],
            $configAll['APPLICATION_VERSION'],
            $config
        );
        //Process get order from api
        $countInsert = 0;
        if ($dataOrder->count() > 0) {
            $dataOrder = $dataOrder->chunk(50);
            try {
                foreach ($dataOrder as $key => $orders) {
                    if ($key > 0) {
                        sleep(5);
                    }
                    $dataInsert = $this->callApiGetOrder($orders, $configAll, $service);
                    if (count($dataInsert) > 0) {
                        $amzModel->insert($dataInsert);
                        $countInsert += count($dataInsert);
                    }
                }
            } catch (\Exception $ex) {
                $message = "Code {$ex->getCode()}: {$ex->getMessage()}";
                Log::info($message);
                print_r($message . PHP_EOL);
                report($ex);
                return false;
            }
        }
        $message = "Insert $countInsert records into dt_web_amazon_data table";
        Log::info($message);
        print_r($message . PHP_EOL);
        //End
        //Process order in case column is_correct = 0
        $orderCorrect = $modelO->getOrderPayCorrect(3);
        $totalCount  = 0;
        if ($orderCorrect->count() > 0) {
            $orderCorrect = $orderCorrect->chunk(50);
            foreach ($orderCorrect as $key => $orders) {
                if ($key > 0) {
                    sleep(5);
                }
                $dataUpdate = $this->callApiGetOrder($orders, $configAll, $service);
                if (count($dataUpdate) > 0) {
                    foreach ($dataUpdate as $item) {
                        $orderNumber = $item['order_number'];
                        $amazonOrderStatus = 0;
                        $webOrderStatus = 0;
                        if ($item['order_status'] === 'Shipped') {
                            $amazonOrderStatus = 8;
                        } elseif ($item['order_status'] === 'Canceled') {
                            $amazonOrderStatus = 10;
                        }
                        $checkOrder = $modelO->checkOrderMapping($orderNumber, $amazonOrderStatus);
                        if ($checkOrder === 1) {
                            $modelMap->updateData($orderNumber, $amazonOrderStatus);
                            $totalCount++;
                        }
                    }
                }
            }
        }
        $message = "Update $totalCount records into dt_web_mapping table";
        Log::info($message);
        print_r($message . PHP_EOL);
    }

    /**
     * Call api get order Amazon to save to dt_web_amazon_data
     *
     * @param  string  $orders
     * @param  array   $configAll
     * @param  object  $service
     * @return array $dataInsert
     */
    private function callApiGetOrder($orders, $configAll, $service)
    {
        $orders  = $orders->pluck('received_order_id')->toArray();
        $request = new \MarketplaceWebServiceOrders_Model_GetOrderRequest();
        $request->setSellerId($configAll['MERCHANT_ID']);
        $request->setAmazonOrderId($orders);
        $xml     = $service->getOrder($request)->toXML();
        $resData = $this->mappingXmlToOrders($xml);
        $dataInsert = [];
        foreach ($resData['orders'] as $itemOrder) {
            $requestPrice = $itemOrder['order_total_amount'];
            $paymentMethod = 0;
            if ($itemOrder['payment_method_detail'] === 'Invoice') {
                $paymentMethod = 5;
            } elseif ($itemOrder['payment_method_detail'] === 'Standard') {
                $paymentMethod = 1;
            } elseif ($itemOrder['payment_method_detail'] === 'CashOnDelivery') {
                $paymentMethod = 3;
                 $requestPrice = $itemOrder['payment_COD'];
            } else {
                $paymentMethod = 1;
            }
            $dataInsert[] = [
                'order_number'          => $itemOrder['amazon_order_id'],
                'payment_method'        => $paymentMethod,
                'payment_method_detail' => $itemOrder['payment_method_detail'],
                'request_price'         => $requestPrice,
                'order_status'          => $itemOrder['order_status']
            ];
        }
        return $dataInsert;
    }

    /**
     * Process data amazon and then save to dt_web_mapping
     *
     * @return void
     */
    private function checkOrderMaping()
    {
        $modelMap       = new DtWebMapping();
        $amzModel       = new DtWebAmazonData();
        $dataOrderCheck = $amzModel->getDataProcessWebAmazon();
        $count     = 0;
        if ($dataOrderCheck->count() > 0) {
            $arrInsert = [];
            foreach ($dataOrderCheck as $data) {
                try {
                    $webPaymentMethod = (int)$data['payment_method'];
                    $webOrderStatus = 0;
                    if ($data['order_status'] === 'Shipped') {
                        $webOrderStatus = 8;
                    } elseif ($data['order_status'] === 'Canceled') {
                        $webOrderStatus = 10;
                    }

                    $diffType = [];
                    if ($data['order_payment_method'] !== $webPaymentMethod) {
                        $diffType[] = '支払方法';
                    }
                    if ($data['order_request_price'] !== $data['request_price'] && $data['order_order_status'] <> 10) {
                        $diffType[] = 'WEB金額';
                    }
                    if ($data['order_order_status'] !== $webOrderStatus) {
                        $diffType[] = 'ステータス';
                    }

                    $diffPrice = (int)$data['order_request_price'] - (int)$data['request_price'];

                    $finishedDate = null;
                    $isCorrected  = 0;
                    if (empty($diffType)) {
                        $finishedDate = date('Y-m-d H:i:s');
                        $isCorrected  = 1;
                    }

                    $arrInsert = [
                        'received_order_id'  => $data['order_number'],
                        'mall_id'            => 3,
                        'web_payment_method' => $webPaymentMethod,
                        'web_order_status'   => $webOrderStatus,
                        'web_request_price'  => $data['request_price'],
                        'different_type'     => implode("\n", $diffType),
                        'different_price'    => $diffPrice,
                        'occorred_reason'    => 0,
                        'process_content'    => 0,
                        'finished_date'      => $finishedDate,
                        'is_corrected'       => $isCorrected,
                        'is_deleted'         => 0,
                        'receive_id'         => 0,
                        'in_ope_cd'          => 'OPE99999',
                        'in_date'            => date('Y-m-d H:i:s'),
                        'up_ope_cd'          => 'OPE99999',
                        'up_date'            => date('Y-m-d H:i:s')
                    ];
                    $modelMap->insertIgnore($arrInsert);
                    $amzModel->where('order_number', $data['order_number'])
                        ->update(['process_flg' => 1]);
                    $count++;
                } catch (\Exception $ex) {
                    $message = "Code {$ex->getCode()}: {$ex->getMessage()}";
                    Log::info($message);
                    print_r($message . PHP_EOL);
                    report($ex);
                    return false;
                }
            }
        }
        $message = "Insert $count records into dt_web_mapping table";
        Log::info($message);
        print_r($message . PHP_EOL);
    }

    /**
     * Mapping xml to orders
     *
     * @param  string $xml
     * @return array
     */
    private function mappingXmlToOrders($xml)
    {
        $rootNode = 'GetOrderResult';

        $dom = simplexml_load_string($xml);
        $resOrders = [];
        if (isset($dom->{$rootNode})
            && isset($dom->{$rootNode}->Orders)
            && isset($dom->{$rootNode}->Orders->Order)) {
            foreach ($dom->{$rootNode}->Orders->Order as $order) {
                $amountPaymentGC  = 0;
                $amountPaymentCOD = 0;
                $amountPaymentCP  = 0;
                if (isset($order->PaymentExecutionDetail)
                    && isset($order->PaymentExecutionDetail->PaymentExecutionDetailItem)) {
                    foreach ($order->PaymentExecutionDetail->PaymentExecutionDetailItem as $item) {
                        $paymentMethod = (string) $item->PaymentMethod;
                        if ($paymentMethod === 'GC') {
                            $amountPaymentGC = (int) $item->Payment->Amount;
                        }
                        if ($paymentMethod === 'COD') {
                            $amountPaymentCOD = (int) $item->Payment->Amount;
                        }
                        if ($paymentMethod === 'PointsAccount') {
                            $amountPaymentCP = (int) $item->Payment->Amount;
                        }
                    }
                }

                $resOrders[] = [
                    'amazon_order_id'                 => (string) $order->AmazonOrderId,
                    'latest_ship_date'                => (string) $order->LatestShipDate,
                    'order_type'                      => (string) $order->OrderType,
                    'purchase_date'                   => (string) $order->PurchaseDate,
                    'payment_GC'                      => $amountPaymentGC,
                    'payment_COD'                     => $amountPaymentCOD,
                    'payment_CP'                      => $amountPaymentCP,
                    'buyer_email'                     => (string) $order->BuyerEmail,
                    'is_replacement_order'            => (string) $order->IsReplacementOrder,
                    'last_update_date'                => (string) $order->LastUpdateDate,
                    'number_of_items_shipped'         => (int) $order->NumberOfItemsShipped,
                    'ship_service_level'              => (string) $order->ShipServiceLevel,
                    'order_status'                    => (string) $order->OrderStatus,
                    'promise_response_due_date'       => (string) $order->PromiseResponseDueDate,
                    'sales_channel'                   => (string) $order->SalesChannel,
                    'shipped_by_amazon_TFM'           => (string) $order->ShippedByAmazonTFM,
                    'is_business_order'               => (string) $order->IsBusinessOrder,
                    'latest_delivery_date'            => (string) $order->LatestDeliveryDate,
                    'number_of_items_unshipped'       => (int) $order->NumberOfItemsUnshipped,
                    'payment_method_detail'           => (string)$order->PaymentMethodDetails->PaymentMethodDetail,
                    'buyer_name'                      => (string) $order->BuyerName,
                    'earliest_delivery_date'          => (string) $order->EarliestDeliveryDate,
                    'order_total_amount'              => (int) $order->OrderTotal->Amount,
                    'is_premium_order'                => (int) $order->IsPremiumOrder,
                    'earliest_ship_date'              => (string) $order->EarliestShipDate,
                    'market_place_id'                 => (string) $order->MarketplaceId,
                    'fulfillment_channel'             => (string) $order->FulfillmentChannel,
                    'payment_method'                  => (string) $order->PaymentMethod,
                    'ship_postal_code'                => (string) $order->ShippingAddress->PostalCode,
                    'ship_state_or_region'            => (string) $order->ShippingAddress->StateOrRegion,
                    'ship_phone'                      => (string) $order->ShippingAddress->Phone,
                    'ship_country_code'               => (string) $order->ShippingAddress->CountryCode,
                    'ship_name'                       => (string) $order->ShippingAddress->Name,
                    'ship_address_line1'              => (string) $order->ShippingAddress->AddressLine1,
                    'ship_address_line2'              => (string) $order->ShippingAddress->AddressLine2,
                    'ship_address_line3'              => (string) $order->ShippingAddress->AddressLine3,
                    'is_prime'                        => (string) $order->IsPrime,
                    'shipment_service_level_category' => (string) $order->ShipmentServiceLevelCategory,
                    'process_flg'                     => 0,
                    'is_deleted'                      => 0,
                    'created_at'                      => date('Y-m-d H:i:s')
                ];
            }
        }

        $nextToken = '';
        if (isset($dom->{$rootNode}) && isset($dom->{$rootNode}->NextToken)) {
            $nextToken = (string) $dom->{$rootNode}->NextToken;
        }

        return [
            'orders' => $resOrders,
            'nextToken' => $nextToken
        ];
    }
}
