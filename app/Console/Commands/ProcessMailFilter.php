<?php
/**
 * Batch process auto mail filter
 *
 * @package    App\Console\Commands
 * @subpackage ProcessMailFilter
 * @copyright  Copyright (c) 2018 RiverCrane! Corporation. All Rights Reserved.
 * @author     Truong Nghia<truong.van.nghia@rivercrane.vn>
 */
namespace App\Console\Commands;

use Illuminate\Console\Command;
use Event;
use App\Events\Command as eCommand;
use Illuminate\Support\Facades\Log;
use App\Models\Batches\DtMailFilter;
use App\Models\Batches\DtReceiveMailList;
use App\Custom\Utilities;
use DB;

class ProcessMailFilter extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'process:mail-filter';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Process auto mail filter';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * The error.
     *
     * @var string
     */
    public $error = [];

    /**
     * The limit.
     *
     * @var int
     */
    public $limit = 50000;

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //Create log
        $arrayReplace = [':', '-'];
        $folder       = str_replace($arrayReplace, '_', $this->signature);
        Log::useDailyFiles(storage_path() . "/logs/$folder/history.log");
        $start    = microtime(true);
        //Process data
        Event::fire(new eCommand($this->signature, array('start' => true)));
        $message = 'Start process auto mail filter';
        Log::info($message);
        print_r($message . PHP_EOL);
        $modelMF  = new DtMailFilter();
        $modelRML = new DtReceiveMailList();
        $datasMF  = $modelMF->get();
        $datasRML = $modelRML->getDataProcessMailFilter($this->limit)->keyBy('rec_mail_index');
        if (count($datasMF) === 0 || count($datasRML) === 0) {
            $message = 'No data';
            Log::info($message);
            print_r($message . PHP_EOL);
        } else {
            try {
                DB::beginTransaction();
                $arrKeys = [];
                $count   = count($datasRML);
                foreach ($datasMF as $dataMF) {
                    foreach ($datasRML as $dataRML) {
                        $arrUpdate = [];
                        if (!in_array($dataRML->rec_mail_index, $arrKeys)) {
                            $arrKeys[$dataRML->rec_mail_index] = $dataRML->rec_mail_index;
                        }
                        if (!empty($dataMF->set_department)) {
                            $arrUpdate['deparment'] = $dataMF->set_department;
                        }
                        if (!empty($dataMF->set_status)) {
                            $arrUpdate['status'] = $dataMF->set_status;
                        }
                        if ((!empty($dataMF->filter_subject)
                            && strpos($dataRML->mail_subject, $dataMF->filter_subject) === false)
                            || (!empty($dataMF->filter_mail_address)
                            && strpos($dataRML->mail_from, $dataMF->filter_mail_address) === false)
                            || (!empty($dataMF->filter_inquiry_code)
                            && strpos($dataRML->inquiry_code, $dataMF->filter_inquiry_code) === false)
                        ) {
                            continue;
                        } else {
                            $arrUpdate['filter_checked'] = 1;
                            $arrUpdate['up_ope_cd']      = 'OPE99999';
                            $arrUpdate['up_date']        = now();
                            $modelRML->updateDataByKey($dataRML->rec_mail_index, $arrUpdate);
                            if (isset($arrKeys[$dataRML->rec_mail_index])) {
                                unset($arrKeys[$dataRML->rec_mail_index]);
                            }
                            if (isset($datasRML[$dataRML->rec_mail_index])) {
                                unset($datasRML[$dataRML->rec_mail_index]);
                            }
                        }
                    }
                }
                if (count($arrKeys) !== 0) {
                    $modelRML->updateData(
                        $arrKeys,
                        [
                            'filter_checked' => 1,
                            'up_ope_cd'      => 'OPE99999',
                            'up_date'        => now(),
                        ]
                    );
                }
                DB::commit();
                $message = "Process success total: $count records";
                Log::info($message);
                print_r($message . PHP_EOL);
            } catch (\Exception $e) {
                DB::rollback();
                $message = Utilities::checkMessageException($e);
                print_r($message . PHP_EOL);
                Log::error($message);
                report($e);
            }
        }
        Event::fire(new eCommand($this->signature, array('end' => true, 'error' => $this->error)));
        $totalTime = round(microtime(true) - $start, 2);
        $message = "End process auto mail filter with total time: $totalTime s.";
        Log::info($message);
        print_r($message);
    }
}
