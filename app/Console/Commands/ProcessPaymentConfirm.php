<?php
/**
 * Batch process payment confirm
 *
 * @package    App\Console\Commands
 * @subpackage ProcessPaymentConfirm
 * @copyright  Copyright (c) 2017 RiverCrane! Corporation. All Rights Reserved.
 * @author     Truong Nghia<truong.van.nghia@rivercrane.vn>
 */

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Notification;
use App\Notifications\SlackNotification;
use Illuminate\Support\Facades\Log;
use App\Models\Batches\MstOrder;
use App\Models\Batches\MstOrderDetail;
use App\Models\Batches\DtOrderProductDetail;
use App\Models\Batches\MstPaymentMethod;
use App\Models\Batches\MstMailTemplate;
use App\Models\Batches\DtSendMailList;
use App\Models\Batches\MstPaymentAccount;
use App\Console\Commands\ProcessMailQueue;
use App\Events\Command as eCommand;
use App\Custom\Utilities;
use Event;
use Config;
use DB;
use App;

class ProcessPaymentConfirm extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'process:payment-confirm';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Process order in case have payment confirm';

    /**
     * Count success.
     *
     * @var int
     */
    public $success = 0;

    /**
     * The notification.
     *
     * @var object
     */
    protected $slack;

     /**
     * Count fail.
     *
     * @var int
     */
    public $fail = 0;

    /**
     * The error.
     *
     * @var array
     */
    public $error = [];

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //Create log
        $arrayReplace = [':', '-'];
        $folder       = str_replace($arrayReplace, '_', $this->signature);
        Log::useDailyFiles(storage_path() . "/logs/$folder/history.log");
        //Process data
        Event::fire(new eCommand($this->signature, array('start' => true)));
        Log::info('Start batch process payment confirm.');
        print_r("Start batch process payment confirm." . PHP_EOL);
        $this->slack = new Notification(CHANNEL['horunba']);
        $start     = microtime(true);
        $modelO    = new MstOrder();
        $modelOPD  = new DtOrderProductDetail();
        $modelOD   = new MstOrderDetail();
        $dataStep1 = $modelO->getDataPaymentConfirm1();
        $check1    = false;
        $check2    = false;
        if (count($dataStep1) !== 0) {
            $receiveID  = '';
            $arrPayment = [2, 6, 7, 13, 14, 15, 16, 17];
            foreach ($dataStep1 as $data) {
                if ($receiveID !== '' && $receiveID === $data->receive_id) {
                    continue;
                }
                $arrUpdate = [
                    'order_sub_status' => ORDER_SUB_STATUS['NEW'],
                    'is_mail_sent'     => 0,
                    'up_ope_cd'        => 'OPE99999',
                ];
                $receiveID = $data->receive_id;
                if (is_null($data->is_available) ||
                    (!is_null($data->is_available) && $data->is_available === $data->is_mail_sent)) {
                    if (in_array($data->payment_method, $arrPayment)) {
                        $arrUpdate['order_status'] = ORDER_STATUS['PAYMENT_CONFIRM'];
                        if (in_array($data->payment_method, [7, 13, 14, 15, 16, 17])) {
                            $arrUpdate['is_mail_sent'] = 1;
                            $arrUpdate['payment_request_date'] = now();
                        }
                    } elseif ($data->payment_method == 1 && $data->mall_id === 9) {
                        $arrUpdate['order_status'] = ORDER_STATUS['PAYMENT_CONFIRM'];
                        $arrUpdate['is_mail_sent'] = 1;
                        $arrUpdate['payment_request_date'] = now();
                        $arrUpdate['payment_status'] = 0;
                    } elseif ($data->mall_id === 10 && in_array($data->payment_method, [1, 18, 19])) {
                        $arrUpdate['order_status'] = ORDER_STATUS['PAYMENT_CONFIRM'];
                        $arrUpdate['is_mail_sent'] = 1;
                        $arrUpdate['payment_request_date'] = now();
                    } else {
                        $arrUpdate['order_status'] = ORDER_STATUS['ORDER'];
                    }
                    $check = $modelO->updateData(array($receiveID), $arrUpdate);
                    if ($check) {
                        $this->success++;
                    } else {
                        $this->fail++;
                    }
                }
            }
            $message = "Update order status from Estimate to [Payment Confirm, Order] success: {$this->success}".
                       " and fail: {$this->fail} records.";
            Log::info($message);
            print_r($message . PHP_EOL);
        } else {
            $check1 = true;
        }
        $dataStep2 = $modelO->getDataPaymentComfirm2();
        if (count($dataStep2) !== 0) {
            $receiveID = '';
            $this->success = 0;
            $this->fail = 0;
            foreach ($dataStep2 as $data) {
                if ($receiveID !== '' && $receiveID === $data->receive_id) {
                    continue;
                }
                DB::transaction(function () use ($modelO, $modelOD, $modelOPD, $data) {
                    $arrUpdate = [];
                    $receiveID = $data->receive_id;
                    if ($data->payment_status === 1) {
                        $arrUpdate['order_sub_status'] = ORDER_SUB_STATUS['DONE'];
                        $arrUpdate['is_mail_sent']     = 0;
                        if ($data->payment_method === 7 || $data->mall_id === 10) {
                            $arrUpdate['is_mail_sent'] = 1;
                        }
                    } else {
                        if (empty($data->payment_request_date)) {
                            $arrUpdate['order_sub_status'] = ORDER_SUB_STATUS['ERROR'];
                            $arrUpdate['err_text'] = 'The payment request date is null.';
                        } else {
                            $checkDate = (time() - strtotime($data->payment_request_date))/ 86400;
                            $mallId = (int)$data->mall_id;
                            if ($mallId === 9) {
                                $expiration = 14;
                            } else {
                                $expiration = $data->expiration_priod;
                            }
                            if ($checkDate >= $data->delay_priod && $checkDate < $expiration) {
                                $arrUpdate['order_sub_status'] = ORDER_SUB_STATUS['DELAY'];
                                $arrUpdate['is_mail_sent']     = 0;
                            } elseif ($checkDate >= $expiration) {
                                $arrUpdate['order_sub_status']    = ORDER_SUB_STATUS['DONE'];
                                $arrUpdate['order_status']        = ORDER_STATUS['CANCEL'];
                                $arrUpdate['ship_charge']         = 0;
                                $arrUpdate['pay_charge_discount'] = 0;
                                $arrUpdate['request_price']       = 0;
                                $arrUpdate['discount']            = 0;
                                $arrUpdate['used_coupon']         = 0;
                                $arrUpdate['used_point']          = 0;
                                $arrUpdate['pay_charge']          = 0;
                                $arrUpdate['goods_price']         = 0;
                                $arrUpdate['total_price']         = 0;
                                $arrUpdate['cancel_reason']       = '7';
                                $arrUpdate['is_mail_sent']        = 1;
                                $modelOPD->updateCancelStock($receiveID);
                                $modelOPD->updateDataByCondition(
                                    ['receive_id' => $receiveID],
                                    ['product_status' => PRODUCT_STATUS['CANCEL']]
                                );

                                $arrUpdateOrderDetail = [
                                    'quantity' => 0,
                                    'price'    => 0,
                                ];
                                $modelOD->updateData(['receive_id' => $receiveID], $arrUpdateOrderDetail);
                            }
                        }
                    }
                    if (count($arrUpdate) !== 0) {
                        $arrUpdate['up_ope_cd'] = 'OPE99999';
                        $check = $modelO->updateData(array($receiveID), $arrUpdate);
                        if (isset($arrUpdate['order_status'])) {
                            $checkStatus = $arrUpdate['order_status'];
                            if ($checkStatus === ORDER_STATUS['CANCEL']) {
                                if ($data->mall_id === 9) {
                                    $this->processParseMail(779, $data);
                                } else {
                                    $this->processParseMail(107, $data);
                                }
                            }
                        }
                        if ($check) {
                            $this->success++;
                        } else {
                            $this->fail++;
                        }
                    }
                }, 5);
            }
            $message = "Update status from [New, Processing] to [Done, Delay, Expiration] success: {$this->success}".
                       " and fail: {$this->fail} records.";
            Log::info($message);
            print_r($message . PHP_EOL);
        } else {
            $check2 = true;
        }

        $dataStepWowma = $modelO->getDataPaymentComfirmWowma();
        $checkWowma    = false;
        if ($dataStepWowma->count() > 0) {
            $environment = 'real';
            if (App::environment(['local', 'test'])) {
                $environment = 'test';
            }
            $shopId    = Config::get("wowma.$environment.shop_id");
            $wowmaAuth = Config::get("wowma.$environment.auth_key");
            $urlApi    = Config::get("wowma.$environment.url");
            $wowmaHeader = array(
                "Content-Type: application/x-www-form-urlencoded",
                "Authorization: $wowmaAuth"
            );
            $wowmaFail    = 0;
            $wowmaSuccess = 0;
            foreach ($dataStepWowma as $data) {
                $wowmaUrl = $urlApi . "/searchTradeInfoProc?shopId={$shopId}&orderId={$data->received_order_id}";
                $ch = curl_init($wowmaUrl);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($ch, CURLOPT_HTTPHEADER, $wowmaHeader);
                $responseXml = curl_exec($ch);
                curl_close($ch);
                $responData = simplexml_load_string($responseXml);
                if ((int)$responData->result->status !== 0) {
                    $errorCode = (string)$responData->result->error->code;
                    $errorMess = (string)$responData->result->error->message;
                    $message = "Confirm payment Wowma order [{$data->received_order_id}] has error:" . PHP_EOL;
                    $message .= "[$errorCode] $errorMess";
                    print_r($message . PHP_EOL);
                    Log::error($message);
                    $error  = "------------------------------------------" . PHP_EOL;
                    $error .= basename(__CLASS__) . PHP_EOL;
                    $error .= PHP_EOL . $message;
                    $error .= PHP_EOL . "------------------------------------------" . PHP_EOL;
                    $this->slack->notify(new SlackNotification($error));
                    $wowmaFail++;
                    $modelO->updateData([$data->receive_id], [
                        'message_api'    => $errorMess,
                        'error_code_api' => $errorCode,
                    ]);
                    continue;
                }
                $settleStatus = (string)$responData->orderInfo->settleStatus;
                if ($settleStatus === 'AD') {
                    $arrUpdateWowma = ['payment_status' => 1];
                    if ($data->order_sub_status === ORDER_SUB_STATUS['DELAY']) {
                        $arrUpdateWowma['order_sub_status'] = ORDER_SUB_STATUS['NEW'];
                        $arrUpdateWowma['is_mail_sent']     = 1;
                    }
                    $modelO->updateData([$data->receive_id], $arrUpdateWowma);
                    $wowmaSuccess++;
                }
            }
            $message = "Confirm payment Wowma success: {$wowmaSuccess}".
                       " and fail: {$wowmaFail} records.";
            Log::info($message);
            print_r($message . PHP_EOL);
        } else {
            $checkWowma = true;
        }
        if ($check1 && $check2 && $checkWowma) {
            Log::info('No data process');
            print_r("No data process" . PHP_EOL);
        }
        Event::fire(new eCommand($this->signature, array('end' => true, 'error' => $this->error)));
        $totalTime = round(microtime(true) - $start, 2);
        Log::info("End batch process payment confirm with total time: $totalTime s.");
        print_r("End batch process payment confirm with total time: $totalTime s.");
    }
    /**
    * Process parse mail by mail_id.
    * @param string $id
    * @param object $data
    * @return void
    */
    public function processParseMail($id, $data)
    {
        $mstMailTemplate  = new MstMailTemplate();
        $modelSML   = new DtSendMailList();
        $modelO   = new MstOrder();
        $modelPA  = new MstPaymentAccount();
        $dataMailTemplate = $mstMailTemplate->getDataCancelOrder($id);
        $mailContent = '';
        $mailSubject = '';
        $arrUpdate   = [];
        if (!empty($dataMailTemplate)) {
            list($mailContent, $arrUpdate) = ProcessMailQueue::processContent(
                $dataMailTemplate->mail_content,
                $data->payment_method,
                $data->received_order_id,
                true
            );
            $mailSubject = ProcessMailQueue::processContent(
                $dataMailTemplate->mail_subject,
                $data->payment_method,
                $data->received_order_id,
                false
            );
            if (!empty($data->signature_content)) {
                $mailContent = $mailContent . PHP_EOL . $data->signature_content;
            }
        }
        $mailId = isset($dataMailTemplate->mail_id) ? $dataMailTemplate->mail_id : '';
        // Add new record to dt_send_mail_list
        $dataAddSendMailList['receive_order_id']    = $data->received_order_id;
        $dataAddSendMailList['order_status_id']     = ORDER_STATUS['CANCEL'];
        $dataAddSendMailList['order_sub_status_id'] = ORDER_SUB_STATUS['DONE'];
        $dataAddSendMailList['operater_send_index'] = 0;
        $dataAddSendMailList['payment_method']      = $data->payment_method;
        $dataAddSendMailList['mall_id']             = $data->mall_id;
        $dataAddSendMailList['mail_id']             = $id;
        $dataAddSendMailList['mail_subject']        = $mailSubject;
        $dataAddSendMailList['mail_content']        = $mailContent;
        $dataAddSendMailList['attached_file_path']  = null;
        $dataAddSendMailList['mail_to']             = $data->email;
        $dataAddSendMailList['mail_cc']             = env('MAIL_CC');
        $dataAddSendMailList['mail_bcc']            = env('MAIL_BCC');
        $dataAddSendMailList['mail_from']           = $data->mall_id === 1 ?
                                                    'rakuten@diy-tool.com' :
                                                    env('MAIL_FROM_ADDRESS');
        $dataAddSendMailList['send_status']         = 0;
        $dataAddSendMailList['send_type']           = 1;
        $dataAddSendMailList['error_code']          = null;
        $dataAddSendMailList['error_message']       = null;
        $dataAddSendMailList['in_ope_cd']           = 'OPE99999';
        $dataAddSendMailList['in_date']             = now();
        $dataAddSendMailList['up_date']             = now();
        $dataAddSendMailList['up_ope_cd']           = 'OPE99999';
        $modelSML->insert($dataAddSendMailList);
        if (count($arrUpdate) !== 0) {
            if ($arrUpdate['flg_payment']) {
                $modelPA->where(['name' => $arrUpdate['name']])
                    ->update(['current_account' => $arrUpdate['current_account']]);
            }
            $modelO->updateData(
                [$arrUpdate['receive_id']],
                [
                    'payment_account'      => $arrUpdate['payment_account'],
                    'payment_request_date' => $arrUpdate['payment_request_date'],
                ]
            );
        }
    }
}
