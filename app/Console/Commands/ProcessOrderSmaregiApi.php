<?php
/**
 * Batch process get info from smaregi api
 *
 * @package    App\Console\Commands
 * @subpackage ProcessOrderSmaregiApi
 * @copyright  Copyright (c) 2018 RiverCrane! Corporation. All Rights Reserved.
 * @author     Le Hung<le.hung.rcvn2012@gmail.com>
 */

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;
use Validator;
use App\Notification;
use App\Notifications\SlackNotification;
use App;
use Config;
use Event;
use App\Events\Command as eCommand;
use App\Models\Batches\DtOrderStoreOrderInfo;
use App\Models\Batches\DtOrderStoreOrderDetail;
use App\Models\Batches\MstOrder;
use App\Models\Batches\MstOrderDetail;

class ProcessOrderSmaregiApi extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'process:smaregi-api';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Get info from smaregi api';

    /**
     * The error.
     *
     * @var string
     */
    public $error = [];

    /**
     * The folder.
     *
     * @var string
     */
    public $folder = null;

    /**
     * Size insert
     * @var int
     */
    private $sizeInsert = 500;

    /**
     * Smaregi contract id
     * @var contract_id
     */
    public $contractId = '';

    /**
     * Smaregi access token
     * @var accessToken
     */
    public $accessToken = '';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        $this->contractId   = env('CONTRACT_ID');
        $this->accessToken  = env('ACCESS_TOKEN');
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $start          = microtime(true);
        $requestParams['header'] = array(
            "Content-Type:application/x-www-form-urlencoded",
            "X_contract_id:$this->contractId",
            "X_access_token:$this->accessToken",
        );
        $fields     = '["storageInfoId","recipientOrderId","orderedDate","status","modified","identificationNo"]';
        $startDate  = date("Y-m-d H:i:s", strtotime("-245 minutes"));
        $requestParams['fields'] = http_build_query(array(
            'proc_name' => 'storage_info_ref',
            'params' => '{
                "fields":'.$fields.',
                "conditions":[{"modified >=":"'.$startDate.'"}],
                "table_name":"StorageInfo"
            }'
        ));

        $response = $this->curlGet($requestParams);
        //Create log
        $arrayReplace = [':', '-'];
        $this->folder       = str_replace($arrayReplace, '_', $this->signature);
        Log::useDailyFiles(storage_path() . "/logs/$this->folder/history.log");
        //Process data
        Event::fire(new eCommand($this->signature, array('start' => true)));
        Log::info('Start process smaregi api');
        print_r("Start process smaregi api" . PHP_EOL);

        // Process Get Order
        print_r("-------- PROCESS STEP 1 - GET ORDERS --------" . PHP_EOL);
        $this->processContent1($response);

        // Process Order Detail
        print_r("-------- PROCESS STEP 2 - GET ORDERS DETAIL --------" . PHP_EOL);
        $this->processContent2();

        // Process Order Status
        print_r("-------- PROCESS STEP 3 - UPDATE STATUS : CASE E02 --------" . PHP_EOL);
        $this->processContent3();

        // Process Order Status
        print_r("-------- PROCESS STEP 4 - UPDATE STATUS : CASE E03 --------" . PHP_EOL);
        $this->processContent4();
        // Process Clone Data And Update horunba_order_id
        print_r("-------- PROCESS STEP 5 - MOVE DATA --------" . PHP_EOL);
        $this->processContent5();

        Event::fire(new eCommand($this->signature, array('end' => true, 'error' => $this->error)));
        $totalTime = round(microtime(true) - $start, 2);
        Log::info("End batch process smaregi  api: $totalTime s.");
        print_r("End batch process smaregi  api: $totalTime s." . PHP_EOL);
    }

    /**
     * Get orders from smaregi api
     *
     * @param   array  $response
     * @return  array
     */
    private function processContent1($response)
    {
        $model              = new DtOrderStoreOrderInfo();
        $apiSmaregiFields   = [
            'order_id',
            'order_to_id',
            'order_date',
            'status',
            'up_date',
            'identification_no'
        ];
        $arrayStorageInfoId = [];
        $arrayInsert        = [];
        $arrError           = [];

        if (!empty($response) && !isset($response->error_code)) {
            if (count($response->result) > 0) {
                foreach ($response->result as $key => $value) {
                    $arrayStorageInfoId[$key] = $value->storageInfoId;
                    if (!isset($value->identificationNo)) {
                        $value->identificationNo = null;
                    }
                    $input = array_combine($apiSmaregiFields, (array)$value);
                    $input['process_status'] = 0;
                    $input['delivery_plan_date'] = null;
                    $input['received_date'] = null;
                    Validator::extend('checkDuplicateId', function (
                        $parameters,
                        $value,
                        $attribute
                    ) use ($arrayStorageInfoId) {
                        if (count(array_unique($arrayStorageInfoId))<count($arrayStorageInfoId)) {
                            return false;
                        }
                        return true;
                    }, 'Duplidate storageInfoId');
                    $validator = Validator::make($input, [
                        'order_id'  => 'required|checkDuplicateId|unique:dt_store_order_info,order_id'
                    ]);
                    $mess = '';
                    if ($validator->fails()) {
                        $messageData = $validator->messages()->toArray();
                        foreach ($messageData as $key1 => $value1) {
                            $mess .=  "$key1:".$input['order_id'] . ' > ' . $value1[0] . "\r\n";
                        }
                        $arrError[$key] = $mess;
                        continue;
                    }

                    $requestParams['header'] = array(
                        "Content-Type: application/x-www-form-urlencoded",
                        "X_contract_id: $this->contractId",
                        "X_access_token: $this->accessToken",
                    );
                    $requestParams['fields'] = http_build_query(array(
                        'proc_name' => 'storage_info_ref',
                        'params' => '{
                            "fields":["storageInfoId","storageStoreId"],
                            "conditions":[{"storageInfoId":"'.$value->storageInfoId.'"}],
                            "table_name":"StorageInfoDelivery"
                        }'
                    ));
                    // Get storageStoreId
                    $responseDetailStoreId = $this->curlGet($requestParams);
                    if (!empty($responseDetailStoreId) && isset($responseDetailStoreId->result)) {
                        if (count($responseDetailStoreId->result) > 0) {
                            foreach ($responseDetailStoreId->result as $key2 => $value2) {
                                $input['storage_store_id'] = $value2->storageStoreId;
                            }
                        }
                    }
                    $arrayInsert[] = $input;
                }

                if (count($arrayInsert) > 0) {
                    $flgInsert = true;
                    try {
                        $model->insert($arrayInsert);
                        Log::info("Total insert :" . count($arrayInsert));
                        print_r("Total insert :" . count($arrayInsert). PHP_EOL);
                    } catch (\Exception $e) {
                        report($e);
                        $flgInsert = false;
                    }
                    if ($flgInsert) {
                        Log::info('Insert success');
                        print_r('Insert success' . PHP_EOL);
                    } else {
                        Log::info('Insert fail');
                        print_r('Insert fail' . PHP_EOL);
                    }
                } else {
                    Log::info('No data insert');
                    print_r('No data insert' . PHP_EOL);
                }
            }
        }
        if (count($arrError) > 0) {
            $err = implode("\n", $arrError);
            Log::info($err);
        }
    }

    /**
     * Get order's detail info
     *
     * @return  none
     */
    private function processContent2()
    {
        $modelOrderInfo  = new DtOrderStoreOrderInfo();
        $data   = $modelOrderInfo->getDataOrdersDetail();
        $orderDetailFields   = [
            'order_id',
            'product_id',
            'store_product_code',
            'product_name',
            'size',
            'color',
            'group_code',
            'product_code',
            'price',
            'quantity',
            'up_date'
        ];
        $data2 = [];
        $data1 = [];
        $arrayInsert = [];
        $modelDetail = new DtOrderStoreOrderDetail();
        if (count($data) > 0) {
            foreach ($data as $key => $value) {
                $requestParams['header'] = array(
                    "Content-Type: application/x-www-form-urlencoded",
                    "X_contract_id: $this->contractId",
                    "X_access_token: $this->accessToken",
                );
                // Case 1
                $requestParams['fields'] = http_build_query(array(
                    'proc_name' => 'storage_info_ref',
                    'params' => '{
                        "fields":["storageInfoId","productId","productCode",
                        "productName","size","color","groupCode","supplierProductNo","cost","quantity","modified"],
                        "conditions":[{"storageInfoId":"'.$value['order_id'].'"}],
                        "table_name":"StorageInfoProduct"
                    }'
                ));
                $response1 = $this->curlGet($requestParams);
                if (!empty($response1) && isset($response1->result)) {
                    if (count($response1->result) > 0) {
                        foreach ($response1->result as $key1 => $value1) {
                            $data1 = (array)$value1;
                            $dataCombine = array_combine($orderDetailFields, $data1);
                            $dataCombine['received_quantity'] = 0;
                            $dataCombine['shortage_quantity'] = 0;
                            $dataCombine['process_status'] = 0;
                            $arrayInsert[] = $dataCombine;
                        }
                    }
                }
                if (count($arrayInsert) > 0) {
                    $flgInsert = true;
                    try {
                        // Insert into detail
                        $modelDetail->insert($arrayInsert);

                        // Update process status
                        $dataWhere = [
                            'order_id' => $value['order_id']
                        ];
                        $dataUpdate = [
                            'process_status' => 1
                        ];
                        $modelOrderInfo->updateData($dataWhere, $dataUpdate);
                        Log::info("Total insert :" . count($arrayInsert));
                        print_r("Total insert :" . count($arrayInsert). PHP_EOL);
                    } catch (\Exception $e) {
                        report($e);
                        $flgInsert = false;
                    }
                    if ($flgInsert) {
                        Log::info('Insert order detail success');
                        print_r('Insert order detail success' . PHP_EOL);
                    } else {
                        Log::info('Insert order detail fail');
                        print_r('Insert order detail fail' . PHP_EOL);
                    }
                } else {
                    // Update process status & error
                    $dataWhere = [
                        'order_id' => $value['order_id']
                    ];
                    $dataUpdate = [
                        'process_status'    => 1,
                        'error_code'        => "E01",
                        'error_message'     => "詳細データがなそうです。",
                    ];
                    $modelOrderInfo->updateData($dataWhere, $dataUpdate);
                    Log::info('No data insert');
                    print_r('No data insert' . PHP_EOL);
                }
                $arrayInsert = [];
            }
        }
    }

    /**
     * Update process status
     *
     * @return  none
     */
    private function processContent3()
    {
        $modelDetail    = new DtOrderStoreOrderDetail();
        $modelOrderInfo = new DtOrderStoreOrderInfo();
        $data           = $modelDetail->getDataOrderDetailForUpdate();
        $dataWhereInfo  = array();
        if (count($data) !== 0) {
            foreach ($data as $key => $value) {
                // Update process status & error
                $dataWhereInfo[] = $value['order_id'];
                $dataWhere = [
                    'order_id' => $value['order_id'],
                    'product_id' => $value['product_id'],
                ];
                $dataUpdate = [
                    'process_status'    => 9,
                    'error_code'        => "E02",
                    'error_message'     => "大都の販売商品ではないです。",
                ];
                $modelDetail->updateData($dataWhere, $dataUpdate);
            }
        }
        if (count($dataWhereInfo) > 0) {
            $dataWhereInfo = array_unique($dataWhereInfo);
            $dataUpdateInfo = [
                'status'            => 6,
                'error_code'        => "E02",
                'error_message'     => "大都の販売商品ではないです。",
            ];
            foreach ($dataWhereInfo as $id) {
                $where = ['order_id' => $id];
                $modelOrderInfo->updateData($where, $dataUpdateInfo);
            }
        }
    }
    /**
     * Update process status
     *
     * @return  none
     */
    private function processContent4()
    {
        $modelDetail    = new DtOrderStoreOrderDetail();
        $modelOrderInfo = new DtOrderStoreOrderInfo();
        $data           = $modelDetail->getDataOrderDetailForUpdate('E03');
        $dataWhereInfo  = array();
        if (count($data) > 0) {
            foreach ($data as $key => $value) {
                // Update process status & error
                $dataWhereInfo = [
                    'order_id' => $value['order_id']
                ];
                $dataWhere = [
                    'order_id' => $value['order_id'],
                    'product_id' => $value['product_id'],
                ];

                $dataUpdate = [
                    'process_status'    => 9,
                    'error_code'        => "E03",
                    'error_message'     => "DFの発注対象商品ではないです。",
                ];
                $modelDetail->updateData($dataWhere, $dataUpdate);
            }
        }
        if (count($dataWhereInfo) > 0) {
            $dataWhereInfo = array_unique($dataWhereInfo);
            $dataUpdateInfo = [
                'status'            => 6,
                'error_code'        => "E03",
                'error_message'     => "DFの発注対象商品ではないです。",
            ];
            foreach ($dataWhereInfo as $id) {
                $where = ['order_id' => $id];
                $modelOrderInfo->updateData($where, $dataUpdateInfo);
            }
        }
    }
    /**
     * Get data then insert to new destination
     *
     * @return  none
     */
    private function processContent5()
    {
        $modelDetail        = new DtOrderStoreOrderDetail();
        $modelOrder         = new MstOrder();
        $modelOrderDetail   = new MstOrderDetail();
        $data = $modelDetail->getDataOrderDetailCloneData();
        $arrOrderDetail     = [];
        $update             = 0;
        if (count($data) > 0) {
            $receiveIdArray = [];
            $receiveId = 0;
            foreach ($data as $key => $value) {
                // Update process status & error
                $orderDate = !empty($value->order_date) ? date('Y-m-d H:i:s', strtotime($value->order_date)) : now();
                $arrDataOrder = [];
                $arrDataOrder['received_order_id']     = (string)$value->store_id;
                $arrDataOrder['mall_id']               = 6;
                $arrDataOrder['customer_id']           = $value->customer_id ? $value->customer_id : 0;
                $arrDataOrder['used_coupon']           = 0;
                $arrDataOrder['used_point']            = 0;
                $arrDataOrder['total_price']           = 0;
                $arrDataOrder['request_price']         = 0;
                $arrDataOrder['order_date']            = $orderDate;
                $arrDataOrder['ship_charge']           = 0;
                $arrDataOrder['pay_charge']            = 0;
                $arrDataOrder['goods_price']           = 0;
                $arrDataOrder['goods_tax']             = 0;
                $arrDataOrder['discount']              = 0;
                $arrDataOrder['arrive_type']           = 0;
                $arrDataOrder['is_multi_ship_address'] = 0;
                $arrDataOrder['ship_wish_time']        = '';
                $arrDataOrder['ship_wish_date']        = '';
                $arrDataOrder['payment_status']        = 1;
                $arrDataOrder['payment_method']        = 11;
                $arrDataOrder['mail_seri']             = md5($value->order_id);
                $arrDataOrder['order_status']          = 0;
                $arrDataOrder['payment_account']       = '';
                $arrDataOrder['customer_question']     = '';
                $arrDataOrder['shop_answer']           = '';
                $arrDataOrder['company_name']          = '';
                $arrDataOrder['cancel_reason']         = '';
                $arrDataOrder['pay_after_charge']      = '';
                $arrDataOrder['pay_charge_discount']   = 0;
                $arrDataOrder['payment_date']          = null;
                $arrDataOrder['payment_confirm_date']  = null;
                $arrDataOrder['in_date']               = now();
                $arrDataOrder['in_ope_cd']             = 'OPE99999';
                $arrDataOrder['up_date']               = now();
                $arrDataOrder['up_ope_cd']             = 'OPE99999';
                if (!in_array((string) $value->store_id, $receiveIdArray)) {
                    $receiveId = $modelOrder->insertGetId($arrDataOrder);
                    $update++;
                }
                $receiveIdArray[] = (string)$value->store_id;
                $arrOrderDetail[$value->order_id][$key]['receive_id']         = (int)$receiveId;
                $arrOrderDetail[$value->order_id][$key]['product_code']       = (string)$value->product_code;
                $arrOrderDetail[$value->order_id][$key]['product_name']       = (string)$value->product_name;
                $arrOrderDetail[$value->order_id][$key]['price']              = 0;
                $arrOrderDetail[$value->order_id][$key]['item_tax']           = 0;
                $arrOrderDetail[$value->order_id][$key]['ship_price']         = 0;
                $arrOrderDetail[$value->order_id][$key]['ship_tax']           = 0;
                $arrOrderDetail[$value->order_id][$key]['quantity']           = (int)$value->quantity;
                $arrOrderDetail[$value->order_id][$key]['receiver_id']        =
                        $value->customer_id ? $value->customer_id : 0;
                $arrOrderDetail[$value->order_id][$key]['ship_date'] = null;
                $arrOrderDetail[$value->order_id][$key]['delivery_person_id']  = '';
                $arrOrderDetail[$value->order_id][$key]['package_ship_number'] = '';
                $arrOrderDetail[$value->order_id][$key]['in_ope_cd']           = 'OPE99999';
                $arrOrderDetail[$value->order_id][$key]['in_date']             = now();
                $arrOrderDetail[$value->order_id][$key]['up_ope_cd']           = 'OPE99999';
                $arrOrderDetail[$value->order_id][$key]['up_date']             = now();
                // Update process status & error
                $dataWhere = [
                    'order_id'      => $value->order_id,
                    'product_id'    => $value->product_id,
                ];
                $dataUpdate = [
                    'process_status'    => 1,
                    'hrnb_order_id'     => (string)$value->store_id
                ];
                $modelDetail->updateData($dataWhere, $dataUpdate);
            }
            Log::info("Total records update horunba_id : $update .");
            print_r("Total records update horunba_id : $update ." . PHP_EOL);
            foreach ($arrOrderDetail as $key => $value) {
                $index = 0;
                foreach ($value as $key1 => $value1) {
                    $index++;
                    $value1['detail_line_num']    = $index;
                    $modelOrderDetail->insert($value1);
                }
            }
            if ($index > 0) {
                Log::info("Move total $index items.");
                print_r("Move total $index items." . PHP_EOL);
            } else {
                Log::info('No data move.');
                print_r('No data move.' . PHP_EOL);
            }
        }
    }
    /**
     * Request process api
     *
     * @param  array $paramOptions
     * @return json encode
     */
    private function curlGet($paramOptions)
    {
        $slack   = new Notification(CHANNEL['horunba']);
        $smaregiUrl     = "https://webapi.smaregi.jp/access/";
        $ch = curl_init($smaregiUrl);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $paramOptions['header']);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $paramOptions['fields']);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $response = curl_exec($ch);
        curl_close($ch);
        $response = json_decode($response);
        if (isset($response->error_code)) {
            $arrError = (array) $response;
            $err = implode("\n", $arrError);
            $error  = "------------------------------------------" . PHP_EOL;
            $error .= basename(__CLASS__) . PHP_EOL;
            $error .= $err;
            $error .= PHP_EOL . "------------------------------------------" . PHP_EOL;
            $slack->notify(new SlackNotification($error));
            Log::info($err);
        }
        return $response;
    }
}
