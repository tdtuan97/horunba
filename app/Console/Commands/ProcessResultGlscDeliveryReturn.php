<?php
/**
 * Batch process result glsc delivery return
 *
 * @package    App\Console\Commands
 * @subpackage ProcessResultGlscDeliveryReturn
 * @copyright  Copyright (c) 2018 RiverCrane! Corporation. All Rights Reserved.
 * @author     Truong Nghia<truong.van.nghia@rivercrane.vn>
 */
namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;
use App\Notifications\SlackNotification;
use App\Models\Batches\DtReturn;
use App\Models\Batches\TShippingPerformanceModel ;
use App\Models\Batches\MstStockStatus ;
use App\Models\Batches\DtReceiptLedger ;
use App\Notification;
use App\Events\Command as eCommand;
use App\Custom\Utilities;
use Event;
use DB;

class ProcessResultGlscDeliveryReturn extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'process:result-glsc-delivery-return';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = "Get return delivery' result(Direct delivery) from Glsc.";

    /**
     * The notification.
     *
     * @var object
     */
    protected $slack;

    /**
     * The error.
     *
     * @var array
     */
    public $error = [];

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $arrayReplace = [':', '-'];
        $folder       = str_replace($arrayReplace, '_', $this->signature);
        Log::useDailyFiles(storage_path() . "/logs/$folder/history.log");
        Event::fire(new eCommand($this->signature, array('start' => true)));
        $slack = new Notification(CHANNEL['horunba']);
        $start = microtime(true);
        Log::info('Start batch process result GLSC delivery return.');
        print_r("Start batch process result GLSC delivery return." . PHP_EOL);
        $modelDR    = new DtReturn();
        $modelDRL   = new DtReceiptLedger();
        $modelTS    = new TShippingPerformanceModel();
        $dataError  = $modelDR->getDataErrDeliveryResult();
        $modelStock = new MstStockStatus();
        Log::info('=== Start process error from result GLSC delivery return.===');
        print_r("=== Start process error from result GLSC delivery return.===" . PHP_EOL);
        if (count($dataError) === 0) {
            Log::info('No data');
            print_r("No data. " . PHP_EOL);
        } else {
            $succErr = 0;
            $failErr = 0;
            foreach ($dataError as $value) {
                $arrUpdate = [];
                $arrWhere  = [];
                $arrUpdate['error_code']           = $value->er_ErrorCd;
                $arrUpdate['error_message']        = $value->er_Message;
                $arrUpdate['delivery_instruction'] = 9;
                $arrWhere['return_no']             = $value->return_no;
                $arrWhere['return_line_no']        = $value->return_line_no;
                try {
                    $modelDR->where($arrWhere)->update($arrUpdate);
                    $succErr++;
                } catch (\Exception $e) {
                    $this->error[] = Utilities::checkMessageException($e);
                    $error  = "------------------------------------------" . PHP_EOL;
                    $error .= basename(__CLASS__) . PHP_EOL;
                    $error .= Utilities::checkMessageException($e);
                    $error .= PHP_EOL . "------------------------------------------" . PHP_EOL;
                    $slack->notify(new SlackNotification($error));
                    Log::error(Utilities::checkMessageException($e));
                    print_r("$error");
                    $failErr++;
                }
            }
            $message  = "Update table dt_return success: $succErr records";
            $message .= " and fail : $failErr records";
            Log::info($message);
            print_r($message . PHP_EOL);
        }
        Log::info('=== End process error from result GLSC delivery return.===');
        print_r("=== End process error from result GLSC delivery return.===" . PHP_EOL);
        Log::info('=== Start process update data from result GLSC delivery return.===');
        print_r("=== Start process update data from result GLSC delivery return.===" . PHP_EOL);
        $arrDataKb = [1, 9];
        $count     = 0;
        $succUp    = 0;
        $failUp    = 0;
        foreach ($arrDataKb as $value) {
            $datas = $modelDR->getDataUpdateFollowKb($value);
            if (count($datas) === 0) {
                $count++;
            } else {
                foreach ($datas as $data) {
                    try {
                        $arrUpdate = [];
                        if ($data->Quantity === $data->PlanQuantity) {
                            $arrUpdate['delivery_instruction'] = 2;
                            $arrUpdate['delivery_real_date']   = $data->DeliDate;
                            $arrUpdate['delivery_real_num']    = $data->Quantity;
                            $modelDRL->add2ReceiptLedgerOutStock([
                                'pa_order_code'        => $data->OrderNo,
                                'pa_product_code'      => $data->ItemCd,
                                'pa_price_invoice'     => $data->price_invoice,
                                'pa_delivery_num'      => $data->Quantity,
                                'pa_supplier_id'       => $data->price_supplier_id,
                                'pa_stock_type'        => 3,
                                'pa_stock_detail_type' => $data->is_mojax === 0 ? 3 : 6,
                            ]);
                            $modelStock->updateStockOutStockR(
                                $data->ItemCd,
                                $data->Quantity,
                                $data->receive_instruction >= 0 ? true : false
                            );
                        } else {
                            if ($data->DataKb === 9) {
                                $arrUpdate['delivery_instruction'] = 8;
                            } else {
                                $arrUpdate['delivery_instruction'] = 9;
                            }
                        }
                        $arrWhere = [
                            'return_no'      => $data->return_no,
                            'return_line_no' => $data->return_line_no,
                            'return_time'    => $data->return_time,
                        ];
                        $modelDR->updateData($arrWhere, $arrUpdate);
                        $modelTS->updateData(['tShippingPerformanceModel_autono' => $data->id], ['NodisplayFlg' => 2]);
                        $succUp++;
                    } catch (\Exception $e) {
                        $this->error[] = Utilities::checkMessageException($e);
                        $error  = "------------------------------------------" . PHP_EOL;
                        $error .= basename(__CLASS__) . PHP_EOL;
                        $error .= Utilities::checkMessageException($e);
                        $error .= PHP_EOL . "------------------------------------------" . PHP_EOL;
                        $slack->notify(new SlackNotification($error));
                        Log::error(Utilities::checkMessageException($e));
                        print_r("$error");
                        $failUp++;
                    }
                }
            }
        }
        if ($count === 4) {
            Log::info('No data');
            print_r("No data. " . PHP_EOL);
        } else {
            $message  = "Update table dt_return success: $succUp records";
            $message .= " and fail : $failUp records";
            Log::info($message);
            print_r($message . PHP_EOL);
        }
        Log::info('=== End process update data from result GLSC delivery return.===');
        print_r("=== End process update data from result GLSC delivery return.===" . PHP_EOL);
        Log::info('=== Start process insert data from result GLSC delivery return.===');
        print_r("=== Start process insert data from result GLSC delivery return.===" . PHP_EOL);
        $dataIn = $modelTS->getDataInsertDeliveryResult();
        $succIn = 0;
        $failIn = 0;
        if (count($dataIn) === 0) {
            Log::info('No data');
            print_r("No data. " . PHP_EOL);
        } else {
            foreach ($dataIn as $value) {
                $arrInsert = [];
                $arrInsert['return_no']            = $value->OrderNo;
                $arrInsert['return_line_no']       = $value->OrderLNo;
                $arrInsert['return_date']          = $value->DeliDate;
                $arrInsert['deje_no']              = 0;
                $arrInsert['return_type_large_id'] = 5;
                $arrInsert['return_type_mid_id']   = 1;
                $arrInsert['status']               = 1;
                $arrInsert['receive_instruction']  = -1;
                $arrInsert['receive_result']       = -1;
                $arrInsert['delivery_instruction'] = 2;
                $arrInsert['delivery_result']      = -1;
                $arrInsert['red_voucher']          = -1;
                $arrInsert['product_code']         = $value->ItemCd;
                $arrInsert['return_quantity']      = $value->Quantity;
                $arrInsert['return_tanka']         = 0;
                $arrInsert['supplier_id']          = $value->price_supplier_id;
                $arrInsert['receive_plan_date']    = '';
                $arrInsert['receive_real_date']    = '';
                $arrInsert['receive_real_num']     = 0;
                $arrInsert['delivery_plan_date']   = '';
                $arrInsert['delivery_real_date']   = now();
                $arrInsert['delivery_real_num']    = $value->Quantity;
                $arrInsert['receive_id']           = '';
                $arrInsert['payment_on_delivery']  = '';
                $arrInsert['note']                 = $value->Note;
                $arrInsert['receive_count']        = 0;
                $arrInsert['delivery_count']       = 0;
                $arrInsert['error_code']           = '';
                $arrInsert['error_message']        = '';
                $arrInsert['in_ope_cd']            = 'OPE99999';
                $arrInsert['in_date']              = now();
                $arrInsert['up_ope_cd']            = 'OPE99999';
                $arrInsert['up_date']              = now();
                try {
                    $modelDR->insert($arrInsert);
                    $modelTS->updateData(['tShippingPerformanceModel_autono' => $value->id], ['NodisplayFlg' => 2]);
                    $modelDRL->add2ReceiptLedgerOutStock([
                            'pa_order_code'        => $value->OrderNo,
                            'pa_product_code'      => $value->ItemCd,
                            'pa_price_invoice'     => $value->price_invoice,
                            'pa_delivery_num'      => $value->Quantity,
                            'pa_supplier_id'       => $value->price_supplier_id,
                            'pa_stock_type'        => 3,
                            'pa_stock_detail_type' => 5,
                        ]);
                    $modelStock->updateStockOutStockR(
                        $value->ItemCd,
                        $value->Quantity,
                        $arrInsert['receive_instruction'] >= 0 ? true : false
                    );
                    $succIn++;
                } catch (\Exception $e) {
                    $this->error[] = Utilities::checkMessageException($e);
                    $error  = "------------------------------------------" . PHP_EOL;
                    $error .= basename(__CLASS__) . PHP_EOL;
                    $error .= Utilities::checkMessageException($e);
                    $error .= PHP_EOL . "------------------------------------------" . PHP_EOL;
                    $slack->notify(new SlackNotification($error));
                    Log::error(Utilities::checkMessageException($e));
                    print_r("$error");
                    $failIn++;
                }
            }
            $message  = "Insert table dt_return success: $succIn records";
            $message .= " and fail : $failIn records";
            Log::info($message);
            print_r($message . PHP_EOL);
        }
        Log::info('=== End process insert data from result GLSC delivery return.===');
        print_r("=== End process insert data from result GLSC delivery return.===" . PHP_EOL);
        Event::fire(new eCommand($this->signature, array('end' => true, 'error' => $this->error)));
        $totalTime = round(microtime(true) - $start, 2);
        Log::info("End batch process result GLSC with total time: $totalTime.");
        print_r("End batch process result GLSC with total time: $totalTime s." . PHP_EOL);
    }
}
