<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use App\Models\Backend\DtUriMapping;
use App\Models\Backend\DtUriRakCoupon;
use App\Models\Backend\DtUriRakPay;
use App\Models\Backend\DtUriYahoo;
use App\Models\Backend\DtUriPaygent;
use App\Models\Backend\DtUriMfk;
use App\Models\Backend\DtUriSagawa;
use App\Models\Backend\DtUriJppost;
use App\Models\Backend\DtUriSeinou;
use App\Models\Backend\DtUriAmazon;
use App\Models\Backend\DtPaymentList;

class ProcessDataCsvUriMapping implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private $action;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($action)
    {
        $this->action = $action;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $modelUM = new DtUriMapping();
        if ($this->action === 'rakuten_pay_csv') {
            $modelRP    = new DtUriRakPay();
            $pointDatas = $modelUM->getDataProcessRakPayPoint();
            if ($pointDatas->count()) {
                $arrIndexs = [];
                foreach ($pointDatas as $value) {
                    $arrIndexs[] = $value->id;
                    $arrUpdate   = [
                        'paid_point'      => $value->paid_point + $value->total_paid_price,
                        'point_paid_date' => $value->paid_date,
                        'is_updated'      => 1,
                    ];
                    $modelUM->updateData(
                        ['received_order_id' => $value->received_order_id],
                        $arrUpdate
                    );
                    $modelRP->where('receive_order_num', $value->received_order_id)
                            ->where('paid_type', '楽天スーパーポイント')
                            ->update(['process_flg' => 1]);
                }
            }
            $rePriceDatas = $modelUM->getDataProcessRakPayRequestPrice();
            if ($rePriceDatas->count()) {
                $arrIndexs = [];
                foreach ($rePriceDatas as $value) {
                    $arrIndexs[] = $value->id;
                    $arrUpdate   = [
                        'paid_request_price' => $value->paid_request_price + $value->total_paid_price,
                        'payment_date'       => $value->paid_date,
                        'payment_name'       => '楽天ペイ',
                        'is_updated'         => 1,
                    ];
                    $modelUM->updateData(
                        ['received_order_id' => $value->received_order_id],
                        $arrUpdate
                    );
                    $modelRP->where('receive_order_num', $value->received_order_id)
                            ->where('paid_type', '<>', '楽天スーパーポイント')
                            ->whereNotIn('summary', ['印紙税相当額（郵便局）', '印紙税'])
                            ->update(['process_flg' => 1]);
                }
            }
        } elseif ($this->action === 'yahoo_pay_csv') {
            $modelUY     = new DtUriYahoo();
            $updateDatas = $modelUM->getDataProcessYahCoupon1();
            if ($updateDatas->count()) {
                $modelUY   = new DtUriYahoo();
                $arrIndexs = [];
                foreach ($updateDatas as $value) {
                    $arrIndexs[] = $value->id;
                    $arrUpdate   = [
                        'paid_point'      => $value->tax_price,
                        'point_paid_date' => $value->paid_date,
                        'is_updated'      => 1,
                    ];
                    $modelUM->updateData(
                        ['received_order_id' => $value->received_order_id],
                        $arrUpdate
                    );
                }
                $modelUY->whereIn('id', $arrIndexs)->update(['process_flg' => 1]);
            }
            $updateDatas2 = $modelUM->getDataProcessYahCoupon2();
            if ($updateDatas2->count()) {
                $arrIndexs = [];
                foreach ($updateDatas2 as $value) {
                    $arrIndexs[] = $value->id;
                    $arrUpdate   = [
                        'paid_coupon'      => $value->tax_price,
                        'coupon_paid_date' => $value->paid_date,
                        'is_updated'       => 1,
                    ];
                    $modelUM->updateData(
                        ['received_order_id' => $value->received_order_id],
                        $arrUpdate
                    );
                }
                $modelUY->whereIn('id', $arrIndexs)->update(['process_flg' => 1]);
            }
            $updateDatas3 = $modelUM->getDataProcessYahCoupon3();
            if ($updateDatas3->count()) {
                $arrIndexs = [];
                foreach ($updateDatas3 as $value) {
                    $arrIndexs[] = $value->id;
                    $arrUpdate   = [
                        'paid_request_price' => $value->paid_request_price,
                        'payment_name'       => 'ヤフー',
                        'payment_date'       => $value->payment_date,
                        'is_updated'         => 1,
                    ];
                    $modelUM->updateData(
                        ['received_order_id' => $value->received_order_id],
                        $arrUpdate
                    );
                    $modelUY->where('receive_order_num', $value->received_order_id)
                            ->whereNotIn('used_item', ['モールクーポン利用料', 'ポイント利用料'])
                            ->where('used_item', 'NOT LIKE', '%キャンセル%')
                            ->update(['process_flg' => 1]);
                }
            }
            $updateDatas4 = $modelUM->getDataProcessYahCoupon4();
            if ($updateDatas4->count()) {
                $modelPL = new DtPaymentList();
                foreach ($updateDatas4 as $value) {
                    $arrUpdate   = [
                        'paid_request_price' => $value->payment_price,
                        'payment_name'       => '三井住友',
                        'payment_date'       => $value->payment_date,
                        'is_updated'         => 1,
                    ];
                    $modelUM->updateData(
                        ['received_order_id' => $value->received_order_id],
                        $arrUpdate
                    );
                    $modelPL->where('index', $value->index)
                            ->update(['payment_code' => $value->payment_code * 2]);
                }
            }
        } elseif ($this->action === 'rakuten_coupon_csv') {
            $updateDatas = $modelUM->getDataProcessRakCouponUpdate();
            if ($updateDatas->count()) {
                $modelRC   = new DtUriRakCoupon();
                $arrIndexs = [];
                foreach ($updateDatas as $value) {
                    $arrIndexs[] = $value->id;
                    $arrUpdate   = [
                        'paid_coupon'      => $value->paid_coupon + $value->coupon_paid_price,
                        'coupon_paid_date' => $value->paid_date,
                        'is_updated'       => 1,
                    ];
                    $modelUM->updateData(
                        ['received_order_id' => $value->received_order_id],
                        $arrUpdate
                    );
                }
                $modelRC->whereIn('id', $arrIndexs)->update(['process_flg' => 1]);
            }
        } elseif ($this->action === 'pro_paygent_csv') {
            $updateDatas = $modelUM->getDataProcessProPaygentCalculate();
            if ($updateDatas->count()) {
                $modelUP   = new DtUriPaygent();
                $arrIndexs = [];
                foreach ($updateDatas as $value) {
                    $arrIndexs[] = $value->id;
                    $arrUpdate   = [
                        'paid_request_price' => $value->total_paid_price,
                        'payment_date'       => $value->paid_date,
                        'payment_name'       => 'ペイジェント',
                        'is_updated'         => 1,
                    ];
                    $modelUM->updateData(
                        ['received_order_id' => $value->received_order_id],
                        $arrUpdate
                    );
                }
                $modelUP->whereIn('id', $arrIndexs)
                        ->update(['process_flg' => 1]);
            }
        } elseif ($this->action === 'pro_mfk_csv') {
            $updateDatas = $modelUM->getDataProcessMfkCalculate();
            if ($updateDatas->count()) {
                $modelUMFK = new DtUriMfk();
                $arrIndexs = [];
                foreach ($updateDatas as $value) {
                    $arrIndexs[] = $value->order_id;
                    $arrUpdate   = [
                        'paid_request_price' => $value->total_paid_price,
                        'payment_date'       => $value->paid_date,
                        'payment_name'       => 'MFK',
                        'is_updated'         => 1,
                    ];
                    $modelUM->updateData(
                        ['received_order_id' => $value->received_order_id],
                        $arrUpdate
                    );
                }
                $modelUMFK->whereIn('order_id', $arrIndexs)->update(['process_flg' => 1]);
            }
        } elseif ($this->action === 'amazon_pay_csv') {
            $updateDatas = $modelUM->getDataProcessAmazonCalculate();
            if ($updateDatas->count()) {
                $modelUA   = new DtUriAmazon();
                $arrIndexs = [];
                foreach ($updateDatas as $value) {
                    $arrIndexs[] = $value->id;
                    $arrUpdate   = [
                        'paid_point'         => 0,
                        'payment_name'       => 'アマゾン',
                        'point_paid_date'    => null,
                        'paid_coupon'        => 0,
                        'coupon_paid_date'   => null,
                        'paid_request_price' => 0,
                        'payment_date'       => $value->max_deposit_date,
                        'is_updated'         => 1,
                    ];
                    if ($value->sum_amount > 0) {
                        if ($value->used_point > 0) {
                            $arrUpdate['paid_point']      = $value->used_point;
                            $arrUpdate['point_paid_date'] = $value->max_deposit_date;
                        }
                        if ($value->used_coupon > 0) {
                            $arrUpdate['paid_coupon']      = $value->sum_amount - $value->used_point;
                            $arrUpdate['coupon_paid_date'] = $value->max_deposit_date;
                        }
                    }
                    if ((int)$value->payment_method !== 3) {
                        $arrUpdate['paid_request_price'] = $value->paid_request_price + $value->sum_amount;
                    }
                    $modelUM->updateData(
                        ['received_order_id' => $value->received_order_id],
                        $arrUpdate
                    );
                    $modelUA->where('order-id', $value->received_order_id)
                            ->whereIn('dt_uri_amazon.amount-description', ['Tax', 'Principal', 'COD', 'COD Tax', 'Shipping', 'ShippingTax', 'CollectOnDeliveryRevenue'])
                            ->update(['process_flg' => 1]);
                }
            }
        } elseif ($this->action === 'sagawa_pay_csv') {
            $updateDatas = $modelUM->getDataProcessSagawaCalculate();
            if ($updateDatas->count()) {
                $modelUS   = new DtUriSagawa();
                $arrIndexs = [];
                foreach ($updateDatas as $value) {
                    $arrIndexs[] = $value->inquiry_no;
                    $arrUpdate   = [
                        'payment_date'       => $value->payment_date,
                        'paid_request_price' => $value->paid_price,
                        'payment_name'       => '佐川',
                        'is_updated'         => 1,
                    ];
                    $modelUM->updateData(
                        ['received_order_id' => $value->received_order_id],
                        $arrUpdate
                    );
                }
                $modelUS->whereIn('inquiry_no', $arrIndexs)->update(['process_flg' => 1]);
            }
        } elseif ($this->action === 'jp_post_csv') {
            $updateDatas = $modelUM->getDataProcessJpPostCalculate();
            if ($updateDatas->count()) {
                $modelUJP   = new DtUriJppost();
                $arrIndexs = [];
                foreach ($updateDatas as $value) {
                    $arrIndexs[] = $value->id;
                    $arrUpdate   = [
                        'paid_request_price' => $value->settlement_price,
                        'payment_date'       => $value->deposit_date,
                        'payment_name'       => '日本郵便',
                        'is_updated'         => 1,
                    ];
                    $modelUM->updateData(
                        ['received_order_id' => $value->received_order_id],
                        $arrUpdate
                    );
                }
                $modelUJP->whereIn('id', $arrIndexs)->update(['process_flg' => 1]);
            }
        } elseif ($this->action === 'seinou_csv') {
            $updateDatas = $modelUM->getDataProcessSeinouCalculate();
            if ($updateDatas->count()) {
                $modelUS   = new DtUriSeinou();
                $arrIndexs = [];
                foreach ($updateDatas as $value) {
                    $arrIndexs[] = $value->id;
                    $arrUpdate   = [
                        'paid_request_price' => $value->cod_price,
                        'payment_date'       => $value->deposit_date,
                        'payment_name'       => '西濃',
                        'is_updated'         => 1,
                    ];
                    $modelUM->updateData(
                        ['received_order_id' => $value->received_order_id],
                        $arrUpdate
                    );
                }
                $modelUS->whereIn('id', $arrIndexs)->update(['process_flg' => 1]);
            }
        }
        $checkPaidDatas = $modelUM->where(['is_updated' => 1])->get();
        if ($checkPaidDatas->count()) {
            foreach ($checkPaidDatas as $value) {
                $tempCal   = $value->paid_request_price + $value->paid_point + $value->paid_coupon;
                $arrUpdate = [
                    'total_paid_price' => $tempCal,
                    'diff_price'       => $value->total_payment_plan - $tempCal,
                    'is_updated'       => 0,
                ];
                if ($value->total_payment_plan - $tempCal === 0) {
                    $arrUpdate['remarks'] = '入金済';
                } elseif (($value->total_payment_plan - $tempCal) !== 0 &&
                    $value->paid_request_price === 0 &&
                    $value->paid_point === 0 &&
                    $value->paid_coupon === 0) {
                    $arrUpdate['remarks'] = '未入金';
                } elseif ($value->total_payment_plan - $tempCal > 0) {
                    $arrUpdate['remarks'] = '入金不足';
                } else {
                    $arrUpdate['remarks'] = '返品で未返金';
                }
                $arrUpdate['is_corrected'] = $tempCal === 0 ? 1 : 0;
                $arrUpdate['process_date'] = $tempCal === 0 ? now() : null;
                $modelUM->updateData(
                    ['received_order_id' => $value->received_order_id],
                    $arrUpdate
                );
            }
        }
    }
}
