<?php
/**
 * Custom CurlPost class.
 *
 * @package App
 * @subpackage Custom
 * @copyright Copyright (c) 2019 RiverCrane Corporation. All Rights Reserved.
 * @author lam.vinh <lam.vinh.rcvn2012@gmail.com>
 */
namespace App\Custom;

use Illuminate\Support\Facades\Log;

class CurlPost
{

    /**
     * push messge to google chat
     *
     * @param  string $hook url webhook from google chat
     * @param  string $content
     * @author lam.vinh 2019-05-13
     * @return string
     */
    public static function pushChatGoogle($webhook = null, $content = null)
    {
        if (empty($webhook)) {
            $webhook = config('google.chat.webhook');
        }
        $data = json_encode([
                'text' => $content
                ], JSON_UNESCAPED_UNICODE);
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $webhook);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, 'POST');
        curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
        curl_setopt($curl, CURLOPT_HTTPHEADER, [
            'Content-Type: application/json',
        ]);
        $response = curl_exec($curl);
        $err      = curl_error($curl);
        curl_close($curl);
        if ($err) {
            Log::error("cURL Error #:" . $err);
        } else {
            Log::info($response);
        }
        return $response;
    }
}
