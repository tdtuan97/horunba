<?php
/**
 * Custom guard class that sets a secure log-in cookie.
 *
 * @package App
 * @subpackage Custom
 * @copyright Copyright (c) 2017 RiverCrane Corporation. All Rights Reserved.
 * @author van.qui <van.qui.rcvn2012@gmail.com>
 */
namespace App\Custom;

class SecureGuard extends \Illuminate\Auth\SessionGuard
{
    /**
     * Override createRecaller with 1 month cookies
     *
     * @param  string  $value
     * @return \Symfony\Component\HttpFoundation\Cookie
     */
    protected function createRecaller($value)
    {
        return $this->getCookieJar()->make($this->getRecallerName(), $value, 60*24*30);
    }

    /**
     * @Override hasValidCredentials of SessionGuard class
     * Determine if the user matches the credentials and actived.
     *
     * @param  mixed  $user
     * @param  array  $credentials
     * @return bool
     */
    protected function hasValidCredentials($user, $credentials)
    {
        return ! is_null($user) && $this->provider->validateCredentials($user, $credentials) && $user->is_active === 1;
    }
}
