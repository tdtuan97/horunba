<?php
/**
 * Custom imap class.
 *
 * @package App
 * @subpackage Custom
 * @copyright Copyright (c) 2018 RiverCrane Corporation. All Rights Reserved.
 * @author van.qui <van.qui.rcvn2012@gmail.com>
 */
namespace App\Custom;

class Imap
{
    private $imap = null;
    
    private $strConn  = '';
    private $username = '';
    private $password = '';
    
    public function __construct($imapConf, $imap = null)
    {
        if ($imap === null) {
            $link = '/imap/ssl';
            if (!empty($imapConf['link'])) {
                $link = $imapConf['link'];
            }
            $folder = 'INBOX';
            if (!empty($imapConf['folder'])) {
                $folder = $imapConf['folder'];
            }
            $this->strConn    = "{{$imapConf['host']}:{$imapConf['port']}{$link}}{$folder}";
            $this->username   = $imapConf['username'];
            $this->password   = $imapConf['password'];
            $this->imap = imap_open($this->strConn, $this->username, $this->password);
        } else {
            $this->imap = $imap;
        }
    }
    
    /**
     * Re open Imap
     *
     * @return void
     */
    public function reOpen()
    {
        $this->imap = imap_open($this->strConn, $this->username, $this->password);
    }

    /**
     * Search email by query
     *
     * @param  string $query
     * @param  object $imap
     * @return object
     */
    public function searchMail($query, $imap = null)
    {
        if ($imap === null) {
            $imap = $this->imap;
        }
        return imap_search($imap, $query);
    }

    /**
     * Get header mail
     *
     * @param  int    $uid
     * @param  object $imap
     * @return object
     */
    public function getHeader($uid, $imap = null)
    {
        if ($imap === null) {
            $imap = $this->imap;
        }
        return imap_header($imap, $uid);
    }

    /**
     * Get body mail
     *
     * @param  int    $uid
     * @param  object $imap
     * @return string
     */
    public function getBody($uid, $imap = null)
    {
        if ($imap === null) {
            $imap = $this->imap;
        }
        $body = $this->getPart($imap, $uid, "TEXT/PLAIN");
        // if HTML body is empty, try getting text body
        if (!$body) {
            $body = $this->getPart($imap, $uid, "TEXT/HTML");
        }
        return $body;
    }

    /**
     * Get part body mail
     *
     * @param   object  $imap
     * @param   int     $uid
     * @param   string  $mimetype
     * @param   object  $structure
     * @param   string  $partNumber
     * @return  mixed
     */
    public function getPart($imap, $uid, $mimetype, $structure = false, $partNumber = false)
    {
        if (!$structure) {
               $structure = imap_fetchstructure($imap, $uid);
        }
        if ($structure) {
            $structMimeType = $this->getMimeType($structure);
            if ($this->compareMineType($mimetype, $structMimeType)) {
                if (!$partNumber) {
                    $partNumber = 1;
                }
                $text = imap_fetchbody($imap, $uid, $partNumber);
                switch ($structure->encoding) {
                    case 3:
                        $text = imap_base64($text);
                        break;
                    case 4:
                        $text = imap_qprint($text);
                        break;
                }
                if (isset($structure->parameters) && isset($structure->parameters[0])
                    && isset($structure->parameters[0]->value)) {
                    $tmpCharset = strtoupper($structure->parameters[0]->value);
                    if ($tmpCharset ==='ISO-2022-JP' || $tmpCharset === 'SHIFT_JIS') {
                        $text = mb_convert_encoding($text, 'UTF-8', $tmpCharset);
                    }
                }
                return [
                    'data' => $text,
                    'mime' => $structMimeType
                ];
            }

            // multipart
            if ($structure->type == 1) {
                foreach ($structure->parts as $index => $subStruct) {
                    $prefix = "";
                    if ($partNumber) {
                        $prefix = $partNumber . ".";
                    }
                    $data = $this->getPart($imap, $uid, $mimetype, $subStruct, $prefix . ($index + 1));
                    if ($data) {
                        return $data;
                    }
                }
            }
        }
        return false;
    }
    
    /**
     * Compare two mine type
     *
     * @param type $first
     * @param type $second
     */
    public function compareMineType($needles, $type)
    {
        if (!is_array($needles)) {
            $needles = [$needles];
        }
        foreach ($needles as $needle) {
            if ($this->checkMineType($needle, $type)) {
                return true;
            }
        }
        return false;
    }
    
    /**
     * Check mine type
     *
     * @param type $needle
     * @param type $type
     * @return boolean
     */
    public function checkMineType($needle, $type)
    {
        $needle = explode("/", $needle);
        $type   = explode("/", $type);
        if ($needle[0] !== '*' && strtoupper($needle[0]) !== strtoupper($type[0])) {
            return false;
        }
        if ($needle[1] !== '*' && strtoupper($needle[1]) !== strtoupper($type[1])) {
            return false;
        }
        return true;
    }

    /**
     * Get mime type
     *
     * @param  object  $structure
     * @return string
     */
    public function getMimeType($structure)
    {
        $primaryMimetype = array("TEXT", "MULTIPART", "MESSAGE", "APPLICATION", "AUDIO", "IMAGE", "VIDEO", "OTHER");

        if ($structure->subtype) {
            return $primaryMimetype[(int)$structure->type] . "/" . $structure->subtype;
        }
        return "TEXT/PLAIN";
    }

    /**
     * Get attachment info
     *
     * @param  int    $uid
     * @param  string $partNum
     * @param  object $imap
     * @return array
     */
    public function getAttachmentInfo($uid, $partNum, $imap = null)
    {
        if ($imap === null) {
            $imap = $this->imap;
        }
        $part = imap_fetchstructure($imap, $uid);
        return $this->getAttachments($imap, $uid, $part, $partNum);
    }

    /**
     * Get attachment from mail
     *
     * @param  object  $imap
     * @param  int     $uid
     * @param  object  $part
     * @param  string  $partNum
     * @return array
     */
    public function getAttachments($imap, $uid, $part, $partNum)
    {
        $attachments = array();
        if (isset($part->parts)) {
            foreach ($part->parts as $key => $subpart) {
                if ($partNum != "") {
                    $newPartNum = $partNum . "." . ($key + 1);
                } else {
                    $newPartNum = ($key + 1);
                }
                $result = $this->getAttachments($imap, $uid, $subpart, $newPartNum);
                if (count($result) != 0) {
                     array_push($attachments, $result);
                }
            }
        } elseif (isset($part->disposition)) {
            if (strtoupper($part->disposition) == "ATTACHMENT" && isset($part->dparameters)) {
                $partStruct = imap_bodystruct($imap, $uid, $partNum);
                $attachmentDetails = array(
                    "name"    => $part->dparameters[0]->value,
                    "partNum" => $partNum,
                    "enc"     => $partStruct->encoding
                );
                return $attachmentDetails;
            }
        } elseif (isset($part->subtype)) {
            if (strtoupper($part->subtype) == 'OCTET-STREAM' && isset($part->parameters)) {
                $partStruct = imap_bodystruct($imap, $uid, $partNum);
                $attachmentDetails = array(
                    "name"    => $part->parameters[0]->value,
                    "partNum" => $partNum,
                    "enc"     => $partStruct->encoding
                );
                return $attachmentDetails;
            }
        }
        return $attachments;
    }

    /**
     * Save attachment
     *
     * @param  int     $uid
     * @param  string  $partNum
     * @param  string  $encoding
     * @param  string  $path
     * @param  object  $imap
     * @return boolean
     */
    public function saveAttachment($uid, $partNum, $encoding, $path, $imap = null)
    {
        if ($imap === null) {
            $imap = $this->imap;
        }
        $partStruct = imap_bodystruct($imap, $uid, $partNum);
        $message    = imap_fetchbody($imap, $uid, $partNum);

        switch ($encoding) {
            case 0:
            case 1:
                $message = imap_8bit($message);
                break;
            case 2:
                $message = imap_binary($message);
                break;
            case 3:
                $message = imap_base64($message);
                break;
            case 4:
                $message = quoted_printable_decode($message);
                break;
        }

        return file_put_contents($path, $message);
    }
    
    
    private $allData = [];
    /**
     * Get all part of body
     *
     * @param int    $uid
     * @param object $imap
     * return array
     */
    public function getAllBody($uid, $imap = null, $mimetype = false)
    {
        if ($imap === null) {
            $imap = $this->imap;
        }
        $this->allData = [];
        $this->getAllBodyData($uid, $imap, $mimetype);
        return $this->allData;
    }
    
    /**
     * Decode data
     *
     * @param  object $data
     * @param  string $encoding
     * @return object
     */
    public function decodeData($data, $encoding)
    {
        switch ($encoding) {
            case 0:
                break;
            case 1:
                break;
            case 2:
                $data = imap_binary($data);
                break;
            case 3:
                $data = imap_base64($data);
                break;
            case 4:
                $data = quoted_printable_decode($data);
                break;
            case 5:
                break;
        }
        return $data;
    }
    
    /**
     * Get all data body
     *
     * @param  int $uid
     * @param  object $imap
     * @param  array  $mimetype
     * @param  object $structure
     * @param  string $partNumber
     * @return boolean
     */
    public function getAllBodyData($uid, $imap, $mimetype = false, $structure = false, $partNumber = false)
    {
        $primaryType = array("TEXT", "MULTIPART", "MESSAGE", "APPLICATION", "AUDIO", "IMAGE", "VIDEO", "OTHER");
        if (!$structure) {
            $structure = imap_fetchstructure($imap, $uid);
        }
        if ($structure) {
            if (isset($structure->parts)) {
                foreach ($structure->parts as $index => $subStruct) {
                    $prefix = "";
                    if ($partNumber) {
                        $prefix = $partNumber . ".";
                    }
                    $data = $this->getAllBodyData($uid, $imap, $mimetype, $subStruct, $prefix . ($index + 1));
                }
            } else {
                if ($mimetype !== false) {
                    $partMimeType = $this->getMimeType($structure);
                    if (!$this->compareMineType($mimetype, $partMimeType)) {
                        return false;
                    }
                }
                if (!$partNumber) {
                    $partNumber = 1;
                }
                
                $data = imap_fetchbody($imap, $uid, $partNumber);
                
                $data = $this->decodeData($data, $structure->encoding);
                
                if (isset($structure->ifparameters) && $structure->ifparameters == 1
                    && isset($structure->parameters) && isset($structure->parameters[0])
                    && isset($structure->parameters[0]->value)) {
                    $tmpCharset = strtoupper($structure->parameters[0]->value);
                    if ($tmpCharset ==='ISO-2022-JP' || $tmpCharset === 'SHIFT_JIS') {
                        $data = mb_convert_encoding($data, 'UTF-8', $tmpCharset);
                    } elseif ($tmpCharset === 'GB2312' || $tmpCharset === 'GB18030') {
                        $data = mb_convert_encoding($data, 'UTF-8', 'GB18030');
                    }
                }
                $isAttachment = false;
                $name         = '';
                $id           = '';
                if (isset($structure->disposition) && strtoupper($structure->disposition) == 'ATTACHMENT') {
                    $isAttachment = true;
                    if (isset($structure->dparameters)) {
                        $name = $structure->dparameters[0]->value;
                    } elseif (isset($structure->parameters)) {
                        $name = $structure->parameters[0]->value;
                    }
                }
                
                if (isset($structure->ifid) && $structure->ifid == 1 && isset($structure->id)) {
                    $id = $structure->id;
                    $id = str_replace(["<", ">"], "", $id);
                }
                
                $this->allData[] = [
                    'data'       => $data,
                    'type'       => $primaryType[$structure->type],
                    'subtype'    => $structure->subtype,
                    'attachment' => $isAttachment,
                    'name'       => $name,
                    'id'         => $id
                ];
                return true;
            }
        }
    }
}
