<?php
/**
 * Process Api GLSC
 *
 * @package    App\Custom
 * @subpackage ProcessApiGLSC
 * @copyright  Copyright (c) 2018 RiverCrane! Corporation. All Rights Reserved.
 * @author     Truong Nghia<truong.van.nghia@rivercrane.vn>
 */
namespace App\Custom;

use App\Models\Batches\DtDelivery;
use App\Models\Batches\DtDeliveryDetail;
use App\Models\Batches\TInsertShippingGroupKey;
use App\Models\Batches\LogTShippingDetailModel;
use App\Models\Batches\LogInsertShippingGlsc;
use Config;
use App;
use SoapClient;
use App\Notification;
use App\Events\Command as eCommand;
use Illuminate\Support\Facades\Log;
use Utilities;

class ProcessApiGLSC
{
    /**
     * Process call api process insert shipping list.
     * @param string $batch
     * @var object
     */
    public static function insertShippingList($batch)
    {
        $slack   = new Notification(CHANNEL['horunba']);
        $modelD  = new DtDelivery();
        $modelDD = new DtDeliveryDetail();
        $modelK  = new TInsertShippingGroupKey();
        $arrKey  = $modelD->getDataProcessInsertShipping(300);
        $arrKeyError = [];
        if ($arrKey->count() !== 0) {
            $arrKey = $arrKey->pluck('received_order_id')->toArray();
        } else {
            return $arrKeyError;
        }
        $datas   = $modelD->getDataProcessInsertShippingByKey($arrKey);
        $auth    = null;
        if (App::environment(['local', 'test'])) {
            $auth = Config::get("apiservices.glsc_test");
        } else {
            $auth = Config::get("apiservices.glsc");
        }
        $successInsert  = 0;
        $failInsert     = 0;
        $arrShipping    = [];
        $arrKeys        = [];
        $arrKeyUpdated  = [];
        $arrReceiveTemp = [];
        $arrSteps       = [];
        $arrStepUpdates = [];
        foreach ($datas as $data) {
            $keyTemp = [
                'received_order_id' => $data->received_order_id,
                'subdivision_num'   => $data->subdivision_num,
            ];
            $arrDataUpdate   = ['error_code' => null, 'error_message' => null];
            $subDivisionNum  = $data->subdivision_num;
            $groupKeyInitial = $data->group_key;
            if ($data->delivery_status !== 0) {
                $temp = $data->received_order_id . $data->subdivision_num;
                if (!isset($arrKeyUpdated[$temp])) {
                    $maxSub = $modelD->getMaxSubdivisionNum($data->received_order_id);
                    $modelK->where('tShippingG_Key', '=', 1)->increment('GroupKey', 1);
                    $groupKey = $modelK->where('tShippingG_Key', '=', 1)->first();
                    $arrDataUpdate['subdivision_num'] = $maxSub->subdivision_num + 1;
                    $arrDataUpdate['group_key']       = $groupKey->GroupKey;
                    $modelDD->updateData($keyTemp, ['subdivision_num' => ($maxSub->subdivision_num + 1)]);
                    $arrKeyUpdated[$temp]['subdivision_num'] = $maxSub->subdivision_num + 1;
                    $arrKeyUpdated[$temp]['group_key']       = $groupKey->GroupKey;
                    $modelD->updateData($keyTemp, $arrDataUpdate);
                }
                $subDivisionNum  = $arrKeyUpdated[$temp]['subdivision_num'];
                $groupKeyInitial = $arrKeyUpdated[$temp]['group_key'];
                $keyTemp['subdivision_num'] = $subDivisionNum;
            } else {
                $modelD->updateData($keyTemp, $arrDataUpdate);
            }

            if (!isset($arrReceiveTemp[$data->received_order_id]) && count($arrReceiveTemp) === 20) {
                $arrSteps[]       = $arrShipping;
                $arrStepUpdates[] = $arrKeys;
                $arrReceiveTemp   = [];
                $arrKeys          = [];
                $arrShipping      = [];
            }
            $arrReceiveTemp[$data->received_order_id] = $data->received_order_id;
            $arrKeys[] = $keyTemp;
            $arrShipping[] = [
                'OrderNo'              => $data->received_order_id,
                'OrderBNo'             => $subDivisionNum,
                'OrderLNo'             => $data->detail_line_num,
                'DeliDate'             => $data->delivery_date,
                'TimeCd'               => $data->delivery_time,
                'DeliveryInstructions' => $data->delivery_instrustions,
                'Account'              => $data->account,
                'DeliPlanDate'         => $data->delivery_plan_date,
                'TotalAmount'          => $data->total_price,
                'RecKb'                => 1,
                'ShippingInstructions' => str_replace([chr(11), chr(29) . '「'], ['', '「'], $data->shipping_instructions),
                'CustomerNm'           => $data->customer_name,
                'DNm'                  => $data->delivery_name,
                'DPostal'              => $data->delivery_postal,
                'DPref'                => $data->delivery_perfecture,
                'DAddress1'            => self::convertMultiByte($data->delivery_city),
                'DAddress2'            => self::convertMultiByte($data->delivery_sub_address),
                'DTel'                 => $data->delivery_tel,
                'DConKb'               => '',
                'Mall'                 => '',
                'Age'                  => '',
                'Sex'                  => '',
                'ItemCd'               => self::convertMultiByte($data->product_code),
                'ItemNm'               => str_replace(["'","”","“",chr(13),chr(10),chr(11), chr(29) . '「'], ['','','',' ',' ','', '「'], self::convertMultiByte($data->product_name)),
                'JanCd'                => $data->product_jan,
                'MakerNm'              => $data->maker_name,
                'MakerNo'              => $data->maker_code,
                'ItemUrl'              => $data->product_image_url,
                'Quantity'             => $data->delivery_num,
                'RecordTag'            => '',
                'GroupKey'             => $groupKeyInitial,
                'ReceNm'               => '',
                'Proviso'              => '',
                'StoreCd'              => $data->store_code,
                'InvoiceRemarks'       => $data->invoice_remarks,
                'DeliveryCode'         => $data->delivery_code,
            ];
        }
        if (count($arrShipping) !== 0) {
            $arrSteps[]       = $arrShipping;
            $arrStepUpdates[] = $arrKeys;
        }
        if (count($arrSteps) !== 0) {
            $glscUrl        = "http://gls.ec-union.biz/glsc/glscapi/GlscApi.asmx?WSDL";
            $successInsert  = 0;
            $failInsert     = 0;
            $successGroup  = 0;
            $failGroup     = 0;
            foreach ($arrSteps as $key => $step) {
                $arrCheckExists = [];
                foreach ($arrStepUpdates[$key] as $k => $v) {
                    if (isset($arrCheckExists[$v['received_order_id']])
                        && $arrCheckExists[$v['received_order_id']] === $v['subdivision_num']) {
                        unset($arrStepUpdates[$key][$k]);
                    } else {
                        $arrCheckExists[$v['received_order_id']] = $v['subdivision_num'];
                    }
                }
                try {
                    $client  = new SoapClient($glscUrl, array(
                        'trace'              => true,
                        'keep_alive'         => true,
                        'connection_timeout' => 60,
                        'cache_wsdl'         => WSDL_CACHE_NONE,
                        'compression'        => SOAP_COMPRESSION_ACCEPT | SOAP_COMPRESSION_GZIP | SOAP_COMPRESSION_DEFLATE,
                    ));
                    $params  = [
                        'Auth' => [
                            'LoginID'              => $auth['LoginID'],
                            'Password'             => $auth['Password'],
                            'DeveloperID'          => $auth['DeveloperID'],
                        ],
                        'Shipping' => $step
                    ];

                    Log::info("Write original insertShippingList xml");
                    $orginalLog = print_r($params, true);
                    Log::info("Write: $orginalLog");
                    $response = $client->insertShippingList($params);
                    Log::info("Write original insertShippingList result");
                    $orginalLog = print_r($response, true);
                    Log::info("Write: $orginalLog");
                    $arrCheckExists = [];
                    foreach ($arrStepUpdates[$key] as $item) {
                        if (isset($arrCheckExists[$item['received_order_id']])
                            && $arrCheckExists[$item['received_order_id']] === $item['subdivision_num']) {
                            continue;
                        } else {
                            $arrCheckExists[$item['received_order_id']] = $item['subdivision_num'];
                        }
                        if (!isset($response->insertShippingListResult->tUpdateReplyModel)) {
                            $successInsert++;
                            $arrUpdate = [];
                            $arrUpdate['delivery_status'] = 4;
                            $arrUpdate['process_date']    = now();
                            $modelD->updateData($item, $arrUpdate);
                        } else {
                            $tUpdate = $response->insertShippingListResult->tUpdateReplyModel;
                            $flg = true;
                            if (is_array($tUpdate)) {
                                foreach ($tUpdate as $keyError) {
                                    if (isset($keyError->Key->string)) {
                                        $arrUpdate = self::processError($keyError, $item);
                                        if (!empty($arrUpdate)) {
                                            $flg = false;
                                            if (isset($arrUpdate['delivery_status']) && $arrUpdate['delivery_status'] === 9) {
                                                $arrKeyError[] = $item['received_order_id'] . '-' . $item['subdivision_num'];
                                                $failInsert++;
                                            } else {
                                                $successInsert++;
                                            }
                                            $modelD->updateData($item, $arrUpdate);
                                        }
                                    }
                                }
                            } else {
                                $arrUpdate = self::processError($tUpdate, $item);
                                if (!empty($arrUpdate)) {
                                    if (isset($arrUpdate['delivery_status']) && $arrUpdate['delivery_status'] === 9) {
                                        $arrKeyError[] = $item['received_order_id'] . '-' . $item['subdivision_num'];
                                        $failInsert++;
                                    } else {
                                        $successInsert++;
                                    }
                                    $flg = false;
                                    $modelD->updateData($item, $arrUpdate);
                                }
                            }
                            if ($flg) {
                                $successInsert++;
                                $arrUpdate = [];
                                $arrUpdate['delivery_status'] = 0;
                                $arrUpdate['process_date']    = now();
                                $modelD->updateData($item, $arrUpdate);
                            }
                        }
                    }
                    self::logInsertShippingGlsc($step);
                } catch (\Exception $e) {
                    $failInsert++;
                    report($e);
                    print_r($e->getMessage());
                }

                // Process insert group key
                $dataGroupKeys = $modelD->getDataProcessUpdateGroupKey();
                foreach ($dataGroupKeys as $dataGroupKey) {
                    $glscUrl = "http://gls.ec-union.biz/glsc/glscapi/GlscApi.asmx?WSDL";
                    $client  = new SoapClient($glscUrl, array(
                        'trace'              => true,
                        'keep_alive'         => true,
                        'connection_timeout' => 30,
                        'cache_wsdl'         => WSDL_CACHE_NONE,
                        'compression'        => SOAP_COMPRESSION_ACCEPT | SOAP_COMPRESSION_GZIP | SOAP_COMPRESSION_DEFLATE,
                    ));
                    $params  = [
                        'Auth' => [
                            'LoginID'              => $auth['LoginID'],
                            'Password'             => $auth['Password'],
                            'DeveloperID'          => $auth['DeveloperID'],
                        ],
                        'GroupKey' => $dataGroupKey->group_key,
                    ];
                    $arrKey = [
                        'received_order_id' => $dataGroupKey->received_order_id,
                        'subdivision_num'   => $dataGroupKey->subdivision_num,
                    ];
                    try {
                        Log::info("Write updateCompletionGroupForShipping xml");
                        $orginalLog = print_r($params, true);
                        Log::info("Write: $orginalLog");
                        $response = $client->updateCompletionGroupForShipping($params);
                        Log::info("Write updateCompletionGroupForShipping result");
                        $orginalLog = print_r($response, true);
                        Log::info("Write: $orginalLog");
                        $arrUpdate = [];
                        $arrUpDD   = [];
                        $checkExists = self::logInsertShipping([$arrKey]);
                        if (!isset($response->updateCompletionGroupForShippingResult->ErrInfo->tErrModel)) {
                            $successGroup++;
                            $arrUpdate = [
                                'delivery_status' => 1,
                            ];
                        } else {
                            $modelK->where('tShippingG_Key', '=', 1)->increment('GroupKey', 1);
                            $groupKey = $modelK->where('tShippingG_Key', '=', 1)->first();
                            $failGroup++;
                            $error = $response->updateCompletionGroupForShippingResult->ErrInfo;
                            if ($checkExists) {
                                $arrUpdate = [
                                    'delivery_status' => 1,
                                    'process_date'    => now(),
                                    'error_message'   => $error->tErrModel->Message,
                                    'error_code'      => $error->tErrModel->ErrorCd,
                                ];
                            } else {
                                $arrUpdate = [
                                    'delivery_status' => 9,
                                    'error_message'   => $error->tErrModel->Message,
                                    'error_code'      => $error->tErrModel->ErrorCd,
                                ];
                                $arrKeyError[] = $arrKey['received_order_id'] . '-' . $arrKey['subdivision_num'];
                            }
                        }
                        $modelD->updateData($arrKey, $arrUpdate);
                        if (count($arrUpDD) !== 0) {
                            $modelDD->updateData($arrKey, $arrUpDD);
                        }
                    } catch (\Exception $e) {
                        $failGroup++;
                        report($e);
                        print_r($e->getMessage());
                    }
                }
            }
            Log::info("Process API insert shipping success:$successInsert and fail $failInsert records");
            print_r("Process API insert shipping success:$successInsert and fail $failInsert records" . PHP_EOL);
            Log::info("Process API update group key success:$successGroup and fail $failGroup records");
            print_r("Process API update group key success:$successGroup and fail $failGroup records" . PHP_EOL);
        } else {
            Log::info("No data API insert shipping");
            print_r("No data API insert shipping" . PHP_EOL);
        }
        return $arrKeyError;
        /*$arrKeys = [
            [
                'received_order_id' => OrderNo,
                'subdivision_num' => OrderBNo
            ]
        ];*/
        //self::logInsertShipping($arrKeys);
    }

    /**
     * Process error.
     * @param array $arrInfo
     * @param array $arrKey
     * @var array
     */
    public static function processError($arrInfo, $arrKey)
    {
        $keyCheck = $arrInfo->Key->string;
        $arrUpdate = [];
        $check = false;
        if ($keyCheck[0] === $arrKey['received_order_id']
            && (int)$keyCheck[1] === $arrKey['subdivision_num']
        ) {
            if (isset($arrInfo->ErrInfo)) {
                if (!isset($arrInfo->ErrInfo->tErrModel)) {
                    $check = true;
                } elseif (is_array($arrInfo->ErrInfo->tErrModel)) {
                    $arrMess = [];
                    $arrCode = [];
                    foreach ($arrInfo->ErrInfo->tErrModel as $err) {
                        $arrMess[] = $err->Message;
                        $arrCode[] = $err->ErrorCd;
                        if ((int)$err->ErrorCd === 7) {
                            $check = true;
                        }
                    }
                    $arrUpdate = [
                        'delivery_status' => 9,
                        'error_message'   => implode('|', $arrMess),
                        'error_code'      => implode('|', $arrCode),
                    ];
                } else {
                    if ((int)$arrInfo->ErrInfo->tErrModel->ErrorCd === 7) {
                        $check = true;
                    }
                    $arrUpdate = [
                        'delivery_status' => 9,
                        'error_message'   => $arrInfo->ErrInfo->tErrModel->Message,
                        'error_code'      => $arrInfo->ErrInfo->tErrModel->ErrorCd,
                    ];
                }
            }
        }
        if ($check) {
            return [
                'delivery_status' => 4,
                'process_date'    => now(),
            ];
        } else {
            return $arrUpdate;
        }
    }

    /**
     * Process insert log.
     * @param array $arrKey
     * @return boolean
     */
    public static function logInsertShipping($arrKey)
    {
        $auth    = null;
        if (App::environment(['local', 'test'])) {
            $auth = Config::get("apiservices.glsc_test");
        } else {
            $auth = Config::get("apiservices.glsc");
        }
        $glscUrl        = "http://gls.ec-union.biz/glsc/glscapi/GlscApi.asmx?WSDL";
        $arrInserts = [];
        try {
            foreach ($arrKey as $item) {
                $params  = [
                    'Auth' => [
                        'LoginID'              => $auth['LoginID'],
                        'Password'             => $auth['Password'],
                        'DeveloperID'          => $auth['DeveloperID'],
                    ],
                    'OrderNo' => $item['received_order_id'],
                    'OrderBNo' => $item['subdivision_num'],
                    'Shipping' => [
                        'OrderNo'              => '',
                        'OrderBNo'             => '',
                        'OrderDate'            => '',
                        'OrderTime'            => '',
                        'CustomerNm'           => '',
                        'DNm'                  => '',
                        'DPostal'              => '',
                        'DPref'                => '',
                        'DAddress1'            => '',
                        'DAddress2'            => '',
                        'DTel'                 => '',
                        'Account'              => '',
                        'DeliveryNm'           => '',
                        'InquiryNo'            => '',
                        'DeliDate'             => '',
                        'TimeCd'               => '',
                        'DeliPlanDate'         => '',
                        'DeliveryInstructions' => '',
                        'ShippingInstructions' => '',
                        'DConKb'               => '',
                        'RecKb'                => '',
                        'Mall'                 => '',
                        'Age'                  => '',
                        'Sex'                  => '',
                        'TotalAmount'          => '',
                        'ShippingStatusKb'     => '',
                        'ReceNm'               => '',
                        'Proviso'              => '',
                        'StoreCd'              => '',
                        'StoreNm'              => '',
                        'InvoiceRemarks'       => '',
                        'DeliveryCode'         => '',
                    ]
                ];
                $client  = new SoapClient($glscUrl, array(
                    'trace'              => true,
                    'keep_alive'         => true,
                    'connection_timeout' => 500,
                    'cache_wsdl'         => WSDL_CACHE_NONE,
                    'compression'        => SOAP_COMPRESSION_ACCEPT | SOAP_COMPRESSION_GZIP | SOAP_COMPRESSION_DEFLATE,
                ));
                $response = $client->getShippingDetail($params);
                if (isset($response->ShipDetail)) {
                    $arrInserts[] = [
                        'OrderNo'              => $response->ShipDetail->OrderNo,
                        'OrderBNo'             => $response->ShipDetail->OrderBNo,
                        'OrderDate'            => $response->ShipDetail->OrderDate,
                        'OrderTime'            => $response->ShipDetail->OrderTime,
                        'CustomerNm'           => $response->ShipDetail->CustomerNm,
                        'DNm'                  => $response->ShipDetail->DNm,
                        'DPostal'              => $response->ShipDetail->DPostal,
                        'DPref'                => $response->ShipDetail->DPref,
                        'DAddress1'            => $response->ShipDetail->DAddress1,
                        'DAddress2'            => $response->ShipDetail->DAddress2,
                        'DTel'                 => $response->ShipDetail->DTel,
                        'Account'              => $response->ShipDetail->Account,
                        'DeliveryNm'           => $response->ShipDetail->DeliveryNm,
                        'InquiryNo'            => $response->ShipDetail->InquiryNo,
                        'DeliDate'             => $response->ShipDetail->DeliDate,
                        'TimeCd'               => $response->ShipDetail->TimeCd,
                        'DeliPlanDate'         => $response->ShipDetail->DeliPlanDate,
                        'DeliveryInstructions' => $response->ShipDetail->DeliveryInstructions,
                        'ShippingInstructions' => $response->ShipDetail->ShippingInstructions,
                        'DConKb'               => $response->ShipDetail->DConKb,
                        'RecKb'                => $response->ShipDetail->RecKb,
                        'Mall'                 => $response->ShipDetail->Mall,
                        'Age'                  => $response->ShipDetail->Age,
                        'Sex'                  => $response->ShipDetail->Sex,
                        'TotalAmount'          => $response->ShipDetail->TotalAmount,
                        'ShippingStatusKb'     => $response->ShipDetail->ShippingStatusKb,
                        'ReceNm'               => $response->ShipDetail->ReceNm,
                        'Proviso'              => $response->ShipDetail->Proviso,
                        'StoreCd'              => $response->ShipDetail->StoreCd,
                        'StoreNm'              => $response->ShipDetail->StoreNm,
                        'InvoiceRemarks'       => $response->ShipDetail->InvoiceRemarks,
                        'DeliveryCode'         => $response->ShipDetail->DeliveryCode,
                    ];
                }

            }
            if (count($arrInserts) !== 0) {
                $modelLT = new LogTShippingDetailModel();
                $modelLT->insert($arrInserts);
                return true;
            } else {
                return false;
            }
        } catch (\Exception $e) {
            report($e);
            print_r($e->getMessage());
        }
    }

    /**
     * Process insert log data to call api.
     * @param array $arrData
     * @return boolean
     */
    public static function logInsertShippingGlsc($arrData)
    {
        $arrInsert = [];
        foreach ($arrData as $key => $item) {
            $arrInsert[$key]['received_order_id'] = $item['OrderNo'];
            $arrInsert[$key]['subdivision_num']   = $item['OrderBNo'];
            $arrInsert[$key]['detail_line_num']   = $item['OrderLNo'];
            $arrInsert[$key]['product_code']      = $item['ItemCd'];
            $arrInsert[$key]['product_name']      = $item['ItemNm'];
            $arrInsert[$key]['delivery_num']      = $item['Quantity'];
            $arrInsert[$key]['group_key']         = $item['GroupKey'];
        }
        $modelLISG = new LogInsertShippingGlsc();
        return $modelLISG->insert($arrInsert);
    }

    /**
     * Covert multibyte to singlebyte.
     * @param string $string
     * @return string
     */
    public static function convertMultiByte($string)
    {
        return mb_convert_kana($string, 'KVa');
    }
}
