<?php
/**
 * Custom commond
 *
 * @package    App\Custom
 * @subpackage Utilities
 * @copyright  Copyright (c) 2018 RiverCrane! Corporation. All Rights Reserved.
 * @author     Truong Nghia<truong.van.nghia@rivercrane.vn>
 */
namespace App\Custom;

use Config;

class Utilities
{
    /**
     * Process check exception.
     * @param object $exception
     * @return string message
     */
    public static function checkMessageException($exception)
    {
        if (empty($exception->getPrevious())) {
            return $exception->getMessage();
        } else {
            return $exception->getPrevious()->getMessage();
        }
    }
    public static function getBackLink($request, $currentPageKey, $backLink)
    {
        $linkConfig = Config::get("redirectlink.{$currentPageKey}");
        $dataUrl = parse_url($backLink, PHP_URL_PATH);
        $currentSesion = $request->session()->get('back_page');
        if (in_array($dataUrl, $linkConfig)) {
            $dataBack = [$currentPageKey => $backLink];
            if (empty($currentSesion)) {
                $request->session()->put(['back_page' => $dataBack]);
            } else {
                $request->session()->put(['back_page' => array_merge($currentSesion, $dataBack)]);
            }
        }
        if (isset($request->session()->get('back_page')[$currentPageKey])) {
            return $request->session()->get('back_page')[$currentPageKey];
        } else {
            return $linkConfig[0];
        }
    }

    /**
     * Process check performance.
     * @param array $arraySearch
     * @param array $arrSort
     * @param array $arrIgnore
     * @return boolean true or false
     */
    public static function checkPerformance($arraySearch, $arrSort, $arrIgnore, $arrCheckZero = [])
    {
        $flag = true;
        foreach ($arrIgnore as $value) {
            unset($arraySearch[$value]);
        }
        if (count(array_filter($arraySearch)) > 0) {
            $flag = false;
        }
        if (count(array_filter($arrSort)) > 0) {
            $flag = false;
        }
        if (count($arrCheckZero)) {
            foreach ($arrCheckZero as $value) {
                if ($arraySearch[$value] === 0 || $arraySearch[$value] === '0') {
                    $flag = false;
                }
            }
        }
        return $flag;
    }
}
