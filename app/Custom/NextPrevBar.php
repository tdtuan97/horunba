<?php
/**
 * Custom commond
 *
 * @package    App\Custom
 * @subpackage NextPrevBar
 * @copyright  Copyright (c) 2018 RiverCrane! Corporation. All Rights Reserved.
 * @author     Van Qui<van.qui@rivercrane.vn>
 */

namespace App\Custom;

class NextPrevBar
{
    /**
     * Generate url for bar
     *
     * @param  string  $indexAction
     * @param  string  $detailAction
     * @param  array   $idNameKeys
     * @param  array   $searchKeys
     * @param  array   $sortKeys
     * @param  object  $model
     * @return array
     */
    public static function generateLink($indexAction, $detailAction, $idNameKeys, $searchKeys, $sortKeys, $model)
    {
        $queryStr     = $_SERVER['QUERY_STRING'];
        $params       = self::parseUrl($queryStr);
        $ignoreKeys   = array_map(
            function ($value) {
                return $value . '_key';
            },
            $idNameKeys
        );
        
        $indexQuery   = self::mergeUrl($params, $ignoreKeys);
        $indexUrl     = $indexAction . (!empty($indexQuery)?("?" . $indexQuery):"");
 
        $arrSearch = [];
        foreach ($searchKeys as $key => $val) {
            if (is_numeric($key)) {
                $arrSearch[$val] = $params[$val] ?? null;
            } else {
                $arrSearch[$key] = $val;
            }
        }
        
        $arrSort = [];
        foreach ($sortKeys as $key => $val) {
            if (is_numeric($key)) {
                $arrSort[$val] = $params['sort_' . $val] ?? null;
            } else {
                $arrSort[$key] = $val;
            }
        }
        $idKeys = [];
        foreach ($idNameKeys as $key) {
            $idKeys[$key] = $params[$key . '_key'] ?? null;
        }
        $countTotal = $model->getNextPrevData(['type' => 'count_total', 'idKeys' => $idKeys], $arrSearch, $arrSort);
        $posResult  = $model->getNextPrevData(['type' => 'get_pos_row', 'idKeys' => $idKeys], $arrSearch, $arrSort);

        if (empty($posResult)) {
            return redirect()->to($indexUrl);
        }
        $rawPos   = (int)$posResult->position;
        $perPage  = empty($params['per_page'])?20:(int)$params['per_page'];
        $page     = ceil($rawPos/$perPage);
        if ($page > 1) {
            $params['page'] = $page;
        } else {
            unset($params['page']);
        }
        
        $indexQuery     = self::mergeUrl($params, $ignoreKeys);
        $indexUrl       = $indexAction . (!empty($indexQuery)?("?" . $indexQuery):"");

        $curPos   = $rawPos - 1;
        $prevPos  = $curPos - 1;
        $result   = $model->getNextPrevData(
            [
                'type' => 'get_item_by_pos',
                'pos' => $prevPos,
                'idKeys' => $idKeys
            ],
            $arrSearch,
            $arrSort
        );
        $nextUrl = null;
        $prevUrl = null;
        $nextId  = null;
        $prevId  = null;
        
        foreach ($result as $key => $val) {
            if (self::compareIdKey($val, $idKeys)) {
                if (isset($result[$key - 1])) {
                    $prevId = $result[$key - 1];
                }
                if (isset($result[$key + 1])) {
                    $nextId = $result[$key + 1];
                }
                break;
            }
        }
        
        $nextKeyChange = [];
        foreach ($idNameKeys as $key) {
            $nextKeyChange[$key . '_key'] = $nextId[$key]??'';
        }
        
        $prevKeyChange = [];
        foreach ($idNameKeys as $key) {
            $prevKeyChange[$key . '_key'] = $prevId[$key]??'';
        }
        
        if ($nextId !== null) {
            $nextQuery = self::mergeUrl($params, ['page'], $nextKeyChange);
            $nextUrl   = $detailAction . (!empty($nextQuery)?("?" . $nextQuery):"");
        }
        if ($prevId !== null) {
            $prevQuery = self::mergeUrl($params, ['page'], $prevKeyChange);
            $prevUrl   = $detailAction . (!empty($prevQuery)?("?" . $prevQuery):"");
        }
        return [
            'pageOnPages' => trans('messages.pos_per_page', ['pos' => $rawPos, 'total' => $countTotal]),
            'nextUrl'  => $nextUrl,
            'prevUrl'  => $prevUrl,
            'indexUrl' => $indexUrl
        ];
    }
    
    /**
     * Compare id keys
     *
     * @param  object  $item
     * @param  array   $arrSource
     * @return boolean
     */
    public static function compareIdKey($item, $arrSource)
    {
        foreach ($arrSource as $key => $val) {
            if ($item[$key] != $val) {
                return false;
            }
        }
        return true;
    }
    
    /**
     * Parse url to array
     *
     * @param  string $queryStr
     * @return array
     */
    public static function parseUrl($queryStr)
    {
        $arrQuery = explode("&", $queryStr);
        $arrParam = array();
        foreach ($arrQuery as $item) {
            $tmp = explode("=", $item);
            if (count($tmp) > 1) {
                $index = urldecode($tmp[0]);
                $value = urldecode($tmp[1]);
                $arrParam[$index][] = $value;
            }
        }
        $params = array();
        foreach ($arrParam as $key => $val) {
            $count = count($val);
            if ($count === 1) {
                $params[$key] = $val[0];
            } elseif ($count > 1) {
                $params[$key] = $val;
            }
        }
        return $params;
    }

    /**
     * Merge array to url
     *
     * @param  array  $params
     * @param  array  $arrIgnore
     * @param  array  $arrChange
     * @return string
     */
    public static function mergeUrl($params, $arrIgnore = [], $arrChange = null)
    {
        $arrUrl = array();
        foreach ($params as $key => $val) {
            if (!in_array($key, $arrIgnore)) {
                if (isset($arrChange[$key])) {
                    $arrUrl[] = "$key=$arrChange[$key]";
                } elseif (is_array($val)) {
                    foreach ($val as $item) {
                        $arrUrl[] = "$key=$item";
                    }
                } else {
                    $arrUrl[] = "$key=$val";
                }
            }
        }
        return implode("&", $arrUrl);
    }
}
