<?php
/**
 * DB Extend
 *
 * @package    App\Custom
 * @subpackage DBExtend
 * @copyright  Copyright (c) 2018 RiverCrane! Corporation. All Rights Reserved.
 * @author     Van Qui<van.qui@rivercrane.vn>
 */
namespace App\Custom;

use DB;
use Config;

trait DBExtend
{
    protected $connectionName;
    
    protected $schemaName;
    
    protected $tableName;

    public function __construct()
    {
        $this->connectionName = parent::getConnectionName();
        $this->schemaName     = Config::get("database.connections.{$this->connectionName}.database");
        $this->tableName      = parent::getTable();
    }
    
    /**
     * process data insert
     *
     * @param  array $data
     * @return array
     */
    public function processDataInsert($data)
    {
        if (key($data) !== 0) {
            $data = [$data];
        }
        $arrKey    = array_keys($data[0]);
        $strKey    = "`" . implode("`,`", $arrKey) . "`";
        $arrParams = [];
        $arrValue  = [];
        $index     = 0;
        foreach ($data as $item) {
            $tmpKey = [];
            foreach ($item as $key => $val) {
                $curKey          = $key . $index;
                $tmpKey[]        = ':' . $curKey;
                $arrValue[$curKey] = $val;
            }
            $index++;
            $arrParams[] = '(' . implode(',', $tmpKey) . ')';
        }
        $strParam = implode(",", $arrParams);
        return [
            'key'   => $strKey,
            'param' => $strParam,
            'value' => $arrValue
        ];
    }
    
    /**
     * Insert ignore data
     *
     * @param type $data
     */
    public function insertIgnore($data)
    {
        $data = $this->processDataInsert($data);
        return DB::insert(
            "INSERT IGNORE INTO {$this->schemaName}.{$this->tableName} ({$data['key']}) VALUES {$data['param']}",
            $data['value']
        );
    }
    
    /**
     * replace data
     *
     * @param type $data
     */
    public function replace($data)
    {
        $data = $this->processDataInsert($data);
        return DB::insert(
            "REPLACE INTO {$this->schemaName}.{$this->tableName} ({$data['key']}) VALUES {$data['param']}",
            $data['value']
        );
    }
}
