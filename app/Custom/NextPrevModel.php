<?php
/**
 * Next Previous Model
 *
 * @package    App\Custom
 * @subpackage NextPrevModel
 * @copyright  Copyright (c) 2018 RiverCrane! Corporation. All Rights Reserved.
 * @author     Van Qui<van.qui@rivercrane.vn>
 */
namespace App\Custom;

use DB;

trait NextPrevModel
{
    /**
     * Get next previous data
     *
     * @param  array $nextPrevCond
     * @param  array $arraySearch
     * @param  array $arraySort
     * @return mixed
     */
    public function getNextPrevData($nextPrevCond, $arraySearch = null, $arraySort = null)
    {
        $selectKeys = array_keys($nextPrevCond['idKeys']);
        $strIdKeys  = "`" . implode("`,`", $selectKeys) . "`";
        $data = $this->select($selectKeys);
        
        if ($nextPrevCond['type'] === 'count_total') {
            $data = $this->conditionQuery($data, $arraySearch);
            return $data->count();
        }
        $data = $this->conditionQuery($data, $arraySearch, $arraySort);
        if ($nextPrevCond['type'] === 'get_pos_row') {
            $data->limit(2147483647);

            $queryTotal = $this->selectRaw("$strIdKeys, @rownum := @rownum + 1 AS position")
                ->from(DB::raw("({$data->toSql()}) as q"))
                ->mergeBindings($data->getQuery())
                ->crossJoin(DB::raw('(SELECT @rownum := 0) r'));

            $resultQuery = $this->selectRaw("$strIdKeys, position")
                ->from(DB::raw("({$queryTotal->toSql()}) as qt"))
                ->mergeBindings($queryTotal->getQuery());
            foreach ($nextPrevCond['idKeys'] as $key => $val) {
                $resultQuery->where($key, $val);
            }
            return $resultQuery->first();
        } elseif ($nextPrevCond['type'] === 'get_item_by_pos') {
            return $data->offset($nextPrevCond['pos'])->limit(3)->get();
        }
        return $data->get();
    }
}
