<?php

return [
    'mail' => [
        'host'     => env('FAX_MAIL_HOST', 'mail2.diyfactory.jp'),
        'port'     => env('FAX_MAIL_PORT', 25),
        'username' => env('FAX_MAIL_USERNAME', 'edi'),
        'password' => env('FAX_MAIL_PASSWORD', 'checkfax1@'),
        'from'     => env('FAX_MAIL_FROM', 'edi@diyfactory.jp'),
        'to'       => env('FAX_MAIL_TO', '@efaxsend.com'),
        'to_test'  => env('FAX_MAIL_TO_TEST', 'rcvntest1@gmail.com')
    ]
];
