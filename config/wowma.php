<?php
return [
    'real' => [
        'url'      => env('WOWMA_REAL_URL', 'https://api.manager.wowma.jp/wmshopapi'),
        'auth_key' =>
            'Bearer ' . env('WOWMA_REAL_AUTH_KEY', 'ae558c616a03229e0b39df5e98c93afb2d13637413dfe7e3aeb8cd26d7f1c00d'),
        'shop_id'  => env('WOWMA_REAL_SHOP_ID', 45162959)
    ],
    'test' => [
        'url'      => env('WOWMA_TEST_URL', 'https://sg.manager.wowma.jp/wmshopapi'),
        'auth_key' =>
            'Bearer ' . env('WOWMA_TEST_AUTH_KEY', 'd90f52ddbf17ebb7dd329d2c01b994084e33350cb5d77e468ab1fc45fd946d81'),
        'shop_id'  => env('WOWMA_TEST_SHOP_ID', 386790)
    ]
];
