class DataForm {
    constructor(getDataUrl, param = [], callback=null) {
        this.selector   = {};
        this.getDataUrl = getDataUrl;
        this.param      = param;
        this.selector['form-template'] = $('#form-template');
        this.selector['main_contain'] = $('#main-contain');
        Handlebars.registerHelper({
            getValueHandle: function (value) {
                if (typeof value === 'undefined' || value === null) {
                    return '';
                }
                return value;
            },
            isEmptyHandle: function (value) {
                return _.isEmpty(value);
            },
            trans: function (value) {
                return _.get(window.i18n, value);
            },

            checkBooleanHandle: function (value) {
                if (value) {
                    return true;
                }
                return false;
            },
            log: function (something) {
                console.log(something);
            },
            inputHandle: function(config) {
                let {
                    groupClass,
                    errorClass,
                    labelClass,
                    labelTitle,
                    fieldGroupClass,
                    errorMessage,
                    hasError,
                    errorPopup,
                    iconPrefix,
                    iconPrefixClass,
                    isRequire,
                    typeField,
                    beforeField,
                    afterField,
                    value,
                    readOnly,
                    ...other
                } = config.hash;
                if (!hasError) {
                    hasError = false;
                }
                if (!groupClass) {
                    groupClass = "form-group";
                }
                if (!errorClass) {
                    errorClass = "has-error";
                }
                if (!labelClass) {
                    labelClass = "control-label";
                }
                if (!labelTitle) {
                    labelTitle = "";
                }
                if (!fieldGroupClass) {
                    fieldGroupClass = "";
                }
                if (!beforeField) {
                    beforeField = "";
                }
                if (!afterField) {
                    afterField = "";
                }
                if (!readOnly) {
                    readOnly = false;
                }
                let attrInput = '';
                for (var key in other) {
                    attrInput += key + '="' + other[key] + '" ';
                }
                if (!typeField) {
                    typeField = '<input ' + attrInput + 'value="' + value + '"' + (readOnly?' readonly':'') + '>';
                } else {
                    typeField = '<textarea ' + attrInput + '>'+ value +'</textarea>';
                }
                let html = "";
                if (hasError) {
                    if (errorPopup) {
                        html += '<div class="' + fieldGroupClass + ' has-feedback">';
                        if (iconPrefix) {
                            html += '<span class="input-group-addon"><i class="' + iconPrefixClass + '"></i></span>';
                        }
                        html += beforeField;
                        html += typeField;
                        html += afterField;
                        html += '<i\
                                class="fa fa-times text-red form-control-feedback icon-error"\
                                data-placement="top"\
                                data-toggle="tooltip"\
                                data-original-title="' + errorMessage + '"></i>';
                        html += '</div>';
                    } else {
                        html += '<div class="' + fieldGroupClass + '">';
                        if (iconPrefix) {
                            html += '<span class="input-group-addon"><i class=' + fieldGroupClass + '"></i></span>';
                        }
                        html += beforeField;
                        html += typeField;
                        html += afterField;
                        html += '<span class="help-block">\
                                    <strong class="error-new-line">' + errorMessage + '</strong>\
                                </span>';
                        html += '</div>';
                    }
                } else {
                    html += '<div class="' + fieldGroupClass + '">';
                    if (iconPrefix) {
                        html += '<span class="input-group-addon"><i class=' + fieldGroupClass + '"></i></span>';
                    }
                    html += beforeField;
                    html += typeField;
                    html += afterField;
                    html += '</div>';
                }
                let htmlReturn = '<div class="' + groupClass;
                if (hasError) {
                    htmlReturn += ' ' + errorClass;
                }
                htmlReturn += '">';
                if (labelTitle) {
                    htmlReturn += '<label class="' + labelClass +'">' + labelTitle;
                    if (isRequire) {
                        htmlReturn += '<span class="text-danger">＊</span>';
                    }
                    htmlReturn += '</label>';
                }
                htmlReturn += html + '</div>';
                return htmlReturn;
            },
            selectHandle: function(config) {
                let {
                    groupClass,
                    errorClass,
                    labelClass,
                    labelClassRight,
                    labelTitle,
                    labelTitleRight,
                    fieldGroupClass,
                    errorMessage,
                    hasError,
                    errorPopup,
                    optionClass,
                    options,
                    handleChangeMultiSelect,
                    valueMulti,
                    isRequire,
                    readOnly,
                    isTitleRight,
                    isTitleLeft,
                    ...other
                } = config.hash;
                if (typeof isTitleLeft === 'undefined') {
                    isTitleLeft = true;
                }
                if (!hasError) {
                    hasError = false;
                }
                if (typeof isTitleRight === 'undefined') {
                    isTitleRight = false;
                }
                if (!readOnly) {
                    readOnly = false;
                }
                if (!groupClass) {
                    groupClass = "form-group";
                }
                if (!errorClass) {
                    errorClass = "has-error";
                }
                if (!labelClass) {
                    labelClass = "control-label";
                }
                if (!labelTitle) {
                    labelTitle = "";
                }
                if (!fieldGroupClass) {
                    fieldGroupClass = "";
                }
                if (!optionClass) {
                    optionClass = '';
                }
                if (!options) {
                    options = {};
                }
                let attrInput = '';
                for (var key in other) {
                    attrInput += key + '="' + other[key] + '" ';
                }
                let arrOptions = options;
                if (!Array.isArray(options)) {
                    arrOptions = [];
                    Object.keys(options).map((key) => {
                        arrOptions.push({key: key, value: options[key]});
                    });
                }
                var strOption = '';
                arrOptions.map((obj) => {
                    if (readOnly === true && typeof readOnly !== undefined) {
                        if (obj.key !== config.hash.value){
                            strOption += '<option disabled="disabled"  value="' + obj.key + '">' + obj.value + '</option>';
                        } else {
                            strOption += '<option value="' + obj.key + '">' + obj.value + '</option>';
                        }
                    } else {
                        if (obj.key != config.hash.value){
                            strOption += '<option value="' + obj.key + '">' + obj.value + '</option>';
                        } else {
                            strOption += '<option selected value="' + obj.key + '">' + obj.value + '</option>';
                        }
                    }
                });
                let html = '';
                if (hasError) {
                    if (errorPopup) {
                        html += '<div class="' + fieldGroupClass + ' has-feedback">\
                            <select ' + attrInput + '>' + strOption +'</select>\
                            <i\
                                class="fa fa-times text-red form-control-feedback icon-error"\
                                data-placement="top"\
                                data-toggle="tooltip"\
                                data-original-title="' + errorMessage + '"\
                            ></i>\
                            </div>';
                    } else {
                        html += '<div class="' + fieldGroupClass + '">\
                                <select ' + attrInput + '>' + strOption +'</select>\
                                <span class="help-block">\
                                    <strong class="error-new-line">' + errorMessage + '</strong>\
                                </span>\
                            </div>';
                    }
                } else {
                    html += '<div class="' + fieldGroupClass + '">\
                                <select ' + attrInput + '>' + strOption +'</select>\
                            </div>';
                }
                let htmlReturn = '<div class="' + groupClass;
                if (hasError) {
                    htmlReturn += ' ' + errorClass;
                }
                htmlReturn += '">';
                if (labelTitle && isTitleLeft) {
                    htmlReturn += '<label class="' + labelClass +'">' + labelTitle;
                    if (isRequire) {
                        htmlReturn += '<span class="text-danger">＊</span>';
                    }
                    htmlReturn += '</label>';
                }
                htmlReturn += html;
                if (isTitleRight) {
                    htmlReturn += '<label class="' + labelClassRight +'">' + labelTitleRight + '</label>';
                }
                htmlReturn += '</div>';
                return htmlReturn;

            },
            radioHandle : function(config) {
                let {
                    currentValue,
                    value,
                    classLabel,
                    ...other
                } = config.hash;
                let attrInput = 'value=' + value + ' ';
                if (currentValue == value) {
                    attrInput += 'checked="checked" ';
                }
                for (var key in other) {
                    attrInput += key + '="' + config.hash[key] + '" ';
                }

                let html = '<label class="control control-radio ' + classLabel + ' ">\
                                <input type="radio"\
                                ' + attrInput + '>\
                                </input>\
                                <div class="nice-icon-check"></div>\
                            </label>';
                return html;
            },
            checkBoxHandle : function(config) {
                let {
                    currentValue,
                    classLabel,
                    value,
                    ...other
                } = config.hash;
                let attrInput = 'value=' + value + ' ';
                if (currentValue == value) {
                    attrInput += 'checked="checked" ';
                }
                for (var key in other) {
                    attrInput += key + '="' + config.hash[key] + '" ';
                }
                let html = '<label class="control nice-checkbox ' + classLabel + ' ">\
                                <input type="checkbox"\
                                ' + attrInput + '>\
                                </input>\
                                <div class="nice-icon-check"></div>\
                            </label>';
                return html;
            },
            eq: function(ope1, ope2) {
                return ope1 === ope2;
            },
            checkMultiCheckBox: function(arrValue, $currentValue) {
                if(typeof arrValue !== 'undefined' && arrValue.indexOf($currentValue) !== -1) {
                    return $currentValue;
                }
                return false;
            }

        });
        this.getDetail(this.getDataUrl, this.param, callback);
    }
    submitForm(submutUrl, dataPost = {}, callback=null) {
        $('.loading').show();
        var _this = this;
        $.ajax({
            url: submutUrl,
            method: "POST",
            data: dataPost,
            dataType: "json",
            success: function(response) {
                if (response.status === 0) {
                    $('.loading').hide();
                    var source   = _this.selector['form-template'].html();
                    var template = Handlebars.compile(source);
                    var html     = template(response);
                    _this.selector['main_contain'].html(html);
                    _this.reRunJs();
                    if (callback !== null && typeof callback === "function") {
                        var param_callback = [];
                        if (typeof response.param_callback !== 'undefined') {
                            param_callback = response.param_callback;
                        }
                        callback.apply(null, param_callback);
                    }
                } else if (typeof response.link_redirect !== 'undefined') {
                    window.location.href = response.link_redirect;
                }
            },
            error: function(jqXHR, textStatus, errorThrown) {
                $('.loading').hide();
                $.notify({
                    icon: 'glyphicon glyphicon-warning-sign',
                    message: '[' + jqXHR.status + '] ' + errorThrown,
                },{
                    type: 'danger',
                    delay: 2000
                });
            }
        });
    }
    reRunJs() {
        $('[data-toggle="tooltip"]').tooltip();
    }

    getDetail (url, param, callback) {
        $('.loading').show();
        var _this = this;
        $.ajax({
            url: url,
            data: param,
            dataType: "json",
            success: function(response) {
                $('.loading').hide();
                if (typeof response.status !== 'undefined' && response.status == 0) {
                    window.location.href = response.link_redirect;
                } else {
                    var source   = _this.selector['form-template'].html();
                    var template = Handlebars.compile(source);
                    var html     = template(response);
                    _this.selector['main_contain'].html(html);
                    _this.reRunJs();
                    if (callback !== null && typeof callback === "function") {
                        callback.apply(null, []);
                    }
                }
            },
            error: function(jqXHR, textStatus, errorThrown) {
                $('.loading').hide();
                $.notify({
                    icon: 'glyphicon glyphicon-warning-sign',
                    message: '[' + jqXHR.status + '] ' + errorThrown,
                },{
                    type: 'danger',
                    delay: 2000
                });
            }
        });
    }

    checkRealTime (url, param, link_redirect) {
        $.ajax({
            url: url,
            method: "POST",
            data: param,
            dataType: "json",
            success: function(response) {
                if (typeof response.accessFlg !== 'undefined' && response.accessFlg === false) {
                    var source   = $('#deny-template').html();
                    var template = Handlebars.compile(source);
                    response.url = link_redirect;
                    var html     = template(response);
                    if (!$('#message-modal-deny').hasClass('in')) {
                        $('#popup-deny').html(html);
                        $('#message-modal-deny').modal('show');
                        clearInterval(deny);
                    }
                }
            }
        });
    }

}
