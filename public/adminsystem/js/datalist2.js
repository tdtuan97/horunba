class DataList2 {
    constructor(getDataUrl, ignoreKey = [], selector = {}, initialCallBack = null) {
        this.selector        = {};
        this.getDataUrl      = getDataUrl;
        this.ignoreKey       = ignoreKey;
        this.initialCallBack = initialCallBack;
        this.selector['search_form'] = $('#search-form');
        this.selector['main_contain'] = $('#main-contain');
        this.selector['list_template'] = $('#list-template');
        this.selector['paginator_template'] = $('#paginator-template');
        if (Object.keys(selector).length > 0) {
            Object.keys(selector).map((key) => {
                this.selector[key] = selector[key];
            });
        }
        Handlebars.registerHelper({
            eq: function(ope1, ope2) {
                return ope1 === ope2;
            },
            gt: function(ope1, ope2) {
                return ope1 > ope2;
            },
            lt: function(ope1, ope2) {
                return ope1 < ope2;
            },
            gte: function(ope1, ope2) {
                return ope1 >= ope2;
            },
            lte: function(ope1, ope2) {
                return ope1 <= ope2;
            }
        });
    }
    search(callback = null, callparam = []) {
        var dataPost = this.selector['search_form'].serialize();
        this.reloadData(dataPost, callback, callparam);
    }
    handleClickPage(page = 1, callback = null, callparam = []) {
        var dataPost = this.selector['search_form'].serialize();
        dataPost += '&page=' + page;
        this.reloadData(dataPost, callback, callparam);
    }
    clearSearch(callback = null, callparam = []) {
        // this.selector['search_form'][0].reset();
        this.selector['search_form'].find('input, select, textarea').not("select[name=per_page], ul[class='multiselect-container dropdown-menu'] input").val('');
        this.selector['search_form'].find("select[multiple='multiple']").multiselect('clearSelection');
        var dataPost = this.selector['search_form'].serialize();
        this.reloadData(dataPost, callback, callparam);
    }
    reloadData(dataPost = {}, callback = null, callparam = []) {
        $('.loading').show();
        var _this = this;
        var _this2 = this;
        $.ajax({
            url: this.getDataUrl,
            data: dataPost,
            dataType: "json",
            success: function(response) {
                if (callback !== null && typeof callback === "function") {
                    callparam.unshift(response);
                    callparam.unshift(_this);
                    callback.apply(null, callparam);
                }
                var htmlPaginator = '';
                if (response.pageData.data && response.pageData.data.length > 0) {
                    var arrData = response.pageData;
                    var paginator = _this.calculatorPageList(arrData.total, arrData.current_page, arrData.per_page, arrData.last_page);
                    var sourcePaginator = _this.selector['paginator_template'].html();
                    var templatePaginator = Handlebars.compile(sourcePaginator);
                    htmlPaginator = templatePaginator(paginator);
                }
                var source   = _this.selector['list_template'].html();
                var template = Handlebars.compile(source);
                var html     = template(response);
                _this.selector['main_contain'].html(html);
                if (_this2.initialCallBack !== null && typeof _this2.initialCallBack === "function") {
                    _this2.initialCallBack.apply(null, []);
                }
                $('.paginator-content').html(htmlPaginator);
                $('.loading').hide();
                _this.pushHistory(dataPost, _this.ignoreKey);
            },
            error: function(jqXHR, textStatus, errorThrown) {
                $('.loading').hide();
                $.notify({
                    icon: 'glyphicon glyphicon-warning-sign',
                    message: '[' + jqXHR.status + '] ' + errorThrown,
                },{
                    type: 'danger',
                    delay: 2000
                });
            }
        });
    }
    calculatorPageList(totalItems, currentPage, itemsPerPage, totalPages) {
        currentPage     = currentPage   ||  1;
        itemsPerPage    = itemsPerPage  ||  20;
        var startPage, endPage;
        if (totalPages <= 10) {
            startPage = 1;
            endPage = totalPages;
        } else {
            if (currentPage <= 6) {
                startPage = 1;
                endPage = 10;
            } else if (currentPage + 4 >= totalPages) {
                startPage = totalPages - 9;
                endPage = totalPages;
            } else {
                startPage = currentPage - 5;
                endPage = currentPage + 4;
            }
        }
        var startIndex = (currentPage - 1) * itemsPerPage;
        var endIndex = Math.min(startIndex + itemsPerPage - 1, totalItems - 1);
        var pages = _.range(startPage, endPage + 1);

        var classDisL    = (currentPage === 1 ? 'disabled' : '');
        var classDisR    = (currentPage === totalPages ? 'disabled' : '');

        return {
            totalPages  : totalPages,
            currentPage : currentPage,
            pages       : pages,
            classDisL   : classDisL,
            classDisR   : classDisR,
            prevPage    : (currentPage !== 1 ? currentPage - 1 : 1),
            nextPage    : (currentPage < totalPages ? currentPage + 1 : totalPages)
        };
    }
    pushHistory(dataPost, ignoreKey = []) {
        dataPost = decodeURIComponent(dataPost);
        var arrHref = window.location.href.split('?');
        var arrDataPost = dataPost.split("&");
        var arrQuery = [];
        if (arrDataPost.length > 0) {
            for (var i = 0; i < arrDataPost.length; i++) {
                var arrHashes = arrDataPost[i].split("=");
                if (arrHashes.length > 1 && ignoreKey.indexOf(arrHashes[0]) == -1 && arrHashes[1]) {
                    if (arrHashes[0] == 'per_page' && arrHashes[1] == 20) {
                        continue;
                    }
                    arrQuery.push(arrHashes[0] + "=" + arrHashes[1]);
                }
            }
        }
        var newHref = arrHref[0];
        if (arrQuery.length > 0) {
            newHref += '?' + arrQuery.join('&');
        }
        window.history.pushState("", "", newHref);
    }
}
