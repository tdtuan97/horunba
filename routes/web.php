<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Auth::routes();
Route::get('phpinfo', function () {
    phpinfo();
});
$this->get('logout', 'Auth\LoginController@logout')->name('logout');
Route::any('receipt/{mailSerial}', 'ReceiptPDFController@receipt');
Route::middleware(['auth', 'role', 'setparams'])->group(function () {
    // List Customer
    Route::any('/customer', 'CustomerController@index')->name('customer.list');
    Route::any('/customer/data-list', 'CustomerController@dataList');
    Route::any('/customer/save', 'CustomerController@save');
    Route::any('/customer/get-form-data', 'CustomerController@getFormData');
    Route::any('/customer/delete', 'CustomerController@delete');
    Route::any('/customer/process-export-csv', 'CustomerController@processExportCsv');
    // List Mail Template
    Route::any('/', 'DashboardController@index');
    Route::any('/settings/mail-template', 'MailTemplateController@index')->name('mail.template');
    Route::any('/settings/mail-template/data-list', 'MailTemplateController@dataList');
    Route::any('/settings/mail-template/save', 'MailTemplateController@save');
    Route::any('/settings/mail-template/detail', 'MailTemplateController@detail');
    Route::any('/settings/mail-template/get-form-data', 'MailTemplateController@getFormData');
    Route::any('/settings/mail-template/get-max-sequence', 'MailTemplateController@getMaxSequence');
    Route::get('/change-password', 'Auth\ChangePasswordController@showChangeForm')->name('change-password');
    Route::any('/reset-password', 'Auth\ChangePasswordController@change');
    // Deje
    Route::any('/deje', 'DejeController@index')->name('deje.list');
    Route::any('/deje/data-list', 'DejeController@dataList');
    Route::any('/deje/save', 'DejeController@save');
    Route::any('/deje/get-date-detail', 'DejeController@getDataDetail');
    Route::any('/deje/detail', 'DejeController@detail');
    Route::any('/deje/process-parse-content', 'DejeController@processParseContent');
    Route::any('/deje/get-product', 'DejeController@getProduct');
    Route::any('/deje/export-issue', 'DejeController@exportIssue');
    Route::any('/deje/update-data-detail', 'DejeController@updateDataDetail');
    Route::any('/deje/clone-data', 'DejeController@cloneData');
    Route::any('/deje/delete', 'DejeController@delete');
    Route::any('/deje/print', 'DejeController@printDetail');
    //Genre screen
    Route::any('/settings/genre', 'GenreController@index')->name('genre');
    Route::any('/settings/genre/data-list', 'GenreController@dataList');
    Route::any('/settings/genre/save', 'GenreController@save');
    Route::any('/settings/genre/delete', 'GenreController@delete');

    // List Mail Timming Management
    Route::any('/settings/mail-timing', 'SendMailTimingController@index')->name('mail.timing');
    Route::any('/settings/mail-timing/data-list', 'SendMailTimingController@dataList');
    Route::any('/settings/mail-timing/change-status', 'SendMailTimingController@changeStatus');
    Route::get('/settings/mail-timing/get-form-data', 'SendMailTimingController@getFormData');
    Route::any('/settings/mail-timing/get-template-name', 'SendMailTimingController@getTemplateName');
    Route::any('/settings/mail-timing/save', 'SendMailTimingController@save');

    // List Send Mail
    Route::any('/send-mail', 'SendMailController@index')->name('send.mail');
    Route::any('/send-mail/dataList', 'SendMailController@dataList');
    Route::any('/send-mail/detail', 'SendMailController@detail');
    Route::any('/send-mail/get-items', 'SendMailController@getItems');

    // List Order Management
    Route::any('/order-management', 'OrderManagementController@index')->name('order.info.list');
    Route::any('/order-management/data-list', 'OrderManagementController@dataList');
    Route::any('/order-management/detail', 'OrderManagementController@detail');
    Route::any('/order-management/change-delay', 'OrderManagementController@changeDelay');
    Route::any('/order-management/change-change-is-confirmed', 'OrderManagementController@changeIsConfirmed');
    Route::any('/order-management/export-issue', 'OrderManagementController@exportIssue');
    Route::any('/order-management/order-editing', 'OrderManagementController@orderEditing');
    Route::any('/order-management/get-form-order-data', 'OrderManagementController@getFormOrderData');
    Route::any('/order-management/order-detail-editing', 'OrderManagementController@orderDetailEditing');
    Route::any('/order-management/get-form-order-detail-data', 'OrderManagementController@getFormOrderDetailData');
    Route::any('/order-management/get-data-product', 'OrderManagementController@getDataProduct');
    Route::any('/order-management/update-flg-mall', 'OrderManagementController@updateFlgMall');
    Route::any('/order-management/re-calculate', 'OrderManagementController@reCalculate');
    Route::any('/order-management/order-new', 'OrderManagementController@newOrder');
    Route::any('/order-management/get-form-order-new', 'OrderManagementController@getFormNewOrderData');
    Route::any('/order-management/get-data-product-name', 'OrderManagementController@getDataProductName');
    Route::any('/order-management/get-data-code-fee', 'OrderManagementController@getDataCodeFee');
    Route::any('/order-management/update-cancel-reason', 'OrderManagementController@updateCancelOrder');
    Route::any('/order-management/update-delivery', 'OrderManagementController@updateDelivery');
    Route::any('/order-management/update-memo-up-ope', 'OrderManagementController@updateMemoUpOPE');
    Route::any('/order-management/check-cancel-all', 'OrderManagementController@checkCancelAll');
    Route::any('/order-management/cancel-not-enought', 'OrderManagementController@cancelNotEnought');
    Route::any('/order-management/process-export-csv', 'OrderManagementController@processExportCsv');
    Route::any('/order-management/import', 'OrderManagementController@import');
    Route::any('/order-management/change-back-list', 'OrderManagementController@changeBackList');
    Route::any('/order-management/change-is-claim', 'OrderManagementController@changeisClaim');
    Route::any('/order-management/change-detail-claim', 'OrderManagementController@changeDetailClaim');
    Route::any('/order-management/change-add-claim', 'OrderManagementController@changeAddClaim');
    //Screen Settings order sub
    Route::any('/settings/order-sub-status', 'SettingsController@orderSubStatusFormData');
    Route::any('/settings/order-status/orderStatusFormData', 'SettingsController@orderStatusFormData');
    Route::any('/settings/order-status/save', 'SettingsController@save');

    //Screen Settlement
    Route::any('/settings/settlement', 'SettlementController@index')->name('settlement');
    Route::any('/settings/settlement/data-list', 'SettlementController@dataList');
    Route::any('/settings/settlement/change-status', 'SettlementController@changeStatus');
    Route::any('/settings/settlement/save', 'SettlementController@save');
    Route::any('/settings/settlement/get-form-data', 'SettlementController@getFormData');

    //Batch
    Route::any('/batch-management', 'BatchManagementController@index')->name('batch');
    Route::any('/batch-management/dataList', 'BatchManagementController@dataList');
    Route::any('/batch-management/processEvent', 'BatchManagementController@processEvent');

    //Order Update Log
    Route::any('/order-update-log/get-info-log', 'OrderUpdateLogController@getInfoLog');
    Route::any('/order-update-log/get-item', 'OrderUpdateLogController@getItem');
    Route::any('/order-update-log/add-memo', 'OrderUpdateLogController@addMemo');
    Route::any('/order-update-log/update-status', 'OrderUpdateLogController@updateStatus');

    //Not Payment List
    Route::any('/not-payment-list', 'NotPaymentListController@index')->name('not_payment_list.list');
    Route::any('/not-payment-list/data-list', 'NotPaymentListController@dataList');

    //Payment
    Route::any('/payment/info-list', 'PaymentController@index')->name('payment.info.list');
    Route::any('/payment/info-list/data-list', 'PaymentController@dataList');
    Route::any('/payment/info-list/update', 'PaymentController@updateReceive');
    Route::any('/payment/info-list/import', 'PaymentController@import');
    Route::any('/payment/unknown-list', 'PaymentController@unknownList')->name('payment.unknown.list');
    Route::any('/payment/unknown-list/data-list', 'PaymentController@dataListUnknown');
    Route::any('/payment/unknown-list/get-order', 'PaymentController@getOrderByReceive');
    Route::any('/payment/unknown-list/save', 'PaymentController@saveReceive');
    Route::any('/payment/unknown-list/delete', 'PaymentController@deleteItem');
    //Re-payment
    Route::any('/repayment', 'RePaymentController@index')->name('repayment.list');
    Route::any('/repayment/save', 'RePaymentController@save');
    Route::any('/repayment/change-flag', 'RePaymentController@changeFlag');
    Route::any('/repayment/get-form-data', 'RePaymentController@getFormData');
    Route::any('/repayment/get-info-order-data', 'RePaymentController@getInfoOrderData');
    Route::any('/repayment/data-list', 'RePaymentController@dataList');
    Route::any('/repayment/process-export-csv', 'RePaymentController@processExportCsv');
    //Refund detail
    Route::any('/refund-detail/save', 'RefundDetailController@save')->name('refundDetail.save');
    Route::any('/refund-detail/get-form-data', 'RefundDetailController@getFormData');
    Route::any('/refund-detail/get-info-order-data', 'RefundDetailController@getInfoOrderData');
    Route::any('/refund-detail/data-list', 'RefundDetailController@dataList');
    //Order to supplier
    Route::any('/order-to-supplier', 'OrderToSupplierController@index')->name('orderToSupplier.list');
    Route::any('/order-to-supplier/data-list', 'OrderToSupplierController@dataList');
    Route::any('/order-to-supplier/change-status', 'OrderToSupplierController@changeStatus');
    Route::any('/order-to-supplier/save', 'OrderToSupplierController@save');
    Route::any('/order-to-supplier/get-product', 'OrderToSupplierController@getDataProduct');
    Route::any('/order-to-supplier/resale', 'OrderToSupplierController@reSale');
    Route::any('/order-to-supplier/return', 'OrderToSupplierController@processReturn');
    Route::any('/order-to-supplier/prepare-process-all', 'OrderToSupplierController@prepareProcessAll');
    Route::any('/order-to-supplier/get-fax-order', 'OrderToSupplierController@getFaxOrder');
    Route::any('/order-to-supplier/get-fax-order-common', 'OrderToSupplierController@getFaxOrderCommon');
    Route::any('/order-to-supplier/process-fax-order', 'OrderToSupplierController@processFaxOrder');
    Route::any('/import', 'Common@importCSV');
    Route::any('/process-export', 'Common@processExportCSV');
    Route::any('/export', 'Common@exportCSV');
    //cancel reason
    Route::any('/settings/cancel-reason-list', 'CancelReasonController@index')->name('cancel.reason.list');
    Route::any('/settings/cancel-reason-list/data-list', 'CancelReasonController@dataList');
    Route::any('/settings/cancel-reason-list/save', 'CancelReasonController@save');
    Route::any('/settings/cancel-reason-list/get-form-data', 'CancelReasonController@getFormData');

    //Users Management
    Route::any('/settings/users-management', 'UsersManagementController@index')->name('users.list');
    Route::any('/settings/users-management/data-list', 'UsersManagementController@dataList');
    Route::any('/settings/users-management/save', 'UsersManagementController@save');
    Route::any('/settings/users-management/get-form-data', 'UsersManagementController@getFormData');
    Route::any('/settings/users-management/change-active', 'UsersManagementController@changeActive');

    // Postal List
    Route::any('/settings/postal-list', 'PostalController@index')->name('postal.list');
    Route::any('/settings/postal-list/save', 'PostalController@save');
    Route::any('/settings/postal-list/change-flag', 'PostalController@changeFlag');
    Route::any('/settings/postal-list/data-list', 'PostalController@dataList');
    Route::any('/settings/postal-list/get-form-data', 'PostalController@getFormData');
    //Shipping free below limit
    Route::any('/settings/shipping-free-below-limit', 'ShippingFreeBelowLimitController@index')->name('shipping.list');
    Route::any('/settings/shipping-free-below-limit/data-list', 'ShippingFreeBelowLimitController@dataList');
    Route::any('/settings/shipping-free-below-limit/save', 'ShippingFreeBelowLimitController@save');
    Route::any('/settings/shipping-free-below-limit/get-form-data', 'ShippingFreeBelowLimitController@getFormData');
    Route::any('/settings/shipping-free-below-limit/change-delete', 'ShippingFreeBelowLimitController@changeDelete');
    // Dashboard checking edit user update
    Route::any('/dashboard/check-item-edit', 'DashboardController@checkItemEdit');
    //Return management
    Route::any('/return-management', 'ReturnManagementController@index')->name('return.list');
    Route::any('/return-management/data-list', 'ReturnManagementController@dataList');
    Route::any('/return-management/save', 'ReturnManagementController@save');
    Route::any('/return-management/get-form-data', 'ReturnManagementController@getFormData');
    Route::any('/return-management/get-data-product', 'ReturnManagementController@getDataProduct');
    Route::any('/return-management/process-mojax', 'ReturnManagementController@processMojax');
    Route::any('/return-management/data-list/import-mojax', 'ReturnManagementController@import');
    Route::any('/return-management/get-address', 'ReturnManagementController@getAddress');
    Route::any('/return-management/delete', 'ReturnManagementController@deleteItem');
    Route::any('/return-management/delete-all', 'ReturnManagementController@deleteAllItem');
    Route::any('/return-management/change-flag', 'ReturnManagementController@changeFlag');
    Route::any('/return-management/update-quantity', 'ReturnManagementController@updateQuantity');
    Route::any('/return-management/save-product', 'ReturnManagementController@saveReturnProduct');
    Route::any('/return-management/get-data-product-input', 'ReturnManagementController@getDataProductInput');
    //Cancel reservation
    Route::any('/cancel-reservation', 'CancelReservationController@index')->name('cancel.reservation.list');
    Route::any('/cancel-reservation/data-list', 'CancelReservationController@dataList');
    Route::any('/cancel-reservation/save', 'CancelReservationController@save');
    Route::any('/cancel-reservation/change-flag', 'CancelReservationController@changeFlag');
    Route::any('/cancel-reservation/get-form-data', 'CancelReservationController@getFormData');
    Route::any('/cancel-reservation/get-data-product', 'CancelReservationController@getDataProduct');

    //Receipt ledger
    Route::any('/receipt-management', 'ReceiptController@index')->name('receipt.list');
    Route::any('/receipt-management/data-list', 'ReceiptController@dataList');
    Route::any('/receipt-management/export-csv', 'ReceiptController@exportCsv');
    Route::any('/receipt-management/file-download', 'ReceiptController@downloadCsv');
    //Receive mail
    Route::any('/receive-mail', 'ReceiveMailController@index')->name('receive.mail.list');
    Route::any('/receive-mail/data-list', 'ReceiveMailController@dataList');
    Route::any('/receive-mail/save', 'ReceiveMailController@save');
    Route::any('/receive-mail/get-form-data', 'ReceiveMailController@getFormData');
    Route::any('/receive-mail/get-order', 'ReceiveMailController@getOrder');
    Route::any('/receive-mail/update-data', 'ReceiveMailController@updateData');
    //Receive mail
    Route::any('/stock-confirm', 'StockConfirmController@index')->name('stock.confirm.list');
    Route::any('/stock-confirm/data-list', 'StockConfirmController@dataList');
    Route::any('/stock-confirm/save', 'StockConfirmController@save');
    Route::any('/stock-confirm/get-form-data', 'StockConfirmController@getFormData');
    Route::any('/stock-confirm/get-order', 'StockConfirmController@getOrder');
    Route::any('/stock-confirm/update-data', 'StockConfirmController@updateData');

    //Edit send mail
    Route::any('/edit-send-mail/save', 'EditSendMailController@save');
    Route::any('/edit-send-mail/get-form-data', 'EditSendMailController@getFormData');
    //web mapping
    Route::any('/web-mapping', 'WebMappingController@index')->name('web.mapping.list');
    Route::any('/web-mapping/data-list', 'WebMappingController@dataList');
    Route::any('/web-mapping/update-status', 'WebMappingController@updateStatus');
    Route::any('/web-mapping/process-api', 'WebMappingController@processApi');
    Route::any('/web-mapping/process-after-import', 'WebMappingController@processAfterImport');
    Route::any('/web-mapping/import', 'WebMappingController@import');
    Route::any('/web-mapping/process-export-csv', 'WebMappingController@processExportCsv');
    Route::any('/web-mapping/process-update-all', 'WebMappingController@processUpdateAll');
    //settlement mapping
    Route::any('/settlement-mapping', 'SettlementMappingController@index')->name('settlement.mapping.list');
    Route::any('/settlement-mapping/data-list', 'SettlementMappingController@dataList');
    Route::any('/settlement-mapping/update-status', 'SettlementMappingController@updateStatus');
    Route::any('/settlement-mapping/process-api', 'SettlementMappingController@processApi');
    Route::any('/settlement-mapping/process-after-import', 'SettlementMappingController@processAfterImport');
    Route::any('/settlement-mapping/import', 'SettlementMappingController@import');
    Route::any('/settlement-mapping/process-export-csv', 'SettlementMappingController@processExportCsv');
    Route::any('/settlement-mapping/process-update-all', 'SettlementMappingController@processUpdateAll');
    //Uri mapping
    Route::any('/uri-mapping', 'UriMappingController@index')->name('uri.mapping.list');
    Route::any('/uri-mapping/data-list', 'UriMappingController@dataList');
    Route::any('/uri-mapping/import', 'UriMappingController@import');
    Route::any('/uri-mapping/process-data-import', 'UriMappingController@processDataImport');
    Route::any('/uri-mapping/export-excel', 'UriMappingController@processExportExcel');
    Route::any('/uri-mapping/save-data', 'UriMappingController@processSaveData');
    Route::any('/uri-mapping/save-data-option', 'UriMappingController@processSaveOption');
    //Edit send mail
    Route::any('/edit-send-mail/save', 'EditSendMailController@save');
    Route::any('/edit-send-mail/get-form-data', 'EditSendMailController@getFormData');
    //List order to supplier by monthly
    Route::any('/order-to-supplier-monthly', 'OrderToSupplierMonthlyController@index')->name('order2supmonthly.list');
    Route::any('/order-to-supplier-monthly/data-list', 'OrderToSupplierMonthlyController@dataList');
    Route::any('/order-to-supplier-monthly/save', 'OrderToSupplierMonthlyController@save');
    Route::any('/order-to-supplier-monthly/get-data-product', 'OrderToSupplierMonthlyController@getDataProduct');
    Route::any('/order-to-supplier-monthly/get-data-supplier', 'OrderToSupplierMonthlyController@getDataSupplier');
    Route::any('/order-to-supplier-monthly/import', 'OrderToSupplierMonthlyController@import');
    Route::any(
        '/order-to-supplier-monthly/process-order-supplier',
        'OrderToSupplierMonthlyController@processOrderToSupplier'
    );
    Route::any('/order-to-supplier-monthly/update-by-check', 'OrderToSupplierMonthlyController@updateByCheckBox');
    Route::any('/import', 'Common@importCSV');
    Route::get('downloaderror/{keyCached}', 'Common@downloadError');
    Route::any('/return-management/save-detail', 'ReturnManagementController@saveReturnDetail');
    Route::any('/return-management/get-repayment-price', 'ReturnManagementController@getRepaymentPrice');
    Route::any('/return-management/get-option-return-detail', 'ReturnManagementController@getOptionReturnDetail');
    //Return address
    Route::any('/return-address', 'ReturnAddressController@index')->name('return.address.list');
    Route::any('/return-address/data-list', 'ReturnAddressController@dataList');
    Route::any('/return-address/save', 'ReturnAddressController@save');
    Route::any('/return-address/save-address', 'ReturnAddressController@saveAddress');
    Route::any('/return-address/get-form-data', 'ReturnAddressController@getFormData');
    Route::any('/return-address/get-form-data-list', 'ReturnAddressController@getFormDataAddress');
    Route::any('/return-address/get-data-product', 'ReturnAddressController@getDataProduct');
    Route::any('/return-address/get-data-info-supplier', 'ReturnAddressController@getDataSupplier');

    //Store order
    Route::any('/store-order', 'StoreOrderController@index')->name('store.order.list');
    Route::any('/store-order/data-list', 'StoreOrderController@dataList');
    Route::any('/store-order/detail', 'StoreOrderController@detail');
    Route::any('/store-order/get-detail', 'StoreOrderController@getDetail');
    Route::any('/store-order/set-to-order', 'StoreOrderController@setToOrder');
    Route::any('/store-order/re-order', 'StoreOrderController@reOrder');
    //DeliveryList
    Route::any('/delivery', 'DeliveryController@index')->name('delivery.list');
    Route::any('/delivery/data-list', 'DeliveryController@dataList');
    Route::any('/delivery/save', 'DeliveryController@save');
    Route::any('/delivery/get-form-data', 'DeliveryController@getFormData');
    Route::any('/delivery/check-validate', 'DeliveryController@checkValidate');
    //Store product
    Route::any('/store-product', 'StoreProductController@index')->name('store.product.list');
    Route::any('/store-product/data-list', 'StoreProductController@dataList');
    Route::any('/store-product/change-is-updated', 'StoreProductController@changeIsUpdated');
    Route::any('/store-product/process-smaregi', 'StoreProductController@processSmaregi');
    Route::any('/store-product/add', 'StoreProductController@addNew');
    Route::any('/store-product/get-product', 'StoreProductController@getDataProduct');
    Route::any('/receive-mail/iframe', 'ReceiveMailController@iframe');
    Route::any('/settings/mail-template/iframe', 'MailTemplateController@iframe');
    Route::any('/send-mail/iframe', 'SendMailController@iframe');
    // Upload CSV
    Route::any('/upload-csv', 'UploadCsvController@index');
    Route::any('/upload-csv/save', 'UploadCsvController@save');
    // Memo
    Route::any('/memo-check', 'MemoCheckController@index');
    Route::any('/memo-check/data-list', 'MemoCheckController@dataList');
    Route::any('/memo-check/save', 'MemoCheckController@save');
    Route::any('/memo-check/notifications', 'MemoCheckController@countNotifications');
    Route::any('/memo-check/count-memo', 'MemoCheckController@countMemo');
    //
    Route::any('/stock-status', 'StockStatusController@index')->name('stock.status.list');
    Route::any('/stock-status/data-list', 'StockStatusController@dataList');
    //Permission
    Route::any('/settings/permission', 'PermissionController@index')->name('permission.list');
    Route::any('/settings/permission/data-list', 'PermissionController@dataList');
    Route::any('/settings/permission/update-permission', 'PermissionController@updatePermission');
    Route::any('/settings/permission/save-controller', 'PermissionController@saveController');
    Route::any('/settings/permission/save-permission', 'PermissionController@savePermission');
    Route::any('/settings/permission/delete-permission', 'PermissionController@deletePermission');
    Route::any('/settings/not-permission', 'NotPermissionController@index')->name('not-permission.page');
    //Controller
    Route::any('/settings/controller', 'ControlController@index')->name('controller.list');
    Route::any('/settings/controller/data-list', 'ControlController@dataList');
    Route::any('/settings/controller/save-controller', 'ControlController@saveController');
    Route::any('/settings/controller/update-controller', 'ControlController@updateController');
    Route::any('/settings/controller/delete-controller', 'ControlController@deleteController');
    //Membership
    Route::any('/membership', 'MembershipController@index')->name('membership.list');
    Route::any('/membership/data-list', 'MembershipController@dataList');
    Route::any('/membership/save', 'MembershipController@save');
    Route::any('/membership/detail', 'MembershipController@detail');
    Route::any('/membership/submit-form', 'MembershipController@submitForm');
    Route::any('/membership/review', 'MembershipController@review');
    Route::any('/membership/process-free-delete', 'MembershipController@processFreeOrDelete');
    Route::any('/membership/buy-point', 'MembershipController@buyPoint');
    Route::any('/membership/buy-point-detail', 'MembershipController@buyPointDetail');
    Route::any('/membership/buy-point-submit-form', 'MembershipController@buyPointSubmit');
    Route::any('/membership/take-lesson', 'MembershipController@takeLesson');
    Route::any('/membership/take-lesson-detail', 'MembershipController@takeLessonDetail');
    Route::any('/membership/take-lesson-submit-form', 'MembershipController@takeLessonSubmit');
    Route::any('/membership/get-info-opt', 'MembershipController@getInfoOpt');
    Route::any('/membership/karute', 'MembershipController@karutePdf');

    //Lesson
    Route::any('/lesson', 'LessonController@index')->name('lesson.list');
    Route::any('/lesson/data-list', 'LessonController@dataList');
    Route::any('/lesson/save', 'LessonController@save');
    Route::any('/lesson/detail', 'LessonController@detail');
    Route::any('/lesson/submit-form', 'LessonController@submitForm');
});

//Use language for reactjs
Route::get('/js/lang.js', function () {
    //Maybe we can use cache in future
    //$strings = Cache::rememberForever('lang.js', function () {
    $lang = config('app.locale');
    $files   = glob(resource_path('lang/' . $lang . '/*.php'));
    $strings = [];

    foreach ($files as $file) {
        $name           = basename($file, '.php');
        $strings[$name] = require $file;
    }
    header('Content-Type: text/javascript');
    echo('window.i18n = ' . json_encode($strings) . ';');
    exit();
})->name('assets.lang');

//Use status for reactjs
Route::get('/js/status.js', function () {
    $strings = [
        'ORDER_STATUS'     => ORDER_STATUS,
        'ORDER_SUB_STATUS' => ORDER_SUB_STATUS,
        'PRODUCT_STATUS'   => PRODUCT_STATUS
    ];
    header('Content-Type: text/javascript');
    echo('window._status = ' . json_encode($strings) . ';');
    exit();
})->name('assets.status');
