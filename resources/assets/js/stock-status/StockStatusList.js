import React, { Component } from 'react';

import { connect } from 'react-redux';
import * as Action from '../common/actions';
import * as queryString from 'query-string';

import BUTTON from '../common/components/form/BUTTON';
import FORM from '../common/components/form/FORM';
import INPUT from '../common/components/form/INPUT';
import DATEPICKER from '../common/components/form/DATEPICKER';

import TABLE from '../common/components/table/TABLE';
import THEAD from '../common/components/table/THEAD';
import TBODY from '../common/components/table/TBODY';
import TR from '../common/components/table/TR';
import TH from '../common/components/table/TH';
import TD from '../common/components/table/TD';
import SELECT from '../common/components/form/SELECT';
import LABEL from '../common/components/form/LABEL';
import moment from 'moment';

import SortComponent from '../common/components/table/SortComponent';
import PaginationContainer from '../common/containers/PaginationContainer'
import Loading from '../common/components/common/Loading';
import axios from 'axios';

class StockStatusList extends Component {

    constructor(props) {
        super(props);
        const stringParams = queryString.parse(props.location.search);
        this.state = {};
        if (!_.isEmpty(stringParams)) {
            this.state = stringParams;
        }
        this.state.processBarColor = 'progress-bar-warning';
        this.props.reloadData(dataListUrl, this.state);
    }

    handleChange = (event) => {
        const target = event.target;
        const value = target.type === 'checkbox' ? target.checked : target.value;
        const name = target.name;
        this.setState({
            [name]: (value)?value:undefined
        },() => {
            if (name === 'per_page') {
                this.handleReloadData();
            }
        });
    }

    handleClearClick = (event) => {
        Object.keys(this.state).map((key) => {
            if (key !== 'per_page') {
                delete this.state[key];
            }
        });
        this.handleReloadData();
    }

    handleReloadData = () => {
        let params = {};
        let dataSource = this.props.dataSource;
        let tmpParams = this.state;
        Object.keys(tmpParams).map(key => {
            if (tmpParams[key] !== ""
                && tmpParams[key] !== null
                && key !== 'page') {
                    params[key] = tmpParams[key];
            } else {
                delete params[key];
            }
        });
        this.props.reloadData(dataListUrl, params, true);
    }

    handleChangeMultiSelect = (name, value, checked) => {
        let currentValue = this.state[name];
        if (typeof currentValue === 'undefined') {
            currentValue = [];
        } else if (!Array.isArray(this.state[name])) {
            currentValue = [this.state[name]];
        }
        if (Array.isArray(value)) {
            currentValue = [...value];
        } else {
            let index = currentValue.indexOf(value);
            if (checked === false) {
                if (index !== -1) {
                    currentValue.splice(index, 1);
                }
            } else {
                if (index === -1) {
                    currentValue.push(value);
                }
            }
        }
        this.setState({
            [name]: currentValue,
        });
    }
    
    componentDidUpdate() {
        if (!_.isEmpty(this.props.dataSource)) {
            $('.loading').hide();
        }
    }

    componentWillMount() {
        window.onpopstate = (event) => {
            location.reload();
        };
    }

    handleClickPage = (page) => {
        this.setState({
            page: page
        },() => {
            this.props.reloadData(dataListUrl, this.state, true);
        });
    }

    handleSortClick = (name, nextSort) => {
        Object.keys(this.state).map((key) => {
            if (key.startsWith('sort_')) {
                delete this.state[key];
            }
        });
        if (nextSort === 'none') {
            delete this.state[name];
            this.props.reloadData(dataListUrl, this.state, true);
        } else {
            this.setState({[name]: nextSort}, () => {
                this.props.reloadData(dataListUrl, this.state, true);
            });
        }

    }

    handleGetData = (name) => {
        Object.keys(this.state).map((key) => {
            delete this.state[key];
        });
        let param = {};
        if (name === 'nanko_glsc') {
            param.nanko_glsc = true;
        }
        this.setState(param, () => {
            this.props.reloadData(dataListUrl, this.state, true);
        })
    }

    render() {
        let dataSource = this.props.dataSource;
        let index = 0;
        let totalItems = 0;
        let itemsPerPage = 20;
        if (!_.isEmpty(dataSource)) {
            index = dataSource.data.from - 1;
            totalItems   = dataSource.data.total;
            itemsPerPage = dataSource.data.per_page;
        }
        let nf = new Intl.NumberFormat();
        return ( (!_.isEmpty(dataSource)) &&
            <div>
                <Loading className="loading" />
                <div className="box">
                    <div className="box-header">
                        <div className="row">
                                <div className="col-md-10">
                                    <h4>{trans('messages.search_title')}</h4>
                                </div>
                                <FORM>
                                <div className="row">
                                    <div className="col-md-12">
                                        <INPUT
                                            name="product_code"
                                            groupClass="form-group col-md-1-25"
                                            labelTitle={trans('messages.product_code')}
                                            onChange={this.handleChange}
                                            value={this.state.product_code||''}
                                            className="form-control input-sm" />

                                        <INPUT
                                            name="nanko_num"
                                            groupClass="form-group col-md-1-25"
                                            labelTitle={trans('messages.nanko_num')}
                                            onChange={this.handleChange}
                                            value={this.state.nanko_num||''}
                                            className="form-control input-sm" />
                                        <INPUT
                                            name="glsc_num"
                                            groupClass="form-group col-md-1-25 "
                                            labelTitle={trans('messages.glsc_num')}
                                            onChange={this.handleChange}
                                            value={this.state.glsc_num||''}
                                            className="form-control input-sm" />
                                        <INPUT
                                            name="order_zan_num"
                                            groupClass="form-group col-md-1-5"
                                            labelTitle={trans('messages.order_zan_num')}
                                            onChange={this.handleChange}
                                            value={this.state.order_zan_num||''}
                                            className="form-control input-sm" />

                                        <INPUT
                                            name="rep_orderring_num"
                                            groupClass="form-group col-md-1-25"
                                            labelTitle={trans('messages.rep_orderring_num')}
                                            onChange={this.handleChange}
                                            value={this.state.rep_orderring_num||''}
                                            className="form-control input-sm" />
                                        <INPUT
                                            name="return_num"
                                            groupClass="form-group col-md-1-5"
                                            labelTitle={trans('messages.return_num')}
                                            onChange={this.handleChange}
                                            value={this.state.return_num||''}
                                            className="form-control input-sm" />

                                        <INPUT
                                            name="wait_delivery_num"
                                            groupClass="form-group col-md-1-5"
                                            labelTitle={trans('messages.wait_delivery_num')}
                                            onChange={this.handleChange}
                                            value={this.state.wait_delivery_num||''}
                                            className="form-control input-sm" />
                                        <INPUT
                                            name="free_zan_num"
                                            groupClass="form-group col-md-1-25"
                                            labelTitle={trans('messages.free_zan_num')}
                                            onChange={this.handleChange}
                                            value={this.state.free_zan_num||''}
                                            className="form-control input-sm" />
                                        <SELECT
                                            name="per_page"
                                            groupClass="form-group col-md-1 per-page pull-right-md"
                                            labelClass=""
                                            labelTitle={trans('messages.per_page')}
                                            onChange={this.handleChange}
                                            value={this.state.per_page||''}
                                            options={{20:20, 40:40, 60:60, 80:80, 100:100}}
                                            className="form-control input-sm" />
                                    </div>
                                </div>
                                <div className="row">
                                    <div className="col-md-12">
                                        <SELECT
                                            id="cheetah_status"
                                            name="cheetah_status"
                                            groupClass="form-group col-md-1-5"
                                            labelClass="control-label"
                                            labelTitle={trans('messages.status')}
                                            handleChangeMultiSelect={this.handleChangeMultiSelect}
                                            valueMulti={this.state.cheetah_status||''}
                                            options={dataSource.cheetah_status_opt}
                                            fieldGroupClass = ""
                                            multiple="multiple"
                                            className="form-control input-sm" />
                                    </div>
                                </div>
                                </FORM>
                        </div>
                        <div className="row">
                            <div className="col-md-9 col-xs-4">
                                <BUTTON className="btn btn-primary margin-form" onClick={(e) => this.handleGetData('nanko_glsc')}>{trans('messages.btn_nanko_glsc_diff')}</BUTTON>
                            </div>
                            <div className="col-md-3 col-xs-8">
                                    <BUTTON className="btn btn-danger pull-right" onClick={this.handleClearClick}>{trans('messages.btn_reset')}</BUTTON>
                                    <BUTTON className="btn btn-primary pull-right margin-element" onClick={this.handleReloadData} >{trans('messages.btn_search')}</BUTTON>
                            </div>
                        </div>
                    </div>
                    <div className="box-body">
                        <div className="row">
                            <div className="col-md-12">
                                {(totalItems/itemsPerPage > 1) &&
                                <div className="row" id="panigation">
                                    <div className="col-md-2-5"></div>
                                    <div className="col-md-7 text-center">
                                        <PaginationContainer handleClickPage={this.handleClickPage}/>
                                    </div>
                                    <div className="col-md-2-5 text-right line-height-md">
                                        {dataSource.data.from}件 - {dataSource.data.to}件 【全{dataSource.data.total}件】
                                    </div>
                                </div>
                                }
                                <TABLE className="table table-striped table-custom">
                                    <THEAD>
                                        <TR>
                                            <TH className="visible-xs visible-sm"></TH>
                                            <TH className="hidden-xs hidden-sm text-center">
                                                #
                                            </TH>
                                            <TH className="text-center text-middle col-max-min-100">
                                                <SortComponent
                                                    name="sort_product_code"
                                                    onClick={this.handleSortClick}
                                                    currentSort={(this.state.sort_product_code)?this.state.sort_product_code:'none'}
                                                    title={trans('messages.product_code')} />
                                            </TH>
                                            <TH className="text-center text-middle col-max-min-100">
                                                {trans('messages.free_zan_num')}
                                            </TH>
                                            <TH className="hidden-xs hidden-sm text-center text-middle  col-max-min-120">
                                                <SortComponent
                                                    name="sort_nanko_num"
                                                    onClick={this.handleSortClick}
                                                    currentSort={(this.state.sort_nanko_num)?this.state.sort_nanko_num:'none'}
                                                    title={trans('messages.nanko_num')} />
                                            </TH>
                                            <TH className="hidden-xs hidden-sm text-center text-middle col-max-min-120 ">
                                                <SortComponent
                                                    name="sort_glsc_num"
                                                    onClick={this.handleSortClick}
                                                    currentSort={(this.state.sort_glsc_num)?this.state.sort_glsc_num:'none'}
                                                    title={trans('messages.glsc_num')} />
                                            </TH>
                                            <TH className="hidden-xs hidden-sm text-center text-middle col-max-min-120">
                                                <SortComponent
                                                    name="sort_order_zan_num"
                                                    onClick={this.handleSortClick}
                                                    currentSort={(this.state.sort_order_zan_num)?this.state.sort_order_zan_num:'none'}
                                                    title={trans('messages.order_zan_num')} />
                                            </TH>
                                            <TH className="hidden-xs hidden-sm text-center text-middle col-max-min-120">
                                                <SortComponent
                                                    name="sort_rep_orderring_num"
                                                    onClick={this.handleSortClick}
                                                    currentSort={(this.state.sort_rep_orderring_num)?this.state.sort_rep_orderring_num:'none'}
                                                    title={trans('messages.rep_orderring_num')} />
                                            </TH>
                                            <TH className="hidden-xs hidden-sm text-center  text-middle col-max-min-100">
                                                <SortComponent
                                                    name="sort_return_num"
                                                    onClick={this.handleSortClick}
                                                    currentSort={(this.state.sort_return_num)?this.state.sort_return_num:'none'}
                                                    title={trans('messages.return_num')} />
                                            </TH>
                                            <TH className="hidden-xs hidden-sm text-center  text-middle col-max-min-100">
                                                <SortComponent
                                                    name="sort_wait_delivery_num"
                                                    onClick={this.handleSortClick}
                                                    currentSort={(this.state.sort_wait_delivery_num)?this.state.sort_wait_delivery_num:'none'}
                                                    title={trans('messages.wait_delivery_num')} />
                                            </TH>
                                            <TH className="hidden-xs hidden-sm text-center  text-middle col-max-min-100">
                                                {trans('messages.status')}
                                            </TH>
                                            <TH className="hidden-xs hidden-sm text-center  text-middle col-max-min-100">
                                                <SortComponent
                                                    name="sort_up_date"
                                                    onClick={this.handleSortClick}
                                                    currentSort={(this.state.sort_request_price)?this.state.sort_request_price:'none'}
                                                    title={trans('messages.up_date')} />
                                            </TH>
                                        </TR>
                                    </THEAD>
                                    <TBODY>
                                        {
                                            (!_.isEmpty(dataSource.data.data))?
                                            dataSource.data.data.map((item) => {
                                                index++;
                                                var upDate = moment.utc(item.up_date, "YYYY-MM-DD");
                                                var formatUpDate = upDate.format("YYYY/MM/DD");
                                                var nf = new Intl.NumberFormat();
                                                let classRow = (index%2 ===0) ? 'odd' : 'even';
                                                let free_zan_num = item.nanko_num　-　item.order_zan_num　-　item.return_num;
                                                let cheetahStatus = '';
                                                let cheetahWarning = '';
                                                if (dataSource.cheetah_status[item.cheetah_status]) {
                                                    cheetahStatus = dataSource.cheetah_status[item.cheetah_status];
                                                    if (item.cheetah_status === 3 || item.cheetah_status === 4) {
                                                        cheetahWarning = ' line-error';
                                                    }

                                                }
                                                return (
                                                    [
                                                    <TR key={"tr" + index} className={classRow + cheetahWarning}>
                                                        <TD className="visible-xs visible-sm text-center">
                                                            <b className="show-info">
                                                                <i className="fa fa-angle-double-down" aria-hidden="true"></i>
                                                            </b>
                                                        </TD>
                                                        <TD className="hidden-xs hidden-sm text-center">
                                                           {nf.format(index)}
                                                        </TD>
                                                        <TD className="text-left cut-text" title={item.product_code}>{item.product_code}</TD>
                                                        <TD className="text-right cut-text" title={free_zan_num}>{free_zan_num > 0 ? free_zan_num : 0}</TD>
                                                        <TD className="hidden-xs hidden-sm cut-text text-right" title={item.nanko_num}>{item.nanko_num}</TD>
                                                        <TD className="hidden-xs hidden-sm cut-text text-right" title={item.glsc_num}>{item.glsc_num}</TD>
                                                        <TD className="hidden-xs hidden-sm cut-text text-right" title={item.order_zan_num}>{item.order_zan_num}</TD>
                                                        <TD className="hidden-xs hidden-sm cut-text text-right" title={item.rep_orderring_num}>{item.rep_orderring_num}</TD>
                                                        <TD className="hidden-xs hidden-sm cut-text text-right" title={item.return_num}>{item.return_num}</TD>
                                                        <TD className="hidden-xs hidden-sm cut-text text-right" title={item.wait_delivery_num}>{item.wait_delivery_num}</TD>
                                                        <TD className="hidden-xs hidden-sm cut-text" title={cheetahStatus}>{cheetahStatus}</TD>
                                                        <TD className="hidden-xs hidden-sm cut-text text-center" title={formatUpDate}>{formatUpDate}</TD>
                                                    </TR>,
                                                    <TR key={"trhide" + index} className="hiden-info" style={{display:"none"}}>
                                                        <TD colSpan="10" className="width-span1">
                                                            <ul id={"temp" + index} >
                                                                <li> <b>NO</b> : #{index}</li>
                                                                <li> <b>{trans('messages.product_code')}</b> : {item.product_code}</li>
                                                                <li> <b>{trans('messages.nanko_num')}</b> : {item.nanko_num}</li>
                                                                <li> <b>{trans('messages.glsc_num')}</b> : {item.glsc_num}</li>
                                                                <li> <b>{trans('messages.receive_id')}</b> : {item.receive_id}</li>
                                                                <li> <b>{trans('messages.order_zan_num')}</b> : {item.order_zan_num}</li>
                                                                <li> <b>{trans('messages.rep_orderring_num')}</b> : {item.rep_orderring_num}</li>
                                                                <li> <b>{trans('messages.return_num')}</b> : {item.return_num}</li>
                                                                <li> <b>{trans('messages.wait_delivery_num')}</b> : {item.wait_delivery_num}</li>
                                                                <li> <b>{trans('messages.status')}</b> : {cheetahStatus}</li>
                                                                <li> <b>{trans('messages.up_date')}</b> : {formatUpDate}</li>
                                                            </ul>
                                                        </TD>
                                                    </TR>
                                                   ]
                                                )
                                                if (index === dataSource.data.to) {
                                                   index = 0;
                                                }
                                            })
                                            :
                                            <TR><TD colSpan="10" className="text-center">{trans('messages.no_data')}</TD></TR>
                                        }
                                    </TBODY>
                                </TABLE>
                                <div className="text-center">
                                    <PaginationContainer handleClickPage={this.handleClickPage}/>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        dataSource: state.commonReducer.dataSource,
        dataResponse: state.commonReducer.dataResponse,
        dataResponseCsv: state.commonReducer.dataResponseCsv
    }
}

const mapDispatchToProps = (dispatch) => {
    let ignore = ['attached_file', 'attached_file_path','message','flg', 'process', 'msg', 'timeRun', 'index_key', 'currPage', 'total', 'filename','processBarColor', 'error','percent', 'status_csv', 'url', 'reload', 'params', 'headers', 'count','realFilename','type','fileCorrect', 'checkAfter'];
    return {
        reloadData: (url, params, pushHistory, ignoreFields) => { dispatch(Action.reloadData(url, params, pushHistory, ignore)) },
        postData: (url, params, headers, callback) => { dispatch(Action.postData(url, params, headers, callback)) },
        postDataCSV: (url, params, headers, count_err) => { dispatch(Action.postDataCSV(url, params, headers, count_err)) }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(StockStatusList)