import React, { Component } from 'react';

import { connect } from 'react-redux';
import * as Action from '../common/actions';
import * as queryString from 'query-string';

import BUTTON from '../common/components/form/BUTTON';
import FORM from '../common/components/form/FORM';
import INPUT from '../common/components/form/INPUT';
import DATEPICKER from '../common/components/form/DATEPICKER';
import SELECT from '../common/components/form/SELECT';
import LABEL from '../common/components/form/LABEL';
import moment from 'moment';

import TABLE from '../common/components/table/TABLE';
import THEAD from '../common/components/table/THEAD';
import TBODY from '../common/components/table/TBODY';
import TR from '../common/components/table/TR';
import TH from '../common/components/table/TH';
import TD from '../common/components/table/TD';

import SortComponent from '../common/components/table/SortComponent';
import PaginationContainer from '../common/containers/PaginationContainer'
import Loading from '../common/components/common/Loading';

class StoreOrderList extends Component {

    constructor(props) {
        super(props);
        const stringParams = queryString.parse(props.location.search);
        this.state = {};
        if (!_.isEmpty(stringParams)) {
            this.state = stringParams;
        }
        this.props.reloadData(dataListUrl, this.state);
    }

    handleChange = (event,item) => {
        const target = event.target;
        const value = target.type === 'checkbox' ? target.checked : target.value;
        const name = target.name;
        this.setState({
            [name]: (value)?value:undefined
        },() => {
            if (name === 'per_page') {
                this.handleReloadData();
            }
        });
    }

    handleClearClick = (event) => {
        Object.keys(this.state).map((key) => {
            if (key !== 'per_page') {
                delete this.state[key];
            }
        });
        this.handleReloadData();
    }

    handleReloadData = () => {
        if (this.state.page) {
            delete this.state.page;
        }
        this.props.reloadData(dataListUrl, this.state, true);
    }

    componentDidUpdate() {
        if (!_.isEmpty(this.props.dataSource)) {
            $('.loading').hide();
        }
        dragscroll.reset();
    }

    componentWillMount() {
        window.onpopstate = (event) => {
            location.reload();
        };
    }

    handleSortClick = (name, nextSort) => {
        Object.keys(this.state).map((key) => {
            if (key.startsWith('sort_')) {
                delete this.state[key];
            }
        });
        if (nextSort === 'none') {
            delete this.state[name];
            this.props.reloadData(dataListUrl, this.state, true);
        } else {
            this.setState({[name]: nextSort}, () => {
                this.props.reloadData(dataListUrl, this.state, true);
            });
        }

    }

    handleClickPage = (page) => {
        this.setState({
            page: page
        },() => {
            this.props.reloadData(dataListUrl, this.state, true);
        });
    }

    handleChangeMultiSelect = (name, value, checked) => {
        let currentValue = this.state[name];
        if (typeof currentValue === 'undefined') {
            currentValue = [];
        } else if (!Array.isArray(this.state[name])) {
            currentValue = [this.state[name]];
        }
        if (Array.isArray(value)) {
            currentValue = [...value];
        } else {
            let index = currentValue.indexOf(value);
            if (checked === false) {
                if (index !== -1) {
                    currentValue.splice(index, 1);
                }
            } else {
                if (index === -1) {
                    currentValue.push(value);
                }
            }
        }
        this.setState({
            [name]: currentValue,
        });
    }

    handleChangeDate = (date, name) => {
        let dateValue = moment(date);
        if (!dateValue.isValid()) {
            dateValue = undefined;
        } else {
            dateValue = dateValue.format('YYYY-MM-DD');
        }
        this.setState({
          [name]: dateValue,
        });
    }

    render() {
        let dataSource = this.props.dataSource;
        let nf = new Intl.NumberFormat();
        let index = 0;
        let totalItems = 0;
        let itemsPerPage = 20;
        if (!_.isEmpty(dataSource)) {
            index = dataSource.data.from - 1;
            totalItems   = dataSource.data.total;
            itemsPerPage = dataSource.data.per_page;
        }
        return ( (!_.isEmpty(dataSource)) &&
            <div>
                <Loading className="loading" />
                <div className="box">
                    <div className="box-header">
                        <div className="row">
                                <div className="col-md-12">
                                    <h4>{trans('messages.search_title')}</h4>
                                </div>
                                <FORM>
                                    <INPUT
                                        name="order_id_from"
                                        groupClass="form-group col-md-1-5"
                                        labelTitle={trans('messages.order_id_from')}
                                        onChange={this.handleChange}
                                        value={this.state.order_id_from || ''}
                                        className="form-control input-sm" />
                                    <INPUT
                                        name="order_id_to"
                                        groupClass="form-group col-md-1-5"
                                        labelTitle={trans('messages.order_id_to')}
                                        onChange={this.handleChange}
                                        value={this.state.order_id_to || ''}
                                        className="form-control input-sm" />
                                    <SELECT
                                        id="storage_store_id"
                                        name="storage_store_id"
                                        groupClass="form-group col-md-1-75"
                                        labelClass="pull-left"
                                        labelTitle={trans('messages.storage_store_id')}
                                        handleChangeMultiSelect={this.handleChangeMultiSelect}
                                        multiple="multiple"
                                        valueMulti={this.state.storage_store_id||''}
                                        options={dataSource.storeOpt}
                                        className="form-control input-sm" />
                                    <SELECT
                                        id="status"
                                        name="status"
                                        groupClass="form-group col-md-1-75"
                                        labelClass="pull-left"
                                        labelTitle={trans('messages.store_order_status')}
                                        handleChangeMultiSelect={this.handleChangeMultiSelect}
                                        multiple="multiple"
                                        valueMulti={this.state.status||''}
                                        options={dataSource.statusOpt}
                                        className="form-control input-sm" />
                                    <DATEPICKER
                                        placeholderText="YYYY-MM-DD"
                                        dateFormat="YYYY-MM-DD"
                                        locale="ja"
                                        groupClass="form-group col-md-1-75"
                                        labelClass="pull-left"
                                        name="delivery_plan_date_from"
                                        className="form-control input-sm"
                                        labelTitle={trans('messages.delivery_plan_date_from')}
                                        id="delivery_plan_date_from"
                                        selected={this.state.delivery_plan_date_from ? moment(this.state.delivery_plan_date_from) : null}
                                        onChange={(date, name) => this.handleChangeDate(date, 'delivery_plan_date_from')} />
                                    <DATEPICKER
                                        placeholderText="YYYY-MM-DD"
                                        dateFormat="YYYY-MM-DD"
                                        locale="ja"
                                        groupClass="form-group col-md-1-75"
                                        labelClass="pull-left"
                                        name="delivery_plan_date_to"
                                        className="form-control input-sm"
                                        labelTitle={trans('messages.delivery_plan_date_to')}
                                        id="delivery_plan_date_to"
                                        selected={this.state.delivery_plan_date_to ? moment(this.state.delivery_plan_date_to) : null}
                                        onChange={(date, name) => this.handleChangeDate(date, 'delivery_plan_date_to')} />
                                    <SELECT
                                        name="per_page"
                                        groupClass="form-group col-md-1 per-page pull-right-md"
                                        labelClass="pull-left"
                                        labelTitle={trans('messages.per_page')}
                                        onChange={this.handleChange}
                                        value={this.state.per_page||''}
                                        options={{20:20, 30:30, 50:50}}
                                        className="form-control input-sm" />
                                </FORM>
                        </div>
                        <div className="row">
                            <div className="col-md-9"></div>
                            <div className="col-md-3 col-xs-8 pull-right">
                                <BUTTON className="btn btn-danger pull-right" onClick={this.handleClearClick}>{trans('messages.btn_reset')}</BUTTON>
                                <BUTTON className="btn btn-primary pull-right margin-element" onClick={this.handleReloadData} >{trans('messages.btn_search')}</BUTTON>
                            </div>
                        </div>
                    </div>
                    <div className="box-body">
                        <div className="row">
                            <div className="col-md-12">
                                {(totalItems/itemsPerPage > 1) &&
                                <div className="row">
                                    <div className="col-md-2-5"></div>
                                    <div className="col-md-7 text-center">
                                        <PaginationContainer handleClickPage={this.handleClickPage}/>
                                    </div>
                                    <div className="col-md-2-5 text-right line-height-md">
                                        {dataSource.data.from}件 - {dataSource.data.to}件 【全{dataSource.data.total}件】
                                    </div>
                                </div>
                                }
                                <div className="table-responsive">
                                <TABLE className="table table-striped table-custom">
                                    <THEAD>
                                        <TR>
                                            <TH className="visible-xs visible-sm"></TH>
                                            <TH className="text-center text-middle">
                                                {trans('messages.index')}
                                            </TH>
                                            <TH className="col-max-min-130 text-center text-middle">
                                                <SortComponent
                                                    name="sort_order_id"
                                                    onClick={this.handleSortClick}
                                                    currentSort={(this.state.sort_order_id)?this.state.sort_order_id:'none'}
                                                    title={trans('messages.order_id')} />
                                            </TH>
                                            <TH className="col-max-min-100 text-center text-middle hidden-xs hidden-sm" >
                                                {trans('messages.store_order_inquiry_no')}
                                            </TH>
                                            <TH className="col-max-min-300 text-center text-middle">
                                                {trans('messages.storage_store_id')}
                                            </TH>
                                            <TH className="col-max-min-90 text-center text-middle hidden-xs hidden-sm" >
                                                <SortComponent
                                                name="sort_order_date"
                                                onClick={this.handleSortClick}
                                                currentSort={(this.state.sort_order_date)?this.state.sort_order_date:'none'}
                                                title={trans('messages.store_order_order_date')} />
                                            </TH>
                                            <TH className="col-max-min-100 text-center text-middle hidden-xs hidden-sm" >
                                                {trans('messages.store_order_status')}
                                            </TH>
                                            <TH className="col-max-min-200 text-center text-middle hidden-xs hidden-sm" >
                                                {trans('messages.error_message')}
                                            </TH>
                                            <TH className="col-max-min-120 text-center text-middle hidden-xs hidden-sm" >
                                                <SortComponent
                                                name="sort_delivery_plan_date"
                                                onClick={this.handleSortClick}
                                                currentSort={(this.state.sort_delivery_plan_date)?this.state.sort_delivery_plan_date:'none'}
                                                title={trans('messages.delivery_plan_date')} />
                                            </TH>
                                            <TH className="col-max-min-100 text-center text-middle hidden-xs hidden-sm" >
                                                <SortComponent
                                                name="sort_delivery_date"
                                                onClick={this.handleSortClick}
                                                currentSort={(this.state.sort_delivery_date)?this.state.sort_delivery_date:'none'}
                                                title={trans('messages.delivery_date')} />
                                            </TH>
                                            <TH className="col-max-min-120 text-center text-middle hidden-xs hidden-sm" >
                                                <SortComponent
                                                name="sort_received_date"
                                                onClick={this.handleSortClick}
                                                currentSort={(this.state.sort_received_date)?this.state.sort_received_date:'none'}
                                                title={trans('messages.received_date')} />
                                            </TH>
                                        </TR>

                                    </THEAD>
                                    <TBODY>
                                    {
                                        (!_.isEmpty(dataSource.data.data))?
                                        dataSource.data.data.map((item) => {
                                            index++;
                                            let classRow = (index%2 ===0) ? 'odd' : 'even';
                                            let orderDate = !_.isEmpty(item.order_date) ? moment.utc(item.order_date, "YYYY-MM-DD hh:mm:ss").format("YYYY/MM/DD") : '';
                                            let deliveryPlanDate = !_.isEmpty(item.delivery_plan_date) ? moment.utc(item.delivery_plan_date, "YYYY-MM-DD hh:mm:ss").format("YYYY/MM/DD") : '';
                                            let deliveryDate = !_.isEmpty(item.delivery_date) ? moment.utc(item.delivery_date, "YYYY-MM-DD hh:mm:ss").format("YYYY/MM/DD") : '';
                                            let receivedDate = !_.isEmpty(item.received_date) ? moment.utc(item.received_date, "YYYY-MM-DD hh:mm:ss").format("YYYY/MM/DD") : '';
                                            let status = '';
                                            dataSource.statusOpt.map((entry) => {
                                                if (entry.key === item.status) {
                                                    status = entry.value;
                                                    return;
                                                }
                                            });
                                            let errorMessage = '';
                                            if (item.status === 6) {
                                                errorMessage = item.error_message;
                                            }
                                            return (
                                                [
                                                <TR key={"tr" + index} className={classRow}>
                                                    <TD className="visible-xs visible-sm">
                                                        <b className="show-info">
                                                            <i className="fa fa-angle-double-down" aria-hidden="true"></i>
                                                        </b>
                                                    </TD>
                                                    <TD className="text-center">
                                                       {index}
                                                    </TD>
                                                    <TD className="text-center" title={item.order_id}>
                                                        <a className="link-detail" href={detailUrl + '?order_id=' + item.order_id}>{item.order_id}</a>
                                                    </TD>
                                                    <TD className="text-right hidden-xs hidden-sm"
                                                        title={item.inquiry_no}>{item.inquiry_no}
                                                    </TD>
                                                    <TD className="text-long-batch"
                                                        title={item.store_name}>{item.store_name}
                                                    </TD>
                                                    <TD className="hidden-xs hidden-sm" title={orderDate}>
                                                        {orderDate}
                                                    </TD>
                                                    <TD className="hidden-xs hidden-sm text-center"
                                                        title={status}>{status}
                                                    </TD>
                                                    <TD className="hidden-xs hidden-sm"
                                                        title={errorMessage}>{errorMessage}
                                                    </TD>
                                                    <TD className="hidden-xs hidden-sm"
                                                        title={deliveryPlanDate}>{deliveryPlanDate}
                                                    </TD>
                                                    <TD className="hidden-xs hidden-sm"
                                                        title={deliveryDate}>{deliveryDate}
                                                    </TD>
                                                    <TD className="hidden-xs hidden-sm"
                                                        title={receivedDate}>{receivedDate}
                                                    </TD>
                                                </TR>,
                                                <TR key={"trhide" + index} className="hiden-info" style={{display:"none"}}>
                                                    <TD colSpan="10" className="width-span1">
                                                        <ul id={"temp" + item.mail_id} >
                                                            <li> <b>NO</b> : #{index}</li>
                                                            <li> <b>{trans('messages.order_id')}</b> : {item.order_id}</li>
                                                            <li> <b>{trans('messages.store_order_inquiry_no')}</b> : {item.inquiry_no}</li>
                                                            <li> <b>{trans('messages.storage_store_id')}</b> : {item.store_name}</li>
                                                            <li> <b>{trans('messages.store_order_order_date')}</b> : {orderDate}</li>
                                                            <li> <b>{trans('messages.store_order_status')}</b> : {status}</li>
                                                            <li> <b>{trans('messages.error_message')}</b> : {errorMessage}</li>
                                                            <li> <b>{trans('messages.delivery_plan_date')}</b> : {deliveryPlanDate}</li>
                                                            <li> <b>{trans('messages.delivery_date')}</b> : {deliveryDate}</li>
                                                            <li> <b>{trans('messages.received_date')}</b> : {receivedDate}</li>
                                                        </ul>
                                                    </TD>
                                                </TR>
                                               ]
                                            )
                                        })
                                        :
                                        <TR><TD colSpan="17" className="text-center">{trans('messages.no_data')}</TD></TR>
                                    }
                                    </TBODY>
                                </TABLE>
                                </div>
                                <div className="text-center">
                                    <PaginationContainer handleClickPage={this.handleClickPage}/>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        dataSource: state.commonReducer.dataSource,
        dataResponse: state.commonReducer.dataResponse
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        reloadData: (url, params, pushHistory, ignoreFields) => { dispatch(Action.reloadData(url, params, pushHistory, ignoreFields)) },
        postData: (url, params, headers, callBackUrl, callBackParams) => { dispatch(Action.postData(url, params, headers, callBackUrl, callBackParams)) }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(StoreOrderList)