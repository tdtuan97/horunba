import React, { Component } from 'react';
import { connect } from 'react-redux';

import * as Action from '../common/actions';
import * as queryString from 'query-string';

import Loading from '../common/components/common/Loading';
import FORM from '../common/components/form/FORM';
import INPUT from '../common/components/form/INPUT';
import SELECT from '../common/components/form/SELECT';
import BUTTON from '../common/components/form/BUTTON';
import CHECKBOX_CUSTOM from '../common/components/form/CHECKBOX_CUSTOM';

import TABLE from '../common/components/table/TABLE';
import THEAD from '../common/components/table/THEAD';
import TBODY from '../common/components/table/TBODY';
import TR from '../common/components/table/TR';
import TH from '../common/components/table/TH';
import TD from '../common/components/table/TD';
import SortComponent from '../common/components/table/SortComponent';
import axios from 'axios';
let timer;
class RefundDetailSave extends Component {

    constructor(props) {
        super(props);
        this.state = {}
        this.state.newDataOrder = [];
        this.state.dataOrder = [];
        this.state.dataCustomer = [];
        this.state.typeSubmit = 'btnReturn';
        let params = queryString.parse(props.location.search);
        if (params['received_order_id']) {
            this.state.origin_receive_order_id = params['received_order_id'];
            this.getInfoProduct();
        }
         this.props.reloadData(formDataUrl, this.state)
    }

    componentWillReceiveProps(nextProps) {
        let dataSource = nextProps.dataSource;

        if (!_.isEmpty(dataSource.data)) {
            this.setState(dataSource.data);
        }
        if (!_.isEmpty(nextProps.dataResponse.method) && nextProps.dataResponse.method === 'save') {
            if (nextProps.dataResponse.status === 1) {
                window.location.href = detailUrl+"?receive_id_key="+nextProps.dataResponse.receive_id;
            } else {
                document.getElementsByName(this.state.typeSubmit)[0].disabled = false;
            }
        }
    }

    handleChange = (event) => {
        const target = event.target;
        const name = target.name;
        const value = target.type === 'checkbox' ? target.checked : target.value;
        this.setState({
            [name]: value
        });
        if (name === 'origin_receive_order_id') {
            clearTimeout(timer);
            let _this = this;
            timer = setTimeout(function() {
                _this.getInfoProduct();
            }, 1000);
        }

    }
    handleProcessNewOrder = (item,keyValue,event) => {
        const target = event.target;
        const name = target.name;
        const value = target.type === 'checkbox' ? target.checked : target.value;

        if (value === true) {
            this.setState({
                newDataOrder : [...this.state.newDataOrder,item]
            });
        } else {
        var array = this.state.newDataOrder;
        const index = array.findIndex(item2 => item2.product_code === item.product_code
                                    && item2.detail_line_num === item.detail_line_num
                                    && item2.sub_line_num === item.sub_line_num
                                    );
            this.setState((prevState) => ({
                newDataOrder: [
                    ...prevState.newDataOrder.slice(0,index),
                    ...prevState.newDataOrder.slice(index+1)
                ]
            }));
        }
    }
    handleChangeNewOrderQuantity = (index, event) => {
        const target = event.target;
        const name = target.name;
        const value = target.type === 'checkbox' ? target.checked : target.value;
        let currentNewDataOrder = [...this.state.newDataOrder];
        currentNewDataOrder[index][name] = value;
        this.setState({
            newDataOrder: currentNewDataOrder
        });
    }
    getInfoProduct = () => {
        $(".loading").show();

        let headers = {'X-Requested-With': 'XMLHttpRequest'};

        axios.post(getInfoOrderUrl, this.state, {headers : headers}
        ).then(response => {
            $(".loading").hide();
            let data = response.data;
            this.setState({
                newDataOrder: {},dataOrder : {}
            });
            this.setState({
                dataOrder   : data.dataOrder,
                dataCustomer : data.dataCustomer,
            });
        }).catch(error => {
            $(".loading").hide();
            throw(error);
        });
    }
    handleCancel = (event) => {
        window.location.href = dashboardUrl;
    }

    handleSubmit = (event) => {
        document.getElementsByName(event.target.name)[0].disabled = true;
        this.state.typeSubmit = event.target.name;
        let headers = {'X-Requested-With': 'XMLHttpRequest'};
        if (this.props.enctype) {
            headers['Content-Type'] = this.props.enctype;
        }
        this.props.postData(this.props.location.pathname, this.state, headers);
    }

    componentDidUpdate() {
        if (!_.isEmpty(this.props.dataSource)) {
            $('.loading').hide();
        }
    }

    render() {
        let dataSource   = this.props.dataSource;
        let dataResponse = this.props.dataResponse;
        let statusOptions= {0:'-----',
                            1:'返品希望',
                            2:'不足分再送',
                            3:'再送付',
                            4:'商品配送間違い',
                            5:'配送中破損',
                            6:'商品返戻待ち',
                            7:'商品配送間違い',
                            8:'過剰発送'};
        let index = 0;
        let i = 0;
        let return_total_price = 0;
        var nf = new Intl.NumberFormat();
        return ((!_.isEmpty(dataSource)) &&
            <div className="row">
            <div className="col-md-12">
            <div className="box box-primary">
            <FORM className="form-horizontal ">
                <Loading className="loading"></Loading>
                <div className="box-header with-border">
                <div className="row">
                    <div className="col-md-3">
                    <SELECT
                        name="status"
                        groupClass=""
                        className="form-control input-sm"
                        fieldGroupClass="col-md-8"
                        labelClass="col-md-2-75"
                        labelTitle={trans('messages.status')}
                        onChange={this.handleChange}
                        value={this.state.status||''}
                        options={statusOptions}
                        errorPopup={true}
                        defaultValue={dataSource.status}
                        hasError={dataResponse.message&&dataResponse.message.status?true:false}
                        errorMessage={dataResponse.message&&dataResponse.message.status?dataResponse.message.status:"" }
                        />
                    </div>
                    <div className="col-md-3">
                    <INPUT
                        name="order_person_name"
                        labelTitle={trans('messages.order_person_name')}
                        className="form-control input-sm"
                        fieldGroupClass="col-md-8"
                        labelClass="col-md-4"
                        groupClass=""
                        errorPopup={true}
                        readOnly
                        value={
                            !_.isEmpty(this.state.dataCustomer)?
                            this.state.dataCustomer['last_name']:'' || ''
                        }
                        hasError={dataResponse.message&&dataResponse.message.order_person_name?true:false}
                        errorMessage={dataResponse.message&&dataResponse.message.order_person_name?dataResponse.message.order_person_name:""}
                        />
                    </div>
                    <div className="col-md-6">
                    <INPUT
                        name="origin_receive_order_id"
                        labelTitle={trans('messages.origin_receive_order_id')}
                        className="form-control input-sm"
                        fieldGroupClass="col-md-8"
                        labelClass="col-md-2-5"
                        groupClass=""
                        errorPopup={true}
                        onChange={this.handleChange}
                        value={this.state.origin_receive_order_id ||''}
                        hasError={dataResponse.message&&dataResponse.message.origin_receive_order_id?true:false}
                        errorMessage={dataResponse.message&&dataResponse.message.origin_receive_order_id?dataResponse.message.origin_receive_order_id:""}
                        />
                    </div>
                </div>
                </div>

                <div className="box-body">
                    <div className="row">
                        <div className="col-md-6">
                        <div className='table-responsive'>
                            <TABLE className="table table-striped table-custom">
                                <THEAD>
                                    <TR>

                                        <TH className="text-center text-middle" width="40%">
                                            {trans('messages.product_code_ots')}
                                        </TH>
                                        <TH className="text-center text-middle" width="20%" >
                                            {trans('messages.sale_price')}
                                        </TH >
                                        <TH className="text-center text-middle" width="20%">
                                            {trans('messages.quantity')}
                                        </TH>
                                        <TH className="text-center text-middle"  width="20%">
                                            {trans('messages.total_sub')}
                                        </TH>
                                    </TR>
                                </THEAD>
                                    <TBODY>
                                        {
                                            (!_.isEmpty(this.state.newDataOrder))?

                                            [this.state.newDataOrder.map((item, key) => {
                                                index++;
                                                return_total_price += (this.state.newDataOrder[key]['quantity']*item.price);
                                                let classRow = (index%2 ===0) ? 'odd' : 'even';
                                                return (
                                                    [
                                                    <TR key={"tr2" + index} className={classRow}>
                                                        <TD className="cut-text text-left" title={item.product_code}>
                                                            <span className="limit-char ">{item.product_code}</span>
                                                        </TD>
                                                        <TD className="cut-text text-right" title={item.price}>
                                                            <span className="limit-char ">{nf.format(item.price)}</span>
                                                        </TD>
                                                        <TD className="cut-text text-right"  title={item.quantity}>
                                                            <INPUT
                                                            name="quantity"
                                                            className="form-control input-sm text-center"
                                                            fieldGroupClass="a"
                                                            groupClass="a"
                                                            maxLength={12}
                                                            minLength={1}
                                                            type='number'
                                                            errorPopup={true}
                                                            onChange={(e) => this.handleChangeNewOrderQuantity(key, e)}
                                                            value={this.state.newDataOrder[key]['quantity'] ||''}
                                                            hasError={dataResponse.message&&
                                                                      dataResponse.message.quantity?
                                                                      true:
                                                                      false
                                                                    }
                                                            errorMessage={dataResponse.message&&
                                                                          dataResponse.message.quantity?
                                                                          dataResponse.message.quantity:
                                                                          ""}
                                                            />
                                                        </TD>
                                                        <TD className="cut-text text-right" title={item.quantity}>
                                                            <span className="limit-char max-width-100">
                                                                    {(
                                                                        nf.format(this.state.newDataOrder[key]['quantity']*item.price)
                                                                    )}
                                                            </span>
                                                        </TD>
                                                    </TR>
                                                   ]
                                                )

                                            })
                                            ,
                                            <TR key="row2">
                                                <TD colSpan="2" className="text-center"></TD>
                                                <TD className="text-right"><b>{trans('messages.total_price')}</b></TD>
                                                <TD className="text-right max-width-100"><b><span className='limit-char max-width-100'>{nf.format(return_total_price)}</span></b></TD>

                                            </TR>
                                            ]
                                            :
                                            <TR><TD colSpan="5" className="text-center">{trans('messages.no_data')}</TD></TR>
                                        }
                                    </TBODY>
                            </TABLE>
                            </div>
                        </div>
                        <div className="col-md-6">
                        <div className="table-responsive">
                            <TABLE className="table table-striped table-custom">
                                    <TBODY>
                                        {
                                            (!_.isEmpty(this.state.dataOrder))?
                                            this.state.dataOrder.map((result) => {
                                                i++;
                                                let classRow = (i%2 ===0) ? 'odd' : 'even';
                                                let keyValue = 'key-' + i;
                                                if (result.err_text === '') {
                                                return (
                                                    [
                                                    <TR key={"tr" + i} className={classRow} >
                                                        <TD className="text-left" width='4%'>
                                                        <CHECKBOX_CUSTOM
                                                        key={'product_status' + i}
                                                        name={'product_status_' + i }
                                                        onClick={(e) => this.handleProcessNewOrder({...result},keyValue,e)}
                                                        value={1} />
                                                        </TD>
                                                        <TD className="cut-text text-left"  width='25%' title={result.product_code}>
                                                            <span className="limit-char ">{result.product_code}</span>
                                                        </TD>
                                                        <TD className="cut-text text-right"  width='45%'title={result.product_name}>
                                                            <span className="">{result.product_name}</span>
                                                        </TD>
                                                        <TD className="cut-text text-right" width='15%' title={result.price}>
                                                            <span className="limit-char ">{result.price}</span>
                                                        </TD>
                                                        <TD className="cut-text text-right" width='10%' title={result.quantity}>
                                                            <span className="limit-char max-width-70 ">{result.quantity}</span>
                                                        </TD>
                                                    </TR>
                                                   ]
                                                )} else {
                                                  return <TR key={'er-' + i}>
                                                            <TD className="text-left">{result.product_code}</TD>
                                                            <TD colSpan="4" className="text-left">{result.err_text}</TD>
                                                        </TR>
                                                }
                                            })
                                            :
                                            <TR className="pull-right">
                                               <TD style={{border:"none"}}><BUTTON name="btnCancel" className="btn btn-primary input-sm margin" type="button" onClick={this.handleCancel}>{"キャンセル"}</BUTTON></TD>
                                            </TR>
                                        }
                                    </TBODY>
                            </TABLE>
                        </div>
                        </div>
                    </div>
                    {!_.isEmpty(this.state.newDataOrder)
                        ?
                    <div className="col-md-6 col-offset-md-6 pull-right">
                        <BUTTON name="btnNotEnough" className="btn btn-primary input-sm margin" type="button" onClick={this.handleSubmit}>{"不足品"}</BUTTON>
                        <BUTTON name="btnResend" className="btn btn-primary input-sm margin" type="button" onClick={this.handleSubmit}>{"再送付"}</BUTTON>
                        <BUTTON name="btnReorder" className="btn btn-primary input-sm margin" type="button" onClick={this.handleSubmit}>{"再注文"}</BUTTON>
                        <BUTTON name="btnCancel" className="btn btn-primary input-sm margin" type="button" onClick={this.handleCancel}>{"キャンセル"}</BUTTON>
                    </div>
                    :
                        '' }
                    </div>
                </FORM>
                </div>
            </div>
            </div>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        dataSource: state.commonReducer.dataSource,
        dataResponse: state.commonReducer.dataResponse
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        reloadData: (url, params) => { dispatch(Action.detailData(url, params)) },
        postData: (url, params, headers) => { dispatch(Action.postData(url, params, headers)) }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(RefundDetailSave)