import React, { Component } from 'react';

import { connect } from 'react-redux';
import * as Action from '../common/actions';
import * as queryString from 'query-string';

import BUTTON from '../common/components/form/BUTTON';
import FORM from '../common/components/form/FORM';
import INPUT from '../common/components/form/INPUT';
import DATEPICKER from '../common/components/form/DATEPICKER';
import SELECT from '../common/components/form/SELECT';
import LABEL from '../common/components/form/LABEL';
import moment from 'moment';

import TABLE from '../common/components/table/TABLE';
import THEAD from '../common/components/table/THEAD';
import TBODY from '../common/components/table/TBODY';
import TR from '../common/components/table/TR';
import TH from '../common/components/table/TH';
import TD from '../common/components/table/TD';

import SortComponent from '../common/components/table/SortComponent';
import PaginationContainer from '../common/containers/PaginationContainer'
import Loading from '../common/components/common/Loading';

class DeliveryList extends Component {

    constructor(props) {
        super(props);
        const stringParams = queryString.parse(props.location.search);
        this.state = {};
        if (!_.isEmpty(stringParams)) {
            this.state = stringParams;
        }
        this.props.reloadData(dataListUrl, this.state);
    }

    handleChange = (event,item) => {
        const target = event.target;
        const value = target.type === 'checkbox' ? target.checked : target.value;
        const name = target.name;
        this.setState({
            [name]: (value)?value:undefined
        },() => {
            if (name === 'per_page') {
                this.handleReloadData();
            }
        });
    }

    handleClearClick = (event) => {
        Object.keys(this.state).map((key) => {
            if (key !== 'per_page') {
                delete this.state[key];
            }
        });
        this.handleReloadData();
    }

    handleReloadData = () => {
        if (this.state.page) {
            delete this.state.page;
        }
        this.props.reloadData(dataListUrl, this.state, true);
    }

    componentDidUpdate() {
        if (!_.isEmpty(this.props.dataSource)) {
            $('.loading').hide();
            $('.show-modal-content').customPopover();
        }
        dragscroll.reset();
    }

    componentWillMount() {
        window.onpopstate = (event) => {
            location.reload();
        };
    }

    handleSortClick = (name, nextSort) => {
        Object.keys(this.state).map((key) => {
            if (key.startsWith('sort_')) {
                delete this.state[key];
            }
        });
        if (nextSort === 'none') {
            delete this.state[name];
            this.props.reloadData(dataListUrl, this.state, true);
        } else {
            this.setState({[name]: nextSort}, () => {
                this.props.reloadData(dataListUrl, this.state, true);
            });
        }

    }

    handleClickPage = (page) => {
        this.setState({
            page: page
        },() => {
            this.props.reloadData(dataListUrl, this.state, true);
        });
    }

    handleChangeMultiSelect = (name, value, checked) => {
        let currentValue = this.state[name];
        if (typeof currentValue === 'undefined') {
            currentValue = [];
        } else if (!Array.isArray(this.state[name])) {
            currentValue = [this.state[name]];
        }
        if (Array.isArray(value)) {
            currentValue = [...value];
        } else {
            let index = currentValue.indexOf(value);
            if (checked === false) {
                if (index !== -1) {
                    currentValue.splice(index, 1);
                }
            } else {
                if (index === -1) {
                    currentValue.push(value);
                }
            }
        }
        this.setState({
            [name]: currentValue,
        });
    }

    handleChangeDate = (date, name) => {
        let dateValue = moment(date);
        if (!dateValue.isValid()) {
            dateValue = undefined;
        } else {
            dateValue = dateValue.format('YYYY-MM-DD');
        }
        this.setState({
          [name]: dateValue,
        });
    }

    render() {
        let dataSource = this.props.dataSource;
        let nf = new Intl.NumberFormat();
        let index = 0;
        let totalItems = 0;
        let itemsPerPage = 20;
        let mallOptions = {};
        if (!_.isEmpty(dataSource)) {
            index = dataSource.data.from - 1;
            totalItems   = dataSource.data.total;
            itemsPerPage = dataSource.data.per_page;
            dataSource.mall_data.map((value) => {
               mallOptions[value.id] = value.name_jp;
            });
        }
        return ( (!_.isEmpty(dataSource)) &&
            <div>
                <Loading className="loading" />
                <div className="box">
                    <div className="box-header">
                        <div className="row">
                                <div className="col-md-12">
                                    <h4>{trans('messages.search_title')}</h4>
                                </div>
                                <FORM>
                                    <SELECT
                                        id="name_jp"
                                        name="name_jp"
                                        groupClass="form-group col-md-1-5"
                                        labelClass=""
                                        labelTitle={trans('messages.name_jp')}
                                        handleChangeMultiSelect={this.handleChangeMultiSelect}
                                        valueMulti={this.state.name_jp||''}
                                        options={mallOptions}
                                        multiple="multiple"
                                        className="form-control input-sm" />
                                    <INPUT
                                        name="received_order_id"
                                        groupClass="form-group col-md-1-5"
                                        labelTitle={trans('messages.receive_order_id')}
                                        onChange={this.handleChange}
                                        value={this.state.received_order_id || ''}
                                        className="form-control input-sm" />
                                    <DATEPICKER
                                        placeholderText="YYYY-MM-DD"
                                        dateFormat="YYYY-MM-DD"
                                        locale="ja"
                                        groupClass="form-group col-md-1-5"
                                        labelClass="pull-left"
                                        name="delivery_date"
                                        className="form-control input-sm"
                                        labelTitle={trans('messages.lbl_delivery_date')}
                                        id="delivery_date"
                                        selected={this.state.delivery_date ? moment(this.state.delivery_date) : null}
                                        onChange={(date, name) => this.handleChangeDate(date, 'delivery_date')} />
                                    <DATEPICKER
                                        placeholderText="YYYY-MM-DD"
                                        dateFormat="YYYY-MM-DD"
                                        locale="ja"
                                        groupClass="form-group col-md-1-75"
                                        labelClass="pull-left"
                                        name="delivery_plan_date_from"
                                        className="form-control input-sm"
                                        labelTitle={trans('messages.delivery_plan_date_from')}
                                        id="delivery_plan_date_from"
                                        selected={this.state.delivery_plan_date_from ? moment(this.state.delivery_plan_date_from) : null}
                                        onChange={(date, name) => this.handleChangeDate(date, 'delivery_plan_date_from')} />
                                    <DATEPICKER
                                        placeholderText="YYYY-MM-DD"
                                        dateFormat="YYYY-MM-DD"
                                        locale="ja"
                                        groupClass="form-group col-md-1-5"
                                        labelClass="pull-left"
                                        name="delivery_plan_date_to"
                                        className="form-control input-sm"
                                        labelTitle={trans('messages.delivery_plan_date_to')}
                                        id="delivery_plan_date_to"
                                        selected={this.state.delivery_plan_date_to ? moment(this.state.delivery_plan_date_to) : null}
                                        onChange={(date, name) => this.handleChangeDate(date, 'delivery_plan_date_to')} />
                                    <INPUT
                                        name="error_message"
                                        groupClass="form-group col-md-1-5"
                                        labelTitle={trans('messages.lbl_error_message')}
                                        onChange={this.handleChange}
                                        value={this.state.error_message || ''}
                                        className="form-control input-sm" />
                                    <SELECT
                                        id="delivery_status"
                                        name="delivery_status"
                                        groupClass="form-group col-md-1-5"
                                        labelClass="pull-left"
                                        labelTitle={trans('messages.lbl_delivery_status')}
                                        handleChangeMultiSelect={this.handleChangeMultiSelect}
                                        multiple="multiple"
                                        valueMulti={this.state.delivery_status||''}
                                        options={dataSource.statusOpt}
                                        className="form-control input-sm" />

                                    <SELECT
                                        name="per_page"
                                        groupClass="form-group col-md-1 per-page pull-right-md"
                                        labelClass="pull-left"
                                        labelTitle={trans('messages.per_page')}
                                        onChange={this.handleChange}
                                        value={this.state.per_page||''}
                                        options={{20:20, 30:30, 50:50}}
                                        className="form-control input-sm" />
                                </FORM>
                        </div>
                        <div className="row">
                            <div className="col-md-9"></div>
                            <div className="col-md-3 col-xs-8 pull-right">
                                <BUTTON className="btn btn-danger pull-right" onClick={this.handleClearClick}>{trans('messages.btn_reset')}</BUTTON>
                                <BUTTON className="btn btn-primary pull-right margin-element" onClick={this.handleReloadData} >{trans('messages.btn_search')}</BUTTON>
                            </div>
                        </div>
                    </div>
                    <div className="box-body">
                        <div className="row">
                            <div className="col-md-12">
                                {(totalItems/itemsPerPage > 1) &&
                                <div className="row">
                                    <div className="col-md-2-5"></div>
                                    <div className="col-md-7 text-center">
                                        <PaginationContainer handleClickPage={this.handleClickPage}/>
                                    </div>
                                    <div className="col-md-2-5 text-right line-height-md">
                                        {dataSource.data.from}件 - {dataSource.data.to}件 【全{dataSource.data.total}件】
                                    </div>
                                </div>
                                }
                                <div className="table-responsive">
                                <TABLE className="table table-striped table-custom">
                                    <THEAD>
                                        <TR>
                                            <TH className="text-center text-middle">
                                                {trans('messages.index')}
                                            </TH>
                                            <TH className="col-max-min-130 text-left text-middle">
                                                <SortComponent
                                                    name="sort_name_jp"
                                                    onClick={this.handleSortClick}
                                                    currentSort={(this.state.sort_name_jp)?this.state.sort_name_jp:'none'}
                                                    title={trans('messages.name_jp')} />
                                            </TH>
                                            <TH className="col-max-min-130 text-left text-middle">
                                                <SortComponent
                                                    name="sort_received_order_id"
                                                    onClick={this.handleSortClick}
                                                    currentSort={(this.state.sort_received_order_id)?this.state.sort_received_order_id:'none'}
                                                    title={trans('messages.receive_order_id')} />
                                            </TH>
                                            <TH className="col-max-min-100 text-center text-middle" >
                                                <SortComponent
                                                    name="sort_subdivision_num"
                                                    onClick={this.handleSortClick}
                                                    currentSort={(this.state.sort_subdivision_num)?this.state.sort_subdivision_num:'none'}
                                                    title={trans('messages.lbl_subdivision_num')} />
                                            </TH>
                                            <TH className="col-max-min-130 text-left text-middle">
                                                <SortComponent
                                                    name="company_name"
                                                    onClick={this.handleSortClick}
                                                    currentSort={(this.state.company_name)?this.state.company_name:'none'}
                                                    title={trans('messages.lbl_company_name')} />
                                            </TH>
                                            <TH className="col-max-min-100 text-center text-middle">
                                                <SortComponent
                                                    name="sort_inquiry_no"
                                                    onClick={this.handleSortClick}
                                                    currentSort={(this.state.sort_inquiry_no)?this.state.sort_inquiry_no:'none'}
                                                    title={trans('messages.lbl_inquiry_no')} />
                                            </TH>
                                            <TH className="col-max-min-100 text-center text-middle">
                                                <SortComponent
                                                    name="sort_delivery_plan_date"
                                                    onClick={this.handleSortClick}
                                                    currentSort={(this.state.sort_delivery_plan_date)?this.state.sort_delivery_plan_date:'none'}
                                                    title={trans('messages.lbl_delivery_plan_date')} />
                                            </TH>
                                            <TH className="col-max-min-100 text-center text-middle">
                                                <SortComponent
                                                    name="sort_delivery_date"
                                                    onClick={this.handleSortClick}
                                                    currentSort={(this.state.sort_delivery_date)?this.state.sort_delivery_date:'none'}
                                                    title={trans('messages.lbl_delivery_date')} />
                                            </TH>
                                            <TH className="col-max-min-100 text-center text-middle">
                                                <SortComponent
                                                    name="sort_delivery_time"
                                                    onClick={this.handleSortClick}
                                                    currentSort={(this.state.sort_delivery_time)?this.state.sort_delivery_time:'none'}
                                                    title={trans('messages.lbl_delivery_time')} />
                                            </TH>
                                            <TH className="col-max-min-130 text-center text-middle">
                                                {trans('messages.lbl_shipping_instructions')}
                                            </TH>
                                            <TH className="col-max-min-100 text-center text-middle">
                                                {trans('messages.lbl_account')}
                                            </TH>
                                            <TH className="col-max-min-100 text-center text-middle">
                                                {trans('messages.lbl_total_price')}
                                            </TH>
                                            <TH className="col-max-min-100 text-center text-middle">
                                                {trans('messages.lbl_error_message')}
                                            </TH>
                                            <TH className="col-max-min-100 text-center text-middle">
                                                {trans('messages.lbl_delivery_status')}
                                            </TH>
                                        </TR>

                                    </THEAD>
                                    <TBODY>
                                    {
                                        (!_.isEmpty(dataSource.data.data))?
                                        dataSource.data.data.map((item) => {
                                            index++;
                                            let classRow = (index%2 ===0) ? 'odd' : 'even';
                                            let deliveryDate = !_.isEmpty(item.delivery_date) ? moment.utc(item.delivery_date, "YYYY-MM-DD hh:mm:ss").format("YYYY/MM/DD") : '';
                                            let deliveryPlanDate = !_.isEmpty(item.delivery_plan_date) ? moment.utc(item.delivery_plan_date, "YYYY-MM-DD hh:mm:ss").format("YYYY/MM/DD") : '';
                                            let status = '';
                                            dataSource.statusOpt.map((entry) => {
                                                if (entry.key === item.delivery_status) {
                                                    status = entry.value;
                                                    return;
                                                }
                                            });

                                            return (
                                                [
                                                <TR key={"tr" + index} className={classRow}>
                                                    <TD className="text-center">
                                                       {index}
                                                    </TD>
                                                    <TD className="text-left "
                                                        title={item.name_jp}>{item.name_jp}
                                                    </TD>
                                                    <TD className="text-left" title={item.received_order_id}>
                                                            <a
                                                                href={saveUrl + "?received_order_id=" + item.received_order_id
                                                                              + "&subdivision_num=" + item.subdivision_num
                                                                            }
                                                                className="link-detail"
                                                            >{item.received_order_id}</a>
                                                    </TD>
                                                    <TD className="text-right "
                                                        title={item.subdivision_num}>{item.subdivision_num}
                                                    </TD>
                                                    <TD className="text-left"
                                                        title={item.company_name}>{item.company_name}
                                                    </TD>
                                                    <TD className="text-right "
                                                        title={item.inquiry_no}>{item.inquiry_no === null || item.inquiry_no === '0' ? '' : item.inquiry_no}
                                                    </TD>
                                                    <TD className="text-right "
                                                        title={deliveryPlanDate}>{deliveryPlanDate}
                                                    </TD>
                                                    <TD className="text-right "
                                                        title={deliveryDate}>{deliveryDate}
                                                    </TD>
                                                    <TD className="text-right "
                                                        title={item.delivery_time}>
                                                        {dataSource.shipWishDateOpt[item.delivery_time] ? dataSource.shipWishDateOpt[item.delivery_time] : ''}
                                                    </TD>
                                                    <TD className="max-width-100" title={item.shipping_instructions}>
                                                        <span className="cut-text limit-char max-w-100 show-modal-content"
                                                        data-content={item.shipping_instructions !== null ? item.shipping_instructions.replace(/(?:\r\n|\r|\n)/g, '<br>') : ''}
                                                        data-toggle="popover"
                                                        data-trigger="hover">
                                                            {item.shipping_instructions}
                                                        </span>
                                                    </TD>
                                                    <TD className="text-left "
                                                        title={item.account}>{item.account}
                                                    </TD>
                                                    <TD className="text-right"
                                                        title={nf.format(item.total_price)}>{nf.format(item.total_price)}
                                                    </TD>
                                                    <TD className="max-width-100" title={item.error_message}>
                                                        <span className="cut-text limit-char max-w-100 show-modal-content"
                                                        data-content={item.error_message !== null ? item.error_message.replace(/(?:\r\n|\r|\n)/g, '<br>') : ''}
                                                        data-toggle="popover"
                                                        data-trigger="hover">
                                                            {item.error_message}
                                                        </span>
                                                    </TD>
                                                    <TD className="text-left"
                                                        title={status}>{status}
                                                    </TD>
                                                </TR>
                                               ]
                                            )
                                        })
                                        :
                                        <TR><TD colSpan="17" className="text-center">{trans('messages.no_data')}</TD></TR>
                                    }
                                    </TBODY>
                                </TABLE>
                                </div>
                                <div className="text-center">
                                    <PaginationContainer handleClickPage={this.handleClickPage}/>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        dataSource: state.commonReducer.dataSource,
        dataResponse: state.commonReducer.dataResponse
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        reloadData: (url, params, pushHistory, ignoreFields) => { dispatch(Action.reloadData(url, params, pushHistory, ignoreFields)) },
        postData: (url, params, headers, callBackUrl, callBackParams) => { dispatch(Action.postData(url, params, headers, callBackUrl, callBackParams)) }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(DeliveryList)