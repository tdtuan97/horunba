import React, { Component } from 'react';
import { connect } from 'react-redux';

import * as Action from '../common/actions';
import * as queryString from 'query-string';

import Loading from '../common/components/common/Loading';
import FORM from '../common/components/form/FORM';
import INPUT from '../common/components/form/INPUT';
import SELECT from '../common/components/form/SELECT';
import BUTTON from '../common/components/form/BUTTON';
import TEXTAREA from '../common/components/form/TEXTAREA';
import DATEPICKER from '../common/components/form/DATEPICKER';
import moment from 'moment';

import TABLE from '../common/components/table/TABLE';
import THEAD from '../common/components/table/THEAD';
import TBODY from '../common/components/table/TBODY';
import TR from '../common/components/table/TR';
import TH from '../common/components/table/TH';
import TD from '../common/components/table/TD';
import MESSAGE_MODAL from '../common/containers/MessageModal';
import axios from 'axios';
let timer;
class DeliverySave extends Component {

    constructor(props) {
        super(props);
        this.state = {};
        let params = queryString.parse(props.location.search);
        if (params['received_order_id'] && params['subdivision_num']) {
            this.state = {
                received_order_id: params['received_order_id'],
                subdivision_num: params['subdivision_num'],
                received_subdivision_detail_line: params['received_order_id'] +"-"+ params['subdivision_num'],
            };
            this.state.accessFlg = true;
        }
        this.first = true;
        this.state.dataError = {};
        this.props.reloadData(formDataUrl, this.state);
        if (previous.includes('delivery') && !previous.includes('save')) {
            this.previous = previous;
        } else {
            this.previous = baseUrl;
        }

    }
    hanleRealtime() {
        if (this.state.received_subdivision_detail_line) {
            let params = {
                accessFlg : this.state.accessFlg,
                query : {
                        page_key : 'DELIVERY-SAVE',
                        primary_key : 'index:' + this.state.received_subdivision_detail_line
                    },
                key :this.state.received_subdivision_detail_line
            };
            if (this.state.accessFlg !== false) {
                this.props.postRealTime(checkEditUrl, params, {'X-Requested-With': 'XMLHttpRequest'});
            }
        }
    }
    componentWillReceiveProps(nextProps) {
        let dataSource = nextProps.dataSource;
        let dataResponse = nextProps.dataResponse;
        let showStatus = true;
        let messageTxt = '';
        if (!_.isEmpty(dataSource.data) && _.isEmpty(nextProps.dataRealTime) && this.first) {
            this.setState({...dataSource.data, deli_days: dataSource.deliDays}, () => {
                if (dataSource.data.delivery_status !== 9
                    && dataSource.data.delivery_status !== 0
                    && dataSource.data.delivery_status !== 7
                    && dataSource.data.delivery_status !== 5) {
                    document.getElementsByName('btnAccept')[0].disabled = true;
                }
                this.hanleRealtime();
            });
            this.first = false;
        }
         if (_.isEmpty(dataSource.data)) {
             window.location.href = baseUrl;
         }
        if (!_.isEmpty(dataResponse.method) && dataResponse.method === 'save') {
            if (dataResponse.status === 1) {
                window.location.href = this.previous;
            } else if(dataResponse.status === 0) {
                showStatus = false;
                messageTxt = dataResponse.message;
                document.getElementsByName('btnAccept')[0].disabled = false;
            } else {
                document.getElementsByName('btnAccept')[0].disabled = false;
            }
        }
        // Check realtime update
        if (!_.isEmpty(nextProps.dataRealTime)) {
            this.setState(nextProps.dataRealTime);
        }
        if(dataResponse.status === 0 && this.state.submit === true && typeof messageTxt !== 'object') {
            if (showStatus === false) {
                $.notify({
                    icon: 'glyphicon glyphicon-warning-sign',
                    message: messageTxt,
                },{
                    type: 'danger',
                    delay: 5000
                });
            }
        }
        this.setState({submit:false});

    }

    handleChange = (event) => {
        const target = event.target;
        const name = target.name;
        const value = target.type === 'checkbox' ? target.checked : target.value;
        let dataSet = {
            [name]: value
        }
        if (name === 'delivery_time' && !_.isEmpty(this.state.delivery_date)) {
            let deliveryDate = this.state.delivery_date
            if (value === '01') {
                deliveryDate = moment.utc(deliveryDate, "YYYYMMDD").subtract((this.state.deli_days + 1), 'days').format("YYYY/MM/DD");
                dataSet.delivery_plan_date = deliveryDate;
            }
        }
        this.setState({...dataSet});
    }


    handleCancel = (event) => {
        window.location.href = this.previous;
    }

    handleChangeDate = (date, name) => {
        let dateValue = moment(date);
        if (!dateValue.isValid()) {
            dateValue = undefined;
        } else {
            dateValue = dateValue.format('YYYY-MM-DD');
        }
        this.setState({
          [name]: dateValue,
        }, () => {
            let headers = {'X-Requested-With': 'XMLHttpRequest'};
            let param = this.state;
            param['action'] = name;
            axios.post(checkUrl, param, {headers : headers}
                ).then(response => {
                    this.setState({messageChange: response.data.message, delivery_plan_date: response.data.delivery_plan_date});
                }).catch(error => {
                    throw(error);
                });
            });
        }

    handleSubmit = (event) => {
        document.getElementsByName('btnAccept')[0].disabled = true;
        let headers = {'X-Requested-With': 'XMLHttpRequest'};
        if (this.props.enctype) {
            headers['Content-Type'] = this.props.enctype;
        }
        this.setState({submit:true});
        this.props.postData(this.props.location.pathname, this.state, headers);
    }

    componentDidUpdate() {
        if (!_.isEmpty(this.props.dataSource)) {
            $('.loading').hide();
        }
        if (this.state.accessFlg === false) {
            $('#message-modal').modal('show');
            //We will auto redirect after 5 seconds
            setTimeout(() => {
                window.location.href = this.previous;
            }, 5000);
        }
    }
    componentWillUnmount () {
        clearInterval(this.state);
    }
    componentDidMount() {
         setInterval(() => {
            this.hanleRealtime()
        }, 5000);

    }
    render() {
        let dataSource   = this.props.dataSource;
        let deliveryCodeOpt = this.props.dataSource.deliveryCodeOpt;
        let deliveryTimeOpt = this.props.dataSource.deliveryTimeOpt;
        let dataResponse = !_.isEmpty(this.props.dataResponse)? this.props.dataResponse : this.state.dataError;
        return ((!_.isEmpty(dataSource)) &&
            <div>
            <div className="box box-primary">
            <FORM className="form-horizontal ">
                <div className="box-body">
                    <Loading className="loading"></Loading>
                    <div className="row">
                        <div className="col-md-4 col-xs-12">
                            <INPUT
                                name="received_order_id"
                                labelTitle={trans('messages.received_order_id')}
                                className="form-control input-sm"
                                fieldGroupClass="col-md-8"
                                labelClass="col-md-4"
                                groupClass=""
                                errorPopup={true}
                                onChange={this.handleChange}
                                readOnly
                                defaultValue={dataSource.data.received_order_id}
                                hasError={dataResponse.message&&dataResponse.message.received_order_id?true:false}
                                errorMessage={dataResponse.message&&dataResponse.message.received_order_id?dataResponse.message.received_order_id:""}
                                />
                            <INPUT
                                name="subdivision_num"
                                labelTitle={trans('messages.lbl_subdivision_num')}
                                className="form-control input-sm"
                                fieldGroupClass="col-md-8"
                                labelClass="col-md-4"
                                groupClass=""
                                readOnly
                                defaultValue={dataSource.data.subdivision_num}
                                hasError={dataResponse.message&&dataResponse.message.subdivision_num?true:false}
                                errorMessage={dataResponse.message&&dataResponse.message.subdivision_num?dataResponse.message.subdivision_num:""}
                                />
                            <INPUT
                                name="inquiry_no"
                                labelTitle={trans('messages.lbl_inquiry_no')}
                                className="form-control input-sm"
                                fieldGroupClass="col-md-8"
                                labelClass="col-md-4"
                                groupClass=""
                                readOnly
                                defaultValue={dataSource.data.inquiry_no === null || dataSource.data.inquiry_no === '0' ? '' : dataSource.data.inquiry_no}
                                hasError={dataResponse.message&&dataResponse.message.inquiry_no?true:false}
                                errorMessage={dataResponse.message&&dataResponse.message.inquiry_no?dataResponse.message.inquiry_no:""}
                                />
                            <div className="form-group">
                            <DATEPICKER
                                placeholderText="YYYY-MM-DD"
                                dateFormat="YYYY-MM-DD"
                                locale="ja"
                                groupClass=" "
                                labelClass="col-md-4"
                                fieldGroupClass="col-md-8"
                                name="delivery_plan_date"
                                errorPopup={true}
                                className="form-control input-sm"
                                labelTitle={trans('messages.delivery_plan_date')}
                                selected={this.state.delivery_plan_date ? moment(this.state.delivery_plan_date) : null}
                                onChange={(date, name) => this.handleChangeDate(date, 'delivery_plan_date')}
                                hasError={dataResponse.message && dataResponse.message.delivery_plan_date ? true : false}
                                errorMessage={dataResponse.message && dataResponse.message.delivery_plan_date ? dataResponse.message.delivery_plan_date : ""} />
                                <div className="col-md-8 col-md-offset-4" style={{color: '#0073b7'}}>※15:00以降は出荷予定日が明日に更新します。</div>
                                <div className="col-md-8 col-md-offset-4 text-red">{(this.state.messageChange && this.state.messageChange.delivery_plan_date) ? this.state.messageChange.delivery_plan_date : ''}</div>
                            </div>
                            <div className="form-group">
                            <DATEPICKER
                                placeholderText="YYYY-MM-DD"
                                dateFormat="YYYY-MM-DD"
                                locale="ja"
                                groupClass=" "
                                labelClass="col-md-4"
                                fieldGroupClass="col-md-8"
                                name="delivery_date"
                                className="form-control input-sm"
                                labelTitle={trans('messages.lbl_delivery_date')}
                                selected={this.state.delivery_date ? moment(this.state.delivery_date) : null}
                                onChange={(date, name) => this.handleChangeDate(date, 'delivery_date')} />
                                <span className="col-md-12 text-red">{(this.state.messageChange && this.state.messageChange.delivery_date) ? this.state.messageChange.delivery_date : ''}</span>
                            </div>
                            <SELECT
                                name="delivery_time"
                                labelTitle={trans('messages.lbl_delivery_time')}
                                className="form-control input-sm"
                                fieldGroupClass="col-md-8"
                                labelClass="col-md-4"
                                options={deliveryTimeOpt}
                                onChange={this.handleChange}
                                defaultValue={this.state.delivery_time || '_none'}
                                errorPopup={true}
                                hasError={(dataResponse.message && dataResponse.message.delivery_time)?true:false}
                                errorMessage={(dataResponse.message&&dataResponse.message.delivery_time)?dataResponse.message.delivery_time:""}
                            />
                        </div>
                        <div className="col-md-4 col-xs-12">
                            <SELECT
                                name="delivery_code"
                                labelTitle={trans('messages.delivery_method')}
                                className="form-control input-sm"
                                fieldGroupClass="col-md-8"
                                labelClass="col-md-4"
                                options={deliveryCodeOpt}
                                onChange={this.handleChange}
                                defaultValue={this.state.delivery_code || '_none'}
                                errorPopup={true}
                                hasError={(dataResponse.message && dataResponse.message.delivery_code)?true:false}
                                errorMessage={(dataResponse.message&&dataResponse.message.delivery_code)?dataResponse.message.delivery_code:""}
                            />
                            <INPUT
                                name="delivery_postal"
                                labelTitle={trans('messages.lbl_delivery_postal')}
                                className="form-control input-sm"
                                fieldGroupClass="col-md-8"
                                labelClass="col-md-4"
                                groupClass=""
                                errorPopup={true}
                                maxLength="7"
                                onChange={this.handleChange}
                                defaultValue={dataSource.data.delivery_postal}
                                hasError={dataResponse.message&&dataResponse.message.delivery_postal?true:false}
                                errorMessage={dataResponse.message&&dataResponse.message.delivery_postal?dataResponse.message.delivery_postal:""}
                                />

                            <INPUT
                                name="delivery_perfecture"
                                labelTitle={trans('messages.lbl_delivery_perfecture')}
                                className="form-control input-sm"
                                fieldGroupClass="col-md-8"
                                labelClass="col-md-4"
                                groupClass=""
                                errorPopup={true}
                                onChange={this.handleChange}
                                defaultValue={dataSource.data.delivery_perfecture}
                                hasError={dataResponse.message&&dataResponse.message.delivery_perfecture?true:false}
                                errorMessage={dataResponse.message&&dataResponse.message.delivery_perfecture?dataResponse.message.delivery_perfecture:""}
                                />
                            <INPUT
                                name="delivery_city"
                                labelTitle={trans('messages.lbl_delivery_city')}
                                className="form-control input-sm"
                                fieldGroupClass="col-md-8"
                                labelClass="col-md-4"
                                groupClass=""
                                errorPopup={true}
                                onChange={this.handleChange}
                                defaultValue={dataSource.data.delivery_city}
                                hasError={dataResponse.message&&dataResponse.message.delivery_city?true:false}
                                errorMessage={dataResponse.message&&dataResponse.message.delivery_city?dataResponse.message.delivery_city:""}
                                />

                            <INPUT
                                name="delivery_sub_address"
                                labelTitle={trans('messages.lbl_delivery_sub_address')}
                                className="form-control input-sm"
                                fieldGroupClass="col-md-8"
                                labelClass="col-md-4"
                                groupClass=""
                                errorPopup={true}
                                onChange={this.handleChange}
                                defaultValue={dataSource.data.delivery_sub_address}
                                hasError={dataResponse.message&&dataResponse.message.delivery_sub_address?true:false}
                                errorMessage={dataResponse.message&&dataResponse.message.delivery_sub_address?dataResponse.message.delivery_sub_address:""}
                                />
                            <INPUT
                                name="delivery_tel"
                                labelTitle={trans('messages.lbl_delivery_tel')}
                                className="form-control input-sm"
                                fieldGroupClass="col-md-8"
                                labelClass="col-md-4"
                                groupClass=""
                                errorPopup={true}
                                onChange={this.handleChange}
                                defaultValue={dataSource.data.delivery_tel}
                                hasError={dataResponse.message&&dataResponse.message.delivery_tel?true:false}
                                errorMessage={dataResponse.message&&dataResponse.message.delivery_tel?dataResponse.message.delivery_tel:""}
                                />
                        </div>
                        <div className="col-md-4 col-xs-12">
                            <TEXTAREA
                                name="shipping_instructions"
                                labelTitle={trans('messages.lbl_shipping_instructions')}
                                className="form-control"
                                fieldGroupClass="col-md-8"
                                labelClass="col-md-4"
                                groupClass=""
                                errorPopup={true}
                                value={this.state.shipping_instructions || ''}
                                onChange={this.handleChange}
                                rows="6"
                            />
                            <TEXTAREA
                                name="error_message"
                                labelTitle={trans('messages.lbl_error_message') }
                                className="form-control"
                                fieldGroupClass="col-md-8"
                                labelClass="col-md-4"
                                groupClass=""
                                errorPopup={true}
                                value={this.state.error_message || ''}
                                onChange={this.handleChange}
                                rows="6"
                            />

                        </div>
                    </div>
                    <div className="row">
                        <div className="col-md-6 col-md-offset-6">
                            <BUTTON className="btn bg-purple input-sm pull-right btn-larger" type="button" onClick={this.handleCancel}>{trans('messages.btn_cancel')}</BUTTON>
                            <BUTTON name="btnAccept" className="btn btn-success input-sm btn-larger pull-right margin-element" type="button" onClick={this.handleSubmit}>{trans('messages.btn_change')}</BUTTON>
                        </div>
                    </div>
                    <hr/>
                    <div className="row">
                        <div className="col-md-12">
                            <div className="table-responsive">
                            <TABLE className="table table-striped table-custom">
                                <THEAD>
                                    <TR>
                                        <TH className="text-left text-middle">
                                            {trans('messages.index')}
                                        </TH>
                                        <TH className="col-max-min-130 text-left text-middle">
                                            {trans('messages.no')}
                                        </TH>
                                        <TH className="col-max-min-100 text-left text-middle">
                                            {trans('messages.product_name')}
                                        </TH>
                                        <TH className="col-max-min-75 text-left text-middle">
                                            {trans('messages.jan_code_ots')}
                                        </TH>
                                        <TH className="col-max-min-75 text-right text-middle">
                                            {trans('messages.quantity')}
                                        </TH>
                                    </TR>

                                </THEAD>
                                <TBODY>
                                {
                                    (!_.isEmpty(dataSource.dataGroup))?
                                    dataSource.dataGroup.map((item,index) => {
                                        index++;
                                        let classRow = (index%2 ===0) ? 'odd' : 'even';

                                        return (
                                            [
                                            <TR key={"tr" + index} className={classRow}>
                                                <TD className="text-left">
                                                   {index}
                                                </TD>
                                                <TD className="text-left "
                                                    title={item.product_code}>{item.product_code}
                                                </TD>
                                                <TD className="text-left "
                                                    title={item.product_name}>{item.product_name}
                                                </TD>
                                                <TD className="text-left "
                                                    title={item.product_jan}>{item.product_jan}
                                                </TD>
                                                <TD className="text-right "
                                                    title={item.delivery_num}>{item.delivery_num}
                                                </TD>
                                            </TR>
                                           ]
                                        )
                                    })
                                    :
                                    <TR><TD colSpan="5" className="text-center">{trans('messages.no_data')}</TD></TR>
                                }
                                </TBODY>
                            </TABLE>
                            </div>
                        </div>
                    </div>
                </div>
            </FORM>
            <MESSAGE_MODAL
                message={this.state.message || ''}
                title={trans('messages.deny_edit')}
                baseUrl={baseUrl}
            />
            </div>
            </div>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        dataSource: state.commonReducer.dataSource,
        dataResponse: state.commonReducer.dataResponse,
        dataRealTime: state.commonReducer.dataRealTime
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        reloadData: (url, params) => { dispatch(Action.detailData(url, params)) },
        postData: (url, params, headers) => { dispatch(Action.postData(url, params, headers))},
        postRealTime: (url, params, headers) => { dispatch(Action.postRealTime(url, params, headers)) }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(DeliverySave)