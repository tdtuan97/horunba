import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';
import TABLE from '../common/components/table/TABLE';
import THEAD from '../common/components/table/THEAD';
import TBODY from '../common/components/table/TBODY';
import TR from '../common/components/table/TR';
import TH from '../common/components/table/TH';
import TD from '../common/components/table/TD';
import { connect } from 'react-redux';
import * as Action from '../common/actions';
import createHistory from 'history/createBrowserHistory';
import BUTTON from '../common/components/form/BUTTON';
import Loading from '../common/components/common/Loading';
import PopupStartEndDate from './containers/PopupStartEndDate';
import moment from 'moment';

class ListBatchManagement extends Component {
    constructor(props) {
        super(props);
        let params = {};
        this.state = {
            baseUrl: baseUrl,
        }
        this.props.reloadData(dataListUrl, params);
        this.intervalReal = "";
    }

    handleClick = (event, command, status) => {
        if (command === 'process:smaregi-sync-product-api' && status === 'start') {
            this.setState({popup:'show','command' : command, 'status' : status}, () => {
                $('#start-end-modal').modal({backdrop: 'static', show: true});
                clearInterval(this.intervalReal);
            });

        } else {
            let params = {'command' : command, 'status' : status};
            this.state = {
                baseUrl: baseUrl,
            }
            this.props.postData(processEvent, params, {'X-Requested-With': 'XMLHttpRequest'}, this.props.reloadData, [dataListUrl, {}]);
        }
    }

    handleCancelStartEnd = () => {
        delete this.state.popup;
        delete this.state.command;
        delete this.state.status;
        delete this.state.start_date;
        delete this.state.end_date;
        $('#start-end-modal').modal('hide');
        this.intervalReal = setInterval(() => {
            this.setState(() => {
                let params = {};
                this.state = {
                    baseUrl: baseUrl,
                }
                this.props.reloadData(dataListUrl, params);
            });
        }, 10000);
    }

    componentDidMount() {
        if (!this.state.popup) {
            this.intervalReal = setInterval(() => {
                this.setState(() => {
                    let params = {};
                    this.state = {
                        baseUrl: baseUrl,
                    }
                    this.props.reloadData(dataListUrl, params);
                });
            }, 10000);
        }
    }

    handleChangeStartEnd = (name, dateValue) => {
        this.setState({
            [name]: dateValue
        });
    }

    handleSubmitStartEnd = () => {
        let params = {
            'command' : this.state.command,
            'status' : this.state.status,
            'startDate': this.state.start_date,
            'endDate': this.state.end_date,
        };
        this.state = {
            baseUrl: baseUrl,
        }
        delete this.state.popup;
        delete this.state.command;
        delete this.state.status;
        delete this.state.start_date;
        delete this.state.end_date;
        $('#start-end-modal').modal('hide');
        this.props.postData(processEvent, params, {'X-Requested-With': 'XMLHttpRequest'}, this.props.reloadData, [dataListUrl, {}]);
        this.intervalReal = setInterval(() => {
            this.setState(() => {
                let params = {};
                this.state = {
                    baseUrl: baseUrl,
                }
                this.props.reloadData(dataListUrl, params);
            });
        }, 10000);
    }

    componentDidUpdate() {
        if (!_.isEmpty(this.props.dataSource)) {
            $('.loading').hide();
        }

        $("table th.resize-thead").resizeble();

        $('.table-responsive').on('scroll', function (e) {
            $('div.scroll-bar-top').scrollLeft($('.table-responsive').scrollLeft());
        });
        $('.table-custom').on('scroll', function (e) {
            $('.table-custom').scrollLeft();
        });
        $('div.scroll-bar-top').on('scroll', function (e) {
            $('.table-responsive').scrollLeft($('div.scroll-bar-top').scrollLeft());
        });
        $(document).ready(function() {
            $('div.scroll-bar-top>div').width($('table.table-custom').width());
        });
        dragscroll.reset();
    }

    render() {
        let dataSource = this.props.dataSource;
        let index = 0;
        return ((!_.isEmpty(dataSource)) &&
            <div>
                <Loading className="loading" />
                <div className="box">
                    <div className="box-body">
                        <div className="row">
                            <div className="col-md-12">
                            <div className="table-responsive" style={{"tableLayout" : "fixed"}}>
                                <TABLE className="table table-striped table-custom">
                                    <THEAD>
                                        <TR>
                                            <TH className="visible-xs visible-sm"></TH>
                                            <TH className="hidden-xs hidden-sm text-center col-max-min-60">#</TH>
                                            <TH className="text-center col-max-min-200">
                                                { trans('messages.batch_management_table_header.command') }
                                            </TH>
                                            <TH className="hidden-xs hidden-sm text-center resize-thead col-max-min-200">
                                                { trans('messages.batch_management_table_header.description') }
                                            </TH>
                                            <TH className="hidden-xs hidden-sm text-center resize-thead col-max-min-170">
                                                { trans('messages.batch_management_table_header.error_message') }
                                            </TH>
                                            <TH className="text-center col-max-min-60">
                                                { trans('messages.batch_management_table_header.status') }
                                            </TH>
                                            <TH className="hidden-xs hidden-sm text-center col-max-min-200">
                                                { trans('messages.batch_management_table_header.last_excute') }
                                            </TH>
                                            <TH className="hidden-xs hidden-sm text-center col-max-min-300">
                                                { trans('messages.batch_management_table_header.process') }
                                            </TH>
                                        </TR>
                                    </THEAD>
                                    <TBODY>
                                        {
                                            (!_.isEmpty(dataSource.data))?
                                            dataSource.data.map((item) => {
                                                index++;
                                                let classRow = '';
                                                let startDis = false;
                                                let nameButton = 'Active'
                                                let statusColor = '';
                                                let statusText = '';
                                                if (item.is_active === 0) {
                                                    classRow = 'bg-light-blue';
                                                    startDis = true;
                                                } else {
                                                    if (item.status_flag === 1 || (!_.isEmpty(dataSource.name) && item.command === dataSource.name)) {
                                                        statusColor = 'batch-active';
                                                        statusText = 'Doing';
                                                        startDis = true;
                                                    } else if (item.status_flag === 0) {
                                                        statusText = 'Finished';
                                                        statusColor = 'batch-stop';
                                                    } else if (item.status_flag === 2) {
                                                        statusText = 'Error';
                                                        statusColor = 'batch-error';
                                                    }
                                                    nameButton = 'Disactive'
                                                }
                                                let lastExcute = item.last_execute;
                                                return (
                                                    [
                                                    <TR key={"tr" + index} className={classRow}>
                                                        <TD className="visible-xs visible-sm col-sm-1 col-xs-1">
                                                            <b className="show-info">
                                                                <i className="fa fa-angle-double-down" aria-hidden="true"></i>
                                                            </b>
                                                        </TD>
                                                        <TD className="hidden-xs hidden-sm col-md-0-5 col-sm-1 col-xs-1 text-center text-middle" title={index}>{index}</TD>
                                                        <TD className="col-xs-2 col-sm-2 text-middle" title={item.command}>{item.command}</TD>
                                                        <TD className="hidden-xs hidden-sm text-center text-long-batch text-middle" title={item.description}>{item.description}</TD>
                                                        <TD className="hidden-xs hidden-sm text-center text-long-batch text-middle" title={item.error_message}>{item.error_message}</TD>
                                                        <TD className="text-middle" title={statusText}>
                                                            <div id="circle" className={statusColor}></div>
                                                        </TD>
                                                        <TD className="hidden-xs hidden-sm cut-text text-center text-middle" title={lastExcute}>{lastExcute}</TD>
                                                        <TD className="hidden-xs hidden-sm cut-text text-middle text-center">
                                                            <BUTTON className="btn btn-danger margin-element" disabled={startDis} onClick={(e) => this.handleClick(e, item.command, 'start')}>Execute</BUTTON>
                                                            <BUTTON className="btn btn-primary margin-element" onClick={(e) => this.handleClick(e, item.command, nameButton)}>{nameButton}</BUTTON>
                                                            <BUTTON className="btn btn-success margin-element" onClick={(e) => this.handleClick(e, item.command, 'reset')}>{trans('messages.btn_reset')}</BUTTON>
                                                        </TD>
                                                    </TR>,
                                                    <TR key={"trhide" + index} className="hiden-info" style={{display:"none"}}>
                                                        <TD colSpan="10" className="width-span1">
                                                            <ul id={"temp" + item.id} >
                                                                <li> <b>NO</b> : #{index}</li>
                                                                <li> <b>{ trans('messages.batch_management_table_header.command') }</b> : {item.command}</li>
                                                                <li> <b>{ trans('messages.batch_management_table_header.description') }</b> : {item.description}</li>
                                                                <li> <b>{ trans('messages.batch_management_table_header.error_message') }</b> : {item.error_message}</li>
                                                                <li> <b>{ trans('messages.batch_management_table_header.status') }</b> : {statusText}</li>
                                                                <li> <b>{ trans('messages.batch_management_table_header.last_excute') }</b> : {lastExcute}</li>
                                                                <li>
                                                                    <BUTTON className="btn btn-danger margin-element" disabled={startDis} onClick={(e) => this.handleClick(e, item.command, 'start')}>Execute</BUTTON>
                                                                    <BUTTON className="btn btn-primary margin-element" onClick={(e) => this.handleClick(e, item.command, nameButton)}>{nameButton}</BUTTON>
                                                                </li>
                                                            </ul>
                                                        </TD>
                                                    </TR>
                                                   ]
                                                )
                                            })
                                            :
                                            <TR><TD colSpan="9" className="text-center">{trans('messages.no_data') }</TD></TR>
                                        }
                                    </TBODY>
                                </TABLE>
                            </div>
                            </div>
                        </div>
                    </div>
                    <PopupStartEndDate
                        startDate={this.state.start_date}
                        endDate={this.state.end_date}
                        handleChangeStartEnd={this.handleChangeStartEnd}
                        handleSubmitStartEnd={this.handleSubmitStartEnd}
                        handleCancelStartEnd={this.handleCancelStartEnd}
                    />
                </div>
            </div>
        );
    }
}

const mapStateToProps = (state) => {
  return {
    dataSource: state.commonReducer.dataSource
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    reloadData: (url, params) => { dispatch(Action.reloadData(url, params)) },
    postData: (url, params, headers, callBack, paramsCallBack) => { dispatch(Action.postData(url, params, headers,callBack, paramsCallBack)) }
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(ListBatchManagement)