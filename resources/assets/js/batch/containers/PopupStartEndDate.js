import React, { Component } from 'react';
import Loading from '../../common/components/common/Loading';
import DATEPICKER from '../../common/components/form/DATEPICKER';
import moment from 'moment';
export default class PopupStartEndDate extends Component {
    constructor(props) {
        super(props);
        this.state = {}
    }
    handleSubmit = () => {
        $(".loading").show();
        this.props.handleSubmitStartEnd();
    }
    handleCancelClick = () => {
        this.props.handleCancelStartEnd()
    }

    handleChangeStartEnd = function(date, name, event) {
        let dateValue = moment(date);
        if (!dateValue.isValid()) {
            dateValue = undefined;
        } else {
            dateValue = dateValue.format('YYYY-MM-DD');
        }

        this.props.handleChangeStartEnd(name, dateValue)
    }
    render() {
        return (
            <div className="modal fade" id="start-end-modal">
                <Loading className="loading" />
                <div className="modal-dialog ">
                    <div className="modal-content">
                        <div className="modal-header">
                        <b>{trans('messages.start_end_model_title')}</b>
                        </div>
                        <div className="modal-body modal-custom-body">
                            <form className="form-horizontal">
                                <DATEPICKER
                                    placeholderText="YYYY-MM-DD"
                                    dateFormat="YYYY-MM-DD"
                                    locale="ja"
                                    name="start_date"
                                    labelTitle={trans('messages.start_date') + "："}
                                    className="form-control input-sm"
                                    fieldGroupClass="col-md-9"
                                    labelClass="col-md-3"
                                    groupClass=""
                                    selected={this.props.startDate ? moment(this.props.startDate) : null}
                                    onChange={(date,name, e) => this.handleChangeStartEnd(date, 'start_date', e)}
                                    />
                                <DATEPICKER
                                    placeholderText="YYYY-MM-DD"
                                    dateFormat="YYYY-MM-DD"
                                    locale="ja"
                                    name="end_date"
                                    labelTitle={trans('messages.end_date') + "："}
                                    className="form-control input-sm"
                                    fieldGroupClass="col-md-9"
                                    labelClass="col-md-3"
                                    minDate={this.props.startDate ? moment(this.props.startDate) : null}
                                    groupClass=""
                                    selected={this.props.endDate ? moment(this.props.endDate) : null}
                                    onChange={(date,name, e) => this.handleChangeStartEnd(date, 'end_date', e)}
                                    />
                            </form>
                        </div>
                        <div className="modal-footer modal-custom-footer">
                            <button type="button" className="btn btn-primary btn-sm modal-custom-button" onClick={this.handleSubmit}>{trans('messages.btn_accept_ok')}</button>
                            <button type="button" className="btn bg-purple btn-sm modal-custom-button" onClick={this.handleCancelClick}>{trans('messages.btn_accept_cancel')}</button>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}