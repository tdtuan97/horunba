import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';

import { connect } from 'react-redux';
import * as Action from '../common/actions';
import * as queryString from 'query-string';
import createHistory from 'history/createBrowserHistory';
import BUTTON from '../common/components/form/BUTTON';
import FORM from '../common/components/form/FORM';
import INPUT from '../common/components/form/INPUT';
import SELECT from '../common/components/form/SELECT';
import LABEL from '../common/components/form/LABEL';
import axios from 'axios';

import TABLE from '../common/components/table/TABLE';
import THEAD from '../common/components/table/THEAD';
import TBODY from '../common/components/table/TBODY';
import TR from '../common/components/table/TR';
import TH from '../common/components/table/TH';
import TD from '../common/components/table/TD';
import SortComponent from '../common/components/table/SortComponent';
import PaginationContainer from '../common/containers/PaginationContainerNew'
import Loading from '../common/components/common/Loading';
import ImportCsv from './csv/ImportCsv';

import DatePicker from 'react-datepicker';
import moment from 'moment';
import 'react-datepicker/dist/react-datepicker.css';

class ListOrderManagement extends Component {

    constructor(props) {
        super(props);
        const stringParams = queryString.parse(props.location.search);
        this.state = {}
        this.curStrQuery = '';
        if (!_.isEmpty(stringParams)) {
            this.state = stringParams;
        }
        this.props.reloadData(dataListUrl, stringParams);
    }

    handleChange = (event) => {
        const target = event.target;
        const value = target.type === 'checkbox' ? target.checked : target.value;
        const name = target.name;
        this.setState({
            [name]: (value)?value:undefined
        },() => {
            if (name === 'per_page' ||
                name === 'order_status' ||
                name === 'name_jp' ||
                name === 'payment_method' ) {
                this.handleReloadData();
            }
        });
    }

    handleChangeMultiSelect = (name, value, checked) => {
        let currentValue = this.state[name];
        if (typeof currentValue === 'undefined') {
            currentValue = [];
        } else if (!Array.isArray(this.state[name])) {
            currentValue = [this.state[name]];
        }
        if (Array.isArray(value)) {
            currentValue = [...value];
        } else {
            let index = currentValue.indexOf(value);
            if (checked === false) {
                if (index !== -1) {
                    currentValue.splice(index, 1);
                }
            } else {
                if (index === -1) {
                    currentValue.push(value);
                }
            }
        }
        this.setState({
            [name]: currentValue,
        });
    };

    handleChangeDate = function(date, name, event) {
        let dateValue = moment(date);
        if (!dateValue.isValid()) {
            dateValue = undefined;
        } else {
            dateValue = dateValue.format('YYYY-MM-DD');
        }
        this.setState({
          [name]: dateValue,
        });
    }

    handleClearClick = (event) => {
        Object.keys(this.state).map((key) => {
            if (key !== 'per_page') {
                delete this.state[key];
            }
        });
        this.handleReloadData();
    }

    handleClickPage = (page) => {
        this.setState({
            page: page
        },() => {
            this.props.reloadData(dataListUrl, this.state, true);
        });
    }

    handleSortClick = (name, nextSort) => {
        Object.keys(this.state).map((key) => {
            if (key.startsWith('sort_')) {
                delete this.state[key];
            }
        });
        if (nextSort === 'none') {
            delete this.state[name];
            this.props.reloadData(dataListUrl, this.state, true);
        } else {
            this.setState({[name]: nextSort}, () => {
                this.props.reloadData(dataListUrl, this.state, true);
            });
        }
    }
    handleReloadData = () => {
        if (this.state.page) {
            delete this.state.page;
        }
        this.props.reloadData(dataListUrl, this.state, true);
    }
    handleGetData = (name) => {
        Object.keys(this.state).map((key) => {
            delete this.state[key];
        });
        let param = {};
        if (name === 'cancel') {
            param.name_jp = [4, 5];
            param.order_status = _.status('ORDER_STATUS.CANCEL');
            param.is_mall_cancel  = 0;
        } else if (name === 'fix') {
            param.is_mall_update = 1;
        } else if (name === 'keppin') {
            param.order_sub_status = [_.status('ORDER_SUB_STATUS.OUT_OF_STOCK'), _.status('ORDER_SUB_STATUS.STOP_SALE')];
            param.show_tab = 'order_detail_tab';
            param.is_delay = 0;
        } else if (name === 'error') {
            param.order_sub_status = [_.status('ORDER_SUB_STATUS.ERROR'),];
            param.another_order_status = 8;
            param.is_delay = 0;
        } else if (name === 'delay') {
            param.is_delay             = 1;
            param.another_order_status = 10;
        }
        this.setState(param, () => {
            this.props.reloadData(dataListUrl, this.state, true);
        })
    }
    componentDidUpdate() {
        if (!_.isEmpty(this.props.dataSource)) {
            $('.loading').hide();
        }
    }
    componentWillMount() {
        window.onpopstate = (event) => {
            location.reload();
        };
    }
    componentWillReceiveProps(nextProps) {
        let param = {};
        Object.keys(this.state).map((item) => {
            if (typeof this.state[item] !== 'undefined') {
                if (typeof this.state[item] === 'array' || typeof this.state[item] === 'object') {
                    param[item] = this.state[item];
                } else {
                    param[item] = _.trim(this.state[item]);
                }
            }
        });
        let curParam = {};
        let ignoreParam = [
            'attached_file',
            'attached_file_path',
            'error',
            'message',
            'msg',
            'status_csv',
            'checkAfter',
            'currPage',
            'fileCorrect',
            'filename',
            'flg',
            'percent',
            'process',
            'realFilename',
            'timeRun',
            'total',
        ];
        Object.keys(param).map((item) => {
            if (ignoreParam.indexOf(item) === -1) {
                curParam[item] = param[item];
            }
        });
        this.curStrQuery = queryString.stringify(curParam);
        let dataSource = nextProps.dataSource;
        if ((!_.isEmpty(dataSource.error))) {
            Object.keys(dataSource.error).map((k) => {
                $.notify({
                    icon: 'glyphicon glyphicon-warning-sign',
                    message: dataSource.error[k],
                },{
                    type: 'danger',
                    delay: 2000
                });

            });
        }

        let dataResponse = nextProps.dataResponseCsv;
        if (!_.isEmpty(dataResponse) && this.flg_check) {
            if (typeof dataResponse.not_permission !== "undefined" && dataResponse.not_permission === 1) {
                location.reload();
            }
            if (typeof dataResponse.reload !== 'undefined' && dataResponse.reload) {
                this.props.postDataCSV(importUrl, dataResponse.params, dataResponse.headers, dataResponse.count_err);
            }
            if(dataResponse.flg === 1) {
                let formData   = new FormData();
                let headers = {'X-Requested-With': 'XMLHttpRequest'};
                Object.keys(dataResponse).map((item) => {
                    formData.append(item, dataResponse[item]);
                });
                formData.append('type', $('#action-modal').val());
                this.setState({
                    process: true,
                    status_csv: 'show',
                    percent: (dataResponse.currPage/dataResponse.timeRun) * 100,
                }, () => {
                    this.props.postDataCSV(importUrl, formData, headers);
                });
            } else if (dataResponse.flg === 0) {
                let dataSet = {
                    process: false,
                    status_csv: false,
                };
                this.flg_check = false;
                if (dataResponse.msg === 'Finish') {
                    if (dataResponse.error !== 1) {
                        $.notify({
                            icon: 'glyphicon glyphicon-warning-sign',
                            message: trans('messages.message_import_success'),
                        },{
                            type: 'info',
                            delay: 2000
                        });
                    }
                    dataSet['msg'] = '';
                    this.setState({...dataResponse,attached_file_path:'', ...dataSet}, () => {
                        if (dataResponse.error === 0) {
                            $('#issue-modal').modal('hide');
                            this.handleReloadData();
                        }
                        $("input[name='browser_attached_file_path']").val('');
                        $("input[name='browser_attached_file_path']").val('');
                    });
                } else {
                    this.setState({...dataResponse, ...dataSet});
                }
            }
        }
    }
    handleExportCsv = () => {
        $(".loading").show();
        let param = {...this.state};
        param['type'] = 'isCsv';
        let headers = {'X-Requested-With': 'XMLHttpRequest'};
        axios.post(processExportCSV, param, {headers : headers}
        ).then(response => {
            $(".loading").hide();
            if (response.data.status === 1) {
                window.location.href = exportCSV +"?fileName="+ response.data.file_name;
            }
        }).catch(error => {
            $(".loading").hide();
            throw(error);
        });
    }

    handleChooseFile = (event) => {
        const target = event.target;
        if (target.files !== null && target.files.length !== 0) {
            const name = target.name;
            let tmpName = name.replace(new RegExp("^browser_"), '');
            this.setState({
                [tmpName]: target.files[0].name,
                ['attached_file']: target.files[0]
            }, ()=> {
                document.getElementsByName(tmpName)[0].value = target.files[0].name;
            });
        }
    }

    handleImportSubmit = () => {
        let formData   = new FormData();
        let headers = {'X-Requested-With': 'XMLHttpRequest'};
        if (!_.isEmpty(this.state)) {
            Object.keys(this.state).map((item) => {
                formData.append(item, this.state[item]);
            });
        }
        this.flg_check = true;
        this.setState({
            process: true,
            status_csv: 'show',
            percent: 0,
        }, () => {
            this.props.postDataCSV(csvUrl, formData, headers);
        });
    }

    handleImportCancel = () => {
        $('#issue-modal').modal('hide');
        this.flg_check = false;
        this.setState({
            attached_file_path: '',
            attached_file: '',
            message: '',
            status_csv: false,
            error: 0,
            msg: '',
        }, () => {
            $("input[name='browser_attached_file_path']").val('');
            this.handleReloadData();
        });
    }

    handleImport = (event, name) => {
        $('#action-modal').val(name);
        $('#issue-modal').modal({backdrop: 'static', show: true});
    }

    render() {
        let dataSource = this.props.dataSource;
        let index = 0;
        let mallOptions = {};
        let paymentOptions = {};
        let orderStatusOptions = {};
        let orderSubStatusOptions = {};
        let totalItems = 0;
        let itemsPerPage = 20;
        if (!_.isEmpty(dataSource)) {
            index = dataSource.pageData.from - 1;

            dataSource.mall_data.map((value) => {
               mallOptions[value.id] = value.name_jp;
            });

            dataSource.settlement_manage_data.map((value) => {
               paymentOptions[value.payment_code] = value.payment_name;
            });

            dataSource.order_status_data.map((value) => {
               orderStatusOptions[value.order_status_id] = value.status_name;
            });
            dataSource.order_sub_status_data.map((value) => {
               orderSubStatusOptions[value.key] = value.value;
            });
            totalItems   = dataSource.pageData.total;
            itemsPerPage = dataSource.pageData.per_page;
        }
        return ( (!_.isEmpty(dataSource)) &&
            <div>
                <Loading className="loading" />
                <div className="box">
                    <div className="box-header">
                        <div className="row">
                            <div className="col-md-12">
                                <h4>{trans('messages.search_title')}</h4>
                            </div>
                            <FORM>
                                <div className="row">
                                <div className="col-md-12">
                                    <SELECT
                                        id="name_jp"
                                        name="name_jp"
                                        groupClass="form-group col-md-1-5"
                                        labelClass=""
                                        labelTitle={trans('messages.name_jp')}
                                        handleChangeMultiSelect={this.handleChangeMultiSelect}
                                        valueMulti={this.state.name_jp||''}
                                        options={mallOptions}
                                        multiple="multiple"
                                        className="form-control input-sm" />
                                    <LABEL
                                        groupClass="form-group col-md-1-5"
                                        labelTitle={trans('messages.order_date_from')}>
                                        <DatePicker
                                            placeholderText="YYYY-MM-DD"
                                            dateFormat="YYYY-MM-DD"
                                            locale="ja"
                                            name="order_date_from"
                                            className="form-control input-sm"
                                            key="order_date_from"
                                            id="order_date_from"
                                            selected={this.state.order_date_from ? moment(this.state.order_date_from) : null}
                                            onChange={(date,name, e) => this.handleChangeDate(date, 'order_date_from', e)} />
                                    </LABEL>
                                    <LABEL
                                        groupClass="form-group col-md-1-5"
                                        labelTitle={trans('messages.order_date_to')}>
                                        <DatePicker
                                            placeholderText="YYYY-MM-DD"
                                            dateFormat="YYYY-MM-DD"
                                            locale="ja"
                                            name="order_date_to"
                                            className="form-control col-md-12 input-sm"
                                            key="order_date_to"
                                            id="order_date_to"
                                            selected={this.state.order_date_to ? moment(this.state.order_date_to) : null}
                                            onChange={(date,name, e) => this.handleChangeDate(date, 'order_date_to', e)} />
                                    </LABEL>
                                    <INPUT
                                        name="receive_id"
                                        groupClass="form-group col-md-1-5"
                                        labelTitle={trans('messages.qa_no')}
                                        onChange={this.handleChange}
                                        value={this.state.receive_id ||''}
                                        className="form-control input-sm" />
                                    <INPUT
                                        name="received_order_id"
                                        groupClass="form-group col-md-1-5"
                                        labelTitle={trans('messages.received_order_id')}
                                        onChange={this.handleChange}
                                        value={this.state.received_order_id ||''}
                                        className="form-control input-sm" />
                                    <INPUT
                                        name="full_name"
                                        groupClass="form-group col-md-1-5"
                                        labelTitle={trans('messages.full_name')}
                                        onChange={this.handleChange}
                                        value={this.state.full_name ||''}
                                        className="form-control input-sm" />
                                    <INPUT
                                        name="name_kana"
                                        groupClass="form-group col-md-1-5"
                                        labelTitle={trans('messages.name_kana')}
                                        onChange={this.handleChange}
                                        value={this.state.name_kana ||''}
                                        className="form-control input-sm" />
                                    <INPUT
                                        name="tel_num"
                                        groupClass="form-group col-md-1-5"
                                        labelTitle={trans('messages.tel_num')}
                                        onChange={this.handleChange}
                                        value={this.state.tel_num ||''}
                                        className="form-control input-sm" />
                                </div>
                                <div className="col-md-12">
                                    <SELECT
                                        id="order_status"
                                        name="order_status"
                                        groupClass="form-group col-md-1"
                                        labelClass=""
                                        labelTitle={trans('messages.order_status')}
                                        handleChangeMultiSelect={this.handleChangeMultiSelect}
                                        valueMulti={this.state.order_status||''}
                                        options={orderStatusOptions}
                                        multiple="multiple"
                                        className="form-control input-sm" />
                                    <SELECT
                                        id="order_sub_status"
                                        name="order_sub_status"
                                        groupClass="form-group col-md-1"
                                        labelClass=""
                                        labelTitle={trans('messages.order_sub_status')}
                                        handleChangeMultiSelect={this.handleChangeMultiSelect}
                                        valueMulti={this.state.order_sub_status||''}
                                        options={orderSubStatusOptions}
                                        multiple="multiple"
                                        className="form-control input-sm" />
                                    <SELECT
                                        id="payment_method"
                                        name="payment_method"
                                        groupClass="form-group col-md-1 col-sm-12"
                                        labelClass="control-label"
                                        labelTitle={trans('messages.payment_name')}
                                        handleChangeMultiSelect={this.handleChangeMultiSelect}
                                        valueMulti={this.state.payment_method||''}
                                        options={paymentOptions}
                                        fieldGroupClass = ""
                                        multiple="multiple"
                                        className="form-control input-sm" />
                                    <LABEL
                                        groupClass="form-group col-md-1-5"
                                        labelTitle={trans('messages.delay_priod_from')}>
                                        <DatePicker
                                            placeholderText="YYYY-MM-DD"
                                            dateFormat="YYYY-MM-DD"
                                            locale="ja"
                                            name="delay_priod_from"
                                            className="form-control input-sm"
                                            key="delay_priod_from"
                                            id="delay_priod_from"
                                            selected={this.state.delay_priod_from ? moment(this.state.delay_priod_from) : null}
                                            onChange={(date,name, e) => this.handleChangeDate(date, 'delay_priod_from', e)} />
                                    </LABEL>
                                    <LABEL
                                        groupClass="form-group col-md-1-5"
                                        labelTitle={trans('messages.delay_priod_to')}>
                                        <DatePicker
                                            placeholderText="YYYY-MM-DD"
                                            dateFormat="YYYY-MM-DD"
                                            locale="ja"
                                            name="delay_priod_to"
                                            className="form-control col-md-12 input-sm"
                                            key="delay_priod_to"
                                            id="delay_priod_to"
                                            selected={this.state.delay_priod_to ? moment(this.state.delay_priod_to) : null}
                                            onChange={(date,name, e) => this.handleChangeDate(date, 'delay_priod_to', e)} />
                                    </LABEL>
                                    <INPUT
                                        name="product_code"
                                        groupClass="form-group col-md-1-5"
                                        labelTitle={trans('messages.product_code')}
                                        onChange={this.handleChange}
                                        value={this.state.product_code ||''}
                                        className="form-control input-sm" />
                                    <INPUT
                                        name="address"
                                        groupClass="form-group col-md-1-5"
                                        labelTitle={trans('messages.receiver_postal')}
                                        onChange={this.handleChange}
                                        value={this.state.address ||''}
                                        className="form-control input-sm" />
                                    <INPUT
                                        name="request_price_from"
                                        groupClass="form-group col-md-1-5"
                                        labelTitle={trans('messages.request_price_from')}
                                        onChange={this.handleChange}
                                        value={this.state.request_price_from ||''}
                                        className="form-control input-sm" />
                                    <INPUT
                                        name="request_price_to"
                                        groupClass="form-group col-md-1-5"
                                        labelTitle={trans('messages.request_price_to')}
                                        onChange={this.handleChange}
                                        value={this.state.request_price_to ||''}
                                        className="form-control input-sm" />
                                    <SELECT
                                        name="per_page"
                                        groupClass="form-group col-md-1 per-page pull-right-md"
                                        labelClass=""
                                        labelTitle={trans('messages.per_page')}
                                        onChange={this.handleChange}
                                        value={this.state.per_page||''}
                                        options={{20:20, 40:40, 60:60, 80:80, 100:100}}
                                        className="form-control input-sm" />
                                </div>
                                </div>
                                </FORM>
                        </div>
                        <div className="row">

                                <div className="col-sm-12 col-xs-12">
                                    <a href={newOrderUrl} className="btn btn-success pull-left margin-element margin-form">{trans('messages.btn_add')}</a>
                                    <BUTTON className="btn btn-success pull-left margin-element margin-form" onClick={(e) => this.handleImport(e, 'new_order')}>{trans('messages.btn_upload_csv')}</BUTTON>
                                    <BUTTON className="btn btn-primary pull-left margin-element margin-form" onClick={(e) => this.handleGetData('cancel')}>{trans('messages.order_get_cancel_list')}</BUTTON>
                                    <BUTTON className="btn btn-primary pull-left margin-element margin-form" onClick={(e) => this.handleGetData('fix')}>{trans('messages.order_get_fix_list')}</BUTTON>
                                    <BUTTON className="btn btn-primary pull-left margin-element margin-form" onClick={(e) => this.handleGetData('keppin')} >{trans('messages.order_get_keppin_list')}</BUTTON>
                                    <BUTTON className="btn btn-primary pull-left margin-element margin-form" onClick={(e) => this.handleGetData('error')} >{trans('messages.btn_error_list')}</BUTTON>
                                    <BUTTON className="btn btn-primary pull-left margin-element margin-form" onClick={(e) => this.handleGetData('delay')} >{trans('messages.btn_delay')}</BUTTON>
                                    <BUTTON className="btn btn-danger pull-right margin-form" onClick={this.handleClearClick}>{trans('messages.btn_reset')}</BUTTON>
                                    <BUTTON className="btn btn-primary pull-right margin-element margin-form" onClick={this.handleReloadData} >{trans('messages.btn_search')}</BUTTON>
                                    <BUTTON className="btn btn-primary margin-form  margin-element" onClick={(e) => this.handleExportCsv()}>{trans('messages.csv_export')}</BUTTON>
                                </div>

                        </div>
                    </div>
                    <div className="box-body">
                        <div className="row">
                            <div className="col-md-12">
                                {(totalItems/itemsPerPage > 1) &&
                                <div className="row" id="panigation">
                                    <div className="col-md-2-5"></div>
                                    <div className="col-md-7 text-center">
                                        <PaginationContainer handleClickPage={this.handleClickPage}/>
                                    </div>
                                    <div className="col-md-2-5 text-right line-height-md">
                                        {dataSource.pageData.from}件 - {dataSource.data.to}件 【全{dataSource.pageData.total}件】
                                    </div>
                                </div>
                                }
                                <TABLE className="table table-striped table-custom">
                                    <THEAD>
                                        <TR>
                                            <TH className="visible-xs visible-sm"></TH>
                                            <TH className="hidden-xs hidden-sm text-center">
                                                #
                                            </TH>
                                            <TH className="hidden-xs hidden-sm text-center text-middle col-max-min-60">
                                                <SortComponent
                                                    name="sort_name_jp"
                                                    onClick={this.handleSortClick}
                                                    currentSort={(this.state.sort_name_jp)?this.state.sort_name_jp:'none'}
                                                    title={trans('messages.name_jp')} />
                                            </TH>
                                            <TH className="hidden-xs hidden-sm text-center text-middle  col-max-min-120">
                                                <SortComponent
                                                    name="sort_order_date"
                                                    onClick={this.handleSortClick}
                                                    currentSort={(this.state.sort_order_date)?this.state.sort_order_date:'none'}
                                                    title={trans('messages.order_date')} />
                                            </TH>
                                            <TH className="text-center  text-middle col-max-min-120 ">
                                                <SortComponent
                                                    name="sort_received_order_id"
                                                    onClick={this.handleSortClick}
                                                    currentSort={(this.state.sort_received_order_id)?this.state.sort_received_order_id:'none'}
                                                    title={trans('messages.received_order_id')} />
                                            </TH>
                                            <TH className="text-center text-middle col-max-min-120">
                                                <SortComponent
                                                    name="sort_receive_id"
                                                    onClick={this.handleSortClick}
                                                    currentSort={(this.state.sort_receive_id)?this.state.sort_receive_id:'none'}
                                                    title={trans('messages.receive_id')} />
                                            </TH>
                                            <TH className="hidden-xs hidden-sm text-center  text-middle col-max-min-120">
                                                <SortComponent
                                                    name="sort_full_name"
                                                    onClick={this.handleSortClick}
                                                    currentSort={(this.state.sort_full_name)?this.state.sort_full_name:'none'}
                                                    title={trans('messages.full_name')} />
                                            </TH>
                                            <TH className="hidden-xs hidden-sm text-center  text-middle col-max-min-100">
                                                <SortComponent
                                                    name="sort_order_status"
                                                    onClick={this.handleSortClick}
                                                    currentSort={(this.state.sort_order_status)?this.state.sort_order_status:'none'}
                                                    title={trans('messages.order_status')} />
                                            </TH>
                                            <TH className="hidden-xs hidden-sm text-center  text-middle col-max-min-100">
                                                <SortComponent
                                                    name="sort_payment_method"
                                                    onClick={this.handleSortClick}
                                                    currentSort={(this.state.sort_payment_method)?this.state.sort_payment_method:'none'}
                                                    title={trans('messages.payment_name')} />
                                            </TH>
                                            <TH className="hidden-xs hidden-sm text-center  text-middle col-max-min-100">
                                                <SortComponent
                                                    name="sort_request_price"
                                                    onClick={this.handleSortClick}
                                                    currentSort={(this.state.sort_request_price)?this.state.sort_request_price:'none'}
                                                    title={trans('messages.request_price')} />
                                            </TH>
                                            <TH className="hidden-xs hidden-sm text-center  text-middle col-max-min-120">{trans('messages.delay_priod_list_order')}
                                            </TH>
                                        </TR>
                                    </THEAD>
                                    <TBODY>
                                        {
                                            (!_.isEmpty(dataSource.data.data))?
                                            dataSource.data.data.map((item) => {
                                                index++;
                                                var orderDate = moment.utc(item.order_date, "YYYY-MM-DD");
                                                var formatOrderDate = orderDate.format("MM/DD");
                                                var nf = new Intl.NumberFormat();
                                                let classRow = (index%2 ===0) ? 'odd' : 'even';
                                                return (
                                                    [
                                                    <TR key={"tr" + index} className={classRow}>
                                                        <TD className="visible-xs visible-sm text-center">
                                                            <b className="show-info">
                                                                <i className="fa fa-angle-double-down" aria-hidden="true"></i>
                                                            </b>
                                                        </TD>
                                                        <TD className="hidden-xs hidden-sm text-center">
                                                           {nf.format(index)}
                                                        </TD>
                                                        <TD className="hidden-xs hidden-sm cut-text" title={item.name_jp}>{item.name_jp}</TD>
                                                        <TD className="hidden-xs hidden-sm" title={item.order_date}>{formatOrderDate}</TD>
                                                        <TD className="cut-text" title={item.received_order_id}>{item.received_order_id}</TD>
                                                        <TD className="cut-text text-center" title={item.qa_no}>
                                                            <a
                                                                href={detailUrl + "?" + (this.curStrQuery?(this.curStrQuery+"&"):"") + "receive_id_key=" + item.receive_id}
                                                                className="link-detail"
                                                            >{item.receive_id}</a>
                                                        </TD>
                                                        <TD className="hidden-xs hidden-sm cut-text max-w-200" title={item.full_name}>{item.full_name}</TD>
                                                        <TD className="hidden-xs hidden-sm cut-text" title={item.status_name}>{item.status_name}</TD>
                                                        <TD className="hidden-xs hidden-sm cut-text" title={item.payment_name}>{item.payment_name}</TD>
                                                        <TD className="hidden-xs hidden-sm cut-text text-right" title={nf.format(item.request_price)}>{nf.format(item.request_price)}</TD>
                                                        <TD className="hidden-xs hidden-sm cut-text" title={item.payment_name}>{item.delay_priod}</TD>
                                                    </TR>,
                                                    <TR key={"trhide" + index} className="hiden-info" style={{display:"none"}}>
                                                        <TD colSpan="10" className="width-span1">
                                                            <ul id={"temp" + item.mail_id} >
                                                                <li> <b>NO</b> : #{item.receive_id}</li>
                                                                <li> <b>{trans('messages.name_jp')}</b> : {item.name_jp}</li>
                                                                <li> <b>{trans('messages.order_date')}</b> : {formatOrderDate}</li>
                                                                <li> <b>{trans('messages.received_order_id')}</b> : {item.received_order_id}</li>
                                                                <li> <b>{trans('messages.receive_id')}</b> : {item.receive_id}</li>
                                                                <li> <b>{trans('messages.full_name')}</b> : {item.full_name}</li>
                                                                <li> <b>{trans('messages.status_name')}</b> : {item.status_name}</li>
                                                                <li> <b>{trans('messages.payment_name')}</b> : {item.payment_name}</li>
                                                                <li> <b>{trans('messages.request_price')}</b> : {nf.format(item.request_price)}</li>
                                                                <li> <b>{trans('messages.delay_priod_list_order')}</b> : {item.delay_priod}</li>
                                                            </ul>
                                                        </TD>
                                                    </TR>
                                                   ]
                                                )
                                                if (index === dataSource.data.to) {
                                                   index = 0;
                                                }
                                            })
                                            :
                                            <TR><TD colSpan="10" className="text-center">{trans('messages.no_data')}</TD></TR>
                                        }
                                    </TBODY>
                                </TABLE>
                                <div className="text-center">
                                    <PaginationContainer handleClickPage={this.handleClickPage}/>
                                </div>
                                <ImportCsv
                                    handleChooseFile={this.handleChooseFile}
                                    handleImportSubmit={this.handleImportSubmit}
                                    value={this.state.attached_file_path ? this.state.attached_file_path : ''}
                                    error={this.state.error}
                                    filename={this.state.filename}
                                    handleImportCancel={this.handleImportCancel}
                                    process={this.state.process ? true : false}
                                    status={this.state.status_csv}
                                    percent={this.state.percent}
                                    hasError={(this.state.message || this.state.msg) ? true : false}
                                    errorMessage={this.state.message || this.state.msg}
                                />
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        )
    }
}
const mapStateToProps = (state) => {
  return {
    dataSource: state.commonReducer.dataSource,
    dataResponseCsv: state.commonReducer.dataResponseCsv
  }
}
    const mapDispatchToProps = (dispatch) => {
  let ignore = [
      'attached_file',
      'attached_file_path',
      'error',
      'message',
      'msg',
      'status_csv',
      'checkAfter',
      'currPage',
      'fileCorrect',
      'filename',
      'flg',
      'percent',
      'process',
      'realFilename',
      'timeRun',
      'total',
  ];
  return {
    reloadData: (url, params, pushHistory, ignoreFields) => { dispatch(Action.reloadData(url, params, pushHistory, ignore)) },
    postDataCSV: (url, params, headers) => { dispatch(Action.postDataCSV(url, params, headers)) },
  }
}
export default connect(mapStateToProps, mapDispatchToProps)(ListOrderManagement)
