import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Loading from '../../common/components/common/Loading';
import axios from 'axios';
import UPLOAD from '../../common/components/form/UPLOAD';
import { connect } from 'react-redux';
import * as Action from '../../common/actions';

class ImportCsv extends Component {
    constructor(props) {
        super(props);
    }
    render() {
        let token = $("meta[name='csrf-token']").attr("content");
        let dataResponse = this.props.dataResponse;
        return (
            <div className="modal fade" id="issue-modal">
                <Loading className="loading" />
                <input type="hidden" id="action-modal" />
                <div className="modal-dialog">
                    <div className={"modal-content modal-custom-content " + (this.props.process ? 'hidden-div-csv' : '')}>
                        <div className={"modal-body modal-custom-body " + (this.props.error === 1 ? 'hidden' : '')}>
                            <p>{trans('messages.upload_csv_order_title')}</p>
                            <form className="form-horizontal"
                            encType={"multipart/form-data; boundary=----WebKitFormBoundary" + token}>
                                <UPLOAD
                                    name="attached_file_path"
                                    className="form-control input-sm"
                                    fieldGroupClass="col-md-12"
                                    itemGroupClass="input-group input-group-sm"
                                    btnClass="btn btn-info btn-flat btn-largest"
                                    btnTitle={trans('messages.btn_browser')}
                                    onChange={this.props.handleChooseFile}
                                    value={this.props.value}
                                    hasError={this.props.hasError}
                                    errorMessage={this.props.errorMessage}/>
                            </form>
                        </div>
                        <div className={"modal-body modal-custom-body " + (this.props.error === 1 ? 'show' : 'hidden')}>
                            <p>アップロードファイルはエラーがあります。<a href={'/downloaderror/' + this.props.filename} className="link-detail">このファイル</a>を参照してください。</p>
                        </div>
                        <div className="modal-footer modal-custom-footer">
                            <button type="button" className="btn btn-primary modal-custom-button" disabled={this.props.error === 1 ? true : false} onClick={(e) => this.props.handleImportSubmit(e)}>{trans('messages.issue_modal_ok')}</button>
                            <button type="button" className="btn bg-purple modal-custom-button" onClick={(e) => this.props.handleImportCancel(e)}>{trans('messages.issue_modal_cancel')}</button>
                        </div>
                    </div>
                    <div className={"progress collapse " + (this.props.status ? this.props.status : '')}>
                        <div className="progress-bar progress-bar-striped active" role="progressbar"
                          aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style={{width : this.props.percent + '%'}}>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}
const mapStateToProps = (state) => {
    return {
        dataSource: state.commonReducer.dataSource,
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        postData: (url, params, headers) => { dispatch(Action.postData(url, params, headers)) }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(ImportCsv)
