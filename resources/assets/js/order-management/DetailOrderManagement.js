import React, { Component } from 'react';
import { connect } from 'react-redux';
import * as Action from '../common/actions';
import axios from 'axios';
import moment from 'moment';
import * as queryString from 'query-string';

import DATEPICKER from '../common/components/form/DATEPICKER';
import Loading from '../common/components/common/Loading';
import ROW_GROUP from '../common/components/table/ROW_GROUP';
import BUTTON from '../common/components/form/BUTTON';
import CHECKBOX  from '../common/components/form/CHECKBOX';
import SELECT from '../common/components/form/SELECT';
import ExportPdfPopup from './containers/ExportPdfPopup';
import MemoPopup from './containers/MemoPopup';
import FormIsDeplay from './containers/FormIsDeplay';
import CancelReasonPopup from './containers/CancelReasonPopup';
import AcceptPopup from './containers/AcceptPopup';
import NotEnoughtCancelPopup from './containers/NotEnoughtCancelPopup';
import RepayTypeCancelPopup from './containers/RepayTypeCancelPopup'
import AddClaimPopup from './containers/AddClaimPopup'
import RepayTypeNotEnoughtCancelPopup from './containers/RepayTypeNotEnoughtCancelPopup'

class DetailOrderManagement extends Component {
    constructor(props) {
        super(props);
        const stringParams = queryString.parse(props.location.search);
        let countParamsFilter = Object.keys(stringParams).length;
        this.tab_active = 'order_info_tab';
        this.curReason = 0;
        this.curCancelReason = 0;
        this.state = {
            full_tab : false,
            hide_tab : false,
            is_delay : true,
            paramsFilter : queryString.stringify(stringParams),
            countParamsFilter : countParamsFilter,
            ...stringParams,
            receive_id: stringParams.receive_id_key,
            cur_log_id: '',
            cur_log_title: '',
            cur_log_contents: ''
        };
        if (_.has(stringParams, 'show_tab')) {
            this.tab_active = stringParams.show_tab;
            this.state.full_tab = true;
            this.state.hide_tab = true;
        }
        this.props.reloadData(detailUrl, this.state);
    }

    componentDidUpdate() {
        if (!_.isEmpty(this.props.dataSource)) {
            $('.loading').hide();
            $('.show-modal-content').customPopover();
        }
    }
    handelCountMemo = () => {
        axios.post(countMemoUrl, [], {headers: {'X-Requested-With': 'XMLHttpRequest'}})
        .then(response => {
            if (response.data.count) {
                $('.label-notify').html(response.data.count).addClass('label-danger');
            } else {
                 $('.label-notify').html('').removeClass('label-danger');;
            }
        }).catch(error => {
            throw(error);
        });
    };
    handleTab = (event, curRecId) => {
        let dataSource = this.props.dataSource;
        let curComp = (dataSource.order_detail.company[curRecId])?dataSource.order_detail.company[curRecId]:[];
        let orderDetail = {
            company: curComp,
            data: dataSource.order_detail.data[curRecId],
            deliCount: dataSource.order_detail.deliCount[curRecId],
            is_return: dataSource.order_detail.is_return,
            url: dataSource.order_detail.url
        }
        this.setState({
            order_detail_tab_index : orderDetail,
            cur_rec_id: curRecId,
        });
    }
    handleIsDelay = (event) => {
        if(event.target.checked) {
            event.preventDefault();
            $('#reason-modal').modal({backdrop: 'static', show: true});
        } else {
            $(".loading").show();
            let params = {receive_id : this.state.receive_id , is_delay : 1, reason: ''};
            axios.post(changeDelayUrl, params, {headers: {'X-Requested-With': 'XMLHttpRequest'}})
            .then(response => {
                if (response.data.status) {
                    this.handleReloadData(detailUrl,{receive_id : this.state.receive_id});
                }
                $(".loading").hide();
            }).catch(error => {
                $(".loading").hide();
                throw(error);
            });
        }

    }
    handleIssue = (event, index) => {
        $('#action-modal').val(index);
        $('#issue-modal').modal({backdrop: 'static', show: true});
    }
    componentWillReceiveProps(nextProps) {
        let dataSource = nextProps.dataSource;
        if ((!_.isEmpty(dataSource))) {
            if (!nextProps.dataSource.data) {
                window.location.href = baseUrl;
            } else {
                let curRecId = dataSource.order_detail.receiver_id[0];
                let curComp  = (dataSource.order_detail.company[curRecId])?dataSource.order_detail.company[curRecId]:[];
                let orderDetail = {
                    company: curComp,
                    data: dataSource.order_detail.data[curRecId],
                    deliCount: dataSource.order_detail.deliCount[curRecId],
                    is_return: dataSource.order_detail.is_return,
                    url: dataSource.order_detail.url
                }
                this.setState({
                    is_delay : dataSource.data.is_delay,
                    order_detail_tab_index : orderDetail,
                    cur_rec_id: curRecId,
                    memo_tab_index : dataSource.memo,
                    mail_histoty_tab_index : dataSource.mail,
                    return_tab_index : dataSource.return,
                    dataClaim : dataSource.dataClaim,
                });
            }
        }
    }
    handleSubmitCancelReason = (reasonId) => {
        this.curReason = reasonId;
        let paramCheck = {reason_id :reasonId, receive_id : this.state.receive_id_key, check_show_repay_cancel_popup : 1};
        $(".loading").show();
        axios.post(cancelReasonUrl, paramCheck , {headers: {'X-Requested-With': 'XMLHttpRequest'}})
        .then(response => {
            if (response.data.status === 5) {
                $('#repay-type-cancel-popup').modal('show');
            } else {
                let param = {reason_id :reasonId, receive_id : this.state.receive_id_key};
                $(".loading").show();
                axios.post(cancelReasonUrl, param , {headers: {'X-Requested-With': 'XMLHttpRequest'}})
                .then(response => {
                    if (response.data.status === 1) {
                        this.handleReloadData(detailUrl,{receive_id : this.state.receive_id});
                    }
                    $(".loading").hide();
                }).catch(error => {
                    $(".loading").hide();
                    throw(error);
                });
            }
            $(".loading").hide();
        }).catch(error => {
            $(".loading").hide();
            throw(error);
        });
    }
    handleSubmitRepayCancel = () => {
        let repayType = $('#repay-type-cancel-popup').find('input[name="repay_type"]:checked').val();

        let param = {reason_id : this.curReason, receive_id : this.state.receive_id_key, repay_type : repayType};
        $(".loading").show();
        axios.post(cancelReasonUrl, param , {headers: {'X-Requested-With': 'XMLHttpRequest'}})
        .then(response => {
            if (response.data.status === 1) {
                this.handleReloadData(detailUrl,{receive_id : this.state.receive_id});
            }
            $(".loading").hide();
        }).catch(error => {
            $(".loading").hide();
            throw(error);
        });
        $('#repay-type-cancel-popup').modal('hide');
    }
    handleSubmitDelivery = (name, data) => {
        $('#access-memo-modal').modal('show');
        if (name.includes('detail')) {
            $('.message-popup').text(trans('messages.message_conf_detail'));
        } else if (name.includes('direct')) {
            $('.message-popup').text(trans('messages.message_conf_direct'));
        }
        this.setState({
            delivery_update : {name: name, ...data}
        });
    }

    handleSubmitDeliveryPopup = () => {
        $(".loading").show();
        let param = this.state.delivery_update;
        param['receive_id'] = this.state.receive_id;
        axios.post(updateDeliveryUrl, param , {headers: {'X-Requested-With': 'XMLHttpRequest'}})
        .then(response => {
            if (response.data.status === 1) {
                $('#access-memo-modal').modal('hide');
                delete this.state.delivery_update;
                this.handleReloadData(detailUrl,{receive_id : this.state.receive_id});
            }
            $(".loading").hide();
        }).catch(error => {
            $(".loading").hide();
            throw(error);
        });
    }

    handleCancelDeliveryPopup = (event) => {
        delete this.state.delivery_update;
        $('#access-memo-modal').modal('hide');
    }

    handleCancelNotEnoughtCancelPopup = () => {
        delete this.state.product_not_cancel;
        delete this.state.status_reason;
        $('#not-cancel-modal').modal('hide');
    }

    handleSubmitNotEnoughtCancelPopup = (cancel_reason) => {
        this.curCancelReason = cancel_reason;
        let paramCheck = {receive_id : this.state.receive_id_key, cancel_reason : cancel_reason, check_show_repay_cancel_popup : 1};
        $(".loading").show();
        axios.post(cancelNotEnought, paramCheck , {headers: {'X-Requested-With': 'XMLHttpRequest'}})
        .then(response => {
            if (response.data.status === 5) {
                $('#repay-type-not-enought-cancel-popup').modal('show');
            } else {
                let param = {receive_id : this.state.receive_id_key, cancel_reason : cancel_reason};
                $(".loading").show();
                axios.post(cancelNotEnought, param , {headers: {'X-Requested-With': 'XMLHttpRequest'}})
                .then(response => {
                    if (response.data.status === 1) {
                        this.handleReloadData(detailUrl,{receive_id : this.state.receive_id});
                    }
                    delete this.state.product_not_cancel;
                    delete this.state.status_reason;
                    $('#not-cancel-modal').modal('hide');
                    $(".loading").hide();
                }).catch(error => {
                    $(".loading").hide();
                    throw(error);
                });
            }
            $(".loading").hide();
        }).catch(error => {
            $(".loading").hide();
            throw(error);
        });
    }

    handleSubmitRepayNotEnoughtCancel = () => {

        let repayType = $('#repay-type-not-enought-cancel-popup').find('input[name="repay_type"]:checked').val();

        let param = {receive_id : this.state.receive_id_key, cancel_reason : this.curCancelReason, repay_type : repayType};
        $(".loading").show();
        axios.post(cancelNotEnought, param , {headers: {'X-Requested-With': 'XMLHttpRequest'}})
        .then(response => {
            if (response.data.status === 1) {
                this.handleReloadData(detailUrl,{receive_id : this.state.receive_id});
            }
            delete this.state.product_not_cancel;
            delete this.state.status_reason;
            $('#not-cancel-modal').modal('hide');
            $('#repay-type-not-enought-cancel-popup').modal('hide');
            $(".loading").hide();
        }).catch(error => {
            $(".loading").hide();
            throw(error);
        });
    }

    handleReloadData = (url,params) => {
        this.props.reloadData(url, params);
    }
    handleLoadDataTabs = (state,url,params) => {
        axios.post(url, params,
            {
                headers: {'X-Requested-With': 'XMLHttpRequest'}
            }
        ).then(response => {
            var stateIndex = state + "_index";
            let paramsState ={};
            paramsState[state + '_has_error'] = false;
            paramsState[stateIndex] = response.data;
            this.setState(paramsState);
        }).catch(error => {
            this.setState({
                [state + '_has_error'] : true
            });
            throw(error);
        });

    }
    handleCancelReason = (event) => {
        axios.post(checkCancelAll, {receive_id: this.state.receive_id},
            {headers: {'X-Requested-With': 'XMLHttpRequest'}}
            ).then(response => {
                if (response.data.status === -1) {
                    this.setState({
                        product_not_cancel: response.data.not_cancel,
                        'status_reason': 1
                    });
                    $('#not-cancel-modal').modal('show');
                } else {
                    $('#cancel-reason-modal').modal('show');
                }
            }).catch(error => {
                throw(error);
            });

    }
    handleAddMemo = (logId) => {
        if (logId !== '') {
            $(".loading").show();
            axios.post(getItemMemoUrl, {log_id: logId},
            {headers: {'X-Requested-With': 'XMLHttpRequest'}}
            ).then(response => {
                let data = response.data.data;
                this.setState({
                    cur_log_id: data.log_id,
                    cur_log_title: data.log_title,
                    cur_log_contents: data.log_contents
                });
                setTimeout(()=>{$('#add-memo-modal').modal({backdrop: 'static', show: true});}, 200);
            }).catch(error => {
                throw(error);
            });
        } else {
            this.setState({
                cur_log_id: '',
                cur_log_title: '',
                cur_log_contents: ''
            });
            $('#add-memo-modal').modal({backdrop: 'static', show: true});
        }
    }

    handleUpdateStatus = (logId, status) => {
        let logStatus = status?0:1;
        $(".loading").show();
        axios.post(updateLogStatusUrl, {log_id: logId, log_status: logStatus},
        {headers: {'X-Requested-With': 'XMLHttpRequest'}}
        ).then(response => {
            if (response.data.status === 1) {
                this.handleLoadDataTabs('memo_tab', getInfoLogUrl, {receive_id : this.props.dataSource.data.receive_id});
            } else {
                throw(response.data.messages);
            }
            // Update notify
            this.handelCountMemo();
        }).catch(error => {
            throw(error);
        });
    }

    handleRedirectEditing = (orderStatus, link) => {
        if (orderStatus < _.status('ORDER_STATUS.SETTLEMENT')) {
            var text = this.state.paramsFilter.replace(/&/g,'|');
            if (this.state.countParamsFilter > 1) {
                window.location.href = link + '&paramsFilter=' + text;
            } else {
                window.location.href = link ;
            }
        } else {
            $.notify({
                icon: 'glyphicon glyphicon-warning-sign',
                message: trans('messages.alert_redirect_editing'),
            },{
                type: 'danger',
                delay: 2000
            });
        }
    }

    handleRedirectAddReturn = (returnNo, link) => {
        if (!returnNo) {
            window.location.href = link;
        } else {
            $.notify({
                icon: 'glyphicon glyphicon-warning-sign',
                message: trans('messages.alert_redirect_add_return'),
            },{
                type: 'danger',
                delay: 2000
            });
        }
    }

    handleReCalculate = () => {
        $(".loading").show();
        axios.post(orderReCalculate, {receive_id : this.state.receive_id}, {headers: {'X-Requested-With': 'XMLHttpRequest'}})
        .then(response => {
            if (response.data.status === 1) {
                this.handleReloadData(detailUrl,{receive_id : this.state.receive_id});
            }
            $(".loading").hide();
        }).catch(error => {
            $(".loading").hide();
            throw(error);
        });
    }
    handleConfirmed = (type) => {
        $(".loading").show();
        axios.post(changeIsComfirmedUrl, {receive_id : this.state.receive_id, type : type}, {headers: {'X-Requested-With': 'XMLHttpRequest'}})
        .then(response => {
            if (response.data.status === 1) {
                this.handleReloadData(detailUrl,{receive_id : this.state.receive_id});
            }
            // if (type === 'cancel') {
            //     if (response.data.status === -1) {
            //         this.setState({
            //             product_not_cancel: response.data.not_cancel,
            //             'status_reason': 1
            //         });
            //         $('#not-cancel-modal').modal('show');
            //     } else {
            //         $('#cancel-reason-modal').modal('show');
            //     }
            // }
            $(".loading").hide();
        }).catch(error => {
            $(".loading").hide();
            throw(error);
        });
    }

    handleMallUpdate = () => {
        $(".loading").show();
        axios.post(orderUpdateFlgMall, {receive_id : this.state.receive_id}, {headers: {'X-Requested-With': 'XMLHttpRequest'}})
        .then(response => {
            if (response.data.status === 1) {
                this.handleReloadData(detailUrl,{receive_id : this.state.receive_id});
            }
            $(".loading").hide();
        }).catch(error => {
            $(".loading").hide();
            throw(error);
        });
    }

    handleChangeUpOPE = (event) => {
        const target = event.target;
        const value  = target.value;
        const log_id = target.getAttribute("data-log-id");
        $(".loading").show();
        axios.post(updateMemoUpOPEUrl, {log_id : log_id, up_ope_cd : value}, {headers: {'X-Requested-With': 'XMLHttpRequest'}})
        .then(response => {
            if (response.data.flg === 1) {
                this.handleReloadData(detailUrl,{receive_id : this.state.receive_id});
            } else {
                throw(response.data.msg);
            }
            $(".loading").hide();
        }).catch(error => {
            $(".loading").hide();
            throw(error);
        });
    }

    handleChangeDate = function(date, date_old, name, event) {
        let dateValue = moment(date);
        $(".loading").show();
        let params = {
            receive_id : this.state.receive_id ,
            delay_priod : dateValue.format('YYYY-MM-DD'),
            date_old: date_old,
            name: name
        };
        axios.post(changeDelayUrl, params, {headers: {'X-Requested-With': 'XMLHttpRequest'}})
        .then(response => {
            if (response.data.status) {
                this.handleReloadData(detailUrl,{receive_id : this.state.receive_id});
            }
            $(".loading").hide();
        }).catch(error => {
            $(".loading").hide();
            throw(error);
        });
    }

    handleChangeBackList = (event) => {
        const target = event.target;
        const value  = target.value;
        $(".loading").show();
        axios.post(changeBackList, {is_black_list : value, receive_id : this.state.receive_id}, {headers: {'X-Requested-With': 'XMLHttpRequest'}})
        .then(response => {
            if (response.data.status) {
                this.handleReloadData(detailUrl,{receive_id : this.state.receive_id});
            }
            $(".loading").hide();
        }).catch(error => {
            $(".loading").hide();
            throw(error);
        });
    }

    handleUpdateClaim = (event) => {
        $(".loading").show();
        axios.post(changeisClaim, {receive_id : this.state.receive_id}, {headers: {'X-Requested-With': 'XMLHttpRequest'}})
        .then(response => {
            if (response.data.status) {
                this.handleReloadData(detailUrl,{receive_id : this.state.receive_id});
            }
            $(".loading").hide();
        }).catch(error => {
            $(".loading").hide();
            throw(error);
        });
    }

    handleClaim = (detailLineNum, itemKey, claimId, deliveryType, event) => {
        let orderDetail = this.state.order_detail_tab_index;
        let newOrderDetail = [];
        let check = false;
        orderDetail.data.map((item, index) => {
            if (typeof item.is_claim === 'undefined') {
                newOrderDetail.push(item);
            }
            if (index === itemKey) {
                if ((typeof orderDetail.data[index + 1] === 'undefined') || (typeof orderDetail.data[index + 1] !== 'undefined' && typeof orderDetail.data[index + 1].is_claim === 'undefined')) {
                    newOrderDetail.push({detail_line_num : detailLineNum, is_claim: true, dataClaim: this.state.dataClaim, claim_id: claimId, delivery_type: deliveryType});
                }
            }
        })
        console.log(newOrderDetail);
        orderDetail.data = newOrderDetail;
        this.setState({
            order_detail_tab_index: orderDetail
        });
    }

    handleChangeClaim = (detailLineNum, event) => {
        const target = event.target;
        const name = target.name;
        const value = target.value;

        $(".loading").show();
        axios.post(changeDetailClaim, {receive_id : this.state.receive_id, claim_id : value, detail_line_num : detailLineNum}, {headers: {'X-Requested-With': 'XMLHttpRequest'}})
        .then(response => {
            if (response.data.status) {
                this.handleReloadData(detailUrl,{receive_id : this.state.receive_id});
            }
            $(".loading").hide();
        }).catch(error => {
            $(".loading").hide();
            throw(error);
        });
    }

    handleAddClaim = () => {
        $('#add-claim-modal').modal('show');
    }

    handleSubmitAddClaimPopup = (claim_content) => {
        $(".loading").show();
        axios.post(changeAddClaim, {claim_content : claim_content}, {headers: {'X-Requested-With': 'XMLHttpRequest'}})
        .then(response => {
            if (response.data.status) {
                $('#add-claim-modal').modal('hide');
                this.handleReloadData(detailUrl,{receive_id : this.state.receive_id});
            }
            $(".loading").hide();
        }).catch(error => {
            $(".loading").hide();
            throw(error);
        });
    }

    handleCancelAddClaimPopup = () => {
        $('#add-claim-modal').modal('hide');
    }

    render() {
        let dataSource = this.props.dataSource;
        var nf = new Intl.NumberFormat();
        let index = 0;
        let receiverFullName   = '';
        let receiverTelNum     = '';
        let receiverZipCode    = '';
        let receiverPrefecture = '';
        let receiverCity       = '';
        let receiverSubAddress = '';
        let deli13 = 0;
        let deli2 = 0;
        if (!_.isEmpty(this.state.order_detail_tab_index)) {
            if (!_.isUndefined(this.state.order_detail_tab_index.data)) {
                let infoCustomer   = this.state.order_detail_tab_index.data[0];
                receiverFullName   = infoCustomer.receiver_full_name;
                receiverTelNum     = infoCustomer.receiver_tel_num;
                receiverZipCode    = infoCustomer.receiver_zip_code;
                receiverPrefecture = infoCustomer.receiver_prefecture;
                receiverCity       = infoCustomer.receiver_city;
                receiverSubAddress = infoCustomer.receiver_sub_address;
            }
            if (!_.isUndefined(this.state.order_detail_tab_index.deliCount)) {
                deli13 = this.state.order_detail_tab_index.deliCount.deli13;
                deli2 = this.state.order_detail_tab_index.deliCount.deli2;
            }
        }
    return (
        (!_.isEmpty(dataSource) && dataSource.data) &&
        <div>
            <Loading className="loading" />
            <div className="row">
                <div className="col-md-12">
                    <div className="box box-primary">
                        <div className="box-body order-detail-padding">
                            <div className="row">
                            {/* Left box  , Position 1 */}
                                <div className="col-md-7 clear-padding-right">
                                    <div className="row">
                                        <div className={"col-md-6 col-xs-12" + (dataSource.link_mall?" text-top-align":"")}>
                                            <ROW_GROUP
                                               key='name_jp'
                                               classNameLabel={'col-sm-4 col-xs-4 clear-padding-right'}
                                               classNameField={'col-sm-8 col-xs-8 clear-padding'}
                                               label={ trans('messages.mapping_mall')}
                                               contents={dataSource.data['name_jp']} />
                                        </div>
                                        <div className="col-md-6">
                                            <div className="form-group">
                                            <div className="col-sm-5 col-xs-5 clear-padding">
                                                {dataSource.link_mall &&
                                                <a target="_blank" className="img-mall-padding" rel="noreferrer" href={dataSource.link_mall}>
                                                    <img src="/img/mall_order.jpg" width="35" />
                                                </a>
                                                }
                                            </div>
                                            <div className="col-sm-7 col-xs-7 clear-padding">
                                                {((dataSource.data.order_status === _.status('ORDER_STATUS.DELIVERY') && dataSource.data.order_sub_status === _.status('ORDER_SUB_STATUS.DONE')) ||
                                                (dataSource.data.order_status === _.status('ORDER_STATUS.SETTLEMENT') && (
                                                    dataSource.data.order_sub_status === _.status('ORDER_SUB_STATUS.DOING')
                                                    || dataSource.data.order_sub_status === _.status('ORDER_SUB_STATUS.NEW')
                                                    || dataSource.data.order_sub_status === _.status('ORDER_SUB_STATUS.DONE')
                                                ))) &&
                                                <a target="_blank" className="img-mall-padding" rel="noreferrer" href={dataSource.linkReceipt} className="btn btn-warning btn-sm text-bold margin-element ">
                                                    {trans('messages.btn_receipt')}
                                                </a>
                                                }
                                            </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="row">
                                        <div className="col-md-6 col-xs-12">
                                            <ROW_GROUP
                                               key='received_order_id'
                                               classNameLabel={'col-sm-4 col-xs-4 clear-padding-right'}
                                               classNameField={'col-sm-8 col-xs-8 clear-padding'}
                                               label={ trans('messages.received_order_id')}
                                               contents={dataSource.data['received_order_id']} />
                                        </div>
                                        <div className="col-md-6">
                                            <ROW_GROUP
                                               key='receive_id'
                                               classNameLabel={'col-sm-5 col-xs-5 control-label clear-padding-right'}
                                               classNameField={'col-sm-7 col-xs-7 clear-padding'}
                                               label={ trans('messages.receive_id')}
                                               contents={dataSource.data['receive_id']} />
                                        </div>
                                    </div>
                                    <div className="row">
                                        <div className="col-md-6 col-xs-12">
                                            <ROW_GROUP
                                               key='order_date'
                                               classNameLabel={'col-sm-4 col-xs-4 control-label clear-padding-right'}
                                               classNameField={'col-sm-8 col-xs-8 clear-padding'}
                                               label={ trans('messages.order_date')}
                                               contents={dataSource.data['order_date']} />
                                        </div>

                                        <div className="col-md-6 col-xs-12">
                                            <ROW_GROUP
                                               key='in_date'
                                               classNameLabel={'col-sm-5 col-xs-5 control-label clear-padding-right'}
                                               classNameField={'col-sm-7 col-xs-7 clear-padding'}
                                               label={ trans('messages.in_date')}
                                               contents={dataSource.data['in_date']} />
                                        </div>
                                    </div>
                                    <hr className="hr-margin"/>
                                    <div className="row">
                                        <div className="col-md-6 col-xs-12">
                                            <ROW_GROUP
                                               key='full_name'
                                               classNameLabel={'col-sm-5 col-xs-5 control-label clear-padding-right'}
                                               classNameField={'col-sm-7 col-xs-7 clear-padding'}
                                               label={ trans('messages.full_name')}
                                               contents={dataSource.data['full_name']} />
                                        </div>
                                        <div className="col-md-6 col-xs-12">
                                            <ROW_GROUP
                                               key='company_name'
                                               classNameLabel={'col-sm-5 col-xs-5 control-label clear-padding-right'}
                                               classNameField={'col-sm-7 col-xs-7 clear-padding'}
                                               label={ trans('messages.company_name')}
                                               contents={dataSource.data['company_name']} />
                                        </div>

                                        <div className="col-md-6 col-xs-12">
                                            <ROW_GROUP
                                               key='zip_code'
                                               classNameLabel={'col-sm-5 col-xs-5 control-label clear-padding-right'}
                                               classNameField={'col-sm-7 col-xs-7 clear-padding'}
                                               label={ trans('messages.zip_code')}
                                               contents={'〒' + (dataSource.data['zip_code'] ? dataSource.data['zip_code'] : '')} />
                                        </div>
                                    </div>
                                    <div className="row">
                                        <div className="col-md-12 text-center">
                                            {
                                                (dataSource.data['prefecture'] ? dataSource.data['prefecture'] : '') + ' ' +
                                                (dataSource.data['city'] ? dataSource.data['city'] : '') + ' ' +
                                                (dataSource.data['sub_address'] ? dataSource.data['sub_address'] : '')
                                            }
                                        </div>
                                    </div>
                                    <div className="row">
                                        <div className="col-md-6 col-xs-12">
                                            <ROW_GROUP
                                               key='tel_num'
                                               classNameLabel={'col-sm-5 col-xs-5 control-label clear-padding-right'}
                                               classNameField={'col-sm-7 col-xs-7 clear-padding'}
                                               label={ trans('messages.tel_num')}
                                               contents={dataSource.data['tel_num']} />
                                        </div>
                                        <div className="col-md-6 col-xs-12">
                                            <ROW_GROUP
                                               key='fax_num'
                                               classNameLabel={'col-sm-5 col-xs-5 control-label clear-padding-right'}
                                               classNameField={'col-sm-7 col-xs-7 clear-padding'}
                                               label={trans('messages.fax_num')}
                                               contents={dataSource.data['fax_num']} />
                                        </div>
                                    </div>
                                    <div className="row">
                                        <div className="col-md-6 col-xs-12">
                                            <ROW_GROUP
                                               key='urgent_tel_num'
                                               classNameLabel={'col-sm-5 col-xs-5 control-label clear-padding-right'}
                                               classNameField={'col-sm-7 col-xs-7 clear-padding'}
                                               label={ trans('messages.urgent_tel_num')}
                                               contents={dataSource.data['urgent_tel_num']} />
                                        </div>
                                    </div>
                                    <div className="row">
                                        <div className="col-md-12">
                                            <ROW_GROUP
                                               key='email'
                                               classNameLabel={'col-sm-3 col-xs-3 control-label clear-padding-right'}
                                               classNameField={'col-sm-7 col-xs-7 clear-padding'}
                                               label={ trans('messages.email')}
                                               contents={dataSource.data['email']} />
                                        </div>
                                    </div>
                                </div>
                                {/* Right box , Position 2 */}
                                <div className="col-md-5">
                                    <div className="row">
                                        <div className="col-md-6 col-xs-12 clear-padding-right">
                                            <ROW_GROUP
                                               key='status_name'
                                               classNameLabel={'col-sm-6  col-xs-6 control-label clear-padding-right'}
                                               classNameField={'col-sm-6 col-xs-6 text-danger text-bold clear-padding'}
                                               label={trans('messages.order_status') + "："}
                                               contents={
                                                    (dataSource.data['status_name']?
                                                    dataSource.data['status_name'] :
                                                    trans('messages.order_status_cancel_mess'))
                                                    +
                                                    (dataSource.data['sub_status_name']?
                                                    "：" + dataSource.data['sub_status_name']: '')
                                                } />
                                        </div>
                                        { (dataSource.data['order_status'] >= _.status('ORDER_STATUS.DELIVERY')) &&
                                        <div className="col-md-6 col-xs-12">
                                            <p className="text-center text-danger text-new-line">{trans('messages.alert_cannot_change_order')}</p>
                                        </div>}
                                    </div>
                                    <div className="row">
                                        <div className="col-md-12">
                                        <div className="col-md-12">
                                            <p className="text-danger text-bold error-new-line">
                                            {
                                                (dataSource.data['err_text']) ? dataSource.data['err_text'] : ''
                                            }
                                            </p>
                                        </div>
                                        </div>
                                    </div>
                                    <hr className="hr-margin"/>
                                    <div className="row">
                                        <div className="col-md-12 col-xs-12 pull-right" >
                                        {(dataSource.data['is_recalculate'] === 1) &&
                                            <BUTTON  style={{'marginBottom':'10px'}} name="btn_calculate" className="btn btn-primary btn-sm text-bold pull-right" onClick={() => {this.handleReCalculate()}}>
                                                {trans('messages.btn_re_calculate')}
                                            </BUTTON>
                                        }
                                        {(dataSource.data['is_confirmed'] === 1) &&
                                            [
                                            <BUTTON
                                                style={{'marginBottom':'10px'}} key="2"
                                                name="btn_comfirm" className="btn btn-info btn-sm text-bold pull-right  margin-element"
                                                onClick={() => {this.handleConfirmed('confirm')}}>
                                                {trans('messages.btn_confirmed')}
                                            </BUTTON>,
                                            <BUTTON style={{'marginBottom':'10px'}} key="1" name="btn_cancel" className="btn btn-warning btn-sm text-bold pull-right margin-element " onClick={() => {this.handleCancelReason()}}>
                                                {trans('messages.btn_cancel_memo')}
                                            </BUTTON>
                                            ]
                                        }
                                        </div>

                                        <div className="col-md-12 col-xs-12">
                                            <ROW_GROUP
                                               key='payment_name'
                                               classNameLabel={'col-sm-3 col-xs-3 control-label clear-padding-right'}
                                               classNameField={'col-sm-9 col-xs-9 clear-padding'}
                                               label={ trans('messages.payment_name')}
                                               contents={dataSource.data['payment_name']} />
                                        </div>
                                    </div>
                                    <div className="row">
                                        <div className="col-md-6 col-xs-12">
                                            <ROW_GROUP
                                               key='sub_total'
                                               classNameLabel={'col-sm-6 col-xs-6 control-label clear-padding-right'}
                                               classNameField={'col-sm-6 col-xs-6 line-bottom clear-padding text-right'}
                                               label={ trans('messages.total_sub')}
                                               contents={'￥' + nf.format(dataSource.data['goods_price'])} />
                                        </div>
                                        <div className="col-md-6 col-xs-12">
                                            <ROW_GROUP
                                               key='pay_charge_discount'
                                               classNameLabel={'col-sm-6 col-xs-6 control-label clear-padding-right'}
                                               classNameField={'col-sm-6 col-xs-6 line-bottom clear-padding text-right'}
                                               label={ trans('messages.pay_charge_discount')}
                                               contents={'￥' + nf.format(dataSource.data['pay_charge_discount'])} />
                                        </div>
                                    </div>
                                    <div className="row">
                                        <div className="col-md-6 col-xs-12">
                                            <ROW_GROUP
                                               key='ship_charge'
                                               classNameLabel={'col-sm-6 col-xs-6 control-label clear-padding-right'}
                                               classNameField={'col-sm-6 col-xs-6 line-bottom clear-padding text-right'}
                                               label={ trans('messages.ship_charge')}
                                               contents={'￥' + nf.format(dataSource.data['ship_charge'])} />
                                        </div>
                                        <div className="col-md-6 col-xs-12">
                                            <ROW_GROUP
                                               key='discount'
                                               classNameLabel={'col-sm-6 col-xs-6 control-label clear-padding-right'}
                                               classNameField={'col-sm-6 col-xs-6 line-bottom clear-padding text-right'}
                                               label={ trans('messages.discount')}
                                               contents={'￥' + nf.format(dataSource.data['discount'])} />
                                        </div>
                                    </div>
                                    <div className="row">
                                        <div className="col-md-6 col-xs-12">
                                            <ROW_GROUP
                                               key='pay_after_charge'
                                               classNameLabel={'col-sm-6 col-xs-6 control-label clear-padding-right'}
                                               classNameField={'col-sm-6 col-xs-6 line-bottom clear-padding text-right'}
                                               label={ trans('messages.pay_after_charge')}
                                               contents={'￥' + nf.format(dataSource.data['pay_after_charge'])} />
                                        </div>
                                        <div className="col-md-6 col-xs-12">
                                            <ROW_GROUP
                                               key='used_point'
                                               classNameLabel={'col-sm-6 col-xs-6 control-label clear-padding-right'}
                                               classNameField={'col-sm-6 col-xs-6 line-bottom clear-padding text-right'}
                                               label={ trans('messages.used_point')}
                                               contents={'￥' + nf.format(dataSource.data['used_point'])} />
                                        </div>
                                    </div>
                                    <div className="row">
                                        <div className="col-md-6 col-xs-12">
                                            <ROW_GROUP
                                               key='pay_charge'
                                               classNameLabel={'col-sm-6 col-xs-6 control-label clear-padding-right'}
                                               classNameField={'col-sm-6 col-xs-6 line-bottom clear-padding text-right'}
                                               label={ trans('messages.pay_charge')}
                                               contents={'￥' + nf.format(dataSource.data['pay_charge'])} />
                                        </div>
                                        <div className="col-md-6 col-xs-12">
                                            <ROW_GROUP
                                               key='used_coupon'
                                               classNameLabel={'col-sm-6 col-xs-6 control-label clear-padding-right'}
                                               classNameField={'col-sm-6 col-xs-6 line-bottom clear-padding text-right'}
                                               label={ trans('messages.used_coupon')}
                                               contents={'￥' + nf.format(dataSource.data['used_coupon'])} />
                                        </div>
                                    </div>
                                    <div className="row">
                                        <div className="col-md-6 col-xs-12">
                                            <ROW_GROUP
                                               key='total_price'
                                               classNameLabel={'col-sm-6 col-xs-6 control-label clear-padding-right'}
                                               classNameField={'col-sm-6 col-xs-6 line-bottom clear-padding text-right'}
                                               label={ trans('messages.total_price')}
                                               contents={'￥' + nf.format(dataSource.data['total_price'])} />
                                        </div>
                                        <div className="col-md-6">
                                            <ROW_GROUP
                                               key='request_price'
                                               classNameLabel={'col-sm-6 col-xs-6 control-label clear-padding-right'}
                                               classNameField={'col-sm-6 col-xs-6 line-bottom clear-padding text-right'}
                                               label={ trans('messages.request_price')}
                                               contents={'￥' + nf.format(dataSource.data['request_price'])} />
                                        </div>
                                    </div>
                                    <div className="row">
                                        <div className="col-md-6 col-xs-12">
                                            <div className="form-group">
                                                <label className="col-sm-6 col-xs-6 control-label clear-padding-right"><img src="/img/back_list_icon.png" width="35" /></label>
                                                <div className="col-sm-6 col-xs-6 clear-padding" dangerouslySetInnerHTML={{__html: dataSource.data['backlist_rank_string']}} >
                                                </div>
                                            </div>
                                        </div>
                                        <div className="col-md-6 margin-top-10">
                                            <SELECT
                                                name="is_black_list"
                                                labelTitle={trans('messages.backlist_comment')}
                                                className="form-control input-sm"
                                                fieldGroupClass="col-md-7"
                                                labelClass="col-md-5"
                                                onChange={this.handleChangeBackList}
                                                defaultValue={dataSource.data.is_black_list}
                                                options={dataSource.bankListOpt}/>
                                        </div>
                                    </div>


                                </div>
                            </div>
                            <hr className="hr-margin"/>
                            <div className="row">
                                <div className="col-md-3-5">
                                    <ROW_GROUP
                                        classNameLabel={'col-md-12 control-label'}
                                        classNameField={'col-md-12 box-info-order text-new-line'}
                                        label={ trans('messages.customer_question')}
                                        contents={dataSource.data['customer_question']} />
                                </div>
                                <div className="col-md-3-5">
                                    <ROW_GROUP
                                        classNameLabel={'col-md-12 control-label'}
                                        classNameField={'col-md-12 box-info-order text-new-line'}
                                        label={ trans('messages.shop_answer')}
                                        contents={dataSource.data['shop_answer']} />
                                </div>
                                <div className="col-md-3-5">
                                    <ROW_GROUP
                                        classNameLabel={'col-md-12 control-label'}
                                        classNameField={'col-md-12 box-info-order text-new-line'}
                                        label={ trans('messages.delivery_instrustions_col')}
                                        contents={dataSource.data['delivery_instrustions_col']} />
                                </div>
                                <div className="col-md-1-5 text-center">
                                    <a onClick={(e) => this.handleUpdateClaim()}>
                                        <img src={dataSource.data['img_is_claim']} height="105" style={{"maxWidth" : "100%"}}/>
                                    </a>
                                </div>
                            </div>
                            <hr className="hr-margin"/>
                            <div className="row row-same-height">
                                <div className="col-md-6">
                                    <div className="row margin-top-3px tool-download">
                                    {/* / Display button PDF download */}

                                        <div className="col-md-3 col-sm-3 col-xs-6 text-center xs-mb">
                                            <BUTTON className="btn btn-info" onClick={(e) => this.handleIssue(e, 'estimate')}>
                                             <i className="fa fa-download"></i>
                                             <span className="text-center">{trans('messages.btn_download_order')}</span>
                                            </BUTTON>
                                        </div>
                                        <div className="col-md-3 col-sm-3 col-xs-6 text-center xs-mb">
                                            <BUTTON className="btn btn-info" onClick={(e) => this.handleIssue(e, 'invoice')}>
                                             <i className="fa fa-download"></i>
                                             <span className="text-center">{trans('messages.btn_download_make_order')}</span>
                                            </BUTTON>
                                        </div>
                                        <div className="col-md-3 col-sm-3 col-xs-6 text-center xs-mb">
                                            <BUTTON className="btn btn-info" onClick={(e) => this.handleIssue(e, 'order-detail')}>
                                             <i className="fa fa-download"></i>
                                             <span className="text-center">{trans('messages.btn_download_problem')}</span>
                                            </BUTTON>
                                        </div>
                                        <div className="col-md-3 col-sm-3 col-xs-6 text-center xs-mb">
                                            <BUTTON
                                                className="btn btn-success"
                                                onClick={(e) => this.handleRedirectEditing(
                                                        dataSource.data['order_status'],
                                                        orderEditingUrl + "?receive_id=" + dataSource.data['receive_id']
                                                    )}
                                                disabled={(dataSource.data['order_status'] < _.status('ORDER_STATUS.SETTLEMENT'))?false:true}
                                                >
                                                <span className="text-center">{trans('messages.btn_detail_editing')}</span>
                                            </BUTTON>
                                        </div>
                                    </div>
                                    <div className={dataSource.data.is_delay==1?"delay-box-checked":""}>
                                    <hr className={dataSource.data.is_delay==1?"hr-margin delay-line-checked":"hr-margin"}/>
                                    <div className="row">
                                        <div className="col-md-3 col-xs-12">
                                            <ROW_GROUP
                                               key='is_delay'
                                               classNameLabel={'col-sm-7 col-xs-7 control-label clear-padding-right'}
                                               classNameField={'col-sm-5 col-xs-5 clear-padding'}
                                               label={ trans('messages.check')}
                                               >
                                                  <label className="control nice-checkbox">
                                                    <CHECKBOX
                                                        name="is_delay"
                                                        checked={dataSource.data.is_delay}
                                                        onChange={this.handleIsDelay}/>
                                                    <div className="nice-icon-check"></div>
                                                  </label>
                                            </ROW_GROUP>
                                        </div>
                                        <div className="col-md-5 col-xs-12">
                                            <ROW_GROUP
                                               key='hold_priod'
                                               classNameLabel={'col-sm-4 col-xs-4 control-label clear-padding'}
                                               classNameField={'col-sm-8 col-xs-8 clear-padding'}
                                               label={ trans('messages.hold_priod')}>
                                               {
                                                    dataSource.data['delay_priod']
                                                    ?
                                                    <DATEPICKER
                                                        placeholderText="YYYY-MM-DD"
                                                        dateFormat="YYYY-MM-DD"
                                                        locale="ja"
                                                        minDate={moment(dataSource.data['delay_priod_limit'])}
                                                        name="ship_wish_date"
                                                        className="form-control input-sm"
                                                        fieldGroupClass="col-md-12"
                                                        selected={dataSource.data['delay_priod'] ? moment(dataSource.data['delay_priod']) : null}
                                                        onChange={(date, name, e) => this.handleChangeDate(date, dataSource.data['delay_priod'], 'delay_priod', e)} />
                                                    :
                                                    '...'
                                               }
                                           </ROW_GROUP>
                                        </div>
                                        <div className="col-md-4 col-xs-12">
                                            <ROW_GROUP
                                               key='hold_by'
                                               classNameLabel={'col-sm-5 col-xs-5 control-label clear-padding-left'}
                                               classNameField={'col-sm-7 col-xs-7 clear-padding'}
                                               label={ trans('messages.hold_by')}
                                               contents={dataSource.data['hold_by']} />
                                        </div>
                                    </div>
                                    <hr className="hr-margin"/>
                                    <div className="row">
                                        <div className="col-md-12 col-xs-12">
                                            <ROW_GROUP
                                               key='delay_reason'
                                               classNameLabel={'col-sm-2 col-xs-2 control-label clear-padding-right'}
                                               classNameField={'col-sm-10 col-xs-10 clear-padding'}
                                               label={ trans('messages.delay_reason')}
                                               contents={dataSource.data['delay_reason'] ? dataSource.data['delay_reason']  : '...'} />
                                        </div>
                                    </div>
                                    <hr className={dataSource.data.is_delay==1?"hr-margin delay-line-checked":"hr-margin"}/>
                                    </div>
                                    {/* Bottom */}
                                    <div className="row custom-tabs">
                                        {/* Left box  , Position 3 */}
                                        <div className="col-md-12">
                                            <div className="nav-tabs-custom clear-margin-navs">
                                              <ul className="nav nav-tabs">
                                                {
                                                    dataSource.order_detail.receiver_id.map((recId, i) => {
                                                        return (
                                                            <li key={'li' + recId} className={this.state.cur_rec_id === recId ? ' active' : '' }>
                                                                <a className={(dataSource.order_detail.receiver_err.includes(recId))?"text-danger-important":""} onClick={(e) => this.handleTab(e,recId)} href="#order_info_tab" data-toggle="tab">{trans('messages.order_receiver_tab') + ' ' + (i+1)}</a>
                                                            </li>
                                                        )
                                                    })
                                                }
                                              </ul>
                                              <div className="tab-content">
                                                <div className={this.tab_active !== 'order_detail_tab' ? 'active tab-pane' : 'tab-pane'} id="order_info_tab">
                                                    <div className="row">
                                                        <div className="col-md-2-5"><p className="margin-top-3px"><b>{trans('messages.multi_ship')}</b></p></div>
                                                        <div className="col-md-9">
                                                            <p className="margin-top-3px">
                                                                {dataSource.data.delivery_type}
                                                                <span className="text-danger inline-text-margin text-bold">
                                                                    {dataSource.data.delivery_type > 1 ? trans('messages.multi_ship_alert') : ''}
                                                                </span>
                                                            </p>
                                                        </div>
                                                    </div>
                                                    <div className="row">
                                                        <div className="col-md-2-5"><p className="margin-top-3px"><b>{trans('messages.reciever_name')}</b></p></div>
                                                        <div className="col-md-3-5"><p className="margin-top-3px">{receiverFullName}</p></div>

                                                        <div className="col-md-2-5"><p className="margin-top-3px"><b>{trans('messages.tel_num')}</b></p></div>
                                                        <div className="col-md-3-5"><p className="margin-top-3px">{receiverTelNum}</p></div>
                                                    </div>
                                                    <div className="row">
                                                        <div className="col-md-2-5"><p className="margin-top-3px"><b>{trans('messages.receiver_postal')}</b></p></div>
                                                        <div className="col-md-9"><p className="margin-top-3px">{'〒' + receiverZipCode}</p></div>
                                                    </div>
                                                    <div className="row">
                                                        <div className="col-md-12 text-center">
                                                            <p className="margin-top-3px">
                                                                {receiverPrefecture}
                                                                {receiverCity}
                                                                {receiverSubAddress}
                                                            </p>
                                                        </div>
                                                    </div>
                                                </div>
                                              </div>
                                              {/* /.tab-content */}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                {/* Right box  , Position 4 */}
                                <div className="col-md-6 clear-padding-left">
                                    <div className="nav-tabs-custom max-height-col clear-margin-navs">
                                        <ul className="nav nav-tabs">
                                          <li className="active"><a href="#memo_tab" data-toggle="tab">{trans('messages.memo_tab')}</a></li>
                                          <li><a href="#mail_histoty_tab" data-toggle="tab">{trans('messages.mail_histoty_tab')}</a></li>
                                          <li><a href="#return_histoty_tab" data-toggle="tab">{trans('messages.return_histoty_tab')}</a></li>
                                        </ul>
                                        <div className="tab-content">
                                            <div className="active tab-pane" id="memo_tab">
                                                <div className="row row-spacing-double">
                                                    <div className="col-md-12">
                                                    <div className="table-responsive ">
                                                    <div className="table-scroll">
                                                        <table className="table table-striped table-custom">
                                                            <thead>
                                                                <tr>
                                                                    <th className="text-center">{trans('messages.memo_status')}</th>
                                                                    <th className="text-center">{trans('messages.memo_in_date')}</th>
                                                                    <th className="text-center">{trans('messages.memo_up_date')}</th>
                                                                    <th className="text-center">{trans('messages.memo_title')}</th>
                                                                    <th className="text-center">{trans('messages.memo_register')}</th>
                                                                    <th className="text-center">{trans('messages.memo_updater')}</th>
                                                                    <th className="text-center">{trans('messages.memo_detail')}</th>
                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                            {
                                                                (!_.isEmpty(this.state.memo_tab_index)
                                                                    && !_.isEmpty(this.state.memo_tab_index.data)) ?

                                                                this.state.memo_tab_index.data.map((item) => {
                                                                let inDate = moment.utc(item.in_date, "YYYY-MM-DD HH:mm").format("MM/DD HH:mm");
                                                                let upDate = moment.utc(item.up_date, "YYYY-MM-DD HH:mm").format("MM/DD HH:mm");;
                                                                let status_mess  = '';
                                                                let status_class = '';
                                                                if (item.log_status === 1) {
                                                                    status_mess  = '済';
                                                                    status_class = 'bg-blue';
                                                                } else {
                                                                    status_mess  = '未';
                                                                    status_class = 'bg-red';
                                                                }
                                                                return (
                                                                <tr key={item.log_id}>
                                                                <td className="text-center"><a onClick={()=>{this.handleUpdateStatus(item.log_id, item.log_status)}}><span className={"label " + status_class}>{status_mess}</span></a></td>
                                                                    <td>{inDate}</td>
                                                                    <td>{upDate}</td>
                                                                    <td className="limit-char-90" title={item.log_title}>{item.log_title}</td>
                                                                    <td>{item.tantou_last_name}</td>
                                                                    <td className="max-width-70">
                                                                        <SELECT
                                                                            key={"memo_up_ope_cd" + item.log_id}
                                                                            name={"memo_up_ope_cd"}
                                                                            className="form-control input-sm"
                                                                            groupClass=" "
                                                                            data-log-id={item.log_id}
                                                                            onChange={this.handleChangeUpOPE}
                                                                            value={item.up_ope_cd}
                                                                            options={dataSource.tantou}
                                                                            errorPopup={true}/>
                                                                    </td>
                                                                    <td className="text-center">
                                                                        <a className="btn btn-info btn-xs" onClick={() => {this.handleAddMemo(item.log_id)}}>{trans('messages.btn_detail')}</a>
                                                                    </td>
                                                                </tr> )
                                                                })
                                                                :<tr><td colSpan="10" className={"text-center" + (this.state.memo_tab_has_error?' text-red':'')}>{
                                                                    this.state.memo_tab_has_error?trans('messages.error_reload_data'):trans('messages.no_data')
                                                                }</td></tr>
                                                            }
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                    </div>
                                                    </div>
                                                </div>
                                                <div className="row">
                                                    <div className="col-md-12">
                                                        <BUTTON className="btn btn-success btn-sm pull-right" onClick={() => {this.handleAddMemo('')}}>
                                                            {trans('messages.btn_add_memo')}
                                                        </BUTTON>
                                                    </div>
                                                </div>
                                            </div>
                                            {/* /.tab-pane */}
                                            <div className="tab-pane" id="mail_histoty_tab">
                                                <div className="row row-spacing-double">
                                                    <div className="col-md-12">
                                                    <div className="table-responsive ">
                                                    <div className="table-scroll">
                                                        <table className="table table-striped table-custom">
                                                            <thead>
                                                                <tr>
                                                                    <th className="text-center">{trans('messages.mail_send_date')}</th>
                                                                    <th className="text-center">{trans('messages.mail_send_time')}</th>
                                                                    <th className="text-center">{trans('messages.mail_send_status')}</th>
                                                                    <th className="text-center">{trans('messages.mail_send_subject')}</th>
                                                                    <th className="text-center">{trans('messages.mail_detail')}</th>
                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                            {
                                                                (!_.isEmpty(this.state.mail_histoty_tab_index)
                                                                    && !_.isEmpty(this.state.mail_histoty_tab_index.data)) ?
                                                                this.state.mail_histoty_tab_index.data.map((item,i) => {
                                                                    let sendDate = '';
                                                                    let sendTime = '';
                                                                    if (item.receive_date !== null) {
                                                                        sendDate = moment(item.receive_date).format("MM/DD");
                                                                        sendTime = moment(item.receive_date).format("HH:mm");
                                                                    }
                                                                    let url = '';
                                                                    let key = '';
                                                                    if (item.type === 'receive_mail') {
                                                                        key = item.rec_mail_index;
                                                                        url = receiveMailSaveUrl + "?rec_mail_index_key=" + item.rec_mail_index;
                                                                    } else {
                                                                        key = item.receive_order_id + item.order_status_id + item.order_sub_status_id;
                                                                        url = sendMailUrl + "?receive_order_id=" + item.receive_order_id + "&order_status_id=" + item.order_status_id + "&order_sub_status_id=" + item.order_sub_status_id + "&operater_send_index=" + item.operater_send_index;
                                                                    }
                                                                    return (
                                                                    <tr key={"mail" + i}>
                                                                        <td className="text-center">{sendDate}</td>
                                                                        <td className="text-center">{sendTime}</td>
                                                                        <td className="text-center">{item.status}</td>
                                                                        <td className="limit-char"><a  title={item.mail_subject}>{item.mail_subject}</a></td>
                                                                        <td className="text-center">
                                                                            <a
                                                                                className="btn btn-info btn-xs"
                                                                                href={url}
                                                                            >
                                                                                <i className="fa fa-envelope-o" aria-hidden="true"></i>
                                                                            </a>
                                                                        </td>
                                                                    </tr>)
                                                                })
                                                                :<tr><td colSpan="10" className="text-center">{
                                                                    trans('messages.no_data')
                                                                }</td></tr>
                                                            }

                                                            </tbody>
                                                        </table>
                                                    </div>
                                                    </div>
                                                </div>
                                                </div>
                                                <div className="row">
                                                    <div className="col-md-12">
                                                        <a
                                                            className="btn btn-success btn-sm pull-right"
                                                            href={editSendMailUrl+"?receive_order_id=" + dataSource.data.received_order_id}
                                                            target="_blank"
                                                            title={trans('messages.btn_move_send_mail')}>
                                                            {trans('messages.btn_move_send_mail')}
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>
                                            {/* /.tab-pane */}
                                            <div className="tab-pane" id="return_histoty_tab">
                                                <div className="row">
                                                    <div className="col-md-12">
                                                        <table className="table table-striped table-custom">
                                                            <thead>
                                                                <tr>
                                                                    <th className="text-center">{trans('messages.return_date')}</th>
                                                                    <th className="text-center">{trans('messages.incharge_person')}</th>
                                                                    <th className="text-center">{trans('messages.return_reason')}</th>
                                                                    <th className="text-center">{trans('messages.return_detail')}</th>
                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                            {
                                                                (!_.isEmpty(this.state.return_tab_index)
                                                                        && !_.isEmpty(this.state.return_tab_index.data))?
                                                                this.state.return_tab_index.data.map((item) => {
                                                                    let returnDate = moment(item.in_date).format('YY/MM/DD');
                                                                    return (
                                                                        <tr key={"reorder" + item.reorder_id}>
                                                                            <td>{returnDate}</td>
                                                                            <td>{item.tantou_last_name}</td>
                                                                            <td>{item.re_order_type}</td>
                                                                            <td className="text-center">
                                                                                <a
                                                                                    className="btn btn-info btn-xs"
                                                                                    href={refundDetailSaveUrl + '?received_order_id=' + dataSource.data.received_order_id}
                                                                                >
                                                                                    {trans('messages.btn_detail')}
                                                                                </a>
                                                                            </td>
                                                                        </tr>
                                                                    );
                                                                })
                                                                :<tr><td colSpan="4" className="text-center">{
                                                                    trans('messages.no_data')
                                                                }</td></tr>
                                                            }
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                        {/* /.tab-pane */}
                                        </div>
                                      {/* /.tab-content */}
                                    </div>
                                </div>
                            </div>
                            {/* /.tab-pane */}
                            <div className="active tab-pane" id="order_detail_tab">
                                <div className="row">
                                    <div className="col-md-12">
                                        <div className="table-responsive">
                                            <table className="table table-striped table-custom">
                                                {/* Display header title */}
                                                <thead>
                                                    <tr>
                                                        <th width="10%" className="text-center">{trans('messages.no')}</th>
                                                        <th width="30%" className="text-center">{trans('messages.product_name')}</th>
                                                        <th width="10%" className="text-center">{trans('messages.first_id')}</th>
                                                        <th width="10%" className="text-center">{trans('messages.unit_price')}</th>
                                                        <th width="10%" className="text-center">{trans('messages.quantity_detail')}</th>
                                                        <th width="10%" className="text-center">{trans('messages.sub_total')}</th>
                                                        <th width="10%" className="text-center">{trans('messages.product_status')}</th>
                                                        {(!_.isEmpty(this.state.order_detail_tab_index) && this.state.order_detail_tab_index.is_return) &&
                                                            <th width="5%" className="text-center"></th>
                                                        }

                                                    </tr>
                                                </thead>
                                                <tbody>
                                                  {/* Display info order */}
                                                  {!_.isEmpty(this.state.order_detail_tab_index) &&
                                                  (deli13 !== 0) &&
                                                    <tr>
                                                        <td colSpan="8">
                                                            <div className="col-md-1-5 col-xs-1-5 clear-padding-right">
                                                                <b>{trans('messages.delivery_type_nanko')}</b>
                                                            </div>
                                                            <div className="col-md-1-5 col-xs-1-5 clear-padding-right">
                                                                <b>{trans('messages.delivery_method')}：</b>
                                                            </div>
                                                            <div className="col-md-1-75 col-xs-1-75 clear-padding-right">
                                                                {(!_.isEmpty(this.state.order_detail_tab_index)) && this.state.order_detail_tab_index.company.nanko}
                                                            </div>
                                                            <div className="col-md-1-75 col-xs-1-75 clear-padding-right">
                                                                <b>{trans('messages.inquiry_no')}：</b>
                                                            </div>
                                                            <div className="col-md-1 col-xs-1 clear-padding-right">
                                                                {dataSource.data.inquiry_no}
                                                            </div>
                                                            <div className="col-md-1-75 col-xs-1-75 clear-padding-right">
                                                                <b>{trans('messages.delivery_instrustions')}：</b>
                                                            </div>
                                                            <div className="col-md-2-75 col-xs-2-75 clear-padding-right">
                                                                {dataSource.data.delivery_instrustions}
                                                            </div>
                                                        </td>
                                                    </tr>
                                                    }
                                                    {/* Display title 3 */}
                                                    { !_.isEmpty(this.state.order_detail_tab_index) && (deli13 !== 0) &&
                                                    <tr>
                                                        <td colSpan="8">
                                                            <div className="col-md-1-5 col-xs-1-5"><b>{trans('messages.delivery_plan_date')}</b></div>
                                                            <div className="col-md-2 col-xs-2">

                                                                {dataSource.data.delivery_plan_date ? moment(dataSource.data.delivery_plan_date).format('YYYY/MM/DD') : ''}
                                                            </div>
                                                            <div className="col-md-1-25 col-xs-1-25"><b>{trans('messages.ship_wish_date')}</b></div>
                                                            <div className="col-md-1-5 col-xs-1-5">{dataSource.data.ship_wish_date}</div>
                                                            <div className="col-md-1-5 col-xs-1-5"><b>{trans('messages.ship_wish_time')}</b></div>
                                                            <div className="col-md-1-5 col-xs-1-5">{dataSource.data.ship_wish_time}</div>
                                                        </td>
                                                    </tr>
                                                    }
                                                    {/* Display parent item product */}
                                                    {(!_.isEmpty(this.state.order_detail_tab_index)) && (!_.isUndefined(this.state.order_detail_tab_index.data)) &&
                                                        this.state.order_detail_tab_index.data.map((item, itemKey) => {
                                                            index++;
                                                            let specify_deli_type = item.specify_deli_type === 2 ? 'warning' : 'success';
                                                            if ((item.delivery_type === 3 ||item.delivery_type === 1 || item.delivery_type === null) && typeof item.is_claim !== 'undefined') {
                                                                return (
                                                                [
                                                                    <tr>
                                                                        <td colSpan="9">
                                                                        <div className="col-md-3 pull-right">
                                                                            <div className="col-sm-11 clear-padding">
                                                                                <SELECT
                                                                                    groupClass="form-group pull-right clear-margin"
                                                                                    name="claim_id"
                                                                                    labelTitle=""
                                                                                    className="form-control input-sm"
                                                                                    fieldGroupClass="col-md-12"
                                                                                    labelClass=" "
                                                                                    onChange={(e) => this.handleChangeClaim(item.detail_line_num, e)}
                                                                                    defaultValue={item.claim_id}
                                                                                    options={item.dataClaim} />
                                                                            </div>
                                                                            <div className="col-sm-1 clear-padding margin-top-6">
                                                                                <BUTTON className="btn btn-box-tool btn-save pull-right" onClick={(e) => this.handleAddClaim(e)}>
                                                                                     <i className="fa fa-plus"></i>
                                                                                </BUTTON>
                                                                            </div>
                                                                        </div>
                                                                        </td>
                                                                    </tr>
                                                                ])
                                                            }
                                                            if (item.delivery_type === 3 ||item.delivery_type === 1 || item.delivery_type === null) {
                                                                return (
                                                                [
                                                                    <tr key={"delivery_type_3_1"+index}>
                                                                        <td>{item.product_code}</td>
                                                                        <td>{item.product_name}</td>
                                                                        <td className="text-right">
                                                                           {_.isEmpty(item.child_data) ?
                                                                                (
                                                                                    item.edi_order_code ?
                                                                                    (
                                                                                        <a target="_blank" title={item.edi_order_code} href={this.state.order_detail_tab_index.url + '/edi/all' + (item.order_type === 2 ? '_direct' : '') +'?s_order_code=' + item.edi_order_code}>
                                                                                                <span className="show-modal-content"
                                                                                                    data-content={item.edi_order_code}
                                                                                                    data-toggle="popover"
                                                                                                    data-placement="top"
                                                                                                    data-trigger="hover">
                                                                                                        EDI
                                                                                                    </span>
                                                                                        </a>
                                                                                    ) : ''
                                                                                )
                                                                                :
                                                                                ''
                                                                            }
                                                                        </td>
                                                                        <td className="text-right">
                                                                            ￥{nf.format(item.price)}
                                                                        </td>
                                                                        <td className="text-right">
                                                                           {nf.format(item.quantity)}
                                                                        </td>
                                                                        <td className="text-right">
                                                                          ￥{nf.format(item.sub_total)}
                                                                        </td>
                                                                        <td className="text-center">
                                                                        { _.isEmpty(item.child_data) &&
                                                                            <span className="label bg-blue">{item.product_status_name}</span>
                                                                        }
                                                                        </td>
                                                                        <td className="text-center" width="5%">
                                                                            <a onClick={(e) => this.handleClaim(item.detail_line_num, itemKey, item.claim_id, item.delivery_type, e)}>
                                                                                <img src={item.claim_id ? "/img/on_detail.gif" : "/img/off_detail.gif"} width="20" className="zoom-claim" />
                                                                            </a>
                                                                        </td>
                                                                        <td className={"text-center " + (!item.delivery_type ? "col-max-min-100 position-relative" : "")}>
                                                                        {/* Display button delivery */}
                                                                        {
                                                                        ((dataSource.data.order_status >= _.status('ORDER_STATUS.NEW') && dataSource.data.order_status < _.status('ORDER_STATUS.ORDER')) ||
                                                                        (dataSource.data.order_status === _.status('ORDER_STATUS.ORDER') && dataSource.data.order_sub_status < _.status('ORDER_SUB_STATUS.DONE'))) &&
                                                                        (
                                                                            (item.delivery_type === 1 || item.delivery_type === 3)
                                                                            ?
                                                                                <a
                                                                                    key={'detail' + item.detail_line_num}
                                                                                    onClick={(e) => this.handleSubmitDelivery('detail', item)}
                                                                                    className="btn btn-danger btn-xs"
                                                                                >
                                                                                {trans('messages.btn_detail_delivery')}
                                                                                </a>
                                                                                :
                                                                                (
                                                                                    <div className="product-delivery">
                                                                                        <a
                                                                                            key={'direct' + item.detail_line_num}
                                                                                            onClick={(e) => this.handleSubmitDelivery('new_direct', item)}
                                                                                            className={"btn btn-xs pull-left " + ((item.specify_deli_type && item.specify_deli_type === 3) ? 'btn-info' : 'btn-danger')}
                                                                                        >
                                                                                        {trans('messages.btn_direct_delivery')}
                                                                                        </a>
                                                                                        <a
                                                                                            key={'detail' + item.detail_line_num}
                                                                                            onClick={(e) => this.handleSubmitDelivery('new_detail', item)}
                                                                                            className={"btn btn-xs pull-left " + ((item.specify_deli_type && item.specify_deli_type === 2) ? 'btn-info' : 'btn-danger')}
                                                                                        >
                                                                                        {trans('messages.btn_detail_delivery')}
                                                                                        </a>
                                                                                    </div>
                                                                                )
                                                                            )
                                                                        }
                                                                        {/* Display button return */}
                                                                        {
                                                                        (
                                                                            (item.product_status === _.status('PRODUCT_STATUS.DIRECTLY_SHIPPING') || item.product_status === _.status('PRODUCT_STATUS.SHIPPED')) &&
                                                                            (
                                                                                (dataSource.data.order_status === _.status('ORDER_STATUS.DELIVERY') && dataSource.data.order_sub_status === _.status('ORDER_SUB_STATUS.DONE')) ||
                                                                                (dataSource.data.order_status > _.status('ORDER_STATUS.DELIVERY') && dataSource.data.order_status !== _.status('ORDER_STATUS.CANCEL'))
                                                                            )
                                                                        )
                                                                        &&
                                                                            <a
                                                                                onClick={(e) => this.handleRedirectAddReturn(
                                                                                        item.return_no,
                                                                                        orderReturnDetailUrl + '?receive_id='
                                                                                        + dataSource.data.receive_id
                                                                                        + '&product_code=' + item.product_code
                                                                                    )}
                                                                                className="btn btn-info btn-xs"
                                                                                disabled={item.return_no?true:false}
                                                                            >
                                                                            {trans('messages.btn_move_return')}
                                                                            </a>
                                                                        }

                                                                        </td>

                                                                    </tr>,
                                                                    item.child_data.map((child) => {
                                                                        return (
                                                                            <tr>
                                                                                <td></td>
                                                                                <td>
                                                                                    <div className="row">
                                                                                        <div className="col-md-3">{child.child_product_code}</div>
                                                                                        <div className="col-md-9">{child.child_product_name}</div>
                                                                                    </div>
                                                                                </td>
                                                                                <td className="text-right">
                                                                                    {
                                                                                        child.child_edi_order_code ?
                                                                                        (
                                                                                            <a target="_blank" title={child.child_edi_order_code} href={this.state.order_detail_tab_index.url + '/edi/all' + (child.order_type === 2 ? '_direct' : '') +'?s_order_code=' + child.child_edi_order_code}>
                                                                                                <span className="show-modal-content"
                                                                                                    data-content={child.child_edi_order_code}
                                                                                                    data-toggle="popover"
                                                                                                    data-placement="top"
                                                                                                    data-trigger="hover">
                                                                                                        EDI
                                                                                                    </span>
                                                                                            </a>
                                                                                        ) : ''
                                                                                    }

                                                                                </td>
                                                                                <td></td>
                                                                                <td className="text-right">{nf.format(child.received_order_num)}</td>
                                                                                <td></td>
                                                                                <td className="text-center">
                                                                                    <span className="label bg-blue">{child.product_status_name}</span>
                                                                                </td>
                                                                                {
                                                                                (
                                                                                    (item.product_status === _.status('PRODUCT_STATUS.DIRECTLY_SHIPPING') || item.product_status === _.status('PRODUCT_STATUS.SHIPPED')) &&
                                                                                    (
                                                                                        (dataSource.data.order_status === _.status('ORDER_STATUS.DELIVERY') && dataSource.data.order_sub_status === _.status('ORDER_SUB_STATUS.DONE')) ||
                                                                                        (dataSource.data.order_status > _.status('ORDER_STATUS.DELIVERY') && dataSource.data.order_status !== _.status('ORDER_STATUS.CANCEL'))
                                                                                    )

                                                                                )
                                                                                &&
                                                                                <td className="text-center">
                                                                                    <a
                                                                                        onClick={(e) => this.handleRedirectAddReturn(
                                                                                                item.return_no,
                                                                                                orderReturnDetailUrl + '?receive_id='
                                                                                                + dataSource.data.receive_id
                                                                                                + '&product_code=' + item.product_code
                                                                                            )}
                                                                                        className="btn btn-info btn-xs"
                                                                                        disabled={item.return_no?true:false}
                                                                                    >
                                                                                    {trans('messages.btn_move_return')}
                                                                                    </a>
                                                                                </td>
                                                                                }
                                                                            </tr>
                                                                        )
                                                                    })
                                                                ])
                                                            }
                                                        })
                                                    }
                                                    {!_.isEmpty(this.state.order_detail_tab_index) && (deli2 !== 0) &&
                                                    <tr>
                                                        <td colSpan="8">
                                                            <div className="col-md-1-5 col-xs-1-5">
                                                                <b>{trans('messages.delivery_type_supplier')}</b>
                                                            </div>
                                                            <div className="col-md-1-5 col-xs-1-5">
                                                                <b>{trans('messages.delivery_method')}：</b>
                                                            </div>
                                                            <div className="col-md-5 col-xs-5">
                                                                {(!_.isEmpty(this.state.order_detail_tab_index)) && this.state.order_detail_tab_index.company.supplier}
                                                            </div>
                                                        </td>
                                                    </tr>
                                                    }
                                                    {!_.isEmpty(this.state.order_detail_tab_index) && (deli2 !== 0) &&
                                                    <tr>
                                                        <td colSpan="8">
                                                            <div className="col-md-1-5 col-xs-1-5">
                                                                <b>{trans('messages.delivery_plan_date')}</b>
                                                            </div>
                                                            <div className="col-md-2 col-xs-2">
                                                                {dataSource.data.delivery_plan_date}
                                                            </div>
                                                        </td>
                                                    </tr>
                                                    }
                                                    {
                                                    (!_.isEmpty(this.state.order_detail_tab_index)) && (!_.isUndefined(this.state.order_detail_tab_index.data)) &&
                                                        this.state.order_detail_tab_index.data.map((item, itemKey) => {
                                                            let specify_deli_type = item.specify_deli_type === 2 ? 'warning' : 'success';
                                                            index++;
                                                            if (item.delivery_type === 2 && typeof item.is_claim !== 'undefined') {
                                                                return (
                                                                [
                                                                    <tr>
                                                                        <td colSpan="9">
                                                                        <div className="col-md-3 pull-right">
                                                                            <div className="col-sm-11 clear-padding">
                                                                                <SELECT
                                                                                    groupClass="form-group pull-right clear-margin"
                                                                                    name="claim_id"
                                                                                    labelTitle=""
                                                                                    className="form-control input-sm"
                                                                                    fieldGroupClass="col-md-12"
                                                                                    labelClass=" "
                                                                                    onChange={(e) => this.handleChangeClaim(item.detail_line_num, e)}
                                                                                    defaultValue={item.claim_id}
                                                                                    options={item.dataClaim} />
                                                                            </div>
                                                                            <div className="col-sm-1 clear-padding margin-top-6">
                                                                                <BUTTON className="btn btn-box-tool btn-save pull-right" onClick={(e) => this.handleAddClaim(e)}>
                                                                                     <i className="fa fa-plus"></i>
                                                                                </BUTTON>
                                                                            </div>
                                                                        </div>
                                                                        </td>
                                                                    </tr>
                                                                ])
                                                            }
                                                            if (item.delivery_type === 2) {
                                                                return ([
                                                                    <tr key={"delivery_type_2"+index}>
                                                                        <td>{item.product_code}</td>
                                                                        <td>{item.product_name}</td>
                                                                        <td className="text-right">
                                                                            {_.isEmpty(item.child_data) ?
                                                                                (
                                                                                    item.edi_order_code ?
                                                                                    (
                                                                                        <a target="_blank" title={item.edi_order_code} href={this.state.order_detail_tab_index.url + '/edi/all' + (item.order_type === 2 ? '_direct' : '') +'?s_order_code=' + item.edi_order_code}>
                                                                                            <span className="show-modal-content"
                                                                                            data-content={item.edi_order_code}
                                                                                            data-toggle="popover"
                                                                                            data-placement="top"
                                                                                            data-trigger="hover">
                                                                                                EDI
                                                                                            </span>
                                                                                        </a>
                                                                                    ) : ''
                                                                                )
                                                                                :
                                                                                ''
                                                                            }
                                                                        </td>
                                                                        <td className="text-right">
                                                                            ￥{nf.format(item.price)}
                                                                        </td>
                                                                        <td className="text-right">
                                                                            {nf.format(item.quantity)}
                                                                        </td>
                                                                        <td className="text-right">
                                                                            ￥{nf.format(item.sub_total)}
                                                                        </td>
                                                                        <td className="text-center">
                                                                            {_.isEmpty(item.child_data) &&
                                                                                <span className="label bg-blue">{item.product_status_name}</span>
                                                                            }
                                                                        </td>
                                                                        <td className="text-center" width="5%">
                                                                            <a onClick={(e) => this.handleClaim(item.detail_line_num, itemKey, item.claim_id, item.delivery_type, e)}>
                                                                                <img src={item.claim_id ? "/img/on_detail.gif" : "/img/off_detail.gif"} width="20" className="zoom-claim" />
                                                                            </a>
                                                                        </td>

                                                                        <td className="text-center">
                                                                            {/* Display button direct */}
                                                                            {
                                                                                ((dataSource.data.order_status >= _.status('ORDER_STATUS.NEW') && dataSource.data.order_status < _.status('ORDER_STATUS.ORDER')) ||
                                                                                (dataSource.data.order_status === _.status('ORDER_STATUS.ORDER') && dataSource.data.order_sub_status < _.status('ORDER_SUB_STATUS.DONE'))) &&
                                                                                (
                                                                                    <a
                                                                                        key={'direct' + item.detail_line_num}
                                                                                        onClick={(e) => this.handleSubmitDelivery('direct', item)}
                                                                                        className="btn btn-danger btn-xs"
                                                                                    >
                                                                                    {trans('messages.btn_direct_delivery')}
                                                                                    </a>
                                                                                )
                                                                            }

                                                                            {/* Display button return */}
                                                                            {
                                                                            (
                                                                                (item.product_status === _.status('PRODUCT_STATUS.DIRECTLY_SHIPPING') || item.product_status === _.status('PRODUCT_STATUS.SHIPPED')) &&
                                                                                (
                                                                                    (dataSource.data.order_status === _.status('ORDER_STATUS.DELIVERY') && dataSource.data.order_sub_status === _.status('ORDER_SUB_STATUS.DONE')) ||
                                                                                    (dataSource.data.order_status > _.status('ORDER_STATUS.DELIVERY') && dataSource.data.order_status !== _.status('ORDER_STATUS.CANCEL'))
                                                                                )

                                                                            )
                                                                            &&
                                                                            <a
                                                                                onClick={(e) => this.handleRedirectAddReturn(
                                                                                        item.return_no,
                                                                                        orderReturnDetailUrl + '?receive_id='
                                                                                        + dataSource.data.receive_id
                                                                                        + '&product_code=' + item.product_code
                                                                                    )}
                                                                                className="btn btn-info btn-xs"
                                                                                disabled={item.return_no?true:false}
                                                                            >
                                                                            {trans('messages.btn_move_return')}
                                                                            </a>
                                                                            }
                                                                        </td>

                                                                    </tr>,
                                                                    item.child_data.map((child) => {
                                                                        return (
                                                                            <tr>
                                                                                <td></td>
                                                                                <td>
                                                                                    <div className="row">
                                                                                        <div className="col-md-3">{child.child_product_code}</div>
                                                                                        <div className="col-md-9">{child.child_product_name}</div>
                                                                                    </div>
                                                                                </td>
                                                                                <td className="text-right">
                                                                                    {
                                                                                        child.child_edi_order_code ?
                                                                                        (
                                                                                            <a target="_blank" title={child.child_edi_order_code} href={this.state.order_detail_tab_index.url + '/edi/all' + (child.order_type === 2 ? '_direct' : '') +'?s_order_code=' + child.child_edi_order_code}>
                                                                                                <span className="show-modal-content"
                                                                                                    data-content={child.child_edi_order_code}
                                                                                                    data-toggle="popover"
                                                                                                    data-placement="top"
                                                                                                    data-trigger="hover">
                                                                                                        EDI
                                                                                                    </span>
                                                                                            </a>
                                                                                        ) : ''
                                                                                    }
                                                                                </td>
                                                                                <td></td>
                                                                                <td className="text-right">{nf.format(child.received_order_num)}</td>
                                                                                <td></td>
                                                                                <td className="text-center">
                                                                                    <span className="label bg-blue">{child.product_status_name}</span>
                                                                                </td>
                                                                                {
                                                                                    (
                                                                                        (item.product_status === _.status('PRODUCT_STATUS.DIRECTLY_SHIPPING') || item.product_status === _.status('PRODUCT_STATUS.SHIPPED')) &&
                                                                                        (
                                                                                            (dataSource.data.order_status === _.status('ORDER_STATUS.DELIVERY') && dataSource.data.order_sub_status === _.status('ORDER_SUB_STATUS.DONE')) ||
                                                                                            (dataSource.data.order_status > _.status('ORDER_STATUS.DELIVERY') && dataSource.data.order_status !== _.status('ORDER_STATUS.CANCEL'))
                                                                                        )

                                                                                    )
                                                                                    &&
                                                                                <td className="text-center">
                                                                                    <a
                                                                                        onClick={(e) => this.handleRedirectAddReturn(
                                                                                                item.return_no,
                                                                                                orderReturnDetailUrl + '?receive_id='
                                                                                                + dataSource.data.receive_id
                                                                                                + '&product_code=' + item.product_code
                                                                                            )}
                                                                                        className="btn btn-info btn-xs"
                                                                                        disabled={item.return_no?true:false}
                                                                                    >
                                                                                    {trans('messages.btn_move_return') }
                                                                                    </a>
                                                                                </td>
                                                                                }
                                                                            </tr>
                                                                        )
                                                                    })
                                                                ])
                                                            }
                                                        })
                                                    }
                                                </tbody>
                                            </table>
                                        </div>
                                        <div className="col-md-12">
                                            <div className="row">
                                                <div className="col-md-6 col-md-offset-6">
                                                    <a className="btn btn-success pull-right"
                                                        onClick={(e) => this.handleRedirectEditing(
                                                                dataSource.data['order_status'],
                                                                orderDetailEditingUrl + "?receive_id=" + dataSource.data['receive_id'] + "&receiver_id=" + this.state.cur_rec_id
                                                            )}
                                                        disabled={(dataSource.data['order_status']<_.status('ORDER_STATUS.SETTLEMENT'))?false:true}
                                                    >{trans('messages.btn_detail_editing')}</a>
                                                    {
                                                        (
                                                            (dataSource.data.order_status === _.status('ORDER_STATUS.DELIVERY') && dataSource.data.order_sub_status === _.status('ORDER_SUB_STATUS.DONE')) ||
                                                            (dataSource.data.order_status > _.status('ORDER_STATUS.DELIVERY') && dataSource.data.order_status !== _.status('ORDER_STATUS.CANCEL'))
                                                        ) ?
                                                    <a
                                                        className="btn btn-success pull-right  margin-element"
                                                        href={orderReturnDetailUrl + "?receive_id=" + dataSource.data['receive_id']}
                                                    >{trans('messages.btn_move_return')}</a>
                                                    : ''
                                                    }
                                                    {
                                                        (
                                                            dataSource.data.order_status <= _.status('ORDER_STATUS.ARRIVAL') ||
                                                            (
                                                                dataSource.data.order_status === _.status('ORDER_STATUS.DELIVERY') &&
                                                                dataSource.data.order_sub_status <= _.status('ORDER_SUB_STATUS.NEW')
                                                            )
                                                        ) ?
                                                    <BUTTON style={{'marginBottom':'10px'}} name="btn_comfirm_cancel" className="btn btn-success pull-right margin-element" onClick={() => {this.handleCancelReason()}}>
                                                        {trans('messages.cancel_order')}
                                                    </BUTTON>
                                                        : ''
                                                    }
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            {/* /.tab-pane */}
                            {(dataSource.data.is_mall_update === 1 || (dataSource.data.is_mall_cancel === 0 && dataSource.data.order_status === _.status('ORDER_STATUS.CANCEL') && (dataSource.data.mall_id ===4 || dataSource.data.mall_id ===5))) &&
                                <div className="col-md-12 text-center">
                                    <BUTTON className="btn btn-info btn-sm text-bold" onClick={() => {this.handleMallUpdate()}}>
                                        {trans('messages.btn_complete_update')}
                                    </BUTTON>
                                </div>
                            }
                        </div>
                    </div>
                </div>
            </div>
            <ExportPdfPopup receiveId={this.state.receive_id} fullName={dataSource.data['full_name']} />
            <MemoPopup
                receive_id={this.state.receive_id}
                log_id={this.state.cur_log_id}
                log_title={this.state.cur_log_title}
                log_contents={this.state.cur_log_contents}
                handelCountMemo={this.handelCountMemo}
                handleLoadDataTabs={this.handleLoadDataTabs}
            />
            <CancelReasonPopup
                handleSubmitCancelReason={this.handleSubmitCancelReason}
                optionCancelReason={dataSource.optionCancelReason}
            />
            <AcceptPopup
                handleSubmit={this.handleSubmitDeliveryPopup}
                handleCancel={this.handleCancelDeliveryPopup}
            />
            <AddClaimPopup
                handleSubmit={this.handleSubmitAddClaimPopup}
                handleCancel={this.handleCancelAddClaimPopup}
            />
            <NotEnoughtCancelPopup
                handleSubmitNotCancel={this.handleSubmitNotEnoughtCancelPopup}
                handleCancelNotCancel={this.handleCancelNotEnoughtCancelPopup}
                dataProduct={this.state.product_not_cancel}
                reason={this.state.status_reason === 1 ? true : false}
                optionCancelReason={dataSource.optionCancelReason}
            />
            <RepayTypeCancelPopup
                handleSubmitRepay={this.handleSubmitRepayCancel}
            />
            <RepayTypeNotEnoughtCancelPopup
                handleSubmitRepay={this.handleSubmitRepayNotEnoughtCancel}
            />
            <FormIsDeplay
                {...this.props}
                {...this.state}
                handleReloadData={this.handleReloadData}
                handleLoadDataTabs={this.handleLoadDataTabs}/>
        </div>
    )
    }
}
const mapStateToProps = (state) => {

  return {
    dataSource : state.commonReducer.dataSource
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    reloadData : (url, params) => { dispatch(Action.detailData(url, params)) }
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(DetailOrderManagement)