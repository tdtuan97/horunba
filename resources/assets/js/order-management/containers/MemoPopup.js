import React, { Component } from 'react';
import Loading from '../../common/components/common/Loading';
import INPUT from '../../common/components/form/INPUT';
import TEXTAREA from '../../common/components/form/TEXTAREA';

import axios from 'axios';

export default class MemoPopup extends Component {
    constructor(props) {
        super(props);
        this.state = {
            receive_id: this.props.receive_id,
            log_id: this.props.log_id,
            log_title: this.props.log_title,
            log_contents: this.props.log_contents,
        }
    }

    handleChange = (event) => {
        const target = event.target;
        const name = target.name;
        const value = target.value;
        this.setState({
            [name]: value
        });
    }

    handleAddClick = () => {
        let params = {};
        params['receive_id']   = this.state.receive_id;
        params['log_id']       = this.state.log_id;
        params['log_title']    = this.state.log_title;
        params['log_contents'] = this.state.log_contents

        $(".loading").show();
        axios.post(addMemoUrl, params, {headers: {'X-Requested-With': 'XMLHttpRequest'}}
        ).then(response => {
            if (response.data.status === 0) {
                let messages = response.data.messages;
                this.setState({
                    log_title_error: (messages.log_title)?messages.log_title:'',
                    log_contents_error: (messages.log_contents)?messages.log_contents:''
                });
                this.props.countMemo();
            } else {
                this.setState({
                    log_title_error: '',
                    log_contents_error: ''
                });
                this.props.handleLoadDataTabs('memo_tab', getInfoLogUrl, {receive_id : this.props.receive_id});
                this.handleCancelClick();
                this.props.handelCountMemo();
            }
            $(".loading").hide();
        }).catch(error => {
            $(".loading").hide();
            throw(error);
        });
    }

    handleCancelClick = () => {
        this.setState({
            log_id: '',
            log_title: '',
            log_contents: '',
            log_title_error: '',
            log_contents_error: ''
        });
        $('#add-memo-modal').modal('hide');
    }

    componentWillReceiveProps(nextProps) {
        this.setState({
            receive_id: nextProps.receive_id,
            log_id: nextProps.log_id,
            log_title: nextProps.log_title,
            log_contents: nextProps.log_contents,
        });
    }

    render() {
        return (
            <div className="modal fade" id="add-memo-modal">
                <Loading className="loading" />
                <div className="modal-dialog ">
                    <div className="modal-content">
                        <div className="modal-header">
                        </div>
                        <div className="modal-body modal-custom-body">
                            <div className="row">
                                <INPUT
                                    name="log_title"
                                    labelTitle={trans('messages.memo_title_popup') + "："}
                                    className="form-control input-sm"
                                    fieldGroupClass="col-md-9"
                                    labelClass="col-md-3"
                                    groupClass="col-md-12 margin-form"
                                    errorPopup={true}
                                    value={this.state.log_title}
                                    onChange={this.handleChange}
                                    hasError={(this.state.log_title_error)?true:false}
                                    errorMessage={(this.state.log_title_error)?this.state.log_title_error:""}
                                    />
                            </div>
                            <div className="row">
                                <TEXTAREA
                                    name="log_contents"
                                    labelTitle={trans('messages.memo_content_popup') + "："}
                                    className="form-control input-sm"
                                    fieldGroupClass="col-md-9"
                                    labelClass="col-md-3"
                                    groupClass="col-md-12 margin-form"
                                    errorPopup={true}
                                    value={this.state.log_contents}
                                    onChange={this.handleChange}
                                    hasError={(this.state.log_contents_error)?true:false}
                                    errorMessage={(this.state.log_contents_error)?this.state.log_contents_error:""}
                                    rows="6"
                                    />
                            </div>
                        </div>
                        <div className="modal-footer modal-custom-footer">
                            <button type="button" className="btn btn-primary btn-sm modal-custom-button" onClick={this.handleAddClick}>{trans('messages.btn_accept_memo')}</button>
                            <button type="button" className="btn bg-purple btn-sm modal-custom-button" onClick={this.handleCancelClick}>{trans('messages.btn_cancel_memo')}</button>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}