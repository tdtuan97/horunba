import React, { Component } from 'react';
import Loading from '../../common/components/common/Loading';
import TEXTAREA from '../../common/components/form/TEXTAREA';

import axios from 'axios';

export default class FormIsDeplay extends Component {
    constructor(props) {
        super(props);
        this.state = {};

    }
    handleChange = (event) => {
        const target = event.target;
        const name = target.name;
        const value = target.value;
        this.setState({
            [name]: value
        });
    }
    handleSubmit = () => {
        $(".loading").show();
        let params = {receive_id : this.props.receive_id , is_delay : this.props.is_delay , reason : this.state.reason};
        let url = this.props.location.pathname.replace("detail", "change-delay");
        axios.post(url, params,
            {
                headers: {'X-Requested-With': 'XMLHttpRequest'}
            }
        ).then(response => {
            if (response.data.status === 0) {
                let messages = response.data.messages;
                this.setState({
                    reason_error: (messages.reason)?messages.reason:'',
                });
            } else {
                this.props.handleReloadData(detailUrl,{receive_id : this.props.receive_id})
                this.props.handleLoadDataTabs('memo_tab', getInfoLogUrl, {receive_id : this.props.receive_id});
                this.setState({reason_error : '', reason : ''});
                $('#reason-modal').modal('hide');
            }
            $(".loading").hide();
        }).catch(error => {
            $(".loading").hide();
            throw(error);
        });
    }

    handleCancelClick = () => {
        this.setState({reason_error : '', reason : ''});
        $('#reason-modal').modal('hide');
    }

    render() {
        return (
            <div className="modal fade" id="reason-modal">
                <Loading className="loading" />
                <input type="hidden" id="action-modal" />
                <div className="modal-dialog modal-custom">
                    <div className="modal-content modal-custom-content">
                        <div className="modal-header">
                            <h4 className="modal-title text-center"><b>{trans('messages.reason')}</b></h4>
                        </div>
                        <div className="modal-body modal-custom-body">
                            <form className="form-horizontal">
                                <TEXTAREA
                                    name="reason"
                                    className="form-control input-sm"
                                    fieldGroupClass="col-md-12"
                                    cols="9"
                                    rows="5"
                                    value ={this.state.reason || ''}
                                    onChange={this.handleChange}
                                    />
                                    {
                                        (this.state.reason_error)?
                                        (
                                            <span className="help-block col-md-12 remove-padding-left">
                                                <strong className="error-new-line text-danger">{this.state.reason_error}</strong>
                                            </span>
                                        ):""
                                    }
                            </form>
                        </div>
                        <div className="modal-footer modal-custom-footer">
                            <button type="button" className="btn btn-primary modal-custom-button" onClick={this.handleSubmit}>{trans('messages.issue_modal_ok')}</button>
                            <button type="button" className="btn bg-purple modal-custom-button" onClick={this.handleCancelClick} >{trans('messages.issue_modal_cancel')}</button>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}
