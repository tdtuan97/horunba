import React, { Component } from 'react';
import Loading from '../../common/components/common/Loading';
import INPUT from '../../common/components/form/INPUT';

import DATEPICKER from '../../common/components/form/DATEPICKER';
import moment from 'moment';
import axios from 'axios';

export default class ExportPdfPopup extends Component {
    constructor(props) {
        super(props);

        this.state = {
            receive_id: this.props.receiveId,
            issue_date: moment(),
            issue_date_raw: moment().format('YYYY-MM-DD'),
            customer_name: (this.props.fullName)?this.props.fullName:''
        }
    }

    handleChangeDate = (date) => {
        this.setState({
            issue_date: date,
            issue_date_raw: date.format('YYYY-MM-DD')
        });
    }

    handleChangeDateRaw(value) {
        this.setState({
            issue_date_raw: value
        });
    }

    handleChange = (event) => {
        const target = event.target;
        const name = target.name;
        const value = target.value;
        this.setState({
            [name]: value
        });
    }

    handleExportClick = () => {
        let params = {};
        let arrParams = [];

        params['receive_id']    = this.state['receive_id'];
        params['issue_date']    = this.state['issue_date_raw'];
        params['customer_name'] = this.state['customer_name'];
        params['action']        = $('#action-modal').val();

        arrParams.push("receive_id=" + this.state['receive_id']);
        arrParams.push("issue_date=" + this.state['issue_date_raw']);
        arrParams.push("customer_name=" + this.state['customer_name']);
        arrParams.push("action=" + $('#action-modal').val());

        $(".loading").show();
        axios.post(exportIssueUrl, params, {headers: {'X-Requested-With': 'XMLHttpRequest'}}
        ).then(response => {
            if (response.data.status === 0) {
                let messages = response.data.messages;
                this.setState({
                    issue_date_error: (messages.issue_date)?messages.issue_date:'',
                    customer_name_error: (messages.customer_name)?messages.customer_name:''
                });
            } else {
                this.setState({
                    issue_date_error: '',
                    customer_name_error: ''
                });
                window.open(exportIssueUrl + "?" + arrParams.join('&') , '_blank');
                this.handleCancelClick();
            }
            $(".loading").hide();
        }).catch(error => {
            $(".loading").hide();
            throw(error);
        });
    }

    handleCancelClick = () => {
        this.setState({
            issue_date: moment(),
            issue_date_raw: moment().format('YYYY-MM-DD'),
            customer_name: (this.props.fullName)?this.props.fullName:'',
            issue_date_error: '',
            customer_name_error: ''
        });
        $('#issue-modal').modal('hide');
    }

    render() {
        return (
            <div className="modal fade" id="issue-modal">
                <Loading className="loading" />
                <input type="hidden" id="action-modal" />
                <div className="modal-dialog modal-custom">
                    <div className="modal-content modal-custom-content">
                        <div className="modal-header">
                            <h4 className="modal-title text-center"><b>{trans('messages.issue_modal_title')}</b></h4>
                        </div>
                        <div className="modal-body modal-custom-body">
                            <p>{trans('messages.issue_modal_message')}</p>
                            <form className="form-horizontal">
                                <DATEPICKER
                                    placeholderText="YYYY-MM-DD"
                                    dateFormat="YYYY-MM-DD"
                                    locale="ja"
                                    name="issue_date"
                                    labelTitle={trans('messages.issue_modal_date') + "："}
                                    className="form-control input-sm"
                                    fieldGroupClass="col-md-9"
                                    labelClass="col-md-3"
                                    groupClass=""
                                    selected={this.state.issue_date}
                                    value={this.state.issue_date_raw}
                                    onChange={this.handleChangeDate}
                                    onChangeRaw={(event) => this.handleChangeDateRaw(event.target.value)}
                                    errorPopup={true}
                                    hasError={(this.state.issue_date_error)?true:false}
                                    errorMessage={(this.state.issue_date_error)?this.state.issue_date_error:""}
                                    />
                                <INPUT
                                    name="customer_name"
                                    labelTitle={trans('messages.issue_modal_name') + "："}
                                    className="form-control input-sm"
                                    fieldGroupClass="col-md-9"
                                    labelClass="col-md-3"
                                    groupClass=""
                                    errorPopup={true}
                                    value={this.state.customer_name}
                                    onChange={this.handleChange}
                                    hasError={(this.state.customer_name_error)?true:false}
                                    errorMessage={(this.state.customer_name_error)?this.state.customer_name_error:""}
                                    />
                            </form>
                        </div>
                        <div className="modal-footer modal-custom-footer">
                            <button type="button" className="btn btn-primary modal-custom-button" onClick={this.handleExportClick}>{trans('messages.issue_modal_ok')}</button>
                            <button type="button" className="btn bg-purple modal-custom-button" onClick={this.handleCancelClick}>{trans('messages.issue_modal_cancel')}</button>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}