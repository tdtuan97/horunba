import React, { Component } from 'react';
import Loading from '../../common/components/common/Loading';
import SELECT from '../../common/components/form/SELECT';
export default class CancelReasonPopup extends Component {
    constructor(props) {
        super(props);
        this.state = {reason_id : 0}
    }
    handleSubmit = () => {
        if (this.state.reason_id !== 0) {
            this.props.handleSubmitCancelReason(this.state);
            $('#cancel-reason-modal').modal('hide');
            this.setState({reason_id:0});
        }
    }
    handleCancelClick = () => {
        $('#cancel-reason-modal').modal('hide');
    }
    handleChange = (event) => {
        const target = event.target;
        const name = target.name;
        const value = target.value;
        this.setState({
            [name]: value
        });
    }    
    render() {
        return (
            <div className="modal fade" id="cancel-reason-modal">
                <Loading className="loading" />
                <div className="modal-dialog ">
                    <div className="modal-content">
                        <div className="modal-header">
                        <b>{trans('messages.cancel_order')}</b>
                        </div>
                        <div className="modal-body modal-custom-body">
                            <div className="row">
                                <div className="col-md-12">
                                    <SELECT
                                        name="reason_id"
                                        labelTitle={trans('messages.cancel_order')}
                                        className="form-control input-sm"
                                        fieldGroupClass="col-md-8"
                                        labelClass="col-md-4"
                                        onChange={this.handleChange}
                                        defaultValue={''}
                                        options={this.props.optionCancelReason}
                                        errorPopup={true}/>
                                </div>                            
                            </div>                            
                        </div>
                        <div className="modal-footer modal-custom-footer">
                            <button type="button" className="btn btn-primary btn-sm modal-custom-button" onClick={this.handleSubmit}>{trans('messages.btn_accept_ok')}</button>
                            <button type="button" className="btn bg-purple btn-sm modal-custom-button" onClick={this.handleCancelClick}>{trans('messages.btn_accept_cancel')}</button>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}