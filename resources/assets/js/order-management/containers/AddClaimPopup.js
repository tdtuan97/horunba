
import React, { Component } from 'react';
import Loading from '../../common/components/common/Loading';
import INPUT from '../../common/components/form/INPUT';
export default class AddClaimPopup extends Component {
    constructor(props) {
        super(props);
        this.state = {}
    }
    handleSubmit = () => {
        $(".loading").show();
        if (!this.state.claim_content) {
            this.setState({
                error_claim_content: 'このclaim_contentのフィルドは必須です。'
            }, () => {
                $(".loading").hide();
            })
        } else {
            this.props.handleSubmit(this.state.claim_content);
        }
    }
    handleCancelClick = () => {
        this.props.handleCancel()
    }
    handleChange = (event) => {
        const target = event.target;
        const name = target.name;
        const value = target.type === 'checkbox' ? (target.checked?1:0) : target.value;
        this.setState({
            [name]: value
        });
    }
    componentDidUpdate() {
        let _this = this;
        $('#add-claim-modal').on('show.bs.modal', function (e) {
            _this.setState({
                claim_content: '',
                error_claim_content: ''
            });
        });
    }
    render() {
        return (
            <div className="modal fade" id="add-claim-modal">
                <Loading className="loading" />
                <div className="modal-dialog ">
                    <div className="modal-content">
                        <div className="modal-header">
                        <b>{trans('messages.claim_title')}</b>
                        </div>
                        <div className="modal-body modal-custom-body">
                            <div className="row">
                                <INPUT
                                    name="claim_content"
                                    type="text"
                                    labelTitle={trans('messages.claim_input') + "："}
                                    className="form-control input-sm"
                                    fieldGroupClass="col-md-9"
                                    labelClass="col-md-3"
                                    groupClass=""
                                    errorPopup={true}
                                    value={this.state.claim_content}
                                    onChange={this.handleChange}
                                    hasError={(this.state.error_claim_content)?true:false}
                                    errorMessage={(this.state.error_claim_content)?this.state.error_claim_content:""}
                                    />
                            </div>
                        </div>
                        <div className="modal-footer modal-custom-footer">
                            <button type="button" className="btn btn-primary btn-sm modal-custom-button" onClick={this.handleSubmit}>{trans('messages.btn_accept_ok')}</button>
                            <button type="button" className="btn bg-purple btn-sm modal-custom-button" onClick={this.handleCancelClick}>{trans('messages.btn_accept_cancel')}</button>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}