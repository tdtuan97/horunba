import React, { Component } from 'react';
import Loading from '../../common/components/common/Loading';
import SELECT from '../../common/components/form/SELECT';
export default class NotEnoughtCancelPopup extends Component {
    constructor(props) {
        super(props);
        this.state = {}
    }
    handleSubmit = () => {
        if ((typeof this.state.reason_id !== 'undefined' && this.state.reason_id !== 0) || !this.props.reason) {
            $(".loading").show();
            this.props.handleSubmitNotCancel(this.state.reason_id);
            $('#not-cancel-modal').modal('hide');
            this.setState({reason_id:0});
        } else {
            this.setState({error: 'Please choose reason.'});
        }
    }
    handleCancelClick = () => {
        this.props.handleCancelNotCancel()
    }
    handleChange = (event) => {
        const target = event.target;
        const name = target.name;
        let value = target.value;
        if (value === '') {
            value = 0;
        }
        this.setState({
            [name]: value
        });
    }
    render() {
        return (
            <div className="modal fade" id="not-cancel-modal">
                <Loading className="loading" />
                <div className="modal-dialog ">
                    <div className="modal-content">
                        <div className="modal-header">
                        <b>EDI出荷済みです。</b><br/>
                        <b>キャンセルする場合は「はい」を</b><br/>
                        <b>キャンセルしない場合は「いいえ」を選択してください。</b>
                        </div>
                        <div className="modal-body modal-custom-body">
                            <div className="row">
                                <div className="col-md-12">
                                    <table className="table table-striped table-custom">
                                        <thead>
                                            <tr>
                                                <th width="10%" className="text-center">{trans('messages.no')}</th>
                                                <th width="35%" className="text-center">{trans('messages.product_name')}</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            {
                                                (!_.isEmpty(this.props.dataProduct)) ?
                                                Object.keys(this.props.dataProduct).map((key) => {
                                                    return ([
                                                        <tr>
                                                            <td>{this.props.dataProduct[key].product_code}</td>
                                                            <td>{this.props.dataProduct[key].product_name}</td>
                                                        </tr>
                                                    ])
                                                })
                                                :
                                                <tr><td colSpan="2" className="text-center">{trans('messages.no_data')}</td></tr>
                                            }
                                        </tbody>
                                    </table>
                                </div>
                                <div className="col-md-12">
                                    {this.props.reason &&
                                        (
                                            <SELECT
                                                name="reason_id"
                                                labelTitle={trans('messages.cancel_order')}
                                                className="form-control input-sm"
                                                fieldGroupClass="col-md-9 reason-popup"
                                                labelClass="col-md-3"
                                                onChange={this.handleChange}
                                                defaultValue={''}
                                                options={this.props.optionCancelReason}
                                                errorPopup={true}
                                                hasError={(this.state.error && this.state.error)?true:false}
                                                errorMessage={(this.state.error&&this.state.error)?this.state.error:""}/>
                                        )
                                    }
                                </div>
                            </div>
                        </div>
                        <div className="modal-footer modal-custom-footer">
                            <button type="button" className="btn btn-primary btn-sm modal-custom-button" onClick={this.handleSubmit}>{trans('messages.btn_accept_ok')}</button>
                            <button type="button" className="btn bg-purple btn-sm modal-custom-button" onClick={this.handleCancelClick}>{trans('messages.btn_accept_cancel')}</button>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}