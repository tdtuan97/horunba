import React, { Component } from 'react';
import Loading from '../../common/components/common/Loading';
import RADIO_CUSTOM from '../../common/components/form/RADIO_CUSTOM';

export default class RepayTypePopup extends Component {
    constructor(props) {
        super(props);
        this.state = {}
    }
    handleSubmitRepay = () => {
        this.props.handleSubmitRepay();
    }
    render() {
        return (
            <div className="modal fade" id="repay-type-popup">
                <Loading className="loading" />
                <div className="modal-dialog modal-sm">
                    <div className="modal-content">
                        <div className="modal-header">
                        <b>{trans('messages.repay_type_popup_header')}</b>
                        </div>
                        <div className="modal-body modal-custom-body">
                            <div className="row">
                                <div className="col-md-12">
                                    <div className="form-group">
                                        <div className="col-md-2">
                                            <RADIO_CUSTOM
                                                name="repay_type"
                                                value="1"
                                                defaultChecked
                                                onChange={this.handleChange}
                                            />
                                        </div>
                                        <label className="col-md-10">{trans('messages.repay_type_cus')}</label>
                                    </div>
                                </div>
                            </div>
                            <div className="row">
                                <div className="col-md-12">
                                    <div className="form-group">
                                        <div className="col-md-2">
                                            <RADIO_CUSTOM
                                                name="repay_type"
                                                value="2"
                                                onChange={this.handleChange}
                                            />
                                        </div>
                                        <label className="col-md-10">{trans('messages.repay_type_city')}</label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="modal-footer modal-custom-footer">
                            <button type="button" className="btn btn-primary btn-sm modal-custom-button" onClick={this.handleSubmitRepay}>{trans('messages.accept_title')}</button>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}