import React, { Component } from 'react';
import { connect } from 'react-redux';

import * as Action from '../common/actions';
import * as queryString from 'query-string';
import axios from 'axios';

import Loading from '../common/components/common/Loading';
import FORM from '../common/components/form/FORM';
import INPUT from '../common/components/form/INPUT';
import SELECT from '../common/components/form/SELECT';
import TEXTAREA from '../common/components/form/TEXTAREA';
import BUTTON from '../common/components/form/BUTTON';
import ROW_GROUP from '../common/components/table/ROW_GROUP';
import MESSAGE_MODAL from '../common/containers/MessageModal';
import RepayTypePopup from './containers/RepayTypePopup'

class OrderEditing extends Component {
    constructor(props) {
        super(props);
        this.state = {}
        let params = queryString.parse(props.location.search);
        if (params['receive_id']) {
            this.state = {receive_id: params['receive_id'], index : params['receive_id']};
            this.hanleRealtime();
        }
        this.first = true;
        this.props.reloadData(getFormOrderDataUrl, this.state);
    }
    hanleRealtime() {
        if (!_.isEmpty(this.state.index)) {
            let params = {
                accessFlg : this.state.accessFlg,
                query : {
                        page_key : 'ORDER-EDITING',
                        primary_key : 'index:' + this.state.index
                    },
                key :this.state.index
            };
            if (this.state.accessFlg !== false) {
                this.props.postRealTime(checkEditUrl, params, {'X-Requested-With': 'XMLHttpRequest'});
            }
        }
    }
    handleCancel = (event) => {
        if (paramsFilter !== '') {
            var stringParamsFilter = paramsFilter.split("|").join("&");
            window.location.href = detailUrl + '?' + stringParamsFilter;
        } else {
            window.location.href = detailUrl + '?receive_id_key=' + this.state.receive_id;
        }
    }

    handleSubmit = (event) => {
        document.getElementsByName('btnAccept')[0].disabled = true;
        let headers = {'X-Requested-With': 'XMLHttpRequest'};

        let formData   = new FormData();
        let dataSource = this.props.dataSource;

        if ((!_.isEmpty(dataSource.data))) {
            Object.keys(dataSource.data).map((item) => {
                let fieldValue = (dataSource.data[item]!==null)?dataSource.data[item]:'';
                formData.append(item, _.trim(fieldValue));
            });
        }
        if (!_.isEmpty(this.state)) {
            Object.keys(this.state).map((item) => {
                formData.append(item, _.trim(this.state[item]));
            });
        }

        formData.append('check_show_repay_popup', 1);

        $(".loading").show();
        axios.post(saveUrl, formData, {headers : headers}
        ).then(response => {
            $(".loading").hide();
            let data = response.data;
            if (data.status === 3) {
                document.getElementsByName('btnAccept')[0].disabled = false;
                $('#repay-type-popup').modal('show');
            } else {
                formData.delete('check_show_repay_popup');
                this.props.postData(saveUrl, formData, headers);
            }
        }).catch(error => {
            $(".loading").hide();
            throw(error);
        });
    }

    handleSubmitRepay = () => {
        let repayType = $('#repay-type-popup').find('input[name="repay_type"]:checked').val();
        let headers = {'X-Requested-With': 'XMLHttpRequest'};

        let formData   = new FormData();
        let dataSource = this.props.dataSource;

        if ((!_.isEmpty(dataSource.data))) {
            Object.keys(dataSource.data).map((item) => {
                let fieldValue = (dataSource.data[item]!==null)?dataSource.data[item]:'';
                formData.append(item, _.trim(fieldValue));
            });
        }
        if (!_.isEmpty(this.state)) {
            Object.keys(this.state).map((item) => {
                formData.append(item, _.trim(this.state[item]));
            });
        }
        formData.append('repay_type', repayType);
        this.props.postData(saveUrl, formData, headers);
    }

    handleChange = (event) => {
        const target = event.target;
        const name = target.name;
        const value = target.type === 'checkbox' ? target.checked : target.value;
        let dataSet = {[name]: value};
        if (name === 'is_black_list') {
            let arrStar = this.props.dataSource.dataStar[value];
            let star = typeof arrStar !== 'undefined' ? arrStar.backlist_rank : 0;
            let is_black_list_string = '';
            for (let i = 1; i <= 5; i++) {
                is_black_list_string += "<span class='fa fa-star " + ((star && star >= i) ? 'checked-start' : '') + "'></span>";
            }
            dataSet.is_black_list_string = is_black_list_string;
        }
        this.setState({
            ...dataSet
        });
    }

    componentWillReceiveProps(nextProps) {
        if (!_.isEmpty(nextProps.dataSource)) {
            if (!nextProps.dataSource.data) {
                window.location.href = baseUrl;
            }
            if (this.first) {
                this.setState({
                    'is_black_list_string' : nextProps.dataSource.data.is_black_list_string
                });
                this.first = false;
            }
        }
        if (!_.isEmpty(nextProps.dataResponse)) {
            if (nextProps.dataResponse.status === 1) {
                if (paramsFilter !== '') {
                    var stringParamsFilter = paramsFilter.split("|").join("&");
                    window.location.href = detailUrl + '?' + stringParamsFilter;
                } else {
                    window.location.href = detailUrl + '?receive_id_key=' + this.state.receive_id;
                }
            } else {
                document.getElementsByName('btnAccept')[0].disabled = false;
            }
        }
        // Check realtime update
        if (!_.isEmpty(nextProps.dataRealTime)) {
            this.setState(nextProps.dataRealTime);
        }
    }

    componentDidUpdate() {
        if (!_.isEmpty(this.props.dataSource)) {
            $('.loading').hide();
        }
        if (this.state.accessFlg === false) {
            $('#message-modal').modal('show');
            //We will auto redirect after 5 seconds
            setTimeout(() => {
                window.location.href = baseUrl;
            }, 5000);
        }
    }
    // Check realtime check for editing
    componentWillUnmount () {
        clearInterval(this.state);
    }
    // Check realtime check for editing
    componentDidMount() {
        setInterval(() => {
            this.hanleRealtime()
        }, 5000);

    }

    render() {
        let dataSource   = this.props.dataSource;
        let dataResponse = this.props.dataResponse;

        return ((!_.isEmpty(dataSource)) &&
        <div>
            <FORM className="form-custom">
            <div className="row">
                <Loading className="loading" />
                <div className="col-md-12">
                    <div className="box box-success">
                        <div className="box-body row">
                            {/* Start left column */}
                            <div className="col-md-8">
                                <div className="row">
                                    <div className="col-md-6">
                                        <ROW_GROUP
                                            classNameLabel={'col-md-4 control-label'}
                                            classNameField={'col-md-8'}
                                            label={trans('messages.receive_order_id')}
                                            contents={dataSource.data.received_order_id} />
                                    </div>
                                    <div className="col-md-6">
                                        <ROW_GROUP
                                            classNameLabel={'col-md-4 control-label'}
                                            classNameField={'col-md-8'}
                                            label={trans('messages.receive_id')}
                                            contents={dataSource.data.receive_id} />
                                    </div>
                                </div>
                                <div className="row">
                                    <div className="col-md-6">
                                        <ROW_GROUP
                                            classNameLabel={'col-md-4 control-label'}
                                            classNameField={'col-md-8'}
                                            label={trans('messages.order_date')}
                                            contents={dataSource.data.order_date} />
                                    </div>
                                    <div className="col-md-6">
                                        <ROW_GROUP
                                            classNameLabel={'col-md-4 control-label'}
                                            classNameField={'col-md-8'}
                                            label={trans('messages.in_date')}
                                            contents={dataSource.data.in_date} />
                                    </div>
                                </div>
                                <div className="row row-spacing-double">
                                    <div className="col-md-6">
                                        <ROW_GROUP
                                            classNameLabel={'col-md-4 control-label'}
                                            classNameField={'col-md-8'}
                                            label={trans('messages.full_name')}>
                                            <div className='box-info-text'>{
                                                (dataSource.data.last_name ? dataSource.data.last_name : '') +
                                                ' ' +
                                                (dataSource.data.first_name ? dataSource.data.first_name : '') +
                                                 '様'}
                                            </div>
                                        </ROW_GROUP>
                                    </div>
                                    <div className="col-md-6">
                                        <ROW_GROUP
                                            classNameLabel={'col-md-4 control-label'}
                                            classNameField={'col-md-8'}
                                            label={trans('messages.company_name')}>
                                            <div className={dataSource.data.company_name?'box-info-text':''}>{dataSource.data.company_name}</div>
                                        </ROW_GROUP>
                                    </div>
                                </div>
                                <div className="row row-spacing">
                                    <div className="col-md-6">
                                        <INPUT
                                            name="zip_code"
                                            labelTitle={trans('messages.customer_postal')}
                                            className="form-control input-sm"
                                            fieldGroupClass="col-md-8"
                                            labelClass="col-md-4"
                                            onChange={this.handleChange}
                                            defaultValue={dataSource.data.zip_code}
                                            errorPopup={true}
                                            hasError={(dataResponse.messages && dataResponse.messages.zip_code)?true:false}
                                            errorMessage={(dataResponse.messages&&dataResponse.messages.zip_code)?dataResponse.messages.zip_code:""}/>
                                    </div>
                                </div>
                                <div className="row row-spacing">
                                    <div className="col-md-12">
                                        <INPUT
                                            name="prefecture"
                                            labelTitle={trans('messages.customer_prefecture')}
                                            className="form-control input-sm"
                                            fieldGroupClass="col-md-9"
                                            labelClass="col-md-3"
                                            onChange={this.handleChange}
                                            defaultValue={dataSource.data.prefecture}
                                            errorPopup={true}
                                            hasError={(dataResponse.messages && dataResponse.messages.prefecture)?true:false}
                                            errorMessage={(dataResponse.messages&&dataResponse.messages.prefecture)?dataResponse.messages.prefecture:""}/>
                                    </div>
                                </div>
                                <div className="row row-spacing">
                                    <div className="col-md-12">
                                        <INPUT
                                            name="city"
                                            labelTitle={trans('messages.customer_city')}
                                            className="form-control input-sm"
                                            fieldGroupClass="col-md-9"
                                            labelClass="col-md-3"
                                            onChange={this.handleChange}
                                            defaultValue={dataSource.data.city}
                                            errorPopup={true}
                                            hasError={(dataResponse.messages && dataResponse.messages.city)?true:false}
                                            errorMessage={(dataResponse.messages&&dataResponse.messages.city)?dataResponse.messages.city:""}/>
                                    </div>
                                </div>
                                <div className="row row-spacing">
                                    <div className="col-md-12">
                                        <INPUT
                                            name="sub_address"
                                            labelTitle={trans('messages.customer_sub_address')}
                                            className="form-control input-sm"
                                            fieldGroupClass="col-md-9"
                                            labelClass="col-md-3"
                                            onChange={this.handleChange}
                                            defaultValue={dataSource.data.sub_address}
                                            errorPopup={true}
                                            hasError={(dataResponse.messages && dataResponse.messages.sub_address)?true:false}
                                            errorMessage={(dataResponse.messages&&dataResponse.messages.sub_address)?dataResponse.messages.sub_address:""}/>
                                    </div>
                                </div>
                                <div className="row row-spacing">
                                    <div className="col-md-3-75">
                                        <INPUT
                                            name="tel_num"
                                            labelTitle={trans('messages.tel_num')}
                                            className="form-control input-sm"
                                            fieldGroupClass="col-md-7"
                                            labelClass="col-md-5 clear-padding-left"
                                            onChange={this.handleChange}
                                            defaultValue={dataSource.data.tel_num}
                                            errorPopup={true}
                                            hasError={(dataResponse.messages && dataResponse.messages.tel_num)?true:false}
                                            errorMessage={(dataResponse.messages&&dataResponse.messages.tel_num)?dataResponse.messages.tel_num:""}/>
                                    </div>
                                    <div className="col-md-3-75">
                                        <INPUT
                                            name="fax_num"
                                            labelTitle={trans('messages.fax_num')}
                                            className="form-control input-sm"
                                            fieldGroupClass="col-md-7"
                                            labelClass="col-md-5 clear-padding-left"
                                            onChange={this.handleChange}
                                            defaultValue={dataSource.data.fax_num}
                                            errorPopup={true}
                                            hasError={(dataResponse.messages && dataResponse.messages.fax_num)?true:false}
                                            errorMessage={(dataResponse.messages&&dataResponse.messages.fax_num)?dataResponse.messages.fax_num:""}/>
                                    </div>
                                    <div className="col-md-4-5">
                                        <INPUT
                                            name="urgent_tel_num"
                                            labelTitle={trans('messages.urgent_tel_num')}
                                            className="form-control input-sm"
                                            fieldGroupClass="col-md-7"
                                            labelClass="col-md-5 clear-padding-left"
                                            onChange={this.handleChange}
                                            defaultValue={dataSource.data.urgent_tel_num}
                                            errorPopup={true}
                                            hasError={(dataResponse.messages && dataResponse.messages.urgent_tel_num)?true:false}
                                            errorMessage={(dataResponse.messages&&dataResponse.messages.urgent_tel_num)?dataResponse.messages.urgent_tel_num:""}/>
                                    </div>
                                </div>
                                <div className="row row-spacing">
                                    <div className="col-md-12">
                                        <INPUT
                                            name="email"
                                            labelTitle={trans('messages.email')}
                                            className="form-control input-sm"
                                            fieldGroupClass="col-md-9"
                                            labelClass="col-md-3"
                                            onChange={this.handleChange}
                                            defaultValue={dataSource.data.email}
                                            errorPopup={true}
                                            hasError={(dataResponse.messages && dataResponse.messages.email)?true:false}
                                            errorMessage={(dataResponse.messages&&dataResponse.messages.email)?dataResponse.messages.email:""}/>
                                    </div>
                                </div>
                                <div className="row row-spacing-double">
                                    <div className="col-md-6">
                                        <SELECT
                                            name="payment_method"
                                            labelTitle={trans('messages.payment_name')}
                                            className="form-control input-sm"
                                            fieldGroupClass="col-md-8"
                                            labelClass="col-md-4"
                                            onChange={this.handleChange}
                                            defaultValue={dataSource.data.payment_method}
                                            options={dataSource.paymentOpt}
                                            errorPopup={true}
                                            hasError={(dataResponse.messages && dataResponse.messages.payment_method)?true:false}
                                            errorMessage={(dataResponse.messages&&dataResponse.messages.payment_method)?dataResponse.messages.payment_method:""}/>
                                    </div>
                                </div>
                                <div className="row row-spacing">
                                    <div className="col-md-6">
                                        <INPUT
                                            disabled={true}
                                            name="sub_total"
                                            labelTitle={trans('messages.total_sub')}
                                            className="form-control input-sm"
                                            fieldGroupClass="col-md-8"
                                            labelClass="col-md-4 text-right"
                                            onChange={this.handleChange}
                                            defaultValue={dataSource.data.goods_price}
                                            errorPopup={true}
                                            hasError={(dataResponse.messages && dataResponse.messages.sub_total)?true:false}
                                            errorMessage={(dataResponse.messages&&dataResponse.messages.sub_total)?dataResponse.messages.sub_total:""}/>
                                    </div>
                                    <div className="col-md-6">
                                        <INPUT
                                            name="pay_charge_discount"
                                            labelTitle={trans('messages.pay_charge_discount')}
                                            className="form-control input-sm"
                                            fieldGroupClass="col-md-8"
                                            labelClass="col-md-4 text-right"
                                            onChange={this.handleChange}
                                            defaultValue={dataSource.data.pay_charge_discount}
                                            errorPopup={true}
                                            hasError={(dataResponse.messages && dataResponse.messages.pay_charge_discount)?true:false}
                                            errorMessage={(dataResponse.messages&&dataResponse.messages.pay_charge_discount)?dataResponse.messages.pay_charge_discount:""}/>
                                    </div>
                                </div>
                                <div className="row row-spacing">
                                    <div className="col-md-6">
                                        <INPUT
                                            name="ship_charge"
                                            labelTitle={trans('messages.ship_charge')}
                                            className="form-control input-sm"
                                            fieldGroupClass="col-md-8"
                                            labelClass="col-md-4 text-right"
                                            onChange={this.handleChange}
                                            defaultValue={dataSource.data.ship_charge}
                                            errorPopup={true}
                                            hasError={(dataResponse.messages && dataResponse.messages.ship_charge)?true:false}
                                            errorMessage={(dataResponse.messages&&dataResponse.messages.ship_charge)?dataResponse.messages.ship_charge:""}/>
                                    </div>
                                    <div className="col-md-6">
                                        <INPUT
                                            name="discount"
                                            labelTitle={trans('messages.discount')}
                                            className="form-control input-sm"
                                            fieldGroupClass="col-md-8"
                                            labelClass="col-md-4 text-right"
                                            onChange={this.handleChange}
                                            defaultValue={dataSource.data.discount}
                                            errorPopup={true}
                                            hasError={(dataResponse.messages && dataResponse.messages.discount)?true:false}
                                            errorMessage={(dataResponse.messages&&dataResponse.messages.discount)?dataResponse.messages.discount:""}/>
                                    </div>
                                </div>
                                <div className="row row-spacing">
                                    <div className="col-md-6">
                                        <INPUT
                                            name="pay_after_charge"
                                            labelTitle={trans('messages.pay_after_charge')}
                                            className="form-control input-sm"
                                            fieldGroupClass="col-md-8"
                                            labelClass="col-md-4 text-right"
                                            onChange={this.handleChange}
                                            defaultValue={dataSource.data.pay_after_charge}
                                            errorPopup={true}
                                            hasError={(dataResponse.messages && dataResponse.messages.pay_after_charge)?true:false}
                                            errorMessage={(dataResponse.messages&&dataResponse.messages.pay_after_charge)?dataResponse.messages.pay_after_charge:""}/>
                                    </div>
                                    <div className="col-md-6">
                                        <INPUT
                                            name="used_point"
                                            labelTitle={trans('messages.used_point')}
                                            className="form-control input-sm"
                                            fieldGroupClass="col-md-8"
                                            labelClass="col-md-4 text-right"
                                            onChange={this.handleChange}
                                            defaultValue={dataSource.data.used_point}
                                            errorPopup={true}
                                            hasError={(dataResponse.messages && dataResponse.messages.used_point)?true:false}
                                            errorMessage={(dataResponse.messages&&dataResponse.messages.used_point)?dataResponse.messages.used_point:""}/>
                                    </div>
                                </div>
                                <div className="row row-spacing">
                                    <div className="col-md-6">
                                        <INPUT
                                            name="pay_charge"
                                            labelTitle={trans('messages.pay_charge')}
                                            className="form-control input-sm"
                                            fieldGroupClass="col-md-8"
                                            labelClass="col-md-4 text-right"
                                            onChange={this.handleChange}
                                            defaultValue={dataSource.data.pay_charge}
                                            errorPopup={true}
                                            hasError={(dataResponse.messages && dataResponse.messages.pay_charge)?true:false}
                                            errorMessage={(dataResponse.messages&&dataResponse.messages.pay_charge)?dataResponse.messages.pay_charge:""}/>
                                    </div>
                                    <div className="col-md-6">
                                        <INPUT
                                            name="used_coupon"
                                            labelTitle={trans('messages.used_coupon')}
                                            className="form-control input-sm"
                                            fieldGroupClass="col-md-8"
                                            labelClass="col-md-4 text-right"
                                            onChange={this.handleChange}
                                            defaultValue={dataSource.data.used_coupon}
                                            errorPopup={true}
                                            hasError={(dataResponse.messages && dataResponse.messages.used_coupon)?true:false}
                                            errorMessage={(dataResponse.messages&&dataResponse.messages.used_coupon)?dataResponse.messages.used_coupon:""}/>
                                    </div>
                                </div>
                                <div className="row row-spacing">
                                    <div className="col-md-6">
                                        <INPUT
                                            name="total_price"
                                            labelTitle={trans('messages.total_price')}
                                            className="form-control input-sm"
                                            fieldGroupClass="col-md-8"
                                            labelClass="col-md-4 text-right"
                                            onChange={this.handleChange}
                                            defaultValue={dataSource.data.total_price}
                                            errorPopup={true}
                                            hasError={(dataResponse.messages && dataResponse.messages.total_price)?true:false}
                                            errorMessage={(dataResponse.messages&&dataResponse.messages.total_price)?dataResponse.messages.total_price:""}/>
                                    </div>
                                    <div className="col-md-6">
                                        <INPUT
                                            name="request_price"
                                            labelTitle={trans('messages.request_price')}
                                            className="form-control input-sm"
                                            fieldGroupClass="col-md-8"
                                            labelClass="col-md-4 text-right"
                                            onChange={this.handleChange}
                                            defaultValue={dataSource.data.request_price}
                                            errorPopup={true}
                                            hasError={(dataResponse.messages && dataResponse.messages.request_price)?true:false}
                                            errorMessage={(dataResponse.messages&&dataResponse.messages.request_price)?dataResponse.messages.request_price:""}/>
                                    </div>
                                </div>
                            </div>
                            {/* End left column */}

                            {/* Start right column */}
                            <div className="col-md-4">
                                <div className="row row-spacing-double">
                                    <div className="col-md-12">
                                        <ROW_GROUP
                                            classNameLabel={'col-md-12 control-label'}
                                            classNameField={'col-md-12'}
                                            label={ trans('messages.customer_question')}>
                                            <div className="box-info-order height-100 text-new-line">
                                                {dataSource.data.customer_question}
                                            </div>
                                        </ROW_GROUP>
                                    </div>
                                </div>
                                <div className="row row-spacing-double">
                                    <div className="col-md-12">
                                        <TEXTAREA
                                            name="shop_answer"
                                            labelTitle={trans('messages.shop_answer')}
                                            className="form-control input-sm"
                                            fieldGroupClass="col-md-12"
                                            labelClass="col-md-12"
                                            onChange={this.handleChange}
                                            defaultValue={dataSource.data.shop_answer}
                                            errorPopup={true}
                                            hasError={(dataResponse.messages && dataResponse.messages.shop_answer)?true:false}
                                            errorMessage={(dataResponse.messages&&dataResponse.messages.shop_answer)?dataResponse.messages.shop_answer:""}
                                            rows="6"/>
                                    </div>
                                </div>
                                <div className="row row-spacing-double">
                                    <div className="col-md-12">
                                        <TEXTAREA
                                            name="delivery_instrustions"
                                            labelTitle={trans('messages.delivery_instrustions_col')}
                                            className="form-control input-sm"
                                            fieldGroupClass="col-md-12"
                                            labelClass="col-md-12"
                                            onChange={this.handleChange}
                                            defaultValue={dataSource.data.delivery_instrustions}
                                            errorPopup={true}
                                            hasError={(dataResponse.messages && dataResponse.messages.delivery_instrustions)?true:false}
                                            errorMessage={(dataResponse.messages&&dataResponse.messages.delivery_instrustions)?dataResponse.messages.delivery_instrustions:""}
                                            rows="8"/>
                                    </div>
                                </div>
                            </div>
                            {/* End right column */}
                        </div>
                        <div className="box-footer">
                            <div className="row margin-top-bot">
                                <div className="col-md-9">

                                </div>
                                <div className="col-md-3 text-right">
                                    <BUTTON name="btnAccept" className="btn btn-success btn-sm btn-larger  margin-element" type="button" onClick={this.handleSubmit}>{trans('messages.btn_accept')}</BUTTON>
                                    <BUTTON className="btn bg-purple btn-sm btn-larger margin-element" type="button" onClick={this.handleCancel}>{trans('messages.btn_cancel')}</BUTTON>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            </FORM>
            <MESSAGE_MODAL
                message={this.state.message || ''}
                title={trans('messages.deny_edit')}
                baseUrl={baseUrl}
            />
            <RepayTypePopup
                handleSubmitRepay={this.handleSubmitRepay}
            />
            </div>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        dataSource: state.commonReducer.dataSource,
        dataResponse: state.commonReducer.dataResponse,
        dataRealTime: state.commonReducer.dataRealTime
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        reloadData: (url, params) => { dispatch(Action.detailData(url, params)) },
        postData: (url, params, headers) => { dispatch(Action.postData(url, params, headers)) },
        postRealTime: (url, params, headers) => { dispatch(Action.postRealTime(url, params, headers)) }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(OrderEditing)