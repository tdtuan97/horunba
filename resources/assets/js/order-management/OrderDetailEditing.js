import React, { Component } from 'react';
import { connect } from 'react-redux';

import * as Action from '../common/actions';
import * as queryString from 'query-string';
import axios from 'axios';
import moment from 'moment';

import DATEPICKER from '../common/components/form/DATEPICKER';
import Loading from '../common/components/common/Loading';
import FORM from '../common/components/form/FORM';
import INPUT from '../common/components/form/INPUT';
import SELECT from '../common/components/form/SELECT';
import TEXTAREA from '../common/components/form/TEXTAREA';
import BUTTON from '../common/components/form/BUTTON';
import ROW_GROUP from '../common/components/table/ROW_GROUP';
import LABEL from '../common/components/form/LABEL';
import MESSAGE_MODAL from '../common/containers/MessageModal'
import CHECKBOX_CUSTOM from '../common/components/form/CHECKBOX_CUSTOM';
import CancelReasonPopup from './containers/CancelReasonPopup';
import NotEnoughtCancelPopup from './containers/NotEnoughtCancelPopup';
import RepayTypePopup from './containers/RepayTypePopup'
import RepayTypeCancelPopup from './containers/RepayTypeCancelPopup'
import RepayTypeNotEnoughtCancelPopup from './containers/RepayTypeNotEnoughtCancelPopup'

let timer;
let timerQuantity;

class OrderDetailEditing extends Component {
    constructor(props) {
        super(props);
        this.state = {}
        this.firstReceiveProps = true;
        let params = queryString.parse(props.location.search);
        if (params['receive_id']) {
            this.state = {receive_id: params['receive_id'] , index : params['receive_id'], receiver_id: params['receiver_id']};
            this.hanleRealtime();
        }
        this.props.reloadData(getFormDataUrl, this.state);
        this.flg = true;
        this.curReason = 0;
        this.curCancelReason = 0;
    }
    hanleRealtime() {
        if (!_.isEmpty(this.state.index)) {
            let params = {
                accessFlg : this.state.accessFlg,
                query : {
                        page_key : 'ORDER-DETAIL-EDITING',
                        primary_key : 'index:' + this.state.index
                    },
                key :this.state.index
            };
            if (this.state.accessFlg !== false) {
                this.props.postRealTime(checkEditUrl, params, {'X-Requested-With': 'XMLHttpRequest'});
            }
        }
    }
    handleCancel = (event) => {
        if (paramsFilter !== '') {
            var stringParamsFilter = paramsFilter.split("|").join("&");
            window.location.href = detailUrl + '?' + stringParamsFilter;
        } else {
            window.location.href = detailUrl + '?receive_id_key=' + this.state.receive_id;
        }
    }

    handleSubmit = () => {
        document.getElementsByName('btnAccept')[0].disabled = true;
        let headers = {'X-Requested-With': 'XMLHttpRequest'};
        this.flg = true;
        $(".loading").show();
        let params = {...this.state, check_show_repay_popup : 1};
        axios.post(saveUrl, params, {headers : headers}
        ).then(response => {
            $(".loading").hide();
            let data = response.data;
            if (data.status === 3) {
                document.getElementsByName('btnAccept')[0].disabled = false;
                $('#repay-type-popup').modal('show');
            } else {
                this.props.postData(saveUrl, this.state, headers);
            }
        }).catch(error => {
            $(".loading").hide();
            throw(error);
        });
    }

    handleSubmitRepay = () => {
        let repayType = $('#repay-type-popup').find('input[name="repay_type"]:checked').val();
        let headers = {'X-Requested-With': 'XMLHttpRequest'};
        this.props.postData(saveUrl, {...this.state, repay_type : repayType}, headers);
    }

    handleChange = (event) => {
        const target = event.target;
        const name = target.name;
        const value = target.type === 'checkbox' ? (target.checked?1:0) : target.value;
        this.setState({
            [name]: value
        });
    }

    handleAddNewLine = () => {
        let newElement = {
            product_code: '',
            product_name: '',
            price: '',
            quantity: '',
            product_status: '',
            new_product: 1,
            child_data: []
        }
        this.setState({
            product_detail: [...this.state.product_detail, newElement]
        });
    }

    handleDeleteNewLine = (index) => {
        if (this.props.dataResponse.product_mess && this.props.dataResponse.product_mess[index]) {
            this.props.dataResponse.product_mess[index] = {};
        }
        this.setState((prevState) => ({
            product_detail: [
                ...prevState.product_detail.slice(0,index),
                ...prevState.product_detail.slice(index+1)
            ]
        }));
    }

    handleChangeProductDetail = (index, event) => {
        const target = event.target;
        const name = target.name;
        const value = target.type === 'checkbox' ? target.checked : target.value;
        let currentDetail = [...this.state.product_detail];
        currentDetail[index][name] = value;

        this.setState({
            product_detail: currentDetail
        }, () => {
            if (name === 'product_code') {
                clearTimeout(timer);
                let _this = this;
                timer = setTimeout(function() {
                    _this.getInfoProduct(index, value);
                }, 1500);
            }
        });
    }

    getInfoProduct = (index, productCode) => {
        $(".loading").show();

        let headers = {'X-Requested-With': 'XMLHttpRequest'};
        let params = { product_code : _.trim(productCode), mall_id : _.trim(this.state.mall_id) };
        axios.post(getDataProductUrl, params, {headers : headers}
        ).then(response => {
            $(".loading").hide();
            let data = response.data.data;
            let currentDetail = [...this.state.product_detail];
            let productName = '';
            let price       = '';
            let childData   = [];
            if (data && data.product_code) {
                productName = data.product_name;
                price       = data.price;
                childData   = data.child_data;
            }
            currentDetail[index]['product_name'] = productName;
            currentDetail[index]['price']        = price;
            currentDetail[index]['child_data']   = childData;
            this.setState({
                product_detail: currentDetail
            });
        }).catch(error => {
            $(".loading").hide();
            throw(error);
        });
    }

    handleCancelNotEnoughtCancelPopup = () => {
        delete this.state.product_not_cancel;
        $('#not-cancel-modal').modal('hide');
    }

    handleSubmitNotEnoughtCancelPopup = (cancel_reason) => {
        this.curCancelReason = cancel_reason;
        if (this.state.status_reason === 0) {
            this.setState({check_cancel:true}, () => {
                this.handleSubmit();
            });
        } else {
            let paramCheck = {receive_id : this.state.receive_id, cancel_reason : cancel_reason, check_show_repay_cancel_popup : 1};
            $(".loading").show();
            axios.post(cancelNotEnought, paramCheck , {headers: {'X-Requested-With': 'XMLHttpRequest'}})
            .then(response => {
                if (response.data.status === 5) {
                    $('#repay-type-not-enought-cancel-popup').modal('show');
                } else {
                    let param = {receive_id : this.state.receive_id, cancel_reason : cancel_reason};
                    $(".loading").show();
                    axios.post(cancelNotEnought, param , {headers: {'X-Requested-With': 'XMLHttpRequest'}})
                    .then(response => {
                        if (response.data.status === 1) {
                            if (paramsFilter !== '') {
                                var stringParamsFilter = paramsFilter.split("|").join("&");
                                window.location.href = detailUrl + '?' + stringParamsFilter;
                            } else {
                                window.location.href = detailUrl + '?receive_id_key=' + this.state.receive_id;
                            }
                        }
                        delete this.state.product_not_cancel;
                        $('#not-cancel-modal').modal('hide');
                        $(".loading").hide();
                    }).catch(error => {
                        $(".loading").hide();
                        throw(error);
                    });
                }
                $(".loading").hide();
            }).catch(error => {
                $(".loading").hide();
                throw(error);
            });
        }

    }

    handleSubmitRepayNotEnoughtCancel = () => {

        let repayType = $('#repay-type-not-enought-cancel-popup').find('input[name="repay_type"]:checked').val();

        let param = {receive_id : this.state.receive_id, cancel_reason : this.curCancelReason , repay_type : repayType};
        $(".loading").show();
        axios.post(cancelNotEnought, param , {headers: {'X-Requested-With': 'XMLHttpRequest'}})
        .then(response => {
            if (response.data.status === 1) {
                if (paramsFilter !== '') {
                    var stringParamsFilter = paramsFilter.split("|").join("&");
                    window.location.href = detailUrl + '?' + stringParamsFilter;
                } else {
                    window.location.href = detailUrl + '?receive_id_key=' + this.state.receive_id;
                }
            }
            delete this.state.product_not_cancel;
            $('#not-cancel-modal').modal('hide');
            $(".loading").hide();
        }).catch(error => {
            $(".loading").hide();
            throw(error);
        });
    }

    componentWillReceiveProps(nextProps) {
        if (!_.isEmpty(nextProps.dataSource)) {
            if (!nextProps.dataSource.data) {
                window.location.href = baseUrl;
            } else if (this.firstReceiveProps) {
                this.firstReceiveProps = false;
                this.setState({...nextProps.dataSource.data});
            }
        }
        if (!_.isEmpty(nextProps.dataResponse) && this.flg) {
            if (nextProps.dataResponse.status === 1) {
                if (paramsFilter !== '') {
                    var stringParamsFilter = paramsFilter.split("|").join("&");
                    window.location.href = detailUrl + '?' + stringParamsFilter;
                } else {
                    window.location.href = detailUrl + '?receive_id_key=' + this.state.receive_id;
                }
            } else {
                if (nextProps.dataResponse.status === -1) {
                    let message = nextProps.dataResponse.messages;
                    if (typeof nextProps.dataResponse.messages === 'object') {
                        message = nextProps.dataResponse.messages.errorInfo;
                    }
                    if (typeof nextProps.dataResponse.flg_not_cancel !== 'undefined') {
                        this.setState({
                            product_not_cancel: nextProps.dataResponse.not_cancel,
                            'status_reason': 0
                        });
                        $('#not-cancel-modal').modal('show');
                    } else {
                        $.notify({
                            icon: 'glyphicon glyphicon-warning-sign',
                            message: message,
                        },{
                            type: 'danger'
                        });
                    }
                }
                document.getElementsByName('btnAccept')[0].disabled = false;
            }
            this.flg = false;
        }
        // Check realtime update
        if (!_.isEmpty(nextProps.dataRealTime)) {
            this.setState(nextProps.dataRealTime);
        }
    }

    componentDidUpdate() {
        if (!_.isEmpty(this.props.dataSource)) {
            $('.loading').hide();
        }
        if (this.state.accessFlg === false) {
            $('#message-modal').modal('show');
            //We will auto redirect after 5 seconds
            setTimeout(() => {
                window.location.href = baseUrl;
            }, 5000);
        }
        let arrHide = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '10', '11', '12', '13', '14', '16', '17', '18', '19', '20', '21', '99', '101'];
        let selectStatus = $('select.product_status');
        selectStatus.map((k,v) => {
            if (typeof v.dataset.target != 'undefined') {
                $.each( v.options, function( key, value ) {
                    if (v.dataset.target != $(this).val() && arrHide.indexOf($(this).val()) !== -1) {
                        $(this).hide();
                    }
                });
            }
        })


    }
    // Check realtime check for editing
    componentWillUnmount () {
        clearInterval(this.state);
    }
    // Check realtime check for editing
    componentDidMount() {
        setInterval(() => {
            this.hanleRealtime()
        }, 5000);

    }

    handleChangeDate = function(date, name, event) {
        let dateValue = moment(date);
        if (!dateValue.isValid()) {
            dateValue = undefined;
        } else {
            if (name === 'ship_wish_time') {
                dateValue = dateValue.format('HH:mm');
            } else {
                dateValue = dateValue.format('YYYY-MM-DD');
            }
        }
        this.setState({
          [name]: dateValue,
        });
    }

    handleSubmitCancelReason = (reasonId) => {
        this.curReason = reasonId;
        let paramCheck = {reason_id :reasonId, receive_id : this.state.receive_id, check_show_repay_cancel_popup : 1};
        $(".loading").show();
        axios.post(cancelReasonUrl, paramCheck , {headers: {'X-Requested-With': 'XMLHttpRequest'}})
        .then(response => {
            if (response.data.status === 5) {
                $('#repay-type-cancel-popup').modal('show');
            } else {
                let param = {reason_id :reasonId, receive_id : this.state.receive_id};
                $(".loading").show();
                axios.post(cancelReasonUrl, param , {headers: {'X-Requested-With': 'XMLHttpRequest'}})
                .then(response => {
                    if (response.data.status === 1) {
                        this.setState({cancel_all : 1}, () => {
                            this.handleSubmit();
                        })
                    }
                    $(".loading").hide();
                }).catch(error => {
                    $(".loading").hide();
                    throw(error);
                });
            }
            $(".loading").hide();
        }).catch(error => {
            $(".loading").hide();
            throw(error);
        });
    }


    handleSubmitRepayCancel = () => {
        let repayType = $('#repay-type-cancel-popup').find('input[name="repay_type"]:checked').val();

        let param = {reason_id : this.curReason, receive_id : this.state.receive_id, repay_type : repayType};
        $(".loading").show();
        axios.post(cancelReasonUrl, param , {headers: {'X-Requested-With': 'XMLHttpRequest'}})
        .then(response => {
            if (response.data.status === 1) {
                this.setState({cancel_all : 1}, () => {
                    this.handleSubmit();
                })
            }
            $(".loading").hide();
        }).catch(error => {
            $(".loading").hide();
            throw(error);
        });
    }

    handleCheckBeforeSave = () => {
        let currentDetail = this.state.product_detail;
        let flg_check = false;
        for (let i = 0; i < currentDetail.length ; i++) {
            if (currentDetail[i]['product_status'] != _.status('PRODUCT_STATUS.CANCEL')) {
                flg_check = true;
                break;
            }
        }
        if (!flg_check) {
            axios.post(checkCancelAll, {receive_id: this.state.receive_id, receiver_id: this.state.receiver_id},
            {headers: {'X-Requested-With': 'XMLHttpRequest'}}
            ).then(response => {
                if (response.data.status === -1) {
                    this.setState({
                        product_not_cancel: response.data.not_cancel,
                        'status_reason': 1
                    });
                    $('#not-cancel-modal').modal('show');
                } else if (response.data.status === 1) {
                    $('#cancel-reason-modal').modal('show');
                } else {
                    this.handleSubmit();
                }
            }).catch(error => {
                throw(error);
            });
        } else {
            this.handleSubmit();
        }
    }

    render() {
        let dataSource   = this.props.dataSource;
        let dataResponse = this.props.dataResponse;
        let isError      = this.state.isError;
        let nf           = new Intl.NumberFormat();
        let count13      = 0;
        let count2       = 0;
        let countNew     = 0;
        return ((!_.isEmpty(dataSource) && dataSource.data) &&

            <div className="row" id="edit-order">
                <Loading className="loading" />
                <div className="col-md-12">
                    <div className="box box-success">
                        <div className="box-body">
                            <FORM className="form-custom">
                            <div className="row row-spacing">
                                <div className="col-md-12">
                                    <div className="row">
                                        <div className="col-md-12">
                                            <div className="col-md-12">{trans('messages.destination')}</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className="row">
                                <div className="col-md-7">
                                    <div className="row row-spacing">
                                                <div className="col-md-3">
                                                    <INPUT
                                                        name="last_name"
                                                        labelTitle={trans('messages.last_name')}
                                                        className="form-control input-sm"
                                                        fieldGroupClass="col-md-9"
                                                        labelClass="col-md-3"
                                                        groupClass="form-group"
                                                        onChange={this.handleChange}
                                                        value={this.state.last_name}
                                                        errorPopup={true}
                                                        hasError={(dataResponse.messages && dataResponse.messages.last_name)?true:false}
                                                        errorMessage={(dataResponse.messages&&dataResponse.messages.last_name)?dataResponse.messages.last_name:""}/>
                                                </div>
                                                <div className="col-md-3">
                                                    <INPUT
                                                        name="first_name"
                                                        labelTitle={trans('messages.first_name')}
                                                        className="form-control input-sm"
                                                        fieldGroupClass="col-md-9"
                                                        labelClass="col-md-3"
                                                        groupClass="form-group"
                                                        onChange={this.handleChange}
                                                        value={this.state.first_name}
                                                        errorPopup={true}
                                                        hasError={(dataResponse.messages && dataResponse.messages.first_name)?true:false}
                                                        errorMessage={(dataResponse.messages&&dataResponse.messages.first_name)?dataResponse.messages.first_name:""}/>
                                                </div>

                                        <div className="col-md-6">
                                            <INPUT
                                                name="tel_num"
                                                labelTitle={trans('messages.tel_num')}
                                                className="form-control input-sm"
                                                fieldGroupClass="col-md-8"
                                                labelClass="col-md-4"
                                                onChange={this.handleChange}
                                                value={this.state.tel_num}
                                                errorPopup={true}
                                                hasError={(dataResponse.messages && dataResponse.messages.tel_num)?true:false}
                                                errorMessage={(dataResponse.messages&&dataResponse.messages.tel_num)?dataResponse.messages.tel_num:""}/>
                                        </div>
                                    </div>
                                    <div className="row row-spacing">
                                        <div className="col-md-12">
                                            <INPUT
                                                name="zip_code"
                                                labelTitle={trans('messages.customer_postal')}
                                                className="form-control input-sm"
                                                fieldGroupClass="col-md-8"
                                                labelClass="col-md-4"
                                                onChange={this.handleChange}
                                                value={this.state.zip_code}
                                                errorPopup={true}
                                                hasError={(dataResponse.messages && dataResponse.messages.zip_code)?true:false}
                                                errorMessage={(dataResponse.messages&&dataResponse.messages.zip_code)?dataResponse.messages.zip_code:""}/>
                                        </div>
                                    </div>
                                    <div className="row row-spacing">
                                        <div className="col-md-12">
                                            <INPUT
                                                name="prefecture"
                                                labelTitle={trans('messages.customer_prefecture')}
                                                className="form-control input-sm"
                                                fieldGroupClass="col-md-8"
                                                labelClass="col-md-4"
                                                onChange={this.handleChange}
                                                value={this.state.prefecture}
                                                errorPopup={true}
                                                hasError={(dataResponse.messages && dataResponse.messages.prefecture)?true:false}
                                                errorMessage={(dataResponse.messages&&dataResponse.messages.prefecture)?dataResponse.messages.prefecture:""}/>
                                        </div>
                                    </div>
                                    <div className="row row-spacing">
                                        <div className="col-md-12">
                                            <INPUT
                                                name="city"
                                                labelTitle={trans('messages.customer_city')}
                                                className="form-control input-sm"
                                                fieldGroupClass="col-md-8"
                                                labelClass="col-md-4"
                                                onChange={this.handleChange}
                                                value={this.state.city}
                                                errorPopup={true}
                                                hasError={(dataResponse.messages && dataResponse.messages.city)?true:false}
                                                errorMessage={(dataResponse.messages&&dataResponse.messages.city)?dataResponse.messages.city:""}/>
                                        </div>
                                    </div>
                                    <div className="row row-spacing">
                                        <div className="col-md-12">
                                            <INPUT
                                                name="sub_address"
                                                labelTitle={trans('messages.customer_sub_address')}
                                                className="form-control input-sm"
                                                fieldGroupClass="col-md-8"
                                                labelClass="col-md-4"
                                                onChange={this.handleChange}
                                                value={this.state.sub_address}
                                                errorPopup={true}
                                                hasError={(dataResponse.messages && dataResponse.messages.sub_address)?true:false}
                                                errorMessage={(dataResponse.messages&&dataResponse.messages.sub_address)?dataResponse.messages.sub_address:""}/>
                                        </div>
                                    </div>
                                </div>
                                <div className="col-md-5">
                                    <div className="row row-spacing">
                                        <div className="col-md-12">
                                            <div className="col-md-12">{trans('messages.delivery_type_nanko')}</div>
                                        </div>
                                    </div>
                                    <div className="row row-spacing">
                                        <div className="col-md-12">
                                            <ROW_GROUP
                                                classNameLabel={'col-md-4 control-label'}
                                                classNameField={'col-md-8'}
                                                label={trans('messages.delivery_plan_date')}
                                                contents={this.state.delivery_plan_date} />
                                        </div>
                                    </div>
                                    <div className="row row-spacing">
                                        <div className="col-md-12">
                                            <DATEPICKER
                                                placeholderText="YYYY-MM-DD"
                                                dateFormat="YYYY-MM-DD"
                                                locale="ja"
                                                minDate={moment()}
                                                name="ship_wish_date"
                                                labelTitle={trans('messages.ship_wish_date')}
                                                className="form-control input-sm"
                                                fieldGroupClass="col-md-5"
                                                labelClass="col-md-4"
                                                selected={this.state.ship_wish_date ? moment(this.state.ship_wish_date) : null}
                                                onChange={(date,name, e) => this.handleChangeDate(date, 'ship_wish_date', e)} />
                                        </div>
                                    </div>
                                    <div className="row row-spacing">
                                        <div className="col-md-12">
                                                <SELECT
                                                    name="ship_wish_time"
                                                    labelTitle={trans('messages.ship_wish_time')}
                                                    className="form-control input-sm"
                                                    fieldGroupClass="col-md-5"
                                                    labelClass="col-md-4"
                                                    onChange={this.handleChange}
                                                    value={this.state.ship_wish_time||''}
                                                    options={dataSource.shipWishDateOpt}
                                                    errorPopup={true}
                                                    hasError={(dataResponse.messages && dataResponse.messages.ship_wish_time)?true:false}
                                                    errorMessage={(dataResponse.messages&&dataResponse.messages.ship_wish_time)?dataResponse.messages.ship_wish_time:""} />
                                        </div>
                                    </div>
                                    <div className="row row-spacing">
                                        <div className="col-md-12">
                                        {

                                            <SELECT
                                                name="delivery_code"
                                                labelTitle={trans('messages.delivery_method')}
                                                className="form-control input-sm"
                                                fieldGroupClass="col-md-5"
                                                labelClass="col-md-4"
                                                onChange={this.handleChange}
                                                value={this.state.delivery_code||''}
                                                options={dataSource.companyOpt}
                                                errorPopup={true}
                                                hasError={(dataResponse.messages && dataResponse.messages.delivery_code)?true:false}
                                                errorMessage={(dataResponse.messages&&dataResponse.messages.delivery_code)?dataResponse.messages.delivery_code:""}/>

                                        }
                                        </div>
                                    </div>
                                </div>
                            </div>

                          </FORM>
                            <div className="row">
                                <div className="col-md-12">
                                <div className="table-responsive">
                                    <table className="table table-striped table-custom">
                                        <thead>
                                            <tr>
                                                <th className="text-left col-md-1">{trans('messages.no')}</th>
                                                <th className="text-left col-md-6">{trans('messages.product_name')}</th>
                                                <th className="text-right col-md-1">{trans('messages.price')}</th>
                                                <th className="text-center col-md-1">{trans('messages.quantity')}</th>
                                                <th className="text-right col-md-1">{trans('messages.sub_total')}</th>
                                                <th className="text-right col-md-1-25">{trans('messages.product_status')}</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        {
                                            (!_.isEmpty(this.state.product_detail))?
                                            this.state.product_detail.map((item, index) => {
                                                let productCode       = item.product_code;
                                                let productName       = item.product_name;
                                                let productPrice      = item.price;
                                                let productQuantity   = item.quantity;
                                                let productStatus     = item.product_status;
                                                let deliveryType      = item.delivery_type;
                                                let productStatusName = '';
                                                dataSource.productStatusOpt.map((ps) => {
                                                    if (item.product_status == ps.key && ps.key != 0) {
                                                        productStatusName = ps.value;
                                                    }
                                                });
                                                if (item.child_product_code) {
                                                    productCode     = item.child_product_code;
                                                    productName     = item.child_product_name;
                                                    productPrice    = item.price_invoice;
                                                    productQuantity = item.received_order_num;
                                                }
                                                let classRow = (index%2 ===0) ? 'odd' : 'even';
                                                if (deliveryType === 1 || deliveryType === 3) {
                                                    count13 += 1;
                                                }
                                                if (deliveryType === 2) {
                                                    count2 += 1;
                                                }
                                                if (item.new_product) {
                                                    countNew += 1;
                                                }
                                                return ([
                                                    <tr>
                                                    {count13 === 1 && count2 === 0 && countNew === 0 &&
                                                        <td colSpan="8">
                                                            <div className="col-md-1-5 col-xs-1-5 clear-padding-right">
                                                                <b>{trans('messages.delivery_type_nanko')}</b>
                                                            </div>
                                                        </td>
                                                    }
                                                    {count2 === 1 && countNew === 0 &&
                                                            <td colSpan="8">
                                                                <div className="col-md-1-5 col-xs-1-5">
                                                                    <b>{trans('messages.delivery_type_supplier')}</b>
                                                                </div>
                                                            </td>
                                                    }
                                                    {countNew === 1 &&
                                                            <td colSpan="8">
                                                                <hr className="hr-margin" />
                                                            </td>
                                                    }
                                                    </tr>,
                                                    <tr key={"product_detail" + index} className={classRow}>
                                                        <td className="col-max-min-120">
                                                        {
                                                            item.new_product?
                                                            <INPUT
                                                                name="product_code"
                                                                groupClass=" "
                                                                className="form-control input-sm"
                                                                value={productCode}
                                                                onChange={(e) => this.handleChangeProductDetail(index, e)}
                                                                errorPopup={true}
                                                                hasError={
                                                                    (dataResponse.product_mess &&
                                                                    dataResponse.product_mess[index] &&
                                                                    dataResponse.product_mess[index]['product_code'])?
                                                                    true:false
                                                                }
                                                                errorMessage={
                                                                    (dataResponse.product_mess &&
                                                                    dataResponse.product_mess[index] &&
                                                                    dataResponse.product_mess[index]['product_code'])?
                                                                    dataResponse.product_mess[index]['product_code']:""
                                                                }
                                                            />
                                                            :productCode
                                                        }
                                                        </td>
                                                        <td className="col-max-min-300 text-justify">
                                                            <p>{productName}</p>
                                                        </td>
                                                        <td className="text-right col-max-min-120">
                                                        {
                                                            item.new_product?
                                                                <INPUT
                                                                    name="price"
                                                                    className="form-control input-sm"
                                                                    groupClass=" "
                                                                    fieldGroupClass="input-group"
                                                                    value={productPrice}
                                                                    onChange={(e) => this.handleChangeProductDetail(index, e)}
                                                                    iconPrefix={true}
                                                                    iconPrefixClass="fa fa-yen"
                                                                    errorPopup={true}
                                                                    hasError={
                                                                        (dataResponse.product_mess &&
                                                                        dataResponse.product_mess[index] &&
                                                                        dataResponse.product_mess[index]['price'])?
                                                                        true:false
                                                                    }
                                                                    errorMessage={
                                                                        (dataResponse.product_mess &&
                                                                        dataResponse.product_mess[index] &&
                                                                        dataResponse.product_mess[index]['price'])?
                                                                        dataResponse.product_mess[index]['price']:""
                                                                    }
                                                                />
                                                            :
                                                            '￥' + nf.format(productPrice)
                                                        }
                                                        </td>
                                                        <td className="text-right col-max-min-120" >
                                                        {
                                                            item.new_product?
                                                            <INPUT
                                                                name="quantity"
                                                                className="form-control input-sm"
                                                                groupClass=" "
                                                                value={productQuantity}
                                                                onChange={(e) => this.handleChangeProductDetail(index, e)}
                                                                errorPopup={true}
                                                                hasError={
                                                                    (dataResponse.product_mess &&
                                                                    dataResponse.product_mess[index] &&
                                                                    dataResponse.product_mess[index]['quantity'])?
                                                                    true:false
                                                                }
                                                                errorMessage={
                                                                    (dataResponse.product_mess &&
                                                                    dataResponse.product_mess[index] &&
                                                                    dataResponse.product_mess[index]['quantity'])?
                                                                    dataResponse.product_mess[index]['quantity']:""
                                                                }
                                                            />
                                                            :
                                                            (
                                                                isError ||
                                                                (!isError && this.state.order_status > _.status('ORDER_STATUS.ORDER')) ||
                                                                ((deliveryType === 1 && productStatus > 9) ||
                                                                ((deliveryType === 2 || deliveryType === 3 ) && (productStatus === 1 || productStatus === 4 || productStatus >= 7))
                                                            ) ?
                                                            nf.format(productQuantity)
                                                            :
                                                            <INPUT
                                                                name="new_quantity"
                                                                className="form-control input-sm"
                                                                value={(typeof item.new_quantity !== 'undefined') ? item.new_quantity :productQuantity}
                                                                type="number"
                                                                onChange={(e) => this.handleChangeProductDetail(index, e)}
                                                                errorPopup={true}
                                                                groupClass=" "
                                                                hasError={
                                                                    (dataResponse.product_mess &&
                                                                    dataResponse.product_mess[index] &&
                                                                    dataResponse.product_mess[index]['new_quantity'])?
                                                                    true:false
                                                                }
                                                                errorMessage={
                                                                    (dataResponse.product_mess &&
                                                                    dataResponse.product_mess[index] &&
                                                                    dataResponse.product_mess[index]['new_quantity'])?
                                                                    dataResponse.product_mess[index]['new_quantity']:""
                                                                }
                                                            />)
                                                        }
                                                        </td>
                                                        <td className="text-right col-max-min-120">
                                                        {
                                                            '￥' + nf.format(productPrice * ((typeof item.new_quantity !== 'undefined') ? item.new_quantity :productQuantity))
                                                        }
                                                        </td>
                                                        {
                                                            (item.new_product === 0) &&
                                                        <td className="text-right col-max-min-120">

                                                            <SELECT
                                                                name="product_status"
                                                                className={"form-control input-sm product_status " + ((isError || (!isError && this.state.order_status > _.status('ORDER_STATUS.ARRIVAL'))) ? 'disable-link' : '')}
                                                                groupClass=" "
                                                                value={isError ? '' : productStatus}
                                                                onChange={(e) => this.handleChangeProductDetail(index, e)}
                                                                data-target={item.product_status_root}
                                                                errorPopup={true}
                                                                options={dataSource.productStatusOpt}
                                                                hasError={
                                                                    (dataResponse.product_mess &&
                                                                    dataResponse.product_mess[index] &&
                                                                    dataResponse.product_mess[index]['product_status'])?
                                                                    true:false
                                                                }
                                                                errorMessage={
                                                                    (dataResponse.product_mess &&
                                                                    dataResponse.product_mess[index] &&
                                                                    dataResponse.product_mess[index]['product_status'])?
                                                                    dataResponse.product_mess[index]['product_status']:""
                                                                }
                                                                readOnly={isError ? true : false}
                                                            />
                                                        </td>
                                                        }
                                                        {
                                                            (item.new_product===1)&&
                                                            <td className="col-max-min-60 text-right">
                                                                <BUTTON
                                                                    name="btnDeleteNewLine"
                                                                    className="btn btn-danger btn-sm"
                                                                    type="button"
                                                                    onClick={(e) => this.handleDeleteNewLine(index, e)}>
                                                                    <i className="fa fa-trash"></i>
                                                                </BUTTON>
                                                            </td>
                                                        }
                                                    </tr>,
                                                    (!isError && item.child_data.map((child, subIndex) => {
                                                        let productStatusNameSub = '';
                                                        dataSource.productStatusOpt.map((ps) => {
                                                            if (child.product_status == ps.key && ps.key != 0) {
                                                                productStatusNameSub = ps.value;
                                                            }
                                                        });
                                                        return (
                                                            <tr key={"child_" + index + "_" + subIndex}>
                                                                <td></td>
                                                                <td>
                                                                    <div className="row">
                                                                        <div className="col-md-2">{child.child_product_code}</div>
                                                                        <div className="col-md-10">{child.child_product_name}</div>
                                                                    </div>
                                                                </td>
                                                                <td></td>
                                                                <td className="text-right">
                                                                    {
                                                                        item.new_product?
                                                                        nf.format(child.component_num * productQuantity)
                                                                        :((typeof item.new_quantity !== 'undefined') ? (item.new_quantity * child.component_num) : child.received_order_num)
                                                                    }
                                                                </td>
                                                                <td></td>
                                                                <td className="text-center">
                                                                    <span className="label bg-blue">{productStatusNameSub}</span>
                                                                </td>
                                                            </tr>
                                                        )
                                                    }))
                                                ])
                                            })
                                            :<tr><td colSpan="6" className="text-center">{trans('messages.no_data')}</td></tr>
                                        }
                                        </tbody>
                                    </table>
                                    </div>
                                    {/*<div className="form-group">
                                        <div className="col-md-7 col-md-offset-5">
                                            <div className="row">
                                            <div className="col-md-4 col-xs-12">
                                                <div className="row">
                                                    <LABEL
                                                        groupClass="col-md-6 clear-padding col-xs-6 text-right"
                                                        labelClass="font_weight_normal"
                                                        labelTitle={trans('messages.pay_charge_discount')}>
                                                    </LABEL>
                                                    <div className="col-md-6 col-xs-6">
                                                        <CHECKBOX_CUSTOM
                                                            name="is_ship_charge_discount"
                                                            defaultChecked={this.state.is_ship_charge_discount === 1 ? true : false}
                                                            onChange={this.handleChange}
                                                         />
                                                    </div>
                                                </div>
                                            </div>
                                            <div className="col-md-4 clear-padding col-xs-12">
                                                <div className="row">
                                                    <div className="col-md-6 text-right clear-padding col-xs-6">
                                                        {trans('messages.total_sub')}
                                                    </div>
                                                    <div className="col-md-6 col-xs-6">
                                                        {nf.format(this.state.total)}
                                                    </div>
                                                </div>
                                                <div className="row">
                                                    <div className="col-md-6 text-right clear-padding col-xs-6">
                                                        {trans('messages.ship_charge')}
                                                    </div>
                                                    <div className="col-md-6 col-xs-6">
                                                        {this.state.ship_charge}
                                                    </div>
                                                </div>
                                                <div className="row">
                                                    <div className="col-md-6 text-right clear-padding col-xs-6">
                                                        {trans('messages.pay_after_charge')}
                                                    </div>
                                                    <div className="col-md-6 col-xs-6">
                                                        {this.state.pay_after_charge}
                                                    </div>
                                                </div>
                                                <div className="row">
                                                    <div className="col-md-6 text-right clear-padding col-xs-6">
                                                        {trans('messages.pay_charge')}
                                                    </div>
                                                    <div className="col-md-6 col-xs-6">
                                                        {this.state.pay_charge}
                                                    </div>
                                                </div>
                                                <div className="row">
                                                    <div className="col-md-6 text-right clear-padding col-xs-6">
                                                        {trans('messages.total_price')}
                                                    </div>
                                                    <div className="col-md-6 col-xs-6">
                                                        {nf.format(this.state.total_price)}
                                                    </div>
                                                </div>
                                            </div>
                                            <div className="col-md-4 clear-padding col-xs-12">
                                                <div className="row">
                                                    <div className="col-md-6 text-right clear-padding col-xs-6">
                                                        {trans('messages.pay_charge_discount')}
                                                    </div>
                                                    <div className="col-md-6 col-xs-6">
                                                        {this.state.pay_charge_discount}
                                                    </div>
                                                </div>
                                                <div className="row">
                                                    <div className="col-md-6 text-right clear-padding col-xs-6">
                                                        {trans('messages.discount')}
                                                    </div>
                                                    <div className="col-md-6 col-xs-6">
                                                        {this.state.discount}
                                                    </div>
                                                </div>
                                                <div className="row">
                                                    <div className="col-md-6 text-right clear-padding col-xs-6">
                                                        {trans('messages.used_point')}
                                                    </div>
                                                    <div className="col-md-6 col-xs-6">
                                                        {this.state.used_point}
                                                    </div>
                                                </div>
                                                <div className="row">
                                                    <div className="col-md-6 text-right clear-padding col-xs-6">
                                                        {trans('messages.used_coupon')}
                                                    </div>
                                                    <div className="col-md-6 col-xs-6">
                                                        {this.state.used_coupon}
                                                    </div>
                                                </div>
                                                <div className="row">
                                                    <div className="col-md-6 text-right clear-padding col-xs-6">
                                                        {trans('messages.request_price')}
                                                    </div>
                                                    <div className="col-md-6 col-xs-6">
                                                        {nf.format(this.state.request_price)}
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        </div>
                                    </div>*/}
                                </div>
                            </div>
                            <div className="row margin-top-bot">
                                <div className="col-md-12">
                                    <BUTTON className="btn bg-purple btn-sm margin-element width-94px pull-right" type="button" onClick={this.handleCancel}>{trans('messages.btn_cancel')}</BUTTON>
                                    <BUTTON
                                        name="btnAccept"
                                        disabled={
                                            (this.state.order_status === _.status('ORDER_STATUS.DELIVERY')
                                                && (this.state.order_sub_status === _.status('ORDER_SUB_STATUS.DOING')
                                                    || this.state.order_sub_status === _.status('ORDER_SUB_STATUS.DONE')))?true:false
                                        }
                                        className="btn btn-success btn-sm btn-larger margin-element pull-right"
                                        type="button"
                                        onClick={this.handleCheckBeforeSave}
                                    >
                                        {trans('messages.btn_accept')}
                                    </BUTTON>
                                    <BUTTON
                                        name="btnAddNew"
                                        className="btn btn-primary  margin-element btn-sm btn-larger pull-right"
                                        type="button"
                                        onClick={this.handleAddNewLine}
                                        disabled={this.state.order_status<=_.status('ORDER_STATUS.ORDER')?false:true}>
                                        {trans('messages.btn_add_product')}
                                    </BUTTON>
                                </div>
                            </div>
                            {/*<div className="row">
                                <div className={"col-md-6 col-md-offset-2" + (!this.state.show_cancel_reason?' hidden':'')}>
                                    <FORM className="form-custom">
                                    <SELECT
                                        name="cancel_reason"
                                        labelTitle={trans('messages.reason_content')}
                                        className="form-control input-sm"
                                        fieldGroupClass="col-md-12"
                                        labelClass="col-md-10"
                                        onChange={this.handleChange}
                                        value={this.state.cancel_reason}
                                        options={dataSource.cancelReasonOpt}
                                        errorPopup={true}
                                        hasError={(dataResponse.messages && dataResponse.messages.cancel_reason)?true:false}
                                        errorMessage={(dataResponse.messages&&dataResponse.messages.cancel_reason)?dataResponse.messages.cancel_reason:""}/>
                                    </FORM>
                                </div>
                            </div>*/}
                        </div>
                    </div>
                </div>
                <MESSAGE_MODAL
                    message={this.state.message || ''}
                    title={trans('messages.deny_edit')}
                    baseUrl={baseUrl}
                />
                <CancelReasonPopup
                    handleSubmitCancelReason={this.handleSubmitCancelReason}
                    optionCancelReason={dataSource.cancelReasonOpt}
                />
                <NotEnoughtCancelPopup
                    handleSubmitNotCancel={this.handleSubmitNotEnoughtCancelPopup}
                    handleCancelNotCancel={this.handleCancelNotEnoughtCancelPopup}
                    dataProduct={this.state.product_not_cancel}
                    reason={this.state.status_reason === 1 ? true : false}
                    optionCancelReason={dataSource.cancelReasonOpt}
                />
                <RepayTypePopup
                    handleSubmitRepay={this.handleSubmitRepay}
                />
                <RepayTypeCancelPopup
                    handleSubmitRepay={this.handleSubmitRepayCancel}
                />
                <RepayTypeNotEnoughtCancelPopup
                    handleSubmitRepay={this.handleSubmitRepayNotEnoughtCancel}
                />
            </div>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        dataSource: state.commonReducer.dataSource,
        dataResponse: state.commonReducer.dataResponse,
        dataRealTime: state.commonReducer.dataRealTime
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        reloadData: (url, params) => { dispatch(Action.detailData(url, params)) },
        postData: (url, params, headers) => { dispatch(Action.postData(url, params, headers)) },
        postRealTime: (url, params, headers) => { dispatch(Action.postRealTime(url, params, headers)) }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(OrderDetailEditing)