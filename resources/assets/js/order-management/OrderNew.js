import React, { Component } from 'react';
import { connect } from 'react-redux';

import * as Action from '../common/actions';
import * as queryString from 'query-string';
import axios from 'axios';

import Loading from '../common/components/common/Loading';
import FORM from '../common/components/form/FORM';
import INPUT from '../common/components/form/INPUT';
import LABEL from '../common/components/form/LABEL';
import SELECT from '../common/components/form/SELECT';
import TEXTAREA from '../common/components/form/TEXTAREA';
import BUTTON from '../common/components/form/BUTTON';
import ROW_GROUP from '../common/components/table/ROW_GROUP';
import CHECKBOX_CUSTOM from '../common/components/form/CHECKBOX_CUSTOM';
import DATEPICKER from '../common/components/form/DATEPICKER';
import RADIO_CUSTOM from '../common/components/form/RADIO_CUSTOM';

import TABLE from '../common/components/table/TABLE';
import THEAD from '../common/components/table/THEAD';
import TBODY from '../common/components/table/TBODY';
import TR from '../common/components/table/TR';
import TH from '../common/components/table/TH';
import TD from '../common/components/table/TD';

import MESSAGE_MODAL from '../common/containers/MessageModal';
import DatePicker from 'react-datepicker';
import moment from 'moment';
import 'react-datepicker/dist/react-datepicker.css';

let timer;
let timerQuantity;
let star = <span className="text-danger">＊</span> ;
class OrderNew extends Component {
    constructor(props) {
        super(props);
        this.firstReceiveProps = true;
        let newElement = {
            product_code: '',
            product_name: '',
            price: 0,
            quantity: 0,
            new_product: 1
        }
        this.state = {
            list_products: [newElement],
            is_the_same : false,
            pay_charge_discount : 0,
            discount : 0,
            used_point : 0,
            used_coupon : 0,
            disable : false,
            list_dels : []
        };
        let params = queryString.parse(props.location.search);
        if (params['receive_id']) {
            this.state = {
                list_products: [newElement],
                receive_id: params['receive_id'],
                index : params['receive_id'],
                is_the_same : false,
                pay_charge_discount : 0,
                discount : 0,
                used_point : 0,
                used_coupon : 0,
                disable : false,
                list_dels : []
            };
        }
        this.state.type_order = 1;
        this.handleChangeOrderDate   = this.handleChangeOrderDate.bind(this);
        this.handleChangeShipWishDate  = this.handleChangeShipWishDate.bind(this);
        this.props.reloadData(getFormNewOrderDataUrl, this.state);
    }
    hanleRealtime() {
        if (!_.isEmpty(this.state.index)) {
            let params = {
                accessFlg : this.state.accessFlg,
                query : {
                        page_key : 'ORDER-NEW',
                        primary_key : 'index:' + this.state.index
                    },
                key :this.state.index
            };
            if (this.state.accessFlg !== false) {
                this.props.postRealTime(checkEditUrl, params, {'X-Requested-With': 'XMLHttpRequest'});
            }
        }
    }
    handleCancel = (event) => {
        window.location.href = baseUrl;
    }
    handleAddNewLine = () => {
        let newElement = {
            product_code: '',
            product_name: '',
            price: 0,
            quantity: 0,
            product_status: '',
            new_product: 1,
            child_data: []
        }
        this.setState({
            list_products: [...this.state.list_products, newElement]
        });
    }
    handleChangeOrderDate = function(date) {
        if (!_.isNull(date)) {
            let todayDate = date.format('YYYY-MM-DD');
            this.setState({
                order_date: todayDate,
            });
        } else {
            delete this.state.order_date;
        }
    }
    handleChangeShipWishDate = function(date) {
        if (!_.isNull(date)) {
            let todayDate = date.format('YYYY-MM-DD');
            this.setState({
                ship_wish_date: todayDate,
            });
        } else {
            delete this.state.ship_wish_date;
        }
    }
    handleSubmit = (event) => {
        document.getElementsByName('btnAccept')[0].disabled = true;
        let headers = {'X-Requested-With': 'XMLHttpRequest'};
        this.props.postData(saveUrl, this.state, headers);
    }

    handleChange = (event) => {
        const target = event.target;
        const name = target.name;
        const value = target.type === 'checkbox' ? target.checked : target.value;
        this.setState({
            [name]: value
        }, () => {
            if (name === 'payment_method') {
                clearTimeout(timer);
                let _this = this;
                timer = setTimeout(function() {
                    _this.getInfoCodeFee(value);
                }, 500);
            } else if(name === 'pay_charge_discount' ||
                name === 'discount'||
                name === 'used_point' ||
                name === 'used_coupon' ||
                name === 'ship_charge'){
                this.calculate(name);
            }
        });

    }
    calculate = (name) => {
        if (
            !_.isEmpty(this.state.list_products[0].product_code)
        ) {
            let sumPrice2  = 0;
            let shipCharge = 0;
            let totalPrice = 0;
            let requestPrice = 0;
            let payAfterCharge  = this.state.pay_after_charge ? this.state.pay_after_charge : 0;
            let payCharge       = this.state.pay_charge ? this.state.pay_charge : 0;
            let chargeDiscount  = this.state.pay_charge_discount ? this.state.pay_charge_discount : 0;
            let discount        = this.state.discount ? this.state.discount : 0;
            let used_point           = this.state.used_point ? this.state.used_point : 0;
            let used_coupon          = this.state.used_coupon ? this.state.used_coupon : 0;
            this.state.list_products.map((item, index) => {
                sumPrice2 += (item.price*item.quantity);
            })
            let dataShippingFee = this.props.dataSource.dataShippingFee;

            if (dataShippingFee.below_limit_price && sumPrice2 < dataShippingFee.below_limit_price && name !== 'ship_charge') {
                shipCharge = dataShippingFee.charge_price,
                totalPrice      = sumPrice2 + dataShippingFee.charge_price + payAfterCharge + payCharge;
            } else {
                if (typeof name !== 'undefined' && name === 'ship_charge') {
                    shipCharge = parseInt(this.state.ship_charge ? this.state.ship_charge : 0);
                }
                totalPrice      = sumPrice2 + payAfterCharge + payCharge + shipCharge;
            }
            requestPrice    = totalPrice - chargeDiscount - discount - used_point - used_coupon ;
            this.setState(
                {
                    total_price : totalPrice,
                    ship_charge : shipCharge,
                    goods_price : sumPrice2,
                    request_price : requestPrice
                }
            );
        }
    }

    handleChangeProductDetail = (index, event) => {
        const target = event.target;
        const name = target.name;
        const value = target.type === 'checkbox' ? target.checked : target.value;
        let currentDetail = [...this.state.list_products];

        currentDetail[index][name] = value;
        let duplicate = 0;
        if (currentDetail.length > 1) {
            currentDetail.map((item, i) => {
                if (item.product_code === value && value !== '') {
                    duplicate++;
                }
            });
        }
        if (duplicate > 1) {
            $(target).addClass('error').attr({title:'Duplicate product code'});
        } else {
            $(target).removeClass('error').attr({title:''});
            duplicate = 0;
        }

        this.setState({
            list_products: currentDetail
        }, () => {
            if (name === 'product_code' && duplicate === 0 && value !== '') {
                clearTimeout(timer);
                let _this = this;
                timer = setTimeout(function() {
                    _this.getInfoProduct(index, value);
                }, 500);
            } else if (name === 'quantity' || name === 'price') {
                this.calculate();
            }
        });
    }

    getInfoCodeFee = (paymentMethod) => {
        if (paymentMethod === '3') {
            $(".loading").show();
            let headers = {'X-Requested-With': 'XMLHttpRequest'};
            axios.post(getDataCodeFeeUrl, this.state, {headers : headers}
            ).then(response => {
                $(".loading").hide();
                let data = response.data.data;
                if (data.cod_fee) {
                    this.setState({
                        pay_after_charge: data.cod_fee
                    });
                    this.calculate();
                }
            }).catch(error => {
                $(".loading").hide();
                throw(error);
            });
        } else {
            this.setState({pay_after_charge:0 , pay_charge:0});
            this.calculate();
        }
    }
    getInfoProduct = (index, productCode) => {
        $(".loading").show();
        let headers = {'X-Requested-With': 'XMLHttpRequest'};
        let params = {product_code : productCode };
        axios.post(getDataProductNameUrl, params, {headers : headers}
        ).then(response => {
            $(".loading").hide();
            let data = response.data.data;
            let currentDetail = [...this.state.list_products];
            let productName = '';
            let price       = '';
            if (data.length > 0) {
                data.map((item, i) => {
                    currentDetail[index]['product_code'] = item.product_code;
                    currentDetail[index]['product_name'] = item.product_name;
                    currentDetail[index]['price']        = item.price_invoice;
                });
                this.setState({
                    list_products: currentDetail
                });
            }
        }).catch(error => {
            $(".loading").hide();
            throw(error);
        });
    }
    handleDeleteNewLine = (index) => {
        if (this.props.dataResponse.messages && this.props.dataResponse.messages['list_products.'+index+'.product_code']) {
            this.props.dataResponse.messages['list_products.'+index+'.product_code'] = '';
            this.props.dataResponse.messages['list_products.'+index+'.price'] = '';
            this.props.dataResponse.messages['list_products.'+index+'.quantity'] = '';
        }
        let listDels = {};
        if (this.state.list_products[index]['receive_id'] && this.state.list_products[index]['detail_line_num']) {
           listDels = this.state.list_products[index];
           this.setState(
                {list_dels: [...this.state.list_dels, listDels]}
            );
        }

        this.setState(
            (prevState) => ({
                list_products: [
                    ...prevState.list_products.slice(0,index),
                    ...prevState.list_products.slice(index+1)
                ]
            })
        );
    }
    componentWillReceiveProps(nextProps) {
        if (!_.isEmpty(nextProps.dataSource)) {
            if (!nextProps.dataSource.data) {
                window.location.href = baseUrl;
            } else if (this.firstReceiveProps) {
                this.firstReceiveProps = false;
                this.setState(
                        {...nextProps.dataSource.data},() => {
                    this.calculate();
                });
                if (!_.isEmpty(nextProps.dataSource.data)) {
                    if (nextProps.dataSource.data.order_status !== 0 || nextProps.dataSource.data.order_sub_status !== 0) {
                        this.setState({
                           message : 'Can not edit this item.',
                           disable : true
                        })
                    }
                    this.hanleRealtime();
                }
            }
        }
        if (!_.isEmpty(nextProps.dataResponse)) {
            if (nextProps.dataResponse.status === 1) {
                window.location.href = baseUrl;
            } else {
                document.getElementsByName('btnAccept')[0].disabled = false;
            }
        }
        // Check realtime update
        if (!_.isEmpty(nextProps.dataRealTime)) {
            this.setState(nextProps.dataRealTime);
        }
    }

    componentDidUpdate() {
        if (!_.isEmpty(this.props.dataSource)) {
            $('.loading').hide();
        }
        $('[data-toggle="tooltip"]').tooltip();
        if (this.state.accessFlg === false) {
            $('#message-modal').modal('show');
            //We will auto redirect after 5 seconds
            setTimeout(() => {
                window.location.href = baseUrl;
            }, 5000);
        }
    }
    // Check realtime check for editing
    componentWillUnmount () {
        clearInterval(this.state);
    }
    // Check realtime check for editing
    componentDidMount() {
        setInterval(() => {
            this.hanleRealtime()
        }, 5000);
    }

    render() {
        let dataSource   = this.props.dataSource;
        let dataResponse = this.props.dataResponse;
        return ((!_.isEmpty(dataSource)) &&
        <div>
            <div className="container-fluid">
            <FORM className="form-horizontal">
                <Loading className="loading" />
                    <div className="box box-success">
                        <div className="box-body">
                            <div className="row">
                                <div className="col-md-2">
                                    <div className="form-group">
                                        <label className="col-md-4">{trans('messages.rat_fax_order')}</label>
                                        <div className="col-md-8">
                                            <RADIO_CUSTOM
                                                name="type_order"
                                                value="1"
                                                defaultChecked
                                                onChange={this.handleChange}
                                            />
                                        </div>
                                    </div>
                                </div>
                                <div className="col-md-2">
                                    <div className="form-group">
                                        <label className="col-md-4">{trans('messages.rat_bihin_ordet')}</label>
                                        <div className="col-md-8">
                                            <RADIO_CUSTOM
                                                name="type_order"
                                                value="2"
                                                onChange={this.handleChange}
                                            />
                                        </div>
                                    </div>
                                </div>
                                <div className="col-md-2">
                                    {(this.state.type_order == 2) &&
                                    <SELECT
                                        groupClass="form-group"
                                        name="deparment"
                                        labelTitle={trans('messages.cmb_deparment')}
                                        className="form-control input-sm"
                                        fieldGroupClass="col-md-8"
                                        labelClass="col-md-4"
                                        onChange={this.handleChange}
                                        options={dataSource.orderDep}
                                        errorPopup={true}
                                        hasError={(dataResponse.messages && dataResponse.messages.deparment)?true:false}
                                        errorMessage={(dataResponse.messages&&dataResponse.messages.deparment)?dataResponse.messages.deparment:""}/>}
                                </div>
                            </div>
                            <div className="row">

                            <div className="col-md-6">
                                <DATEPICKER
                                    placeholderText="YYYY-MM-DD"
                                    dateFormat="YYYY-MM-DD"
                                    locale="ja"
                                    minDate={moment()}
                                    name="order_date"
                                    groupClass="form-group"
                                    labelClass="col-md-3 col-xs-12"
                                    fieldGroupClass="col-md-4 col-xs-12"
                                    labelTitle={trans('messages.payment_order_date')}
                                    className="form-control input-sm"
                                    key="order_date"
                                    id="order_date"
                                    selected={this.state.order_date ? moment(this.state.order_date) : null}
                                    onChange={this.handleChangeOrderDate}
                                    errorPopup={true}
                                    hasError={(dataResponse.messages && dataResponse.messages.order_date)?true:false}
                                    errorMessage={(dataResponse.messages&&dataResponse.messages.order_date)?dataResponse.messages.order_date:""}
                                    />
                                <INPUT
                                    name="company_name"
                                    labelTitle={trans('messages.company_name')}
                                    groupClass="form-group"
                                    className="form-control input-sm"
                                    fieldGroupClass="col-md-9 col-xs-12"
                                    labelClass="col-md-3 col-xs-12"
                                    onChange={this.handleChange}
                                    defaultValue={dataSource.data.company_name}
                                    errorPopup={true}
                                    hasError={(dataResponse.messages && dataResponse.messages.company_name)?true:false}
                                    errorMessage={(dataResponse.messages&&dataResponse.messages.company_name)?dataResponse.messages.company_name:""}/>
                                <LABEL
                                    groupClass="form-group"
                                    labelClass="col-md-3 col-xs-12"
                                    fieldGroupClass="col-md-9 col-xs-12"
                                    labelTitle={trans('messages.full_name')}>
                                    <div className="col-md-6">
                                        <INPUT
                                            name="last_name"
                                            labelTitle={trans('messages.last_name')}
                                            groupClass="row"
                                            className="form-control input-sm"
                                            fieldGroupClass="col-md-8 col-xs-12"
                                            labelClass="col-md-2 col-xs-12"
                                            onChange={this.handleChange}
                                            defaultValue={dataSource.data.last_name}
                                            errorPopup={true}
                                            hasError={(dataResponse.messages && dataResponse.messages.last_name)?true:false}
                                            errorMessage={(dataResponse.messages&&dataResponse.messages.last_name)?dataResponse.messages.last_name:""}/>
                                    </div>
                                    <div className="col-md-6">
                                        <INPUT
                                            name="first_name"
                                            labelTitle={trans('messages.first_name')}
                                            groupClass="row"
                                            className="form-control input-sm"
                                            fieldGroupClass="col-md-8 col-xs-12"
                                            labelClass="col-md-2 col-xs-12"
                                            onChange={this.handleChange}
                                            defaultValue={dataSource.data.first_name}
                                            errorPopup={true}
                                            hasError={(dataResponse.messages && dataResponse.messages.first_name)?true:false}
                                            errorMessage={(dataResponse.messages&&dataResponse.messages.first_name)?dataResponse.messages.first_name:""}/>
                                    </div>
                                </LABEL>
                                <INPUT
                                    name="zip_code"
                                    labelTitle={[trans('messages.postal_code'), star]}
                                    groupClass="form-group"
                                    className="form-control input-sm"
                                    fieldGroupClass="col-md-6 col-xs-12"
                                    labelClass="col-md-3 col-xs-12"
                                    onChange={this.handleChange}
                                    defaultValue={dataSource.data.zip_code}
                                    errorPopup={true}
                                    hasError={(dataResponse.messages && dataResponse.messages.zip_code)?true:false}
                                    errorMessage={(dataResponse.messages&&dataResponse.messages.zip_code)?dataResponse.messages.zip_code:""}/>
                                <INPUT
                                    name="prefecture"
                                    labelTitle={[trans('messages.return_address'), star]}
                                    groupClass="form-group"
                                    className="form-control input-sm"
                                    fieldGroupClass="col-md-7 col-xs-12"
                                    labelClass="col-md-3 col-xs-12"
                                    onChange={this.handleChange}
                                    defaultValue={dataSource.data.prefecture}
                                    errorPopup={true}
                                    hasError={(dataResponse.messages && dataResponse.messages.prefecture)?true:false}
                                    errorMessage={(dataResponse.messages&&dataResponse.messages.prefecture)?dataResponse.messages.prefecture:""}/>
                                <INPUT
                                    name="city"
                                    labelTitle={[trans('messages.address_1'), star]}
                                    groupClass="form-group"
                                    className="form-control input-sm"
                                    fieldGroupClass="col-md-9 col-xs-12"
                                    labelClass="col-md-3 col-xs-12"
                                    onChange={this.handleChange}
                                    defaultValue={dataSource.data.city}
                                    errorPopup={true}
                                    hasError={(dataResponse.messages && dataResponse.messages.city)?true:false}
                                    errorMessage={(dataResponse.messages&&dataResponse.messages.city)?dataResponse.messages.city:""}/>
                                <INPUT
                                    name="sub_address"
                                    labelTitle={trans('messages.address_2')}
                                    groupClass="form-group"
                                    className="form-control input-sm"
                                    fieldGroupClass="col-md-9 col-xs-12"
                                    labelClass="col-md-3 col-xs-12"
                                    onChange={this.handleChange}
                                    defaultValue={dataSource.data.sub_address}
                                    errorPopup={true}
                                    hasError={(dataResponse.messages && dataResponse.messages.sub_address)?true:false}
                                    errorMessage={(dataResponse.messages&&dataResponse.messages.sub_address)?dataResponse.messages.sub_address:""}/>
                                <INPUT
                                    name="tel_num"
                                    labelTitle={[trans('messages.tel_num'), star]}
                                    groupClass="form-group"
                                    className="form-control input-sm"
                                    fieldGroupClass="col-md-4 col-xs-12"
                                    labelClass="col-md-3 col-xs-12"
                                    onChange={this.handleChange}
                                    defaultValue={dataSource.data.tel_num}
                                    errorPopup={true}
                                    hasError={(dataResponse.messages && dataResponse.messages.tel_num)?true:false}
                                    errorMessage={(dataResponse.messages&&dataResponse.messages.tel_num)?dataResponse.messages.tel_num:""}/>
                                <INPUT
                                    name="fax_num"
                                    labelTitle={trans('messages.fax_num')}
                                    groupClass="form-group"
                                    className="form-control input-sm"
                                    fieldGroupClass="col-md-4 col-xs-12"
                                    labelClass="col-md-3 col-xs-12"
                                    onChange={this.handleChange}
                                    defaultValue={dataSource.data.fax_num}
                                    errorPopup={true}
                                    hasError={(dataResponse.messages && dataResponse.messages.fax_num)?true:false}
                                    errorMessage={(dataResponse.messages&&dataResponse.messages.fax_num)?dataResponse.messages.fax_num:""}/>
                                <INPUT
                                    name="email"
                                    labelTitle={[trans('messages.email'),star]}
                                    groupClass="form-group"
                                    className="form-control input-sm"
                                    fieldGroupClass="col-md-9 col-xs-12"
                                    labelClass="col-md-3 col-xs-12"
                                    onChange={this.handleChange}
                                    defaultValue={dataSource.data.email}
                                    errorPopup={true}
                                    hasError={(dataResponse.messages && dataResponse.messages.email)?true:false}
                                    errorMessage={(dataResponse.messages&&dataResponse.messages.email)?dataResponse.messages.email:""}/>
                            </div>

                            <div className="col-md-6">
                                <LABEL
                                    groupClass="form-group"
                                    labelClass="col-md-3 col-xs-12"
                                    fieldGroupClass="col-md-4 col-xs-12"
                                    labelTitle={trans('messages.is_the_same')}>
                                    <CHECKBOX_CUSTOM
                                        name="is_the_same"
                                        defaultChecked={this.state.is_the_same === true ? true : false}
                                        onChange={this.handleChange}
                                     />
                                </LABEL>
                                {
                                (this.state.is_the_same === false)
                                ?
                                [
                                <LABEL
                                    key={"reciever_name"}
                                    groupClass="form-group"
                                    labelClass="col-md-3 col-xs-12"
                                    fieldGroupClass="col-md-9 col-xs-12"
                                    labelTitle={trans('messages.re_company_name')}>
                                    <div className="col-md-6">
                                        <INPUT
                                            key={"re_last_name"}
                                            name="re_last_name"
                                            labelTitle={trans('messages.last_name')}
                                            groupClass="row"
                                            className="form-control input-sm"
                                            fieldGroupClass="col-md-8 col-xs-12"
                                            labelClass="col-md-2 col-xs-12"
                                            onChange={this.handleChange}
                                            defaultValue={dataSource.data.re_last_name}
                                            errorPopup={true}
                                            hasError={(dataResponse.messages && dataResponse.messages.re_last_name)?true:false}
                                            errorMessage={(dataResponse.messages&&dataResponse.messages.re_last_name)?dataResponse.messages.re_last_name:""}/>
                                    </div>
                                    <div className="col-md-6">
                                        <INPUT
                                            key={"re_first_name"}
                                            name="re_first_name"
                                            labelTitle={trans('messages.first_name')}
                                            groupClass="row"
                                            className="form-control input-sm"
                                            fieldGroupClass="col-md-8 col-xs-12"
                                            labelClass="col-md-2 col-xs-12"
                                            onChange={this.handleChange}
                                            defaultValue={dataSource.data.re_first_name}
                                            errorPopup={true}
                                            hasError={(dataResponse.messages && dataResponse.messages.re_first_name)?true:false}
                                            errorMessage={(dataResponse.messages&&dataResponse.messages.re_first_name)?dataResponse.messages.re_first_name:""}/>
                                    </div>
                                </LABEL>
                                ,<INPUT
                                    key="re_postal"
                                    name="re_postal"
                                    labelTitle={[trans('messages.return_postal'), star]}
                                    groupClass="form-group"
                                    className="form-control input-sm"
                                    fieldGroupClass="col-md-6 col-xs-12"
                                    labelClass="col-md-3 col-xs-12"
                                    onChange={this.handleChange}
                                    defaultValue={dataSource.data.re_postal}
                                    errorPopup={true}
                                    hasError={(dataResponse.messages && dataResponse.messages.re_postal)?true:false}
                                    errorMessage={(dataResponse.messages&&dataResponse.messages.re_postal)?dataResponse.messages.re_postal:""}/>
                                ,<INPUT
                                    key="re_province"
                                    name="re_province"
                                    labelTitle={[trans('messages.return_address'), star]}
                                    groupClass="form-group"
                                    className="form-control input-sm"
                                    fieldGroupClass="col-md-7 col-xs-12"
                                    labelClass="col-md-3 col-xs-12"
                                    onChange={this.handleChange}
                                    defaultValue={dataSource.data.re_province}
                                    errorPopup={true}
                                    hasError={(dataResponse.messages && dataResponse.messages.re_province)?true:false}
                                    errorMessage={(dataResponse.messages&&dataResponse.messages.re_province)?dataResponse.messages.re_province:""}/>
                                ,<INPUT
                                    key="re_address_1"
                                    name="re_address_1"
                                    labelTitle={[trans('messages.address_1'), star]}
                                    groupClass="form-group"
                                    className="form-control input-sm"
                                    fieldGroupClass="col-md-9 col-xs-12"
                                    labelClass="col-md-3 col-xs-12"
                                    onChange={this.handleChange}
                                    defaultValue={dataSource.data.re_address_1}
                                    errorPopup={true}
                                    hasError={(dataResponse.messages && dataResponse.messages.re_address_1)?true:false}
                                    errorMessage={(dataResponse.messages&&dataResponse.messages.re_address_1)?dataResponse.messages.re_address_1:""}/>
                                ,<INPUT
                                    key="re_address_2"
                                    name="re_address_2"
                                    labelTitle={trans('messages.address_2')}
                                    groupClass="form-group"
                                    className="form-control input-sm"
                                    fieldGroupClass="col-md-9 col-xs-12"
                                    labelClass="col-md-3 col-xs-12"
                                    onChange={this.handleChange}
                                    defaultValue={dataSource.data.re_address_2}
                                    errorPopup={true}
                                    hasError={(dataResponse.messages && dataResponse.messages.re_address_2)?true:false}
                                    errorMessage={(dataResponse.messages&&dataResponse.messages.re_address_2)?dataResponse.messages.re_address_2:""}/>
                                ,<INPUT
                                    key="re_tel"
                                    name="re_tel"
                                    labelTitle={[trans('messages.tel_num'), star]}
                                    groupClass="form-group"
                                    className="form-control input-sm"
                                    fieldGroupClass="col-md-4 col-xs-12"
                                    labelClass="col-md-3 col-xs-12"
                                    onChange={this.handleChange}
                                    defaultValue={dataSource.data.re_tel}
                                    errorPopup={true}
                                    hasError={(dataResponse.messages && dataResponse.messages.re_tel)?true:false}
                                    errorMessage={(dataResponse.messages&&dataResponse.messages.re_tel)?dataResponse.messages.re_tel:""}/>
                                ]: ''
                                }
                            </div>
                        </div>
                        <hr/>
                        <div className="row">
                            <div className="col-md-12">
                                <div className="row">
                                    <div className="col-md-6">
                                        <SELECT
                                            groupClass="form-group"
                                            name="payment_method"
                                            labelTitle={[trans('messages.payment_name'), star]}
                                            className="form-control input-sm"
                                            fieldGroupClass="col-md-9"
                                            labelClass="col-md-3"
                                            onChange={this.handleChange}
                                            defaultValue={dataSource.data.payment_method}
                                            options={dataSource.paymentOpt}
                                            errorPopup={true}
                                            hasError={(dataResponse.messages && dataResponse.messages.payment_method)?true:false}
                                            errorMessage={(dataResponse.messages&&dataResponse.messages.payment_method)?dataResponse.messages.payment_method:""}/>
                                        <DATEPICKER
                                            placeholderText="YYYY-MM-DD"
                                            dateFormat="YYYY-MM-DD"
                                            locale="ja"
                                            minDate={moment()}
                                            name="ship_wish_date"
                                            groupClass="form-group"
                                            labelClass="col-md-3 col-xs-12"
                                            fieldGroupClass="col-md-9 col-xs-12"
                                            labelTitle={trans('messages.re_wish_date')}
                                            className="form-control input-sm"
                                            key="ship_wish_date"
                                            id="ship_wish_date"
                                            selected={this.state.ship_wish_date ? moment(this.state.ship_wish_date) : null}
                                            onChange={this.handleChangeShipWishDate}
                                            errorPopup={true}
                                            hasError={(dataResponse.messages && dataResponse.messages.ship_wish_date)?true:false}
                                            errorMessage={(dataResponse.messages&&dataResponse.messages.ship_wish_date)?dataResponse.messages.ship_wish_date:""}
                                            />
                                    </div>
                                    <div className="col-md-6">
                                        <SELECT
                                            groupClass="form-group"
                                            name="delivery_method"
                                            labelTitle={[trans('messages.delivery_method'), star]}
                                            className="form-control input-sm"
                                            fieldGroupClass="col-md-9"
                                            labelClass="col-md-3"
                                            onChange={this.handleChange}
                                            defaultValue={dataSource.data.delivery_method}
                                            options={dataSource.deliveryOpt}
                                            errorPopup={true}
                                            hasError={(dataResponse.messages && dataResponse.messages.delivery_method)?true:false}
                                            errorMessage={(dataResponse.messages&&dataResponse.messages.delivery_method)?dataResponse.messages.delivery_method:""}/>
                                        <SELECT
                                            name="ship_wish_time"
                                            labelTitle={trans('messages.re_wish_time')}
                                            className="form-control input-sm"
                                            fieldGroupClass="col-md-9"
                                            labelClass="col-md-3"
                                            groupClass="form-group"
                                            onChange={this.handleChange}
                                            value={this.state.ship_wish_time||''}
                                            options={dataSource.shipWishDateOpt}
                                            errorPopup={true}
                                            hasError={(dataResponse.messages && dataResponse.messages.ship_wish_time)?true:false}
                                            errorMessage={(dataResponse.messages&&dataResponse.messages.ship_wish_time)?dataResponse.messages.ship_wish_time:""} />
                                    </div>
                                </div>
                            </div>
                        </div>
                        <hr/>
                        <div className="row">
                            <div className="col-md-12">
                            <div className="table-responsive">
                                <TABLE className="table table-striped table-custom">
                                <THEAD>
                                    <TR>
                                        <TH className="text-left">
                                            {trans('messages.new_product_code')}
                                            {star}
                                        </TH>
                                        <TH className="text-left">
                                            {trans('messages.product_name')}
                                        </TH>
                                        <TH className="text-left">
                                            {trans('messages.quantity')}
                                            {star}
                                        </TH>
                                        <TH className="text-left">
                                            {trans('messages.price')}
                                            {star}
                                        </TH>
                                        <TH className="text-left">
                                            {trans('messages.sum_price')}
                                        </TH>
                                        <TH className="text-center">

                                        </TH>

                                    </TR>
                                </THEAD>
                                <TBODY>
                                    {
                                        (!_.isEmpty(this.state.list_products))?
                                        this.state.list_products.map((item, index) => {
                                            let productCode       = item.product_code;
                                            let productName       = item.product_name;
                                            let productPrice      = item.price || 0;
                                            let productQuantity   = item.quantity;
                                            let productSumPrice   = (item.quantity*item.price);
                                            var productCodeErr    = (dataResponse.messages &&
                                                                dataResponse.messages['list_products.'+index+'.product_code'] &&
                                                                dataResponse.messages['list_products.'+index+'.product_code'] )?
                                                                dataResponse.messages['list_products.'+index+'.product_code']
                                                                .toString().replace('list_products.'+index+'.', '').replace('_', ' '):"";
                                            var priceErr    = (dataResponse.messages &&
                                                                dataResponse.messages['list_products.'+index+'.price'] &&
                                                                dataResponse.messages['list_products.'+index+'.price'] )?
                                                                dataResponse.messages['list_products.'+index+'.price']
                                                                .toString().replace('list_products.'+index+'.', '').replace('_', ' '):"";
                                            var quantityErr    = (dataResponse.messages &&
                                                                dataResponse.messages['list_products.'+index+'.quantity'] &&
                                                                dataResponse.messages['list_products.'+index+'.quantity'] )?
                                                                dataResponse.messages['list_products.'+index+'.quantity']
                                                                .toString().replace('list_products.'+index+'.', '').replace('_', ' '):"";
                                            var productCodeHasErr = (dataResponse.messages &&
                                                                dataResponse.messages['list_products.'+index+'.product_code'] &&
                                                                dataResponse.messages['list_products.'+index+'.product_code'] )?
                                                                true:false;
                                            var priceHasErr = (dataResponse.messages &&
                                                                dataResponse.messages['list_products.'+index+'.price'] &&
                                                                dataResponse.messages['list_products.'+index+'.price'] )?
                                                                true:false;
                                            var quantityHasErr = (dataResponse.messages &&
                                                                dataResponse.messages['list_products.'+index+'.quantity'] &&
                                                                dataResponse.messages['list_products.'+index+'.quantity'] )?
                                                                true:false;
                                            var orderDate = moment.utc(item.order_date, "YYYY-MM-DD");
                                            var formatOrderDate = orderDate.format("MM/DD");
                                            var nf = new Intl.NumberFormat();
                                            let classRow = (index%2 ===0) ? 'odd' : 'even';
                                            return (
                                                [
                                                <TR key={"tr" + index} className={classRow + ' fade-in'}>
                                                    <TD className="col-max-min-200">
                                                       <INPUT
                                                            name="product_code"
                                                            groupClass="form-group"
                                                            className="form-control input-sm"
                                                            fieldGroupClass="col-md-12"
                                                            value={productCode}
                                                            onChange={(e) => this.handleChangeProductDetail(index, e)}
                                                            errorPopup={true}
                                                            hasError={productCodeHasErr}
                                                            errorMessage={productCodeErr}
                                                        />
                                                    </TD>
                                                    <TD className="col-max-min-300">
                                                       <INPUT
                                                            name="product_name"
                                                            groupClass="form-group"
                                                            className="form-control input-sm"
                                                            fieldGroupClass="col-md-12"
                                                            readOnly
                                                            value={productName}

                                                        />
                                                    </TD>
                                                    <TD className="col-max-min-60">
                                                       <INPUT
                                                            name="quantity"
                                                            groupClass="form-group"
                                                            className="form-control input-sm"
                                                            fieldGroupClass="col-md-12"
                                                            value={productQuantity}
                                                            onChange={(e) => this.handleChangeProductDetail(index, e)}
                                                            errorPopup={true}
                                                            hasError={quantityHasErr}
                                                            errorMessage={quantityErr}
                                                        />
                                                    </TD>
                                                    <TD className="col-max-min-60">
                                                       <INPUT
                                                            name="price"
                                                            groupClass="form-group"
                                                            className="form-control input-sm"
                                                            fieldGroupClass="col-md-12"
                                                            value={productPrice}
                                                            onChange={(e) => this.handleChangeProductDetail(index, e)}
                                                            errorPopup={true}
                                                            hasError={priceHasErr}
                                                            errorMessage={priceErr}
                                                        />
                                                    </TD>
                                                    <TD className="col-max-min-60">
                                                       <INPUT
                                                            name="sum_price"
                                                            groupClass="form-group"
                                                            className="form-control input-sm"
                                                            fieldGroupClass="col-md-12"
                                                            readOnly
                                                            value={productSumPrice|0}
                                                        />
                                                    </TD>
                                                    <TD className="text-center">
                                                        <BUTTON
                                                            name="btnDeleteNewLine"
                                                            className="btn btn-danger btn-sm"
                                                            type="button"
                                                            onClick={(e) => this.handleDeleteNewLine(index, e)}>
                                                            <i className="fa fa-trash"></i>
                                                        </BUTTON>
                                                    </TD>
                                                </TR>
                                               ]

                                            )
                                            if (index === dataSource.data.to) {
                                               index = 0;
                                            }
                                        })
                                        :
                                        <TR><TD colSpan="9" className="text-center">{trans('messages.no_data')}</TD></TR>
                                    }
                                    <tr>
                                        <td colSpan="6">
                                            <BUTTON
                                                name="btnAddNew"
                                                className="btn btn-primary btn-sm btn-larger "
                                                type="button"
                                                onClick={this.handleAddNewLine}
                                                >
                                                {trans('messages.btn_add_product')}
                                            </BUTTON>
                                        </td>
                                    </tr>
                                </TBODY>
                            </TABLE>
                            </div>
                        </div>
                        </div>
                        <hr/>
                        <div className="row">
                            <div  className="col-md-12">
                                <div className="row">
                                    <div className="col-md-4">
                                        <LABEL
                                            groupClass="form-group"
                                            labelClass="col-md-3 col-xs-12"
                                            fieldGroupClass="col-md-9 col-xs-12"
                                            labelTitle={trans('messages.check')}>
                                            <CHECKBOX_CUSTOM
                                                name="is_delay"
                                                defaultChecked={this.state.is_delay === 1 ? true : false}
                                                onChange={this.handleChange}
                                             />
                                        </LABEL>
                                    </div>
                                    <div className="col-md-4">
                                        <INPUT
                                            name="goods_price"
                                            readOnly
                                            type="number"
                                            labelTitle={trans('messages.total_sub')}
                                            groupClass="form-group"
                                            className="form-control input-sm"
                                            fieldGroupClass="col-md-6 col-xs-12"
                                            labelClass="col-md-6 col-xs-12"
                                            onChange={this.handleChange}
                                            value={this.state.goods_price|0}
                                            errorPopup={true}
                                            hasError={(dataResponse.messages && dataResponse.messages.goods_price)?true:false}
                                            errorMessage={(dataResponse.messages&&dataResponse.messages.goods_price)?dataResponse.messages.goods_price:""}/>
                                    </div>
                                    <div className="col-md-4">
                                        <INPUT
                                            name="pay_charge_discount"
                                            type="number"
                                            labelTitle={trans('messages.pay_charge_discount')}
                                            groupClass="form-group"
                                            className="form-control input-sm"
                                            fieldGroupClass="col-md-6 col-xs-12"
                                            labelClass="col-md-6 col-xs-12"
                                            onChange={this.handleChange}
                                            defaultValue={this.state.pay_charge_discount|0}
                                            errorPopup={true}
                                            hasError={(dataResponse.messages && dataResponse.messages.pay_charge_discount)?true:false}
                                            errorMessage={(dataResponse.messages&&dataResponse.messages.pay_charge_discount)?dataResponse.messages.pay_charge_discount:""}/>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div className="row">
                            <div  className="col-md-12">
                                <div className="row">
                                    <div className="col-md-4">
                                    </div>
                                    <div className="col-md-4">
                                        <INPUT
                                            name="ship_charge"
                                            type="number"
                                            labelTitle={trans('messages.ship_charge')}
                                            groupClass="form-group"
                                            className="form-control input-sm"
                                            fieldGroupClass="col-md-6 col-xs-12"
                                            labelClass="col-md-6 col-xs-12"
                                            onChange={this.handleChange}
                                            value={this.state.ship_charge|0}/>
                                    </div>
                                    <div className="col-md-4">
                                        <INPUT
                                            name="discount"
                                            type="number"
                                            labelTitle={trans('messages.discount')}
                                            groupClass="form-group"
                                            className="form-control input-sm"
                                            fieldGroupClass="col-md-6 col-xs-12"
                                            labelClass="col-md-6 col-xs-12"
                                            onChange={this.handleChange}
                                            defaultValue={this.state.discount|0}
                                            errorPopup={true}
                                            hasError={(dataResponse.messages && dataResponse.messages.discount)?true:false}
                                            errorMessage={(dataResponse.messages&&dataResponse.messages.discount)?dataResponse.messages.discount:""}/>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div className="row">
                            <div  className="col-md-12">
                                <div className="row">
                                    <div className="col-md-4">
                                    </div>
                                    <div className="col-md-4">
                                        <INPUT
                                            name="pay_after_charge"
                                            labelTitle={trans('messages.pay_after_charge')}
                                            type="number"
                                            groupClass="form-group"
                                            className="form-control input-sm"
                                            fieldGroupClass="col-md-6 col-xs-12"
                                            labelClass="col-md-6 col-xs-12"
                                            readOnly
                                            value={this.state.pay_after_charge|0}/>
                                    </div>
                                    <div className="col-md-4">
                                        <INPUT
                                            name="used_point"
                                            labelTitle={trans('messages.used_point')}
                                            type="number"
                                            groupClass="form-group"
                                            className="form-control input-sm"
                                            fieldGroupClass="col-md-6 col-xs-12"
                                            labelClass="col-md-6 col-xs-12"
                                            onChange={this.handleChange}
                                            defaultValue={this.state.used_point|0}
                                            errorPopup={true}
                                            hasError={(dataResponse.messages && dataResponse.messages.used_point)?true:false}
                                            errorMessage={(dataResponse.messages&&dataResponse.messages.used_point)?dataResponse.messages.used_point:""}/>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div className="row">
                            <div  className="col-md-12">
                                <div className="row">
                                    <div className="col-md-4">
                                    </div>
                                    <div className="col-md-4">
                                        <INPUT
                                            name="pay_charge"
                                            type="number"
                                            labelTitle={trans('messages.pay_charge')}
                                            groupClass="form-group"
                                            className="form-control input-sm"
                                            fieldGroupClass="col-md-6 col-xs-12"
                                            labelClass="col-md-6 col-xs-12"
                                            readOnly
                                            value={this.state.pay_charge|0}/>
                                    </div>
                                    <div className="col-md-4">
                                        <INPUT
                                            name="used_coupon"
                                            type="number"
                                            labelTitle={trans('messages.used_coupon')}
                                            groupClass="form-group"
                                            className="form-control input-sm"
                                            fieldGroupClass="col-md-6 col-xs-12"
                                            labelClass="col-md-6 col-xs-12"
                                            onChange={this.handleChange}
                                            defaultValue={this.state.used_coupon|0}
                                            errorPopup={true}
                                            hasError={(dataResponse.messages && dataResponse.messages.used_coupon)?true:false}
                                            errorMessage={(dataResponse.messages&&dataResponse.messages.used_coupon)?dataResponse.messages.used_coupon:""}/>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div className="row">
                            <div  className="col-md-12">
                                <div className="row">
                                    <div className="col-md-4">
                                    </div>
                                    <div className="col-md-4">
                                        <INPUT
                                            name="total_price"
                                            type="number"
                                            labelTitle={trans('messages.new_total_price')}
                                            groupClass="form-group"
                                            className="form-control input-sm"
                                            fieldGroupClass="col-md-6 col-xs-12"
                                            labelClass="col-md-6 col-xs-12"
                                            readOnly
                                            value={this.state.total_price|0}/>
                                    </div>
                                    <div className="col-md-4">
                                        <INPUT
                                            name="request_price"
                                            type="number"
                                            labelTitle={trans('messages.request_price')}
                                            groupClass="form-group"
                                            className="form-control input-sm"
                                            fieldGroupClass="col-md-6 col-xs-12"
                                            labelClass="col-md-6 col-xs-12"
                                            readOnly
                                            value={this.state.request_price|0}/>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div className="row">
                            <div  className="col-md-12">
                                <div className="row">
                                    <div className="col-md-12">
                                        <TEXTAREA
                                            name="customer_question"
                                            labelTitle={trans('messages.note')}
                                            groupClass="form-group"
                                            className="form-control input-sm"
                                            fieldGroupClass="col-md-11 col-xs-12"
                                            labelClass="col-md-1 col-xs-12"
                                            onChange={this.handleChange}
                                            defaultValue={dataSource.data.customer_question}
                                            errorPopup={true}
                                            hasError={(dataResponse.messages && dataResponse.messages.customer_question)?true:false}
                                            errorMessage={(dataResponse.messages&&dataResponse.messages.customer_question)?dataResponse.messages.customer_question:""}
                                            rows="6"/>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <hr/>
                        <div className="row">
                            <div className="col-md-9"></div>
                            <div className="col-md-3 text-right">
                            {
                                (this.state.disable === false) ?
                                [<BUTTON key="btn-1" className="btn bg-purple pull-right  pull-right" type="button" onClick={this.handleCancel}>{trans('messages.btn_cancel')}</BUTTON>,
                                <BUTTON key="btn-2" name="btnAccept" className="btn btn-success margin-element" type="button" onClick={this.handleSubmit}>{trans('messages.btn_accept_memo')}</BUTTON>]
                                :''
                            }
                            </div>
                        </div>

                    </div>
                </div>
            </FORM>
            <MESSAGE_MODAL
                message={this.state.message || ''}
                title={trans('messages.deny_edit')}
                baseUrl={baseUrl}
            />
            </div>
            </div>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        dataSource: state.commonReducer.dataSource,
        dataResponse: state.commonReducer.dataResponse,
        dataRealTime: state.commonReducer.dataRealTime
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        reloadData: (url, params) => { dispatch(Action.detailData(url, params)) },
        postData: (url, params, headers) => { dispatch(Action.postData(url, params, headers)) },
        postRealTime: (url, params, headers) => { dispatch(Action.postRealTime(url, params, headers)) }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(OrderNew)