import React, { Component } from 'react';
import Loading from '../common/components/common/Loading';
export default class AcceptPopup extends Component {
    constructor(props) {
        super(props);
        this.state = {}
    }
    handleSubmit = () => {
        $(".loading").show();
        if (this.props.name === 'is_corrected') {
            this.props.handleEditMapAll();
        } else if (this.props.name === 'occorred_reason_all') {
            this.props.handleOccorredReason();
        } else if (this.props.name === 'process_all') {
            this.props.handleProcessAll();
        }
        $('#access-memo-modal').modal('hide');
    }
    handleCancelClick = () => {
        this.props.handleCancel()
    }
    render() {
        return (
            <div className="modal fade" id="access-memo-modal">
                <Loading className="loading" />
                <div className="modal-dialog ">
                    <div className="modal-content">
                        <div className="modal-header">
                        <b>{trans('messages.accept_title')}</b>

                        </div>
                        <div className="modal-body modal-custom-body">
                            <div className="row">
                                <div className="col-md-12 message-popup">
                                    <p>{trans('messages.memo_popup_ask')}</p>
                                </div>
                            </div>
                        </div>
                        <div className="modal-footer modal-custom-footer">
                            <button type="button" className="btn btn-primary btn-sm modal-custom-button" onClick={this.handleSubmit}>{trans('messages.btn_accept_ok')}</button>
                            <button type="button" className="btn bg-purple btn-sm modal-custom-button" onClick={this.handleCancelClick}>{trans('messages.btn_accept_cancel')}</button>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}