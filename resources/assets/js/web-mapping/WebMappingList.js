import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import * as Action from '../common/actions';
import * as helpers from '../common/helpers';
import * as queryString from 'query-string';
import createHistory from 'history/createBrowserHistory';
import BUTTON from '../common/components/form/BUTTON';
import FORM from '../common/components/form/FORM';
import INPUT from '../common/components/form/INPUT';
import SELECT from '../common/components/form/SELECT';
import LABEL from '../common/components/form/LABEL';
import CHECKBOX_CUSTOM from '../common/components/form/CHECKBOX_CUSTOM';
import TABLE from '../common/components/table/TABLE';
import THEAD from '../common/components/table/THEAD';
import TBODY from '../common/components/table/TBODY';
import TR from '../common/components/table/TR';
import TH from '../common/components/table/TH';
import TD from '../common/components/table/TD';
import SortComponent from '../common/components/table/SortComponent';
import PaginationContainer from '../common/containers/PaginationContainerNew'
import Loading from '../common/components/common/Loading';
import MessagePopup from '../common/components/common/MessagePopup';
import AcceptPopup from './AcceptPopup';
import ImportCsv from './csv/ImportCsv';
import DatePicker from 'react-datepicker';
import moment from 'moment';
import 'react-datepicker/dist/react-datepicker.css';
import axios from 'axios';
class WebMappingList extends Component {
    constructor(props) {
        super(props);
        const stringParams = helpers.parseStringToParams(props.location.search);
        this.state = {}
        if (!_.isEmpty(stringParams)) {
            this.state = stringParams;
        }
        this.state.attached_file_path = '';
        this.props.reloadData(dataListUrl, stringParams);
    }

    handleChange = (event) => {
        const target = event.target;
        let value = target.value;
        let name = target.name;
        this.setState({
            [name]: (value)?value:''
        },() => {
            if (name === 'per_page') {
                this.handleReloadData();
            }
        });
    }

    handleChangeMultiSelect = (name, value, checked) => {
        let currentValue = this.state[name];
        if (typeof currentValue === 'undefined') {
            currentValue = [];
        } else if (!Array.isArray(this.state[name])) {
            currentValue = [this.state[name]];
        }
        if (Array.isArray(value)) {
            currentValue = [...value];
        } else {
            let index = currentValue.indexOf(value);
            if (checked === false) {
                if (index !== -1) {
                    currentValue.splice(index, 1);
                }
            } else {
                if (index === -1) {
                    currentValue.push(value);
                }
            }
        }
        this.setState({
            [name]: currentValue,
        });
    }

    handleChangeDate = function(date, name, event) {
        if (!_.isNull(date)) {
            let todayDate = date.format('YYYY-MM-DD');
            this.setState({
                [name]: todayDate,
            });
        } else {
            delete this.state[name];
        }
    }

    handleClearClick = (event) => {
        Object.keys(this.state).map((key) => {
            if (key !== 'per_page') {
                delete this.state[key];
            }
        });
        this.handleReloadData();
    }

    handleClickPage = (page) => {
        this.setState({
            page: page
        },() => {
            this.props.reloadData(dataListUrl, this.state, true);
        });
    }

    handleSortClick = (name, nextSort) => {
        Object.keys(this.state).map((key) => {
            if (key.startsWith('sort_')) {
                delete this.state[key];
            }
        });
        if (nextSort === 'none') {
            delete this.state[name];
            this.props.reloadData(dataListUrl, this.state, true);
        } else {
            this.setState({[name]: nextSort}, () => {
                this.props.reloadData(dataListUrl, this.state, true);
            });
        }
    }
    handleReloadData = (a) => {
        let params = {};
        let dataSource = this.props.dataSource;
        let tmpParams = this.state;
        Object.keys(tmpParams).map(key => {
            if (tmpParams[key] !== ""
                && tmpParams[key] !== null
                && key !== 'flg'
                && key !== 'attached_file_path'
                && key !== 'attached_file'
                && key !== 'acccept_name'
                && key !== 'process_all'
                && key !== 'occorred_reason_all'
                && key !== 'is_corrected_all'
                && key !== 'page') {
                    params[key] = tmpParams[key];
            } else {
                delete params[key];
            }
        });
        this.props.reloadData(dataListUrl, params, true);
    }
    componentDidUpdate() {
        if (!_.isEmpty(this.props.dataSource)) {
            $('.loading').hide();
        }
        delete this.state['finish'];
        delete this.state['message_success'];
        dragscroll.reset();
        $("table th.resize-thead").resizeble();
    }
    componentWillMount() {
        window.onpopstate = (event) => {
            location.reload();
        };
    }

    handleChooseFile = (event) => {
        const target = event.target;
        if (target.files !== null && target.files.length !== 0) {
            const name = target.name;
            let tmpName = name.replace(new RegExp("^browser_"), '');
            this.setState({
                [tmpName]: target.files[0].name,
                ['attached_file']: target.files[0]
            }, ()=> {
                document.getElementsByName(tmpName)[0].value = target.files[0].name;
            });
        }
    }

    handleImportSubmit = () => {
        let formData   = new FormData();
        let headers = {'X-Requested-With': 'XMLHttpRequest'};
        if (!_.isEmpty(this.state)) {
            Object.keys(this.state).map((item) => {
                formData.append(item, this.state[item]);
            });
        }
        this.flg_check = true;
        this.setState({
            process: true,
            status_csv: 'show',
            percent: 0,
        }, () => {
            formData.append('name_csv', $('#action-modal').val());
            this.props.postDataCSV(csvUrl, formData, headers);
        });
    }

    handleImportCancel = () => {
        $('#issue-modal').modal('hide');
        this.flg_check = false;
        this.setState({
            attached_file_path: '',
            attached_file: '',
            status_csv: false,
            message: '',
            finish: 0,
            error: 0,
            msg: '',
            modalTitle: '',
        }, () => {
            $("input[name='browser_attached_file_path']").val('');
            this.handleReloadData();
        });
    }

    handleImport = (event, name) => {
        $('#action-modal').val(name);
        if (name === 'import_diy_csv') {
            this.setState({modalTitle : trans('messages.web_mapping_diy_modal_title')});
        } else {
            this.setState({modalTitle : trans('messages.web_mapping_biz_modal_title')});
        }
        $('#issue-modal').modal({backdrop: 'static', show: true});
    }
    handleExportCsv = () => {
        $(".loading").show();
        let param = {...this.state};
        param['type'] = 'isCsv';
        let headers = {'X-Requested-With': 'XMLHttpRequest'};
        axios.post(processExportCSV, param, {headers : headers}
        ).then(response => {
            $(".loading").hide();
            if (response.data.status === 1) {
                window.location.href = exportCSV +"?fileName="+ response.data.file_name;
            }
        }).catch(error => {
            $(".loading").hide();
            throw(error);
        });
    }
    componentWillReceiveProps(nextProps) {
        let dataResponse = nextProps.dataResponseCsv;
        if (!_.isEmpty(dataResponse) && this.flg_check) {
            if (typeof dataResponse.reload !== 'undefined' && dataResponse.reload) {
                this.props.postDataCSV(importUrl, dataResponse.params, dataResponse.headers, dataResponse.count_err);
            }
            let formData   = new FormData();
            let headers = {'X-Requested-With': 'XMLHttpRequest'};
            Object.keys(dataResponse).map((item) => {
                formData.append(item, dataResponse[item]);
            });
            formData.append('type', $('#action-modal').val());
            let showStatus = true;
            if(dataResponse.flg === 1) {
                this.setState({
                    process: true,
                    status_csv: 'show',
                    percent: (dataResponse.currPage/dataResponse.timeRun) * 100,
                }, () => {
                    this.props.postDataCSV(importUrl, formData, headers);
                });
            } else if (dataResponse.flg === 0) {
                let dataSet = {
                    process: false,
                    status_csv: false,
                };
                if (dataResponse.process === 'uploadCsv') {
                    if (dataResponse.msg === 'Finish') {
                        dataSet = {
                            process: true,
                            status_csv: 'show',
                            finish: 1,
                        };
                        $('#popup-message').text(trans('messages.message_import_success'));
                        this.setState({...dataResponse,attached_file_path:'', ...dataSet}, () => {
                            this.props.postDataCSV(processAfterImportUrl, formData, headers);
                            $("input[name='browser_attached_file_path']").val('');
                            $("input[name='browser_attached_file_path']").val('');
                        });
                    } else {
                        showStatus = false;
                        this.setState({...dataResponse, ...dataSet});
                    }
                } else {
                    if (dataResponse.message_success === '') {
                        $('#popup-message').text(trans('messages.message_no_data_mapping'));
                    } else {
                        $('#popup-message').text(dataResponse.message_success);
                    }
                    dataSet['msg'] = '';
                    dataSet['finish'] = 0;
                    this.setState({...dataResponse, ...dataSet}, () => {
                        if (dataResponse.error === 0) {
                            $('#issue-modal').modal('hide');
                            this.handleReloadData();
                        }
                    });
                }
                if (showStatus) {
                    $('#popup-status').addClass('show');
                    setInterval(function(){
                        $('#popup-status').removeClass('show');
                    }, 5000);
                }
            }
            if (typeof dataResponse.message_success !== 'undefined') {
                this.flg_check = false;
            }
        }
    }

    handleButtonList = (event, id, name, value = null) => {
        if (name === 'occorred_reason' || name === 'process_content') {
            const target = event.target;
            value = target.value;
        }
        let headers = {'X-Requested-With': 'XMLHttpRequest'};
        this.props.postData(
            updateStatusUrl,
            {
                id : id,
                name: name,
                value: value,
            },
            headers,
            () => this.props.reloadData(dataListUrl, this.state)
        );
    }

    handleProcessAPI = (event, name) => {
        let headers = {'X-Requested-With': 'XMLHttpRequest'};
        $(".loading").show();
            axios.post(processApiUrl, {name: name}, headers
            ).then(response => {
                $(".loading").hide();
                if (response.data.flg === 1) {
                    if (response.data.message_success === '') {
                        $.notify({
                            icon: 'glyphicon glyphicon-warning-sign',
                            message: trans('messages.message_no_data_process'),
                        },{
                            type: 'danger',
                            delay: 2000
                        });
                    } else {
                        this.setState({...response.data}, () => {
                             $.notify({
                                icon: 'glyphicon glyphicon-warning-sign',
                                message: response.data.message_success,
                            },{
                                type: 'success',
                                delay: 2000
                            });
                        });
                    }
                } else if (response.data.flg === -1) {
                     $.notify({
                        icon: 'glyphicon glyphicon-warning-sign',
                        message: response.data.message,
                    },{
                        type: 'danger',
                        delay: 2000
                    });
                } else if (response.data.flg === 0) {
                     $.notify({
                        icon: 'glyphicon glyphicon-warning-sign',
                        message: response.data.message,
                    },{
                        type: 'danger',
                        delay: 2000
                    });
                }
            }).catch(error => {
                $(".loading").hide();
                throw(error);
            });
    }
    handleEditMapAll = () => {
        let params = {...this.state};
        delete this.state['is_corrected_all'];
        delete this.state['acccept_name'];
        let headers = {'X-Requested-With': 'XMLHttpRequest'};
        axios.post(processUpdateAll, params, {headers : headers}
        ).then(response => {
            $(".loading").hide();
            if (response.data.status === 1) {
                this.handleReloadData()
            }
        }).catch(error => {
            $(".loading").hide();
            throw(error);
        });
    }
    handleOccorredReason = () => {
        let params = {...this.state};
        delete this.state['occorred_reason_all'];
        delete this.state['acccept_name'];
        let headers = {'X-Requested-With': 'XMLHttpRequest'};
        axios.post(processUpdateAll, params, {headers : headers}
        ).then(response => {
            $(".loading").hide();
            if (response.data.status === 1) {
                this.handleReloadData()
            }
        }).catch(error => {
            $(".loading").hide();
            throw(error);
        });
    }

    handleProcessAll = () => {
        let params = {...this.state};
        delete this.state['process_all'];
        delete this.state['acccept_name'];
        let headers = {'X-Requested-With': 'XMLHttpRequest'};
        axios.post(processUpdateAll, params, {headers : headers}
        ).then(response => {
            $(".loading").hide();
            if (response.data.status === 1) {
                this.handleReloadData()
            }
        }).catch(error => {
            $(".loading").hide();
            throw(error);
        });
    }

    handleAccept = (event) => {
        const target = event.target;
        let value = target.value;
        let name = target.name;
        let flgPopup = true;
        if (name === 'is_corrected') {
            this.setState({ acccept_name :  name, 'is_corrected_all' : true});
        } else if (name === 'occorred_reason_all') {
            if (value == 0) {
                let flgPopup = false;
            }
            this.setState({ acccept_name :  name ,  occorred_reason_all : value});
        } else if (name === 'process_all') {
            if (value == 0) {
                let flgPopup = false;
            }
            this.setState({ acccept_name :  name ,  process_all : value});
        }
        $('#access-memo-modal').modal({backdrop: 'static', show: true});

    }
    handleCancelAccept = () => {
        delete this.state.acccept_name;
        delete this.state.process_all;
        delete this.state.occorred_reason_all;
        delete this.state.is_corrected_all;
        $('#access-memo-modal').modal('hide');
    }
    render() {
        let nf = new Intl.NumberFormat();
        let dataSource = this.props.dataSource;
        let index = 0;
        let mallOptions = {};
        let diffirentOptions = {};
        let processOptions = {};
        let processOptionsAll = {};
        let occurredOptions = {};
        let occurredOptionsAll = {};
        let settlementOptions = {};
        let orderStatusOptions = {};
        let isCorrectedOptions = {};
        if (!_.isEmpty(dataSource)) {
            index            = dataSource.data.from - 1;
            mallOptions      = dataSource.mallData;
            diffirentOptions = dataSource.differentData;
            processOptions   = dataSource.processData;
            processOptionsAll   = {'0' : '--------------------', ...dataSource.processData};
            occurredOptions  = dataSource.occurredData;
            occurredOptionsAll  = {'0' : '--------------------', ...dataSource.occurredData};
            settlementOptions = dataSource.smData;
            orderStatusOptions = dataSource.osData;
            isCorrectedOptions = dataSource.isCorrectedData;
        }
        return ( (!_.isEmpty(dataSource)) &&
            <div>
                <Loading className="loading" />
                <div className="box">
                    <div className="box-header">
                        <div className="row">
                            <div className="col-md-12">
                                <h4>{trans('messages.search_title')}</h4>
                            </div>
                            <div className="col-md-12">
                                <FORM className="row">
                                <div className="col-md-12">
                                    <div className="row">
                                        <SELECT
                                            id="name_jp"
                                            name="name_jp"
                                            groupClass="form-group col-md-1-25"
                                            labelClass=""
                                            labelTitle={trans('messages.mall_name')}
                                            handleChangeMultiSelect={this.handleChangeMultiSelect}
                                            valueMulti={this.state.name_jp||''}
                                            options={mallOptions}
                                            multiple="multiple"
                                            className="form-control input-sm" />
                                        <INPUT
                                            name="received_order_id"
                                            groupClass="form-group col-md-1-5"
                                            labelTitle={trans('messages.received_order_id')}
                                            onChange={this.handleChange}
                                            value={this.state.received_order_id || ''}
                                            className="form-control input-sm"
                                            errorPopup={true}
                                            hasError={dataSource.message&&dataSource.message.received_order_id?true:false}
                                            errorMessage={dataSource.message&&dataSource.message.received_order_id?dataSource.message.received_order_id:""}/>
                                        <LABEL
                                            groupClass="form-group col-md-1-25 clear-padding-right"
                                            labelTitle={trans('messages.order_date_from')}>
                                            <DatePicker
                                                placeholderText="YYYY-MM-DD"
                                                dateFormat="YYYY-MM-DD"
                                                locale="ja"
                                                name="order_date_from"
                                                className="form-control input-sm"
                                                key="order_date_from"
                                                id="order_date_from"
                                                openToDate ={this.state.order_date_from ? moment(this.state.order_date_from) : moment().subtract(7, 'day')}
                                                selected={this.state.order_date_from ? moment(this.state.order_date_from) : null}
                                                onChange={(date,name, e) => this.handleChangeDate(date, 'order_date_from', e)} />
                                        </LABEL>
                                        <div className="col-md-0-25 hidden-sm"><span className="to-from-child">～</span></div>
                                        <LABEL
                                            groupClass="form-group col-md-1-25 clear-padding-left"
                                            labelTitle={trans('messages.order_date_to')}>
                                            <DatePicker
                                                placeholderText="YYYY-MM-DD"
                                                dateFormat="YYYY-MM-DD"
                                                locale="ja"
                                                name="order_date_to"
                                                className="form-control col-md-12 input-sm"
                                                key="order_date_to"
                                                id="order_date_to"
                                                selected={this.state.order_date_to ? moment(this.state.order_date_to) : null}
                                                onChange={(date, name, e) => this.handleChangeDate(date, 'order_date_to', e)} />
                                        </LABEL>
                                        <LABEL
                                            groupClass="form-group col-md-1-25 clear-padding-right"
                                            labelTitle={trans('messages.sold_date_from')}>
                                            <DatePicker
                                                placeholderText="YYYY-MM-DD"
                                                dateFormat="YYYY-MM-DD"
                                                locale="ja"
                                                name="sold_date_from"
                                                className="form-control input-sm"
                                                key="sold_date_from"
                                                id="sold_date_from"
                                                openToDate ={this.state.order_date_from ? moment(this.state.order_date_from) : moment().subtract(7, 'day')}
                                                selected={this.state.sold_date_from ? moment(this.state.sold_date_from) : null}
                                                onChange={(date, name, e) => this.handleChangeDate(date, 'sold_date_from', e)} />
                                        </LABEL>
                                        <div className="col-md-0-25 hidden-sm"><span className="to-from-child">～</span></div>
                                        <LABEL
                                            groupClass="form-group col-md-1-25 clear-padding-left"
                                            labelTitle={trans('messages.sold_date_to')}>
                                            <DatePicker
                                                placeholderText="YYYY-MM-DD"
                                                dateFormat="YYYY-MM-DD"
                                                locale="ja"
                                                name="sold_date_to"
                                                className="form-control col-md-12 input-sm"
                                                key="sold_date_to"
                                                id="sold_date_to"
                                                selected={this.state.sold_date_to ? moment(this.state.sold_date_to) : null}
                                                onChange={(date, name, e) => this.handleChangeDate(date, 'sold_date_to', e)} />
                                        </LABEL>
                                        <SELECT
                                            id="diferent_type"
                                            name="diferent_type"
                                            groupClass="form-group col-md-1-25"
                                            labelClass=""
                                            labelTitle={trans('messages.different_type')}
                                            handleChangeMultiSelect={this.handleChangeMultiSelect}
                                            valueMulti={this.state.diferent_type||''}
                                            options={diffirentOptions}
                                            multiple="multiple"
                                            className="form-control input-sm" />
                                        <SELECT
                                            id="process_content"
                                            name="process_content"
                                            groupClass="form-group col-md-1-25"
                                            labelClass=""
                                            labelTitle={trans('messages.process_content')}
                                            handleChangeMultiSelect={this.handleChangeMultiSelect}
                                            valueMulti={this.state.process_content||''}
                                            options={processOptions}
                                            multiple="multiple"
                                            className="form-control input-sm" />
                                        <SELECT
                                            name="per_page"
                                            groupClass="form-group col-md-1 per-page pull-right-md"
                                            labelClass="pull-left"
                                            labelTitle={trans('messages.per_page')}
                                            onChange={this.handleChange}
                                            value={this.state.per_page||''}
                                            options={{20:20, 40:40, 60:60, 80:80, 100:100}}
                                            className="form-control input-sm" />
                                        </div>
                                    </div>
                                    <div className="col-md-12">
                                        <div className="row">
                                            <SELECT
                                                id="setlement_hrnb"
                                                name="setlement_hrnb"
                                                groupClass="form-group col-md-1-5"
                                                labelClass=""
                                                labelTitle={trans('messages.search_setlement_hrnb')}
                                                handleChangeMultiSelect={this.handleChangeMultiSelect}
                                                valueMulti={this.state.setlement_hrnb||''}
                                                options={settlementOptions}
                                                multiple="multiple"
                                                className="form-control input-sm" />
                                            <SELECT
                                                id="setlement_web"
                                                name="setlement_web"
                                                groupClass="form-group col-md-1-5"
                                                labelClass=""
                                                labelTitle={trans('messages.search_setlement_web')}
                                                handleChangeMultiSelect={this.handleChangeMultiSelect}
                                                valueMulti={this.state.setlement_web||''}
                                                options={settlementOptions}
                                                multiple="multiple"
                                                className="form-control input-sm" />
                                            <INPUT
                                                name="request_price_hrnb"
                                                groupClass="form-group col-md-1-5"
                                                labelTitle={trans('messages.search_request_price_hrnb')}
                                                onChange={this.handleChange}
                                                value={this.state.request_price_hrnb || ''}
                                                className="form-control input-sm"
                                                errorPopup={true}
                                                hasError={dataSource.message&&dataSource.message.request_price_hrnb?true:false}
                                                errorMessage={dataSource.message&&dataSource.message.request_price_hrnb?dataSource.message.request_price_hrnb:""}/>
                                            <INPUT
                                                name="request_price_web"
                                                groupClass="form-group col-md-1-5"
                                                labelTitle={trans('messages.search_request_price_web')}
                                                onChange={this.handleChange}
                                                value={this.state.request_price_web || ''}
                                                className="form-control input-sm"
                                                errorPopup={true}
                                                hasError={dataSource.message&&dataSource.message.request_price_web?true:false}
                                                errorMessage={dataSource.message&&dataSource.message.request_price_web?dataSource.message.request_price_web:""}/>
                                            <INPUT
                                                name="different_price"
                                                groupClass="form-group col-md-1-5"
                                                labelTitle={trans('messages.search_different_price')}
                                                onChange={this.handleChange}
                                                value={this.state.different_price || ''}
                                                className="form-control input-sm"
                                                errorPopup={true}
                                                hasError={dataSource.message&&dataSource.message.different_price?true:false}
                                                errorMessage={dataSource.message&&dataSource.message.different_price?dataSource.message.different_price:""}/>
                                            <SELECT
                                                id="status_hrnb"
                                                name="status_hrnb"
                                                groupClass="form-group col-md-1-75"
                                                labelClass=""
                                                labelTitle={trans('messages.search_status_hrnb')}
                                                handleChangeMultiSelect={this.handleChangeMultiSelect}
                                                valueMulti={this.state.status_hrnb||''}
                                                options={orderStatusOptions}
                                                multiple="multiple"
                                                className="form-control input-sm" />
                                            <SELECT
                                                id="status_web"
                                                name="status_web"
                                                groupClass="form-group col-md-1-5"
                                                labelClass=""
                                                labelTitle={trans('messages.search_status_web')}
                                                handleChangeMultiSelect={this.handleChangeMultiSelect}
                                                valueMulti={this.state.status_web||''}
                                                options={orderStatusOptions}
                                                multiple="multiple"
                                                className="form-control input-sm" />
                                            <SELECT
                                                id="is_correct"
                                                name="is_correct"
                                                groupClass="form-group col-md-1-25"
                                                labelClass=""
                                                labelTitle={trans('messages.match')}
                                                handleChangeMultiSelect={this.handleChangeMultiSelect}
                                                valueMulti={this.state.is_correct||''}
                                                options={isCorrectedOptions}
                                                multiple="multiple"
                                                className="form-control input-sm" />
                                        </div>
                                    </div>
                                </FORM>
                            </div>
                            </div>
                        </div>

                        <div className="box-body">
                            <div className="row">
                                <div className=" col-md-6 col-xs-12 margin-form">
                                    {/*<div className="pull-left margin-element pull-xs-right">
                                        <BUTTON className="btn btn-primary" onClick={(e) => this.handleProcessAPI(e, 'rakuten')}>{trans('messages.button_rakuten')}</BUTTON>
                                    </div>
                                    <div className=" pull-left margin-element pull-xs-right">
                                        <BUTTON className="btn btn-primary" onClick={(e) => this.handleProcessAPI(e, 'yahoo')}>{trans('messages.button_yahoo')}</BUTTON>
                                    </div>
                                    <div className=" pull-left margin-element pull-xs-right">
                                        <BUTTON className="btn btn-primary" onClick={(e) => this.handleImport(e, 'import_diy_csv')}>{trans('messages.import_diy_csv')}</BUTTON>
                                    </div>
                                    <div className=" pull-left margin-element pull-xs-right">
                                        <BUTTON className="btn btn-primary" onClick={(e) => this.handleImport(e, 'import_biz_csv')} >{trans('messages.import_biz_csv')}</BUTTON>
                                    </div><div className=" pull-left margin-element pull-xs-right">
                                        <BUTTON className="btn btn-primary" onClick={(e) => this.handleProcessAPI(e, 'b_dash_pro')} >{trans('messages.b_dash_pro')}</BUTTON>
                                    </div>
                                    <div className=" pull-left margin-element pull-xs-right">
                                        <BUTTON className="btn btn-primary" onClick={(e) => this.handleProcessAPI(e, 'amazon')} >{trans('messages.amazon')}</BUTTON>
                                    </div>*/}
                                    <div className=" pull-left margin-element pull-xs-right">
                                        <BUTTON className="btn btn-success margin-form  margin-element" onClick={(e) => this.handleExportCsv()}>{trans('messages.csv_export')}</BUTTON>
                                    </div>
                                </div>
                                <div className="col-md-6 col-xs-12">
                                <BUTTON className="btn btn-danger  pull-right margin-element" onClick={this.handleClearClick}>{trans('messages.btn_reset')}</BUTTON>
                                <BUTTON className="btn btn-primary  pull-right margin-element" onClick={this.handleReloadData} >{trans('messages.btn_search')}</BUTTON>
                                </div>
                            </div>
                            <div className="text-right ">
                                <p>{dataSource.pageData.from}件 - {dataSource.pageData.to}件 【全{dataSource.pageData.total}件】</p>
                            </div>
                            <div>

                                <div className="text-center">
                                    <PaginationContainer handleClickPage={this.handleClickPage}/>
                                </div>
                                <div className="table-responsive">
                                <TABLE className="table table-striped table-custom">
                                    <THEAD>
                                        <TR>
                                            <TH className="text-center text-middle col-max-min-60" rowSpan="2">
                                            </TH>
                                            <TH className="text-center text-middle col-max-min-100" rowSpan="2">
                                                <SortComponent
                                                    name="sort_name_jp"
                                                    onClick={this.handleSortClick}
                                                    currentSort={(this.state.sort_name_jp)?this.state.sort_name_jp:'none'}
                                                    title={trans('messages.mapping_mall')} />
                                            </TH>
                                            <TH className="text-center text-middle col-max-min-120" rowSpan="2">
                                                <SortComponent
                                                    name="sort_received_order_id"
                                                    onClick={this.handleSortClick}
                                                    currentSort={(this.state.sort_received_order_id)?this.state.sort_received_order_id:'none'}
                                                    title={trans('messages.received_order_id')} />
                                            </TH>
                                            <TH className="text-center text-middle col-max-min-100">
                                                <SortComponent
                                                    name="sort_order_date"
                                                    onClick={this.handleSortClick}
                                                    currentSort={(this.state.sort_order_date)?this.state.sort_order_date:'none'}
                                                    title={trans('messages.payment_order_date')} />
                                            </TH>
                                            <TH className="text-center text-middle col-max-min-100" rowSpan="2">{trans('messages.different_type')}
                                            </TH>
                                            <TH className="text-center text-middle" colSpan="2">{trans('messages.payment_name')}
                                            </TH>
                                            <TH className="text-center text-middle" colSpan="2">{trans('messages.request_price')}
                                            </TH>
                                            <TH className="text-center text-middle" rowSpan="2">
                                                <SortComponent
                                                    name="sort_different_price"
                                                    onClick={this.handleSortClick}
                                                    currentSort={(this.state.sort_different_price)?this.state.sort_different_price:'none'}
                                                    title={trans('messages.disparity_of_price')} />
                                            </TH>
                                            <TH className="text-center text-middle" colSpan="2">{trans('messages.payment_order_sub_status')}
                                            </TH>
                                            <TH className="text-center text-middle col-max-min-100" >{trans('messages.occurred_reason')}
                                            </TH>
                                            <TH className="text-center text-middle col-max-min-100">{trans('messages.process_content')}
                                            </TH>
                                            <TH className="text-center text-middle">{trans('messages.bulk')}
                                            </TH>
                                            <TH className="text-center text-middle col-max-min-100" rowSpan="2">{trans('messages.finished_date')}
                                            </TH>
                                        </TR>
                                        <TR>
                                            <TH className="text-center text-middle">
                                                <SortComponent
                                                    name="sort_delivery_date"
                                                    onClick={this.handleSortClick}
                                                    currentSort={(this.state.sort_delivery_date)?this.state.sort_delivery_date:'none'}
                                                    title={trans('messages.sold_date')} />
                                            </TH>
                                            <TH className="text-center text-middle">
                                                <SortComponent
                                                    name="sort_payment_method"
                                                    onClick={this.handleSortClick}
                                                    currentSort={(this.state.sort_payment_method)?this.state.sort_payment_method:'none'}
                                                    title={trans('messages.setlement_hrnb')} />
                                            </TH>
                                            <TH className="text-center text-middle">
                                                <SortComponent
                                                    name="sort_web_payment_method"
                                                    onClick={this.handleSortClick}
                                                    currentSort={(this.state.sort_web_payment_method)?this.state.sort_web_payment_method:'none'}
                                                    title={trans('messages.mapping_web')} />
                                            </TH>
                                            <TH className="text-center text-middle">
                                                <SortComponent
                                                    name="sort_request_price"
                                                    onClick={this.handleSortClick}
                                                    currentSort={(this.state.sort_request_price)?this.state.sort_request_price:'none'}
                                                    title={trans('messages.setlement_hrnb')} />
                                            </TH>
                                            <TH className="text-center text-middle">
                                                <SortComponent
                                                    name="sort_web_request_price"
                                                    onClick={this.handleSortClick}
                                                    currentSort={(this.state.sort_web_request_price)?this.state.sort_web_request_price:'none'}
                                                    title={trans('messages.mapping_web')} />
                                            </TH>
                                            <TH className="text-center text-middle">
                                                <SortComponent
                                                    name="sort_order_status"
                                                    onClick={this.handleSortClick}
                                                    currentSort={(this.state.sort_order_status)?this.state.sort_order_status:'none'}
                                                    title={trans('messages.setlement_hrnb')} />
                                            </TH>
                                            <TH className="text-center text-middle">
                                                <SortComponent
                                                    name="sort_web_order_status"
                                                    onClick={this.handleSortClick}
                                                    currentSort={(this.state.sort_web_order_status)?this.state.sort_web_order_status:'none'}
                                                    title={trans('messages.mapping_web')} />
                                            </TH>
                                            <TH className="text-center text-middle ">
                                                <SELECT
                                                    groupClass="col-md-12 clear-padding text-middle"
                                                    labelClass=""
                                                    name="occorred_reason_all"
                                                    value={this.state.occorred_reason_all || ''}
                                                    options={occurredOptionsAll}
                                                    onChange={(e) => this.handleAccept(e)}
                                                    className={"form-control input-sm "} />
                                            </TH>
                                            <TH className="text-center text-middle">
                                                <SELECT
                                                    groupClass="col-md-12 clear-padding text-middle"
                                                    labelClass=""
                                                    name="process_all"
                                                    value={this.state.process_all || ''}
                                                    options={processOptionsAll}
                                                    onChange={(e) => this.handleAccept(e)}
                                                    className={"form-control input-sm"} />
                                            </TH>
                                            <TH className="text-center text-middle">
                                                <BUTTON
                                                    className="btn btn-primary text-center"
                                                    name="is_corrected"
                                                    onClick={(e) => this.handleAccept(e)}>
                                                    {trans('messages.btn_edit_map_all')}
                                                </BUTTON>
                                            </TH>
                                        </TR>
                                    </THEAD>
                                    <TBODY>
                                        {
                                            (!_.isEmpty(dataSource.data.data))?
                                            dataSource.data.data.map((item) => {
                                                index++;

                                                let formatOrderDate = !_.isEmpty(item.order_date) ? moment.utc(item.order_date, "YYYY-MM-DD").format("YYYY/MM/DD") : '';
                                                let formatSoldDate = !_.isEmpty(item.delivery_date) ? moment.utc(item.delivery_date, "YYYYMMDD").format("YYYY/MM/DD") : '';
                                                let formatFinishDate = !_.isEmpty(item.finished_date) ? moment.utc(item.finished_date, "YYYY-MM-DD").format("YYYY/MM/DD") : '';
                                                let classRow = '';
                                                let check = false;

                                                if (item.is_corrected === 1 || item.is_deleted === 1) {
                                                    classRow = 'color-mapping-status';
                                                    check = true;
                                                }
                                                return (
                                                    [
                                                    <TR key={"tr" + index + classRow + item.occorred_reason + item.process_content} className={classRow}>
                                                        <TD className="text-center text-middle">
                                                           <a target="_blank" href={'/order-management/detail?receive_id_key='+item.receive_id}>
                                                                <img src="/img/uri_img_order_detail.png" width="35" />
                                                           </a>
                                                        </TD>
                                                        <TD className="cut-text text-middle" title={item.name_jp} rowSpan="2">{item.name_jp}</TD>
                                                        <TD className="cut-text text-middle" title={item.received_order_id} rowSpan="2">{item.received_order_id}</TD>
                                                        <TD title={formatOrderDate}>{formatOrderDate}</TD>
                                                        <TD className="text-middle text-new-line" title={item.different_type} rowSpan="2">{item.different_type}</TD>
                                                        <TD className="text-left text-middle" title={item.hrnb_payment_name} rowSpan="2">{item.hrnb_payment_name}</TD>
                                                        <TD className="text-middle text-left" title={item.web_payment_name} rowSpan="2">{item.web_payment_name}</TD>
                                                        <TD className="text-right text-middle" title={nf.format(item.request_price)} rowSpan="2">{nf.format(item.request_price )}</TD>
                                                        <TD className="text-right text-middle" title={nf.format(item.web_request_price)} rowSpan="2">{nf.format(item.web_request_price)}</TD>
                                                        <TD className="text-right text-middle" title={nf.format(item.request_price - item.web_request_price)} rowSpan="2">{nf.format(item.request_price - item.web_request_price)}</TD>
                                                        <TD className="text-right text-middle" title={item.hrnb_order_status_name} rowSpan="2">{item.hrnb_order_status_name}</TD>
                                                        <TD className="text-right text-middle" title={item.web_order_status_name} rowSpan="2">{item.web_order_status_name}</TD>
                                                        <TD className="text-center text-middle" rowSpan="2">
                                                            <SELECT
                                                                groupClass="col-md-12 clear-padding text-middle"
                                                                labelClass=""
                                                                value={item.occorred_reason || ''}
                                                                options={occurredOptions}
                                                                disabled={check ? true : false}
                                                                onChange={(e) => this.handleButtonList(e, item.received_order_id, 'occorred_reason')}
                                                                className={"form-control input-sm " + (check ? 'color-mapping-status' : '')} />
                                                        </TD>
                                                        <TD className="text-center text-middle" rowSpan="2">
                                                            <SELECT
                                                                groupClass="col-md-12 clear-padding text-middle"
                                                                labelClass=""
                                                                value={item.process_content || ''}
                                                                disabled={check ? true : false}
                                                                onChange={(e) => this.handleButtonList(e, item.received_order_id, 'process_content')}
                                                                options={processOptions}
                                                                className={"form-control input-sm " + (check ? 'color-mapping-status' : '')} />
                                                        </TD>
                                                        {!check ?
                                                        <TD className="text-center cut-text">
                                                            <BUTTON className="btn btn-primary text-center" onClick={(e) => this.handleButtonList(e, item.received_order_id, 'is_corrected', item.is_corrected)}>{trans('messages.match')}</BUTTON>
                                                        </TD>
                                                        :
                                                        <TD className="text-middle text-center" rowSpan="2">
                                                            <BUTTON className="btn btn-primary" onClick={(e) => this.handleButtonList(e, item.received_order_id, 'all', null)}>{trans('messages.mapping_active')}</BUTTON>
                                                        </TD>}
                                                        <TD className="text-center text-middle" title={formatFinishDate} rowSpan="2">{formatFinishDate}</TD>
                                                    </TR>,
                                                    <TR key={"tr2" + index} className={classRow}>
                                                        <TD className="text-center text-middle">
                                                           <a target="_blank" href={item.link_mall}><img src="/img/uri_img_web_detail.png" width="29" /></a>
                                                        </TD>
                                                        <TD title={formatSoldDate}>{formatSoldDate}</TD>
                                                        {!check &&
                                                            <TD className="text-center cut-text">
                                                                <BUTTON className="btn btn-primary text-center" onClick={(e) => this.handleButtonList(e, item.received_order_id, 'is_deleted', item.is_deleted)}>{trans('messages.disable')}</BUTTON>
                                                            </TD>}
                                                    </TR>
                                                   ]

                                                )
                                                if (index === dataSource.pageData.to) {
                                                   index = 0;
                                                }
                                            })
                                            :
                                            <TR><TD colSpan="17" className="text-center">{trans('messages.no_data')}</TD></TR>
                                        }
                                    </TBODY>
                                </TABLE>
                                </div>
                                <div className="text-center">
                                    <PaginationContainer handleClickPage={this.handleClickPage}/>
                                </div>
                                <ImportCsv
                                    handleChooseFile={this.handleChooseFile}
                                    handleImportSubmit={this.handleImportSubmit}
                                    value={this.state.attached_file_path ? this.state.attached_file_path : ''}
                                    error={this.state.error}
                                    filename={this.state.filename}
                                    modalTitle={this.state.modalTitle}
                                    handleImportCancel={this.handleImportCancel}
                                    process={this.state.process ? true : false}
                                    status={this.state.status_csv}
                                    percent={this.state.percent}
                                    finish={this.state.finish}
                                    hasError={(this.state.message || this.state.msg) ? true : false}
                                    errorMessage={this.state.message || this.state.msg}
                                />
                                <AcceptPopup
                                    handleEditMapAll ={this.handleEditMapAll}
                                    handleOccorredReason ={this.handleOccorredReason}
                                    handleCancel={this.handleCancelAccept}
                                    handleProcessAll={this.handleProcessAll}
                                    name={this.state.acccept_name}
                                />
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}
const mapStateToProps = (state) => {
  return {
    dataSource: state.commonReducer.dataSource,
    dataResponse: state.commonReducer.dataResponse,
    dataResponseCsv: state.commonReducer.dataResponseCsv
  }
}
const  mapDispatchToProps = (dispatch) => {
  let ignore = ['attached_file', 'attached_file_path','message','flg', 'process', 'msg', 'timeRun', 'currPage',
      'total', 'filename', 'error','percent', 'status_csv', 'url', 'reload', 'params', 'headers', 'count',
      'realFilename','type','fileCorrect', 'checkAfter', 'modalTitle', 'update_all_occorred_reason',
      'occorred_reason_all', 'update_all', 'acccept_name', 'is_corrected', 'process_all'
  ];
  return {
    reloadData: (url, params, pushHistory, ignoreFields) => { dispatch(Action.reloadData(url, params, pushHistory, ignore)) },
    postData: (url, params, headers,callback,callParam) => { dispatch(Action.postData(url, params, headers,callback,callParam)) },
    postDataCSV: (url, params, headers, count_err) => { dispatch(Action.postDataCSV(url, params, headers, count_err)) }
  }
}
export default connect(mapStateToProps, mapDispatchToProps)(WebMappingList)
