import React, { Component } from 'react';
import { connect } from 'react-redux';

import * as Action from '../common/actions';
import * as queryString from 'query-string';

import Loading from '../common/components/common/Loading';
import FORM from '../common/components/form/FORM';
import INPUT from '../common/components/form/INPUT';
import SELECT from '../common/components/form/SELECT';
import BUTTON from '../common/components/form/BUTTON';
import MESSAGE_MODAL from '../common/containers/MessageModal'

class SaveSendMailTiming extends Component {

    constructor(props) {
        super(props);
        this.state = {}
        let params = queryString.parse(props.location.search);
        if (params['order_status_id'] && params['order_sub_status_id']) {
            this.state = {
                order_status_id: params['order_status_id'],
                order_sub_status_id: params['order_sub_status_id'],
                index : params['order_status_id'] + '-' + params['order_sub_status_id']
            };
            this.hanleRealtime();
        }
        this.props.reloadData(formDataUrl, this.state);
    }
    hanleRealtime() {
        if (!_.isEmpty(this.state.index)) {
            let params = {
                accessFlg : this.state.accessFlg,
                query : {
                        page_key : 'SAVE-SEND-MAIL-TIMING',
                        primary_key : 'order_status_id:' + this.state.order_status_id + '/order_sub_status_id:' + this.state.order_sub_status_id,
                    },
                key :this.state.index
            };
            if (this.state.accessFlg !== false) {
                this.props.postRealTime(checkEditUrl, params, {'X-Requested-With': 'XMLHttpRequest'});
            }
        }
    }
    componentWillReceiveProps(nextProps) {
        let dataResponse = nextProps.dataResponse;
        if (!_.isEmpty(dataResponse.mailTemplateOption)) {
            let mailId = '';
            if (!_.isEmpty(this.state.mail_id)) {
                mailId = this.state.mail_id;
            }
            this.setState({
                mailTemplateOption: dataResponse.mailTemplateOption,
                mail_id: mailId
            });
            document.getElementsByName('mail_id')[0].value = mailId;
        }

        if (!_.isEmpty(nextProps.dataResponse.method) && nextProps.dataResponse.method === 'save') {
            if (nextProps.dataResponse.status === 1) {
                window.location.href = baseUrl;
            } else {
                document.getElementsByName('btnAccept')[0].disabled = false;
            }
        }
        // Check realtime update
        if (!_.isEmpty(nextProps.dataRealTime)) {
            this.setState(nextProps.dataRealTime);
        }
    }

    handleChange = (event) => {
        const target = event.target;
        const name = target.name;
        let value = target.value;
        if (value === '_none') {
            value = '';
        }
        let fieldName = '';
        switch(name) {
            case 'status_name':
                fieldName = 'order_status_id';
                break;
            case 'sub_status_name':
                fieldName = 'order_sub_status_id';
                break;
            case 'template_name':
                fieldName = 'mail_id';
                break;
            case 'genre':
                this.props.postData(
                    getTemplateUrl,
                    {genre: value},
                    {'X-Requested-With': 'XMLHttpRequest'}
                );
                return;
            default:
                return;
        }
        this.setState({
            [fieldName]: value
        }, () => {
            document.getElementsByName(fieldName)[0].value = value;
        });
    }

    handleCancel = (event) => {
        window.location.href = baseUrl;
    }

    handleSubmit = (event) => {
        document.getElementsByName('btnAccept')[0].disabled = true;
        let headers = {'X-Requested-With': 'XMLHttpRequest'};
        if (this.props.enctype) {
            headers['Content-Type'] = this.props.enctype;
        }
        let formData   = new FormData();
        let dataSource = this.props.dataSource;
        if (dataSource.mail_id !== null) {
            formData.append('mail_id', dataSource.mail_id);
        }
        let genre = document.getElementsByName('genre')[0].value;
        if (genre !== '_none') {
            formData.append('genre', genre);
        }
        if (this.state && Object.keys(this.state).length > 0) {
            Object.keys(this.state).map((item) => {
                formData.append(item, this.state[item]);
            });
        }

        this.props.postData(this.props.location.pathname, formData, headers);
    }

    componentDidUpdate() {
        if (!_.isEmpty(this.props.dataSource)) {
            $('.loading').hide();
        }
        if (this.state.accessFlg === false) {
            $('#message-modal').modal('show');
            //We will auto redirect after 5 seconds
            setTimeout(() => {
                window.location.href = baseUrl;
            }, 5000);
        }
    }
    // Check realtime check for editing
    componentWillUnmount () {
        clearInterval(this.state);
    }
    // Check realtime check for editing
    componentDidMount() {
        setInterval(() => {
            this.hanleRealtime()
        }, 5000);

    }

    render() {
        let dataSource   = this.props.dataSource;
        let dataResponse = this.props.dataResponse;

        return ((!_.isEmpty(dataSource)) &&
        <div >
        <div className="box box-primary">
            <div className="container">
            <FORM className="form-horizontal">
                <div className="box-body">
                <Loading className="loading"></Loading>
                <div className="row">
                    <div className="col-md-12">
                        <div className="row">
                            <div className="col-md-3">
                                <INPUT
                                    name="order_status_id"
                                    labelTitle={trans('messages.order_status_id')}
                                    className="form-control input-sm"
                                    fieldGroupClass="col-md-6"
                                    labelClass="col-md-6"
                                    groupClass=""
                                    errorPopup={true}
                                    defaultValue={dataSource.order_status_id}
                                    hasError={dataResponse.message&&dataResponse.message.order_status_id?true:false}
                                    errorMessage={dataResponse.message&&dataResponse.message.order_status_id?dataResponse.message.order_status_id:""}
                                    disabled/>
                            </div>
                            <div className="col-md-9">
                                 <SELECT
                                    name="status_name"
                                    className="form-control input-sm"
                                    fieldGroupClass="col-md-4"
                                    labelClass=""
                                    groupClass=""
                                    readOnly={dataSource.order_status_id?true:false}
                                    onChange={this.handleChange}
                                    options={dataSource.orderStatusOption}
                                    defaultValue={dataSource.order_status_id}
                                    hasError={dataResponse.message&&dataResponse.message.status_name?true:false}
                                    errorMessage={dataResponse.message&&dataResponse.message.status_name?dataResponse.message.status_name:""}
                            />
                            </div>
                        </div>
                        <div className="row">
                            <div className="col-md-3">
                                <INPUT
                                    name="order_sub_status_id"
                                    labelTitle={trans('messages.order_sub_status_id')}
                                    className="form-control input-sm"
                                    fieldGroupClass="col-md-6"
                                    labelClass="col-md-6"
                                    groupClass=""
                                    errorPopup={true}
                                    defaultValue={dataSource.order_sub_status_id}
                                    hasError={dataResponse.message&&dataResponse.message.order_sub_status_id?true:false}
                                    errorMessage={dataResponse.message&&dataResponse.message.order_sub_status_id?dataResponse.message.order_sub_status_id:""}
                                    disabled/>
                            </div>
                            <div className="col-md-9">
                                 <SELECT
                                    readOnly={dataSource.order_sub_status_id?true:false}
                                    name="sub_status_name"
                                    labelTitle=""
                                    className="form-control input-sm"
                                    fieldGroupClass="col-md-4"
                                    labelClass=""
                                    groupClass=""
                                    onChange={this.handleChange}
                                    options={dataSource.orderSubStatusOption}
                                    defaultValue={dataSource.order_sub_status_id}
                                    hasError={dataResponse.message&&dataResponse.message.sub_status_name?true:false}
                                    errorMessage={dataResponse.message&&dataResponse.message.sub_status_name?dataResponse.message.sub_status_name:""}
                            />
                            </div>
                        </div>
                        <div className="row">
                            <div className="col-md-3">
                                <INPUT
                                    name="mail_id"
                                    labelTitle={trans('messages.template_id')}
                                    className="form-control input-sm"
                                    fieldGroupClass="col-md-6"
                                    labelClass="col-md-6"
                                    groupClass=""
                                    errorPopup={true}
                                    onChange={this.handleChange}
                                    defaultValue={dataSource.mail_id}
                                    hasError={dataResponse.message&&dataResponse.message.mail_id?true:false}
                                    errorMessage={dataResponse.message&&dataResponse.message.mail_id?dataResponse.message.mail_id:""}
                                    disabled/>
                            </div>
                            <div className="col-md-3">
                                 <SELECT
                                    name="genre"
                                    className="form-control input-sm"
                                    fieldGroupClass="col-md-12"
                                    labelClass=""
                                    groupClass=""
                                    onChange={this.handleChange}
                                    options={dataSource.genreOption}
                                    defaultValue={dataSource.genre_id}
                                    hasError={dataResponse.message&&dataResponse.message.genre?true:false}
                                    errorMessage={dataResponse.message&&dataResponse.message.genre?dataResponse.message.genre:""}
                            />
                            </div>
                            <div className="col-md-4">
                                 <SELECT
                                    name="template_name"
                                    className="form-control input-sm"
                                    fieldGroupClass=" "
                                    labelClass=" "
                                    groupClass=" "
                                    onChange={this.handleChange}
                                    options={dataResponse.mailTemplateOption||dataSource.mailTemplateOption}
                                    defaultValue={dataSource.mail_id}
                                    hasError={dataResponse.message&&dataResponse.message.template_name?true:false}
                                    errorMessage={dataResponse.message&&dataResponse.message.template_name?dataResponse.message.template_name:""}
                            />
                            </div>
                        </div>
                    </div>
                </div>
                    <div className="row">
                        <div className="col-md-offset-6 col-md-4 col-md-offset-right-2">
                            <BUTTON name="btnAccept" className="btn btn-success input-sm btn-larger margin-element" type="button" onClick={this.handleSubmit}>{"登録"}</BUTTON>
                            <BUTTON className="btn bg-purple input-sm btn-larger" type="button" onClick={this.handleCancel}>{"取消"}</BUTTON>
                        </div>
                    </div>
                </div>
            </FORM>
            <MESSAGE_MODAL
                    message={this.state.message || ''}
                    title={trans('messages.deny_edit')}
                    baseUrl={baseUrl}
            />
            </div>
            </div>
        </div>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        dataSource: state.commonReducer.dataSource,
        dataResponse: state.commonReducer.dataResponse,
        dataRealTime: state.commonReducer.dataRealTime
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        reloadData: (url, params) => { dispatch(Action.detailData(url, params)) },
        postData: (url, params, headers) => { dispatch(Action.postData(url, params, headers)) },
        postRealTime: (url, params, headers) => { dispatch(Action.postRealTime(url, params, headers)) }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(SaveSendMailTiming)