import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';

import { connect } from 'react-redux';
import * as Action from '../common/actions';
import * as queryString from 'query-string';

import BUTTON from '../common/components/form/BUTTON';
import FORM from '../common/components/form/FORM';
import INPUT from '../common/components/form/INPUT';
import SELECT from '../common/components/form/SELECT';
import CHECKBOX_CUSTOM from '../common/components/form/CHECKBOX_CUSTOM';

import TABLE from '../common/components/table/TABLE';
import THEAD from '../common/components/table/THEAD';
import TBODY from '../common/components/table/TBODY';
import TR from '../common/components/table/TR';
import TH from '../common/components/table/TH';
import TD from '../common/components/table/TD';
import PaginationContainer from '../common/containers/PaginationContainer'

import Loading from '../common/components/common/Loading';

class ListSendMailTiming extends Component {

    constructor(props) {
        super(props);
        const stringParams = queryString.parse(props.location.search);
        this.state = {};
        if (!_.isEmpty(stringParams)) {
            this.state = stringParams;
        }
        this.props.reloadData(dataListUrl, stringParams);
    }

    handleChange = (event) => {
        const target = event.target;
        const value = target.type === 'checkbox' ? target.checked : target.value;
        const name = target.name;
        this.setState({
            [name]: (value)?value:undefined
        },() => {
            if (name === 'per_page') {
                this.handleReloadData();
            }
        });
    }

     handleClearClick = (event) => {
        Object.keys(this.state).map((key) => {
            if (key !== 'per_page') {
                delete this.state[key];
            }
        });
        this.handleReloadData();
    }

    handleReloadData = () => {
        if (this.state.page) {
            delete this.state.page;
        }
        this.props.reloadData(dataListUrl, this.state, true);
    }

    handleClickPage = (page) => {
        this.setState({
            page: page
        },() => {
            this.props.reloadData(dataListUrl, this.state, true);
        });
    }

    handleIsAvailableChange = (orderStatusId, orderSubStatusId, event) => {
        const value = event.target.checked;
        let params  = {
            order_status_id : orderStatusId,
            order_sub_status_id : orderSubStatusId,
            is_available : value?1:0,
        }
        this.props.postData(changeStatus, params, {'X-Requested-With': 'XMLHttpRequest'}, this.handleReloadData);
    }

    componentDidUpdate() {
        if (!_.isEmpty(this.props.dataSource)) {
            $('.loading').hide();
        }
    }

    render() {
        let dataSource = this.props.dataSource;
        let index = 0;
        let totalItems = 0;
        let itemsPerPage = 20;
        if (!_.isEmpty(dataSource)) {
            index = dataSource.data.from - 1;
            totalItems   = dataSource.data.total;
            itemsPerPage = dataSource.data.per_page;
        }
        return ( (!_.isEmpty(dataSource)) &&
            <div>
                <Loading className="loading" />
                <div className="row">
                    <div className="col-xs-12">
                        <div className="box">
                            <div className="box-header">
                                <div className="row">
                                    <div className="col-md-12">
                                        <h4>{trans('messages.search_title')}</h4>
                                    </div>
                                        <FORM>
                                            <INPUT
                                                name="status_name"
                                                groupClass="form-group col-md-2"
                                                labelTitle={trans('messages.status_name')}
                                                onChange={this.handleChange}
                                                value={this.state.status_name || ''}
                                                className="form-control input-sm" />
                                            <INPUT
                                                name="sub_status_name"
                                                groupClass="form-group col-md-2"
                                                value={this.state.sub_status_name || ''}
                                                onChange={this.handleChange}
                                                labelTitle={trans('messages.sub_status_name')}
                                                className="form-control input-sm" />
                                            <INPUT
                                                name="template_name"
                                                value={this.state.template_name || ''}
                                                onChange={this.handleChange}
                                                groupClass="form-group col-md-3-75"
                                                labelTitle={trans('messages.template_name')}
                                                className="form-control input-sm" />

                                            <SELECT
                                                name="per_page"
                                                groupClass="form-group col-md-offset-3 col-md-2 per-page pull-right-md"
                                                labelClass="pull-right"
                                                labelTitle={trans('messages.per_page')}
                                                onChange={this.handleChange}
                                                value={this.state.per_page || ''}
                                                options={{20:20, 40:40, 60:60, 80:80, 100:100}}
                                                className="form-control input-sm" />
                                        </FORM>
                                </div>
                                <div className="row">
                                    <div className="col-md-9 pull-left">
                                        <a href={saveUrl} className="btn btn-success input-sm">{trans('messages.btn_add')}</a>
                                    </div>
                                    <div className="col-md-3 pull-right">
                                            <BUTTON className="btn btn-danger pull-right" onClick={this.handleClearClick}>{trans('messages.btn_reset')}</BUTTON>
                                            <BUTTON className="btn btn-primary margin-element pull-right" onClick={this.handleReloadData} >{trans('messages.btn_search')}</BUTTON>
                                    </div>
                                </div>
                            </div>
                            <div className="box-body">
                                <div className="dataTables_wrapper form-inline dt-bootstrap">
                                    <div className="row">
                                        <div className="col-sm-12">
                                            {(totalItems/itemsPerPage > 1) &&
                                            <div className="row">
                                                <div className="col-md-2-5"></div>
                                                <div className="col-md-7 text-center">
                                                    <PaginationContainer handleClickPage={this.handleClickPage}/>
                                                </div>
                                                <div className="col-md-2-5 text-right line-height-md">
                                                    {dataSource.data.from}件 - {dataSource.data.to}件 【全{dataSource.data.total}件】
                                                </div>
                                            </div>
                                            }
                                            <TABLE className="table table-striped table-custom">
                                                <THEAD>
                                                    <TR>
                                                        <TH className="hidden-lg text-center" ></TH>
                                                        <TH className="hidden-xs hidden-sm text-center">＃</TH>
                                                        <TH className="text-center  col-max-min-60">{trans('messages.status_name')}</TH>
                                                        <TH className="hidden-xs hidden-sm text-center col-max-min-60">{trans('messages.sub_status_name')}</TH>
                                                        <TH className="hidden-xs hidden-sm text-center">{trans('messages.template_name')}</TH>
                                                        <TH className="text-center  col-max-min-60">{trans('messages.is_available')}</TH>
                                                    </TR>
                                                </THEAD>
                                                <TBODY>
                                                    {
                                                        (!_.isEmpty(dataSource.data.data))?
                                                        dataSource.data.data.map((item) => {
                                                            index++;
                                                            let classRow = (index%2 ===0) ? 'odd' : 'even';
                                                            return (
                                                                [<TR key={"tr" + index} className={classRow}>
                                                                    <TD className="width-span1 hidden-lg text-center" >
                                                                        <b className="show-info">
                                                                            <i className="fa fa-angle-double-down" aria-hidden="true"></i>
                                                                        </b>
                                                                    </TD>
                                                                    <TD className="width-span1 hidden-xs hidden-sm text-center">{index}</TD>
                                                                    <TD>
                                                                    <a className="link-detail" href={saveUrl + "?order_status_id=" +
                                                                                item.order_status_id + "&order_sub_status_id=" + item.order_sub_status_id}>
                                                                        <span>{item.status_name}</span></a>
                                                                    </TD>
                                                                    <TD className="cut-text hidden-xs hidden-sm text-center">{item.sub_status_name}</TD>
                                                                    <TD className="hidden-xs hidden-sm">{item.template_name}</TD>
                                                                    <TD className="text-center">
                                                                        <CHECKBOX_CUSTOM
                                                                        key={'order_status[' + item.order_status_id + '_' + item.order_sub_status_id + Math.random() +']'}
                                                                        defaultChecked={item.is_available === 1 ? true : false}
                                                                        name={'order_status[' + item.order_status_id + '_' + item.order_sub_status_id + ']'}
                                                                        onChange={(e) => this.handleIsAvailableChange(item.order_status_id, item.order_sub_status_id, e)}
                                                                        value={item.is_available} />

                                                                    </TD>
                                                                </TR>,
                                                                <TR key={"trhide" + index} className="hiden-info" style={{display:"none"}}>
                                                                    <TD colSpan="5" className="width-span1">
                                                                        <ul>
                                                                        <li> <b>#</b> : {index}</li>
                                                                            <li>
                                                                                <b>{trans('messages.status_name')} : </b>
                                                                                {item.status_name}
                                                                            </li>
                                                                            <li>
                                                                                <b>{trans('messages.sub_status_name')} : </b>
                                                                                {item.sub_status_name}
                                                                            </li>
                                                                            <li>
                                                                                <b>{trans('messages.template_name')} : </b>
                                                                                {item.template_name}
                                                                            </li>
                                                                            <li>
                                                                                <b>{trans('messages.is_available')} : </b>
                                                                                {(item.is_available === 1) ? 'Available' : 'Un available'}
                                                                            </li>
                                                                        </ul>
                                                                    </TD>
                                                                </TR>
                                                                ]
                                                            )
                                                        }):
                                                        <TR><TD colSpan="9" className="text-center">{trans('messages.no_data')}</TD></TR>
                                                    }
                                                </TBODY>
                                            </TABLE>
                                            <PaginationContainer handleClickPage={this.handleClickPage}/>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        dataSource: state.commonReducer.dataSource,
        dataResponse: state.commonReducer.dataResponse
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        reloadData: (url, params, pushHistory, ignoreFields) => { dispatch(Action.reloadData(url, params, pushHistory, ignoreFields)) },
        postData: (url, params, headers, callback) => { dispatch(Action.postData(url, params, headers, callback)) }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(ListSendMailTiming)