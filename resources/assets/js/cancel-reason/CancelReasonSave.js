import React, { Component } from 'react';
import { connect } from 'react-redux';

import * as Action from '../common/actions';
import * as queryString from 'query-string';

import Loading from '../common/components/common/Loading';
import FORM from '../common/components/form/FORM';
import INPUT from '../common/components/form/INPUT';
import SELECT from '../common/components/form/SELECT';
import TEXTAREA from '../common/components/form/TEXTAREA';
import UPLOAD from '../common/components/form/UPLOAD';
import BUTTON from '../common/components/form/BUTTON';
import MESSAGE_MODAL from '../common/containers/MessageModal';

class CancelReasonSave extends Component {
    constructor(props) {
        super(props);
        this.state = {}
        let params = queryString.parse(props.location.search);
        this.firstRender = true;
        if (params['reason_id']) {
            this.state.reason_id = params['reason_id'];
            this.state.index = params['reason_id'];
            this.state.accessFlg = true;
            this.hanleRealtime();
        }

        this.props.reloadData(getFormDataUrl, this.state);
    }

    handleCancel = (event) => {
        window.location.href = baseUrl;
    }

    hanleRealtime() {
        if (!_.isEmpty(this.state.index)) {
            let params = {
                accessFlg : this.state.accessFlg,
                query : {
                        page_key : 'CANCEL-SAVE',
                        primary_key : 'index:' + this.state.index
                    },
                key :this.state.index
            };
            if (this.state.accessFlg !== false) {
                this.props.postRealTime(checkEditUrl, params, {'X-Requested-With': 'XMLHttpRequest'});
            }
        }
    }

    handleSubmit = (event) => {
        document.getElementsByName('btnAccept')[0].disabled = true;
        let headers = {'X-Requested-With': 'XMLHttpRequest'};

        this.props.postData(saveUrl, this.state, headers);
    }

    handleChange = (event) => {
        const target = event.target;
        const name = target.name;
        const value = target.type === 'checkbox' ? target.checked : target.value;
        this.setState({
            [name]: value
        });
    }

    componentWillReceiveProps(nextProps) {
        if (!_.isEmpty(nextProps.dataSource)) {            
            if (!_.isEmpty(nextProps.dataSource.data)) {
                if (this.firstRender) {
                    this.firstRender = false;
                    this.setState(nextProps.dataSource.data);
                }
            } else {
                if (this.state.reason_id && _.isEmpty(nextProps.dataSource.data)) {
                    window.location.href = baseUrl;
                }            
            }
        }
        if (!_.isEmpty(nextProps.dataResponse)) {
            if (nextProps.dataResponse.status === 1) {
                window.location.href = baseUrl;
            } else {
                document.getElementsByName('btnAccept')[0].disabled = false;
            }
        }
        // Check realtime update
        if (!_.isEmpty(nextProps.dataRealTime)) {
            this.setState(nextProps.dataRealTime);
        }
    }

    componentWillUnmount () {
        clearInterval(this.state);
    }
    componentDidMount() {
        setInterval(() => {
            this.hanleRealtime()
        }, 5000);

    }

    componentDidUpdate() {
        if (!_.isEmpty(this.props.dataSource)) {
            $('.loading').hide();
        }
        if (this.state.accessFlg === false) {
            $('#message-modal').modal('show');
            //We will auto redirect after 5 seconds
            setTimeout(() => {
                window.location.href = baseUrl;
            }, 5000);
        }
    }

    render() {
        let dataSource   = this.props.dataSource;
        let dataResponse = this.props.dataResponse;

        return ((!_.isEmpty(dataSource)) &&
            <div>
            <div className="box box-primary">
            <FORM
                className="form-horizontal">
                <div className="box-body">
                <Loading className="loading" />
                <div className="row">
                    <div className="col-md-8 col-md-offset-2 col-md-offset-right-2">
                        <INPUT
                            name="type"
                            labelTitle={trans('messages.reason_type')}
                            className="form-control input-sm"
                            fieldGroupClass="col-md-5"
                            labelClass="col-md-3"
                            onChange={this.handleChange}
                            defaultValue={dataSource.data.type}
                            errorPopup={true}
                            hasError={(dataResponse.message && dataResponse.message.type)?true:false}
                            errorMessage={(dataResponse.message&&dataResponse.message.type)?dataResponse.message.type:""}/>
                        <TEXTAREA
                            name="reason_content"
                            labelTitle={trans('messages.reason_content')}
                            className="form-control input-sm"
                            fieldGroupClass="col-md-9"
                            labelClass="col-md-3"
                            onChange={this.handleChange}
                            defaultValue={dataSource.data.reason_content}
                            errorPopup={true}
                            hasError={(dataResponse.message && dataResponse.message.reason_content)?true:false}
                            errorMessage={(dataResponse.message&&dataResponse.message.reason_content)?dataResponse.message.reason_content:""}
                            rows="4"/>
                        <TEXTAREA
                            name="add_2_mail_text"
                            labelTitle={trans('messages.add_2_mail_text')}
                            className="form-control input-sm"
                            fieldGroupClass="col-md-9"
                            labelClass="col-md-3"
                            onChange={this.handleChange}
                            defaultValue={dataSource.data.add_2_mail_text}
                            errorPopup={true}
                            hasError={(dataResponse.message && dataResponse.message.add_2_mail_text)?true:false}
                            errorMessage={(dataResponse.message&&dataResponse.message.add_2_mail_text)?dataResponse.message.add_2_mail_text:""}
                            rows="10"/>
                    </div>
                </div>
                <div className="row">
                    <div className="col-md-8 col-md-offset-2 col-md-offset-right-2">
                        <BUTTON className="btn bg-purple input-sm pull-right pull-right btn-larger" type="button" onClick={this.handleCancel}>{trans('messages.btn_cancel')}</BUTTON>
                        <BUTTON name="btnAccept" className="btn btn-success input-sm btn-larger pull-right margin-element" type="button" onClick={this.handleSubmit}>{trans('messages.btn_accept')}</BUTTON>
                    </div>
                </div>
            </div>
            </FORM>
            </div>
            <MESSAGE_MODAL
                message={this.state.message || ''}
                title={trans('messages.deny_edit')}
                baseUrl={baseUrl}
            />
        </div>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        dataSource: state.commonReducer.dataSource,
        dataResponse: state.commonReducer.dataResponse,
        dataRealTime: state.commonReducer.dataRealTime
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        reloadData: (url, params) => { dispatch(Action.detailData(url, params)) },
        postData: (url, params, headers) => { dispatch(Action.postData(url, params, headers)) },
        postRealTime: (url, params, headers) => { dispatch(Action.postRealTime(url, params, headers)) }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(CancelReasonSave)