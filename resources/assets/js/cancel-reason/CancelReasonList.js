import React, { Component } from 'react';

import { connect } from 'react-redux';
import * as Action from '../common/actions';
import * as queryString from 'query-string';

import BUTTON from '../common/components/form/BUTTON';
import FORM from '../common/components/form/FORM';
import INPUT from '../common/components/form/INPUT';
import SELECT from '../common/components/form/SELECT';

import TABLE from '../common/components/table/TABLE';
import THEAD from '../common/components/table/THEAD';
import TBODY from '../common/components/table/TBODY';
import TR from '../common/components/table/TR';
import TH from '../common/components/table/TH';
import TD from '../common/components/table/TD';

import SortComponent from '../common/components/table/SortComponent';
import PaginationContainer from '../common/containers/PaginationContainer'
import Loading from '../common/components/common/Loading';

class CancelReasonList extends Component {

    constructor(props) {
        super(props);
        const stringParams = queryString.parse(props.location.search);
        this.state = {};
        if (!_.isEmpty(stringParams)) {
            this.state = stringParams;
        }
        this.props.reloadData(dataListUrl, this.state);
    }

    handleChange = (event) => {
        const target = event.target;
        const value = target.type === 'checkbox' ? target.checked : target.value;
        const name = target.name;
        this.setState({
            [name]: (value)?value:undefined
        },() => {
            if (name === 'per_page') {
                this.handleReloadData();
            }
        });
    }

    handleClearClick = (event) => {
        Object.keys(this.state).map((key) => {
            if (key !== 'per_page') {
                delete this.state[key];
            }
        });
        this.handleReloadData();
    }

    handleReloadData = () => {
        if (this.state.page) {
            delete this.state.page;
        }
        this.props.reloadData(dataListUrl, this.state, true);
    }

    componentDidUpdate() {
        if (!_.isEmpty(this.props.dataSource)) {
            $('.loading').hide();
        }
    }

    componentWillMount() {
        window.onpopstate = (event) => {
            location.reload();
        };
    }

    handleSortClick = (name, nextSort) => {
        Object.keys(this.state).map((key) => {
            if (key.startsWith('sort_')) {
                delete this.state[key];
            }
        });
        if (nextSort === 'none') {
            delete this.state[name];
            this.props.reloadData(dataListUrl, this.state, true);
        } else {
            this.setState({[name]: nextSort}, () => {
                this.props.reloadData(dataListUrl, this.state, true);
            });
        }

    }

    handleClickPage = (page) => {
        this.setState({
            page: page
        },() => {
            this.props.reloadData(dataListUrl, this.state, true);
        });
    }

    render() {
        let dataSource = this.props.dataSource;
        let index = 0;
        let totalItems = 0;
        let itemsPerPage = 20;
        if (!_.isEmpty(dataSource)) {
            index = dataSource.data.from - 1;
            totalItems   = dataSource.data.total;
            itemsPerPage = dataSource.data.per_page;
        }

        return ( (!_.isEmpty(dataSource)) &&
            <div>
                <Loading className="loading" />
                <div className="box">
                    <div className="box-header">
                        <div className="row">
                                <div className="col-md-12">
                                    <h4>{trans('messages.search_title')}</h4>
                                </div>
                                <FORM>
                                    <INPUT
                                        name="type"
                                        groupClass="form-group col-md-2-25"
                                        labelTitle={trans('messages.reason_type')}
                                        onChange={this.handleChange}
                                        value={this.state.type||''}
                                        className="form-control input-sm" />
                                    <INPUT
                                        name="reason_content"
                                        groupClass="form-group col-md-2-5"
                                        labelTitle={trans('messages.reason_content')}
                                        onChange={this.handleChange}
                                        value={this.state.reason_content||''}
                                        className="form-control input-sm" />
                                    <SELECT
                                        name="per_page"
                                        groupClass="form-group col-md-1 col-md-offset-6 per-page pull-right-md"
                                        labelClass="pull-left"
                                        labelTitle={trans('messages.per_page')}
                                        onChange={this.handleChange}
                                        value={this.state.per_page||''}
                                        options={{20:20, 40:40, 60:60, 80:80, 100:100}}
                                        className="form-control input-sm" />
                                </FORM>
                        </div>
                        <div className="row">
                            <div className="col-md-9 col-xs-4">
                                <a href={saveUrl} className="btn btn-success input-sm">{trans('messages.btn_add')}</a>
                            </div>
                            <div className="col-md-3 col-xs-8">
                                <BUTTON className="btn btn-danger pull-right" onClick={this.handleClearClick}>{trans('messages.btn_reset')}</BUTTON>
                                <BUTTON className="btn btn-primary pull-right margin-element" onClick={this.handleReloadData} >{trans('messages.btn_search')}</BUTTON>
                            </div>
                        </div>
                    </div>
                    <div className="box-body">
                        <div className="row">
                            <div className="col-md-12">
                                {(totalItems/itemsPerPage > 1) &&
                                <div className="row">
                                    <div className="col-md-2-5"></div>
                                    <div className="col-md-7 text-center">
                                        <PaginationContainer handleClickPage={this.handleClickPage}/>
                                    </div>
                                    <div className="col-md-2-5 text-right line-height-md">
                                        {dataSource.data.from}件 - {dataSource.data.to}件 【全{dataSource.data.total}件】
                                    </div>
                                </div>
                                }
                                <TABLE className="table table-striped table-custom">
                                    <THEAD>
                                        <TR>
                                            <TH className="visible-xs visible-sm text-center"></TH>
                                            <TH className="visible-xs visible-sm text-center  col-max-min-60">
                                                <SortComponent
                                                    name="sort_reason_id"
                                                    onClick={this.handleSortClick}
                                                    currentSort={(this.state.sort_reason_id)?this.state.sort_reason_id:'none'}
                                                    title={trans('messages.reason_id')} />
                                            </TH>
                                            <TH className="visible-xs visible-sm text-center  col-max-min-60">
                                                <SortComponent
                                                    name="sort_reason_content"
                                                    onClick={this.handleSortClick}
                                                    currentSort={(this.state.sort_reason_content)?this.state.sort_reason_content:'none'}
                                                    title={trans('messages.reason_content')} />
                                            </TH>
                                            <TH className="hidden-xs hidden-sm text-center">
                                                <SortComponent
                                                    name="sort_reason_id"
                                                    onClick={this.handleSortClick}
                                                    currentSort={(this.state.sort_reason_id)?this.state.sort_reason_id:'none'}
                                                    title={trans('messages.reason_id')} />
                                            </TH>
                                            <TH className="hidden-xs hidden-sm text-center">
                                                <SortComponent
                                                    name="sort_type"
                                                    onClick={this.handleSortClick}
                                                    currentSort={(this.state.sort_type)?this.state.sort_type:'none'}
                                                    title={trans('messages.reason_type')} />
                                            </TH>
                                            <TH className="hidden-xs hidden-sm text-center">
                                                <SortComponent
                                                    name="sort_reason_content"
                                                    onClick={this.handleSortClick}
                                                    currentSort={(this.state.sort_reason_content)?this.state.sort_reason_content:'none'}
                                                    title={trans('messages.reason_content')} />
                                            </TH>
                                            <TH className="hidden-xs hidden-sm text-center">
                                                <SortComponent
                                                    name="sort_add_2_mail_text"
                                                    onClick={this.handleSortClick}
                                                    currentSort={(this.state.sort_add_2_mail_text)?this.state.sort_add_2_mail_text:'none'}
                                                    title={trans('messages.add_2_mail_text')} />
                                            </TH>
                                        </TR>
                                    </THEAD>
                                    <TBODY>
                                        {
                                            (!_.isEmpty(dataSource.data.data))?
                                            dataSource.data.data.map((item) => {
                                                index++;
                                                let classRow = (index%2 ===0) ? 'odd' : 'even';
                                                return (
                                                    [
                                                    <TR key={"tr" + index} className={classRow}>
                                                        <TD className="visible-xs visible-sm text-center">
                                                            <b className="show-info">
                                                                <i className="fa fa-angle-double-down" aria-hidden="true"></i>
                                                            </b>
                                                        </TD>
                                                        <TD className="visible-xs visible-sm text-center">
                                                            <a className="link-detail" href={saveUrl + "?reason_id=" + item.reason_id}>
                                                                {item.reason_id}
                                                            </a>
                                                        </TD>
                                                        <TD className="visible-xs visible-sm cut-text"  title={item.reason_content}>
                                                            <span className="limit-char max-width-100">{item.reason_content}</span>
                                                        </TD>
                                                        <TD className="hidden-xs hidden-sm text-center">
                                                            <a className="link-detail" href={saveUrl + "?reason_id=" + item.reason_id}>
                                                                {item.reason_id}
                                                            </a>
                                                        </TD>
                                                        <TD className="hidden-xs hidden-sm cut-text" title={item.type}>
                                                            <span className="limit-char max-width-350">{item.type}</span>
                                                        </TD>
                                                        <TD className="hidden-xs hidden-sm cut-text" title={item.reason_content}>
                                                            <span className="limit-char max-width-350">{item.reason_content}</span>
                                                        </TD>
                                                        <TD className="hidden-xs hidden-sm cut-text" title={item.add_2_mail_text}>
                                                            <span className="limit-char max-width-350">{item.add_2_mail_text}</span>
                                                        </TD>
                                                    </TR>,
                                                    <TR key={"trhide" + index} className="hiden-info" style={{display:"none"}}>
                                                        <TD colSpan="3" className="visible-xs visible-sm width-span1">
                                                            <ul id={"temp" + item.reason_id}>
                                                                <li>
                                                                    <b>{trans('messages.reason_id')} : </b>
                                                                    {item.reason_id}
                                                                </li>
                                                                <li>
                                                                    <b>{trans('messages.reason_type')} : </b>
                                                                    {item.type}
                                                                </li>
                                                                <li>
                                                                    <b>{trans('messages.reason_content')} : </b>
                                                                    {item.reason_content}
                                                                </li>
                                                                <li>
                                                                    <b>{trans('messages.add_2_mail_text')} : </b>
                                                                    {item.add_2_mail_text}
                                                                </li>
                                                            </ul>
                                                        </TD>
                                                    </TR>
                                                   ]
                                                )
                                            })
                                            :
                                            <TR><TD colSpan="9" className="text-center">{trans('messages.no_data')}</TD></TR>
                                        }
                                    </TBODY>
                                </TABLE>
                                <div className="text-center">
                                    <PaginationContainer handleClickPage={this.handleClickPage}/>
                                </div>
                            </div>
                        </div>
                    </div>
                    </div>
                </div>

        )
    }
}

const mapStateToProps = (state) => {
    return {
        dataSource: state.commonReducer.dataSource
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        reloadData: (url, params, pushHistory, ignoreFields) => { dispatch(Action.reloadData(url, params, pushHistory, ignoreFields)) }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(CancelReasonList)