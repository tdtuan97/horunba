import React, { Component } from 'react';

import { connect } from 'react-redux';
import * as Action from '../common/actions';
import * as queryString from 'query-string';
import moment from 'moment';
import BUTTON from '../common/components/form/BUTTON';
import FORM from '../common/components/form/FORM';
import INPUT from '../common/components/form/INPUT';
import SELECT from '../common/components/form/SELECT';
import DATEPICKER from '../common/components/form/DATEPICKER';

import TABLE from '../common/components/table/TABLE';
import THEAD from '../common/components/table/THEAD';
import TBODY from '../common/components/table/TBODY';
import TR from '../common/components/table/TR';
import TH from '../common/components/table/TH';
import TD from '../common/components/table/TD';
import PaginationContainer from '../common/containers/PaginationContainer'
import SortComponent from '../common/components/table/SortComponent';

import Loading from '../common/components/common/Loading';

class NotPaymentList extends Component {

    constructor(props) {
        super(props);
        const stringParams = queryString.parse(props.location.search);
        this.state = {};
        if (!_.isEmpty(stringParams)) {
            this.state = stringParams;
        }
        this.props.reloadData(dataListUrl, stringParams);
    }

    handleChange = (event) => {
        const target = event.target;
        const value = target.type === 'checkbox' ? target.checked : target.value;
        const name = target.name;
        this.setState({
            [name]: (value)?value:undefined
        },() => {
            if (name === 'per_page') {
                this.handleReloadData();
            }
        });
    }

    handleChangeMultiSelect = (name, value, checked) => {
        let currentValue = this.state[name];
        if (typeof currentValue === 'undefined') {
            currentValue = [];
        } else if (!Array.isArray(this.state[name])) {
            currentValue = [this.state[name]];
        }
        if (Array.isArray(value)) {
            currentValue = [...value];
        } else {
            let index = currentValue.indexOf(value);
            if (checked === false) {
                if (index !== -1) {
                    currentValue.splice(index, 1);
                }
            } else {
                if (index === -1) {
                    currentValue.push(value);
                }
            }
        }
        this.setState({
            [name]: currentValue,
        });
    }

    handleChangeDate = function(date, name, event) {
        let dateValue = moment(date);
        if (!dateValue.isValid()) {
            dateValue = undefined;
        } else {
            dateValue = dateValue.format('YYYY-MM-DD');
        }
        this.setState({
          [name]: dateValue,
        });
    }

     handleClearClick = (event) => {
        Object.keys(this.state).map((key) => {
            if (key !== 'per_page') {
                delete this.state[key];
            }
        });
        this.handleReloadData();
    }

    handleReloadData = () => {
        if (this.state.page) {
            delete this.state.page;
        }
        this.props.reloadData(dataListUrl, this.state, true);
    }

    handleSortClick = (name, nextSort) => {
        Object.keys(this.state).map((key) => {
            if (key.startsWith('sort_')) {
                delete this.state[key];
            }
        });
        if (nextSort === 'none') {
            delete this.state[name];
            this.props.reloadData(dataListUrl, this.state, true);
        } else {
            this.setState({[name]: nextSort}, () => {
                this.props.reloadData(dataListUrl, this.state, true);
            });
        }
    }

    handleClickPage = (page) => {
        this.setState({
            page: page
        },() => {
            this.props.reloadData(dataListUrl, this.state, true);
        });
    }

    componentDidUpdate() {
        if (!_.isEmpty(this.props.dataSource)) {
            $('.loading').hide();
        }
    }

    render() {
        let nf = new Intl.NumberFormat();
        let dataSource = this.props.dataSource;
        let index = 0;
        let totalItems = 0;
        let itemsPerPage = 20;
        if (!_.isEmpty(dataSource)) {
            index = dataSource.data.from - 1;
            totalItems   = dataSource.data.total;
            itemsPerPage = dataSource.data.per_page;
        }
        return ( (!_.isEmpty(dataSource)) &&
            <div>
                <Loading className="loading" />
                <div className="box">
                    <div className="box-header">
                        <div className="row">
                            <div className="col-md-12">
                                <h4>{trans('messages.search_title')}</h4>
                            </div>
                            <FORM>
                                <SELECT
                                    id="mall_id"
                                    name="mall_id"
                                    groupClass="form-group col-md-1-5"
                                    labelTitle={trans('messages.name_jp')}
                                    handleChangeMultiSelect={this.handleChangeMultiSelect}
                                    valueMulti={this.state.mall_id || ''}
                                    options={dataSource.mallData}
                                    multiple="multiple"
                                    className="form-control input-sm" />
                                <DATEPICKER
                                    groupClass="form-group col-md-1-25 clear-padding-right"
                                    labelTitle={trans('messages.order_date_from')}
                                    placeholderText="YYYY-MM-DD"
                                    dateFormat="YYYY-MM-DD"
                                    locale="ja"
                                    name="order_date_from"
                                    className="form-control input-sm"
                                    selected={this.state.order_date_from ? moment(this.state.order_date_from) : null}
                                    onChange={(date, name, e) => this.handleChangeDate(date, 'order_date_from', e)} />
                                <div className="col-md-0-25 hidden-xs hidden-sm"><span className="to-from-child">～</span></div>
                                <DATEPICKER
                                    groupClass="form-group col-md-1-25 clear-padding-left"
                                    labelTitle={trans('messages.order_date_to')}
                                    placeholderText="YYYY-MM-DD"
                                    dateFormat="YYYY-MM-DD"
                                    locale="ja"
                                    name="order_date_to"
                                    className="form-control col-md-12 input-sm"
                                    selected={this.state.order_date_to ? moment(this.state.order_date_to) : null}
                                    onChange={(date, name, e) => this.handleChangeDate(date, 'order_date_to', e)} />
                                <INPUT
                                    name="received_order_id"
                                    groupClass="form-group col-md-1-5"
                                    labelTitle={trans('messages.received_order_id')}
                                    onChange={this.handleChange}
                                    value={this.state.received_order_id || ''}
                                    className="form-control input-sm" />
                                <INPUT
                                    name="full_name"
                                    groupClass="form-group col-md-1-5"
                                    labelTitle={trans('messages.order_name')}
                                    onChange={this.handleChange}
                                    value={this.state.full_name || ''}
                                    className="form-control input-sm" />
                                <INPUT
                                    name="full_name_kana"
                                    groupClass="form-group col-md-2"
                                    labelTitle={trans('messages.full_name_kana')}
                                    onChange={this.handleChange}
                                    value={this.state.full_name_kana || ''}
                                    className="form-control input-sm" />
                                <INPUT
                                    name="payment_account"
                                    groupClass="form-group col-md-1-25"
                                    labelTitle={trans('messages.payment_account_num')}
                                    onChange={this.handleChange}
                                    value={this.state.payment_account || ''}
                                    className="form-control input-sm" />
                                 <INPUT
                                    name="request_price"
                                    groupClass="form-group col-md-1-25"
                                    labelTitle={trans('messages.request_price')}
                                    onChange={this.handleChange}
                                    value={this.state.request_price || ''}
                                    className="form-control input-sm" />
                                <SELECT
                                    name="per_page"
                                    groupClass="form-group col-md-1 per-page pull-right-md"
                                    labelClass="pull-left"
                                    labelTitle={trans('messages.per_page')}
                                    onChange={this.handleChange}
                                    value={this.state.per_page || ''}
                                    options={{20:20, 40:40, 60:60, 80:80, 100:100}}
                                    className="form-control input-sm" />
                            </FORM>
                        </div>
                        <div className="row">
                            <div className="col-md-9">
                            </div>
                            <div className="col-md-3">
                                    <BUTTON className="btn btn-danger pull-right" onClick={this.handleClearClick}>{trans('messages.btn_reset')}</BUTTON>
                                    <BUTTON className="btn btn-primary pull-right margin-element" onClick={this.handleReloadData} >{trans('messages.btn_search')}</BUTTON>
                            </div>
                        </div>
                    </div>
                    <div className="box-body">
                        <div className="dataTables_wrapper form-inline dt-bootstrap">
                            <div className="row">
                                <div className="col-sm-12">
                                    {(totalItems/itemsPerPage > 1) &&
                                    <div className="row">
                                        <div className="col-md-2-5"></div>
                                        <div className="col-md-7 text-center">
                                            <PaginationContainer handleClickPage={this.handleClickPage}/>
                                        </div>
                                        <div className="col-md-2-5 text-right line-height-md">
                                            {dataSource.data.from}件 - {dataSource.data.to}件 【全{dataSource.data.total}件】
                                        </div>
                                    </div>
                                    }
                                    <TABLE className="table table-striped table-custom">
                                        <THEAD>
                                            <TR>
                                                <TH className=" col-max-min-60 visible-xs visible-sm text-middle"></TH>
                                                <TH className=" col-max-min-60 text-center text-middle">＃</TH>
                                                <TH className=" col-max-min-60 text-center text-middle">
                                                    <SortComponent
                                                        name="sort_mall_id"
                                                        onClick={this.handleSortClick}
                                                        currentSort={(this.state.sort_mall_id)?this.state.sort_mall_id:'none'}
                                                        title={trans('messages.name_jp')} />

                                                </TH>
                                                <TH className=" col-max-min-60 text-center text-middle">
                                                    <SortComponent
                                                        name="sort_order_date"
                                                        onClick={this.handleSortClick}
                                                        currentSort={(this.state.sort_order_date)?this.state.sort_order_date:'none'}
                                                        title={trans('messages.order_date_1')} />

                                                </TH>
                                                <TH className=" col-max-min-120 text-center  text-middle">
                                                    <SortComponent
                                                        name="sort_received_order_id"
                                                        onClick={this.handleSortClick}
                                                        currentSort={(this.state.sort_received_order_id)?this.state.sort_received_order_id:'none'}
                                                        title={trans('messages.received_order_id')} />

                                                </TH>
                                                <TH className=" col-max-min-120 text-center  text-middle hidden-xs hidden-sm">
                                                    {trans('messages.full_name')}
                                                </TH>
                                                <TH className=" col-max-min-200 text-center  text-middle hidden-xs hidden-sm">
                                                    {trans('messages.full_name_kana_col')}
                                                </TH>
                                                <TH className=" col-max-min-100 text-center  text-middle hidden-xs hidden-sm">
                                                    {trans('messages.payment_name')}
                                                </TH>
                                                <TH className=" col-max-min-100 text-center  text-middle hidden-xs hidden-sm">
                                                    {trans('messages.payment_account_num')}
                                                </TH>
                                                <TH className=" col-max-min-100 text-center  text-middle hidden-xs hidden-sm">
                                                    {trans('messages.request_price')}
                                                </TH>
                                            </TR>
                                        </THEAD>
                                        <TBODY>
                                            {
                                                (!_.isEmpty(dataSource.data.data))?
                                                dataSource.data.data.map((item) => {
                                                    index++;
                                                    let classRow = (index%2 ===0) ? 'odd' : 'even';
                                                    let orderDate = item.order_date ? moment.utc(item.order_date, "YYYY/MM/DD hh:mm:ss").format("YYYY/MM/DD hh:mm:ss") : '';
                                                    return (
                                                        [<TR key={"tr" + index} className={classRow}>
                                                            <TD className="visible-xs visible-sm text-center">
                                                                <b className="show-info">
                                                                    <i className="fa fa-angle-double-down" aria-hidden="true"></i>
                                                                </b>
                                                            </TD>
                                                            <TD className="text-center">
                                                                {index}
                                                            </TD>
                                                            <TD title={item.name_jp}>
                                                                {item.name_jp}
                                                            </TD>
                                                            <TD title={orderDate}>
                                                                {orderDate}
                                                            </TD>
                                                            <TD title={item.received_order_id}>
                                                                {item.received_order_id}
                                                            </TD>
                                                            <TD className="hidden-xs hidden-sm" title={item.full_name}>
                                                                {item.full_name}
                                                            </TD>
                                                            <TD className="hidden-xs hidden-sm" title={item.full_name_kana}>
                                                                {item.full_name_kana}
                                                            </TD>
                                                            <TD className="hidden-xs hidden-sm" title={item.payment_name}>
                                                                {item.payment_name}
                                                            </TD>
                                                            <TD className="hidden-xs hidden-sm" title={item.payment_account}>
                                                                {item.payment_account}
                                                            </TD>
                                                            <TD className="hidden-xs hidden-sm text-right" title={nf.format(item.request_price)}>
                                                                {nf.format(item.request_price)}
                                                            </TD>
                                                        </TR>,
                                                        <TR key={"trhide" + index} className="hiden-info" style={{display:"none"}}>
                                                            <TD colSpan="9" className="width-span1">
                                                                <ul id={"temp" + index} >
                                                                    <li><b>{trans('messages.name_jp')}</b> : {item.name_jp}</li>
                                                                    <li><b>{trans('messages.order_date_1')}</b> : {orderDate}</li>
                                                                    <li><b>{trans('messages.received_order_id')}</b> : {item.received_order_id}</li>
                                                                    <li><b>{trans('messages.full_name')}</b> : {item.full_name}</li>
                                                                    <li><b>{trans('messages.full_name_kana_col')}</b> : {item.full_name_kana}</li>
                                                                    <li><b>{trans('messages.payment_name')}</b> : {item.payment_name}</li>
                                                                    <li><b>{trans('messages.payment_account_num')}</b> : {item.payment_account}</li>
                                                                    <li><b>{trans('messages.request_price')}</b> : {nf.format(item.request_price)}</li>
                                                                </ul>
                                                            </TD>
                                                        </TR>
                                                    ]
                                                    )
                                                }):
                                                <TR><TD colSpan="9" className="text-center">{trans('messages.no_data')}</TD></TR>
                                            }
                                        </TBODY>
                                    </TABLE>
                                    <PaginationContainer handleClickPage={this.handleClickPage}/>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        dataSource: state.commonReducer.dataSource,
        dataResponse: state.commonReducer.dataResponse
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        reloadData: (url, params, pushHistory, ignoreFields) => { dispatch(Action.reloadData(url, params, pushHistory, ignoreFields)) },
        postData: (url, params, headers,callback,callParam) => { dispatch(Action.postData(url, params, headers,callback,callParam)) }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(NotPaymentList)