import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';

import { connect } from 'react-redux';
import * as Action from '../common/actions';
import * as queryString from 'query-string';

import BUTTON from '../common/components/form/BUTTON';
import FORM from '../common/components/form/FORM';
import INPUT from '../common/components/form/INPUT';
import SELECT from '../common/components/form/SELECT';
import CHECKBOX_CUSTOM from '../common/components/form/CHECKBOX_CUSTOM';

import TABLE from '../common/components/table/TABLE';
import THEAD from '../common/components/table/THEAD';
import TBODY from '../common/components/table/TBODY';
import TR from '../common/components/table/TR';
import TH from '../common/components/table/TH';
import TD from '../common/components/table/TD';
import DATEPICKER from '../common/components/form/DATEPICKER';

import PopupRemark from './include/PopupRemark';
import AcceptPopup from './include/AcceptPopup';
import FaxConfirmPopup from './include/FaxConfirmPopup';
import FaxFormPopup from './include/FaxFormPopup';
import PopupConfirmCreateOrder from './include/PopupConfirmCreateOrder';

import PaginationContainerNew from '../common/containers/PaginationContainerNew';
import axios from 'axios';

import Loading from '../common/components/common/Loading';
import moment from 'moment';

class OrderToSupplierList extends Component {

    constructor(props) {
        super(props);
        const stringParams = queryString.parse(props.location.search);
        this.state = {};
        if (!_.isEmpty(stringParams)) {
            this.state = stringParams;
        }
        this.props.reloadData(dataListUrl, stringParams);
    }
    handleChangeMultiSelect = (name, value, checked) => {
        let currentValue = this.state[name];
        if (typeof currentValue === 'undefined') {
            currentValue = [];
        } else if (!Array.isArray(this.state[name])) {
            currentValue = [this.state[name]];
        }
        if (Array.isArray(value)) {
            currentValue = [...value];
        } else {
            let index = currentValue.indexOf(value);
            if (checked === false) {
                if (index !== -1) {
                    currentValue.splice(index, 1);
                }
            } else {
                if (index === -1) {
                    currentValue.push(value);
                }
            }
        }
        if (name === 'order_type') {
            if (currentValue.indexOf("1") !== -1 && currentValue.indexOf("2") === -1) {
                currentValue.push("2");
            } else if (currentValue.indexOf("1") === -1 && currentValue.indexOf("2") !== -1) {
                currentValue.splice(currentValue.indexOf("2"), 1);
            }
        } else if (name === 'delivery_type') {
            if (currentValue.indexOf("1") !== -1 && currentValue.indexOf("3") === -1) {
                currentValue.push("3");
                currentValue.push("4");
            } else if (currentValue.indexOf("1") === -1 && currentValue.indexOf("3") !== -1) {
                currentValue.splice(currentValue.indexOf("3"), 1);
                currentValue.splice(currentValue.indexOf("4"), 1);
            }
        }

        this.setState({
            [name]: currentValue,
        });
    }
    handleChange = (event) => {
        const target = event.target;
        const value = target.type === 'checkbox' ? target.checked : target.value;
        const name = target.name;
        this.setState({
            [name]: (value)?value:undefined
        },() => {
            if (name === 'per_page' ) {
                this.handleReloadData();
            }
            if (name === 'order_type' ) {
                this.handleReloadData();
            }
        });
    }

     handleClearClick = (event) => {
        Object.keys(this.state).map((key) => {
            if (key !== 'per_page') {
                delete this.state[key];
            }
        });
        this.handleReloadData();
    }

    handleReloadData = () => {
        if (this.state.page) {
            delete this.state.page;
        }
        this.props.reloadData(dataListUrl, this.state, true);
    }

    handleClickPage = (page) => {
        this.setState({
            page: page
        },() => {
            this.props.reloadData(dataListUrl, this.state, true);
        });
    }

    handleChangeSelect = (event) => {
        const target = event.target;
        const value = target.value;
        const name = target.name;
        let strDatas = target.dataset.target;
        let arrDatas = strDatas.split(",");
        let params = {name: name, order_code: arrDatas[0], edi_order_code: arrDatas[1], [name]:value};

        this.props.postData(changeStatusUrl, params,
                    {'X-Requested-With': 'XMLHttpRequest'},
                    this.props.reloadData, [dataListUrl, this.state, true]
                    );
    }
    handleIsCancel = (orderCode, ediOrderCode, isCancel, orderType, mOrderTypeId, event) => {
        let params  = {
            order_code : orderCode,
            order_type : orderType,
            edi_order_code : ediOrderCode,
            is_cancel   : isCancel,
            m_order_type_id : mOrderTypeId,
        }

        this.props.postData(changeStatusUrl, params,
                            {'X-Requested-With': 'XMLHttpRequest'},
                            this.props.reloadData, [dataListUrl, this.state, true]
                            );
    }
    handlePopupConfirmNewOrder = (orderCode, ediOrderCode, orderType, productCode, stateFlg, productStatus, event) => {

        let params  = {
            order_code : orderCode,
            state_flg  : stateFlg,
            edi_order_code : ediOrderCode,
            order_type : orderType,
            product_code : productCode,
            product_status : productStatus,
        }

        this.setState({IsAvailable : params}, () => {
            if(orderType != 3 && orderType != 4 && productStatus != 15){
                $('#modal-confirm-new-order').modal('show');
            }else{
                this.handleIsAvailable(orderCode,ediOrderCode,orderType,productCode,stateFlg,productStatus,1,event);
            }
        })
    }

    handleIsAvailable = (orderCode, ediOrderCode, orderType, productCode, stateFlg, productStatus, flgReOrder, event) => {

        let params  = {
            order_code : orderCode,
            state_flg  : stateFlg,
            edi_order_code : ediOrderCode,
            order_type : orderType,
            product_code : productCode,
            product_status : productStatus,
            flg_re_order : flgReOrder,
        }
        $('#modal-confirm-new-order').modal('hide');
        this.props.postData(changeStatusUrl, params,
                            {'X-Requested-With': 'XMLHttpRequest'},
                            this.props.reloadData, [dataListUrl, this.state, true]
                            );
    }

    handleGetData = (name) => {
        Object.keys(this.state).map((key) => {
            delete this.state[key];
        });
        let params = {[name]: true};
        this.setState(params, () => {
            this.props.reloadData(dataListUrl, params, true);
        })
    }

    componentDidUpdate() {
        if (!_.isEmpty(this.props.dataSource)) {
            $('.loading').hide();
        }
        dragscroll.reset();
        $("table th").resizeble();
        let _this = this;
        $('#modal-confirm-new-order').on('hidden.bs.modal', function () {
            _this.setState({IsAvailable: {}});
        });
    }

    handleRemarkSubmit = () => {
        this.props.postData(changeStatusUrl, {...this.state.dataRemark},
            {'X-Requested-With': 'XMLHttpRequest'},
                this.handleRemarkCancel
            );
    }

    handleRemarkCancel = () => {
        delete this.state.dataRemark;
        this.props.reloadData(dataListUrl, this.state,true);
        $('#issue-modal').modal('hide');
    }

    handleRemarkChange = (event) => {
        let target = event.target;
        let name = target.name;
        let value = target.value;
        let currentValue = {...this.state.dataRemark};
        currentValue[name] = value;
        this.setState({dataRemark: currentValue});
    }

    handleExportCsv = () => {
        $(".loading").show();
        let param = {...this.state};
        param['type'] = 'order-to-supplier';
        let headers = {'X-Requested-With': 'XMLHttpRequest'};
        axios.post(processExportCSV, param, {headers : headers}
        ).then(response => {
            $(".loading").hide();
            if (response.data.status === 1) {
                window.location.href = exportCSV +"?fileName="+ response.data.file_name;
            }
        }).catch(error => {
            $(".loading").hide();
            throw(error);
        });
    }

    handleClick = (key, e) => {
        let currentValue = {...this.state};
        let arrDatas = key.split(",");
        currentValue.dataRemark = {order_code: arrDatas[0], edi_order_code: arrDatas[1], remarks: arrDatas[2]};
        this.setState({...currentValue}, () => {
            $('#issue-modal').modal({backdrop: 'static', show: true});
        });
    }
    handleChangeDate = function(date, name, event) {
        let dateValue = moment(date);
        if (!dateValue.isValid()) {
            dateValue = undefined;
        } else {
            dateValue = dateValue.format('YYYY-MM-DD');
        }
        this.setState({
          [name]: dateValue,
        });
    }

    handleBtnResale = (orderCode, productCode, event) => {
        let params  = {
            order_code : orderCode,
            product_code  : productCode
        }
        this.props.postData(
            reSaleUrl,
            params,
            {'X-Requested-With': 'XMLHttpRequest'},
            this.props.reloadData, [dataListUrl, this.state, true]
        );
    }

    handleBtnReturn = (productCode, orderNum, orderCode, ediOrderCode, priceInvoice, processStatus, event) => {
        // window.location.href = saveReturnProductUrl + "?product_code=" + productCode + "&order_num=" + orderNum + "&order_code=" + orderCode;
        let params  = {
            order_num : orderNum,
            order_code : orderCode,
            edi_order_code : ediOrderCode,
            product_code  : productCode,
            price_invoice : priceInvoice,
            process_status : processStatus
        }
        this.props.postData(
            returnUrl,
            params,
            {'X-Requested-With': 'XMLHttpRequest'},
            this.props.reloadData, [dataListUrl, this.state, true]
        );

    }
    handleRemarkAllChange = (event) => {
        let target = event.target;
        let name = target.name;
        let value = target.value;
        this.setState({process_value: value});
    }
    handleAccept = (event) => {
        const target = event.target;
        let value = target.value;
        let name = target.name;
        this.setState({ process_name :  name ,  process_value: (value)? value:null });
        $('#access-memo-modal').modal({backdrop: 'static', show: true});

    }
    handleCancelAccept = () => {
        delete this.state.remarks_all;
        this.setState({
            process_name : null,
            process_value : null
        }, () => {
             $('#access-memo-modal').modal('hide');
        })
    }
    handleProcessAll = () => {
        this.setState({'process_all_flag' : 1},() => {
            this.props.postData(
                prepareProcessAllUrl,
                this.state,
                {'X-Requested-With': 'XMLHttpRequest'},
                this.props.reloadData, [dataListUrl, this.state, true]
            );
        });

        $('#access-memo-modal').modal('hide');
        $(".loading").hide();
    }
    componentWillReceiveProps(nextProps) {
        if (!_.isEmpty(nextProps.dataSource)) {
            if (!_.isEmpty(nextProps.dataSource.data)) {
                let data = nextProps.dataSource;
                this.setState({process_all_flag : data.processAllFlag});
            }
        }
    }
    processFaxOrder = (ediOrderCode, productCode) => {
        this.setState({fax_order_code: ediOrderCode, fax_product_code: productCode},() => {
            $('#fax-confirm-popup').modal({backdrop: 'static', show: true});
        });
    }
    handleChooseFaxType = (type, data) => {
        if (data) {
            $('#fax-confirm-popup').modal('hide');
            let name = '';
            let fax  = '';
            let key_cd = '';
            let is_supplier = 0;
            if (type === 'maker') {
                name = data.maker_full_nm;
                fax = data.maker_fax;
                key_cd = data.maker_cd;
            } else if (type === 'supplier') {
                name = data.supplier_nm;
                fax = data.supplier_fax;
                key_cd = data.supplier_cd;
                is_supplier = 1;
            }
            this.setState({
                cur_edi_order_code: '',
                cur_product_name_long: data.product_name_long,
                cur_name: name,
                cur_fax: fax,
                cur_key_cd: key_cd,
                cur_is_supplier: is_supplier,
                cur_login_user: data.login_user,
                cur_index: data.index
            }, () => {
                $('#fax-form-popup').modal({backdrop: 'static', show: true});
            });
        } else {
            this.processItemFaxOrder(type);
        }
    }

    processItemFaxOrder = (type) => {
        $('#fax-confirm-popup').modal('hide');
        $(".loading").show();
        let headers = {'X-Requested-With': 'XMLHttpRequest'};
        axios.post(
            getFaxOrderUrl,
            {type: type, fax_order_code: this.state.fax_order_code, fax_product_code: this.state.fax_product_code},
            {headers : headers}
        ).then(response => {
            $(".loading").hide();
            if (response.data.flg === 1) {
                let data = response.data.msg;
                let name = '';
                let fax  = '';
                let key_cd = '';
                let is_supplier = 0;
                if (type === 'maker') {
                    name = data.maker_full_nm;
                    fax = data.maker_fax;
                    key_cd = data.maker_cd;
                } else if (type === 'supplier') {
                    name = data.supplier_nm;
                    fax = data.supplier_fax;
                    key_cd = data.supplier_cd;
                    is_supplier = 1;
                }
                this.setState({
                    cur_edi_order_code: data.edi_order_code,
                    cur_product_name_long: data.product_name_long,
                    cur_name: name,
                    cur_fax: fax,
                    cur_key_cd: key_cd,
                    cur_is_supplier: is_supplier,
                    cur_login_user: data.login_user,
                    cur_index: data.index
                }, () => {
                    $('#fax-form-popup').modal({backdrop: 'static', show: true});
                });
            } else if (typeof response.data.msg !== 'undefined') {
                $.notify({
                    icon: 'glyphicon glyphicon-warning-sign',
                    message: response.data.msg,
                },{
                    type: 'danger',
                    delay: 2000
                });
            }
        }).catch(error => {
            $(".loading").hide();
            throw(error);
        });
    }

    render() {
        let dataSource = this.props.dataSource;
        let index = 0;
        let totalItems = 0;
        let itemsPerPage = 20;
        if (!_.isEmpty(dataSource)) {
            index = dataSource.pageData.from - 1;
            totalItems   = dataSource.pageData.total;
            itemsPerPage = dataSource.pageData.per_page;
        }
        return ( (!_.isEmpty(dataSource)) &&
            <div>
                <Loading className="loading" />
                <div className="row">
                    <div className="col-xs-12">
                        <div className="box">
                            <div className="box-header">
                                <div className="row">
                                    <div className="col-md-12">
                                        <h4>{trans('messages.search_title')}</h4>
                                    </div>
                                        <FORM>
                                            <DATEPICKER
                                                groupClass="form-group col-md-1-5"
                                                labelTitle={trans('messages.order_date_ots')}
                                                placeholderText="YYYY-MM-DD"
                                                dateFormat="YYYY-MM-DD"
                                                locale="ja"
                                                name="order_date"
                                                className="form-control input-sm"
                                                key="order_date"
                                                id="order_date"
                                                selected={this.state.order_date ? moment(this.state.order_date) : null}
                                                onChange={(date, name, e) => this.handleChangeDate(date, 'order_date', e)} />
                                            <INPUT
                                                name="order_code"
                                                groupClass="form-group col-md-1-75"
                                                value={this.state.order_code || ''}
                                                onChange={this.handleChange}
                                                labelTitle={trans('messages.order_code_ots')}
                                                className="form-control input-sm" />
                                            <SELECT
                                                id="order_type"
                                                name="order_type"
                                                groupClass="form-group col-md-1-75"
                                                labelClass="pull-left"
                                                multiple="multiple"
                                                labelTitle={trans('messages.order_type')}
                                                handleChangeMultiSelect={this.handleChangeMultiSelect}
                                                valueMulti={this.state.order_type || ''}
                                                options={{1 : '客注' , 3 : '補充', 4: '臨時', 5 : '予定外'}}
                                                className="form-control input-sm" />
                                            <SELECT
                                                id="delivery_type"
                                                name="delivery_type"
                                                groupClass="form-group col-md-1-75"
                                                labelClass="pull-left"
                                                multiple="multiple"
                                                labelTitle={trans('messages.delivery_type')}
                                                handleChangeMultiSelect={this.handleChangeMultiSelect}
                                                valueMulti={this.state.delivery_type || ''}
                                                options={{1 : '南港' , 2 : '直送'}}
                                                className="form-control input-sm" />
                                            <INPUT
                                                name="product_code"
                                                groupClass="form-group col-md-1-75"
                                                value={this.state.product_code || ''}
                                                onChange={this.handleChange}
                                                labelTitle={trans('messages.product_code_ots')}
                                                className="form-control input-sm" />
                                            <INPUT
                                                name="product_jan"
                                                groupClass="form-group col-md-1-75"
                                                value={this.state.product_jan || ''}
                                                onChange={this.handleChange}
                                                labelTitle={trans('messages.jan_code_ots')}
                                                className="form-control input-sm" />
                                            <INPUT
                                                name="product_name"
                                                value={this.state.product_name || ''}
                                                onChange={this.handleChange}
                                                groupClass="form-group col-md-1-75"
                                                labelTitle={trans('messages.product_name')}
                                                className="form-control input-sm" />

                                            <div className="row">
                                                <div className="col-md-12">
                                                    <DATEPICKER
                                                        groupClass="form-group col-md-1-5"
                                                        labelTitle={trans('messages.edi_answer_date_o2s')}
                                                        placeholderText="YYYY-MM-DD"
                                                        dateFormat="YYYY-MM-DD"
                                                        locale="ja"
                                                        name="edi_delivery_date"
                                                        className="form-control input-sm"
                                                        key="edi_delivery_date"
                                                        id="edi_delivery_date"
                                                        selected={this.state.edi_delivery_date ? moment(this.state.edi_delivery_date) : null}
                                                        onChange={(date, name, e) => this.handleChangeDate(date, 'edi_delivery_date', e)} />
                                                    <DATEPICKER
                                                        groupClass="form-group col-md-1-75"
                                                        labelTitle={trans('messages.arrival_plan_date_o2s')}
                                                        placeholderText="YYYY-MM-DD"
                                                        dateFormat="YYYY-MM-DD"
                                                        locale="ja"
                                                        name="arrival_date_plan"
                                                        className="form-control input-sm"
                                                        key="arrival_date_plan"
                                                        id="arrival_date_plan"
                                                        selected={this.state.arrival_date_plan ? moment(this.state.arrival_date_plan) : null}
                                                        onChange={(date, name, e) => this.handleChangeDate(date, 'arrival_date_plan', e)} />
                                                    <DATEPICKER
                                                        groupClass="form-group col-md-1-75"
                                                        labelTitle={trans('messages.arrival_date_o2s')}
                                                        placeholderText="YYYY-MM-DD"
                                                        dateFormat="YYYY-MM-DD"
                                                        locale="ja"
                                                        name="arrival_date"
                                                        className="form-control input-sm"
                                                        key="arrival_date"
                                                        id="arrival_date"
                                                        selected={this.state.arrival_date ? moment(this.state.arrival_date) : null}
                                                        onChange={(date, name, e) => this.handleChangeDate(date, 'arrival_date', e)} />
                                                    <INPUT
                                                        name="supplier_name"
                                                        value={this.state.supplier_name || ''}
                                                        onChange={this.handleChange}
                                                        groupClass="form-group col-md-1-75"
                                                        labelTitle={trans('messages.supplier_name')}
                                                        className="form-control input-sm" />
                                                    <INPUT
                                                        name="maker_full_nm"
                                                        value={this.state.maker_full_nm || ''}
                                                        onChange={this.handleChange}
                                                        groupClass="form-group col-md-1-75"
                                                        labelTitle={trans('messages.maker_name')}
                                                        className="form-control input-sm" />
                                                    <SELECT
                                                        id="ope_status"
                                                        name="ope_status"
                                                        groupClass="form-group col-md-1-75"
                                                        multiple="multiple"
                                                        labelTitle={trans('messages.ope_status')}
                                                        handleChangeMultiSelect={this.handleChangeMultiSelect}
                                                        valueMulti={this.state.ope_status || ''}
                                                        options={dataSource.opeStatusS}
                                                        className="form-control input-sm" />
                                                    <SELECT
                                                        name="per_page"
                                                        groupClass="form-group col-md-2 per-page pull-right-md"
                                                        labelClass="pull-right"
                                                        labelTitle={trans('messages.per_page')}
                                                        onChange={this.handleChange}
                                                        value={this.state.per_page || ''}
                                                        options={{20:20, 40:40, 60:60, 80:80, 100:100}}
                                                        className="form-control input-sm" />
                                                </div>
                                            </div>
                                        </FORM>
                                </div>
                                <div className="row">
                                    <div className="col-md-8 col-xs-12 ">
                                        <a href={newUrl} className="btn btn-success pull-left margin-element margin-form">{trans('messages.btn_add_un_order')}</a>
                                        <BUTTON className="btn btn-success pull-left margin-element margin-form" onClick={() => this.processFaxOrder('', '')}>{trans('messages.btn_fax')}</BUTTON>
                                        <BUTTON className="btn btn-primary pull-left margin-element margin-form" onClick={(e) => this.handleExportCsv()}>{trans('messages.csv_export')}</BUTTON>
                                        <BUTTON className="btn btn-primary pull-left margin-element margin-form" onClick={(e) => this.handleGetData('delivery_delay')}>{trans('messages.btn_delivery_delay')}</BUTTON>
                                        <BUTTON className="btn btn-primary pull-left margin-element margin-form" onClick={(e) => this.handleGetData('receive_delay')}>{trans('messages.btn_receive_delay')}</BUTTON>
                                        <BUTTON className="btn btn-primary pull-left margin-element margin-form" onClick={(e) => this.handleGetData('receive_incomplete')} >{trans('messages.btn_receive_incomplete')}</BUTTON>
                                        <BUTTON className="btn btn-primary pull-left margin-element margin-form" onClick={(e) => this.handleGetData('note_check')} >{trans('messages.btn_note_check')}</BUTTON>
                                        <BUTTON className="btn btn-primary pull-left" onClick={(e) => this.handleGetData('not_answer')} >{trans('messages.btn_not_answer')}</BUTTON>
                                    </div>
                                    <div className="col-md-4 col-xs-12 margin-form">
                                            <BUTTON className="btn btn-danger pull-right" onClick={this.handleClearClick}>{trans('messages.btn_reset')}</BUTTON>
                                            <BUTTON className="btn btn-primary pull-right margin-element" onClick={this.handleReloadData} >{trans('messages.btn_search')}</BUTTON>
                                    </div>
                                </div>
                            </div>
                            <div className="box-body">
                                <div className="dataTables_wrapper form-inline dt-bootstrap">
                                    <div className="row">
                                        <div className="col-sm-12">
                                            {(totalItems/itemsPerPage > 1) &&
                                            <div className="row">
                                                <div className="col-md-2-5"></div>
                                                <div className="col-md-7 text-center">
                                                    <PaginationContainerNew handleClickPage={this.handleClickPage}/>
                                                </div>
                                                <div className="col-md-2-5 text-right line-height-md">
                                                    {dataSource.pageData.from}件 - {dataSource.pageData.to}件 【全{dataSource.pageData.total}件】
                                                </div>
                                            </div>
                                            }
                                            <div id="dragscroll" className="table-responsive">
                                                <TABLE className="table table-striped table-custom table-line-bottom" id="main-table">
                                                    <THEAD>
                                                        <TR>
                                                            <TH rowSpan="2" className="hidden-lg" ></TH>
                                                            <TH rowSpan="2" className="" ></TH>
                                                            <TH className="text-center text-middle" >
                                                                <BUTTON name="btn_resale_all" onClick={(e) => this.handleAccept(e) } className="btn input-sm btn-primary">{trans('messages.btn_resale_all')}</BUTTON>
                                                            </TH>
                                                            <TH className="text-center text-middle col-max-min-100">{trans('messages.order_date_ots')}</TH>
                                                            <TH className="text-center text-middle hidden-xs hidden-sm col-max-min-60" >{trans('messages.order_type')}</TH>
                                                            <TH className="text-center text-middle hidden-xs hidden-sm" >{trans('messages.product_code_ots')}</TH>
                                                            <TH rowSpan="2" className="col-max-min-100 text-center text-middle hidden-xs hidden-sm">{trans('messages.product_name')}</TH>
                                                            <TH className="text-center text-middle hidden-xs hidden-sm" >{trans('messages.price_invoice_ots')}</TH>
                                                            <TH className="text-center text-middle hidden-xs hidden-sm col-max-min-100" >{trans('messages.edi_answer_date_o2s')}</TH>
                                                            <TH className="text-center text-middle hidden-xs hidden-sm col-max-min-120">{trans('messages.arrival_plan_date_o2s')}</TH>
                                                            <TH className="text-center text-middle hidden-xs hidden-sm">{trans('messages.supplier_name')}</TH>
                                                            <TH className="text-center text-middle" width="5%">{trans('messages.edi_note')}</TH>
                                                            <TH className="text-center text-middle col-max-min-120">
                                                                {trans('messages.remarks')}
                                                            </TH>
                                                            <TH className="text-center text-middle" width="5%">
                                                                {trans('messages.receive_mail_incharge_person')}
                                                                    <SELECT
                                                                        name='ope_tantou_all'
                                                                        options={dataSource.tantou}
                                                                        onChange={(e) => this.handleAccept(e) }
                                                                        className={"form-control input-sm max-width-70 "} />
                                                            </TH>
                                                            <TH rowSpan="2" className="" ></TH>
                                                        </TR>
                                                        <TR>

                                                            <TH className="text-center text-middle">
                                                                <BUTTON name="btn_return_all" onClick={(e) => this.handleAccept(e) }   className="btn input-sm btn-primary">{trans('messages.btn_return_all')}</BUTTON>
                                                            </TH>
                                                            <TH className="text-center text-middle">{trans('messages.order_code_ots')}</TH>
                                                            <TH className="text-center text-middle hidden-xs hidden-sm">{trans('messages.delivery_type')}</TH>
                                                            <TH className="text-center text-middle hidden-xs hidden-sm">{trans('messages.jan_code_ots')}</TH>
                                                            <TH className="text-center text-middle hidden-xs hidden-sm">{trans('messages.quantity')}</TH>
                                                            <TH className="text-center text-middle hidden-xs hidden-sm">{trans('messages.in_stock_left')}</TH>
                                                            <TH className="text-center text-middle hidden-xs hidden-sm">{trans('messages.arrival_date_o2s')}</TH>
                                                            <TH className="text-center text-middle hidden-xs hidden-sm">{trans('messages.maker_name')}</TH>

                                                            <TH className="text-center text-middle">{trans('messages.reason_note')}</TH>
                                                            <TH className="text-center text-middle" >
                                                                <BUTTON name="btn_note_all" onClick={(e) => this.handleAccept(e) }  className="btn input-sm btn-primary">{trans('messages.btn_note_all')}</BUTTON>
                                                            </TH>
                                                            <TH className="text-center text-middle">
                                                                {trans('messages.ope_status')}
                                                                <SELECT
                                                                name='ope_status_all'
                                                                onChange={(e) => this.handleAccept(e) }
                                                                options={dataSource.opeStatus}
                                                                className={"form-control input-sm max-width-70 "} />
                                                            </TH>
                                                        </TR>
                                                    </THEAD>
                                                    <TBODY>
                                                        {
                                                            (!_.isEmpty(dataSource.data.data))?
                                                            dataSource.data.data.map((item) => {
                                                                index++;
                                                                let rowColor = (index%2 ===0) ? 'odd' : 'even';
                                                                let classRow = '';
                                                                let checkEdit = true;
                                                                if (item.is_cancel === 1 || item.is_confirmed === 1) {
                                                                    classRow = 'disabled ';
                                                                    checkEdit = false;
                                                                }
                                                                let orderType = (item.order_type === 1 || item.order_type === 2) ? '客注' : (item.order_type === 3) ? '補充' : (item.order_type === 5) ? '予定外' : '臨時';
                                                                let delayNote  = (typeof dataSource.delayNote[item.delay_note] !== 'undefined') ? dataSource.delayNote[item.delay_note] : item.delay_note;
                                                                let deliveryType = (item.order_type === 1 || item.order_type === 3 || item.order_type === 4 || item.order_type === 5) ? '南港' : '直送' ;
                                                                let check = false;
                                                                if (item.incomplete_quantity > 0) {
                                                                    check = true;
                                                                }
                                                                let url = '';
                                                                let m_order_type_id = '';
                                                                if (item.order_type === 2) {
                                                                    url = dataSource.url + '/edi/all_direct?s_order_code=';
                                                                    m_order_type_id = item.type_direct;
                                                                } else {
                                                                    url = dataSource.url + '/edi/all?s_order_code=';
                                                                    m_order_type_id = item.type_detail;
                                                                }
                                                                let keyO = item.order_code + ',' + item.edi_order_code + ',' + (item.remarks ? item.remarks : '');
                                                                let checkRe = true;
                                                                if (item.ope_status == 2) {
                                                                    checkRe = false;
                                                                }
                                                                let checkOpeStatus = true;
                                                                if (item.order_type == 5 && item.ope_status == 2) {
                                                                    checkOpeStatus = false;
                                                                }
                                                                let rowWarning = '';
                                                                if (item.is_lost === 1) {
                                                                    rowWarning = ' row-warning';
                                                                }
                                                                let nf = new Intl.NumberFormat();
                                                                return (
                                                                    [<TR key={"tr1" + index} className={classRow + ' ' + rowColor + rowWarning}>
                                                                        <TD rowSpan='2' className="width-span1 hidden-lg line-bottom text-middle text-center">
                                                                            <b className="show-info">
                                                                                <i className="fa fa-angle-double-down" aria-hidden="true"></i>
                                                                            </b>
                                                                        </TD>

                                                                        <TD className={"width-span1  text-center text-middle"}>
                                                                            <a target="_blank" href={url + item.edi_order_code}>
                                                                            <span>EDI</span>
                                                                            </a>
                                                                        </TD>

                                                                        <TD className={"width-span1  text-center text-middle"}>
                                                                        {(item.order_type == 5) &&
                                                                            (checkRe?<BUTTON onClick={(e) => this.handleBtnResale(item.order_code, item.product_code, e)} className="btn input-sm btn-info">{trans('messages.btn_resale')}</BUTTON>
                                                                            : <span disabled className="btn bg-purple" >{trans('messages.btn_resale')}</span>)
                                                                        }
                                                                        </TD>

                                                                        <TD className="text-middle" ><span className="limit-char ">{item.order_date}</span></TD>
                                                                        <TD className='text-middle hidden-xs hidden-sm text-left'><span>{orderType}</span></TD>
                                                                        <TD className='text-middle hidden-xs hidden-sm'><span>{item.product_code}</span></TD>
                                                                        <TD rowSpan='2' className="text-middle line-bottom hidden-xs hidden-sm"><span>{item.product_name}</span></TD>
                                                                        <TD className="text-right text-middle hidden-xs hidden-sm"><span>{item.price_invoice}</span></TD>
                                                                        <TD className="text-middle hidden-xs hidden-sm"><span>{item.edi_delivery_date}</span></TD>
                                                                        <TD className='hidden-xs hidden-sm text-middle'><span>{item.arrival_date_plan}</span></TD>
                                                                        <TD className='text-middle hidden-xs hidden-sm'><span>{item.supplier_nm}</span></TD>
                                                                        <TD className="text-middle text-left "><span>{item.order_note}</span></TD>
                                                                        <TD
                                                                            rowSpan='2'
                                                                            onClick={checkEdit ? (() => this.handleClick(keyO)) : ''}
                                                                            className="text-middle line-bottom hidden-xs hidden-sm click-edit-grid">
                                                                                <div className="box-remarks">
                                                                                    <div className="text-long-batch col-max-min-90">
                                                                                        {item.remarks}
                                                                                    </div>
                                                                                </div>
                                                                        </TD>
                                                                        <TD className="text-middle">
                                                                            <SELECT
                                                                            name='ope_tantou'
                                                                            data-target={item.order_code + ',' + item.edi_order_code}
                                                                            onChange={this.handleChangeSelect}
                                                                            value={item.ope_tantou || ''}
                                                                            options={dataSource.tantou}
                                                                            className={"form-control input-sm max-width-70 " + (!checkEdit ? 'disable_event' : '')} />

                                                                        </TD>
                                                                        <TD className="width-span1 text-center text-middle" rowSpan="2">
                                                                        { (item.process_status === 5 && item.order_type != 2) &&
                                                                            <BUTTON
                                                                            disabled={item.is_confirmed === 1 ? true : false}
                                                                            className="btn btn-success"
                                                                            onClick={(e) => this.handlePopupConfirmNewOrder(item.order_code, item.edi_order_code,item.order_type, item.product_code, 1, item.product_status, e)}
                                                                            >{trans('messages.btn_ots_correct')}</BUTTON>
                                                                        }
                                                                        </TD>

                                                                    </TR>,
                                                                    <TR key={"tr2" + index} className={classRow + " line-bottom " + rowColor + rowWarning}>
                                                                        {
                                                                            <TD className="width-span1 text-center text-middle">
                                                                                {(item.receive_id !== null && item.receive_id !== 0) &&
                                                                                [<a key={"link_to_od" + index} target="_blank" href={orderDetailLink + '?receive_id_key=' + item.receive_id}>
                                                                                <span>受</span>
                                                                                </a>,
                                                                                <br key={"br" + index} />
                                                                                ]}
                                                                                <button
                                                                                    className="btn btn-link"
                                                                                    onClick={() => this.processFaxOrder(item.edi_order_code, item.product_code)}>
                                                                                    <span>FAX</span>
                                                                                </button>

                                                                            </TD>
                                                                        }
                                                                        <TD className={"width-span1  text-center text-middle"}>
                                                                        {(item.order_type == 5) &&
                                                                            (checkRe?<BUTTON onClick={(e) => this.handleBtnReturn(item.product_code, item.order_num, item.order_code, item.edi_order_code, item.price_invoice, item.process_status, e)} className="btn input-sm btn-primary">{trans('messages.btn_return')}</BUTTON>
                                                                            : <span disabled className="btn bg-purple" >{trans('messages.btn_return')}</span>)
                                                                        }
                                                                        </TD>
                                                                        <TD className="text-left text-middle"><span className="limit-char">{item.order_code}</span></TD>
                                                                        <TD className="text-left text-middle hidden-xs hidden-sm"><span className='hidden-xs hidden-sm'>{deliveryType}</span></TD>
                                                                        <TD className="text-right text-middle hidden-xs hidden-sm"><span className='hidden-xs hidden-sm'>{item.product_jan}</span></TD>
                                                                        <TD className="text-right text-middle hidden-xs hidden-sm"><span>{item.order_num}</span></TD>
                                                                        <TD className="text-right text-middle hidden-xs hidden-sm">
                                                                            <span>
                                                                                { ( (item.process_status <= 5 && item.incomplete_quantity === 0 ) || item.incomplete_quantity === null ) ? '' : item.incomplete_quantity}
                                                                            </span>
                                                                        </TD>
                                                                        <TD className='hidden-xs text-middle hidden-sm'><span className="limit-char ">{item.arrival_date}</span></TD>
                                                                        <TD className='hidden-xs text-middle hidden-sm'><span className="limit-char">{item.maker_full_nm}</span></TD>
                                                                        <TD className="text-middle">
                                                                            <SELECT
                                                                            name='reason_note'
                                                                            data-target={item.order_code + ',' + item.edi_order_code}
                                                                            onChange={this.handleChangeSelect}
                                                                            value={item.reason_note || ''}
                                                                            options={dataSource.reasonNote}
                                                                            className={"form-control input-sm max-width-70 " + (!checkEdit ? 'disable_event' : '')} />
                                                                        </TD>
                                                                        <TD className="text-middle">
                                                                            <SELECT
                                                                            name='ope_status'
                                                                            data-target={item.order_code + ',' + item.edi_order_code}
                                                                            onChange={this.handleChangeSelect}
                                                                            value={nf.format(item.ope_status) || ''}
                                                                            options={dataSource.opeStatus}
                                                                            className={"form-control input-sm max-width-70 " + ((!checkEdit || !checkOpeStatus) ? 'disable_event' : '')} />
                                                                        </TD>
                                                                    </TR>,
                                                                    <TR key={"trhide" + index} className="hiden-info hidden-lg" style={{display:"none"}}>
                                                                        <TD colSpan="6" className="width-span1">
                                                                            <ul>
                                                                                <li> <b>#</b> : {index}</li>
                                                                                <li>
                                                                                    <a target="_blank" href={url + item.order_code}>
                                                                                        <span>EDI</span>
                                                                                    </a>
                                                                                </li>
                                                                                {
                                                                                    (item.receive_id !== null && item.receive_id !== 0) &&
                                                                                        <li>
                                                                                            <a target="_blank" href={orderDetailLink + '?receive_id_key=' + item.receive_id}>
                                                                                            <span>受注</span>
                                                                                            </a>
                                                                                        </li>
                                                                                }

                                                                                <li>
                                                                                    <b>{trans('messages.edi_status')} : </b>
                                                                                    {item.procesStatus}
                                                                                </li>
                                                                                <li>
                                                                                    <b>{trans('messages.delay_reason')} : </b>
                                                                                    <SELECT
                                                                                    name='delay_note'
                                                                                    onChange={(e) => this.handleChangeSelect()}
                                                                                    value={item.delay_note || 0 }
                                                                                    options={dataSource.reasonNote }
                                                                                    className={"form-control input-sm max-width-70 " + (!checkEdit ? 'disable_event' : '')} />
                                                                                </li>
                                                                                <li rowSpan='2'
                                                                                    onClick={checkEdit ? (() => this.handleClick(keyO)): ''}>
                                                                                        {item.remarks}

                                                                                </li>
                                                                                <li>
                                                                                    <b>{trans('messages.order_date_ots')} : </b>
                                                                                    {item.order_date}
                                                                                </li>
                                                                                <li>
                                                                                    <b>{trans('messages.order_code_ots')} : </b>
                                                                                    {item.order_code}
                                                                                </li>
                                                                                <li>
                                                                                    <b>{trans('messages.product_code_ots')} : </b>
                                                                                    {item.product_code}
                                                                                </li>
                                                                                <li>
                                                                                    <b>{trans('messages.product_name')} : </b>
                                                                                    {item.product_name}
                                                                                </li>
                                                                                <li>
                                                                                    <b>{trans('messages.price_invoice_ots')} : </b>
                                                                                    {item.price_invoice}
                                                                                </li>
                                                                                <li>
                                                                                    <b>{trans('messages.edi_answer_date_o2s')} : </b>
                                                                                    {item.edi_delivery_date}
                                                                                </li>
                                                                                <li>
                                                                                    <b>{trans('messages.in_stock_left')} : </b>
                                                                                    {item.incomplete_quantity}
                                                                                </li>
                                                                                <li>
                                                                                    <b>{trans('messages.supplier_id')} : </b>
                                                                                    {item.supplier_id}
                                                                                </li>
                                                                                <li>
                                                                                    <b>{trans('messages.supplier_name')} : </b>
                                                                                    {item.supplier_nm}
                                                                                </li>
                                                                                <li>
                                                                                    <b>{trans('messages.jan_code_ots')} : </b>
                                                                                    {item.product_jan}
                                                                                </li>
                                                                                <li>
                                                                                    <b>{trans('messages.quantity')} : </b>
                                                                                    {item.quantity}
                                                                                </li>
                                                                                <li>
                                                                                    <b>{trans('messages.arrival_date_o2s')} : </b>
                                                                                    {item.arrival_date}
                                                                                </li>
                                                                                <li>
                                                                                    <b>{trans('messages.maker_code')} : </b>
                                                                                    {item.product_maker_code}
                                                                                </li>
                                                                                <li>
                                                                                    <b>{trans('messages.maker_name')} : </b>
                                                                                    {item.maker_full_nm}
                                                                                </li>
                                                                                <li>
                                                                                    <b>{trans('messages.order_by')} : </b>
                                                                                    {deliveryType}
                                                                                </li>
                                                                                <li>
                                                                                    <b>{trans('messages.order_type')} : </b>
                                                                                    {orderType}
                                                                                </li>

                                                                            </ul>
                                                                        </TD>
                                                                    </TR>
                                                                    ]
                                                                )
                                                            }):
                                                            <TR><TD colSpan="14" className="text-center">{trans('messages.no_data')}</TD></TR>
                                                        }
                                                    </TBODY>
                                                </TABLE>
                                                </div>
                                            <PaginationContainerNew handleClickPage={this.handleClickPage}/>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="row">
                    <PopupRemark
                        remarks={(typeof this.state.dataRemark !== 'undefined') ? this.state.dataRemark.remarks : '' }
                        handleRemarkSubmit={this.handleRemarkSubmit}
                        handleRemarkCancel={this.handleRemarkCancel}
                        handleRemarkChange={this.handleRemarkChange}
                    />
                    <AcceptPopup
                        handleCancel={this.handleCancelAccept}
                        handleProcessAll={this.handleProcessAll}
                        handleRemarkAllChange={this.handleRemarkAllChange}
                        name={this.state.process_name}
                    />
                    <FaxConfirmPopup
                        fax_order_code={this.state.fax_order_code}
                        fax_product_code={this.state.fax_product_code}
                        handleChooseFaxType={this.handleChooseFaxType}
                    />
                    <FaxFormPopup
                        edi_order_code={this.state.cur_edi_order_code}
                        product_name_long={this.state.cur_product_name_long}
                        fax_name={this.state.cur_name}
                        fax={this.state.cur_fax}
                        key_cd={this.state.cur_key_cd}
                        is_supplier={this.state.cur_is_supplier}
                        login_user={this.state.cur_login_user}
                        index={this.state.cur_index}
                    />
                    <PopupConfirmCreateOrder
                        handleSubmit={this.handleIsAvailable}
                        param_update={this.state.IsAvailable}
                    />
                </div>
            </div>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        dataSource: state.commonReducer.dataSource,
        dataResponse: state.commonReducer.dataResponse
    }
}

const mapDispatchToProps = (dispatch) => {
    let ignore = [
        'process_all_flag',
        'process_name',
        'process_value',
        'remarks_all',
        'fax_order_code',
        'fax_product_code',
        'cur_edi_order_code',
        'cur_product_name_long',
        'cur_name',
        'cur_fax',
        'cur_key_cd',
        'cur_is_supplier',
        'cur_login_user',
        'cur_index',
        'IsAvailable'
    ];
    return {
        reloadData: (url, params, pushHistory, ignoreFields) => { dispatch(Action.reloadData(url, params, pushHistory, ignore)) },
        postData: (url, params, headers, callBack, paramsCallBack) => { dispatch(Action.postData(url, params, headers,callBack, paramsCallBack)) }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(OrderToSupplierList)