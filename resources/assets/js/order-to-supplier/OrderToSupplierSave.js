import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';

import { connect } from 'react-redux';
import * as Action from '../common/actions';
import * as queryString from 'query-string';

import BUTTON from '../common/components/form/BUTTON';
import FORM from '../common/components/form/FORM';
import INPUT from '../common/components/form/INPUT';
import LABEL from '../common/components/form/LABEL';
import ROW_GROUP from '../common/components/table/ROW_GROUP';
import axios from 'axios';
import Loading from '../common/components/common/Loading';
import moment from 'moment';
var timer;
class OrderToSupplierSave extends Component {

    constructor(props) {
        super(props);
        const stringParams = queryString.parse(props.location.search);
        this.state = {isPost : false};
        if (!_.isEmpty(stringParams)) {
            this.state = stringParams;
        }
        this.props.reloadData(saveUrl, stringParams);
    }

    handleChange = (event) => {
        const target = event.target;
        const value = target.value;
        const name = target.name;
        this.setState({
            [name]: (value)?value:undefined
        }, () => {
            if (name === 'product_code' || name === 'product_jan') {
                clearTimeout(timer);
                let _this = this;
                timer = setTimeout(function() {
                    _this.getInfoProduct(value, name);
                }, 1500);
            }
        });
    }


    componentDidUpdate() {
        if (!_.isEmpty(this.props.dataSource)) {
            $('.loading').hide();
        }
    }
    handleCancel = (event) => {
        window.location.href = baseUrl;
    }

    getInfoProduct = (value, name) => {
        $(".loading").show();
        let formData   = new FormData();
        let headers = {'X-Requested-With': 'XMLHttpRequest'};
        formData.append([name], value);
        axios.post(getDataProductUrl, formData, {headers : headers}
        ).then(response => {
            $(".loading").hide();
            if (response.data.info !== null) {
                this.setState({...response.data.info});
            } else {
                delete this.state;
                this.setState({[name]: (value)?value:undefined});
            }
        }).catch(error => {
            $(".loading").hide();
            this.getInfoProduct(value, name);
            throw(error);
        });
    }
     componentWillReceiveProps(nextProps) {
        if (!_.isEmpty(nextProps.dataResponse)) {
            if (nextProps.dataResponse.status === 1) {
                window.location.href = baseUrl;
            } else {
                document.getElementsByName('btnAccept')[0].disabled = false;
            }
        }
    }
    handleSubmit = (event) => {
        document.getElementsByName('btnAccept')[0].disabled = true;
        let headers = {'X-Requested-With': 'XMLHttpRequest'};
        this.setState({isPost : true} , () => {
            this.props.postData(saveUrl, this.state, headers);
        })

    }
    render() {
        let dataSource = this.props.dataSource;
        let dataResponse = this.props.dataResponse;
        return ((!_.isEmpty(this.props.dataSource)) &&
            <div>
            <div className="row">
            <div className="col-md-12">
            <FORM className="form-horizontal">
                <Loading className="loading" />
                    <div className="box box-success">
                        <div className="box-body">
                            <div className="row">
                                <div className="col-md-6">
                                    <INPUT
                                        name="product_code"
                                        labelTitle={trans('messages.product_code')}
                                        groupClass="form-group"
                                        className="form-control input-sm"
                                        fieldGroupClass="col-md-9 col-xs-12"
                                        labelClass="col-md-3 col-xs-12"
                                        onChange={this.handleChange}
                                        value={this.state.product_code || ''}
                                        isRequire={true}
                                        errorPopup={true}
                                        hasError={(dataResponse.message && dataResponse.message.product_code)?true:false}
                                        errorMessage={(dataResponse.message&&dataResponse.message.product_code)?dataResponse.message.product_code:""}/>
                                    <INPUT
                                        name="product_name"
                                        labelTitle={trans('messages.product_name')}
                                        className="form-control input-sm"
                                        fieldGroupClass="col-md-9"
                                        labelClass="col-md-3 input-pro-name"
                                        readOnly
                                        value={this.state.product_name || ''}/>
                                    <INPUT
                                        name="product_jan"
                                        labelTitle={trans('messages.jan_code_ots')}
                                        className="form-control input-sm"
                                        fieldGroupClass="col-md-9"
                                        labelClass="col-md-3"
                                        onChange={this.handleChange}
                                        isRequire={true}
                                        value={this.state.product_jan || ''}
                                        errorPopup={true}
                                        hasError={(dataResponse.message && dataResponse.message.product_jan)?true:false}
                                        errorMessage={(dataResponse.message&&dataResponse.message.product_jan)?dataResponse.message.product_jan:""}/>
                                    <INPUT
                                        name="order_num"
                                        labelTitle={trans('messages.quantity')}
                                        className="form-control input-sm"
                                        fieldGroupClass="col-md-9"
                                        labelClass="col-md-3"
                                        isRequire={true}
                                        onChange={this.handleChange}
                                        value={this.state.order_num || ''}
                                        errorPopup={true}
                                        hasError={(dataResponse.message && dataResponse.message.order_num)?true:false}
                                        errorMessage={(dataResponse.message&&dataResponse.message.order_num)?dataResponse.message.order_num:""}/>
                                    <INPUT
                                        name="price_invoice"
                                        labelTitle={trans('messages.price')}
                                        className="form-control input-sm"
                                        fieldGroupClass="col-md-9"
                                        labelClass="col-md-3"
                                        readOnly
                                        value={this.state.price_invoice || ''}/>
                                </div>
                                <div className="col-md-6">
                                    <INPUT
                                        name="supplier_id"
                                        labelTitle={trans('messages.supplier_id')}
                                        className="form-control input-sm"
                                        fieldGroupClass="col-md-9"
                                        labelClass="col-md-3"
                                        readOnly
                                        value={this.state.supplier_id || ''}
                                        />
                                    <INPUT
                                        name="supplier_nm"
                                        labelTitle={trans('messages.supplier_name')}
                                        className="form-control input-sm"
                                        fieldGroupClass="col-md-9"
                                        labelClass="col-md-3"
                                        readOnly
                                        value={this.state.supplier_nm || ''}
                                        />
                                </div>
                            </div>
                            <div className="row">
                                <div className="col-md-12">
                                    <BUTTON className="btn bg-purple input-sm pull-right btn-larger pull-right" type="button" onClick={this.handleCancel}>{trans('messages.btn_cancel')}</BUTTON>
                                    <BUTTON name="btnAccept" id="btnSubmit" className="btn btn-success input-sm btn-larger margin-element pull-right" type="button" onClick={this.handleSubmit}>{trans('messages.btn_accept')}</BUTTON>
                                </div>
                            </div>
                        </div>
                    </div>
            </FORM>
            </div>
            </div>
            </div>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        dataSource: state.commonReducer.dataSource,
        dataResponse: state.commonReducer.dataResponse
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        reloadData: (url, params, pushHistory, ignoreFields) => { dispatch(Action.reloadData(url, params, pushHistory, ignoreFields)) },
        postData: (url, params, headers, callBack, paramsCallBack) => { dispatch(Action.postData(url, params, headers,callBack, paramsCallBack)) }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(OrderToSupplierSave)