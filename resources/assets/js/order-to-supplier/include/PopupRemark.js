import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Loading from '../../common/components/common/Loading';
import axios from 'axios';
import TEXTAREA from '../../common/components/form/TEXTAREA';
import { connect } from 'react-redux';
import * as Action from '../../common/actions';

class PopupRemark extends Component {
    constructor(props) {
        super(props);
    }
    render() {
        let token = $("meta[name='csrf-token']").attr("content");
        return (
            <div className="modal fade" id="issue-modal">
                <Loading className="loading" />
                <input type="hidden" id="action-modal" />
                <div className="modal-dialog">
                    <div className="modal-content modal-custom-content">
                        <div className="modal-body modal-custom-body">
                            <p>{trans('messages.change_remarks')}</p>
                            <form className="form-horizontal"
                            encType={"multipart/form-data; boundary=----WebKitFormBoundary" + token}>
                                <TEXTAREA
                                    name="remarks"
                                    labelTitle={trans('messages.memo_content_popup') + "："}
                                    className="form-control input-sm"
                                    fieldGroupClass="col-md-9"
                                    labelClass="col-md-3"
                                    groupClass=""
                                    errorPopup={true}
                                    value={this.props.remarks}
                                    onChange={this.props.handleRemarkChange}
                                    rows="6"
                                    />
                            </form>
                        </div>
                        <div className="modal-footer modal-custom-footer">
                            <button type="button" className="btn btn-primary modal-custom-button"onClick={(e) => this.props.handleRemarkSubmit(e)}>{trans('messages.issue_modal_ok')}</button>
                            <button type="button" className="btn bg-purple modal-custom-button" onClick={(e) => this.props.handleRemarkCancel(e)}>{trans('messages.issue_modal_cancel')}</button>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}
const mapStateToProps = (state) => {
    return {
        dataSource: state.commonReducer.dataSource,
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        postData: (url, params, headers) => { dispatch(Action.postData(url, params, headers)) }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(PopupRemark)
