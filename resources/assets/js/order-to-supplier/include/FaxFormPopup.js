import React, { Component } from 'react';
import axios from 'axios';
import Loading from '../../common/components/common/Loading';
import './fax-form.css';

import INPUT from '../../common/components/form/INPUT';
import TEXTAREA from '../../common/components/form/TEXTAREA';

export default class FaxFormPopup extends Component {
    constructor(props) {
        super(props);
        this.state = {messages: null}
    }

    componentWillReceiveProps(nextProps) {
        let fax_contents = '';
        if (nextProps.edi_order_code) {
            fax_contents += "【発注番号】" + nextProps.edi_order_code + "\n";
        }
        fax_contents += "【商品名】" + nextProps.product_name_long + "\n";
        if (nextProps.is_supplier === 1) {
            fax_contents += "【仕入先名】" + nextProps.fax_name;
        } else {
            fax_contents += "【メーカー名】" + nextProps.fax_name;
        }

        this.setState({
            edi_order_code: nextProps.edi_order_code,
            product_name_long: nextProps.product_name_long,
            fax_name: nextProps.fax_name + '　ご担当者様',
            fax: nextProps.fax,
            key_cd: nextProps.key_cd,
            is_supplier: nextProps.is_supplier,
            login_user: nextProps.login_user,
            index: nextProps.index,
            fax_contents: fax_contents,
            messages: null
        });
    }

    handleChange = (event) => {
        const target = event.target;
        const value = target.type === 'checkbox' ? target.checked : target.value;
        const name = target.name;
        this.setState({
            [name]: (value)?value:undefined
        });
    }

    handleSubmit = () => {
        $(".loading").show();
        let headers = {'X-Requested-With': 'XMLHttpRequest'};
        axios.post(
            processFaxOrderUrl,
            this.state,
            {headers : headers}
        ).then(response => {
            $(".loading").hide();
            if (response.data.flg === 1) {
                $.notify({
                    icon: 'glyphicon glyphicon glyphicon-ok',
                    message: response.data.msg,
                },{
                    type: 'success',
                    delay: 2000
                });
                this.setState({messages: null}, () => {$('#fax-form-popup').modal('hide');});
            } else if (response.data.flg === 0) {
                this.setState({messages: response.data.msg});
            } else if (typeof response.data.msg !== 'undefined') {
                this.setState({messages: null}, () => {
                    $('#fax-form-popup').modal('hide');
                    $.notify({
                        icon: 'glyphicon glyphicon glyphicon-warning-sign',
                        message: response.data.msg,
                    },{
                        type: 'danger',
                        delay: 2000
                    });
                });
            }
        }).catch(error => {
            $(".loading").hide();
            throw(error);
        });
    }
    handleCancel = () => {
        $('#fax-form-popup').modal('hide');
        $(".loading").hide();
    }
    render() {
        return (
            <div className="modal fade" id="fax-form-popup">
                <Loading className="loading" />
                <div className="modal-dialog modal-lg">
                    <div className="modal-content">
                        <div className="modal-header">
                            <b>{trans('messages.fax_form_title')}</b>
                        </div>
                        <div className="modal-body modal-custom-body">
                            <div className="row">
                                <div className="col-md-12 main-contain">
                                    <div className="row">
                                        <div className="col-md-12">
                                            <div className="box-top">
                                                <span>FAX送付状</span>
                                            </div>
                                        </div>
                                    </div>
                                    <br/>
                                    <div className="row">
                                        <div className="col-md-8">
                                            <div className="row">
                                                <div className="col-md-12">
                                                    <INPUT
                                                        className="form-control input-lg"
                                                        name="fax_name"
                                                        value={this.state.fax_name || ''}
                                                        onChange={this.handleChange}
                                                        errorPopup={true}
                                                        hasError={(this.state.messages && this.state.messages.fax_name)?true:false}
                                                        errorMessage={(this.state.messages&&this.state.messages.fax_name)?this.state.messages.fax_name:""}
                                                    />
                                                </div>
                                            </div>
                                            <br/>
                                            <div className="row">
                                                <div className="col-md-3-5 col-md-offset-2 clear-padding-right">
                                                    送付先FAX番号：
                                                </div>
                                                <div className="col-md-5 clear-padding-left">
                                                    <INPUT
                                                        className="form-control input-sm"
                                                        name="fax"
                                                        value={this.state.fax || ''}
                                                        onChange={this.handleChange}
                                                        errorPopup={true}
                                                        hasError={(this.state.messages && this.state.messages.fax)?true:false}
                                                        errorMessage={(this.state.messages&&this.state.messages.fax)?this.state.messages.fax:""}
                                                    />
                                                </div>
                                            </div>
                                        </div>
                                        <div className="col-md-4">
                                            <span>株式会社大都</span><br/>
                                            <span>担当：{this.state.login_user}</span><br/>
                                            <span>電話：06-6715-1172</span><br/>
                                            <span>FAX：06-6715-1666</span><br/><br/>
                                            <span>問合せNo.{this.state.index}</span>
                                        </div>
                                    </div>
                                    <br/>
                                    <div className="row">
                                        <div className="col-md-12">
                                            <div className="panel panel-custom">
                                                <div className="panel-heading">連　絡　事　項</div>
                                                <div className="panel-body">
                                                    <TEXTAREA
                                                        className="form-control"
                                                        name="fax_contents"
                                                        value={this.state.fax_contents || ''}
                                                        onChange={this.handleChange}
                                                        errorPopup={true}
                                                        hasError={(this.state.messages && this.state.messages.fax_contents)?true:false}
                                                        errorMessage={(this.state.messages&&this.state.messages.fax_contents)?this.state.messages.fax_contents:""}
                                                        rows="15"
                                                    />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="modal-footer modal-custom-footer">
                            <button type="button" className="btn btn-info btn-sm modal-custom-button" onClick={this.handleSubmit}>{trans('messages.fax_form_send')}</button>
                            <button type="button" className="btn bg-purple btn-sm modal-custom-button" onClick={this.handleCancel}>{trans('messages.btn_cancel')}</button>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}