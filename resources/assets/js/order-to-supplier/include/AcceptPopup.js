import React, { Component } from 'react';
import Loading from '../../common/components/common/Loading';
import TEXTAREA from '../../common/components/form/TEXTAREA';
export default class AcceptPopup extends Component {
    constructor(props) {
        super(props);
        this.state = {}
    }
    handleSubmit = () => {
        $(".loading").show();
        this.props.handleProcessAll();
        $("textarea[name='remarks_all']").val('');
        $('#access-memo-modal').modal('hide');
    }
    handleCancelClick = () => {        
        this.props.handleCancel()
    }
    render() {
        return (
            <div className="modal fade" id="access-memo-modal">
                <Loading className="loading" />
                <div className="modal-dialog ">
                    <div className="modal-content">
                        <div className="modal-header">
                            <b>{trans('messages.accept_title')}</b>
                        </div>
                        <div className="modal-body modal-custom-body">
                            <div className="row">
                                <div className="col-md-12 message-popup">
                                { (this.props.name === 'btn_note_all') ? 
                                   [ 
                                        <div className="form-group" key="title_remark_box">
                                            <p className="col-md-12" key="title_remark">{trans('messages.memo_ask_return_address_remark')}</p>
                                        </div>,
                                        <TEXTAREA
                                            name="remarks_all"
                                            key="remarks_all"
                                            labelTitle={trans('messages.memo_content_popup') + "："}
                                            className="form-control input-sm"
                                            fieldGroupClass="col-md-9"
                                            labelClass="col-md-3"
                                            groupClass=""
                                            errorPopup={true}
                                            value={this.props.process_value}
                                            onChange={this.props.handleRemarkAllChange}
                                            rows="6"
                                            />,
                                        <div className="form-group" key="title_remark_box_2">
                                            <div className="col-md-12">
                                                <br />
                                                <p  key="title_ask">{trans('messages.memo_ask_return_address_inform')}</p>
                                            </div>
                                        </div>
                                    ]
                                      :
                                    <p>{trans('messages.memo_ask_return_address_inform')}</p>
                                    
                                }
                                </div>
                            </div>
                        </div>
                        <div className="modal-footer modal-custom-footer">
                            <button type="button" className="btn btn-primary btn-sm modal-custom-button" onClick={this.handleSubmit}>{trans('messages.btn_accept_ok')}</button>
                            <button type="button" className="btn bg-purple btn-sm modal-custom-button" onClick={this.handleCancelClick}>{trans('messages.btn_accept_cancel')}</button>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}