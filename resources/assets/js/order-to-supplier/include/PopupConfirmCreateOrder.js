import React, { Component } from 'react';
import Loading from '../../common/components/common/Loading';
export default class PopupConfirmCreateOrder extends Component {
    constructor(props) {
        super(props);
        this.state = {}
    }
    handleSubmit = (e) => {
        this.props.handleSubmit(
            this.props.param_update.order_code,
            this.props.param_update.edi_order_code,
            this.props.param_update.order_type,
            this.props.param_update.product_code,
            this.props.param_update.state_flg,
            this.props.param_update.product_status,
            1,
            e
        );
    }

    handleSubmitNoClick = (e) => {
        this.props.handleSubmit(
            this.props.param_update.order_code,
            this.props.param_update.edi_order_code,
            this.props.param_update.order_type,
            this.props.param_update.product_code,
            this.props.param_update.state_flg,
            this.props.param_update.product_status,
            0,
            e
        );

    }

    handleCancelClick = (e) => {
        $('#modal-confirm-new-order').modal('hide');
    }
    render() {
        return (
            <div className="modal fade" id="modal-confirm-new-order">
                <Loading className="loading" />
                <div className="modal-dialog ">
                    <div className="modal-content">
                        <div className="modal-header text-center">
                        <b>{trans('messages.accept_title')}</b>
                        </div>
                        <div className="modal-body modal-custom-body">
                            <div className="row">
                                <div className="col-md-12 text-center">
                                    {trans('messages.message_confirm_new_order')}
                                </div>
                            </div>
                        </div>
                        <div className="modal-footer modal-custom-footer">
                            <div className='row'>
                                <div className="col-md-12">
                                    <button type="button" className="btn btn-primary btn-sm modal-custom-button margin-element" onClick={this.handleSubmit}>{trans('messages.btn_accept_ok')}</button>

                                    <button type="button" className="btn bg-purple btn-sm modal-custom-button margin-element" onClick={this.handleSubmitNoClick}>{trans('messages.btn_accept_cancel')}</button>

                                    <button type="button" className="btn bg-default btn-sm modal-custom-button pull-right " onClick={this.handleCancelClick}>{trans('messages.btn_cancel')}</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}