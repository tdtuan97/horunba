import React, { Component } from 'react';
import axios from 'axios';
import Loading from '../../common/components/common/Loading';
import INPUT from '../../common/components/form/INPUT';

export default class FaxConfirmPopup extends Component {
    constructor(props) {
        super(props);
        this.state = {
            fax_product_code: '',
            err_product_code: ''
        }
    }
    componentWillReceiveProps(nextProps) {
        this.setState({
            fax_product_code: '',
            err_product_code: ''
        });
    }
    handleChange = (event) => {
        const target = event.target;
        const value = target.type === 'checkbox' ? target.checked : target.value;
        const name = target.name;
        this.setState({
            [name]: (value)?value:undefined
        });
    }
    handleChooseFaxType = (type) => {
        if (!this.props.fax_order_code) {
            $(".loading").show();
            let headers = {'X-Requested-With': 'XMLHttpRequest'};
            axios.post(
                getFaxOrderCommonUrl,
                {type: type, fax_product_code: this.state.fax_product_code},
                {headers : headers}
            ).then(response => {
                $(".loading").hide();
                let data = response.data;
                if (data.flg === 1) {
                    this.props.handleChooseFaxType(type, data.msg);
                } else if (data.flg === -1) {
                    this.setState({err_product_code: data.msg.fax_product_code});
                } else if (typeof data.msg !== 'undefined') {
                    $.notify({
                        icon: 'glyphicon glyphicon-warning-sign',
                        message: data.msg,
                    },{
                        type: 'danger',
                        delay: 2000
                    });
                }
            }).catch(error => {
                $(".loading").hide();
                throw(error);
            });
        } else {
            this.props.handleChooseFaxType(type);
        }

    }
    handleCancel = () => {
        this.setState({
            fax_product_code: '',
            err_product_code: ''
        });
        $('#fax-confirm-popup').modal('hide');
        $(".loading").hide();
    }
    render() {
        return (
            <div className="modal fade" id="fax-confirm-popup">
                <Loading className="loading" />
                <div className="modal-dialog ">
                    <div className="modal-content">
                        <div className="modal-header">
                            <b>{trans('messages.fax_confirm_title')}</b>
                        </div>
                        <div className="modal-body modal-custom-body">
                            <div className="row">
                                <div className="col-md-12">
                                {
                                    (!this.props.fax_order_code)?
                                    <div className="row">
                                        <div className="col-md-2">
                                            <label>{trans('messages.product_code')}</label>
                                        </div>
                                        <div className="col-md-4">
                                            <INPUT
                                                name="fax_product_code"
                                                onChange={this.handleChange}
                                                value={this.state.fax_product_code || ''}
                                                className="form-control input-sm"
                                                errorPopup={true}
                                                hasError={(this.state.err_product_code)?true:false}
                                                errorMessage={this.state.err_product_code || ''}
                                            />
                                        </div>
                                    </div>
                                    :trans('messages.fax_confirm_message')
                                }
                                </div>
                            </div>
                        </div>
                        <div className="modal-footer modal-custom-footer">
                            <button type="button" className="btn btn-primary btn-sm modal-custom-button" onClick={() => this.handleChooseFaxType('maker')}>{trans('messages.btn_fax_confirm_maker')}</button>
                            <button type="button" className="btn btn-info btn-sm modal-custom-button" onClick={() => this.handleChooseFaxType('supplier')}>{trans('messages.btn_fax_confirm_supplier')}</button>
                            <button type="button" className="btn bg-purple btn-sm modal-custom-button" onClick={this.handleCancel}>{trans('messages.btn_fax_confirm_cancel')}</button>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}