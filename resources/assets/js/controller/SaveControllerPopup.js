import React, { Component } from 'react';
import Loading from '../common/components/common/Loading';
import INPUT from '../common/components/form/INPUT';
import TEXTAREA from '../common/components/form/TEXTAREA';
import CHECKBOX_CUSTOM from '../common/components/form/CHECKBOX_CUSTOM';
import LABEL from '../common/components/form/LABEL';

import axios from 'axios';

export default class SaveControllerPopup extends Component {
    constructor(props) {
        super(props);
        this.state = {};
    }

    handleChange = (event) => {
        const target = event.target;
        const name = target.name;
        const value = target.type === 'checkbox' ? (target.checked?1:0) : target.value;
        this.setState({
            [name]: value
        });
    }

    handleSaveClick = () => {
        let params = {};
        params['id']            = this.state.id;
        params['screen_name']   = this.state.screen_name;
        params['screen_class']  = this.state.screen_class;
        params['action_list']   = this.state.action_list;
        params['action_access'] = this.state.action_access;
        params['no_list']       = this.state.no_list;
        params['flg_access']    = this.state.flg_access;

        $(".loading").show();
        axios.post(saveCtrlUrl, params, {headers: {'X-Requested-With': 'XMLHttpRequest'}}
        ).then(response => {
            if (response.data.status === 0) {
                let message = response.data.message;
                this.setState({...response.data});
            } else {
                this.handleCancelClick();
                this.props.handleReloadData();
            }
            $(".loading").hide();
        }).catch(error => {
            $(".loading").hide();
            throw(error);
        });
    }

    handleCancelClick = () => {
        this.setState({
            id: '',
            screen_name: '',
            screen_class: '',
            action_list: '',
            action_access: '',
            action: '',
            message: '',
            no_list: 0,
            flg_access: 0
        });
        $('#save-control-modal').modal('hide');
    }

    componentWillReceiveProps(nextProps) {
        if (typeof nextProps.data_edit !== 'undefined') {
            this.setState({
                id:nextProps.data_edit.id,
                screen_name: nextProps.data_edit.screen_name,
                screen_class: nextProps.data_edit.screen_class,
                action_list: nextProps.data_edit.action_list,
                action_access: nextProps.data_edit.action_access,
                no_list: nextProps.data_edit.no_list,
                flg_access: nextProps.data_edit.flg_access,
                action: 'edit'
            });
        }
    }

    render() {
        let action_popup = trans('messages.add_controller');
        if (this.state.action === 'edit') {
            action_popup = trans('messages.genre_edit');
        }
        return (
            <div className="modal fade" id="save-control-modal">
                <Loading className="loading" />
                <div className="modal-dialog ">
                    <div className="modal-content">
                        <div className="modal-header">
                        {action_popup}
                        </div>
                        <div className="modal-body modal-custom-body">
                        <form className="form-horizontal">
                            <div className="row">
                                <INPUT
                                    name="screen_name"
                                    labelTitle={trans('messages.screen_name')}
                                    className="form-control input-sm"
                                    fieldGroupClass="col-md-9"
                                    labelClass="col-md-3"
                                    groupClass="col-md-12 margin-form"
                                    errorPopup={true}
                                    value={this.state.screen_name||''}
                                    onChange={this.handleChange}
                                    />
                            </div>
                            <div className="row">
                                <INPUT
                                    name="screen_class"
                                    labelTitle={trans('messages.screen_class')}
                                    className="form-control input-sm"
                                    fieldGroupClass="col-md-9"
                                    labelClass="col-md-3"
                                    groupClass="col-md-12 margin-form"
                                    errorPopup={true}
                                    value={this.state.screen_class||''}
                                    onChange={this.handleChange}
                                    hasError={(this.state.message && this.state.message.screen_class)?true:false}
                                    errorMessage={(this.state.message && this.state.message.screen_class)?this.state.message.screen_class:""}
                                    />
                            </div>
                            <div className="row">
                                <TEXTAREA
                                    name="action_list"
                                    labelTitle={trans('messages.action_list')}
                                    className="form-control input-sm"
                                    fieldGroupClass="col-md-9"
                                    labelClass="col-md-3"
                                    groupClass="col-md-12 margin-form"
                                    errorPopup={true}
                                    value={this.state.action_list||''}
                                    onChange={this.handleChange}
                                    />
                            </div>
                            <div className="row">
                                <TEXTAREA
                                    name="action_access"
                                    labelTitle={trans('messages.action_access')}
                                    className="form-control input-sm"
                                    fieldGroupClass="col-md-9"
                                    labelClass="col-md-3"
                                    groupClass="col-md-12 margin-form"
                                    errorPopup={true}
                                    value={this.state.action_access||''}
                                    onChange={this.handleChange}
                                    />
                            </div>
                            <div className="row">
                                <div className="col-md-12 margin-form">
                                <LABEL
                                    groupClass="col-md-3"
                                    labelClass=" "
                                    labelTitle={trans('messages.no_list')}>
                                </LABEL>
                                <div className="col-md-9">
                                    <CHECKBOX_CUSTOM
                                        key={Math.random()}
                                        name="no_list"
                                        defaultChecked={this.state.no_list === 1 ? true : false}
                                        onChange={this.handleChange}
                                     />
                                </div>
                                </div>
                            </div>
                            <div className="row">
                                <div className="col-md-12 margin-form">
                                <LABEL
                                    groupClass="col-md-3"
                                    labelClass=" "
                                    labelTitle={trans('messages.flg_access')}>
                                </LABEL>
                                <div className="col-md-9">
                                    <CHECKBOX_CUSTOM
                                        key={Math.random()}
                                        name="flg_access"
                                        defaultChecked={this.state.flg_access === 1 ? true : false}
                                        onChange={this.handleChange}
                                     />
                                </div>
                                </div>
                            </div>
                        </form>
                        </div>
                        <div className="modal-footer modal-custom-footer">
                            <button type="button" className="btn btn-primary btn-sm modal-custom-button" onClick={this.handleSaveClick}>{trans('messages.btn_accept')}</button>
                            <button type="button" className="btn bg-purple btn-sm modal-custom-button" onClick={this.handleCancelClick}>{trans('messages.btn_cancel')}</button>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}