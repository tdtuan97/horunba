import React, { Component } from 'react';

import { connect } from 'react-redux';
import * as Action from '../common/actions';
import * as queryString from 'query-string';
import axios from 'axios';

import BUTTON from '../common/components/form/BUTTON';
import FORM from '../common/components/form/FORM';
import INPUT from '../common/components/form/INPUT';
import SELECT from '../common/components/form/SELECT';

import TABLE from '../common/components/table/TABLE';
import THEAD from '../common/components/table/THEAD';
import TBODY from '../common/components/table/TBODY';
import TR from '../common/components/table/TR';
import TH from '../common/components/table/TH';
import TD from '../common/components/table/TD';
import CHECKBOX_CUSTOM from '../common/components/form/CHECKBOX_CUSTOM';

import SortComponent from '../common/components/table/SortComponent';
import PaginationContainer from '../common/containers/PaginationContainer'
import Loading from '../common/components/common/Loading';
import SaveControllerPopup from './SaveControllerPopup';

class ControllerList extends Component {

    constructor(props) {
        super(props);
        const stringParams = queryString.parse(props.location.search);
        this.state = {};
        if (!_.isEmpty(stringParams)) {
            this.state = stringParams;
        }
        this.props.reloadData(dataListUrl, this.state);
    }

    handleChange = (event) => {
        const target = event.target;
        const value = target.type === 'checkbox' ? target.checked : target.value;
        const name = target.name;
        this.setState({
            [name]: (value)?value:undefined
        },() => {
            if (name === 'per_page') {
                this.handleReloadData();
            }
        });
    }

    handleClearClick = (event) => {
        Object.keys(this.state).map((key) => {
            if (key !== 'per_page') {
                delete this.state[key];
            }
        });
        this.handleReloadData();
    }

    handleReloadData = () => {
        if (this.state.page) {
            delete this.state.page;
        }
        if (this.state.data_edit) {
            delete this.state.data_edit;
        }
        this.props.reloadData(dataListUrl, this.state, true);
    }

    componentDidUpdate() {
        if (!_.isEmpty(this.props.dataSource)) {
            $('.loading').hide();
            $('.show-modal-content').customPopover();
        }
    }

    componentWillMount() {
        window.onpopstate = (event) => {
            location.reload();
        };
    }

    handleClickPage = (page) => {
        this.setState({
            page: page
        },() => {
            this.props.reloadData(dataListUrl, this.state, true);
        });
    }

    handleAddController = () => {
        $('#save-control-modal').modal({backdrop: 'static', show: true});
    }

    handleChecked = (index, name, event) => {
        const target = event.target;
        const value = target.checked === true ? 1 : 0;
        let headers = {'X-Requested-With': 'XMLHttpRequest'};
        this.props.postData(
            UpCtrlUrl,
            {key : index, value : value, name : name},
            headers,
            this.handleReloadData,
        );
    }

    handleDelete = (index, event) => {
        let headers = {'X-Requested-With': 'XMLHttpRequest'};
        this.props.postData(
            delCtrlUrl,
            {key : index},
            headers,
            this.handleReloadData,
        );
    }

    handleEdit = (item, event) => {
        event.preventDefault();
        this.setState({
            data_edit : item,
        }, () => {
            $('#save-control-modal').modal({backdrop: 'static', show: true});
        });
    }

    render() {
        let dataSource   = this.props.dataSource;
        let index        = 0;
        let totalItems   = 0;
        let itemsPerPage = 20;
        let genreOption  = '';
        if (!_.isEmpty(dataSource)) {
            index        = dataSource.data.from - 1;
            totalItems   = dataSource.data.total;
            itemsPerPage = dataSource.data.per_page;
        }
        return ( (!_.isEmpty(dataSource)) &&
            <div>
                <Loading className="loading" />
                <div className="box">
                    <div className="box-header">
                        <div className="row">
                                <div className="col-md-12">
                                    <h4>{trans('messages.search_title')}</h4>
                                </div>
                                <FORM>
                                    <SELECT
                                        name="per_page"
                                        groupClass="form-group col-md-1 per-page pull-right-md"
                                        labelClass="pull-right"
                                        labelTitle={trans('messages.per_page')}
                                        onChange={this.handleChange}
                                        value={this.state.per_page||''}
                                        options={{20:20, 40:40, 60:60, 80:80, 100:100}}
                                        className="form-control input-sm" />
                                </FORM>
                        </div>
                        <div className="row">
                            <div className="col-md-9 col-xs-4">
                                <BUTTON className="btn btn-success input-sm margin-element" onClick={this.handleAddController}>{trans('messages.btn_add_controller')}</BUTTON>
                            </div>
                            <div className="col-md-3 col-xs-8">
                                <BUTTON className="btn btn-danger pull-right" onClick={this.handleClearClick}>{trans('messages.btn_reset')}</BUTTON>
                                <BUTTON className="btn btn-primary pull-right margin-element" onClick={this.handleReloadData} >{trans('messages.btn_search')}</BUTTON>
                            </div>
                        </div>
                    </div>
                    <div className="box-body">
                        <div className="row">
                            <div className="col-md-12">
                                {(totalItems/itemsPerPage > 1) &&
                                <div className="row">
                                    <div className="col-md-2-5"></div>
                                    <div className="col-md-7 text-center">
                                        <PaginationContainer handleClickPage={this.handleClickPage}/>
                                    </div>
                                    <div className="col-md-2-5 text-right line-height-md">
                                        {dataSource.data.from}件 - {dataSource.data.to}件 【全{dataSource.data.total}件】
                                    </div>
                                </div>
                                }
                                <div className="table-responsive">
                                <TABLE className="table table-striped table-custom">
                                    <THEAD>
                                        <TR>
                                            <TH className="text-center">#</TH>
                                            <TH className="text-center">{trans('messages.screen_name')}</TH>
                                            <TH className="text-center">{trans('messages.screen_class')}</TH>
                                            <TH className="text-center">{trans('messages.action_list')}</TH>
                                            <TH className="text-center">{trans('messages.action_access')}</TH>
                                            <TH className="text-center">{trans('messages.no_list')}</TH>
                                            <TH className="text-center">{trans('messages.flg_access')}</TH>
                                            <TH className="text-center">{trans('messages.genre_delete')}</TH>
                                        </TR>
                                    </THEAD>
                                    <TBODY>
                                        {
                                            (!_.isEmpty(dataSource.data.data))?
                                            dataSource.data.data.map((item) => {
                                                index++;
                                                let classRow = (index%2 ===0) ? 'odd' : 'even';
                                                return (
                                                    <TR key={"tr" + index} className={classRow}>
                                                        <TD className="text-center"><a onClick={(e) => this.handleEdit(item, e)}>{item.id}</a></TD>
                                                        <TD title={item.screen_name}>
                                                            {item.screen_name}
                                                        </TD>
                                                        <TD title={item.screen_class}>
                                                            {item.screen_class}
                                                        </TD>
                                                        <TD className="text-new-line">
                                                            {item.action_list}
                                                        </TD>
                                                        <TD className="text-new-line">
                                                            {item.action_access}
                                                        </TD>
                                                        <TD className="text-center">
                                                            <CHECKBOX_CUSTOM
                                                                key={item.id + Math.random()}
                                                                defaultChecked={item.no_list === 1 ? true : false}
                                                                onChange={(e) => this.handleChecked(item.id, 'no_list', e)}
                                                             />
                                                        </TD>
                                                        <TD className="text-center">
                                                            <CHECKBOX_CUSTOM
                                                                key={item.id + Math.random()}
                                                                defaultChecked={item.flg_access === 1 ? true : false}
                                                                onChange={(e) => this.handleChecked(item.id, 'flg_access', e)}
                                                             />
                                                        </TD>
                                                        <TD className="text-center">
                                                            <button type="button" name="delete_all" className="btn btn-box-tool btn-remove"
                                                            data-target="#access-memo-delete-modal" data-backdrop="static" onClick={(e) => this.handleDelete(item.id, e)}
                                                            >
                                                                <i className="fa fa-trash-o"></i>
                                                            </button>
                                                        </TD>
                                                    </TR>
                                                )
                                            })
                                            :
                                            <TR><TD colSpan="9" className="text-center">{trans('messages.no_data')}</TD></TR>
                                        }
                                    </TBODY>
                                </TABLE>
                                </div>
                                <div className="text-center">
                                    <PaginationContainer handleClickPage={this.handleClickPage}/>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <SaveControllerPopup
                    handleReloadData={this.handleReloadData}
                    data_edit={this.state.data_edit}
                />
            </div>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        dataSource: state.commonReducer.dataSource
    }
}

const mapDispatchToProps = (dispatch) => {
    let ignore = ['data_edit'];
    return {
        reloadData: (url, params, pushHistory, ignoreFields) => { dispatch(Action.reloadData(url, params, pushHistory, ignore)) },
        postData: (url, params, headers,callback,callParam) => { dispatch(Action.postData(url, params, headers,callback,callParam)) },
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(ControllerList)