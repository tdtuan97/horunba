import React, { Component } from 'react';

import { connect } from 'react-redux';
import * as Action from '../common/actions';
import * as queryString from 'query-string';
import axios from 'axios';

import BUTTON from '../common/components/form/BUTTON';
import FORM from '../common/components/form/FORM';
import INPUT from '../common/components/form/INPUT';
import CHECKBOX_CUSTOM from '../common/components/form/CHECKBOX_CUSTOM';
import SELECT from '../common/components/form/SELECT';
import LABEL from '../common/components/form/LABEL';

import TABLE from '../common/components/table/TABLE';
import THEAD from '../common/components/table/THEAD';
import TBODY from '../common/components/table/TBODY';
import TR from '../common/components/table/TR';
import TH from '../common/components/table/TH';
import TD from '../common/components/table/TD';

import SortComponent from '../common/components/table/SortComponent';
import PaginationContainer from '../common/containers/PaginationContainer'
import Loading from '../common/components/common/Loading';
import PopupConfirmUp from './containers/PopupConfirmUp';
import PopupConfirmDelete from './containers/PopupConfirmDelete';

class StoreProductList extends Component {

    constructor(props) {
        super(props);
        const stringParams = queryString.parse(props.location.search);
        this.state = {};
        if (!_.isEmpty(stringParams)) {
            this.state = stringParams;
        }
        this.props.reloadData(dataListUrl, this.state);
    }

    handleChange = (event,item) => {
        const target = event.target;
        const value = target.type === 'checkbox' ? target.checked : target.value;
        const name = target.name;
        this.setState({
            [name]: (value)?value:undefined
        },() => {
            if (name === 'per_page') {
                this.handleReloadData();
            }
        });
    }

    handleClearClick = (event) => {
        Object.keys(this.state).map((key) => {
            if (key !== 'per_page') {
                delete this.state[key];
            }
        });
        this.handleReloadData();
    }

    handleReloadData = () => {
        if (this.state.page) {
            delete this.state.page;
        }
        this.props.reloadData(dataListUrl, this.state, true);
    }

    componentDidUpdate() {
        if (!_.isEmpty(this.props.dataSource)) {
            $('.loading').hide();
        }
        dragscroll.reset();
        $("table th").resizeble();
    }

    componentWillMount() {
        window.onpopstate = (event) => {
            location.reload();
        };
    }

    componentWillReceiveProps(nextProps) {
        if (!_.isEmpty(nextProps.dataSource.data)) {
            let isAllUpdated = 1;
            let productCode  = [];
            nextProps.dataSource.data.data.map((item) => {
                if (item.product_jan_new !== null && item.product_jan_new !== '') {
                    if (item.is_updated != 1) {
                        isAllUpdated = 2;
                    }
                    productCode.push(item.product_code);
                }
            });
            this.setState({is_updated_all : isAllUpdated, product_code : productCode});
        }
    }

    handleSortClick = (name, nextSort) => {
        Object.keys(this.state).map((key) => {
            if (key.startsWith('sort_')) {
                delete this.state[key];
            }
        });
        if (nextSort === 'none') {
            delete this.state[name];
            this.props.reloadData(dataListUrl, this.state, true);
        } else {
            this.setState({[name]: nextSort}, () => {
                this.props.reloadData(dataListUrl, this.state, true);
            });
        }

    }

    handleClickPage = (page) => {
        this.setState({
            page: page
        },() => {
            this.props.reloadData(dataListUrl, this.state, true);
        });
    }

    handleDfHhandling = (productCode, dfHandling) => {
        let check = dfHandling === 1 ? true : false;
        $('input[name="df_handling[' + productCode + ']"]').prop('checked', check);
    }

    handleIsUpdated = (productCode, isUpdated) => {
        $(".loading").show();
        isUpdated = isUpdated===1?2:1;
        if (productCode === 'all') {
            productCode = this.state.product_code;
        } else {
            productCode = [productCode];
        }
        axios.post(
            changeIsUpdatedUrl,
            {product_code: productCode, is_updated: isUpdated},
            {headers: {'X-Requested-With': 'XMLHttpRequest'}}
        )
        .then(response => {
            $(".loading").hide();
            if (response.data.status === 1) {
                this.props.reloadData(dataListUrl, this.state, true);
            } else {
                $.notify({
                    icon: 'glyphicon glyphicon-warning-sign',
                    message: response.data.messages,
                },{
                    type: 'danger',
                    delay: 2000
                });
            }
        })
        .catch(error => {
            $(".loading").hide();
            throw(error);
        });
    }

    handleProcessSmaregi = (name, productCode) => {

        this.setState({dataProccess: {action: name, product_code: productCode}} , () => {
            if (name === 'up') {
                $('#confirm-up-modal').modal('show');
            } else {
                console.log('this.state.dataProccess');
                $('#confirm-delete-modal').modal('show');
            }
        })
    }

    handleSubmitPopup = (event) => {
        axios.post(
            processSmaregiUrl,
            this.state.dataProccess,
            {headers: {'X-Requested-With': 'XMLHttpRequest'}}
        )
        .then(response => {
            $(".loading").hide();
            delete this.state.dataProccess;
            $('#confirm-delete-modal').modal('hide');
            $('#confirm-up-modal').modal('hide');
            if (response.data.status === 1) {
                this.props.reloadData(dataListUrl, this.state, true);
            } else {
                $.notify({
                    icon: 'glyphicon glyphicon-warning-sign',
                    message: response.data.messages,
                },{
                    type: 'danger',
                    delay: 2000
                });
            }

        })
        .catch(error => {
            $(".loading").hide();
            throw(error);
        });
        console.log(this.state.dataProccess);
    }

    handleCancelPopup = (event) => {
        console.log(this.state.dataProccess);
        delete this.state.dataProccess;
        $('#confirm-delete-modal').modal('hide');
        console.log(this.state.dataProccess);
    }

    render() {
        let dataSource   = this.props.dataSource;
        let nf           = new Intl.NumberFormat();
        let index        = 0;
        let totalItems   = 0;
        let itemsPerPage = 20;
        if (!_.isEmpty(dataSource)) {
            index = dataSource.data.from - 1;
            totalItems   = dataSource.data.total;
            itemsPerPage = dataSource.data.per_page;
        }
        return ( (!_.isEmpty(dataSource)) &&
            <div>
                <Loading className="loading" />
                <div className="box">
                    <div className="box-header">
                        <div className="row">
                                <div className="col-md-12">
                                    <h4>{trans('messages.search_title')}</h4>
                                </div>
                                <FORM>
                                    <INPUT
                                        name="product_code_from"
                                        groupClass="form-group col-md-1-75"
                                        labelTitle={trans('messages.product_code_from')}
                                        onChange={this.handleChange}
                                        value={this.state.product_code_from || ''}
                                        className="form-control input-sm" />
                                    <INPUT
                                        name="product_code_to"
                                        groupClass="form-group col-md-1-75"
                                        labelTitle={trans('messages.product_code_to')}
                                        onChange={this.handleChange}
                                        value={this.state.product_code_to || ''}
                                        className="form-control input-sm" />
                                    <INPUT
                                        name="store_product_id_from"
                                        groupClass="form-group col-md-1-75"
                                        labelTitle={trans('messages.store_product_id_from')}
                                        onChange={this.handleChange}
                                        value={this.state.store_product_id_from || ''}
                                        className="form-control input-sm" />
                                    <INPUT
                                        name="store_product_id_to"
                                        groupClass="form-group col-md-1-75"
                                        labelTitle={trans('messages.store_product_id_to')}
                                        onChange={this.handleChange}
                                        value={this.state.store_product_id_to || ''}
                                        className="form-control input-sm" />
                                    <SELECT
                                        name="product_jan_new"
                                        groupClass="form-group col-md-1-75"
                                        labelClass="pull-left"
                                        labelTitle={trans('messages.product_jan_new')}
                                        onChange={this.handleChange}
                                        value={this.state.product_jan_new||''}
                                        options={dataSource.janOpt}
                                        className="form-control input-sm" />
                                    <SELECT
                                        name="df_handling"
                                        groupClass="form-group col-md-1-75"
                                        labelClass="pull-left"
                                        labelTitle={trans('messages.df_handling_search')}
                                        onChange={this.handleChange}
                                        value={this.state.df_handling||''}
                                        options={dataSource.dfHandlingOpt}
                                        className="form-control input-sm" />
                                    <SELECT
                                        name="per_page"
                                        groupClass="form-group col-md-1 per-page pull-right-md"
                                        labelClass="pull-left"
                                        labelTitle={trans('messages.per_page')}
                                        onChange={this.handleChange}
                                        value={this.state.per_page||''}
                                        options={{20:20, 30:30, 50:50}}
                                        className="form-control input-sm" />
                                </FORM>
                        </div>
                        <div className="row">
                            <div className="col-md-9">
                                <a href={addNewUrl} className="btn btn-success input-sm">{trans('messages.btn_open_screen')}</a>
                            </div>
                            <div className="col-md-3 col-xs-8 pull-right">
                                <BUTTON className="btn btn-danger pull-right" onClick={this.handleClearClick}>{trans('messages.btn_reset')}</BUTTON>
                                <BUTTON className="btn btn-primary pull-right margin-element" onClick={this.handleReloadData} >{trans('messages.btn_search')}</BUTTON>
                            </div>
                        </div>
                    </div>
                    <div className="box-body">
                        <div className="row">
                            <div className="col-md-12">
                                {(totalItems/itemsPerPage > 1) &&
                                <div className="row">
                                    <div className="col-md-2-5"></div>
                                    <div className="col-md-7 text-center">
                                        <PaginationContainer handleClickPage={this.handleClickPage}/>
                                    </div>
                                    <div className="col-md-2-5 text-right line-height-md">
                                        {dataSource.data.from}件 - {dataSource.data.to}件 【全{dataSource.data.total}件】
                                    </div>
                                </div>
                                }
                                <div className="table-responsive">
                                <TABLE className="table table-striped table-custom">
                                    <THEAD>
                                        <TR>
                                            <TH className="visible-xs visible-sm"></TH>
                                            <TH className="text-center text-middle">
                                                {trans('messages.index')}
                                            </TH>
                                            <TH className="col-max-min-130 text-center text-middle">
                                                <SortComponent
                                                    name="sort_product_code"
                                                    onClick={this.handleSortClick}
                                                    currentSort={(this.state.sort_product_code)?this.state.sort_product_code:'none'}
                                                    title={trans('messages.product_code')} />
                                            </TH>
                                            <TH className="col-max-min-120 text-center text-middle hidden-xs hidden-sm" >
                                                <SortComponent
                                                name="sort_store_product_id"
                                                onClick={this.handleSortClick}
                                                currentSort={(this.state.sort_store_product_id)?this.state.sort_store_product_id:'none'}
                                                title={trans('messages.store_product_id')} />
                                            </TH>
                                            <TH className="col-max-min-100 resize-thead text-center text-middle hidden-xs hidden-sm" >
                                                {trans('messages.product_name')}
                                            </TH>
                                            <TH className="col-max-min-100 text-center text-middle" >
                                                {trans('messages.df_handling')}
                                            </TH>
                                            <TH className="col-max-min-120 text-center text-middle hidden-xs hidden-sm" >
                                                <SortComponent
                                                name="sort_product_jan"
                                                onClick={this.handleSortClick}
                                                currentSort={(this.state.sort_product_jan)?this.state.sort_product_jan:'none'}
                                                title={trans('messages.store_product_jan')} />
                                            </TH>
                                            <TH className="col-max-min-100 text-center text-middle hidden-xs hidden-sm" >
                                                {trans('messages.price')}
                                            </TH>
                                            <TH className="col-max-min-100 text-center text-middle hidden-xs hidden-sm" >
                                                {trans('messages.product_cost')}
                                            </TH>
                                            <TH className="col-max-min-130 text-center text-middle hidden-xs hidden-sm" >
                                                {trans('messages.product_size')}
                                            </TH>
                                            <TH className="col-max-min-130 text-center text-middle hidden-xs hidden-sm" >
                                                {trans('messages.product_color')}
                                            </TH>
                                            <TH className="text-center text-middle" >
                                            </TH>
                                        </TR>

                                    </THEAD>
                                    <TBODY>
                                    {
                                        (!_.isEmpty(dataSource.data.data))?
                                        dataSource.data.data.map((item) => {
                                            index++;
                                            let classRow = (index%2 ===0) ? 'odd' : 'even';

                                            return (
                                                [
                                                <TR key={"tr" + index} className={classRow}>
                                                    <TD className="visible-xs visible-sm">
                                                        <b className="show-info">
                                                            <i className="fa fa-angle-double-down" aria-hidden="true"></i>
                                                        </b>
                                                    </TD>
                                                    <TD className="text-center">
                                                       {index}
                                                    </TD>
                                                    <TD className="text-left" title={item.product_code}>
                                                            {item.product_code}
                                                    </TD>
                                                    <TD className="hidden-xs hidden-sm text-center"
                                                        title={item.store_product_id}>{item.store_product_id}
                                                    </TD>
                                                    <TD className="hidden-xs hidden-sm text-long-batch"
                                                        title={item.store_product_name}>{item.store_product_name}
                                                    </TD>
                                                    <TD className="text-center">
                                                        <CHECKBOX_CUSTOM
                                                            key={'df_handling[' + item.product_code +']'}
                                                            defaultChecked={item.df_handling === 1 ? true : false}
                                                            name={'df_handling[' + item.product_code +']'}
                                                            onChange={() => this.handleDfHhandling(item.product_code, item.df_handling)}
                                                            value={item.df_handling||0} />
                                                    </TD>
                                                    <TD className="hidden-xs hidden-sm"
                                                        title={item.product_jan}>{item.product_jan}
                                                    </TD>
                                                    <TD className="hidden-xs hidden-sm text-right"
                                                        title={nf.format(item.product_price)}>{nf.format(item.product_price)}
                                                    </TD>
                                                    <TD className="hidden-xs hidden-sm text-right"
                                                        title={nf.format(item.product_cost)}>{nf.format(item.product_cost)}
                                                    </TD>
                                                    <TD className="hidden-xs hidden-sm"
                                                        title={item.product_size}>{item.product_size}
                                                    </TD>
                                                    <TD className="hidden-xs hidden-sm"
                                                        title={item.product_color}>{item.product_color}
                                                    </TD>
                                                    <TD className="text-center">
                                                        { (item.is_updated !== 0 && item.is_updated !== 1) &&
                                                            <a className={ (item.is_updated !== 2 && item.is_updated !== 4) ? "padding-right-20" : ""}
                                                                onClick={() => this.handleProcessSmaregi('up', item.product_code)}
                                                            >
                                                                <i className="fa fa-cloud-upload fa-2x" style={{color: '#3097D1'}}></i>
                                                            </a>
                                                        }
                                                        { (item.is_updated !== 2 && item.is_updated !== 4) &&
                                                            <a
                                                                onClick={() => this.handleProcessSmaregi('delete', item.product_code)}
                                                            >
                                                                <i className="fa fa-trash fa-2x" style={{color: 'red'}}></i>
                                                            </a>
                                                        }
                                                    </TD>
                                                </TR>,
                                                <TR key={"trhide" + index} className="hiden-info" style={{display:"none"}}>
                                                    <TD colSpan="10" className="width-span1">
                                                        <ul id={"temp" + item.mail_id} >
                                                            <li> <b>NO</b> : #{index}</li>
                                                            <li> <b>{trans('messages.product_code')}</b> : {item.product_code}</li>
                                                            <li> <b>{trans('messages.store_product_id')}</b> : {item.store_product_id}</li>
                                                            <li> <b>{trans('messages.product_name')}</b> : {item.store_product_name}</li>
                                                            <li> <b>{trans('messages.store_product_jan')}</b> : {item.product_jan}</li>
                                                            <li> <b>{trans('messages.price')}</b> : {nf.format(item.product_price)}</li>
                                                            <li> <b>{trans('messages.product_cost')}</b> : {nf.format(item.product_cost)}</li>
                                                            <li> <b>{trans('messages.product_size')}</b> : {item.product_size}</li>
                                                            <li> <b>{trans('messages.product_color')}</b> : {item.product_color}</li>
                                                            <li> <b>{trans('messages.product_jan_new')}</b> : {item.product_jan_new}</li>
                                                        </ul>
                                                    </TD>
                                                </TR>
                                               ]
                                            )
                                        })
                                        :
                                        <TR><TD colSpan="17" className="text-center">{trans('messages.no_data')}</TD></TR>
                                    }
                                    </TBODY>
                                </TABLE>
                                </div>
                                <div className="text-center">
                                    <PaginationContainer handleClickPage={this.handleClickPage}/>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <PopupConfirmUp
                    handleSubmitAccept={this.handleSubmitPopup}
                    handleCancelAccept={this.handleCancelPopup}
                />
                <PopupConfirmDelete
                    handleSubmitAccept={this.handleSubmitPopup}
                    handleCancelAccept={this.handleCancelPopup}
                />
            </div>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        dataSource: state.commonReducer.dataSource,
        dataResponse: state.commonReducer.dataResponse
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        reloadData: (url, params, pushHistory, ignoreFields) => { dispatch(Action.reloadData(url, params, pushHistory, ['product_code', 'is_updated_all'])) },
        postData: (url, params, headers, callBackUrl, callBackParams) => { dispatch(Action.postData(url, params, headers, callBackUrl, callBackParams)) }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(StoreProductList)