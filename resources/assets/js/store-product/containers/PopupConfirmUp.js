import React, { Component } from 'react';
import Loading from '../../common/components/common/Loading';
export default class PopupConfirmUp extends Component {
    constructor(props) {
        super(props);
        this.state = {}
    }
    render() {
        return (
            <div className="modal fade" id="confirm-up-modal">
                <Loading className="loading" />
                <div className="modal-dialog ">
                    <div className="modal-content">
                        <div className="modal-header">
                            <b>{trans('messages.btn_accept')}</b>
                        </div>
                        <div className="modal-body modal-custom-body">
                            <div className="row">
                                <div className="col-md-12 message-popup">
                                    {trans('messages.smaregi_popup_up_message')}
                                </div>
                            </div>
                        </div>
                        <div className="modal-footer modal-custom-footer">
                            <button type="button" className="btn btn-primary btn-sm modal-custom-button" onClick={this.props.handleSubmitAccept}>{trans('messages.btn_accept_ok')}</button>
                            <button type="button" className="btn bg-purple btn-sm modal-custom-button" onClick={this.props.handleCancelAccept}>{trans('messages.btn_accept_cancel')}</button>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}