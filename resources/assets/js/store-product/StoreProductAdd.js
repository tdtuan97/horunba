import React, { Component } from 'react';

import { connect } from 'react-redux';
import * as Action from '../common/actions';
import * as queryString from 'query-string';
import axios from 'axios';

import BUTTON from '../common/components/form/BUTTON';
import FORM from '../common/components/form/FORM';
import INPUT from '../common/components/form/INPUT';
import CHECKBOX_CUSTOM from '../common/components/form/CHECKBOX_CUSTOM';
import SELECT from '../common/components/form/SELECT';
import LABEL from '../common/components/form/LABEL';

import TABLE from '../common/components/table/TABLE';
import THEAD from '../common/components/table/THEAD';
import TBODY from '../common/components/table/TBODY';
import TR from '../common/components/table/TR';
import TH from '../common/components/table/TH';
import TD from '../common/components/table/TD';

import SortComponent from '../common/components/table/SortComponent';
import PaginationContainer from '../common/containers/PaginationContainer'
import Loading from '../common/components/common/Loading';
import PopupConfirmUp from './containers/PopupConfirmUp';
import PopupConfirmDelete from './containers/PopupConfirmDelete';

var timer;
class StoreProductAdd extends Component {

    constructor(props) {
        super(props);
        this.state = {};
        this.props.reloadData(addNewUrl, this.state);
    }

    componentDidUpdate() {
        if (!_.isEmpty(this.props.dataSource)) {
            $('.loading').hide();
        }
    }

    componentWillReceiveProps(nextProps) {
        if (!_.isEmpty(nextProps.dataSource)) {
            if (!this.state.dataProduct) {
                let dataProduct = [];
                dataProduct.push({...nextProps.dataSource.default});
                    this.setState({dataProduct: dataProduct, default: nextProps.dataSource.default});
            }
        }
    }

    handleSubmit = (event) => {
        axios.post(
            addNewUrl,
            {dataProduct: this.state.dataProduct},
            {headers: {'X-Requested-With': 'XMLHttpRequest'}}
        )
        .then(response => {
            $(".loading").hide();
            if (response.data.status === 1) {
                $.notify({
                    icon: 'glyphicon glyphicon-warning-sign',
                    message: trans('messages.ajax_update_success'),
                },{
                    type: 'success',
                    delay: 2000
                });
                window.location.href = baseUrl;
            } else {
                let index = response.data.index;
                let currentDetail = this.state.dataProduct;
                if (typeof response.data.productMessage !== 'undefined') {
                    currentDetail[index]['productMessage'] = response.data.productMessage;
                }
                this.setState({dataProduct: currentDetail}, () => {
                    $.notify({
                        icon: 'glyphicon glyphicon-warning-sign',
                        message: response.data.message,
                    },{
                        type: 'danger',
                        delay: 2000
                    });
                });
            }

        })
        .catch(error => {
            $(".loading").hide();
            throw(error);
        });
    }

    handleAddNewProduct = () => {
        let dataProduct = this.state.dataProduct;
        dataProduct.push({...this.state.default});
            this.setState({dataProduct: dataProduct}, () => {
        });
    }

    handleDeleteProduct = (index, event) => {
        let dataProduct = this.state.dataProduct;
        dataProduct.splice(index, 1);
        this.setState({dataProduct: this.state.dataProduct}, () => {
        });
    }

    handleCancel = (event) => {
        window.location.href = baseUrl;
    }

    handleChangeProductDetail = (index, event) => {
        const target = event.target;
        const name = target.name;
        const value = target.value;
        let currentDetail = this.state.dataProduct;
        currentDetail[index][name] = value;
        this.setState({
            dataProduct: currentDetail
        }, () => {
            clearTimeout(timer);
            let _this = this;
            timer = setTimeout(function() {
                _this.getInfoProduct(index, currentDetail[index]);
            }, 1500);
        });
    }

    getInfoProduct = (index, currentData) => {
        $(".loading").show();
        let headers = {'X-Requested-With': 'XMLHttpRequest'};
        axios.post(getDataProductUrl, currentData, {headers : headers}
        ).then(response => {
            $(".loading").hide();
            let currentDetail = this.state.dataProduct;
            if (typeof response.data.productMessage !== 'undefined') {
                currentDetail[index]['productMessage'] = response.data.productMessage;
            } else {
                delete currentDetail[index]['productMessage'];
                currentDetail[index] = {...currentDetail[index], ...response.data.productInfo};
            }
            this.setState({dataProduct: currentDetail});
        }).catch(error => {
            $(".loading").hide();
            throw(error);
        });
    }

    render() {
        let dataSource = this.props.dataSource;
        let j          = 0;
        let length     = _.size(this.state.dataProduct);
        return (!_.isEmpty(this.props.dataSource) &&
            <div className="box box-primary">
            <div className="box-body">
            <div className="container-fluid">
            <FORM
                className="form-horizontal" id="return_detail">
                <Loading className="loading" />
                    <div className="row">
                        <div className="col-md-12 margin-form">
                        <div className="table-responsive">
                            <TABLE className="table table-custom" style={{ borderCollapse: "collapse" }}>
                                <THEAD>
                                    <TR>
                                        <TH className="text-center">{trans('messages.store_order_detail_no')}</TH>
                                        <TH className="text-center">{trans('messages.product_code')}</TH>
                                        <TH className="text-center">{trans('messages.product_name')}</TH>
                                        <TH className="text-center">{trans('messages.product_jan')}</TH>
                                        <TH className="text-center">{trans('messages.product_maker_code')}</TH>
                                        <TH className="text-center"></TH>
                                    </TR>
                                </THEAD>
                                <TBODY>
                                    {
                                        (!_.isEmpty(this.state.dataProduct)) &&
                                        this.state.dataProduct.map((item, index) => {
                                            j =  j + 1;
                                            return (
                                                <TR key={"product_detail"  + index}>
                                                    <TD className="text-center col-md-0-5" key={"no" + j}>
                                                        {j}
                                                    </TD>
                                                    <TD className="col-md-1-5">
                                                        <INPUT
                                                            key={"product_code" + index}
                                                            name="product_code"
                                                            className="form-control input-sm"
                                                            groupClass=" "
                                                            value={item.product_code || ''}
                                                            onChange={(e) => {this.handleChangeProductDetail(index, e)}}
                                                            errorPopup={true}
                                                            hasError={item.productMessage ? true:false}
                                                            errorMessage={item.productMessage?item.productMessage:""}
                                                            />
                                                    </TD>
                                                    <TD className="col-md-4-5 text-center">
                                                        <INPUT
                                                            key={"product_name" + index}
                                                            name="product_name"
                                                            className="form-control input-sm"
                                                            groupClass=" "
                                                            value={item.product_name_long || ''}
                                                            onChange={(e) => {this.handleChangeProductDetail(index, e)}}
                                                            errorPopup={true}
                                                            readOnly={true}
                                                            hasError={item.productMessage ? true:false}
                                                            errorMessage={item.productMessage?item.productMessage:""}
                                                            />
                                                    </TD>
                                                    <TD className="col-md-1-5 text-center">
                                                        <INPUT
                                                            key={"product_jan" + index}
                                                            name="product_jan"
                                                            className="form-control input-sm"
                                                            groupClass=" "
                                                            value={item.product_jan || ''}
                                                            onChange={(e) => {this.handleChangeProductDetail(index, e)}}
                                                            errorPopup={true}
                                                            hasError={item.productMessage ? true:false}
                                                            errorMessage={item.productMessage?item.productMessage:""}
                                                            />
                                                    </TD>
                                                    <TD className="col-md-1-5 text-center">
                                                        <INPUT
                                                            key={"product_maker_code" + index}
                                                            name="product_maker_code"
                                                            className="form-control input-sm"
                                                            groupClass=" "
                                                            value={item.product_maker_code || ''}
                                                            onChange={(e) => {this.handleChangeProductDetail(index, e)}}
                                                            errorPopup={true}
                                                            readOnly={true}
                                                            hasError={item.productMessage ? true:false}
                                                            errorMessage={item.productMessage?item.productMessage:""}
                                                            />
                                                    </TD>
                                                    <TD className="col-md-0-5 text-center">
                                                     { length !== 1 &&
                                                        <a onClick={(e) => this.handleDeleteProduct(index, e)}>
                                                            <i className="fa fa-trash fa-2x" style={{color: 'red'}}></i>
                                                        </a>
                                                    }
                                                    </TD>
                                                </TR>
                                            )
                                        })
                                    }
                                </TBODY>
                            </TABLE>
                        </div>
                        <div className="row">
                            <div className="col-md-12 margin-form">
                                <BUTTON
                                    className="btn btn-success input-sm btn-larger pull-right"
                                    type="button" onClick={this.handleAddNewProduct}
                                >
                                    {trans('messages.btn_add_product')}
                                </BUTTON>
                            </div>
                        </div>
                        <div className="row">
                            <div className="col-md-12 margin-form">
                                <BUTTON className="btn bg-purple input-sm pull-right" type="button" onClick={this.handleCancel}>{trans('messages.btn_back')}</BUTTON>
                                <BUTTON name="btnAccept" id="btnSubmit" className="btn btn-success input-sm btn-larger pull-right margin-element" type="button" onClick={this.handleSubmit}>{trans('messages.label_add')}</BUTTON>
                            </div>
                        </div>
                        </div>
                    </div>
            </FORM>
            </div>
            </div>
            </div>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        dataSource: state.commonReducer.dataSource,
        dataResponse: state.commonReducer.dataResponse
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        reloadData: (url, params, pushHistory, ignoreFields) => { dispatch(Action.reloadData(url, params, pushHistory, ['product_code', 'is_updated_all'])) },
        postData: (url, params, headers, callBackUrl, callBackParams) => { dispatch(Action.postData(url, params, headers, callBackUrl, callBackParams)) }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(StoreProductAdd)