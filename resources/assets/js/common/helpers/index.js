import * as queryString from 'query-string';
import createHistory from 'history/createBrowserHistory';
import axios from 'axios';
// Parse string to params
let data = null;
export const parseStringToParams = (stringSearch) => {
    let parsed = queryString.parse(stringSearch); 
    return parsed;    
};
// Build sort params
export const buildSortParams = (dataSource, column, nextSort) => {
    let params = {};    
    Object.keys(dataSource).map(key => {
        if (dataSource[key] !== "" && dataSource[key] !== null) {
            if (isJsonString(dataSource[key])) {
                params[key] = JSON.parse(dataSource[key]);
            } else {
                params[key] = dataSource[key];
            }                
        } else {
            delete params[key];
        }
    });
    if (nextSort === 'none') {
        delete params["sort"][column];
    } else if (params["sort"]) {
        Object.assign(params["sort"], {[column]: nextSort});
    } else {
        params["sort"] = {[column]: nextSort};
    } 
    return params; 
};
// Convert query string to object
export const convertQueryToObject = (searchString) => {
    let params = {};
    let parse  = queryString.parse(searchString);
    Object.keys(parse).map((key) => {
        let regex  = /([a-zA-Z0-9_]+)\[([a-zA-Z0-9_]*)\]/g;
        let result = regex.exec(key.trim());
        if (result !== null) {
            if (!params[result[1]]) {
                params[result[1]] = {[result[2]]: parse[key]};
            } else {
                params[result[1]][result[2]] = parse[key];
            }
        } else {
            params[key] = parse[key];
        }
    });
    return params;
};
// Convert object to array
export const convertObjectToArray = (object) => {
    let params = [];
    Object.keys(object).map((key) => {
        if (typeof object[key] === 'object') {
            Object.keys(object[key]).map((k) => {
                params.push(key + "[" + k + "]=" +  object[key][k]);
            });
        } else {
            params.push(key + "=" + object[key]);
        }
    });
    return params;
}
// Checking is json
export const isJsonString = (str) => {
    try {
        JSON.parse(str);
    } catch (e) {
        return false;
    }
    return true;
}

// Update url on address bar
export const updateAddressUrl = (params) => {
    const history = createHistory();
    history.push({
        search: '?' + params.join("&")
    });
}
export const realTimeUpdate = (url,params) => {
        axios.post(url, params, 
            {
                headers: {'X-Requested-With': 'XMLHttpRequest'}
            }
        ).then(response => {
            data = response.data;
        }).catch(error => {
            throw(error);
        }); 
    return data;
}

//export const realTimeUpdate = function(url,params) {
//   let data = null;
//            axios.post(url, params, 
//            {
//                headers: {'X-Requested-With': 'XMLHttpRequest'}
//            }
//            ).then(response => {
//               return response.data;
//                //callback(response.data);
//            }).then(function(parsedData) {
//                data = parsedData;
//            }).catch(error => {
//                throw(error);
//            })
// // )
//  return data;
//};