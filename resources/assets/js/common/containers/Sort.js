import React, { Component } from "react";
import * as helpers from '../helpers';
export default class Sort extends Component {
    constructor(props) {
        super(props);
    } 
    render() {
        let currentSort = (this.props.dataSource.sort[this.props.column_id] !== undefined) ? this.props.dataSource.sort[this.props.column_id] : 'none';
        let nextSort    = "asc";        
        let sort = (helpers.isJsonString(this.props.dataSource.sort) === false) ? this.props.dataSource.sort : JSON.parse(this.props.dataSource.sort);
        if (this.props.dataSource.sorted === true) {
            if (typeof sort[this.props.column_id] !== undefined ) {
                if (sort[this.props.column_id] === 'asc') {  
                    currentSort = 'asc';
                    nextSort    = "desc";  
                } else if (sort[this.props.column_id] === 'desc') {
                    currentSort = "desc";
                    nextSort    = "none";
                }
            }
        }
    return (
        <a href="javascript:void(0)" onClick={(e)=>this.props.handleSortClick(this.props.column_id, nextSort)}> 
            <span>{this.props.name}</span>
            <i className={(currentSort === "none")? "fa fa-arrows-v": (currentSort === "asc")?
                "fa fa-sort-amount-asc":
                "fa fa-sort-amount-desc"
            }>
            </i>
        </a>
    );
  }
}



