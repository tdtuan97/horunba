import React, { Component } from "react";
import { connect } from 'react-redux';
import * as Action from '../actions';
import _ from 'lodash';
import createHistory from 'history/createBrowserHistory'
import * as queryString from 'query-string';
import * as helpers from '../../common/helpers';
import UL from '../components/pagination/UL';
import LI from '../components/pagination/LI';
import A from '../components/pagination/A';

class PaginationContainer extends Component {

    constructor(props) {
        super(props);
    }

    calculatorPageList(totalItems, currentPage, itemsPerPage, totalPages) {
        currentPage     = currentPage   ||  1;
        itemsPerPage    = itemsPerPage  ||  20;
        var startPage, endPage;
        if (totalPages <= 10) {
            // less than 10 total pages so show all
            startPage = 1;
            endPage = totalPages;
        } else {
            // more than 10 total pages so calculate start and end pages
            if (currentPage <= 6) {
                startPage = 1;
                endPage = 10;
            } else if (currentPage + 4 >= totalPages) {
                startPage = totalPages - 9;
                endPage = totalPages;
            } else {
                startPage = currentPage - 5;
                endPage = currentPage + 4;
            }
        }
        // calculate start and end item indexes
        var startIndex = (currentPage - 1) * itemsPerPage;
        var endIndex = Math.min(startIndex + itemsPerPage - 1, totalItems - 1);
        // create an array of pages to ng-repeat in the paginator control
        var pages = _.range(startPage, endPage + 1);
        // return object with all paginator properties required by the view
        return {
            totalItems      : totalItems,
            currentPage     : currentPage,
            itemsPerPage    : itemsPerPage,
            totalPages      : totalPages,
            startPage       : startPage,
            endPage         : endPage,
            startIndex      : startIndex,
            endIndex        : endIndex,
            pages           : pages
        };
    }

    render() {
        const paginationData = (_.isEmpty(this.props.dataSource) === false) ? this.props.dataSource.data : '' ;
        let keyPagination = 0;
        let currentPage  = paginationData.current_page;
        let totalPages   = paginationData.last_page;
        let totalItems   = paginationData.total;
        let itemsPerPage = paginationData.per_page;
        let classDisL    = (currentPage === 1 ? 'disabled' : '');
        let classDisR    = (currentPage === totalPages ? 'disabled' : '');
        let paginator = this.calculatorPageList(totalItems, currentPage, itemsPerPage, totalPages);
        if (!paginator.pages || paginator.pages.length <= 1) {
            return null;
        }
        return ( (!_.isEmpty(paginationData)) &&
            <UL className="pagination">
                <LI className={classDisL}>
                <A  onClick={
                    (e) =>
                        (classDisL === 'disabled') ? e.preventDefault() : this.props.handleClickPage(1)} >
                    <span className="first-p">{(trans('messages.first_p')) ? trans('messages.first_p') :'<<' }</span>
                </A>
                </LI>
                <LI className={classDisL}>
                    <A  onClick={
                        (e) =>
                        (classDisL === 'disabled' ? e.preventDefault() : this.props.handleClickPage((currentPage !== 1 ? currentPage - 1 : 1)))
                    } >
                    <span className="prev-p">{'<'}</span>
                    </A>
                </LI>
                {
                    paginator.pages.map((page, index) => {
                        let classDisM    = (currentPage === page ? 'disabled' : '');
                        let pageLabel = page;

                        return (
                            <LI key={(keyPagination++)} className={(page===currentPage)? 'active' :''}>
                                <A className ={'page-item'} onClick={(e) => (classDisM === 'disabled' ? e.preventDefault() : this.props.handleClickPage(page, e))} >{pageLabel}</A>
                            </LI>
                        )
                    })
                }
                <LI className={classDisR}>
                    <A  onClick={
                        (e) =>
                        (classDisR === 'disabled' ? e.preventDefault() : this.props.handleClickPage((currentPage < totalPages ? currentPage + 1 : totalPages), e))
                        } >
                        <span className="next-p">{'>'}</span>
                    </A>
                </LI>
                <LI className={classDisR}>
                    <A onClick={(e) => (classDisR === 'disabled' ? e.preventDefault() : this.props.handleClickPage(totalPages, e))} >
                        <span className="last-p">{(trans('messages.last_p')) ? (trans('messages.last_p')) :'>>' }</span>
                    </A>
                </LI>
            </UL>
        )
    }
}

const mapStateToProps = (state) => {

  return {
    dataSource: state.commonReducer.dataSource
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    reloadData: (url, params) => { dispatch(Action.reloadData(url, params)) }
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(PaginationContainer)