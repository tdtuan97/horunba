import React, { Component } from "react";
import Loading from '../components/common/Loading';
import * as Action from '../actions';
import { connect } from 'react-redux';

import FORM from '../components/form/FORM';

class FormContainer extends Component {

    constructor(props) {
        super(props);
    }

    componentDidMount() {
        this.props.onRef(this)
    }
    componentWillUnmount() {
        this.props.onRef(null)
    }

    handleSubmit = (event) => {
        let headers = {'X-Requested-With': 'XMLHttpRequest'};
        if (this.props.enctype) {
            headers['Content-Type'] = this.props.enctype;
        }
        let formData = new FormData();
        if (this.state && Object.keys(this.state).length > 0) {
            Object.keys(this.state).map((item) => {
                formData.append(item, this.state[item]);
            });
        }
        this.props.submitFormData(this.props.baseUrl, formData, headers);
    }

    handleChange = (event) => {
        const target = event.target;
        const name = target.name;
        const value = target.type === 'checkbox' ? target.checked : target.value;
        let keyState = name;
        if (name.indexOf('[') !== -1) {
            let keyPart = name.split('[');
            let keyName = keyPart[0];
            let keyIndex = keyPart[1].substring(0, keyPart[1].indexOf(']'));
            keyState = keyName + "[" + keyIndex + "]";
        }
        if (target.type === 'file') {
            if (this.state !== undefined && this.state !== null) {
                Object.keys(this.state).map((item) => {
                    if (item.indexOf(keyState) !== -1) {
                        delete this.state[item];
                    }
                });
            }

            let i = 0;
            Object.keys(target.files).map((item) => {
                this.setState({
                    [keyState + "[" + i + "]"]: target.files[item]
                });
                i++;
            })
        } else {
            this.setState({
                [keyState]: value
            });
        }

    }

    render() {
        return <FORM
                    id={this.props.id}
                    name={this.props.name}
                    className={this.props.className}
                    action={this.props.action}
                    method={this.props.method}
                    >
                    {this.props.children}
                </FORM>
    }
}
const mapStateToProps = (state) => {
    return {
        dataSource: state.dataSource
    }
}
const mapDispatchToProps = (dispatch) => {
    return {
        submitFormData: (url, params, headers) => { dispatch(Action.submitFormData(url, params, headers)) }
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(FormContainer)