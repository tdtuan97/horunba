import React, { Component } from "react";
import Loading from '../components/common/Loading';
import * as Action from '../actions';
import { connect } from 'react-redux';
import { Redirect } from 'react-router-dom';

import FORM from '../components/search/FORM';
import GROUP from '../components/search/GROUP';
import LABEL from '../components/search/LABEL';
import INPUT from '../components/search/INPUT';
import BUTTON from '../components/search/BUTTON';
import ERROR from '../components/search/ERROR';

class Form extends Component {

    constructor(props) {
        super(props);
        let states = {};
        this.props.config.form.fields.map(field => {
            states[field.name] = field.defaultValue;
        })
        states.redirect = false;
        this.state = states;

    }

    handleSubmit = (event) => {
        let params = this.state;
        let dataSource = this.props.dataSource;
         this.props.submitFormData(this.props.config.url.baseUrl, params);
    }
    handleChange = (event) => {
        const target = event.target;
        const value = target.type === 'checkbox' ? target.checked : target.value;
        const name = target.name;
        this.setState({
            [name]: value
        });
    }
    render() {
        const configForm = this.props.config.form;
        let keyComponent = 0;
        let errorCommon = '';
        if (typeof this.props.dataSource.messageCommon !== 'undefined') {
            errorCommon = <ERROR errors={this.props.dataSource.messageCommon} />
        }
        if (typeof this.props.dataSource.redirect !== 'undefined' &&
            typeof this.props.dataSource.redirect) {
            window.location.reload();
        }
        return (
            <div className="row">
                <div className="col-xs-12">
                    <FORM className={configForm.formClass}>
                        {errorCommon}
                        {
                            configForm.fields.map(field => {
                                let currentField;
                                let Type = field.typeField;
                                let Field = field.name;
                                let error = '';
                                let checkError = '';
                                let child = (field.child)?field.child:null;
                                if (typeof this.props.dataSource.message !== 'undefined' &&
                                 typeof this.props.dataSource.message[Field] !== 'undefined') {
                                    error = <ERROR errors={this.props.dataSource.message[Field]} />;
                                    checkError = ' has-error';
                                }
                                currentField = (
                                    <div className="col-md-6" >
                                        <Type
                                            onChange={this.handleChange}
                                            className={field.fieldClass }
                                            name={field.name}
                                            type={field.type}
                                            value={this.state[Field]}
                                            placeholder={field.placeholder}>
                                            {child}
                                        </Type>
                                        {error}
                                    </div>
                                )
                                return (<GROUP key={(keyComponent++)} className={field.groupClass + checkError}>
                                    <LABEL className={field.classLabel}>{field.label}</LABEL>
                                    {currentField}
                                </GROUP>)
                            })
                        }
                        {
                            <GROUP className={configForm.submitButton.groupClass}>
                                <div className="col-md-6 col-md-offset-4">
                                    <BUTTON
                                        type={configForm.submitButton.type}
                                        className={configForm.submitButton.buttonClass}
                                        onClick={this.handleSubmit}
                                        >
                                        {configForm.submitButton.title}
                                    </BUTTON>
                                </div>
                            </GROUP>
                        }
                    </FORM>
                </div>
            </div>
        )
    }
}
const mapStateToProps = (state) => {

  return {
    dataSource: state.dataSource
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    submitFormData: (url, params) => { dispatch(Action.submitFormData(url, params)) }
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Form)