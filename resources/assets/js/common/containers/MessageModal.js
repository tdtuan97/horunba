import React, { Component } from 'react';
import PropTypes from 'prop-types';
export default class MessageModal extends Component {
    constructor(props) {
        super(props);
        this.state = { 'show': ''};

    }
    handleChange = (event) => {
        const target = event.target;
        const name = target.name;
        const value = target.value;
        this.setState({
            [name]: value
        });
    }
    componentDidMount() {
        if (!_.isEmpty(this.props.message)) {
            this.setState({ 'show': 'in'})
            $('#message-modal').modal('show');
        }
    }
    handleCancelClick = () => {
        $('#message-modal').modal('hide');
        window.location.href = this.props.baseUrl;
    }
    handleOnlyViewClick = () => {
        $('#message-modal').modal('hide');
        $("form :input:not(#btn-back-custom)").attr("disabled", true);
        $('form a').click(function(e) {
            e.preventDefault();
        });
    }

    render() {
        return (
            <div className={"modal fade " + this.state.show} id="message-modal" data-backdrop="static" data-keyboard="false">
                <div className="modal-dialog modal-custom">
                    <div className="modal-content modal-custom-content">
                        <div className="modal-header">
                            <h4 className="modal-title text-center"><b>{this.props.title}</b></h4>
                        </div>
                        <div className="modal-body modal-custom-body">
                            {this.props.message}
                        </div>
                        <div className="modal-footer modal-custom-footer">
                            <button type="button" className="btn bg-purple modal-custom-button pull-right" onClick={this.handleCancelClick} >{trans('messages.issue_modal_ok')}</button>
                            {this.props.allowView &&
                                <button type="button" className="btn btn-success input-sm modal-custom-button margin-element" onClick={this.handleOnlyViewClick} >{trans('messages.btn_only_view')}</button>
                            }
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}
