import React, { Component } from "react";
import ERROR from './ERROR';

export default class UPLOAD extends Component {
    constructor(props) {
        super(props)
    }
    handleClick = (event) => {
        document.getElementsByName("browser_" + this.props.name)[0].click();
    }

    render() {
        let {
            groupClass,
            errorClass,
            labelClass,
            labelTitle,
            fieldGroupClass,
            itemGroupClass,
            btnClass,
            btnTitle,
            btnClear,
            errorMessage,
            hasError,
            name,
            clear,
            onClick,
            ...other
        } = this.props;

        if (!hasError) {
            hasError = false;
        }
        if (!groupClass) {
            groupClass = "form-group";
        }
        if (!errorClass) {
            errorClass = "has-error";
        }
        if (!labelClass) {
            labelClass = "control-label";
        }
        if (!labelTitle) {
            labelTitle = "";
        }
        if (!fieldGroupClass) {
            fieldGroupClass = "";
        }
        if (!itemGroupClass) {
            itemGroupClass = "input-group";
        }
        if (!btnClass) {
            btnClass = "btn btn-info btn-flat";
        }
        if (!btnTitle) {
            btnTitle = "Browser";
        }
        if (!btnClear) {
            btnClear = "Clear";
        }

        if (!errorMessage) {
            errorMessage = "";
        } else {
            errorMessage = <ERROR>{errorMessage}</ERROR>
        }

        return (
            <div className={groupClass + ((hasError)?(" " + errorClass):"")}>
                <label className={labelClass}>{labelTitle}</label>
                <div className={fieldGroupClass}>
                    <div className={itemGroupClass}>
                        <input {...other} name={name} onChange={this.props.onChange} readOnly={clear ? true : false} />
                        <span className="input-group-btn">
                            {clear &&
                                <button type="button" className="btn btn-danger" onClick={this.props.onClick}>{btnClear}</button>
                            }
                            <button type="button" className={btnClass} onClick={this.handleClick}>{btnTitle}</button>
                            <input name={"browser_" + name} type="file" className="hidden" onChange={this.props.onChange}/>
                        </span>
                    </div>
                    {errorMessage}
                </div>
            </div>

        )
    }
}
