import React, { Component } from "react";
import ERROR from './ERROR';

export default class UPLOAD_CUSTOM extends Component {
    constructor(props) {
        super(props)
    }
    handleClick = (event) => {
        document.getElementsByName("browser_" + this.props.name)[0].click();
    }

    render() {
        let {
            groupClass,
            errorClass,
            labelClass,
            labelTitle,
            fieldGroupClass,
            itemGroupClass,
            btnClass,
            btnTitle,
            btnClear,
            errorMessage,
            hasError,
            name,
            clear,
            onClick,
            value,
            ...other
        } = this.props;

        if (!value) {
            value = '選択されていません';
        }
        if (!hasError) {
            hasError = false;
        }
        if (!groupClass) {
            groupClass = "form-group";
        }
        if (!errorClass) {
            errorClass = "has-error";
        }
        if (!labelClass) {
            labelClass = "control-label";
        }
        if (!labelTitle) {
            labelTitle = "";
        }
        if (!fieldGroupClass) {
            fieldGroupClass = "";
        }
        if (!itemGroupClass) {
            itemGroupClass = "input-group";
        }
        if (!btnClass) {
            btnClass = "btn btn-info btn-flat";
        }
        if (!btnTitle) {
            btnTitle = "Browser";
        }
        if (!btnClear) {
            btnClear = "Clear";
        }
        let textColor = '';
        if (!errorMessage) {
            errorMessage = "";
        } else {
            textColor = 'text-red';
            errorMessage = <ERROR>{errorMessage}</ERROR>
        }

        return (
            <div className={groupClass + ((hasError)?(" " + errorClass):"")}>
                <label className={labelClass}>{labelTitle}</label>
                <div className={fieldGroupClass}>
                    <div className={itemGroupClass}>
                        <span className="input-group-btn btn_attached_custom">
                            <button type="button" className={btnClass} onClick={this.handleClick}>{btnTitle}</button>
                            <input name={"browser_" + name} type="file" className="hidden" onChange={this.props.onChange}/>
                        </span>
                        <span className={"span_attached_custom " + textColor}>
                            {value}
                        </span>
                    </div>
                    {errorMessage}
                </div>
            </div>

        )
    }
}
