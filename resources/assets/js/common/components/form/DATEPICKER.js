import React, { Component } from "react";
import DatePicker from 'react-datepicker';
import 'react-datepicker/dist/react-datepicker.css';
import ERROR from './ERROR';

export default class DATEPICKER extends Component {
    render() {
        let {
            groupClass,
            errorClass,
            labelClass,
            labelTitle,
            fieldGroupClass,
            errorMessage,
            hasError,
            errorPopup,
            ...other
        } = this.props;
        if (!hasError) {
            hasError = false;
        }
        if (!groupClass) {
            groupClass = "form-group";
        }        
        if (!errorClass) {
            errorClass = "has-error";
        }
        if (!labelClass) {
            labelClass = "control-label";
        }
        if (!labelTitle) {
            labelTitle = "";
        }
        if (!fieldGroupClass) {
            fieldGroupClass = "";
        }
        let isSm = false;
        if({...other}.className && {...other}.className.indexOf('input-sm') !== -1) {
            isSm = true;
        }
        let component = null;
        if (hasError) {
            if (errorPopup) {
                component = (
                    <div className={fieldGroupClass + " has-feedback"}>
                            <DatePicker {...other}/>
                            <i 
                                className="fa fa-times text-red form-control-feedback icon-error"
                                data-placement="top"
                                data-toggle="tooltip"
                                data-original-title={errorMessage}
                            ></i>
                    </div>
                );
            } else {
                component = (
                    <div className={fieldGroupClass}>
                        <DatePicker {...other}/>
                        <ERROR>{errorMessage}</ERROR>
                    </div>
                );
            }
        } else {
            component = (
                <div className={fieldGroupClass}>
                    <DatePicker {...other}/>
                </div>
            );
        }
        
        return (
            <div className={groupClass + ((hasError)?(" " + errorClass):"")}>
                {(labelTitle) ? <label className={labelClass}>{labelTitle}</label> : ''}
                {component}
            </div>
        )
    }
    
    componentDidUpdate() {
        $('[data-toggle="tooltip"]').tooltip();
    }
}
