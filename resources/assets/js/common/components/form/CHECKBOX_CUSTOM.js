import React, { Component } from "react";

export default class CHECKBOX_CUSTOM extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <label className="control nice-checkbox">
                <input type="checkbox"
                    {...this.props}
                >
                    {this.props.children}
                </input>
                <div className="nice-icon-check"></div>
            </label>
        )
    }
}