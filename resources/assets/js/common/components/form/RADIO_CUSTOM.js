import React, { Component } from "react";

export default class RADIO_CUSTOM extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <label className="control control-radio">
                <input type="radio"
                    {...this.props}
                >
                    {this.props.children}
                </input>
                <div className="nice-icon-check"></div>
            </label>
        )
    }
}