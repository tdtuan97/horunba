import React, { Component } from "react";
import ERROR from './ERROR';

export default class TEXTAREA extends Component {
    render() {
        let {
            groupClass,
            errorClass,
            labelClass,
            labelTitle,
            fieldGroupClass,
            errorMessage,
            hasError,
            errorPopup,
            expandContent,
            isRequire,
            ...other
        } = this.props;

        if (!hasError) {
            hasError = false;
        }
        if (!groupClass) {
            groupClass = "form-group";
        }
        if (!errorClass) {
            errorClass = "has-error";
        }
        if (!labelClass) {
            labelClass = "control-label";
        }
        if (!labelTitle) {
            labelTitle = "";
        }
        if (!fieldGroupClass) {
            fieldGroupClass = "";
        }

        let isSm = false;
        if({...other}.className && {...other}.className.indexOf('input-sm') !== -1) {
            isSm = true;
        }
        let component = null;
        if (hasError) {
            if (errorPopup) {
                component = (
                    <div className={fieldGroupClass + " has-feedback"}>
                            {!_.has(expandContent) ? expandContent : ''}
                            <textarea {...other} />
                            <i
                                className="fa fa-times text-red form-control-feedback icon-error"
                                data-placement="top"
                                data-toggle="tooltip"
                                data-original-title={errorMessage}
                            ></i>
                    </div>
                );
            } else {
                component = (
                    <div className={fieldGroupClass}>
                        {!_.has(expandContent) ? expandContent : ''}
                        <textarea {...other} />
                        <ERROR>{errorMessage}</ERROR>
                    </div>
                );
            }
        } else {
            component = (
                <div className={fieldGroupClass}>
                     {!_.has(expandContent) ? expandContent : ''}
                    <textarea {...other} />
                </div>
            );
        }

        return (
            <div className={groupClass + ((hasError)?(" " + errorClass):"")}>
                {(labelTitle) ? <label className={labelClass}>{labelTitle}{isRequire && <span className="text-danger">＊</span>}</label> : ''}
                {component}
            </div>
        )
    }
}
