import React, { Component } from "react";

export default class IFRAME extends Component {
    render() {
        let {
            groupClass,
            errorClass,
            labelClass,
            labelTitle,
            fieldGroupClass,
            ...other
        } = this.props;

        if (!groupClass) {
            groupClass = "form-group";
        }
        if (!labelClass) {
            labelClass = "control-label";
        }
        if (!labelTitle) {
            labelTitle = "";
        }
        if (!fieldGroupClass) {
            fieldGroupClass = "";
        }
        let component = null;
        component = (
            <div className={fieldGroupClass}>
                <iframe {...other} />
            </div>
        );

        return (
            <div className={groupClass}>
                {(labelTitle) ? <label className={labelClass}>{labelTitle}</label> : ''}
                {component}
            </div>
        )
    }
}
