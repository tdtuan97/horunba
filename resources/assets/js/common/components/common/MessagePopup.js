import React, { Component } from "react";

export default class MessagePopup extends Component {
    componentWillReceiveProps(nextProps) {
        if (!this.props.manualHide) {
            setTimeout(() => $('.popup-error').removeClass("show"), 3000);
        }
    }
    render() {
        return (
            <div className={"popup-error fix-message collapse " + this.props.status}>
                <div className={"alert alert-dismissible " + this.props.classType}>
                    <button type="button" className="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h4><i className="icon fa fa-ban"></i> Alert!</h4>
                    <p className="text-new-line">{this.props.message}</p>
                </div>
            </div>
        )
    }
}
