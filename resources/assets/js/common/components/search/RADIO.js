import React, { Component } from "react";

export default class RADIO extends Component {
    render() {
        return (<input 
                    className={this.props.className} 
                    name={this.props.name}
                    type="radio" 
                    value={this.props.value}
                    checked={this.props.checked}
                    onChange={this.props.onChange}
                    defaultChecked={this.props.defaultChecked}
                />
        )
    }
}
