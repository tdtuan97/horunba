import React, { Component } from "react";

export default class OPTION extends Component {
    render() {
        return <option className={this.props.className} value={(this.props.value) ? this.props.value : ''}>{this.props.children}</option>
    }
}
