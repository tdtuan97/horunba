import React, { Component } from "react";

export default class CHECKBOX extends Component {
    render() {
        return (<input 
                    className={this.props.className}
                    name={this.props.name}
                    type="checkbox"
                    value={this.props.valule}
                    checked={this.props.checked}
                    onChange={this.props.onChange}
                />
        )
    }
}
