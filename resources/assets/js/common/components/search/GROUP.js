import React, { Component } from "react";

export default class GROUP extends Component {
    render() {
        return <div className={this.props.className}>{this.props.children}</div>;
    }
}
