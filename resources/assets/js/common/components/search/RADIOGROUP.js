import React, { Component } from "react";

export default class RADIOGROUP extends Component {
    render() {
        return <div className={this.props.className} onChange={this.props.onChange}>{this.props.children}</div>;
    }
}
