import React, { Component } from "react";

export default class SELECT extends Component {
    render() {
        return (<select 
                    className={this.props.className} 
                    multiple={this.props.multiple}
                    name={this.props.name}
                    id={this.props.id}
                    type={this.props.type}
                    defaultValue={this.props.defaultValue}
                    onChange={this.props.onChange}
                >
                    {this.props.children}
                </select>
        )
    }
}
