import React, { Component } from "react";

export default class INPUT extends Component {
    render() {
        return (                             
                <input 
                    className={this.props.className}
                    id={this.props.id}
                    name={this.props.name}
                    type={this.props.type}
                    value={this.props.value}
                    onChange={this.props.onChange}
                    placeholder={this.props.placeholder}
                />
        )
    }
}
