import React, { Component } from "react";

export default class GROUP_BLOCK extends Component {
    render() {
        return (
                <div className={this.props.parentDiv.className}>
                    <div className={this.props.className}>                   
                        {this.props.children}
                    </div>
                </div>
                );
    }
}
