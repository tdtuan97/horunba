import React, { Component } from "react";

export default class TEXTAREA extends Component {
    render() {
        return (<textarea 
                    className={this.props.className}
                    name={this.props.name}
                    type={this.props.type}
                    value={this.props.value}
                    onChange={this.props.onChange}
                    placeholder={this.props.placeholder}
                />)
    }
}
