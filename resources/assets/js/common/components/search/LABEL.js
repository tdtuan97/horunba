import React, { Component } from "react";

export default class LABEL extends Component {
    render() {
        return <label className={this.props.className}>{this.props.children}</label>;
    }
}
