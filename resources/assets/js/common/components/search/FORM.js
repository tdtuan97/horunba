import React, { Component } from "react";

export default class FORM extends Component {
    render() {
        return <form className={this.props.className} id={this.props.id}>{this.props.children}</form>;
    }
}
