import React, { Component } from "react";

export default class FORM extends Component {
    render() {
        return (<button
                    className={this.props.className} 
                    id={this.props.id} 
                    type="button" onClick={this.props.onClick}
                    >
                        {this.props.children}
                </button>)
    }
}
