import React, { Component } from "react";

export default class ERROR extends Component {
    render() {
        return (
                <span className="help-block">
                    <strong>{ this.props.errors }</strong>
                </span>
            )
    }
}
