import React, { Component } from "react";

export default class LI extends Component {
    render() {
        return <li className={this.props.className}>{this.props.children}</li>
    }
}