import React, { Component } from "react";

export default class A extends Component {
    render() {
        return <a href="javascript:void(0)" className={this.props.className} onClick={this.props.onClick}>{this.props.children}</a>
    }
}