import React, { Component } from "react";

export default class UL extends Component {
    render() {
        return <ul className={this.props.className}>{this.props.children}</ul>
    }
}
