import React, { Component } from "react";

export default class TBODY extends Component {
    render() {
        return <tbody {...this.props}>{this.props.children}</tbody>
    }
}
