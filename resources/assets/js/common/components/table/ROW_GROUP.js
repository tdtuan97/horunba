import React, { Component } from "react";

export default class ROW_GROUP extends Component {
    render() {
        return (
                <div className="form-group">
                    <label className={this.props.classNameLabel}>{this.props.label}</label>
                    <div className={this.props.classNameField}>
                        {this.props.contents}
                        {this.props.children}        
                    </div>
                </div>
                );
    }
}
