import React, { Component } from "react";

export default class SORTComponent extends Component {
    render() {
        let currentSort = (this.props.currentSort)?this.props.currentSort:'none';
        let nextSort = 'asc';
        if (currentSort === 'asc') {
            nextSort = 'desc';
        } else if (currentSort === 'desc') {
            nextSort = 'none';
        }
        return (
            <a href="javascript:void(0)" onClick={() => this.props.onClick(this.props.name, nextSort)}> 
                <span>{this.props.title}</span>
                <i className={(currentSort === "none")? "fa fa-arrows-v": (currentSort === "asc")?
                    "fa fa-sort-amount-asc":
                    "fa fa-sort-amount-desc"
                }>
                </i>
            </a>
        );
    }
}
