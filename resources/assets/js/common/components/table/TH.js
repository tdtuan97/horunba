import React, { Component } from "react";

export default class TH extends Component {
    render() {
        return <th {...this.props}>{this.props.children}</th>
    }
}
