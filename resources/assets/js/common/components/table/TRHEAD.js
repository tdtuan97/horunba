import React, { Component } from "react";

export default class TRHEAD extends Component {
    render() {
        return <tr className={this.props.className}>{this.props.children}</tr>
    }
}
