import React, { Component } from "react";

export default class TR extends Component {
    render() {
        return <tr {...this.props}>{this.props.children}</tr>
    }
}
