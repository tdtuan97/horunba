import React, { Component } from "react";

export default class SORT extends Component {
    render() {
        return <a href="javascript:void(0)" onClick={this.props.onClick}>{this.props.children} <i className={this.props.className}></i></a>
    }
}
