import React, { Component } from "react";

export default class THEAD extends Component {
    render() {
        return <thead {...this.props}>{this.props.children}</thead>
    }
}
