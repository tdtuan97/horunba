export const RELOAD_DATA = 'RELOAD_DATA';
export const SUBMIT_FORM = 'SUBMIT_FORM';
export const DETAIL = 'DETAIL';
export const POST_DATA = 'POST_DATA';
export const POST_REAL_TIME = 'POST_REAL_TIME';
export const POST_DATA_CSV = 'POST_DATA_CSV';