import {
    RELOAD_DATA,
    SUBMIT_FORM,
    DETAIL,
    POST_DATA,
    POST_REAL_TIME,
    POST_DATA_CSV,
} from '../constants/action-types';
import { combineReducers } from 'redux';

function dataSource(
    state = {},
    action) {
    switch (action.type) {
        case RELOAD_DATA:
            return action.dataSource;
        case SUBMIT_FORM:
            return action.dataSource;
        case DETAIL:
            return action.dataSource;
        default:
            return state;
    }
}

function dataResponse(
    state = {},
    action) {
    switch (action.type) {
        case POST_DATA:
            return action.dataResponse;
        default:
            return state;
    }
}
function dataRealTime(
    state = {},
    action) {
    switch (action.type) {
        case POST_REAL_TIME:
            return action.dataRealTime;
        default:
            return state;
    }
}

function dataResponseCsv(
    state = {},
    action) {
    switch (action.type) {
        case POST_DATA_CSV:
            return action.dataResponseCsv;
        default:
            return state;
    }
}

const rootReducer = combineReducers({
    dataSource,
    dataResponse,
    dataRealTime,
    dataResponseCsv
})

export default rootReducer;