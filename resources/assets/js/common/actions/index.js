import {
    RELOAD_DATA,
    SUBMIT_FORM,
    DETAIL,
    POST_DATA,
    POST_REAL_TIME,
    POST_DATA_CSV,
} from '../constants/action-types'
import axios from 'axios';
import * as queryString from 'query-string';
import createHistory from 'history/createBrowserHistory';

function reloadDataAction(dataSource) {
    return {
        type: RELOAD_DATA,
        dataSource,
    }
}

export function reloadData(url, params, pushHistory = false, ignoreFields = []) {
    if (pushHistory) {
        let tmpParams = {...params};
        if (ignoreFields.length > 0) {
            ignoreFields.map((item) => {
                if (item in tmpParams) {
                    delete tmpParams[item];
                }
            });
        }
        const history = createHistory();
        let param = {};
        Object.keys(tmpParams).map((item) => {
            if (typeof tmpParams[item] !== 'undefined') {
                if (typeof tmpParams[item] === 'array' || typeof tmpParams[item] === 'object') {
                    param[item] = tmpParams[item];
                } else {
                    param[item] = _.trim(tmpParams[item]);
                }
            }
        });
        history.push({
            search: '?' + queryString.stringify(param)
        });
    }
    $(".loading").show();
    return dispatch => {
        return axios.get(url, {
                headers: {'X-Requested-With': 'XMLHttpRequest'},
                params
            }
        ).then(response => {
            $(".loading").hide();
            $("div.box.hide-table").show();
            // Set data respone for dataSource
            dispatch(reloadDataAction(response.data));

        }).catch(error => {
            $(".loading").hide();
            throw(error);
        });
    }
}
/*----DETAIL----*/
function detailDataAction(dataSource) {
    return {
        type: DETAIL,
        dataSource,
    }
}

export function detailData(url, params) {
    $(".loading").show();
    return dispatch => {
        return axios.get(url, {
                headers: {'X-Requested-With': 'XMLHttpRequest'},
                params
            }
        ).then(response => {
            $(".loading").hide();
            // Set data respone for dataSource
            dispatch(detailDataAction(response.data));
        }).catch(error => {
            $(".loading").hide();
            throw(error);
        });
    }
}

/*----SUBMIT FORM----*/

function submitFormAction(dataSource) {
    return {
        type: SUBMIT_FORM,
        dataSource,
    }
}

export function submitFormData(url, params, headers = {}) {
    $(".loading").show();
    return dispatch => {
        return axios.post(url,params, {headers : headers}
        ).then(response => {
            $(".loading").hide();
            dispatch(submitFormAction(response.data));
        }).catch(error => {
            $(".loading").hide();
            throw(error);
        });
    }
}

/*----Post data----*/

function postDataAction(dataResponse) {
    return {
        type: POST_DATA,
        dataResponse,
    }
}

/*----Post real time----*/

function postDataRealTimeAction(dataRealTime) {
    return {
        type: POST_REAL_TIME,
        dataRealTime,
    }
}

/**
 * post data process
 * @param  string   url
 * @param  Object   params
 * @param  object   headers
 * @param  function callback
 * @param  array    callParam data for function callback
 * Note    If use funcion callback so data response must have status is 1
 */
export function postData(url, params, headers = {}, callback = null, callParam = null) {
    $(".loading").show();
    return dispatch => {
        return axios.post(url,params, {headers : headers}
        ).then(response => {
            $(".loading").hide();
            dispatch(postDataAction(response.data));
            if (response.data.status === 1 && typeof callback === "function") {
                callback.apply(null, callParam);
            }
        }).catch(error => {
            $(".loading").hide();
            dispatch(postDataAction({reload: true, url : url, params :params, headers :headers }));
        });
    }
}

/**
 * post data process
 * @param  string   url
 * @param  Object   params
 * @param  object   headers
 * @param  function callback
 * @param  array    callParam data for function callback
 * Note    If use funcion callback so data response must have status is 1
 */
export function postRealTime(url, params, headers = {}, callback = null, callParam = null) {
    return dispatch => {
        return axios.post(url,params, {headers : headers}
        ).then(response => {
            dispatch(postDataRealTimeAction(response.data));
            if (response.data.status === 1 && typeof callback === "function") {
                callback.apply(null, callParam);
            }
        }).catch(error => {
            throw(error);
        });
    }
}
/*----Post data----*/

function postDataCsvAction(dataResponseCsv) {
    return {
        type: POST_DATA_CSV,
        dataResponseCsv,
    }
}
/**
 * post data process
 * @param  string   url
 * @param  Object   params
 * @param  object   headers
 * @param  function callback
 * @param  array    callParam data for function callback
 * Note    If use funcion callback so data response must have status is 1
 */
export function postDataCSV(url, params, headers = {}, count_err = 0) {
    if (count_err !== 0) {
        params.set('count_err', count_err);
    }
    return dispatch => {
        return axios.post(url,params, {headers : headers}
        ).then(response => {
            $(".loading").hide();
            dispatch(postDataCsvAction(response.data));
        }).catch(error => {
            count_err = count_err + 1;
            dispatch(postDataCsvAction({reload: true, count_err: count_err, url : url, params :params, headers :headers }));
        });
    }
}