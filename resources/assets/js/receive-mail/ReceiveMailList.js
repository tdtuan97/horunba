import React, { Component } from 'react';

import { connect } from 'react-redux';
import * as Action from '../common/actions';
import * as queryString from 'query-string';

import BUTTON from '../common/components/form/BUTTON';
import FORM from '../common/components/form/FORM';
import INPUT from '../common/components/form/INPUT';
import SELECT from '../common/components/form/SELECT';
import LABEL from '../common/components/form/LABEL';
import DATEPICKER from '../common/components/form/DATEPICKER';
import CHECKBOX from '../common/components/form/CHECKBOX';

import TABLE from '../common/components/table/TABLE';
import THEAD from '../common/components/table/THEAD';
import TBODY from '../common/components/table/TBODY';
import TR from '../common/components/table/TR';
import TH from '../common/components/table/TH';
import TD from '../common/components/table/TD';
import moment from 'moment';
import PopupInformDelete from './containers/PopupInformDelete';
import SortComponent from '../common/components/table/SortComponent';
import PaginationContainer from '../common/containers/PaginationContainer'
import Loading from '../common/components/common/Loading';
import axios from 'axios';

class ReceiveMailList extends Component {

    constructor(props) {
        super(props);
        const stringParams = queryString.parse(props.location.search);
        this.state = {};
        this.curStrQuery = '';
        if (!_.isEmpty(stringParams)) {
            this.state = stringParams;
        }
        this.props.reloadData(dataListUrl, this.state);
    }

    handleChange = (event) => {
        const target = event.target;
        const value = target.type === 'checkbox' ? target.checked : target.value;
        const name = target.name;
        this.setState({
            [name]: (value)?value:undefined
        },() => {
            if (name === 'per_page' || name === 'status' || name === 'deparment') {
                this.handleReloadData();
            }
        });
    }
    handleProcessAll = () => {
        let searchIDs = $("input[name^='rec_mail_index_checkbox']:checkbox:checked").map(function(){
            return $(this).val();
          }).get();
        $('#access-memo-delete-modal').modal('hide');
        let headers = {'X-Requested-With': 'XMLHttpRequest'};
        axios.post(updateUrl, {name: 'update_status_4', 'rec_mail_index_checkbox' : searchIDs}, {headers : headers}
        ).then(response => {
            $(".loading").hide();
            $("input[name^='rec_mail_index_checkbox']:checkbox:checked").prop('checked', false);
            $("input[name='checkAllIds']:checkbox:checked").prop('checked', false);
            this.props.reloadData(dataListUrl, this.state, true);
        }).catch(error => {
            $(".loading").hide();
            throw(error);
        });
    }

    handleAccept = (event) => {
        const target = event.target;
        let value = target.value;
        let name = target.name;
        let searchIDs = $("input[name^='rec_mail_index_checkbox']:checkbox:checked").map(function(){
            return $(this).val();
        }).get();

        if(searchIDs.length > 0) {
            $('#access-memo-delete-modal').modal('show');
        }
    }
    handleCancelAccept = () => {
        $('#access-memo-delete-modal').modal('hide');
        $("input[name^='rec_mail_index_checkbox']:checkbox:checked").prop('checked', false);
        $("input[name='checkAllIds']:checkbox:checked").prop('checked', false);
    }
    handleChangeMultiSelect = (name, value, checked) => {
        let currentValue = this.state[name];
        if (typeof currentValue === 'undefined') {
            currentValue = [];
        } else if (!Array.isArray(this.state[name])) {
            currentValue = [this.state[name]];
        }
        if (Array.isArray(value)) {
            currentValue = [...value];
        } else {
            let index = currentValue.indexOf(value);
            if (checked === false) {
                if (index !== -1) {
                    currentValue.splice(index, 1);
                }
            } else {
                if (index === -1) {
                    currentValue.push(value);
                }
            }
        }
        this.setState({
            [name]: currentValue,
        });
    }

    handleClearClick = (event) => {
        Object.keys(this.state).map((key) => {
            if (key !== 'per_page') {
                delete this.state[key];
            }
        });
        this.handleReloadData();
    }

    handleReloadData = () => {
        if (this.state.page) {
            delete this.state.page;
        }
        this.props.reloadData(dataListUrl, this.state, true);
    }

    componentDidUpdate() {
        if (!_.isEmpty(this.props.dataSource)) {
            $('.loading').hide();
            $('.show-modal-content').customPopoverManual();
        }
        $('#access-memo-delete-modal').on('hidden.bs.modal', function (e) {
          $("input[name^='rec_mail_index_checkbox']:checkbox:checked").prop('checked', false);
        })
        $('#checkAllIds').click (function () {
             var checkedStatus = this.checked;
            $("input[name^='rec_mail_index_checkbox']:checkbox").each(function () {
                $(this).prop('checked', checkedStatus);
             });
        });
    }
    componentWillMount() {
        window.onpopstate = (event) => {
            location.reload();
        };
    }

    handleSortClick = (name, nextSort) => {
        Object.keys(this.state).map((key) => {
            if (key.startsWith('sort_')) {
                delete this.state[key];
            }
        });
        if (nextSort === 'none') {
            delete this.state[name];
            this.props.reloadData(dataListUrl, this.state, true);
        } else {
            this.setState({[name]: nextSort}, () => {
                this.props.reloadData(dataListUrl, this.state, true);
            });
        }

    }

    handleChangeDate = function(date, name, event) {
        let dateValue = moment(date);
        if (!dateValue.isValid()) {
            dateValue = undefined;
        } else {
            dateValue = dateValue.format('YYYY-MM-DD HH:mm:ss');
        }
        this.setState({
          [name]: dateValue,
        });
    }

    handleClickPage = (page) => {
        this.setState({
            page: page
        },() => {
            this.props.reloadData(dataListUrl, this.state, true);
        });
    }

    handleChangeInGridView = (event) => {
        $(".loading").show();
        const target = event.target;
        const value = target.value;
        const name = target.name;
        const rec_mail_index = target.dataset.target;
        let param = {name: name, rec_mail_index: rec_mail_index, value:value};
        let headers = {'X-Requested-With': 'XMLHttpRequest'};
        axios.post(updateUrl, param, {headers : headers}
        ).then(response => {
            $(".loading").hide();
            this.props.reloadData(dataListUrl, this.state);
        }).catch(error => {
            $(".loading").hide();
            throw(error);
        });
    }

    componentWillReceiveProps(nextProps) {
        let param = {};
        Object.keys(this.state).map((item) => {
            if (typeof this.state[item] !== 'undefined') {
                if (typeof this.state[item] === 'array' || typeof this.state[item] === 'object') {
                    param[item] = this.state[item];
                } else {
                    param[item] = _.trim(this.state[item]);
                }
            }
        });
        this.curStrQuery = queryString.stringify(param);
    }

    render() {
        let nf = new Intl.NumberFormat();
        let dataSource = this.props.dataSource;
        let index = 0;
        let totalItems = 0;
        let itemsPerPage = 20;
        if (!_.isEmpty(dataSource)) {
            index = dataSource.data.from - 1;
            totalItems   = dataSource.data.total;
            itemsPerPage = dataSource.data.per_page;
        }

        let arrDefault = [{key : '', value: '-----'}];
        let arrStatus = arrDefault.concat(dataSource.statusData);
        let arrDeparment = arrDefault.concat(dataSource.deparmentData);
        return ( (!_.isEmpty(dataSource)) &&
            <div>
                <Loading className="loading" />
                <div className="box">
                    <div className="box-header">
                        <div className="row">
                            <div className="col-md-12">
                                <h4>{trans('messages.search_title')}</h4>
                                <FORM>
                                    <div className="row">
                                        <SELECT
                                            id="status"
                                            name="status"
                                            groupClass="form-group col-md-2"
                                            labelTitle={trans('messages.receive_mail_status')}
                                            handleChangeMultiSelect={this.handleChangeMultiSelect}
                                            valueMulti={this.state.status||''}
                                            options={dataSource.statusData}
                                            multiple="multiple"
                                            className="form-control input-sm" />
                                        <SELECT
                                            id="deparment"
                                            name="deparment"
                                            groupClass="form-group col-md-2"
                                            labelTitle={trans('messages.receive_mail_deparment')}
                                            handleChangeMultiSelect={this.handleChangeMultiSelect}
                                            valueMulti={this.state.deparment||''}
                                            options={dataSource.deparmentData}
                                            multiple="multiple"
                                            className="form-control input-sm" />
                                        <INPUT
                                            name="incharge_person"
                                            groupClass="form-group col-md-2"
                                            labelTitle={trans('messages.receive_mail_incharge_person')}
                                            onChange={this.handleChange}
                                            value={this.state.incharge_person||''}
                                            className="form-control input-sm" />
                                        <INPUT
                                            name="mail_subject"
                                            groupClass="form-group col-md-3"
                                            labelTitle={trans('messages.receive_mail_subject')}
                                            onChange={this.handleChange}
                                            value={this.state.mail_subject||''}
                                            className="form-control input-sm" />
                                        <SELECT
                                            name="per_page"
                                            groupClass="form-group col-md-1 per-page pull-right-md"
                                            labelClass="pull-left"
                                            labelTitle={trans('messages.per_page')}
                                            onChange={this.handleChange}
                                            value={this.state.per_page||''}
                                            options={{20:20, 40:40, 60:60, 80:80, 100:100}}
                                            className="form-control input-sm" />
                                    </div>
                                    <div className="row">
                                        <DATEPICKER
                                            placeholderText="YYYY-MM-DD"
                                            dateFormat="YYYY-MM-DD"
                                            locale="ja"
                                            name="receive_date_from"
                                            groupClass="form-group col-md-2 date-time"
                                            labelTitle={trans('messages.receive_date_from')}
                                            className="form-control input-sm"
                                            selected={this.state.receive_date_from ? moment(this.state.receive_date_from) : null}
                                            onChange={(date, name, e) => this.handleChangeDate(date, 'receive_date_from', e)} />
                                        <div className="col-md-0-25 hidden-xs hidden-sm"><span className="to-from-child">～</span></div>
                                        <DATEPICKER
                                            placeholderText="YYYY-MM-DD"
                                            dateFormat="YYYY-MM-DD"
                                            locale="ja"
                                            name="receive_date_to"
                                            groupClass="form-group col-md-2 date-time"
                                            labelTitle={trans('messages.receive_date_to')}
                                            className="form-control input-sm"
                                            selected={this.state.receive_date_to ? moment(this.state.receive_date_to) : null}
                                            onChange={(date, name, e) => this.handleChangeDate(date, 'receive_date_to', e)} />
                                         <INPUT
                                            name="mail_from"
                                            groupClass="form-group col-md-3"
                                            labelTitle={trans('messages.receive_mail_from')}
                                            onChange={this.handleChange}
                                            value={this.state.mail_from||''}
                                            className="form-control input-sm" />
                                        <INPUT
                                            name="mail_to"
                                            groupClass="form-group col-md-2"
                                            labelTitle={trans('messages.receive_mail_to')}
                                            onChange={this.handleChange}
                                            value={this.state.mail_to||''}
                                            className="form-control input-sm" />
                                        <SELECT
                                            id="is_reply"
                                            name="is_reply"
                                            groupClass="form-group col-md-2"
                                            labelTitle={trans('messages.is_reply')}
                                            handleChangeMultiSelect={this.handleChangeMultiSelect}
                                            valueMulti={this.state.is_reply||''}
                                            options={dataSource.isReplyConf}
                                            multiple="multiple"
                                            className="form-control input-sm" />
                                    </div>
                                </FORM>
                            </div>
                        </div>
                        <div className="row">
                            <div className="col-md-9 col-xs-4">
                                <a href={EditSendUrl} className="btn btn-success input-sm">{trans('messages.btn_move_send_mail')}</a>
                            </div>
                            <div className="col-md-3 col-xs-8">
                                <BUTTON className="btn btn-danger pull-right" onClick={this.handleClearClick}>{trans('messages.btn_reset')}</BUTTON>
                                <BUTTON className="btn btn-primary pull-right margin-element" onClick={this.handleReloadData} >{trans('messages.btn_search')}</BUTTON>
                            </div>
                        </div>
                    </div>
                    <div className="box-body">
                        <div className="row">
                            <div className="col-md-12">
                                {(totalItems/itemsPerPage > 1) &&
                                <div className="row">
                                    <div className="col-md-2-5"></div>
                                    <div className="col-md-7 text-center">
                                        <PaginationContainer handleClickPage={this.handleClickPage}/>
                                    </div>
                                    <div className="col-md-2-5 text-right line-height-md">
                                        {dataSource.data.from}件 - {dataSource.data.to}件 【全{dataSource.data.total}件】
                                    </div>
                                </div>
                                }
                                <div className="table-responsive">
                                <TABLE className="table table-striped table-custom">
                                    <THEAD>
                                        <TR>
                                            <TH colSpan="1" className="text-center no-attr">
                                                <button type="button" name="delete_all" className="btn btn-box-tool btn-remove"
                                                data-target="#access-memo-delete-modal" data-backdrop="static"
                                                onClick={(e) => this.handleAccept(e)}>
                                                    <i className="fa fa-trash-o"></i>
                                                </button>
                                            </TH>
                                        </TR>
                                        <TR>
                                            <TH className="visible-xs visible-sm"></TH>
                                            <TH className="text-center col-max-min-100">
                                                <label className="control nice-checkbox">
                                                    <input type="checkbox" name="checkAllIds" id="checkAllIds" value="1" />
                                                    <div className="nice-icon-check"></div>
                                                </label>
                                            </TH>
                                            <TH className="text-center">
                                                <SortComponent
                                                    name="sort_rec_mail_index"
                                                    onClick={this.handleSortClick}
                                                    currentSort={(this.state.sort_rec_mail_index)?this.state.sort_rec_mail_index:'none'}
                                                    title={trans('messages.index')} />
                                            </TH>
                                            <TH className="hidden-xs hidden-sm text-center col-max-min-120">
                                                <SortComponent
                                                    name="sort_status"
                                                    onClick={this.handleSortClick}
                                                    currentSort={(this.state.sort_status)?this.state.sort_status:'none'}
                                                    title={trans('messages.receive_mail_status')} />
                                            </TH>
                                            <TH className="hidden-xs hidden-sm text-center col-max-min-120">
                                                <SortComponent
                                                    name="sort_deparment"
                                                    onClick={this.handleSortClick}
                                                    currentSort={(this.state.sort_deparment)?this.state.sort_deparment:'none'}
                                                    title={trans('messages.receive_mail_deparment')} />
                                            </TH>
                                            <TH className="hidden-xs hidden-sm text-center col-max-min-120">
                                                <SortComponent
                                                    name="sort_incharge_person"
                                                    onClick={this.handleSortClick}
                                                    currentSort={(this.state.sort_incharge_person)?this.state.sort_incharge_person:'none'}
                                                    title={trans('messages.receive_mail_incharge_person')} />
                                            </TH>
                                            <TH className="text-center col-max-min-100">
                                                <SortComponent
                                                    name="sort_mail_subject"
                                                    onClick={this.handleSortClick}
                                                    currentSort={(this.state.sort_mail_subject)?this.state.sort_mail_subject:'none'}
                                                    title={trans('messages.receive_mail_subject')} />
                                            </TH>
                                            <TH className="text-center col-max-min-200">
                                                <SortComponent
                                                    name="sort_mail_content"
                                                    onClick={this.handleSortClick}
                                                    currentSort={(this.state.sort_mail_content)?this.state.sort_mail_content:'none'}
                                                    title={trans('messages.mail_content')} />
                                            </TH>
                                            <TH className="hidden-xs hidden-sm text-center col-max-min-130">
                                                <SortComponent
                                                    name="sort_receive_date"
                                                    onClick={this.handleSortClick}
                                                    currentSort={(this.state.sort_receive_date)?this.state.sort_receive_date:'none'}
                                                    title={trans('messages.receive_date')} />
                                            </TH>
                                            <TH className="text-center col-max-min-100">
                                                <SortComponent
                                                    name="sort_mail_from"
                                                    onClick={this.handleSortClick}
                                                    currentSort={(this.state.sort_mail_from)?this.state.sort_mail_from:'none'}
                                                    title={trans('messages.receive_mail_from')} />
                                            </TH>
                                        </TR>
                                    </THEAD>
                                    <TBODY>
                                        {
                                            (!_.isEmpty(dataSource.data.data))?
                                            dataSource.data.data.map((item) => {
                                                index++;
                                                let formatReceiveDate = !_.isEmpty(item.receive_date) ? moment(item.receive_date).format("YYYY/MM/DD HH:mm:ss") : '';
                                                let classRow = (index%2 ===0) ? 'odd' : 'even';
                                                let classReply = item.is_reply === 1 ? 'replied' : '';
                                                let content  = null;
                                                if (item.mail_content !== null) {
                                                    if (item.mail_content.match('^(<html>|<!DOCTYPE|<!doctype)')) {
                                                        content  = item.mail_content.replace(/(?:\r\n|\r|\n)/g, '') ;
                                                    } else {
                                                        content  =  item.mail_content.replace(/(?:\r\n|\r|\n)/g, '<br>');
                                                    }
                                                }
                                                content  =  content.replace(/(?:'br'\/|br \/|br ")/g, 'br');
                                                let listContent = content.split("<br>").filter(x => x);
                                                let xhtml = '';
                                                listContent.map((itemv) => {
                                                    if (itemv.startsWith(">")) {
                                                        itemv = itemv.replace('>', '&gt;')
                                                    }
                                                    xhtml += itemv + '<br>';
                                                });
                                                xhtml = xhtml.replace(/<br>{2,}/g, '<br>');
                                                return (
                                                    [
                                                    <TR key={"tr" + index} className={classRow + ' ' + classReply}>
                                                        <TD className="visible-xs visible-sm text-center">
                                                            <b className="show-info">
                                                                <i className="fa fa-angle-double-down" aria-hidden="true"></i>
                                                            </b>
                                                        </TD>
                                                        <TD className="text-center">
                                                            <label className="control nice-checkbox">
                                                                <CHECKBOX
                                                                    name={'rec_mail_index_checkbox['+item.rec_mail_index+']'}
                                                                    key={'rec_mail_index_checkbox' + item.rec_mail_index}
                                                                    value={item.rec_mail_index}
                                                                    data-index={item.rec_mail_index_checkbox}
                                                                />
                                                                <div className="nice-icon-check"></div>
                                                            </label>
                                                        </TD>
                                                        <TD className="text-center" title={item.rec_mail_index}>
                                                            <a className="link-detail" href={saveUrl + "?" + (this.curStrQuery?(this.curStrQuery+"&"):"") + "rec_mail_index_key=" + item.rec_mail_index}>
                                                                {item.rec_mail_index}
                                                            </a>
                                                        </TD>
                                                        <TD className="hidden-xs hidden-sm " title={item.status}>
                                                            <SELECT
                                                                name="status_list"
                                                                data-target={item.rec_mail_index}
                                                                groupClass=" "
                                                                value={item.status||''}
                                                                options={arrStatus}
                                                                onChange={this.handleChangeInGridView}
                                                                className="form-control input-sm" />
                                                        </TD>
                                                        <TD className="hidden-xs hidden-sm" title={item.deparment}>
                                                            <SELECT
                                                                name="deparment_list"
                                                                data-target={item.rec_mail_index}
                                                                groupClass=" "
                                                                value={item.deparment||''}
                                                                options={arrDeparment}
                                                                onChange={this.handleChangeInGridView}
                                                                className="form-control input-sm" />
                                                        </TD>
                                                        <TD className="hidden-xs hidden-sm" title={item.incharge_person}>
                                                            <SELECT
                                                                name="incharge_person_list"
                                                                data-target={item.rec_mail_index}
                                                                groupClass=" "
                                                                value={item.incharge_person||''}
                                                                options={dataSource.tantou}
                                                                onChange={this.handleChangeInGridView}
                                                                className="form-control input-sm" />
                                                        </TD>
                                                        <TD className="cut-text text-long-batch max-w-200" title={item.mail_subject}>{item.mail_subject}</TD>
                                                        <TD className="max-w-200">
                                                            <span className="cut-text limit-char max-w-200 show-modal-content"  data-id={item.rec_mail_index} data-title={item.mail_subject}
                                                            data-content={xhtml}
                                                            data-toggle="popover"
                                                            data-trigger="forcus">
                                                                {item.mail_content}
                                                            </span>
                                                        </TD>
                                                        <TD className="hidden-xs hidden-sm text-center" title={formatReceiveDate}>{formatReceiveDate}</TD>
                                                        <TD title={item.mail_from}>{item.mail_from}</TD>

                                                    </TR>,
                                                    <TR key={"trhide" + index} className="hiden-info" style={{display:"none"}}>
                                                        <TD colSpan="10" className="width-span1">
                                                            <ul id={"temp" + item.rec_mail_index} >
                                                                <li> <b>{trans('messages.receive_mail_status')}</b> : {item.status}</li>
                                                                <li> <b>{trans('messages.receive_mail_deparment')}</b> : {item.deparment}</li>
                                                                <li> <b>{trans('messages.receive_mail_incharge_person')}</b> : {item.incharge_person}</li>
                                                                <li> <b>{trans('messages.receive_mail_subject')}</b> : {item.mail_subject}</li>
                                                                <li> <b>{trans('messages.receive_date')}</b> : {formatReceiveDate}</li>
                                                                <li> <b>{trans('messages.receive_mail_from')}</b> : {item.mail_from}</li>
                                                            </ul>
                                                        </TD>
                                                        </TR>
                                                   ]

                                                )
                                                if (index === dataSource.data.to) {
                                                   index = 0;
                                                }
                                            })
                                            :
                                            <TR><TD colSpan="12" className="text-center">{trans('messages.no_data')}</TD></TR>
                                        }
                                    </TBODY>
                                </TABLE>
                                </div>
                                <div className="text-center">
                                    <PaginationContainer handleClickPage={this.handleClickPage}/>
                                </div>
                                <PopupInformDelete  handleProcessAll={this.handleProcessAll} handleCancelAccept={this.handleCancelAccept} />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        dataSource: state.commonReducer.dataSource,
        dataResponse: state.commonReducer.dataResponse
    }
}

const mapDispatchToProps = (dispatch) => {
    let ignore = ['acccept_name', 'rec_mail_index_checkbox'];
    return {
        reloadData: (url, params, pushHistory, ignoreFields) => { dispatch(Action.reloadData(url, params, pushHistory, ignore)) },
        postData: (url, params, headers, callback) => { dispatch(Action.postData(url, params, headers, callback)) }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(ReceiveMailList)