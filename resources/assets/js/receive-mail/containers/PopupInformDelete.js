import React, { Component } from 'react';
import Loading from '../../common/components/common/Loading';
export default class PopupInformDelete extends Component {
    constructor(props) {
        super(props);
        this.state = {}
    }
    render() {
        return (
            <div className="modal fade" id="access-memo-delete-modal">
                <Loading className="loading" />
                <div className="modal-dialog ">
                    <div className="modal-content">
                        <div className="modal-header">
                            <b>{trans('messages.btn_accept')}</b>
                        </div>
                        <div className="modal-body modal-custom-body">
                            <div className="row">
                                <div className="col-md-12 message-popup">
                                    {trans('messages.inform_delete_receive_mail')}
                                </div>
                            </div>
                        </div>
                        <div className="modal-footer modal-custom-footer">
                            <button type="button" className="btn btn-primary btn-sm modal-custom-button" onClick={this.props.handleProcessAll}>{trans('messages.btn_accept_ok')}</button>
                            <button type="button" className="btn bg-purple btn-sm modal-custom-button" onClick={this.props.handleCancelAccept}>{trans('messages.btn_accept_cancel')}</button>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}