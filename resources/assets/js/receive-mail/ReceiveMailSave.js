import React, { Component } from 'react';
import { connect } from 'react-redux';

import * as Action from '../common/actions';
import * as queryString from 'query-string';
import moment from 'moment';
import axios from 'axios';

import Loading from '../common/components/common/Loading';
import FORM from '../common/components/form/FORM';
import INPUT from '../common/components/form/INPUT';
import TEXTAREA from '../common/components/form/TEXTAREA';
import SELECT from '../common/components/form/SELECT';
import BUTTON from '../common/components/form/BUTTON';
import LABEL from '../common/components/form/LABEL';
import MESSAGE_MODAL from '../common/containers/MessageModal';
import MessagePopup from '../common/components/common/MessagePopup';
import EDITOR from '../common/components/form/EDITOR';

class ReceiveMailSave extends Component {
    constructor(props) {
        super(props);
        this.state = {}
        this.firstRender = true;
        let params = queryString.parse(props.location.search);
        if (params['rec_mail_index_key']) {
            this.state.rec_mail_index = params['rec_mail_index_key'];
            this.state.accessFlg = true;
            this.hanleRealtime();
        }
        this.detailWindow = null;
        this.props.reloadData(getFormDataUrl, this.state);
    }

    handleCancel = (event) => {
        window.location.href = previousUrl;
    }

    hanleRealtime() {
        if (this.state.rec_mail_index) {
            let params = {
                accessFlg : this.state.accessFlg,
                query : {
                        page_key : 'RECEIVE-MAIL-SAVE',
                        primary_key : 'index:' + this.state.rec_mail_index
                    },
                key :this.state.rec_mail_index
            };
            if (this.state.accessFlg !== false) {
                this.props.postRealTime(checkEditUrl, params, {'X-Requested-With': 'XMLHttpRequest'});
            }
        }
    }

    handleSubmit = (event) => {
        document.getElementsByName('btnAccept')[0].disabled = true;
        let headers = {'X-Requested-With': 'XMLHttpRequest'};

        this.props.postData(saveUrl, this.state, headers);
    }

    handleChange = (event) => {
        const target = event.target;
        const name = target.name;
        const value = target.type === 'checkbox' ? (target.checked?1:0) : target.value;
        this.setState({
            [name]: value
        });
    }

    componentWillUnmount () {
        clearInterval(this.state);
    }

    componentDidMount() {
        setInterval(() => {
            this.hanleRealtime()
        }, 5000);
    }

    componentWillReceiveProps(nextProps) {
        if (!_.isEmpty(nextProps.dataSource)) {
            if (!_.isEmpty(nextProps.dataSource.data)) {
                if (this.firstRender) {
                    this.firstRender = false;
                    this.setState(nextProps.dataSource.data);
                }

            } else {
                window.location.href = baseUrl;
            }
        }

        if (!_.isEmpty(nextProps.dataResponse)) {
            if (nextProps.dataResponse.status === 1) {
                window.location.href = previousUrl;
            } else {
                document.getElementsByName('btnAccept')[0].disabled = false;
            }
        }

        // Check realtime update
        if (!_.isEmpty(nextProps.dataRealTime)) {
            this.setState(nextProps.dataRealTime);
        }
    }

    componentDidUpdate() {
        if (!_.isEmpty(this.props.dataSource)) {
            $('.loading').hide();
        }
        if (this.state.accessFlg === false) {
            $('#message-modal').modal('show');
            //We will auto redirect after 5 seconds
            setTimeout(() => {
                window.location.href = baseUrl;
            }, 5000);
        }
    }

    handleOpenPage = (page) => {
        if (page === 'edit_send_mail') {
            let param = "?rec_mail_index=" + this.state.rec_mail_index;
            let checkNull = _.isNull(this.state.receive_order_id);
            if (this.state.receive_order_id !== '' && checkNull === false) {
                let params = {receive_order_id: this.state.receive_order_id, type: page}
                $(".loading").show();
                axios.post(getOrderUrl, params, {headers: {'X-Requested-With': 'XMLHttpRequest'}}
                ).then(response => {
                    if (response.data.status === 0) {
                        this.setState({msg_receive:response.data.message});
                    } else {
                        this.setState({msg_receive:""});
                        let data = response.data.data;
                        if (page === 'order_detail') {
                            window.location.href = orderDetailUrl + "?receive_id_key=" + data.receive_id;
                        } else if (page === 'edit_send_mail') {
                            window.location.href = sendMailUrl + "?receive_order_id=" + this.state.receive_order_id + "&rec_mail_index=" + this.state.rec_mail_index;
                        }

                    }
                    $(".loading").hide();
                }).catch(error => {
                    $(".loading").hide();
                    throw(error);
                });
            } else {
                window.location.href = sendMailUrl + param;
            }
        } else {
            let params = {receive_order_id: this.state.receive_order_id, type: page}
            $(".loading").show();
            axios.post(getOrderUrl, params, {headers: {'X-Requested-With': 'XMLHttpRequest'}}
            ).then(response => {
                if (response.data.status === 0) {

                    this.setState({msg_receive: response.data.message});
                } else {
                    this.setState({msg_receive:""});
                    let data = response.data.data;
                    if (page === 'order_detail') {
                        this.detailWindow = window.open(orderDetailUrl + "?receive_id_key=" + data.receive_id, 'detailWindow');
                    } else if (page === 'edit_send_mail') {
                        window.location.href = sendMailUrl + "?receive_order_id=" + this.state.receive_order_id;
                    }

                }
                $(".loading").hide();
            }).catch(error => {
                $(".loading").hide();
                throw(error);
            });
            if (this.detailWindow !== null) {
                this.detailWindow.focus();
            }
        }
    }

    render() {
        let dataSource   = this.props.dataSource;
        let dataResponse = this.props.dataResponse;
        let formatReceiveDate = !_.isEmpty(this.state.receive_date) ? moment(this.state.receive_date).format("YYYY/MM/DD HH:mm:ss") : '';
        let isErrReceiveOrderId = false;
        let msgReceiveOrderId = '';
        if (dataResponse.message) {
            if (dataResponse.message.receive_order_id) {
                isErrReceiveOrderId = true;
                msgReceiveOrderId = dataResponse.message.receive_order_id;
            }
        } else if (this.state.msg_receive) {
            isErrReceiveOrderId = true;
            msgReceiveOrderId = this.state.msg_receive;
        }
        return ((!_.isEmpty(dataSource) && (!_.isEmpty(dataSource.data))) &&
            <div>
            <FORM
                className="form-horizontal padding-form-small">
                <Loading className="loading" />
                <div className="row">
                    <div className="col-md-7">
                        <INPUT
                            name="mail_subject"
                            labelTitle={trans('messages.mail_subject')}
                            className="form-control input-sm"
                            fieldGroupClass="col-md-9"
                            labelClass="col-md-2"
                            onChange={this.handleChange}
                            value={this.state.mail_subject || ''}
                            readOnly={true}/>
                        <EDITOR
                            apiKey='sm3yi664b07edp6t6hf1trx3xy6ziv5xg41muurzssvy4773'
                            init={{
                                force_br_newlines : true,
                                force_p_newlines : false,
                                forced_root_block : '',
                                branding: false,
                                menubar: false,
                                toolbar: false,
                                height : 350
                            }}
                            name="mail_content"
                            labelTitle={trans('messages.receive_mail_content')}
                            className="form-control input-sm"
                            fieldGroupClass="col-md-9"
                            labelClass="col-md-2"
                            value={dataSource.data.mail_content || ''}
                        />
                        <div id="group-mail-reply">
                        {
                            (!_.isEmpty(dataSource.dataReply))?
                            dataSource.dataReply.map((item) => {
                                let id = item.receive_order_id + item.order_status_id + item.order_sub_status_id + item.operater_send_index;
                                return ([
                                    <div className="form-group">
                                        <label className="col-md-2 label-reply-mail">
                                            {trans('messages.mail_subject')}
                                        </label>
                                        <a
                                            className="link-mail-reply  col-md-9"
                                            data-toggle="collapse"
                                            data-target={"#" + id}
                                            data-parent="#group-mail-reply"
                                        >
                                            {item.mail_subject}
                                        </a>
                                    </div>,
                                    <div id={id} className="collapse form-group">
                                        <EDITOR
                                            apiKey='sm3yi664b07edp6t6hf1trx3xy6ziv5xg41muurzssvy4773'
                                            init={{
                                                force_br_newlines : true,
                                                force_p_newlines : false,
                                                forced_root_block : '',
                                                branding: false,
                                                menubar: false,
                                                toolbar: false,
                                                height : 350
                                            }}
                                            name="mail_content"
                                            labelTitle=" "
                                            groupClass=" "
                                            className="form-control input-sm"
                                            fieldGroupClass="col-md-9"
                                            labelClass="col-md-2"
                                            value={item.mail_content || ''}
                                        />
                                    </div>
                                ])
                            })
                            :
                            ""
                        }
                        </div>
                        <INPUT
                            name="attached_file_path1"
                            labelTitle={trans('messages.attached_file_path')}
                            className="form-control input-sm input-link"
                            fieldGroupClass="col-md-9"
                            labelClass="col-md-2"
                            onClick={() => {this.state.attached_file_path1 ? window.open(this.state.attached_file_path1, '_blank') : ''}}
                            value={this.state.attached_file_path1 || ''}
                            readOnly={true}/>
                        <INPUT
                            name="attached_file_path2"
                            labelTitle=" "
                            className="form-control input-sm input-link"
                            fieldGroupClass="col-md-9"
                            labelClass="col-md-2"
                            onClick={() => {this.state.attached_file_path2 ? window.open(this.state.attached_file_path2, '_blank') : ''}}
                            value={this.state.attached_file_path2 || ''}
                            readOnly={true}/>
                        <INPUT
                            name="attached_file_path3"
                            labelTitle=" "
                            className="form-control input-sm input-link"
                            fieldGroupClass="col-md-9"
                            labelClass="col-md-2"
                            onClick={() => {this.state.attached_file_path3 ? window.open(this.state.attached_file_path3, '_blank') : ''}}
                            value={this.state.attached_file_path3 || ''}
                            readOnly={true}/>
                    </div>
                    <div className="col-md-4-75">
                        <div className="row">
                            <div className="col-md-8 col-xs-8">
                                <INPUT
                                    name="receive_date"
                                    labelTitle={trans('messages.receive_date')}
                                    className="form-control input-sm"
                                    fieldGroupClass="col-md-8 col-xs-8"
                                    labelClass="col-md-4 col-xs-4"
                                    groupClass="form-group"
                                    readOnly
                                    value={formatReceiveDate || ''}
                                    />
                            </div>
                            <div className="col-md-4 col-xs-4">
                                <BUTTON
                                    onClick={() => this.handleOpenPage('edit_send_mail')}
                                    type="button"
                                    className="btn btn-primary input-sm btn-larger pull-right">
                                    {trans('messages.btn_reply')}
                                </BUTTON>
                            </div>
                        </div>
                        <INPUT
                            name="mail_from"
                            labelTitle={trans('messages.receive_mail_from')}
                            className="form-control input-sm"
                            fieldGroupClass="col-md-12"
                            labelClass="col-md-12"
                            onChange={this.handleChange}
                            value={this.state.mail_from || ''}
                            readOnly={true}/>
                        <INPUT
                            name="mail_to"
                            labelTitle={trans('messages.receive_mail_to')}
                            className="form-control input-sm"
                            fieldGroupClass="col-md-12"
                            labelClass="col-md-12"
                            onChange={this.handleChange}
                            value={this.state.mail_to || ''}
                            readOnly={true}/>
                        <div className={(isErrReceiveOrderId)?"form-group has-error":"form-group"}>
                            <label className="col-md-12">{trans('messages.related_order_number')}</label>
                            <div className="col-md-12">
                                <div className="input-group input-group-sm">
                                    <input
                                        name="receive_order_id"
                                        className="form-control input-sm"
                                        onChange={this.handleChange}
                                        value={this.state.receive_order_id || ''}/>
                                    {(isErrReceiveOrderId) &&
                                        <i
                                            className="fa fa-times text-red form-control-feedback icon-error-right"
                                            data-placement="top"
                                            data-toggle="tooltip"
                                            data-original-title={msgReceiveOrderId}
                                        ></i>
                                    }
                                    <span className="input-group-btn">
                                        <BUTTON
                                            onClick={() => this.handleOpenPage('order_detail')}
                                            type="button"
                                            className="btn btn-info btn-flat">
                                            {trans('messages.btn_open')}
                                        </BUTTON>
                                    </span>
                                </div>
                            </div>
                        </div>
                        <div className="row">
                            <div className="col-md-6">
                                <SELECT
                                    name="status"
                                    labelTitle={trans('messages.receive_mail_status')}
                                    className="form-control input-sm"
                                    fieldGroupClass="col-md-12"
                                    labelClass="col-md-12"
                                    onChange={this.handleChange}
                                    value={this.state.status||''}
                                    options={dataSource.statusData}
                                    errorPopup={true}
                                    hasError={(dataResponse.message && dataResponse.message.status)?true:false}
                                    errorMessage={(dataResponse.message && dataResponse.message.status)?dataResponse.message.status:""}
                                    />
                            </div>
                            <div className="col-md-6">
                                <SELECT
                                    name="inquiry_code"
                                    labelTitle={trans('messages.inquiry_code')}
                                    className="form-control input-sm"
                                    fieldGroupClass="col-md-12"
                                    labelClass="col-md-12"
                                    onChange={this.handleChange}
                                    value={this.state.inquiry_code||''}
                                    options={dataSource.reasonData}
                                    errorPopup={true}
                                    hasError={(dataResponse.message && dataResponse.message.inquiry_code)?true:false}
                                    errorMessage={(dataResponse.message && dataResponse.message.inquiry_code)?dataResponse.message.inquiry_code:""}
                                    />
                            </div>
                        </div>
                        <div className="row">
                            <div className="col-md-6">
                                <SELECT
                                    name="deparment"
                                    labelTitle={trans('messages.receive_mail_deparment')}
                                    className="form-control input-sm"
                                    fieldGroupClass="col-md-12"
                                    labelClass="col-md-12"
                                    onChange={this.handleChange}
                                    value={this.state.deparment||''}
                                    options={dataSource.deparmentData}
                                    errorPopup={true}
                                    hasError={(dataResponse.message && dataResponse.message.deparment)?true:false}
                                    errorMessage={(dataResponse.message && dataResponse.message.deparment)?dataResponse.message.deparment:""}
                                    />
                            </div>
                            <div className="col-md-6">
                                <SELECT
                                    name="incharge_person"
                                    labelTitle={trans('messages.receive_mail_incharge_person')}
                                    className="form-control input-sm"
                                    fieldGroupClass="col-md-12"
                                    labelClass="col-md-12"
                                    onChange={this.handleChange}
                                    value={this.state.incharge_person||''}
                                    options={dataSource.tantouData}
                                    errorPopup={true}
                                    hasError={(dataResponse.message && dataResponse.message.incharge_person)?true:false}
                                    errorMessage={(dataResponse.message && dataResponse.message.incharge_person)?dataResponse.message.incharge_person:""}
                                    />
                            </div>

                        </div>
                        <TEXTAREA
                            name="cs_note"
                            labelTitle={trans('messages.btn_cs_note') + "："}
                            className="form-control"
                            fieldGroupClass="col-md-12"
                            labelClass="col-md-12"
                            groupClass=""
                            errorPopup={true}
                            value={this.state.cs_note || ''}
                            onChange={this.handleChange}
                            rows="6"
                            />
                    </div>
                </div>
                <div className="row">
                    <div className="col-md-7"></div>
                    <div className="col-md-4-75">
                        <BUTTON className="btn bg-purple input-sm pull-right btn-larger" type="button" onClick={this.handleCancel}>{trans('messages.btn_cancel')}</BUTTON>
                        <BUTTON name="btnAccept" className="btn btn-success btn-larger input-sm pull-right margin-element" type="button" onClick={this.handleSubmit}>{trans('messages.btn_accept')}</BUTTON>
                    </div>
                </div>
            </FORM>
            <MESSAGE_MODAL
                message={this.state.message || ''}
                title={trans('messages.deny_edit')}
                baseUrl={baseUrl}
            />
            </div>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        dataSource: state.commonReducer.dataSource,
        dataResponse: state.commonReducer.dataResponse,
        dataRealTime: state.commonReducer.dataRealTime
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        reloadData: (url, params) => { dispatch(Action.detailData(url, params)) },
        postData: (url, params, headers) => { dispatch(Action.postData(url, params, headers)) },
        postRealTime: (url, params, headers) => { dispatch(Action.postRealTime(url, params, headers)) }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(ReceiveMailSave)