import React, { Component } from 'react';

import { connect } from 'react-redux';
import * as Action from '../common/actions';
import * as queryString from 'query-string';

import BUTTON from '../common/components/form/BUTTON';
import FORM from '../common/components/form/FORM';
import INPUT from '../common/components/form/INPUT';
import SELECT from '../common/components/form/SELECT';
import LABEL from '../common/components/form/LABEL';
import DATEPICKER from '../common/components/form/DATEPICKER';

import TABLE from '../common/components/table/TABLE';
import THEAD from '../common/components/table/THEAD';
import TBODY from '../common/components/table/TBODY';
import TR from '../common/components/table/TR';
import TH from '../common/components/table/TH';
import TD from '../common/components/table/TD';
import moment from 'moment';

import SortComponent from '../common/components/table/SortComponent';
import PaginationContainer from '../common/containers/PaginationContainer'
import Loading from '../common/components/common/Loading';
import axios from 'axios';

class StockConfirmList extends Component {

    constructor(props) {
        super(props);
        const stringParams = queryString.parse(props.location.search);
        this.state = {};
        if (!_.isEmpty(stringParams)) {
            this.state = stringParams;
        }
        this.props.reloadData(dataListUrl, this.state);
    }

    handleChange = (event) => {
        const target = event.target;
        const value = target.type === 'checkbox' ? target.checked : target.value;
        const name = target.name;
        this.setState({
            [name]: (value)?value:undefined
        },() => {
            if (name === 'per_page' || name === 'status' || name === 'deparment') {
                this.handleReloadData();
            }
        });
    }

    handleChangeMultiSelect = (name, value, checked) => {
        let currentValue = this.state[name];
        if (typeof currentValue === 'undefined') {
            currentValue = [];
        } else if (!Array.isArray(this.state[name])) {
            currentValue = [this.state[name]];
        }
        if (Array.isArray(value)) {
            currentValue = [...value];
        } else {
            let index = currentValue.indexOf(value);
            if (checked === false) {
                if (index !== -1) {
                    currentValue.splice(index, 1);
                }
            } else {
                if (index === -1) {
                    currentValue.push(value);
                }
            }
        }
        this.setState({
            [name]: currentValue,
        });
    }

    handleClearClick = (event) => {
        Object.keys(this.state).map((key) => {
            if (key !== 'per_page') {
                delete this.state[key];
            }
        });
        this.handleReloadData();
    }

    handleExportCsv = () => {
        $(".loading").show();
        let param = {...this.state};
        param['type'] = 'stock-confirm';
        let headers = {'X-Requested-With': 'XMLHttpRequest'};
        axios.post(processExportCSV, param, {headers : headers}
        ).then(response => {
            $(".loading").hide();
            if (response.data.status === 1) {
                window.location.href = exportCSV +"?fileName="+ response.data.file_name;
            }
        }).catch(error => {
            $(".loading").hide();
            throw(error);
        });
    }

    handleReloadData = () => {
        if (this.state.page) {
            delete this.state.page;
        }
        this.props.reloadData(dataListUrl, this.state, true);
    }


    handleNotAnswer = () => {
        this.setState({not_answer : true}, () => {
            this.handleReloadData()
        });
    }

    componentDidUpdate() {
        if (!_.isEmpty(this.props.dataSource)) {
            $('.loading').hide();
        }
    }

    componentWillMount() {
        window.onpopstate = (event) => {
            location.reload();
        };
    }

    handleSortClick = (name, nextSort) => {
        Object.keys(this.state).map((key) => {
            if (key.startsWith('sort_')) {
                delete this.state[key];
            }
        });
        if (nextSort === 'none') {
            delete this.state[name];
            this.props.reloadData(dataListUrl, this.state, true);
        } else {
            this.setState({[name]: nextSort}, () => {
                this.props.reloadData(dataListUrl, this.state, true);
            });
        }
    }

    handleChangeDate = function(date, name, event) {
        let dateValue = moment(date);
        if (!dateValue.isValid()) {
            dateValue = undefined;
        } else {
            dateValue = dateValue.format('YYYY-MM-DD');
        }
        this.setState({
          [name]: dateValue,
        });
    }

    handleClickPage = (page) => {
        this.setState({
            page: page
        },() => {
            this.props.reloadData(dataListUrl, this.state, true);
        });
    }

    render() {
        let nf = new Intl.NumberFormat();
        let dataSource = this.props.dataSource;
        let index = 0;
        let totalItems = 0;
        let itemsPerPage = 20;
        if (!_.isEmpty(dataSource)) {
            index = dataSource.data.from - 1;
            totalItems   = dataSource.data.total;
            itemsPerPage = dataSource.data.per_page;
        }
        return ( (!_.isEmpty(dataSource)) &&
            <div>
                <Loading className="loading" />
                <div className="box">
                    <div className="box-header">
                        <div className="row">
                            <div className="col-md-12">
                                <h4>{trans('messages.search_title')}</h4>
                                <FORM>
                                    <div className="row">
                                        <INPUT
                                            name="estimate_code_from"
                                            groupClass="form-group col-md-2"
                                            labelTitle={trans('messages.estimate_code_from')}
                                            onChange={this.handleChange}
                                            value={this.state.estimate_code_from||''}
                                            className="form-control input-sm" />
                                        <INPUT
                                            name="estimate_code_to"
                                            groupClass="form-group col-md-2"
                                            labelTitle={trans('messages.estimate_code_to')}
                                            onChange={this.handleChange}
                                            value={this.state.estimate_code_to||''}
                                            className="form-control input-sm" />
                                        <SELECT
                                            id="m_order_type_id"
                                            name="m_order_type_id"
                                            groupClass="form-group col-md-1"
                                            labelTitle={trans('messages.product_status')}
                                            handleChangeMultiSelect={this.handleChangeMultiSelect}
                                            valueMulti={this.state.m_order_type_id||''}
                                            options={dataSource.statusOptions}
                                            multiple="multiple"
                                            className="form-control input-sm" />
                                        <DATEPICKER
                                            placeholderText="YYYY-MM-DD"
                                            dateFormat="YYYY-MM-DD"
                                            locale="ja"
                                            name="estimate_date_from"
                                            groupClass="form-group col-md-2"
                                            labelTitle={trans('messages.estimate_date_from')}
                                            className="form-control input-sm"
                                            selected={this.state.estimate_date_from ? moment(this.state.estimate_date_from) : null}
                                            onChange={(date, name, e) => this.handleChangeDate(date, 'estimate_date_from', e)} />

                                        <DATEPICKER
                                            placeholderText="YYYY-MM-DD"
                                            dateFormat="YYYY-MM-DD"
                                            locale="ja"
                                            name="estimate_date_to"
                                            groupClass="form-group col-md-2"
                                            labelTitle={trans('messages.estimate_date_to')}
                                            className="form-control input-sm"
                                            selected={this.state.estimate_date_to ? moment(this.state.estimate_date_to) : null}
                                            onChange={(date, name, e) => this.handleChangeDate(date, 'estimate_date_to', e)} />
                                        <INPUT
                                            name="supplier_name"
                                            groupClass="form-group col-md-2"
                                            labelTitle={trans('messages.supplier_name')}
                                            onChange={this.handleChange}
                                            value={this.state.supplier_name||''}
                                            className="form-control input-sm" />
                                        <SELECT
                                            name="per_page"
                                            groupClass="form-group col-md-1 per-page pull-right decrement-mgl-1"
                                            labelClass="pull-left"
                                            labelTitle={trans('messages.per_page')}
                                            onChange={this.handleChange}
                                            value={this.state.per_page||''}
                                            options={{20:20, 40:40, 60:60, 80:80, 100:100}}
                                            className="form-control input-sm" />
                                    </div>
                                    <div className="row">
                                        <INPUT
                                            name="product_code"
                                            groupClass="form-group col-md-2"
                                            labelTitle={trans('messages.product_code')}
                                            onChange={this.handleChange}
                                            value={this.state.product_code||''}
                                            className="form-control input-sm" />                                        
                                    </div>
                                </FORM>
                            </div>
                        </div>
                        <div className="row">
                            <div className="col-md-12 col-xs-12">
                                <BUTTON className="btn btn-primary input-sm pull-left margin-element margin-form" onClick={this.handleExportCsv} >{trans('messages.csv_export')}</BUTTON>
                                <BUTTON className="btn btn-primary input-sm pull-left margin-element margin-form" onClick={this.handleNotAnswer} >{trans('messages.btn_not_answer_stock_confirm')}</BUTTON>
                                <BUTTON className="btn btn-danger pull-right" onClick={this.handleClearClick}>{trans('messages.btn_reset')}</BUTTON>
                                <BUTTON className="btn btn-primary pull-right margin-element" onClick={this.handleReloadData} >{trans('messages.btn_search')}</BUTTON>
                            </div>
                        </div>
                    </div>
                    <div className="box-body">
                        <div className="row">
                            <div className="col-md-12">
                                {(totalItems/itemsPerPage > 1) &&
                                <div className="row">
                                    <div className="col-md-2-5"></div>
                                    <div className="col-md-7 text-center">
                                        <PaginationContainer handleClickPage={this.handleClickPage}/>
                                    </div>
                                    <div className="col-md-2-5 text-right line-height-md">
                                        {dataSource.data.from}件 - {dataSource.data.to}件 【全{dataSource.data.total}件】
                                    </div>
                                </div>
                                }
                                <div className="table-responsive">
                                <TABLE className="table table-striped table-custom">
                                    <THEAD>
                                         <TR>
                                            <TH className="text-center col-max-min-60" rowSpan="2">

                                            </TH>
                                            <TH className="text-center col-max-min-120">
                                                <SortComponent
                                                    name="sort_estimate_date"
                                                    onClick={this.handleSortClick}
                                                    currentSort={(this.state.sort_estimate_date)?this.state.sort_estimate_date:'none'}
                                                    title={trans('messages.estimate_date')} />
                                            </TH>
                                            <TH className="text-center col-max-min-100">
                                                <SortComponent
                                                    name="sort_product_code"
                                                    onClick={this.handleSortClick}
                                                    currentSort={(this.state.sort_product_code)?this.state.sort_product_code:'none'}
                                                    title={trans('messages.product_code')} />
                                            </TH>
                                            <TH className="text-center text-middle col-max-min-200" rowSpan="2">
                                                <SortComponent
                                                    name="sort_product_name"
                                                    onClick={this.handleSortClick}
                                                    currentSort={(this.state.sort_product_name)?this.state.sort_product_name:'none'}
                                                    title={trans('messages.product_name')} />
                                            </TH>
                                            <TH className="text-center text-middle col-max-min-100">
                                                <SortComponent
                                                    name="sort_m_order_type_id"
                                                    onClick={this.handleSortClick}
                                                    currentSort={(this.state.sort_m_order_type_id)?this.state.sort_m_order_type_id:'none'}
                                                    title={trans('messages.product_status')} />
                                            </TH>
                                            <TH className="text-center text-middle col-max-min-100">
                                                <SortComponent
                                                    name="sort_supplier_name"
                                                    onClick={this.handleSortClick}
                                                    currentSort={(this.state.sort_supplier_name)?this.state.sort_supplier_name:'none'}
                                                    title={trans('messages.supplier_name')} />
                                            </TH>
                                            <TH className="text-center text-middle col-max-min-120" rowSpan="2">
                                                <SortComponent
                                                    name="sort_edi_answer_date"
                                                    onClick={this.handleSortClick}
                                                    currentSort={(this.state.sort_edi_answer_date)?this.state.sort_edi_answer_date:'none'}
                                                    title={trans('messages.edi_answer_date')} />
                                            </TH>
                                            <TH className="text-center text-middle  col-max-min-100" rowSpan="2">
                                                {trans('messages.edi_memo')}
                                            </TH>
                                            <TH className="text-center text-middle col-max-min-100" rowSpan="2">
                                                {trans('messages.edi_other')}
                                            </TH>

                                        </TR>
                                        <TR>
                                            <TH className="text-center">
                                                <SortComponent
                                                    name="sort_estimate_code"
                                                    onClick={this.handleSortClick}
                                                    currentSort={(this.state.sort_estimate_code)?this.state.sort_estimate_code:'none'}
                                                    title={trans('messages.estimate_code')} />
                                            </TH>
                                            <TH className="text-center">
                                                <SortComponent
                                                    name="sort_jan_code"
                                                    onClick={this.handleSortClick}
                                                    currentSort={(this.state.sort_jan_code)?this.state.sort_jan_code:'none'}
                                                    title={trans('messages.product_jan')} />
                                            </TH>
                                            <TH className="text-center">
                                                <SortComponent
                                                    name="sort_quantity"
                                                    onClick={this.handleSortClick}
                                                    currentSort={(this.state.sort_quantity)?this.state.sort_quantity:'none'}
                                                    title={trans('messages.quantity')} />
                                            </TH>
                                            <TH className="text-center">
                                                <SortComponent
                                                    name="sort_maker_name"
                                                    onClick={this.handleSortClick}
                                                    currentSort={(this.state.sort_maker_name)?this.state.sort_maker_name:'none'}
                                                    title={trans('messages.maker_name')} />
                                            </TH>
                                        </TR>
                                    </THEAD>
                                    <TBODY>
                                        {
                                            (!_.isEmpty(dataSource.data.data))?
                                            dataSource.data.data.map((item) => {
                                                index++;
                                                let estimateDate = !_.isEmpty(item.estimate_date) ? moment(item.estimate_date).format("YYYY/MM/DD") : '';
                                                let ediAnswerDate = !_.isEmpty(item.updated_at) ? moment(item.updated_at).format("YYYY/MM/DD HH:mm:ss") : '';

                                                let status = Object.keys(dataSource.statusOptions).map((i) => {
                                                       return dataSource.statusOptions[i]['key'] === item.m_order_type_id ? dataSource.statusOptions[i]['value'] : '';
                                                });
                                                let classRow = (index%2 ===0) ? 'odd' : 'even';
                                                return (
                                                    [
                                                    <TR key={"tr1" + index} className={classRow}>
                                                        <TD className="visible-xs visible-sm text-center">
                                                            <b className="show-info">
                                                                <i className="fa fa-angle-double-down" aria-hidden="true"></i>
                                                            </b>
                                                        </TD>
                                                        <TD className="text-center" title={item.rec_mail_index}>
                                                            <a className="link-detail" target="_blank" href={dataSource.ediZaikoUrl + "?s_order_code=" + item.estimate_code}>
                                                                {trans('messages.btn_edi')}
                                                            </a>
                                                        </TD>
                                                        <TD className="text-center" title={estimateDate}>
                                                            {estimateDate}
                                                        </TD>
                                                        <TD className="text-left" title={item.product_code}>
                                                            {item.product_code}
                                                        </TD>
                                                        <TD className="text-left text-middle" title={item.product_name} rowSpan="2">
                                                            {item.product_name}
                                                        </TD>
                                                        <TD className="text-left text-middle" title={status} >
                                                            {status}
                                                        </TD>
                                                        <TD className="text-left text-middle" title={item.supplier_nm}>
                                                            {item.supplier_nm}
                                                        </TD>
                                                        <TD className="text-center text-middle" title={ediAnswerDate} rowSpan="2">
                                                            {ediAnswerDate}
                                                        </TD>
                                                        <TD className="text-left text-middle" title={item.memo} rowSpan="2">
                                                            {item.memo}
                                                        </TD>
                                                        <TD className="text-left text-middle" title={item.other} rowSpan="2">
                                                            {item.other}
                                                        </TD>
                                                    </TR>,
                                                    <TR key={"tr2" + index} className={classRow}>
                                                        <TD className="text-center" title={item.rec_mail_index}>
                                                            <a className="link-detail" target="_blank" href={orderDetailUrl + "?receive_id_key=" + item.receive_id}>
                                                                 {trans('messages.btn_hrnb_order')}
                                                            </a>
                                                        </TD>
                                                        <TD className="text-left" title={item.estimate_code}>
                                                            {item.estimate_code}
                                                        </TD>
                                                        <TD className="text-left" title={item.estimate_code}>
                                                            {item.product_jan}
                                                        </TD>
                                                        <TD className="text-right" title={item.quantity}>
                                                            {item.quantity}
                                                        </TD>
                                                        <TD className="text-left" title={item.maker_name}>
                                                            {item.maker_name}
                                                        </TD>
                                                    </TR>
                                                   ]
                                                )
                                                if (index === dataSource.data.to) {
                                                   index = 0;
                                                }
                                            })
                                            :
                                            <TR><TD colSpan="9" className="text-center">{trans('messages.no_data')}</TD></TR>
                                        }
                                    </TBODY>
                                </TABLE>
                                </div>
                                <div className="text-center">
                                    <PaginationContainer handleClickPage={this.handleClickPage}/>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        dataSource: state.commonReducer.dataSource,
        dataResponse: state.commonReducer.dataResponse
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        reloadData: (url, params, pushHistory, ignoreFields) => { dispatch(Action.reloadData(url, params, pushHistory, ignoreFields)) },
        postData: (url, params, headers, callback) => { dispatch(Action.postData(url, params, headers, callback)) }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(StockConfirmList)