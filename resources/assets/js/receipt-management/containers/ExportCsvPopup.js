import React, { Component } from 'react';
import Loading from '../../common/components/common/Loading';
import INPUT from '../../common/components/form/INPUT';

import DATEPICKER from '../../common/components/form/DATEPICKER';
import moment from 'moment';
import axios from 'axios';

export default class ExportPdfPopup extends Component {
    constructor(props) {
        super(props);

        this.state = {
            receive_id: this.props.receiveId,
            from_date: moment(),
            to_date: moment(),
            from_date_raw: moment().format('YYYY-MM-DD'),
            to_date_raw: moment().format('YYYY-MM-DD')
        }
    }

    handleChangeDate = (date) => {
        this.setState({
            from_date: date,
            from_date_raw: date.format('YYYY-MM-DD')
        });
    }
    handleChangeToDate = (date) => {
        this.setState({
            to_date: date,
            to_date_raw: date.format('YYYY-MM-DD')
        });
    }

    handleChangeDateRaw(value) {
        this.setState({
            from_date_raw: value,
        });
    }
    handleChangeToDateRaw(value) {
        this.setState({
            to_date_raw: value,
        });
    }

    handleChange = (event) => {
        const target = event.target;
        const name = target.name;
        const value = target.value;
        this.setState({
            [name]: value
        });
    }

    handleExportClick = () => {
        let params = {};
        let arrParams = [];

        params['from_date']    = this.state['from_date_raw'];
        params['to_date']    = this.state['to_date_raw'];

        arrParams.push("from_date=" + this.state['from_date_raw']);
        arrParams.push("to_date=" + this.state['to_date_raw']);

        $(".loading").show();
        axios.post(exportCsvUrl, params, {headers: {'X-Requested-With': 'XMLHttpRequest'}}
        ).then(response => {
            $('.modal-custom-body').find('p').remove();
            if (response.data.status === 0) {
                let messages = response.data.messages;
                if (messages) {
                    this.setState({
                        from_date_error: (messages.from_date)?messages.from_date:'',
                        to_date_error: (messages.to_date)?messages.to_date:''
                    });
                }
                if (response.data.data.length === 0 && !(messages)) {
                    $('.modal-custom-body').append('<p><font color="red">No data for export.</font></p>');
                    $('.modal-custom-body').find('p').fadeOut(3000)
                }

            } else {
                this.setState({
                    from_date_error: '',
                    to_date_error: ''
                });
                $('#issue-modal').modal('hide');
                this.handleCancelClick();
                window.location.href = fileDownloadUrl +"?fileName="+ response.data.fileNameDownload;
            }
            $(".loading").hide();
        }).catch(error => {
            $(".loading").hide();
            throw(error);
        });
    }

    handleCancelClick = () => {
        this.setState({
            from_date: moment(),
            from_date_raw: moment().format('YYYY-MM-DD'),
            from_date_error: '',
            to_date: moment(),
            to_date_raw: moment().format('YYYY-MM-DD'),
            to_date_error: ''
        });
        $('#issue-modal').modal('hide');
    }

    render() {
        return (
            <div className="modal fade" id="issue-modal">
                <Loading className="loading" />
                <input type="hidden" id="action-modal" />
                <div className="modal-dialog modal-custom">
                    <div className="modal-content modal-custom-content">
                        <div className="modal-header">
                            <h4 className="modal-title text-center"><b>{trans('messages.csv_condition_output_title')}</b></h4>
                        </div>
                        <div className="modal-body modal-custom-body">
                            <div className="row">
                            <form className="form-horizontal">
                                <label className="col-md-4">{trans('messages.output_time')}:</label>
                                <DATEPICKER
                                    placeholderText="YYYY-MM-DD"
                                    dateFormat="YYYY-MM-DD"
                                    locale="ja"
                                    name="from_date"
                                    className="form-control input-sm"
                                    fieldGroupClass=""
                                    labelClass=""
                                    groupClass="col-md-4"
                                    selected={this.state.from_date}
                                    value={this.state.from_date_raw}
                                    onChange={this.handleChangeDate}
                                    onChangeRaw={(event) => this.handleChangeDateRaw(event.target.value)}
                                    errorPopup={true}
                                    hasError={(this.state.from_date_error)?true:false}
                                    errorMessage={(this.state.from_date_error)?this.state.from_date_error:""}
                                    />

                                <DATEPICKER
                                    placeholderText="YYYY-MM-DD"
                                    dateFormat="YYYY-MM-DD"
                                    locale="ja"
                                    name="to_date"
                                    className="form-control input-sm"
                                    fieldGroupClass=""
                                    labelClass=""
                                    groupClass="col-md-4"
                                    selected={this.state.to_date}
                                    value={this.state.to_date_raw}
                                    onChange={this.handleChangeToDate}
                                    onChangeRaw={(event) => this.handleChangeToDateRaw(event.target.value)}
                                    errorPopup={true}
                                    hasError={(this.state.to_date_error)?true:false}
                                    errorMessage={(this.state.to_date_error)?this.state.to_date_error:""}
                                    />
                            </form>
                            </div>
                        </div>
                        <div className="modal-footer modal-custom-footer">
                            <button type="button" className="btn btn-primary modal-custom-button" onClick={this.handleExportClick}>{trans('messages.issue_modal_ok')}</button>
                            <button type="button" className="btn bg-purple modal-custom-button" onClick={this.handleCancelClick}>{trans('messages.issue_modal_cancel')}</button>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}