import React, { Component } from 'react';

import { connect } from 'react-redux';
import * as Action from '../common/actions';
import * as queryString from 'query-string';

import BUTTON from '../common/components/form/BUTTON';
import FORM from '../common/components/form/FORM';
import INPUT from '../common/components/form/INPUT';
import DATEPICKER from '../common/components/form/DATEPICKER';

import TABLE from '../common/components/table/TABLE';
import THEAD from '../common/components/table/THEAD';
import TBODY from '../common/components/table/TBODY';
import TR from '../common/components/table/TR';
import TH from '../common/components/table/TH';
import TD from '../common/components/table/TD';
import SELECT from '../common/components/form/SELECT';
import LABEL from '../common/components/form/LABEL';
import moment from 'moment';

import SortComponent from '../common/components/table/SortComponent';
import PaginationContainer from '../common/containers/PaginationContainer'
import Loading from '../common/components/common/Loading';
import axios from 'axios';

class ReceiptList extends Component {

    constructor(props) {
        super(props);
        const stringParams = queryString.parse(props.location.search);
        this.state = {};
        if (!_.isEmpty(stringParams)) {
            this.state = stringParams;
        }
        this.props.reloadData(dataListUrl, this.state);
    }

    handleChange = (event) => {
        const target = event.target;
        let value = target.type === 'checkbox' ? target.checked : target.value;
        const name = target.name;
        if (value === '_none') {
            value = '';
        }
        this.setState({
            [name]: value
        },() => {
            if (name === 'per_page' || name === 'stock_type' || name === 'stock_detail_type') {
                this.handleReloadData();
            }
        });
    }
    handleIssue = (event, index) => {
        let params = {};

        Object.keys(this.state).map((key) => {
            if (key !== 'per_page') {
                params[key] = this.state[key];
            }
        });

        $(".loading").show();
        axios.post(exportCsvUrl, params, {headers: {'X-Requested-With': 'XMLHttpRequest'}}
        ).then(response => {
            if (response.data.status !== 0) {
                window.location.href = fileDownloadUrl +"?fileName="+ response.data.fileNameDownload;
            }
            $(".loading").hide();
        }).catch(error => {
            $(".loading").hide();
            throw(error);
        });
    }
    handleClearClick = (event) => {
        Object.keys(this.state).map((key) => {
            if (key !== 'per_page') {
                delete this.state[key];
            }
        });
        this.handleReloadData();
    }

    handleReloadData = () => {
        if (this.state.page) {
            delete this.state.page;
        }
        this.props.reloadData(dataListUrl, this.state, true);
    }

    componentDidUpdate() {
        if (!_.isEmpty(this.props.dataSource)) {
            $('.loading').hide();
        }
        dragscroll.reset();
        $("table th").resizeble();
    }

    componentWillMount() {
        window.onpopstate = (event) => {
            location.reload();
        };
    }

    handleSortClick = (name, nextSort) => {
        Object.keys(this.state).map((key) => {
            if (key.startsWith('sort_')) {
                delete this.state[key];
            }
        });
        if (nextSort === 'none') {
            delete this.state[name];
            this.props.reloadData(dataListUrl, this.state, true);
        } else {
            this.setState({[name]: nextSort}, () => {
                this.props.reloadData(dataListUrl, this.state, true);
            });
        }

    }

    handleChangeDate = function(date, name, event) {
        let dateValue = moment(date);
        if (!dateValue.isValid()) {
            dateValue = undefined;
        } else {
            dateValue = dateValue.format('YYYY-MM-DD');
        }
        this.setState({
          [name]: dateValue,
        });
    }

    handleClickPage = (page) => {
        this.setState({
            page: page
        },() => {
            this.props.reloadData(dataListUrl, this.state, true);
        });
    }

    handleChangeMultiSelect = (name, value, checked) => {
        let currentValue = this.state[name];
        if (typeof currentValue === 'undefined') {
            currentValue = [];
        } else if (!Array.isArray(this.state[name])) {
            currentValue = [this.state[name]];
        }
        if (Array.isArray(value)) {
            currentValue = [...value];
        } else {
            let index = currentValue.indexOf(value);
            if (checked === false) {
                if (index !== -1) {
                    currentValue.splice(index, 1);
                }
            } else {
                if (index === -1) {
                    currentValue.push(value);
                }
            }
        }
        this.setState({
            [name]: currentValue,
        });
    }

    render() {
        let dataSource = this.props.dataSource;
        let index = 0;
        let stockType = [];
        let stockDetailType = [];
        let totalItems = 0;
        let itemsPerPage = 20;
        if (!_.isEmpty(dataSource)) {
            index = dataSource.data.from - 1;
            stockType = dataSource.stockType;
            stockDetailType = dataSource.stockDetailType;
            totalItems   = dataSource.data.total;
            itemsPerPage = dataSource.data.per_page;
        }
        var nf = new Intl.NumberFormat();
        return ( (!_.isEmpty(dataSource)) &&
            <div>
                <Loading className="loading" />
                <div className="box">
                    <div className="box-header">
                        <div className="row">
                                <div className="col-md-12">
                                    <h4>{trans('messages.search_title')}</h4>
                                </div>
                                <FORM>
                                    <SELECT
                                        id="stock_type"
                                        name="stock_type"
                                        groupClass="form-group col-md-1-5"
                                        labelClass=""
                                        labelTitle={trans('messages.stock_type')}
                                        handleChangeMultiSelect={this.handleChangeMultiSelect}
                                        valueMulti={this.state.stock_type || ''}
                                        options={stockType}
                                        multiple="multiple"
                                        className="form-control input-sm" />
                                    <SELECT
                                        id="stock_detail_type"
                                        name="stock_detail_type"
                                        groupClass="form-group col-md-1-5"
                                        labelClass=""
                                        labelTitle={trans('messages.stock_detail_type')}
                                        handleChangeMultiSelect={this.handleChangeMultiSelect}
                                        valueMulti={this.state.stock_detail_type || ''}
                                        options={stockDetailType}
                                        multiple="multiple"
                                        className="form-control input-sm" />
                                    <INPUT
                                        name="recognite_key"
                                        groupClass="form-group col-md-2"
                                        labelTitle={trans('messages.recognite_key')}
                                        onChange={this.handleChange}
                                        value={this.state.recognite_key || ''}
                                        className="form-control input-sm" />
                                    <INPUT
                                        name="product_code"
                                        groupClass="form-group col-md-2"
                                        labelTitle={trans('messages.product_code')}
                                        onChange={this.handleChange}
                                        value={this.state.product_code || ''}
                                        className="form-control input-sm" />
                                    <DATEPICKER
                                        groupClass="form-group col-md-1-75 clear-padding"
                                        labelTitle={trans('messages.up_date_from')}
                                        placeholderText="YYYY-MM-DD"
                                        dateFormat="YYYY-MM-DD"
                                        locale="ja"
                                        name="up_date_from"
                                        className="form-control input-sm"
                                        key="up_date_from"
                                        id="up_date_from"
                                        openToDate ={moment().subtract(7, 'day')}
                                        selected={this.state.up_date_from ? moment(this.state.up_date_from) : null}
                                        onChange={(date, name, e) => this.handleChangeDate(date, 'up_date_from', e)} />
                                    <div className="col-md-0-25 hidden-xs hidden-sm"><span className="to-from-child">～</span></div>
                                    <DATEPICKER
                                        groupClass="form-group col-md-1-75 clear-padding"
                                        labelTitle={trans('messages.up_date_to')}
                                        placeholderText="YYYY-MM-DD"
                                        dateFormat="YYYY-MM-DD"
                                        locale="ja"
                                        name="up_date_to"
                                        className="form-control col-md-12 input-sm"
                                        key="up_date_to"
                                        id="up_date_to"
                                        selected={this.state.up_date_to ? moment(this.state.up_date_to) : null}
                                        onChange={(date, name, e) => this.handleChangeDate(date, 'up_date_to', e)} />
                                    <SELECT
                                        name="per_page"
                                        groupClass="form-group col-md-1 per-page pull-right-md"
                                        labelClass="pull-left"
                                        labelTitle={trans('messages.per_page')}
                                        onChange={this.handleChange}
                                        value={this.state.per_page||''}
                                        options={{20:20, 40:40, 60:60, 80:80, 100:100}}
                                        className="form-control input-sm" />
                                </FORM>
                        </div>
                        <div className="row">
                            <div className="col-md-6 col-xs-12 margin-bottom-btn">
                                <BUTTON className="btn btn-primary pull-xs-right" onClick={(e) => this.handleIssue(e, 'estimate')} >{trans('messages.csv_export')}</BUTTON>
                            </div>
                            <div className="col-md-6 col-xs-12 pull-right">
                                <BUTTON className="btn btn-danger pull-right" onClick={this.handleClearClick}>{trans('messages.btn_reset')}</BUTTON>
                                <BUTTON className="btn btn-primary pull-right margin-element" onClick={this.handleReloadData} >{trans('messages.btn_search')}</BUTTON>
                            </div>
                        </div>
                    </div>
                    <div className="box-body">
                        <div className="row">
                            <div className="col-md-12">
                                {(totalItems/itemsPerPage > 1) &&
                                <div className="row">
                                    <div className="col-md-2-5"></div>
                                    <div className="col-md-7 text-center">
                                        <PaginationContainer handleClickPage={this.handleClickPage}/>
                                    </div>
                                    <div className="col-md-2-5 text-right line-height-md">
                                        {dataSource.data.from}件 - {dataSource.data.to}件 【全{dataSource.data.total}件】
                                    </div>
                                </div>
                                }
                                <div className="table-responsive">
                                <TABLE className="table table-striped table-custom">
                                    <THEAD>
                                        <TR>
                                            <TH className="col-max-min-60 text-center text-middle">
                                                <SortComponent
                                                    name="sort_index"
                                                    onClick={this.handleSortClick}
                                                    currentSort={(this.state.sort_index)?this.state.sort_index:'none'}
                                                    title={trans('messages.index')} />

                                            </TH>
                                            <TH className="hidden-xs hidden-sm  col-max-min-60 text-center text-middle">
                                                <SortComponent
                                                    name="sort_stock_type"
                                                    onClick={this.handleSortClick}
                                                    currentSort={(this.state.sort_stock_type)?this.state.sort_stock_type:'none'}
                                                    title={trans('messages.stock_type')} />
                                            </TH>
                                            <TH className="hidden-xs hidden-sm  col-max-min-100 text-center text-middle" >
                                                <SortComponent
                                                    name="sort_stock_detail_type"
                                                    onClick={this.handleSortClick}
                                                    currentSort={(this.state.sort_stock_detail_type)?this.state.sort_stock_detail_type:'none'}
                                                    title={trans('messages.stock_detail_type')} />
                                            </TH>
                                            <TH className="hidden-xs hidden-sm col-max-min-100 text-center text-middle">
                                                <SortComponent
                                                    name="sort_recognite_key"
                                                    onClick={this.handleSortClick}
                                                    currentSort={(this.state.sort_recognite_key)?this.state.sort_recognite_key:'none'}
                                                    title={trans('messages.recognite_key')} />
                                            </TH>
                                            <TH className="col-max-min-130 text-center text-middle">
                                                <SortComponent
                                                    name="sort_product_code"
                                                    onClick={this.handleSortClick}
                                                    currentSort={(this.state.sort_product_code)?this.state.sort_product_code:'none'}
                                                    title={trans('messages.product_code')} />
                                            </TH>
                                            <TH className="hidden-xs hidden-sm resize-thead text-center text-middle">
                                                <SortComponent
                                                    name="sort_product_name"
                                                    onClick={this.handleSortClick}
                                                    currentSort={(this.state.sort_product_name)?this.state.sort_product_name:'none'}
                                                    title={trans('messages.product_name')} />
                                            </TH>
                                            <TH className="col-max-min-100 text-center text-middle">
                                                <SortComponent
                                                    name="sort_stock_num"
                                                    onClick={this.handleSortClick}
                                                    currentSort={(this.state.sort_stock_num)?this.state.sort_stock_num:'none'}
                                                    title={trans('messages.stock_num')} />
                                            </TH>
                                            <TH className="col-max-min-100 text-center text-middle">
                                                    {trans('messages.price_invoice')}
                                            </TH>
                                            <TH className="col-max-min-100 text-center text-middle">
                                                    {trans('messages.stock_detail_num')}
                                            </TH>
                                            <TH className="col-max-min-100 text-center text-middle">
                                                    {trans('messages.change_num')}
                                            </TH>
                                            <TH className="hidden-xs hidden-sm col-max-min-130 text-center text-middle">
                                                    {trans('messages.up_date')}
                                            </TH>
                                        </TR>
                                    </THEAD>
                                    <TBODY>
                                    {
                                        (!_.isEmpty(dataSource.data.data))?
                                        dataSource.data.data.map((item) => {
                                            if (typeof item.count !== 'undefined' ) {
                                                index++;
                                            }
                                            let classRow = (index%2 ===0) ? 'odd' : 'even';
                                            let valStockType = stockType.find(key => key.key === item.stock_type);
                                            let valStockDType = stockDetailType.find(key => key.key === item.stock_detail_type);
                                            if (typeof valStockType !== 'undefined') {
                                                valStockType = valStockType['value']
                                            }
                                            if (typeof valStockDType !== 'undefined') {
                                                valStockDType = valStockDType['value']
                                            }
                                            let formatUpDate = !_.isEmpty(item.up_date) ? moment.utc(item.up_date, "YYYY-MM-DD HH:mm:ss").format("YYYY/MM/DD HH:mm:ss") : '';
                                            if (typeof item.count === 'undefined' ) {
                                                return ([
                                                    <TR className={classRow}>
                                                        <TD className="cut-text text-right"
                                                        title={nf.format(item.price_invoice)}>{nf.format(item.price_invoice)}
                                                        </TD>
                                                        <TD className="cut-text  text-right"
                                                            title={item.stock_detail_num}>{item.stock_detail_num}
                                                        </TD>
                                                        <TD className="cut-text  text-right"
                                                            title={item.change_num}>{item.change_num}
                                                        </TD>
                                                        <TD className="hidden-xs hidden-sm cut-text text-center"
                                                            title={formatUpDate}>{formatUpDate}
                                                        </TD>
                                                    </TR>
                                                ]);
                                            }
                                            return (
                                                [
                                                <TR key={"tr" + index} className={classRow}>
                                                    <TD className="text-center" rowSpan={item.count}>
                                                       {item.index}
                                                    </TD>
                                                    <TD className="hidden-xs hidden-sm cut-text" rowSpan={item.count}
                                                        title={valStockType}>{valStockType}
                                                    </TD>
                                                    <TD className="hidden-xs hidden-sm cut-text" rowSpan={item.count}
                                                        title={valStockDType}>{valStockDType}
                                                    </TD>
                                                    <TD className="hidden-xs hidden-sm cut-text" rowSpan={item.count}
                                                        title={item.recognite_key}>{item.recognite_key}
                                                    </TD>
                                                    <TD className="cut-text" rowSpan={item.count}
                                                        title={item.product_code}>{item.product_code}
                                                    </TD>
                                                    <TD className="hidden-xs hidden-sm cut-text col-max-min-160" rowSpan={item.count}
                                                        title={item.product_name}>{item.product_name}
                                                    </TD>
                                                    <TD className="cut-text text-right" rowSpan={item.count}
                                                        title={item.stock_num}>{item.stock_num}
                                                    </TD>
                                                    <TD className="cut-text text-right"
                                                    title={nf.format(item.price_invoice)}>{nf.format(item.price_invoice)}
                                                    </TD>
                                                    <TD className="cut-text  text-right"
                                                        title={item.stock_detail_num}>{item.stock_detail_num}
                                                    </TD>
                                                    <TD className="cut-text  text-right"
                                                        title={item.change_num}>{item.change_num}
                                                    </TD>
                                                    <TD className="hidden-xs hidden-sm cut-text text-center"
                                                        title={formatUpDate}>{formatUpDate}
                                                    </TD>
                                                </TR>
                                               ]
                                            )
                                        })
                                        :
                                        <TR><TD colSpan="11" className="text-center">{trans('messages.no_data')}</TD></TR>
                                    }
                                    </TBODY>
                                </TABLE>
                                </div>
                                <div className="text-center">
                                    <PaginationContainer handleClickPage={this.handleClickPage}/>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        dataSource: state.commonReducer.dataSource,
        dataResponse: state.commonReducer.dataResponse
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        reloadData: (url, params, pushHistory, ignoreFields) => { dispatch(Action.reloadData(url, params, pushHistory, ignoreFields)) },
        postData: (url, params, headers, callback) => { dispatch(Action.postData(url, params, headers, callback)) }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(ReceiptList)