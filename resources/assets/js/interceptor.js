import axios from 'axios';
axios.interceptors.response.use((response) => {
if (typeof response.data.not_permission !== "undefined" && response.data.not_permission === 1) {
    $.notify({
        icon: 'glyphicon glyphicon-warning-sign',
        message: trans('messages.not_permission'),
    },{
        type: 'error'
    });
}
return response;
}, (error) => {
return Promise.reject(error);
});