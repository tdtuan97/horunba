import React, { Component } from 'react';

import { connect } from 'react-redux';
import * as Action from '../common/actions';
import * as queryString from 'query-string';

import BUTTON from '../common/components/form/BUTTON';
import FORM from '../common/components/form/FORM';
import INPUT from '../common/components/form/INPUT';
import SELECT from '../common/components/form/SELECT';
import CHECKBOX from '../common/components/form/CHECKBOX';
import LABEL from '../common/components/form/LABEL';

import TABLE from '../common/components/table/TABLE';
import THEAD from '../common/components/table/THEAD';
import TBODY from '../common/components/table/TBODY';
import TR from '../common/components/table/TR';
import TH from '../common/components/table/TH';
import TD from '../common/components/table/TD';
import axios from 'axios';

import SortComponent from '../common/components/table/SortComponent';
import PaginationContainer from '../common/containers/PaginationContainer'
import Loading from '../common/components/common/Loading';
import DatePicker from 'react-datepicker';
import moment from 'moment';
import 'react-datepicker/dist/react-datepicker.css';

class RePaymentList extends Component {

    constructor(props) {
        super(props);
        const stringParams = queryString.parse(props.location.search);
        this.state = {};
        if (!_.isEmpty(stringParams)) {
            this.state = stringParams;
        }
        // this.state['sort_repay_no'] = 'desc';
        this.props.reloadData(dataListUrl, this.state);
    }
    componentDidUpdate() {
        if (!_.isEmpty(this.props.dataSource)) {
            $('.loading').hide();
        }
    }

    componentWillMount() {
        window.onpopstate = (event) => {
            location.reload();
        };
    }

    handleChange = (event) => {
        const target = event.target;
        const value = target.type === 'checkbox' ? target.checked : target.value;
        const name =  target.name ;
        this.setState({
            [name]: (value)?value:undefined
        },() => {
            if (name === 'per_page' ||
                name === 'repayment_status'
                ) {
                this.handleReloadData();
            }
        });

    }
    handleChangeStatus = (event) => {
        let headers = {'X-Requested-With': 'XMLHttpRequest'};
        const target = event.target;
        const repay_no = event.target.getAttribute('data-repay_no');
        const value = target.type === 'checkbox' ? target.checked : target.value;
        const name =  target.name ;
        const formData = {'where' : {repay_no:repay_no} , 'data' : {[name] : value} };
        this.props.postData(changeFlagUrl, formData, headers);
        this.props.reloadData(dataListUrl, this.state);
    }
    handleChangeFinished = (event, item) => {
        let headers = {'X-Requested-With': 'XMLHttpRequest'};
        this.props.postData(changeFlagUrl, item, headers , this.props.reloadData, [dataListUrl, this.state, true]);
    }

    handleClearClick = (event) => {
        Object.keys(this.state).map((key) => {
            if (key !== 'per_page') {
                delete this.state[key];
            }
        });
        this.handleReloadData();
    }

    handleReloadData = () => {
        if (this.state.page) {
            delete this.state.page;
        }
        this.props.reloadData(dataListUrl, this.state, true);
    }

    handleSortClick = (name, nextSort) => {
        Object.keys(this.state).map((key) => {
            if (key.startsWith('sort_')) {
                delete this.state[key];
            }
        });
        if (nextSort === 'none') {
            delete this.state[name];
            this.props.reloadData(dataListUrl, this.state, true);
        } else {
            this.setState({[name]: nextSort}, () => {
                this.props.reloadData(dataListUrl, this.state, true);
            });
        }

    }
    handleClickPage = (page) => {
        this.setState({
            page: page
        },() => {
            this.props.reloadData(dataListUrl, this.state, true);
        });
    }
    handleChangeDate = function(date, name, event) {
        let dateValue = moment(date);
        if (!dateValue.isValid()) {
            dateValue = undefined;
        } else {
            dateValue = dateValue.format('YYYY-MM-DD');
        }
        this.setState({
          [name]: dateValue,
        });
    }
    handleExportCsv = () => {
        $(".loading").show();
        let param = {...this.state};
        param['type'] = 'isCsv';
        let headers = {'X-Requested-With': 'XMLHttpRequest'};
        axios.post(processExportCSV, param, {headers : headers}
        ).then(response => {
            $(".loading").hide();
            if (response.data.status === 1) {
                window.location.href = exportCSV +"?fileName="+ response.data.file_name;
            }
        }).catch(error => {
            $(".loading").hide();
            throw(error);
        });
    }

    handleChangeMultiSelect = (name, value, checked) => {
        let currentValue = this.state[name];
        if (typeof currentValue === 'undefined') {
            currentValue = [];
        } else if (!Array.isArray(this.state[name])) {
            currentValue = [this.state[name]];
        }
        if (Array.isArray(value)) {
            currentValue = [...value];
        } else {
            let index = currentValue.indexOf(value);
            if (checked === false) {
                if (index !== -1) {
                    currentValue.splice(index, 1);
                }
            } else {
                if (index === -1) {
                    currentValue.push(value);
                }
            }
        }
        this.setState({
            [name]: currentValue,
        });
    }
    render() {
        let dataSource = this.props.dataSource;
        let i = 0;
        var nf = new Intl.NumberFormat();
        let totalItems = 0;
        let itemsPerPage = 20;
        if (!_.isEmpty(dataSource)) {
            totalItems   = dataSource.data.total;
            itemsPerPage = dataSource.data.per_page;
        }
        return ( (!_.isEmpty(dataSource)) &&
            <div>
                <Loading className="loading" />
                <div className="box">
                    <div className="box-header">
                        <div className="row">
                            <div className="col-md-12">
                                <h4>{trans('messages.search_title')}</h4>
                            </div>
                            <FORM >
                                <div className="row">
                                    <div className="col-md-12">
                                        <INPUT
                                            name="repay_name"
                                            groupClass="form-group col-md-2-25"
                                            labelTitle={trans('messages.repayment')}
                                            onChange={this.handleChange}
                                            value={this.state.repay_name||''}
                                            className="form-control input-sm" />
                                        <INPUT
                                            name="repay_bank_name"
                                            groupClass="form-group col-md-2-5"
                                            labelTitle={trans('messages.rep_bank_name')}
                                            onChange={this.handleChange}
                                            value={this.state.repay_bank_name||''}
                                            className="form-control input-sm" />
                                        <INPUT
                                            name="repay_bank_branch_name"
                                            groupClass="form-group col-md-2-5"
                                            labelTitle={trans('messages.rep_bank_branch_name')}
                                            onChange={this.handleChange}
                                            value={this.state.repay_bank_branch_name||''}
                                            className="form-control input-sm" />
                                        <INPUT
                                            name="repay_bank_account"
                                            groupClass="form-group col-md-1-75"
                                            labelTitle={trans('messages.payment_account_num')}
                                            onChange={this.handleChange}
                                            value={this.state.repay_bank_account||''}
                                            className="form-control input-sm" />
                                        <SELECT
                                            id="repayment_status"
                                            name="repayment_status"
                                            groupClass="form-group col-md-1-75"
                                            labelTitle={trans('messages.repayment_status')}
                                            fieldGroupClass = ""
                                            onChange={this.handleChange}
                                            handleChangeMultiSelect={this.handleChangeMultiSelect}
                                            valueMulti={this.state.repayment_status||''}
                                            options={{0:'未', 1:'済'}}
                                            multiple="multiple"
                                            className="form-control input-sm" />

                                        <SELECT
                                            name="per_page"
                                            groupClass="form-group col-md-1 per-page pull-right-md"
                                            labelClass="pull-left"
                                            labelTitle={trans('messages.per_page')}
                                            onChange={this.handleChange}
                                            value={this.state.per_page||''}
                                            options={{20:20, 40:40, 60:60, 80:80, 100:100}}
                                            className="form-control input-sm" />
                                    </div>
                                    <div className="col-md-12">
                                        <LABEL
                                            groupClass="form-group col-md-2-25"
                                            labelTitle={trans('messages.repay_date_from')}>
                                            <DatePicker
                                                placeholderText="YYYY-MM-DD"
                                                dateFormat="YYYY-MM-DD"
                                                locale="ja"
                                                name="repay_date_from"
                                                className="form-control input-sm"
                                                key="repay_date_from"
                                                id="repay_date_from"
                                                selected={this.state.repay_date_from ? moment(this.state.repay_date_from) : null}
                                                onChange={(date,name, e) => this.handleChangeDate(date, 'repay_date_from', e)} />
                                        </LABEL>
                                        <LABEL
                                            groupClass="form-group col-md-2-25"
                                            labelTitle={trans('messages.repay_date_to')}>
                                            <DatePicker
                                                placeholderText="YYYY-MM-DD"
                                                dateFormat="YYYY-MM-DD"
                                                locale="ja"
                                                name="repay_date_to"
                                                className="form-control col-md-12 input-sm"
                                                key="repay_date_to"
                                                id="repay_date_to"
                                                selected={this.state.repay_date_to ? moment(this.state.repay_date_to) : null}
                                                onChange={(date,name, e) => this.handleChangeDate(date, 'repay_date_to', e)} />
                                        </LABEL>
                                        <INPUT
                                            name="received_order_id"
                                            groupClass="form-group col-md-1-75"
                                            labelTitle={trans('messages.received_order_id')}
                                            onChange={this.handleChange}
                                            value={this.state.received_order_id||''}
                                            className="form-control input-sm" />
                                    </div>
                                </div>
                            </FORM>
                        </div>
                        <div className="row">
                            <div className="col-md-9 col-xs-6">
                                <a href={saveUrl} className="btn btn-success  margin-element margin-form ">{trans('messages.btn_add')}</a>
                                <BUTTON className="btn btn-primary margin-form  margin-element" onClick={(e) => this.handleExportCsv()}>{trans('messages.csv_export')}</BUTTON>
                            </div>
                            <div className="col-md-3 col-xs-6">
                                    <BUTTON className="btn btn-danger pull-right margin-form margin-form" onClick={this.handleClearClick}>{trans('messages.btn_reset')}</BUTTON>
                                    <BUTTON className="btn btn-primary pull-right margin-element margin-form" onClick={this.handleReloadData} >{trans('messages.btn_search')}</BUTTON>
                            </div>
                        </div>
                    </div>
                    <div className="box-body">
                        <div className="row">
                            <div className="col-md-12">
                                {(totalItems/itemsPerPage > 1) &&
                                <div className="row">
                                    <div className="col-md-2-5"></div>
                                    <div className="col-md-7 text-center">
                                        <PaginationContainer handleClickPage={this.handleClickPage}/>
                                    </div>
                                    <div className="col-md-2-5 text-right line-height-md">
                                        {dataSource.data.from}件 - {dataSource.data.to}件 【全{dataSource.data.total}件】
                                    </div>
                                </div>
                                }
                                <div className="table-responsive">
                                <TABLE className="table table-striped table-custom">
                                    <THEAD>
                                        <TR>
                                            <TH className="visible-xs visible-sm"></TH>
                                            <TH className=""></TH>
                                            <TH className="text-center text-middle">
                                                <SortComponent
                                                    name="sort_repay_no"
                                                    onClick={this.handleSortClick}
                                                    currentSort={(this.state.sort_repay_no)?this.state.sort_repay_no:'none'}
                                                    title={trans('messages.index')} />
                                            </TH>
                                            <TH className="hidden-xs text-center text-middle hidden-sm col-max-min-120">
                                                <SortComponent
                                                    name="sort_repay_name"
                                                    onClick={this.handleSortClick}
                                                    currentSort={(this.state.sort_repay_name)?this.state.sort_repay_name:'none'}
                                                    title={trans('messages.repayment')} />
                                            </TH>
                                            <TH className="hidden-xs hidden-sm text-center text-middle  col-max-min-120">
                                                <SortComponent
                                                    name="sort_total_return_price"
                                                    onClick={this.handleSortClick}
                                                    currentSort={(this.state.sort_total_return_price)?this.state.sort_total_return_price:'none'}
                                                    title={trans('messages.total_return_price_screen')} />
                                            </TH>
                                            <TH className="hidden-xs hidden-sm text-center text-middle col-max-min-100">
                                                <SortComponent
                                                    name="sort_repay_date"
                                                    onClick={this.handleSortClick}
                                                    currentSort={(this.state.sort_repay_date)?this.state.sort_repay_date:'none'}
                                                    title={trans('messages.repay_date')} />
                                            </TH>
                                            <TH className="hidden-xs hidden-sm text-center text-middle col-max-min-100">
                                                <SortComponent
                                                    name="sort_repay_time"
                                                    onClick={this.handleSortClick}
                                                    currentSort={(this.state.sort_repay_time)?this.state.sort_repay_time:'none'}
                                                    title={trans('messages.repay_time')} />
                                            </TH>
                                            <TH className="hidden-xs hidden-sm text-center text-middle col-max-min-100">
                                                <SortComponent
                                                    name="sort_repay_bank_name"
                                                    onClick={this.handleSortClick}
                                                    currentSort={(this.state.sort_repay_bank_name)?this.state.sort_repay_bank_name:'none'}
                                                    title={trans('messages.rep_bank_name')} />
                                            </TH>
                                            <TH className="hidden-xs hidden-sm text-center text-middle col-max-min-100">
                                                <SortComponent
                                                    name="sort_repay_bank_branch_name"
                                                    onClick={this.handleSortClick}
                                                    currentSort={(this.state.sort_repay_bank_branch_name)?this.state.sort_repay_bank_branch_name:'none'}
                                                    title={trans('messages.rep_bank_branch_name')} />
                                            </TH>
                                            <TH className="hidden-xs hidden-sm text-center text-middle col-max-min-100">
                                                <SortComponent
                                                    name="sort_repay_bank_account"
                                                    onClick={this.handleSortClick}
                                                    currentSort={(this.state.sort_repay_bank_account)?this.state.sort_repay_bank_account:'none'}
                                                    title={trans('messages.rep_bank_account')} />
                                            </TH>
                                            <TH className="hidden-xs hidden-sm text-center text-middle col-max-min-100">
                                                <SortComponent
                                                    name="sort_customer_name"
                                                    onClick={this.handleSortClick}
                                                    currentSort={(this.state.sort_customer_name)?this.state.sort_customer_name:'none'}
                                                    title={trans('messages.customer_name')} />
                                            </TH>
                                            <TH className="hidden-xs hidden-sm text-center text-middle col-max-min-100">
                                                <SortComponent
                                                    name="sort_repay_name"
                                                    onClick={this.handleSortClick}
                                                    currentSort={(this.state.sort_repayment_name)?this.state.sort_repayment_name:'none'}
                                                    title={trans('messages.rep_name')} />
                                            </TH>
                                            <TH className="text-center text-middle col-max-min-100">
                                                <SortComponent
                                                    name="sort_request_price"
                                                    onClick={this.handleSortClick}
                                                    currentSort={(this.state.sort_request_price)?this.state.sort_request_price:'none'}
                                                    title={trans('messages.request_price_2')} />
                                            </TH>
                                                <TH className="text-center text-middle col-max-min-100">
                                                <SortComponent
                                                    name="sort_receive_id"
                                                    onClick={this.handleSortClick}
                                                    currentSort={(this.state.sort_receive_id)?this.state.sort_receive_id:'none'}
                                                    title={trans('messages.repayment_receive_id')} />
                                            </TH>
                                            <TH className="text-center text-middle col-max-min-100">
                                                <SortComponent
                                                    name="sort_repayment_status"
                                                    onClick={this.handleSortClick}
                                                    currentSort={(this.state.sort_repayment_status)?this.state.sort_repayment_status:'none'}
                                                    title={trans('messages.repayment_status')} />
                                            </TH>
                                            <TH className="text-center text-middle col-max-min-100">
                                                {trans('messages.is_return')}
                                            </TH>
                                            <TH className="text-center text-middle col-max-min-100">
                                                {trans('messages.repayment_in_date')}
                                            </TH>
                                            <TH className="text-center text-middle col-max-min-100">
                                                {trans('messages.mall_name')}
                                            </TH>
                                            <TH className="text-center text-middle col-max-min-100">
                                                {trans('messages.received_order_id')}
                                            </TH>
                                        </TR>
                                    </THEAD>
                                    <TBODY>
                                        {
                                            (!_.isEmpty(dataSource.data.data))?
                                            dataSource.data.data.map((item) => {
                                                i++;
                                                let classRow = (i%2 ===0) ? 'odd' : 'even';
                                                let isReturn = dataSource.arrIsReturn[item.is_return];
                                                let repayInDate = moment.utc(item.in_date, "YYYY-MM-DD").format("YYYY/MM/DD");;
                                                return (
                                                    [
                                                    <TR key={"tr" + i} className={classRow}>
                                                        <TD className="visible-xs visible-sm text-center">
                                                            <b className="show-info">
                                                                <i className="fa fa-angle-double-down" aria-hidden="true"></i>
                                                            </b>
                                                        </TD>
                                                        <TD className="cut-text text-center"  title={item.repay_name}>
                                                            <BUTTON className="btn bg-purple input-sm "
                                                                    data-repay_no={item.repay_no}
                                                                    data-repayment_status={item.repayment_status}
                                                                    onClick={(e) => this.handleChangeFinished(e,item)}>
                                                            {item.repayment_status === 0 ? '完了' : '戻る'}
                                                            </BUTTON>
                                                        </TD>
                                                        <TD className="text-center">
                                                            <a className="link-detail" href={saveUrl + "?repay_no=" + item.repay_no}>
                                                                {item.repay_no}
                                                            </a>
                                                        </TD>
                                                        <TD className="hidden-xs hidden-sm cut-text text-left"  title={item.repay_name}>
                                                            <span className="limit-char max-width-70">{item.repay_name}</span>
                                                        </TD>
                                                        <TD className="hidden-xs hidden-sm cut-text text-right"  title={item.total_return_price}>
                                                            <span className="limit-char max-width-70">{nf.format(item.total_return_price)}</span>
                                                        </TD>
                                                        <TD className="hidden-xs hidden-sm cut-text text-left"  title={item.repayment_date}>
                                                           {item.repayment_date ? new Date(item.repayment_date).toISOString().split('T')[0] : ''}
                                                        </TD>
                                                        <TD className="hidden-xs hidden-sm cut-text text-left"  title={item.one_area_n_pos}>
                                                            {item.repayment_date ? new Date(item.repayment_date).toLocaleTimeString('ja-JP') : ''}
                                                        </TD>
                                                        <TD className="hidden-xs hidden-sm cut-text text-left"  title={item.repay_bank_name}>
                                                            <span className="limit-char max-width-70">{item.repay_bank_name}</span>
                                                        </TD>
                                                        <TD className="hidden-xs hidden-sm cut-text text-left"  title={item.repay_bank_branch_name}>
                                                            <span className="limit-char max-width-70">{item.repay_bank_branch_name}</span>
                                                        </TD>
                                                        <TD className="hidden-xs hidden-sm cut-text text-left"  title={item.repay_bank_account}>
                                                            <span className="limit-char max-width-70">{item.repay_bank_account}</span>
                                                        </TD>
                                                        <TD className="hidden-xs hidden-sm cut-text text-left"  title={item.full_name}>
                                                            <span className="limit-char max-width-70">{item.full_name}</span>
                                                        </TD>
                                                        <TD className="hidden-xs hidden-sm cut-text text-left"  title="">
                                                            <span className="limit-char max-width-70"></span>
                                                        </TD>
                                                        <TD className="cut-text text-right"  title={item.request_price}>
                                                            <span className="limit-char max-width-100">{nf.format(item.request_price)}</span>
                                                        </TD>
                                                        <TD className="cut-text text-right"  title={item.payment_id}>
                                                            <span className="limit-char max-width-100">{item.receive_id}</span>
                                                        </TD>
                                                        <TD className="cut-text text-center"  title={item.repayment_status}>
                                                            <span className="limit-char max-width-100">{(item.repayment_status === 0 ? '未':'済')}</span>
                                                        </TD>
                                                        <TD className="cut-text text-center"  title={isReturn}>
                                                            <span className="limit-char max-width-100">{isReturn}</span>
                                                        </TD>
                                                        <TD className="cut-text text-center"  title={repayInDate}>
                                                            <span className="limit-char max-width-100">{repayInDate}</span>
                                                        </TD>
                                                        <TD className="cut-text text-center"  title={item.name_jp}>
                                                            <span className="limit-char max-width-100">{item.name_jp}</span>
                                                        </TD>
                                                        <TD className="cut-text"  title={item.received_order_id}>
                                                            <span className="limit-char max-width-100">{item.received_order_id}</span>
                                                        </TD>
                                                    </TR>,
                                                    <TR key={"trhide" + i} className="hiden-info" style={{display:"none"}}>
                                                        <TD colSpan="6" className="visible-xs visible-sm width-span1">
                                                            <ul id={"temp" + item.repay_no}>
                                                                <li>
                                                                    <b>{trans('messages.index')} : </b>{item.repay_no}
                                                                </li>
                                                                <li>
                                                                    <b>{trans('messages.repayment')} : </b>{item.repay_name}
                                                                </li>
                                                                <li>
                                                                    <b>{trans('messages.total_return_price_screen')} : </b>{item.total_return_price}
                                                                </li>
                                                                <li>
                                                                    <b>{trans('messages.repay_date')} : </b>
                                                                    {(new Date(item.repayment_date)).toISOString().split('T')[0]}
                                                                </li>
                                                                <li>
                                                                    <b>{trans('messages.repay_time')} : </b>
                                                                    {(new Date(item.repayment_date).toLocaleTimeString())}
                                                                </li>
                                                                <li>
                                                                    <b>{trans('messages.rep_bank_name')} : </b>{item.repay_bank_name}
                                                                </li>
                                                                <li>
                                                                    <b>{trans('messages.rep_bank_branch_name')} : </b>{item.repay_bank_branch_name}
                                                                </li>
                                                                <li>
                                                                    <b>{trans('messages.rep_bank_account')} : </b>{item.repay_bank_account}
                                                                </li>
                                                                <li>
                                                                    <b>{trans('messages.rep_name')} : </b>{item.repayment_name}
                                                                </li>
                                                                <li>
                                                                    <b>{trans('messages.customer_name')} : </b>{item.full_name}
                                                                </li>
                                                                <li>
                                                                    <b>{trans('messages.request_price_2')} : </b>{item.request_price}
                                                                </li>
                                                                <li>
                                                                    <b>{trans('messages.repayment_receive_id')} : </b>{item.receive_id}
                                                                </li>
                                                                <li>
                                                                    <b>{trans('messages.status')} : </b>{item.repayment_status}
                                                                </li>
                                                                <li>
                                                                    <b>{trans('messages.is_return')} : </b>{isReturn}
                                                                </li>
                                                                <li>
                                                                    <b>{trans('messages.repayment_in_date')} : </b>{repayInDate}
                                                                </li>
                                                                <li>
                                                                    <b>{trans('messages.mall_name')} : </b>{item.name_jp}
                                                                </li>
                                                                <li>
                                                                    <b>{trans('messages.received_order_id')} : </b>{item.received_order_id}
                                                                </li>
                                                            </ul>
                                                        </TD>
                                                    </TR>
                                                   ]
                                                )
                                            })
                                            :
                                            <TR><TD colSpan="14" className="text-center">{trans('messages.no_data')}</TD></TR>
                                        }
                                    </TBODY>
                                </TABLE>
                                </div>
                                <div className="text-center">
                                    <PaginationContainer handleClickPage={this.handleClickPage}/>
                                </div>
                            </div>
                        </div>
                    </div>
                    </div>
                </div>

        )
    }
}

const mapStateToProps = (state) => {
    return {
        dataSource: state.commonReducer.dataSource
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        reloadData: (url, params, pushHistory, ignoreFields) => { dispatch(Action.reloadData(url, params, pushHistory, ignoreFields)) },
        postData: (url, params, headers, callBackUrl, callBackParams) => { dispatch(Action.postData(url, params, headers, callBackUrl, callBackParams)) }

    }
}

export default connect(mapStateToProps, mapDispatchToProps)(RePaymentList)