import React, { Component } from 'react';
import { connect } from 'react-redux';

import * as Action from '../common/actions';
import * as queryString from 'query-string';

import Loading from '../common/components/common/Loading';
import FORM from '../common/components/form/FORM';
import INPUT from '../common/components/form/INPUT';
import SELECT from '../common/components/form/SELECT';
import BUTTON from '../common/components/form/BUTTON';

import TABLE from '../common/components/table/TABLE';
import THEAD from '../common/components/table/THEAD';
import TBODY from '../common/components/table/TBODY';
import TR from '../common/components/table/TR';
import TH from '../common/components/table/TH';
import TD from '../common/components/table/TD';
import MESSAGE_MODAL from '../common/containers/MessageModal';
import axios from 'axios';
let timer;
class RePaymentSave extends Component {

    constructor(props) {
        super(props);
        this.state = {};
        let params = queryString.parse(props.location.search);
        if (params['repay_no']) {
            this.state = {
                repay_no: params['repay_no'],
            };
            this.state.accessFlg = true;
        }
        this.state.dataInfo  = {};
        this.state.dataOrder = {};
        this.state.dataError = {};
        this.props.reloadData(formDataUrl, this.state);

    }
    hanleRealtime() {
        if (this.state.repay_no) {
            let params = {
                accessFlg : this.state.accessFlg,
                query : {
                        page_key : 'REPAYMENT-SAVE',
                        primary_key : 'index:' + this.state.repay_no
                    },
                key :this.state.repay_no
            };
            if (this.state.accessFlg !== false) {
                this.props.postRealTime(checkEditUrl, params, {'X-Requested-With': 'XMLHttpRequest'});
            }
        }
    }
    componentWillReceiveProps(nextProps) {
        let dataSource = nextProps.dataSource;
        if (!_.isEmpty(dataSource.data) && _.isEmpty(nextProps.dataRealTime)) {
            this.setState({...dataSource.data, dataOrder: dataSource.dataOrder}, () => {
                this.hanleRealtime();
            });
        }
        // if (_.isEmpty(dataSource.data)) {
        //     window.location.href = baseUrl;
        // }
        if (!_.isEmpty(nextProps.dataResponse.method) && nextProps.dataResponse.method === 'save') {
            if (nextProps.dataResponse.status === 1) {
                window.location.href = baseUrl;
            } else {
                document.getElementsByName('btnAccept')[0].disabled = false;
            }
        }
        // Check realtime update
        if (!_.isEmpty(nextProps.dataRealTime)) {
            this.setState(nextProps.dataRealTime);
        }
    }

    handleChange = (event) => {
        const target = event.target;
        const name = target.name;
        const value = target.type === 'checkbox' ? target.checked : target.value;
        if (name === 'received_order_id') {
            clearTimeout(timer);
            let _this = this;
            timer = setTimeout(function() {
                _this.getInfoProduct(value);
            }, 1000);
        }
        if (name === 'repayment_type') {
            clearTimeout(timer);
            let _this = this;
            timer = setTimeout(function() {
                _this.getRepaymentPrice(
                    _this.state.receive_id,
                    value
                );
            }, 2000);
        }
        let dataSetState = {[name]: value};
        let arrReturnPriceCheck = ['repay_price', 'return_point', 'repay_coupon', 'return_fee', 'other_commission', 'debosit_commission'];
        if (arrReturnPriceCheck.indexOf(name) !== -1) {
            let arrValue = [];
            arrReturnPriceCheck.map((item) => {
                if (name === item) {
                    arrValue[item] = parseInt(value) || 0;
                } else {
                    arrValue[item] = parseInt(this.state[item]) || 0;
                }
            });
            let totalReturnPrice = 0;
            if (parseInt(this.state['repayment_type']) === 1) {
                if (arrValue['return_point'] + arrValue['repay_coupon']
                    - (arrValue['return_fee'] + arrValue['other_commission']) > 0 ) {
                    totalReturnPrice = arrValue['repay_price'];
                } else {
                    totalReturnPrice = arrValue['repay_price'] + arrValue['return_point']
                        + arrValue['repay_coupon'] - (arrValue['return_fee'] + arrValue['other_commission']);
                }
            } else {
                totalReturnPrice = arrValue['repay_price'] + arrValue['debosit_commission'];
            }
            dataSetState['total_return_price'] = totalReturnPrice;
        }

        this.setState(dataSetState);
    }

    getRepaymentPrice = (receiveId, returnType) => {
        $(".loading").show();
        let param = {
            receive_id : receiveId,
            return_type : returnType,
            type_check_return : 'check_repayment'
        };
        let headers = {'X-Requested-With': 'XMLHttpRequest'};
        axios.post(getRepaymentPriceUrl, param, {headers : headers}
        ).then(response => {
            let dataSet = {};
            dataSet.repay_price  = response.data.repayPrice;
            dataSet.return_point = response.data.repayPoint;
            dataSet.repay_coupon = response.data.repayCoupon;
            dataSet.sum_price    = response.data.sumPrice;

            let totalReturnPrice  = 0;
            let repayPrice        = parseInt(response.data.repayPrice) || 0;
            let repayPoint        = parseInt(response.data.repayPoint) || 0;
            let repayCoupon       = parseInt(response.data.repayCoupon) || 0;
            let returnFee         = parseInt(this.state.return_fee) || 0;
            let otherCommission   = parseInt(this.state.other_commission) || 0;
            let debositCommission = parseInt(this.state.debosit_commission) || 0;
            let repayPointSet     = repayPoint;
            let repayCouponSet    = repayCoupon;
            if (parseInt(returnType) === 1) {
                if (!this.state.repay_no) {
                    returnFee = 2000;
                    dataSet.return_fee = 2000;
                    let cal = repayPoint - (returnFee + otherCommission);
                    if (cal < 0) {
                        dataSet.return_point = 0;
                        repayPointSet        = 0;
                        if ((repayCoupon + cal) < 0) {
                            dataSet.repay_coupon = 0;
                            repayCouponSet       = 0;
                        } else {
                            dataSet.repay_coupon = repayCoupon + cal;
                            repayCouponSet       = repayCoupon + cal;
                        }
                    } else {
                        dataSet.return_point = cal;
                        repayPointSet        = cal;
                    }
                }
                if(repayPoint + repayCoupon - (returnFee + otherCommission) > 0){
                    totalReturnPrice = repayPrice;
                } else {
                    totalReturnPrice = repayPrice + repayPoint + repayCoupon - (returnFee + otherCommission);
                }
            } else {
                totalReturnPrice = repayPrice + debositCommission;
                if (!this.state.repay_no) {
                    dataSet.return_fee = 0;
                }
            }
            dataSet.total_return_price = totalReturnPrice;
            dataSet.order_return_price = response.data.orderReturnPrice;
            dataSet.order_return_point = response.data.orderReturnPoint;
            dataSet.order_return_coupon = response.data.orderReturnCoupon;
            $('input[name=repay_price]').val(parseInt(response.data.repayPrice) || 0);
            $('input[name=return_point]').val(parseInt(repayPointSet) || 0);
            $('input[name=repay_coupon]').val(parseInt(repayCouponSet) || 0);
            $('input[name=total_return_price]').val(parseInt(totalReturnPrice) || 0);
            this.setState({...dataSet}, () => {
                $(".loading").hide();
            });
        }).catch(error => {
            $(".loading").hide();
            throw(error);
        });
    }

    getInfoProduct = (receiveOrderId) => {
        $(".loading").show();
        let formData = new FormData();
        let headers = {'X-Requested-With': 'XMLHttpRequest'};
        formData.append('received_order_id', receiveOrderId);
        formData.append('repay_no', this.state.repay_no);
        axios.post(getInfoOrderUrl, formData, {headers : headers}
        ).then(response => {
            $(".loading").hide();
            let data = response.data;
            let sale_price = [];
            let received_order_num = [];
            let sub_total = [];
            if (typeof data.status !== undefined) {
                this.setState({
                    dataError : data
                }) ;
            }
//            if (!this.state.repay_no) {
//                this.setState({return_fee : 2000});
//            }
            this.setState({
                ...data.dataInfo , dataOrder : data.dataOrder
            });
            if(!_.isEmpty(data.dataOrder)){
                data.dataOrder.map((item) => {
                        sale_price['sale_price-' + item.receive_id +'-'+ item.detail_line_num +'-'+ item.sub_line_num] = item.sale_price
                        received_order_num['received_order_num-' + item.receive_id +'-'+ item.detail_line_num +'-'+ item.sub_line_num] = item.received_order_num
                        sub_total['sub_total-' + item.receive_id +'-'+ item.detail_line_num +'-'+ item.sub_line_num] = item.sub_total
               });
              this.setState(sale_price)
              this.setState(received_order_num)
              this.setState(sub_total)
            }
            this.getRepaymentPrice(this.state.receive_id, this.state.repayment_type);
        }).catch(error => {
            $(".loading").hide();
            throw(error);
        });
    }

    handleCancel = (event) => {
        window.location.href = baseUrl;
    }

    handleSubmit = (event) => {
        this.setState({dataError : ''})
        document.getElementsByName('btnAccept')[0].disabled = true;
        let headers = {'X-Requested-With': 'XMLHttpRequest'};
        if (this.props.enctype) {
            headers['Content-Type'] = this.props.enctype;
        }

        this.props.postData(this.props.location.pathname, this.state, headers);
    }

    componentDidUpdate() {
        if (!_.isEmpty(this.props.dataSource)) {
            $('.loading').hide();
        }
        if (this.state.accessFlg === false) {
            $('#message-modal').modal('show');
            //We will auto redirect after 5 seconds
            setTimeout(() => {
                window.location.href = baseUrl;
            }, 5000);
        }
    }
    componentWillUnmount () {
        clearInterval(this.state);
    }
    componentDidMount() {
         setInterval(() => {
            this.hanleRealtime()
        }, 5000);

    }
    render() {
        let dataSource   = this.props.dataSource;
        let dataResponse = this.state.dataError ? this.state.dataError : this.props.dataResponse;
        console.log(dataSource);
        let index = 0;
        let return_total_price = 0;
        var nf = new Intl.NumberFormat();
        return ((!_.isEmpty(dataSource)) &&
            <div>
            <div className="box box-primary">
            <FORM className="form-horizontal ">
                <div className="box-body">
                <Loading className="loading"></Loading>
                <div className="row">
                    <div className="col-md-5">
                        <div className="row">
                            <div className="col-md-12">
                                <INPUT
                                    name="received_order_id"
                                    labelTitle={trans('messages.received_order_id')}
                                    className="form-control input-sm"
                                    fieldGroupClass="col-md-8 "
                                    labelClass="col-md-4"
                                    groupClass=""
                                    errorPopup={true}
                                    readOnly={typeof this.state.repay_no !== 'undefined' ? true : false}
                                    onChange={this.handleChange}
                                    defaultValue={dataSource.data.received_order_id}
                                    hasError={dataResponse.message&&dataResponse.message.received_order_id?true:false}
                                    errorMessage={dataResponse.message&&dataResponse.message.received_order_id?dataResponse.message.received_order_id:""}
                                    />
                            </div>
                            <div className="col-md-12">
                                <INPUT
                                    name="repay_bank_code"
                                    labelTitle={trans('messages.rep_bank_code')}
                                    className="form-control input-sm"
                                    fieldGroupClass="col-md-4"
                                    labelClass="col-md-4"
                                    groupClass=""
                                    errorPopup={true}
                                    onChange={this.handleChange}
                                    defaultValue={dataSource.data.repay_bank_code}
                                    hasError={dataResponse.message&&dataResponse.message.repay_bank_code?true:false}
                                    errorMessage={dataResponse.message&&dataResponse.message.repay_bank_code?dataResponse.message.repay_bank_code:""}
                                    />
                            </div>
                            <div className="col-md-12">
                                <INPUT
                                    name="repay_bank_name"
                                    labelTitle={trans('messages.rep_bank_name')}
                                    className="form-control input-sm"
                                    fieldGroupClass="col-md-8"
                                    labelClass="col-md-4"
                                    groupClass=""
                                    errorPopup={true}
                                    onChange={this.handleChange}
                                    defaultValue={dataSource.data.repay_bank_name}
                                    hasError={dataResponse.message&&dataResponse.message.repay_bank_name?true:false}
                                    errorMessage={dataResponse.message&&dataResponse.message.repay_bank_name?dataResponse.message.repay_bank_name:""}
                                    />
                            </div>
                            <div className="col-md-12">
                                <INPUT
                                    name="repay_bank_branch_code"
                                    labelTitle={trans('messages.rep_bank_branch_code')}
                                    className="form-control input-sm"
                                    fieldGroupClass="col-md-8"
                                    labelClass="col-md-4"
                                    groupClass=""
                                    errorPopup={true}
                                    onChange={this.handleChange}
                                    defaultValue={dataSource.data.repay_bank_branch_code}
                                    hasError={dataResponse.message&&dataResponse.message.repay_bank_branch_code?true:false}
                                    errorMessage={dataResponse.message&&dataResponse.message.repay_bank_branch_code?dataResponse.message.repay_bank_branch_code:""}
                                    />
                            </div>
                            <div className="col-md-12">
                                <INPUT
                                    name="repay_bank_branch_name"
                                    labelTitle={trans('messages.rep_bank_branch_name')}
                                    className="form-control input-sm"
                                    fieldGroupClass="col-md-8"
                                    labelClass="col-md-4"
                                    groupClass=""
                                    errorPopup={true}
                                    onChange={this.handleChange}
                                    defaultValue={dataSource.data.repay_bank_branch_name}
                                    hasError={dataResponse.message&&dataResponse.message.repay_bank_branch_name?true:false}
                                    errorMessage={dataResponse.message&&dataResponse.message.repay_bank_branch_name?dataResponse.message.repay_bank_branch_name:""}
                                    />
                            </div>
                            <div className="col-md-12">
                                <INPUT
                                    name="repay_bank_account"
                                    labelTitle={trans('messages.rep_bank_account')}
                                    className="form-control input-sm"
                                    fieldGroupClass="col-md-8"
                                    labelClass="col-md-4"
                                    groupClass=""
                                    errorPopup={true}
                                    onChange={this.handleChange}
                                    defaultValue={dataSource.data.repay_bank_account}
                                    hasError={dataResponse.message&&dataResponse.message.repay_bank_account?true:false}
                                    errorMessage={dataResponse.message&&dataResponse.message.repay_bank_account?dataResponse.message.repay_bank_account:""}
                                    />
                            </div>

                            <div className="col-md-12">
                                <INPUT
                                    name="repay_name"
                                    labelTitle={trans('messages.rep_name')}
                                    className="form-control input-sm"
                                    fieldGroupClass="col-md-8"
                                    labelClass="col-md-4"
                                    groupClass=""
                                    errorPopup={true}
                                    onChange={this.handleChange}
                                    defaultValue={dataSource.data.repay_name}
                                    hasError={dataResponse.message&&dataResponse.message.repay_name?true:false}
                                    errorMessage={dataResponse.message&&dataResponse.message.repay_name?dataResponse.message.repay_name:""}
                                    />
                            </div>
                            <div className="col-md-12">
                                <INPUT
                                    name="repay_price"
                                    labelTitle={trans('messages.repayment_price')}
                                    className="form-control input-sm"
                                    fieldGroupClass="col-md-8"
                                    labelClass="col-md-4"
                                    groupClass=""
                                    errorPopup={true}
                                    onChange={this.handleChange}
                                    defaultValue={dataSource.data.repay_price}
                                    hasError={dataResponse.message&&dataResponse.message.repay_price?true:false}
                                    errorMessage={dataResponse.message&&dataResponse.message.repay_price?dataResponse.message.repay_price:""}
                                    />
                            </div>
                            <div className="col-md-12">
                                <INPUT
                                    name="return_point"
                                    labelTitle={trans('messages.return_point')}
                                    className="form-control input-sm"
                                    fieldGroupClass="col-md-8"
                                    labelClass="col-md-4"
                                    groupClass=""
                                    errorPopup={true}
                                    onChange={this.handleChange}
                                    defaultValue={dataSource.data.return_point}
                                    hasError={dataResponse.message&&dataResponse.message.return_point?true:false}
                                    errorMessage={dataResponse.message&&dataResponse.message.return_point?dataResponse.message.return_point:""}
                                    />
                            </div>
                            <div className="col-md-12">
                                <INPUT
                                    name="repay_coupon"
                                    labelTitle={trans('messages.repay_coupon')}
                                    className="form-control input-sm"
                                    fieldGroupClass="col-md-8"
                                    labelClass="col-md-4"
                                    groupClass=""
                                    errorPopup={true}
                                    onChange={this.handleChange}
                                    defaultValue={dataSource.data.return_coupon}
                                    hasError={dataResponse.message&&dataResponse.message.repay_coupon?true:false}
                                    errorMessage={dataResponse.message&&dataResponse.message.repay_coupon?dataResponse.message.repay_coupon:""}
                                    />
                            </div>
                            <div className="col-md-12">
                                <INPUT
                                    name="debosit_commission"
                                    labelTitle={trans('messages.debosit_commission')}
                                    className="form-control input-sm"
                                    fieldGroupClass="col-md-8"
                                    labelClass="col-md-4"
                                    groupClass=""
                                    errorPopup={true}
                                    onChange={this.handleChange}
                                    defaultValue={dataSource.data.deposit_transfer_commission}
                                    hasError={dataResponse.message&&dataResponse.message.debosit_commission?true:false}
                                    errorMessage={dataResponse.message&&dataResponse.message.debosit_commission?dataResponse.message.debosit_commission:""}
                                    />
                            </div>
                            <div className="col-md-12">
                                <INPUT
                                    name="other_commission"
                                    labelTitle={trans('messages.other_commission')}
                                    className="form-control input-sm"
                                    fieldGroupClass="col-md-8"
                                    labelClass="col-md-4"
                                    groupClass=""
                                    errorPopup={true}
                                    onChange={this.handleChange}
                                    defaultValue={dataSource.data.other_commission}
                                    hasError={dataResponse.message&&dataResponse.message.other_commission?true:false}
                                    errorMessage={dataResponse.message&&dataResponse.message.other_commission?dataResponse.message.other_commission:""}
                                    />
                            </div>

                            <div className="col-md-12">
                                <SELECT
                                    name="is_return"
                                    labelTitle={trans('messages.is_return')}
                                    className="form-control input-sm"
                                    fieldGroupClass="col-md-4"
                                    labelClass="col-md-4"
                                    onChange={this.handleChange}
                                    options={dataSource.isReturn}
                                    errorPopup={true}
                                    defaultValue={dataSource.data.is_return}
                                    hasError={(dataResponse.message && dataResponse.message.is_return)?true:false}
                                    errorMessage={(dataResponse.message&&dataResponse.message.is_return)?dataResponse.message.is_return:""}
                                    />
                            </div>
                        </div>
                    </div>
                    <div className="col-md-7">
                        <div className="row">
                            <div className="col-md-12">
                                <div className="form-group">
                                    <label className="col-md-2">{trans('messages.customer_id')}</label>
                                    <div className="col-md-8">
                                        <p className="add-line-dotted">
                                        {
                                            typeof this.state.full_name !== 'undefined' ?
                                                this.state.full_name
                                                :
                                                '------------'
                                        }
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <div className="col-md-12">
                                <div className="form-group">
                                    <label className="col-md-2">{trans('messages.payment_name')}</label>
                                    <div className="col-md-6 ">
                                        <p className="add-line-dotted">
                                        {
                                            typeof this.state.payment_name !== 'undefined' ?
                                                this.state.payment_name
                                                :
                                                '------------'
                                        }
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="row">
                            <div className="col-md-6">
                                <div className="form-group">
                                    <label className="col-md-4">{trans('messages.total_sub')}</label>
                                    <div className="col-md-8">
                                        <p className="add-line-bottom-dotted">
                                            {
                                            typeof this.state.sub_total !== 'undefined' ?
                                                (this.state.sub_total ? nf.format(this.state.sub_total) : 0)
                                                :
                                                '0'
                                            }
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <div className="col-md-6">
                                <div className="form-group">
                                    <label className="col-md-4">{trans('messages.used_point')}</label>
                                    <div className="col-md-8">
                                        <p className="add-line-bottom-dotted">
                                            {
                                            typeof this.state.used_point !== 'undefined' ?
                                                this.state.used_point
                                                :
                                                '0'
                                            }
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="row">
                            <div className="col-md-6">
                                <div className="form-group">
                                    <label className="col-md-4">{trans('messages.pay_charge_discount')}</label>
                                    <div className="col-md-6 ">
                                        <p className="add-line-bottom-dotted">
                                        {
                                            typeof this.state.pay_charge_discount !== 'undefined' ?
                                                this.state.pay_charge_discount
                                                :
                                                0
                                        }
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <div className="col-md-6">
                                <div className="form-group">
                                    <label className="col-md-4">{trans('messages.pay_charge')}</label>
                                    <div className="col-md-6 ">
                                        <p className="add-line-bottom-dotted">
                                        {
                                            typeof this.state.pay_charge !== 'undefined' ?
                                                (this.state.pay_charge ? nf.format(this.state.pay_charge) : 0)
                                                :
                                                0
                                        }
                                        </p>
                                    </div>

                                </div>
                            </div>
                        </div>
                        <div className="row">
                            <div className="col-md-6">
                                <div className="form-group">
                                    <label className="col-md-4">{trans('messages.ship_charge')}</label>
                                    <div className="col-md-6 ">
                                        <p className="add-line-bottom-dotted">
                                        {
                                            typeof this.state.ship_charge !== 'undefined' ?
                                                (this.state.ship_charge ? nf.format(this.state.ship_charge) : 0)
                                                :
                                                0
                                        }
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <div className="col-md-6">
                                <div className="form-group">
                                    <label className="col-md-4">{trans('messages.used_coupon')}</label>
                                    <div className="col-md-6 ">
                                        <p className="add-line-bottom-dotted">
                                        {
                                            typeof this.state.used_coupon !== 'undefined' ?
                                                this.state.used_coupon
                                                :
                                                0
                                        }
                                        </p>
                                    </div>

                                </div>
                            </div>
                        </div>
                        <div className="row">
                            <div className="col-md-6">
                                <div className="form-group">
                                    <label className="col-md-4">{trans('messages.discount')}</label>
                                    <div className="col-md-6 ">
                                        <p className="add-line-bottom-dotted">
                                        {
                                            typeof this.state.discount !== 'undefined' ?
                                                (this.state.discount ? nf.format(this.state.discount) : 0)
                                                :
                                                0
                                        }
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <div className="col-md-6">
                                <div className="form-group">
                                    <label className="col-md-4">{trans('messages.total_price')}</label>
                                    <div className="col-md-6 ">
                                        <p className="add-line-bottom-dotted">
                                        {
                                            typeof this.state.total_price !== 'undefined' ?
                                                (this.state.total_price ? nf.format(this.state.total_price) : 0)
                                                :
                                                0
                                        }
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="row">
                            <div className="col-md-6">
                                <div className="form-group">
                                    <label className="col-md-4">{trans('messages.pay_after_charge')}</label>
                                    <div className="col-md-6 ">
                                        <p className="add-line-bottom-dotted">
                                        {
                                            typeof this.state.pay_after_charge !== 'undefined' ?
                                                (this.state.pay_after_charge ? nf.format(this.state.pay_after_charge) : 0)
                                                :
                                                0
                                        }
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <div className="col-md-6">
                                <div className="form-group">
                                    <label className="col-md-4">{trans('messages.request_price')}</label>
                                    <div className="col-md-6 ">
                                        <p className="add-line-bottom-dotted">
                                        {
                                            typeof this.state.request_price !== 'undefined' ?
                                                (this.state.request_price ? nf.format(this.state.request_price) : 0)
                                                :
                                                0
                                        }
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div className="row">
                        <div className="col-md-12"><h4>返品一覧</h4></div>
                            <div className="col-md-11 col-md-offset-1">
                                <TABLE className="table table-striped table-custom">
                                    <THEAD>
                                        <TR>
                                            <TH className="text-center">商品コード</TH>
                                            <TH className="text-center">売価</TH>
                                            <TH className="text-center">個数</TH>
                                            <TH className="text-center">小計</TH>
                                        </TR>
                                    </THEAD>
                                    <TBODY>
                                    {
                                            (!_.isEmpty(this.state.dataOrder)) && this.state.order_status !== _.status('ORDER_STATUS.ON_HOLD')?
                                            [this.state.dataOrder.map((item) => {
                                                index++;
                                                let classRow = (index%2 ===0) ? 'odd' : 'even';
                                                let returnQuantity = item.return_quantity;
                                                if (!item.return_quantity) {
                                                    returnQuantity = item.quantity;
                                                }
                                                let subTotal  = item.sale_price * returnQuantity;
                                                return_total_price += subTotal || 0;
                                                return (
                                                    <TR key={"tr" + index} className={classRow}>
                                                        <TD className="cut-text text-left" title={item.product_code}>
                                                            <span className="limit-char max-width-100">{item.product_code}</span>
                                                        </TD>
                                                        <TD className="text-right cut-text" title={item.sale_price}>
                                                            {nf.format(item.sale_price)}
                                                        </TD>
                                                        <TD className="text-right cut-text" title={returnQuantity }>
                                                            <span className="limit-char max-width-100">{nf.format(returnQuantity)}</span>
                                                        </TD>
                                                        <TD className="text-right cut-text" title={item.sub_total}>
                                                            <span className="limit-char max-width-100">{nf.format(subTotal)}</span>
                                                        </TD>
                                                    </TR>
                                                )
                                            }),
                                            <TR key='return_total_price'>
                                                <TD colSpan="2">

                                                </TD>
                                                <TD className="text-right cut-text" >
                                                <b>合計: </b>
                                                </TD>
                                                <TD  className="text-right cut-text" title={return_total_price}>
                                                <span className="limit-char max-width-100"><b>{nf.format(return_total_price)}</b></span>
                                                </TD>
                                            </TR>
                                            ]
                                            :
                                            <TR><TD colSpan="4" className="text-center">{trans('messages.no_data')}</TD></TR>
                                        }
                                    </TBODY>
                                </TABLE>
                            </div>

                            {
                                (this.state.receive_id)?
                                    <div className="col-md-12">
                                        <div className="row">
                                            <div className="col-md-6">
                                                <SELECT
                                                    name="repayment_type"
                                                    labelTitle={trans('messages.return_type')}
                                                    className="form-control input-sm"
                                                    fieldGroupClass="col-md-6"
                                                    labelClass="col-md-4"
                                                    onChange={this.handleChange}
                                                    options={{0:'大都のせい', 1:'お客様の希望'}}
                                                    defaultValue={
                                                            dataSource.data.repayment_type !== '' ?
                                                            dataSource.data.repayment_type : this.state.repayment_type === 1 ? 1 : 0
                                                            }
                                                    errorPopup={true}
                                                    hasError={(dataResponse.message && dataResponse.message.repayment_type)?true:false}
                                                    errorMessage={(dataResponse.message&&dataResponse.message.repayment_type)?dataResponse.message.repayment_type:""}
                                                    disabled={(this.state.is_return===0||this.state.is_return===2)?true:false}
                                                    />
                                            </div>
                                        </div>
                                    </div>
                                : ''
                            }
                            <div className="col-md-12">
                                <div className="row">
                                    <div className="col-md-6">
                                        <div className="form-group">
                                            <label className="col-md-4">{trans('messages.return_fee')}</label>
                                            <div className="col-md-8">
                                                <input name="return_fee" className="form-control input-sm" disabled="disabled" value={this.state.return_fee || 0} onChange={this.handleChange} />
                                            </div>
                                        </div>
                                    </div>
                                    <div className="col-md-6">
                                        <div className="form-group">
                                            <label className="col-md-4">{trans('messages.total_return_price')}</label>
                                            <div className="col-md-8">
                                                <input name="total_return_price" className="form-control input-sm" disabled="disabled" value={this.state.total_return_price || 0} />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>


                            <div className="col-md-12">
                                <BUTTON className="btn bg-purple input-sm pull-right btn-larger" type="button" onClick={this.handleCancel}>{"取消"}</BUTTON>
                                <BUTTON name="btnAccept" className="btn btn-success input-sm btn-larger pull-right margin-element" type="button" onClick={this.handleSubmit}>{"登録"}</BUTTON>
                            </div>
                        </div>
                    </div>
                </div>
                </div>
            </FORM>
            <MESSAGE_MODAL
                message={this.state.message || ''}
                title={trans('messages.deny_edit')}
                baseUrl={baseUrl}
            />
            </div>
            </div>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        dataSource: state.commonReducer.dataSource,
        dataResponse: state.commonReducer.dataResponse,
        dataRealTime: state.commonReducer.dataRealTime
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        reloadData: (url, params) => { dispatch(Action.detailData(url, params)) },
        postData: (url, params, headers) => { dispatch(Action.postData(url, params, headers))},
        postRealTime: (url, params, headers) => { dispatch(Action.postRealTime(url, params, headers)) }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(RePaymentSave)