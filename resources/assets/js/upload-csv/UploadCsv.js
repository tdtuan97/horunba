import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import * as Action from '../common/actions';
import * as helpers from '../common/helpers';
import * as queryString from 'query-string';
import createHistory from 'history/createBrowserHistory';
import FORM from '../common/components/form/FORM';
import SELECT from '../common/components/form/SELECT';
import Loading from '../common/components/common/Loading';
import UPLOAD from '../common/components/form/UPLOAD';

import axios from 'axios';

class UploadCsv extends Component {
    constructor(props) {
        super(props);
        this.state = {mall:'',attached_file_csv_path : '', message : '', flg : false, flgCsv : false}
        this.handleChange = this.handleChange.bind(this);
        this.handleImportSubmit = this.handleImportSubmit.bind(this);
    }

    handleChange = (event) => {
        const target = event.target;
        const name = target.name;
        const value = target.value;
        this.setState({
            [name]: value
        });
    }
    handleChooseFile = (event) => {
        const target = event.target;
        if (target.files !== null && target.files.length !== 0) {
            const name = target.name;
            let tmpName = name.replace(new RegExp("^browser_"), '');
            this.setState({
                [tmpName]: target.files[0].name,
                ['attached_file_csv']: target.files[0]
            }, ()=> {
                document.getElementsByName(tmpName)[0].value = target.files[0].name;
            });
        }
    }
    handleImportSubmit = () => {
        $(".csvErr").remove();
        $(".csvSuccess").remove();
        let formData   = new FormData();
        let headers = {'X-Requested-With': 'XMLHttpRequest'};
        if (!_.isEmpty(this.state)) {
            Object.keys(this.state).map((item) => {
                formData.append(item, this.state[item]);
            });
        }
        this.flg_check = true;
        this.setState({
            process: true
        }, () => {
            this.props.postDataCSV(uploadCsvUrl, formData, headers);
        });
    }

    handleCancelClick = () => {
        this.setState({
            mall: '',
            flg: 0,
            flgCsv: 0,
            message: '',
            attached_file_csv_path: ''
        },() => {
            $("input[name='attached_file_csv_path']").val('');
            $("input[name='browser_attached_file_csv_path']").val('');
        });
        delete this.props.dataResponseCsv.message;
        $('#add-csv-modal').modal('hide');
    }
    componentWillReceiveProps(nextProps) {
        let dataResponseCsv = nextProps.dataResponseCsv;
        $(".csvErr").remove();
        $(".csvSuccess").remove();
        if(dataResponseCsv.flg === 1) {
            this.setState({...dataResponseCsv,attached_file_csv_path:''}, () => {
                if (dataResponseCsv.error === 0) {
                    $('#add-csv-modal').modal('hide');
                }
                $("input[name='attached_file_csv_path']").val('');
                $("input[name='browser_attached_file_csv_path']").val('');
            });
        }
        if(dataResponseCsv.flg === 0 && dataResponseCsv.flgCsv === 0) {
            this.setState({...dataResponseCsv,attached_file_csv_path:''}, () => {
                if (dataResponseCsv.error === 0) {
                    $('#add-csv-modal').modal('hide');
                }
                $("input[name='attached_file_csv_path']").val('');
                $("input[name='browser_attached_file_csv_path']").val('');
            });
        }
    }
    render() {
        let dataResponseCsv = this.props.dataResponseCsv ? this.props.dataResponseCsv : [];
        let token = $("meta[name='csrf-token']").attr("content");
        let mallOptions = {'':'------',
                        'amazon':'アマゾン受注データ',
                        'diy':'本店受注データ',
                        'biz':'BIZ受注データ'};
        return (
            <div className="modal fade" id="add-csv-modal">

                <div className="modal-dialog modal-custom">
                    <div className="modal-content modal-custom-content">
                        <div className="modal-header">
                        <b>{trans('messages.upload_csv')}</b>
                        </div>
                        <div className="modal-body modal-custom-body">
                            <FORM className="form-horizontal"  encType={"multipart/form-data; boundary=----WebKitFormBoundary" + token}>
                                <SELECT
                                    name="mall"
                                    labelTitle={trans('messages.csv_mall')}
                                    className="form-control input-sm"
                                    fieldGroupClass="col-md-9"
                                    labelClass="col-md-3"
                                    groupClass=""
                                    errorPopup={true}
                                    onChange={this.handleChange}
                                    value={this.state.mall||''}
                                    options={mallOptions}
                                    hasError={(dataResponseCsv && _.has(dataResponseCsv, 'message.mall'))?true:false}
                                    errorMessage={(dataResponseCsv && dataResponseCsv.message)?dataResponseCsv.message.mall:""}/>
                                <UPLOAD
                                    name="attached_file_csv_path"
                                    labelTitle={trans('messages.btn_browser')}
                                    className="form-control input-sm"
                                    fieldGroupClass="col-md-9"
                                    labelClass="col-md-3"
                                    groupClass="form-group"
                                    itemGroupClass="input-group input-group-sm"
                                    btnClass="btn btn-info btn-flat btn-largest"
                                    btnTitle={trans('messages.btn_browser')}
                                    onChange={this.handleChooseFile}
                                    value={this.state.attached_file_csv_path || ''}
                                    hasError={(dataResponseCsv &&  _.has(dataResponseCsv, 'message.attached_file_csv_path'))?true:false}
                                    errorMessage={(dataResponseCsv && dataResponseCsv.message)?dataResponseCsv.message.attached_file_csv_path:""}
                                    />
                            </FORM>
                            <div className="inform-message">
                                <div className="content-message">
                                    {
                                         (this.state.flg === 0
                                            && this.state.flgCsv === 0
                                            && !_.isEmpty(this.state.message) )
                                        ?
                                           <div className="text-danger" style={
                                                {
                                                overflow: 'auto',
                                                height : '100px',
                                                width : '100%'
                                                }
                                            }>
                                                <b>
                                                    <div dangerouslySetInnerHTML={{__html:this.state.message || ''}} />
                                                </b>
                                           </div>
                                        :
                                            <div className="text-success">
                                                <b>
                                                    <div dangerouslySetInnerHTML={{__html:this.state.message || ''}} />
                                                </b>
                                           </div>
                                    }
                                </div>
                            </div>
                        </div>

                        <div className="modal-footer modal-custom-footer">
                            <button type="button" className="btn btn-primary btn-sm modal-custom-button" onClick={(e)=>this.handleImportSubmit(e)}>OK</button>
                            <button type="button" className="btn bg-purple btn-sm modal-custom-button" onClick={this.handleCancelClick}>{trans('messages.btn_cancel_memo')}</button>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}
const mapStateToProps = (state) => {
  return {
    dataResponseCsv: state.commonReducer.dataResponseCsv
  }
}
const  mapDispatchToProps = (dispatch) => {
  return {
    postDataCSV: (url, params, headers,callback,callParam) => { dispatch(Action.postDataCSV(url, params, headers,callback,callParam)) }
  }
}
export default connect(mapStateToProps, mapDispatchToProps)(UploadCsv)
