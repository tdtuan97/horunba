import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import Loading from '../common/components/common/Loading';
export default class UploadPopup extends Component {
    constructor(props) {
        super(props);
        this.state = {}        
    }

    handleClick = () => {        
        $('#add-csv-modal').modal({backdrop: 'static', show: true});        
    }

    handleCancelClick = () => {
        $('#add-csv-modal').modal('hide');        
    }
    componentDidMount = () => {
        $('#add-csv-modal').modal('hide');
    }

    render() {
        return (
            <a onClick={this.handleClick}>{trans('messages.upload_csv')}</a>            
        )
    }
}