import React from 'react';
import { render } from 'react-dom';
import { showFunction } from './jsCustom.js';
import {
  BrowserRouter as Router,
  Route,
  Link,
  Switch,
  Redirect
} from 'react-router-dom'
import createBrowserHistory from 'history/createBrowserHistory';
import ListMailTemplate from './list-mail-template/ListMailTemplate';
import SaveMailTemplate from './list-mail-template/SaveMailTemplate';
import DetailMailTemplate from './list-mail-template/DetailMailTemplate';
import ChangePassword from './change-password/ChangePassword';
import ListSendMailTiming from './send-mail-timing/ListSendMailTiming';
import SaveSendMailTiming from './send-mail-timing/SaveSendMailTiming';
import SaveSettingOrderStatus from './settings/SaveSettingOrderStatus';
import ListSendMail from './send-mail/ListSendMail';
import DetailSendMail from './send-mail/DetailSendMail';
import ListOrderManagement from './order-management/ListOrderManagement';
import DetailOrderManagement from './order-management/DetailOrderManagement';
import OrderNew from './order-management/OrderNew';
import OrderEditing from './order-management/OrderEditing';
import OrderDetailEditing from './order-management/OrderDetailEditing';
import ListSettlement from './settlement/ListSettlement';
import SaveSettlement from './settlement/SaveSettlement';
import ListBatchManagement from './batch/ListBatchManagement';
import ListPaymentManagement from './payment/ListPaymentManagement';
import ListPaymentUnknown from './payment/ListPaymentUnknown';
import CancelReasonList from './cancel-reason/CancelReasonList';
import PostalList from './postal/PostalList';
import PostalSave from './postal/PostalSave';
import CancelReasonSave from './cancel-reason/CancelReasonSave';
import UsersList from './users-management/UsersList';
import UsersSave from './users-management/UsersSave';
import ShippingFreeBelowLimitList from './shipping-free-below-limit/ShippingFreeBelowLimitList';
import ShippingFreeBelowLimitSave from './shipping-free-below-limit/ShippingFreeBelowLimitSave';
import ReturnAddressList from './return-address/ReturnAddressList';
import ReturnAddressSave from './return-address/ReturnAddressSave';
import ReturnList from './return-management/ReturnList';
import ReturnSave from './return-management/ReturnSave';
import ReturnSaveProduct from './return-management/ReturnSaveProduct';
import CancelReservationList from './cancel-reservation/CancelReservationList';
import CancelReservationSave from './cancel-reservation/CancelReservationSave';
import ReturnSaveDetail from './return-management/ReturnSaveDetail';
import RePaymentSave from './repayment/RePaymentSave';
import RePaymentList from './repayment/RePaymentList';
import NotPaymentList from './not-payment-list/NotPaymentList';
import RefundDetailSave from './refund-detail/RefundDetailSave';
import ReceiptList from './receipt-management/ReceiptList';
import ReceiveMailList from './receive-mail/ReceiveMailList';
import ReceiveMailSave from './receive-mail/ReceiveMailSave';
import OrderToSupplierList from './order-to-supplier/OrderToSupplierList';
import OrderToSupplierSave from './order-to-supplier/OrderToSupplierSave';
import EditSendMailSave from './edit-send-mail/EditSendMailSave';
import WebMappingList from './web-mapping/WebMappingList';
import SettlementMappingList from './settlement-mapping/SettlementMappingList';
import OrderToSupplierMonthList from './ordertosup-month/OrderToSupplierMonthList';
import OrderToSupplierMonthSave from './ordertosup-month/OrderToSupplierMonthSave';
import StoreOrderList from './store-order/StoreOrderList';
import DeliveryList from './delivery/DeliveryList';
import DeliverySave from './delivery/DeliverySave';
import StoreProductList from './store-product/StoreProductList';
import StoreProductAdd from './store-product/StoreProductAdd';
import UploadCsv from './upload-csv/UploadCsv';
import UploadPopup from './upload-csv/UploadPopup';
import StockConfirmList from './stock-confirm/StockConfirmList';
import MemoCheckList from './memo-check/MemoCheckList';
import StockStatusList from './stock-status/StockStatusList';
import DejeList from './deje/DejeList';
import DejeDetail from './deje/DejeDetail';
import GenreList from './genre/GenreList';
import PermissionList from './permission/PermissionList';
import ControllerList from './controller/ControllerList';
import DejeSave from './deje/DejeSave';
import CustomerList from './customer/CustomerList'
import CustomerSave from './customer/CustomerSave'
import { Provider } from 'react-redux';
import configureStore from './configureStore';
import './common/css/main.css';
import './interceptor.js';
const store = configureStore();
const history = createBrowserHistory();
//Init language
window.trans = (string) => _.get(window.i18n, string);
//console.log(trans('messages.per_page'));

//Init status
window._.status = (string) => _.get(window._status, string);

render(
    <Provider store={store}>
        <Router>
            <Switch>
                <Route exact path="/customer/" component={CustomerList}/>
                <Route exact path="/customer/save" component={CustomerSave}/>
                <Route exact path="/settings/genre" component={GenreList}/>
                <Route exact path="/settings/mail-template" component={ListMailTemplate}/>
                <Route exact path="/settings/mail-template/save" component={SaveMailTemplate}/>
                <Route exact path="/settings/mail-template/detail" component={DetailMailTemplate}/>
                <Route exact path="/change-password" component={ChangePassword}/>
                <Route exact path="/send-mail" component={ListSendMail}/>
                <Route exact path="/send-mail/detail" component={DetailSendMail}/>
                <Route exact path="/settings/mail-timing" component={ListSendMailTiming}/>
                <Route exact path="/settings/mail-timing/save" component={SaveSendMailTiming}/>
                <Route exact path="/settings/order-status" component={SaveSettingOrderStatus}/>
                <Route exact path="/order-management" component={ListOrderManagement}/>
                <Route exact path="/order-management/detail" component={DetailOrderManagement}/>
                <Route exact path="/order-management/order-editing" component={OrderEditing}/>
                <Route exact path="/order-management/order-detail-editing" component={OrderDetailEditing}/>
                <Route exact path="/order-management/order-new" component={OrderNew}/>
                <Route exact path="/settings/settlement" component={ListSettlement}/>
                <Route exact path="/settings/settlement/save" component={SaveSettlement}/>
                <Route exact path="/batch-management" component={ListBatchManagement}/>
                <Route exact path="/payment/info-list" component={ListPaymentManagement}/>
                <Route exact path="/payment/unknown-list" component={ListPaymentUnknown}/>
                <Route exact path="/settings/cancel-reason-list" component={CancelReasonList}/>
                <Route exact path="/settings/cancel-reason-list/save" component={CancelReasonSave}/>
                <Route exact path="/settings/users-management" component={UsersList}/>
                <Route exact path="/settings/postal-list" component={PostalList}/>
                <Route exact path="/settings/postal-list/save" component={PostalSave}/>
                <Route exact path="/settings/users-management/save" component={UsersSave}/>
                <Route exact path="/settings/shipping-free-below-limit" component={ShippingFreeBelowLimitList}/>
                <Route exact path="/settings/shipping-free-below-limit/save" component={ShippingFreeBelowLimitSave}/>
                <Route exact path="/return-management" component={ReturnList}/>
                <Route exact path="/return-management/save" component={ReturnSave}/>
                <Route exact path="/return-management/save-product" component={ReturnSaveProduct}/>
                <Route exact path="/cancel-reservation" component={CancelReservationList}/>
                <Route exact path="/cancel-reservation/save" component={CancelReservationSave}/>
                <Route exact path="/return-management/save-detail" component={ReturnSaveDetail}/>
                <Route exact path="/return-address" component={ReturnAddressList}/>
                <Route exact path="/return-address/save" component={ReturnAddressSave}/>
                <Route exact path="/not-payment-list" component={NotPaymentList}/>
                <Route exact path="/repayment" component={RePaymentList}/>
                <Route exact path="/repayment/save" component={RePaymentSave}/>
                <Route exact path="/refund-detail/save" component={RefundDetailSave}/>
                <Route exact path="/receipt-management" component={ReceiptList}/>
                <Route exact path="/receive-mail" component={ReceiveMailList}/>
                <Route exact path="/receive-mail/save" component={ReceiveMailSave}/>
                <Route exact path="/order-to-supplier" component={OrderToSupplierList}/>
                <Route exact path="/order-to-supplier/save" component={OrderToSupplierSave}/>
                <Route exact path="/edit-send-mail/save" component={EditSendMailSave}/>
                <Route exact path="/web-mapping" component={WebMappingList}/>
                <Route exact path="/settlement-mapping" component={SettlementMappingList}/>
                <Route exact path="/order-to-supplier-monthly" component={OrderToSupplierMonthList}/>
                <Route exact path="/order-to-supplier-monthly/save" component={OrderToSupplierMonthSave}/>
                <Route exact path="/store-order" component={StoreOrderList}/>
                <Route exact path="/delivery" component={DeliveryList}/>
                <Route exact path="/delivery/save" component={DeliverySave}/>
                <Route exact path="/store-product" component={StoreProductList}/>
                <Route exact path="/upload-csv/save" component={UploadCsv}/>
                <Route exact path="/stock-confirm" component={StockConfirmList}/>
                <Route exact path="/memo-check" component={MemoCheckList}/>
                <Route exact path="/stock-status" component={StockStatusList}/>
                <Route exact path="/deje/save" component={DejeSave}/>
                <Route exact path="/deje/detail" component={DejeDetail}/>
                <Route exact path="/deje" component={DejeList}/>
                <Route exact path="/settings/permission" component={PermissionList}/>
                <Route exact path="/settings/controller" component={ControllerList}/>
                <Route exact path="/store-product/add" component={StoreProductAdd}/>
            </Switch>
        </Router>
    </Provider>,
    document.getElementById('root'),
    showFunction
    );
render(
    <Provider store={store}>
        <UploadPopup />
    </Provider>,
    document.getElementById('uploadCsv')
 );
render(
    <Provider store={store}>
        <UploadCsv />
    </Provider>,
    document.getElementById('showPopup')
 );



