import React, { Component } from 'react';

import { connect } from 'react-redux';
import * as Action from '../common/actions';
import * as queryString from 'query-string';

import BUTTON from '../common/components/form/BUTTON';
import FORM from '../common/components/form/FORM';
import INPUT from '../common/components/form/INPUT';
import DATEPICKER from '../common/components/form/DATEPICKER';

import TABLE from '../common/components/table/TABLE';
import THEAD from '../common/components/table/THEAD';
import TBODY from '../common/components/table/TBODY';
import TR from '../common/components/table/TR';
import TH from '../common/components/table/TH';
import TD from '../common/components/table/TD';
import SELECT from '../common/components/form/SELECT';
import LABEL from '../common/components/form/LABEL';
import moment from 'moment';

import SortComponent from '../common/components/table/SortComponent';
import PaginationContainer from '../common/containers/PaginationContainer'
import Loading from '../common/components/common/Loading';

class ReturnAddressList extends Component {

    constructor(props) {
        super(props);
        this.curStrQuery = '';
        const stringParams = queryString.parse(props.location.search);
        this.state = {};
        if (!_.isEmpty(stringParams)) {
            this.state = stringParams;
        }
        this.props.reloadData(dataListUrl, this.state);
    }

    handleChange = (event) => {
        const target = event.target;
        const value = target.type === 'checkbox' ? target.checked : target.value;
        const name = target.name;
        this.setState({
            [name]: (value)?value:''
        },() => {
            if (name === 'per_page') {
                this.handleReloadData();
            }
        });
    }

    handleClearClick = (event) => {
        Object.keys(this.state).map((key) => {
            if (key !== 'per_page') {
                delete this.state[key];
            }
        });
        this.handleReloadData();
    }

    handleReloadData = () => {
        if (this.state.page) {
            delete this.state.page;
        }
        this.props.reloadData(dataListUrl, this.state, true);
    }

    componentDidUpdate() {
        if (!_.isEmpty(this.props.dataSource)) {
            $('.loading').hide();
        }
        var heightTable = $('table.table-custom').height();
        $("table th").addClass('resize-thead');
        $("table th").resizeble();
        $('.table-responsive').on('scroll', function (e) {
            $('div.scroll-bar-top').scrollLeft($('.table-responsive').scrollLeft());
        });
        $('.table-custom').on('scroll', function (e) {
            $('.table-custom').scrollLeft();
        });
        $('div.scroll-bar-top').on('scroll', function (e) {
            $('.table-responsive').scrollLeft($('div.scroll-bar-top').scrollLeft());
        });
        $(document).ready(function() {
            $('div.scroll-bar-top>div').width($('table.table-custom').width());
        });
        dragscroll.reset();
    }

    componentWillMount() {
        window.onpopstate = (event) => {
            location.reload();
        };
    }

    handleSortClick = (name, nextSort) => {
        Object.keys(this.state).map((key) => {
            if (key.startsWith('sort_')) {
                delete this.state[key];
            }
        });
        if (nextSort === 'none') {
            delete this.state[name];
            this.props.reloadData(dataListUrl, this.state, true);
        } else {
            this.setState({[name]: nextSort}, () => {
                this.props.reloadData(dataListUrl, this.state, true);
            });
        }

    }

    handleClickPage = (page) => {
        this.setState({
            page: page
        },() => {
            this.props.reloadData(dataListUrl, this.state, true);
        });
    }
    componentWillReceiveProps(nextProps) {
        let param = {};
        Object.keys(this.state).map((item) => {
            if (typeof this.state[item] !== 'undefined') {
                if (typeof this.state[item] === 'array' || typeof this.state[item] === 'object') {
                    param[item] = this.state[item];
                } else {
                    param[item] = _.trim(this.state[item]);
                }
            }
        });
        this.curStrQuery = queryString.stringify(param);
    }    
    render() {
        let dataSource = this.props.dataSource;
        let index = 0;
        let totalItems = 0;
        let itemsPerPage = 20;
        if (!_.isEmpty(dataSource)) {
            index = dataSource.data.from - 1;
            totalItems   = dataSource.data.total;
            itemsPerPage = dataSource.data.per_page;
        }

        return ( (!_.isEmpty(dataSource)) &&
            <div>
                <Loading className="loading" />
                <div className="box">
                    <div className="box-header">
                        <div className="row">
                                <div className="col-md-12">
                                    <h4>{trans('messages.search_title')}</h4>
                                </div>
                                <FORM>
                                    <INPUT
                                        name="supplier_nm"
                                        groupClass="form-group col-md-1-5"
                                        labelTitle={trans('messages.supplier_name')}
                                        onChange={this.handleChange}
                                        value={this.state.supplier_nm||''}
                                        className="form-control input-sm" />
                                    <INPUT
                                        name="return_postal"
                                        groupClass="form-group col-md-1-5"
                                        labelTitle={trans('messages.return_postal')}
                                        onChange={this.handleChange}
                                        value={this.state.return_postal||''}
                                        className="form-control input-sm" />
                                    <INPUT
                                        name="return_pref"
                                        groupClass="form-group col-md-1-5"
                                        labelTitle={trans('messages.return_pref')}
                                        onChange={this.handleChange}
                                        value={this.state.return_pref||''}
                                        className="form-control input-sm" />
                                    <INPUT
                                        name="return_address"
                                        groupClass="form-group col-md-1-5"
                                        labelTitle={trans('messages.return_address')}
                                        onChange={this.handleChange}
                                        value={this.state.return_address||''}
                                        className="form-control input-sm" />
                                    <INPUT
                                        name="return_tel"
                                        groupClass="form-group col-md-1-5"
                                        labelTitle={trans('messages.return_tel')}
                                        onChange={this.handleChange}
                                        value={this.state.return_tel||''}
                                        className="form-control input-sm" />
                                    
                                    <SELECT
                                        name="per_page"
                                        groupClass="form-group col-md-1 per-page pull-right-md"
                                        labelClass="pull-left"
                                        labelTitle={trans('messages.per_page')}
                                        onChange={this.handleChange}
                                        value={this.state.per_page||''}
                                        options={{20:20, 40:40, 60:60, 80:80, 100:100}}
                                        className="form-control input-sm" />
                                </FORM>
                        </div>
                        <div className="row">
                            <div className="col-md-9 col-xs-4">
                                <a href={saveUrl} className="btn btn-success input-sm">{trans('messages.btn_add')}</a>
                            </div>
                            <div className="col-md-3 col-xs-8">
                                    <BUTTON className="btn btn-danger pull-right" onClick={this.handleClearClick}>{trans('messages.btn_reset')}</BUTTON>
                                    <BUTTON className="btn btn-primary pull-right margin-element" onClick={this.handleReloadData} >{trans('messages.btn_search')}</BUTTON>
                            </div>
                        </div>
                    </div>
                    <div className="box-body">
                        <div className="row">
                            <div className="col-md-12">
                                {(totalItems/itemsPerPage > 1) &&
                                <div className="row">
                                    <div className="col-md-2-5"></div>
                                    <div className="col-md-7 text-center">
                                        <PaginationContainer handleClickPage={this.handleClickPage}/>
                                    </div>
                                    <div className="col-md-2-5 text-right line-height-md">
                                        {dataSource.data.from}件 - {dataSource.data.to}件 【全{dataSource.data.total}件】
                                    </div>
                                </div>
                                }
                                <div className="table-responsive dragscroll">
                                <TABLE className="table table-striped table-custom">
                                    <THEAD>
                                        <TR>
                                            <TH className="col-max-min-60 text-center text-middle">
                                                {trans('messages.index')}
                                            </TH>
                                            <TH className="col-max-min-100 text-center text-middle">
                                                <SortComponent
                                                    name="sort_supplier_nm"
                                                    onClick={this.handleSortClick}
                                                    currentSort={(this.state.sort_supplier_nm)?this.state.sort_supplier_nm:'none'}
                                                    title={trans('messages.supplier_name')} />
                                            </TH>
                                            <TH className="col-max-min-100 text-center text-middle">
                                                <SortComponent
                                                    name="sort_return_nm"
                                                    onClick={this.handleSortClick}
                                                    currentSort={(this.state.sort_return_nm)?this.state.sort_return_nm:'none'}
                                                    title={trans('messages.return_nm')} />
                                            </TH>
                                            <TH className=" col-max-min-120 text-center text-middle">
                                                <SortComponent
                                                    name="sort_return_postal"
                                                    onClick={this.handleSortClick}
                                                    currentSort={(this.state.sort_return_postal)?this.state.sort_return_postal:'none'}
                                                    title={trans('messages.return_postal')} />
                                            </TH>
                                            <TH className="col-max-min-100 text-center text-middle">
                                                <SortComponent
                                                    name="sort_return_pref"
                                                    onClick={this.handleSortClick}
                                                    currentSort={(this.state.sort_return_pref)?this.state.sort_return_pref:'none'}
                                                    title={trans('messages.return_pref')} />
                                            </TH>
                                            <TH className="col-max-min-60 text-center" >
                                                <SortComponent
                                                    name="sort_return_address"
                                                    onClick={this.handleSortClick}
                                                    currentSort={(this.state.sort_return_address)?this.state.sort_return_address:'none'}
                                                    title={trans('messages.return_address')} />
                                            </TH>
                                            <TH className="col-max-min-100 text-center text-middle">
                                                <SortComponent
                                                    name="sort_return_tel"
                                                    onClick={this.handleSortClick}
                                                    currentSort={(this.state.sort_return_tel)?this.state.sort_return_tel:'none'}
                                                    title={trans('messages.return_tel')} />
                                            </TH>                                            
                                        </TR>                                        
                                    </THEAD>
                                    <TBODY>
                                    {
                                        (!_.isEmpty(dataSource.data.data))?
                                        dataSource.data.data.map((item) => {
                                            index++;
                                            let classRow = (index%2 ===0) ? 'odd' : 'even';
                                            return (
                                                [
                                                <TR key={"tr" + index} className={classRow}>
                                                    <TD className="hidden-xs hidden-sm text-center">
                                                        <a
                                                            href={saveUrl + "?" + (this.curStrQuery?(this.curStrQuery+"&"):"") + "supplier_cd=" + item.supplier_cd}
                                                            className="link-detail"
                                                        >{index}
                                                        </a>                                                       
                                                    </TD>
                                                    <TD className="text-left" title={item.supplier_nm}>
                                                            {item.supplier_nm}
                                                    </TD>
                                                    <TD className="text-left" title={item.return_nm}>
                                                            {item.return_nm}
                                                    </TD>
                                                    <TD className="text-right" title={item.return_postal}>
                                                            {item.return_postal}
                                                    </TD>
                                                    <TD className="text-left" title={item.return_pref}>
                                                            {item.return_pref}
                                                    </TD>
                                                    <TD className="text-left" title={item.return_address}>
                                                            {item.return_address}
                                                    </TD>
                                                    <TD className="text-right" title={item.return_tel}>
                                                            {item.return_tel}
                                                    </TD>                                             
                                                </TR>
                                               ]
                                            )
                                        })
                                        :
                                        <TR><TD colSpan="7" className="text-center">{trans('messages.no_data')}</TD></TR>
                                    }
                                    </TBODY>
                                </TABLE>
                                </div>
                                <div className="text-center">
                                    <PaginationContainer handleClickPage={this.handleClickPage}/>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        dataSource: state.commonReducer.dataSource,
        dataResponse: state.commonReducer.dataResponse
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        reloadData: (url, params, pushHistory, ignoreFields) => { dispatch(Action.reloadData(url, params, pushHistory, ignoreFields)) },
        postData: (url, params, headers, callback) => { dispatch(Action.postData(url, params, headers, callback)) }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(ReturnAddressList)