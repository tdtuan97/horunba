import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Loading from '../../common/components/common/Loading';
import axios from 'axios';
import UPLOAD from '../../common/components/form/UPLOAD';
import { connect } from 'react-redux';
import * as Action from '../../common/actions';
import BUTTON from '../../common/components/form/BUTTON';
import FORM from '../../common/components/form/FORM';
import INPUT from '../../common/components/form/INPUT';
import TABLE from '../../common/components/table/TABLE';
import THEAD from '../../common/components/table/THEAD';
import TBODY from '../../common/components/table/TBODY';
import TR from '../../common/components/table/TR';
import TH from '../../common/components/table/TH';
import TD from '../../common/components/table/TD';
import SELECT from '../../common/components/form/SELECT';
import LABEL from '../../common/components/form/LABEL';
import CHECKBOX_CUSTOM from '../../common/components/form/CHECKBOX_CUSTOM';
import moment from 'moment';

class FormAddress extends Component {
    constructor(props) {
        super(props);
    }
    render() {
        let token = $("meta[name='csrf-token']").attr("content");
        let dataSource = this.props.dataSource;
        let index = 0;
        return (
            <div className="modal fade" id="issue-modal">
                <Loading className="loading" />
                <input type="hidden" id="action-modal" />
                <div className="modal-dialog">
                    <div className={"modal-content modal-custom-content " + (this.props.process ? 'hidden-div-csv' : '')}>
                        <div className={"modal-body modal-custom-body " + (this.props.error === 1 ? 'hidden' : '')}>
                            <p>返品住所編集</p>
                            <form className="form-horizontal"
                            encType={"multipart/form-data; boundary=----WebKitFormBoundary" + token}>
                            <TABLE className="table table-striped table-custom">
                                    <THEAD>
                                        <TR>
                                            <TH className="visible-xs visible-sm"></TH>
                                            <TH className="hidden-xs hidden-sm text-center">
                                                #
                                            </TH>
                                            <TH className="hidden-xs hidden-sm text-center ">{trans('messages.return_nm')}</TH>
                                            <TH className="hidden-xs hidden-sm text-center ">{trans('messages.return_postal')}</TH>
                                            <TH className="hidden-xs hidden-sm text-center ">{trans('messages.return_address')}</TH>
                                            <TH className="hidden-xs hidden-sm text-center ">{trans('messages.return_pref')}</TH>
                                            <TH className="hidden-xs hidden-sm text-center ">{trans('messages.return_tel')}</TH>
                                            <TH className="hidden-xs hidden-sm text-center "></TH>
                                        </TR>
                                    </THEAD>
                                    <TBODY>
                                        {
                                            (!_.isEmpty(dataSource.dataReturnAddress))?
                                            dataSource.dataReturnAddress.map((item) => {
                                                index++;
                                                let classRow = (index%2 ===0) ? 'odd' : 'even';
                                                let nf = new Intl.NumberFormat();
                                                return (
                                                    [
                                                    <TR key={"tr" + item.index} className={classRow}>
                                                        <TD className="visible-xs visible-sm text-center">
                                                            <b className="show-info">
                                                                <i className="fa fa-angle-double-down" aria-hidden="true"></i>
                                                            </b>
                                                        </TD>
                                                        <TD className="hidden-xs hidden-sm text-center">
                                                           {index}
                                                        </TD>
                                                        <TD className="hidden-xs hidden-sm text-center">
                                                           {item.return_nm}
                                                        </TD>
                                                        <TD className="cut-text text-left" title={item.return_postal}>{item.return_postal}</TD>
                                                        <TD className="text-left text-long-batch max-w-200" title={item.return_address}>{item.return_address}</TD>
                                                        <TD className="hidden-xs hidden-sm cut-text text-right" title={item.return_pref}>{item.return_pref}</TD>
                                                        <TD className="hidden-xs hidden-sm cut-text text-left text-long-batch max-w-200" title={item.return_tel}>{item.return_tel}</TD>
                                                        <TD className="hidden-xs hidden-sm text-center">
                                                            <CHECKBOX_CUSTOM
                                                                defaultChecked={item.is_selected ? true : false}
                                                                onChange={(e) => this.handleChecked(item.index, item.is_selected, 'is_selected', e)}
                                                             />
                                                        </TD>

                                                    </TR>,
                                                    <TR key={"trhide" + index} className="hiden-info" style={{display:"none"}}>
                                                        <TD colSpan="10" className="width-span1">
                                                            <ul id={"temp" + item.mail_id} >
                                                                <li> <b>{trans('messages.product_code')}</b> : {item.product_code}</li>
                                                                <li> <b>{trans('messages.product_name')}</b> : {item.product_name}</li>
                                                                <li> <b>{trans('messages.search_supplier_code')}</b> : {item.supplier_id}</li>
                                                                <li> <b>{trans('messages.supplier_name')}</b> : {item.supplier_nm}</li>
                                                                <li> <b>{trans('messages.order_quantity')}</b> : {item.order_num}</li>
                                                                <li> <b>{trans('messages.search_price_invoice')}</b> : {nf.format(item.price_invoice)}</li>
                                                                <li> <b>{trans('messages.order_price')}</b> : {nf.format(item.order_price)}</li>
                                                                <li> <b>{trans('messages.note')}</b> : {item.note}</li>
                                                                <li> <b>{trans('messages.search_is_ordered')}</b>:
                                                                    <div className="monthly-checkbox">
                                                                        <CHECKBOX_CUSTOM
                                                                            defaultChecked={item.is_ordered ? true : false}
                                                                        />
                                                                    </div>
                                                                </li>
                                                                <li> <b>{trans('messages.is_cancel')}</b> :
                                                                    <div className="monthly-checkbox">
                                                                        <CHECKBOX_CUSTOM
                                                                            defaultChecked={item.is_cancel ? true : false}
                                                                        />
                                                                    </div>
                                                                </li>
                                                                <li> <b>{trans('messages.error_code')}</b> : {item.error_code}</li>
                                                                <li> <b>{trans('messages.error_message')}</b> : {item.error_message}</li>
                                                            </ul>
                                                        </TD>
                                                        </TR>
                                                   ]

                                                )
                                                if (index === dataSource.data.to) {
                                                   index = 0;
                                                }
                                            })
                                            :
                                            <TR><TD colSpan="6" className="text-center">{trans('messages.no_data')}</TD></TR>
                                        }
                                    </TBODY>
                                </TABLE>

                            </form>
                        </div>
                        <div className="modal-footer modal-custom-footer">
                            <button type="button" className="btn btn-success" >{trans('messages.btn_add_return_address')}</button>
                            <button type="button" className="btn btn-primary modal-custom-button" disabled={this.props.error === 1 ? true : false} onClick={(e) => this.props.handleImportSubmit(e)}>{trans('messages.btn_accept')}</button>
                            <button type="button" className="btn bg-purple modal-custom-button" onClick={(e) => this.props.handleImportCancel(e)}>{trans('messages.btn_cancel')}</button>
                        </div>
                    </div>
                    <div className={"progress collapse " + (this.props.status ? this.props.status : '')}>
                        <div className="progress-bar progress-bar-striped active" role="progressbar"
                          aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style={{width : this.props.percent + '%'}}>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}
const mapStateToProps = (state) => {
    return {
        dataSource: state.commonReducer.dataSource,
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        postData: (url, params, headers) => { dispatch(Action.postData(url, params, headers)) }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(FormAddress)
