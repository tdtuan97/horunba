import React, { Component } from 'react';
import { connect } from 'react-redux';

import * as Action from '../common/actions';
import * as queryString from 'query-string';
import moment from 'moment';

import Loading from '../common/components/common/Loading';
import FORM from '../common/components/form/FORM';
import INPUT from '../common/components/form/INPUT';
import TEXTAREA from '../common/components/form/TEXTAREA';
import SELECT from '../common/components/form/SELECT';
import BUTTON from '../common/components/form/BUTTON';
import CHECKBOX_CUSTOM from '../common/components/form/CHECKBOX_CUSTOM';
import LABEL from '../common/components/form/LABEL';
import DATEPICKER from '../common/components/form/DATEPICKER';
import MessagePopup from '../common/components/common/MessagePopup';
import MESSAGE_MODAL from '../common/containers/MessageModal';
import TABLE from '../common/components/table/TABLE';
import THEAD from '../common/components/table/THEAD';
import TBODY from '../common/components/table/TBODY';
import TR from '../common/components/table/TR';
import TH from '../common/components/table/TH';
import TD from '../common/components/table/TD';
import FormAdress from './form-address/FormAdress';
import axios from 'axios';

var timer;
class ReturnAddressSave extends Component {
    constructor(props) {
        super(props);
        this.state = {return_id : '', return_postal : '', return_pref : '', return_address : '', return_tel : '',save : false};
        let params = queryString.parse(props.location.search);
        if (params['supplier_cd']) {
            this.state.supplier_cd          = params['supplier_cd'];
            this.state.save                 = true;
            this.state.index = params['supplier_cd'];
        }
        this.curStrQuery = params;
        this.props.reloadData(getFormDataUrl, this.state);
    }
    handleCancel = (event) => {
        window.location.href = baseUrl;
    }
    hanleRealtime() {
        if (!_.isEmpty(this.state.index)) {
            let params = {
                accessFlg : this.state.accessFlg,
                query : {
                        page_key : 'RETURN-ADRESS-SAVE',
                        primary_key : 'return_id:' + this.state.index
                    },
                key :this.state.index
            };
            if (this.state.accessFlg !== false) {
                this.props.postRealTime(checkEditUrl, params, {'X-Requested-With': 'XMLHttpRequest'});
            }
        }
    }
    handleSubmit = (event) => {
        let headers = {'X-Requested-With': 'XMLHttpRequest'};
        let params  = {...this.state};
        delete params.dataReturnAddressList;
        this.props.postData(saveUrl, params, headers,  this.props.reloadData , [getFormDataUrl, params, true]);
    }
    handleChange = (event) => {
        const target = event.target;
        const name = target.name;
        const value = target.type === 'checkbox' ? target.checked : target.value;
        this.setState({
           [name]: (value)?value:undefined
        });
    }

    
    handleCancel = (event) => {       
        let param = {};
        Object.keys(this.curStrQuery).map((item) => {
            if (typeof this.curStrQuery[item] !== 'undefined') {
                if (typeof this.curStrQuery[item] === 'array' || typeof this.curStrQuery[item] === 'object') {
                    param[item] = this.curStrQuery[item];
                } else {
                    param[item] = _.trim(this.curStrQuery[item]);
                }
            }
        });
        delete param['supplier_cd'];
        this.curStrQuery = queryString.stringify(param);
        window.location.href = baseUrl + "?" + this.curStrQuery;
    }
    componentWillUnmount () {
        clearInterval(this.state);
    }
    handleCreate = (event) => {
        let params = { return_postal : '', return_pref : '', return_address : '', return_tel : '', save : false};        
        this.setState({...params});
    }   
    componentWillReceiveProps(nextProps) {
        let dataSource = nextProps.dataSource;       
        if (!_.isEmpty(dataSource.dataReturnAddress)) {
            if (this.state.save === true) {
                if (this.props.dataSource.time !== nextProps.dataSource.time) {
                    this.setState({...dataSource.dataReturnAddress, dataReturnAddressList: dataSource.dataReturnAddressList}, () => {
                        $('table tr').removeClass('bg-yellow-active');
                        $('table tbody tr:first').addClass('bg-yellow-active');
                    });
                }
            } else {
                if (this.props.dataSource.time !== nextProps.dataSource.time) {                    
                    this.setState({...dataSource.dataReturnAddress, dataReturnAddressList: dataSource.dataReturnAddressList} , () => {
                        $('table tr').removeClass('bg-yellow-active');
                        $('table tbody tr:first').addClass('bg-yellow-active');                        
                    });
                }
            }
        } 
        // Check realtime update
        if (!_.isEmpty(nextProps.dataRealTime)) {
            this.setState(nextProps.dataRealTime);
        }
    }

    componentDidUpdate() {
        if (!_.isEmpty(this.props.dataSource)) {
            $('.loading').hide();
        }
    }
    componentDidMount() {
        setInterval(() => {
            this.hanleRealtime()
        }, 5000);
    }
    
    handleChangeSupplierDetail = (event) => {
        const target = event.target;
        const name = target.name;
        const value = target.value;
        this.setState({
            [name]: value
        }, () => {
            if (name === 'supplier_cd') {
                clearTimeout(timer);
                let _this = this;
                timer = setTimeout(function() {
                    _this.getInfoSupplier(value);
                }, 1500);
            }
        });
    }
    handleEdit = (event, item) => {
        const target = event.target;
        this.setState({save:true,...item}, () => { 
            $('table tr').removeClass('bg-yellow-active');
            $('#row-'+item.return_id).addClass('bg-yellow-active');

        });
        $("html, body").animate({ scrollTop: 0 }, "slow");
    }
    
    getInfoSupplier = (supplierCd) => {
        $(".loading").show();
        let headers = {'X-Requested-With': 'XMLHttpRequest'};
            axios.post(getDataSupplierUrl, {'supplier_cd' : supplierCd}, {headers : headers}
        ).then(response => {
            $(".loading").hide();
            if (typeof response.data.dataInfo !== 'undefined') {
                if (!_.isEmpty(response.data.dataInfo)) {
                    this.setState({message: '', ...response.data.dataInfo, dataReturnAddressList : response.data.dataReturnAddressList});
                } else if (!_.isEmpty(response.data.message)) {
                    this.setState({message : response.data.message, supplier_nm : '', return_nm : ''});
                }
            } 
        }).catch(error => {
            $(".loading").hide();
            this.getInfoSupplier(supplierCd);
            throw(error);
        });
    }
    render() {
        let dataSource   = this.props.dataSource;
        let dataResponse = {};
        if (!_.isEmpty(this.state.message)) {
            dataResponse.message = this.state.message;
        } else {
            dataResponse = this.props.dataResponse;
        }
        var nf = new Intl.NumberFormat();
        return (!_.isEmpty(dataSource) &&
            <div className="box box-primary">
            <div className="box-body">
            <div className="container-fluid">
            <FORM
                className="form-horizontal" id="return_address">
                <Loading className="loading" />
                    <div className="row">
                        <div className="col-md-6 margin-form">
                            <INPUT
                                name="supplier_cd"
                                labelTitle={trans('messages.supplier_cd')}
                                groupClass="row"
                                className="form-control input-sm"
                                fieldGroupClass="col-md-9"
                                labelClass="col-md-3"
                                readOnly={this.state.save === true ? true : false}
                                value={this.state.supplier_cd || ''}
                                errorPopup={true}
                                onChange={(e) => this.handleChangeSupplierDetail(e)}
                                hasError={(dataResponse.message && dataResponse.message.supplier_cd)?true:false}
                                errorMessage={(dataResponse.message&&dataResponse.message.supplier_cd)?dataResponse.message.supplier_cd:""}                                
                               />
                        </div>
                        <div className="col-md-6 margin-form">
                            <INPUT
                                name="return_postal"
                                labelTitle={trans('messages.return_postal')}
                                groupClass="row"
                                className="form-control input-sm"
                                fieldGroupClass="col-md-9"
                                labelClass="col-md-3"
                                onChange={(e) => this.handleChange(e)}
                                value={this.state.return_postal || ''}
                                errorPopup={true}
                                hasError={(dataResponse.message && dataResponse.message.return_postal)?true:false}
                                errorMessage={(dataResponse.message&&dataResponse.message.return_postal)?dataResponse.message.return_postal:""}                                
                               />
                        </div>
                    </div>
                    <div className="row">
                        <div className="col-md-6 margin-form">
                            <INPUT
                                name="supplier_nm"
                                labelTitle={trans('messages.supplier_name')}
                                groupClass="row"
                                className="form-control input-sm"
                                fieldGroupClass="col-md-9"
                                labelClass="col-md-3"
                                onChange={(e) => this.handleChange(e)}
                                value={this.state.supplier_nm || ''}
                                readOnly={true}
                                errorPopup={true}
                                hasError={(dataResponse.message && dataResponse.message.supplier_nm)?true:false}
                                errorMessage={(dataResponse.message&&dataResponse.message.supplier_nm)?dataResponse.message.supplier_nm:""}                                
                               />
                        </div>
                        <div className="col-md-6 margin-form">
                            <INPUT
                                name="return_pref"
                                labelTitle={trans('messages.return_pref')}
                                groupClass="row"
                                className="form-control input-sm"
                                fieldGroupClass="col-md-9"
                                labelClass="col-md-3"
                                onChange={(e) => this.handleChange(e)}
                                value={this.state.return_pref || ''}
                                errorPopup={true}
                                hasError={(dataResponse.message && dataResponse.message.return_pref)?true:false}
                                errorMessage={(dataResponse.message&&dataResponse.message.return_pref)?dataResponse.message.return_pref:""}                                
                               />
                        </div>
                    </div>
                    <div className="row">
                        <div className="col-md-6 margin-form">
                            <INPUT
                                name="return_nm"
                                labelTitle={trans('messages.return_nm')}
                                groupClass="row"
                                className="form-control input-sm"
                                fieldGroupClass="col-md-9"
                                labelClass="col-md-3"
                                onChange={(e) => this.handleChange(e)}
                                value={this.state.return_nm || ''}
                                errorPopup={true}
                                hasError={(dataResponse.message && dataResponse.message.return_nm)?true:false}
                                errorMessage={(dataResponse.message&&dataResponse.message.return_nm)?dataResponse.message.return_nm:""}                                
                               />
                        </div>
                        <div className="col-md-6 margin-form">
                            <INPUT
                                name="return_address"
                                labelTitle={trans('messages.return_address')}
                                groupClass="row"
                                className="form-control input-sm"
                                fieldGroupClass="col-md-9"
                                labelClass="col-md-3"
                                onChange={(e) => this.handleChange(e)}
                                value={this.state.return_address || ''}
                                errorPopup={true}
                                hasError={(dataResponse.message && dataResponse.message.return_address)?true:false}
                                errorMessage={(dataResponse.message&&dataResponse.message.return_address)?dataResponse.message.return_address:""}                                
                               />
                        </div>
                    </div>
                    <div className="row">
                        <div className="col-md-6 margin-form">
                        </div>
                        <div className="col-md-6 margin-form">
                            <INPUT
                                name="return_tel"
                                labelTitle={trans('messages.return_tel')}
                                groupClass="row"
                                className="form-control input-sm"
                                fieldGroupClass="col-md-9"
                                labelClass="col-md-3"
                                onChange={(e) => this.handleChange(e)}
                                value={this.state.return_tel || ''}
                                errorPopup={true}
                                hasError={(dataResponse.message && dataResponse.message.return_tel)?true:false}
                                errorMessage={(dataResponse.message&&dataResponse.message.return_tel)?dataResponse.message.return_tel:""}                                
                               />
                        </div>
                    </div>                    
                    <div className="row">
                        <div className="col-md-12 margin-form">
                            <BUTTON className="btn bg-purple input-sm pull-right btn-larger" type="button" onClick={this.handleCancel}>{trans('messages.btn_cancel')}</BUTTON>
                            <BUTTON name="btnAccept" id="btnSubmit" className="btn btn-success input-sm btn-larger pull-right margin-element" type="button" onClick={this.handleSubmit}>{trans('messages.btn_accept')}</BUTTON>
                        </div>
                    </div>
                    <hr />
                    <div className="row">
                        <div className="col-md-12 margin-form">
                        <div className="row">
                            <div className="col-md-12 text-right line-height-md">
                                {
                                        (!_.isEmpty(this.state.dataReturnAddressList)) ?                            
                                            '【全' + this.state.dataReturnAddressList.length + '件】'
                                        : ''
                                }
                            </div>
                        </div>                        
                        <div className="table-responsive">
                            <TABLE className="table table-striped table-custom" style={{ borderCollapse: "collapse" }}>
                                <THEAD>
                                    <TR>
                                        <TH className="text-center ">{'#'}</TH>
                                        <TH className="text-center ">{trans('messages.supplier_name')}</TH>
                                        <TH className="text-center ">{trans('messages.return_nm')}</TH>
                                        <TH className="text-center ">{trans('messages.return_postal')}</TH>
                                        <TH className="text-center ">{trans('messages.return_pref')}</TH>
                                        <TH className="text-center">{trans('messages.return_address')}</TH>
                                        <TH className="text-center">{trans('messages.return_tel')}</TH>
                                    </TR>
                                </THEAD>
                                <TBODY>
                                    {
                                        (!_.isEmpty(this.state.dataReturnAddressList)) ?
                                        this.state.dataReturnAddressList.map((item, index) => {
                                            return ([
                                                <TR key={"return_address"  + item.return_id} className={index === 0 ? 'bg-yellow-active' : ''} id={'row-' + item.return_id}>
                                                    <TD className="col-max-min-20 text-center" >
                                                        <a className="link-detail" title={item.return_id} onClick={(e) => this.handleEdit(e, item)}><b>{item.return_id}</b></a>
                                                    </TD>
                                                    <TD className="col-max-min-120">{item.supplier_nm}</TD>
                                                    <TD className="col-max-min-120">{item.return_nm}</TD>
                                                    <TD className="col-max-min-120">{item.return_postal}</TD>
                                                    <TD className="col-max-min-120">{item.return_pref}</TD>
                                                    <TD className="col-max-min-120">{item.return_address}</TD>
                                                    <TD className="col-max-min-120">{item.return_tel}</TD>
                                                </TR>
                                            ])
                                        }) :
                                        <TR><TD colSpan="7" className="text-center">{trans('messages.no_data')}</TD></TR>
                                    }
                                </TBODY>
                            </TABLE>
                        </div>
                        </div>
                    </div>
                    <div className="row">
                        <div className="col-md-12 margin-form">
                            <BUTTON name="btnAccept" id="btnCreate" className="btn btn-success input-sm btn-larger pull-right margin-element" type="button" onClick={this.handleCreate}>{trans('messages.btn_create')}</BUTTON>
                        </div>
                    </div>                    
            </FORM>
            </div>
            </div>
            </div>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        dataSource: state.commonReducer.dataSource,
        dataResponse: state.commonReducer.dataResponse,
        dataRealTime: state.commonReducer.dataRealTime
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        reloadData: (url, params) => { dispatch(Action.detailData(url, params)) },
        postData: (url, params, headers, callBack, paramsCallBack) => { dispatch(Action.postData(url, params, headers,callBack, paramsCallBack)) },
        postRealTime: (url, params, headers) => { dispatch(Action.postRealTime(url, params, headers)) }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(ReturnAddressSave)