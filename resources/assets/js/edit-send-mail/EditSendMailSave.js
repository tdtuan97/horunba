import React, { Component } from 'react';
import { connect } from 'react-redux';

import * as Action from '../common/actions';
import * as queryString from 'query-string';
import moment from 'moment';

import Loading from '../common/components/common/Loading';
import FORM from '../common/components/form/FORM';
import INPUT from '../common/components/form/INPUT';
import TEXTAREA from '../common/components/form/TEXTAREA';
import EDITOR from '../common/components/form/EDITOR';
import SELECT from '../common/components/form/SELECT';
import BUTTON from '../common/components/form/BUTTON';
import LABEL from '../common/components/form/LABEL';
import UPLOAD from '../common/components/form/UPLOAD';
import MessagePopup from '../common/components/common/MessagePopup';
import AcceptPopup from './containers/AcceptPopup';
import CHECKBOX_CUSTOM from '../common/components/form/CHECKBOX_CUSTOM';

class EditSendMailSave extends Component {
    constructor(props) {
        super(props);
        this.state = {attached_file : null, attached_file_path : null}
        let params = queryString.parse(props.location.search);

        if (params['receive_order_id'] && params['rec_mail_index']) {
            this.state.rec_mail_index = params['rec_mail_index'];
            this.state.receive_order_id = params['receive_order_id'];
        } else if (params['receive_order_id']) {
            this.state.receive_order_id = params['receive_order_id'];
        } else if (params['rec_mail_index']) {
            this.state.rec_mail_index = params['rec_mail_index'];
        }
        this.props.reloadData(getFormDataUrl, this.state);
    }

    handleCancel = (event) => {
        window.location.href = previousUrl;
    }

    handleSubmit = (event) => {
        document.getElementsByName('btnAccept')[0].disabled = true;
        let headers = {'X-Requested-With': 'XMLHttpRequest'};
        if (this.props.enctype) {
            headers['Content-Type'] = this.props.enctype;
        }
        let formData   = new FormData();
        if (!_.isEmpty(this.state)) {
            Object.keys(this.state).map((item) => {
                if (item === 'attached_file') {
                    formData.append(item, this.state[item]);
                } else {
                    formData.append(item, _.trim(this.state[item]));
                }
            });
        }
        this.props.postData(saveUrl, formData, headers);
        $('#access-memo-modal').modal('hide');
    }
    handleBeforeSent = (event) => {
        $('#access-memo-modal').modal('show');
    }
    handleChange = (event) => {
        const target = event.target;
        const name = target.name;
        const value = target.type === 'checkbox' ? (target.checked?1:0) : target.value;
        if (target.type === 'file') {
            let tmpName = name.replace(new RegExp("^browser_"), '');
            this.setState({
                [tmpName]: target.files[0].name ? target.files[0].name : '',
                ['attached_file']: target.files[0]
            }, ()=> {
                document.getElementsByName(tmpName)[0].value = target.files[0].name;

            });
        } else if (name === 'genre') {
            this.setState({
                genre: value,
                mail_id: ''
            }, () => {
                this.props.reloadData(getFormDataUrl, {
                    genre: this.state.genre,
                    rec_mail_index: this.state.rec_mail_index,
                    receive_order_id: this.state.receive_order_id});
            });
        } else {
            this.setState({
                [name]: value,
            }, () => {
                if (name === 'mail_id') {
                    this.props.reloadData(getFormDataUrl, {
                        genre: this.state.genre,
                        mail_id: this.state.mail_id,
                        rec_mail_index: this.state.rec_mail_index,
                        receive_order_id: this.state.receive_order_id
                    });
                }
            });
        }
    }

    handleEditorChange = (name, content, event) => {
        this.setState({
            [name]: content,
        });
    }

    handleClear = (event) => {
        this.setState({
            attached_file : undefined,
            attached_file_path : undefined
        });
        $('input[name="attached_file_path"]').val('');
        $('input[name="browser_attached_file_path"]').val('');
    }

    componentWillReceiveProps(nextProps) {
        if (!_.isEmpty(nextProps.dataSource) && this.props.dataSource !== nextProps.dataSource) {
            if (nextProps.dataSource.attached_file_path === null || nextProps.dataSource.attached_file_path === '' ) {
                delete nextProps.dataSource.attached_file_path;
            }
            let dataSet = {};
            dataSet.arrUpdate = nextProps.dataSource.arrUpdate;
            dataSet.genreData = nextProps.dataSource.genreData;
            dataSet.templateData = nextProps.dataSource.templateData;
            if (!_.isEmpty(nextProps.dataSource.mail_content)) {
                dataSet.mail_content = nextProps.dataSource.mail_content;
            }
            if (!_.isEmpty(nextProps.dataSource.mail_from)) {
                dataSet.mail_from = nextProps.dataSource.mail_from;
            }
            if (!_.isEmpty(nextProps.dataSource.mail_id)) {
                dataSet.mail_id = nextProps.dataSource.mail_id;
            }
            if (!_.isEmpty(nextProps.dataSource.mail_subject)) {
                dataSet.mail_subject = nextProps.dataSource.mail_subject;
            }
            if (!_.isEmpty(nextProps.dataSource.mail_to)) {
                dataSet.mail_to = nextProps.dataSource.mail_to;
            }
            if (!_.isEmpty(nextProps.dataSource.mail_to_bcc)) {
                dataSet.mail_to_bcc = nextProps.dataSource.mail_to_bcc;
            }
            if (!_.isEmpty(nextProps.dataSource.mail_to_cc)) {
                dataSet.mail_to_cc = nextProps.dataSource.mail_to_cc;
            }
            if (!_.isEmpty(nextProps.dataSource.receive_mail_content)) {
                dataSet.receive_mail_content = nextProps.dataSource.receive_mail_content;
            }
            this.setState({...dataSet});
        }

        if (!_.isEmpty(nextProps.dataResponse)) {
            if (nextProps.dataResponse.status === 1) {
                window.location.href = previousUrl;
            } else {
                document.getElementsByName('btnAccept')[0].disabled = false;
                if (nextProps.dataResponse.status === -1) {
                    this.setState({popup_message : nextProps.dataResponse.message, popup_status: "show", popup_type: "alert-error"});
                    setTimeout(this.handleHidePopup, 3000);
                }
                if (nextProps.dataResponse.status === 0)
                {
                    let messageRec = nextProps.dataResponse.message.rec_mail_index;
                    let messageOrder = nextProps.dataResponse.message.receive_order_id;
                    if (!_.isEmpty(messageRec)) {
                        this.setState({popup_message : messageRec, popup_status: "show", popup_type: "alert-error"});
                        setTimeout(this.handleHidePopup, 5000);
                    }
                    if (!_.isEmpty(messageOrder)) {
                        this.setState({popup_message : messageOrder, popup_status: "show", popup_type: "alert-error"});
                        setTimeout(this.handleHidePopup, 5000);
                    }
                }
            }
        }
    }

    handleHidePopup = () => {
        this.setState({popup_message : "", popup_status: ""});
    }

    componentDidUpdate() {
        if (!_.isEmpty(this.props.dataSource)) {
            $('.loading').hide();
        }
    }

    render() {
        let dataSource   = this.props.dataSource;
        let dataResponse = this.props.dataResponse;
        let token        = $("meta[name='csrf-token']").attr("content");
        return (!_.isEmpty(dataSource) &&
            <div>
            <div className="row">
            <div className="col-md-12">
            <div className="box box-primary">
            <FORM encType={"multipart/form-data; boundary=----WebKitFormBoundary" + token}
                className="form-horizontal">
                <div className="box-body">
                <Loading className="loading" />
                <div className="row">
                    <div className="col-md-6">
                        <SELECT
                            name="genre"
                            labelTitle={trans('messages.genre')}
                            className="form-control input-sm"
                            fieldGroupClass="col-md-9"
                            labelClass="col-md-3"
                            groupClass="form-group"
                            onChange={this.handleChange}
                            value={this.state.genre || ''}
                            options={this.state.genreData}
                            errorPopup={true}
                            hasError={(dataResponse.message && dataResponse.message.genre)?true:false}
                            errorMessage={(dataResponse.message && dataResponse.message.genre)?dataResponse.message.genre:""}
                            />
                    </div>
                    <div className="col-md-6">
                    {!this.state.rec_mail_index && !this.state.rec_mail_index ?
                        <div className="row">
                            <div className="col-md-offset-3 col-md-4">
                                <div className="form-group">
                                    <div className="col-md-2">
                                        <CHECKBOX_CUSTOM
                                            name="chk_shien"
                                            defaultChecked={this.state.chk_shien === 1 ? true : false}
                                            onChange={this.handleChange}
                                         />
                                    </div>
                                    <label className="col-md-9">
                                        {trans('messages.chk_shien')}
                                    </label>
                                </div>
                            </div>
                        </div>
                        :
                        ""
                    }
                    </div>
                </div>
                <div className="row">
                    <div className="col-md-6">
                        <SELECT
                            name="mail_id"
                            labelTitle={trans('messages.template_name')}
                            className="form-control input-sm"
                            fieldGroupClass="col-md-9"
                            labelClass="col-md-3"
                            groupClass="form-group"
                            onChange={this.handleChange}
                            value={this.state.mail_id || ''}
                            options={this.state.templateData}
                            errorPopup={true}
                            hasError={(dataResponse.message && dataResponse.message.mail_id)?true:false}
                            errorMessage={(dataResponse.message && dataResponse.message.mail_id)?dataResponse.message.mail_id:""}
                            />
                    </div>
                    <div className="col-md-6">
                        <INPUT
                            name="mail_to"
                            labelTitle={trans('messages.receive_mail_to')}
                            className="form-control input-sm"
                            fieldGroupClass="col-md-9"
                            labelClass="col-md-3"
                            groupClass="form-group"
                            readOnly={this.state.chk_shien === 1 ? true : false}
                            onChange={this.handleChange}
                            value={this.state.mail_to || ''}
                            hasError={(dataResponse.message && dataResponse.message.mail_to)?true:false}
                            errorMessage={(dataResponse.message && dataResponse.message.mail_to)?dataResponse.message.mail_to:""}
                            />
                    </div>
                </div>
                <div className="row">
                    <div className="col-md-6">
                        <INPUT
                            name="mail_from"
                            labelTitle={trans('messages.receive_mail_from')}
                            className="form-control input-sm"
                            fieldGroupClass="col-md-9"
                            labelClass="col-md-3"
                            groupClass="form-group"
                            onChange={this.handleChange}
                            value={this.state.mail_from || ''}
                            />
                    </div>
                    <div className="col-md-6">
                        <INPUT
                            name="mail_to_cc"
                            labelTitle={trans('messages.receive_mail_cc')}
                            className="form-control input-sm"
                            fieldGroupClass="col-md-9"
                            labelClass="col-md-3"
                            groupClass="form-group"
                            onChange={this.handleChange}
                            value={this.state.mail_to_cc || ''}
                            />
                    </div>
                </div>
                <div className="row">
                    <div className="col-md-6">
                        <INPUT
                            name="mail_subject"
                            labelTitle={trans('messages.template_subject')}
                            className="form-control input-sm"
                            fieldGroupClass="col-md-9"
                            labelClass="col-md-3"
                            groupClass="form-group"
                            onChange={this.handleChange}
                            value={this.state.mail_subject || ''}
                            errorPopup={true}
                            hasError={(dataResponse.message && dataResponse.message.mail_subject)?true:false}
                            errorMessage={(dataResponse.message && dataResponse.message.mail_subject)?dataResponse.message.mail_subject:""}
                            />
                    </div>
                    <div className="col-md-6">
                        <INPUT
                            name="mail_to_bcc"
                            labelTitle={trans('messages.receive_mail_bcc')}
                            className="form-control input-sm"
                            fieldGroupClass="col-md-9"
                            labelClass="col-md-3"
                            groupClass="form-group"
                            onChange={this.handleChange}
                            value={this.state.mail_to_bcc || ''}
                            />
                    </div>
                </div>
                <div className="row">
                    <div className="col-md-6">
                        <EDITOR
                            apiKey='sm3yi664b07edp6t6hf1trx3xy6ziv5xg41muurzssvy4773'
                            init={{
                                force_br_newlines : true,
                                force_p_newlines : false,
                                forced_root_block : '',
                                branding: false,
                                menubar: '',
                                toolbar: 'undo redo | bold italic | alignleft aligncenter alignright | forecolor backcolor | copyButton',
                                fontsize_formats: '8pt 9pt 10pt 11pt 12pt 13pt 14pt 15pt 16pt 17pt 18pt 24pt 36pt 48pt 60pt 72pt 96pt',
                                height : 300,
                                plugins: "paste",
                                paste_as_text: true,
                                setup: function (editor) {
                                    editor.addButton('copyButton', {
                                        text: 'Copy content',
                                        onclick: function (_) {
                                            var el = document.createElement('textarea');
                                            el.value = editor.getContent({format : 'text'});
                                            el.setAttribute('readonly', '');
                                            el.style = {position: 'absolute', left: '-9999px'};
                                            document.body.appendChild(el);
                                            el.select();
                                            document.execCommand('copy');
                                            document.body.removeChild(el);
                                        }
                                    });
                                }
                            }}
                            name="mail_content"
                            labelTitle={trans('messages.mail_content')}
                            className="form-control input-sm"
                            fieldGroupClass="col-md-9"
                            labelClass="col-md-3"
                            groupClass="form-group"
                            onEditorChange={(event) => this.handleEditorChange('mail_content', event)}
                            value={this.state.mail_content || ''}
                            errorPopup={true}
                            hasError={(dataResponse.message && dataResponse.message.mail_content)?true:false}
                            errorMessage={(dataResponse.message && dataResponse.message.mail_content)?dataResponse.message.mail_content:""}
                        />
                    </div>
                    <div className="col-md-6">
                        {  (this.state.receive_order_id)  || (this.state.rec_mail_index) ?
                        <TEXTAREA
                            name="receive_mail_content"
                            labelTitle={trans('messages.receive_mail_content')}
                            className="form-control input-sm"
                            fieldGroupClass="col-md-9"
                            labelClass="col-md-3"
                            groupClass="form-group"
                            onChange={this.handleChange}
                            value={this.state.receive_mail_content || ''}
                            errorPopup={true}
                            rows="15"
                            hasError={(dataResponse.message && dataResponse.message.receive_mail_content)?true:false}
                            errorMessage={(dataResponse.message && dataResponse.message.receive_mail_content)?dataResponse.message.receive_mail_content:""}
                            />
                        : '' }
                    </div>
                </div>
                <div className="row">
                    <div className="col-md-6">
                        <UPLOAD
                            name="attached_file_path"
                            labelTitle={trans('messages.attached_file_path')}
                            className="form-control input-sm"
                            fieldGroupClass="col-md-9"
                            labelClass="col-md-3"
                            groupClass="form-group"
                            itemGroupClass="input-group input-group-sm"
                            btnClass="btn btn-info btn-flat btn-largest"
                            btnTitle={trans('messages.btn_browser')}
                            clear={true}
                            onClick={this.handleClear}
                            btnClear={trans('messages.btn_clear')}
                            onChange={this.handleChange}
                            value={this.state.attached_file_path || ''}
                            hasError={(dataResponse.message && dataResponse.message['attached_file'])?true:false}
                            errorMessage={(dataResponse.message&&dataResponse.message['attached_file'])?dataResponse.message['attached_file']:""}/>
                    </div>
                    <div className="col-md-6">
                        <BUTTON className="btn bg-purple input-sm pull-right btn-larger pull-right" type="button" onClick={this.handleCancel}>{trans('messages.btn_cancel')}</BUTTON>
                        <BUTTON name="btnAccept" className="btn btn-success input-sm btn-larger pull-right margin-element" type="button" onClick={this.handleBeforeSent}>{trans('messages.btn_accept')}</BUTTON>
                    </div>
                </div>
            </div>
            </FORM>
            <MessagePopup
                message={this.state.popup_message}
                status={this.state.popup_status}
                classType={this.state.popup_type}
                manualHide={true}
            />
            <AcceptPopup
                handleSubmit={this.handleSubmit}
            />
            </div>
            </div>
            </div>
            </div>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        dataSource: state.commonReducer.dataSource,
        dataResponse: state.commonReducer.dataResponse,
        dataRealTime: state.commonReducer.dataRealTime
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        reloadData: (url, params) => { dispatch(Action.detailData(url, params)) },
        postData: (url, params, headers) => { dispatch(Action.postData(url, params, headers)) },
        postRealTime: (url, params, headers) => { dispatch(Action.postRealTime(url, params, headers)) }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(EditSendMailSave)