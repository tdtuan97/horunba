import React, { Component } from 'react';
import Loading from '../../common/components/common/Loading';
export default class AcceptPopup extends Component {
    constructor(props) {
        super(props);
        this.state = {}
    }
    handleSubmit = () => {
        $(".loading").show();
        this.props.handleSubmit();
    }
    handleCancelClick = () => {
        $('#access-memo-modal').modal('hide');
    }
    render() {
        return (
            <div className="modal fade" id="access-memo-modal">
                <Loading className="loading" />
                <div className="modal-dialog ">
                    <div className="modal-content">
                        <div className="modal-header">
                        <b>{trans('messages.accept_title')}</b>
                        </div>
                        <div className="modal-body modal-custom-body">
                            <div className="row">
                                <div className="col-md-12">
                                    {trans('messages.accept_content')}
                                </div>                            
                            </div>                            
                        </div>
                        <div className="modal-footer modal-custom-footer">
                            <button type="button" className="btn btn-primary btn-sm modal-custom-button" onClick={this.handleSubmit}>{trans('messages.btn_accept_ok')}</button>
                            <button type="button" className="btn bg-purple btn-sm modal-custom-button" onClick={this.handleCancelClick}>{trans('messages.btn_accept_cancel')}</button>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}