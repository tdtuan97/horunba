import React, { Component } from 'react';
import { connect } from 'react-redux';

import * as Action from '../common/actions';
import * as queryString from 'query-string';
import moment from 'moment';

import Loading from '../common/components/common/Loading';
import FORM from '../common/components/form/FORM';
import INPUT from '../common/components/form/INPUT';
import SELECT from '../common/components/form/SELECT';
import BUTTON from '../common/components/form/BUTTON';
import CHECKBOX_CUSTOM from '../common/components/form/CHECKBOX_CUSTOM';
import LABEL from '../common/components/form/LABEL';
import DATEPICKER from '../common/components/form/DATEPICKER';
import MESSAGE_MODAL from '../common/containers/MessageModal';

class ShippingFreeBelowLimitSave extends Component {
    constructor(props) {
        super(props);
        this.state = {}
        this.firstRender = true;
        let params = queryString.parse(props.location.search);
        if (params['index']) {
            this.state.index = params['index'];
            this.state.accessFlg = true;
        }
        this.props.reloadData(getFormDataUrl, this.state);
    }

    handleCancel = (event) => {
        window.location.href = baseUrl;
    }

    hanleRealtime() {
        if (this.state.index !== '') {
            let params = {
                accessFlg : this.state.accessFlg,
                query : {
                        page_key : 'SHIPPING-FREE-SAVE',
                        primary_key : 'index:' + this.state.index
                    },
                key :this.state.index
            };
            if (this.state.accessFlg !== false) {
                this.props.postRealTime(checkEditUrl, params, {'X-Requested-With': 'XMLHttpRequest'});
            }
        }
    }

    handleSubmit = (event) => {
        document.getElementsByName('btnAccept')[0].disabled = true;
        let headers = {'X-Requested-With': 'XMLHttpRequest'};
        this.props.postData(saveUrl, this.state, headers);
    }

    handleChange = (event) => {
        const target = event.target;
        const name = target.name;
        const value = target.type === 'checkbox' ? (target.checked?1:0) : target.value;
        this.setState({
            [name]: value
        });
    }

    handleChangeDate = function(date, name, event) {
        let dateValue = moment(date);
        if (!dateValue.isValid()) {
            dateValue = undefined;
        } else {
            dateValue = dateValue.format('YYYY-MM-DD');
        }
        this.setState({
          [name]: dateValue,
        });
    }
    componentWillUnmount () {
        clearInterval(this.state);
    }
    componentDidMount() {
        setInterval(() => {
            this.hanleRealtime()
        }, 5000);

    }
    componentWillReceiveProps(nextProps) {
        let dataSource = nextProps.dataSource;
        if (!_.isEmpty(nextProps.dataSource)) {
            if (!nextProps.dataSource.data) {
                window.location.href = baseUrl;
            } else {
                let dateSet = {};
                if (nextProps.dataSource.data.start_date && this.firstRender) {
                    let tmpStartDate = moment(nextProps.dataSource.data.start_date);
                    if (tmpStartDate.isValid()) {
                        dateSet['start_date'] = tmpStartDate.format('YYYY-MM-DD');
                    }
                }
                if (nextProps.dataSource.data.end_date && this.firstRender) {
                    let tmpEndDate = moment(nextProps.dataSource.data.end_date);
                    if (tmpEndDate.isValid()) {
                        dateSet['end_date'] = tmpEndDate.format('YYYY-MM-DD');
                    }
                }
                if (!_.isEmpty(dateSet)) {
                    this.setState(dateSet);
                }
            }
            this.firstRender = false;
            if (!_.isEmpty(dataSource.data)  && _.isEmpty(nextProps.dataRealTime) ) {
                this.setState({...dataSource.data}, () => {
                    this.hanleRealtime();
                });
            }
        }

        if (!_.isEmpty(nextProps.dataResponse)) {
            if (nextProps.dataResponse.status === 1) {
                window.location.href = baseUrl;
            } else {
                document.getElementsByName('btnAccept')[0].disabled = false;
            }
        }

        // Check realtime update
        if (!_.isEmpty(nextProps.dataRealTime)) {
            this.setState(nextProps.dataRealTime);
        }
    }

    componentDidUpdate() {
        if (!_.isEmpty(this.props.dataSource)) {
            $('.loading').hide();
        }
        if (this.state.accessFlg === false) {
            $('#message-modal').modal('show');
            //We will auto redirect after 5 seconds
            setTimeout(() => {
                window.location.href = baseUrl;
            }, 5000);
        }
    }

    render() {
        let dataSource   = this.props.dataSource;
        let dataResponse = this.props.dataResponse;

        return ((!_.isEmpty(dataSource)) &&
            <div>
            <div className="box box-primary">
            <FORM
                className="form-horizontal">
                <div className="box-body">
                <Loading className="loading" />
                <div className="row">
                    <div className="col-md-6 col-md-offset-3 col-md-offset-right-3">
                        <SELECT
                            name="mall_id"
                            labelTitle={trans('messages.mall_name')}
                            className="form-control input-sm"
                            fieldGroupClass="col-md-8"
                            labelClass="col-md-4"
                            onChange={this.handleChange}
                            defaultValue={dataSource.data.mall_id}
                            options={dataSource.mallOpt}
                            errorPopup={true}
                            hasError={(dataResponse.message && dataResponse.message.mall_id)?true:false}
                            errorMessage={(dataResponse.message&&dataResponse.message.mall_id)?dataResponse.message.mall_id:""}/>
                        <INPUT
                            name="charge_price"
                            labelTitle={trans('messages.charge_price_save')}
                            className="form-control input-sm"
                            fieldGroupClass="col-md-8"
                            labelClass="col-md-4"
                            onChange={this.handleChange}
                            defaultValue={dataSource.data.charge_price}
                            errorPopup={true}
                            hasError={(dataResponse.message && dataResponse.message.charge_price)?true:false}
                            errorMessage={(dataResponse.message&&dataResponse.message.charge_price)?dataResponse.message.charge_price:""}/>
                        <INPUT
                            name="below_limit_price"
                            labelTitle={trans('messages.below_limit_price')}
                            className="form-control input-sm"
                            fieldGroupClass="col-md-8"
                            labelClass="col-md-4"
                            onChange={this.handleChange}
                            defaultValue={dataSource.data.below_limit_price}
                            errorPopup={true}
                            hasError={(dataResponse.message && dataResponse.message.below_limit_price)?true:false}
                            errorMessage={(dataResponse.message&&dataResponse.message.below_limit_price)?dataResponse.message.below_limit_price:""}/>
                        <DATEPICKER
                            placeholderText="YYYY-MM-DD"
                            dateFormat="YYYY-MM-DD"
                            locale="ja"
                            name="start_date"
                            minDate={moment()}
                            labelTitle={trans('messages.start_date')}
                            className="form-control input-sm"
                            fieldGroupClass="col-md-8"
                            labelClass="col-md-4"
                            selected={this.state.start_date ? moment(this.state.start_date) : null}
                            onChange={(date,name, e) => this.handleChangeDate(date, 'start_date', e)}
                            errorPopup={true}
                            hasError={(dataResponse.message && dataResponse.message.start_date)?true:false}
                            errorMessage={(dataResponse.message&&dataResponse.message.start_date)?dataResponse.message.start_date:""}/>
                        <DATEPICKER
                            placeholderText="YYYY-MM-DD"
                            dateFormat="YYYY-MM-DD"
                            locale="ja"
                            name="end_date"
                            minDate={moment()}
                            labelTitle={trans('messages.end_date')}
                            className="form-control input-sm"
                            fieldGroupClass="col-md-8"
                            labelClass="col-md-4"
                            selected={this.state.end_date ? moment(this.state.end_date) : null}
                            onChange={(date,name, e) => this.handleChangeDate(date, 'end_date', e)}
                            errorPopup={true}
                            hasError={(dataResponse.message && dataResponse.message.end_date)?true:false}
                            errorMessage={(dataResponse.message&&dataResponse.message.end_date)?dataResponse.message.end_date:""}/>
                        <SELECT
                            name="is_delete"
                            labelTitle={trans('messages.is_delete')}
                            className="form-control input-sm"
                            fieldGroupClass="col-md-8"
                            labelClass="col-md-4"
                            onChange={this.handleChange}
                            defaultValue={dataSource.data.is_delete}
                            options={dataSource.delOpt}
                            errorPopup={true}
                            hasError={(dataResponse.message && dataResponse.message.is_delete)?true:false}
                            errorMessage={(dataResponse.message&&dataResponse.message.is_delete)?dataResponse.message.is_delete:""}/>
                    </div>
                </div>
                <div className="row">
                    <div className="col-md-6 col-md-offset-3 col-md-offset-right-3">
                        <BUTTON className="btn bg-purple input-sm btn-larger pull-right" type="button" onClick={this.handleCancel}>{trans('messages.btn_cancel')}</BUTTON>
                        <BUTTON name="btnAccept" className="btn btn-success input-sm btn-larger margin-element pull-right" type="button" onClick={this.handleSubmit}>{trans('messages.btn_accept')}</BUTTON>
                    </div>
                </div>
                </div>
            </FORM>
            <MESSAGE_MODAL
                message={this.state.message || ''}
                title={trans('messages.deny_edit')}
                baseUrl={baseUrl}
            />
            </div>
            </div>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        dataSource: state.commonReducer.dataSource,
        dataResponse: state.commonReducer.dataResponse,
        dataRealTime: state.commonReducer.dataRealTime
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        reloadData: (url, params) => { dispatch(Action.detailData(url, params)) },
        postData: (url, params, headers) => { dispatch(Action.postData(url, params, headers)) },
        postRealTime: (url, params, headers) => { dispatch(Action.postRealTime(url, params, headers)) }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(ShippingFreeBelowLimitSave)