import React, { Component } from 'react';

import { connect } from 'react-redux';
import * as Action from '../common/actions';
import * as queryString from 'query-string';
import DatePicker from 'react-datepicker';
import moment from 'moment';
import 'react-datepicker/dist/react-datepicker.css';

import BUTTON from '../common/components/form/BUTTON';
import FORM from '../common/components/form/FORM';
import INPUT from '../common/components/form/INPUT';
import SELECT from '../common/components/form/SELECT';
import CHECKBOX_CUSTOM from '../common/components/form/CHECKBOX_CUSTOM';
import LABEL from '../common/components/form/LABEL';

import TABLE from '../common/components/table/TABLE';
import THEAD from '../common/components/table/THEAD';
import TBODY from '../common/components/table/TBODY';
import TR from '../common/components/table/TR';
import TH from '../common/components/table/TH';
import TD from '../common/components/table/TD';

import SortComponent from '../common/components/table/SortComponent';
import PaginationContainer from '../common/containers/PaginationContainer'
import Loading from '../common/components/common/Loading';

class ShippingFreeBelowLimitList extends Component {

    constructor(props) {
        super(props);
        const stringParams = queryString.parse(props.location.search);
        this.state = {};
        if (!_.isEmpty(stringParams)) {
            this.state = stringParams;
        }
        this.props.reloadData(dataListUrl, this.state);
    }

    handleChange = (event) => {
        const target = event.target;
        const value = target.type === 'checkbox' ? target.checked : target.value;
        const name = target.name;
        this.setState({
            [name]: (value)?value:''
        },() => {
            if (name === 'per_page' || name === 'mall_id' || name === 'is_delete') {
                this.handleReloadData();
            }
        });
    };

    handleChangeMultiSelect = (name, value, checked) => {
        let currentValue = this.state[name];
        if (typeof currentValue === 'undefined') {
            currentValue = [];
        } else if (!Array.isArray(this.state[name])) {
            currentValue = [this.state[name]];
        }
        if (Array.isArray(value)) {
            currentValue = [...value];
        } else {
            let index = currentValue.indexOf(value);
            if (checked === false) {
                if (index !== -1) {
                    currentValue.splice(index, 1);
                }
            } else {
                if (index === -1) {
                    currentValue.push(value);
                }
            }
        }
        this.setState({
            [name]: currentValue,
        });
    };

    handleClearClick = (event) => {
        Object.keys(this.state).map((key) => {
            if (key !== 'per_page') {
                delete this.state[key];
            }
        });
        this.handleReloadData();
    };

    handleReloadData = () => {
        if (this.state.page) {
            delete this.state.page;
        }
        this.props.reloadData(dataListUrl, this.state, true);
    };

    componentDidUpdate() {
        if (!_.isEmpty(this.props.dataSource)) {
            $('.loading').hide();
        }
    }

    componentWillMount() {
        window.onpopstate = (event) => {
            location.reload();
        };
    };

    handleSortClick = (name, nextSort) => {
        Object.keys(this.state).map((key) => {
            if (key.startsWith('sort_')) {
                delete this.state[key];
            }
        });
        if (nextSort === 'none') {
            delete this.state[name];
            this.props.reloadData(dataListUrl, this.state, true);
        } else {
            this.setState({[name]: nextSort}, () => {
                this.props.reloadData(dataListUrl, this.state, true);
            });
        }

    }

    handleChangeDate = function(date, name, event) {
        let dateValue = moment(date);
        if (!dateValue.isValid()) {
            dateValue = undefined;
        } else {
            dateValue = dateValue.format('YYYY-MM-DD');
        }
        this.setState({
          [name]: dateValue,
        });
    };

    handleClickPage = (page) => {
        this.setState({
            page: page
        },() => {
            this.props.reloadData(dataListUrl, this.state, true);
        });
    }

    handleIsDeleteChange = (index, event) => {
        const value = event.target.checked;
        let params  = {
            index : index,
            is_delete : value?1:0
        };
        this.props.postData(changeDeleteUrl, params, {'X-Requested-With': 'XMLHttpRequest'}, this.handleReloadData);
    };

    render() {
        let dataSource = this.props.dataSource;
        let index = 0;
        let totalItems = 0;
        let itemsPerPage = 20;
        if (!_.isEmpty(dataSource)) {
            index = dataSource.data.from - 1;
            totalItems   = dataSource.data.total;
            itemsPerPage = dataSource.data.per_page;
        }

        return ( (!_.isEmpty(dataSource)) &&
            <div>
                <Loading className="loading" />
                <div className="box">
                    <div className="box-header">
                        <div className="row">
                                <div className="col-md-12">
                                    <h4>{trans('messages.search_title')}</h4>
                                </div>
                                <FORM>
                                    <SELECT
                                        id="mall_id"
                                        name="mall_id"
                                        groupClass="form-group col-md-1-5"
                                        labelTitle={trans('messages.mall_name')}
                                        handleChangeMultiSelect={this.handleChangeMultiSelect}
                                        valueMulti={this.state.mall_id||''}
                                        options={dataSource.mallOpt}
                                        multiple="multiple"
                                        className="form-control input-sm" />
                                    <INPUT
                                        name="charge_price_from"
                                        groupClass="form-group col-md-1-75"
                                        labelTitle={trans('messages.charge_price_from')}
                                        onChange={this.handleChange}
                                        value={this.state.charge_price_from||''}
                                        className="form-control input-sm" />
                                    <INPUT
                                        name="charge_price_to"
                                        groupClass="form-group col-md-1-75"
                                        labelTitle={trans('messages.charge_price_to')}
                                        onChange={this.handleChange}
                                        value={this.state.charge_price_to||''}
                                        className="form-control input-sm" />
                                    <LABEL
                                        groupClass="form-group col-md-1-75"
                                        labelTitle={trans('messages.start_date_from')}>
                                        <DatePicker
                                            placeholderText="YYYY-MM-DD"
                                            dateFormat="YYYY-MM-DD"
                                            locale="ja"
                                            name="start_date_from"
                                            className="form-control input-sm"
                                            selected={this.state.start_date_from ? moment(this.state.start_date_from) : null}
                                            onChange={(date,name, e) => this.handleChangeDate(date, 'start_date_from', e)} />
                                    </LABEL>
                                    <LABEL
                                        groupClass="form-group col-md-1-75"
                                        labelTitle={trans('messages.start_date_to')}>
                                        <DatePicker
                                            placeholderText="YYYY-MM-DD"
                                            dateFormat="YYYY-MM-DD"
                                            locale="ja"
                                            name="start_date_to"
                                            className="form-control input-sm"
                                            selected={this.state.start_date_to ? moment(this.state.start_date_to) : null}
                                            onChange={(date,name, e) => this.handleChangeDate(date, 'start_date_to', e)} />
                                    </LABEL>
                                    <SELECT
                                        id="is_delete"
                                        name="is_delete"
                                        groupClass="form-group col-md-1-25"
                                        labelTitle={trans('messages.is_delete')}
                                        handleChangeMultiSelect={this.handleChangeMultiSelect}
                                        valueMulti={this.state.is_delete||''}
                                        options={dataSource.delOpt}
                                        multiple="multiple"
                                        className="form-control input-sm" />

                                    <SELECT
                                        name="per_page"
                                        groupClass="form-group col-md-1 col-md-offset-1 per-page pull-right-md"
                                        labelClass="pull-right"
                                        labelTitle={trans('messages.per_page')}
                                        onChange={this.handleChange}
                                        value={this.state.per_page||''}
                                        options={{20:20, 40:40, 60:60, 80:80, 100:100}}
                                        className="form-control input-sm" />
                                </FORM>
                        </div>
                        <div className="row">
                            <div className="col-md-9 col-xs-4">
                                <a href={saveUrl} className="btn btn-success input-sm">{trans('messages.btn_add')}</a>
                            </div>
                            <div className="col-md-3 col-xs-8">
                                <BUTTON className="btn btn-danger pull-right" onClick={this.handleClearClick}>{trans('messages.btn_reset')}</BUTTON>
                                <BUTTON className="btn btn-primary pull-right margin-element" onClick={this.handleReloadData} >{trans('messages.btn_search')}</BUTTON>
                            </div>
                        </div>
                    </div>
                    <div className="box-body">
                        <div className="row">
                            <div className="col-md-12">
                                {(totalItems/itemsPerPage > 1) &&
                                <div className="row">
                                    <div className="col-md-2-5"></div>
                                    <div className="col-md-7 text-center">
                                        <PaginationContainer handleClickPage={this.handleClickPage}/>
                                    </div>
                                    <div className="col-md-2-5 text-right line-height-md">
                                        {dataSource.data.from}件 - {dataSource.data.to}件 【全{dataSource.data.total}件】
                                    </div>
                                </div>
                                }
                                <TABLE className="table table-striped table-custom">
                                    <THEAD>
                                        <TR>
                                            <TH className="visible-xs visible-sm text-center"></TH>
                                            <TH className="text-center">
                                                <SortComponent
                                                    name="sort_index"
                                                    onClick={this.handleSortClick}
                                                    currentSort={(this.state.sort_index)?this.state.sort_index:'none'}
                                                    title={trans('messages.index')} />
                                            </TH>
                                            <TH className="hidden-xs hidden-sm text-center">
                                                <SortComponent
                                                    name="sort_mall_name"
                                                    onClick={this.handleSortClick}
                                                    currentSort={(this.state.sort_mall_name)?this.state.sort_mall_name:'none'}
                                                    title={trans('messages.mall_name')} />
                                            </TH>
                                            <TH className="hidden-xs hidden-sm text-center">
                                                <SortComponent
                                                    name="sort_charge_price"
                                                    onClick={this.handleSortClick}
                                                    currentSort={(this.state.sort_charge_price)?this.state.sort_charge_price:'none'}
                                                    title={trans('messages.charge_price')} />
                                            </TH>
                                            <TH className="hidden-xs hidden-sm text-center">
                                                <SortComponent
                                                    name="sort_below_limit_price"
                                                    onClick={this.handleSortClick}
                                                    currentSort={(this.state.sort_below_limit_price)?this.state.sort_below_limit_price:'none'}
                                                    title={trans('messages.below_limit_price')} />
                                            </TH>
                                            <TH className="hidden-xs hidden-sm text-center">
                                                <SortComponent
                                                    name="sort_start_date"
                                                    onClick={this.handleSortClick}
                                                    currentSort={(this.state.sort_start_date)?this.state.sort_start_date:'none'}
                                                    title={trans('messages.start_date')} />
                                            </TH>
                                            <TH className="hidden-xs hidden-sm text-center">
                                                <SortComponent
                                                    name="sort_end_date"
                                                    onClick={this.handleSortClick}
                                                    currentSort={(this.state.sort_end_date)?this.state.sort_end_date:'none'}
                                                    title={trans('messages.end_date')} />
                                            </TH>
                                            <TH className="text-center">
                                                {trans('messages.is_delete')}
                                            </TH>
                                        </TR>
                                    </THEAD>
                                    <TBODY>
                                    {
                                        (!_.isEmpty(dataSource.data.data))?
                                        dataSource.data.data.map((item) => {
                                            index++;
                                            let classRow = (index%2 ===0) ? 'odd' : 'even';
                                            return (
                                                [
                                                <TR key={"tr" + index} className={classRow}>
                                                    <TD className="visible-xs visible-sm text-center">
                                                        <b className="show-info">
                                                            <i className="fa fa-angle-double-down" aria-hidden="true"></i>
                                                        </b>
                                                    </TD>
                                                    <TD className="text-center">
                                                        <a className="link-detail" href={saveUrl + "?index=" + item.index}>
                                                            {item.index}
                                                        </a>
                                                    </TD>
                                                    <TD className="hidden-xs hidden-sm cut-text"  title={item.name_jp}>
                                                        <span className="limit-char max-width-100">{item.name_jp}</span>
                                                    </TD>
                                                    <TD className="hidden-xs hidden-sm cut-text text-right" title={'￥' + item.charge_price}>
                                                        <span className="limit-char max-width-100">{'￥' + item.charge_price}</span>
                                                    </TD>
                                                    <TD className="hidden-xs hidden-sm cut-text text-right" title={'￥' + item.below_limit_price}>
                                                        <span className="limit-char max-width-100">{'￥' + item.below_limit_price}</span>
                                                    </TD>
                                                    <TD className="hidden-xs hidden-sm cut-text" title={item.start_date}>
                                                        <span className="limit-char max-width-100">{item.start_date}</span>
                                                    </TD>
                                                    <TD className="hidden-xs hidden-sm cut-text" title={item.end_date}>
                                                        <span className="limit-char max-width-150">{item.end_date}</span>
                                                    </TD>
                                                    <TD className="text-center">
                                                        <CHECKBOX_CUSTOM
                                                            key={'is_delete' + item.index + Math.random()}
                                                            defaultChecked={item.is_delete}
                                                            name={'is_delete[' + item.index + ']'}
                                                            onChange={(e) => this.handleIsDeleteChange(item.index, e)}
                                                            />
                                                    </TD>
                                                </TR>,
                                                <TR key={"trhide" + index} className="hiden-info" style={{display:"none"}}>
                                                    <TD colSpan="4" className="visible-xs visible-sm width-span1">
                                                        <ul id={"temp" + item.index}>
                                                            <li>
                                                                <b>{trans('messages.index')} : </b>
                                                                {item.index}
                                                            </li>
                                                            <li>
                                                                <b>{trans('messages.mall_name')} : </b>
                                                                {item.mall_id}
                                                            </li>
                                                            <li>
                                                                <b>{trans('messages.charge_price')} : </b>
                                                                {'￥' + item.charge_price}
                                                            </li>
                                                            <li>
                                                                <b>{trans('messages.below_limit_price')} : </b>
                                                                {'￥' + item.below_limit_price}
                                                            </li>
                                                            <li>
                                                                <b>{trans('messages.start_date')} : </b>
                                                                {item.start_date}
                                                            </li>
                                                            <li>
                                                                <b>{trans('messages.end_date')} : </b>
                                                                {item.end_date}
                                                            </li>
                                                        </ul>
                                                    </TD>
                                                </TR>
                                               ]
                                            )
                                        })
                                        :
                                        <TR><TD colSpan="9" className="text-center">{trans('messages.no_data')}</TD></TR>
                                    }
                                    </TBODY>
                                </TABLE>
                                <div className="text-center">
                                    <PaginationContainer handleClickPage={this.handleClickPage}/>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        dataSource: state.commonReducer.dataSource,
        dataResponse: state.commonReducer.dataResponse
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        reloadData: (url, params, pushHistory, ignoreFields) => { dispatch(Action.reloadData(url, params, pushHistory, ignoreFields)) },
        postData: (url, params, headers, callback) => { dispatch(Action.postData(url, params, headers, callback)) }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(ShippingFreeBelowLimitList)