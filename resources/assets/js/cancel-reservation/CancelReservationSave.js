import React, { Component } from 'react';
import { connect } from 'react-redux';

import * as Action from '../common/actions';
import * as queryString from 'query-string';
import moment from 'moment';

import Loading from '../common/components/common/Loading';
import FORM from '../common/components/form/FORM';
import INPUT from '../common/components/form/INPUT';
import TEXTAREA from '../common/components/form/TEXTAREA';
import SELECT from '../common/components/form/SELECT';
import BUTTON from '../common/components/form/BUTTON';
import CHECKBOX_CUSTOM from '../common/components/form/CHECKBOX_CUSTOM';
import LABEL from '../common/components/form/LABEL';
import DATEPICKER from '../common/components/form/DATEPICKER';
import MessagePopup from '../common/components/common/MessagePopup';
import MESSAGE_MODAL from '../common/containers/MessageModal';
import axios from 'axios';
var timer;

class CancelReservationSave extends Component {
    constructor(props) {
        super(props);
        let params = queryString.parse(props.location.search);
        this.state = {};
        this.firstRender = true;

        if (params['mall_id'] && params['receive_order_id']) {
            this.state.mall_id   = params['mall_id'];
            this.state.receive_order_id = params['receive_order_id'];
            this.state.action = 'save';
            this.state.accessFlg = true;
            this.state.index = this.state.mall_id + this.state.receive_order_id;
        } else {
            this.state = {mall_id : null, receive_order_id : null, note : null, is_deleted : 0 , action : 'add' }
        }
        this.props.reloadData(getFormDataUrl, this.state);
    }
    handleCancel = (event) => {
        window.location.href = baseUrl;
    }
    hanleRealtime() {
        if (!_.isEmpty(this.state.index)) {
            let params = {
                accessFlg : this.state.accessFlg,
                query : {
                        page_key : 'CANCEL-RESERVATION-SAVE',
                        primary_key : 'mall_id:' + this.state.mall_id + '/receive_order_id:' + this.state.receive_order_id
                    },
                key :this.state.index
            };
            if (this.state.accessFlg !== false) {
                this.props.postRealTime(checkEditUrl, params, {'X-Requested-With': 'XMLHttpRequest'});
            }
        }
    }
    handleSubmit = (event) => {
        $("#btnSubmit").prop('disabled', true);
        let headers = {'X-Requested-With': 'XMLHttpRequest'};
        if (!_.isEmpty(this.state)) {
            this.props.postData(saveUrl, this.state, headers);
        }
    }
    handleChange = (event) => {
        const target = event.target;
        const name = target.name;
        const value = target.type === 'checkbox' ? target.checked : target.value;
        this.setState({
            [name]: value
        });
    }
    componentWillReceiveProps(nextProps) {
        if (!_.isEmpty(nextProps.dataSource)) {
            if (!_.isEmpty(nextProps.dataSource.data)) {
                if (this.firstRender) {
                    this.hanleRealtime();
                    this.firstRender = false;
                    this.setState(nextProps.dataSource.data);
                }
            }
        }
        if (!_.isEmpty(nextProps.dataResponse)) {
            if (nextProps.dataResponse.status === 1) {
                window.location.href = baseUrl;
            } else {
                document.getElementsByName('btnAccept')[0].disabled = false;
            }
        }
        // Check realtime update
        if (!_.isEmpty(nextProps.dataRealTime)) {
            this.setState(nextProps.dataRealTime);
        }
    }
    componentDidUpdate() {
        if (!_.isEmpty(this.props.dataSource)) {
            $('.loading').hide();
        }
    }

    componentDidMount() {
        setInterval(() => {
            this.hanleRealtime();
        }, 5000);

    }

    render() {
        let dataSource   = this.props.dataSource;
        let dataResponse = this.props.dataResponse;

        return ((!_.isEmpty(dataSource) ) &&
            <div>
            <div className="box box-primary">
            <div className="box-header with-border">
            <FORM
                className="form-horizontal">
                <div className="box-body">
                <Loading className="loading" />
                <MessagePopup classType="alert-error" status={(this.state.product_before === this.state.receive_order_id && this.state.receive_order_id) ? 'show' : ''} message={trans('messages.product_not_exists')} />
                <div className="row">
                    <div className="col-md-8">
                        <SELECT
                            name="mall_id"
                            key="mall_id"
                            labelTitle={trans('messages.mall_name')}
                            className="form-control input-sm"
                            fieldGroupClass="col-md-4"
                            labelClass="col-md-3"
                            options={dataSource.dataMall}
                            onChange={this.handleChange}
                            defaultValue={this.state.mall_id || '_none'}
                            errorPopup={true}
                            readOnly={(this.state.mall_id && dataSource.data !== null) ? true : false}
                            hasError={(dataResponse.message && dataResponse.message.mall_id)?true:false}
                            errorMessage={(dataResponse.message&&dataResponse.message.mall_id)?dataResponse.message.mall_id:""}
                            />
                        <INPUT
                            name="receive_order_id"
                            labelTitle={trans('messages.receive_order_id')}
                            className="form-control input-sm"
                            fieldGroupClass="col-md-6"
                            labelClass="col-md-3"
                            onChange={this.handleChange}
                            defaultValue={this.state.receive_order_id || ''}
                            errorPopup={true}
                            readOnly={(this.state.mall_id && dataSource.data !== null) ? true : false}
                            hasError={(dataResponse.message && dataResponse.message.receive_order_id)?true:false}
                            errorMessage={(dataResponse.message&&dataResponse.message.receive_order_id)?dataResponse.message.receive_order_id:""}/>
                        <TEXTAREA
                            name="note"
                            labelTitle={trans('messages.note')}
                            className="form-control input-sm"
                            fieldGroupClass="col-md-9"
                            rows="8"
                            labelClass="col-md-3"
                            onChange={this.handleChange}
                            defaultValue={this.state.note || ''}
                            errorPopup={true}
                            hasError={(dataResponse.message && dataResponse.message.note)?true:false}
                            errorMessage={(dataResponse.message&&dataResponse.message.note)?dataResponse.message.note:""}
                            />
                        <LABEL
                            labelTitle={trans('messages.is_deleted')}
                            labelClass="col-md-3 col-xs-4"
                            fieldGroupClass="col-md-9  col-xs-8">
                            <CHECKBOX_CUSTOM
                                name="is_deleted"
                                defaultChecked={this.state.is_deleted === 1 ? true : false}
                                onChange={this.handleChange} />
                        </LABEL>
                    </div>
                </div>
                <div className="row">
                    <div className="col-md-5-5"></div>
                    <div className="col-md-2-5">
                        <div className="row">
                            <div className="col-md-6 col-xs-6">
                                <BUTTON name="btnAccept" id="btnSubmit" className="btn btn-success input-sm btn-larger" type="button" onClick={this.handleSubmit}>{trans('messages.btn_accept')}</BUTTON>
                            </div>
                            <div className="col-md-6 col-xs-6">
                                <BUTTON className="btn bg-purple input-sm pull-right btn-larger" type="button" onClick={this.handleCancel}>{trans('messages.btn_cancel')}</BUTTON>
                            </div>
                        </div>
                    </div>
                </div>
                </div>
            </FORM>
            <MESSAGE_MODAL
                message={this.state.message || ''}
                title={trans('messages.deny_edit')}
                baseUrl={baseUrl}
            />
            </div>
        </div>
    </div>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        dataSource: state.commonReducer.dataSource,
        dataResponse: state.commonReducer.dataResponse,
        dataRealTime: state.commonReducer.dataRealTime
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        reloadData: (url, params) => { dispatch(Action.detailData(url, params)) },
        postData: (url, params, headers) => { dispatch(Action.postData(url, params, headers)) },
        postRealTime: (url, params, headers) => { dispatch(Action.postRealTime(url, params, headers)) }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(CancelReservationSave)