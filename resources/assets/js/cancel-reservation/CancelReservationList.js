import React, { Component } from 'react';

import { connect } from 'react-redux';
import * as Action from '../common/actions';
import * as queryString from 'query-string';

import BUTTON from '../common/components/form/BUTTON';
import FORM from '../common/components/form/FORM';
import INPUT from '../common/components/form/INPUT';
import DATEPICKER from '../common/components/form/DATEPICKER';

import TABLE from '../common/components/table/TABLE';
import THEAD from '../common/components/table/THEAD';
import TBODY from '../common/components/table/TBODY';
import TR from '../common/components/table/TR';
import TH from '../common/components/table/TH';
import TD from '../common/components/table/TD';
import SELECT from '../common/components/form/SELECT';
import LABEL from '../common/components/form/LABEL';
import CHECKBOX_CUSTOM from '../common/components/form/CHECKBOX_CUSTOM';
import moment from 'moment';

import SortComponent from '../common/components/table/SortComponent';
import PaginationContainer from '../common/containers/PaginationContainer'
import Loading from '../common/components/common/Loading';

class CancelReservationList extends Component {

    constructor(props) {
        super(props);
        const stringParams = queryString.parse(props.location.search);
        this.state = {};
        if (!_.isEmpty(stringParams)) {
            this.state = stringParams;
        }
        this.props.reloadData(dataListUrl, this.state);
    }

    handleChecked = (event,item) => {
        let headers = {'X-Requested-With': 'XMLHttpRequest'};
        const target = event.target;
        const value = target.type === 'checkbox' ? target.checked : target.value;
        const name = target.name;
        item['is_deleted'] = value;

        this.props.postData(
            changeFlagUrl,
            item,
            headers,
            this.handleReloadData()
        );
    }

    handleChange = (event,item) => {
        const target = event.target;
        const value = target.type === 'checkbox' ? target.checked : target.value;
        const name = target.name;
        this.setState({
            [name]: (value)?value:undefined
        },() => {
            if (name === 'per_page' ||
                name === 'status' ||
                name === 'mall_id' ) {
                this.handleReloadData();
            }
        });
    }

    handleClearClick = (event) => {
        Object.keys(this.state).map((key) => {
            if (key !== 'per_page') {
                delete this.state[key];
            }
        });
        this.handleReloadData();
    }

    handleReloadData = () => {
        if (this.state.page) {
            delete this.state.page;
        }
        this.props.reloadData(dataListUrl, this.state, true);
    }

    componentDidUpdate() {
        if (!_.isEmpty(this.props.dataSource)) {
            $('.loading').hide();
        }
    }

    componentWillMount() {
        window.onpopstate = (event) => {
            location.reload();
        };
    }

    handleSortClick = (name, nextSort) => {
        Object.keys(this.state).map((key) => {
            if (key.startsWith('sort_')) {
                delete this.state[key];
            }
        });
        if (nextSort === 'none') {
            delete this.state[name];
            this.props.reloadData(dataListUrl, this.state, true);
        } else {
            this.setState({[name]: nextSort}, () => {
                this.props.reloadData(dataListUrl, this.state, true);
            });
        }

    }

    handleClickPage = (page) => {
        this.setState({
            page: page
        },() => {
            this.props.reloadData(dataListUrl, this.state, true);
        });
    }
    handleChangeMultiSelect = (name, value, checked) => {
        let currentValue = this.state[name];
        if (typeof currentValue === 'undefined') {
            currentValue = [];
        } else if (!Array.isArray(this.state[name])) {
            currentValue = [this.state[name]];
        }
        if (Array.isArray(value)) {
            currentValue = [...value];
        } else {
            let index = currentValue.indexOf(value);
            if (checked === false) {
                if (index !== -1) {
                    currentValue.splice(index, 1);
                }
            } else {
                if (index === -1) {
                    currentValue.push(value);
                }
            }
        }
        this.setState({
            [name]: currentValue,
        });
    }
    render() {
        let dataSource = this.props.dataSource;
        let index = 0;
        let totalItems = 0;
        let itemsPerPage = 20;
        let mallOptions = {};
        if (!_.isEmpty(dataSource)) {
            index = dataSource.data.from - 1;
            totalItems   = dataSource.data.total;
            itemsPerPage = dataSource.data.per_page;
        }
        return ( (!_.isEmpty(dataSource)) &&
            <div>
                <Loading className="loading" />
                <div className="box">
                    <div className="box-header">
                        <div className="row">
                                <div className="col-md-12">
                                    <h4>{trans('messages.search_title')}</h4>
                                </div>
                                <FORM>
                                    <SELECT
                                        id="mall_id"
                                        name="mall_id"
                                        groupClass="form-group col-md-1 per-page "
                                        labelClass="pull-left"
                                        labelTitle={trans('messages.mall_name')}
                                        handleChangeMultiSelect={this.handleChangeMultiSelect}
                                        multiple="multiple"
                                        valueMulti={this.state.mall_id||''}
                                        options={dataSource.dataMall}
                                        className="form-control input-sm" />
                                    <SELECT
                                        id="status"
                                        name="status"
                                        groupClass="form-group col-md-1 per-page "
                                        labelClass="pull-left"
                                        labelTitle={trans('messages.return_status')}
                                        handleChangeMultiSelect={this.handleChangeMultiSelect}
                                        valueMulti={this.state.status||''}
                                        options={{0:'未',1:'済'}}
                                        multiple="multiple"
                                        className="form-control input-sm" />
                                    <SELECT
                                        name="per_page"
                                        groupClass="form-group col-md-1 per-page pull-right-md"
                                        labelClass="pull-left"
                                        labelTitle={trans('messages.per_page')}
                                        onChange={this.handleChange}
                                        value={this.state.per_page||''}
                                        options={{20:20, 40:40, 60:60, 80:80, 100:100}}
                                        className="form-control input-sm" />
                                </FORM>
                        </div>
                        <div className="row">
                            <div className="col-md-9 col-xs-4">
                                <a href={saveUrl} className="btn btn-success input-sm">{trans('messages.btn_add')}</a>
                            </div>
                            <div className="col-md-3 col-xs-8">
                                    <BUTTON className="btn btn-danger pull-right" onClick={this.handleClearClick}>{trans('messages.btn_reset')}</BUTTON>
                                    <BUTTON className="btn btn-primary pull-right margin-element" onClick={this.handleReloadData} >{trans('messages.btn_search')}</BUTTON>
                            </div>
                        </div>
                    </div>
                    <div className="box-body">
                        <div className="row">
                            <div className="col-md-12">
                                {(totalItems/itemsPerPage > 1) &&
                                <div className="row">
                                    <div className="col-md-2-5"></div>
                                    <div className="col-md-7 text-center">
                                        <PaginationContainer handleClickPage={this.handleClickPage}/>
                                    </div>
                                    <div className="col-md-2-5 text-right line-height-md">
                                        {dataSource.data.from}件 - {dataSource.data.to}件 【全{dataSource.data.total}件】
                                    </div>
                                </div>
                                }
                                <div className="table-responsive">
                                <TABLE className="table table-striped table-custom">
                                    <THEAD>
                                        <TR>
                                            <TH className="visible-xs visible-sm"></TH>
                                            <TH className="hidden-xs hidden-sm col-max-min-20 text-center text-middle">
                                                {trans('messages.index')}
                                            </TH>
                                            <TH className="col-max-min-100 text-center text-middle">
                                                <SortComponent
                                                    name="sort_name_jp"
                                                    onClick={this.handleSortClick}
                                                    currentSort={(this.state.sort_name_jp)?this.state.sort_name_jp:'none'}
                                                    title={trans('messages.mall_name')} />
                                            </TH>
                                            <TH className="col-max-min-100 text-center text-middle" >
                                                <SortComponent
                                                    name="sort_receive_order_id"
                                                    onClick={this.handleSortClick}
                                                    currentSort={(this.state.sort_receive_order_id)?this.state.sort_receive_order_id:'none'}
                                                    title={trans('messages.receive_order_id')} />
                                            </TH>
                                            <TH className="hidden-xs hidden-sm col-max-min-60 text-center text-middle">
                                                <SortComponent
                                                    name="sort_status"
                                                    onClick={this.handleSortClick}
                                                    currentSort={(this.state.sort_status)?this.state.sort_status:'none'}
                                                    title={trans('messages.status')} />
                                            </TH>
                                            <TH className="text-center col-max-min-60" >
                                                <SortComponent
                                                name="sort_is_deleted"
                                                onClick={this.handleSortClick}
                                                currentSort={(this.state.sort_is_deleted)?this.state.sort_is_deleted:'none'}
                                                title={trans('messages.is_deleted')} />
                                            </TH>
                                            <TH className="hidden-xs hidden-sm text-center" >
                                                <SortComponent
                                                name="sort_status"
                                                onClick={this.handleSortClick}
                                                currentSort={(this.state.sort_note)?this.state.sort_note:'none'}
                                                title={trans('messages.note')} />
                                            </TH>
                                            <TH className="hidden-xs hidden-sm text-center" >
                                                <SortComponent
                                                name="sort_in_date"
                                                onClick={this.handleSortClick}
                                                currentSort={(this.state.sort_in_date)?this.state.sort_in_date:'none'}
                                                title={trans('messages.in_date')} />
                                            </TH>
                                            <TH className="hidden-xs hidden-sm text-center" >
                                                <SortComponent
                                                name="sort_up_date"
                                                onClick={this.handleSortClick}
                                                currentSort={(this.state.sort_up_date)?this.state.sort_up_date:'none'}
                                                title={trans('messages.up_date')} />
                                            </TH>
                                        </TR>

                                    </THEAD>
                                    <TBODY>
                                    {
                                        (!_.isEmpty(dataSource.data.data))?
                                        dataSource.data.data.map((item) => {
                                            index++;
                                            let classRow = (index%2 ===0) ? 'odd' : 'even';
                                            let status = item.status === 1 ? '済' : '未';
                                            let upDate = !_.isEmpty(item.up_date) ? moment.utc(item.up_date, "YYYY-MM-DD hh:mm:ss").format("YYYY/MM/DD") : '';
                                            let inDate = !_.isEmpty(item.in_date) ? moment.utc(item.in_date, "YYYY-MM-DD hh:mm:ss").format("YYYY/MM/DD") : '';
                                            return (
                                                [
                                                <TR key={"tr" + index} className={classRow}>
                                                    <TD className="visible-xs visible-sm">
                                                        <b className="show-info">
                                                            <i className="fa fa-angle-double-down" aria-hidden="true"></i>
                                                        </b>
                                                    </TD>
                                                    <TD className="hidden-xs hidden-sm text-center">
                                                       {index}
                                                    </TD>
                                                    <TD className="text-left" title={item.name_jp}>
                                                            <a className="link-detail" href={saveUrl + "?mall_id=" + item.mall_id + '&receive_order_id=' + item.receive_order_id}>
                                                                {item.name_jp}
                                                            </a>
                                                    </TD>
                                                    <TD className="text-left"
                                                        title={item.receive_order_id}>{item.receive_order_id}
                                                    </TD>
                                                    <TD className="hidden-xs hidden-sm text-center"
                                                        title={status}>{status}
                                                    </TD>
                                                    <TD className="text-center" title={item.is_deleted}>
                                                        <CHECKBOX_CUSTOM
                                                            key={"checkbox"+item.receive_order_id+item.mall_id+item.is_deleted}
                                                            name={"checkbox"+item.receive_order_id+item.mall_id}
                                                            defaultChecked={item.is_deleted === 1 ? true : false}
                                                            onChange={(e) => this.handleChecked(e,item)} />
                                                    </TD>
                                                    <TD className="hidden-xs hidden-sm text-left"
                                                        title={item.note}>{item.note}
                                                    </TD>
                                                    <TD className="hidden-xs hidden-sm text-center"
                                                        title={inDate}>{inDate}
                                                    </TD>
                                                    <TD className="hidden-xs hidden-sm text-center"
                                                        title={upDate}>{upDate}
                                                    </TD>
                                                </TR>,
                                                <TR key={"trhide" + index} className="hiden-info" style={{display:"none"}}>
                                                    <TD colSpan="10" className="width-span1">
                                                        <ul id={"temp" + item.mail_id} >
                                                            <li> <b>NO</b> : #{index}</li>
                                                            <li> <b>{trans('messages.mall_name')}</b> : {item.name_jp}</li>
                                                            <li> <b>{trans('messages.receive_order_id')}</b> : {item.received_order_id}</li>
                                                            <li> <b>{trans('messages.status')}</b> : {item.status}</li>
                                                            <li> <b>{trans('messages.is_deleted')}</b> : {item.is_deleted}</li>
                                                            <li> <b>{trans('messages.note')}</b> : {item.note}</li>
                                                            <li> <b>{trans('messages.in_date')}</b> : {item.in_date}</li>
                                                            <li> <b>{trans('messages.up_date')}</b> : {item.up_date}</li>
                                                        </ul>
                                                    </TD>
                                                </TR>
                                               ]
                                            )
                                        })
                                        :
                                        <TR><TD colSpan="17" className="text-center">{trans('messages.no_data')}</TD></TR>
                                    }
                                    </TBODY>
                                </TABLE>
                                </div>
                                <div className="text-center">
                                    <PaginationContainer handleClickPage={this.handleClickPage}/>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        dataSource: state.commonReducer.dataSource,
        dataResponse: state.commonReducer.dataResponse
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        reloadData: (url, params, pushHistory, ignoreFields) => { dispatch(Action.reloadData(url, params, pushHistory, ignoreFields)) },
        postData: (url, params, headers, callBackUrl, callBackParams) => { dispatch(Action.postData(url, params, headers, callBackUrl, callBackParams)) }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(CancelReservationList)