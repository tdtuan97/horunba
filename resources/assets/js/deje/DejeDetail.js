import React, { Component } from 'react';
import { connect } from 'react-redux';

import * as Action from '../common/actions';
import * as queryString from 'query-string';
import Loading from '../common/components/common/Loading';
import FORM from '../common/components/form/FORM';
import LABEL from '../common/components/form/LABEL';
import INPUT from '../common/components/form/INPUT';
import SELECT from '../common/components/form/SELECT';
import TEXTAREA from '../common/components/form/TEXTAREA';
import BUTTON from '../common/components/form/BUTTON';
import UPLOAD_CUSTOM from '../common/components/form/UPLOAD_CUSTOM';
import RADIO_CUSTOM from '../common/components/form/RADIO_CUSTOM';
import CHECKBOX_CUSTOM from '../common/components/form/CHECKBOX_CUSTOM';
import EDITOR from '../common/components/form/EDITOR';
import ExportPdfPopup from './containers/ExportPdfPopup';
import axios from 'axios';
import moment from 'moment';
class DejeDetail extends Component {

    constructor(props) {
        super(props);
        this.state = {status:'add'}
        let params = queryString.parse(props.location.search);
        if (params['deje_id']) {
            this.state.deje_id          = params['deje_id'];
            this.state.status           = 'check';
            this.state.comment_content  = '';
            this.state.first            = true;
            this.state.index            = params['deje_id'];
            if (params['show'] === 'comment') {
                this.state.show  = params['show'];
            }
        }
        this.props.reloadData(getDataDetail, this.state);
    }
    componentWillReceiveProps(nextProps) {
        let dataSource = nextProps.dataSource;
        let dataResponse = nextProps.dataResponse;
        if (!_.isEmpty(dataSource.data) && !_.isEmpty(dataSource.dataComment)) {
            this.setState({...dataSource.data, dataComment : dataSource.dataComment} ,() => {
                if (this.state.show !== null && !_.isEmpty(this.state.show)) {
                    $('html, body').animate({
                        scrollTop: $("#comment-go").offset().top
                    }, 1000);
                    $("textarea[name=comment_content]").focus();
                }
            });
        } else {
            this.setState({...dataSource.data, dataComment : null} ,() => {
                if (this.state.show !== null && !_.isEmpty(this.state.show)) {
                    $('html, body').animate({
                        scrollTop: $("#comment-go").offset().top
                    }, 1000);
                    $("textarea[name=comment_content]").focus();
                }
            });
        }

        if (_.isEmpty(dataSource.data)) {
            window.location.href = baseUrl;
        }
    }

    handleIssue = (event, index) => {
        $('#issue-modal').modal({backdrop: 'static', show: true});
    }

    handleCancel = (event) => {
        window.location.href = backUrl;
    }
    handleChange = (event) => {
        const target = event.target;
        const name = target.name;
        const value = target.type === 'checkbox' ? (target.checked?1:0) : target.value;
        this.setState({
            [name]: value
        });
    }
    checkFile (file, url) {
        let fileType = /[^.]+$/.exec(file);
        if (_.isEmpty(file)) {
            return "";
        }
        if (['gif', 'jpg', 'jpeg', 'tiff', 'png'].indexOf(_.lowerCase(fileType[0])) >= 0) {
            return <img src={url + file}  height="300" width="300" className="imgPop" />
        } else {
            return <a href={url + file} target="_blank">{file}</a>
        }
    }
    componentDidUpdate() {
        if (!_.isEmpty(this.props.dataSource)) {
            $('.loading').hide();
        }
        // Get the image and insert it inside the modal - use its "alt" text as a caption
        $(document).ready(function(){
            $(document).on("click", ".imgPop", function(){
                var img = "<img src="+ $(this).attr('src') +" style='width:100%;' />";
                $('#imageModal').modal('show');
                $('#imageModal').on('shown.bs.modal', function() {
                    $("#imageModal .modal-body").html(img);
                })
            })
        })
    }
    handleEditorChange = (name, content, event) => {
        this.setState({
            [name]: content,
        });
    }
    handleReloadData = () => {
        this.setState({comment_content:null}, () => {
            this.props.reloadData(getDataDetail, this.state);
            document.getElementsByName('btnAccept')[0].disabled = false;
            $('.loading').hide();
        });
    }
    handleSubmit = (event) => {
        let param = {deje_id :this.state.deje_id, comment_content : this.state.comment_content, action : 'add'};
        $(".loading").show();
        axios.post(this.props.location.pathname, param , {headers: {'X-Requested-With': 'XMLHttpRequest'}})
        .then(response => {
            if (response.data.status === 1) {
                this.setState({show : null, message : null} , () => {
                    this.handleReloadData();
                });
            }else if (response.data.status === 0) {
                this.setState({'show' : null, 'message' : response.data.message} , () => {
                    this.handleReloadData();
                });
            }
            $(".loading").hide();
        }).catch(error => {
            $(".loading").hide();
            throw(error);
        });
    }
    handleRemove = (event, item) => {
        let param = {comment_id : item.comment_id, action: 'remove'};
        $(".loading").show();
        axios.post(this.props.location.pathname, param , {headers: {'X-Requested-With': 'XMLHttpRequest'}})
        .then(response => {
            if (response.data.status === 1) {
                this.setState({'show' : null, message : null} , () => {
                    this.handleReloadData();
                });
            } else if (response.data.status === 0) {
                this.setState({'show' : null, 'message' : response.data.message} , () => {
                    this.handleReloadData();
                });
            }
            $(".loading").hide();
        }).catch(error => {
            $(".loading").hide();
            throw(error);
        });
    }

    handleEcuChecked = (event) => {
        const target = event.target;
        const name = target.name;
        const value = target.type === 'checkbox' ? (target.checked?1:0) : target.value;
        if (value) {
            this.setState({show_ecu : ''});
        } else {
            this.setState({show_ecu : 'none'});
        }
    }

    handleChangeAndSave = (event) => {
        const target = event.target;
        const name = target.name;
        const value = target.type === 'checkbox' ? (target.checked?1:0) : target.value;
        // let dataSet = {[name]: value};
        // this.setState(dataSet, () => {
            let headers = {'X-Requested-With': 'XMLHttpRequest'};
            let param = {name: name, value: value, deje_id: this.state.deje_id};
            this.props.postData(updateDataUrl, param, headers, this.messageSuccess);
        // });
    }

    messageSuccess = () => {
        this.props.reloadData(getDataDetail, this.state);
        $.notify({
            icon: 'glyphicon glyphicon-warning-sign',
            message: trans('messages.ajax_update_success'),
        },{
            type: 'success'
        });
    }

    handleClone = () => {
        $(".loading").show();
        axios.post(urlClone, {deje_id :this.state.deje_id} , {headers: {'X-Requested-With': 'XMLHttpRequest'}})
        .then(response => {
            if (response.data.status === 1) {
                $.notify({
                    icon: 'glyphicon glyphicon-warning-sign',
                    message: trans('messages.ajax_update_success'),
                },{
                    type: 'success'
                });
            }
            $(".loading").hide();
            window.location.href = response.data.url_redirect;
        }).catch(error => {
            $(".loading").hide();
            throw(error);
        });
    }

    render() {
        let dataSource        = this.props.dataSource;
        let dataResponse      = this.props.dataResponse;
        let cmbObjectType     = [];
        let cmbProcessStatus  = [];
        let cmbProcessMethod  = [];
        let dataTantou        = [];
        let dataTantouAll     = [];
        let dataBusinesssTyle = [];
        let fileUrl = [];
        let totalEcu = 0;
        if (!_.isEmpty(dataSource)) {
            cmbObjectType     = dataSource.cmbObjectType;
            cmbProcessStatus  = dataSource.cmbProcessStatus;
            cmbProcessMethod  = dataSource.cmbProcessMethod;
            dataTantou        = dataSource.dataTantou;
            dataTantouAll     = dataSource.dataTantouAll;
            dataBusinesssTyle = dataSource.dataBusinesssTyle;
            fileUrl = dataSource.fileUrl;
        }
        let token        = $("meta[name='csrf-token']").attr("content");
        return ((!_.isEmpty(dataSource)) &&
            <div>
            <div className="box box-primary">
            <FORM className="form-horizontal" encType={"multipart/form-data; boundary=----WebKitFormBoundary" + token}>
                <div className="box-body">
                    <Loading className="loading"></Loading>
                    <div className="row">
                        <div className="col-md-8">
                            <div className="row">
                                <div className="col-md-3">
                                    <INPUT
                                        name="deje_id"
                                        labelTitle={trans('messages.deje_no') + ":"}
                                        className="form-control input-sm"
                                        fieldGroupClass="col-md-10"
                                        labelClass="col-md-2 clear-padding-right"
                                        groupClass=""
                                        readOnly="true"
                                        disabled
                                        defaultValue={("000000000" + dataSource.data.deje_id).slice(-10)||""}
                                        />
                                </div>
                                <div className="col-md-4-5">
                                    <INPUT
                                        name="subject"
                                        labelTitle={trans('messages.lbl_in_date') + ":"}
                                        className="form-control input-sm"
                                        fieldGroupClass="col-md-8"
                                        labelClass="col-md-4 clear-padding-right"
                                        groupClass=""
                                        readOnly="true"
                                        disabled
                                        defaultValue={dataSource.data.in_date||""}
                                        />
                                </div>
                                <div className="col-md-4-5">
                                    <INPUT
                                        name="subject"
                                        labelTitle={trans('messages.lbl_up_date') + ":"}
                                        className="form-control input-sm"
                                        fieldGroupClass="col-md-8"
                                        labelClass="col-md-4 clear-padding-right"
                                        groupClass=""
                                        readOnly="true"
                                        disabled
                                        defaultValue={dataSource.data.up_date||""}
                                        />
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="row">
                        <div className="col-md-8 col-xs-12">
                            <INPUT
                                name="subject"
                                labelTitle={trans('messages.deje_title') + ":"}
                                className="form-control input-sm"
                                fieldGroupClass="col-md-10"
                                labelClass="col-md-2"
                                groupClass=""
                                readOnly="true"
                                disabled
                                value={dataSource.data.subject||""}
                                />
                        </div>
                    </div>
                    <div className="row">
                        <div className="col-md-4 col-xs-12">
                            <SELECT
                                name="object_type"
                                labelTitle={trans('messages.deje_object_type')}
                                className="form-control input-sm"
                                fieldGroupClass="col-md-8"
                                labelClass="col-md-4"
                                options={cmbObjectType}
                                value={this.state.object_type || '_none'}
                                readOnly="true"
                                disabled
                                onChange={(e) => this.handleChangeAndSave(e)}
                            />
                        </div>
                        <div className="col-md-4 col-xs-12">
                            <SELECT
                                name="in_ope_cd"
                                labelTitle={trans('messages.deje_in_ope_cd')}
                                className="form-control input-sm"
                                fieldGroupClass="col-md-8"
                                labelClass="col-md-4"
                                options={dataTantouAll}
                                readOnly="true"
                                disabled
                                value={this.state.in_ope_cd || '_none'}
                                onChange={(e) => this.handleChangeAndSave(e)}
                            />
                        </div>
                    </div>
                    <div className="row">
                        <div className="col-md-4 col-xs-12">
                            <SELECT
                                name="process_status"
                                labelTitle={trans('messages.deje_process_status')}
                                className="form-control input-sm"
                                fieldGroupClass="col-md-8"
                                labelClass="col-md-4"
                                options={cmbProcessStatus}
                                readOnly="true"
                                value={this.state.process_status || '_none'}
                                disabled
                                onChange={(e) => this.handleChangeAndSave(e)}
                            />
                        </div>
                        <div className="col-md-4 col-xs-12">
                            <SELECT
                                name="business_stype"
                                labelTitle={trans('messages.deje_business_stype')}
                                className="form-control input-sm"
                                fieldGroupClass="col-md-8"
                                labelClass="col-md-4"
                                options={dataBusinesssTyle}
                                readOnly="true"
                                disabled
                                value={this.state.business_stype || '_none'}
                                onChange={(e) => this.handleChangeAndSave(e)}
                            />
                        </div>
                    </div>
                    <div className="row">
                        <div className="col-md-4 col-xs-12">
                            <SELECT
                                name="dai_tantou"
                                labelTitle={trans('messages.deje_dai_tantou')}
                                className="form-control input-sm"
                                fieldGroupClass="col-md-8"
                                labelClass="col-md-4"
                                options={dataTantou}
                                readOnly="true"
                                disabled
                                value={this.state.dai_tantou || '_none'}
                                onChange={(e) => this.handleChangeAndSave(e)}
                            />
                        </div>
                        <div className="col-md-4 col-xs-12">
                            <SELECT
                                name="process_method"
                                labelTitle={trans('messages.deje_process_method')}
                                className="form-control input-sm"
                                fieldGroupClass="col-md-8"
                                labelClass="col-md-4"
                                options={cmbProcessMethod}
                                readOnly="true"
                                disabled
                                value={this.state.process_method || '_none'}
                                onChange={(e) => this.handleChangeAndSave(e)}
                            />
                        </div>
                    </div>
                    <div className="row">
                        <div className="col-md-8 col-xs-12">
                            <TEXTAREA
                                name="contents"
                                labelTitle={trans('messages.deje_contents')}
                                className="form-control"
                                fieldGroupClass="col-md-10"
                                labelClass="col-md-2"
                                groupClass=""
                                errorPopup={true}
                                value={this.state.contents || ''}
                                readOnly="true"
                                disabled
                                rows="20"
                            />
                        </div>

                        <div className="col-md-4 pointer-events-custom">
                            <table className="table box-ecu">
                            <thead>
                                <tr>
                                    <td colSpan="3">
                                    <label>
                                        <CHECKBOX_CUSTOM
                                            name="chk_request"
                                            defaultChecked={(!_.isEmpty(dataSource.dataEcu))?true:false}
                                            onChange={this.handleEcuChecked} />
                                            &nbsp;{trans('messages.deje_chk_request')}
                                    </label>
                                    </td>
                                </tr>
                            </thead>
                            {!_.isEmpty(dataSource.dataEcu) &&
                                <tbody style={{display: this.state.show_ecu}}>
                                    <tr>
                                        <td className="text-center">{trans('messages.deje_product_code')}</td>
                                        <td className="text-center">{trans('messages.deje_ecu_quantity')}</td>
                                        <td className="text-center">{trans('messages.deje_ecu_price')}</td>
                                    </tr>
                                    {
                                        dataSource.dataEcu.map((item) => {
                                            totalEcu += (item.quantity * item.price_komi);
                                            return (
                                                <tr key={"ecu" + item.line_num}>
                                                    <td>{item.product_code}</td>
                                                    <td className="text-right">{item.quantity}</td>
                                                    <td className="text-right">{item.price_komi}</td>
                                                </tr>
                                            )
                                    })}
                                    <tr>
                                        <td colSpan="3" className="text-center">{trans('messages.deje_ecu_price')}：{totalEcu}</td>
                                    </tr>
                                </tbody>
                            }
                            </table>
                        </div>
                    </div>
                    <div className="row">
                        <div className="col-md-8 col-xs-12">
                            <LABEL
                                name="attach_file_1"
                                labelTitle={trans('messages.deje_attached') + '.1'}
                                className="form-control input-sm"
                                fieldGroupClass="col-md-10"
                                labelClass="col-md-2"
                                groupClass="form-group"
                             >
                                <span>{this.checkFile(this.state.attach_file_1, fileUrl) || ''}</span>
                            </LABEL>
                        </div>
                        <div className="col-md-8 col-xs-12">
                            <LABEL
                                name="attach_file_2"
                                labelTitle={trans('messages.deje_attached') + '.2'}
                                className="form-control input-sm"
                                fieldGroupClass="col-md-10"
                                labelClass="col-md-2"
                                groupClass="form-group"
                             >
                                <span>{this.checkFile(this.state.attach_file_2, fileUrl) || ''}</span>
                            </LABEL>

                        </div>
                        <div className="col-md-8 col-xs-12">
                            <LABEL
                                name="attach_file_3"
                                labelTitle={trans('messages.deje_attached', fileUrl) + '.3'}
                                className="form-control input-sm"
                                fieldGroupClass="col-md-10"
                                labelClass="col-md-2"
                                groupClass="form-group"
                             >
                                <span>{this.checkFile(this.state.attach_file_3, fileUrl) || ''}</span>
                            </LABEL>
                        </div>
                        <div className="col-md-8 col-xs-12">
                            <LABEL
                                name="attach_file_4"
                                labelTitle={trans('messages.deje_attached') + '.4'}
                                className="form-control input-sm"
                                fieldGroupClass="col-md-10"
                                labelClass="col-md-2"
                                groupClass="form-group"
                             >
                                <span>{this.checkFile(this.state.attach_file_4, fileUrl) || ''}</span>
                            </LABEL>
                        </div>
                        <div className="col-md-8 col-xs-12">
                            <LABEL
                                name="attach_file_5"
                                labelTitle={trans('messages.deje_attached') + '.5'}
                                className="form-control input-sm"
                                fieldGroupClass="col-md-10"
                                labelClass="col-md-2"
                                groupClass="form-group"
                             >
                                <span>{this.checkFile(this.state.attach_file_5, fileUrl) || ''}</span>
                            </LABEL>
                        </div>
                        <div className="col-md-8 col-xs-12">
                            <LABEL
                                name="attach_file_6"
                                labelTitle={trans('messages.deje_attached') + '.6'}
                                className="form-control input-sm"
                                fieldGroupClass="col-md-10"
                                labelClass="col-md-2"
                                groupClass="form-group"
                             >
                                <span>{this.checkFile(this.state.attach_file_6, fileUrl) || ''}</span>
                            </LABEL>
                        </div>
                    </div>
                        {/*<div className="" >
                            <hr className="hr-full" />
                            <div className="form-group">
                                <div className="col-md-12"  id="comment-go">
                                    <div className="row">
                                        <label className="col-md-12">{trans('messages.deje_comment')}</label>
                                    </div>
                                </div>
                                <div className="col-md-8">
                                    <div className="row">
                                        <div className="col-md-6"><strong>{"☆" + dataSource.opeName}</strong></div>
                                        <div className="col-md-3">
                                            <label>
                                            <RADIO_CUSTOM
                                                name="editor_type"
                                                value="0"
                                                defaultChecked
                                                onChange={this.handleChange}
                                                 />
                                            <span>{" " + trans('messages.rat_text')}</span>

                                            </label>
                                        </div>
                                        <div className="col-md-3">
                                        <label>
                                            <RADIO_CUSTOM
                                                name="editor_type"
                                                value="1"
                                                onChange={this.handleChange}
                                                 />
                                            <span>{" " + trans('messages.rat_html')}</span>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div className="col-md-8">
                                    {
                                        (this.state.editor_type === '1') ?
                                            <EDITOR
                                                apiKey='sm3yi664b07edp6t6hf1trx3xy6ziv5xg41muurzssvy4773'
                                                init={{
                                                    force_br_newlines : true,
                                                    force_p_newlines : false,
                                                    forced_root_block : '',
                                                    branding: false,
                                                    menubar: '',
                                                    toolbar1: "undo redo | styleselect | forecolor backcolor | fontsizeselect | bold italic underline | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link unlink anchor | print preview code ",
                                                    plugins: "textcolor colorpicker paste lists autolink link",
                                                    preview_styles: 'font-size color',
                                                    fontsize_formats: '8pt 9pt 10pt 11pt 12pt 13pt 14pt 15pt 16pt 17pt 18pt 24pt 36pt 48pt 60pt 72pt 96pt',
                                                    height : 100,
                                                    paste_as_text: true
                                                }}
                                                name="comment_content"
                                                className="form-control input-sm"
                                                fieldGroupClass="col-md-12"
                                                groupClass="form-group"
                                                onEditorChange={(event) => this.handleEditorChange('comment_content', event)}
                                                value={this.state.comment_content || ''}
                                                errorPopup={true}
                                                hasError={(this.state.message && this.state.message.comment_content)?true:false}
                                                errorMessage={(this.state.message && this.state.message.comment_content)?this.state.message.comment_content:""}
                                            />
                                            :
                                            <TEXTAREA
                                                name="comment_content"
                                                className="form-control"
                                                fieldGroupClass="col-md-12"
                                                groupClass=""
                                                errorPopup={true}
                                                onChange={this.handleChange}
                                                value={this.state.comment_content || ''}
                                                rows="7"
                                                errorPopup={true}
                                                hasError={(this.state.message && this.state.message.comment_content)?true:false}
                                                errorMessage={(this.state.message && this.state.message.comment_content)?this.state.message.comment_content:""}
                                            />
                                    }
                                </div>
                                <div className="col-md-12">
                                    <div className="row">
                                           <div className="col-md-2">
                                            <BUTTON name="btnAccept" className="btn btn-primary input-sm pull-left  btn-small" type="button" onClick={this.handleSubmit}>{trans('messages.btn_add_cm')}</BUTTON>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>*/}
                    {
                        (!_.isEmpty(this.state.dataComment))?
                        <div className="row pointer-events-custom">
                            <div className="col-md-12" >
                                <hr className="hr-full" />
                                    {
                                        this.state.dataComment.map((item, index) => {
                                            return ([
                                                <div className="row">
                                                    <div className="col-md-1 text-center">
                                                        <span className="number"><strong>{index+1}</strong></span>
                                                    </div>
                                                    <div className="col-md-7">
                                                    <div className="row">

                                                            <div className="col-md-3">{"☆ " + item.full_name}</div>
                                                            <div className="col-md-5">{trans('messages.deje_cm_date') + " : "}{item.in_date}</div>
                                                            <div className="col-md-4">
                                                                <BUTTON name="btnRemove" className="btn btn-none input-sm pull-right btn-small" type="button" onClick={(e) => this.handleRemove(e,item)}>
                                                                    <i className="fa fa-times-circle fa-2" aria-hidden="true"></i>
                                                                    {trans('messages.btn_cm_remove')}
                                                                </BUTTON>
                                                            </div>
                                                        </div>
                                                        <div className="form-group">
                                                            <div className="col-md-12 ">
                                                                <div className="mail-content">
                                                                    <div dangerouslySetInnerHTML={{__html: item.contents.replace(/\r?\n/g, '<br />')}} />
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            ])
                                        })
                                    }
                            </div>
                        </div> : ''
                    }

                    <div className="row">
                        <div className="col-md-8">
                            <BUTTON className="btn bg-purple input-sm pull-right btn-larger" type="button" onClick={this.handleCancel}>{trans('messages.btn_cancel_deje')}</BUTTON>
                            <BUTTON name="btnAccept" className="btn btn-success input-sm pull-right margin-element" type="button" onClick={this.handleClone}>{trans('messages.deje_clone')}</BUTTON>
                        </div>
                    </div>

                </div>
            </FORM>
            </div>
            <div id="imageModal" className="modal fade" tabIndex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                <div className="modal-dialog">
                   <div className="modal-content">
                       <div className="modal-body">
                       </div>
                   </div>
                 </div>
            </div>
            <ExportPdfPopup deje_id={this.state.deje_id} />
            </div>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        dataSource: state.commonReducer.dataSource,
        dataResponse: state.commonReducer.dataResponse
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        reloadData: (url, params) => { dispatch(Action.detailData(url, params)) },
        postData: (url, params, headers,callback,callParam) => { dispatch(Action.postData(url, params, headers,callback,callParam)) },
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(DejeDetail)