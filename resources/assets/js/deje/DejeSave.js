import React, { Component } from 'react';
import { connect } from 'react-redux';

import * as Action from '../common/actions';
import * as queryString from 'query-string';
import Loading from '../common/components/common/Loading';
import FORM from '../common/components/form/FORM';
import INPUT from '../common/components/form/INPUT';
import SELECT from '../common/components/form/SELECT';
import TEXTAREA from '../common/components/form/TEXTAREA';
import BUTTON from '../common/components/form/BUTTON';
import UPLOAD_CUSTOM from '../common/components/form/UPLOAD_CUSTOM';
import CHECKBOX_CUSTOM from '../common/components/form/CHECKBOX_CUSTOM';
import RADIO_CUSTOM from '../common/components/form/RADIO_CUSTOM';
import MESSAGE_MODAL from '../common/containers/MessageModal';
import ExportPdfPopup from './containers/ExportPdfPopup';
import AcceptCommnetPopup from './containers/AcceptCommnetPopup';
import EDITOR from '../common/components/form/EDITOR';
import Dropzone from 'react-dropzone';
import axios from 'axios';

let timer;
class DejeSave extends Component {

    constructor(props) {
        super(props);
        this.state = {status:'add'}
        let params = queryString.parse(props.location.search);
        if (params['deje_id']) {
            this.state.deje_id = params['deje_id'];
            this.state.status  = 'edit';
            this.first         = true;
            this.state.index   = params['deje_id'];
        }
        if (params['deje_clone_id']) {
            this.state.deje_clone_id = params['deje_clone_id'];
            this.state.status  = 'add';
            this.first         = true;
            this.state.index   = params['deje_clone_id'];
        }
        this.state.submit   = false;
        this.state.show_ecu = 'none';
        this.state.dataEcu  = [];
        this.props.reloadData(getDataDetail, this.state);
    }
    hanleRealtime() {
        if (this.state.deje_id) {
            let params = {
                accessFlg : this.state.accessFlg,
                query : {
                        page_key : 'DEJE-SAVE',
                        primary_key : 'index:' + this.state.deje_id
                    },
                key :this.state.deje_id
            };
            if (this.state.accessFlg !== false) {
                this.props.postRealTime(checkEditUrl, params, {'X-Requested-With': 'XMLHttpRequest'});
            }
        }
    }
    checkFile (file, url) {
        let fileType = /[^.]+$/.exec(file);
        if (_.isEmpty(file)) {
            return "";
        }
        if (['gif', 'jpg', 'jpeg', 'tiff', 'png'].indexOf(_.lowerCase(fileType[0])) >= 0) {
            return (<a href={url + file} data-toggle="lightbox" data-gallery="example-gallery">
                        <img src={url + file}  height="300" width="300" className="imgPop img-fluid" />
                    </a>)
        } else {
            return <a href={url + file} target="_blank">{file}</a>
        }
    }
    componentWillReceiveProps(nextProps) {
        let dataSource = nextProps.dataSource;
        let dataResponse = nextProps.dataResponse;
        if (!_.isEmpty(dataSource.data) && _.isEmpty(nextProps.dataRealTime) && this.first) {
            let showEcu = 'none';
            if (dataSource.dataEcu.length > 0) {
                showEcu = '';
            }
            this.setState({...dataSource.data, dataComment: dataSource.dataComment, process:dataSource.process, dataEcu: _.cloneDeep(dataSource.dataEcu), show_ecu: showEcu}, () => {
                this.hanleRealtime();
            });
            this.first = false;
        }
         if (_.isEmpty(dataSource.data) && this.first) {
             window.location.href = baseUrl;
         }
        if (!_.isEmpty(dataResponse.method) && dataResponse.method === 'save') {
            if (dataResponse.status === 1) {
                $.notify({
                    icon: 'glyphicon glyphicon-warning-sign',
                    message: dataResponse.message,
                },{
                    type: 'success'
                });
                window.location.href = decodeURI(backUrl);
            } else if(dataResponse.status === 0) {
                $.notify({
                    icon: 'glyphicon glyphicon-warning-sign',
                    message: dataResponse.message,
                },{
                    type: 'danger'
                });
                document.getElementsByName('btnAccept')[0].disabled = false;
            } else {
                document.getElementsByName('btnAccept')[0].disabled = false;
            }
        }
        // // Check realtime update
        if (!_.isEmpty(nextProps.dataRealTime)) {
            let {
                message,
                ...other
            } = nextProps.dataRealTime;
            this.setState({...other, message_realtime: message});
        }

    }

    handleChange = (event) => {
        const target = event.target;
        const name = target.name;
        const value = target.type === 'checkbox' ? (target.checked?1:0) : target.value;
        if (target.type === 'file') {
            let tmpName = name.replace(new RegExp("^browser_"), '');
            let dataSet = {
                [tmpName]: target.files[0].name ? target.files[0].name : '',
            };
            if (target.files[0].size <= 20971520) {
                dataSet['input_' + tmpName] = target.files[0];
            } else {
                dataSet['input_' + tmpName] = 'error';
            }
            this.setState(dataSet);
        } else {
            let dataSet = {[name]: value};
            this.setState(dataSet);
        }
    }

    handlePrint = (event) => {
        window.open(printDejeUrl + "?deje_id=" + this.state.deje_id , '_blank');
    }

    handleCancel = (event) => {
        window.location.href = decodeURI(backUrl);
    }

    handleSubmit = (event) => {
        let headers = {'X-Requested-With': 'XMLHttpRequest'};
        if (this.props.enctype) {
            headers['Content-Type'] = this.props.enctype;
        }
        let formData   = new FormData();
        if (!_.isEmpty(this.state)) {
            Object.keys(this.state).map((item) => {
                if (item.includes('input_attach_file')) {
                    formData.append(item, this.state[item]);
                } else if (item === 'dataEcu') {
                    formData.append(item, JSON.stringify(this.state[item]));
                } else {
                    formData.append(item, _.trim(this.state[item]));
                }
            });
        }
        this.props.postData(saveUrl, formData, headers);
        this.setState({'message' : {}, 'submit' : true});
    }

    componentDidUpdate() {
        if (!_.isEmpty(this.props.dataSource)) {
            $('.loading').hide();
        }
        if (this.state.accessFlg === false) {
            $('#message-modal').modal('show');
        }
    }
    componentDidMount() {
         setInterval(() => {
            this.hanleRealtime()
        }, 5000);
        $(document).on('click', '[data-toggle="lightbox"]', function(event) {
            event.preventDefault();
            $(this).ekkoLightbox();
        });
    }
    handleParseContent = () => {
        this.setState({'message' : {}, 'submit' : false}, () => {
            let headers = {'X-Requested-With': 'XMLHttpRequest'};
            let param = this.state;
            param['action'] = name;
            axios.post(
                parseUrl,
                param,
                {headers : headers}
            ).then(response => {
                if (response.data.status === 1) {
                    let currentContent = !_.isEmpty(this.state.contents) ? this.state.contents + '\r\n' + response.data.contents : response.data.contents;
                    this.setState({contents: currentContent});
                } else {
                    // if (response.data.message) {
                    //     let error = {};
                    //     error.order_id = response.data.message.order_id ? response.data.message.order_id : '';
                    //     error.product_code = response.data.message.product_code ? response.data.message.product_code : '';
                    //     error.business_stype = response.data.message.business_stype ? response.data.message.business_stype : '';
                    //     this.setState({'message' : error, 'submit' : false}, () => {
                    //         $.notify({
                    //             icon: 'glyphicon glyphicon-warning-sign',
                    //             message: trans('messages.parse_content_error'),
                    //         },{
                    //             type: 'danger'
                    //         });
                    //     });
                    // } else {
                    //     $.notify({
                    //         icon: 'glyphicon glyphicon-warning-sign',
                    //         message: trans('messages.data_not_exists'),
                    //     },{
                    //         type: 'danger'
                    //     });
                    // }
                    let currentContent = typeof this.state.contents !== 'undefined' ? this.state.contents + response.data.contents : response.data.contents;
                    this.setState({contents: currentContent});
                }
            }).catch(error => {
                throw(error);
            });
        });
        let arrProductCode = [];
        arrProductCode[0] = this.state["product_code[0]"];
        arrProductCode[1] = this.state["product_code[1]"];
        arrProductCode[2] = this.state["product_code[2]"];
        this.getInfoProduct(null, arrProductCode);
    }
    handleEcuChecked = (event) => {
        const target = event.target;
        const name = target.name;
        const value = target.type === 'checkbox' ? (target.checked?1:0) : target.value;
        if (value) {
            this.setState({show_ecu : ''});
        } else {
            this.setState({show_ecu : 'none'});
        }
    }

    handleEcuData = (item, action, event) => {
        if (this.state.accessFlg === false) {
            return false;
        }
        let dataEcu = this.state.dataEcu;
        let curItem = null;
        let curIndex = null;
        if (item) {
            curItem = dataEcu.find(it => it.line_num === item.line_num);
            curIndex = dataEcu.findIndex(it => it.line_num === item.line_num);
        }
        let name = '';
        let value = '';
        switch (action) {
            case 'edit':
                if (curItem) {
                    curItem.is_edit = true;
                }
                break;
            case 'cancel':
                if (curItem) {
                    curItem.is_edit = false;
                }
                if (curItem.item_type === 'new') {
                    dataEcu = dataEcu.filter(it => it.line_num !== item.line_num);
                } else {
                    dataEcu[curIndex] = {...this.props.dataSource.dataEcu.find(it => it.line_num === item.line_num)};
                }
                break;
            case 'input':
                const target = event.target;
                name = target.name;
                value = target.type === 'checkbox' ? (target.checked?1:0) : target.value;
                curItem[name] = value;
                break;
            case 'delete':
                curItem['item_type'] = 'delete';
                break;
            case 'add':
                let lineNum = Math.max(...dataEcu.map(o => o.line_num), 0) + 1;
                let newItem = {
                    deje_id: this.state.deje_id,
                    line_num: lineNum,
                    product_code: '',
                    quantity: '',
                    price_komi: '',
                    is_edit: true,
                    item_type: 'new'
                }
                dataEcu.push(newItem);
                break;
        }
        this.setState({dataEcu: dataEcu}, () => {
            if (action === 'input' && name === 'product_code') {
                clearTimeout(timer);
                let _this = this;
                timer = setTimeout(function() {
                    _this.getInfoProduct(curItem, value);
                }, 1500);
            }
        });
    }

    handleEditorChange = (name, content, event) => {
        this.setState({
            [name]: content,
        });
    }

    getInfoProduct = (curItem, productCode) => {
        $(".loading").show();
        let headers = {'X-Requested-With': 'XMLHttpRequest'};
        let params = null;
        if (Array.isArray(productCode)) {
            params = {product_code : productCode};
        } else {
            params = {product_code : _.trim(productCode)};
        }

        axios.post(getProductUrl, params, {headers : headers}
        ).then(response => {
            $(".loading").hide();
            let data = response.data;
            let price = '';
            if (data) {
                let curDataEcu = [...this.state.dataEcu];
                if (curItem === null) {
                    curDataEcu = curDataEcu.filter(it => it.is_parse !== true);
                    let lineNum = Math.max(...curDataEcu.map(o => o.line_num), 0) + 1;
                    data.map((item) => {
                        let newItem = {
                            deje_id: item.deje_id,
                            line_num: lineNum++,
                            product_code: item.product_code,
                            quantity: '',
                            price_komi: Math.ceil(item.price_invoice * 1.08),
                            is_edit: true,
                            item_type: 'new',
                            is_parse: true
                        }
                        curDataEcu.push(newItem);
                    });
                } else {
                    curDataEcu.find(item => item.line_num === curItem.line_num).price_komi = Math.ceil(data.price_invoice * 1.08);
                }
                this.setState({
                    dataEcu: curDataEcu
                });
            }
        }).catch(error => {
            $(".loading").hide();
            throw(error);
        });
    }

    handleSubmitComment = (event) => {
        let param = {deje_id :this.state.deje_id, comment_content : this.state.comment_content, action : 'add'};
        $(".loading").show();
        axios.post(saveComtUrl, param , {headers: {'X-Requested-With': 'XMLHttpRequest'}})
        .then(response => {
            if (response.data.status === 1) {
                this.setState({show : null, message_comment : null, dataComment: response.data.dataComment, comment_content: ''} , () => {
                    // $.notify({
                    //     icon: 'glyphicon glyphicon-warning-sign',
                    //     message: dataResponse.message,
                    // },{
                    //     type: 'error'
                    // });
                });
            }else if (response.data.status === 0) {
                this.setState({'show' : null, 'message_comment' : response.data.message} , () => {
                    // this.handleReloadData();
                });
            }
            $(".loading").hide();
        }).catch(error => {
            $(".loading").hide();
            throw(error);
        });
    }
    handleIssue = (event, index) => {
        $('#issue-modal').modal({backdrop: 'static', show: true});
    }

    handleSubmitDeletePopup = (event) => {
        let param = {comment_id : this.state.deje_comment_delete, action: 'remove', deje_id :this.state.deje_id};
        $(".loading").show();
        axios.post(saveComtUrl, param , {headers: {'X-Requested-With': 'XMLHttpRequest'}})
        .then(response => {
            if (response.data.status === 1) {
                this.setState({'show' : null, message : null, dataComment: response.data.dataComment} , () => {
                    // $.notify({
                    //     icon: 'glyphicon glyphicon-warning-sign',
                    //     message: ,
                    // },{
                    //     type: 'error'
                    // });
                });
            } else if (response.data.status === 0) {
                this.setState({'show' : null, 'message' : response.data.message, dataComment: response.data.dataComment} , () => {
                    // this.handleReloadData();
                });
            }
            delete this.state.deje_comment_delete;
            $('#access-delete-comment-modal').modal('hide');
            $(".loading").hide();
        }).catch(error => {
            $(".loading").hide();
            throw(error);
        });
    }

    handleRemove = (event, item) => {
        this.setState({deje_comment_delete: item.comment_id}, () => {
            $('#access-delete-comment-modal').modal('show');
        });
    }

    handleCancelDeletePopup = (event, item) => {
        delete this.state.deje_comment_delete;
        $('#access-delete-comment-modal').modal('hide');

    }

    handleRemoveImage = (event, field) => {
        this.setState({[field] : null, [field + '_realname'] : null});
    }

    handleClone = () => {
        window.location.href = saveUrl + '?deje_clone_id=' + this.state.deje_id;
        // $(".loading").show();
        // axios.post(urlClone, {deje_id :this.state.deje_id} , {headers: {'X-Requested-With': 'XMLHttpRequest'}})
        // .then(response => {
        //     if (response.data.status === 1) {
        //         $.notify({
        //             icon: 'glyphicon glyphicon-warning-sign',
        //             message: trans('messages.ajax_update_success'),
        //         },{
        //             type: 'success'
        //         });
        //     }
        //     $(".loading").hide();
        //     window.location.href = response.data.url_redirect;
        // }).catch(error => {
        //     $(".loading").hide();
        //     throw(error);
        // });
    }

    handleAttached = (name, files) => {
        let dataSet = {
            [name]: files[0].name ? files[0].name : '',
            [name + '_realname']: files[0].name ? files[0].name : '',
        };
        if (files[0].size <= 20971520) {
            dataSet['input_' + name] = files[0];
        } else {
            dataSet['input_' + name] = 'error';
        }
        this.setState(dataSet);
    }

    handleExportClick = () => {
        let params = {};
        let arrParams = [];

        params['deje_id']    = this.state['deje_id'];
        params['order_id']    = this.state['order_id_export'];
        params['customer_name'] = this.state['customer_name'];

        arrParams.push("deje_id=" + this.state['deje_id']);
        arrParams.push("order_id=" + this.state['order_id_export']);
        arrParams.push("customer_name=" + this.state['customer_name']);

        $(".loading").show();
        axios.post(exportIssueUrl, params, {headers: {'X-Requested-With': 'XMLHttpRequest'}}
        ).then(response => {
            if (response.data.status === 0) {
                let messages = response.data.messages;
                this.setState({
                    order_id_error: (messages.order_id)?messages.order_id:'',
                    customer_name_error: (messages.customer_name)?messages.customer_name:''
                });
            } else {
                this.setState({
                    order_id_error: '',
                    customer_name_error: ''
                });
                window.open(exportIssueUrl + "?" + arrParams.join('&') , '_blank');
                this.handleCancelClick();
            }
            $(".loading").hide();
        }).catch(error => {
            $(".loading").hide();
            throw(error);
        });
    }

    render() {
        let dataSource        = this.props.dataSource;
        let dataResponse      = this.props.dataResponse;
        let cmbObjectType     = [];
        let cmbProcessStatus  = [];
        let cmbProcessMethod  = [];
        let dataTantou        = [];
        let dataBusinesssTyle = [];
        let totalEcu          = 0;
        let fileUrl = [];
        if (!_.isEmpty(dataSource)) {
            cmbObjectType     = dataSource.cmbObjectType;
            cmbProcessStatus  = dataSource.cmbProcessStatus;
            cmbProcessMethod  = dataSource.cmbProcessMethod;
            dataTantou        = dataSource.dataTantou;
            dataBusinesssTyle = dataSource.dataBusinesssTyle;
            fileUrl = dataSource.fileUrl;
        }
        let length = 0;
        if (this.state.dataComment) {
            let comment = this.state.dataComment
            length = comment.length + 1;
        }
        let messageBusinessStype = '';
        let messageProductCode = '';
        let messageOrderId = '';
        if (this.state.message && this.state.message.business_stype) {
            messageBusinessStype = this.state.message.business_stype;
        } else if (dataResponse.message && dataResponse.message.business_stype && this.state.submit) {
            messageBusinessStype = dataResponse.message.business_stype;
        }
        if (this.state.message && this.state.message.product_code) {
            messageProductCode = this.state.message.product_code;
        } else if (dataResponse.message && dataResponse.message.product_code && this.state.submit) {
            messageProductCode = dataResponse.message.product_code;
        }
        if (this.state.message && this.state.message.order_id) {
            messageOrderId = this.state.message.order_id;
        } else if (dataResponse.message && dataResponse.message.order_id && this.state.submit) {
            messageOrderId = dataResponse.message.order_id;
        }
        let token        = $("meta[name='csrf-token']").attr("content");
        let ecuError = null;
        if (dataResponse.message && dataResponse.message.ecuError) {
            ecuError = dataResponse.message.ecuError;
        }

        return ((!_.isEmpty(dataSource)) &&
            <div>
            <div className="box box-primary">
            <FORM className="form-horizontal" encType={"multipart/form-data; boundary=----WebKitFormBoundary" + token}
>
                <div className="box-body">
                    <Loading className="loading"></Loading>
                    <div className="row deje-block-button">
                        <div className="col-md-4 deje-button">
                            {
                                this.state.process === 'edit' &&
                                <BUTTON name="btnAccept" className="btn btn-success input-sm pull-left margin-element margin-form btn-inverse-custom" type="button" onClick={this.handleClone}>{trans('messages.deje_clone')}</BUTTON>
                            }
                            <BUTTON name="btnAccept" className="btn btn-success input-sm btn-larger pull-left margin-element margin-form" type="button" onClick={this.handleSubmit}>{trans('messages.btn_change_deje')}</BUTTON>
                            <BUTTON id="btn-back-custom" className="btn bg-purple input-sm pull-left btn-larger margin-element margin-form" type="button" onClick={this.handleCancel}>{trans('messages.btn_cancel_deje')}</BUTTON>
                            <BUTTON className="btn bg-primary input-sm pull-left btn-larger margin-form" type="button" onClick={this.handlePrint}>{trans('messages.btn_print_deje')}</BUTTON>
                        </div>
                    </div>
                    <div className="row deje-margin">
                        { this.state.process === 'edit' && <div className="col-md-8 col-xs-12">
                        <div className="form-group">
                            <INPUT
                                name="deje_id"
                                labelTitle={trans('messages.deje_no') + ":"}
                                className="form-control input-sm"
                                fieldGroupClass="col-md-2"
                                labelClass="col-md-2"
                                groupClass=" "
                                defaultValue={this.state.deje_id||""}
                                readOnly="true"
                                disabled
                                />
                            <INPUT
                                name="in_date"
                                labelTitle={trans('messages.lbl_in_date') + ":"}
                                className="form-control input-sm"
                                fieldGroupClass="col-md-2-5 clear-padding-left"
                                labelClass="col-md-1-5 clear-padding-left"
                                groupClass=" "
                                defaultValue={this.state.in_date||""}
                                readOnly="true"
                                disabled
                                />
                            <INPUT
                                name="up_date"
                                labelTitle={trans('messages.lbl_up_date') + ":"}
                                className="form-control input-sm"
                                fieldGroupClass="col-md-2-5 clear-padding-left"
                                labelClass="col-md-1-5 clear-padding-left"
                                groupClass=" "
                                defaultValue={this.state.up_date||""}
                                readOnly="true"
                                disabled
                                />
                        </div>
                        </div>}
                    </div>
                    <div className="row">
                        { this.state.process === 'edit' && <div className="col-md-8 col-xs-12">
                        <div className="form-group">
                            <INPUT
                                name="in_ope_cd"
                                labelTitle={trans('messages.lbl_in_ope') + ":"}
                                className="form-control input-sm"
                                fieldGroupClass="col-md-2"
                                labelClass="col-md-2"
                                groupClass=" "
                                defaultValue={this.state.in_ope_cd||""}
                                readOnly="true"
                                disabled
                                />
                            <INPUT
                                name="up_ope_cd"
                                labelTitle={trans('messages.memo_updater') + ":"}
                                className="form-control input-sm"
                                fieldGroupClass="col-md-2-5 clear-padding-left"
                                labelClass="col-md-1-5 clear-padding-left"
                                groupClass=" "
                                defaultValue={this.state.up_ope_cd||""}
                                readOnly="true"
                                disabled
                                />
                        </div>
                        </div>}
                    </div>
                    <div className="row">
                        <div className="col-md-8 col-xs-12">
                            <INPUT
                                name="subject"
                                labelTitle={trans('messages.deje_title') + ':'}
                                className="form-control input-sm"
                                fieldGroupClass="col-md-10"
                                labelClass="col-md-2"
                                groupClass=""
                                errorPopup={true}
                                onChange={this.handleChange}
                                defaultValue={this.state.subject||""}
                                hasError={dataResponse.message&&dataResponse.message.subject?true:false}
                                errorMessage={dataResponse.message&&dataResponse.message.subject?dataResponse.message.subject:""}
                                />
                        </div>
                    </div>
                    <div className="row">
                        <div className="col-md-4 col-xs-12">
                            <SELECT
                                name="object_type"
                                labelTitle={trans('messages.deje_object_type')}
                                className="form-control input-sm"
                                fieldGroupClass="col-md-8"
                                labelClass="col-md-4"
                                options={cmbObjectType}
                                onChange={this.handleChange}
                                defaultValue={this.state.object_type || '_none'}
                                errorPopup={true}
                                hasError={(dataResponse.message && dataResponse.message.object_type)?true:false}
                                errorMessage={(dataResponse.message&&dataResponse.message.object_type)?dataResponse.message.object_type:""}
                            />
                        </div>
                    </div>
                    <div className="row">
                        <div className="col-md-4 col-xs-12">
                            <SELECT
                                name="process_status"
                                labelTitle={trans('messages.deje_process_status')}
                                className="form-control input-sm"
                                fieldGroupClass="col-md-8"
                                labelClass="col-md-4"
                                options={cmbProcessStatus}
                                onChange={this.handleChange}
                                defaultValue={this.state.process_status || '_none'}
                                errorPopup={true}
                                isRequire={true}
                                hasError={(dataResponse.message && dataResponse.message.process_status)?true:false}
                                errorMessage={(dataResponse.message&&dataResponse.message.process_status)?dataResponse.message.process_status:""}
                            />
                        </div>
                        <div className="col-md-4 col-xs-12">
                            <SELECT
                                name="business_stype"
                                labelTitle={trans('messages.deje_business_stype')}
                                className="form-control input-sm"
                                fieldGroupClass="col-md-8"
                                labelClass="col-md-4"
                                options={dataBusinesssTyle}
                                onChange={this.handleChange}
                                defaultValue={this.state.business_stype || '_none'}
                                errorPopup={true}
                                isRequire={true}
                                hasError={(messageBusinessStype)?true:false}
                                errorMessage={(messageBusinessStype)?messageBusinessStype:""}
                            />
                        </div>
                    </div>
                    <div className="row">
                        <div className="col-md-4 col-xs-12">
                            <SELECT
                                name="dai_tantou"
                                labelTitle={trans('messages.deje_dai_tantou')}
                                className="form-control input-sm"
                                fieldGroupClass="col-md-8"
                                labelClass="col-md-4"
                                options={dataTantou}
                                onChange={this.handleChange}
                                defaultValue={this.state.dai_tantou || '_none'}
                                errorPopup={true}
                                hasError={(dataResponse.message && dataResponse.message.dai_tantou)?true:false}
                                errorMessage={(dataResponse.message&&dataResponse.message.dai_tantou)?dataResponse.message.dai_tantou:""}
                            />
                        </div>
                        <div className="col-md-4 col-xs-12">
                            <SELECT
                                name="process_method"
                                labelTitle={trans('messages.deje_process_method')}
                                className="form-control input-sm"
                                fieldGroupClass="col-md-8"
                                labelClass="col-md-4"
                                options={cmbProcessMethod}
                                onChange={this.handleChange}
                                defaultValue={this.state.process_method || '_none'}
                                errorPopup={true}
                                hasError={(dataResponse.message && dataResponse.message.process_method)?true:false}
                                errorMessage={(dataResponse.message&&dataResponse.message.process_method)?dataResponse.message.process_method:""}
                            />
                        </div>
                    </div>
                    <div className="row">
                        <div className="col-md-8 col-xs-12">
                            <TEXTAREA
                                name="contents"
                                labelTitle={trans('messages.deje_contents')}
                                className="form-control"
                                fieldGroupClass="col-md-10"
                                labelClass="col-md-2"
                                groupClass=""
                                isRequire={true}
                                errorPopup={true}
                                value={this.state.contents || ''}
                                onChange={this.handleChange}
                                rows="25"
                                hasError={(dataResponse.message && dataResponse.message.contents)?true:false}
                                errorMessage={(dataResponse.message&&dataResponse.message.contents)?dataResponse.message.contents:""}
                            />
                        </div>
                        <div className="col-md-4 col-xs-12">
                            <INPUT
                                name="order_id[0]"
                                labelTitle={trans('messages.deje_order_id')}
                                className="form-control input-sm"
                                fieldGroupClass="col-md-12"
                                labelClass="col-md-12"
                                groupClass=""
                                errorPopup={true}
                                onChange={this.handleChange}
                                defaultValue={this.state.order_id||""}
                                hasError={messageOrderId?true:false}
                                errorMessage={messageOrderId?messageOrderId:""}
                            />
                            <INPUT
                                name="order_id[1]"
                                labelTitle=""
                                className="form-control input-sm"
                                fieldGroupClass="col-md-12"
                                labelClass=" "
                                groupClass=""
                                errorPopup={true}
                                onChange={this.handleChange}
                                defaultValue={this.state.order_id||""}
                                hasError={messageOrderId?true:false}
                                errorMessage={messageOrderId?messageOrderId:""}
                            />
                            <INPUT
                                name="order_id[2]"
                                labelTitle=""
                                className="form-control input-sm"
                                fieldGroupClass="col-md-12"
                                labelClass=" "
                                groupClass=""
                                errorPopup={true}
                                onChange={this.handleChange}
                                defaultValue={this.state.order_id||""}
                                hasError={messageOrderId?true:false}
                                errorMessage={messageOrderId?messageOrderId:""}
                            />

                            <INPUT
                                name="product_code[0]"
                                labelTitle={trans('messages.deje_product_code')}
                                className="form-control input-sm"
                                fieldGroupClass="col-md-12"
                                labelClass="col-md-12"
                                groupClass=""
                                errorPopup={true}
                                onChange={this.handleChange}
                                defaultValue={this.state.product_code||""}
                                hasError={messageProductCode?true:false}
                                errorMessage={messageProductCode?messageProductCode:""}
                            />
                            <INPUT
                                name="product_code[1]"
                                labelTitle=""
                                className="form-control input-sm"
                                fieldGroupClass="col-md-12"
                                labelClass=" "
                                groupClass=""
                                errorPopup={true}
                                onChange={this.handleChange}
                                defaultValue={this.state.product_code||""}
                                hasError={messageProductCode?true:false}
                                errorMessage={messageProductCode?messageProductCode:""}
                            />
                            <INPUT
                                name="product_code[2]"
                                labelTitle=""
                                className="form-control input-sm"
                                fieldGroupClass="col-md-12"
                                labelClass=" "
                                groupClass=""
                                errorPopup={true}
                                onChange={this.handleChange}
                                defaultValue={this.state.product_code||""}
                                hasError={messageProductCode?true:false}
                                errorMessage={messageProductCode?messageProductCode:""}
                            />
                            <span className="input-group-btn">
                                <BUTTON
                                    onClick={() => this.handleParseContent()}
                                    type="button"
                                    className="btn btn-info btn-flat input-sm btn-larger">
                                    {trans('messages.btn_parse_content')}
                                </BUTTON>
                            </span>

                            <table className="table box-ecu">
                            <thead>
                                <tr>
                                    <td colSpan="5">
                                    <label>
                                        <CHECKBOX_CUSTOM
                                            name="chk_request"
                                            defaultChecked={(!_.isEmpty(dataSource.dataEcu))?true:false}
                                            onChange={this.handleEcuChecked} />
                                            &nbsp;{trans('messages.deje_chk_request')}
                                    </label>
                                    </td>
                                </tr>
                            </thead>
                            <tbody style={{display: this.state.show_ecu}}>
                                <tr>
                                    <td colSpan="5">
                                        <INPUT
                                            name="order_id_export"
                                            labelTitle={trans('messages.deje_issue_order_id') + "："}
                                            className="form-control input-sm"
                                            fieldGroupClass="col-md-9"
                                            labelClass="col-md-3"
                                            groupClass=""
                                            errorPopup={true}
                                            value={this.state.order_id_export}
                                            onChange={this.handleChange}
                                            hasError={(this.state.order_id_error)?true:false}
                                            errorMessage={(this.state.order_id_error)?this.state.order_id_error:""}
                                            />

                                    </td>
                                </tr>
                                <tr>
                                    <td colSpan="5">
                                        <INPUT
                                            name="customer_name"
                                            labelTitle={trans('messages.deje_issue_customer_name') + "："}
                                            className="form-control input-sm"
                                            fieldGroupClass="col-md-9"
                                            labelClass="col-md-3"
                                            groupClass=""
                                            errorPopup={true}
                                            value={this.state.customer_name}
                                            onChange={this.handleChange}
                                            hasError={(this.state.customer_name_error)?true:false}
                                            errorMessage={(this.state.customer_name_error)?this.state.customer_name_error:""}
                                            />
                                    </td>
                                </tr>
                                <tr>
                                    <td className="text-center">{trans('messages.deje_product_code')}</td>
                                    <td className="text-center">{trans('messages.deje_ecu_quantity')}</td>
                                    <td className="text-center">{trans('messages.deje_ecu_price')}</td>
                                    <td colSpan="2"></td>
                                </tr>
                                {!_.isEmpty(this.state.dataEcu) &&
                                    this.state.dataEcu.filter(item => item.item_type !== 'delete').map((item) => {
                                        totalEcu += (item.quantity * item.price_komi);
                                        if (!item.is_edit) {
                                            return (
                                                <tr key={"ecu" + item.line_num}>
                                                    <td width="30%">{item.product_code}</td>
                                                    <td width="20%" className="text-right">{item.quantity}</td>
                                                    <td width="30%" className="text-right">{item.price_komi}</td>
                                                    <td width="10%" className="text-center">
                                                        <a href="javascript:void(0)" onClick={() => this.handleEcuData(item, 'edit')}>
                                                            <i className="fa fa-fw fa-pencil text-warning"></i>
                                                        </a>
                                                    </td>
                                                    <td width="10%" className="text-center">
                                                        <a href="javascript:void(0)" onClick={() => this.handleEcuData(item, 'delete')}>
                                                            <i className="fa fa-fw fa-times text-danger"></i>
                                                        </a>
                                                    </td>
                                                </tr>
                                                )
                                        } else {
                                            return (
                                                <tr key={"ecu" + item.line_num}>
                                                    <td width="30%">
                                                        <INPUT
                                                            name="product_code"
                                                            className="form-control input-sm"
                                                            fieldGroupClass=" "
                                                            labelClass=" "
                                                            groupClass=" "
                                                            errorPopup={true}
                                                            onChange={(event) => this.handleEcuData(item, 'input', event)}
                                                            value={item.product_code||""}
                                                            hasError={ecuError&&ecuError[item.line_num]&&ecuError[item.line_num].product_code?true:false}
                                                            errorMessage={ecuError&&ecuError[item.line_num]&&ecuError[item.line_num].product_code?ecuError[item.line_num].product_code:""}/>
                                                    </td>
                                                    <td width="20%">
                                                        <INPUT
                                                            name="quantity"
                                                            className="form-control input-sm"
                                                            fieldGroupClass=" "
                                                            labelClass=" "
                                                            groupClass=" "
                                                            errorPopup={true}
                                                            onChange={(event) => this.handleEcuData(item, 'input', event)}
                                                            value={(item.quantity||item.quantity==0)?item.quantity:""}
                                                            hasError={ecuError&&ecuError[item.line_num]&&ecuError[item.line_num].quantity?true:false}
                                                            errorMessage={ecuError&&ecuError[item.line_num]&&ecuError[item.line_num].quantity?ecuError[item.line_num].quantity:""}/>
                                                    </td>
                                                    <td width="30%">
                                                        <INPUT
                                                            name="price_komi"
                                                            className="form-control input-sm"
                                                            fieldGroupClass=" "
                                                            labelClass=" "
                                                            groupClass=" "
                                                            errorPopup={true}
                                                            onChange={(event) => this.handleEcuData(item, 'input', event)}
                                                            value={(item.price_komi||item.price_komi==0)?item.price_komi:""}
                                                            hasError={ecuError&&ecuError[item.line_num]&&ecuError[item.line_num].price_komi?true:false}
                                                            errorMessage={ecuError&&ecuError[item.line_num]&&ecuError[item.line_num].price_komi?ecuError[item.line_num].price_komi:""}/>
                                                    </td>
                                                    <td width="20%" className="text-center text-middle" colSpan="2">
                                                        <a href="javascript:void(0)" onClick={() => this.handleEcuData(item, 'cancel')}>
                                                            <i className="fa fa-fw fa-ban text-danger"></i>
                                                        </a>
                                                    </td>
                                                </tr>
                                            )
                                        }
                                })}
                                <tr>
                                    <td colSpan="3" className="text-center">{trans('messages.deje_ecu_price')}：{totalEcu}</td>
                                    <td colSpan="2" className="text-center">
                                        <a href="javascript:void(0)" onClick={() => this.handleEcuData(null, 'add')}>
                                            <i className="fa fa-fw fa-plus text-success"></i>
                                        </a>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                            {
                                this.state.process === 'edit' && <button type="button" className="btn btn-success" onClick={this.handleExportClick}>{trans('messages.btn_deje_issue')}</button>
                            }
                        </div>
                    </div>
                    <div className="row">
                        <div className="col-md-8 col-xs-12">
                            <div className="form-group">
                                <div>
                                    <label className="col-md-2">{trans('messages.deje_attached') + '.1'}</label>
                                    <div className="col-md-9">
                                        <div className="input-group input-group-sm">
                                            <Dropzone onDrop={(acceptedFiles) => {this.handleAttached('attach_file_1', acceptedFiles)}}>
                                              {({getRootProps, getInputProps}) => (
                                                <section className="container-dropzone">
                                                  <div {...getRootProps({className: 'dropzone', multiple: false})}>
                                                    <input {...getInputProps()} />
                                                    <p>{this.state.attach_file_1_realname ? this.state.attach_file_1_realname : trans('messages.upload_file_drag_drop')}</p>
                                                  </div>
                                                </section>
                                              )}
                                            </Dropzone>
                                        </div>
                                        <span className="text-red">{(dataResponse.message&&dataResponse.message.input_attach_file_2)?dataResponse.message.input_attach_file_2:""}</span>
                                    </div>
                                </div>
                            {this.state.attach_file_1 &&
                                (
                                    <div className="col-md-1">
                                        <BUTTON name="btnRemove" className="btn btn-none input-sm pull-right btn-small" type="button" onClick={(e) => this.handleRemoveImage(e,'attach_file_1', this.state.attach_file_1)}>
                                        <i className="fa fa-times-circle fa-2" aria-hidden="true"></i>
                                        {trans('messages.btn_cm_remove')}
                                        </BUTTON>
                                    </div>
                                )
                            }
                            </div>
                            {this.state.attach_file_1 && this.state.attach_file_1 === this.state.attach_file_1_before &&
                                <div className="form-group">
                                <span className="col-md-offset-2 col-md-5">{this.checkFile(this.state.attach_file_1, fileUrl) || ''}</span>
                                </div>
                            }
                        </div>
                        <div className="col-md-8 col-xs-12">
                            <div className="form-group">
                                <div>
                                    <label className="col-md-2">{trans('messages.deje_attached') + '.2'}</label>
                                    <div className="col-md-9">
                                        <div className="input-group input-group-sm">
                                            <Dropzone onDrop={(acceptedFiles) => {this.handleAttached('attach_file_2', acceptedFiles)}}>
                                              {({getRootProps, getInputProps}) => (
                                                <section className="container-dropzone">
                                                  <div {...getRootProps({className: 'dropzone', multiple: false})}>
                                                    <input {...getInputProps()} />
                                                    <p>{this.state.attach_file_2_realname ? this.state.attach_file_2_realname : trans('messages.upload_file_drag_drop')}</p>
                                                  </div>
                                                </section>
                                              )}
                                            </Dropzone>
                                        </div>
                                        <span className="text-red">{(dataResponse.message&&dataResponse.message.input_attach_file_2)?dataResponse.message.input_attach_file_2:""}</span>
                                    </div>
                                </div>
                            {this.state.attach_file_2 &&
                                (
                                    <div className="col-md-1">
                                        <BUTTON name="btnRemove" className="btn btn-none input-sm pull-right btn-small" type="button" onClick={(e) => this.handleRemoveImage(e,'attach_file_2', this.state.attach_file_2)}>
                                        <i className="fa fa-times-circle fa-2" aria-hidden="true"></i>
                                        {trans('messages.btn_cm_remove')}
                                        </BUTTON>
                                    </div>
                                )
                            }
                            </div>
                            {this.state.attach_file_2 && this.state.attach_file_2 === this.state.attach_file_2_before &&
                                <div className="form-group">
                                <span className="col-md-offset-2 col-md-5">{this.checkFile(this.state.attach_file_2, fileUrl) || ''}</span>
                                </div>
                            }
                        </div>
                        <div className="col-md-8 col-xs-12">
                            <div className="form-group">
                                <div>
                                    <label className="col-md-2">{trans('messages.deje_attached') + '.3'}</label>
                                    <div className="col-md-9">
                                        <div className="input-group input-group-sm">
                                            <Dropzone onDrop={(acceptedFiles) => {this.handleAttached('attach_file_3', acceptedFiles)}}>
                                              {({getRootProps, getInputProps}) => (
                                                <section className="container-dropzone">
                                                  <div {...getRootProps({className: 'dropzone', multiple: false})}>
                                                    <input {...getInputProps()} />
                                                    <p>{this.state.attach_file_3_realname ? this.state.attach_file_3_realname : trans('messages.upload_file_drag_drop')}</p>
                                                  </div>
                                                </section>
                                              )}
                                            </Dropzone>
                                        </div>
                                        <span className="text-red">{(dataResponse.message&&dataResponse.message.input_attach_file_3)?dataResponse.message.input_attach_file_3:""}</span>
                                    </div>
                                </div>
                            {this.state.attach_file_3 &&
                                (
                                    <div className="col-md-1">
                                        <BUTTON name="btnRemove" className="btn btn-none input-sm pull-right btn-small" type="button" onClick={(e) => this.handleRemoveImage(e,'attach_file_3', this.state.attach_file_3)}>
                                        <i className="fa fa-times-circle fa-2" aria-hidden="true"></i>
                                        {trans('messages.btn_cm_remove')}
                                        </BUTTON>
                                    </div>
                                )
                            }
                            </div>
                            {this.state.attach_file_3 && this.state.attach_file_3 === this.state.attach_file_3_before &&
                                <div className="form-group">
                                <span className="col-md-offset-2 col-md-5">{this.checkFile(this.state.attach_file_3, fileUrl) || ''}</span>
                                </div>
                            }
                        </div>
                        <div className="col-md-8 col-xs-12">
                            <div className="form-group">
                                <div>
                                    <label className="col-md-2">{trans('messages.deje_attached') + '.4'}</label>
                                    <div className="col-md-9">
                                        <div className="input-group input-group-sm">
                                            <Dropzone onDrop={(acceptedFiles) => {this.handleAttached('attach_file_4', acceptedFiles)}}>
                                              {({getRootProps, getInputProps}) => (
                                                <section className="container-dropzone">
                                                  <div {...getRootProps({className: 'dropzone', multiple: false})}>
                                                    <input {...getInputProps()} />
                                                    <p>{this.state.attach_file_4_realname ? this.state.attach_file_4_realname : trans('messages.upload_file_drag_drop')}</p>
                                                  </div>
                                                </section>
                                              )}
                                            </Dropzone>
                                        </div>
                                        <span className="text-red">{(dataResponse.message&&dataResponse.message.input_attach_file_4)?dataResponse.message.input_attach_file_4:""}</span>
                                    </div>
                                </div>
                            {this.state.attach_file_4 &&
                                (
                                    <div className="col-md-1">
                                        <BUTTON name="btnRemove" className="btn btn-none input-sm pull-right btn-small" type="button" onClick={(e) => this.handleRemoveImage(e,'attach_file_4', this.state.attach_file_4)}>
                                        <i className="fa fa-times-circle fa-2" aria-hidden="true"></i>
                                        {trans('messages.btn_cm_remove')}
                                        </BUTTON>
                                    </div>
                                )
                            }
                            </div>
                            {this.state.attach_file_4 && this.state.attach_file_4 === this.state.attach_file_4_before &&
                                <div className="form-group">
                                <span className="col-md-offset-2 col-md-5">{this.checkFile(this.state.attach_file_4, fileUrl) || ''}</span>
                                </div>
                            }
                        </div>
                        <div className="col-md-8 col-xs-12">
                            <div className="form-group">
                                <div>
                                    <label className="col-md-2">{trans('messages.deje_attached') + '.5'}</label>
                                    <div className="col-md-9">
                                        <div className="input-group input-group-sm">
                                            <Dropzone onDrop={(acceptedFiles) => {this.handleAttached('attach_file_5', acceptedFiles)}}>
                                              {({getRootProps, getInputProps}) => (
                                                <section className="container-dropzone">
                                                  <div {...getRootProps({className: 'dropzone', multiple: false})}>
                                                    <input {...getInputProps()} />
                                                    <p>{this.state.attach_file_5_realname ? this.state.attach_file_5_realname : trans('messages.upload_file_drag_drop')}</p>
                                                  </div>
                                                </section>
                                              )}
                                            </Dropzone>
                                        </div>
                                        <span className="text-red">{(dataResponse.message&&dataResponse.message.input_attach_file_5)?dataResponse.message.input_attach_file_5:""}</span>
                                    </div>
                                </div>
                            {this.state.attach_file_5 &&
                                (
                                    <div className="col-md-1">
                                        <BUTTON name="btnRemove" className="btn btn-none input-sm pull-right btn-small" type="button" onClick={(e) => this.handleRemoveImage(e,'attach_file_5', this.state.attach_file_5)}>
                                        <i className="fa fa-times-circle fa-2" aria-hidden="true"></i>
                                        {trans('messages.btn_cm_remove')}
                                        </BUTTON>
                                    </div>
                                )
                            }
                            </div>
                            {this.state.attach_file_5 && this.state.attach_file_5 === this.state.attach_file_5_before &&
                                <div className="form-group">
                                <span className="col-md-offset-2 col-md-5">{this.checkFile(this.state.attach_file_5, fileUrl) || ''}</span>
                                </div>
                            }
                        </div>
                        <div className="col-md-8 col-xs-12">
                            <div className="form-group">
                                <div>
                                    <label className="col-md-2">{trans('messages.deje_attached') + '.6'}</label>
                                    <div className="col-md-9">
                                        <div className="input-group input-group-sm">
                                            <Dropzone onDrop={(acceptedFiles) => {this.handleAttached('attach_file_6', acceptedFiles)}}>
                                              {({getRootProps, getInputProps}) => (
                                                <section className="container-dropzone">
                                                  <div {...getRootProps({className: 'dropzone', multiple: false})}>
                                                    <input {...getInputProps()} />
                                                    <p>{this.state.attach_file_6_realname ? this.state.attach_file_6_realname : trans('messages.upload_file_drag_drop')}</p>
                                                  </div>
                                                </section>
                                              )}
                                            </Dropzone>
                                        </div>
                                        <span className="text-red">{(dataResponse.message&&dataResponse.message.input_attach_file_6)?dataResponse.message.input_attach_file_6:""}</span>
                                    </div>
                                </div>
                            {this.state.attach_file_6 &&
                                (
                                    <div className="col-md-1">
                                        <BUTTON name="btnRemove" className="btn btn-none input-sm pull-right btn-small" type="button" onClick={(e) => this.handleRemoveImage(e,'attach_file_6', this.state.attach_file_6)}>
                                        <i className="fa fa-times-circle fa-2" aria-hidden="true"></i>
                                        {trans('messages.btn_cm_remove')}
                                        </BUTTON>
                                    </div>
                                )
                            }
                            </div>
                            {this.state.attach_file_6 && this.state.attach_file_6 === this.state.attach_file_6_before &&
                                <div className="form-group">
                                <span className="col-md-offset-2 col-md-5">{this.checkFile(this.state.attach_file_6, fileUrl) || ''}</span>
                                </div>
                            }
                        </div>
                        <div className="col-md-4">
                            {
                                this.state.process === 'edit' &&
                                <BUTTON name="btnAccept" className="btn btn-success input-sm pull-left margin-element margin-form btn-inverse-custom" type="button" onClick={this.handleClone}>{trans('messages.deje_clone')}</BUTTON>
                            }
                            <BUTTON name="btnAccept" className="btn btn-success input-sm btn-larger pull-left margin-element margin-form" type="button" onClick={this.handleSubmit}>{trans('messages.btn_change_deje')}</BUTTON>
                            <BUTTON id="btn-back-custom" className="btn bg-purple input-sm pull-left btn-larger margin-element margin-form" type="button" onClick={this.handleCancel}>{trans('messages.btn_cancel_deje')}</BUTTON>
                            <BUTTON className="btn bg-primary input-sm pull-left btn-larger margin-form" type="button" onClick={this.handlePrint}>{trans('messages.btn_print_deje')}</BUTTON>
                        </div>
                    </div>
                    <div className="row">
                    </div>
                    { this.state.process === 'edit' &&
                    <div className="" >
                        <hr className="hr-full" />
                        <div className="form-group">
                            <div className="col-md-12"  id="comment-go">
                                <div className="row">
                                    <label className="col-md-12">{trans('messages.deje_comment')}</label>
                                </div>
                            </div>
                            <div className="col-md-8">
                                <div className="row">
                                    <div className="col-md-6"><strong>{"☆" + dataSource.opeName}</strong></div>
                                    <div className="col-md-3">
                                        <label>
                                        <RADIO_CUSTOM
                                            name="editor_type"
                                            value="0"
                                            defaultChecked
                                            onChange={this.handleChange}
                                             />
                                        <span>{" " + trans('messages.rat_text')}</span>

                                        </label>
                                    </div>
                                    <div className="col-md-3">
                                    <label>
                                        <RADIO_CUSTOM
                                            name="editor_type"
                                            value="1"
                                            onChange={this.handleChange}
                                             />
                                        <span>{" " + trans('messages.rat_html')}</span>
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div className="col-md-8">
                                {
                                    (this.state.editor_type === '1') ?
                                        <EDITOR
                                            apiKey='sm3yi664b07edp6t6hf1trx3xy6ziv5xg41muurzssvy4773'
                                            init={{
                                                force_br_newlines : true,
                                                force_p_newlines : false,
                                                forced_root_block : '',
                                                branding: false,
                                                menubar: '',
                                                toolbar1: "undo redo | styleselect | forecolor backcolor | fontsizeselect | bold italic underline | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link unlink anchor | print preview code ",
                                                plugins: "textcolor colorpicker paste lists autolink link",
                                                preview_styles: 'font-size color',
                                                fontsize_formats: '8pt 9pt 10pt 11pt 12pt 13pt 14pt 15pt 16pt 17pt 18pt 24pt 36pt 48pt 60pt 72pt 96pt',
                                                height : 100,
                                                paste_as_text: true
                                            }}
                                            name="comment_content"
                                            className="form-control input-sm"
                                            fieldGroupClass="col-md-12"
                                            groupClass="form-group"
                                            onEditorChange={(event) => this.handleEditorChange('comment_content', event)}
                                            value={this.state.comment_content || ''}
                                            errorPopup={true}
                                            hasError={(this.state.message_comment && this.state.message_comment.comment_content)?true:false}
                                            errorMessage={(this.state.message_comment && this.state.message_comment.comment_content)?this.state.message_comment.comment_content:""}
                                        />
                                        :
                                        <TEXTAREA
                                            name="comment_content"
                                            className="form-control"
                                            fieldGroupClass="col-md-12"
                                            groupClass=""
                                            errorPopup={true}
                                            onChange={this.handleChange}
                                            value={this.state.comment_content || ''}
                                            rows="7"
                                            errorPopup={true}
                                            hasError={(this.state.message_comment && this.state.message_comment.comment_content)?true:false}
                                            errorMessage={(this.state.message_comment && this.state.message_comment.comment_content)?this.state.message_comment.comment_content:""}
                                        />
                                }
                            </div>
                            <div className="col-md-12">
                                <div className="row">
                                       <div className="col-md-2">
                                        <BUTTON name="btnAccept" className="btn btn-primary input-sm pull-left  btn-small" type="button" onClick={this.handleSubmitComment}>{trans('messages.btn_add_cm')}</BUTTON>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    }
                    {
                    (!_.isEmpty(this.state.dataComment))?
                    <div className="row" >
                        <div className="col-md-12" >
                            <hr className="hr-full" />
                                {

                                    this.state.dataComment.map((item, index) => {
                                        length -= 1;
                                        return ([
                                            <div className="row">
                                                <div className="col-md-1 text-center">
                                                    <span className="number"><strong>{length}</strong></span>
                                                </div>
                                                <div className="col-md-7">
                                                <div className="row">

                                                        <div className="col-md-3">{"☆ " + item.full_name}</div>
                                                        <div className="col-md-5">{trans('messages.deje_cm_date') + " : "}{item.in_date}</div>
                                                        <div className="col-md-4">
                                                            <BUTTON name="btnRemove" className="btn btn-none input-sm pull-right btn-small" type="button" onClick={(e) => this.handleRemove(e,item)}>
                                                                <i className="fa fa-times-circle fa-2" aria-hidden="true"></i>
                                                                {trans('messages.btn_cm_remove')}
                                                            </BUTTON>
                                                        </div>
                                                    </div>
                                                    <div className="form-group">
                                                        <div className="col-md-12 ">
                                                            <div className="mail-content">
                                                                <div dangerouslySetInnerHTML={{__html: item.contents.replace(/\r?\n/g, '<br />')}} />
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        ])
                                    })
                                }
                        </div>
                    </div> : ''
                    }
                </div>
            </FORM>
            <ExportPdfPopup deje_id={this.state.deje_id} />
            </div>
            <MESSAGE_MODAL
                message={this.state.message_realtime || ''}
                title={trans('messages.deny_edit')}
                baseUrl={baseUrl}
                allowView={this.state.times ? false : true}
            />
            <AcceptCommnetPopup
                handleSubmit={this.handleSubmitDeletePopup}
                handleCancel={this.handleCancelDeletePopup}
            />
            </div>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        dataSource: state.commonReducer.dataSource,
        dataResponse: state.commonReducer.dataResponse,
        dataRealTime: state.commonReducer.dataRealTime
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        reloadData: (url, params) => { dispatch(Action.detailData(url, params)) },
        postData: (url, params, headers) => { dispatch(Action.postData(url, params, headers))},
        postRealTime: (url, params, headers) => { dispatch(Action.postRealTime(url, params, headers)) }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(DejeSave)