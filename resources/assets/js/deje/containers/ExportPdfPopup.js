import React, { Component } from 'react';
import Loading from '../../common/components/common/Loading';
import INPUT from '../../common/components/form/INPUT';

import DATEPICKER from '../../common/components/form/DATEPICKER';
import moment from 'moment';
import axios from 'axios';

export default class ExportPdfPopup extends Component {
    constructor(props) {
        super(props);

        this.state = {
            deje_id: this.props.deje_id,
            order_id: '',
            customer_name: ''
        }
    }

    handleChange = (event) => {
        const target = event.target;
        const name = target.name;
        const value = target.value;
        this.setState({
            [name]: value
        });
    }

    handleExportClick = () => {
        let params = {};
        let arrParams = [];

        params['deje_id']    = this.state['deje_id'];
        params['order_id']    = this.state['order_id'];
        params['customer_name'] = this.state['customer_name'];

        arrParams.push("deje_id=" + this.state['deje_id']);
        arrParams.push("order_id=" + this.state['order_id']);
        arrParams.push("customer_name=" + this.state['customer_name']);

        $(".loading").show();
        axios.post(exportIssueUrl, params, {headers: {'X-Requested-With': 'XMLHttpRequest'}}
        ).then(response => {
            if (response.data.status === 0) {
                let messages = response.data.messages;
                this.setState({
                    order_id_error: (messages.order_id)?messages.order_id:'',
                    customer_name_error: (messages.customer_name)?messages.customer_name:''
                });
            } else {
                this.setState({
                    order_id_error: '',
                    customer_name_error: ''
                });
                window.open(exportIssueUrl + "?" + arrParams.join('&') , '_blank');
                this.handleCancelClick();
            }
            $(".loading").hide();
        }).catch(error => {
            $(".loading").hide();
            throw(error);
        });
    }

    handleCancelClick = () => {
        this.setState({
            order_id: '',
            customer_name: '',
            order_id_error: '',
            customer_name_error: ''
        });
        $('#issue-modal').modal('hide');
    }

    render() {
        return (
            <div className="modal fade" id="issue-modal">
                <Loading className="loading" />
                <input type="hidden" id="action-modal" />
                <div className="modal-dialog modal-custom">
                    <div className="modal-content modal-custom-content">
                        <div className="modal-header">
                            <h4 className="modal-title text-center"><b>{trans('messages.deje_issue_modal_title')}</b></h4>
                        </div>
                        <div className="modal-body modal-custom-body">
                            <p>{trans('messages.deje_issue_modal_message')}</p>
                            <form className="form-horizontal">
                                <INPUT
                                    name="order_id"
                                    labelTitle={trans('messages.deje_issue_order_id') + "："}
                                    className="form-control input-sm"
                                    fieldGroupClass="col-md-9"
                                    labelClass="col-md-3"
                                    groupClass=""
                                    errorPopup={true}
                                    value={this.state.order_id}
                                    onChange={this.handleChange}
                                    hasError={(this.state.order_id_error)?true:false}
                                    errorMessage={(this.state.order_id_error)?this.state.order_id_error:""}
                                    />
                                <INPUT
                                    name="customer_name"
                                    labelTitle={trans('messages.deje_issue_customer_name') + "："}
                                    className="form-control input-sm"
                                    fieldGroupClass="col-md-9"
                                    labelClass="col-md-3"
                                    groupClass=""
                                    errorPopup={true}
                                    value={this.state.customer_name}
                                    onChange={this.handleChange}
                                    hasError={(this.state.customer_name_error)?true:false}
                                    errorMessage={(this.state.customer_name_error)?this.state.customer_name_error:""}
                                    />
                            </form>
                        </div>
                        <div className="modal-footer modal-custom-footer">
                            <button type="button" className="btn btn-primary modal-custom-button" onClick={this.handleExportClick}>{trans('messages.issue_modal_ok')}</button>
                            <button type="button" className="btn bg-purple modal-custom-button" onClick={this.handleCancelClick}>{trans('messages.issue_modal_cancel')}</button>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}