import React, { Component } from 'react';

import { connect } from 'react-redux';
import * as Action from '../common/actions';
import * as queryString from 'query-string';

import BUTTON from '../common/components/form/BUTTON';
import FORM from '../common/components/form/FORM';
import INPUT from '../common/components/form/INPUT';
import SELECT from '../common/components/form/SELECT';

import TABLE from '../common/components/table/TABLE';
import THEAD from '../common/components/table/THEAD';
import TBODY from '../common/components/table/TBODY';
import TR from '../common/components/table/TR';
import TH from '../common/components/table/TH';
import TD from '../common/components/table/TD';

import SortComponent from '../common/components/table/SortComponent';

import PaginationContainer from '../common/containers/PaginationContainer'

import Loading from '../common/components/common/Loading';
import AcceptPopup from './containers/AcceptPopup';
import axios from 'axios';
var timer;
class DejeList extends Component {

    constructor(props) {
        super(props);
        const stringParams = queryString.parse(props.location.search);
        this.state = {};
        if (!_.isEmpty(stringParams)) {
            this.state = stringParams;
        }
        this.props.reloadData(dataListUrl, this.state);
        this.top = false;
    }

    handleChange = (event) => {
        const target = event.target;
        const value = target.type === 'checkbox' ? target.checked : target.value;
        const name = target.name;
        this.setState({
            [name]: (value)?value:undefined
        },() => {
            if (name === 'per_page') {
                this.handleReloadData();
            } else {
                clearTimeout(timer);
                let _this = this;
                timer = setTimeout(function() {
                    _this.handleReloadData();
                }, 1500);
            }
        });
    }

    handleClearClick = (event) => {
        Object.keys(this.state).map((key) => {
            if (key !== 'per_page') {
                delete this.state[key];
            }
        });
        this.handleReloadData();
    }

    handleReloadData = () => {
        if (this.state.page) {
            delete this.state.page;
        }
        this.props.reloadData(dataListUrl, this.state, true);
    }

    componentDidUpdate() {
        if (!_.isEmpty(this.props.dataSource)) {
            $('.loading').hide();
            $('.show-modal-content').customPopover({delay: {show: 200, hide: 200}, trigger:""});
        }
        if (this.top) {
            this.top = false;
            jQuery('html,body').animate({ scrollTop: 0 }, 'slow');
        }


        $('body').on('click', function (e) {
            $('[data-toggle="popover"]').each(function () {
                console.log($(this).is(e.target));
                console.log($(this).has(e.target).length);
                console.log($('.popover').has(e.target).length);
                if (!$(this).is(e.target) && $(this).has(e.target).length === 0 && $('.popover').has(e.target).length === 0) {
                    $(this).popover('hide');
                } else if ($(this).is(e.target)) {
                    $(this).popover('show');
                }
            });
        });
    }

    componentWillMount() {
        window.onpopstate = (event) => {
            location.reload();
        };
    }

    handleSortClick = (name, nextSort) => {
        Object.keys(this.state).map((key) => {
            if (key.startsWith('sort_')) {
                delete this.state[key];
            }
        });
        if (nextSort === 'none') {
            delete this.state[name];
            this.props.reloadData(dataListUrl, this.state, true);
        } else {
            this.setState({[name]: nextSort}, () => {
                this.props.reloadData(dataListUrl, this.state, true);
            });
        }

    }

    handleClickPage = (page) => {
        this.setState({
            page: page
        },() => {
            this.props.reloadData(dataListUrl, this.state, true);
            this.top = true;
        });
    }

    handleChangeMultiSelect = (name, value, checked) => {
        let currentValue = this.state[name];
        if (typeof currentValue === 'undefined') {
            currentValue = [];
        } else if (!Array.isArray(this.state[name])) {
            currentValue = [this.state[name]];
        }
        if (Array.isArray(value)) {
            currentValue = [...value];
        } else {
            let index = currentValue.indexOf(value);
            if (checked === false) {
                if (index !== -1) {
                    currentValue.splice(index, 1);
                }
            } else {
                if (index === -1) {
                    currentValue.push(value);
                }
            }
        }
        this.setState({
            [name]: currentValue,
        }, () => {
            this.handleReloadData();
        });
    }

    handlePopupDelete = (event, deje_id) => {
        this.setState({
            deje_id: deje_id,
        }, () => {
            $('#access-modal').modal('show');
        });

    }

    handleSubmitPopup = () => {
        $(".loading").show();
        axios.post(deleteUrl, {deje_id: this.state.deje_id} , {headers: {'X-Requested-With': 'XMLHttpRequest'}})
        .then(response => {
            if (response.data.status === 1) {
                this.handleReloadData();
            }
            delete this.state.deje_id;
            $(".loading").hide();
            $('#access-modal').modal('hide');
        }).catch(error => {
            $(".loading").hide();
            throw(error);
        });
    }

    handleCancelPopup = () => {
        delete this.state.deje_id;
        $('#access-modal').modal('hide');
    }

    render() {
        let dataSource = this.props.dataSource;
        let index = 0;
        let totalItems = 0;
        let itemsPerPage = 20;
        let objectType  = '';
        let tantou  = '';
        let processMethod  = '';
        let businessStype  = '';
        let processStatus  = '';
        if (!_.isEmpty(dataSource)) {
            index = dataSource.data.from - 1;
            totalItems          = dataSource.data.total;
            itemsPerPage        = dataSource.data.per_page;
            objectType          = dataSource.objectType;
            tantou              = dataSource.tantou ;
            processStatus       = dataSource.processStatus ;
            processMethod       = dataSource.processMethod ;
            businessStype       = dataSource.businessStype ;
        }
        return ( (!_.isEmpty(dataSource)) &&
            <div>
                <Loading className="loading" />
                <div className="box">
                    <div className="box-header">
                        <div className="row">
                                <div className="col-md-12">
                                    <h4>{trans('messages.search_title')}</h4>
                                </div>
                                <FORM>
                                <div className="row">
                                    <div className="col-md-12">
                                        <INPUT
                                            name="deje_id"
                                            groupClass="form-group col-md-1"
                                            labelTitle={trans('messages.deje_no')}
                                            onChange={this.handleChange}
                                            value={this.state.deje_id||''}
                                            className="form-control input-sm" />
                                        <INPUT
                                            name="subject"
                                            groupClass="form-group col-md-1-5"
                                            labelTitle={trans('messages.src_subject')}
                                            onChange={this.handleChange}
                                            value={this.state.subject||''}
                                            className="form-control input-sm" />
                                        <SELECT
                                            id="business_stype"
                                            name="business_stype"
                                            groupClass="form-group col-md-1-5"
                                            labelClass=""
                                            labelTitle={trans('messages.src_business_stype')}
                                            handleChangeMultiSelect={this.handleChangeMultiSelect}
                                            valueMulti={this.state.business_stype||''}
                                            options={businessStype}
                                            multiple="multiple"
                                            className="form-control input-sm" />
                                        <SELECT
                                            id="object_type"
                                            name="object_type"
                                            groupClass="form-group col-md-1-5"
                                            labelClass=""
                                            labelTitle={trans('messages.lbl_object_type')}
                                            handleChangeMultiSelect={this.handleChangeMultiSelect}
                                            valueMulti={this.state.object_type||''}
                                            options={objectType}
                                            multiple="multiple"
                                            className="form-control input-sm" />
                                        <SELECT
                                            id="process_status"
                                            name="process_status"
                                            groupClass="form-group col-md-1-5"
                                            labelClass=""
                                            labelTitle={trans('messages.src_process_status')}
                                            handleChangeMultiSelect={this.handleChangeMultiSelect}
                                            valueMulti={this.state.process_status}
                                            options={processStatus}
                                            multiple="multiple"
                                            className="form-control input-sm" />
                                        <SELECT
                                            id="process_method"
                                            name="process_method"
                                            groupClass="form-group col-md-1-5"
                                            labelClass=""
                                            labelTitle={trans('messages.lbl_process_method')}
                                            handleChangeMultiSelect={this.handleChangeMultiSelect}
                                            valueMulti={this.state.process_method||''}
                                            options={processMethod}
                                            multiple="multiple"
                                            className="form-control input-sm" />
                                        <INPUT
                                            name="contents"
                                            groupClass="form-group col-md-1-75"
                                            labelTitle={trans('messages.lbl_content')}
                                            onChange={this.handleChange}
                                            value={this.state.contents||''}
                                            className="form-control input-sm" />
                                        <INPUT
                                            name="search_all"
                                            groupClass="form-group col-md-1-75"
                                            labelTitle={trans('messages.deje_search_all')}
                                            onChange={this.handleChange}
                                            value={this.state.search_all||''}
                                            className="form-control input-sm" />
                                        <SELECT
                                            name="per_page"
                                            groupClass="form-group col-md-1 per-page pull-right-md"
                                            labelClass="pull-right"
                                            labelTitle={trans('messages.per_page')}
                                            onChange={this.handleChange}
                                            value={this.state.per_page||''}
                                            options={{20:20, 40:40, 60:60, 80:80, 100:100}}
                                            className="form-control input-sm" />
                                        </div>
                                    </div>
                                </FORM>
                        </div>
                        <div className="row">
                            <div className="col-md-9 col-xs-4">
                                <a href={saveUrl} className="btn btn-success input-sm">{trans('messages.btn_add')}</a>
                            </div>
                            <div className="col-md-3 col-xs-8">
                                <BUTTON className="btn btn-danger pull-right" onClick={this.handleClearClick}>{trans('messages.btn_reset')}</BUTTON>
                                {/*<BUTTON className="btn btn-primary pull-right margin-element" onClick={this.handleReloadData} >{trans('messages.btn_search')}</BUTTON>*/}
                            </div>
                        </div>
                    </div>
                    <div className="box-body">
                        <div className="row">
                            <div className="col-md-12">
                                {(totalItems/itemsPerPage > 1) &&
                                <div className="row">
                                    <div className="col-md-2-5"></div>
                                    <div className="col-md-7 text-center">
                                        <PaginationContainer handleClickPage={this.handleClickPage}/>
                                    </div>
                                    <div className="col-md-2-5 text-right line-height-md">
                                        {dataSource.data.from}件 - {dataSource.data.to}件 【全{dataSource.data.total}件】
                                    </div>
                                </div>
                                }
                                <div className="table-responsive">
                                <TABLE className="table table-striped table-custom">
                                    <THEAD>
                                        <TR>
                                            <TH className="text-center"></TH>
                                            <TH className="text-center col-max-min-60">
                                                <SortComponent
                                                    name="sort_deje_id"
                                                    onClick={this.handleSortClick}
                                                    currentSort={(this.state.sort_deje_id)?this.state.sort_deje_id:'none'}
                                                    title={"#"} />
                                            </TH>
                                            <TH className="text-center col-max-min-100">
                                                <SortComponent
                                                    name="sort_process_status"
                                                    onClick={this.handleSortClick}
                                                    currentSort={(this.state.sort_process_status)?this.state.sort_process_status:'none'}
                                                    title={trans('messages.lbl_process_status')} />
                                            </TH>
                                            <TH className="text-center col-max-min-200">
                                                <SortComponent
                                                    name="sort_subject"
                                                    onClick={this.handleSortClick}
                                                    currentSort={(this.state.sort_subject)?this.state.sort_subject:'none'}
                                                    title={trans('messages.lbl_subject')} />
                                            </TH>
                                            <TH className="text-center col-max-min-100">
                                                <SortComponent
                                                    name="sort_object_type"
                                                    onClick={this.handleSortClick}
                                                    currentSort={(this.state.sort_object_type)?this.state.sort_object_type:'none'}
                                                    title={trans('messages.lbl_object_type')} />
                                            </TH>
                                            <TH className="text-center col-max-min-100">
                                                <SortComponent
                                                    name="sort_business_stype"
                                                    onClick={this.handleSortClick}
                                                    currentSort={(this.state.sort_business_stype)?this.state.sort_business_stype:'none'}
                                                    title={trans('messages.lbl_business_stype')} />
                                            </TH>
                                            <TH className="text-center col-max-min-80">
                                                <SortComponent
                                                    name="sort_dai_tantou"
                                                    onClick={this.handleSortClick}
                                                    currentSort={(this.state.sort_dai_tantou)?this.state.sort_dai_tantou:'none'}
                                                    title={trans('messages.lbl_dai_tantou')} />
                                            </TH>
                                            <TH className="text-center col-max-min-100">
                                                <SortComponent
                                                    name="sort_process_method"
                                                    onClick={this.handleSortClick}
                                                    currentSort={(this.state.sort_process_method)?this.state.sort_process_method:'none'}
                                                    title={trans('messages.lbl_process_method')} />
                                            </TH>
                                            <TH className="text-center col-max-min-120">
                                                <SortComponent
                                                    name="sort_in_date"
                                                    onClick={this.handleSortClick}
                                                    currentSort={(this.state.sort_in_date)?this.state.sort_in_date:'none'}
                                                    title={trans('messages.lbl_in_date')} />
                                            </TH>

                                            <TH className="text-center col-max-min-80">
                                                <SortComponent
                                                    name="sort_in_ope"
                                                    onClick={this.handleSortClick}
                                                    currentSort={(this.state.sort_in_ope)?this.state.sort_in_ope:'none'}
                                                    title={trans('messages.lbl_in_ope')} />
                                            </TH>
                                            <TH className="text-center col-max-min-120">
                                                <SortComponent
                                                    name="sort_up_date"
                                                    onClick={this.handleSortClick}
                                                    currentSort={(this.state.sort_up_date)?this.state.sort_up_date:'none'}
                                                    title={trans('messages.lbl_up_date')} />
                                            </TH>
                                            <TH className="text-center"></TH>
                                        </TR>
                                    </THEAD>
                                    <TBODY>
                                        {
                                            (!_.isEmpty(dataSource.data.data))?
                                            dataSource.data.data.map((item) => {
                                                index++;
                                                let classRow = (index%2 ===0) ? 'odd' : 'even';
                                                return (
                                                    [
                                                    <TR key={"tr" + index} className={classRow}>

                                                        {/*<TD className="text-center" title="View info">
                                                            <a  href={detailUrl + "?deje_id=" + item.deje_id}>
                                                                <i className="fa fa-fw fa-eye text-danger"></i>
                                                            </a>
                                                        </TD>*/}
                                                        <TD className="text-center" title="Edit info">
                                                            <a  href={saveUrl + "?deje_id=" + item.deje_id}>
                                                                <i className="fa fa-fw fa-pencil text-warning"></i>
                                                            </a>
                                                        </TD>
                                                        <TD className="text-center">{item.deje_id}</TD>

                                                        <TD className={"process_status_" + item.process_status} title={item.process_status}>
                                                            <span className="">{item.process_status !== null ? processStatus.find(x => x.key == item.process_status).value : ''}</span>
                                                        </TD>
                                                        <TD className="max-width-200" title={item.subject}>
                                                            <span key={"popup_content_" + item.deje_id} className="cut-text limit-char max-w-200 show-modal-content" data-title={item.subject}
                                                            data-content={item.contents !== null ? item.contents.replace(/(?:\r\n|\r|\n)/g, '<br>') : ''}
                                                            data-toggle="popover" data-placement="right"
                                                            >
                                                                {item.subject}
                                                            </span>
                                                        </TD>
                                                        <TD className="" title={item.object_type}>
                                                            <span className="">{objectType.find(x => x.key == item.object_type) ? objectType.find(x => x.key == item.object_type).value : ''}</span>
                                                        </TD>
                                                        <TD className="" title={item.business_stype}>
                                                            <span className="">{item.business_stype !== null ? businessStype.find(x => x.key == item.business_stype).value : ''}</span>
                                                        </TD>
                                                        <TD className="col-max-min-80" title={item.dai_tantou}>
                                                            <span className="">{tantou.find(x => x.key == item.dai_tantou) ? tantou.find(x => x.key == item.dai_tantou).value : ''}</span>
                                                        </TD>
                                                        <TD className={"process_method_" + item.process_method} title={item.process_method}>
                                                            <span className="">{processMethod.find(x => x.key == item.process_method) ? processMethod.find(x => x.key == item.process_method).value : '' }</span>
                                                        </TD>
                                                        <TD className="text-center" title={item.in_date}>
                                                            <span className="">{item.in_date}</span>
                                                        </TD>
                                                        <TD className="" title={item.in_ope_cd}>
                                                            <span className="">{item.tantou_last_name}</span>
                                                        </TD>
                                                        <TD className="text-center">{item.up_date}</TD>
                                                        <TD className="text-center" title="Edit info">
                                                            <a onClick={(e) => this.handlePopupDelete(e, item.deje_id)}>
                                                                <i className="fa fa-fw fa-trash text-danger"></i>
                                                            </a>
                                                        </TD>
                                                    </TR>
                                                   ]
                                                )
                                            })
                                            :
                                            <TR><TD colSpan="13" className="text-center">{trans('messages.no_data')}</TD></TR>
                                        }
                                    </TBODY>
                                </TABLE>
                                </div>
                                <div className="text-center" id="pagination">
                                    <PaginationContainer handleClickPage={this.handleClickPage}/>
                                </div>
                            </div>
                        </div>
                    </div>
                    </div>
                    <AcceptPopup
                        deje_id={this.state.deje_id}
                        handleSubmit={this.handleSubmitPopup}
                        handleCancel={this.handleCancelPopup}
                    />
                </div>

        )
    }
}

const mapStateToProps = (state) => {

  return {
    dataSource: state.commonReducer.dataSource
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    reloadData: (url, params, pushHistory, ignoreFields) => { dispatch(Action.reloadData(url, params, pushHistory, ignoreFields)) }
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(DejeList)