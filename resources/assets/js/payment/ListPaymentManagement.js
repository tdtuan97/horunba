import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import * as Action from '../common/actions';
import * as helpers from '../common/helpers';
import * as queryString from 'query-string';
import createHistory from 'history/createBrowserHistory';
import BUTTON from '../common/components/form/BUTTON';
import FORM from '../common/components/form/FORM';
import INPUT from '../common/components/form/INPUT';
import SELECT from '../common/components/form/SELECT';
import LABEL from '../common/components/form/LABEL';
import CHECKBOX_CUSTOM from '../common/components/form/CHECKBOX_CUSTOM';
import TABLE from '../common/components/table/TABLE';
import THEAD from '../common/components/table/THEAD';
import TBODY from '../common/components/table/TBODY';
import TR from '../common/components/table/TR';
import TH from '../common/components/table/TH';
import TD from '../common/components/table/TD';
import SortComponent from '../common/components/table/SortComponent';
import PaginationContainer from '../common/containers/PaginationContainer'
import Loading from '../common/components/common/Loading';
import MessagePopup from '../common/components/common/MessagePopup';
import ImportCsv from './csv/ImportCsv';
import DatePicker from 'react-datepicker';
import moment from 'moment';
import 'react-datepicker/dist/react-datepicker.css';
import axios from 'axios';
class ListPaymentManagement extends Component {
    constructor(props) {
        super(props);
        const stringParams = helpers.parseStringToParams(props.location.search);
        this.state = {}
        if (!_.isEmpty(stringParams)) {
            this.state = stringParams;
        }
        this.flg_check = false;
        this.state.attached_file_path = '';
        this.props.reloadData(dataListUrl, stringParams);
    }

    handleChange = (event) => {
        const target = event.target;
        let value = target.value;
        if (value === '_none') {
            value = '';
        }
        let name = target.name;
        this.setState({
            [name]: value
        },() => {
            if (name === 'per_page' || name === 'order_status' || name === 'name_jp') {
                this.handleReloadData();
            }
        });
    }

    handleChangeMultiSelect = (name, value, checked) => {
        let currentValue = this.state[name];
        if (typeof currentValue === 'undefined') {
            currentValue = [];
        } else if (!Array.isArray(this.state[name])) {
            currentValue = [this.state[name]];
        }
        if (Array.isArray(value)) {
            currentValue = [...value];
        } else {
            let index = currentValue.indexOf(value);
            if (checked === false) {
                if (index !== -1) {
                    currentValue.splice(index, 1);
                }
            } else {
                if (index === -1) {
                    currentValue.push(value);
                }
            }
        }
        this.setState({
            [name]: currentValue,
        });
    }

    handleChangeDate = function(date, name, event) {
        let dateValue = moment(date);
        if (!dateValue.isValid()) {
            dateValue = undefined;
        } else {
            dateValue = dateValue.format('YYYY-MM-DD');
        }
        this.setState({
          [name]: dateValue,
        });
    }

    handleClearClick = (event) => {
        Object.keys(this.state).map((key) => {
            if (key !== 'per_page') {
                delete this.state[key];
            }
        });
        this.handleReloadData();
    }

    handleClickPage = (page) => {
        this.setState({
            page: page
        },() => {
            this.props.reloadData(dataListUrl, this.state, true);
        });
    }

    handleSortClick = (name, nextSort) => {
        Object.keys(this.state).map((key) => {
            if (key.startsWith('sort_')) {
                delete this.state[key];
            }
        });
        if (nextSort === 'none') {
            delete this.state[name];
            this.props.reloadData(dataListUrl, this.state, true);
        } else {
            this.setState({[name]: nextSort}, () => {
                this.props.reloadData(dataListUrl, this.state, true);
            });
        }
    }
    handleReloadData = (a) => {
        let params = {};
        let dataSource = this.props.dataSource;
        let tmpParams = this.state;
        Object.keys(tmpParams).map(key => {
            if (tmpParams[key] !== ""
                && tmpParams[key] !== null
                && key !== 'flg'
                && key !== 'attached_file_path'
                && key !== 'attached_file'
                && key !== 'page') {
                    params[key] = tmpParams[key];
            } else {
                delete params[key];
            }
        });
        this.props.reloadData(dataListUrl, params, true);
    }
    componentDidUpdate() {
        if (!_.isEmpty(this.props.dataSource)) {
            $('.loading').hide();
        }
    }
    componentWillMount() {
        window.onpopstate = (event) => {
            location.reload();
        };
    }

    handleChooseFile = (event) => {
        const target = event.target;
        if (target.files !== null && target.files.length !== 0) {
            const name = target.name;
            let tmpName = name.replace(new RegExp("^browser_"), '');
            this.setState({
                [tmpName]: target.files[0].name,
                ['attached_file']: target.files[0]
            }, ()=> {
                document.getElementsByName(tmpName)[0].value = target.files[0].name;
            });
        }
    }

    handleImportSubmit = () => {
        let formData   = new FormData();
        let headers = {'X-Requested-With': 'XMLHttpRequest'};
        let check = true;
        if (!_.isEmpty(this.state)) {
            Object.keys(this.state).map((item) => {
                if (item === 'attached_file_path') {
                    if (this.state[item] === '') {
                        check = false;
                        this.setState({
                            'message' : trans('messages.message_choose_file'),
                        })
                    } else {
                        let extension = this.getFileExtension(this.state[item]);
                        if (!_.isEmpty(extension) && extension[0] !== 'csv') {
                            check = false;
                            this.setState({
                                'message' : trans('messages.message_choose_file_incorrect'),
                            })
                        }
                    }
                }
                formData.append(item, this.state[item]);
                // formData.append('type', $('#action-modal').val());
            });
        }
        if (check) {
            this.flg_check = true;
            this.setState({
                process: true,
                status_csv: 'show',
                percent: 0,
            }, () => {
                formData.append('name_csv', $('#action-modal').val());
                this.props.postData(csvUrl, formData, headers);
            });
        }
    }

    handleChecked = (index, event) => {
        let headers = {'X-Requested-With': 'XMLHttpRequest'};
        this.props.postData(
            updateReceiveUrl,
            {index : index},
            headers,
            this.handleReloadData,
        );
    }

    getFileExtension = (fileName) => {
        return (/[.]/.exec(fileName)) ? /[^.]+$/.exec(fileName) : undefined;
    }

    handleImportCancel = () => {
        $('#issue-modal').modal('hide');
        this.flg_check = false;
        this.setState({
            attached_file_path: '',
            attached_file: '',
            status_csv: false,
            message: '',
            finish: 0,
            error: 0,
            msg: '',
        }, () => {
            $("input[name='browser_attached_file_path']").val('');
            this.handleReloadData();
        });
    }

    handleImport = (event, name) => {
        $('#action-modal').val(name);
        $('#modalTitle').text( name === 'mitsu_bank_csv' ? trans('messages.import_bank_csv') : trans('messages.import_rakuten_bank_csv'));
        $('#issue-modal').modal({backdrop: 'static', show: true});
    }

    componentWillReceiveProps(nextProps) {
        let dataResponse = nextProps.dataResponse;
        if (!_.isEmpty(dataResponse) && this.flg_check) {
            if(dataResponse.flg === 1) {
                let formData   = new FormData();
                let headers = {'X-Requested-With': 'XMLHttpRequest'};
                Object.keys(dataResponse).map((item) => {
                    formData.append(item, dataResponse[item]);
                });
                formData.append('type', $('#action-modal').val());
                this.setState({
                    process: true,
                    status_csv: 'show',
                    percent: (dataResponse.currPage/dataResponse.timeRun) * 100,
                }, () => {
                    this.props.postData(importUrl, formData, headers);
                });
            } else {

                let dataSet = {
                    process: false,
                    status_csv: false,
                };

                if (dataResponse.msg === 'Finish') {
                    $.notify({
                        icon: 'glyphicon glyphicon-warning-sign',
                        message: 'Import success!!!',
                    },{
                        type: 'info',
                        delay: 2000
                    });
                    dataSet['msg'] = '';
                    this.setState({...dataResponse, attached_file_path:'', ...dataSet}, () => {
                        if (dataResponse.error === 0) {
                            $('#issue-modal').modal('hide');
                            this.handleReloadData();
                        }
                        $("input[name='browser_attached_file_path']").val('');
                        $("input[name='browser_attached_file_path']").val('');
                    });
                } else {
                    this.setState({...dataResponse,...dataSet});
                }
            }
        }
        if (dataResponse.msg === 'Finish') {
            this.flg_check = false;
        }
    }
    render() {
        let nf = new Intl.NumberFormat();
        let dataSource = this.props.dataSource;
        let index = 0;
        let mallOptions = {};
        let orderStatusOptions = {};
        let totalItems = 0;
        let itemsPerPage = 20;
        if (!_.isEmpty(dataSource)) {
            index = dataSource.data.from - 1;
            mallOptions = dataSource.mallData;
            orderStatusOptions = dataSource.orderStatusData;
            totalItems   = dataSource.data.total;
            itemsPerPage = dataSource.data.per_page;
        }
        return ( (!_.isEmpty(dataSource)) &&
            <div>
                <Loading className="loading" />
                <div className="box">
                    <div className="box-header">
                        <div className="row">
                            <div className="col-md-12">
                                <h4>{trans('messages.search_title')}</h4>
                            </div>
                            <div className="row">
                                <FORM>
                                    <div className="col-md-12">
                                        <SELECT
                                            id="name_jp"
                                            name="name_jp"
                                            groupClass="form-group col-md-1-25"
                                            labelClass=""
                                            labelTitle={trans('messages.name_jp')}
                                            handleChangeMultiSelect={this.handleChangeMultiSelect}
                                            valueMulti={this.state.name_jp||''}
                                            options={mallOptions}
                                            multiple="multiple"
                                            className="form-control input-sm" />
                                        <LABEL
                                            groupClass="form-group col-md-1-5 clear-padding"
                                            labelTitle={trans('messages.payment_order_date_from')}>
                                            <DatePicker
                                                placeholderText="YYYY-MM-DD"
                                                dateFormat="YYYY-MM-DD"
                                                locale="ja"
                                                name="order_date_from"
                                                className="form-control input-sm"
                                                key="order_date_from"
                                                id="order_date_from"
                                                selected={this.state.order_date_from ? moment(this.state.order_date_from) : null}
                                                onChange={(date,name, e) => this.handleChangeDate(date, 'order_date_from', e)} />
                                        </LABEL>
                                        <div className="col-md-0-25 hidden-xs hidden-sm"><span className="to-from-child">～</span></div>
                                        <LABEL
                                            groupClass="form-group col-md-1-5 clear-padding"
                                            labelTitle={trans('messages.payment_order_date_to')}>
                                            <DatePicker
                                                placeholderText="YYYY-MM-DD"
                                                dateFormat="YYYY-MM-DD"
                                                locale="ja"
                                                name="order_date_to"
                                                className="form-control col-md-12 input-sm"
                                                key="order_date_to"
                                                id="order_date_to"
                                                selected={this.state.order_date_to ? moment(this.state.order_date_to) : null}
                                                onChange={(date, name, e) => this.handleChangeDate(date, 'order_date_to', e)} />
                                        </LABEL>
                                        <INPUT
                                            name="request_price_from"
                                            groupClass="form-group col-md-1-75"
                                            labelTitle={trans('messages.request_price_from')}
                                            onChange={this.handleChange}
                                            value={this.state.request_price_from ||''}
                                            className="form-control input-sm" />
                                        <div className="col-md-0-25 hidden-xs hidden-sm"><span className="to-from-child">～</span></div>
                                        <INPUT
                                            name="request_price_to"
                                            groupClass="form-group col-md-1-75"
                                            labelTitle={trans('messages.request_price_to')}
                                            onChange={this.handleChange}
                                            value={this.state.request_price_to ||''}
                                            className="form-control input-sm" />
                                        <SELECT
                                            id="order_status"
                                            name="order_status"
                                            groupClass="form-group col-md-1-75"
                                            labelClass=""
                                            labelTitle={trans('messages.payment_order_sub_status')}
                                            handleChangeMultiSelect={this.handleChangeMultiSelect}
                                            valueMulti={this.state.order_status||''}
                                            options={orderStatusOptions}
                                            multiple="multiple"
                                            className="form-control input-sm" />
                                        <INPUT
                                            name="receive_id"
                                            groupClass="form-group col-md-1-75"
                                            labelTitle={trans('messages.receive_id')}
                                            onChange={this.handleChange}
                                            value={this.state.receive_id ||''}
                                            className="form-control input-sm" />
                                    </div>

                                    <div className="col-md-12">
                                        <INPUT
                                            name="full_name"
                                            groupClass="form-group col-md-1-75"
                                            labelTitle={trans('messages.full_name')}
                                            onChange={this.handleChange}
                                            value={this.state.full_name ||''}
                                            className="form-control input-sm" />
                                        <INPUT
                                            name="payment_account"
                                            groupClass="form-group col-md-1-75"
                                            labelTitle={trans('messages.payment_account_num')}
                                            onChange={this.handleChange}
                                            value={this.state.payment_account ||''}
                                            className="form-control input-sm" />
                                        <SELECT
                                            name="per_page"
                                            groupClass="form-group col-md-1 per-page pull-right-md"
                                            labelClass="pull-left"
                                            labelTitle={trans('messages.per_page')}
                                            onChange={this.handleChange}
                                            value={this.state.per_page||''}
                                            options={{20:20, 40:40, 60:60, 80:80, 100:100}}
                                            className="form-control input-sm" />
                                    </div>
                                </FORM>
                            </div>
                        </div>
                        <div className="row">
                            <div className="col-md-6 col-xs-12">
                                <div className="pull-left pull-xs-right margin-bottom-btn">
                                    <BUTTON className="btn btn-primary margin-element" onClick={(e) => this.handleImport(e, 'mitsu_bank_csv')}>{trans('messages.import_bank_csv')}</BUTTON>
                                    <BUTTON className="btn btn-primary" onClick={(e) => this.handleImport(e, 'rakuten_bank_csv')} >{trans('messages.import_rakuten_bank_csv')}</BUTTON>
                                </div>
                            </div>
                            <div className="col-md-6 col-xs-12">
                                <BUTTON className="btn btn-danger pull-right" onClick={this.handleClearClick}>{trans('messages.btn_reset')}</BUTTON>
                                <BUTTON className="btn btn-primary pull-right margin-element" onClick={this.handleReloadData} >{trans('messages.btn_search')}</BUTTON>
                            </div>
                        </div>
                    </div>

                    <div className="box-body">
                        <div className="row">
                            <div className="col-md-12">
                                {(totalItems/itemsPerPage > 1) &&
                                    <div className="row">
                                        <div className="col-md-2-5"></div>
                                        <div className="col-md-7 text-center">
                                            <PaginationContainer handleClickPage={this.handleClickPage}/>
                                        </div>
                                        <div className="col-md-2-5 text-right line-height-md">
                                            {dataSource.data.from}件 - {dataSource.data.to}件 【全{dataSource.data.total}件】
                                        </div>
                                    </div>
                                }
                                <div className="table-responsive">
                                <TABLE className="table table-striped table-custom">
                                    <THEAD>
                                        <TR>
                                            <TH className="visible-xs visible-sm"></TH>
                                            <TH className="hidden-xs hidden-sm text-center text-middle">
                                                #
                                            </TH>
                                            <TH className="text-center  text-middle col-max-min-60">
                                                <SortComponent
                                                    name="sort_name_jp"
                                                    onClick={this.handleSortClick}
                                                    currentSort={(this.state.sort_name_jp)?this.state.sort_name_jp:'none'}
                                                    title={trans('messages.name_jp')} />
                                            </TH>
                                            <TH className="hidden-xs hidden-sm text-center  text-middle col-max-min-100">
                                                <SortComponent
                                                    name="sort_order_date"
                                                    onClick={this.handleSortClick}
                                                    currentSort={(this.state.sort_order_date)?this.state.sort_order_date:'none'}
                                                    title={trans('messages.payment_order_date')} />
                                            </TH>
                                            <TH className="text-center text-middle col-max-min-100">
                                                <SortComponent
                                                    name="sort_received_order_id"
                                                    onClick={this.handleSortClick}
                                                    currentSort={(this.state.sort_received_order_id)?this.state.sort_received_order_id:'none'}
                                                    title={trans('messages.payment_received_order_id')} />
                                            </TH>
                                            <TH className="hidden-xs hidden-sm text-center  text-middle col-max-min-100">
                                                <SortComponent
                                                    name="sort_full_name"
                                                    onClick={this.handleSortClick}
                                                    currentSort={(this.state.sort_full_name)?this.state.sort_full_name:'none'}
                                                    title={trans('messages.payment_full_name')} />
                                            </TH>
                                            <TH className="hidden-xs hidden-sm text-center  text-middle col-max-min-100">
                                                <SortComponent
                                                    name="sort_order_status"
                                                    onClick={this.handleSortClick}
                                                    currentSort={(this.state.sort_order_status)?this.state.sort_order_status:'none'}
                                                    title={trans('messages.payment_order_sub_status')} />
                                            </TH>
                                            <TH className="hidden-xs hidden-sm text-center  text-middle col-max-min-100">
                                                <SortComponent
                                                    name="sort_request_price"
                                                    onClick={this.handleSortClick}
                                                    currentSort={(this.state.sort_request_price)?this.state.sort_request_price:'none'}
                                                    title={trans('messages.request_price')} />
                                            </TH>
                                            <TH className="hidden-xs hidden-sm text-center  text-middle col-max-min-100">
                                                <SortComponent
                                                    name="sort_request_payment_date"
                                                    onClick={this.handleSortClick}
                                                    currentSort={(this.state.sort_request_payment_date)?this.state.sort_request_payment_date:'none'}
                                                    title={trans('messages.request_payment_date')} />
                                            </TH>
                                            <TH className="hidden-xs hidden-sm text-center  text-middle col-max-min-100">
                                                <SortComponent
                                                    name="sort_pay_price"
                                                    onClick={this.handleSortClick}
                                                    currentSort={(this.state.sort_pay_price)?this.state.sort_pay_price:'none'}
                                                    title={trans('messages.pay_price')} />
                                            </TH>
                                            <TH className="hidden-xs hidden-sm text-center  text-middle col-max-min-100">
                                                <SortComponent
                                                    name="sort_payment_date"
                                                    onClick={this.handleSortClick}
                                                    currentSort={(this.state.sort_payment_date)?this.state.sort_payment_date:'none'}
                                                    title={trans('messages.payment_date')} />
                                            </TH>
                                            <TH className="hidden-xs hidden-sm text-center  text-middle col-max-min-100">
                                                <SortComponent
                                                    name="sort_remain_amount"
                                                    onClick={this.handleSortClick}
                                                    currentSort={(this.state.sort_remain_amount)?this.state.sort_remain_amount:'none'}
                                                    title={trans('messages.remain_amount')} />
                                            </TH>
                                            <TH className="text-center  text-middle col-max-min-100">{trans('messages.payment_check_box')}</TH>
                                        </TR>
                                    </THEAD>
                                    <TBODY>
                                        {
                                            (!_.isEmpty(dataSource.data.data))?
                                            dataSource.data.data.map((item) => {
                                                index++;

                                                let formatOrderDate = !_.isEmpty(item.order_date) ? moment.utc(item.order_date, "YYYY-MM-DD").format("MM/DD") : '';
                                                let formatPaymentDate = !_.isEmpty(item.payment_date) ? moment.utc(item.payment_date, "YYYY-MM-DD").format("MM/DD") : '';
                                                let formatPaymentReDate = !_.isEmpty(item.payment_request_date) ? moment.utc(item.payment_request_date, "YYYY-MM-DD").format("MM/DD") : '';
                                                let classRow = (index%2 ===0) ? 'odd' : 'even';
                                                return (
                                                    [
                                                    <TR key={"tr" + index} className={classRow}>
                                                        <TD className="visible-xs visible-sm text-center">
                                                            <b className="show-info">
                                                                <i className="fa fa-angle-double-down" aria-hidden="true"></i>
                                                            </b>
                                                        </TD>
                                                        <TD className="hidden-xs hidden-sm text-center">
                                                           {nf.format(index)}
                                                        </TD>
                                                        <TD className="cut-text" title={item.name_jp}>{item.name_jp}</TD>
                                                        <TD className="hidden-xs hidden-sm" title={formatOrderDate}>{formatOrderDate}</TD>
                                                        <TD className="cut-text" title={item.received_order_id}>{item.received_order_id}</TD>
                                                        <TD className="hidden-xs hidden-sm cut-text text-long-batch max-w-200" title={item.full_name}>{item.full_name}</TD>
                                                        <TD className="hidden-xs hidden-sm cut-text text-center" title={item.order_status}>{item.order_status}</TD>
                                                        <TD className="hidden-xs hidden-sm cut-text text-right" title={nf.format(item.request_price)}>{nf.format(item.request_price)}</TD>
                                                        <TD className="hidden-xs hidden-sm cut-text" title={formatPaymentReDate}>{formatPaymentReDate}</TD>
                                                        <TD className="hidden-xs hidden-sm cut-text text-right" title={nf.format(item.payment_price)}>{nf.format(item.payment_price)}</TD>
                                                        <TD className="hidden-xs hidden-sm cut-text" title={formatPaymentDate}>{formatPaymentDate}</TD>
                                                        <TD className="hidden-xs hidden-sm cut-text text-right" title={nf.format(item.remain_amount)}>{nf.format(item.remain_amount)}</TD>
                                                        <TD className="text-center">
                                                            <CHECKBOX_CUSTOM
                                                                key={item.index + Math.random()}
                                                                defaultChecked={item.index ? true : false}
                                                                onChange={(e) => this.handleChecked(item.index, e)}
                                                             />
                                                        </TD>
                                                    </TR>,
                                                    <TR key={"trhide" + index} className="hiden-info" style={{display:"none"}}>
                                                        <TD colSpan="10" className="width-span1">
                                                            <ul id={"temp" + item.mail_id} >
                                                                <li> <b>{trans('messages.payment_order_date')}</b> : {formatOrderDate}</li>
                                                                <li> <b>{trans('messages.payment_received_order_id')}</b> : {item.received_order_id}</li>
                                                                <li> <b>{trans('messages.payment_full_name')}</b> : {item.full_name}</li>
                                                                <li> <b>{trans('messages.payment_order_sub_status')}</b> : {item.order_status}</li>
                                                                <li> <b>{trans('messages.request_price')}</b> : {item.request_price}</li>
                                                                <li> <b>{trans('messages.request_payment_date')}</b> : {formatPaymentReDate}</li>
                                                                <li> <b>{trans('messages.pay_price')}</b> : {item.payment_price}</li>
                                                                <li> <b>{trans('messages.payment_date')}</b> : {formatPaymentDate}</li>
                                                                <li> <b>{trans('messages.payment_name')}</b> : {item.remain_amount}</li>
                                                            </ul>
                                                        </TD>
                                                        </TR>
                                                   ]

                                                );
                                                if (index === dataSource.data.to) {
                                                   index = 0;
                                                }
                                            })
                                            :
                                            <TR><TD colSpan="12" className="text-center">{trans('messages.no_data')}</TD></TR>
                                        }
                                    </TBODY>
                                </TABLE>
                                </div>
                                <div className="text-center">
                                    <PaginationContainer handleClickPage={this.handleClickPage}/>
                                </div>
                                <ImportCsv
                                    handleChooseFile={this.handleChooseFile}
                                    handleImportSubmit={this.handleImportSubmit}
                                    value={this.state.attached_file_path ? this.state.attached_file_path : ''}
                                    error={this.state.error}
                                    filename={this.state.filename}
                                    handleImportCancel={this.handleImportCancel}
                                    process={this.state.process ? true : false}
                                    status={this.state.status_csv}
                                    percent={this.state.percent}
                                    hasError={(this.state.message || this.state.msg) ? true : false}
                                    errorMessage={this.state.message || this.state.msg}
                                />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}
const mapStateToProps = (state) => {
  return {
    dataSource: state.commonReducer.dataSource,
    dataResponse: state.commonReducer.dataResponse
  }
}
const  mapDispatchToProps = (dispatch) => {
  let ignore = ['attached_file', 'attached_file_path','message','flg', 'process', 'msg', 'timeRun', 'currPage', 'total', 'filename', 'error','percent', 'status_csv', 'url', 'reload', 'params', 'headers', 'count','realFilename','type','fileCorrect', 'finish', 'checkAfter'];
  return {
    reloadData: (url, params, pushHistory, ignoreFields) => { dispatch(Action.reloadData(url, params, pushHistory, ignore)) },
    postData: (url, params, headers,callback,callParam) => { dispatch(Action.postData(url, params, headers,callback,callParam)) }
  }
}
export default connect(mapStateToProps, mapDispatchToProps)(ListPaymentManagement)
