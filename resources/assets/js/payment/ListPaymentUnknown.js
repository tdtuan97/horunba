import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import * as Action from '../common/actions';
import * as helpers from '../common/helpers';
import * as queryString from 'query-string';
import createHistory from 'history/createBrowserHistory';
import BUTTON from '../common/components/form/BUTTON';
import FORM from '../common/components/form/FORM';
import INPUT from '../common/components/form/INPUT';
import SELECT from '../common/components/form/SELECT';
import LABEL from '../common/components/form/LABEL';
import CHECKBOX_CUSTOM from '../common/components/form/CHECKBOX_CUSTOM';
import TABLE from '../common/components/table/TABLE';
import THEAD from '../common/components/table/THEAD';
import TBODY from '../common/components/table/TBODY';
import TR from '../common/components/table/TR';
import TH from '../common/components/table/TH';
import TD from '../common/components/table/TD';
import SortComponent from '../common/components/table/SortComponent';
import PaginationContainer from '../common/containers/PaginationContainer'
import MessagePopup from '../common/components/common/MessagePopup';
import ImportCsv from './csv/ImportCsv';
import DatePicker from 'react-datepicker';
import moment from 'moment';
import 'react-datepicker/dist/react-datepicker.css';
import axios from 'axios';
import Loading from '../common/components/common/Loading';
import Popup from './containers/Popup';
import PopupInformDelete from './containers/PopupInformDelete';

class ListPaymentUnknown extends Component {
    constructor(props) {
        super(props);
        const stringParams = helpers.parseStringToParams(props.location.search);
        this.state = {}
        if (!_.isEmpty(stringParams)) {
            this.state = stringParams;
        }
        this.props.reloadData(dataListUrl, stringParams);
    }
    handleChange = (event) => {
        const target = event.target;
        let value = target.value;
        if (value === '_none') {
            value = '';
        }
        let name = target.name;
        let headers = {'X-Requested-With': 'XMLHttpRequest'};
        this.setState({
            [name]: value,
        },() => {
            if (name === 'per_page') {
                this.handleReloadData();
            }
        });
    }

    handleChangeDate = function(date, name, event) {
        let dateValue = moment(date);
        if (!dateValue.isValid()) {
            dateValue = undefined;
        } else {
            dateValue = dateValue.format('YYYY-MM-DD');
        }
        this.setState({
          [name]: dateValue,
        });
    }

    handleClearClick = (event) => {
        Object.keys(this.state).map((key) => {
            if (key !== 'per_page') {
                if (key.startsWith('receive_id_')) {
                    $('input[name=' + key + ']').val('');
                } else {
                    delete this.state[key];
                }
            }
        });
        this.handleReloadData();
    }
    handleClickPage = (page) => {
        Object.keys(this.state).map((key) => {
            if (key.startsWith('receive_id_')) {
                delete this.state[key];
            }
        });
        this.setState({
            page: page
        },() => {
            this.props.reloadData(dataListUrl, this.state, true);
        });
    }
    handleSortClick = (name, nextSort) => {
        Object.keys(this.state).map((key) => {
            if (key.startsWith('sort_')) {
                delete this.state[key];
            }
        });
        if (nextSort === 'none') {
            delete this.state[name];
            this.handleReloadData();
        } else {
            this.setState({[name]: nextSort}, () => {
                this.handleReloadData();
            });
        }
    }
    handleReloadData = () => {
        let params = {};
        let dataSource = this.props.dataSource;
        let tmpParams = this.state;
        let removeInput = {};
        let arrKey = ['request_price', 'order_date', 'price', 'full_name'];
        Object.keys(tmpParams).map(key => {
            if (tmpParams[key] !== ""
                && tmpParams[key] !== null
                && key !== 'baseUrl'
                && !key.startsWith('receive_id_')
                && key !== 'page') {
                    params[key] = tmpParams[key];
            } else {
                delete params[key];
            }
            if (key.startsWith('receive_id_')) {
                let id = key.replace('receive_id_', '');
                arrKey.map((item) => {
                    $('#' + item + '_' + id).text('');
                });
                removeInput[key] = '';
            }
        });

        this.setState(removeInput,() => {
            this.props.reloadData(dataListUrl, params, true , ['index_key']);
        });

    }
    handleBeforeDelete = (index,event) => {
        $('#access-memo-delete-modal').modal('show');
        this.state.index_key = index
    }
    handleDelete = () => {
        $('#access-memo-delete-modal').modal('show');
        this.props.postData(deleteUrl, {'index_key' : this.state.index_key},
                    {'X-Requested-With': 'XMLHttpRequest'},
                    this.handleReloadData
                    );
        $('#access-memo-delete-modal').modal('hide');
    }
    componentDidUpdate() {
        if (!_.isEmpty(this.props.dataSource)) {
            $('.loading').hide();
        }
        dragscroll.reset();
    }
    componentWillMount() {
        window.onpopstate = (event) => {
            location.reload();
        };
    }
    handleSubmitInput(id, payment_code, payment_price, e) {
        let state = this.state;

        let headers = {'X-Requested-With': 'XMLHttpRequest'};
        axios.post(saveReceive, { index: id, 'receive_id' : state['receive_id_' + id],  payment_code: payment_code, payment_price: payment_price}, {headers : headers}
        ).then(response => {
            if (response.data.flg === 0) {
                this.handleReloadData();
            } else if (response.data.flg === 1) {
                $('.message-popup').text(response.data.messages);
                $('#access-memo-modal').modal('show');
            } else if (response.data.flg === 2) {
                $('.message-popup').text(response.data.messages);
                $('#access-memo-modal').modal('show');
            }
        }).catch(error => {
            throw(error);
        });
    }
    handleChangeInput = (event) => {
        const target = event.target;
        let value = target.value;
        let name = target.name;
        let headers = {'X-Requested-With': 'XMLHttpRequest'};
        let data = {};
        let arrKey = ['request_price', 'order_date', 'price', 'full_name'];
        let receiveID = this.state[name];
        if ((event.which === 13 || event.type === 'blur') && receiveID !== value && value !== '') {
            this.setState({
                [name]: value,
            },() => {
                $(".loading").show();
                axios.post(getOrderReceive, { [name]: value }, {headers : headers}
                ).then(response => {
                    $(".loading").hide();
                    arrKey.map((item) => {
                        $('#' + item + '_' + response.data.index).text('');
                    });
                    let check = false;
                    for (let key in response.data.info) {
                        check = true;
                        if (key.includes('order_date_')) {
                            $('#' + key).text(!_.isEmpty(response.data.info[key]) ? moment.utc(response.data.info[key], "YYYY-MM-DD").format("MM/DD") : '');
                        } else {
                            $('#' + key).text(response.data.info[key]);
                        }
                    }
                    $('#' + name).removeClass('btn-primary').removeClass('bg-purple');
                    if (!check) {
                        $('#' + name).prop('disabled', true);
                        $('#' + name).addClass('bg-purple');
                    } else {
                        $('#' + name).prop('disabled', false);
                        $('#' + name).addClass('btn-primary');
                    }
                }).catch(error => {
                    $(".loading").hide();
                    throw(error);
                });
            });
        }
    }

    handleCancelInput = (id, e) => {
        this.setState({
            ['receive_id_' + id]: '',
        });
        $('#full_name_' + id).text('');
        $('#request_price_' + id).text('');
        $('#price_' + id).text('');
        $('#order_date_' + id).text('');
        $('input[name=receive_id_' + id + ']').val('');
    }
    render() {
        let nf = new Intl.NumberFormat();
        let dataSource = this.props.dataSource;
        let index = 0;
        let totalItems = 0;
        let itemsPerPage = 20;
        if (!_.isEmpty(dataSource)) {
            index = dataSource.data.from - 1;
            totalItems   = dataSource.data.total;
            itemsPerPage = dataSource.data.per_page;
        }
        return ( (!_.isEmpty(dataSource)) &&
            <div>
                <Loading className="loading" />
                <div className="box">
                    <div className="box-header">
                        <div className="row">
                            <div className="col-md-12">
                                <h4>{trans('messages.search_title')}</h4>
                            </div>
                            <FORM>
                                <LABEL
                                    groupClass="form-group col-md-1-75"
                                    labelTitle={trans('messages.unknown_payment_date_from')}>
                                    <DatePicker
                                        placeholderText="YYYY-MM-DD"
                                        dateFormat="YYYY-MM-DD"
                                        locale="ja"
                                        name="payment_date_from"
                                        className="form-control input-sm"
                                        key="payment_date_from"
                                        id="payment_date_from"
                                        selected={this.state.payment_date_from ? moment(this.state.payment_date_from) : null}
                                        onChange={(date,name, e) => this.handleChangeDate(date, 'payment_date_from', e)} />
                                </LABEL>
                                <div className="col-md-0-25 hidden-xs hidden-sm"><span className="to-from-child">～</span></div>
                                <LABEL
                                    groupClass="form-group col-md-1-75"
                                    labelTitle={trans('messages.unknown_payment_date_to')}>
                                    <DatePicker
                                        placeholderText="YYYY-MM-DD"
                                        dateFormat="YYYY-MM-DD"
                                        locale="ja"
                                        name="payment_date_to"
                                        className="form-control col-md-12 input-sm"
                                        key="payment_date_to"
                                        id="payment_date_to"
                                        selected={this.state.payment_date_to ? moment(this.state.payment_date_to) : null}
                                        onChange={(date, name, e) => this.handleChangeDate(date, 'payment_date_to', e)} />
                                </LABEL>
                                <INPUT
                                    name="payment_price_from"
                                    groupClass="form-group col-md-1-5"
                                    labelTitle={trans('messages.unknown_payment_price_from')}
                                    onChange={this.handleChange}
                                    value={this.state.payment_price_from ||''}
                                    className="form-control input-sm" />
                                <div className="col-md-0-25 hidden-xs hidden-sm"><span className="to-from-child">～</span></div>
                                <INPUT
                                    name="payment_price_to"
                                    groupClass="form-group col-md-1-5"
                                    labelTitle={trans('messages.unknown_payment_price_to')}
                                    onChange={this.handleChange}
                                    value={this.state.payment_price_to ||''}
                                    className="form-control input-sm" />
                                <INPUT
                                    name="account_num_from"
                                    groupClass="form-group col-md-1-5"
                                    labelTitle={trans('messages.unknown_account_num_from')}
                                    onChange={this.handleChange}
                                    value={this.state.account_num_from ||''}
                                    className="form-control input-sm" />
                                <div className="col-md-0-25 hidden-xs hidden-sm"><span className="to-from-child">～</span></div>
                                <INPUT
                                    name="account_num_to"
                                    groupClass="form-group col-md-1-75"
                                    labelTitle={trans('messages.unknown_account_num_to')}
                                    onChange={this.handleChange}
                                    value={this.state.account_num_to ||''}
                                    className="form-control input-sm" />
                                <SELECT
                                    name="per_page"
                                    groupClass="form-group col-md-1-25 per-page pull-right-md"
                                    labelClass="pull-left"
                                    labelTitle={trans('messages.per_page')}
                                    onChange={this.handleChange}
                                    value={this.state.per_page||''}
                                    options={{20:20, 40:40, 60:60, 80:80, 100:100}}
                                    className="form-control input-sm" />
                            </FORM>
                        </div>
                        <div className="row">
                            <div className="col-md-12 col-xs-12">
                                <BUTTON className="btn btn-danger pull-right" onClick={this.handleClearClick}>{trans('messages.btn_reset')}</BUTTON>
                                <BUTTON className="btn btn-primary pull-right margin-element" onClick={this.handleReloadData} >{trans('messages.btn_search')}</BUTTON>
                            </div>
                        </div>
                    </div>

                    <div className="box-body">
                        <div className="row">
                            <div className="col-md-12">
                                {(totalItems/itemsPerPage > 1) &&
                                    <div className="row">
                                        <div className="col-md-2-5"></div>
                                        <div className="col-md-7 text-center">
                                            <PaginationContainer handleClickPage={this.handleClickPage}/>
                                        </div>
                                        <div className="col-md-2-5 text-right line-height-md">
                                            {dataSource.data.from}件 - {dataSource.data.to}件 【全{dataSource.data.total}件】
                                        </div>
                                    </div>
                                }
                                <div className="table-responsive">
                                <TABLE className="table table-striped payment-unknow table-custom">
                                    <THEAD>
                                        <TR>
                                            <TH className="visible-xs visible-sm"></TH>
                                            <TH className="hidden-xs hidden-sm text-center">
                                                #
                                            </TH>
                                            <TH className="text-center unknown-payment-name">
                                                <SortComponent
                                                    name="sort_payment_name"
                                                    onClick={this.handleSortClick}
                                                    currentSort={(this.state.sort_payment_name)?this.state.sort_payment_name:'none'}
                                                    title={trans('messages.unknown_payment_name')} />
                                            </TH>
                                            <TH className="hidden-xs hidden-sm text-center">
                                                <SortComponent
                                                    name="sort_payment_price"
                                                    onClick={this.handleSortClick}
                                                    currentSort={(this.state.sort_payment_price)?this.state.sort_payment_price:'none'}
                                                    title={trans('messages.pay_price')} />
                                            </TH>
                                            <TH className="hidden-xs hidden-sm text-center">
                                                <SortComponent
                                                    name="sort_payment_date"
                                                    onClick={this.handleSortClick}
                                                    currentSort={(this.state.sort_payment_date)?this.state.sort_payment_date:'none'}
                                                    title={trans('messages.payment_date')} />
                                            </TH>
                                            <TH className="hidden-xs hidden-sm text-center">
                                                <SortComponent
                                                    name="sort_relative_time"
                                                    onClick={this.handleSortClick}
                                                    currentSort={(this.state.sort_relative_time)?this.state.sort_relative_time:'none'}
                                                    title={trans('messages.payment_time')} />
                                            </TH>
                                            <TH className="hidden-xs hidden-sm text-center">
                                                <SortComponent
                                                    name="sort_payment_code"
                                                    onClick={this.handleSortClick}
                                                    currentSort={(this.state.sort_payment_code)?this.state.sort_payment_code:'none'}
                                                    title={trans('messages.payment_bank')} />
                                            </TH>
                                            <TH className="hidden-xs hidden-sm text-center">
                                                <SortComponent
                                                    name="sort_receive_account_num"
                                                    onClick={this.handleSortClick}
                                                    currentSort={(this.state.sort_receive_account_num)?this.state.sort_receive_account_num:'none'}
                                                    title={trans('messages.payment_account_num')} />
                                            </TH>
                                            <TH className="hidden-xs hidden-sm text-center">
                                                {trans('messages.receive_order_id')}
                                            </TH>
                                            <TH className="text-center" />
                                            <TH className="text-center" />
                                            <TH className="text-center" />
                                            <TH className="text-center unknown-payment-name">
                                                {trans('messages.customer_name')}
                                            </TH>
                                            <TH className="text-center">
                                                {trans('messages.request_price')}
                                            </TH>
                                            <TH className="text-center">
                                                {trans('messages.remain_amount')}
                                            </TH>
                                            <TH className="text-center">
                                                {trans('messages.order_date_1')}
                                            </TH>
                                        </TR>
                                    </THEAD>
                                    <TBODY>
                                        {
                                            (!_.isEmpty(dataSource.data.data))?
                                            dataSource.data.data.map((item) => {
                                                index++;
                                                let classRow = (index%2 ===0) ? 'odd' : 'even';
                                                return (
                                                    [
                                                    <TR key={"tr" + item.index} className={classRow}>
                                                        <TD className="visible-xs visible-sm text-center">
                                                            <b className="show-info">
                                                                <i className="fa fa-angle-double-down" aria-hidden="true"></i>
                                                            </b>
                                                        </TD>
                                                        <TD className="hidden-xs hidden-sm cut-text text-center">
                                                           {nf.format(index)}
                                                        </TD>
                                                        <TD className="text-long-batch" title={item.payment_name}>{item.payment_name}</TD>
                                                        <TD className="hidden-xs hidden-sm text-right" title={nf.format(item.payment_price)}>{nf.format(item.payment_price)}</TD>
                                                        <TD className="hidden-xs hidden-sm cut-text" title={item.payment_date}>{item.payment_date}</TD>
                                                        <TD className="hidden-xs hidden-sm cut-text max-w-200" title={item.relative_time}>{item.relative_time}</TD>
                                                        <TD className="hidden-xs hidden-sm cut-text" title={item.payment_code }>{item.payment_code }</TD>
                                                        <TD className="hidden-xs hidden-sm cut-text" title={item.receive_account_num}>{item.receive_account_num}</TD>
                                                        <TD className="cut-text text-center">
                                                            <INPUT
                                                                key={"unk" + item.index}
                                                                name={'receive_id_' + item.index }
                                                                groupClass="w-100"
                                                                onKeyPress={this.handleChangeInput}
                                                                onBlur ={this.handleChangeInput}
                                                                className="form-control input-sm"
                                                                errorPopup={true}
                                                                hasError={(this.state.messages && this.state.messages['receive_id_' + item.index])?true:false}
                                                                errorMessage={(this.state.messages && this.state.messages['receive_id_' + item.index])?this.state.messages['receive_id_' + item.index]:""}/>
                                                        </TD>
                                                        <TD key={"btn" + this.state['receive_id_' + item.index]} className="text-center">
                                                            <BUTTON id={'receive_id_' + item.index} className="btn bg-purple" onClick={(e) => {this.handleSubmitInput(item.index, item.payment_method, item.payment_price, e);}} disabled={(!this.state['receive_id_' + item.index]) ? true : false} >{trans('messages.match_button')}</BUTTON>
                                                        </TD>
                                                        <TD key={"btn_cancel" + this.state['receive_id_' + item.index]} className="text-center">
                                                            <BUTTON id={'cancel_' + item.index} className="btn bg-purple" onClick={(e) => {this.handleCancelInput(item.index, e);}} disabled={(!this.state['receive_id_' + item.index]) ? true : false} >{trans('messages.btn_cancel')}</BUTTON>
                                                        </TD>
                                                        <TD key={"btn_delete" + this.state['receive_id_' + item.index]} className="text-center">
                                                            <BUTTON id={'delete_' + item.index} className="btn btn-danger" onClick={(e) => {this.handleBeforeDelete(item.index, e);}} >{trans('messages.btn_delete')}</BUTTON>
                                                        </TD>
                                                        <TD key={'full_name_' + item.index} className="text-long-batch" id={'full_name_' + item.index} />
                                                        <TD key={'request_price_' + item.index} className="text-center" id={'request_price_' + item.index} />
                                                        <TD key={'price_' + item.index} className="text-center" id={'price_' + item.index} />
                                                        <TD key={'order_date_' + item.index} className="text-center" id={'order_date_' + item.index} />
                                                    </TR>,
                                                    <TR key={"trhide" + index} className="hiden-info" style={{display:"none"}}>
                                                        <TD colSpan="10" className="width-span1">
                                                            <ul id={"temp" + item.mail_id} >
                                                                <li> <b>NO</b> : #{item.receive_id}</li>
                                                                <li> <b>{trans('messages.name_jp')}</b> : {item.payment_name}</li>
                                                                <li> <b>{trans('messages.payment_order_date')}</b> : {item.payment_price}</li>
                                                                <li> <b>{trans('messages.payment_received_order_id')}</b> : {item.payment_date}</li>
                                                                <li> <b>{trans('messages.payment_full_name')}</b> : {item.relative_time}</li>
                                                                <li> <b>{trans('messages.payment_order_sub_status')}</b> : {item.payment_code}</li>
                                                                <li> <b>{trans('messages.request_price')}</b> : {item.receive_account_num}</li>
                                                            </ul>
                                                        </TD>
                                                        </TR>
                                                   ]
                                                )
                                                if (index === dataSource.data.to) {
                                                   index = 0;
                                                }
                                            })
                                            :
                                            <TR><TD colSpan="13" className="text-center">{trans('messages.no_data')}</TD></TR>
                                        }
                                    </TBODY>
                                </TABLE>
                                </div>
                                <div className="text-center">
                                    <PaginationContainer handleClickPage={this.handleClickPage}/>
                                </div>
                            </div>
                        </div>
                        <Popup />
                        <PopupInformDelete  handleDelete={this.handleDelete} />
                    </div>
                </div>
            </div>
        )
    }
}
const mapStateToProps = (state) => {
  return {
    dataSource: state.commonReducer.dataSource,
    dataResponse: state.commonReducer.dataResponse
  }
}
const  mapDispatchToProps = (dispatch) => {
  const ignoreFields = [];
  return {
    reloadData: (url, params, pushHistory, ignoreFields) => { dispatch(Action.reloadData(url, params, pushHistory, ignoreFields)) },
    postData: (url, params, headers,callback,callParam) => { dispatch(Action.postData(url, params, headers,callback,callParam)) }
  }
}
export default connect(mapStateToProps, mapDispatchToProps)(ListPaymentUnknown)
