import React, { Component } from 'react';
import { connect } from 'react-redux';

import * as Action from '../common/actions';
import * as Helpers from '../common/helpers';
import * as queryString from 'query-string';

import Loading from '../common/components/common/Loading';
import FORM from '../common/components/form/FORM';
import INPUT from '../common/components/form/INPUT';
import SELECT from '../common/components/form/SELECT';
import TEXTAREA from '../common/components/form/TEXTAREA';
import UPLOAD from '../common/components/form/UPLOAD';
import BUTTON from '../common/components/form/BUTTON';
import MESSAGE_MODAL from '../common/containers/MessageModal';

class PostalSave extends Component {
    constructor(props) {
        super(props);
        this.state = {}
        let params = queryString.parse(props.location.search);
        if (params['index']) {
            this.state.accessFlg  = true;
            this.state.index      = params['index'];
            this.hanleRealtime();
        }
        this.props.reloadData(getFormDataUrl, this.state);
    }

    handleCancel = (event) => {
        window.location.href = baseUrl;
    }
    hanleRealtime() {
        if (!_.isEmpty(this.state.index)) {
            let params = {
                accessFlg : this.state.accessFlg,
                query : {
                        page_key : 'POSTAL-SAVE',
                        primary_key : 'index:' + this.state.index
                    },
                key :this.state.index
            };
            if (this.state.accessFlg !== false) {
                this.props.postRealTime(checkEditUrl, params, {'X-Requested-With': 'XMLHttpRequest'});
            }
        }
    }
    componentWillUnmount () {
        clearInterval(this.state);
    }
    componentDidMount() {
        setInterval(() => {
            this.hanleRealtime()
        }, 5000);

    }

    handleSubmit = (event) => {
        document.getElementsByName('btnAccept')[0].disabled = true;
        let headers = {'X-Requested-With': 'XMLHttpRequest'};

        let formData   = new FormData();
        let dataSource = this.props.dataSource;

        if ((!_.isEmpty(dataSource.data))) {
            Object.keys(dataSource.data).map((item) => {
                formData.append(item, dataSource.data[item]);
            });
        }
        if (!_.isEmpty(this.state)) {
            Object.keys(this.state).map((item) => {
                formData.append(item, this.state[item]);
            });
        }
        this.props.postData(saveUrl, formData, headers);
    }

    handleChange = (event) => {
        const target = event.target;
        const name = target.name;
        const value = target.type === 'checkbox' ? target.checked : target.value;
        this.setState({
            [name]: value
        });
    }

    componentWillReceiveProps(nextProps) {
        // Check get data update
        if (!_.isEmpty(nextProps.dataSource)) {
            if (!nextProps.dataSource.data) {
                window.location.href = baseUrl;
            }
        }
        // Check post data update
        if (!_.isEmpty(nextProps.dataResponse)) {
            if (nextProps.dataResponse.status === 1) {
                window.location.href = baseUrl;
            } else {
                document.getElementsByName('btnAccept')[0].disabled = false;
            }
        }
        // Check realtime update
        if (!_.isEmpty(nextProps.dataRealTime)) {
            this.setState(nextProps.dataRealTime);
        }
    }

    componentDidUpdate() {
        if (!_.isEmpty(this.props.dataSource)) {
            $('.loading').hide();
        }
        if (this.state.accessFlg === false) {
            $('#message-modal').modal('show');
            //We will auto redirect after 5 seconds
            setTimeout(() => {
                window.location.href = baseUrl;
            }, 5000);
        }
    }

    render() {
        let dataSource   = this.props.dataSource;
        let dataResponse = this.props.dataResponse;
        return ((!_.isEmpty(dataSource)) &&
        <div>
        <div className="box box-primary">
            <FORM
                className="form-horizontal">
                <div className="box-body">
                <Loading className="loading" />
                <div className="row">
                    <div className="col-md-6 col-md-offset-3 col-md-offset-right-3">
                        <INPUT
                            name="postal_code"
                            labelTitle={trans('messages.postal_code')}
                            className="form-control input-sm"
                            fieldGroupClass="col-md-8"
                            labelClass="col-md-4"
                            onChange={this.handleChange}
                            defaultValue={dataSource.data.postal_code}
                            errorPopup={true}
                            hasError={(dataResponse.message && dataResponse.message.postal_code)?true:false}
                            errorMessage={(dataResponse.message&&dataResponse.message.postal_code)?dataResponse.message.postal_code:""}/>
                        <INPUT
                            name="prefecture"
                            labelTitle={trans('messages.prefecture')}
                            className="form-control input-sm"
                            fieldGroupClass="col-md-8"
                            labelClass="col-md-4"
                            onChange={this.handleChange}
                            defaultValue={dataSource.data.prefecture}
                            errorPopup={true}
                            hasError={(dataResponse.message && dataResponse.message.prefecture)?true:false}
                            errorMessage={(dataResponse.message&&dataResponse.message.prefecture)?dataResponse.message.prefecture:""}/>
                        <INPUT
                            name="city"
                            labelTitle={trans('messages.city')}
                            className="form-control input-sm"
                            fieldGroupClass="col-md-8"
                            labelClass="col-md-4"
                            onChange={this.handleChange}
                            defaultValue={dataSource.data.city}
                            errorPopup={true}
                            hasError={(dataResponse.message && dataResponse.message.city)?true:false}
                            errorMessage={(dataResponse.message&&dataResponse.message.city)?dataResponse.message.city:""}/>
                        <INPUT
                            name="sub_address"
                            labelTitle={trans('messages.sub_address')}
                            className="form-control input-sm"
                            fieldGroupClass="col-md-8"
                            labelClass="col-md-4"
                            onChange={this.handleChange}
                            defaultValue={dataSource.data.sub_address}
                            errorPopup={true}
                            hasError={(dataResponse.message && dataResponse.message.sub_address)?true:false}
                            errorMessage={(dataResponse.message&&dataResponse.message.sub_address)?dataResponse.message.sub_address:""}/>
                        <INPUT
                            name="full_address"
                            labelTitle={trans('messages.full_address')}
                            className="form-control input-sm"
                            fieldGroupClass="col-md-8"
                            labelClass="col-md-4"
                            onChange={this.handleChange}
                            defaultValue={dataSource.data.full_address}
                            errorPopup={true}
                            hasError={(dataResponse.message && dataResponse.message.full_address)?true:false}
                            errorMessage={(dataResponse.message&&dataResponse.message.full_address)?dataResponse.message.full_address:""}/>
                        <SELECT
                            name="one_area_n_pos"
                            labelTitle={trans('messages.one_area_n_pos')}
                            className="form-control input-sm"
                            fieldGroupClass="col-md-8"
                            labelClass="col-md-4"
                            onChange={this.handleChange}
                            errorPopup={true}
                            defaultValue={(typeof dataSource.data.one_area_n_pos !== "undefined") ? dataSource.data.one_area_n_pos : ''}
                            hasError={(dataResponse.message && dataResponse.message.one_area_n_pos)?true:false}
                            errorMessage={(dataResponse.message&&dataResponse.message.one_area_n_pos)?dataResponse.message.one_area_n_pos:""}
                            options={{'':'----','0':'無', '1':'有'}}
                            />
                        <SELECT
                            name="one_pos_n_area"
                            labelTitle={trans('messages.one_pos_n_area')}
                            className="form-control input-sm"
                            fieldGroupClass="col-md-8"
                            labelClass="col-md-4"
                            onChange={this.handleChange}
                            errorPopup={true}
                            defaultValue={(typeof dataSource.data.one_pos_n_area !== "undefined") ? dataSource.data.one_pos_n_area : ''}
                            hasError={(dataResponse.message && dataResponse.message.one_pos_n_area)?true:false}
                            errorMessage={(dataResponse.message&&dataResponse.message.one_pos_n_area)?dataResponse.message.one_pos_n_area:""}
                            options={{'':'----',0:'無', 1:'有'}}
                            />
                        <SELECT
                            name="can_not_daihiki"
                            labelTitle={trans('messages.can_not_daihiki')}
                            className="form-control input-sm"
                            fieldGroupClass="col-md-8"
                            labelClass="col-md-4"
                            onChange={this.handleChange}
                            errorPopup={true}
                            defaultValue={(typeof dataSource.data.can_not_daihiki !== "undefined") ? dataSource.data.can_not_daihiki : ''}
                            hasError={(dataResponse.message && dataResponse.message.can_not_daihiki)?true:false}
                            errorMessage={(dataResponse.message&&dataResponse.message.can_not_daihiki)?dataResponse.message.can_not_daihiki:""}
                            options={{'':'----',0:'無', 1:'有'}}
                            />
                        <SELECT
                            name="can_not_order_time"
                            labelTitle={trans('messages.can_not_order_time')}
                            className="form-control input-sm"
                            fieldGroupClass="col-md-8"
                            labelClass="col-md-4"
                            onChange={this.handleChange}
                            errorPopup={true}
                            defaultValue={(typeof dataSource.data.can_not_order_time !== "undefined") ? dataSource.data.can_not_order_time : ''}
                            hasError={(dataResponse.message && dataResponse.message.can_not_order_time)?true:false}
                            errorMessage={(dataResponse.message&&dataResponse.message.can_not_order_time)?dataResponse.message.can_not_order_time:""}
                            options={{'':'----',0:'無', 1:'有'}}
                            />
                        <INPUT
                            name="deli_days"
                            labelTitle={trans('messages.deli_days')}
                            className="form-control input-sm"
                            fieldGroupClass="col-md-8"
                            labelClass="col-md-4"
                            onChange={this.handleChange}
                            defaultValue={dataSource.data.deli_days}
                            errorPopup={true}
                            type="number"
                            hasError={(dataResponse.message && dataResponse.message.deli_days)?true:false}
                            errorMessage={(dataResponse.message&&dataResponse.message.deli_days)?dataResponse.message.deli_days:""}/>
                        <INPUT
                            name="add_ship_charge"
                            labelTitle={trans('messages.add_ship_charge')}
                            className="form-control input-sm"
                            fieldGroupClass="col-md-8"
                            labelClass="col-md-4"
                            type="number"
                            onChange={this.handleChange}
                            defaultValue={dataSource.data.add_ship_charge}
                            errorPopup={true}
                            hasError={(dataResponse.message && dataResponse.message.add_ship_charge)?true:false}
                            errorMessage={(dataResponse.message&&dataResponse.message.add_ship_charge)?dataResponse.message.add_ship_charge:""}/>

                    </div>
                </div>
                <div className="row">
                    <div className="col-md-6 col-md-offset-3 col-md-offset-right-3">
                    <BUTTON className="btn bg-purple input-sm btn-larger pull-right" type="button" onClick={this.handleCancel}>{trans('messages.btn_cancel')}</BUTTON>
                    <BUTTON name="btnAccept" className="btn btn-success input-sm btn-larger pull-right margin-element" type="button" onClick={this.handleSubmit}>{trans('messages.btn_accept')}</BUTTON>
                    </div>
                </div>
            </div>
            </FORM>
            <MESSAGE_MODAL
                message={this.state.message || ''}
                title={trans('messages.deny_edit')}
                baseUrl={baseUrl}
            />
        </div>
        </div>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        dataSource: state.commonReducer.dataSource,
        dataResponse: state.commonReducer.dataResponse,
        dataRealTime: state.commonReducer.dataRealTime
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        reloadData: (url, params) => { dispatch(Action.detailData(url, params)) },
        postData: (url, params, headers) => { dispatch(Action.postData(url, params, headers)) },
        postRealTime: (url, params, headers) => { dispatch(Action.postRealTime(url, params, headers)) }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(PostalSave)