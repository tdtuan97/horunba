import React, { Component } from 'react';

import { connect } from 'react-redux';
import * as Action from '../common/actions';
import * as queryString from 'query-string';

import BUTTON from '../common/components/form/BUTTON';
import FORM from '../common/components/form/FORM';
import INPUT from '../common/components/form/INPUT';
import SELECT from '../common/components/form/SELECT';
import CHECKBOX from '../common/components/form/CHECKBOX';

import TABLE from '../common/components/table/TABLE';
import THEAD from '../common/components/table/THEAD';
import TBODY from '../common/components/table/TBODY';
import TR from '../common/components/table/TR';
import TH from '../common/components/table/TH';
import TD from '../common/components/table/TD';

import SortComponent from '../common/components/table/SortComponent';
import PaginationContainer from '../common/containers/PaginationContainer'
import Loading from '../common/components/common/Loading';

class PostalList extends Component {

    constructor(props) {
        super(props);
        const stringParams = queryString.parse(props.location.search);
        this.state = {};
        if (!_.isEmpty(stringParams)) {
            this.state = stringParams;
        }
        this.state['sort_index'] = 'desc';
        this.props.reloadData(dataListUrl, this.state);
    }
    componentDidUpdate() {
        if (!_.isEmpty(this.props.dataSource)) {
            $('.loading').hide();
        }
    }

    componentWillMount() {
        window.onpopstate = (event) => {
            location.reload();
        };
    }

    handleChange = (event) => {
        const target = event.target;
        const value = target.type === 'checkbox' ? target.checked : target.value;
        const name =  target.name ;
        this.setState({
            [name]: (value)?value:undefined
        },() => {
            if (name === 'per_page' ||
                name === 'one_area_n_pos' ||
                name === 'one_pos_n_area' ||
                name === 'can_not_daihiki' ||
                name === 'can_not_order_time' ||
                name === 'add_ship_charge'
                ) {
                this.handleReloadData();
            }
        });

    }

    handleChangeMultiSelect = (name, value, checked) => {
        let currentValue = this.state[name];
        if (typeof currentValue === 'undefined') {
            currentValue = [];
        } else if (!Array.isArray(this.state[name])) {
            currentValue = [this.state[name]];
        }
        if (Array.isArray(value)) {
            currentValue = [...value];
        } else {
            let index = currentValue.indexOf(value);
            if (checked === false) {
                if (index !== -1) {
                    currentValue.splice(index, 1);
                }
            } else {
                if (index === -1) {
                    currentValue.push(value);
                }
            }
        }
        this.setState({
            [name]: currentValue,
        });
    }

    handleChangeStatus = (event) => {
        let headers = {'X-Requested-With': 'XMLHttpRequest'};
        const target = event.target;
        const index = event.target.getAttribute('data-index');
        const value = target.type === 'checkbox' ? target.checked : target.value;
        const name =  target.name ;
        const formData = {'where' : {index:index} , 'data' : {[name] : value} };
        this.props.postData(changeFlagUrl, formData, headers);
        this.props.reloadData(dataListUrl, this.state);
    }

    handleClearClick = (event) => {
        Object.keys(this.state).map((key) => {
            if (key !== 'per_page') {
                delete this.state[key];
            }
        });
        this.handleReloadData();
    }

    handleReloadData = () => {
        if (this.state.page) {
            delete this.state.page;
        }
        this.props.reloadData(dataListUrl, this.state, true);
    }

    handleSortClick = (name, nextSort) => {
        Object.keys(this.state).map((key) => {
            if (key.startsWith('sort_')) {
                delete this.state[key];
            }
        });
        if (nextSort === 'none') {
            delete this.state[name];
            this.props.reloadData(dataListUrl, this.state, true);
        } else {
            this.setState({[name]: nextSort}, () => {
                this.props.reloadData(dataListUrl, this.state, true);
            });
        }

    }
    handleClickPage = (page) => {
        this.setState({
            page: page
        },() => {
            this.props.reloadData(dataListUrl, this.state, true);
        });
    }

    render() {
        let dataSource = this.props.dataSource;
        let i = 0;
        let options = {};
        let addShipChargeOptions = {};
        let totalItems = 0;
        let itemsPerPage = 20;
        if (!_.isEmpty(dataSource)) {
            i = dataSource.data.from - 1;
            dataSource.data_add_ship_charge.map((value) => {
               addShipChargeOptions[value.add_ship_charge] = value.add_ship_charge;
            });
            options      = dataSource.option;
            totalItems   = dataSource.data.total;
            itemsPerPage = dataSource.data.per_page;
        }
        return ( (!_.isEmpty(dataSource)) &&
            <div>
                <Loading className="loading" />
                <div className="box">
                    <div className="box-header">
                        <div className="row">
                            <div className="col-md-12">
                                <h4>{trans('messages.search_title')}</h4>
                            </div>
                            <FORM >
                                <div className="row">
                                    <div className="col-md-12">
                                    <INPUT
                                        name="postal_code"
                                        groupClass="form-group col-md-1-25"
                                        labelTitle={trans('messages.postal_code')}
                                        onChange={this.handleChange}
                                        value={this.state.postal_code||''}
                                        className="form-control input-sm" />
                                    <INPUT
                                        name="prefecture"
                                        groupClass="form-group col-md-1-5"
                                        labelTitle={trans('messages.prefecture')}
                                        onChange={this.handleChange}
                                        value={this.state.prefecture||''}
                                        className="form-control input-sm" />
                                    <INPUT
                                        name="city"
                                        groupClass="form-group col-md-1-5"
                                        labelTitle={trans('messages.city')}
                                        onChange={this.handleChange}
                                        value={this.state.city||''}
                                        className="form-control input-sm" />
                                    <SELECT
                                        id="one_area_n_pos"
                                        name="one_area_n_pos"
                                        groupClass="form-group col-md-1-75"
                                        labelTitle={trans('messages.one_area_n_pos')}
                                        handleChangeMultiSelect={this.handleChangeMultiSelect}
                                        valueMulti={this.state.one_area_n_pos||''}
                                        options={options}
                                        multiple="multiple"
                                        className="form-control input-sm" />
                                    <SELECT
                                        id="one_pos_n_area"
                                        name="one_pos_n_area"
                                        groupClass="form-group col-md-1-75"
                                        labelTitle={trans('messages.one_pos_n_area')}
                                        handleChangeMultiSelect={this.handleChangeMultiSelect}
                                        valueMulti={this.state.one_pos_n_area||''}
                                        options={options}
                                        multiple="multiple"
                                        className="form-control input-sm" />
                                    <SELECT
                                        id="can_not_daihiki"
                                        name="can_not_daihiki"
                                        groupClass="form-group col-md-1-5"
                                        labelTitle={trans('messages.can_not_daihiki')}
                                        handleChangeMultiSelect={this.handleChangeMultiSelect}
                                        valueMulti={this.state.can_not_daihiki||''}
                                        options={{0:'不', 1:'可'}}
                                        multiple="multiple"
                                        className="form-control input-sm" />
                                    <SELECT
                                        id="can_not_order_time"
                                        name="can_not_order_time"
                                        groupClass="form-group col-md-1-5"
                                        labelTitle={trans('messages.can_not_order_time')}
                                        handleChangeMultiSelect={this.handleChangeMultiSelect}
                                        valueMulti={this.state.can_not_order_time||''}
                                        options={{0:'不', 1:'可'}}
                                        multiple="multiple"
                                        className="form-control input-sm" />

                                    <SELECT
                                        name="per_page"
                                        groupClass="form-group col-md-1 per-page pull-right-md"
                                        labelClass="pull-left"
                                        labelTitle={trans('messages.per_page')}
                                        onChange={this.handleChange}
                                        value={this.state.per_page||''}
                                        options={{20:20, 40:40, 60:60, 80:80, 100:100}}
                                        className="form-control input-sm" />
                                    </div>
                                </div>
                                <div className="row">
                                    <div className="col-md-12">
                                    <INPUT
                                        name="deli_days"
                                        groupClass="form-group col-md-2"
                                        labelTitle={trans('messages.deli_days')}
                                        onChange={this.handleChange}
                                        value={this.state.deli_days||''}
                                        className="form-control input-sm" />
                                    <SELECT
                                        id="add_ship_charge"
                                        groupClass="form-group col-md-2"
                                        name="add_ship_charge"
                                        labelTitle={trans('messages.add_ship_charge')}
                                        handleChangeMultiSelect={this.handleChangeMultiSelect}
                                        valueMulti={this.state.add_ship_charge||''}
                                        options={options}
                                        multiple="multiple"
                                        className="form-control input-sm" />
                                    </div>
                                </div>
                            </FORM>
                        </div>
                        <div className="row">
                            <div className="col-md-9 col-xs-4">
                                <a href={saveUrl} className="btn btn-success input-sm">{trans('messages.btn_add')}</a>
                            </div>
                            <div className="col-md-3 col-xs-8">
                                <BUTTON className="btn btn-danger pull-right" onClick={this.handleClearClick}>{trans('messages.btn_reset')}</BUTTON>
                                <BUTTON className="btn btn-primary pull-right margin-element" onClick={this.handleReloadData} >{trans('messages.btn_search')}</BUTTON>
                            </div>
                        </div>
                    </div>
                    <div className="box-body">
                        <div className="row">
                            <div className="col-md-12">
                                {(totalItems/itemsPerPage > 1) &&
                                <div className="row">
                                    <div className="col-md-2-5"></div>
                                    <div className="col-md-7 text-center">
                                        <PaginationContainer handleClickPage={this.handleClickPage}/>
                                    </div>
                                    <div className="col-md-2-5 text-right line-height-md">
                                        {dataSource.data.from}件 - {dataSource.data.to}件 【全{dataSource.data.total}件】
                                    </div>
                                </div>
                                }
                                <TABLE className="table table-striped table-custom">
                                    <THEAD>
                                        <TR>
                                            <TH className="visible-xs visible-sm text-center"></TH>
                                            <TH className="text-center text-middle">
                                                <SortComponent
                                                    name="sort_index"
                                                    onClick={this.handleSortClick}
                                                    currentSort={(this.state.sort_index)?this.state.sort_index:'none'}
                                                    title={trans('messages.index')} />
                                            </TH>
                                            <TH className="hidden-xs text-middle hidden-sm col-md-1 text-center">
                                                <SortComponent
                                                    name="sort_postal_code"
                                                    onClick={this.handleSortClick}
                                                    currentSort={(this.state.sort_postal_code)?this.state.sort_postal_code:'none'}
                                                    title={trans('messages.postal_code')} />
                                            </TH>
                                            <TH className="hidden-xs hidden-sm text-center text-middle col-md-1">
                                                <SortComponent
                                                    name="sort_prefecture"
                                                    onClick={this.handleSortClick}
                                                    currentSort={(this.state.sort_prefecture)?this.state.sort_prefecture:'none'}
                                                    title={trans('messages.prefecture')} />
                                            </TH>
                                            <TH className="hidden-xs hidden-sm text-center text-middle col-md-1">
                                                <SortComponent
                                                    name="sort_city"
                                                    onClick={this.handleSortClick}
                                                    currentSort={(this.state.sort_city)?this.state.sort_city:'none'}
                                                    title={trans('messages.city')} />
                                            </TH>
                                            <TH className="hidden-xs hidden-sm text-center text-middle col-md-1">
                                                <SortComponent
                                                    name="sort_sub_address"
                                                    onClick={this.handleSortClick}
                                                    currentSort={(this.state.sort_sub_address)?this.state.sort_sub_address:'none'}
                                                    title={trans('messages.sub_address')} />
                                            </TH>
                                            <TH className="hidden-xs hidden-sm text-center text-middle col-md-1">
                                                <SortComponent
                                                    name="sort_full_address"
                                                    onClick={this.handleSortClick}
                                                    currentSort={(this.state.sort_full_address)?this.state.sort_full_address:'none'}
                                                    title={trans('messages.full_address')} />
                                            </TH>
                                            <TH className="text-center text-middle">
                                                <SortComponent
                                                    name="sort_one_area_n_pos"
                                                    onClick={this.handleSortClick}
                                                    currentSort={(this.state.sort_one_area_n_pos)?this.state.sort_one_area_n_pos:'none'}
                                                    title={trans('messages.one_area_n_pos')} />
                                            </TH>
                                            <TH className="text-center text-middle">
                                                <SortComponent
                                                    name="sort_one_pos_n_area"
                                                    onClick={this.handleSortClick}
                                                    currentSort={(this.state.sort_one_pos_n_area)?this.state.sort_one_pos_n_area:'none'}
                                                    title={trans('messages.one_pos_n_area')} />
                                            </TH>
                                            <TH className="text-middle text-center">
                                                <SortComponent
                                                    name="sort_can_not_daihiki"
                                                    onClick={this.handleSortClick}
                                                    currentSort={(this.state.sort_can_not_daihiki)?this.state.sort_can_not_daihiki:'none'}
                                                    title={trans('messages.can_not_daihiki')} />
                                            </TH>
                                            <TH className="text-middle text-center">
                                                <SortComponent
                                                    name="sort_can_not_order_time"
                                                    onClick={this.handleSortClick}
                                                    currentSort={(this.state.sort_can_not_order_time)?this.state.sort_can_not_order_time:'none'}
                                                    title={trans('messages.can_not_order_time')} />
                                            </TH>
                                            <TH className="text-middle hidden-xs hidden-sm text-center">
                                                <SortComponent
                                                    name="sort_deli_days"
                                                    onClick={this.handleSortClick}
                                                    currentSort={(this.state.sort_deli_days)?this.state.sort_deli_days:'none'}
                                                    title={trans('messages.deli_days')} />
                                            </TH>
                                            <TH className="text-middle hidden-xs hidden-sm text-center">
                                                <SortComponent
                                                    name="sort_add_ship_charge"
                                                    onClick={this.handleSortClick}
                                                    currentSort={(this.state.sort_add_ship_charge)?this.state.sort_add_ship_charge:'none'}
                                                    title={trans('messages.add_ship_charge')} />
                                            </TH>
                                        </TR>
                                    </THEAD>
                                    <TBODY>
                                        {
                                            (!_.isEmpty(dataSource.data.data))?
                                            dataSource.data.data.map((item) => {
                                                i++;
                                                let classRow = (i%2 ===0) ? 'odd' : 'even';
                                                return (
                                                    [
                                                    <TR key={"tr" + i} className={classRow}>
                                                        <TD className="visible-xs visible-sm text-center">
                                                            <b className="show-info">
                                                                <i className="fa fa-angle-double-down" aria-hidden="true"></i>
                                                            </b>
                                                        </TD>
                                                        <TD className="text-center">
                                                            <a className="link-detail" href={saveUrl + "?index=" + item.index}>
                                                                {item.index}
                                                            </a>
                                                        </TD>
                                                        <TD className="hidden-xs hidden-sm cut-text"  title={item.postal_code}>
                                                            <span className="limit-char max-width-70">{item.postal_code}</span>
                                                        </TD>
                                                        <TD className="hidden-xs hidden-sm cut-text"  title={item.prefecture}>
                                                            <span className="">{item.prefecture}</span>
                                                        </TD>
                                                        <TD className="hidden-xs hidden-sm cut-text"  title={item.city}>
                                                            <span className="">{item.city}</span>
                                                        </TD>
                                                        <TD className="hidden-xs hidden-sm cut-text"  title={item.sub_address}>
                                                            <span className="">{item.sub_address}</span>
                                                        </TD>
                                                        <TD className="hidden-xs hidden-sm cut-text"  title={item.full_address}>
                                                            <span className="">{item.full_address}</span>
                                                        </TD>
                                                        <TD className="cut-text text-center"  title={item.one_area_n_pos}>
                                                            <label className="control nice-checkbox">
                                                                <CHECKBOX
                                                                    name={'one_area_n_pos'}
                                                                    key={'one_area_n_pos' + item.index + Math.random()}
                                                                    defaultChecked={item.one_area_n_pos}
                                                                    value="1"
                                                                    onChange={this.handleChangeStatus}
                                                                    data-index={item.index}
                                                                />
                                                                <div className="nice-icon-check"></div>
                                                            </label>
                                                        </TD>
                                                        <TD className="cut-text text-center"  title={item.one_pos_n_area}>
                                                            <label className="control nice-checkbox">
                                                                <CHECKBOX
                                                                    name={'one_pos_n_area'}
                                                                    key={'one_pos_n_area' + item.index + Math.random()}
                                                                    defaultChecked={item.one_pos_n_area}
                                                                    value="1"
                                                                    onChange={this.handleChangeStatus}
                                                                    data-index={item.index}
                                                                />
                                                                <div className="nice-icon-check"></div>
                                                            </label>
                                                        </TD>
                                                        <TD className="cut-text text-center"  title={item.can_not_daihiki}>
                                                            <label className="control nice-checkbox">
                                                                <CHECKBOX
                                                                    name={'can_not_daihiki'}
                                                                    key={'can_not_daihiki' + item.index + Math.random()}
                                                                    defaultChecked={item.can_not_daihiki}
                                                                    value="1"
                                                                    onChange={this.handleChangeStatus}
                                                                    data-index={item.index}
                                                                />
                                                                <div className="nice-icon-check"></div>
                                                            </label>
                                                        </TD>
                                                        <TD className="cut-text text-center"  title={item.can_not_order_time}>
                                                            <label className="control nice-checkbox">
                                                                <CHECKBOX
                                                                    name={'can_not_order_time'}
                                                                    key={'can_not_order_time' + item.index}
                                                                    defaultChecked={item.can_not_order_time}
                                                                    value="1"
                                                                    onChange={this.handleChangeStatus}
                                                                    data-index={item.index}
                                                                />
                                                                <div className="nice-icon-check"></div>
                                                            </label>
                                                        </TD>
                                                        <TD className="hidden-xs hidden-sm cut-text text-right"  title={item.deli_days}>
                                                            <span className="limit-char max-width-100">{item.deli_days}</span>
                                                        </TD>
                                                        <TD className="hidden-xs hidden-sm cut-text text-right"  title={item.add_ship_charge}>
                                                            <span className="limit-char max-width-100">{item.add_ship_charge}</span>
                                                        </TD>

                                                    </TR>,
                                                    <TR key={"trhide" + i} className="hiden-info" style={{display:"none"}}>
                                                        <TD colSpan="6" className="visible-xs visible-sm width-span1">
                                                            <ul id={"temp" + item.index}>
                                                                <li>
                                                                    <b>{trans('messages.postal_code')} : </b>
                                                                    {item.postal_code}
                                                                </li>
                                                                <li>
                                                                    <b>{trans('messages.prefecture')} : </b>
                                                                    {item.prefecture}
                                                                </li>
                                                                <li>
                                                                    <b>{trans('messages.city')} : </b>
                                                                    {item.city}
                                                                </li>
                                                                <li>
                                                                    <b>{trans('messages.sub_address')} : </b>
                                                                    {item.sub_address}
                                                                </li>
                                                                <li>
                                                                    <b>{trans('messages.one_area_n_pos')} : </b>
                                                                    {item.one_area_n_pos}
                                                                </li>
                                                                <li>
                                                                    <b>{trans('messages.one_area_n_pos')} : </b>
                                                                    {item.one_area_n_pos}
                                                                </li>
                                                                <li>
                                                                    <b>{trans('messages.can_not_daihiki')} : </b>
                                                                    {item.can_not_daihiki}
                                                                </li>
                                                                <li>
                                                                    <b>{trans('messages.can_not_order_time')} : </b>
                                                                    {item.can_not_order_time}
                                                                </li>
                                                                <li>
                                                                    <b>{trans('messages.deli_days')} : </b>
                                                                    {item.deli_days}
                                                                </li>
                                                                <li>
                                                                    <b>{trans('messages.add_ship_charge')} : </b>
                                                                    {item.add_ship_charge}
                                                                </li>
                                                            </ul>
                                                        </TD>
                                                    </TR>
                                                   ]
                                                )
                                            })
                                            :
                                            <TR><TD colSpan="11" className="text-center">{trans('messages.no_data')}</TD></TR>
                                        }
                                    </TBODY>
                                </TABLE>
                                <div className="text-center">
                                    <PaginationContainer handleClickPage={this.handleClickPage}/>
                                </div>
                            </div>
                        </div>
                    </div>
                    </div>
                </div>

        )
    }
}

const mapStateToProps = (state) => {
    return {
        dataSource: state.commonReducer.dataSource
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        reloadData: (url, params, pushHistory, ignoreFields) => { dispatch(Action.reloadData(url, params, pushHistory, ignoreFields)) },
        postData: (url, params, headers) => { dispatch(Action.postData(url, params, headers)) }

    }
}

export default connect(mapStateToProps, mapDispatchToProps)(PostalList)