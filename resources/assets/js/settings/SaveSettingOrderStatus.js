import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import axios from 'axios';
import * as Action from '../common/actions';
import  { Redirect } from 'react-router-dom'

import Loading from '../common/components/common/Loading';
import FORM from '../common/components/form/FORM';
import INPUT from '../common/components/form/INPUT';
import SELECT from '../common/components/form/SELECT';
import BUTTON from '../common/components/form/BUTTON';

import TABLE from '../common/components/table/TABLE';
import THEAD from '../common/components/table/THEAD';
import TBODY from '../common/components/table/TBODY';
import TR from '../common/components/table/TR';
import TH from '../common/components/table/TH';
import TD from '../common/components/table/TD';

class SaveSettingOrderStatus extends Component {

    constructor(props) {
        super(props);
        var getUrl = window.location;
        var baseUrl = getUrl .protocol + "//" + getUrl.host + "/" + getUrl.pathname.split('/')[1];
        this.state = {baseUrl: baseUrl, renderFirstTime: true}
        
        let location = decodeURIComponent(window.location.href);
        let arrLocal = location.split('?');
        let query = '';
        if (arrLocal.length > 1) {
            query = '?' + arrLocal[1]
        }
        this.props.getData(baseUrl+'/order-status/orderStatusFormData' + query);
    }
    componentWillReceiveProps(nextProps) {
        
        if (!_.isEmpty(nextProps.dataSource)) {
            let data = {};
            if (!_.isEmpty(nextProps.dataSource.data)) {
                nextProps.dataSource.data.data.forEach((element, index) => {
                       data['delay_priod['+ element.order_status_id+']'] = element.delay_priod;
                       data['expiration_priod[' + element.order_status_id+']'] = element.expiration_priod;               
                });
            } 
            if (!_.isEmpty(data)) {
                this.setState(data);
            }
        }
    }   
    componentDidUpdate() {
        if (!_.isEmpty(this.props.dataSource)) {
            $('.loading').hide();           
        }       
    }
    
    handleChange = (event) => {
        const target = event.target;
        const name = target.name;
        const value = target.value;
        this.setState({
            [name]: value
        });
    }
    
    handleCancel = (event) => {
        window.location.href = this.state.baseUrl;
    }
    
    handleSubmit = (event) => {
        let headers = {'X-Requested-With': 'XMLHttpRequest'};
        if (this.props.enctype) {
            headers['Content-Type'] = this.props.enctype;
        }
        let formData = new FormData();
        if (this.state && Object.keys(this.state).length > 0) {
            Object.keys(this.state).map((item) => {
                formData.append(item, this.state[item]);
            });
        }        
        this.props.submitFormData(window.location.href + '/save', formData, headers);
    }

    render() {
        let dataSource = this.props.dataSource;
        let index = 0;
        return ((!_.isEmpty(dataSource))?
            <FORM className="">
                <Loading className="loading"></Loading>
                <div>
                <Loading className="loading" />
                <div className="box">
                    <div className="box-header">
                        <div className="row">
                            <div className="col-xs-12">                                                        
                                <BUTTON className="btn btn-success margin-element pull-right" type="button" onClick={this.handleSubmit}>
                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{trans('messages.btn_accept')}&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                </BUTTON>
                            </div>                         
                        </div>
                    </div>
                    <div className="box-body">
                        <div className="row">
                            <div className="col-md-12">
                                 <TABLE className="table table-striped">
                                    <THEAD>
                                        <TR>
                                            <TH className="hidden-lg"></TH>
                                            <TH width="10%" className="hidden-xs hidden-sm text-center ">{trans('messages.order_status_id')}</TH>
                                            <TH className="hidden-xs hidden-sm">{trans('messages.status_name')}</TH>
                                            <TH  className="text-center ">{trans('messages.delay_priod')}</TH>
                                            <TH className="text-center ">{trans('messages.expiration_priod')}</TH>
                                        </TR>
                                    </THEAD>
                                    <TBODY>
                                        {   
                                            (!_.isEmpty(dataSource) && !_.isEmpty(dataSource.data) )?
                                            dataSource.data.data.map((item) => {
                                                index++;
                                                let classRow = (index%2 ===0) ? 'odd' : 'even';
                                                return (
                                                    [
                                                    <TR key={"tr" + index} className={classRow}>
                                                        <TD className="visible-xs visible-sm">     
                                                            <b className="show-info">
                                                                <i className="fa fa-angle-double-down" aria-hidden="true"></i>
                                                            </b>                                     
                                                        </TD>
                                                        <TD className="hidden-xs hidden-sm text-center ">{item.order_status_id}</TD>
                                                        <TD className="hidden-xs hidden-sm">{item.status_name}</TD>
                                                        <TD className="text-center ">
                                                         <INPUT
                                                            name={'delay_priod[' + item.order_status_id + ']'}
                                                            value={this.state['delay_priod[' + item.order_status_id + ']'] }
                                                            onChange={this.handleChange.bind(this)}
                                                            groupClass="col-md-4 col-xs-10 col-centered"
                                                            className={
                                                            dataSource.message 
                                                            && 
                                                            !_.isEmpty(dataSource.message[item.order_status_id])
                                                            &&
                                                            !_.isEmpty(dataSource.message[item.order_status_id]['delay_priod_'+item.order_status_id])
                                                             ? 'form-control text-center input-sm error' : ' text-center form-control input-sm'
                                                            } />
                                                        
                                                        </TD>
                                                        <TD className="text-center">
                                                         <INPUT
                                                            name={'expiration_priod[' + item.order_status_id+']'}
                                                            value={this.state['expiration_priod[' + item.order_status_id+']']}
                                                            onChange={this.handleChange.bind(this)}
                                                            groupClass="col-md-4 col-xs-10 col-centered"
                                                            className={
                                                            dataSource.message 
                                                            && 
                                                            !_.isEmpty(dataSource.message[item.order_status_id])
                                                            &&
                                                            !_.isEmpty(dataSource.message[item.order_status_id]['expiration_priod_'+item.order_status_id],dataSource.message)
                                                             ? 'form-control text-center input-sm error' : 'form-control text-center input-sm'
                                                            }
                                                            hasError={dataSource.message&&dataSource.message.genre?true:false}
                                                          />

                                                        </TD>

                                                        
                                                    </TR>,
                                                    <TR key={"trhide" + index} className="hiden-info" style={{display:"none"}}>
                                                        <TD colSpan="10" className="width-span1">
                                                            <ul id={"temp" + item.order_status_id} >
                                                                <li>
                                                                    <b>{trans('messages.order_status_id')} : </b>
                                                                    <p>{item.order_status_id }</p>
                                                                </li>                                                                                                                       
                                                                <li>
                                                                    <b>{trans('messages.status_name')} : </b>
                                                                    <p>{item.status_name }</p>
                                                                </li>                                                                                                                       
                                                                <li>
                                                                    <b>{trans('messages.delay_priod')} : </b>
                                                                    <p>{item.delay_priod }</p>
                                                                </li>                                                                                                                       
                                                                <li>
                                                                    <b>{trans('messages.order_status_id')} : </b>
                                                                    <p>{item.expiration_priod }</p>
                                                                </li>                                                                                                                       
                                                            </ul>                                                            
                                                        </TD>
                                                    </TR>

                                                   ]
                                                )
                                            })
                                            
                                            :
                                            <TR><TD colSpan="9" className="text-center">{trans('messages.no_data')}</TD></TR>
                                        }
                                              
                                    </TBODY>
                                </TABLE>
                                <div className="row">
                                    <div className="col-xs-12">                                                        
                                        <BUTTON className="btn btn-success margin-element pull-right" type="button" onClick={this.handleSubmit}>
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{trans('messages.btn_accept')}&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        </BUTTON>
                                    </div> 
                                </div>
                            </div>
                        </div>
                    </div>
                    </div>
                </div>
            </FORM>
            :<Loading className="loading"></Loading>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        dataSource: state.commonReducer.dataSource
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        getData: (url, params, headers) => { dispatch(Action.detailData(url, params)) },
        submitFormData: (url, params, headers) => { dispatch(Action.submitFormData(url, params, headers)) }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(SaveSettingOrderStatus)