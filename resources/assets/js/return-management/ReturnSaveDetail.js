import React, { Component } from 'react';
import { connect } from 'react-redux';

import * as Action from '../common/actions';
import * as queryString from 'query-string';
import moment from 'moment';

import Loading from '../common/components/common/Loading';
import FORM from '../common/components/form/FORM';
import INPUT from '../common/components/form/INPUT';
import TEXTAREA from '../common/components/form/TEXTAREA';
import SELECT from '../common/components/form/SELECT';
import BUTTON from '../common/components/form/BUTTON';
import CHECKBOX_CUSTOM from '../common/components/form/CHECKBOX_CUSTOM';
import LABEL from '../common/components/form/LABEL';
import DATEPICKER from '../common/components/form/DATEPICKER';
import MessagePopup from '../common/components/common/MessagePopup';
import MESSAGE_MODAL from '../common/containers/MessageModal';
import TABLE from '../common/components/table/TABLE';
import THEAD from '../common/components/table/THEAD';
import TBODY from '../common/components/table/TBODY';
import TR from '../common/components/table/TR';
import TH from '../common/components/table/TH';
import TD from '../common/components/table/TD';
import FormAdress from './form-address/FormAdress';
import axios from 'axios';

var timer;
class ReturnSaveDetail extends Component {
    constructor(props) {
        super(props);
        this.state = {status:'add'}
        this.firstRender = true;
        let params = queryString.parse(props.location.search);
        if (params['receive_id']) {
            this.state.receive_id    = params['receive_id'];
            this.state.status       = 'check';
            this.state.first        = true;
            this.state.index        = params['receive_id'];
            this.hanleRealtime();
        }
        if (params['product_code']) {
            this.state.product_code = params['product_code'];
        }
        this.state.return_type_large_id = '1';
        this.props.reloadData(saveUrl, this.state);
    }
    handleCancel = (event) => {
        window.location.href = baseUrl;
    }
    hanleRealtime() {
        if (!_.isEmpty(this.state.index)) {
            let params = {
                accessFlg : this.state.accessFlg,
                query : {
                        page_key : 'RETURN-DETAIL-SAVE',
                        primary_key : 'receive_id:' + this.state.index
                    },
                key :this.state.index
            };
            if (this.state.accessFlg !== false) {
                this.props.postRealTime(checkEditUrl, params, {'X-Requested-With': 'XMLHttpRequest'});
            }
        }
    }
    handleSubmit = (event) => {
        let headers = {'X-Requested-With': 'XMLHttpRequest'};
        let data = {};
        if (!_.isEmpty(this.state)) {
            Object.keys(this.state).map((item) => {
                data[item] = this.state[item];
            });
        this.props.postData(saveUrl, data, headers);
        }
    }
    handleChange = (event) => {
        let target = event.target;
        let name = target.name;
        let value = target.type === 'checkbox' ? (target.checked ? 1 : 0) : (target.value === '_none' ? '' : target.value);
        let dataSet = {};
        if (name === 'return_type') {
            this.getReturnDetail(value);
            clearTimeout(timer);
            let _this = this;
            timer = setTimeout(function() {
                _this.getRepaymentPrice(
                    _this.state.receive_id,
                    value,
                    _this.state.check_return,
                    _this.state.return_quantity
                );
            }, 2000);
        }
        if (name.includes('return_quantity')) {
            let key = name.split(/(\d+)/);
            let arrName = name.split('[');
            name = arrName[0];
            let currentValue = this.state[name];
            if (value !== '') {
                currentValue[key[1]] = parseInt(value);
            } else {
                currentValue[key[1]] = 0;
            }
            clearTimeout(timer);
            let _this = this;
            timer = setTimeout(function() {
                _this.getRepaymentPrice(
                    _this.state.receive_id,
                    _this.state.return_type,
                    _this.state.check_return,
                    currentValue
                );
            }, 2000);
            value = currentValue;
        }
        if (target.type === 'checkbox') {
            if (name.includes('payment_on_delivery')) {
                if (target.checked) {
                    let note = '【着払い出荷】'+ (typeof this.state.note !== 'undefined' ? "\n" + _.trim(this.state.note) : '');
                    dataSet.note = note;
                    $('textarea[name=note]').val(note);

                } else {
                    let note = _.trim(_.trim(this.state.note).replace(/【着払い出荷】/g,''));
                    dataSet.note = _.trim(note);
                    $('textarea[name=note]').val(note);
                }
                dataSet[name] = value;
            } else {
                let key = name.split(/(\d+)/);
                let arrName = name.split('[');
                name = arrName[0];
                let currentValue = this.state[name];
                if (target.checked) {
                    currentValue[key[1]] = 1;
                } else {
                    currentValue[key[1]] = 0;
                }
                clearTimeout(timer);
                let _this = this;
                timer = setTimeout(function() {
                    _this.getRepaymentPrice(
                        _this.state.receive_id,
                        _this.state.return_type,
                        currentValue,
                        _this.state.return_quantity
                    );
                }, 2000);
                dataSet[name] = currentValue;
            }
        } else {
            dataSet[name] = value;
        }

        if (name === 'return_type_mid_id') {
            if (value === '2') {
                if (typeof this.state.note === 'undefined') {
                    this.setState({note: '【仕入先返品】直貼り禁止'});
                } else {
                   this.setState({note: _.trim(this.state.note) + '\n【仕入先返品】直貼り禁止'});
                }
            } else {
                if (typeof this.state.note === 'undefined') {
                    this.setState({note: ''});
                } else {
                    this.setState({note: _.trim(_.trim(this.state.note).replace(/【仕入先返品】直貼り禁止/g,''))});
                }
            }
        }

        let arrReturnPriceCheck = [
            'repay_price',
            'return_point',
            'repay_coupon',
            'return_fee',
            'other_commission',
            'debosit_commission',
        ];
        if (arrReturnPriceCheck.indexOf(name) !== -1) {
            let arrValue = [];
            arrReturnPriceCheck.map((item) => {
                if (name === item) {
                    arrValue[item] = parseInt(value) || 0;
                } else {
                    arrValue[item] = parseInt(this.state[item]) || 0;
                }
            });
            if (parseInt(this.state['return_type']) === 1) {
                if (arrValue['return_point'] + arrValue['repay_coupon']
                    - (arrValue['return_fee'] + arrValue['other_commission']) > 0 ) {
                    dataSet['total_return_price'] = arrValue['repay_price'];
                } else {
                    dataSet['total_return_price'] = arrValue['repay_price'] + arrValue['return_point']
                        + arrValue['repay_coupon'] - (arrValue['return_fee'] + arrValue['other_commission']);
                }
            } else {
                dataSet['total_return_price'] = arrValue['repay_price'] + arrValue['debosit_commission'];
            }
            $('input[name=total_return_price]').val(dataSet['total_return_price']);
        }

        this.setState({
            ...dataSet
        });
    }
    getRepaymentPrice = (receiveId, returnType, checkReturn, returnQuantity) => {
        $(".loading").show();
        let param = {
            receive_id : receiveId,
            return_type : returnType,
            check_return : checkReturn,
            return_quantity: returnQuantity,
            default_quantity: this.state.default_quantity
        };
        let headers = {'X-Requested-With': 'XMLHttpRequest'};
        axios.post(getRepaymentPriceUrl, param, {headers : headers}
        ).then(response => {

            let dataSet = {};
            dataSet.repay_price  = response.data.repayPrice;
            dataSet.return_point = response.data.repayPoint;
            dataSet.repay_coupon = response.data.repayCoupon;
            dataSet.sum_price    = response.data.sumPrice;

            let returnCommission  = 0;
            let totalReturnPrice  = 0;
            let repayPrice        = parseInt(response.data.repayPrice) || 0;
            let repayPoint        = parseInt(response.data.repayPoint) || 0;
            let repayCoupon       = parseInt(response.data.repayCoupon) || 0;
            let otherCommission   = parseInt(this.state.other_commission) || 0;
            let debositCommission = parseInt(this.state.debosit_commission) || 0;
            let repayPointSet     = repayPoint;
            let repayCouponSet    = repayCoupon;
            if (parseInt(returnType) === 1) {
                returnCommission = 2000;
                let cal = repayPoint - (returnCommission + otherCommission);
                if (cal < 0) {
                    dataSet.return_point = 0;
                    repayPointSet        = 0;
                    if ((repayCoupon + cal) < 0) {
                        dataSet.repay_coupon = 0;
                        repayCouponSet       = 0;
                    } else {
                        dataSet.repay_coupon = repayCoupon + cal;
                        repayCouponSet       = repayCoupon + cal;
                    }
                } else {
                    dataSet.return_point = cal;
                    repayPointSet        = cal;
                }
                if(repayPoint + repayCoupon - (returnCommission + otherCommission) > 0){
                    totalReturnPrice = repayPrice;
                } else {
                    totalReturnPrice = repayPrice + repayPoint + repayCoupon - (returnCommission + otherCommission);
                }
            } else {
                totalReturnPrice = repayPrice + debositCommission;
            }
            dataSet.return_fee  = returnCommission;
            dataSet.total_return_price = totalReturnPrice;
            dataSet.order_return_price = response.data.orderReturnPrice;
            dataSet.order_return_point = response.data.orderReturnPoint;
            dataSet.order_return_coupon = response.data.orderReturnCoupon;
            $('input[name=repay_price]').val(parseInt(response.data.repayPrice) || 0);
            $('input[name=return_point]').val(parseInt(repayPointSet) || 0);
            $('input[name=repay_coupon]').val(parseInt(repayCouponSet) || 0);
            $('input[name=return_fee]').val(parseInt(returnCommission) || 0);
            $('input[name=total_return_price]').val(parseInt(totalReturnPrice) || 0);
            this.setState({...dataSet});
        }).catch(error => {
            $(".loading").hide();
            throw(error);
        });
    }

    getReturnDetail = function (returnId) {
        let headers = {'X-Requested-With': 'XMLHttpRequest'};
        let param = {
            return_id : returnId,
        }
        axios.post(optReturnDetailUrl, param, {headers : headers}
        ).then(response => {
            this.setState({returnTypePotion: response.data.returnTypePotion});
        }).catch(error => {
            $(".loading").hide();
            throw(error);
        });
    }
    handleChangeDate = function(date, name, event) {
        let dateValue = moment(date);
        if (!dateValue.isValid()) {
            dateValue = undefined;
        } else {
            dateValue = dateValue.format('YYYY-MM-DD');
        }
        this.setState({
          [name]: dateValue,
        });
    }
    handleCancel = (event) => {
        window.location.href = orderDetailUrl + '?receive_id_key=' + this.state.receive_id;
    }
    componentWillUnmount () {
        clearInterval(this.state);
    }
    handleFormAddress = (supplierCd) => {
        this.setState({supplier_cd:supplierCd}, () => {
            let headers = {'X-Requested-With': 'XMLHttpRequest'};
            this.props.postData(getFormDataUrl, {supplier_cd:supplierCd}, headers);
        });
        $('#issue-modal').modal({backdrop: 'static', show: true});
    }
    handleCancelPopup = () => {
        this.setState({dataReturnAddress:{}}, () => {
            let param = {receive_id:this.state.receive_id,status:'reload'};
            this.props.reloadData(saveUrl, param);
            $('#issue-modal').modal('hide');
        });
    }
    componentWillReceiveProps(nextProps) {
        if (!_.isEmpty(nextProps.dataSource) &&
            (!this.props.dataSource ||
                this.props.dataSource.time !== nextProps.dataSource.time)) {
            if (nextProps.dataSource.status === 'done') {
                window.location.href = baseUrl;
            } else {
                let dataSet = {status : nextProps.dataSource.status, default_quantity: nextProps.dataSource.default_quantity, data_update: []};
                if (nextProps.dataSource.status === 'add') {
                    if (nextProps.dataSource.type !== 'reload') {
                        dataSet.check_return = nextProps.dataSource.default_check_return;
                        dataSet.return_quantity = nextProps.dataSource.default_return_quantity;
                    }
                }
                dataSet.return_date = moment().format("YYYY-MM-DD");
                dataSet.receive_plan_date = moment().format("YYYY-MM-DD");
                dataSet.delivery_plan_date = moment().format("YYYY-MM-DD");
                this.setState(dataSet);
            }
            if (!_.isEmpty(nextProps.dataSource.data)) {
                let data = nextProps.dataSource.data;
                let dataSet = {...data, returnTypePotion: nextProps.dataSource.returnTypePotion};
                if (!_.isEmpty(this.state)) {
                    Object.keys(this.state).map((item) => {
                        if (item !== 'status') {
                            dataSet[item] = this.state[item];
                        }
                    });
                }
                this.setState(dataSet);
            }

        }
        if (!_.isEmpty(nextProps.dataResponse) && (!this.props.dataResponse || this.props.dataResponse.time !== nextProps.dataResponse.time)) {
            if (!_.isEmpty(nextProps.dataResponse) && nextProps.dataResponse.status === 'done') {
                window.location.href = orderDetailUrl + '?receive_id_key=' + this.state.receive_id;
            }
            if (!_.isEmpty(nextProps.dataResponse) && !_.isEmpty(nextProps.dataResponse.dataReturnAddress)) {
                this.setState({dataReturnAddress:nextProps.dataResponse.dataReturnAddress});
            }
        }
        // Check realtime update
        if (!_.isEmpty(nextProps.dataRealTime)) {
            this.setState(nextProps.dataRealTime);
        }
    }
    handleEdit = () => {
        $('.show-edit').removeClass('collapse');
    }
    componentDidUpdate() {
        if (!_.isEmpty(this.props.dataSource)) {
            $('.loading').hide();
        }
        if (this.state.accessFlg === false) {
            $('#message-modal').modal('show');
            //We will auto redirect after 5 seconds
            setTimeout(() => {
                window.location.href = baseUrl;
            }, 5000);
        }
    }
    componentDidMount() {
        setInterval(() => {
            this.hanleRealtime()
        }, 5000);
    }
    handleAddNewLine = () => {
        let dataSet = !_.isEmpty(this.state.dataReturnAddress) ? [...this.state.dataReturnAddress] : [];
        let newElement = {
            supplier_cd: this.state.supplier_cd,
            return_tel: '',
            return_pref: '',
            return_postal: '',
            return_nm: '',
            new_item: 1,
            return_address: '',
            is_selected: 0
        }
        dataSet.push(newElement);
        this.setState({
            dataReturnAddress: dataSet
        });
    }
    handleAddressSubmit = (e) => {
        let headers = {'X-Requested-With': 'XMLHttpRequest'};
        document.getElementsByName(e.target.name)[0].disabled = true;
        $(".loading").show();
        let params = {};
        params['dataReturnAddress'] = this.state.dataReturnAddress;
        params['supplier_cd'] = this.state.supplier_cd;
        axios.post(saveAddressUrl,params, {headers : headers, overrideMimeType : "text/html;charset=Shift_JIS" }
        ).then(response => {
            $(".loading").hide();
            let data = response.data;
            if (data.status === 1) {
                this.handleCancelPopup();
            } else if (data.messages) {
                if (typeof data.messages === 'object') {
                    this.setState({
                       messages :  data.messages
                    });
                } else {
                    this.setState({
                       messages_total :  data.messages
                    });
                }

            }

        }).catch(error => {
            $(".loading").hide();
            throw(error);
        });
        document.getElementsByName(e.target.name)[0].disabled = false;
    }
    handleChangeAddressDetail = (index, event) => {
        const target = event.target;
        const name = target.name;
        const value = target.type === 'checkbox' ? (target.checked?1:0) : target.value;
        let currentDetail = [...this.state.dataReturnAddress];
        if (target.type === 'checkbox') {
            for(let key in currentDetail) {
                currentDetail[key].is_selected = 0
            };
        }
        currentDetail[index][name] = value;
        this.setState({
            dataReturnAddress: currentDetail
        });
    }

    handleChangeEdit = (return_no, return_line_no, return_time, receive_instruction, event) => {
        let target = event.target;
        let name = target.name;
        let value = target.type === 'checkbox' ? (target.checked ? 1 : 0) : (target.value === '_none' ? '' : target.value);
        let dataSet = {[name]: value, return_no: return_no, return_line_no: return_line_no, return_time: return_time, receive_instruction: receive_instruction};
        let current = this.state.data_update;
        current[name] = dataSet;
        this.setState({
            data_update: {...current}
        });
    }

    render() {
        let dataSource          = this.props.dataSource;
        let dataResponse        = this.props.dataResponse;
        let returnTypeOptions   = [];
        let LargeOptions        = [];
        let returnTypeAllPotion = [];
        let MidOptions          = [];
        if (!_.isEmpty(dataSource)) {
            returnTypeOptions   = dataSource.dataReturnType;
            LargeOptions        = dataSource.dataLarge;
            returnTypeAllPotion = dataSource.returnTypeAllPotion;
            MidOptions          = dataSource.dataMid;
        }
        let isAdd = false;
        if (this.state.status === 'add') {
            isAdd = true;
        }
        var nf = new Intl.NumberFormat();
        return (!_.isEmpty(dataSource) &&
            <div className="box box-primary">
            <div className="box-body">
            <FORM
                className="form-horizontal form-return" id="return_detail">
                <Loading className="loading" />
                <MessagePopup classType="alert-error" status={(this.state.product_before === this.state.product_code && this.state.product_code) ? 'show' : ''} message={trans('messages.product_not_exists')} />
                <div className="row" style={{ borderBottom: "1px #000 solid" }}>
                    <h4>{trans('messages.return_detail_step_1')}</h4>
                    <div className="col-md-12">
                        <INPUT
                            name="receive_id"
                            labelTitle={trans('messages.returm_receive_id')}
                            className="form-control input-sm"
                            fieldGroupClass="col-md-4"
                            labelClass="col-md-2"
                            readOnly={true}
                            defaultValue={this.state.receive_id || ''}/>
                    </div>
                </div>
                <div className="row" style={{ borderBottom: "1px #000 solid" }}>

                        <h4 >{trans('messages.return_detail_step_2')}</h4>
                        {(!isAdd && this.state.return) &&
                            Object.keys(this.state.return).map((k, item) => {
                                let key = "return_type_mid_id" + this.state.return[k].return_no + this.state.return[k].return_line_no + this.state.return[k].return_time;
                                return (
                                    <div className="col-md-12 return_edit" key={'product_' + k}>
                                        <div className="row">
                                            <div className="col-md-6">
                                                <div className="row">
                                                <label className="col-md-12"><span className="pull-left">{this.state.return[k].times + trans('messages.return_times')}</span></label>
                                                </div>
                                            </div>

                                        </div>
                                        <div className="row">
                                            <div className="col-md-12">
                                                <div className="row">
                                                    <div className="col-md-6">
                                                    <DATEPICKER
                                                        groupClass="form-group"
                                                        placeholderText="YYYY-MM-DD"
                                                        dateFormat="YYYY-MM-DD"
                                                        locale="ja"
                                                        disabled={true}
                                                        name="return_date"
                                                        minDate={moment()}
                                                        labelTitle={trans('messages.return_date')}
                                                        className="form-control input-sm"
                                                        fieldGroupClass="col-md-9"
                                                        labelClass="col-md-3"
                                                        selected={this.state.return[k].return_date ? moment(this.state.return[k].return_date) : null}
                                                        />
                                                    </div>
                                                    <div className="col-md-6 ">
                                                        <INPUT
                                                            name="deje_no"
                                                            labelTitle={trans('messages.deje_num')}
                                                            groupClass="form-group"
                                                            className="form-control input-sm"
                                                            fieldGroupClass="col-md-9"
                                                            disabled={true}
                                                            labelClass="col-md-3"
                                                            defaultValue={this.state.return[k].deje_no || ''}/>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div className="row">
                                            <div className="col-md-12">
                                                <div className="row">
                                                    <div className="col-md-6 ">
                                                    <SELECT
                                                        name="return_type"
                                                        labelTitle={trans('messages.return_type')}
                                                        groupClass="form-group"
                                                        className="form-control input-sm"
                                                        fieldGroupClass="col-md-9"
                                                        disabled={true}
                                                        labelClass="col-md-3"
                                                        options={returnTypeOptions}
                                                        defaultValue={this.state.return[k].return_type !== '' ? this.state.return[k].return_type : '_none'}
                                                        />
                                                    </div>
                                                    <div className="col-md-6 ">
                                                    <SELECT
                                                        name="return_type_detail"
                                                        labelTitle={trans('messages.return_type_detail')}
                                                        groupClass="form-group"
                                                        className="form-control input-sm"
                                                        fieldGroupClass="col-md-9"
                                                        disabled={true}
                                                        labelClass="col-md-3"
                                                        options={returnTypeAllPotion}
                                                        defaultValue={this.state.return[k].return_type_detail !== '' ? this.state.return[k].return_type_detail : '_none'}
                                                        />
                                                    </div>
                                                </div>
                                            </div>
                                        </div>


                                        <div className="row">
                                            <div className="col-md-12">
                                                <div className="row">
                                                    <div className="col-md-6 ">
                                                        <SELECT
                                                            name="return_type_large_id"
                                                            labelTitle={trans('messages.return_type_large')}
                                                            className="form-control input-sm"
                                                            groupClass="form-group"
                                                            fieldGroupClass="col-md-9"
                                                            disabled={true}
                                                            labelClass="col-md-3"
                                                            options={LargeOptions}
                                                            defaultValue={this.state.return[k].return_type_large_id || '_none'}/>
                                                    </div>
                                                    <div className="col-md-6 ">
                                                        <SELECT
                                                            name={key}
                                                            labelTitle={trans('messages.return_type_mid')}
                                                            className="form-control input-sm"
                                                            groupClass="form-group"
                                                            fieldGroupClass="col-md-9"
                                                            labelClass="col-md-3"
                                                            disabled={this.state.return[k].return_type_mid_id === '6' ? false : true}
                                                            options={MidOptions}
                                                            errorPopup={true}
                                                            hasError={(dataResponse.message && dataResponse.message[key])?true:false}
                                                            errorMessage={(dataResponse.message&&dataResponse.message[key])?dataResponse.message[key]:""}
                                                            onChange={(e) => this.handleChangeEdit(this.state.return[k].return_no, this.state.return[k].return_line_no, this.state.return[k].return_time, this.state.return[k].receive_instruction, e)}

                                                            defaultValue={this.state.return[k].return_type_mid_id  || '_none'}/>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div className="form-group">
                                            <div className="col-md-12">
                                            <div className="table-responsive">
                                                <TABLE className="table table-striped table-custom table-rm-sdt" style={{ borderCollapse: "collapse" }}>
                                                    <THEAD>
                                                        <TR>
                                                            <TH className="text-left " colSpan="2">{trans('messages.product_code')}</TH>
                                                            <TH className="text-left ">{trans('messages.product_name')}</TH>
                                                            <TH className="text-right ">{trans('messages.quantity')}</TH>
                                                            <TH className="text-right ">{trans('messages.return_sale_price')}</TH>
                                                            <TH className="text-right ">{trans('messages.total_sub')}</TH>
                                                            <TH className="text-right ">{trans('messages.return_price')}</TH>
                                                            <TH className="text-right ">{trans('messages.txt_edi_order_code')}</TH>
                                                            <TH className="text-center "></TH>
                                                            <TH className="text-center"></TH>
                                                        </TR>
                                                    </THEAD>
                                                    <TBODY>
                                                        {
                                                            (!_.isEmpty(dataSource.dataProduct)) &&
                                                            dataSource.dataProduct.map((item) => {
                                                                let check = false;
                                                                let isSet = false;
                                                                if (item.product_code) {
                                                                    isSet = true;
                                                                }
                                                                if (!isAdd && this.state.return[k].check_return[item.detail_line_num] === 1) {
                                                                    check = true;
                                                                }
                                                                return (
                                                                    <TR key={"return_tr_" + (isSet ? '' : 'child_') + item.sub_line_num + k}>
                                                                        <TD className={"col-md-1 " + (isSet ? "" : "") + ((check && isSet) ? " is_return" : "") } colSpan={isSet ? "2" : "1"}>{item.product_code}</TD>
                                                                        {!item.product_code &&
                                                                            <TD className={" col-md-1 " + (check ? " is_return" : "")}>{item.child_product_code}</TD>
                                                                        }
                                                                        <TD className={" col-md-3 " + (check ? " is_return" : "")}>{item.product_name}</TD>

                                                                        <TD className={" col-md-0-5 text-right " + (check ? " is_return" : "")}>
                                                                            { (check) ?
                                                                                this.state.return[k].return_quantity[item.detail_line_num]
                                                                                :
                                                                                item.quantity
                                                                            }
                                                                        </TD>
                                                                        <TD className={" text-right col-md-1 " + (isSet ? '' : 'bg-disable') + (check ? " is_return" : "")}>{item.price ? nf.format(item.price) : ''}</TD>
                                                                        <TD className={" col-md-1 text-right " + (check ? " is_return" : "")}>{(item.price ? parseInt(item.price) : 0) * ((check) ? parseInt(this.state.return[k].return_quantity[item.detail_line_num]) : parseInt(item.quantity))}</TD>
                                                                        <TD className={" col-md-1 text-right " + (check ? " is_return" : "")}>{nf.format(item.price_invoice)}</TD>
                                                                        <TD className={" col-md-1 " + (check ? " is_return" : "")}>{item.edi_order_code}</TD>
                                                                        <TD className={" col-md-0-5 text-center" + (check ? " is_return" : "")}>
                                                                            <CHECKBOX_CUSTOM
                                                                                name={"check_return[" + item.detail_line_num +"]"}
                                                                                disabled={true}
                                                                                defaultChecked={check ? true : false}
                                                                             />
                                                                        </TD>
                                                                        <TD className={"col-md-2 " + ((check && (this.state.return[k].return_type_mid_id === '2' || (typeof this.state.data_update[key] !== 'undefined' && this.state.data_update[key][key] === '2'))) ? '' : 'hide-opacity')}>
                                                                            <a className={"link-detail " + (item.return_address ? '' : "text-red ") + (this.state.return[k].return_type_mid_id === '6' ? '' : 'disable-link' )} href="javascript:void(0)" onClick={(supplier_cd) => this.handleFormAddress(item.price_supplier_id)}>{item.address}</a>
                                                                        </TD>
                                                                    </TR>
                                                                )
                                                            })
                                                        }
                                                    </TBODY>
                                                </TABLE>
                                            </div>
                                            </div>
                                        </div>
                                        <div className="row">
                                            <div className="col-md-6">
                                                <DATEPICKER
                                                    placeholderText="YYYY-MM-DD"
                                                    dateFormat="YYYY-MM-DD"
                                                    locale="ja"
                                                    minDate={moment()}
                                                    name="receive_plan_date"
                                                    disabled={true}
                                                    labelTitle={trans('messages.receive_plan_date')}
                                                    groupClass="form-group"
                                                    className="form-control input-sm"
                                                    fieldGroupClass="col-md-9"
                                                    labelClass="col-md-3"
                                                    selected={this.state.return[k].receive_plan_date ? moment(this.state.return[k].receive_plan_date) : null}/>
                                            </div>
                                            <div className="col-md-6">
                                            <DATEPICKER
                                                placeholderText="YYYY-MM-DD"
                                                dateFormat="YYYY-MM-DD"
                                                locale="ja"
                                                minDate={moment()}
                                                name="delivery_plan_date"
                                                disabled={true}
                                                labelTitle={trans('messages.delivery_plan_date')}
                                                groupClass="form-group"
                                                className="form-control input-sm"
                                                fieldGroupClass="col-md-9"
                                                labelClass="col-md-3"
                                                selected={this.state.return[k].delivery_plan_date ? moment(this.state.return[k].delivery_plan_date) : null}/>
                                            </div>
                                        </div>
                                        <div className="row">
                                            <div className="col-md-6 col-md-offset-right-6">
                                                <div className="form-group">
                                                    <LABEL
                                                        groupClass="col-md-3 col-xs-6"
                                                        labelClass=" "
                                                        labelTitle={trans('messages.payment_on_delivery')}>
                                                    </LABEL>
                                                    <div className="col-md-2 col-xs-6">
                                                        <CHECKBOX_CUSTOM
                                                            name="payment_on_delivery"
                                                            disabled={true}
                                                            defaultChecked={this.state.return[k].payment_on_delivery === 1 ? true : false}
                                                         />
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div className="row">
                                            <div className="col-md-6 col-md-offset-right-6">

                                                <TEXTAREA
                                                    name="note"
                                                    labelTitle={trans('messages.note')}
                                                    className="form-control input-sm"
                                                    groupClass="form-group"
                                                    fieldGroupClass="col-md-9"
                                                    rows="4"
                                                    disabled={true}
                                                    labelClass="col-md-3"
                                                    defaultValue={this.state.return[k].note || ''}/>
                                            </div>
                                        </div>
                                    </div>
                                )
                            })

                        }
                    <div className={"col-md-12 " + (!isAdd ? 'show-edit collapse' : '')}>
                        <div className="row">
                            <div className="col-md-6">
                                <div className="form-group">
                                    <label className="col-md-3">{this.state.times_return + trans('messages.return_times')}</label>
                                </div>
                            </div>

                        </div>
                        <div className="row">
                            <div className="col-md-6">
                                <DATEPICKER
                                    groupClass="form-group"
                                    placeholderText="YYYY-MM-DD"
                                    dateFormat="YYYY-MM-DD"
                                    locale="ja"
                                    minDate={moment()}
                                    name="return_date"
                                    labelTitle={trans('messages.return_date')}
                                    className="form-control input-sm"
                                    fieldGroupClass="col-md-9"
                                    labelClass="col-md-3"
                                    selected={this.state.return_date ? moment(this.state.return_date) : null}
                                    onChange={(date,name, e) => this.handleChangeDate(date, 'return_date', e)} />
                            </div>
                            <div className="col-md-6 ">
                                <INPUT
                                    name="deje_no"
                                    labelTitle={trans('messages.deje_num')}
                                    groupClass="form-group"
                                    className="form-control input-sm"
                                    fieldGroupClass="col-md-9"
                                    labelClass="col-md-3"
                                    onChange={this.handleChange}
                                    defaultValue={this.state.deje_no || ''}
                                    errorPopup={true}
                                    hasError={(dataResponse.message && dataResponse.message.deje_no)?true:false}
                                    errorMessage={(dataResponse.message&&dataResponse.message.deje_no)?dataResponse.message.deje_no:""}
                                    />
                            </div>
                        </div>
                        <div className="row">
                            <div className="col-md-6 ">
                                <SELECT
                                    name="return_type"
                                    labelTitle={trans('messages.return_type')}
                                    groupClass="form-group"
                                    className="form-control input-sm"
                                    fieldGroupClass="col-md-9"
                                    labelClass="col-md-3"
                                    options={returnTypeOptions}
                                    onChange={this.handleChange}
                                    defaultValue={this.state.return_type !== '' ? this.state.return_type : '_none'}
                                    errorPopup={true}
                                    hasError={(dataResponse.message && dataResponse.message.return_type)?true:false}
                                    errorMessage={(dataResponse.message&&dataResponse.message.return_type)?dataResponse.message.return_type:""}
                                    />
                            </div>
                            <div className="col-md-6 ">
                                <SELECT
                                    name="return_type_detail"
                                    labelTitle={trans('messages.return_type_detail')}
                                    groupClass="form-group"
                                    className="form-control input-sm"
                                    fieldGroupClass="col-md-9"
                                    labelClass="col-md-3"
                                    options={this.state.returnTypePotion}
                                    onChange={this.handleChange}
                                    defaultValue={this.state.return_type_detail !== '' ? this.state.return_type_detail : '_none'}
                                    errorPopup={true}
                                    hasError={(dataResponse.message && dataResponse.message.return_type_detail)?true:false}
                                    errorMessage={(dataResponse.message&&dataResponse.message.return_type_detail)?dataResponse.message.return_type_detail:""}
                                    />
                            </div>
                        </div>
                        <div className="row">
                            <div className="col-md-6">
                                <SELECT
                                    name="return_type_large_id"
                                    labelTitle={trans('messages.return_type_large')}
                                    className="form-control input-sm"
                                    groupClass="form-group"
                                    fieldGroupClass="col-md-9"
                                    labelClass="col-md-3"
                                    options={LargeOptions}
                                    onChange={this.handleChange}
                                    defaultValue={this.state.return_type_large_id || '_none'}
                                    errorPopup={true}
                                    hasError={(dataResponse.message && dataResponse.message.return_type_large_id)?true:false}
                                    errorMessage={(dataResponse.message&&dataResponse.message.return_type_large_id)?dataResponse.message.return_type_large_id:""}
                                    />
                            </div>
                            <div className="col-md-6">
                                <SELECT
                                    name="return_type_mid_id"
                                    labelTitle={trans('messages.return_type_mid')}
                                    className="form-control input-sm"
                                    groupClass="form-group"
                                    fieldGroupClass="col-md-9"
                                    labelClass="col-md-3"
                                    options={MidOptions}
                                    onChange={this.handleChange}
                                    defaultValue={this.state.return_type_mid_id  || '_none'}
                                    errorPopup={true}
                                    hasError={(dataResponse.message && dataResponse.message.return_type_mid_id)?true:false}
                                    errorMessage={(dataResponse.message&&dataResponse.message.return_type_mid_id)?dataResponse.message.return_type_mid_id:""}
                                    />
                            </div>
                        </div>
                        <div className="form-group">
                            <div className="col-md-12">
                            <div className="table-responsive">
                                <TABLE className="table table-striped table-custom table-rm-sdt" style={{ borderCollapse: "collapse" }}>
                                    <THEAD>
                                        <TR>
                                            <TH className="text-center " colSpan="2">{trans('messages.product_code')}</TH>
                                            <TH className="text-center ">{trans('messages.product_name')}</TH>
                                            <TH className="text-center ">{trans('messages.quantity')}</TH>
                                            <TH className="text-center ">{trans('messages.return_sale_price')}</TH>
                                            <TH className="text-center ">{trans('messages.total_sub')}</TH>
                                            <TH className="text-center ">{trans('messages.return_price')}</TH>
                                            <TH className="text-center ">{trans('messages.txt_edi_order_code')}</TH>
                                            <TH className="text-center "></TH>
                                            <TH className={"text-center " + (!isAdd ? "border-repay" : "")}></TH>
                                        </TR>
                                    </THEAD>
                                    <TBODY>
                                        {
                                            (!_.isEmpty(dataSource.dataProduct)) &&
                                            dataSource.dataProduct.map((item) => {
                                                let check = false;
                                                let currentCheck = false;
                                                let isSet = false;
                                                if (item.product_code) {
                                                    isSet = true;
                                                }
                                                if (!isAdd && this.state.checked[item.detail_line_num] === 1) {
                                                    check = true;
                                                }
                                                if (this.state.check_return[item.detail_line_num] === 1) {
                                                    currentCheck = true;
                                                }
                                                return (
                                                    <TR key={"tr_" + (isSet ? '' : 'child_') + item.sub_line_num} className={check ? 'color-repay-dis' : ''}>
                                                        <TD className={"col-md-1 " + (isSet ? "" : (!isAdd ? "color-repay" : "")) } colSpan={isSet ? "2" : "1"}>{item.product_code}</TD>
                                                        {!item.product_code &&
                                                            <TD className=" col-md-1">{item.child_product_code}</TD>
                                                        }
                                                        <TD className=" col-md-3">{item.product_name}</TD>

                                                        <TD className=" col-md-1">
                                                            { (isSet) ?
                                                                ((check) ?
                                                                    this.state.return_quantity[item.detail_line_num]
                                                                    :
                                                                    <INPUT
                                                                        name={"return_quantity[" + item.detail_line_num +"]"}
                                                                        groupClass="w-100"
                                                                        className="form-control input-sm"
                                                                        value={this.state.return_quantity[item.detail_line_num]}
                                                                        type="number"
                                                                        min="0"
                                                                        max={item.received_order_num}
                                                                        onChange={this.handleChange}
                                                                        errorPopup={true}
                                                                        hasError={(dataResponse.message && dataResponse.message.return_quantity && dataResponse.message.return_quantity[item.detail_line_num])?true:false}
                                                                        errorMessage={(dataResponse.message && dataResponse.message.return_quantity && dataResponse.message.return_quantity[item.detail_line_num])?dataResponse.message.return_quantity[item.detail_line_num]:""}
                                                                        />
                                                                )
                                                                :
                                                                item.received_order_num
                                                            }
                                                        </TD>
                                                        <TD className={" col-md-1 text-right " + (isSet ? '' : 'bg-disable')}>{item.price ? nf.format(item.price) : ''}</TD>
                                                        <TD className=" col-md-1 text-right">{nf.format(item.price*item.quantity)}</TD>
                                                        <TD className=" col-md-1 text-right">{nf.format(item.price_invoice)}</TD>
                                                        <TD className=" col-md-1">{item.edi_order_code}</TD>
                                                        <TD className=" col-md-0-5">
                                                            {!check &&
                                                                <CHECKBOX_CUSTOM
                                                                    key={currentCheck}
                                                                    name={"check_return[" + item.detail_line_num +"]"}
                                                                    defaultChecked={currentCheck ? true : false}
                                                                    onChange={this.handleChange}
                                                                 />
                                                            }
                                                        </TD>
                                                        <TD className={"col-md-2 " + ((this.state.check_return[item.detail_line_num] === 1 && this.state.return_type_mid_id === '2') ? '' : 'hide-opacity')}>
                                                            <a className={"link-detail " + (item.return_address ? '' : "text-red ")} href="javascript:void(0)" onClick={(supplier_cd) => this.handleFormAddress(item.price_supplier_id)}>{item.address}</a>
                                                        </TD>
                                                    </TR>
                                                )
                                            })
                                        }
                                    </TBODY>
                                </TABLE>
                            </div>
                            </div>
                        </div>
                        <div className="row">
                            <div className="col-md-6">
                                <DATEPICKER
                                    placeholderText="YYYY-MM-DD"
                                    dateFormat="YYYY-MM-DD"
                                    locale="ja"
                                    minDate={moment()}
                                    name="receive_plan_date"
                                    labelTitle={trans('messages.receive_plan_date')}
                                    groupClass="form-group"
                                    className="form-control input-sm"
                                    fieldGroupClass="col-md-9"
                                    labelClass="col-md-3"
                                    selected={this.state.receive_plan_date ? moment(this.state.receive_plan_date) : null}
                                    onChange={(date,name, e) => this.handleChangeDate(date, 'receive_plan_date', e)} />
                            </div>
                            <div className="col-md-6">
                                <DATEPICKER
                                    placeholderText="YYYY-MM-DD"
                                    dateFormat="YYYY-MM-DD"
                                    locale="ja"
                                    minDate={moment()}
                                    name="delivery_plan_date"
                                    labelTitle={trans('messages.delivery_plan_date')}
                                    groupClass="form-group"
                                    className="form-control input-sm"
                                    fieldGroupClass="col-md-9"
                                    labelClass="col-md-3"
                                    selected={this.state.delivery_plan_date ? moment(this.state.delivery_plan_date) : null}
                                    onChange={(date,name, e) => this.handleChangeDate(date, 'delivery_plan_date', e)} />
                            </div>
                        </div>
                        <div className="row">
                            <div className="col-md-6 col-md-offset-right-6">
                                <div className="form-group">
                                    <LABEL
                                        groupClass="col-md-3 col-xs-6"
                                        labelClass=" "
                                        labelTitle={trans('messages.payment_on_delivery')}>
                                    </LABEL>
                                    <div className="col-md-2 col-xs-6">
                                        <CHECKBOX_CUSTOM
                                        name="payment_on_delivery"
                                        defaultChecked={this.state.payment_on_delivery === 1 ? true : false}
                                        onChange={this.handleChange}
                                         />
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="row">
                            <div className="col-md-6 col-md-offset-right-6">
                                <TEXTAREA
                                    name="note"
                                    labelTitle={trans('messages.note')}
                                    className="form-control input-sm"
                                    groupClass="form-group"
                                    fieldGroupClass="col-md-9"
                                    rows="4"
                                    labelClass="col-md-3"
                                    onChange={this.handleChange}
                                    value={this.state.note || ''}/>
                            </div>
                        </div>
                    </div>
                    <div className="col-md-11 col-md-offset-1 margin-top-bot">
                        <BUTTON name="btnEdit" id="btnEdit" className={"btn btn-success input-sm btn-larger pull-right " + (this.state.status !== 'edit' ? 'hide' : '')} type="button" onClick={this.handleEdit}>{trans('messages.btn_edit')}</BUTTON>
                    </div>
                </div>
                <div className="row">
                    <h4 className="">{trans('messages.return_detail_step_3')}</h4>
                    {(!isAdd && this.state.return) &&
                        Object.keys(this.state.return).map((k, item) => {
                            return (
                                <div className="row return_edit" key={'repay_' + k}>
                                    <div className="col-md-12">
                                        <div className="col-md-6 form-group">
                                            <label className="col-md-3 clear-padding">{this.state.return[k].times + trans('messages.return_times')}</label>
                                        </div>
                                    </div>
                                    <div className="col-md-12">
                                        <div className="col-md-5 form-group">
                                            <INPUT
                                                labelTitle={trans('messages.bank_code')}
                                                groupClass=" "
                                                className="form-control input-sm"
                                                fieldGroupClass="col-md-3 clear-padding-left"
                                                labelClass="col-md-3 clear-padding"
                                                defaultValue={this.state.return[k].repay_bank_code || ''}/>
                                            <INPUT
                                                labelTitle={trans('messages.payment_bank')}
                                                className="form-control input-sm"
                                                groupClass=" "
                                                fieldGroupClass="col-md-4 clear-padding-left"
                                                labelClass="col-md-2 clear-padding"
                                                defaultValue={this.state.return[k].repay_bank_name || ''}/>
                                        </div>
                                        <div className="col-md-7 form-group">
                                            <INPUT
                                                labelTitle={trans('messages.rep_bank_branch_code')}
                                                groupClass=" "
                                                className="form-control input-sm"
                                                fieldGroupClass="col-md-3 clear-padding-left"
                                                labelClass="col-md-2 clear-padding"
                                                defaultValue={this.state.return[k].repay_bank_branch_code || ''}/>
                                            <INPUT
                                                labelTitle={trans('messages.rep_bank_branch_name')}
                                                className="form-control input-sm"
                                                groupClass=" "
                                                fieldGroupClass="col-md-5 clear-padding-left"
                                                labelClass="col-md-2 clear-padding"
                                                defaultValue={this.state.return[k].repay_bank_branch_name || ''}/>
                                        </div>
                                    </div>
                                    <div className="col-md-12">
                                        <div className="col-md-5 form-group">
                                            <INPUT
                                                labelTitle={trans('messages.rep_name')}
                                                groupClass=" "
                                                className="form-control input-sm"
                                                fieldGroupClass="col-md-3 clear-padding-left"
                                                labelClass="col-md-3 clear-padding"
                                                defaultValue={this.state.return[k].repay_name || ''}/>
                                            <INPUT
                                                labelTitle={trans('messages.rep_bank_account')}
                                                groupClass=" "
                                                className="form-control input-sm"
                                                fieldGroupClass="col-md-4 clear-padding-left"
                                                labelClass="col-md-2 clear-padding"
                                                defaultValue={this.state.return[k].repay_bank_account || ''}/>
                                        </div>
                                        <div className="col-md-7 form-group">
                                            <INPUT
                                                labelTitle={trans('messages.return_point')}
                                                groupClass=" "
                                                className="form-control input-sm"
                                                fieldGroupClass="col-md-3 clear-padding-left"
                                                labelClass="col-md-2 clear-padding"
                                                defaultValue={this.state.return[k].return_point || 0}/>
                                            <INPUT
                                                labelTitle={trans('messages.repay_price')}
                                                className="form-control input-sm"
                                                groupClass=" "
                                                fieldGroupClass="col-md-5 clear-padding-left"
                                                labelClass="col-md-2 clear-padding"
                                                defaultValue={this.state.return[k].repay_price || 0}/>
                                        </div>
                                    </div>
                                    <div className="col-md-12">
                                        <div className="col-md-5 form-group">
                                            <INPUT
                                                labelTitle={trans('messages.debosit_commission')}
                                                groupClass=" "
                                                className="form-control input-sm"
                                                fieldGroupClass="col-md-3 clear-padding-left"
                                                labelClass="col-md-3 clear-padding"
                                                defaultValue={this.state.return[k].debosit_commission || ''}/>
                                            <INPUT
                                                labelTitle={trans('messages.return_fee_repay')}
                                                groupClass=" "
                                                className="form-control input-sm"
                                                fieldGroupClass="col-md-4 clear-padding-left"
                                                labelClass="col-md-2 clear-padding"
                                                defaultValue={this.state.return[k].return_fee || ''}/>
                                        </div>
                                        <div className="col-md-7 form-group">
                                            <INPUT
                                                labelTitle={trans('messages.other_commission')}
                                                groupClass=" "
                                                className="form-control input-sm"
                                                fieldGroupClass="col-md-3 clear-padding-left"
                                                labelClass="col-md-2 clear-padding"
                                                defaultValue={this.state.return[k].other_commission || ''}/>
                                            <INPUT
                                                labelTitle={trans('messages.repay_coupon')}
                                                className="form-control input-sm"
                                                groupClass=" "
                                                fieldGroupClass="col-md-5 clear-padding-left"
                                                labelClass="col-md-2 clear-padding"
                                                defaultValue={this.state.return[k].return_coupon || ''}/>
                                        </div>
                                    </div>
                                    <div className="col-md-12">
                                        <div className="col-md-5 form-group">
                                            <INPUT
                                                labelTitle={trans('messages.total_return_price')}
                                                className="form-control input-sm"
                                                groupClass=" "
                                                fieldGroupClass="col-md-3 clear-padding-left"
                                                labelClass="col-md-3 clear-padding"
                                                defaultValue={this.state.return[k].total_return_price || ''}/>
                                        </div>
                                    </div>
                                </div>
                            )
                        })
                    }
                    <div className={"row " + (!isAdd ? 'show-edit collapse' : '')}>
                    <div className="col-md-12">
                        <div className="col-md-6 form-group">
                            <label className="col-md-3 clear-padding">{this.state.times_return + trans('messages.return_times')}</label>
                        </div>
                    </div>
                    <div className="col-md-12">
                        <div className="col-md-5 form-group">
                            <INPUT
                                name="repay_bank_code"
                                labelTitle={trans('messages.bank_code')}
                                groupClass=" "
                                className="form-control input-sm"
                                fieldGroupClass="col-md-3 clear-padding-left"
                                labelClass="col-md-3 clear-padding"
                                onChange={this.handleChange}
                                defaultValue={this.state.repay_bank_code || ''}/>
                            <INPUT
                                name="repay_bank_name"
                                labelTitle={trans('messages.payment_bank')}
                                className="form-control input-sm"
                                groupClass=" "
                                fieldGroupClass="col-md-4 clear-padding-left"
                                labelClass="col-md-2 clear-padding"
                                onChange={this.handleChange}
                                defaultValue={this.state.repay_bank_name || ''}/>
                        </div>
                        <div className="col-md-7 form-group">
                            <INPUT
                                name="repay_bank_branch_code"
                                labelTitle={trans('messages.rep_bank_branch_code')}
                                groupClass=" "
                                className="form-control input-sm"
                                fieldGroupClass="col-md-3 clear-padding-left"
                                labelClass="col-md-2 clear-padding"
                                onChange={this.handleChange}
                                defaultValue={this.state.repay_bank_branch_code || ''}/>
                            <INPUT
                                name="repay_bank_branch_name"
                                labelTitle={trans('messages.rep_bank_branch_name')}
                                className="form-control input-sm"
                                groupClass=" "
                                fieldGroupClass="col-md-5 clear-padding-left"
                                labelClass="col-md-2 clear-padding"
                                onChange={this.handleChange}
                                defaultValue={this.state.repay_bank_branch_name || ''}/>
                        </div>
                    </div>
                    <div className="col-md-12">
                        <div className="col-md-5 form-group">
                            <INPUT
                                name="repay_name"
                                labelTitle={trans('messages.rep_name')}
                                groupClass=" "
                                className="form-control input-sm"
                                fieldGroupClass="col-md-3 clear-padding-left"
                                labelClass="col-md-3 clear-padding"
                                onChange={this.handleChange}
                                defaultValue={this.state.repay_name || ''}/>
                            <INPUT
                                name="repay_bank_account"
                                labelTitle={trans('messages.rep_bank_account')}
                                groupClass=" "
                                className="form-control input-sm"
                                fieldGroupClass="col-md-4 clear-padding-left"
                                labelClass="col-md-2 clear-padding"
                                onChange={this.handleChange}
                                defaultValue={this.state.repay_bank_account || ''}
                                errorPopup={true}
                                hasError={(dataResponse.message && dataResponse.message.repay_bank_account)?true:false}
                                errorMessage={(dataResponse.message&&dataResponse.message.repay_bank_account)?dataResponse.message.repay_bank_account:""}/>
                        </div>
                        <div className="col-md-7 form-group">
                            <INPUT
                                name="return_point"
                                labelTitle={trans('messages.return_point')}
                                groupClass=" "
                                className="form-control input-sm"
                                fieldGroupClass="col-md-3 clear-padding-left"
                                labelClass="col-md-2 clear-padding"
                                onChange={this.handleChange}
                                defaultValue={this.state.return_point || ''}
                                errorPopup={true}
                                hasError={(dataResponse.message && dataResponse.message.return_point)?true:false}
                                errorMessage={(dataResponse.message&&dataResponse.message.return_point)?dataResponse.message.return_point:""}/>
                            <INPUT
                                name="repay_price"
                                labelTitle={trans('messages.repay_price')}
                                className="form-control input-sm"
                                groupClass=" "
                                fieldGroupClass="col-md-5 clear-padding-left"
                                labelClass="col-md-2 clear-padding"
                                onChange={this.handleChange}
                                defaultValue={this.state.repay_price || ''}
                                errorPopup={true}
                                hasError={(dataResponse.message && dataResponse.message.repay_price)?true:false}
                                errorMessage={(dataResponse.message&&dataResponse.message.repay_price)?dataResponse.message.repay_price:""}/>
                        </div>
                    </div>
                    <div className="col-md-12">
                        <div className="col-md-5 form-group">
                            <INPUT
                                name="debosit_commission"
                                labelTitle={trans('messages.debosit_commission')}
                                groupClass=" "
                                className="form-control input-sm"
                                fieldGroupClass="col-md-3 clear-padding-left"
                                labelClass="col-md-3 clear-padding"
                                onChange={this.handleChange}
                                defaultValue={this.state.debosit_commission || ''}
                                errorPopup={true}
                                hasError={(dataResponse.message && dataResponse.message.debosit_commission)?true:false}
                                errorMessage={(dataResponse.message&&dataResponse.message.debosit_commission)?dataResponse.message.debosit_commission:""}/>
                            <INPUT
                                name="return_fee"
                                labelTitle={trans('messages.return_fee_repay')}
                                groupClass=" "
                                className="form-control input-sm"
                                fieldGroupClass="col-md-4 clear-padding-left"
                                labelClass="col-md-2 clear-padding"
                                onChange={this.handleChange}
                                defaultValue={this.state.return_fee || ''}
                                errorPopup={true}
                                hasError={(dataResponse.message && dataResponse.message.return_fee)?true:false}
                                errorMessage={(dataResponse.message&&dataResponse.message.return_fee)?dataResponse.message.return_fee:""}/>
                        </div>
                        <div className="col-md-7 form-group">
                            <INPUT
                                name="other_commission"
                                labelTitle={trans('messages.other_commission')}
                                groupClass=" "
                                className="form-control input-sm"
                                fieldGroupClass="col-md-3 clear-padding-left"
                                labelClass="col-md-2 clear-padding"
                                onChange={this.handleChange}
                                defaultValue={this.state.other_commission || ''}
                                errorPopup={true}
                                hasError={(dataResponse.message && dataResponse.message.other_commission)?true:false}
                                errorMessage={(dataResponse.message&&dataResponse.message.other_commission)?dataResponse.message.other_commission:""}/>
                            <INPUT
                                name="repay_coupon"
                                labelTitle={trans('messages.repay_coupon')}
                                className="form-control input-sm"
                                groupClass=" "
                                fieldGroupClass="col-md-5 clear-padding-left"
                                labelClass="col-md-2 clear-padding"
                                onChange={this.handleChange}
                                defaultValue={this.state.repay_coupon || ''}
                                errorPopup={true}
                                hasError={(dataResponse.message && dataResponse.message.repay_coupon)?true:false}
                                errorMessage={(dataResponse.message&&dataResponse.message.repay_coupon)?dataResponse.message.repay_coupon:""}/>
                        </div>
                    </div>
                    <div className="col-md-12">
                        <div className="col-md-5 form-group">
                            <INPUT
                                name="total_return_price"
                                labelTitle={trans('messages.total_return_price')}
                                className="form-control input-sm"
                                groupClass=" "
                                fieldGroupClass="col-md-3 clear-padding-left"
                                labelClass="col-md-3 clear-padding"
                                onChange={this.handleChange}
                                defaultValue={this.state.total_return_price || ''}
                                errorPopup={true}
                                hasError={(dataResponse.message && dataResponse.message.total_return_price)?true:false}
                                errorMessage={(dataResponse.message&&dataResponse.message.total_return_price)?dataResponse.message.total_return_price:""}/>
                        </div>
                    </div>
                    </div>
                </div>
                <div className="row margin-top-bot">
                    <div className="col-md-6 col-md-offset-6 col-md-offset-right-6 ">
                        <BUTTON className="btn bg-purple input-sm pull-right btn-larger" type="button" onClick={this.handleCancel}>{trans('messages.btn_cancel')}</BUTTON>
                        <BUTTON name="btnAccept" id="btnSubmit" className="btn btn-success input-sm btn-larger margin-element pull-right" type="button" onClick={this.handleSubmit}>{trans('messages.btn_accept')}</BUTTON>
                    </div>
                </div>
            </FORM>
                <FormAdress
                    dataReturnAddress={this.state.dataReturnAddress}
                    messagesTotal={this.state.messages_total}
                    messages={this.state.messages}
                    handleAddNewLine={this.handleAddNewLine}
                    supplierCd={this.state.supplier_cd}
                    handleAddressSubmit={this.handleAddressSubmit}
                    handleChangeAddressDetail={this.handleChangeAddressDetail}
                    handleImportCancel={this.handleCancelPopup}/>
            </div>
            <MESSAGE_MODAL
                message={this.state.message || ''}
                title={trans('messages.deny_edit')}
                baseUrl={baseUrl}
            />
            </div>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        dataSource: state.commonReducer.dataSource,
        dataResponse: state.commonReducer.dataResponse,
        dataRealTime: state.commonReducer.dataRealTime
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        reloadData: (url, params) => { dispatch(Action.detailData(url, params)) },
        postData: (url, params, headers) => { dispatch(Action.postData(url, params, headers)) },
        postRealTime: (url, params, headers) => { dispatch(Action.postRealTime(url, params, headers)) }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(ReturnSaveDetail)