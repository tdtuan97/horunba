import React, { Component } from 'react';
import Loading from '../../common/components/common/Loading';
export default class PopupProduct extends Component {
    constructor(props) {
        super(props);
        this.state = {}
    }
    handleCancelClick = () => {
        this.props.handleCancel()
    }
    render() {
        return (
            <div className="modal fade" id="popup-product-modal">
                <Loading className="loading" />
                <div className="modal-dialog ">
                    <div className="modal-content">
                        <div className="modal-header">
                        <b>{trans('messages.model_warning_point_title')}</b>
                        </div>
                        <div className="modal-body modal-custom-body">
                            <div className="row">
                                <div className="col-md-12 message-popup">
                                    {trans('messages.popup_edi_order_code')}
                                </div>
                            </div>
                        </div>
                        <div className="modal-footer modal-custom-footer">
                            <button type="button" className="btn bg-purple btn-sm modal-custom-button" onClick={this.handleCancelClick}>{trans('messages.btn_accept_cancel')}</button>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}