import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Loading from '../../common/components/common/Loading';
import axios from 'axios';
import UPLOAD from '../../common/components/form/UPLOAD';
import { connect } from 'react-redux';
import * as Action from '../../common/actions';
import BUTTON from '../../common/components/form/BUTTON';
import FORM from '../../common/components/form/FORM';
import INPUT from '../../common/components/form/INPUT';
import DATEPICKER from '../../common/components/form/DATEPICKER';

import TABLE from '../../common/components/table/TABLE';
import THEAD from '../../common/components/table/THEAD';
import TBODY from '../../common/components/table/TBODY';
import TR from '../../common/components/table/TR';
import TH from '../../common/components/table/TH';
import TD from '../../common/components/table/TD';
import SELECT from '../../common/components/form/SELECT';
import LABEL from '../../common/components/form/LABEL';
import ERROR from '../../common/components/form/ERROR';
import CHECKBOX_CUSTOM from '../../common/components/form/CHECKBOX_CUSTOM';
import moment from 'moment';

class FormAddress extends Component {
    constructor(props) {
        super(props);
    }
    render() {
        let token           = $("meta[name='csrf-token']").attr("content");
        let dataSource      = this.props.dataReturnAddress;
        let dataMessages    = this.props.messages;
        let dataMessagesTotal    = this.props.messagesTotal;
        let index = 0;
        let returnAddressId = 0;
        if (this.props.returnAddressId) {
            returnAddressId = this.props.returnAddressId;
        }
        return (
            <div className="modal fade" id="issue-modal">
                <Loading className="loading" />
                <input type="hidden" id="action-modal" />
                <div className="modal-dialog" style={{'maxWidth' : '1000px' , 'width' : '100%'}}>
                    <div className={"modal-content modal-custom-content " + (this.props.process ? 'hidden-div-csv' : '')}>
                        <div className={"modal-body modal-custom-body " + (this.props.error === 1 ? 'hidden' : '')}>
                            <p>返品住所編集</p>
                            <form className="form-horizontal"
                            encType={"multipart/form-data; boundary=----WebKitFormBoundary" + token}>
                            <span className="help-block">
                                <strong className="error-new-line text-red">{dataMessagesTotal}</strong>
                            </span>
                            <TABLE className="table table-striped table-custom">
                                    <THEAD>
                                        <TR>
                                            <TH className="visible-xs visible-sm"></TH>
                                            <TH className="hidden-xs hidden-sm text-center">
                                                #
                                            </TH>
                                            <TH className="hidden-xs hidden-sm text-center ">{trans('messages.return_nm')}</TH>
                                            <TH className="hidden-xs hidden-sm text-center ">{trans('messages.return_postal')}</TH>
                                            <TH className="hidden-xs hidden-sm text-center ">{trans('messages.return_pref')}</TH>
                                            <TH className="hidden-xs hidden-sm text-center ">{trans('messages.return_address')}</TH>
                                            <TH className="hidden-xs hidden-sm text-center ">{trans('messages.return_tel')}</TH>
                                            <TH className="hidden-xs hidden-sm text-center "></TH>
                                        </TR>
                                    </THEAD>
                                    <TBODY>
                                        {

                                            (!_.isEmpty(dataSource))?
                                            dataSource.map((item, index) => {
                                                let nf = new Intl.NumberFormat();
                                                let i = index + 1;
                                                let return_nm =  item.return_nm ;
                                                let return_postal =  item.return_postal ;
                                                let return_address =  item.return_address ;
                                                let return_pref =  item.return_pref ;
                                                let return_tel =  item.return_tel ;
                                                let is_selected =  item.is_selected ;
                                                if (returnAddressId != 0 && this.props.firstTime === true) {
                                                    is_selected = 0;
                                                    if (returnAddressId == item.return_id) {
                                                        is_selected = 1;
                                                    }
                                                }
                                                return (
                                                    [
                                                    <TR key={"tr" + item.index}>
                                                        <TD className="visible-xs visible-sm text-center">
                                                            <b className="show-info">
                                                                <i className="fa fa-angle-double-down" aria-hidden="true"></i>
                                                            </b>
                                                        </TD>
                                                        <TD className="hidden-xs hidden-sm text-center">
                                                           {i}
                                                        </TD>
                                                        <TD className="hidden-xs hidden-sm text-center">
                                                           {
                                                            item.new_item  ?
                                                            <INPUT
                                                                name="return_nm"
                                                                className="form-control input-sm"
                                                                value={return_nm}
                                                                groupClass="."
                                                                onChange={(e) => this.props.handleChangeAddressDetail(index, e)}
                                                                errorPopup={true}
                                                                hasError={
                                                                    (dataMessages &&
                                                                    dataMessages[index] &&
                                                                    dataMessages[index]['return_nm'])?
                                                                    true:false
                                                                }
                                                                errorMessage={
                                                                    (dataMessages &&
                                                                    dataMessages[index] &&
                                                                    dataMessages[index]['return_nm'])?
                                                                    dataMessages[index]['return_nm']:""
                                                                }
                                                            />
                                                            : return_nm
                                                           }
                                                        </TD>
                                                        <TD className="cut-text text-left" title={return_postal}>
                                                        {
                                                            item.new_item  ?
                                                            <INPUT
                                                                name="return_postal"
                                                                className="form-control input-sm"
                                                                value={return_postal}
                                                                groupClass="."
                                                                onChange={(e) => this.props.handleChangeAddressDetail(index, e)}
                                                                errorPopup={true}
                                                                hasError={
                                                                    (dataMessages &&
                                                                    dataMessages[index] &&
                                                                    dataMessages[index]['return_postal'])?
                                                                    true:false
                                                                }
                                                                errorMessage={
                                                                    (dataMessages &&
                                                                    dataMessages[index] &&
                                                                    dataMessages[index]['return_postal'])?
                                                                    dataMessages[index]['return_postal']:""
                                                                }
                                                            />
                                                            : return_postal
                                                           }

                                                        </TD>
                                                        <TD className="hidden-xs hidden-sm cut-text text-right" title={return_pref}>
                                                        {
                                                            item.new_item  ?
                                                            <INPUT
                                                                name="return_pref"
                                                                className="form-control input-sm"
                                                                value={return_pref}
                                                                groupClass="."
                                                                onChange={(e) => this.props.handleChangeAddressDetail(index, e)}
                                                                errorPopup={true}
                                                                hasError={
                                                                    (dataMessages &&
                                                                    dataMessages[index] &&
                                                                    dataMessages[index]['return_pref'])?
                                                                    true:false
                                                                }
                                                                errorMessage={
                                                                    (dataMessages &&
                                                                    dataMessages[index] &&
                                                                    dataMessages[index]['return_pref'])?
                                                                    dataMessages[index]['return_pref']:""
                                                                }
                                                            />
                                                            : return_pref
                                                           }
                                                        </TD>
                                                        <TD className="text-left text-long-batch max-w-200" title={return_address}>
                                                        {
                                                            item.new_item  ?
                                                            <INPUT
                                                                name="return_address"
                                                                className="form-control input-sm"
                                                                value={return_address}
                                                                groupClass="."
                                                                onChange={(e) => this.props.handleChangeAddressDetail(index, e)}
                                                                errorPopup={true}
                                                                hasError={
                                                                    (dataMessages &&
                                                                    dataMessages[index] &&
                                                                    dataMessages[index]['return_address'])?
                                                                    true:false
                                                                }
                                                                errorMessage={
                                                                    (dataMessages &&
                                                                    dataMessages[index] &&
                                                                    dataMessages[index]['return_address'])?
                                                                    dataMessages[index]['return_address']:""
                                                                }
                                                            />
                                                            : return_address
                                                           }
                                                        </TD>
                                                        <TD className="hidden-xs hidden-sm cut-text text-left text-long-batch max-w-200" title={return_tel}>
                                                        {
                                                            item.new_item  ?
                                                            <INPUT
                                                                name="return_tel"
                                                                className="form-control input-sm"
                                                                value={return_tel}
                                                                groupClass="."
                                                                onChange={(e) => this.props.handleChangeAddressDetail(index, e)}
                                                                errorPopup={true}
                                                                hasError={
                                                                    (dataMessages &&
                                                                    dataMessages[index] &&
                                                                    dataMessages[index]['return_tel'])?
                                                                    true:false
                                                                }
                                                                errorMessage={
                                                                    (dataMessages &&
                                                                    dataMessages[index] &&
                                                                    dataMessages[index]['return_tel'])?
                                                                    dataMessages[index]['return_tel']:""
                                                                }
                                                            />
                                                            : return_tel
                                                           }
                                                        </TD>
                                                        <TD className="hidden-xs hidden-sm text-center">
                                                            <CHECKBOX_CUSTOM
                                                                key={"is_selected" + item.return_id + is_selected}
                                                                defaultChecked={is_selected ? true : false}
                                                                name="is_selected"
                                                                className="is-check"
                                                                onChange={(e) => this.props.handleChangeAddressDetail(index, e)}
                                                                value={is_selected}

                                                             />
                                                        </TD>

                                                    </TR>
                                                   ]

                                                )

                                            })
                                            :
                                            <TR><TD colSpan="7" className="text-center">{trans('messages.no_data')}</TD></TR>
                                        }
                                    </TBODY>
                                </TABLE>

                            </form>
                        </div>
                        <div className="modal-footer modal-custom-footer">
                            <button type="button" className="btn btn-success" onClick={(e) => this.props.handleAddNewLine(e)}>{trans('messages.btn_add_return_address')}</button>
                        </div>
                        <div className="modal-footer modal-custom-footer">
                            <button name="btnSubmit" type="button" className="btn btn-primary modal-custom-button" disabled={this.props.error === 1 ? true : false} onClick={(e) => this.props.handleAddressSubmit(e)}>{trans('messages.btn_accept')}</button>
                            <button name="btnCancel" type="button" className="btn bg-purple modal-custom-button" onClick={(e) => this.props.handleImportCancel(e)}>{trans('messages.btn_cancel')}</button>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}
const mapStateToProps = (state) => {
    return {
        dataSource: state.commonReducer.dataSource,
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        postData: (url, params, headers) => { dispatch(Action.postData(url, params, headers)) }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(FormAddress)
