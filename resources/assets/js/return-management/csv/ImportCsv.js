import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Loading from '../../common/components/common/Loading';
import axios from 'axios';
import UPLOAD from '../../common/components/form/UPLOAD';
import RADIO_CUSTOM from '../../common/components/form/RADIO_CUSTOM';
import { connect } from 'react-redux';
import * as Action from '../../common/actions';

class ImportCsv extends Component {
    constructor(props) {
        super(props);
    }
    render() {
        let token = $("meta[name='csrf-token']").attr("content");
        let dataResponse = this.props.dataResponse;
        return (
            <div className="modal fade" id="issue-modal">
                <Loading className="loading" />
                <input type="hidden" id="action-modal" />
                <div className="modal-dialog">
                    <div className={"modal-content modal-custom-content " + (this.props.process ? 'hidden-div-csv' : '')}>
                        <div className={"modal-body modal-custom-body " + ((this.props.error === 1 || this.props.finish === 1) ? 'hidden' : '')}>
                            <p>{trans('messages.return_management_modal_title')}</p>
                            <form className="form-horizontal"
                            encType={"multipart/form-data; boundary=----WebKitFormBoundary" + token}>
                                <UPLOAD
                                    name="attached_file_path"
                                    className="form-control input-sm"
                                    fieldGroupClass="col-md-12"
                                    itemGroupClass="input-group input-group-sm"
                                    btnClass="btn btn-info btn-flat btn-largest"
                                    btnTitle={trans('messages.btn_browser')}
                                    onChange={this.props.handleChooseFile}
                                    value={this.props.value}
                                    hasError={this.props.hasError}
                                    errorMessage={this.props.errorMessage}/>
                                <div className="row">
                                    <div className="col-md-3">
                                    <label>
                                        <RADIO_CUSTOM
                                            name="return_type_mid"
                                            value="1"
                                            defaultChecked
                                             />
                                        <span>{" " + trans('messages.return_type_mid_radio')}</span>  

                                        </label>                                        
                                    </div>
                                    <div className="col-md-6"> 
                                    <label>
                                        <RADIO_CUSTOM
                                            name="return_type_mid"
                                            value="2"                                      
                                             />
                                        <span>{" " + trans('messages.btn_mojax')}</span>  
                                        </label>
                                    </div>
                                </div>
                                
                            </form>
                        </div>
                        <div className={"modal-body modal-custom-body " + (this.props.error === 1 ? 'show' : 'hidden')}>
                            <p> Have error please refer attachment <a href={'/downloaderror/' + this.props.filename} className="link-detail">file</a></p>
                        </div>
                        <div className={"modal-body modal-custom-body " + ((this.props.error !== 1 && this.props.finish === 1) ? 'show' : 'hidden')}>
                            <p>{trans('messages.message_import_success')}</p>
                        </div>
                        <div className="modal-footer modal-custom-footer">
                            <button type="button" className="btn btn-primary modal-custom-button" disabled={this.props.error === 1 ? true : false} onClick={(e) => this.props.handleImportSubmit(e)}>{trans('messages.issue_modal_ok')}</button>
                            <button type="button" className="btn bg-purple modal-custom-button" onClick={(e) => this.props.handleImportCancel(e)}>{trans('messages.issue_modal_cancel')}</button>
                        </div>
                    </div>
                    <div className={"progress collapse " + (this.props.status ? this.props.status : '')}>
                        <div className={"progress-bar " + this.props.processBarColor +" progress-bar-striped active"} role="progressbar"
                          aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style={{width : this.props.percent + '%'}}>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}
const mapStateToProps = (state) => {
    return {
        dataSource: state.commonReducer.dataSource,
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        postData: (url, params, headers) => { dispatch(Action.postData(url, params, headers)) }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(ImportCsv)
