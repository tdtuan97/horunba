import React, { Component } from 'react';
import { connect } from 'react-redux';

import * as Action from '../common/actions';
import * as queryString from 'query-string';
import moment from 'moment';

import Loading from '../common/components/common/Loading';
import FORM from '../common/components/form/FORM';
import INPUT from '../common/components/form/INPUT';
import TEXTAREA from '../common/components/form/TEXTAREA';
import SELECT from '../common/components/form/SELECT';
import BUTTON from '../common/components/form/BUTTON';
import CHECKBOX_CUSTOM from '../common/components/form/CHECKBOX_CUSTOM';
import CHECKBOX from '../common/components/form/CHECKBOX'
import LABEL from '../common/components/form/LABEL';
import DATEPICKER from '../common/components/form/DATEPICKER';
import MessagePopup from '../common/components/common/MessagePopup';
import MESSAGE_MODAL from '../common/containers/MessageModal';
import TABLE from '../common/components/table/TABLE';
import THEAD from '../common/components/table/THEAD';
import TBODY from '../common/components/table/TBODY';
import TR from '../common/components/table/TR';
import TH from '../common/components/table/TH';
import TD from '../common/components/table/TD';
import SavePopupInformDelete from './modal-delete/SavePopupInformDelete';
import FormAdress from './form-address/FormAdress';
import axios from 'axios';
import PopupProduct from './containers/PopupProduct'

var timer;
class ReturnSaveProduct extends Component {
    constructor(props) {
        super(props);
        this.state = {status:'check', firstTime: true, editFlag:false}
        this.firstRender = true;
        let params = queryString.parse(props.location.search);
        let defaultParams = {};
        if (params['return_no']) {
            this.state.return_no    = params['return_no'];
            this.state.status       = 'check';
            this.state.first        = true;
            this.state.index        = params['return_no'];
            this.hanleRealtime();
        }
        if (params['product_code'] && params['order_num'] && params['order_code']) {
            this.state.product_code = params['product_code'];
            this.state.order_num = params['order_num'];
            this.state.order_code = params['order_code'];
        }
        this.defaultParams = this.state;
        this.props.reloadData(saveUrl, this.state);
    }
    handleCancel = (event) => {
        window.location.href = baseUrl;
    };
    hanleRealtime() {
         if (!_.isEmpty(this.state.index)) {
             let params = {
                 accessFlg : this.state.accessFlg,
                 query : {
                         page_key : 'RETURN-DETAIL-SAVE',
                         primary_key : 'return_no:' + this.state.index
                     },
                 key :this.state.index
             };
             if (this.state.accessFlg !== false) {
                 this.props.postRealTime(checkEditUrl, params, {'X-Requested-With': 'XMLHttpRequest'});
             }
         }
    }
    handleSubmit = (event) => {
        let headers = {'X-Requested-With': 'XMLHttpRequest'};
        this.props.postData(saveUrl, this.state, headers);
    };
    handleChange = (event) => {
        let target = event.target;
        let name = target.name;
        let value = target.type === 'checkbox' ? (target.checked ? 1 : 0) : (target.value === '_none' ? '' : target.value);
        let dataSet = {};
        if (target.type === 'checkbox') {
            if (name.includes('payment_on_delivery')) {
                if (target.checked) {
                    let note = '【着払い出荷】'+ (typeof this.state.note !== 'undefined' ? "\n" + _.trim(this.state.note): '');
                    dataSet.note = note;
                    $('textarea[name=note]').val(note);

                } else {
                    let note = _.trim(_.trim(this.state.note).replace(/【着払い出荷】/g,''));
                    dataSet.note = _.trim(note);
                    $('textarea[name=note]').val(note);
                }
                dataSet[name] = value;
            }
        } else {
            dataSet[name] = value;
        }
        this.setState({
            ...dataSet
        });
    }

    handleChangeTypeMid = (event) => {
        let target = event.target;
        let name = target.name;
        let value = target.value === '_none' ? '' : target.value;
        let dataSet = {[name] : value};
        let headers = {'X-Requested-With': 'XMLHttpRequest'};
        if (value === '2') {
            if (typeof this.state.note === 'undefined') {
                this.setState({note: '【仕入先返品】直貼り禁止'});
            } else {
               this.setState({note: _.trim(this.state.note) + '\n【仕入先返品】直貼り禁止'});
            }
        } else {
            if (typeof this.state.note === 'undefined') {
                this.setState({note: ''});
            } else {
                this.setState({note: _.trim(_.trim(this.state.note).replace(/【仕入先返品】直貼り禁止/g,''))});
            }
        }
        if (value === '2' && this.state.dataProduct[0].product_code !== '') {
            axios.post(getAddressUrl, {dataProduct: this.state.dataProduct}, {headers : headers}
            ).then(response => {
                dataSet['dataProduct'] = response.data.dataProduct;
                this.setState(dataSet);
            }).catch(error => {
                $(".loading").hide();
                throw(error);
            });
        } else if ((String(this.state.return_type_mid_id) === '2' && value !== '2') || typeof this.state.return_type_mid_id === 'undefined') {
            dataSet['dataProduct'] = [];
            Object.keys(this.state.dataProduct).map((index) => {
                let temp = this.state.dataProduct[index];
                temp['return_id']      = 0;
                temp['return_address'] = 0;
                temp['address_name']   = '';
                dataSet['dataProduct'][index] = temp;
            })
            this.setState(dataSet);
        } else {
            this.setState({[name] : value});
        }
    }
    handleChangeDate = function(date, name, event) {
        let dateValue = moment(date);
        if (!dateValue.isValid()) {
            dateValue = undefined;
        } else {
            dateValue = dateValue.format('YYYY-MM-DD');
        }
        this.setState({
          [name]: dateValue,
        });
    }
    handleCancel = (event) => {
        window.location.href = baseUrl;
    }
    componentWillUnmount () {
        clearInterval(this.state);
    }
    handleFormAddress = (supplierCd, index) => {
        this.setState({supplier_cd:supplierCd, address_index: index, firstTime: true, messages_total: ''}, () => {
            let headers = {'X-Requested-With': 'XMLHttpRequest'};
            this.props.postData(getFormDataUrl, {supplier_cd:supplierCd}, headers);
        });
        $('#issue-modal').modal({backdrop: 'static', show: true});
    }
    handleCancelPopup = () => {
        delete this.state.address_index;
        delete this.state.supplier_cd;
        this.setState({dataReturnAddress:{}, firstTime:false}, () => {
            $('#issue-modal').modal('hide');
        });
    }
    componentWillReceiveProps(nextProps) {
        if (!_.isEmpty(nextProps.dataSource) &&
            (!this.props.dataSource ||
                this.props.dataSource.time !== nextProps.dataSource.time )) {
            if (nextProps.dataSource.status === 'done') {
                window.location.href = baseUrl;
            } else {
                let dataSet = {dataProduct: nextProps.dataSource.dataProduct, default: nextProps.dataSource.default, status:nextProps.dataSource.status};
                if (nextProps.dataSource.status === 'edit') {
                    dataSet = {...dataSet, ...nextProps.dataSource.data};
                }
                this.setState(dataSet);
            }
        }
        if (!_.isEmpty(nextProps.dataResponse)) {
            if (typeof nextProps.dataResponse.status !== 'undefined' && nextProps.dataResponse.status === 'done') {
               window.location.href = baseUrl;
            }
            if (!_.isEmpty(nextProps.dataResponse.dataReturnAddress)
            && this.props.dataResponse.time !== nextProps.dataResponse.time) {
                this.setState({dataReturnAddress:nextProps.dataResponse.dataReturnAddress});
            }
            if (!_.isEmpty(nextProps.dataResponse.messageError)) {
                let currentDetail = this.state.dataProduct;
                currentDetail.map((item) => {
                    delete item.quantityMessage;
                    delete item.returnTankaMessage;
                });
                nextProps.dataResponse.messageError.map((item) => {
                    currentDetail[item.index]['quantityMessage'] = item.quantityMessage;
                    currentDetail[item.index]['returnTankaMessage'] = item.returnTankaMessage;
                    currentDetail[item.index]['address']         = item.address;
                })
                this.setState({dataProduct: currentDetail});
            }
         }
        // Check realtime update
        if (!_.isEmpty(nextProps.dataRealTime)) {
            this.setState(nextProps.dataRealTime);
        }
    }
    handleEdit = () => {
        $('.show-edit').removeClass('collapse');
    }
    componentDidUpdate() {
        if (!_.isEmpty(this.props.dataSource)) {
            $('.loading').hide();
        }
        if (this.state.accessFlg === false) {
            $('#message-modal').modal('show');
            //We will auto redirect after 5 seconds
            setTimeout(() => {
                window.location.href = baseUrl;
            }, 5000);
        }
    }
    componentDidMount() {
        setInterval(() => {
            this.hanleRealtime()
        }, 5000);
    }
    handleAddNewLine = () => {
        let dataSet = !_.isEmpty(this.state.dataReturnAddress) ? [...this.state.dataReturnAddress] : [];
        let newElement = {
            supplier_cd: this.state.supplier_cd,
            return_tel: '',
            return_pref: '',
            return_postal: '',
            return_nm: '',
            new_item: 1,
            return_address: '',
            is_selected: 0
        }
        dataSet.push(newElement);
        this.setState({
            dataReturnAddress: dataSet
        });
    }
    handleAddressSubmit = (e) => {
        let headers = {'X-Requested-With': 'XMLHttpRequest'};
        document.getElementsByName(e.target.name)[0].disabled = true;
        $(".loading").show();
        let params = {};
        params['dataReturnAddress'] = this.state.dataReturnAddress;
        params['supplier_cd'] = this.state.supplier_cd;
        axios.post(saveAddressUrl,params, {headers : headers, overrideMimeType : "text/html;charset=Shift_JIS" }
        ).then(response => {
            $(".loading").hide();
            let data = response.data;
            if (data.status === 1) {
                if (!_.isEmpty(data.data)) {
                    let currentDetail = this.state.dataProduct;
                    currentDetail[this.state.address_index] = {...currentDetail[this.state.address_index], ...data.data};
                    this.setState({dataProduct: currentDetail});
                }
                this.handleCancelPopup();
            } else if (data.messages) {
                if (typeof data.messages === 'object') {
                    this.setState({
                       messages :  data.messages
                    });
                } else {
                    this.setState({
                       messages_total :  data.messages
                    });
                }

            }

        }).catch(error => {
            $(".loading").hide();
            throw(error);
        });
        document.getElementsByName(e.target.name)[0].disabled = false;
    }
    handleChangeAddressDetail = (index, event) => {
        const target = event.target;
        const name = target.name;
        const value = target.type === 'checkbox' ? (target.checked?1:0) : target.value;
        let currentDetail = [...this.state.dataReturnAddress];
        if (target.type === 'checkbox') {
            for(let key in currentDetail) {
                currentDetail[key].is_selected = 0
            };
        }
        currentDetail[index][name] = value;
        this.setState({
            dataReturnAddress: currentDetail,
            firstTime: false
        });
    }
    handleChangeProductDetail = (index, event) => {
        const target = event.target;
        const name = target.name;
        const value = target.value;
        let currentDetail = this.state.dataProduct;
        currentDetail[index][name] = value;
        this.setState({
            dataProduct: currentDetail
        }, () => {
            if (name === 'product_code' || name === 'edi_order_code') {
                clearTimeout(timer);
                let _this = this;
                if (!_.isEmpty(currentDetail[index]['product_code'])) {
                    timer = setTimeout(function() {
                        _this.getInfoProduct(index, currentDetail[index]['product_code']);
                    }, 1500);
                }
            }
        });
    }
    handleAccept = (event) => {
        const target = event.target;
        let value = target.value;
        let name = target.name;
        let searchIDs = $("input[name^='return_index_checkbox']:checkbox:checked").map(function(){
            return $(this).val();
        }).get();

        if(searchIDs.length > 0) {
            $('#access-memo-delete-modal').modal('show');
        }
    }
    handleItemUpdate = (event, item, action) => {
        const target = event.target;
        let headers = {'X-Requested-With': 'XMLHttpRequest'};
        if (action === 'update') {
            this.setState({editFlag: true, itemUpdate : {...item}}, () => {
                $(target).closest('tr').find("input[name='quantity']").focus();
            });

        } else if(action === 'save') {
            this.setState({editFlag: false, itemUpdate : {...item}}, () => {
                $(".loading").show();
                let params  = {...item, updateQuantity : true};
                axios.post(updateQuantityUrl, params, {headers : headers}
                ).then(response => {
                    $(".loading").hide();
                    if (response.data.status === 0) {
                        $.notify({
                            icon: 'glyphicon glyphicon-warning-sign',
                            message: response.data.messageErrorQuantity,
                        },{
                            type: 'danger',
                            delay: 2000
                        });
                    }
                    this.props.reloadData(saveUrl, this.defaultParams);
                }).catch(error => {
                    $(".loading").hide();
                    this.props.reloadData(saveUrl, this.defaultParams);
                    throw(error);
                });
            });
        } else {
           this.setState({editFlag: false, itemUpdate : null})
        }
    }
    handleCancelAccept = () => {
        $('#access-memo-delete-modal').modal('hide');
        $("input[name^='return_index_checkbox']:checkbox:checked").prop('checked', false);
    }
    handleProcessAll = () => {
        let searchIDs = $("input[name^='return_index_checkbox']:checkbox:checked").map(function(){
            return $(this).val();
          }).get();
        $('#access-memo-delete-modal').modal('hide');
        let headers = {'X-Requested-With': 'XMLHttpRequest'};
            axios.post(deleteAllUrl, {return_index_checkbox : searchIDs}, {headers : headers}
        ).then(response => {
            $(".loading").hide();
            $("input[name^='return_index_checkbox']:checkbox:checked").prop('checked', false);
            this.props.reloadData(saveUrl, this.defaultParams);
        }).catch(error => {
            $(".loading").hide();
            this.props.reloadData(saveUrl, this.defaultParams);
            throw(error);
        });
    }
    getInfoProduct = (index, productCode) => {
        $(".loading").show();
        let formData   = new FormData();
        let headers = {'X-Requested-With': 'XMLHttpRequest'};
        formData.append('product_code', productCode);
        if (typeof this.state.dataProduct[index]['edi_order_code'] !== 'undefined') {
            formData.append('edi_order_code', this.state.dataProduct[index]['edi_order_code']);
        }
        axios.post(getDataProductUrl, formData, {headers : headers}
        ).then(response => {
            $(".loading").hide();
            let currentDetail = this.state.dataProduct;
            if (typeof response.data.productMessage !== 'undefined') {
                currentDetail[index]['productMessage'] = response.data.productMessage;
                if (typeof response.data.show_popup !== 'undefined') {
                    $('#popup-product-modal').modal('show');
                }
            } else {
                delete currentDetail[index]['productMessage'];
                currentDetail[index] = {...currentDetail[index], ...response.data.productInfo};
                let defaultVal = this.state.default;
                currentDetail.push({...this.state.default});
            }
            this.setState({dataProduct: currentDetail});
        }).catch(error => {
            $(".loading").hide();
            throw(error);
        });
    };

    handleCancelPopupProduct = () => {
        $('#popup-product-modal').modal('hide');
    };

    render() {
        let dataSource   = this.props.dataSource;
        let dataResponse = this.props.dataResponse;
        let returnTypeOptions = [];
        let LargeOptions = [];
        let MidOptions = [];
        if (!_.isEmpty(dataSource)) {
            returnTypeOptions = dataSource.dataReturnType;
            LargeOptions = dataSource.dataLarge;
            MidOptions = dataSource.dataMid;
        }
        let isAdd = false;
        if (this.state.status === 'add') {
            isAdd = true;
        }
        var nf = new Intl.NumberFormat();
        return (!_.isEmpty(dataSource) &&
            <div className="box box-primary">
            <div className="box-body">
            <div className="container-fluid">
            <FORM
                className="form-horizontal" id="return_detail">
                <Loading className="loading" />
                <MessagePopup classType="alert-error" status={(this.state.product_before === this.state.product_code && this.state.product_code) ? 'show' : ''} message={trans('messages.product_not_exists')} />
                    <div className="row">
                        <div className="col-md-6 margin-form">
                            <INPUT
                                name="return_no"
                                labelTitle={trans('messages.return_no')}
                                groupClass="row"
                                className="form-control input-sm"
                                fieldGroupClass="col-md-9"
                                labelClass="col-md-3"
                                readOnly={true}
                                defaultValue={this.state.return_no || ''}/>
                        </div>
                    </div>
                    <div className="row">
                        <div className="col-md-6 margin-form">
                            <INPUT
                                name="deje_no"
                                labelTitle={trans('messages.deje_num')}
                                groupClass="row"
                                className="form-control input-sm"
                                fieldGroupClass="col-md-9"
                                labelClass="col-md-3"
                                onChange={this.handleChange}
                                defaultValue={this.state.deje_no || ''}
                                errorPopup={true}
                                hasError={(dataResponse.message && dataResponse.message.deje_no)?true:false}
                                errorMessage={(dataResponse.message&&dataResponse.message.deje_no)?dataResponse.message.deje_no:""}
                                />
                        </div>
                        <div className="col-md-6 margin-form">
                            <DATEPICKER
                                groupClass="row"
                                placeholderText="YYYY-MM-DD"
                                dateFormat="YYYY-MM-DD"
                                locale="ja"
                                name="return_date"
                                minDate={moment()}
                                labelTitle={trans('messages.return_date')}
                                className="form-control input-sm"
                                fieldGroupClass="col-md-9"
                                labelClass="col-md-3"
                                selected={this.state.return_date ? moment(this.state.return_date) : null}
                                onChange={(date,name, e) => this.handleChangeDate(date, 'return_date', e)}
                                errorPopup={true}
                                hasError={(dataResponse.message && dataResponse.message.return_date)?true:false}
                                errorMessage={(dataResponse.message&&dataResponse.message.return_date)?dataResponse.message.return_date:""}
                                />
                        </div>
                    </div>
                    <div className="row">
                        <div className="col-md-6 margin-form">
                            <SELECT
                                name="return_type_large_id"
                                labelTitle={trans('messages.return_type_large')}
                                className="form-control input-sm"
                                groupClass="row"
                                fieldGroupClass="col-md-9"
                                labelClass="col-md-3"
                                options={LargeOptions}
                                onChange={this.handleChange}
                                defaultValue={this.state.return_type_large_id || '_none'}
                                errorPopup={true}
                                hasError={(dataResponse.message && dataResponse.message.return_type_large_id)?true:false}
                                errorMessage={(dataResponse.message&&dataResponse.message.return_type_large_id)?dataResponse.message.return_type_large_id:""}
                                />
                        </div>
                        <div className="col-md-6 margin-form">
                            <SELECT
                                name="return_type_mid_id"
                                labelTitle={trans('messages.return_type_mid')}
                                className="form-control input-sm"
                                groupClass="row"
                                fieldGroupClass="col-md-9"
                                labelClass="col-md-3"
                                options={MidOptions}
                                onChange={this.handleChangeTypeMid}
                                defaultValue={this.state.return_type_mid_id  || '_none'}
                                errorPopup={true}
                                hasError={(dataResponse.message && dataResponse.message.return_type_mid_id)?true:false}
                                errorMessage={(dataResponse.message&&dataResponse.message.return_type_mid_id)?dataResponse.message.return_type_mid_id:""}
                                />
                        </div>
                    </div>
                    <div className="row">
                        <div className="col-md-12 margin-form">
                        <div className="table-responsive">
                            <TABLE className="table table-custom table-rm-sdt" style={{ borderCollapse: "collapse" }}>
                                <THEAD>
                                    <TR>
                                        <TH className="text-center " colSpan="2">{trans('messages.product_code')}</TH>
                                        <TH className="text-center ">{trans('messages.product_name')}</TH>
                                        <TH className="text-center ">{trans('messages.quantity')}</TH>
                                        <TH className="text-center ">{trans('messages.return_price_invoice')}</TH>
                                        <TH className="text-center ">{trans('messages.return_price')}</TH>
                                        <TH className="text-center ">{trans('messages.txt_edi_order_code')}</TH>
                                        <TH className="text-center">{trans('messages.btn_detail_editing')}</TH>
                                        <TH className="text-center">
                                            <button type="button" name="delete_all" className="btn btn-box-tool btn-remove"
                                            data-target="#access-memo-delete-modal" data-backdrop="static" onClick={(e) => this.handleAccept(e)}
                                            >
                                                <i className="fa fa-trash-o"></i>
                                            </button>
                                        </TH>
                                        <TH className="text-left">{trans('messages.addres_return')}</TH>
                                    </TR>
                                </THEAD>
                                <TBODY>
                                    {
                                        (!_.isEmpty(this.state.dataProduct)) &&
                                        this.state.dataProduct.map((item, index) => {
                                            let classErr = item.error_code?' line-error':'';
                                            let i =  this.state.dataProduct.length;
                                            let j =  index + 1;

                                            return ([
                                                <TR key={"product_detail"  + index} className={classErr}>
                                                    <TD className="col-max-min-120" colSpan="2">
                                                        {(item.is_add === 1 )?
                                                            <INPUT
                                                                key={"product_code" + index}
                                                                name="product_code"
                                                                className="form-control input-sm"
                                                                groupClass=" "
                                                                value={item.product_code || ''}
                                                                onChange={(e) => {this.handleChangeProductDetail(index, e)}}
                                                                errorPopup={true}
                                                                hasError={item.productMessage ? true:false}
                                                                errorMessage={item.productMessage?item.productMessage:""}/>
                                                            :
                                                            item.product_code
                                                        }
                                                    </TD>
                                                    <TD className="col-max-min-120">{item.product_name}</TD>
                                                    <TD className="col-max-min-120">
                                                    {(item.is_add === 1 ) || (this.state.editFlag === true && this.state.itemUpdate.return_line_no === item.return_line_no)?
                                                        <INPUT
                                                            key={"quantity" + index}
                                                            name="quantity"
                                                            className="form-control input-sm"
                                                            groupClass=" "
                                                            value={item.quantity || ''}
                                                            type="number"
                                                            min="0"
                                                            onChange={(e) => {this.handleChangeProductDetail(index, e)}}
                                                            errorPopup={true}
                                                            hasError={item.quantityMessage ? true:false}
                                                            errorMessage={item.quantityMessage?item.quantityMessage:""}
                                                            />
                                                        :
                                                        item.return_quantity
                                                    }
                                                    </TD>
                                                    <TD className={"col-md-1 text-right"}>{item.return_price}</TD>
                                                    <TD className="col-md-1 text-right">
                                                        <INPUT
                                                            key={"return_tanka" + index}
                                                            name="return_tanka"
                                                            className="form-control input-sm"
                                                            groupClass=" "
                                                            value={item.return_tanka || ''}
                                                            type="number"
                                                            min="1"
                                                            onChange={(e) => {this.handleChangeProductDetail(index, e)}}
                                                            errorPopup={true}
                                                            hasError={item.returnTankaMessage ? true:false}
                                                            errorMessage={item.returnTankaMessage?item.returnTankaMessage:""}
                                                            />
                                                    </TD>
                                                    <TD className="col-md-1 text-right">
                                                        {(item.is_add === 1 )?
                                                            <INPUT
                                                                key={"edi_order_code" + index}
                                                                name="edi_order_code"
                                                                className="form-control input-sm"
                                                                groupClass=" "
                                                                value={item.edi_order_code || ''}
                                                                readOnly={true}
                                                                onChange={(e) => {this.handleChangeProductDetail(index, e)}}/>
                                                            :
                                                            item.edi_order_code
                                                        }
                                                    </TD>
                                                    <TD className="col-md-1 text-center">
                                                    {(item.is_add !== 1 && i !== j) ? (this.state.editFlag === true && this.state.itemUpdate.return_line_no === item.return_line_no)?
                                                        [<button key="btn-save" type="button" name="edit" className="btn btn-box-tool btn-save"
                                                        data-target="#access-memo-delete-modal" data-backdrop="static"
                                                        onClick={(e) => {this.handleItemUpdate(e, item, 'save')}}>
                                                            <i className="fa fa-save"></i>
                                                        </button>,
                                                        <button key="btn-cancel" type="button" name="edit" className="btn btn-box-tool btn-remove"
                                                        data-target="#access-memo-delete-modal" data-backdrop="static"
                                                        onClick={(e) => {this.handleItemUpdate(e, item, 'cancel')}}>
                                                            <i className="fa  fa-remove "></i>
                                                        </button>      ]
                                                        :
                                                        <button type="button" name="edit" className="btn btn-box-tool btn-edit"
                                                        data-target="#access-memo-delete-modal" data-backdrop="static"
                                                        onClick={(e) => {this.handleItemUpdate(e, item, 'update')}}>
                                                            <i className="fa fa-edit"></i>
                                                        </button>
                                                        : ''
                                                    }
                                                    </TD>
                                                    <TD className="col-md-1 text-center">
                                                    { (i !== j) ?
                                                        <label className="control nice-checkbox">
                                                            <CHECKBOX
                                                                name={'return_index_checkbox['+index+']'}
                                                                key={'return_index_checkbox' + index}
                                                                value={item.return_no + '_' + item.return_line_no  + '_' + item.return_time}
                                                            />
                                                            <div className="nice-icon-check"></div>
                                                        </label>
                                                        : ''
                                                    }
                                                    </TD>
                                                    <TD className={"col-md-2 " + (parseInt(this.state.return_type_mid_id) === 2 ? '' : 'hide-opacity')}>
                                                            <a className={"link-detail " + ((item.return_id && String(item.return_id) !== '0') ? '' : 'text-red')} href="javascript:void(0)" onClick={() => this.handleFormAddress(item.supplier_id, index)}>{item.address_name}</a>
                                                    </TD>
                                                </TR>,
                                                (!_.isEmpty(item.child) && item.child.map((child, subIndex) => {
                                                    return (<TR className="yellow" key={"product_child"  + subIndex}>
                                                        <TD className="col-md-1" colSpan="1"></TD>
                                                        <TD className="col-md-1" colSpan="1">{child.product_code}</TD>
                                                        <TD className="col-md-3">{child.product_name}</TD>
                                                        <TD className="col-md-1-5">{item.is_add === 1 ? (item.quantity * child.component_num) : child.return_quantity}</TD>
                                                        <TD className={"col-md-1 text-right"}>{child.price_invoice}</TD>
                                                        <TD className="col-md-1 text-right">{child.price_invoice}</TD>
                                                    </TR>)
                                                }))
                                            ])
                                        })
                                    }
                                </TBODY>
                            </TABLE>
                        </div>
                        </div>
                    </div>
                    <div className="row">
                        <div className="col-md-6 margin-form">
                            <DATEPICKER
                                placeholderText="YYYY-MM-DD"
                                dateFormat="YYYY-MM-DD"
                                locale="ja"
                                name="receive_plan_date"
                                labelTitle={trans('messages.receive_plan_date')}
                                groupClass="row"
                                minDate={moment()}
                                className="form-control input-sm"
                                fieldGroupClass="col-md-9"
                                labelClass="col-md-3"
                                selected={this.state.receive_plan_date ? moment(this.state.receive_plan_date) : null}
                                onChange={(date,name, e) => this.handleChangeDate(date, 'receive_plan_date', e)} />
                        </div>
                        <div className="col-md-6 margin-form">
                            <DATEPICKER
                                placeholderText="YYYY-MM-DD"
                                dateFormat="YYYY-MM-DD"
                                locale="ja"
                                name="delivery_plan_date"
                                labelTitle={trans('messages.delivery_plan_date')}
                                groupClass="row"
                                minDate={moment()}
                                className="form-control input-sm"
                                fieldGroupClass="col-md-9"
                                labelClass="col-md-3"
                                selected={this.state.delivery_plan_date ? moment(this.state.delivery_plan_date) : null}
                                onChange={(date,name, e) => this.handleChangeDate(date, 'delivery_plan_date', e)} />
                        </div>
                    </div>
                    <div className="row">
                        <div className="col-md-6 margin-form">
                            <div className="row">
                                <LABEL
                                    groupClass="col-md-3 col-xs-6"
                                    labelClass=" "
                                    labelTitle={trans('messages.payment_on_delivery')}>
                                </LABEL>
                                <div className="col-md-2 col-xs-6">
                                    <CHECKBOX_CUSTOM
                                        name="payment_on_delivery"
                                        defaultChecked={this.state.payment_on_delivery === 1 ? true : false}
                                        onChange={this.handleChange}
                                     />
                                </div>
                            </div>
                        </div>
                        <div className="col-md-6 margin-form">
                            <TEXTAREA
                                name="note"
                                labelTitle={trans('messages.note')}
                                className="form-control input-sm"
                                groupClass="row"
                                fieldGroupClass="col-md-9"
                                rows="4"
                                labelClass="col-md-3"
                                onChange={this.handleChange}
                                value={this.state.note || ''}
                                />
                        </div>
                    </div>
                    <div className="row">
                        <div className="col-md-12 margin-form">
                            <BUTTON className="btn bg-purple input-sm pull-right btn-larger" type="button" onClick={this.handleCancel}>{trans('messages.btn_cancel')}</BUTTON>
                           { (this.state.editFlag === false) ?
                            <BUTTON name="btnAccept" id="btnSubmit" className="btn btn-success input-sm btn-larger pull-right margin-element" type="button" onClick={this.handleSubmit}>{trans('messages.btn_accept')}</BUTTON>
                            : ''
                           }

                        </div>
                    </div>
            </FORM>
            </div>
                <FormAdress
                    firstTime={this.state.firstTime}
                    returnAddressId={dataSource.data.return_address_id}
                    dataReturnAddress={this.state.dataReturnAddress}
                    messagesTotal={this.state.messages_total}
                    messages={this.state.messages}
                    handleAddNewLine={this.handleAddNewLine}
                    supplierCd={this.state.supplier_cd}
                    handleAddressSubmit={this.handleAddressSubmit}
                    handleChangeAddressDetail={this.handleChangeAddressDetail}
                    handleImportCancel={this.handleCancelPopup}/>
            </div>
            <MESSAGE_MODAL
                message={this.state.message || ''}
                title={trans('messages.deny_edit')}
                baseUrl={baseUrl}
            />
            <SavePopupInformDelete
                handleProcessAll={this.handleProcessAll}
                handleCancelAccept={this.handleCancelAccept}
            />
            <PopupProduct
                handleCancel={this.handleCancelPopupProduct}
            />
            </div>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        dataSource: state.commonReducer.dataSource,
        dataResponse: state.commonReducer.dataResponse,
        dataRealTime: state.commonReducer.dataRealTime
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        reloadData: (url, params) => { dispatch(Action.detailData(url, params)) },
        postData: (url, params, headers) => { dispatch(Action.postData(url, params, headers)) },
        postRealTime: (url, params, headers) => { dispatch(Action.postRealTime(url, params, headers)) }
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(ReturnSaveProduct)