import React, { Component } from 'react';
import { connect } from 'react-redux';

import * as Action from '../common/actions';
import * as queryString from 'query-string';
import moment from 'moment';

import Loading from '../common/components/common/Loading';
import FORM from '../common/components/form/FORM';
import INPUT from '../common/components/form/INPUT';
import TEXTAREA from '../common/components/form/TEXTAREA';
import SELECT from '../common/components/form/SELECT';
import BUTTON from '../common/components/form/BUTTON';
import CHECKBOX_CUSTOM from '../common/components/form/CHECKBOX_CUSTOM';
import LABEL from '../common/components/form/LABEL';
import DATEPICKER from '../common/components/form/DATEPICKER';
import MessagePopup from '../common/components/common/MessagePopup';
import MESSAGE_MODAL from '../common/containers/MessageModal';
import axios from 'axios';
var timer;
const arrKey = [
    'return_no',
    'return_date',
    'return_time',
    'return_type_large_id',
    'return_type_mid_id',
    'deje_no',
    'product_code',
    'parent_product_code',
    'return_quantity',
    'return_tanka',
    'receive_plan_date',
    'payment_on_delivery',
    'note',
    'delivery_plan_date',
    'supplier_id',
    'receive_id',
];
class ReturnSave extends Component {
    constructor(props) {
        super(props);
        this.state = {status:'add'}
        this.firstRender = true;
        let params = queryString.parse(props.location.search);
        if (params['return_no']) {
            this.state.return_no    = params['return_no'];
            this.state.product_code = params['product_code'];
            this.state.return_time  = params['return_time'];
            this.state.parent_product_code = params['product_code'];
            this.state.status       = 'edit';
            this.state.index        = params['return_no'];
            this.state.accessFlg    = true;
            this.state.first        = true;
            this.hanleRealtime();
        }
        if (params['receive_id']) {
            this.state.first        = true;
            this.state.receive_id   = params['receive_id'];
            this.state.product_code = params['product_code'];
            this.state.parent_product_code = params['product_code'];
        }
        this.props.reloadData(saveUrl, this.state);
    }
    handleCancel = (event) => {
        window.location.href = baseUrl;
    }
    hanleRealtime() {
        if (!_.isEmpty(this.state.index)) {
            let params = {
                accessFlg : this.state.accessFlg,
                query : {
                        page_key : 'RETURN-SAVE',
                        primary_key : 'index:' + this.state.index
                    },
                key :this.state.index
            };
            if (this.state.accessFlg !== false) {
                this.props.postRealTime(checkEditUrl, params, {'X-Requested-With': 'XMLHttpRequest'});
            }
        }
    }
    handleSubmit = (event) => {
        $("#btnSubmit").prop('disabled', true);
        let headers = {'X-Requested-With': 'XMLHttpRequest'};
        let formData   = new FormData();
        if (!_.isEmpty(this.state)) {
            Object.keys(this.state).map((item) => {
                if (arrKey.indexOf(item) !== -1) {
                    formData.append(item, this.state[item]);
                }
            });
        this.props.postData(saveUrl, formData, headers);
        }
    }
    handleChange = (event) => {
        const target = event.target;
        const name = target.name;
        const value = target.type === 'checkbox' ? (target.checked?1:0) : (target.value === '_none' ? '' : target.value);
        let check = {};
        if (name === 'return_type_mid_id') {
            if (value !== '' && typeof this.props.dataSource.dataMid[value].value !== 'undefined'
                && this.props.dataSource.dataMid[value].value === '仕入先返品') {
                check.checkDisplay = 'show';
            } else {
                check.checkDisplay = 'hide';
            }
        }
        if (name === 'parent_product_code') {
            clearTimeout(timer);
            let _this = this;
            timer = setTimeout(function() {
                _this.getInfoProduct(value);
            }, 1000);
        }
        if (target.type === 'checkbox') {
            if (target.checked) {
                check.note = '【着払い出荷】';
                $('textarea[name=note]').val('【着払い出荷】');

            } else {
                check.note = '';
                $('textarea[name=note]').val('');
            }
        }
        this.setState({
            ...check,
            [name]: value
        });
    }
    getInfoProduct = (productCode, returnTanka) => {
        $(".loading").show();
        $("#btnSubmit").prop('disabled', true);
        let formData   = new FormData();
        let headers = {'X-Requested-With': 'XMLHttpRequest'};
        formData.append('product_code', productCode);
        axios.post(getDataProductUrl, formData, {headers : headers}
        ).then(response => {
            $(".loading").hide();
            let dataSet = {};
            let dataDefault = {
                price_invoice : '',
                product_name:'',
                supplier_return_postal: '',
                supplier_return_pref: '',
                supplier_return_address: '',
                supplier_return_nm: '',
                supplier_return_tel: '',
            };
            dataSet = {...dataDefault,...response.data.dataProduct,...response.data.dataSupplier};
            if (!response.data.dataProduct) {
                dataSet['product_before'] = productCode;
            } else {
                dataSet['return_tanka'] = returnTanka ? returnTanka : response.data.dataProduct.price_invoice;
            }
            this.setState({...dataSet});
            $("#btnSubmit").prop('disabled', false);
        }).catch(error => {
            $(".loading").hide();
            throw(error);
        });
    }
    handleChangeDate = function(date, name, event) {
        let dateValue = moment(date);
        if (!dateValue.isValid()) {
            dateValue = undefined;
        } else {
            dateValue = dateValue.format('YYYY-MM-DD');
        }
        this.setState({
          [name]: dateValue,
        });
    }
    componentWillUnmount () {
        clearInterval(this.state);
    }
    componentDidMount() {
        setInterval(() => {
            this.hanleRealtime()
        }, 5000);
    }
    componentWillReceiveProps(nextProps) {
        if (!_.isEmpty(nextProps.dataSource) && this.state.first) {
            if (nextProps.dataSource.status === 999) {
                if (!_.isEmpty(nextProps.dataSource.return_no)) {
                    window.location.href = saveUrl + '?return_no=' + nextProps.dataSource.return_no + '&product_code=' + nextProps.dataSource.product_code;
                } else {
                    window.location.href = baseUrl;
                }
            }
            if (!_.isEmpty(nextProps.dataSource.data)) {
                let data = nextProps.dataSource.data;
                let dateSet = {...data};
                if (!_.isEmpty(this.state)) {
                    Object.keys(this.state).map((item) => {
                        if (arrKey.indexOf(item) !== -1) {
                            dateSet[item] = this.state[item];
                        }
                    });
                }
                if (dateSet.return_type_mid_id !== '' && typeof dateSet.return_type_mid_id !== 'undefined'
                    && nextProps.dataSource.dataMid[dateSet.return_type_mid_id].value === '仕入先返品') {
                    dateSet.checkDisplay = 'show';
                } else {
                    dateSet.checkDisplay = 'hide';
                }
                if (typeof dateSet.product_code !== 'undefined') {
                    this.getInfoProduct(dateSet.product_code, dateSet.return_tanka);
                }
                this.setState(dateSet);
            } else {
                if (typeof this.state.product_code !== 'undefined') {
                    this.getInfoProduct(this.state.product_code);
                }
            }
            this.state.first = undefined;
        }
        if (!_.isEmpty(nextProps.dataResponse)) {
            if (nextProps.dataResponse.status === 1) {
                window.location.href = baseUrl;
            }
            if (nextProps.dataResponse.status === 0) {
                $("#btnSubmit").prop('disabled', false);
            }
        }
         // Check realtime update
        if (!_.isEmpty(nextProps.dataRealTime)) {
            this.setState(nextProps.dataRealTime);
        }
    }
    componentDidUpdate() {
        if (!_.isEmpty(this.props.dataSource)) {
            $('.loading').hide();
        }
        if (this.state.accessFlg === false) {
            $('#message-modal').modal('show');
            //We will auto redirect after 5 seconds
            setTimeout(() => {
                window.location.href = baseUrl;
            }, 5000);
        }
    }
    render() {
        let dataSource   = this.props.dataSource;
        let dataResponse = this.props.dataResponse;
        let LargeOptions = [];
        let MidOptions = [];
        let ProductOptions = [];
        if (!_.isEmpty(dataSource)) {
            LargeOptions = dataSource.dataLarge;
            MidOptions = dataSource.dataMid;
            ProductOptions = dataSource.dataProduct;
        }
        return ((!_.isEmpty(dataSource) && dataSource.status !== 999 ) &&
            <div>
            <FORM
                className="form-horizontal padding-form">
                <Loading className="loading" />
                <MessagePopup classType="alert-error" status={(this.state.product_before === this.state.product_code && this.state.product_code) ? 'show' : ''} message={trans('messages.product_not_exists')} />
                <div className="row">
                    <div className="col-md-6">
                        <INPUT
                            name="return_no"
                            labelTitle={trans('messages.return_no')}
                            className="form-control input-sm"
                            fieldGroupClass="col-md-9"
                            labelClass="col-md-3"
                            readOnly={true}
                            defaultValue={this.state.return_no || ''}/>
                        <DATEPICKER
                            placeholderText="YYYY-MM-DD"
                            dateFormat="YYYY-MM-DD"
                            locale="ja"
                            minDate={moment()}
                            name="return_date"
                            labelTitle={trans('messages.return_date')}
                            className="form-control input-sm"
                            fieldGroupClass="col-md-9"
                            labelClass="col-md-3"
                            selected={this.state.return_date ? moment(this.state.return_date) : null}
                            onChange={(date,name, e) => this.handleChangeDate(date, 'return_date', e)} />
                        <SELECT
                            name="return_type_large_id"
                            labelTitle={trans('messages.return_type_large')}
                            className="form-control input-sm"
                            fieldGroupClass="col-md-9"
                            labelClass="col-md-3"
                            options={LargeOptions}
                            onChange={this.handleChange}
                            defaultValue={this.state.return_type_large_id || '_none'}
                            errorPopup={true}
                            hasError={(dataResponse.message && dataResponse.message.return_type_large_id)?true:false}
                            errorMessage={(dataResponse.message&&dataResponse.message.return_type_large_id)?dataResponse.message.return_type_large_id:""}
                            />
                        <SELECT
                            name="return_type_mid_id"
                            labelTitle={trans('messages.return_type_mid')}
                            className="form-control input-sm"
                            fieldGroupClass="col-md-9"
                            labelClass="col-md-3"
                            options={MidOptions}
                            onChange={this.handleChange}
                            defaultValue={this.state.return_type_mid_id  || '_none'}
                            errorPopup={true}
                            hasError={(dataResponse.message && dataResponse.message.return_type_mid_id)?true:false}
                            errorMessage={(dataResponse.message&&dataResponse.message.return_type_mid_id)?dataResponse.message.return_type_mid_id:""}
                            />
                        <INPUT
                            name="deje_no"
                            labelTitle={trans('messages.deje_num')}
                            className="form-control input-sm"
                            fieldGroupClass="col-md-9"
                            labelClass="col-md-3"
                            onChange={this.handleChange}
                            defaultValue={this.state.deje_no || ''}/>
                        { _.isEmpty(this.state.receive_id) ?
                            <INPUT
                                name="parent_product_code"
                                labelTitle={trans('messages.product_code')}
                                className="form-control input-sm"
                                fieldGroupClass="col-md-9"
                                labelClass="col-md-3"
                                onChange={this.handleChange}
                                defaultValue={this.state.parent_product_code || ''}
                                errorPopup={true}
                                hasError={(dataResponse.message && dataResponse.message.parent_product_code)?true:false}
                                errorMessage={(dataResponse.message&&dataResponse.message.parent_product_code)?dataResponse.message.parent_product_code:""}/>
                            :
                            <SELECT
                                name="parent_product_code"
                                labelTitle={trans('messages.product_code')}
                                className="form-control input-sm"
                                fieldGroupClass="col-md-9"
                                labelClass="col-md-3"
                                options={ProductOptions}
                                onChange={this.handleChange}
                                defaultValue={this.state.product_code  || '_none'}
                                errorPopup={true}
                                hasError={(dataResponse.message && dataResponse.message.parent_product_code)?true:false}
                                errorMessage={(dataResponse.message&&dataResponse.message.parent_product_code)?dataResponse.message.parent_product_code:""}
                            />
                        }
                        <span id="message-pro-name" className="col-md-offset-3 col-md-9">修理品、DF返品、予定外入荷の場合は、商品番号を入力。</span>
                        <INPUT
                            key={this.state.product_name + "product_name"}
                            name="product_name"
                            labelTitle={trans('messages.product_name')}
                            className="form-control input-sm"
                            fieldGroupClass="col-md-9"
                            labelClass="col-md-3 input-pro-name"
                            onChange={this.handleChange}
                            defaultValue={this.state.product_name || ''}
                            readOnly={true}/>
                        <INPUT
                            name="return_quantity"
                            labelTitle={trans('messages.quantity')}
                            className="form-control input-sm"
                            fieldGroupClass="col-md-9"
                            labelClass="col-md-3"
                            onChange={this.handleChange}
                            errorPopup={true}
                            defaultValue={this.state.return_quantity || ''}
                            hasError={(dataResponse.message && dataResponse.message.return_quantity)?true:false}
                            errorMessage={(dataResponse.message&&dataResponse.message.return_quantity)?dataResponse.message.return_quantity:""}
                            />
                        <INPUT
                            key={this.state.price_invoice + "price_invoice"}
                            name="price_invoice"
                            labelTitle={trans('messages.price_invoice')}
                            className="form-control input-sm"
                            fieldGroupClass="col-md-9"
                            labelClass="col-md-3"
                            onChange={this.handleChange}
                            defaultValue={this.state.price_invoice || ''}
                            readOnly={true}/>
                        <INPUT
                            key={this.state.price_invoice + "return_tanka"}
                            name="return_tanka"
                            labelTitle={trans('messages.return_price')}
                            className="form-control input-sm"
                            fieldGroupClass="col-md-9"
                            labelClass="col-md-3"
                            onChange={this.handleChange}
                            defaultValue={this.state.return_tanka || ''}/>
                        <DATEPICKER
                            placeholderText="YYYY-MM-DD"
                            dateFormat="YYYY-MM-DD"
                            locale="ja"
                            minDate={moment()}
                            name="receive_plan_date"
                            labelTitle={trans('messages.receive_plan_date')}
                            className="form-control input-sm"
                            fieldGroupClass="col-md-9"
                            labelClass="col-md-3"
                            selected={this.state.receive_plan_date ? moment(this.state.receive_plan_date) : null}
                            onChange={(date,name, e) => this.handleChangeDate(date, 'receive_plan_date', e)} />
                        <div className="row form-group padding-only-pc">
                            <DATEPICKER
                                placeholderText="YYYY-MM-DD"
                                dateFormat="YYYY-MM-DD"
                                locale="ja"
                                name="delivery_plan_date"
                                minDate={moment()}
                                labelTitle={trans('messages.delivery_plan_date')}
                                className="form-control input-sm"
                                fieldGroupClass="col-md-4 return-delivery-plan"
                                groupClass="data-picker-inline"
                                labelClass="col-md-3 clear-padding"
                                selected={this.state.delivery_plan_date ? moment(this.state.delivery_plan_date) : null}
                                onChange={(date,name, e) => this.handleChangeDate(date, 'delivery_plan_date', e)} />
                            <LABEL
                                groupClass="col-md-3"
                                labelClass=" "
                                labelTitle={trans('messages.payment_on_delivery')}>
                            </LABEL>
                            <div className="col-md-2">
                                <CHECKBOX_CUSTOM
                                    name="payment_on_delivery"
                                    defaultChecked={this.state.payment_on_delivery === 1 ? true : false}
                                    onChange={this.handleChange}
                                 />
                            </div>
                        </div>
                        <TEXTAREA
                            name="note"
                            labelTitle={trans('messages.note')}
                            className="form-control input-sm"
                            fieldGroupClass="col-md-9"
                            rows="8"
                            labelClass="col-md-3"
                            onChange={this.handleChange}
                            defaultValue={this.state.note || ''}/>
                    </div>
                    <div className="col-md-6">
                        <div key={this.state.checkDisplay + "lable"} className={(this.state.checkDisplay ? this.state.checkDisplay : 'hide') + " panel panel-default"} >
                            <div className="panel-body">
                                <LABEL
                                    groupClass="form-group col-md-12"
                                    labelTitle={trans('messages.return_fix_address')}>
                                </LABEL>
                                <INPUT
                                    key={this.state.supplier_return_postal + "return_postal"}
                                    name="return_postal"
                                    labelTitle={trans('messages.return_fix_postal')}
                                    className="form-control input-sm"
                                    fieldGroupClass="col-md-9"
                                    labelClass="col-md-3"
                                    defaultValue={this.state.supplier_return_postal || ''}
                                    readOnly={true}/>
                                <INPUT
                                    key={this.state.supplier_return_pref + "return_pref"}
                                    name="return_pref"
                                    labelTitle={trans('messages.return_fix_prefecture_city')}
                                    className="form-control input-sm"
                                    fieldGroupClass="col-md-9"
                                    labelClass="col-md-3"
                                    defaultValue={this.state.supplier_return_pref || ''}
                                    readOnly={true}/>
                                <INPUT
                                    key={this.state.supplier_return_address + "return_address"}
                                    name="return_sub_address"
                                    labelTitle={trans('messages.return_fix_sub_address')}
                                    className="form-control input-sm"
                                    fieldGroupClass="col-md-9"
                                    labelClass="col-md-3"
                                    defaultValue={this.state.supplier_return_address || ''}
                                    readOnly={true}/>
                                <TEXTAREA
                                    key={this.state.supplier_return_nm + "return_nm"}
                                    name="return_nm"
                                    labelTitle={trans('messages.return_fix_name')}
                                    className="form-control input-sm"
                                    fieldGroupClass="col-md-9"
                                    rows="3"
                                    labelClass="col-md-3"
                                    defaultValue={this.state.supplier_return_nm || ''}
                                    readOnly={true}/>
                                <INPUT
                                    key={this.state.supplier_return_tel + "return_tel"}
                                    name="return_tel"
                                    labelTitle={trans('messages.return_fix_tel')}
                                    className="form-control input-sm"
                                    fieldGroupClass="col-md-9"
                                    labelClass="col-md-3"
                                    defaultValue={this.state.supplier_return_tel || ''}
                                    readOnly={true}/>
                            </div>
                        </div>

                        <div key={this.state.checkDisplay + "input"} className={(this.state.checkDisplay ? this.state.checkDisplay : 'hide') + " panel panel-default"}>
                            <div className="panel-body">
                                <LABEL
                                    groupClass="form-group col-md-12"
                                    labelTitle={trans('messages.return_changed_address')}>
                                </LABEL>
                                <INPUT
                                    name="return_postal"
                                    labelTitle={trans('messages.return_change_postal')}
                                    className="form-control input-sm"
                                    fieldGroupClass="col-md-9"
                                    labelClass="col-md-3"
                                    onChange={this.handleChange}
                                    readOnly={true}
                                    defaultValue={this.state.return_postal || ''}/>
                                <INPUT
                                    name="return_pref"
                                    labelTitle={trans('messages.return_change_prefecture_city')}
                                    className="form-control input-sm"
                                    fieldGroupClass="col-md-9"
                                    labelClass="col-md-3"
                                    onChange={this.handleChange}
                                    readOnly={true}
                                    defaultValue={this.state.return_pref || ''}/>
                                <INPUT
                                    name="return_address"
                                    labelTitle={trans('messages.return_change_sub_address')}
                                    className="form-control input-sm"
                                    fieldGroupClass="col-md-9"
                                    labelClass="col-md-3"
                                    onChange={this.handleChange}
                                    readOnly={true}
                                    defaultValue={this.state.return_address || ''}/>
                                <INPUT
                                    name="return_nm"
                                    labelTitle={trans('messages.return_change_name')}
                                    className="form-control input-sm"
                                    fieldGroupClass="col-md-9"
                                    rows="5"
                                    labelClass="col-md-3"
                                    onChange={this.handleChange}
                                    readOnly={true}
                                    defaultValue={this.state.return_nm || ''}/>
                                <INPUT
                                    name="return_tel"
                                    labelTitle={trans('messages.return_change_tel')}
                                    className="form-control input-sm"
                                    fieldGroupClass="col-md-9"
                                    labelClass="col-md-3"
                                    onChange={this.handleChange}
                                    readOnly={true}
                                    defaultValue={this.state.return_tel || ''}/>
                                <span>返送先は個別で指定がない場合、空欄のままで登録すると、<br />
                                      設定済返送先が登録されます。</span>
                            </div>
                        </div>

                    </div>
                </div>
                <div className="row">
                    <div className="col-md-3 pull-right">
                        <div className="row">
                            <div className="col-md-6 col-xs-6">
                                <BUTTON name="btnAccept" id="btnSubmit" className="btn btn-success input-sm btn-larger" type="button" onClick={this.handleSubmit}>{trans('messages.btn_accept')}</BUTTON>
                            </div>
                            <div className="col-md-6 col-xs-6">
                                <BUTTON className="btn bg-purple input-sm pull-right btn-larger" type="button" onClick={this.handleCancel}>{trans('messages.btn_cancel')}</BUTTON>
                            </div>
                        </div>
                    </div>
                </div>
            </FORM>
            <MESSAGE_MODAL
                message={this.state.message || ''}
                title={trans('messages.deny_edit')}
                baseUrl={baseUrl}
            />
            </div>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        dataSource: state.commonReducer.dataSource,
        dataResponse: state.commonReducer.dataResponse,
        dataRealTime: state.commonReducer.dataRealTime
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        reloadData: (url, params) => { dispatch(Action.detailData(url, params)) },
        postData: (url, params, headers) => { dispatch(Action.postData(url, params, headers)) },
        postRealTime: (url, params, headers) => { dispatch(Action.postRealTime(url, params, headers)) }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(ReturnSave)