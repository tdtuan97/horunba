import React, { Component } from 'react';

import { connect } from 'react-redux';
import * as Action from '../common/actions';
import * as queryString from 'query-string';

import BUTTON from '../common/components/form/BUTTON';
import FORM from '../common/components/form/FORM';
import INPUT from '../common/components/form/INPUT';
import DATEPICKER from '../common/components/form/DATEPICKER';

import TABLE from '../common/components/table/TABLE';
import THEAD from '../common/components/table/THEAD';
import TBODY from '../common/components/table/TBODY';
import TR from '../common/components/table/TR';
import TH from '../common/components/table/TH';
import TD from '../common/components/table/TD';
import SELECT from '../common/components/form/SELECT';
import LABEL from '../common/components/form/LABEL';
import moment from 'moment';

import SortComponent from '../common/components/table/SortComponent';
import PaginationContainer from '../common/containers/PaginationContainer'
import Loading from '../common/components/common/Loading';
import axios from 'axios';
import ImportCsv from './csv/ImportCsv';
import PopupInformDelete from './modal-delete/PopupInformDelete';

class ReturnList extends Component {

    constructor(props) {
        super(props);
        const stringParams = queryString.parse(props.location.search);
        this.state = {};
        if (!_.isEmpty(stringParams)) {
            this.state = stringParams;
        }
        this.state.processBarColor = 'progress-bar-warning';
        this.props.reloadData(dataListUrl, this.state);
    }

    handleChange = (event) => {
        const target = event.target;
        const value = target.type === 'checkbox' ? target.checked : target.value;
        const name = target.name;
        this.setState({
            [name]: (value)?value:undefined
        },() => {
            if (name === 'per_page') {
                this.handleReloadData();
            }
        });
    }

    handleClearClick = (event) => {
        Object.keys(this.state).map((key) => {
            if (key !== 'per_page') {
                delete this.state[key];
            }
        });
        this.handleReloadData();
    }

    handleReloadData = () => {
        let params = {};
        let dataSource = this.props.dataSource;
        let tmpParams = this.state;
        Object.keys(tmpParams).map(key => {
            if (tmpParams[key] !== ""
                && tmpParams[key] !== null
                && key !== 'flg'
                && key !== 'attached_file_path'
                && key !== 'attached_file'
                && key !== 'page') {
                    params[key] = tmpParams[key];
            } else {
                delete params[key];
            }
        });
        this.props.reloadData(dataListUrl, params, true);
    }
    handleChangeMultiSelect = (name, value, checked) => {
        let currentValue = this.state[name];
        if (typeof currentValue === 'undefined') {
            currentValue = [];
        } else if (!Array.isArray(this.state[name])) {
            currentValue = [this.state[name]];
        }
        if (Array.isArray(value)) {
            currentValue = [...value];
        } else {
            let index = currentValue.indexOf(value);
            if (checked === false) {
                if (index !== -1) {
                    currentValue.splice(index, 1);
                }
            } else {
                if (index === -1) {
                    currentValue.push(value);
                }
            }
        }
        this.setState({
            [name]: currentValue,
        });
    }
    componentDidUpdate() {
        if (!_.isEmpty(this.props.dataSource)) {
            $('.loading').hide();
        }
        var heightTable = $('table.table-custom').height();
        $("table th").addClass('resize-thead');
        $("table th").resizeble();

        $('.table-responsive').on('scroll', function (e) {
            $('div.scroll-bar-top').scrollLeft($('.table-responsive').scrollLeft());
        });
        $('.table-custom').on('scroll', function (e) {
            $('.table-custom').scrollLeft();
        });
        $('div.scroll-bar-top').on('scroll', function (e) {
            $('.table-responsive').scrollLeft($('div.scroll-bar-top').scrollLeft());
        });
        $(document).ready(function() {
            $('div.scroll-bar-top>div').width($('table.table-custom').width());
        });
        delete this.state['finish'];
        delete this.state['message_success'];
        dragscroll.reset();
    }

    componentWillMount() {
        window.onpopstate = (event) => {
            location.reload();
        };
    }

    handleSortClick = (name, nextSort) => {
        Object.keys(this.state).map((key) => {
            if (key.startsWith('sort_')) {
                delete this.state[key];
            }
        });
        if (nextSort === 'none') {
            delete this.state[name];
            this.props.reloadData(dataListUrl, this.state, true);
        } else {
            this.setState({[name]: nextSort}, () => {
                this.props.reloadData(dataListUrl, this.state, true);
            });
        }

    }

    handleChangeDate = function(date, name, event) {
        let dateValue = moment(date);
        if (!dateValue.isValid()) {
            dateValue = undefined;
        } else {
            dateValue = dateValue.format('YYYY-MM-DD');
        }
        this.setState({
          [name]: dateValue,
        });
    }

    handleClickPage = (page) => {
        this.setState({
            page: page
        },() => {
            this.props.reloadData(dataListUrl, this.state, true);
        });
    }
    handleBeforeDelete = (index = {},event) => {
        $('#access-memo-delete-modal').modal('show');
        this.setState({index_key : index});
    }
    handleDelete = () => {
        $('#access-memo-delete-modal').modal('show');
        this.props.postData(deleteUrl, {'index_key' : this.state.index_key},
                    {'X-Requested-With': 'XMLHttpRequest'},
                    this.handleReloadData
                    );
        $('#access-memo-delete-modal').modal('hide');
    }
    handlechangeFlag = (index = {},event) => {
        this.props.postData(changeFlagUrl, index,
                    {'X-Requested-With': 'XMLHttpRequest'},
                    this.handleReloadData
                    );
        $('#access-memo-delete-modal').modal('hide');
    }
    handleIsDeleteChange = (index, event) => {
        const value = event.target.checked;
        let params  = {
            index : index,
            is_delete : value?1:0
        }
        this.props.postData(changeDeleteUrl, params, {'X-Requested-With': 'XMLHttpRequest'}, this.handleReloadData);
    }

    handleMojax = () => {
        $(".loading").show();
        let params  = {status: 'mojax'}
        axios.post(mojaxUrl, params, {headers: {'X-Requested-With': 'XMLHttpRequest'}}
        ).then(response => {
            $(".loading").hide();
            if (typeof response.data.msg !== 'undefined' && response.data.msg !== '') {
                $.notify({
                    icon: 'glyphicon glyphicon-warning-sign',
                    message: response.data.msg,
                },{
                    type: 'info',
                    delay: 2000
                });
                this.handleReloadData();
            }
        }).catch(error => {
            $(".loading").hide();
            throw(error);
        });
    }

    handleChooseFile = (event) => {
        const target = event.target;
        if (target.files !== null && target.files.length !== 0) {
            const name = target.name;
            let tmpName = name.replace(new RegExp("^browser_"), '');
            this.setState({
                [tmpName]: target.files[0].name,
                ['attached_file']: target.files[0]
            }, ()=> {
                document.getElementsByName(tmpName)[0].value = target.files[0].name;
            });
        }
    }

    handleImportSubmit = () => {
        let formData   = new FormData();
        let headers = {'X-Requested-With': 'XMLHttpRequest'};
        if (!_.isEmpty(this.state)) {
            Object.keys(this.state).map((item) => {
                formData.append(item, this.state[item]);
            });
        }
        this.flg_check = true;
        this.setState({
            process: true,
            status_csv: 'show',
            percent: 0,
        }, () => {
            formData.append('name_csv', $('#action-modal').val());
            this.props.postDataCSV(csvUrl, formData, headers);
        });
    }

    handleImportCancel = () => {
        $('#issue-modal').modal('hide');
        this.flg_check = false;
        this.setState({
            attached_file_path: '',
            attached_file: '',
            status_csv: false,
            process: false,
            message: '',
            finish: 0,
            error: 0,
            msg: '',
        }, () => {
            $("input[name='browser_attached_file_path']").val('');
            this.handleReloadData();
        });
    }

    handleImport = (event, name) => {
        $('#action-modal').val(name);
        $('#issue-modal').modal({backdrop: 'static', show: true});
    }

    componentWillReceiveProps(nextProps) {
        let dataResponse = nextProps.dataResponseCsv;
        if (!_.isEmpty(dataResponse)  && this.flg_check) {
            if (typeof dataResponse.not_permission !== "undefined" && dataResponse.not_permission === 1) {
                location.reload();
            }
            if (typeof dataResponse.reload !== 'undefined' && dataResponse.reload) {
                this.props.postDataCSV(importUrl, dataResponse.params, dataResponse.headers, dataResponse.count_err);
            }
            let formData   = new FormData();
            let headers = {'X-Requested-With': 'XMLHttpRequest'};
            Object.keys(dataResponse).map((item) => {
                formData.append(item, dataResponse[item]);
            });
            formData.append('type', $('#action-modal').val());
            let return_type_mid = $("input[type='radio'][name='return_type_mid']:checked").val();
            formData.append('return_type_mid', return_type_mid);
            let showStatus = true;
            if(dataResponse.flg === 1) {
                if (typeof dataResponse.error !== 'undefined' && dataResponse.error === 1) {
                    let dataSet = {
                        process: false,
                        status_csv: false,
                        finish: 1,
                    };
                    this.setState({...dataResponse,attached_file_path:'', ...dataSet}, () => {
                        $("input[name='browser_attached_file_path']").val('');
                        $("input[name='browser_attached_file_path']").val('');
                    });
                } else {
                    if (dataResponse.checkAfter === 1 && dataResponse.currPage === 0) {
                        $.notify({
                            icon: 'glyphicon glyphicon-warning-sign',
                            message: 'Process import.',
                        },{
                            type: 'info',
                            delay: 2000
                        });
                    }
                    this.setState({
                        process: true,
                        status_csv: 'show',
                        percent: (dataResponse.currPage/dataResponse.timeRun) * 100,
                        processBarColor: dataResponse.checkAfter === 0 ? 'progress-bar-warning' : 'progress-bar-primary',
                    }, () => {
                        this.props.postDataCSV(importUrl, formData, headers);
                    });
                }
            }
            else if (dataResponse.flg === 0) {
                let dataSet = {
                    process: false,
                    status_csv: false,
                };
                let messageTxt = '';
                if (dataResponse.process === 'uploadCsv') {
                    if (dataResponse.msg === 'Finish') {
                        dataSet = {
                            status_csv: 'show',
                            finish: 1,
                        };
                        messageTxt = trans('messages.message_import_success');
                        this.setState({...dataResponse,attached_file_path:'', ...dataSet}, () => {
                            this.handleImportCancel();
                            $("input[name='browser_attached_file_path']").val('');
                            $("input[name='browser_attached_file_path']").val('');
                        });
                    } else {
                        showStatus = false;
                        this.setState({...dataResponse, ...dataSet});
                    }
                    this.flg_check = false;
                }
                if (showStatus) {
                    $.notify({
                        icon: 'glyphicon glyphicon-warning-sign',
                        message: messageTxt,
                    },{
                        type: 'info',
                        delay: 2000
                    });
                }
            }
        }
    }

    render() {
        let dataSource = this.props.dataSource;
        let index = 0;
        let totalItems = 0;
        let itemsPerPage = 20;
        if (!_.isEmpty(dataSource)) {
            index = dataSource.data.from - 1;
            totalItems   = dataSource.data.total;
            itemsPerPage = dataSource.data.per_page;
        }
        let nf = new Intl.NumberFormat();
        return ( (!_.isEmpty(dataSource)) &&
            <div>
                <Loading className="loading" />
                <div className="box">
                    <div className="box-header">
                        <div className="row">
                                <div className="col-md-12">
                                    <h4>{trans('messages.search_title')}</h4>
                                </div>
                                <FORM>
                                    <INPUT
                                        name="return_no"
                                        groupClass="form-group col-md-1-25"
                                        labelTitle={trans('messages.return_no')}
                                        onChange={this.handleChange}
                                        value={this.state.return_no||''}
                                        className="form-control input-sm" />
                                    <DATEPICKER
                                        groupClass="form-group col-md-1-5 clear-padding"
                                        labelTitle={trans('messages.return_date_from')}
                                        placeholderText="YYYY-MM-DD"
                                        dateFormat="YYYY-MM-DD"
                                        locale="ja"
                                        name="return_date_from"
                                        className="form-control input-sm"
                                        key="return_date_from"
                                        id="return_date_from"
                                        selected={this.state.return_date_from ? moment(this.state.return_date_from) : null}
                                        onChange={(date, name, e) => this.handleChangeDate(date, 'return_date_from', e)} />
                                    <div className="col-md-0-25 hidden-xs hidden-sm"><span className="to-from-child">～</span></div>
                                    <DATEPICKER
                                        groupClass="form-group col-md-1-5 clear-padding"
                                        labelTitle={trans('messages.return_date_to')}
                                        placeholderText="YYYY-MM-DD"
                                        dateFormat="YYYY-MM-DD"
                                        locale="ja"
                                        name="return_date_to"
                                        className="form-control col-md-12 input-sm"
                                        key="return_date_to"
                                        id="return_date_to"
                                        selected={this.state.return_date_to ? moment(this.state.return_date_to) : null}
                                        onChange={(date, name, e) => this.handleChangeDate(date, 'return_date_to', e)} />
                                    <INPUT
                                        name="deje_no"
                                        groupClass="form-group col-md-1-25"
                                        labelTitle={trans('messages.deje_num')}
                                        onChange={this.handleChange}
                                        value={this.state.deje_no||''}
                                        className="form-control input-sm" />
                                    <INPUT
                                        name="product_code_from"
                                        groupClass="form-group col-md-1-5 clear-padding"
                                        labelTitle={trans('messages.product_code_from')}
                                        onChange={this.handleChange}
                                        value={this.state.product_code_from||''}
                                        className="form-control input-sm" />
                                    <div className="col-md-0-25 hidden-xs hidden-sm"><span className="to-from-child">～</span></div>
                                    <INPUT
                                        name="product_code_to"
                                        groupClass="form-group col-md-1-5 clear-padding"
                                        labelTitle={trans('messages.product_code_to')}
                                        onChange={this.handleChange}
                                        value={this.state.product_code_to||''}
                                        className="form-control input-sm" />
                                    <INPUT
                                        name="supplier_name"
                                        groupClass="form-group col-md-1-5"
                                        labelTitle={trans('messages.supplier_name')}
                                        onChange={this.handleChange}
                                        value={this.state.supplier_name||''}
                                        className="form-control input-sm" />
                                    <SELECT
                                        name="per_page"
                                        groupClass="form-group col-md-1 per-page pull-right-md"
                                        labelClass="pull-left"
                                        labelTitle={trans('messages.per_page')}
                                        onChange={this.handleChange}
                                        value={this.state.per_page||''}
                                        options={{20:20, 40:40, 60:60, 80:80, 100:100}}
                                        className="form-control input-sm" />
                                    <div className="row">
                                    <div className="col-md-12">
                                        <SELECT
                                            id="return_type_large_id"
                                            name="return_type_large_id"
                                            groupClass="form-group col-md-1-5"
                                            labelClass=""
                                            labelTitle={trans('messages.search_large_kbn')}
                                            handleChangeMultiSelect={this.handleChangeMultiSelect}
                                            valueMulti={this.state.return_type_large_id||''}
                                            options={dataSource.arrLarge}
                                            multiple="multiple"
                                            className="form-control input-sm" />
                                        <SELECT
                                            id="return_type_mid_id"
                                            name="return_type_mid_id"
                                            groupClass="form-group col-md-1-5"
                                            labelClass=""
                                            labelTitle={trans('messages.search_mid_kbn')}
                                            handleChangeMultiSelect={this.handleChangeMultiSelect}
                                            valueMulti={this.state.return_type_mid_id||''}
                                            options={dataSource.arrMid}
                                            multiple="multiple"
                                            className="form-control input-sm" />
                                        <SELECT
                                            id="return_status"
                                            name="return_status"
                                            groupClass="form-group col-md-1-5"
                                            labelClass="control-label"
                                            labelTitle={trans('messages.search_status')}
                                            handleChangeMultiSelect={this.handleChangeMultiSelect}
                                            valueMulti={this.state.return_status||''}
                                            options={dataSource.arrStatusOption}
                                            fieldGroupClass = ""
                                            multiple="multiple"
                                            className="form-control input-sm" />
                                        <SELECT
                                            id="receive_instruction"
                                            name="receive_instruction"
                                            groupClass="form-group col-md-1-5"
                                            labelClass="control-label"
                                            labelTitle={trans('messages.search_receive')}
                                            handleChangeMultiSelect={this.handleChangeMultiSelect}
                                            valueMulti={this.state.receive_instruction||''}
                                            options={dataSource.arrCommonStatus}
                                            fieldGroupClass = ""
                                            multiple="multiple"
                                            className="form-control input-sm" />
                                        <SELECT
                                            id="delivery_instruction"
                                            name="delivery_instruction"
                                            groupClass="form-group col-md-1-5"
                                            labelClass="control-label"
                                            labelTitle={trans('messages.search_delivery')}
                                            handleChangeMultiSelect={this.handleChangeMultiSelect}
                                            valueMulti={this.state.delivery_instruction||''}
                                            options={dataSource.arrCommonStatus}
                                            fieldGroupClass = ""
                                            multiple="multiple"
                                            className="form-control input-sm" />
                                        <SELECT
                                            id="red_voucher"
                                            name="red_voucher"
                                            groupClass="form-group col-md-1-5"
                                            labelClass="control-label"
                                            labelTitle={trans('messages.search_akaden')}
                                            handleChangeMultiSelect={this.handleChangeMultiSelect}
                                            valueMulti={this.state.red_voucher||''}
                                            options={dataSource.arrCommonStatus}
                                            fieldGroupClass = ""
                                            multiple="multiple"
                                            className="form-control input-sm" />
                                        </div>
                                        </div>
                                </FORM>
                        </div>
                        <div className="row">
                            <div className="col-md-9 col-xs-4">
                                <a href={saveProductUrl} className="btn btn-success input-sm margin-element margin-form">{trans('messages.btn_add')}</a>
                                <BUTTON className="btn btn-primary margin-element margin-form" onClick={this.handleMojax}>{trans('messages.btn_mojax')}</BUTTON>
                                <BUTTON className="btn btn-primary margin-form" onClick={(e) => this.handleImport(e, 'import_mojax_csv')}>{trans('messages.mojax_csv_import')}</BUTTON>
                            </div>
                            <div className="col-md-3 col-xs-8">
                                    <BUTTON className="btn btn-danger pull-right" onClick={this.handleClearClick}>{trans('messages.btn_reset')}</BUTTON>
                                    <BUTTON className="btn btn-primary pull-right margin-element" onClick={this.handleReloadData} >{trans('messages.btn_search')}</BUTTON>
                            </div>
                        </div>
                    </div>
                    <div className="box-body">
                        <div className="row">
                            <div className="col-md-12">
                                {(totalItems/itemsPerPage > 1) &&
                                <div className="row">
                                    <div className="col-md-2-5"></div>
                                    <div className="col-md-7 text-center">
                                        <PaginationContainer handleClickPage={this.handleClickPage}/>
                                    </div>
                                    <div className="col-md-2-5 text-right line-height-md">
                                        {dataSource.data.from}件 - {dataSource.data.to}件 【全{dataSource.data.total}件】
                                    </div>
                                </div>
                                }
                                <div className="table-responsive">
                                <TABLE className="table table-striped table-custom">
                                    <THEAD>
                                        <TR>
                                            <TH className="visible-xs visible-sm"></TH>
                                            <TH className="hidden-xs hidden-sm col-max-min-60 text-center text-middle" rowSpan="2">
                                                {trans('messages.index')}
                                            </TH>
                                            <TH className="hidden-xs hidden-sm col-max-min-60 text-center text-middle" rowSpan="2">
                                                {trans('messages.btn_delete')}
                                            </TH>
                                            <TH className="col-max-min-100 text-center text-middle" rowSpan="2">
                                                <SortComponent
                                                    name="sort_return_no"
                                                    onClick={this.handleSortClick}
                                                    currentSort={(this.state.sort_return_no)?this.state.sort_return_no:'none'}
                                                    title={trans('messages.return_no')} />
                                            </TH>
                                            <TH className="col-max-min-100 text-center text-middle"  rowSpan="2">
                                                {trans('messages.return_date')}
                                            </TH>
                                            <TH className="hidden-xs hidden-sm col-max-min-120 text-center text-middle" rowSpan="2">
                                                {trans('messages.deje_num')}
                                            </TH>
                                            <TH className="hidden-xs hidden-sm col-max-min-120 text-center text-middle" rowSpan="2">
                                                {trans('messages.order_type')}
                                            </TH>
                                            <TH className="hidden-xs hidden-sm text-center text-middle col-max-min-120" rowSpan="2">{trans('messages.glsc_receive')}</TH>
                                            <TH className="hidden-xs hidden-sm text-center text-middle col-max-min-120" rowSpan="2">{trans('messages.supplier_return_no')}</TH>
                                            <TH className="hidden-xs hidden-sm col-max-min-40 text-center text-middle col-max-min-60" rowSpan="2">
                                                {trans('messages.return_status')}
                                            </TH>
                                            <TH className="hidden-xs hidden-sm text-center text-middle col-max-min-60" rowSpan="2">{trans('messages.arrival')}</TH>
                                            <TH className="hidden-xs hidden-sm text-center text-middle col-max-min-60"  rowSpan="2">{trans('messages.shipment')}</TH>
                                            <TH className="hidden-xs hidden-sm col-max-min-60 text-center text-middle " rowSpan="2">
                                                {trans('messages.red_voucher')}
                                            </TH>
                                            <TH className="col-max-min-100 text-center text-middle" rowSpan="2">
                                                {trans('messages.product_code')}
                                            </TH>
                                            <TH className="col-max-min-200 text-center text-middle" rowSpan="2">
                                                {trans('messages.product_name')}
                                            </TH>
                                            <TH className="hidden-xs hidden-sm col-max-min-100 text-center text-middle" rowSpan="2">
                                                {trans('messages.product_jan')}
                                            </TH>
                                            <TH className="hidden-xs hidden-sm col-max-min-60 text-center text-middle" rowSpan="2">
                                                {trans('messages.quantity')}
                                            </TH>
                                            <TH className="hidden-xs hidden-sm text-center">
                                                {trans('messages.supplier_name')}
                                            </TH>
                                            <TH className="col-max-min-100 text-center text-middle" rowSpan="2">{trans('messages.list_return_price')}
                                            </TH>
                                        </TR>
                                        <TR>


                                            <TH className="hidden-xs hidden-sm col-max-min-200 text-center">
                                                {trans('messages.mall_name')}
                                            </TH>
                                        </TR>
                                    </THEAD>
                                    <TBODY>
                                    {
                                        (!_.isEmpty(dataSource.data.data))?
                                        dataSource.data.data.map((item, itemIndex) => {
                                            index++;
                                            let classRow = (index%2 ===0) ? 'odd' : 'even';
                                            let classErr = item.error_code?' line-error':'';

                                            let lableRedVoucher = item.red_voucher === 1 ? '中' : item.red_voucher === 2 ? '済' : '--';
                                            let url = item.receive_id !== null ? (saveDetailUrl + "?receive_id=" + item.receive_id) : (saveProductUrl + "?return_no=" + item.return_no);
                                            let urlDeje = "https://www.yinhena.jp/cgi-bin/cbdb/db.cgi?page=DBRecord&did=431&rid=" + item.deje_no;
                                            if (item.deje_check === 1) {
                                                urlDeje = dataSource.urlDeje + item.deje_no;
                                            }
                                            return (
                                                <TR key={"tr" + index + itemIndex} className={classRow + classErr}>
                                                    <TD className="visible-xs visible-sm">
                                                        <b className="show-info">
                                                            <i className="fa fa-angle-double-down" aria-hidden="true"></i>
                                                        </b>
                                                    </TD>
                                                    <TD className="hidden-xs hidden-sm text-center">
                                                       {index}
                                                    </TD>
                                                    <TD className="hidden-xs hidden-sm text-center">
                                                    {
                                                        item.status === 0  && item.receive_id === null ?
                                                       <BUTTON className="btn btn-xs btn-danger"
                                                        onClick={(e) => {this.handleBeforeDelete(
                                                                    {   return_no :item.return_no,
                                                                        return_line_no :item.return_line_no,
                                                                        return_time :item.return_time}, e);}} >
                                                            {trans('messages.btn_delete')}
                                                        </BUTTON>
                                                         : ''
                                                    }
                                                    </TD>
                                                    <TD className="text-center">
                                                        <a className="link-detail" href={url}>
                                                            {item.return_no}
                                                        </a>
                                                    </TD>
                                                    <TD className="cut-text"
                                                            title={item.return_date}>{item.return_date}
                                                        </TD>
                                                        <TD className="hidden-xs hidden-sm cut-text text-left"
                                                            title={item.deje_no}>
                                                            <a className="link-detail" target="_blank" href={urlDeje}>
                                                                {item.deje_no}
                                                            </a>
                                                        </TD>
                                                        <TD className="hidden-xs hidden-sm cut-text"
                                                            title={item.return_type_large + "\n" + item.return_type_mid}>
                                                            <p className="text-long-batch col-max-min-120">
                                                                {item.return_type_large}
                                                            </p>
                                                            <p className="text-long-batch col-max-min-120">
                                                                {item.return_type_mid}
                                                            </p>
                                                        </TD>
                                                        <TD className="hidden-xs hidden-sm cut-text"
                                                            title={item.glsc_receive}>{item.glsc_receive}
                                                        </TD>
                                                        <TD className="hidden-xs hidden-sm cut-text"
                                                            title={item.glsc_receive}>{item.supplier_return_no}
                                                        </TD>
                                                        <TD className="hidden-xs hidden-sm cut-text text-center"
                                                            title={item.status}>{dataSource.arrStatus[item.status]}
                                                        </TD>
                                                        <TD className="hidden-xs hidden-sm cut-text text-center"
                                                            title={item.receive_instruction}>{dataSource.arrReceiveInstruction[item.receive_instruction]}
                                                        </TD>

                                                        <TD className="hidden-xs hidden-sm cut-text text-center"
                                                            title={item.delivery_instruction}>{dataSource.arrDeliveryInstruction[item.delivery_instruction]}
                                                        </TD>
                                                        <TD className="hidden-xs hidden-sm cut-text text-center"
                                                            title={item.red_voucher}>
                                                            {   ( item.red_voucher === 1)  ?
                                                                    <BUTTON className="btn btn-xs btn-info"
                                                                         onClick={(e) => this.handlechangeFlag(
                                                                                    {red_voucher : 2 ,
                                                                                    return_no :item.return_no,
                                                                                    return_line_no :item.return_line_no,
                                                                                    return_time :item.return_time},e)} >
                                                                        {lableRedVoucher}
                                                                    </BUTTON>
                                                                :  (item.red_voucher === 2) ?

                                                                    <BUTTON className="btn btn-xs btn-success"
                                                                    onClick={(e) => this.handlechangeFlag(
                                                                                    {red_voucher : 1,
                                                                                    return_no :item.return_no,
                                                                                    return_line_no :item.return_line_no,
                                                                                    return_time :item.return_time},e)} >
                                                                        {lableRedVoucher}
                                                                    </BUTTON>
                                                                    : dataSource.arrDeliveryInstruction[item.red_voucher]

                                                            }

                                                        </TD>
                                                        <TD className="cut-text"
                                                            title={item.parent_product_code}>{item.parent_product_code}
                                                        </TD>
                                                        <TD className="cut-text"
                                                            title={item.product_name}>{item.product_name}
                                                        </TD>
                                                        <TD className="hidden-xs hidden-sm cut-text"
                                                            title={item.product_jan}>{item.product_jan}
                                                        </TD>
                                                        <TD className="hidden-xs hidden-sm cut-text text-right"
                                                            title={item.return_quantity}>{item.return_quantity}
                                                        </TD>
                                                        <TD className="hidden-xs hidden-sm cut-text"
                                                            title={item.supplier_nm + "\n" + item.maker_full_nm}>
                                                            <p className="text-long-batch col-max-min-200">
                                                                {item.supplier_nm}
                                                            </p>
                                                            <p className="text-long-batch col-max-min-200">
                                                                {item.maker_full_nm}
                                                            </p>
                                                        </TD>
                                                        <TD className="cut-text text-right"
                                                            title={item.return_tanka}>{nf.format(item.return_tanka)}
                                                    </TD>
                                                </TR>
                                            )
                                        })
                                        :
                                        <TR><TD colSpan="17" className="text-center">{trans('messages.no_data')}</TD></TR>
                                    }
                                    </TBODY>
                                </TABLE>
                                </div>
                                <div className="text-center">
                                    <PaginationContainer handleClickPage={this.handleClickPage}/>
                                </div>
                                <ImportCsv
                                    handleChooseFile={this.handleChooseFile}
                                    handleChange={this.handleChange}
                                    handleImportSubmit={this.handleImportSubmit}
                                    value={this.state.attached_file_path ? this.state.attached_file_path : ''}
                                    error={this.state.error}
                                    filename={this.state.filename}
                                    handleImportCancel={this.handleImportCancel}
                                    process={this.state.process ? true : false}
                                    status={this.state.status_csv}
                                    percent={this.state.percent}
                                    processBarColor={this.state.processBarColor}
                                    finish={this.state.finish}
                                    hasError={(this.state.message || this.state.msg) ? true : false}
                                    errorMessage={this.state.message || this.state.msg}
                                />
                                <PopupInformDelete  handleDelete={this.handleDelete}
                                    message={
                                        trans('messages.delete_message')
                                        .replace(":return_no", typeof this.state.index_key !== 'undefined' ? this.state.index_key.return_no : '')
                                        .replace(":index_key", typeof this.state.index_key !== 'undefined' ?
                                            this.state.index_key.return_no + this.state.index_key.return_time  + this.state.index_key.return_line_no : '' )} />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        dataSource: state.commonReducer.dataSource,
        dataResponse: state.commonReducer.dataResponse,
        dataResponseCsv: state.commonReducer.dataResponseCsv
    }
}

const mapDispatchToProps = (dispatch) => {
    let ignore = ['attached_file', 'attached_file_path','message','flg', 'process', 'msg', 'timeRun', 'index_key', 'currPage', 'total', 'filename','processBarColor', 'error','percent', 'status_csv', 'url', 'reload', 'params', 'headers', 'count','realFilename','type','fileCorrect', 'checkAfter'];
    return {
        reloadData: (url, params, pushHistory, ignoreFields) => { dispatch(Action.reloadData(url, params, pushHistory, ignore)) },
        postData: (url, params, headers, callback) => { dispatch(Action.postData(url, params, headers, callback)) },
        postDataCSV: (url, params, headers, count_err) => { dispatch(Action.postDataCSV(url, params, headers, count_err)) }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(ReturnList)