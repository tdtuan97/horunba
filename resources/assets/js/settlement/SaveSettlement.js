import React, { Component } from 'react';
import { connect } from 'react-redux';

import * as Action from '../common/actions';
import * as Helpers from '../common/helpers';
import * as queryString from 'query-string';

import Loading from '../common/components/common/Loading';
import FORM from '../common/components/form/FORM';
import INPUT from '../common/components/form/INPUT';
import SELECT from '../common/components/form/SELECT';
import TEXTAREA from '../common/components/form/TEXTAREA';
import UPLOAD from '../common/components/form/UPLOAD';
import BUTTON from '../common/components/form/BUTTON';
import MESSAGE_MODAL from '../common/containers/MessageModal';

class SaveSettlement extends Component {
    constructor(props) {
        super(props);
        this.state = {};
        let params = queryString.parse(props.location.search);
        if (params['payment_code']) {
            this.state.payment_code = params['payment_code'];
            this.state.index = params['payment_code'];
            this.state.accessFlg = true;
        }

        this.props.reloadData(formDataUrl, this.state);
    }

    handleCancel = (event) => {
        window.location.href = baseUrl;
    }

    hanleRealtime() {
        if (!_.isEmpty(this.state.index)) {
            let params = {
                accessFlg : this.state.accessFlg,
                query : {
                        page_key : 'SETTLEMENT-SAVE',
                        primary_key : 'payment_code:' + this.state.payment_code
                    },
                key :this.state.index
            };
            if (this.state.accessFlg !== false) {
                this.props.postRealTime(checkEditUrl, params, {'X-Requested-With': 'XMLHttpRequest'});
            }
        }
    }

    handleSubmit = (event) => {
        document.getElementsByName('btnAccept')[0].disabled = true;
        let headers = {'X-Requested-With': 'XMLHttpRequest'};
        this.props.postData(saveUrl, this.state, headers);
    }

    handleChange = (event) => {
        const target = event.target;
        const name = target.name;
        const value = target.type === 'checkbox' ? target.checked : target.value;
        this.setState({
            [name]: value
        });
    }
    componentWillUnmount () {
        clearInterval(this.state);
    }
    componentDidMount() {
        setInterval(() => {
            this.hanleRealtime()
        }, 5000);

    }
    componentWillReceiveProps(nextProps) {
        // Check get data update
        //this.setState({...nextProps.dataSource.data});
        let dataSource = nextProps.dataSource;
        if (!_.isEmpty(dataSource.data)  && _.isEmpty(nextProps.dataRealTime) ) {
            this.setState({...dataSource.data}, () => {
                this.hanleRealtime();
            });
        }
        if (!_.isEmpty(nextProps.dataSource)) {
            if (!nextProps.dataSource.data) {
                window.location.href = baseUrl;
            }
        }
        // Check post data update
        if (!_.isEmpty(nextProps.dataResponse)) {
            if (nextProps.dataResponse.status === true) {
                window.location.href = baseUrl;
            } else {
                document.getElementsByName('btnAccept')[0].disabled = false;
            }
        }
        // Check realtime update
        if (!_.isEmpty(nextProps.dataRealTime)) {
           this.setState(nextProps.dataRealTime);
        }
    }

    componentDidUpdate() {
        if (!_.isEmpty(this.props.dataSource)) {
            $('.loading').hide();
        }
        if (this.state.accessFlg === false) {
            $('#message-modal').modal('show');
            //We will auto redirect after 5 seconds
            setTimeout(() => {
                window.location.href = baseUrl;
            }, 5000);
        }
    }

    render() {
        let dataSource   = this.props.dataSource;
        let dataResponse = this.props.dataResponse;
        let paymentRequest = {
            '' : '-----',
            0 : '無',
            1 : '有',
        };
        let haveParams = {
            '' : '-----',
            0 : '無',
            1 : '有',
        };

        return ((!_.isEmpty(dataSource)) &&
            <div>
            <div className="content">
            <div className="box box-primary">

            <FORM className="form-horizontal">
                <div className="box-body">
                <Loading className="loading" />
                <div className="row">
                    <div className="col-md-8 col-md-offset-1 col-md-offset-right-3 ">
                        <div className="row">
                            <div className="col-md-12">
                                <INPUT
                                    name="payment_name"
                                    labelTitle={trans('messages.settlement_name')}
                                    className="form-control input-sm"
                                    fieldGroupClass="col-md-3-5"
                                    labelClass="col-md-4"
                                    onChange={this.handleChange}
                                    defaultValue={dataSource.data.payment_name}
                                    errorPopup={true}
                                    hasError={(dataResponse.message && dataResponse.message.payment_name)?true:false}
                                    errorMessage={(dataResponse.message&&dataResponse.message.payment_name)?dataResponse.message.payment_name:""}/>
                                <SELECT
                                    name="payment_request"
                                    labelTitle={trans('messages.payment_check')}
                                    className="form-control input-sm"
                                    fieldGroupClass="col-md-2"
                                    labelClass="col-md-4"
                                    groupClass=""
                                    onChange={this.handleChange}
                                    options={paymentRequest}
                                    defaultValue={_.has(dataSource.data, 'payment_request') ? dataSource.data.payment_request : ''}
                                    hasError={dataResponse.message&&dataResponse.message.payment_request?true:false}
                                    errorMessage={dataResponse.message&&dataResponse.message.payment_request?dataResponse.message.payment_request:""}/>
                                <TEXTAREA
                                    name="add_2_mail_text"
                                    labelTitle={trans('messages.add_2_mail_text')}
                                    className="form-control input-sm"
                                    fieldGroupClass="col-md-8"
                                    labelClass="col-md-4"
                                    onChange={this.handleChange}
                                    defaultValue={dataSource.data.add_2_mail_text}
                                    errorPopup={true}
                                    hasError={(dataResponse.message && dataResponse.message.add_2_mail_text)?true:false}
                                    errorMessage={(dataResponse.message&&dataResponse.message.add_2_mail_text)?dataResponse.message.add_2_mail_text:""}
                                    rows="10"/>
                                <TEXTAREA
                                    name="add_2_receipt_text"
                                    labelTitle={trans('messages.add_2_receipt_text')}
                                    className="form-control input-sm"
                                    fieldGroupClass="col-md-8"
                                    labelClass="col-md-4"
                                    onChange={this.handleChange}
                                    defaultValue={dataSource.data.add_2_receipt_text}
                                    errorPopup={true}
                                    hasError={(dataResponse.message && dataResponse.message.add_2_receipt_text)?true:false}
                                    errorMessage={(dataResponse.message&&dataResponse.message.add_2_receipt_text)?dataResponse.message.add_2_receipt_text:""}
                                    rows="4"/>
                                <SELECT
                                    name="have_para"
                                    labelTitle={trans('messages.have_parameter')}
                                    className="form-control input-sm"
                                    fieldGroupClass="col-md-2"
                                    labelClass="col-md-4"
                                    groupClass=""
                                    onChange={this.handleChange}
                                    options={haveParams}
                                    defaultValue={_.has(dataSource.data,'have_para') ?  dataSource.data.have_para : ''}
                                    hasError={dataResponse.message&&dataResponse.message.have_para?true:false}
                                    errorMessage={dataResponse.message&&dataResponse.message.have_para?dataResponse.message.have_para:""}/>
                            </div>
                        </div>
                        <div className="row">
                            <div className="col-md-8 col-md-offset-4 col-md-offset-right-0">
                                <BUTTON className="btn bg-purple input-sm btn-larger pull-right" type="button" onClick={this.handleCancel}>{trans('messages.btn_cancel')}</BUTTON>
                                <BUTTON name="btnAccept" className="btn btn-success input-sm btn-larger pull-right margin-element" type="button" onClick={this.handleSubmit}>{trans('messages.btn_accept')}</BUTTON>
                            </div>
                            </div>
                        </div>
                    </div>
                </div>
            </FORM>
            </div>
            <MESSAGE_MODAL
                message={this.state.message || ''}
                title={trans('messages.deny_edit')}
                baseUrl={baseUrl}
            />
            </div>
        </div>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        dataSource: state.commonReducer.dataSource,
        dataResponse: state.commonReducer.dataResponse,
        dataRealTime: state.commonReducer.dataRealTime
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        reloadData: (url, params) => { dispatch(Action.detailData(url, params)) },
        postData: (url, params, headers) => { dispatch(Action.postData(url, params, headers)) },
        postRealTime: (url, params, headers) => { dispatch(Action.postRealTime(url, params, headers)) }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(SaveSettlement)