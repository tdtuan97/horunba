import React, { Component } from 'react';

import { connect } from 'react-redux';
import * as Action from '../common/actions';
import * as queryString from 'query-string';
import BUTTON from '../common/components/form/BUTTON';
import FORM from '../common/components/form/FORM';
import INPUT from '../common/components/form/INPUT';
import SELECT from '../common/components/form/SELECT';
import CHECKBOX_CUSTOM from '../common/components/form/CHECKBOX_CUSTOM';

import TABLE from '../common/components/table/TABLE';
import THEAD from '../common/components/table/THEAD';
import TBODY from '../common/components/table/TBODY';
import TR from '../common/components/table/TR';
import TH from '../common/components/table/TH';
import TD from '../common/components/table/TD';
import PaginationContainer from '../common/containers/PaginationContainer'

import Loading from '../common/components/common/Loading';

class ListSettlement extends Component {

    constructor(props) {
        super(props);
        const stringParams = queryString.parse(props.location.search);
        this.state = {};
        if (!_.isEmpty(stringParams)) {
            this.state = stringParams;
        }
        this.props.reloadData(dataListUrl, stringParams);
    }

    handleChange = (event) => {
        const target = event.target;
        const value = target.type === 'checkbox' ? target.checked : target.value;
        const name = target.name;
        this.setState({
            [name]: (value)?value:''
        },() => {
            if (name === 'per_page' ||
                name === 'payment_check') {
                this.handleReloadData();
            }
        });
    }

    handleChangeMultiSelect = (name, value, checked) => {
        let currentValue = this.state[name];
        if (typeof currentValue === 'undefined') {
            currentValue = [];
        } else if (!Array.isArray(this.state[name])) {
            currentValue = [this.state[name]];
        }
        if (Array.isArray(value)) {
            currentValue = [...value];
        } else {
            let index = currentValue.indexOf(value);
            if (checked === false) {
                if (index !== -1) {
                    currentValue.splice(index, 1);
                }
            } else {
                if (index === -1) {
                    currentValue.push(value);
                }
            }
        }
        this.setState({
            [name]: currentValue,
        });
    }

     handleClearClick = (event) => {
        Object.keys(this.state).map((key) => {
            if (key !== 'per_page') {
                delete this.state[key];
            }
        });
        this.handleReloadData();
    }

    handleReloadData = () => {
        if (this.state.page) {
            delete this.state.page;
        }
        this.props.reloadData(dataListUrl, this.state, true);
    }

    handleClickPage = (page) => {
        this.setState({
            page: page
        },() => {
            this.props.reloadData(dataListUrl, this.state, true);
        });
    }

    handleCheckBoxies = (paymentCode, typeProcess,event) => {
        const value = event.target.checked;
        let params  = {
            payment_code : paymentCode,
            payment_checkbox : value?1:0,
        }
        switch (typeProcess) {
            case 1 :
                params.type = 'payment_request'
                break;
            case 2:
                params.type = 'have_para'
                break;
        }
        this.props.postData(changeStatusUrl, params, {'X-Requested-With': 'XMLHttpRequest'}, this.handleReloadData);
        //because we filter payment_request , so we must be reload data
        this.handleReloadData();
    }

    componentDidUpdate() {
        if (!_.isEmpty(this.props.dataSource)) {
            $('.loading').hide();
            $('.show-modal-content').customPopover();
        }
    }

    render() {
        let dataSource = this.props.dataSource;
        let index = 0;
        let paymentRequest = {
            0 : '無',
            1 : '有',
        };
        let totalItems = 0;
        let itemsPerPage = 20;
        if (!_.isEmpty(dataSource)) {
            index = dataSource.data.from - 1;
            totalItems   = dataSource.data.total;
            itemsPerPage = dataSource.data.per_page;
        }
        return ( (!_.isEmpty(dataSource)) &&
            <div>
                <Loading className="loading" />
                <div className="box">
                    <div className="box-header">
                        <div className="row">
                            <div className="col-md-12">
                                <h4>{trans('messages.search_title')}</h4>
                            </div>
                            <FORM>
                                <SELECT
                                    id="payment_check"
                                    name="payment_check"
                                    groupClass="form-group col-md-1-75"
                                    labelTitle={trans('messages.payment_check')}
                                    handleChangeMultiSelect={this.handleChangeMultiSelect}
                                    valueMulti={this.state.payment_check || ''}
                                    options={paymentRequest}
                                    multiple="multiple"
                                    className="form-control input-sm" />
                                <SELECT
                                    name="per_page"
                                    groupClass="form-group col-md-offset-9 col-md-1 per-page pull-right-md"
                                    labelClass="pull-left"
                                    labelTitle={trans('messages.per_page')}
                                    onChange={this.handleChange}
                                    value={this.state.per_page || ''}
                                    options={{20:20, 40:40, 60:60, 80:80, 100:100}}
                                    className="form-control input-sm" />
                            </FORM>
                        </div>
                        <div className="row">
                            <div className="col-md-9">
                                <a href={saveUrl} className="btn btn-success input-sm pull-left">{trans('messages.btn_add')}</a>
                            </div>
                            <div className="col-md-3">
                                    <BUTTON className="btn btn-danger pull-right" onClick={this.handleClearClick}>{trans('messages.btn_reset')}</BUTTON>
                                    <BUTTON className="btn btn-primary pull-right margin-element" onClick={this.handleReloadData} >{trans('messages.btn_search')}</BUTTON>
                            </div>
                        </div>
                    </div>
                    <div className="box-body">
                        <div className="dataTables_wrapper form-inline dt-bootstrap">
                            <div className="row">
                                <div className="col-sm-12">
                                    {(totalItems/itemsPerPage > 1) &&
                                    <div className="row">
                                        <div className="col-md-2-5"></div>
                                        <div className="col-md-7 text-center">
                                            <PaginationContainer handleClickPage={this.handleClickPage}/>
                                        </div>
                                        <div className="col-md-2-5 text-right line-height-md">
                                            {dataSource.data.from}件 - {dataSource.data.to}件 【全{dataSource.data.total}件】
                                        </div>
                                    </div>
                                    }
                                    <div className="table-responsive">
                                    <TABLE className="table table-striped table-custom">
                                        <THEAD>
                                            <TR>
                                                <TH className="hidden-sm text-center col-max-min-60">＃</TH>
                                                <TH className='hidden-sm text-center col-max-min-200'>{trans('messages.settlement_name')}</TH>
                                                <TH className="hidden-xs text-center col-max-min-300">{trans('messages.add_2_mail_text')}</TH>
                                                <TH className="hidden-sm text-center col-max-min-130">{trans('messages.add_2_receipt_text')}</TH>
                                                <TH className="hidden-sm text-center col-max-min-130">{trans('messages.payment_check')}</TH>
                                                <TH className="hidden-sm text-center col-max-min-130">{trans('messages.have_parameter')}</TH>
                                            </TR>
                                        </THEAD>
                                        <TBODY>
                                            {
                                                (!_.isEmpty(dataSource.data.data))?
                                                dataSource.data.data.map((item) => {
                                                    index++;
                                                    let classRow = (index%2 ===0) ? 'odd' : 'even';
                                                    return (
                                                        [<TR key={"tr" + index} className={classRow}>
                                                            <TD className="width-span1 hidden-sm text-center">
                                                                <a className="link-detail" href={saveUrl+'?payment_code='+ item.payment_code}><span>{index}</span></a></TD>
                                                            <TD className="hidden-sm">{item.payment_name}</TD>
                                                            <TD className="hidden-xslimit-char max-width-300" title={item.add_2_mail_text}>
                                                                <span className="cut-text limit-char max-width-300 show-modal-content"
                                                                data-content={ item.add_2_mail_text !== null ? item.add_2_mail_text.replace(/(?:\r\n|\r|\n)/g, '<br>') : ''}
                                                                data-toggle="popover"
                                                                data-trigger="hover">
                                                                    {item.add_2_mail_text}
                                                                </span>
                                                            </TD>
                                                             <TD className="hidden-xs limit-char max-width-300" title={item.add_2_receipt_text}>
                                                                <span className="cut-text limit-char max-width-300 show-modal-content"
                                                                data-content={ item.add_2_receipt_text !== null ? item.add_2_receipt_text.replace(/(?:\r\n|\r|\n)/g, '<br>') : ''}
                                                                data-toggle="popover"
                                                                data-trigger="hover">
                                                                    {item.add_2_receipt_text}
                                                                </span>
                                                            </TD>
                                                            <TD className="hidden-sm text-center">
                                                                <CHECKBOX_CUSTOM
                                                                defaultChecked = {item.payment_request}
                                                                key={'payment_request'+'_'+item.payment_code + Math.random()}
                                                                name = {'payment_request'+'_'+item.payment_code}
                                                                onChange={(e) => this.handleCheckBoxies(item.payment_code, 1, e)}
                                                                value={item.payment_request} />
                                                            </TD>
                                                            <TD className="hidden-sm text-center">
                                                                <CHECKBOX_CUSTOM
                                                                defaultChecked = {item.have_para}
                                                                key={'have_para'+'_'+item.payment_code + Math.random()}
                                                                name = {'have_para'+'_'+item.payment_code}
                                                                onChange={(e) => this.handleCheckBoxies(item.payment_code, 2, e)}
                                                                value={item.have_para} />
                                                            </TD>
                                                        </TR>,
                                                        <TR key={"trhide" + index} className="hiden-info" style={{display:"none"}}>
                                                            <TD colSpan="5" className="width-span1 visible-xs visible-sm">
                                                                <ul>
                                                                <li> <b>NO</b> : #{index}</li>
                                                                    <li>
                                                                        <b>{trans('messages.settlement_name')} : </b>
                                                                        <p>{item.payment_name}</p>
                                                                    </li>
                                                                    <li>
                                                                        <b>{trans('messages.payment_check')} : </b>
                                                                        <p>{item.sub_status_name}</p>
                                                                    </li>
                                                                    <li>
                                                                        <b>{trans('messages.have_parameter')} : </b>
                                                                        <p>{item.template_name}</p>
                                                                    </li>
                                                                </ul>
                                                            </TD>
                                                        </TR>
                                                        ]
                                                    )
                                                }):
                                                <TR><TD colSpan="9" className="text-center">{trans('messages.no_data')}</TD></TR>
                                            }
                                        </TBODY>
                                    </TABLE>
                                    </div>
                                    <PaginationContainer handleClickPage={this.handleClickPage}/>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        dataSource: state.commonReducer.dataSource,
        dataResponse: state.commonReducer.dataResponse
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        reloadData: (url, params, pushHistory, ignoreFields) => { dispatch(Action.reloadData(url, params, pushHistory, ignoreFields)) },
        postData: (url, params, headers,callback,callParam) => { dispatch(Action.postData(url, params, headers,callback,callParam)) }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(ListSettlement)