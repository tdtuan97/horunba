import React, { Component } from 'react';

import { connect } from 'react-redux';
import * as Action from '../common/actions';
import * as queryString from 'query-string';
import axios from 'axios';

import BUTTON from '../common/components/form/BUTTON';
import FORM from '../common/components/form/FORM';
import INPUT from '../common/components/form/INPUT';
import SELECT from '../common/components/form/SELECT';

import TABLE from '../common/components/table/TABLE';
import THEAD from '../common/components/table/THEAD';
import TBODY from '../common/components/table/TBODY';
import TR from '../common/components/table/TR';
import TH from '../common/components/table/TH';
import TD from '../common/components/table/TD';

import SortComponent from '../common/components/table/SortComponent';
import PaginationContainer from '../common/containers/PaginationContainer'
import Loading from '../common/components/common/Loading';

import SavePopup from './SavePopup';

class GenreList extends Component {

    constructor(props) {
        super(props);
        const stringParams = queryString.parse(props.location.search);
        this.state = {};
        if (!_.isEmpty(stringParams)) {
            this.state = stringParams;
        }
        this.props.reloadData(dataListUrl, this.state);
    }

    handleChange = (event) => {
        const target = event.target;
        const value = target.type === 'checkbox' ? target.checked : target.value;
        const name = target.name;
        this.setState({
            [name]: (value)?value:undefined
        },() => {
            if (name === 'per_page') {
                this.handleReloadData();
            }
        });
    };

    handleClearClick = (event) => {
        Object.keys(this.state).map((key) => {
            if (key !== 'per_page') {
                delete this.state[key];
            }
        });
        this.handleReloadData();
    };

    handleReloadData = () => {
        if (this.state.page) {
            delete this.state.page;
        }
        this.props.reloadData(dataListUrl, this.state, true);
    };

    componentDidUpdate() {
        if (!_.isEmpty(this.props.dataSource)) {
            $('.loading').hide();
            $('.show-modal-content').customPopover();
        }
    };

    componentWillMount() {
        window.onpopstate = (event) => {
            location.reload();
        };
    };

    handleSortClick = (name, nextSort) => {
        Object.keys(this.state).map((key) => {
            if (key.startsWith('sort_')) {
                delete this.state[key];
            }
        });
        if (nextSort === 'none') {
            delete this.state[name];
            this.props.reloadData(dataListUrl, this.state, true);
        } else {
            this.setState({[name]: nextSort}, () => {
                this.props.reloadData(dataListUrl, this.state, true);
            });
        }
    };

    handleClickPage = (page) => {
        this.setState({
            page: page
        },() => {
            this.props.reloadData(dataListUrl, this.state, true);
        });
    };

    handleSaveClick = (method, genreId, genreName, remarks) => {
        this.setState({
            cur_method: method,
            cur_genre_id: genreId,
            cur_genre_name: genreName,
            cur_remarks: remarks
        });
        $('#save-genre-modal').modal({backdrop: 'static', show: true});
    };

    handleDeleteClick = (genreId) => {
        $(".loading").show();
        axios.post(deleteUrl, {genre_id: genreId}, {headers: {'X-Requested-With': 'XMLHttpRequest'}}
        ).then(response => {
            if (response.data.status === 0) {
                let message = response.data.message;
                $.notify({
                    icon: 'glyphicon glyphicon-warning-sign',
                    message: message,
                },{
                    type: 'danger',
                    delay: 2000
                });
            } else {
                this.handleReloadData();
            }
            $(".loading").hide();
        }).catch(error => {
            $(".loading").hide();
            throw(error);
        });
    };

    render() {
        let dataSource = this.props.dataSource;
        let index = 0;
        let totalItems = 0;
        let itemsPerPage = 20;
        let genreOption = '';
        if (!_.isEmpty(dataSource)) {
            index = dataSource.data.from - 1;
            totalItems   = dataSource.data.total;
            itemsPerPage = dataSource.data.per_page;
            genreOption  = dataSource.genre;
        }
        return ( (!_.isEmpty(dataSource)) &&
            <div>
                <Loading className="loading" />
                <div className="box">
                    <div className="box-header">
                        <div className="row">
                                <div className="col-md-12">
                                    <h4>{trans('messages.search_title')}</h4>
                                </div>
                                <FORM>
                                    <INPUT
                                        name="genre_name"
                                        groupClass="form-group col-md-2"
                                        labelTitle={trans('messages.genre_name')}
                                        onChange={this.handleChange}
                                        value={this.state.genre_name||''}
                                        className="form-control input-sm" />
                                    <INPUT
                                        name="remarks"
                                        groupClass="form-group col-md-2"
                                        labelTitle={trans('messages.genre_remarks')}
                                        onChange={this.handleChange}
                                        value={this.state.remarks||''}
                                        className="form-control input-sm" />
                                    <SELECT
                                        name="per_page"
                                        groupClass="form-group col-md-1 per-page pull-right-md"
                                        labelClass="pull-right"
                                        labelTitle={trans('messages.per_page')}
                                        onChange={this.handleChange}
                                        value={this.state.per_page||''}
                                        options={{20:20, 40:40, 60:60, 80:80, 100:100}}
                                        className="form-control input-sm" />
                                </FORM>
                        </div>
                        <div className="row">
                            <div className="col-md-9 col-xs-4">
                                <BUTTON className="btn btn-success input-sm" onClick={() => {this.handleSaveClick('add', '', '', '')}}>{trans('messages.btn_add')}</BUTTON>
                            </div>
                            <div className="col-md-3 col-xs-8">
                                <BUTTON className="btn btn-danger pull-right" onClick={this.handleClearClick}>{trans('messages.btn_reset')}</BUTTON>
                                <BUTTON className="btn btn-primary pull-right margin-element" onClick={this.handleReloadData} >{trans('messages.btn_search')}</BUTTON>
                            </div>
                        </div>
                    </div>
                    <div className="box-body">
                        <div className="row">
                            <div className="col-md-12">
                                {(totalItems/itemsPerPage > 1) &&
                                <div className="row">
                                    <div className="col-md-2-5"></div>
                                    <div className="col-md-7 text-center">
                                        <PaginationContainer handleClickPage={this.handleClickPage}/>
                                    </div>
                                    <div className="col-md-2-5 text-right line-height-md">
                                        {dataSource.data.from}件 - {dataSource.data.to}件 【全{dataSource.data.total}件】
                                    </div>
                                </div>
                                }
                                <div className="table-responsive">
                                <TABLE className="table table-striped table-custom">
                                    <THEAD>
                                        <TR>
                                            <TH className="text-center">#</TH>
                                            <TH className="text-center">
                                                <SortComponent
                                                    name="sort_genre_name"
                                                    onClick={this.handleSortClick}
                                                    currentSort={(this.state.sort_genre_name)?this.state.sort_genre_name:'none'}
                                                    title={trans('messages.genre_name')} />
                                            </TH>
                                            <TH className="text-center">
                                                <SortComponent
                                                    name="sort_remarks"
                                                    onClick={this.handleSortClick}
                                                    currentSort={(this.state.sort_remarks)?this.state.sort_remarks:'none'}
                                                    title={trans('messages.genre_remarks')} />
                                            </TH>
                                            <TH className="text-center">{trans('messages.genre_edit')}</TH>
                                            <TH className="text-center">{trans('messages.genre_delete')}</TH>
                                        </TR>
                                    </THEAD>
                                    <TBODY>
                                        {
                                            (!_.isEmpty(dataSource.data.data))?
                                            dataSource.data.data.map((item) => {
                                                index++;
                                                let classRow = (index%2 ===0) ? 'odd' : 'even';
                                                let disRow  = '';
                                                let disEdit = false;
                                                let disDel  = false;
                                                if (item.del_flg === 1) {
                                                    disRow  = ' deleted_row';
                                                    disEdit = true;
                                                    disDel  = true;
                                                }
                                                return (
                                                    <TR key={"tr" + index} className={classRow + disRow}>
                                                        <TD className="text-center">{index}</TD>
                                                        <TD title={item.genre_name}>
                                                            {item.genre_name}
                                                        </TD>
                                                        <TD title={item.remarks}>
                                                            {item.remarks}
                                                        </TD>
                                                        <TD className="text-center">
                                                            <BUTTON disabled={disEdit} className="btn btn-primary input-sm" onClick={() => {this.handleSaveClick('edit', item.genre_id, item.genre_name, item.remarks)}}>{trans('messages.genre_edit')}</BUTTON>
                                                        </TD>
                                                        <TD className="text-center">
                                                            <BUTTON disabled={disDel} className="btn btn-danger input-sm" onClick={() => {this.handleDeleteClick(item.genre_id)}}>{trans('messages.genre_delete')}</BUTTON>
                                                        </TD>
                                                    </TR>
                                                )
                                            })
                                            :
                                            <TR><TD colSpan="9" className="text-center">{trans('messages.no_data')}</TD></TR>
                                        }
                                    </TBODY>
                                </TABLE>
                                </div>
                                <div className="text-center">
                                    <PaginationContainer handleClickPage={this.handleClickPage}/>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <SavePopup
                    method={this.state.cur_method}
                    genre_id={this.state.cur_genre_id}
                    genre_name={this.state.cur_genre_name}
                    remarks={this.state.cur_remarks}
                    handleReloadData={this.handleReloadData}
                />
            </div>

        )
    }
}

const mapStateToProps = (state) => {
    return {
        dataSource: state.commonReducer.dataSource
    }
};

const mapDispatchToProps = (dispatch) => {
    let ignore = ['cur_method', 'cur_genre_id', 'cur_genre_name','cur_remarks'];
    return {
        reloadData: (url, params, pushHistory, ignoreFields) => { dispatch(Action.reloadData(url, params, pushHistory, ignore)) }
    }
};
export default connect(mapStateToProps, mapDispatchToProps)(GenreList)