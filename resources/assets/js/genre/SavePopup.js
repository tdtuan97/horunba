import React, { Component } from 'react';
import Loading from '../common/components/common/Loading';
import INPUT from '../common/components/form/INPUT';
import TEXTAREA from '../common/components/form/TEXTAREA';

import axios from 'axios';

export default class SavePopup extends Component {
    constructor(props) {
        super(props);
        this.state = {
            method: this.props.method,
            genre_id: this.props.genre_id,
            genre_name: this.props.genre_name,
            remarks: this.props.remarks
        }
    }

    componentWillReceiveProps(nextProps) {
        this.setState({
            method: nextProps.method,
            genre_id: nextProps.genre_id,
            genre_name: nextProps.genre_name,
            remarks: nextProps.remarks,
        });
    }

    handleChange = (event) => {
        const target = event.target;
        const name = target.name;
        const value = target.value;
        this.setState({
            [name]: value
        });
    }

    handleSaveClick = () => {
        let params = {};
        params['method']     = this.state.method;
        params['genre_id']   = this.state.genre_id;
        params['genre_name'] = this.state.genre_name;
        params['remarks']    = this.state.remarks;

        $(".loading").show();
        axios.post(saveUrl, params, {headers: {'X-Requested-With': 'XMLHttpRequest'}}
        ).then(response => {
            if (response.data.status === 0) {
                let message = response.data.message;
                this.setState({
                    genre_id_error: (message.genre_id)?message.genre_id:'',
                    genre_name_error: (message.genre_name)?message.genre_name:''
                });
            } else {
                this.handleCancelClick();
                this.props.handleReloadData();
            }
            $(".loading").hide();
        }).catch(error => {
            $(".loading").hide();
            throw(error);
        });
    }

    handleCancelClick = () => {
        this.setState({
            method: '',
            genre_id: '',
            genre_name: '',
            remarks: '',
            genre_id_error: '',
            genre_name_error: ''
        });
        $('#save-genre-modal').modal('hide');
    }

    render() {
        let title = trans('messages.genre_add_popup_title');
        if (this.state.method === 'edit') {
            title = trans('messages.genre_edit_popup_title');
        }
        return (
            <div className="modal fade" id="save-genre-modal">
                <Loading className="loading" />
                <div className="modal-dialog ">
                    <div className="modal-content">
                        <div className="modal-header">
                        {title}
                        </div>
                        <div className="modal-body modal-custom-body">
                        <form className="form-horizontal">
                            <div className="row">
                                <INPUT
                                    name="genre_name"
                                    labelTitle={trans('messages.genre_name')}
                                    className="form-control input-sm"
                                    fieldGroupClass="col-md-9"
                                    labelClass="col-md-3"
                                    groupClass="col-md-12 margin-form"
                                    errorPopup={true}
                                    value={this.state.genre_name||''}
                                    onChange={this.handleChange}
                                    hasError={(this.state.genre_name_error)?true:false}
                                    errorMessage={(this.state.genre_name_error)?this.state.genre_name_error:""}
                                    />
                            </div>
                            <div className="row">
                                <TEXTAREA
                                    name="remarks"
                                    labelTitle={trans('messages.genre_remarks')}
                                    className="form-control input-sm"
                                    fieldGroupClass="col-md-9"
                                    labelClass="col-md-3"
                                    groupClass="col-md-12 margin-form"
                                    errorPopup={true}
                                    value={this.state.remarks||''}
                                    onChange={this.handleChange}
                                    hasError={(this.state.remarks_error)?true:false}
                                    errorMessage={(this.state.remarks_error)?this.state.remarks_error:""}
                                    />
                            </div>
                        </form>
                        </div>
                        <div className="modal-footer modal-custom-footer">
                            <button type="button" className="btn btn-primary btn-sm modal-custom-button" onClick={this.handleSaveClick}>{trans('messages.btn_accept')}</button>
                            <button type="button" className="btn bg-purple btn-sm modal-custom-button" onClick={this.handleCancelClick}>{trans('messages.btn_cancel')}</button>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}