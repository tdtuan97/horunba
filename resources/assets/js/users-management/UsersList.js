import React, { Component } from 'react';

import { connect } from 'react-redux';
import * as Action from '../common/actions';
import * as queryString from 'query-string';

import BUTTON from '../common/components/form/BUTTON';
import FORM from '../common/components/form/FORM';
import INPUT from '../common/components/form/INPUT';
import SELECT from '../common/components/form/SELECT';
import CHECKBOX_CUSTOM from '../common/components/form/CHECKBOX_CUSTOM';

import TABLE from '../common/components/table/TABLE';
import THEAD from '../common/components/table/THEAD';
import TBODY from '../common/components/table/TBODY';
import TR from '../common/components/table/TR';
import TH from '../common/components/table/TH';
import TD from '../common/components/table/TD';

import SortComponent from '../common/components/table/SortComponent';
import PaginationContainer from '../common/containers/PaginationContainer'
import Loading from '../common/components/common/Loading';

class UsersList extends Component {

    constructor(props) {
        super(props);
        const stringParams = queryString.parse(props.location.search);
        this.state = {};
        if (!_.isEmpty(stringParams)) {
            this.state = stringParams;
        }
        this.props.reloadData(dataListUrl, this.state);
    }

    handleChange = (event) => {
        const target = event.target;
        const value = target.type === 'checkbox' ? target.checked : target.value;
        const name = target.name;
        this.setState({
            [name]: (value)?value:undefined
        },() => {
            if (name === 'per_page' || name === 'tantou_department' || name === 'tantou_role') {
                this.handleReloadData();
            }
        });
    }

    handleChangeMultiSelect = (name, value, checked) => {
        let currentValue = this.state[name];
        if (typeof currentValue === 'undefined') {
            currentValue = [];
        } else if (!Array.isArray(this.state[name])) {
            currentValue = [this.state[name]];
        }
        if (Array.isArray(value)) {
            currentValue = [...value];
        } else {
            let index = currentValue.indexOf(value);
            if (checked === false) {
                if (index !== -1) {
                    currentValue.splice(index, 1);
                }
            } else {
                if (index === -1) {
                    currentValue.push(value);
                }
            }
        }
        this.setState({
            [name]: currentValue,
        });
    }

    handleClearClick = (event) => {
        Object.keys(this.state).map((key) => {
            if (key !== 'per_page') {
                delete this.state[key];
            }
        });
        this.handleReloadData();
    }

    handleReloadData = () => {
        if (this.state.page) {
            delete this.state.page;
        }
        this.props.reloadData(dataListUrl, this.state, true);
    }

    componentDidUpdate() {
        if (!_.isEmpty(this.props.dataSource)) {
            $('.loading').hide();
        }
    }

    componentWillMount() {
        window.onpopstate = (event) => {
            location.reload();
        };
    }

    handleSortClick = (name, nextSort) => {
        Object.keys(this.state).map((key) => {
            if (key.startsWith('sort_')) {
                delete this.state[key];
            }
        });
        if (nextSort === 'none') {
            delete this.state[name];
            this.props.reloadData(dataListUrl, this.state, true);
        } else {
            this.setState({[name]: nextSort}, () => {
                this.props.reloadData(dataListUrl, this.state, true);
            });
        }

    }

    handleClickPage = (page) => {
        this.setState({
            page: page
        },() => {
            this.props.reloadData(dataListUrl, this.state, true);
        });
    }

    handleIsActiveChange = (tantouCode, event) => {
        const value = event.target.checked;
        let params  = {
            tantou_code : tantouCode,
            is_active : value?1:0
        }
        this.props.postData(changeActiveUrl, params, {'X-Requested-With': 'XMLHttpRequest'});
    }

    render() {
        let dataSource = this.props.dataSource;
        let index = 0;
        let totalItems = 0;
        let itemsPerPage = 20;
        if (!_.isEmpty(dataSource)) {
            index = dataSource.data.from - 1;
            totalItems   = dataSource.data.total;
            itemsPerPage = dataSource.data.per_page;
        }

        return ( (!_.isEmpty(dataSource)) &&
            <div>
                <Loading className="loading" />
                <div className="box">
                    <div className="box-header">
                        <div className="row">
                                <div className="col-md-12">
                                    <h4>{trans('messages.search_title')}</h4>
                                </div>
                                <FORM>
                                    <INPUT
                                        name="tantou_code"
                                        groupClass="form-group col-md-1-75"
                                        labelTitle={trans('messages.tantou_code')}
                                        onChange={this.handleChange}
                                        value={this.state.tantou_code||''}
                                        className="form-control input-sm" />
                                    <INPUT
                                        name="tantou_last_name"
                                        groupClass="form-group col-md-2"
                                        labelTitle={trans('messages.tantou_last_name')}
                                        onChange={this.handleChange}
                                        value={this.state.tantou_last_name||''}
                                        className="form-control input-sm" />
                                    <INPUT
                                        name="tantou_first_name"
                                        groupClass="form-group col-md-2"
                                        labelTitle={trans('messages.tantou_first_name')}
                                        onChange={this.handleChange}
                                        value={this.state.tantou_first_name||''}
                                        className="form-control input-sm" />
                                    <SELECT
                                        id="tantou_department"
                                        name="tantou_department"
                                        groupClass="form-group col-md-2"
                                        labelTitle={trans('messages.tantou_department')}
                                        handleChangeMultiSelect={this.handleChangeMultiSelect}
                                        valueMulti={this.state.tantou_department||''}
                                        options={dataSource.departmentOpt}
                                        multiple="multiple"
                                        className="form-control input-sm" />
                                    <SELECT
                                        id="tantou_role"
                                        name="tantou_role"
                                        groupClass="form-group col-md-2"
                                        labelTitle={trans('messages.tantou_role')}
                                        handleChangeMultiSelect={this.handleChangeMultiSelect}
                                        valueMulti={this.state.tantou_role||''}
                                        options={dataSource.roleOpt}
                                        multiple="multiple"
                                        className="form-control input-sm" />

                                    <SELECT
                                        name="per_page"
                                        groupClass="form-group col-md-1 col-md-offset-1 per-page pull-right-md"
                                        labelClass="pull-right"
                                        labelTitle={trans('messages.per_page')}
                                        onChange={this.handleChange}
                                        value={this.state.per_page||''}
                                        options={{20:20, 40:40, 60:60, 80:80, 100:100}}
                                        className="form-control input-sm" />
                                </FORM>
                        </div>
                        <div className="row">
                            <div className="col-md-9 col-xs-4">
                                <a href={saveUrl} className="btn btn-success input-sm">{trans('messages.btn_add')}</a>
                            </div>
                            <div className="col-md-3 col-xs-8">
                                <BUTTON className="btn btn-danger pull-right" onClick={this.handleClearClick}>{trans('messages.btn_reset')}</BUTTON>
                                <BUTTON className="btn btn-primary pull-right margin-element" onClick={this.handleReloadData} >{trans('messages.btn_search')}</BUTTON>
                            </div>
                        </div>
                    </div>
                    <div className="box-body">
                        <div className="row">
                            <div className="col-md-12">
                                {(totalItems/itemsPerPage > 1) &&
                                <div className="row">
                                    <div className="col-md-2-5"></div>
                                    <div className="col-md-7 text-center">
                                        <PaginationContainer handleClickPage={this.handleClickPage}/>
                                    </div>
                                    <div className="col-md-2-5 text-right line-height-md">
                                        {dataSource.data.from}件 - {dataSource.data.to}件 【全{dataSource.data.total}件】
                                    </div>
                                </div>
                                }
                                <TABLE className="table table-striped table-custom">
                                    <THEAD>
                                        <TR>
                                            <TH className="visible-xs visible-sm text-center"></TH>
                                            <TH className="text-center">#</TH>
                                            <TH className="text-center">
                                                <SortComponent
                                                    name="sort_tantou_code"
                                                    onClick={this.handleSortClick}
                                                    currentSort={(this.state.sort_tantou_code)?this.state.sort_tantou_code:'none'}
                                                    title={trans('messages.tantou_code')} />
                                            </TH>
                                            <TH className="hidden-xs hidden-sm text-center">
                                                <SortComponent
                                                    name="sort_tantou_last_name"
                                                    onClick={this.handleSortClick}
                                                    currentSort={(this.state.sort_tantou_last_name)?this.state.sort_tantou_last_name:'none'}
                                                    title={trans('messages.tantou_last_name')} />
                                            </TH>
                                            <TH className="hidden-xs hidden-sm text-center">
                                                <SortComponent
                                                    name="sort_tantou_first_name"
                                                    onClick={this.handleSortClick}
                                                    currentSort={(this.state.sort_tantou_first_name)?this.state.sort_tantou_first_name:'none'}
                                                    title={trans('messages.tantou_first_name')} />
                                            </TH>
                                            <TH className="hidden-xs hidden-sm text-center">
                                                <SortComponent
                                                    name="sort_tantou_department"
                                                    onClick={this.handleSortClick}
                                                    currentSort={(this.state.sort_tantou_department)?this.state.sort_tantou_department:'none'}
                                                    title={trans('messages.tantou_department')} />
                                            </TH>
                                            <TH className="hidden-xs hidden-sm text-center">
                                                <SortComponent
                                                    name="sort_tantou_tel"
                                                    onClick={this.handleSortClick}
                                                    currentSort={(this.state.sort_tantou_tel)?this.state.sort_tantou_tel:'none'}
                                                    title={trans('messages.tantou_tel')} />
                                            </TH>
                                            <TH className="hidden-xs hidden-sm text-center">
                                                <SortComponent
                                                    name="sort_tantou_mail"
                                                    onClick={this.handleSortClick}
                                                    currentSort={(this.state.sort_tantou_mail)?this.state.sort_tantou_mail:'none'}
                                                    title={trans('messages.tantou_mail')} />
                                            </TH>
                                            <TH className="hidden-xs hidden-sm text-center">
                                                <SortComponent
                                                    name="sort_tantou_role"
                                                    onClick={this.handleSortClick}
                                                    currentSort={(this.state.sort_tantou_role)?this.state.sort_tantou_role:'none'}
                                                    title={trans('messages.tantou_role')} />
                                            </TH>
                                            <TH className="text-center">
                                                {trans('messages.is_active')}
                                            </TH>
                                        </TR>
                                    </THEAD>
                                    <TBODY>
                                    {
                                        (!_.isEmpty(dataSource.data.data))?
                                        dataSource.data.data.map((item) => {
                                            index++;
                                            let classRow = (index%2 ===0) ? 'odd' : 'even';

                                            let tantouRole = dataSource.tantouRole[item.tantou_role] != "undefined" ? dataSource.tantouRole[item.tantou_role] : '';
                                            return (
                                                [
                                                <TR key={"tr" + index} className={classRow}>
                                                    <TD className="visible-xs visible-sm text-center">
                                                        <b className="show-info">
                                                            <i className="fa fa-angle-double-down" aria-hidden="true"></i>
                                                        </b>
                                                    </TD>
                                                    <TD className="text-center">{index}</TD>
                                                    <TD>
                                                        <a className="link-detail" href={saveUrl + "?tantou_code=" + item.tantou_code}>
                                                            {item.tantou_code}
                                                        </a>
                                                    </TD>
                                                    <TD className="hidden-xs hidden-sm cut-text"  title={item.tantou_last_name}>
                                                        <span className="limit-char max-width-100">{item.tantou_last_name}</span>
                                                    </TD>
                                                    <TD className="hidden-xs hidden-sm cut-text" title={item.tantou_first_name}>
                                                        <span className="limit-char max-width-100">{item.tantou_first_name}</span>
                                                    </TD>
                                                    <TD className="hidden-xs hidden-sm cut-text" title={item.depart_nm ? item.depart_nm : item.tantou_department}>
                                                        <span className="limit-char max-width-100">{item.depart_nm ? item.depart_nm : item.tantou_department}</span>
                                                    </TD>
                                                    <TD className="hidden-xs hidden-sm cut-text" title={item.tantou_tel}>
                                                        <span className="limit-char max-width-100">{item.tantou_tel}</span>
                                                    </TD>
                                                    <TD className="hidden-xs hidden-sm cut-text" title={item.tantou_mail}>
                                                        <span className="limit-char max-width-150">{item.tantou_mail}</span>
                                                    </TD>
                                                    <TD className="hidden-xs hidden-sm cut-text" title={tantouRole}>
                                                        <span className="limit-char max-width-100">{tantouRole}</span>
                                                    </TD>
                                                    <TD className="text-center">
                                                        <CHECKBOX_CUSTOM
                                                            key={item.tantou_code + Math.random()}
                                                            defaultChecked={item.is_active}
                                                            name={'is_active[' + item.tantou_code + ']'}
                                                            onChange={(e) => this.handleIsActiveChange(item.tantou_code, e)}
                                                            value={item.is_active} />
                                                    </TD>
                                                </TR>,
                                                <TR key={"trhide" + index} className="hiden-info" style={{display:"none"}}>
                                                    <TD colSpan="4" className="visible-xs visible-sm width-span1">
                                                        <ul id={"temp" + item.reason_id}>
                                                            <li> <b>#</b> : {index}</li>
                                                            <li>
                                                                <b>{trans('messages.tantou_code')} : </b>
                                                                {item.tantou_code}
                                                            </li>
                                                            <li>
                                                                <b>{trans('messages.tantou_last_name')} : </b>
                                                                {item.tantou_last_name}
                                                            </li>
                                                            <li>
                                                                <b>{trans('messages.tantou_first_name')} : </b>
                                                                {item.tantou_first_name}
                                                            </li>
                                                            <li>
                                                                <b>{trans('messages.tantou_department')} : </b>
                                                                {item.tantou_department}
                                                            </li>
                                                            <li>
                                                                <b>{trans('messages.tantou_tel')} : </b>
                                                                {item.tantou_tel}
                                                            </li>
                                                            <li>
                                                                <b>{trans('messages.tantou_mail')} : </b>
                                                                {item.tantou_mail}
                                                            </li>
                                                            <li>
                                                                <b>{trans('messages.tantou_role')} : </b>
                                                                {tantouRole}
                                                            </li>
                                                        </ul>
                                                    </TD>
                                                </TR>
                                               ]
                                            )
                                        })
                                        :
                                        <TR><TD colSpan="9" className="text-center">{trans('messages.no_data')}</TD></TR>
                                    }
                                    </TBODY>
                                </TABLE>
                                <div className="text-center">
                                    <PaginationContainer handleClickPage={this.handleClickPage}/>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        dataSource: state.commonReducer.dataSource,
        dataResponse: state.commonReducer.dataResponse
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        reloadData: (url, params, pushHistory, ignoreFields) => { dispatch(Action.reloadData(url, params, pushHistory, ignoreFields)) },
        postData: (url, params, headers) => { dispatch(Action.postData(url, params, headers)) }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(UsersList)