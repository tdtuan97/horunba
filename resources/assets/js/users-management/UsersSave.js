import React, { Component } from 'react';
import { connect } from 'react-redux';

import * as Action from '../common/actions';
import * as queryString from 'query-string';

import Loading from '../common/components/common/Loading';
import FORM from '../common/components/form/FORM';
import INPUT from '../common/components/form/INPUT';
import SELECT from '../common/components/form/SELECT';
import BUTTON from '../common/components/form/BUTTON';
import CHECKBOX_CUSTOM from '../common/components/form/CHECKBOX_CUSTOM';

class UsersSave extends Component {
    constructor(props) {
        super(props);
        this.state = {}
        let params = queryString.parse(props.location.search);
        if (params['tantou_code']) {
            this.state = {tantou_code: params['tantou_code']};
        }
        this.props.reloadData(getFormDataUrl, this.state);
    }

    handleCancel = (event) => {
        window.location.href = baseUrl;
    }

    handleSubmit = (event) => {
        document.getElementsByName('btnAccept')[0].disabled = true;
        let headers = {'X-Requested-With': 'XMLHttpRequest'};

        let formData   = new FormData();
        let dataSource = this.props.dataSource;
        formData.append('is_active', 1);
        if ((!_.isEmpty(dataSource.data))) {
            Object.keys(dataSource.data).map((item) => {
                formData.append(item, dataSource.data[item]);
            });
        }
        if (!_.isEmpty(this.state)) {
            Object.keys(this.state).map((item) => {
                formData.append(item, this.state[item]);
            });
        }

        this.props.postData(saveUrl, formData, headers);
    }

    handleChange = (event) => {
        const target = event.target;
        const name = target.name;
        const value = target.type === 'checkbox' ? (target.checked?1:0) : target.value;
        this.setState({
            [name]: value
        });
    }

    componentWillReceiveProps(nextProps) {
        if (!_.isEmpty(nextProps.dataSource)) {
            if (!nextProps.dataSource.data) {
                window.location.href = previous;
            }
        }

        if (!_.isEmpty(nextProps.dataResponse)) {
            if (nextProps.dataResponse.status === 1) {
                window.location.href = previous;
            } else {
                document.getElementsByName('btnAccept')[0].disabled = false;
            }
        }
    }

    componentDidUpdate() {
        if (!_.isEmpty(this.props.dataSource)) {
            $('.loading').hide();
        }
    }
    render() {
        let dataSource   = this.props.dataSource;
        let dataResponse = this.props.dataResponse;

        return ((!_.isEmpty(dataSource)) &&
        <div className="box box-primary">
            <FORM
                className="form-horizontal "
                autoComplete="on">
                <div className="box-body">
                <Loading className="loading" />
                <div className="row">
                    <div className="col-md-6 col-md-offset-3 col-md-offset-right-3">
                        <INPUT
                            name="tantou_code"
                            labelTitle={trans('messages.tantou_code')}
                            className="form-control input-sm"
                            fieldGroupClass="col-md-8"
                            labelClass="col-md-4"
                            onChange={this.handleChange}
                            defaultValue={dataSource.data.tantou_code}
                            errorPopup={true}
                            hasError={(dataResponse.message && dataResponse.message.tantou_code)?true:false}
                            errorMessage={(dataResponse.message&&dataResponse.message.tantou_code)?dataResponse.message.tantou_code:""}
                            disabled/>
                        <INPUT
                            name="tantou_last_name"
                            labelTitle={trans('messages.tantou_last_name')}
                            className="form-control input-sm"
                            fieldGroupClass="col-md-8"
                            labelClass="col-md-4"
                            onChange={this.handleChange}
                            defaultValue={dataSource.data.tantou_last_name}
                            errorPopup={true}
                            hasError={(dataResponse.message && dataResponse.message.tantou_last_name)?true:false}
                            errorMessage={(dataResponse.message&&dataResponse.message.tantou_last_name)?dataResponse.message.tantou_last_name:""}/>
                        <INPUT
                            name="tantou_first_name"
                            labelTitle={trans('messages.tantou_first_name')}
                            className="form-control input-sm"
                            fieldGroupClass="col-md-8"
                            labelClass="col-md-4"
                            onChange={this.handleChange}
                            defaultValue={dataSource.data.tantou_first_name}
                            errorPopup={true}
                            hasError={(dataResponse.message && dataResponse.message.tantou_first_name)?true:false}
                            errorMessage={(dataResponse.message&&dataResponse.message.tantou_first_name)?dataResponse.message.tantou_first_name:""}/>
                        <INPUT
                            name="tantou_first_name"
                            labelTitle={trans('messages.tantou_first_name')}
                            className="form-control input-sm"
                            fieldGroupClass="col-md-8"
                            labelClass="col-md-4"
                            onChange={this.handleChange}
                            defaultValue={dataSource.data.tantou_first_name}
                            errorPopup={true}
                            hasError={(dataResponse.message && dataResponse.message.tantou_first_name)?true:false}
                            errorMessage={(dataResponse.message&&dataResponse.message.tantou_first_name)?dataResponse.message.tantou_first_name:""}/>
                        <SELECT
                            name="tantou_department"
                            labelTitle={trans('messages.tantou_department')}
                            className="form-control input-sm"
                            fieldGroupClass="col-md-8"
                            labelClass="col-md-4"
                            onChange={this.handleChange}
                            defaultValue={dataSource.data.tantou_department}
                            options={dataSource.departOpt}
                            errorPopup={true}
                            hasError={(dataResponse.message && dataResponse.message.tantou_department)?true:false}
                            errorMessage={(dataResponse.message&&dataResponse.message.tantou_department)?dataResponse.message.tantou_department:""}/>
                        <SELECT
                            name="tantou_role"
                            labelTitle={trans('messages.tantou_role')}
                            className="form-control input-sm"
                            fieldGroupClass="col-md-8"
                            labelClass="col-md-4"
                            onChange={this.handleChange}
                            defaultValue={dataSource.data.tantou_role}
                            options={dataSource.roleOpt}
                            errorPopup={true}
                            hasError={(dataResponse.message && dataResponse.message.tantou_role)?true:false}
                            errorMessage={(dataResponse.message&&dataResponse.message.tantou_role)?dataResponse.message.tantou_role:""}/>
                        <INPUT
                            name="tantou_tel"
                            labelTitle={trans('messages.tantou_tel')}
                            className="form-control input-sm"
                            fieldGroupClass="col-md-8"
                            labelClass="col-md-4"
                            onChange={this.handleChange}
                            defaultValue={dataSource.data.tantou_tel}
                            errorPopup={true}
                            hasError={(dataResponse.message && dataResponse.message.tantou_tel)?true:false}
                            errorMessage={(dataResponse.message&&dataResponse.message.tantou_tel)?dataResponse.message.tantou_tel:""}/>
                        <INPUT
                            name="tantou_mail"
                            labelTitle={trans('messages.tantou_mail')}
                            className="form-control input-sm"
                            fieldGroupClass="col-md-8"
                            labelClass="col-md-4"
                            onChange={this.handleChange}
                            autoComplete="nope"
                            defaultValue={dataSource.data.tantou_mail}
                            errorPopup={true}
                            hasError={(dataResponse.message && dataResponse.message.tantou_mail)?true:false}
                            errorMessage={(dataResponse.message&&dataResponse.message.tantou_mail)?dataResponse.message.tantou_mail:""}/>
                        <INPUT
                            name="tantou_password"
                            labelTitle={trans('messages.tantou_password')}
                            groupClass="form-group own-instruction-password"
                            className="form-control input-sm"
                            fieldGroupClass="col-md-8"
                            labelClass="col-md-4"
                            type="password"
                            autoComplete="new-password"
                            onChange={this.handleChange}
                            defaultValue={dataSource.data.tantou_password}
                            errorPopup={true}
                            hasError={(dataResponse.message && dataResponse.message.tantou_password)?true:false}
                            errorMessage={(dataResponse.message&&dataResponse.message.tantou_password)?dataResponse.message.tantou_password:""}/>
                        <div className="form-group">
                            <div className="col-md-4 col-xs-4"></div>
                            <div className="col-md-8 col-xs-8">
                                <label className="instruction-password">※6文字以上<br/>※半角英数字のみ<br/>※「大文字」「小文字」「数字」は必ず使用してください</label>
                            </div>
                        </div>
                        <div className="form-group">
                            <label className="col-md-4 col-xs-4">{trans('messages.is_active_save')}</label>
                            <div className="col-md-8 col-xs-8">
                                <CHECKBOX_CUSTOM
                                    defaultChecked={
                                        (dataSource.data.is_active === 1 || dataSource.data.is_active === 0)
                                        ?dataSource.data.is_active
                                        :true
                                    }
                                    name="is_active"
                                    onChange={this.handleChange}/>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="row">
                    <div className="col-md-6 col-md-offset-3 col-md-offset-right-3">
                        <BUTTON name="btnAccept" className="btn btn-success input-sm btn-larger" type="button" onClick={this.handleSubmit}>{trans('messages.btn_accept')}</BUTTON>
                        <BUTTON className="btn bg-purple input-sm pull-right btn-larger" type="button" onClick={this.handleCancel}>{trans('messages.btn_cancel')}</BUTTON>
                    </div>
                </div>
                </div>
            </FORM>
        </div>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        dataSource: state.commonReducer.dataSource,
        dataResponse: state.commonReducer.dataResponse
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        reloadData: (url, params) => { dispatch(Action.detailData(url, params)) },
        postData: (url, params, headers) => { dispatch(Action.postData(url, params, headers)) }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(UsersSave)