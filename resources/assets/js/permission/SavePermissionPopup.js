import React, { Component } from 'react';
import Loading from '../common/components/common/Loading';
import INPUT from '../common/components/form/INPUT';
import TEXTAREA from '../common/components/form/TEXTAREA';
import CHECKBOX_CUSTOM from '../common/components/form/CHECKBOX_CUSTOM';
import LABEL from '../common/components/form/LABEL';
import SELECT from '../common/components/form/SELECT';
import RADIO_CUSTOM from '../common/components/form/RADIO_CUSTOM';
import ERROR from '../common/components/form/ERROR';

import axios from 'axios';

export default class SavePermissionPopup extends Component {
    constructor(props) {
        super(props);
        this.state = {}
    }

    handleChange = (event) => {
        const target = event.target;
        const name = target.name;
        const value = target.type === 'checkbox' ? (target.checked?1:0) : target.value;
        this.setState({
            [name]: value
        });
    }

    handleSaveClick = () => {
        let params = {};
        params['depart_id']     = this.state.depart_id;
        params['depart_nm']     = this.state.depart_nm;
        params['func_cap_cd']   = this.state.func_cap_cd;
        params['func_cd']       = this.state.func_cd;
        params['screen_class']  = this.state.screen_class;
        params['action']        = this.state.action;
        params['action_denied'] = this.state.action_denied;

        $(".loading").show();
        axios.post(savePerUrl, params, {headers: {'X-Requested-With': 'XMLHttpRequest'}}
        ).then(response => {
            if (response.data.status === 0) {
                let message = response.data.message;
                this.setState({...response.data});
            } else {
                this.handleCancelClick();
                this.props.handleReloadData();
            }
            $(".loading").hide();
        }).catch(error => {
            $(".loading").hide();
            throw(error);
        });
    }

    handleCancelClick = () => {
        this.setState({
            depart_id: '',
            depart_nm: '',
            func_cap_cd: '',
            func_cd: '',
            screen_class: '',
            action: '',
            action_denied: '',
            message: '',
        });
        $('#save-permission-modal').modal('hide');
    }

    componentWillReceiveProps(nextProps) {
        if (typeof nextProps.data_edit !== 'undefined') {
            this.setState({
                depart_id: nextProps.data_edit.depart_id,
                depart_nm: nextProps.data_edit.depart_nm,
                func_cap_cd: nextProps.data_edit.func_cap_cd,
                func_cd: nextProps.data_edit.func_cd,
                screen_class: nextProps.data_edit.screen_class,
                action_denied: nextProps.data_edit.action_denied,
                action: 'edit'
            });
        }
    }

    render() {
        let action_popup = trans('messages.add_permission');
        if (this.state.action === 'edit') {
            action_popup = trans('messages.genre_edit');
        }
        return (
            <div className="modal fade" id="save-permission-modal">
                <Loading className="loading" />
                <div className="modal-dialog ">
                    <div className="modal-content">
                        <div className="modal-header">
                        {action_popup}
                        </div>
                        <div className="modal-body modal-custom-body">
                        <form className="form-horizontal">
                            <div className="row">
                                <div className={'col-md-12 ' + ((this.state.message && this.state.message.exists_record)?"has-error":"")}>
                                    <ERROR>{(this.state.message && this.state.message.exists_record)?this.state.message.exists_record:""}</ERROR>
                                </div>
                            </div>
                            <div className="row">
                                <INPUT
                                    name="depart_id"
                                    labelTitle={trans('messages.depart_id')}
                                    className="form-control input-sm"
                                    fieldGroupClass="col-md-9"
                                    labelClass="col-md-3"
                                    groupClass="col-md-12 margin-form"
                                    errorPopup={true}
                                    readOnly={this.state.action === 'edit' ? true : false}
                                    value={this.state.depart_id||''}
                                    onChange={this.handleChange}
                                    hasError={(this.state.message && this.state.message.depart_id)?true:false}
                                    errorMessage={(this.state.message && this.state.message.depart_id)?this.state.message.depart_id:""}
                                    />
                            </div>
                            <div className="row">
                                <INPUT
                                    name="depart_nm"
                                    labelTitle={trans('messages.depart_name')}
                                    className="form-control input-sm"
                                    fieldGroupClass="col-md-9"
                                    labelClass="col-md-3"
                                    groupClass="col-md-12 margin-form"
                                    errorPopup={true}
                                    value={this.state.depart_nm||''}
                                    onChange={this.handleChange}
                                    hasError={(this.state.message && this.state.message.depart_nm)?true:false}
                                    errorMessage={(this.state.message && this.state.message.depart_nm)?this.state.message.depart_nm:""}
                                    />
                            </div>
                            <div className="row">
                                <SELECT
                                    name="screen_class"
                                    groupClass="col-md-12 margin-form"
                                    labelClass="col-md-3"
                                    fieldGroupClass="col-md-9"
                                    labelTitle={trans('messages.screen_class')}
                                    onChange={this.handleChange}
                                    value={this.state.screen_class||''}
                                    options={this.props.controlOpt}
                                    className="form-control input-sm"
                                    readOnly={this.state.action === 'edit' ? true : false}
                                    errorPopup={true}
                                    hasError={(this.state.message && this.state.message.screen_class)?true:false}
                                    errorMessage={(this.state.message && this.state.message.screen_class)?this.state.message.screen_class:""}/>
                            </div>
                            <div className="row">
                                <div className={"col-md-12 margin-form " + ((this.state.message && this.state.message.func_cap_cd)?"has-error":"")}>
                                    <LABEL
                                        groupClass="col-md-3"
                                        labelClass=" "
                                        labelTitle={trans('messages.func_cap_cd')}>
                                    </LABEL>
                                    <div className="col-md-9">
                                        <fieldset id={"func_cap_cd"}>
                                            <label>
                                                <RADIO_CUSTOM
                                                    name={"func_cap_cd"}
                                                    value="2"
                                                    checked={this.state.func_cap_cd == 2 ? true : false}
                                                    onChange={this.handleChange}
                                                    />
                                                <span>{" " + trans('messages.permission_full') + " "}</span>
                                            </label>
                                            <label>
                                                <RADIO_CUSTOM
                                                    name={"func_cap_cd"}
                                                    value="1"
                                                    checked={this.state.func_cap_cd == 1 ? true : false}
                                                    onChange={this.handleChange}
                                                    />
                                                <span>{" " + trans('messages.permission_view') + " "}</span>
                                            </label>
                                            <label>
                                                <RADIO_CUSTOM
                                                    name={"func_cap_cd"}
                                                    value="0"
                                                    checked={(this.state.func_cap_cd == 0 && this.state.func_cap_cd != "") ? true : false}
                                                    onChange={this.handleChange}
                                                    />
                                                <span>{" " + trans('messages.permission_none') + " "}</span>
                                            </label>
                                        </fieldset>
                                        <ERROR>{(this.state.message && this.state.message.func_cap_cd)?this.state.message.func_cap_cd:""}</ERROR>
                                    </div>
                                </div>
                            </div>
                            <div className="row">
                                <div className={"col-md-12 margin-form " + ((this.state.message && this.state.message.func_cd)?"has-error":"")}>
                                    <LABEL
                                        groupClass="col-md-3"
                                        labelClass=" "
                                        labelTitle={trans('messages.func_cd')}>
                                    </LABEL>
                                    <div className="col-md-9">
                                        <fieldset id={"func_cd"}>
                                            <label>
                                                <RADIO_CUSTOM
                                                    name={"func_cd"}
                                                    value="2"
                                                    checked={this.state.func_cd == 2 ? true : false}
                                                    onChange={this.handleChange}
                                                    />
                                                <span>{" " + trans('messages.permission_full') + " "}</span>
                                            </label>
                                            <label>
                                                <RADIO_CUSTOM
                                                    name={"func_cd"}
                                                    value="1"
                                                    checked={this.state.func_cd == 1 ? true : false}
                                                    onChange={this.handleChange}
                                                    />
                                                <span>{" " + trans('messages.permission_view') + " "}</span>
                                            </label>
                                            <label>
                                                <RADIO_CUSTOM
                                                    name={"func_cd"}
                                                    value="0"
                                                    checked={(this.state.func_cd == 0 && this.state.func_cd != "") ? true : false}
                                                    onChange={this.handleChange}
                                                    />
                                                <span>{" " + trans('messages.permission_none') + " "}</span>
                                            </label>
                                        </fieldset>
                                        <ERROR>{(this.state.message && this.state.message.func_cd)?this.state.message.func_cd:""}</ERROR>
                                    </div>
                                </div>
                            </div>
                            <div className="row">
                                <TEXTAREA
                                    name="action_denied"
                                    labelTitle={trans('messages.add_action_denied')}
                                    className="form-control input-sm"
                                    fieldGroupClass="col-md-9"
                                    labelClass="col-md-3"
                                    groupClass="col-md-12 margin-form"
                                    errorPopup={true}
                                    value={this.state.action_denied||''}
                                    onChange={this.handleChange}
                                    />.
                            </div>
                        </form>
                        </div>
                        <div className="modal-footer modal-custom-footer">
                            <button type="button" className="btn btn-primary btn-sm modal-custom-button" onClick={this.handleSaveClick}>{trans('messages.btn_accept')}</button>
                            <button type="button" className="btn bg-purple btn-sm modal-custom-button" onClick={this.handleCancelClick}>{trans('messages.btn_cancel')}</button>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}