import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import * as Action from '../common/actions';
import * as helpers from '../common/helpers';
import * as queryString from 'query-string';
import createHistory from 'history/createBrowserHistory';
import BUTTON from '../common/components/form/BUTTON';
import FORM from '../common/components/form/FORM';
import INPUT from '../common/components/form/INPUT';
import SELECT from '../common/components/form/SELECT';
import LABEL from '../common/components/form/LABEL';
import CHECKBOX_CUSTOM from '../common/components/form/CHECKBOX_CUSTOM';
import TABLE from '../common/components/table/TABLE';
import THEAD from '../common/components/table/THEAD';
import TBODY from '../common/components/table/TBODY';
import TR from '../common/components/table/TR';
import TH from '../common/components/table/TH';
import TD from '../common/components/table/TD';
import SortComponent from '../common/components/table/SortComponent';
import PaginationContainer from '../common/containers/PaginationContainer'
import Loading from '../common/components/common/Loading';
import MessagePopup from '../common/components/common/MessagePopup';
import ImportCsv from './csv/ImportCsv';
import DatePicker from 'react-datepicker';
import moment from 'moment';
import 'react-datepicker/dist/react-datepicker.css';
import axios from 'axios';
class OrderToSupplierMonthList extends Component {
    constructor(props) {
        super(props);
        const stringParams = helpers.parseStringToParams(props.location.search);
        this.state = {}
        if (!_.isEmpty(stringParams)) {
            this.state = stringParams;
        }
        this.state.attached_file_path = '';
        this.props.reloadData(dataListUrl, stringParams);
    }

    handleChange = (event) => {
        const target = event.target;
        let value = target.value;
        if (value === '_none') {
            value = '';
        }
        let name = target.name;
        this.setState({
            [name]: (value)?value:''
        },() => {
            if (name === 'per_page' || name === 'is_ordered' || name === 'is_cancel') {
                this.handleReloadData();
            }
        });
    }

    handleProcessOrderToSupplier = (event) => {
        let headers = {'X-Requested-With': 'XMLHttpRequest'};
        $(".loading").show();
            axios.post(O2SUrl, {}, {headers : headers}
            ).then(response => {
                $(".loading").hide();
                if (response.data.flg === 1) {
                    let dataSet = {};
                    if (response.data.message_success === '') {
                        $.notify({
                            icon: 'glyphicon glyphicon-warning-sign',
                            message: trans('messages.message_no_data_process'),
                        },{
                            type: 'info',
                            delay: 2000
                        });
                    } else {
                        this.setState({...response.data}, () => {
                             $.notify({
                                icon: 'glyphicon glyphicon-warning-sign',
                                message: response.data.message_success,
                            },{
                                type: 'success',
                                delay: 2000
                            });
                        });
                    }
                }
            }).catch(error => {
                $(".loading").hide();
                throw(error);
            });
    }

    handleClearClick = (event) => {
        Object.keys(this.state).map((key) => {
            if (key !== 'per_page') {
                delete this.state[key];
            }
        });
        this.handleReloadData();
    }

    handleClickPage = (page) => {
        this.setState({
            page: page
        },() => {
            this.props.reloadData(dataListUrl, this.state, true);
        });
    }

    handleSortClick = (name, nextSort) => {
        Object.keys(this.state).map((key) => {
            if (key.startsWith('sort_')) {
                delete this.state[key];
            }
        });
        if (nextSort === 'none') {
            delete this.state[name];
            this.props.reloadData(dataListUrl, this.state, true);
        } else {
            this.setState({[name]: nextSort}, () => {
                this.props.reloadData(dataListUrl, this.state, true);
            });
        }
    }
    handleReloadData = (a) => {
        let params = {};
        let dataSource = this.props.dataSource;
        let tmpParams = this.state;
        Object.keys(tmpParams).map(key => {
            if (tmpParams[key] !== ""
                && tmpParams[key] !== null
                && key !== 'flg'
                && key !== 'attached_file_path'
                && key !== 'attached_file'
                && key !== 'msg'
                && key !== 'page') {
                    params[key] = tmpParams[key];
            } else {
                delete params[key];
            }
        });
        this.props.reloadData(dataListUrl, params, true);
    }
    componentDidUpdate() {
        if (!_.isEmpty(this.props.dataSource)) {
            $('.loading').hide();
        }
        delete this.state['flg'];
        delete this.state['msg'];
        dragscroll.reset();
        $("table th").resizeble();
    }
    componentWillMount() {
        window.onpopstate = (event) => {
            location.reload();
        };
    }

    handleChooseFile = (event) => {
        const target = event.target;
        if (target.files !== null && target.files.length !== 0) {
            const name = target.name;
            let tmpName = name.replace(new RegExp("^browser_"), '');
            this.setState({
                [tmpName]: target.files[0].name,
                ['attached_file']: target.files[0]
            }, ()=> {
                document.getElementsByName(tmpName)[0].value = target.files[0].name;
            });
        }
    }

    handleImportSubmit = () => {
        let formData   = new FormData();
        let headers = {'X-Requested-With': 'XMLHttpRequest'};
        if (!_.isEmpty(this.state)) {
            Object.keys(this.state).map((item) => {
                formData.append(item, this.state[item]);
            });
        }
        this.flg_check = true;
        this.setState({
            process: true,
            status_csv: 'show',
            percent: 0,
        }, () => {
            this.props.postDataCSV(csvUrl, formData, headers);
        });
    }

    handleChecked = (index, value, name, event) => {
        if (value === null) {
            value = 0;
        }
        let headers = {'X-Requested-With': 'XMLHttpRequest'};
        this.props.postData(
            updateUrl,
            {index: index, name : name, value: value},
            headers,
            this.handleReloadData,
        );
    }

    handleImportCancel = () => {
        $('#issue-modal').modal('hide');
        this.flg_check = false;
        this.setState({
            attached_file_path: '',
            attached_file: '',
            message: '',
            status_csv: false,
            error: 0,
            msg: '',
        }, () => {
            $("input[name='browser_attached_file_path']").val('');
            this.handleReloadData();
        });
    }

    handleImport = (event, name) => {
        $('#action-modal').val(name);
        $('#issue-modal').modal({backdrop: 'static', show: true});
    }

    componentWillReceiveProps(nextProps) {
        let dataResponse = nextProps.dataResponseCsv;
        if (!_.isEmpty(dataResponse) && this.flg_check) {
            if (typeof dataResponse.not_permission !== "undefined" && dataResponse.not_permission === 1) {
                location.reload();
            }
            if (typeof dataResponse.reload !== 'undefined' && dataResponse.reload) {
                this.props.postDataCSV(importUrl, dataResponse.params, dataResponse.headers, dataResponse.count_err);
            }
            if(dataResponse.flg === 1) {
                let formData   = new FormData();
                let headers = {'X-Requested-With': 'XMLHttpRequest'};
                Object.keys(dataResponse).map((item) => {
                    formData.append(item, dataResponse[item]);
                });
                formData.append('type', $('#action-modal').val());
                this.setState({
                    process: true,
                    status_csv: 'show',
                    percent: (dataResponse.currPage/dataResponse.timeRun) * 100,
                }, () => {
                    this.props.postDataCSV(importUrl, formData, headers);
                });
            } else if (dataResponse.flg === 0) {
                let dataSet = {
                    process: false,
                    status_csv: false,
                };
                this.flg_check = false;
                if (dataResponse.msg === 'Finish') {
                    $.notify({
                        icon: 'glyphicon glyphicon-warning-sign',
                        message: trans('messages.message_import_success'),
                    },{
                        type: 'info',
                        delay: 2000
                    });
                    dataSet['msg'] = '';
                    this.setState({...dataResponse,attached_file_path:'', ...dataSet}, () => {
                        if (dataResponse.error === 0) {
                            $('#issue-modal').modal('hide');
                            this.handleReloadData();
                        }
                        $("input[name='browser_attached_file_path']").val('');
                        $("input[name='browser_attached_file_path']").val('');
                    });
                } else {
                    this.setState({...dataResponse, ...dataSet});
                }
            }
        }
    }

    render() {
        let dataSource = this.props.dataSource;
        let index = 0;
        let options = [];
        let totalItems = 0;
        let itemsPerPage = 20;
        if (!_.isEmpty(dataSource)) {
            index = dataSource.data.from - 1;
            options = dataSource.option;
            totalItems   = dataSource.data.total;
            itemsPerPage = dataSource.data.per_page;
        }
        return ( (!_.isEmpty(dataSource)) &&
            <div>
                <Loading className="loading" />
                <div className="box">
                    <div className="box-header">
                        <div className="row">
                            <div className="col-md-12">
                                <h4>{trans('messages.search_title')}</h4>
                            </div>
                                <FORM>
                                    <INPUT
                                        name="product_code"
                                        groupClass="form-group col-md-2"
                                        labelTitle={trans('messages.product_code')}
                                        onChange={this.handleChange}
                                        value={this.state.product_code ||''}
                                        className="form-control input-sm" />
                                    <INPUT
                                        name="supplier_id"
                                        groupClass="form-group col-md-2"
                                        labelTitle={trans('messages.search_supplier_code')}
                                        onChange={this.handleChange}
                                        value={this.state.supplier_id ||''}
                                        className="form-control input-sm" />
                                    <SELECT
                                        name="is_ordered"
                                        groupClass="form-group col-md-1-5"
                                        labelClass="pull-left"
                                        labelTitle={trans('messages.search_is_ordered')}
                                        onChange={this.handleChange}
                                        value={this.state.is_ordered||''}
                                        options={options}
                                        className="form-control input-sm" />
                                    <SELECT
                                        name="is_cancel"
                                        groupClass="form-group col-md-1-5"
                                        labelClass="pull-left"
                                        labelTitle={trans('messages.is_cancel')}
                                        onChange={this.handleChange}
                                        value={this.state.is_cancel||''}
                                        options={options}
                                        className="form-control input-sm" />
                                    <SELECT
                                        name="per_page"
                                        groupClass="form-group col-md-1 per-page pull-right-md"
                                        labelClass="pull-left"
                                        labelTitle={trans('messages.per_page')}
                                        onChange={this.handleChange}
                                        value={this.state.per_page||''}
                                        options={{20:20, 40:40, 60:60, 80:80, 100:100}}
                                        className="form-control input-sm" />
                                </FORM>
                        </div>
                        <div className="row">
                            <div className="col-md-6 col-xs-12 margin-bottom-btn">
                                <BUTTON className="btn btn-primary pull-xs-right" onClick={this.handleProcessOrderToSupplier}>{trans('messages.import_order_to_supplier_csv')}</BUTTON>
                                <BUTTON className="btn btn-primary margin-element pull-xs-right pull-left" onClick={(e) => this.handleImport(e, 'monthly_csv')}>{trans('messages.import_csv')}</BUTTON>
                            </div>
                            <div className="col-md-6 col-xs-12">
                                <BUTTON className="btn btn-danger pull-right" onClick={this.handleClearClick}>{trans('messages.btn_reset')}</BUTTON>
                                <BUTTON className="btn btn-primary pull-right margin-element" onClick={this.handleReloadData} >{trans('messages.btn_search')}</BUTTON>
                            </div>
                        </div>
                    </div>

                    <div className="box-body">
                        <div className="row">
                            <div className="col-md-12">
                                {(totalItems/itemsPerPage > 1) &&
                                <div className="row">
                                    <div className="col-md-2-5"></div>
                                    <div className="col-md-7 text-center">
                                        <PaginationContainer handleClickPage={this.handleClickPage}/>
                                    </div>
                                    <div className="col-md-2-5 text-right line-height-md">
                                        {dataSource.data.from}件 - {dataSource.data.to}件 【全{dataSource.data.total}件】
                                    </div>
                                </div>
                                }
                                <div id="dragscroll" className="table-responsive">
                                <TABLE className="table table-striped table-custom">
                                    <THEAD>
                                        <TR>
                                            <TH className="visible-xs visible-sm"></TH>
                                            <TH className="hidden-xs hidden-sm text-center">
                                                #
                                            </TH>
                                            <TH className="text-center col-max-min-120">
                                                <SortComponent
                                                    name="sort_product_code"
                                                    onClick={this.handleSortClick}
                                                    currentSort={(this.state.sort_product_code)?this.state.sort_product_code:'none'}
                                                    title={trans('messages.product_code')} />
                                            </TH>
                                            <TH className="hidden-xs hidden-sm text-center resize-thead col-max-min-100">
                                                <SortComponent
                                                    name="sort_product_name"
                                                    onClick={this.handleSortClick}
                                                    currentSort={(this.state.sort_product_name)?this.state.sort_product_name:'none'}
                                                    title={trans('messages.product_name')} />
                                            </TH>
                                            <TH className="text-center col-max-min-120">
                                                <SortComponent
                                                    name="sort_supplier_id"
                                                    onClick={this.handleSortClick}
                                                    currentSort={(this.state.sort_supplier_id)?this.state.sort_supplier_id:'none'}
                                                    title={trans('messages.search_supplier_code')} />
                                            </TH>
                                            <TH className="hidden-xs hidden-sm text-center col-max-min-200">
                                                <SortComponent
                                                    name="sort_supplier_name"
                                                    onClick={this.handleSortClick}
                                                    currentSort={(this.state.sort_supplier_name)?this.state.sort_supplier_name:'none'}
                                                    title={trans('messages.supplier_name')} />
                                            </TH>
                                            <TH className="hidden-xs hidden-sm text-center col-max-min-100">
                                                <SortComponent
                                                    name="sort_order_num"
                                                    onClick={this.handleSortClick}
                                                    currentSort={(this.state.sort_order_num)?this.state.sort_order_num:'none'}
                                                    title={trans('messages.order_quantity')} />
                                            </TH>
                                            <TH className="hidden-xs hidden-sm text-center col-max-min-100">
                                                <SortComponent
                                                    name="sort_price_invoice"
                                                    onClick={this.handleSortClick}
                                                    currentSort={(this.state.sort_price_invoice)?this.state.sort_price_invoice:'none'}
                                                    title={trans('messages.search_price_invoice')} />
                                            </TH>
                                            <TH className="hidden-xs hidden-sm text-center col-max-min-100">
                                                <SortComponent
                                                    name="sort_order_price"
                                                    onClick={this.handleSortClick}
                                                    currentSort={(this.state.sort_order_price)?this.state.sort_order_price:'none'}
                                                    title={trans('messages.order_price')} />
                                            </TH>
                                            <TH className="hidden-xs hidden-sm text-center col-max-min-60">{trans('messages.note')}</TH>
                                            <TH className="hidden-xs hidden-sm text-center col-max-min-100">{trans('messages.search_is_ordered')}</TH>
                                            <TH className="hidden-xs hidden-sm text-center col-max-min-100">{trans('messages.is_cancel')}</TH>
                                            <TH className="hidden-xs hidden-sm text-center col-max-min-100">{trans('messages.error_code')}</TH>
                                            <TH className="hidden-xs hidden-sm text-center resize-thead col-max-min-200">{trans('messages.error_message')}</TH>
                                        </TR>
                                    </THEAD>
                                    <TBODY>
                                        {
                                            (!_.isEmpty(dataSource.data.data))?
                                            dataSource.data.data.map((item) => {
                                                index++;
                                                let classRow = (index%2 ===0) ? 'odd' : 'even';
                                                let nf = new Intl.NumberFormat();
                                                return (
                                                    [
                                                    <TR key={"tr" + item.index} className={classRow}>
                                                        <TD className="visible-xs visible-sm text-center">
                                                            <b className="show-info">
                                                                <i className="fa fa-angle-double-down" aria-hidden="true"></i>
                                                            </b>
                                                        </TD>
                                                        <TD className="hidden-xs hidden-sm text-center">
                                                           <a href={saveUrl + '?index=' +item.index} className="link-detail">{item.index}</a>
                                                        </TD>
                                                        <TD className="cut-text text-left" title={item.product_code}>{item.product_code}</TD>
                                                        <TD className="text-left text-long-batch max-w-200" title={item.product_name}>{item.product_name}</TD>
                                                        <TD className="hidden-xs hidden-sm cut-text text-right" title={item.supplier_id}>{item.supplier_id}</TD>
                                                        <TD className="hidden-xs hidden-sm cut-text text-left text-long-batch max-w-200" title={item.supplier_nm}>{item.supplier_nm}</TD>
                                                        <TD className="hidden-xs hidden-sm cut-text text-right" title={item.order_num}>{item.order_num}</TD>
                                                        <TD className="hidden-xs hidden-sm cut-text text-right" title={nf.format(item.price_invoice)}>{nf.format(item.price_invoice)}</TD>
                                                        <TD className="hidden-xs hidden-sm cut-text text-right" title={nf.format(item.order_price)}>{nf.format(item.order_price)}</TD>
                                                        <TD className="hidden-xs hidden-sm cut-text text-right text-long-batch max-w-200" title={item.note}>{item.note}</TD>
                                                        <TD className="hidden-xs hidden-sm text-center">
                                                            <CHECKBOX_CUSTOM
                                                                defaultChecked={item.is_ordered ? true : false}
                                                                readOnly
                                                                disabled
                                                             />
                                                        </TD>
                                                        <TD className="hidden-xs hidden-sm text-center">
                                                            <CHECKBOX_CUSTOM
                                                                defaultChecked={item.is_cancel ? true : false}
                                                                onChange={(e) => this.handleChecked(item.index, item.is_cancel, 'is_cancel', e)}
                                                             />
                                                        </TD>
                                                        <TD className="hidden-xs hidden-sm cut-text" title={item.error_code}>{item.error_code}</TD>
                                                        <TD className="hidden-xs hidden-sm cut-text text-left text-long-batch max-w-200" title={item.error_message}>{item.error_message}</TD>
                                                    </TR>,
                                                    <TR key={"trhide" + index} className="hiden-info" style={{display:"none"}}>
                                                        <TD colSpan="10" className="width-span1">
                                                            <ul id={"temp" + item.mail_id} >
                                                                <li> <b>{trans('messages.product_code')}</b> : {item.product_code}</li>
                                                                <li> <b>{trans('messages.product_name')}</b> : {item.product_name}</li>
                                                                <li> <b>{trans('messages.search_supplier_code')}</b> : {item.supplier_id}</li>
                                                                <li> <b>{trans('messages.supplier_name')}</b> : {item.supplier_nm}</li>
                                                                <li> <b>{trans('messages.order_quantity')}</b> : {item.order_num}</li>
                                                                <li> <b>{trans('messages.search_price_invoice')}</b> : {nf.format(item.price_invoice)}</li>
                                                                <li> <b>{trans('messages.order_price')}</b> : {nf.format(item.order_price)}</li>
                                                                <li> <b>{trans('messages.note')}</b> : {item.note}</li>
                                                                <li> <b>{trans('messages.search_is_ordered')}</b>:
                                                                    <div className="monthly-checkbox">
                                                                        <CHECKBOX_CUSTOM
                                                                            defaultChecked={item.is_ordered ? true : false}
                                                                        />
                                                                    </div>
                                                                </li>
                                                                <li> <b>{trans('messages.is_cancel')}</b> :
                                                                    <div className="monthly-checkbox">
                                                                        <CHECKBOX_CUSTOM
                                                                            defaultChecked={item.is_cancel ? true : false}
                                                                        />
                                                                    </div>
                                                                </li>
                                                                <li> <b>{trans('messages.error_code')}</b> : {item.error_code}</li>
                                                                <li> <b>{trans('messages.error_message')}</b> : {item.error_message}</li>
                                                            </ul>
                                                        </TD>
                                                        </TR>
                                                   ]

                                                )
                                                if (index === dataSource.data.to) {
                                                   index = 0;
                                                }
                                            })
                                            :
                                            <TR><TD colSpan="13" className="text-center">{trans('messages.no_data')}</TD></TR>
                                        }
                                    </TBODY>
                                </TABLE>
                                </div>
                                <div className="text-center">
                                    <PaginationContainer handleClickPage={this.handleClickPage}/>
                                </div>
                                <ImportCsv
                                    handleChooseFile={this.handleChooseFile}
                                    handleImportSubmit={this.handleImportSubmit}
                                    value={this.state.attached_file_path ? this.state.attached_file_path : ''}
                                    error={this.state.error}
                                    filename={this.state.filename}
                                    handleImportCancel={this.handleImportCancel}
                                    process={this.state.process ? true : false}
                                    status={this.state.status_csv}
                                    percent={this.state.percent}
                                    hasError={(this.state.message || this.state.msg) ? true : false}
                                    errorMessage={this.state.message || this.state.msg}
                                />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}
const mapStateToProps = (state) => {
  return {
    dataSource: state.commonReducer.dataSource,
    dataResponse: state.commonReducer.dataResponse,
    dataResponseCsv: state.commonReducer.dataResponseCsv
  }
}
const  mapDispatchToProps = (dispatch) => {
  let ignore = [
        'attached_file',
        'attached_file_path',
        'message','flg',
        'process',
        'msg',
        'timeRun',
        'currPage',
        'total',
        'filename',
        'error',
        'percent',
        'status_csv',
        'url',
        'reload',
        'params',
        'headers',
        'count',
        'realFilename',
        'type',
        'fileCorrect',
        'message_success',
        'checkAfter',
    ];
  return {
    reloadData: (url, params, pushHistory, ignoreFields) => { dispatch(Action.reloadData(url, params, pushHistory, ignore)) },
    postData: (url, params, headers,callback,callParam) => { dispatch(Action.postData(url, params, headers,callback,callParam)) },
    postDataCSV: (url, params, headers) => { dispatch(Action.postDataCSV(url, params, headers)) }
  }
}
export default connect(mapStateToProps, mapDispatchToProps)(OrderToSupplierMonthList)
