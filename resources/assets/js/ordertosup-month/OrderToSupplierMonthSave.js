import React, { Component } from 'react';
import { connect } from 'react-redux';

import * as Action from '../common/actions';
import * as queryString from 'query-string';
import moment from 'moment';

import Loading from '../common/components/common/Loading';
import FORM from '../common/components/form/FORM';
import INPUT from '../common/components/form/INPUT';
import TEXTAREA from '../common/components/form/TEXTAREA';
import SELECT from '../common/components/form/SELECT';
import BUTTON from '../common/components/form/BUTTON';
import CHECKBOX_CUSTOM from '../common/components/form/CHECKBOX_CUSTOM';
import LABEL from '../common/components/form/LABEL';
import DATEPICKER from '../common/components/form/DATEPICKER';
import MessagePopup from '../common/components/common/MessagePopup';
import MESSAGE_MODAL from '../common/containers/MessageModal';
import axios from 'axios';
var timer_product;
var timer_supplier;
const arrKey = [
    'index',
    'order_code',
    'order_date',
    'product_code',
    'supplier_id',
    'order_num',
    'order_price',
    'is_ordered',
    'is_cancel',
    'note',
];
class OrderToSupplierMonthSave extends Component {
    constructor(props) {
        super(props);
        this.state = {status:'add'}
        this.firstRender = true;
        let params = queryString.parse(props.location.search);
        if (params['index']) {
            this.state.index    = params['index'];
            this.state.status       = 'edit';
            this.state.index        = params['index'];
            this.state.accessFlg    = true;
            this.state.first        = true;
            this.hanleRealtime();
        }
        this.props.reloadData(saveUrl, this.state);
    }
    handleCancel = (event) => {
        window.location.href = baseUrl;
    }
    hanleRealtime() {
        if (!_.isEmpty(this.state.index)) {
            let params = {
                accessFlg : this.state.accessFlg,
                query : {
                        page_key : 'RETURN-SAVE',
                        primary_key : 'index:' + this.state.index
                    },
                key :this.state.index
            };
            if (this.state.accessFlg !== false) {
                this.props.postRealTime(checkEditUrl, params, {'X-Requested-With': 'XMLHttpRequest'});
            }
        }
    }
    handleSubmit = (event) => {
        let headers = {'X-Requested-With': 'XMLHttpRequest'};
        let params = {};
        if (!_.isEmpty(this.state)) {
            Object.keys(this.state).map((item) => {
                if (arrKey.indexOf(item) !== -1) {
                    params[item] = this.state[item];
                }
            });
        this.props.postData(saveUrl, params, headers);
        }
    }

    handleChange = (event) => {
        const target = event.target;
        const name = target.name;
        const value = target.type === 'checkbox' ? (target.checked?1:0) : target.value;
        if (name === 'product_code') {
            clearTimeout(timer_product);
            let _this = this;
            timer_product = setTimeout(function() {
                _this.getInfoProduct(value);
            }, 1000);
        }
        if (name === 'supplier_id') {
            clearTimeout(timer_supplier);
            let _this = this;
            timer_supplier = setTimeout(function() {
                _this.getInfoSupplier(value);
            }, 1000);
        }
        let dataSet = {};
        if (name === 'order_num' || name === 'order_price') {
            if ((name === 'order_price' && this.state.order_num) ||
                (name === 'order_num' && this.state.order_price)) {
                dataSet.total_price = (name === 'order_price' ? this.state.order_num : this.state.order_price) * value;
                $('input[name=total_price]').val(dataSet.total_price);
            }
        }
        this.setState({
            ...dataSet,
            [name]: value
        });
    }
    getInfoProduct = (productCode, load) => {
        $(".loading").show();
        let headers = {'X-Requested-With': 'XMLHttpRequest'};
        let params = {product_code : productCode };
        axios.post(getDataProductUrl, params, {headers : headers}
        ).then(response => {
            $(".loading").hide();
            if (_.isEmpty(response.data.dataInfo)) {
                this.setState({
                    product_name : '',
                    supplier_id : '',
                    supplier_nm : '',
                });
            } else {
                this.setState({...response.data.dataInfo});
            }
        }).catch(error => {
            $(".loading").hide();
            throw(error);
        });
    }

    getInfoSupplier = (supplierCode) => {
        $(".loading").show();
        let formData   = new FormData();
        let headers = {'X-Requested-With': 'XMLHttpRequest'};
        formData.append('supplier_id', supplierCode);
        axios.post(getDataSupplierUrl, formData, {headers : headers}
        ).then(response => {
            $(".loading").hide();
            if (_.isEmpty(response.data.dataSupplier)) {
                this.setState({supplier_name : ''});
            } else {
                this.setState({...response.data.dataSupplier});
            }
        }).catch(error => {
            $(".loading").hide();
            throw(error);
        });
    }
    handleChangeDate = function(date, name, event) {
        let dateValue = moment(date);
        if (!dateValue.isValid()) {
            dateValue = undefined;
        } else {
            dateValue = dateValue.format('YYYY-MM-DD');
        }
        this.setState({
          [name]: dateValue,
        });
    }
    componentWillUnmount () {
        clearInterval(this.state);
    }
    componentDidMount() {
        setInterval(() => {
            this.hanleRealtime()
        }, 5000);
    }
    componentWillReceiveProps(nextProps) {
        if (!_.isEmpty(nextProps.dataSource) && this.state.first) {
            if (!_.isEmpty(nextProps.dataSource.data)) {
                let data = nextProps.dataSource.data;
                let dateSet = {...data};
                if (!_.isEmpty(this.state)) {
                    Object.keys(this.state).map((item) => {
                        if (arrKey.indexOf(item) !== -1) {
                            dateSet[item] = this.state[item];
                        }
                    });
                }
                if (typeof dateSet.order_num !== 'undefined' &&
                    typeof dateSet.order_price !== 'undefined') {
                    dateSet.total_price = dateSet.order_num * dateSet.order_price;
                }
                this.setState(dateSet);
            } else {
                if (typeof this.state.product_code !== 'undefined') {
                    this.getInfoProduct(this.state.product_code);
                }
            }
            this.state.first = undefined;
        }
        if (!_.isEmpty(nextProps.dataResponse)) {
            if (nextProps.dataResponse.status === 1) {
                window.location.href = baseUrl;
            }
        }
         // Check realtime update
        if (!_.isEmpty(nextProps.dataRealTime)) {
            this.setState(nextProps.dataRealTime);
        }
    }
    componentDidUpdate() {
        if (!_.isEmpty(this.props.dataSource)) {
            $('.loading').hide();
        }      
    }
    render() {
        let dataSource   = this.props.dataSource;
        let dataResponse = this.props.dataResponse;
        return (!_.isEmpty(dataSource) &&
            <div>
            <div className="box box-primary">
            <div className="container-fluid">
            <FORM
                className="form-horizontal">
                <div className="box-body">                
                <Loading className="loading" />
                {/*<MessagePopup classType="alert-error" status={(this.state.product_before === this.state.product_code && this.state.product_code) ? 'show' : ''} message={trans('messages.product_not_exists')} />*/}
                <div className="row">
                    <div className="col-md-12">
                    <div className="row">
                        <div className="col-md-6 margin-form">
                            <INPUT
                                name="order_code"
                                labelTitle={trans('messages.order_code')}
                                className="form-control input-sm"
                                groupClass="row"
                                fieldGroupClass="col-md-6"
                                labelClass="col-md-4"
                                readOnly={true}
                                onChange={this.handleChange}
                                defaultValue={this.state.order_code || ''}/>
                        </div>
                        <div className="col-md-6 margin-form">
                        <DATEPICKER
                            placeholderText="YYYY-MM-DD"
                            dateFormat="YYYY-MM-DD"
                            locale="ja"
                            minDate={moment()}
                            name="order_date"
                            labelTitle={trans('messages.order_date_monthly')}
                            className="form-control input-sm"
                            groupClass="row"
                            fieldGroupClass="col-md-6"
                            labelClass="col-md-3"
                            selected={this.state.order_date ? moment(this.state.order_date) : null}
                            onChange={(date,name, e) => this.handleChangeDate(date, 'order_date', e)} />
                        </div>
                        </div>
                    </div>
                </div>
                <div className="row">
                    <div className="col-md-12">
                        <div className="row">
                            <div className="col-md-6 margin-form">                
                                <INPUT
                                    name="product_code"
                                    labelTitle={trans('messages.product_code')}
                                    className="form-control input-sm"
                                    groupClass="row"
                                    fieldGroupClass="col-md-6"
                                    labelClass="col-md-4"
                                    onChange={this.handleChange}
                                    defaultValue={this.state.product_code || ''}
                                    errorPopup={true}
                                    hasError={(dataResponse.message && dataResponse.message.product_code)?true:false}
                                    errorMessage={(dataResponse.message&&dataResponse.message.product_code)?dataResponse.message.product_code:""}/>
                            </div>
                            <div className="col-md-6 margin-form"> 
                                <INPUT
                                    key={this.state.product_name}
                                    name="product_name"
                                    labelTitle={trans('messages.product_name')}
                                    readOnly={true}
                                    className="form-control input-sm"
                                    groupClass="row"
                                    fieldGroupClass="col-md-9"
                                    labelClass="col-md-3"
                                    defaultValue={this.state.product_name || ''}/>
                            </div>
                        </div>
                    </div>                            
                </div>
                <div className="row">
                    <div className="col-md-12">
                        <div className="row">
                            <div className="col-md-6 margin-form">                     
                                <INPUT
                                    key={this.state.product_name}
                                    name="supplier_id"
                                    labelTitle={trans('messages.supplier_code')}
                                    className="form-control input-sm"
                                    groupClass="row"
                                    fieldGroupClass="col-md-6"
                                    labelClass="col-md-4"
                                    onChange={this.handleChange}
                                    defaultValue={this.state.supplier_id || ''}/>
                            </div>
                            <div className="col-md-6 margin-form"> 
                                <INPUT
                                    key={this.state.supplier_name}
                                    name="supplier_name"
                                    labelTitle={trans('messages.supplier_name')}
                                    readOnly={true}
                                    className="form-control input-sm"
                                    groupClass="row"
                                    fieldGroupClass="col-md-9"
                                    labelClass="col-md-3"
                                    onChange={this.handleChange}
                                    defaultValue={this.state.supplier_name || ''}/>
                            </div>
                        </div>
                    </div>                            
                </div>
                <div className="row">
                    <div className="col-md-12">
                        <div className="row">
                            <div className="col-md-6 margin-form">                   
                                <INPUT
                                    name="order_num"
                                    labelTitle={trans('messages.order_quantity')}
                                    className="form-control input-sm"
                                    type="number"
                                    groupClass="row"
                                    fieldGroupClass="col-md-6"
                                    labelClass="col-md-4"
                                    onChange={this.handleChange}
                                    defaultValue={this.state.order_num || ''}
                                    errorPopup={true}
                                    hasError={(dataResponse.message && dataResponse.message.order_num)?true:false}
                                    errorMessage={(dataResponse.message&&dataResponse.message.order_num)?dataResponse.message.order_num:""}/>
                            </div>
                            <div className="col-md-6">
                                <div className="row">
                                    <div className="col-md-6 margin-form">
                                        <INPUT
                                            name="order_price"
                                            labelTitle={trans('messages.order_unit_price')}
                                            className="form-control input-sm"
                                            groupClass="row"
                                            fieldGroupClass="col-md-6"
                                            labelClass="col-md-6"
                                            type="number"
                                            onChange={this.handleChange}
                                            defaultValue={this.state.order_price || ''}
                                            errorPopup={true}
                                            hasError={(dataResponse.message && dataResponse.message.order_price)?true:false}
                                            errorMessage={(dataResponse.message&&dataResponse.message.order_price)?dataResponse.message.order_price:""}/>
                                    </div>
                                    <div className="col-md-6 margin-form">
                                        <INPUT
                                            name="total_price"
                                            labelTitle={trans('messages.order_total_price')}
                                            readOnly={true}
                                            className="form-control input-sm"
                                            groupClass="row"
                                            fieldGroupClass="col-md-6"
                                            labelClass="col-md-6 "
                                            onChange={this.handleChange}
                                            defaultValue={this.state.total_price || ''}/>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                
                <div className="row">
                    <div className="col-md-12">
                        <div className="row">  
                            <div className="col-md-6 margin-form">
                                <LABEL
                                    groupClass="row"
                                    labelTitle={trans('messages.is_ordered')}
                                    labelClass="col-md-4 col-xs-4"
                                    fieldGroupClass="col-md-6  col-xs-8">
                                    <CHECKBOX_CUSTOM
                                        name="is_ordered"
                                        defaultChecked={this.state.is_ordered === 1 ? true : false}
                                        onChange={this.handleChange} />
                                </LABEL>
                            </div>
                            <div className="col-md-6 margin-form">
                                <LABEL groupClass="row"
                                    labelTitle={trans('messages.is_cancel')}
                                    labelClass="col-md-3 col-xs-4"
                                    fieldGroupClass="col-md-6  col-xs-8">
                                    <CHECKBOX_CUSTOM
                                        name="is_cancel"
                                        defaultChecked={this.state.is_cancel === 1 ? true : false}
                                        onChange={this.handleChange} />
                                </LABEL>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="row margin-form">
                    <div className="col-md-12">
                        <TEXTAREA
                            name="note"
                            labelTitle={trans('messages.note')}
                            className="form-control input-sm"
                            groupClass="row"
                            fieldGroupClass="col-md-10"
                            rows="8"
                            labelClass="col-md-2"
                            onChange={this.handleChange}
                            defaultValue={this.state.note || ''}/>
                    </div>
                </div>
                <div className="row margin-form">
                    <div className="col-md-3 col-md-offset-9">
                        <BUTTON name="btnAccept" id="btnSubmit" className="btn btn-success input-sm btn-larger" type="button" onClick={this.handleSubmit}>{trans('messages.btn_accept')}</BUTTON>
                        <BUTTON className="btn bg-purple input-sm pull-right btn-larger" type="button" onClick={this.handleCancel}>{trans('messages.btn_cancel')}</BUTTON>
                    </div>
                </div>
                </div>
            </FORM>
            </div>
            <MESSAGE_MODAL
                message={this.state.message || ''}
                title={trans('messages.deny_edit')}
                baseUrl={baseUrl}
            />
            </div>
        </div>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        dataSource: state.commonReducer.dataSource,
        dataResponse: state.commonReducer.dataResponse,
        dataRealTime: state.commonReducer.dataRealTime
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        reloadData: (url, params) => { dispatch(Action.detailData(url, params)) },
        postData: (url, params, headers) => { dispatch(Action.postData(url, params, headers)) },
        postRealTime: (url, params, headers) => { dispatch(Action.postRealTime(url, params, headers)) }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(OrderToSupplierMonthSave)