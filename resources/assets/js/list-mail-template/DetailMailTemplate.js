import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Loading from '../common/components/common/Loading';
import ROW_GROUP from '../common/components/table/ROW_GROUP';
import BUTTON from '../common/components/form/BUTTON';
import IFRAME from '../common/components/form/IFRAME';
import {
  Link
} from 'react-router-dom';
import { connect } from 'react-redux';
import * as Action from '../common/actions';
import * as queryString from 'query-string';

class DetailMailTemplate extends Component {
    constructor(props) {
        super(props);
        let params = queryString.parse(props.location.search);
        if (params['mail_id']) {
            this.state = {mail_id: params['mail_id']};
            this.props.reloadData(formDataUrl, this.state);
        } else {
            window.location.href = baseUrl;
        }
    }

    handleCancel = (event) => {
        window.location.href = baseUrl;
    }

    componentDidUpdate() {
        if (!_.isEmpty(this.props.dataSource)) {
            $('.loading').hide();
        }
    }

    componentWillReceiveProps(nextProps) {
        if (!_.isEmpty(nextProps.dataSource)) {
            if (!nextProps.dataSource.data) {
                window.location.href = baseUrl;
            }
        }
    }

    resizeIframe = (event) => {
        let height = 0;
        let currentHeight = event.target.contentWindow.document.body.scrollHeight;
        if (currentHeight > 300) {
            height = 300;
        } else {
            height = currentHeight;
        }
        event.target.style.height = height + 'px';
    }

    render() {
        let dataSource = this.props.dataSource;
        const fields   = ['mail_id', 'name_genre', 'template_name', 'mail_subject', 'mail_content', 'attached_file_path', 'sequence'];
        return (
            (!_.isEmpty(dataSource)) &&
            <div>
            <div className="box box-primary">
                <div className="box-body">
                <div className="row">
                    <div className="col-md-8">
                        <div className="form-horizontal padding-form">
                            <div className="row">
                                <Loading className="loading"></Loading>
                                <div className="col-md-12">
                                {
                                    fields.map((value) => {
                                        let title = value;
                                        if (value === 'mail_id') {
                                            title = 'template_id';
                                        } else if (value === 'mail_subject') {
                                            title = 'template_subject';
                                        } else if (value === 'mail_content') {
                                            return(
                                                    <IFRAME
                                                        key={value}
                                                        name="mail_content"
                                                        labelTitle={trans('messages.receive_mail_content')}
                                                        className="form-control input-sm"
                                                        fieldGroupClass="col-md-9"
                                                        labelClass="col-md-3"
                                                        height="200"
                                                        onLoad={this.resizeIframe}
                                                        src={iframeUrl + "?mail_id=" + this.state.mail_id} />
                                                )
                                        }
                                        return (
                                            <ROW_GROUP
                                                key={value}
                                                classNameLabel={'col-md-3'}
                                                classNameField={'col-md-9 text-new-line'}
                                                label={trans('messages.'+title)}
                                                contents={dataSource.data[value]} />
                                        )
                                    })
                                }
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="col-md-3">
                        <p className=" box-select">
                            <strong>{trans('messages.mail_code_description')}：</strong><br/>
                            {
                                dataSource.mailParam && dataSource.mailParam.map((item) => {
                                    return (
                                        <a href="javascript:void(0);" key={"i" + item}>
                                            {item}<br/>
                                        </a>
                                    )
                                })
                            }
                        </p>
                    </div>
                 </div>
                <div className="row">
                    <div className="col-md-8">

                    </div>
                    <div className="col-md-3">
                        <div className="row">
                            <div className="col-md-4 col-xs-4">
                            {
                                (_.isEmpty(dataSource.data) === true) ? '' :
                                <a href={saveUrl + "?mail_id=" + dataSource.data['mail_id'] } className="btn btn-success input-sm" >{trans('messages.btn_go_to_edit')}</a>
                            }
                            </div>
                            <div className="col-md-4 col-xs-4 text-center">
                            {
                                (_.isEmpty(dataSource.data) === true) ? '' :
                                <a href={saveUrl + "?mail_id_copy=" + dataSource.data['mail_id'] } className="btn btn-primary input-sm" >{trans('messages.btn_dupplicate')}</a>
                            }
                            </div>
                            <div className="col-md-4 col-xs-4">
                            {
                                (_.isEmpty(dataSource.data) === true) ? '' :
                                <BUTTON className="btn bg-purple input-sm pull-right" type="button" onClick={this.handleCancel}>{trans('messages.btn_cancel')}</BUTTON>
                            }
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            </div>
        </div>
        )
    }
}
const mapStateToProps = (state) => {
    return {
        dataSource: state.commonReducer.dataSource
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        reloadData: (url, params) => { dispatch(Action.detailData(url, params)) }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(DetailMailTemplate)