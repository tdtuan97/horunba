import React, { Component } from 'react';

import { connect } from 'react-redux';
import * as Action from '../common/actions';
import * as queryString from 'query-string';

import BUTTON from '../common/components/form/BUTTON';
import FORM from '../common/components/form/FORM';
import INPUT from '../common/components/form/INPUT';
import SELECT from '../common/components/form/SELECT';

import TABLE from '../common/components/table/TABLE';
import THEAD from '../common/components/table/THEAD';
import TBODY from '../common/components/table/TBODY';
import TR from '../common/components/table/TR';
import TH from '../common/components/table/TH';
import TD from '../common/components/table/TD';

import SortComponent from '../common/components/table/SortComponent';

import PaginationContainer from '../common/containers/PaginationContainer'

import Loading from '../common/components/common/Loading';

class ListMailTemplate extends Component {

    constructor(props) {
        super(props);
        const stringParams = queryString.parse(props.location.search);
        this.state = {};
        if (!_.isEmpty(stringParams)) {
            this.state = stringParams;
        }
        this.props.reloadData(dataListUrl, this.state);
    }

    handleChange = (event) => {
        const target = event.target;
        const value = target.type === 'checkbox' ? target.checked : target.value;
        const name = target.name;
        this.setState({
            [name]: (value)?value:undefined
        },() => {
            if (name === 'per_page') {
                this.handleReloadData();
            }
        });
    }

    handleClearClick = (event) => {
        Object.keys(this.state).map((key) => {
            if (key !== 'per_page') {
                delete this.state[key];
            }
        });
        this.handleReloadData();
    }

    handleReloadData = () => {
        if (this.state.page) {
            delete this.state.page;
        }
        this.props.reloadData(dataListUrl, this.state, true);
    }

    componentDidUpdate() {
        if (!_.isEmpty(this.props.dataSource)) {
            $('.loading').hide();
            $('.show-modal-content').customPopover();
        }
    }

    componentWillMount() {
        window.onpopstate = (event) => {
            location.reload();
        };
    }

    handleSortClick = (name, nextSort) => {
        Object.keys(this.state).map((key) => {
            if (key.startsWith('sort_')) {
                delete this.state[key];
            }
        });
        if (nextSort === 'none') {
            delete this.state[name];
            this.props.reloadData(dataListUrl, this.state, true);
        } else {
            this.setState({[name]: nextSort}, () => {
                this.props.reloadData(dataListUrl, this.state, true);
            });
        }

    }

    handleClickPage = (page) => {
        this.setState({
            page: page
        },() => {
            this.props.reloadData(dataListUrl, this.state, true);
        });
    }

    handleChangeMultiSelect = (name, value, checked) => {
        let currentValue = this.state[name];
        if (typeof currentValue === 'undefined') {
            currentValue = [];
        } else if (!Array.isArray(this.state[name])) {
            currentValue = [this.state[name]];
        }
        if (Array.isArray(value)) {
            currentValue = [...value];
        } else {
            let index = currentValue.indexOf(value);
            if (checked === false) {
                if (index !== -1) {
                    currentValue.splice(index, 1);
                }
            } else {
                if (index === -1) {
                    currentValue.push(value);
                }
            }
        }
        this.setState({
            [name]: currentValue,
        });
    }

    render() {
        let dataSource = this.props.dataSource;
        let index = 0;
        let totalItems = 0;
        let itemsPerPage = 20;
        let genreOption = '';
        if (!_.isEmpty(dataSource)) {
            index = dataSource.data.from - 1;
            totalItems   = dataSource.data.total;
            itemsPerPage = dataSource.data.per_page;
            genreOption  = dataSource.genre;
        }
        return ( (!_.isEmpty(dataSource)) &&
            <div>
                <Loading className="loading" />
                <div className="box">
                    <div className="box-header">
                        <div className="row">
                                <div className="col-md-12">
                                    <h4>{trans('messages.search_title')}</h4>
                                </div>
                                <FORM>
                                    <INPUT
                                        name="mail_id"
                                        groupClass="form-group col-md-2"
                                        labelTitle={trans('messages.template_id')}
                                        onChange={this.handleChange}
                                        value={this.state.mail_id||''}
                                        className="form-control input-sm" />
                                    <INPUT
                                        name="mail_subject"
                                        groupClass="form-group col-md-2"
                                        labelTitle={trans('messages.mail_subject')}
                                        onChange={this.handleChange}
                                        value={this.state.mail_subject||''}
                                        className="form-control input-sm" />
                                    <INPUT
                                        name="mail_content"
                                        groupClass="form-group col-md-2-75"
                                        labelTitle={trans('messages.mail_content_search')}
                                        onChange={this.handleChange}
                                        value={this.state.mail_content||''}
                                        className="form-control input-sm" />
                                    <SELECT
                                        id="genre"
                                        name="genre"
                                        groupClass="form-group col-md-2"
                                        labelClass=""
                                        labelTitle={trans('messages.genre')}
                                        handleChangeMultiSelect={this.handleChangeMultiSelect}
                                        valueMulti={this.state.genre||''}
                                        options={genreOption}
                                        multiple="multiple"
                                        className="form-control input-sm" />
                                    <INPUT
                                        name="template_name"
                                        groupClass="form-group col-md-2"
                                        labelTitle={trans('messages.template_name')}
                                        onChange={this.handleChange}
                                        value={this.state.template_name||''}
                                        className="form-control input-sm" />
                                    <SELECT
                                        name="per_page"
                                        groupClass="form-group col-md-1 per-page pull-right-md"
                                        labelClass="pull-right"
                                        labelTitle={trans('messages.per_page')}
                                        onChange={this.handleChange}
                                        value={this.state.per_page||''}
                                        options={{20:20, 40:40, 60:60, 80:80, 100:100}}
                                        className="form-control input-sm" />
                                </FORM>
                        </div>
                        <div className="row">
                            <div className="col-md-9 col-xs-4">
                                <a href={saveUrl} className="btn btn-success input-sm">{trans('messages.btn_add')}</a>
                            </div>
                            <div className="col-md-3 col-xs-8">
                                <BUTTON className="btn btn-danger pull-right" onClick={this.handleClearClick}>{trans('messages.btn_reset')}</BUTTON>
                                <BUTTON className="btn btn-primary pull-right margin-element" onClick={this.handleReloadData} >{trans('messages.btn_search')}</BUTTON>
                            </div>
                        </div>
                    </div>
                    <div className="box-body">
                        <div className="row">
                            <div className="col-md-12">
                                {(totalItems/itemsPerPage > 1) &&
                                <div className="row">
                                    <div className="col-md-2-5"></div>
                                    <div className="col-md-7 text-center">
                                        <PaginationContainer handleClickPage={this.handleClickPage}/>
                                    </div>
                                    <div className="col-md-2-5 text-right line-height-md">
                                        {dataSource.data.from}件 - {dataSource.data.to}件 【全{dataSource.data.total}件】
                                    </div>
                                </div>
                                }
                                <div className="table-responsive">
                                <TABLE className="table table-striped table-custom">
                                    <THEAD>
                                        <TR>
                                            <TH className="visible-xs visible-sm text-center"></TH>
                                            <TH className="visible-xs visible-sm text-center">#</TH>
                                            <TH className="visible-xs visible-sm text-center">
                                                <SortComponent
                                                    name="sort_template_name"
                                                    onClick={this.handleSortClick}
                                                    currentSort={(this.state.sort_template_name)?this.state.sort_template_name:'none'}
                                                    title={trans('messages.template_name')} />
                                            </TH>
                                            <TH className="hidden-xs hidden-sm text-center">＃</TH>
                                            <TH className="hidden-xs hidden-sm text-center col-max-min-60">
                                                <SortComponent
                                                    name="sort_mail_id"
                                                    onClick={this.handleSortClick}
                                                    currentSort={(this.state.sort_mail_id)?this.state.sort_mail_id:'none'}
                                                    title={trans('messages.template_id')} />
                                            </TH>
                                            <TH className="hidden-xs hidden-sm text-center col-max-min-200">
                                                <SortComponent
                                                    name="sort_mail_subject"
                                                    onClick={this.handleSortClick}
                                                    currentSort={(this.state.sort_mail_subject)?this.state.sort_mail_subject:'none'}
                                                    title={trans('messages.mail_subject')} />
                                            </TH>
                                            <TH className="hidden-xs hidden-sm text-center col-max-min-300">
                                                <SortComponent
                                                    name="sort_mail_content"
                                                    onClick={this.handleSortClick}
                                                    currentSort={(this.state.sort_mail_content)?this.state.sort_mail_content:'none'}
                                                    title={trans('messages.mail_content')} />
                                            </TH>
                                            <TH className="hidden-xs hidden-sm text-center col-max-min-120">
                                                <SortComponent
                                                    name="sort_attached_file_path"
                                                    onClick={this.handleSortClick}
                                                    currentSort={(this.state.sort_attached_file_path)?this.state.sort_attached_file_path:'none'}
                                                    title={trans('messages.attached_file_path')} />
                                            </TH>
                                            <TH className="hidden-xs hidden-sm text-center col-max-min-60">
                                                <SortComponent
                                                    name="sort_genre"
                                                    onClick={this.handleSortClick}
                                                    currentSort={(this.state.sort_genre)?this.state.sort_genre:'none'}
                                                    title={trans('messages.genre')} />
                                            </TH>
                                            <TH className="hidden-xs hidden-sm text-center col-max-min-60">
                                                <SortComponent
                                                    name="sort_sequence"
                                                    onClick={this.handleSortClick}
                                                    currentSort={(this.state.sort_sequence)?this.state.sort_sequence:'none'}
                                                    title={trans('messages.sequence')} />
                                            </TH>
                                            <TH className="hidden-xs hidden-sm text-center col-max-min-200">
                                                <SortComponent
                                                    name="sort_template_name"
                                                    onClick={this.handleSortClick}
                                                    currentSort={(this.state.sort_template_name)?this.state.sort_template_name:'none'}
                                                    title={trans('messages.template_name')} />
                                            </TH>
                                            <TH className="hidden-xs hidden-sm text-center col-max-min-120">
                                                <SortComponent
                                                    name="sort_in_date"
                                                    onClick={this.handleSortClick}
                                                    currentSort={(this.state.sort_in_date)?this.state.sort_in_date:'none'}
                                                    title={trans('messages.template_in_date')} />
                                            </TH>
                                            <TH className="hidden-xs hidden-sm text-center col-max-min-120">
                                                <SortComponent
                                                    name="sort_up_date"
                                                    onClick={this.handleSortClick}
                                                    currentSort={(this.state.sort_up_date)?this.state.sort_up_date:'none'}
                                                    title={trans('messages.up_date')} />
                                            </TH>
                                        </TR>
                                    </THEAD>
                                    <TBODY>
                                        {
                                            (!_.isEmpty(dataSource.data.data))?
                                            dataSource.data.data.map((item) => {
                                                index++;
                                                let classRow = (index%2 ===0) ? 'odd' : 'even';
                                                return (
                                                    [
                                                    <TR key={"tr" + index} className={classRow}>
                                                        <TD className="visible-xs visible-sm text-center">
                                                            <b className="show-info">
                                                                <i className="fa fa-angle-double-down" aria-hidden="true"></i>
                                                            </b>
                                                        </TD>
                                                        <TD className="text-center">{index}</TD>
                                                        <TD className="hidden-xs hidden-sm text-center">
                                                            <a className="link-detail" href={detailUrl + "?mail_id=" + item.mail_id}>
                                                                {item.mail_id}

                                                            </a>
                                                        </TD>
                                                        <TD className="hidden-lg">
                                                            <a href={detailUrl + "?mail_id=" + item.mail_id}>
                                                               {item.template_name}
                                                            </a>
                                                        </TD>
                                                        <TD className="hidden-xs hidden-sm max-width-300 limit-char" title={item.mail_subject}>
                                                            {item.mail_subject}
                                                        </TD>
                                                        <TD className="hidden-xs hidden-sm max-width-300" title={item.mail_content}>
                                                            <span className="cut-text limit-char max-w-200 show-modal-content" data-title={item.mail_subject}
                                                            data-content={item.mail_content !== null ? item.mail_content.replace(/(?:\r\n|\r|\n)/g, '<br>') : ''}
                                                            data-toggle="popover"
                                                            data-trigger="hover">
                                                                {item.mail_content}
                                                            </span>
                                                        </TD>
                                                        <TD className="hidden-xs hidden-sm cut-text" title={item.attached_file_path}>
                                                            <span className="limit-char max-width-100">{item.attached_file_path}</span>
                                                        </TD>
                                                        <TD className="hidden-xs hidden-sm text-center" title={item.name_genre}>
                                                            <span className="limit-char max-width-100">{item.name_genre}</span>
                                                        </TD>
                                                        <TD className="hidden-xs hidden-sm text-center" title={item.sequence}>
                                                            <span className="limit-char max-width-100">{item.sequence}</span>
                                                        </TD>
                                                        <TD className="hidden-xs hidden-sm cut-text" title={item.template_name}>
                                                            <span className="limit-char max-width-100">{item.template_name}</span>
                                                        </TD>
                                                        <TD className="hidden-xs hidden-sm text-center">{(new Date(item.in_date)).toISOString().split('T')[0]}</TD>
                                                        <TD className="hidden-xs hidden-sm  text-center">{(new Date(item.up_date)).toISOString().split('T')[0]}</TD>
                                                    </TR>,
                                                    <TR key={"trhide" + index} className="hiden-info" style={{display:"none"}}>
                                                        <TD colSpan="10" className="width-span1">
                                                            <ul id={"temp" + item.mail_id} >
                                                                <li>
                                                                    <b>{trans('messages.template_id')} : </b>
                                                                    {item.mail_id}
                                                                </li>
                                                                <li>
                                                                    <b>{trans('messages.mail_subject')} : </b>
                                                                    {item.mail_subject}
                                                                </li>
                                                                <li>
                                                                    <b>{trans('messages.mail_content')} : </b>
                                                                    {item.mail_content}
                                                                </li>
                                                                <li>
                                                                    <b>{trans('messages.attached_file_path')} : </b>
                                                                    {item.attached_file_path}
                                                                </li>
                                                                <li>
                                                                    <b>{trans('messages.genre')} : </b>
                                                                    {item.genre}
                                                                </li>
                                                                <li>
                                                                    <b>{trans('messages.template_name')} : </b>
                                                                    {item.template_name}
                                                                </li>
                                                                <li>
                                                                    <b>{trans('messages.template_in_date')} : </b>
                                                                    {(new Date(item.in_date)).toISOString().split('T')[0]}
                                                                </li>
                                                                <li>
                                                                    <b>{trans('messages.up_date')} : </b>
                                                                    {(new Date(item.up_date)).toISOString().split('T')[0]}
                                                                </li>
                                                            </ul>
                                                        </TD>
                                                    </TR>
                                                   ]
                                                )
                                            })
                                            :
                                            <TR><TD colSpan="9" className="text-center">{trans('messages.no_data')}</TD></TR>
                                        }
                                    </TBODY>
                                </TABLE>
                                </div>
                                <div className="text-center">
                                    <PaginationContainer handleClickPage={this.handleClickPage}/>
                                </div>
                            </div>
                        </div>
                    </div>
                    </div>
                </div>

        )
    }
}

const mapStateToProps = (state) => {

  return {
    dataSource: state.commonReducer.dataSource
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    reloadData: (url, params, pushHistory, ignoreFields) => { dispatch(Action.reloadData(url, params, pushHistory, ignoreFields)) }
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(ListMailTemplate)