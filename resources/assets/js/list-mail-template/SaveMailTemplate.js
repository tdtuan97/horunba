import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import axios from 'axios';

import * as Action from '../common/actions';
import * as queryString from 'query-string';

import Loading from '../common/components/common/Loading';
import FORM from '../common/components/form/FORM';
import INPUT from '../common/components/form/INPUT';
import SELECT from '../common/components/form/SELECT';
import TEXTAREA from '../common/components/form/TEXTAREA';
import UPLOAD from '../common/components/form/UPLOAD';
import BUTTON from '../common/components/form/BUTTON';
import MESSAGE_MODAL from '../common/containers/MessageModal';

class SaveMailTemplate extends Component {

    constructor(props) {
        super(props);
        this.state = {destination:'mail_content'}
        let params = queryString.parse(props.location.search);
        if (params['mail_id']) {
            this.state.mail_id = params['mail_id'];
            this.state.index = params['mail_id'];
            this.state.accessFlg = true;
            this.hanleRealtime();
        } else if (params['mail_id_copy']) {
            this.state.mail_id_copy = params['mail_id_copy'];
        }
        this.first = true;
        this.showMess = false;
        this.props.reloadData(formDataUrl, this.state);
    }

    handleCancel = (event) => {
        window.location.href = baseUrl;
    }

    handleClear = (event) => {
        this.setState({
            attached_file : '',
            attached_file_path : ''
        });
    }

    hanleRealtime() {
       if (!_.isEmpty(this.state.index)) {
           let params = {
               accessFlg : this.state.accessFlg,
               query : {
                       page_key : 'MAIL-TEMPLATE-SAVE',
                       primary_key : 'index:' + this.state.index
                   },
               key :this.state.index
           };
           if (this.state.accessFlg !== false) {
               this.props.postRealTime(checkEditUrl, params, {'X-Requested-With': 'XMLHttpRequest'});
           }
       }
    }

    handleSubmit = (event) => {
        document.getElementsByName('btnAccept')[0].disabled = true;
        let headers = {'X-Requested-With': 'XMLHttpRequest'};
        if (this.props.enctype) {
            headers['Content-Type'] = this.props.enctype;
        }
        let formData   = new FormData();
        let dataSource = this.props.dataSource;

        if (!_.isEmpty(this.state)) {
            Object.keys(this.state).map((item) => {
                formData.append(item, this.state[item]);
            });
        }
        this.showMess = true;
        this.props.postData(saveUrl, formData, headers);
    }

    setDestination(name = null){
        this.setState({destination : name});
    }

    handleChange = (event) => {
        const target = event.target;
        const name = target.name;
        const value = target.type === 'checkbox' ? target.checked : target.value;
        if (name === "mail_subject") {
           this.setDestination(name);
        } else if (name === "mail_content") {
           this.setDestination(name);
        }
        if (target.type === 'file') {
            let tmpName = name.replace(new RegExp("^browser_"), '');
            this.setState({
                [tmpName]: target.files[0].name,
                ['attached_file']: target.files[0]
            }, ()=> {
                document.getElementsByName(tmpName)[0].value = target.files[0].name;
            });
        } else {
            this.setState({
                [name]: value
            }, () => {
                if (name === 'genre') {
                    let headers = {'X-Requested-With': 'XMLHttpRequest'};
                    let params = { genre : _.trim(this.state.genre), mail_id : _.trim(this.state.mail_id) };
                    $(".loading").show();
                    axios.post(getMaxSequenceUrl, params, {headers : headers}
                    ).then(response => {
                        $(".loading").hide();
                        if (response.data.flg === 1) {
                            this.setState({sequence : response.data.data + 1});
                        }
                    }).catch(error => {
                        $(".loading").hide();
                        throw(error);
                    });
                }
            });
        }
    }

    handleTextClick = (item, event) => {
        let element = document.getElementsByName(this.state.destination)[0];
        let cursorPosition = element.selectionEnd;
        let value = element.value;
        let output = [value.slice(0, cursorPosition), item, value.slice(cursorPosition)].join('');
        this.setState({
            [this.state.destination]: output
        },() => {
            element.value = output;
            element.focus();
            element.selectionEnd = (cursorPosition + item.length);
        });
    }

    componentWillReceiveProps(nextProps) {
        if (!_.isEmpty(nextProps.dataSource)) {
            if (!nextProps.dataSource.data) {
                window.location.href = baseUrl;
            } else if (this.first) {
                this.setState({...nextProps.dataSource.data});
                this.first = false;
            }
        }
        if (!_.isEmpty(nextProps.dataResponse)) {
            if (nextProps.dataResponse.status === 1) {
                window.location.href = detailUrl + '?mail_id=' + nextProps.dataResponse.mail_id;
            } else {
                if (nextProps.dataResponse.message && nextProps.dataResponse.message.check_unique_mail_template && this.showMess) {
                    $.notify({
                        icon: 'glyphicon glyphicon-warning-sign',
                        message: nextProps.dataResponse.message.check_unique_mail_template,
                    },{
                        type: 'danger',
                        delay: 3000
                    });
                    this.showMess = false;
                }
                document.getElementsByName('btnAccept')[0].disabled = false;
            }
        }
        // Check realtime update
        if (!_.isEmpty(nextProps.dataRealTime)) {
            this.setState(nextProps.dataRealTime);
        }
    }

    componentWillUnmount () {
        clearInterval(this.state);
    }
    componentDidMount() {
        setInterval(() => {
            this.hanleRealtime()
        }, 5000);

    }

    componentDidUpdate() {
        if (!_.isEmpty(this.props.dataSource)) {
            $('.loading').hide();
        }
        if (this.state.accessFlg === false) {
            $('#message-modal').modal('show');
            //We will auto redirect after 5 seconds
            setTimeout(() => {
                window.location.href = baseUrl;
            }, 5000);
        }
    }

    render() {
        let token        = $("meta[name='csrf-token']").attr("content");
        let dataSource   = this.props.dataSource;
        let dataResponse = this.props.dataResponse;
        return ((!_.isEmpty(dataSource)) &&
            <div>
            <div className="box box-primary">
            <FORM
                className="form-horizontal padding-form"
                encType={"multipart/form-data; boundary=----WebKitFormBoundary" + token}>
                <div className="box-body">
                <Loading className="loading" />
                <div className="row">
                    <div className="col-md-8">
                        <INPUT
                            name="mail_id"
                            labelTitle={trans('messages.template_id')}
                            className="form-control input-sm"
                            fieldGroupClass="col-md-3"
                            labelClass="col-md-3"
                            onChange={this.handleChange}
                            defaultValue={dataSource.data.mail_id}
                            hasError={(dataResponse.message && dataResponse.message.mail_id)?true:false}
                            errorMessage={(dataResponse.message&&dataResponse.message.mail_id)?dataResponse.message.mail_id:""}
                            disabled/>
                        <SELECT
                            name="genre"
                            labelTitle={trans('messages.genre')}
                            className="form-control input-sm"
                            fieldGroupClass="col-md-3"
                            labelClass="col-md-3"
                            onChange={this.handleChange}
                            options={dataSource.genre}
                            defaultValue={dataSource.data.genre}
                            errorPopup={true}
                            hasError={(dataResponse.message && dataResponse.message.genre)?true:false}
                            errorMessage={(dataResponse.message&&dataResponse.message.genre)?dataResponse.message.genre:""}
                            />
                        <INPUT
                            name="template_name"
                            labelTitle={trans('messages.template_name')}
                            className="form-control input-sm"
                            fieldGroupClass="col-md-3"
                            labelClass="col-md-3"
                            onChange={this.handleChange}
                            defaultValue={dataSource.data.template_name}
                            errorPopup={true}
                            hasError={(dataResponse.message && dataResponse.message.template_name)?true:false}
                            errorMessage={(dataResponse.message&&dataResponse.message.template_name)?dataResponse.message.template_name:""}/>
                        <INPUT
                            name="mail_subject"
                            labelTitle={trans('messages.save_timing_mail_subject')}
                            className="form-control input-sm"
                            fieldGroupClass="col-md-9"
                            labelClass="col-md-3"
                            onClick={this.handleChange}
                            onChange={this.handleChange}
                            defaultValue={dataSource.data.mail_subject}
                            errorPopup={true}
                            hasError={(dataResponse.message && dataResponse.message.mail_subject)?true:false}
                            errorMessage={(dataResponse.message&&dataResponse.message.mail_subject)?dataResponse.message.mail_subject:""}/>
                        <TEXTAREA
                            name="mail_content"
                            labelTitle={trans('messages.mail_content')}
                            className="form-control input-sm"
                            fieldGroupClass="col-md-9"
                            labelClass="col-md-3"
                            onClick={this.handleChange}
                            onChange={this.handleChange}
                            defaultValue={dataSource.data.mail_content}
                            errorPopup={true}
                            hasError={(dataResponse.message && dataResponse.message.mail_content)?true:false}
                            errorMessage={(dataResponse.message&&dataResponse.message.mail_content)?dataResponse.message.mail_content:""}
                            rows="20"/>
                        <UPLOAD
                            name="attached_file_path"
                            labelTitle={trans('messages.attached_file_path')}
                            className="form-control input-sm"
                            fieldGroupClass="col-md-9"
                            labelClass="col-md-3"
                            clear={true}
                            onClick={this.handleClear}
                            btnClear={trans('messages.btn_clear')}
                            itemGroupClass="input-group input-group-sm"
                            btnClass="btn btn-info btn-flat btn-largest"
                            btnTitle={trans('messages.btn_browser')}
                            onChange={this.handleChange}
                            value={this.state.attached_file_path || ''}
                            hasError={(dataResponse.message && dataResponse.message['attached_file'])?true:false}
                            errorMessage={(dataResponse.message&&dataResponse.message['attached_file'])?dataResponse.message['attached_file']:""}/>
                        <INPUT
                            name="sequence"
                            labelTitle={trans('messages.sequence')}
                            className="form-control input-sm"
                            fieldGroupClass="col-md-3"
                            labelClass="col-md-3"
                            onChange={this.handleChange}
                            value={this.state.sequence || ''}
                            errorPopup={true}
                            hasError={(dataResponse.message && dataResponse.message.sequence)?true:false}
                            errorMessage={(dataResponse.message&&dataResponse.message.sequence)?dataResponse.message.sequence:""}/>
                    </div>
                    <div className="col-md-3">
                        <p className="box-select">
                            <strong>{trans('messages.mail_code_description')}：</strong><br/>
                            {
                                dataSource.mailParam.map((item) => {
                                    return (
                                        <a href="javascript:void(0);" key={"i" + item} onClick={(event) => this.handleTextClick(item, event)}>
                                            {item}<br/>
                                        </a>
                                    )
                                })
                            }
                        </p>
                    </div>
                </div>
                <div className="row">
                    <div className="col-md-8">

                    </div>
                    <div className="col-md-3">
                        <div className="row">
                            <div className="col-md-4 col-xs-4">
                                <BUTTON name="btnAccept" className="btn btn-success input-sm btn-larger" type="button" onClick={this.handleSubmit}>{trans('messages.btn_accept')}</BUTTON>
                            </div>
                            <div className="col-md-4 col-xs-4 text-center">
                            {
                                (dataSource.data['mail_id'])&&
                                <a href={saveUrl + "?mail_id_copy=" + dataSource.data['mail_id'] } className="btn btn-primary input-sm" >{trans('messages.btn_dupplicate')}</a>
                            }
                            </div>
                            <div className="col-md-4 col-xs-4 pull-right">
                                <BUTTON className="btn bg-purple input-sm pull-right btn-larger" type="button" onClick={this.handleCancel}>{trans('messages.btn_cancel')}</BUTTON>
                            </div>
                        </div>
                    </div>
                </div>
                </div>
            </FORM>
            <MESSAGE_MODAL
                message={this.state.message || ''}
                title={trans('messages.deny_edit')}
                baseUrl={baseUrl}
            />
            </div>
            </div>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        dataSource: state.commonReducer.dataSource,
        dataResponse: state.commonReducer.dataResponse,
        dataRealTime: state.commonReducer.dataRealTime
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        reloadData: (url, params) => { dispatch(Action.detailData(url, params)) },
        postData: (url, params, headers) => { dispatch(Action.postData(url, params, headers)) },
        postRealTime: (url, params, headers) => { dispatch(Action.postRealTime(url, params, headers)) }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(SaveMailTemplate)