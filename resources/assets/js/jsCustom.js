const showFunction = () => {
    $(document).on('click','b.show-info', function(){
        var iClass = $(this).find('i').attr('class');
        if (iClass === 'fa fa-angle-double-down') {
            $(this).find('i').removeClass('fa-angle-double-down')
                             .addClass('fa-angle-double-up');
        } else {
            $(this).find('i').removeClass('fa-angle-double-up')
                             .addClass('fa-angle-double-down');
        }
        $(this).closest('tr').next('.hiden-info').toggle();
        $(this).closest('tr').next().next('.hiden-info').toggle();

    });


    $("textarea[name=comment_content]").focus();
    $.fn.resizeble = function() {
         var pressed = false;
         var start = undefined;
         var startX, startWidth;
         var dataThead = null;

         this.mousedown(function(e) {
         var parentHeight = $(this).parent().height();
         var childHeight = $(this).next().height();
         var line_height = (Math.round(((parentHeight - childHeight) / 2) * 2)) + 1;
             start = $(this);
             pressed = true;
             startX = e.pageX;
             startWidth = $(this).width();
             $(start).addClass("resizing");

         });
         $(document).mousemove(function(e) {
             if(pressed) {
                var w = startWidth+(e.pageX-startX)+"px !important;"
                $(start).attr('style','min-width:'+w);
             }
         });
         $(document).mouseup(function() {
             if(pressed) {
                 $(start).removeClass("resizing");
                 pressed = false;
             }
         });
     };
    $.fn.customPopover = function(options = {}) {
          var popoverTemplate = [
            '<div class="popover popover-custom-width" role="tooltip">',
            '<div class="arrow"></div>',
            '<h3 class="popover-title popover-custom-title"></h3>',
            '<div class="popover-content "></div></div>'
        ].join('');
        var delayOpt = options.delay;
        var delayShow = 500;
        var delayHide = 2000;
        if (typeof delayOpt !== 'undefined') {
            if (typeof delayOpt.show !== 'undefined') {
                delayShow = delayOpt.show;
            }
            if (typeof delayOpt.hide !== 'undefined') {
                delayHide = delayOpt.hide;
            }
        }
        this.popover({
            trigger: typeof options.trigger !== 'undefined'? options.trigger : 'hover',
            animation: true,
            html: true,
            delay: { "show": delayShow, "hide": delayHide },
            container: 'body',
            placement: typeof this.data('placement') !== 'undefined' ? this.data('placement')  : 'left',
            template : popoverTemplate,
            content : function () {

            }
        });
     };
    $.fn.customPopoverManual = function(options = {}) {
          var popoverTemplate = [
            '<div class="popover popover-custom-width" role="tooltip">',
            '<div class="arrow"></div>',
            '<h3 class="popover-title popover-custom-title"></h3>',
            '<div class="popover-content "></div></div>'
        ].join('');
        this.popover({
            trigger: 'manual',
            html: true,
            animation: true,
            container: 'body',
            placement: typeof this.data('placement') !== 'undefined' ? this.data('placement')  : 'left',
            template : popoverTemplate,
        }).on('mouseenter', function () {
            var _this = this;
            $(this).popover('show');
            $('.popover').on('mouseleave', function () {
                $(_this).popover('hide');
            });
        }).on('mouseleave', function () {
            var _this = this;
            setTimeout(function () {
                if (!$('.popover:hover').length) {
                    $(_this).popover('hide');
                }
            }, 100);
        });
     };
}
export {showFunction};
