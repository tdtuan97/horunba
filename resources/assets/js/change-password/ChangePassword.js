import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Form from '../common/containers/Form';
import configureStore from '../common/configureStore';
import { Provider } from 'react-redux';
const store = configureStore();
export default class ChangePassword extends Component {

    render() {
        const config = {
            url: {
              baseUrl: '/reset-password',
            },

            form: {
                formClass: 'form-horizontal',
                fields: [
                    {
                        typeField: "INPUT",
                        label: "現在パスワード",
                        classLabel: "col-md-4 control-label",
                        name: "old_tantou_password",
                        type: "password",
                        title: "",
                        defaultValue: "",
                        placeholder: "",
                        fieldClass: "form-control",
                        groupClass: "form-group own-instruction-password"
                    },
                    {
                        typeField: "LABEL",
                        classLabel: "col-md-4 control-label",
                        name: "instruction",
                        fieldClass: "form-lable",
                        groupClass: "form-group",
                        child: <div className="instruction-password">※6文字以上<br/>※半角英数字のみ<br/>※「大文字」「小文字」「数字」は必ず使用してください</div>,
                    },
                    {
                        typeField: "INPUT",
                        label: "新たなパスワード",
                        classLabel: "col-md-4 control-label",
                        name: "tantou_password",
                        type: "password",
                        title: "",
                        defaultValue: "",
                        placeholder: "",
                        fieldClass: "form-control",
                        groupClass: "form-group"
                    },
                    {
                        typeField: "INPUT",
                        label: "もう一度入力して下さい",
                        classLabel: "col-md-4 control-label",
                        name: "tantou_password_confirmation",
                        type: "password",
                        title: "",
                        defaultValue: "",
                        placeholder: "",
                        fieldClass: "form-control",
                        groupClass: "form-group"
                    },
                ],
                submitButton: {
                    title: "パスワード変更",
                    isVisible: true,
                    type: "BUTTON",
                    buttonClass: "btn btn-primary",
                    groupClass: "form-group"
                },
            },
            type: "Form",
        }
        return (
            <Provider store={store}>
              <Form  config={config} />
            </Provider>//
        )
    }
}
