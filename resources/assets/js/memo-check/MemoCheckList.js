import React, { Component } from 'react';

import { connect } from 'react-redux';
import * as Action from '../common/actions';
import * as queryString from 'query-string';

import BUTTON from '../common/components/form/BUTTON';
import FORM from '../common/components/form/FORM';
import INPUT from '../common/components/form/INPUT';
import SELECT from '../common/components/form/SELECT';
import LABEL from '../common/components/form/LABEL';
import TEXTAREA from '../common/components/form/TEXTAREA';
import DATEPICKER from '../common/components/form/DATEPICKER';

import TABLE from '../common/components/table/TABLE';
import THEAD from '../common/components/table/THEAD';
import TBODY from '../common/components/table/TBODY';
import TR from '../common/components/table/TR';
import TH from '../common/components/table/TH';
import TD from '../common/components/table/TD';
import moment from 'moment';

import SortComponent from '../common/components/table/SortComponent';
import PaginationContainer from '../common/containers/PaginationContainer'
import Loading from '../common/components/common/Loading';
import axios from 'axios';
import AcceptPopup from './containers/AcceptPopup';

class MemoCheckList extends Component {

    constructor(props) {
        super(props);
        const stringParams = queryString.parse(props.location.search);
        this.state = {};
        if (!_.isEmpty(stringParams)) {
            this.state = stringParams;
        }
        this.props.reloadData(dataListUrl, this.state);
    }

    handleChange = (index, event) => {
        const target = event.target;
        const value = target.type === 'checkbox' ? target.checked : target.value;
        const name = target.name;
        this.setState({
            [name]: (value)?value:''
        },() => {
            if (name === 'per_page' || name === 'status' || name === 'deparment') {
                this.handleReloadData();
            }
        });
    }

    handleClearClick = (event) => {
        Object.keys(this.state).map((key) => {
            if (key !== 'per_page') {
                delete this.state[key];
            }
        });
        this.handleReloadData();
    }
    handleSubmit = (index,event) => {
        let headers = {'X-Requested-With': 'XMLHttpRequest'};
        let formData   = new FormData();
        let dataSource = this.props.dataSource;

        if ((!_.isEmpty(dataSource.data))) {
            Object.keys(dataSource.data).map((item, i) => {
                formData.append(item, dataSource.data[item]);
            });
        }
        if (!_.isEmpty(this.state)) {
            Object.keys(this.state).map((item) => {
                formData.append(item, this.state[item]);
            });
        }
        this.props.postData(saveUrl, formData, headers);
    }
    handleUpdate = (logId,event) => {
        let headers = {'X-Requested-With': 'XMLHttpRequest'};
        let formData   = new FormData();
        let dataSource = this.props.dataSource;
        const target = event.target;
        const value = target.type === 'checkbox' ? target.checked : target.value;
        const name = target.name;
        if ((!_.isEmpty(dataSource.data))) {
            Object.keys(dataSource.data).map((item) => {
                formData.append(item, dataSource.data[item]);
            });
        }
        if (!_.isEmpty(this.state)) {
            Object.keys(this.state).map((item) => {
                formData.append(item, this.state[item]);
            });
        }
        if (name === 'log_status') {
            formData.append('log_status' , $(target).text());
        }
        if (name === 'log_incharge_person') {
           formData.append('up_ope_cd' , $(target).val());
        }
        formData.append('log_id' , logId);
        this.props.postData(saveUrl, formData,
                    headers,
                    this.handleReloadData
                    );
    }
    handleBeforeSent = (logId,event) => {
        $('#access-memo-modal').modal('show');
        const target = event.target;
        const name = target.name;
        this.setState({
            log_title    : $(target).closest('td').find('input').val(),
            log_contents : $(target).closest('td').find('textarea').val(),
            log_id       : logId
        });
    }
    handleUpdateLog = () => {
        this.props.postData(saveUrl, this.state,
                    {'X-Requested-With': 'XMLHttpRequest'},
                    this.handleReloadData
                    );
        $('#access-memo-modal').modal('hide');
    }

    handleReloadData = () => {
        if (this.state.page) {
            delete this.state.page;
        }
        let dataSource = this.props.dataSource;
        if ((!_.isEmpty(dataSource.data.data))) {
            Object.keys(dataSource.data.data).map((item, i) => {
                let logTitle = 'log_title' + i;
                let logContents = 'log_contents' + i;
                delete this.state[logTitle];
                delete this.state[logContents];
            });
        }

        this.props.reloadData(dataListUrl, this.state, true, ['log_title', 'log_contents', 'log_id']);
    }
    handleChangeMultiSelect = (name, value, checked) => {
        let currentValue = this.state[name];
        if (typeof currentValue === 'undefined') {
            currentValue = [];
        } else if (!Array.isArray(this.state[name])) {
            currentValue = [this.state[name]];
        }
        if (Array.isArray(value)) {
            currentValue = [...value];
        } else {
            let index = currentValue.indexOf(value);
            if (checked === false) {
                if (index !== -1) {
                    currentValue.splice(index, 1);
                }
            } else {
                if (index === -1) {
                    currentValue.push(value);
                }
            }
        }
        this.setState({
            [name]: currentValue,
        });
    }
    componentWillReceiveProps(nextProps) {
        if (!_.isEmpty(nextProps.dataSource)) {
            if (!_.isEmpty(nextProps.dataSource.data)) {
                this.countMemo();
            }
        }
    }
    componentDidUpdate() {
        if (!_.isEmpty(this.props.dataSource)) {
            $('.loading').hide();
        }
        $('table.table-striped.table-custom.table-hover').on('click','tr', function(){
            $('table.table-striped.table-custom.table-hover tr').removeClass('active');
            $(this).addClass('active');

        })

        $('body').on('click', 'button.modal-custom-button, button.btn.btn-sm.btn-warning' , function (){
            var me = $('table.table-striped.table-custom.table-hover').find('tr.active').removeClass('active');
            me.find('.tool-bar').fadeOut("slow");
        });
    }
    countMemo = () => {
        axios.post(countMemoUrl, [], {headers: {'X-Requested-With': 'XMLHttpRequest'}})
        .then(response => {
            if (response.data.count) {
                $('.label-notify').html(response.data.count).addClass('label-danger');
            } else {
                 $('.label-notify').html('').removeClass('label-danger');
            }
        }).catch(error => {
            throw(error);
        });
    };
    componentWillMount() {
        window.onpopstate = (event) => {
            location.reload();
        };
    }
    handleChangeDate = function(date, name, event) {
        let dateValue = moment(date);
        if (!dateValue.isValid()) {
            dateValue = undefined;
        } else {
            dateValue = dateValue.format('YYYY-MM-DD HH:mm:ss');
        }
        this.setState({
          [name]: dateValue,
        });
    }

    handleClickPage = (page) => {
        this.setState({
            page: page
        },() => {
            this.props.reloadData(dataListUrl, this.state, true, ['log_title', 'log_contents', 'log_id']);
        });
    }

    render() {
        let nf = new Intl.NumberFormat();
        let dataSource = this.props.dataSource;
        let dataResponse = this.props.dataResponse;
        let index = 0;
        let totalItems = 0;
        let itemsPerPage = 20;
        let mallOptions = {};
        let opeOptions = {}
        let searchLogStatus = {
            0 : '未',
            1 : '済',
        };
        if (!_.isEmpty(dataSource)) {
            index = dataSource.data.from - 1;
            totalItems   = dataSource.data.total;
            itemsPerPage = dataSource.data.per_page;
            mallOptions  = dataSource.mallData;
            opeOptions = dataSource.opeData;
        }

        return ( (!_.isEmpty(dataSource)) &&
            <div>
                <Loading className="loading" />
                <div className="box">
                    <div className="box-header">
                        <div className="row">
                            <div className="col-md-12">
                                <h4>{trans('messages.search_title')}</h4>
                                <FORM>
                                    <div className="row">
                                        <SELECT
                                            id="name_jp"
                                            name="name_jp"
                                            groupClass="form-group col-md-1-5"
                                            labelClass=""
                                            labelTitle={trans('messages.name_jp')}
                                            handleChangeMultiSelect={this.handleChangeMultiSelect}
                                            valueMulti={this.state.name_jp||''}
                                            options={mallOptions}
                                            multiple="multiple"
                                            className="form-control input-sm" />
                                        <SELECT
                                            id="operator"
                                            name="operator"
                                            groupClass="form-group col-md-1-5"
                                            labelClass=""
                                            labelTitle={trans('messages.operator')}
                                            handleChangeMultiSelect={this.handleChangeMultiSelect}
                                            valueMulti={this.state.operator||''}
                                            options={opeOptions}
                                            multiple="multiple"
                                            className="form-control input-sm" />
                                        <DATEPICKER
                                            placeholderText="YYYY-MM-DD HH:mm:ss"
                                            dateFormat="YYYY-MM-DD HH:mm:ss"
                                            showTimeSelect
                                            timeFormat="HH:mm"
                                            timeIntervals={15}
                                            timeCaption="time"
                                            locale="ja"
                                            name="in_date_from"
                                            groupClass="form-group col-md-2 date-time"
                                            labelTitle={trans('messages.in_date_from')}
                                            className="form-control input-sm"
                                            selected={this.state.in_date_from ? moment(this.state.in_date_from) : null}
                                            onChange={(date, name, e) => this.handleChangeDate(date, 'in_date_from', e)} />
                                        <DATEPICKER
                                            placeholderText="YYYY-MM-DD HH:mm:ss"
                                            dateFormat="YYYY-MM-DD HH:mm:ss"
                                            showTimeSelect
                                            timeFormat="HH:mm"
                                            timeIntervals={15}
                                            timeCaption="time"
                                            locale="ja"
                                            name="in_date_to"
                                            groupClass="form-group col-md-2 date-time"
                                            labelTitle={trans('messages.in_date_to')}
                                            className="form-control input-sm"
                                            selected={this.state.in_date_to ? moment(this.state.in_date_to) : null}
                                            onChange={(date, name, e) => this.handleChangeDate(date, 'in_date_to', e)} />
                                        <INPUT
                                            name="search_log_title"
                                            groupClass="form-group col-md-2"
                                            labelTitle={trans('messages.memo_log_title')}
                                            onChange={(e) => this.handleChange(1,e)}
                                            value={this.state.search_log_title||''}
                                            className="form-control input-sm" />
                                        <INPUT
                                            name="search_log_content"
                                            groupClass="form-group col-md-2"
                                            labelTitle={trans('messages.memo_log_contents')}
                                            onChange={(e) => this.handleChange(1,e)}
                                            value={this.state.search_log_content||''}
                                            className="form-control input-sm" />
                                        <SELECT
                                            name="per_page"
                                            groupClass="form-group col-md-1 per-page pull-right decrement-mgl-1"
                                            labelClass="pull-left"
                                            labelTitle={trans('messages.per_page')}
                                            onChange={(e) => this.handleChange(1,e)}
                                            value={this.state.per_page||''}
                                            options={{20:20, 40:40, 60:60, 80:80, 100:100}}
                                            className="form-control input-sm" />
                                    </div>
                                    <div className="row">
                                        <SELECT
                                            id="search_log_status"
                                            name="search_log_status"
                                            groupClass="form-group col-md-1-5"
                                            labelTitle={trans('messages.log_memo_status')}
                                            handleChangeMultiSelect={this.handleChangeMultiSelect}
                                            valueMulti={this.state.search_log_status || ''}
                                            options={searchLogStatus}
                                            multiple="multiple"
                                            className="form-control input-sm" />
                                    </div>
                                </FORM>
                            </div>
                        </div>
                        <div className="row">
                            <div className="col-md-9 col-xs-4">

                            </div>
                            <div className="col-md-3 col-xs-8">
                                <BUTTON className="btn btn-danger pull-right" onClick={this.handleClearClick}>{trans('messages.btn_reset')}</BUTTON>
                                <BUTTON className="btn btn-primary pull-right margin-element" onClick={this.handleReloadData} >{trans('messages.btn_search')}</BUTTON>
                            </div>
                        </div>
                    </div>
                    <div className="box-body">
                        <div className="row">
                            <div className="col-md-12">
                                {(totalItems/itemsPerPage > 1) &&
                                <div className="row">
                                    <div className="col-md-2-5"></div>
                                    <div className="col-md-7 text-center">
                                        <PaginationContainer handleClickPage={this.handleClickPage}/>
                                    </div>
                                    <div className="col-md-2-5 text-right line-height-md">
                                        {dataSource.data.from}件 - {dataSource.data.to}件 【全{dataSource.data.total}件】
                                    </div>
                                </div>
                                }
                                <div className="table-responsive">
                                <TABLE className="table table-striped table-custom table-hover" style={{"minWidth" : "900px"}}>
                                    <TBODY>
                                        {
                                            (!_.isEmpty(dataSource.data.data))?
                                            dataSource.data.data.map((item, i) => {
                                                index++;
                                                let logTitle = 'log_title'+i;
                                                let logContent = 'log_contents'+i;
                                                let status = item.log_status === 1 ? '済' : '未';
                                                let statusColor = item.log_status === 1 ? 'primary' : 'danger';
                                                let orderDate = !_.isEmpty(item.order_date) ? moment.utc(item.order_date).format("YYYY/MM/DD") : '';
                                                let inDate = !_.isEmpty(item.in_date) ? moment.utc(item.in_date).format("YYYY/MM/DD HH:mm:ss") : '';
                                                let classRow = (index%2 ===0) ? 'odd' : 'even';
                                                return (
                                                    [
                                                    <tr key={"tr1" + index} className={classRow}>
                                                        <TD className="text-left col-max-min-130">
                                                            <INPUT
                                                                name="name_jp"
                                                                groupClass="row margin-form"
                                                                labelTitle={trans('messages.name_jp')}
                                                                value={item.name_jp||''}
                                                                readOnly
                                                                fieldGroupClass="col-md-9 clear-padding"
                                                                labelClass="col-md-3 clear-padding"
                                                                className="form-control input-sm" />
                                                            <INPUT
                                                                name="person_name"
                                                                groupClass="row margin-form"
                                                                labelTitle={trans('messages.order_person_name')}
                                                                value={item.full_name||''}
                                                                readOnly
                                                                fieldGroupClass="col-md-9 clear-padding"
                                                                labelClass="col-md-3 clear-padding"
                                                                className="form-control input-sm" />
                                                            <div className="row margin-form">
                                                                <div className="col-md-12 clear-padding">
                                                                    <a href={orderDetailUrl + '?receive_id_key=' + item.receive_id } className="btn btn-sm btn-success pull-right" onClick={this.handleClearClick}>{trans('messages.btn_move_detail')}</a>
                                                                </div>
                                                            </div>
                                                        </TD>
                                                        <TD className="text-left  col-max-min-130">
                                                            <INPUT
                                                                name="order_date"
                                                                groupClass="row margin-form"
                                                                labelTitle={trans('messages.payment_order_date')}
                                                                value={orderDate}
                                                                readOnly
                                                                fieldGroupClass="col-md-8 clear-padding"
                                                                labelClass="col-md-4 clear-padding"
                                                                className="form-control input-sm" />
                                                            <INPUT
                                                                name="create_date"
                                                                groupClass="row margin-form"
                                                                labelTitle={trans('messages.template_in_date')}
                                                                value={inDate}
                                                                readOnly
                                                                fieldGroupClass="col-md-8 clear-padding"
                                                                labelClass="col-md-4 clear-padding"
                                                                className="form-control input-sm" />
                                                            <div className="row margin-form">
                                                                <div className="col-md-12 text-right clear-padding">
                                                                    <label  className="margin-element">{trans('messages.log_memo_status')}</label>
                                                                    <BUTTON name="log_status" data-value={item.log_status} className={"btn btn-sm btn-"+statusColor} onClick={(e) => this.handleUpdate(item.log_id,e)}>{status}</BUTTON>
                                                                </div>
                                                            </div>
                                                        </TD>
                                                        <TD className="text-left  col-max-min-130">
                                                            <INPUT
                                                                name="status_name"
                                                                groupClass="row margin-form"
                                                                labelTitle={trans('messages.status')}
                                                                value={item.status_name}
                                                                readOnly
                                                                fieldGroupClass="col-md-8 clear-padding"
                                                                labelClass="col-md-4 clear-padding"
                                                                className="form-control input-sm" />
                                                            <INPUT
                                                                name="person_name"
                                                                groupClass="row margin-form"
                                                                labelTitle={trans('messages.operator')}
                                                                value={item.tantou_last_name}
                                                                readOnly
                                                                fieldGroupClass="col-md-8 clear-padding"
                                                                labelClass="col-md-4 clear-padding"
                                                                className="form-control input-sm" />
                                                            <SELECT
                                                                name="log_incharge_person"
                                                                labelTitle={trans('messages.log_incharge_person')}
                                                                groupClass="row margin-form"
                                                                fieldGroupClass="col-md-8 clear-padding"
                                                                labelClass="col-md-4 clear-padding"
                                                                className="form-control input-sm"
                                                                value={item.up_ope_cd}
                                                                onChange={(e) => this.handleUpdate(item.log_id, e)}
                                                                data-target={item.up_ope_cd}
                                                                options={dataSource.tantouOptions}
                                                                />
                                                        </TD>
                                                        <TD className="text-left">
                                                            <INPUT
                                                                key={"_inputlog_title"+i}
                                                                name={"log_title"+i}
                                                                groupClass="row margin-form"
                                                                labelTitle={trans('messages.memo_title')}
                                                                value={_.has(this.state,logTitle) ? this.state[logTitle] : item.log_title  || ''}
                                                                onChange={(e) => this.handleChange(i, e)}
                                                                fieldGroupClass="col-md-10 clear-padding"
                                                                labelClass="col-md-2 clear-padding"
                                                                className="form-control input-sm" />
                                                            <TEXTAREA
                                                                key={"input_log_contents"+i}
                                                                name={"log_contents"+i}
                                                                groupClass="row margin-form"
                                                                labelTitle={trans('messages.mail_content_search')}
                                                                fieldGroupClass="col-md-10 clear-padding"
                                                                labelClass="col-md-2 clear-padding"
                                                                className="form-control input-sm"
                                                                onChange={(e) => this.handleChange(i, e)}
                                                                value={this.state[logContent] || ''}
                                                                value={_.has(this.state,logContent) ? this.state[logContent] : item.log_contents  || ''}
                                                                errorPopup={true}
                                                                hasError={(dataResponse.messages && dataResponse.messages.log_contents)?true:false}
                                                                errorMessage={(dataResponse.messages&&dataResponse.messages.log_contents)?dataResponse.messages.log_contents:""}
                                                                rows="6"/>
                                                            <div className="row tool-bar" style={{'display' : 'none'}}>
                                                                <div className="col-md-6 col-md-offset-6 text-right clear-padding">
                                                                        <div className="pull-right">
                                                                            <BUTTON name="btn_update" className={"btn btn-sm btn-primary margin-element"} onClick={(e) => this.handleBeforeSent(item.log_id, e)}>{trans('messages.btn_confirm')}</BUTTON>
                                                                            <BUTTON className={"btn btn-sm btn-warning"} onClick={this.handleClearClick}>{trans('messages.btn_cancel')}</BUTTON>
                                                                        </div>
                                                                </div>
                                                            </div>
                                                        </TD>
                                                    </tr>
                                                   ]
                                                )
                                                if (index === dataSource.data.to) {
                                                   index = 0;
                                                }
                                            })
                                            :
                                            <TR><TD colSpan="4" className="text-center">{trans('messages.no_data')}</TD></TR>
                                        }
                                    </TBODY>
                                </TABLE>
                                </div>
                                <div className="text-center">
                                    <PaginationContainer handleClickPage={this.handleClickPage}/>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            <AcceptPopup
                handleUpdateLog={this.handleUpdateLog}
            />
            </div>

        )
    }
}

const mapStateToProps = (state) => {
    return {
        dataSource: state.commonReducer.dataSource,
        dataResponse: state.commonReducer.dataResponse
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        reloadData: (url, params, pushHistory, ignoreFields) => { dispatch(Action.reloadData(url, params, pushHistory, ignoreFields)) },
        postData: (url, params, headers, callBackUrl, callBackParams) => { dispatch(Action.postData(url, params, headers, callBackUrl, callBackParams)) }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(MemoCheckList)