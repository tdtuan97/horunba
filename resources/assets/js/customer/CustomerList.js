import React, {Component} from 'react';

import {connect} from 'react-redux';
import * as Action from '../common/actions';
import * as queryString from 'query-string';
import axios from 'axios';

import BUTTON from '../common/components/form/BUTTON';
import FORM from '../common/components/form/FORM';
import INPUT from '../common/components/form/INPUT';
import SELECT from '../common/components/form/SELECT';

import TABLE from '../common/components/table/TABLE';
import THEAD from '../common/components/table/THEAD';
import TBODY from '../common/components/table/TBODY';
import TR from '../common/components/table/TR';
import TH from '../common/components/table/TH';
import TD from '../common/components/table/TD';

import SortComponent from '../common/components/table/SortComponent';
import PaginationContainer from '../common/containers/PaginationContainer'
import Loading from '../common/components/common/Loading';
import ImportCsv from "../payment/csv/ImportCsv";
import AcceptPopup from "./containers/AcceptPopup";

class CustomerList extends Component {

    constructor(props) {
        super(props);
        const stringParams = queryString.parse(props.location.search);
        this.state = {};
        if (!_.isEmpty(stringParams)) {
            this.state = stringParams;
        }
        this.props.reloadData(dataListUrl, this.state);
    };

    handleChange = (event) => {
        const target = event.target;
        let value = target.value;
        if (value === '_none') {
            value = '';
        }
        let name = target.name;
        this.setState({
            [name]: value
        }, () => {
            if (name === 'per_page' || name === 'order_status' || name === 'name_jp') {
                this.handleReloadData();
            }
        });
    };
    handleClearClick = (event) => {
        Object.keys(this.state).map((key) => {
            if (key !== 'per_page') {
                delete this.state[key];
            }
        });
        this.handleReloadData();
    };
    handleClickPage = (page) => {
        this.setState({
            page: page
        }, () => {
            this.props.reloadData(dataListUrl, this.state, true);
        });
    };
    handleSortClick = (name, nextSort) => {
        Object.keys(this.state).map((key) => {
            if (key.startsWith('sort_')) {
                delete this.state[key];
            }
        });
        if (nextSort === 'none') {
            delete this.state[name];
            this.handleReloadData();
        } else {
            this.setState({[name]: nextSort}, () => {
                this.handleReloadData();
            });
        }
    };
    handleReloadData = (a) => {
        let params = {};
        let dataSource = this.props.dataSource;
        let tmpParams = this.state;
        Object.keys(tmpParams).map(key => {
            if (tmpParams[key] !== ""
                && tmpParams[key] !== null
                && key !== 'flg'
                && key !== 'attached_file_path'
                && key !== 'attached_file'
                && key !== 'page') {
                params[key] = tmpParams[key];
            } else {
                delete params[key];
            }
        });
        this.props.reloadData(dataListUrl, params, true);
    };
    handlePopupDelete = (event, customer_id) => {
        this.setState({
            customer_id: customer_id,
        }, () => {
            $('#access-modal').modal('show');
        });
    };
    handleSubmitPopup = () => {
        $(".loading").show();
        axios.post(deleteUrl, {customer_id: this.state.customer_id}, {headers: {'X-Requested-With': 'XMLHttpRequest'}})
            .then(response => {
                delete this.state.customer_id;
                if (response.data.status === 1) {
                    this.handleReloadData();
                }
                $(".loading").hide();
                $('#access-modal').modal('hide');
            }).catch(error => {
            $(".loading").hide();
            throw(error);
        });
    };
    handleExportCsv = () => {
        $(".loading").show();
        let param = {...this.state};
        param['type'] = 'isCsv';
        let headers = {'X-Requested-With': 'XMLHttpRequest'};
        axios.post(processExportCSV, param, {headers : headers}
        ).then(response => {
            $(".loading").hide();
            if (response.data.status === 1) {
                window.location.href = exportCSV +"?fileName="+ response.data.file_name;
            }
        }).catch(error => {
            $(".loading").hide();
            throw(error);
        });
    };
    handleCancelPopup = () => {
        delete this.state.customer_id;
        $('#access-modal').modal('hide');
    };

    componentDidUpdate() {
        if (!_.isEmpty(this.props.dataSource)) {
            $('.loading').hide();
            $('.show-modal-content').customPopover();
        }
    };

    componentWillMount() {
        window.onpopstate = (event) => {
            location.reload();
        };
    };

    render() {
        let dataSource = this.props.dataSource;
        let index = 0;
        let totalItems = 0;
        let itemsPerPage = 20;
        let genreOption = '';
        if (!_.isEmpty(dataSource)) {
            index = dataSource.data.from - 1;
            totalItems = dataSource.data.total;
            itemsPerPage = dataSource.data.per_page;
        }
        return ((!_.isEmpty(dataSource)) &&
            <div>
                <Loading className="loading"/>
                <div className="box">
                    <div className="box-header">
                        <div className="row">
                            <div className="col-md-12">
                                <h4>{trans('messages.search_title')}</h4>
                            </div>
                            <div className="row">
                                <FORM>
                                    <div className="col-md-12">
                                        <INPUT
                                            name="customer_id"
                                            groupClass="form-group col-md-1-75"
                                            labelTitle="Customer ID"
                                            onChange={this.handleChange}
                                            value={this.state.customer_id || ''}
                                            className="form-control input-sm"/>
                                        <INPUT
                                            name="last_name_kana"
                                            groupClass="form-group col-md-1-75"
                                            labelTitle="Last Name Kana"
                                            onChange={this.handleChange}
                                            value={this.state.last_name_kana || ''}
                                            className="form-control input-sm"/>
                                        <INPUT
                                            name="email"
                                            groupClass="form-group col-md-1-75"
                                            labelTitle="Email"
                                            onChange={this.handleChange}
                                            value={this.state.email || ''}
                                            className="form-control input-sm"/>
                                        <INPUT
                                            name="tel_num"
                                            groupClass="form-group col-md-1-75"
                                            labelTitle="Tel Number"
                                            onChange={this.handleChange}
                                            value={this.state.tel_num || ''}
                                            className="form-control input-sm"/>
                                        <INPUT
                                            name="prefecture"
                                            groupClass="form-group col-md-1-75"
                                            labelTitle="Prefecture"
                                            onChange={this.handleChange}
                                            value={this.state.prefecture || ''}
                                            className="form-control input-sm"/>
                                        <INPUT
                                            name="city"
                                            groupClass="form-group col-md-1-75"
                                            labelTitle="City"
                                            onChange={this.handleChange}
                                            value={this.state.city || ''}
                                            className="form-control input-sm"/>
                                    </div>
                                    <div className="col-md-12">
                                        <INPUT
                                            name="sub_address"
                                            groupClass="form-group col-md-1-75"
                                            labelTitle="Sub Address"
                                            onChange={this.handleChange}
                                            value={this.state.sub_address || ''}
                                            className="form-control input-sm"/>
                                        <SELECT
                                            name="per_page"
                                            groupClass="form-group col-md-1 per-page pull-right-md"
                                            labelClass="pull-left"
                                            labelTitle={trans('messages.per_page')}
                                            onChange={this.handleChange}
                                            value={this.state.per_page || ''}
                                            options={{20: 20, 40: 40, 60: 60, 80: 80, 100: 100}}
                                            className="form-control input-sm"/>
                                    </div>
                                </FORM>
                            </div>
                        </div>
                        <div className="row">
                            <div className="col-md-6 col-xs-12">
                                <a href={saveUrl} className="btn btn-success  margin-element margin-form ">{trans('messages.btn_add')}</a>
                                <BUTTON className="btn btn-primary margin-form  margin-element" onClick={(e) => this.handleExportCsv()}>{trans('messages.csv_export')}</BUTTON>
                            </div>
                            <div className="col-md-6 col-xs-12">
                                <BUTTON className="btn btn-danger pull-right"
                                        onClick={this.handleClearClick}>{trans('messages.btn_reset')}</BUTTON>
                                <BUTTON className="btn btn-primary pull-right margin-element"
                                        onClick={this.handleReloadData}>{trans('messages.btn_search')}</BUTTON>
                            </div>
                        </div>
                    </div>
                    <div className="box-body">
                        <div className="row">
                            <div className="col-md-12">
                                {(totalItems / itemsPerPage > 1) &&
                                <div className="row">
                                    <div className="col-md-2-5"></div>
                                    <div className="col-md-7 text-center">
                                        <PaginationContainer handleClickPage={this.handleClickPage}/>
                                    </div>
                                    <div className="col-md-2-5 text-right line-height-md">
                                        {dataSource.data.from}件 - {dataSource.data.to}件 【全{dataSource.data.total}件】
                                    </div>
                                </div>
                                }
                                <div className="table-responsive">
                                    <TABLE className="table table-striped table-custom">
                                        <THEAD>
                                            <TR>
                                                <TH className="visible-xs visible-sm text-center"></TH>
                                                <TH className="text-center"></TH>
                                                <TH className="text-center text-middle col-max-min-120">
                                                    <SortComponent
                                                        name="sort_customer_id"
                                                        onClick={this.handleSortClick}
                                                        currentSort={(this.state.sort_customer_id) ? this.state.sort_customer_id : 'none'}
                                                        title="Customer ID"/>
                                                </TH>
                                                <TH className="text-center text-middle col-max-min-200">
                                                    <SortComponent
                                                        name="sort_last_name_kana"
                                                        onClick={this.handleSortClick}
                                                        currentSort={(this.state.sort_last_name_kana) ? this.state.sort_last_name_kana : 'none'}
                                                        title="Last Name Kana"/>
                                                </TH>
                                                <TH className="hidden-xs hidden-sm text-center text-middle col-max-min-200">
                                                    <SortComponent
                                                        name="sort_email"
                                                        onClick={this.handleSortClick}
                                                        currentSort={(this.state.sort_email) ? this.state.sort_email : 'none'}
                                                        title="Email"/>
                                                </TH>
                                                <TH className="text-center text-middle col-max-min-120">
                                                    <SortComponent
                                                        name="sort_tel_num"
                                                        onClick={this.handleSortClick}
                                                        currentSort={(this.state.sort_tel_num) ? this.state.sort_tel_num : 'none'}
                                                        title="Tel Number"/>
                                                </TH>
                                                <TH className="hidden-xs hidden-sm text-center text-middle col-max-min-100">
                                                    <SortComponent
                                                        name="sort_prefecture"
                                                        onClick={this.handleSortClick}
                                                        currentSort={(this.state.sort_prefecture) ? this.state.sort_prefecture : 'none'}
                                                        title="Prefecture"/>
                                                </TH>
                                                <TH className="hidden-xs hidden-sm text-center text-middle col-max-min-100">
                                                    <SortComponent
                                                        name="sort_city"
                                                        onClick={this.handleSortClick}
                                                        currentSort={(this.state.sort_city) ? this.state.sort_city : 'none'}
                                                        title="City"/>
                                                </TH>
                                                <TH className="hidden-xs hidden-sm text-center text-middle col-max-min-300">
                                                    <SortComponent
                                                        name="sort_sub_address"
                                                        onClick={this.handleSortClick}
                                                        currentSort={(this.state.sort_sub_address) ? this.state.sort_sub_address : 'none'}
                                                        title="Sub Address"/>
                                                </TH>
                                            </TR>
                                        </THEAD>
                                        <TBODY>
                                            {
                                                (!_.isEmpty(dataSource.data.data)) ?
                                                    dataSource.data.data.map((item) => {
                                                        index++;
                                                        let classRow = (index % 2 === 0) ? 'odd' : 'even';
                                                        return (
                                                            [
                                                                <TR key={"tr" + index} className={classRow}>
                                                                    <TD className="visible-xs visible-sm text-center">
                                                                        <b className="show-info">
                                                                            <i className="fa fa-angle-double-down"
                                                                               aria-hidden="true"></i>
                                                                        </b>
                                                                    </TD>
                                                                    <TD className="text-center" title="Edit info">
                                                                        <a onClick={(e) => this.handlePopupDelete(e, item.customer_id)}>
                                                                            <i className="fa fa-fw fa-trash text-danger"></i>
                                                                        </a>
                                                                    </TD>
                                                                    <TD className="cut-text text-center"
                                                                        title={item.customer_id}>
                                                                        <a
                                                                            href={saveUrl + "?" + (this.curStrQuery ? (this.curStrQuery + "&") : "") + "customer_id=" + item.customer_id}
                                                                            className="link-detail"
                                                                        >{item.customer_id}</a>
                                                                    </TD>
                                                                    <TD className="cut-text"
                                                                        title={item.last_name_kana}>{item.last_name_kana}</TD>
                                                                    <TD className="hidden-xs hidden-sm cut-text"
                                                                        title={item.email}>{item.email}</TD>
                                                                    <TD className="cut-text text-center"
                                                                        title={item.tel_num}>{item.tel_num}</TD>
                                                                    <TD className="hidden-xs hidden-sm cut-text text-center"
                                                                        title={item.prefecture}>{item.prefecture}</TD>
                                                                    <TD className="hidden-xs hidden-sm cut-text"
                                                                        title={item.city}>{item.city}</TD>
                                                                    <TD className="hidden-xs hidden-sm max-width-200"
                                                                        title={item.sub_address}>
                                                                        <span key={"popup_content_" + item.customer_id}
                                                                              className="cut-text limit-char max-w-200 show-modal-content"
                                                                              data-title="Sub Address"
                                                                              data-content={item.sub_address !== null ? item.sub_address.replace(/(?:\r\n|\r|\n)/g, '<br>') : ''}
                                                                              data-toggle="popover"
                                                                              data-placement="left"
                                                                        >{item.sub_address}
                                                                        </span>
                                                                    </TD>
                                                                </TR>,
                                                                <TR key={"trhide" + index} className="hiden-info"
                                                                    style={{display: "none"}}>
                                                                    <TD colSpan="10" className="width-span1">
                                                                        <ul id={"temp" + item.customer_id}>
                                                                            <li><b>NO</b> : #{item.customer_id}</li>
                                                                            <li><b>Last Name
                                                                                Kana</b> : {item.last_name_kana}</li>
                                                                            <li><b>Email</b> : {item.email}</li>
                                                                            <li><b>Tel Number</b> : {item.tel_num}</li>
                                                                            <li><b>Prefecture</b> : {item.prefecture}
                                                                            </li>
                                                                            <li><b>City</b> : {item.city}</li>
                                                                            <li><b>Sub Address</b> : {item.sub_address}
                                                                            </li>
                                                                        </ul>
                                                                    </TD>
                                                                </TR>
                                                            ]

                                                        );
                                                        if (index === dataSource.data.to) {
                                                            index = 0;
                                                        }
                                                    })
                                                    :
                                                    <TR><TD colSpan="12"
                                                            className="text-center">{trans('messages.no_data')}</TD></TR>
                                            }
                                        </TBODY>
                                    </TABLE>
                                </div>
                                <div className="text-center">
                                    <PaginationContainer handleClickPage={this.handleClickPage}/>
                                </div>
                                <ImportCsv
                                    handleChooseFile={this.handleChooseFile}
                                    handleImportSubmit={this.handleImportSubmit}
                                    value={this.state.attached_file_path ? this.state.attached_file_path : ''}
                                    error={this.state.error}
                                    filename={this.state.filename}
                                    handleImportCancel={this.handleImportCancel}
                                    process={this.state.process ? true : false}
                                    status={this.state.status_csv}
                                    percent={this.state.percent}
                                    hasError={(this.state.message || this.state.msg) ? true : false}
                                    errorMessage={this.state.message || this.state.msg}
                                />
                            </div>
                        </div>
                    </div>
                </div>
                <AcceptPopup
                    customer_id={this.state.customer_id}
                    handleSubmit={this.handleSubmitPopup}
                    handleCancel={this.handleCancelPopup}
                />
            </div>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        dataSource: state.commonReducer.dataSource
    }
}

const mapDispatchToProps = (dispatch) => {
    let ignore = [''];
    return {
        reloadData: (url, params, pushHistory, ignoreFields) => {
            dispatch(Action.reloadData(url, params, pushHistory, ignore))
        },
        postData: (url, params, headers, callback, callParam) => {
            dispatch(Action.postData(url, params, headers, callback, callParam))
        },
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(CustomerList)