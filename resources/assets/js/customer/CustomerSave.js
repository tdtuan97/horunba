import React, {Component} from 'react';
import {connect} from 'react-redux';

import * as Action from '../common/actions';
import * as queryString from 'query-string';

import Loading from '../common/components/common/Loading';
import FORM from '../common/components/form/FORM';
import INPUT from '../common/components/form/INPUT';
import BUTTON from '../common/components/form/BUTTON';
import TEXTAREA from '../common/components/form/TEXTAREA'
import MESSAGE_MODAL from '../common/containers/MessageModal';
import axios from 'axios';

let timer;

class CustomerSave extends Component {

    constructor(props) {
        super(props);
        this.state = {};
        let params = queryString.parse(props.location.search);
        if (params['customer_id']) {
            this.state = {
                customer_id: params['customer_id'],
            };
            this.state.accessFlg = true;
        }
        console.log(this.state);
        this.props.reloadData(formDataUrl, this.state);
    }

    handleRealtime() {
        if (this.state.repay_no) {
            let params = {
                accessFlg: this.state.accessFlg,
                query: {
                    page_key: 'CUSTOMER-SAVE',
                    primary_key: 'index:' + this.state.customer_id
                },
                key: this.state.repay_no
            };
            if (this.state.accessFlg !== false) {
                this.props.postRealTime(checkEditUrl, params, {'X-Requested-With': 'XMLHttpRequest'});
            }
        }
    }

    componentWillReceiveProps(nextProps) {
        let dataSource = nextProps.dataSource;
        if (!_.isEmpty(dataSource.data) && _.isEmpty(nextProps.dataRealTime)) {
            this.setState({...dataSource.data}, () => {
                this.handleRealtime();
            });
        }
        if (!_.isEmpty(nextProps.dataResponse.method) && nextProps.dataResponse.method === 'save') {
            if (nextProps.dataResponse.status === 1) {
                console.log('susscess');
                window.location.href = baseUrl;
            } else {
                document.getElementsByName('btnAccept')[0].disabled = false;
            }
        }
        // Check realtime update
        if (!_.isEmpty(nextProps.dataRealTime)) {
            this.setState(nextProps.dataRealTime);
        }
    }

    handleChange = (event) => {
        const target = event.target;
        const name = target.name;
        const value = target.type === 'checkbox' ? target.checked : target.value;
        let dataSetState = {[name]: value};
        this.setState(dataSetState);
    };
    handleCancel = (event) => {
        window.location.href = baseUrl;
    };
    handleSubmit = (event) => {
        this.setState({dataError: ''});
        document.getElementsByName('btnAccept')[0].disabled = true;
        let headers = {'X-Requested-With': 'XMLHttpRequest'};
        if (this.props.enctype) {
            headers['Content-Type'] = this.props.enctype;
        }
        this.props.postData(this.props.location.pathname, this.state, headers);
    };

    componentDidUpdate() {
        if (!_.isEmpty(this.props.dataSource)) {
            $('.loading').hide();
        }
        if (this.state.accessFlg === false) {
            $('#message-modal').modal('show');
            //We will auto redirect after 5 seconds
            setTimeout(() => {
                window.location.href = baseUrl;
            }, 5000);
        }
    }

    componentWillUnmount() {
        clearInterval(this.state);
    }

    componentDidMount() {
        setInterval(() => {
            this.handleRealtime()
        }, 5000);

    }

    render() {
        let dataSource = this.props.dataSource;
        let dataResponse = this.state.dataError ? this.state.dataError : this.props.dataResponse;
        return ((!_.isEmpty(dataSource)) &&
            <div>
                <div className="box box-primary">
                    <FORM className="form-horizontal ">
                        <div className="box-body">
                            <Loading className="loading"></Loading>
                            <div className="row">
                                <div className="col-md-5">
                                    <div className="row">
                                        <div className="col-md-12">
                                            <INPUT
                                                name="first_name"
                                                labelTitle="First Name"
                                                className="form-control input-sm"
                                                fieldGroupClass="col-md-8"
                                                labelClass="col-md-4"
                                                groupClass=""
                                                errorPopup={true}
                                                onChange={this.handleChange}
                                                defaultValue={dataSource.data.first_name}
                                                hasError={dataResponse.message && dataResponse.message.first_name ? true : false}
                                                errorMessage={dataResponse.message && dataResponse.message.first_name ? dataResponse.message.first_name : ""}
                                            />
                                        </div>

                                        <div className="col-md-12">
                                            <INPUT
                                                name="first_name_kana"
                                                labelTitle="First Name Kana"
                                                className="form-control input-sm"
                                                fieldGroupClass="col-md-8"
                                                labelClass="col-md-4"
                                                groupClass=""
                                                errorPopup={true}
                                                onChange={this.handleChange}
                                                defaultValue={dataSource.data.first_name_kana}
                                                hasError={dataResponse.message && dataResponse.message.first_name_kana ? true : false}
                                                errorMessage={dataResponse.message && dataResponse.message.first_name_kana ? dataResponse.message.first_name_kana : ""}
                                            />
                                        </div>
                                        <div className="col-md-12">
                                            <INPUT
                                                name="email"
                                                labelTitle="Email"
                                                className="form-control input-sm"
                                                fieldGroupClass="col-md-8"
                                                labelClass="col-md-4"
                                                groupClass=""
                                                errorPopup={true}
                                                onChange={this.handleChange}
                                                defaultValue={dataSource.data.email}
                                                hasError={dataResponse.message && dataResponse.message.email ? true : false}
                                                errorMessage={dataResponse.message && dataResponse.message.email ? dataResponse.message.email : ""}
                                            />
                                        </div>
                                        <div className="col-md-12">
                                            <INPUT
                                                name="tel_num"
                                                labelTitle="Tel Number"
                                                className="form-control input-sm"
                                                fieldGroupClass="col-md-8"
                                                labelClass="col-md-4"
                                                groupClass=""
                                                errorPopup={true}
                                                onChange={this.handleChange}
                                                defaultValue={dataSource.data.tel_num}
                                                hasError={dataResponse.message && dataResponse.message.tel_num ? true : false}
                                                errorMessage={dataResponse.message && dataResponse.message.tel_num ? dataResponse.message.tel_num : ""}
                                            />
                                        </div>
                                        <div className="col-md-12">
                                            <INPUT
                                                name="fax_num"
                                                labelTitle="Fax Number"
                                                className="form-control input-sm"
                                                fieldGroupClass="col-md-8"
                                                labelClass="col-md-4"
                                                groupClass=""
                                                errorPopup={true}
                                                onChange={this.handleChange}
                                                defaultValue={dataSource.data.fax_num}
                                                hasError={dataResponse.message && dataResponse.message.fax_num ? true : false}
                                                errorMessage={dataResponse.message && dataResponse.message.fax_num ? dataResponse.message.fax_num : ""}
                                            />
                                        </div>
                                        <div className="col-md-12">
                                            <INPUT
                                                name="zip_code"
                                                labelTitle="Zip Code"
                                                className="form-control input-sm"
                                                fieldGroupClass="col-md-8"
                                                labelClass="col-md-4"
                                                groupClass=""
                                                errorPopup={true}
                                                onChange={this.handleChange}
                                                defaultValue={dataSource.data.zip_code}
                                                hasError={dataResponse.message && dataResponse.message.zip_code ? true : false}
                                                errorMessage={dataResponse.message && dataResponse.message.zip_code ? dataResponse.message.zip_code : ""}
                                            />
                                        </div>
                                    </div>
                                </div>
                                <div className="col-md-7">
                                    <div className="col-md-12">
                                        <INPUT
                                            name="last_name"
                                            labelTitle="Last Name"
                                            className="form-control input-sm"
                                            fieldGroupClass="col-md-8"
                                            labelClass="col-md-4"
                                            groupClass=""
                                            errorPopup={true}
                                            onChange={this.handleChange}
                                            defaultValue={dataSource.data.last_name}
                                            hasError={dataResponse.message && dataResponse.message.last_name ? true : false}
                                            errorMessage={dataResponse.message && dataResponse.message.last_name ? dataResponse.message.first_name : ""}
                                        />
                                    </div>
                                    <div className="col-md-12">
                                        <INPUT
                                            name="last_name_kana"
                                            labelTitle="Last Name Kana"
                                            className="form-control input-sm"
                                            fieldGroupClass="col-md-8"
                                            labelClass="col-md-4"
                                            groupClass=""
                                            errorPopup={true}
                                            onChange={this.handleChange}
                                            defaultValue={dataSource.data.last_name_kana}
                                            hasError={dataResponse.message && dataResponse.message.last_name_kana ? true : false}
                                            errorMessage={dataResponse.message && dataResponse.message.last_name_kana ? dataResponse.message.last_name_kana : ""}
                                        />
                                    </div>
                                    <div className="col-md-12">
                                        <INPUT
                                            name="prefecture"
                                            labelTitle="Prefecture"
                                            className="form-control input-sm"
                                            fieldGroupClass="col-md-8"
                                            labelClass="col-md-4"
                                            groupClass=""
                                            errorPopup={true}
                                            onChange={this.handleChange}
                                            defaultValue={dataSource.data.prefecture}
                                            hasError={dataResponse.message && dataResponse.message.prefecture ? true : false}
                                            errorMessage={dataResponse.message && dataResponse.message.prefecture ? dataResponse.message.prefecture : ""}
                                        />
                                    </div>
                                    <div className="col-md-12">
                                        <INPUT
                                            name="city"
                                            labelTitle="City"
                                            className="form-control input-sm"
                                            fieldGroupClass="col-md-8"
                                            labelClass="col-md-4"
                                            groupClass=""
                                            errorPopup={true}
                                            onChange={this.handleChange}
                                            defaultValue={dataSource.data.city}
                                            hasError={dataResponse.message && dataResponse.message.city ? true : false}
                                            errorMessage={dataResponse.message && dataResponse.message.city ? dataResponse.message.city : ""}
                                        />
                                    </div>
                                    <div className="col-md-12">
                                        <TEXTAREA
                                            name="sub_address"
                                            labelTitle="Sub Address"
                                            className="form-control input-sm"
                                            fieldGroupClass="col-md-8"
                                            labelClass="col-md-4"
                                            groupClass=""
                                            errorPopup={true}
                                            onChange={this.handleChange}
                                            defaultValue={dataSource.data.sub_address}
                                            hasError={dataResponse.message && dataResponse.message.sub_address ? true : false}
                                            errorMessage={dataResponse.message && dataResponse.message.sub_address ? dataResponse.message.sub_address : ""}
                                        />
                                    </div>
                                    <div className="row">
                                        <div className="col-md-12">
                                            <BUTTON className="btn bg-purple input-sm pull-right btn-larger"
                                                    type="button" onClick={this.handleCancel}>{"取消"}</BUTTON>
                                            <BUTTON name="btnAccept"
                                                    className="btn btn-success input-sm btn-larger pull-right margin-element"
                                                    type="button" onClick={this.handleSubmit}>{"登録"}</BUTTON>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </FORM>
                    <MESSAGE_MODAL
                        message={this.state.message || ''}
                        title={trans('messages.deny_edit')}
                        baseUrl={baseUrl}
                    />
                </div>
            </div>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        dataSource: state.commonReducer.dataSource,
        dataResponse: state.commonReducer.dataResponse,
        dataRealTime: state.commonReducer.dataRealTime
    }
};

const mapDispatchToProps = (dispatch) => {
    return {
        reloadData: (url, params) => {
            dispatch(Action.detailData(url, params))
        },
        postData: (url, params, headers) => {
            dispatch(Action.postData(url, params, headers))
        },
        postRealTime: (url, params, headers) => {
            dispatch(Action.postRealTime(url, params, headers))
        }
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(CustomerSave)