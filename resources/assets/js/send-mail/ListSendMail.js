import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';

import { connect } from 'react-redux';
import * as Action from '../common/actions';
import createHistory from 'history/createBrowserHistory';
import BUTTON from '../common/components/form/BUTTON';
import FORM from '../common/components/form/FORM';
import INPUT from '../common/components/form/INPUT';
import SELECT from '../common/components/form/SELECT';

import TABLE from '../common/components/table/TABLE';
import THEAD from '../common/components/table/THEAD';
import TBODY from '../common/components/table/TBODY';
import TR from '../common/components/table/TR';
import TH from '../common/components/table/TH';
import TD from '../common/components/table/TD';
import SORT from '../common/components/table/SORT';
import PaginationContainer from '../common/containers/PaginationContainer'

import Loading from '../common/components/common/Loading';
class ListSendMail extends Component {

    constructor(props) {
        super(props);

        let location = decodeURIComponent(window.location.href);
        let arrLoca = location.split('?');
        this.state = {baseUrl: arrLoca[0]}
        let params = {};
        if (arrLoca.length > 1) {
            arrLoca[1].split('&').map(item => {
                let tmp = item.split('=');
                if (tmp.length === 2) {
                    this.state[tmp[0]] = tmp[1];
                    params[tmp[0]] = tmp[1];
                }
            })
        }

        this.props.reloadData(this.state.baseUrl + "/dataList", params);
    }

    handleChange = (event) => {
        const target = event.target;
        const value = target.type === 'checkbox' ? target.checked : target.value;
        const name = target.name;
        this.setState({
            [name]: value
        },() => {
            if (name === 'per_page' || name === 'send_status' ) {
                this.handleReloadData();
            }
        });
    }

    handleClearClick = (event) => {
        this.setState({
            receive_order_id : '',
            order_status_id: '',
            order_sub_status_id: '',
            mail_subject: '',
            send_status: '',
            sort: '',
        }, () => {
            this.handleReloadData();
        });
    }
    sort = (column_id, name) => {
        let dataSource  = this.props.dataSource;
        let currentSort = (dataSource.sort[column_id] !== undefined) ? dataSource.sort[column_id] : 'none';
        let nextSort    = "asc";
        let sort = (this.isJsonString(dataSource.sort) === false) ? dataSource.sort : JSON.parse(JSON.parse(JSON.stringify(dataSource.sort)));
        if (dataSource.sorted === true) {
            if (typeof sort[column_id] !== undefined ) {
                if (sort[column_id] === 'asc') {
                    currentSort = 'asc';
                    nextSort    = "desc";
                } else if (sort[column_id] === 'desc') {
                    currentSort = "desc";
                    nextSort    = "none";
                }
            }
        }
        return  <SORT
                    onClick={(e)=>this.handleSortClick(column_id, nextSort)}
                    className={
                    (currentSort === "none")?
                    "fa fa-arrows-v":
                    (currentSort === "asc")?
                        "fa fa-sort-amount-asc":
                        "fa fa-sort-amount-desc"
                }> <span>{name}</span></SORT>
    }
    handleSortClick = (column, nextSort) => {
        let params = {};
        let dataSource = this.props.dataSource;
        Object.keys(dataSource.params).map(key => {
            if (dataSource.params[key] !== "" && dataSource.params[key] !== null) {
                if (this.isJsonString(dataSource.params[key])) {
                    params[key] = JSON.parse(dataSource.params[key]);
                } else {
                    params[key] = dataSource.params[key];
                }

            } else {
                delete params[key];
            }
        });
        if (nextSort === 'none') {
            delete params["sort"][column];
        } else if (params["sort"]) {
            Object.assign(params["sort"], {[column]: nextSort});
        } else {
            params["sort"] = {[column]: nextSort};
        }
        let search = [];
        Object.keys(params).map((key) => {
            if (typeof params[key] === 'object') {
                Object.keys(params[key]).map((k) => {
                    search.push(key + "[" + k + "]=" +  params[key][k]);
                });
            } else {
                search.push(key + "=" + params[key]);
            }
        });
        const history = createHistory();
        history.push({
            search: '?' + search.join("&")
        });
        this.props.reloadData(this.state.baseUrl + "/dataList", params);
    }

    isJsonString(str) {
        try {
            JSON.parse(str);
        } catch (e) {
            return false;
        }
        return true;
    }
    handleReloadData = () => {
        let params = {};
        let dataSource = this.props.dataSource;
        Object.keys(this.state).map(key => {
            if (this.state[key] !== ""
                    && this.state[key] !== null
                    && key !== 'baseUrl'
                    && key !== 'page') {
                params[key] = this.state[key];
            } else {
                delete params[key];
            }
        });
        let search = [];
        Object.keys(params).map((key) => {
            if (typeof params[key] === 'object') {
                Object.keys(params[key]).map((k) => {
                    search.push(key + "[" + k + "]=" +  params[key][k]);
                });
            } else {
                search.push(key + "=" + params[key]);
            }
        });
        const history = createHistory();
        history.push({
            search: '?' + search.join("&")
        });
        this.props.reloadData(this.state.baseUrl + "/dataList", params);
    }

    componentDidUpdate() {
        if (!_.isEmpty(this.props.dataSource)) {
            $('.loading').hide();
        }
    }

    componentWillMount() {
        window.onpopstate = (event) => {
            location.reload();
        };
    }
    render() {
        const config = {
            url: {
              baseUrl: this.state.baseUrl + "/dataList"
            },
            pagination: {
                paginationClass: "pagination",
                disabledClass: "disabled",
                activeClass: "active"
            }
        }
        let dataSource = this.props.dataSource;
        let index = 0;
        if (!_.isEmpty(dataSource)) {
            index = dataSource.data.from - 1;
        }
        return ( (!_.isEmpty(dataSource)) &&
            <div>
                <Loading className="loading" />
                <div className="box">
                    <div className="box-header">
                        <div className="row">
                            <div className="col-md-12">
                                <div className="col-md-12">
                                    <h4>{trans('messages.search_title')}</h4>
                                </div>
                                <FORM>
                                    <INPUT
                                        name="receive_order_id"
                                        groupClass="form-group col-md-2"
                                        labelTitle={trans('messages.receive_order_id')}
                                        onChange={this.handleChange}
                                        value={this.state.receive_order_id||''}
                                        className="form-control input-sm" />
                                    <INPUT
                                        name="order_status_id"
                                        groupClass="form-group col-md-2"
                                        labelTitle={trans('messages.order_status_id')}
                                        onChange={this.handleChange}
                                        value={this.state.order_status_id||''}
                                        className="form-control input-sm" />
                                    <INPUT
                                        name="order_sub_status_id"
                                        groupClass="form-group col-md-3"
                                        labelTitle={trans('messages.order_sub_status_id')}
                                        onChange={this.handleChange}
                                        value={this.state.order_sub_status_id||''}
                                        className="form-control input-sm" />
                                    <INPUT
                                        name="mail_subject"
                                        groupClass="form-group col-md-2"
                                        labelTitle={trans('messages.mail_subject')}
                                        onChange={this.handleChange}
                                        value={this.state.mail_subject||''}
                                        className="form-control input-sm" />
                                    <SELECT
                                        name="send_status"
                                        groupClass="form-group col-md-2"
                                        labelClass=""
                                        labelTitle={trans('messages.send_status')}
                                        onChange={this.handleChange}
                                        value={this.state.send_status||''}
                                        options={{'':'--Choose--',0:'未', 1: '済'}}
                                        className="form-control input-sm" />
                                    <SELECT
                                        name="per_page"
                                        groupClass="form-group col-md-1"
                                        labelClass="pull-right"
                                        labelTitle={trans('messages.per_page')}
                                        onChange={this.handleChange}
                                        value={this.state.per_page||''}
                                        options={{20:20, 40:40, 60:60, 80:80, 100:100}}
                                        className="form-control input-sm" />
                                </FORM>
                            </div>
                        </div>
                    </div>
                    <div className="box-body">
                        <div className="row">
                            <div className="col-md-9 col-xs-4">
                            </div>
                            <div className="col-md-3 col-xs-8">
                                    <BUTTON className="btn btn-danger  pull-right margin-element" onClick={this.handleClearClick}>{trans('messages.btn_reset')}</BUTTON>
                                    <BUTTON className="btn btn-primary  pull-right margin-element" onClick={this.handleReloadData} >{trans('messages.btn_search')}</BUTTON>
                            </div>
                        </div>
                        <div className="row">
                            <div className="col-md-12">
                                <div className="text-right box-total-item">
                                    <p>{dataSource.data.from}件 - {dataSource.data.to}件 【全{dataSource.data.total}件】</p>
                                </div>
                                <div className="text-center">
                                    <PaginationContainer config={config} />
                                </div>
                                <TABLE className="table table-striped table-custom">
                                    <THEAD>
                                        <TR>
                                            <TH className="visible-xs visible-sm"></TH>
                                            <TH className="visible-xs visible-sm">#</TH>
                                            <TH className="visible-xs visible-sm">
                                                {this.sort('receive_order_id',trans('messages.receive_order_id'))}
                                            </TH>
                                            <TH className="hidden-xs hidden-sm text-center">＃</TH>
                                            <TH className="hidden-xs hidden-sm text-center">
                                                {this.sort('receive_order_id',trans('messages.receive_order_id'))}
                                            </TH>
                                            <TH className="hidden-xs hidden-sm text-center">
                                                {this.sort('order_status_id',trans('messages.order_status_id'))}
                                            </TH>
                                            <TH className="hidden-xs hidden-sm text-center">
                                                {this.sort('order_sub_status_id',trans('messages.order_sub_status_id'))}
                                            </TH>
                                            <TH className="text-center">
                                                {this.sort('mail_subject',trans('messages.mail_subject'))}
                                            </TH>
                                            <TH className="hidden-xs hidden-sm text-center">
                                                {this.sort('send_status',trans('messages.send_status'))}
                                            </TH>
                                            <TH className="hidden-xs hidden-sm text-center">
                                                {this.sort('in_date',trans('messages.in_date'))}
                                            </TH>
                                        </TR>
                                    </THEAD>
                                    <TBODY>
                                        {
                                            (!_.isEmpty(dataSource.data.data))?
                                            dataSource.data.data.map((item) => {
                                                index++;
                                                let classRow = (index%2 ===0) ? 'odd' : 'even';
                                                return (
                                                    [
                                                    <TR key={"tr" + index} className={classRow}>
                                                        <TD className="visible-xs visible-sm">
                                                            <b className="show-info">
                                                                <i className="fa fa-angle-double-down" aria-hidden="true"></i>
                                                            </b>
                                                        </TD>
                                                        <TD className=" text-center">{dataSource.data.from++}</TD>
                                                        <TD className="hidden-xs hidden-sm  text-center">
                                                            <a href={"/send-mail/detail?receive_order_id=" + item.receive_order_id} title={item.receive_order_id}>
                                                                {item.receive_order_id}
                                                            </a>
                                                        </TD>
                                                        <TD className="hidden-lg text-center">
                                                            <a href={this.state.baseUrl + "/detail?mail_id=" + item.mail_id}>
                                                               {item.receive_order_id}
                                                            </a>
                                                        </TD>
                                                        <TD className="hidden-xs hidden-sm cut-text  text-center" title={item.order_status_id}>{item.order_status_id}</TD>
                                                        <TD className="hidden-xs hidden-sm cut-text  text-center" title={item.order_sub_status_id}>{item.order_sub_status_id}</TD>
                                                        <TD className="cut-text  text-center" title={item.mail_subject}>{item.mail_subject}</TD>
                                                        <TD className="hidden-xs hidden-sm cut-text  text-center" title={item.send_status}>{(item.send_status === 0) ? '未' : '済'}</TD>
                                                        <TD className="hidden-xs hidden-sm  text-center">{(new Date(item.in_date)).toISOString().split('T')[0]}</TD>

                                                    </TR>,
                                                    <TR key={"trhide" + index} className="hiden-info" style={{display:"none"}}>
                                                        <TD colSpan="10" className="width-span1">
                                                            <ul id={"temp" + item.mail_id} >
                                                                <li> <b>NO</b> : #{index}</li>
                                                                <li>
                                                                    <b>{trans('messages.receive_order_id')} : </b>
                                                                    <p>{item.receive_order_id}</p>
                                                                </li>
                                                                <li>
                                                                    <b>{trans('messages.order_status_id')} : </b>
                                                                    <p>{item.order_status_id}</p>
                                                                </li>
                                                                <li>
                                                                    <b>{trans('messages.order_sub_status_id')} : </b>
                                                                    <p>{item.order_sub_status_id}</p>
                                                                </li>
                                                                <li>
                                                                    <b>{trans('messages.mail_subject')} : </b>
                                                                    <p>{item.mail_subject}</p>
                                                                </li>
                                                                <li>
                                                                    <b>{trans('messages.send_status')} : </b>
                                                                    <p>{(item.send_status === 0) ? '未' : '済'}</p>
                                                                </li>
                                                                <li>
                                                                    <b>{trans('messages.in_date')} : </b>
                                                                    <p>{(new Date(item.in_date)).toISOString().split('T')[0]}</p>
                                                                </li>
                                                            </ul>
                                                        </TD>
                                                    </TR>
                                                   ]
                                                )
                                            })
                                            :
                                            <TR><TD colSpan="9" className="text-center">{trans('messages.no_data')}</TD></TR>
                                        }
                                    </TBODY>
                                </TABLE>
                                <div className="text-center">
                                    <PaginationContainer config={config} />
                                </div>
                            </div>
                        </div>
                    </div>
                    </div>
                </div>

        )
    }
}

const mapStateToProps = (state) => {

  return {
    dataSource: state.commonReducer.dataSource
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    reloadData: (url, params) => { dispatch(Action.reloadData(url, params)) }
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(ListSendMail)