import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Loading from '../common/components/common/Loading';
import ROW_GROUP from '../common/components/table/ROW_GROUP';
import BUTTON from '../common/components/form/BUTTON';
import IFRAME from '../common/components/form/IFRAME';
import * as queryString from 'query-string';
import {
  Link
} from 'react-router-dom';
import { connect } from 'react-redux';
import * as Action from '../common/actions';


class DetailSendMail extends Component {
    constructor(props) {
            super(props);
            let states = {mail_id:null};
            this.state = states;
            const stringParams = queryString.parse(props.location.search);
            this.state = {...stringParams};
            this.props.reloadData(baseUrl, this.state);
        }

        handleCancel = (event) => {
            window.location.href = baseUrl;
        }

        componentDidUpdate() {
            if (!_.isEmpty(this.props.dataSource)) {
                $('.loading').hide();
            }
        }

        resizeIframe = (event) => {
            let height = 0;
            let currentHeight = event.target.contentWindow.document.body.scrollHeight;
            if (currentHeight > 300) {
                height = 300;
            } else {
                height = currentHeight;
            }
            event.target.style.height = height + 'px';
        }

        componentWillReceiveProps(nextProps) {
        if (!_.isEmpty(nextProps.dataSource)) {
            if (_.isEmpty(nextProps.dataSource.data)) {
                window.location.href = dashboardUrl;
            }
        }
    }

        render() {
            let dataSource = this.props.dataSource;

            const config = {
            fields :[
                {
                    id : "receive_order_id",
                    name : "receive_order_id",
                    fieldClass : ""
                },
                {
                    id : "order_status_id",
                    name : "status_name",
                    fieldClass : ""
                },
                {
                    id : "order_sub_status_id",
                    name : "sub_status_name",
                    fieldClass : ""
                },
                {
                    id : "mail_subject",
                    name : "mail_subject",
                    fieldClass : ""
                },
                {
                    id : "mail_content",
                    name : "mail_content",
                    fieldClass : ""
                },
                {
                    id : "attached_file_path",
                    name : "attached_file_path",
                    fieldClass : ""
                },
            ]
        }
        return (
                (!_.isEmpty(dataSource)) &&

                <div>
                <div className="row">
                     <div className="col-md-12">
                         <div className="form-horizontal padding-form">
                             <div className="row">
                                 <Loading className="loading"></Loading>
                                 <div className="col-md-12">
                                     {
                                         config.fields.map((value,key) =>{
                                            if (value.id === 'mail_content') {
                                                return <IFRAME
                                                        key={key}
                                                        name="mail_content"
                                                        labelTitle={trans('messages.mail_content')}
                                                        className="form-control input-sm"
                                                        fieldGroupClass="col-md-8 text-new-line"
                                                        labelClass="col-md-4 label-form"
                                                        height="200"
                                                        onLoad={this.resizeIframe}
                                                        src={iframeUrl + "?receive_order_id=" + dataSource.data.receive_order_id + "&order_status_id=" + dataSource.data.order_status_id + "&order_sub_status_id=" + dataSource.data.order_sub_status_id + "&operater_send_index="+dataSource.data.operater_send_index} />
                                            } else {
                                                return  <ROW_GROUP
                                                       key={key}
                                                       classNameLabel={'col-md-4 label-form'}
                                                       classNameField={'col-md-8 text-new-line'}
                                                       label={trans('messages.'+value.id)}
                                                       contents={dataSource.data[value.name]} />
                                            }

                                         })
                                     }
                                 </div>
                             </div>
                         </div>
                     </div>
                 </div>
                <div className="row">
                    <div className="col-md-12 button-box text-center">
                        {
                            (_.isEmpty(dataSource.data) === true) ? '' :
                            <a href={ordelUrl + '?receive_id_key=' + dataSource.data.receive_id} className="btn btn-success input-sm" >{trans('messages.send_mail_index')}</a>
                        }
                    </div>
                </div>
                </div>
        )
    }
}
const mapStateToProps = (state) => {

  return {
    dataSource: state.commonReducer.dataSource
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    reloadData: (url, params,) => { dispatch(Action.detailData(url, params)) }
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(DetailSendMail)