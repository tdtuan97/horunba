<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines contain the default error messages used by
    | the validator class. Some of these rules have multiple versions such
    | as the size rules. Feel free to tweak each of these messages here.
    |
    */

    'accepted'             => 'この:attributeをご承認お願いします。',
    'active_url'           => 'この:attributeがURLのフォーマットではなくてURLをご入力願いします。',
    'after'                => 'この:attributeが:dateの以降でお願いします。',
    'after_or_equal'       => 'この:attributeが:dateの以降でお願いします。',
    'alpha'                => 'この:attributeが英文字のみでお願いします。',
    'alpha_dash'           => 'この:attributeが英文字、英数字と"-"のみ許可になります。',
    'alpha_num'            => 'この:attributeが英文字と英数字のみ許可になります。',
    'array'                => 'この:attributeは配列でお願いします。',
    'before'               => 'この:attributeが:dateの以前でお願いします。',
    'before_or_equal'      => 'この:attributeが:dateの以前でお願いします。',
    'between'              => [
        'numeric' => 'この:attributeの値が:minと:maxの間にご入力お願いします。',
        'file'    => 'この:attributeの容量は:minから:maxKbまでお願いします。',
        'string'  => 'この:attributeの桁数は:minから:maxまでお願いします。',
        'array'   => 'この:attributeのアイテム数は:minから:maxまでお願いします。',
    ],
    'boolean'              => 'この:attributeの値がTRUEとFALSEのみ許可です。',
    'confirmed'            => 'The :attribute confirmation does not match.',
    'date'                 => 'この:attributeが不正な日付です。',
    'date_format'          => 'この:attributeのフォーマットは:formatでお願いします。',
    'different'            => 'この:attributeと:otherは違う値でお願いします。',
    'digits'               => 'この:attributeの桁数は:digitsでお願いします。',
    'digits_between'       => 'この:attributeの桁数は:minから:maxまでお願いします。',
    'dimensions'           => 'The :attribute has invalid image dimensions.',
    'distinct'             => 'この:attributeは重複になっています。',
    'email'                => 'この:attributeがメールアドレスでお願いします。',
    'exists'               => 'ご選択された:attributeがよくないです。',
    'file'                 => 'この:attributeがファイルでお願いします。',
    'filled'               => 'The :attribute field must have a value.',
    'image'                => 'この:attributeは画像でお願いします。',
    'in'                   => 'ご選択された:attributeがよくないです。',
    'in_array'             => 'この:attributeが:otherに存在していないです。',
    'integer'              => 'この:attributeの値はINTでお願いします。',
    'ip'                   => 'この:attributeはIPアドレスでお願いします。',
    'ipv4'                 => 'この:attributeはIPv4のアドレスでお願いします。',
    'ipv6'                 => 'この:attributeはIPv6のアドレスでお願いします。',
    'json'                 => 'この:attributeはJSONのフォーマットでお願いします。',
    'max'                  => [
        'numeric' => 'The :attribute may not be greater than :max.',
        'file'    => 'The :attribute may not be greater than :max kilobytes.',
        'string'  => 'The :attribute may not be greater than :max characters.',
        'array'   => 'The :attribute may not have more than :max items.',
    ],
    'mimes'                => 'The :attribute must be a file of type: :values.',
    'mimetypes'            => 'The :attribute must be a file of type: :values.',
    'min'                  => [
        'numeric' => 'The :attribute must be at least :min.',
        'file'    => 'The :attribute must be at least :min kilobytes.',
        'string'  => 'The :attribute must be at least :min characters.',
        'array'   => 'The :attribute must have at least :min items.',
    ],
    'not_in'               => 'ご選択された:attributeがよくないです。',
    'numeric'              => 'この:attributeが英数字でお願いします。',
    'present'              => 'この:attributeの field must be present',
    'regex'                => 'この:attributeのフォーマットは不正です。',
    'required'             => 'この:attributeのフィルドは必須です。',
    'required_if'          => ':otherが:valueの場合、この:attributeが必須です。',
    'required_unless'      => ':otherの値が:valuesにある場合、この:attributeが必須です。',
    'required_with'        => 'The :attribute field is required when :values is present.',
    'required_with_all'    => 'The :attribute field is required when :values is present.',
    'required_without'     => 'The :attribute field is required when :values is not present.',
    'required_without_all' => 'The :attribute field is required when none of :values are present.',
    'same'                 => 'この:attributeと:otherが一致しないといけないです。',
    'size'                 => [
        'numeric' => 'The :attribute must be :size.',
        'file'    => 'The :attribute must be :size kilobytes.',
        'string'  => 'The :attribute must be :size characters.',
        'array'   => 'The :attribute must contain :size items.',
    ],
    'string'               => 'この:attributeが配列でお願いします。',
    'timezone'             => 'The :attribute must be a valid zone.',
//    'unique'               => 'The :attribute has already been taken.',
    'unique'               => 'この:attributeが既に存在しています。',
    'uploaded'             => 'この:attributeがアップロードできなかった。ご確認お願いします。',
    'url'                  => 'この:attributeはエラーです。ご確認お願いします。',
    'phone_number'         => 'この:attributeはエラーです。ご確認お願いします。',
    'zip_code'             => 'この:attributeはエラーです。ご確認お願いします。',
    'fax_number'           => 'この:attributeはエラーです。ご確認お願いします。',
    /*
    |--------------------------------------------------------------------------
    | Custom Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | Here you may specify custom validation messages for attributes using the
    | convention "attribute.rule" to name the lines. This makes it quick to
    | specify a specific custom language line for a given attribute rule.
    |
    */

    'custom' => [
        'cancel_reason' => [
            'required_if' => 'この:attributeが必須です。',
        ],
        'order_num' => [
            'min' => '発注個数が良くないです。',
        ],
        'order_price' => [
            'min' => '発注価格が良くないです。',
        ],
        'importFile' => [
            'required'   => 'ファイルをご選択お願いします。',
            'max'        => 'この:attributeの容量は大きすぎです。上限は10Mbとなります。',
            'mine_c_s_v' => 'このファイルのフォーマットはサポートしていません。CSVのフォーマットでお願いいたします。',
            'is_sijs'    => 'エンコードはShift_JISでお願いいたします。',
        ],
        'proviso'                   => [
            'required'  => '但書は必須です。',
            'max'       => '但書は:max文字以内でご入力して下さい。'
        ],
        'full_name'                   => [
            'required'  => '宛名は必須です。',
            'max'       => '宛名は:max文字以内でご入力して下さい。'
        ],
        'tantou_password' => [
            'regex' => '大文字、小文字、数字を混ぜて入力して下さい。',
        ],
        'tantou_password_confirmation' => [
            'same' => '確認のパスワードが一致されていません。',
        ],
    ],

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Attributes
    |--------------------------------------------------------------------------
    |
    | The following language lines are used to swap attribute place-holders
    | with something more reader friendly such as E-Mail Address instead
    | of "email". This simply helps us make messages a little cleaner.
    |
    */

    'attributes' => [
        'attached_file_path.0' => 'Attached file path'
    ],
    'is_valid_date'                          => 'この:attributeが不正な日付です。',
    'check_postal_code_and_address'          => '●注文者の郵便番号と住所が不一致です。',
    'check_postal_code_and_address_receiver' => '●送付先の郵便番号と住所が不一致です。',
    'check_limit_request_price'              => '●高額注文のクレジット決済です。本人確認してください。',
    'check_delivery_direct_and_payment'      => '●代引きで直送はできません。',
    'check_tel_digits'                       => '●電話番号の桁数を確認してください。',
    'check_tel_digits1'                      => '●電話番号の桁数を確認してください。',
    'check_black_list'                       => '●ブラックリスト',
    'check_charge_fee_transport'             => '●中継料が発生します。再計算ボタンを押してください。',
    'check_charge_fee_delivery'              => '●送料無料です。再計算ボタンを押してください。',
    'check_charge_fee_cul_delivery'          => '●送料が異なります。再計算ボタンを押してください。',
    'check_payment_delivery_fee'             => '●代引き手数料が異なります。再計算ボタンを押してください。',
    'check_paid_by_point'                    => '●全額ポイントなのに請求額が０円ではないです。',
    'check_paid_not_by_point'                => '請求額が0円なのに全額ポイントじゃない.',
    'check_product_code_exists'              => 'マスタDBに存在していない商品があります。',
    'check_order_product'                    => 'この受注番号は返品したことがないです。',
    'check_product_set_valid'                => '子商品が存在していません。',
    'check_postal_code_and_address_receiver_length' => '●送付先の住所が不足です。',
    'check_supplier_id_exists'               => 'Supplier not exists.',
    'check_not_has_receive_id'               => 'This order was repaid',

    'check_fax_order'                        => 'ご選択された:attributeがよくないです。',
    'check_unique_mail_template'             => '既に存在しているテンプレートがあります。ご確認お願いいたします。',
    'unique_open_lesson'                     => 'この:attributeが既に存在しています。',
    'mine_c_s_v' => 'このファイルのフォーマットはサポートしていません。CSVのフォーマットでお願いいたします。',
    'is_sijs'    => 'エンコードはShift_JISでお願いいたします。',
];
