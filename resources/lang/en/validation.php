<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines contain the default error messages used by
    | the validator class. Some of these rules have multiple versions such
    | as the size rules. Feel free to tweak each of these messages here.
    |
    */

    'accepted'             => 'The :attribute must be accepted.',
    'active_url'           => 'The :attribute is not a valid URL.',
    'after'                => 'The :attribute must be a date after :date.',
    'after_or_equal'       => 'The :attribute must be a date after or equal to :date.',
    'alpha'                => 'The :attribute may only contain letters.',
    'alpha_dash'           => 'The :attribute may only contain letters, numbers, and dashes.',
    'alpha_num'            => 'The :attribute may only contain letters and numbers.',
    'array'                => 'The :attribute must be an array.',
    'before'               => 'The :attribute must be a date before :date.',
    'before_or_equal'      => 'The :attribute must be a date before or equal to :date.',
    'between'              => [
        'numeric' => 'The :attribute must be between :min and :max.',
        'file'    => 'The :attribute must be between :min and :max kilobytes.',
        'string'  => 'The :attribute must be between :min and :max characters.',
        'array'   => 'The :attribute must have between :min and :max items.',
    ],
    'boolean'              => 'The :attribute field must be true or false.',
    'confirmed'            => 'The :attribute confirmation does not match.',
    'date'                 => 'The :attribute is not a valid date.',
    'date_format'          => 'The :attribute does not match the format :format.',
    'different'            => 'The :attribute and :other must be different.',
    'digits'               => 'The :attribute must be :digits digits.',
    'digits_between'       => 'The :attribute must be between :min and :max digits.',
    'dimensions'           => 'The :attribute has invalid image dimensions.',
    'distinct'             => 'The :attribute field has a duplicate value.',
    'email'                => 'The :attribute must be a valid email address.',
    'exists'               => 'The selected :attribute is invalid.',
    'file'                 => 'The :attribute must be a file.',
    'filled'               => 'The :attribute field must have a value.',
    'image'                => 'The :attribute must be an image.',
    'in'                   => 'The selected :attribute is invalid.',
    'in_array'             => 'The :attribute field does not exist in :other.',
    'integer'              => 'The :attribute must be an integer.',
    'ip'                   => 'The :attribute must be a valid IP address.',
    'ipv4'                 => 'The :attribute must be a valid IPv4 address.',
    'ipv6'                 => 'The :attribute must be a valid IPv6 address.',
    'json'                 => 'The :attribute must be a valid JSON string.',
    'max'                  => [
        'numeric' => 'The :attribute may not be greater than :max.',
        'file'    => 'The :attribute may not be greater than :max kilobytes.',
        'string'  => 'The :attribute may not be greater than :max characters.',
        'array'   => 'The :attribute may not have more than :max items.',
    ],
    'mimes'                => 'The :attribute must be a file of type: :values.',
    'mimetypes'            => 'The :attribute must be a file of type: :values.',
    'min'                  => [
        'numeric' => 'The :attribute must be at least :min.',
        'file'    => 'The :attribute must be at least :min kilobytes.',
        'string'  => 'The :attribute must be at least :min characters.',
        'array'   => 'The :attribute must have at least :min items.',
    ],
    'not_in'               => 'The selected :attribute is invalid.',
    'numeric'              => 'The :attribute must be a number.',
    'present'              => 'The :attribute field must be present.',
    'regex'                => 'The :attribute format is invalid.',
    'required'             => 'The :attribute field is required.',
    'required_if'          => 'The :attribute field is required when :other is :value.',
    'required_unless'      => 'The :attribute field is required unless :other is in :values.',
    'required_with'        => 'The :attribute field is required when :values is present.',
    'required_with_all'    => 'The :attribute field is required when :values is present.',
    'required_without'     => 'The :attribute field is required when :values is not present.',
    'required_without_all' => 'The :attribute field is required when none of :values are present.',
    'same'                 => 'The :attribute and :other must match.',
    'size'                 => [
        'numeric' => 'The :attribute must be :size.',
        'file'    => 'The :attribute must be :size kilobytes.',
        'string'  => 'The :attribute must be :size characters.',
        'array'   => 'The :attribute must contain :size items.',
    ],
    'string'               => 'The :attribute must be a string.',
    'timezone'             => 'The :attribute must be a valid zone.',
    'unique'               => 'The :attribute has already been taken.',
    'uploaded'             => 'The :attribute failed to upload.',
    'url'                  => 'The :attribute format is invalid.',
    'phone_number'         => 'The :attribute format is invalid.',
    'zip_code'             => 'The :attribute format is invalid.',
    'fax_number'           => 'The :attribute format is invalid.',
    /*
    |--------------------------------------------------------------------------
    | Custom Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | Here you may specify custom validation messages for attributes using the
    | convention "attribute.rule" to name the lines. This makes it quick to
    | specify a specific custom language line for a given attribute rule.
    |
    */

    'custom' => [
        'cancel_reason' => [
            'required_if' => 'The :attribute field is required.',
        ],
        'order_num' => [
            'min' => '発注個数が良くないです。',
        ],
        'order_price' => [
            'min' => '発注価格が良くないです。',
        ],
        'importFile' => [
            'required'   => 'The import file field is required.',
            'max'        => 'The :attribute may not be greater than 10Mb.',
            'mine_c_s_v' => 'The file must be a csv file.',
            'is_sijs'    => 'The content in file must have encode is SIJS.',
        ],
        'proviso'                   => [
            'required'  => '但書は必須です。',
            'max'       => '但書は:max文字以内でご入力して下さい。'
        ],
        'full_name'                   => [
            'required'  => '宛名は必須です。',
            'max'       => '宛名は:max文字以内でご入力して下さい。'
        ]
    ],

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Attributes
    |--------------------------------------------------------------------------
    |
    | The following language lines are used to swap attribute place-holders
    | with something more reader friendly such as E-Mail Address instead
    | of "email". This simply helps us make messages a little cleaner.
    |
    */

    'attributes' => [
        'attached_file_path.0' => 'Attached file path'
    ],
    'is_valid_date'                          => 'The :attribute is not a valid date.',
    'check_postal_code_and_address'          => '●注文者の郵便番号と住所が不一致です。',
    'check_postal_code_and_address_receiver' => '●送付先の郵便番号と住所が不一致です。',
    'check_limit_request_price'              => '●高額注文のクレジット決済です。本人確認してください。',
    'check_delivery_direct_and_payment'      => '●代引きで直送はできません。',
    'check_tel_digits'                       => '●電話番号の桁数を確認してください。',
    'check_black_list'                       => '●ブラックリスト',
    'check_charge_fee_transport'             => '●中継料が発生します。再計算ボタンを押してください。',
    'check_charge_fee_delivery'              => '●送料無料です。再計算ボタンを押してください。',
    'check_charge_fee_cul_delivery'          => '●送料が異なります。再計算ボタンを押してください。',
    'check_payment_delivery_fee'             => '●代引き手数料が異なります。再計算ボタンを押してください。',
    'check_paid_by_point'                    => '●全額ポイントなのに請求額が０円ではないです。',
    'check_paid_not_by_point'                => '請求額が0円なのに全額ポイントじゃない.',
    'check_product_code_exists'              => 'マスタDBに存在していない商品があります。',
    'check_order_product'                    => 'This receive order id does not have returned orders',
    'check_product_set_valid'                => 'The child product is not exists in table mst_product.'
];
