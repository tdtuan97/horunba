@extends('layouts.main')
@section('titlePage', __('messages.send_mail_timing_header'))
@section('content')
<section class="content-header">
    <h1>
        {{__('messages.send_mail_timing_header')}}
        <small>{{__('messages.mail_template_sub_header')}}</small>
    </h1>
    <ol class="breadcrumb">
        <li class="active">
            <a href="{{action('SendMailTimingController@index')}}">{{__('messages.send_mail_timing_menu_title')}}</a>
        </li>
    </ol>
</section>

<section class="content">
    <!-- Put your code here -->
    <div id="root"></div>
</section>
@endsection
@section('javascript')
<script>
var baseUrl     = "{{action('SendMailTimingController@index')}}";
var saveUrl     = "{{action('SendMailTimingController@save')}}";
var dataListUrl = "{{action('SendMailTimingController@dataList')}}";
var changeStatus = "{{action('SendMailTimingController@changeStatus')}}";
$('#system-setting-menu').addClass('active');
$('#auto-send-mail-timing-menu').addClass('active');
</script>
@endsection