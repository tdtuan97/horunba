 @extends('layouts.main')
@section('titlePage', __('messages.add_mail_timing_header'))
@section('content')
<section class="content-header">
    <h1>
        {{__('messages.add_mail_timing_header')}}
        <small>{{__('messages.add_mail_timing_sub_header')}}</small>
    </h1>
    <ol class="breadcrumb">
        <li>
            <a href="{{action('SendMailTimingController@index')}}">{{__('messages.send_mail_timing_menu_title')}}</a>
        </li>
        <li>
            <a href="{{Request::fullUrl()}}">{{__('messages.send_mail_timing_menu_title')}}</a>
        </li>
    </ol>
</section>

<section class="content">
    <!-- Put your code here -->
    <div id="root"></div>
</section>
@endsection
@section('javascript')
<script>
var baseUrl        = "{{action('SendMailTimingController@index')}}";
var saveUrl        = "{{action('SendMailTimingController@save')}}";
var formDataUrl    = "{{action('SendMailTimingController@getFormData')}}";
var getTemplateUrl = "{{action('SendMailTimingController@getTemplateName')}}";
var checkEditUrl   = "{{action('DashboardController@checkItemEdit')}}";
$('#system-setting-menu').addClass('active');
$('#auto-send-mail-timing-menu').addClass('active');
</script>
@endsection