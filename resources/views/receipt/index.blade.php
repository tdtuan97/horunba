@extends('layouts.main')
@section('titlePage', __('messages.receipt_management_header'))
@section('content')
<section class="content-header">
    <h1>
        {{__('messages.receipt_management_header')}}
    </h1>
    <ol class="breadcrumb">
        <li class="active">
            <a href="{{action('ReceiptController@index')}}">
                {{__('messages.receipt_management_breadcrumb_title')}}
            </a>
        </li>
    </ol>
</section>

<section class="content">
    <!-- Put your code here -->
    <div id="root"></div>
</section>
@endsection
@section('javascript')
<script>
    var baseUrl         = "{{action('ReceiptController@index')}}";
    var exportCsvUrl    = "{{action('ReceiptController@exportCsv')}}";
    var fileDownloadUrl = "{{action('ReceiptController@downloadCsv')}}";
    var dataListUrl     = "{{action('ReceiptController@dataList')}}";
    $('#receipt-management-menu').addClass('active');
</script>
@endsection