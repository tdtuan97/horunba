<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<request>
    <shopId>{{$shopId}}</shopId>
    <orderId>{{$orderId}}</orderId>
    <shippingDate>{{$shippingDate}}</shippingDate>
    <shippingCarrier>{{$shippingCarrier}}</shippingCarrier>
    <shippingNumber>{{$shippingNumber}}</shippingNumber>
</request>