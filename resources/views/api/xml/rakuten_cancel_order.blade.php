<?xml version='1.0' encoding='UTF-8'?>
<SOAP-ENV:Envelope xmlns:SOAP-ENV='http://schemas.xmlsoap.org/soap/envelope/' xmlns:ns1='http://orderapi.rms.rakuten.co.jp/rms/mall/order/api/ws'>
    <SOAP-ENV:Body>
        <ns1:cancelOrder>
            <arg0>
                <authKey>{{$authKey}}</authKey>
                <shopUrl>{{$shopUrl}}</shopUrl>
                <userName>{{$userName}}</userName>
            </arg0>
            <arg1>
                <requestId>{{$requestId}}</requestId>
                @foreach ($rakOrderCancel as $order)
                <cancelModelList>
                    <reasonId>11</reasonId>
                    <orderNumber>{{$order['received_order_id']}}</orderNumber>
                    <restoreInventoryFlag>0</restoreInventoryFlag>
                </cancelModelList>
                @endforeach
            </arg1>
        </ns1:cancelOrder>
    </SOAP-ENV:Body>
</SOAP-ENV:Envelope>