<?xml version='1.0' encoding='UTF-8'?>
<SOAP-ENV:Envelope xmlns:SOAP-ENV='http://schemas.xmlsoap.org/soap/envelope/' xmlns:ns1='http://orderapi.rms.rakuten.co.jp/rms/mall/order/api/ws'>
    <SOAP-ENV:Body>
        <ns1:getOrder>
            <arg0>
                <authKey>{{$authKey}}</authKey>
                <shopUrl>{{$shopUrl}}</shopUrl>
                <userName>{{$userName}}</userName>
            </arg0>
            <arg1>
                <isOrderNumberOnlyFlg>false</isOrderNumberOnlyFlg>
                <orderSearchModel>
                    <dateType>1</dateType>
                    <startDate>{{$startDate}}</startDate>
                    <endDate>{{$endDate}}</endDate>
                </orderSearchModel>
                @foreach ($orders as $order)
                <orderNumber>{{$order}}</orderNumber>
                @endforeach
            </arg1>
        </ns1:getOrder>
    </SOAP-ENV:Body>
</SOAP-ENV:Envelope>