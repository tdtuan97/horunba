<?xml version='1.0' encoding='UTF-8'?>
<SOAP-ENV:Envelope xmlns:SOAP-ENV='http://schemas.xmlsoap.org/soap/envelope/' xmlns:ns1='http://orderapi.rms.rakuten.co.jp/rms/mall/order/api/ws'>
    <SOAP-ENV:Body>
        <ns1:changeStatus>
            <arg0>
                <authKey>{{$authKey}}</authKey>
                <shopUrl>{{$shopUrl}}</shopUrl>
                <userName>{{$userName}}</userName>
            </arg0>
            <arg1>
                <requestId>{{$requestId}}</requestId>
                <orderStatusModel>
                    @foreach ($orders as $id)
                    <orderNumber>{{$id}}</orderNumber>
                    @endforeach
                    <statusName>{{$status}}</statusName>
                </orderStatusModel>
            </arg1>
        </ns1:changeStatus>
    </SOAP-ENV:Body>
</SOAP-ENV:Envelope>