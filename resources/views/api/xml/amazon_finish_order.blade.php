<?xml version="1.0" encoding="UTF-8"?>
<AmazonEnvelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:noNamespaceSchemaLocation="amzn-envelope.xsd">
<Header>
    <DocumentVersion>1.01</DocumentVersion>
    <MerchantIdentifier>{{$merchantIdentifier}}</MerchantIdentifier>
</Header>
    <MessageType>OrderFulfillment</MessageType>
    @foreach ($params as $param)
    <Message>
        <MessageID>{{$param['receive_id']}}</MessageID>
        <OperationType>Update</OperationType>
        <OrderFulfillment>
            <AmazonOrderID>{{$param['order_id']}}</AmazonOrderID>
            <FulfillmentDate>{{date('Y-m-d\TH:i:s+09:00', strtotime($param['shipment_date']))}}</FulfillmentDate>
            <FulfillmentData>
                <CarrierName>{{$param['carrier_name']}}</CarrierName>
                <ShipperTrackingNumber>{{$param['tracking_number']}}</ShipperTrackingNumber>
            </FulfillmentData>
            @if($param['payment_method'] === 3)<CODCollectionMethod>DirectPayment</CODCollectionMethod>
            @endif
        </OrderFulfillment>
    </Message>
    @endforeach
</AmazonEnvelope>
