<?xml version="1.0" encoding="UTF-8"?>
<AmazonEnvelope xsi:noNamespaceSchemaLocation="amzn-envelope.xsd" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
    <Header>
        <DocumentVersion>1.01</DocumentVersion>
        <MerchantIdentifier>{{$merchantIdentifier}}</MerchantIdentifier>
    </Header>
    <MessageType>OrderSourcingOnDemand</MessageType>
    @foreach ($data as $key => $item)    
    <Message>				
        <MessageID>{{$item['message_id']}}</MessageID>			
        <OrderSourcingOnDemand>			
                <AmazonOrderID>{{$item['received_order_id']}}</AmazonOrderID>		
                <SKU>{{$item['product_code']}}</SKU>		
                <EstimatedShipDate>{{$item['shipment_date_plan']}}T00:00:00+09:00</EstimatedShipDate>		
        </OrderSourcingOnDemand>			
    </Message>   
    @endforeach
</AmazonEnvelope>