<?xml version='1.0' encoding='UTF-8'?>
<SOAP-ENV:Envelope xmlns:SOAP-ENV='http://schemas.xmlsoap.org/soap/envelope/' xmlns:ns1='http://orderapi.rms.rakuten.co.jp/rms/mall/order/api/ws'>
    <SOAP-ENV:Body>
        <ns1:rBankAccountTransfer>
            <arg0>
                <authKey>{{$authKey}}</authKey>
                <shopUrl>{{$shopUrl}}</shopUrl>
                <userName>{{$userName}}</userName>
            </arg0>
            <arg1>
                <requestId>{{$requestId}}</requestId>
                @foreach ($orders as $order)
                <orderNumber>{{$order->received_order_id}}</orderNumber>
                @endforeach
            </arg1>
        </ns1:rBankAccountTransfer>
    </SOAP-ENV:Body>
</SOAP-ENV:Envelope>