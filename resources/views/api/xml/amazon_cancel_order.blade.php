<?xml version="1.0" encoding="UTF-8"?>
<AmazonEnvelope xsi:noNamespaceSchemaLocation="amzn-envelope.xsd" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
    <Header>
        <DocumentVersion>1.01</DocumentVersion>
        <MerchantIdentifier>{{$merchantIdentifier}}</MerchantIdentifier>
    </Header>
    <MessageType>OrderAcknowledgement</MessageType>
    @foreach ($data as $key => $item)
    <Message>
        <MessageID>{{$item['receive_id']}}</MessageID>
        <OperationType>Update</OperationType>
        <OrderAcknowledgement>
            <AmazonOrderID>{{$item['received_order_id']}}</AmazonOrderID>
            <StatusCode>{{$item['status_code']}}</StatusCode>
            @foreach ($item['order_item'] as $orderItem)
            <Item>
                <AmazonOrderItemCode>{{$orderItem['order_item_code']}}</AmazonOrderItemCode>
                <CancelReason>{{$orderItem['cancel_reason']}}</CancelReason>
            </Item>
            @endforeach
        </OrderAcknowledgement>
    </Message>
    @endforeach
</AmazonEnvelope>