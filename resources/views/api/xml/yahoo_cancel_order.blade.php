<Req>
    <Target>
        <OrderId>{{$orderId}}</OrderId>
        <IsPointFix>false</IsPointFix>
        <OperationUser>{{$operationUser}}</OperationUser>
    </Target>
    <Order>
        <OrderStatus>4</OrderStatus>
        <CancelReason>{{$cancelReason}}</CancelReason>
        <CancelReasonDetail>{{$cancelReasonDetail}}</CancelReasonDetail>
    </Order>
    <SellerId>{{$sellerId}}</SellerId>
</Req>