@switch($param)
@case('[###注文情報]')

================
注文情報
================
　■　当店お問合番号　　　：{{ $datas['receive_id'] }}
　■　モールご注文番号　　　：{{ $datas['received_order_id'] }}
　■　ご注文日時　　　　　：{{ $datas['order_date'] }}
　■　ご注文者様氏名　　　：{{ $datas['order_last_name'] }}　{{ $datas['order_first_name'] }}様
　■　お支払い方法　　　　：{{ $datas['payment_name'] }}@break

@case('[###送付先情報]')
================
送付先情報 ({{$index}})
================
　■　{{ $datas['ship_last_name'] }} {{ $datas['ship_first_name'] }}様
　■　{{ $datas['ship_zip_code'] }}
　■　{{ $datas['ship_prefecture'] }}{{ $datas['ship_city'] }}{{ $datas['ship_sub_address'] }}
　■　{{ $datas['ship_tel_num'] }}
--------------------------------------------------------
商品明細
--------------------------------------------------------
@isset($datas['child'])
@if (count($datas['child'] > 0))
@foreach ($datas['child'] as $vData)
[{{ $vData['product_code'] }}]{{ $vData['product_name'] }}　{{ $vData['price'] }}円×　{{ $vData['quantity'] }}点
@endforeach
@endif
@else
[{{ $datas['product_code'] }}]{{ $datas['product_name'] }}　{{ $datas['price'] }}円×　{{ $datas['quantity'] }}点
@endisset
@break

@case('[###送付先情報感謝]')
================
送付先情報
================
　■　{{ $datas['ship_last_name'] }} {{ $datas['ship_first_name'] }}様
　■　{{ $datas['ship_zip_code'] }}
　■　{{ $datas['ship_prefecture'] }}{{ $datas['ship_city'] }}{{ $datas['ship_sub_address'] }}
　■　{{ $datas['ship_tel_num'] }}
--------------------------------------------------------
商品明細
--------------------------------------------------------
@isset($datas['child'])
@if (count($datas['child'] > 0))
@foreach ($datas['child'] as $vData)
[{{ $vData['product_code'] }}]{{ $vData['product_name'] }}　{{ $vData['price'] }}円×　{{ $vData['quantity'] }}点
@endforeach
@endif
@else
[{{ $datas['product_code'] }}]{{ $datas['product_name'] }}　{{ $datas['price'] }}円×　{{ $datas['quantity'] }}点
@endisset
@break

@case('[###決済情報]')
--------------------------------------------------------
小計　　　　　　　{{$datas['goods_price']}}円
送料　　　　　　　{{$datas['ship_charge']}}円
代引料　　　　　　{{$datas['pay_after_charge']}}円
決済料　　　　　　{{$datas['pay_charge']}}円
送料値引　　　　　{{$datas['pay_charge_discount']}}円
値引　　　　　　　{{$datas['discount']}}円
ポイント　　　　　@if($datas['used_point'] > 0)-@endif{{ $datas['used_point'] }}円
クーポン　　　　　@if($datas['used_coupon'] > 0)-@endif{{ $datas['used_coupon'] }}円
お支払い額　　　　{{$datas['request_price'] }}円
--------------------------------------------------------@break

@case('[###会社名]')
{{ $datas['company_name']}}@break

@case('[###注文者氏名]')
{{ $datas['order_last_name']}}　{{ $datas['order_first_name'] }}@break

@case('[###店舗名]')
@if ($datas['mall_id'] === 4)
DIY FACTORY ビジネス@else
DIY FACTORY オンラインショップ@endif
@break

@case('[###注文者会社含氏名]')
{{ $datas['company_name']}}　{{ $datas['order_last_name'] }}　{{ $datas['order_first_name'] }}@break

@case('[###入金金額]')
{{ $datas['payment_price'] }}@break

@case('[###入金日]')
{{ $datas['payment_date'] }}@break

@case('[###入金確認日]')
{{ $datas['payment_confirm_date'] }}@break

@case('[###決済方法別テンプレ]')
■お支払方法は【銀行振込み】です。

■銀行名　　： 三井住友銀行　ドットコム支店（店番号953）
■口座種別　： 普通
■口座名義　： カ）ダイト
■口座番号　： {{ $datas['payment_account'] }}

※振込口座は、1回のご注文ごとに生成される、お客様専用の口座番号です。
※お振込手数料はお客様のご負担となります。
※入金確認は当店営業時間内のみとなります。
※国内からの振込に限られます。

{{  date("Y/m/d", strtotime(now() . "+7 days"))}}までにご入金いただけない場合には、誠に勝手ながら、自動キャンセルとなりますのご了承ください。@break

@case('[###請求金額]')
{{ $datas['request_price'] }}@break

@case('[###問合ナンバー]')
{{ $datas['receive_id'] }}@break

@case('[###お客様備考欄]')
{{ $datas['customer_question'] }}@break

@case('[###店舗回答欄]')
{{ $datas['shop_answer'] }} @break

@case('[###入金口座]')
{{ $datas['payment_account'] }}@break

@case('[###一週間後]')
{{  date("Y/m/d", strtotime(now() . "+7 days"))}}@break

@case('[###配送業者]')
@if (empty($datas['inquiry_no']))
メーカー直送 @break
@else
{{ $datas['ship_company_name'] }} @break
@endif

@case('[###送り状番号]')
@if (empty($datas['inquiry_no']))
メーカー直送 @break
@else
{{ $datas['inquiry_no'] }} @break
@endif

@case('[###発送完了日]')
{{ $datas['shipment_date'] ? date("Y-m-d", strtotime($datas['shipment_date'])) : date("Y-m-d")}}@break

@case('[###キャンセル理由]')
{{ $datas['cancel_reason'] }}@break

@case('[###発送日]')
{{ $datas['delivery_plan_date'] ? date("Y/m/d",strtotime($datas['delivery_plan_date'])) : '' }}@break

@case('[###配送先氏名]')
{{ $datas['ship_last_name'] }} {{ $datas['ship_first_name'] }}@break

@case('[###翌日]')
{{  date("Y/m/d", strtotime(now() . "+1 days")) }}@break

@case('[###受注番号]')
{{ $datas['received_order_id'] }}@break

@case('[###本日]')
{{  date("Y/m/d") }}@break

@case('[###注文日]')
{{ $datas['order_date'] }}@break

@case('[###入金締切日]')
{{  date("Y/m/d", strtotime(now() . "+7 days")) }}@break

@case('[###領収書無]')
@if ($datas['payment_method'] === 3 OR
$datas['payment_method'] === 17 OR
$datas['payment_method'] === 18 OR
$datas['payment_method'] === 19)
※【{{ $datas['payment_name'] }}】は発行不可
@endif
@break

@case('[###領収書URL]')
@if ($datas['payment_method'] === 1 OR
$datas['payment_method'] === 2 OR
$datas['payment_method'] === 6 OR
$datas['payment_method'] === 7 OR
$datas['payment_method'] === 9 OR
$datas['payment_method'] === 13 OR
$datas['payment_method'] === 14 OR
$datas['payment_method'] === 15 OR
$datas['payment_method'] === 16 OR
$datas['payment_method'] === 20)
■領収書自動発行URL：
{{ url("/").'/receipt/'.$datas['mail_seri'] }}

■領収書自動発行システム_ご利用手順書：
https://docs.google.com/document/d/11oXnWZGXWEMB4vlFu_ozpBAPvtKmX5IROzs0iJUzpEo/edit

■ダウンロード期限日：{{  date("Y/m/d", strtotime($datas['shipment_date'] . "+90 days")) }}

※有効期限は出荷日から90日間です。
※期限切れ及びご入力間違いの場合でも、再発行はできませんのでご注意ください。
@endif
@break

@case('[###送付先情報欠品・廃番]')
<?php $index = 0 ?>
@foreach($datas as $dats)
<?php $index++ ?>
@foreach($dats as $key => $data)
@if ($key === 0)
================
   送付先情報 ({{$index}})
================
　■　{{ $data['ship_last_name'] }}　{{ $data['ship_first_name'] }}様
　■　{{ $data['ship_zip_code'] }}
　■　{{ $data['ship_prefecture'] }}{{ $data['ship_city'] }}{{ $data['ship_sub_address'] }}
　■　{{ $data['ship_tel_num'] }}
--------------------------------------------------------
   商品明細
--------------------------------------------------------
@endif
[{{ $data['product_code'] }}]{{ $data['product_name'] }}　{{ $data['price'] }}円×　{{ $data['quantity'] }}点　{{ $data['status_name'] }}
@endforeach
@endforeach
@break

@case('[###送付先情報発送日案内]')
<?php $k = 0; ?>
@foreach ($datas as $dats)
<?php $i = 0; ?>
@foreach ($dats as $data)
@if ($i === 0)
━━━━━━━━━━━━━━━
@if($data['delivery_type'] === 2)
■発送予定日：{{ date("Y/m/d", strtotime($data['delivery_date'])) }}
@else
■発送予定日：{{ date("Y/m/d", strtotime($data['delivery_plan_date'])) }}
@endif
━━━━━━━━━━━━━━━
@endif
================
@if(in_array($data['delivery_type'], [1, 3]))
   送付先情報 ({{ ++$k }})当店より発送
@else
   送付先情報 ({{ ++$k }})メーカーより直送
@endif
================
　■　{{ $data['ship_last_name'] }}　{{ $data['ship_first_name'] }}様
　■　{{ $data['ship_zip_code'] }}
　■　{{ $data['ship_prefecture'] }}{{ $data['ship_city'] }}{{ $data['ship_sub_address'] }}
　■　{{ $data['ship_tel_num'] }}
--------------------------------------------------------
   商品明細
--------------------------------------------------------
@foreach ($data['product'] as $item)
[{{ $item['product_code'] }}]{{ $item['product_name'] }}　{{ $item['price'] }}円×　{{ $item['quantity'] }}点
@endforeach
@endforeach
@endforeach
@break

@case('[###2日後]')
{{  date("Y/m/d", strtotime(now() . "+2 days")) }}@break

@case('[###モール]')
@if ($datas['mall_id'] === 5)
https://b-dash2.diyfactory.jp/form/contact?type=dfh
@elseif($datas['mall_id'] === 4)
https://www.monotos.co.jp/user_data/contact/index.php
@elseif($datas['mall_id'] === 1)
https://b-dash2.diyfactory.jp/form/contact?type=dfr
@elseif($datas['mall_id'] === 2)
https://b-dash2.diyfactory.jp/form/contact?type=dfy
@endif
@break

@case('[###注文履歴URL]')
@if ($datas['mall_id'] === 9)
https://review.rakuten.co.jp/rd/0_198680_198680_0/
@elseif($datas['mall_id'] === 2)
https://shopping.yahoo.co.jp/store_rating/diy-tool/store/review/?sc_i=shp_pc_search_itemlist_shsr_strrvw
@endif
@break

@case('[###決済方法別テンプレサ]')
@if ($datas['payment_method'] === 2 OR $datas['payment_method'] === 6)
■ご注文につきまして
・注文内容の変更およびキャンセルは承ることができません。
・1ヶ月以上の長期欠品が判明した場合、メールにてご連絡致します。
・発送日が分かり次第、メールにてご案内させていただきます。

・銀行振込口座はお支払期限とあわせて別途メールでお知らせします。

ご質問がございましたら、下記問合せフォームよりご連絡くださいませ。
[###モール]@else
■ご注文につきまして
・注文内容の変更およびキャンセルは承ることができません。
・1ヶ月以上の長期欠品が判明した場合、メールにてご連絡致します。
・発送日が分かり次第、メールにてご案内させていただきます。


ご質問がございましたら、お問合せフォームよりご連絡くださいませ。
[###モール]@endif
@break

@case('[###決済方法別]')
@if ($datas['payment_method'] === 2)
■銀行名　： 三井住友銀行　ドットコム支店（店番号953）
■口座種別： 普通
■口座名義： カ）ダイト
■口座番号： {{ $datas['payment_account'] }}
@elseif ($datas['payment_method'] === 6)
■銀行名：楽天銀行（ラクテンギンコウ）
■支店名：楽天市場支店（ラクテンイチバシテン）
■預金種別：普通
■口座番号：1339284
■口座名義：ラクテン（ＤＩＹＦＡＣＴＯＲＹＯＮＬＩＮＥＳＨＯＰ@endif
@break

@endswitch