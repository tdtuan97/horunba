@extends('layouts.main')
@section('titlePage', __('messages.return_management_save_header'))
@section('content')
<section class="content-header">
    <h1>
        {{__('messages.return_management_save_header')}}
        <small></small>
    </h1>
    <ol class="breadcrumb">
        <li>
            <a href="{{action('ReturnManagementController@index')}}">
                {{__('messages.return_management_breadcrumb_title')}}
            </a>
        </li>
    </ol>
</section>

<section class="content">
    <!-- Put your code here -->
    <div id="root"></div>
</section>
@endsection
@section('javascript')
<script>
    var baseUrl              = "{{action('ReturnManagementController@index')}}";
    var saveUrl              = "{{action('ReturnManagementController@saveReturnDetail')}}";
    var orderDetailUrl       = "{{action('OrderManagementController@detail')}}";
    var getRepaymentPriceUrl = "{{action('ReturnManagementController@getRepaymentPrice')}}";
    var checkEditUrl         = "{{action('DashboardController@checkItemEdit')}}";
    var getFormDataUrl       = "{{action('ReturnAddressController@getFormData')}}";
    var saveAddressUrl       = "{{action('ReturnAddressController@saveAddress')}}";
    var optReturnDetailUrl   = "{{action('ReturnManagementController@getOptionReturnDetail')}}";
    $('#return-management-menu').addClass('active');
</script>
@endsection