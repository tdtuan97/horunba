@extends('layouts.main')
@section('titlePage', __('messages.return_management_header'))
@section('content')
<section class="content-header">
    <h1>
        {{__('messages.return_management_header')}}
    </h1>
    <ol class="breadcrumb">
        <li>
            <a href="{{action('ReturnManagementController@index')}}">
                {{__('messages.return_management_breadcrumb_title')}}
            </a>
        </li>
    </ol>
</section>
<div class="popup-error fix-message collapse" id="popup-status">
    <div class="alert alert-dismissible alert-success">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <h4><i class="icon fa fa-check"></i> Alert!</h4>
        <p class="text-new-line" id="popup-message"></p>
    </div>
</div>
<section class="content">
    <!-- Put your code here -->
    <div id="root"></div>
</section>
@endsection
@section('javascript')
<script>
    var deleteUrl      = "{{action('ReturnManagementController@deleteItem')}}";
    var changeFlagUrl  = "{{action('ReturnManagementController@changeFlag')}}";
    var baseUrl        = "{{action('ReturnManagementController@index')}}";
    var saveDetailUrl  = "{{action('ReturnManagementController@saveReturnDetail')}}";
    var saveProductUrl = "{{action('ReturnManagementController@saveReturnProduct')}}";
    var dataListUrl    = "{{action('ReturnManagementController@dataList')}}";
    var mojaxUrl       = "{{action('ReturnManagementController@processMojax')}}";
    var importUrl      = "{{action('ReturnManagementController@import')}}";
    var csvUrl         = "{{action('Common@importCSV')}}";
    $('#return-management-menu').addClass('active');
</script>
@endsection