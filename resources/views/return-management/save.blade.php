@extends('layouts.main')
@section('titlePage', __('messages.return_management_save_header'))
@section('content')
<section class="content-header">
    <h1>
        {{__('messages.return_management_save_header')}}
        <small></small>
    </h1>
    <ol class="breadcrumb">
        <li>
            <a href="{{action('ReturnManagementController@index')}}">
                {{__('messages.return_management_breadcrumb_title')}}
            </a>
        </li>
        <li>
            <a href="{{Request::fullUrl()}}">
                {{__('messages.return_management_save_header')}}
            </a>
        </li>
    </ol>
</section>

<section class="content">
    <!-- Put your code here -->
    <div id="root"></div>
</section>
@endsection
@section('javascript')
<script>
    var baseUrl        = "{{action('ReturnManagementController@index')}}";
    var saveUrl        = "{{action('ReturnManagementController@save')}}";
    var getFormDataUrl = "{{action('ReturnManagementController@getFormData')}}";
    var getDataProductUrl = "{{action('ReturnManagementController@getDataProduct')}}";
    var checkEditUrl   = "{{action('DashboardController@checkItemEdit')}}";
    $('#return-management-menu').addClass('active');
    $(document).ready(function() {
        var width = $(document).width();
        if (width <= 992) {
            $('#message-pro-name').insertAfter('.input-pro-name');
        }
    });
</script>
@endsection