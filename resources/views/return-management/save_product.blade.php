@extends('layouts.main')
@section('titlePage', __('messages.return_management_save_header'))
@section('content')
<section class="content-header">
    <h1>
        {{__('messages.return_management_save_header')}}
        <small></small>
    </h1>
    <ol class="breadcrumb">
        <li>
            <a href="{{action('ReturnManagementController@index')}}">
                <i class="fa fa-dashboard"></i> {{__('messages.return_management_breadcrumb_title')}}
            </a>
        </li>
        <li class="active">{{__('messages.save_action')}}</li>
    </ol>
</section>

<section class="content">
    <!-- Put your code here -->
    <div id="root"></div>
</section>
@endsection
@section('javascript')
<script>
    var baseUrl           = "{{action('ReturnManagementController@index')}}";
    var saveUrl           = "{{action('ReturnManagementController@saveReturnProduct')}}";
    var deleteAllUrl      = "{{action('ReturnManagementController@deleteAllItem')}}";
    var updateQuantityUrl = "{{action('ReturnManagementController@updateQuantity')}}";
    var getFormDataUrl    = "{{action('ReturnAddressController@getFormData')}}";
    var saveAddressUrl    = "{{action('ReturnAddressController@saveAddress')}}";
    var getDataProductUrl = "{{action('ReturnManagementController@getDataProductInput')}}";
    var checkEditUrl      = "{{action('DashboardController@checkItemEdit')}}";
    var getAddressUrl     = "{{action('ReturnManagementController@getAddress')}}";
    $('#return-management-menu').addClass('active');
    $(document).ready(function() {
        var width = $(document).width();
        if (width <= 992) {
            $('#message-pro-name').insertAfter('.input-pro-name');
        }
    });
</script>
@endsection