@extends('layouts.main')
@section('titlePage', __('messages.controller_header'))
@section('content')
<section class="content-header">
    <h1>
        {{__('messages.controller_header')}}
    </h1>
    <ol class="breadcrumb">
        <li>
            <a href="{{action('ControlController@index')}}">{{__('messages.controller_breadcrumb_title')}}</a>
        </li>
    </ol>
</section>

<section class="content">
    <!-- Put your code here -->
    <div id="root"></div>
</section>
@endsection
@section('javascript')
<script>
    var baseUrl             = "{{action('ControlController@index')}}";
    var dataListUrl         = "{{action('ControlController@dataList')}}";
    var saveCtrlUrl         = "{{action('ControlController@saveController')}}";
    var UpCtrlUrl           = "{{action('ControlController@updateController')}}";
    var delCtrlUrl          = "{{action('ControlController@deleteController')}}";
    $('#system-setting-menu').addClass('active');
    $('#controller-menu').addClass('active');
</script>
@endsection