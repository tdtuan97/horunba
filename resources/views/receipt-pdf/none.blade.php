@if ($type === 'mail_serial_invalid')
<div style="margin:10px;">
    <h3 style="background-color:#efefefff; color:#cc0000; padding: 10px; padding-right: 70px; display:inline-block;">
        エラー：ページを正常に表示できませんでした。
    </h3>
    <div style="margin-left:20px">
        URLの内容を再度ご確認の上、ページにアクセスして下さい。<br>
        <br>
        なお、毎日午前3:00～午前8:00はシステムメンテナンス時間です。<br>
        こちらの時間にアクセスできない場合は、時間をおいて再度アクセスして下さい。<br>
        <br>
        それでもURLにアクセスできない場合は、<br>
        大変申し訳ございませんが、こちらのページは削除/URL誤り/URL変更がされた可能性がございます。<br>
        恐れ入りますが、店舗にお問合せ下さいますようお願い申し上げます。
    </div>
</div>
@elseif ($type === 'download_expired')
<p style="text-align: center; margin-top: 30px; width: 100%;">
    こちらの領収書発行画面は期限が切れています。<br>
    領収書発行期限は出荷日より90日以内となっておりますのでご了承ください。
</p>
@endif