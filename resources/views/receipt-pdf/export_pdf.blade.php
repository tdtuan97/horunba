<!DOCTYPE html>
<html lang="ja">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>領収書発行</title>
    <style type="text/css">
        html{
            font-size: 14px;
        }
        .content{
            /*border: 1px solid #000;*/
            border-bottom: 2px solid #000;
            width: 190.5mm;
            padding-bottom: 12px;
            height: 98.1mm;
        }
        @font-face {
            font-family: 'MS-Mincho';
            src: url('fonts/MS-Mincho.ttf') format('truetype');
        }
        @page { margin-top: 15px; }
        * {
          font-family: 'MS-Mincho';
        }
        .table {
            border-collapse: collapse;
            width: 100%;
            max-width: 100%;
            margin-bottom: 20px;
        }
        .table-bordered {
            border: 1px solid #ddd;
        }
        .table-bordered>tbody>tr>td, .table-bordered>tbody>tr>th, .table-bordered>tfoot>tr>td, .table-bordered>tfoot>tr>th, .table-bordered>thead>tr>td, .table-bordered>thead>tr>th {
            border: 1px solid #ddd;
        }
        .table>tbody>tr>td, .table>tbody>tr>th, .table>tfoot>tr>td, .table>tfoot>tr>th, .table>thead>tr>td, .table>thead>tr>th {
            padding: 8px;
            line-height: 1.42857143;
            vertical-align: top;
            border-top: 1px solid #ddd;
        }

        .account-div {
            clear: both;
            width: 250px;
            padding-bottom: 2px;
            margin-top: 10px;
            margin-left: 8px;
            border-bottom-style: solid;
            border-bottom-width: thin;
        }
    </style>
</head>
<body>
    <div class="content">
        <div style="width: 40mm;border-bottom: 1px solid #000;text-align: center;float: left;font-size: 30px;margin: 0 auto;">
            領収書
        </div>
        <div style="float: right;width: 70mm;text-align: right;">
            <?php
                $date = explode("-", date('Y-m-d', strtotime($data[0]->receipt_date)));
                $japanDate =  $date[0] . '年' . $date[1] . '月'  . $date[2] . '日' ;
            ?>
            <div>領収日:{{$japanDate}}</div>
            <div>No.{{$data[0]->received_order_id}}</div>
            <div>[電子領収書につき印紙不要]</div>
        </div>
        <div style="clear: both;height: 10px;"></div>
        <div style="float: left;font-size: 20px; width: 450px; word-wrap: break-word;">
            <span style="border-bottom: 1px solid #000;">{{$data[0]->full_name}} 様</span>
        </div>
        <div style="clear: both;height: 10px;"></div>
        <div style="width: 100mm;text-align: center;margin: 0 auto;background: #E9E9E9;padding-bottom: 10px;">
            <span style="border-bottom: 2px solid #000;font-size: 30px;">
                ¥ {{number_format($data[0]->request_price, 0, ',', ',')}}  -
            </span>
        </div>
        <div style="width: 100mm;text-align: center;margin: 0 auto; word-wrap: break-word;">
            但 {{ $data[0]->proviso }} (税込)として、上記正に領収いたしました。
        </div>
        <div style="clear: both;height: 20px;"></div>
        <div style="width: 30mm;float: right;">
            <img src="receipt-pdf/img/icon_pdf.jpg" width="100%">
        </div>
        <div style="width: 80mm;float: right;">
            <div style="text-align: left;font-size: 16px;">
                株式会社大都
            </div>
            <div style="text-align: left;">
                〒544-0025
            </div>
            <div style="text-align: left;">
                大阪府
            </div>
            <div style="text-align: left;">
                大阪市生野区生野東2-5-3
            </div>
        </div>
    </div>
</body>
</html>
