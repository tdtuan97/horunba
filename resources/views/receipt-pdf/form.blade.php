<!DOCTYPE html>
<html lang="ja">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="{{ asset('/plugins/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('/receipt-pdf/css/custom.css') }}" rel="stylesheet">
    <link href="{{ asset('/plugins/bootstrap-datepicker/css/bootstrap-datepicker.min.css') }}" rel="stylesheet">
    <title>領収書発行</title>
        <style type="text/css">
        body {
            padding: 30px 0;
        }
        hr {
            border-color: black;
        }
        .hide-border {
            border-left-color: white ! important;
            border-right-color: white ! important;
        }
        .margin-table {
            margin-top: 30px;
            margin-bottom: 3px;
        }
    </style>
    </head>
    <body style="margin-bottom: 60px;">
        <div class="container">
        @if (count($data) > 0)
        <div style="text-align: center;font-size: 18px;font-weight: bold;padding-bottom: 10px;">領収書発行</div>
            <form id="frmReceipt" action="{{action('ReceiptPDFController@receipt',['mail_serial'=>$mail_serial])}}" method="post">
                @if (count($errors) > 0)
                <table width="99%" align="center" cellspacing="3" cellpadding="5" id="error_display">
                    @foreach ($errors as $error)
                        <tr>
                            <td align="left"><font color="red">{{ $error }}</font></td>
                        </tr>
                    @endforeach
                </table>
                @endif

                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <table class="table table-bordered">
                        <tbody><tr>
                            <td>名前（会社名）</td>
                            <td id="td_full_name">
                            @if ((int)$data[0]->pdf_status !== 1)
                                <input type="text" class="form-control" name="full_name" id="full_name" value="{{$data[0]->last_name . $data[0]->first_name}}">
                            @else
                                {{$data[0]->full_name}}
                            @endif

                            </td>
                        </tr>
                        <tr>
                            <td>注文日</td>
                            <td>{{$data[0]->OrderDate}} {{$data[0]->order_date}} </td>
                        </tr>
                        <tr>
                            <td>受注番号</td>
                            <td>{{$data[0]->received_order_id}}</td>
                        </tr>
                        <tr>
                            <td>但し書き</td>
                            <td id="td_proviso">
                            @if ((int)$data[0]->pdf_status !== 1)
                                <input type="text" class="form-control" name="proviso" id="proviso" value="{{$data[0]->proviso}}">
                            @else
                                {{$data[0]->proviso}}
                            @endif
                            </td>
                        </tr>
                        <tr>
                            <td>領収日</td>
                            <td id="td_receipt_date">
                            @if ((int)$data[0]->pdf_status !== 1 && Auth::check())

                                <input type="text" class="form-control" name="receipt_date" id="receipt_date"  class="receipt_date" value="{{$data[0]->receipt_date}}">
                            @else
                                @if (empty($data[0]->receipt_date))
                                    {{$data[0]->shipment_date}}
                                @else
                                    {{$data[0]->receipt_date}}
                                @endif
                            @endif
                            </td>
                        </tr>
                    </tbody></table>
                    <table id="tbldata" class="table table-bordered">
                        <thead>
                            <tr>
                                <th>商品ID</th>
                                <th>商品名</th>
                                <th>単価</th>
                                <th>数量</th>
                                <th>合計</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($data as $dat)
                            <tr>
                                <td>{{$dat->product_code}} </td>
                                <td>{{$dat->product_name}} </td>
                                <td>{{number_format($dat->price, 0, ',', ',')}} </td>
                                <td>{{$dat->quantity}}</td>
                                <td>{{number_format($dat->price * $dat->quantity, 0, ',', ',')}} </td>
                            </tr>
                            @endforeach
                            <tr>
                                <td colspan="3"></td>
                                <td>商品合計(税込)</td>
                                <td>{{number_format($dat->goods_price, 0, ',', ',')}}</td>
                            </tr>
                            <tr>
                                <td colspan="5" class="hide-border"></td>
                            </tr>
                            <tr>
                                <td colspan="3"></td>
                                <td>送料</td>
                                <td>{{(int)$data[0]->ship_charge}}</td>
                            </tr>
                            <tr>
                                <td colspan="3"></td>
                                <td>決済手数料</td>
                                <td>{{(int)$data[0]->pay_charge}}</td>
                            </tr>
                            <tr>
                                <td colspan="3"></td>
                                <td>合計金額</td>
                                <td>{{number_format($data[0]->total_price, 0, ',', ',')}}</td>
                            </tr>
                            <tr>
                                <td colspan="5" class="hide-border"></td>
                            </tr>
                            <tr>
                                <td colspan="3"></td>
                                <td>送料値引</td>
                                <td>{{(int)$data[0]->pay_charge_discount}}</td>
                            </tr>
                            <tr>
                                <td colspan="3"></td>
                                <td>クーポン</td>
                                <td>{{(int)$data[0]->used_coupon}}</td>
                            </tr>
                            <tr>
                                <td colspan="3"></td>
                                <td>ポイント</td>
                                <td>{{(int)$data[0]->used_point}}</td>
                            </tr>
                            <tr>
                                <td colspan="3"></td>
                                <td>割引</td>
                                <td>{{(int)$data[0]->discount}}</td>
                            </tr>
                            <tr>
                                <td colspan="3"></td>
                                <td>合計金額</td>
                                <td>{{number_format($data[0]->request_price, 0, ',', ',')}}</td>
                            </tr>


                        </tbody>
                    </table>

                    <table class="table table-bordered margin-table">
                        <tbody>
                            <tr>
                                <td>宛名と但書を変更しての領収書再発行は致しかねますのでご了承下さい。</td>
                            </tr>
                        </tbody>
                    </table>

                     <table class="table table-bordered">
                       <tbody>
                           <tr>
                               <td>出荷日より90日間のみこのページにアクセスできます。</td>
                           </tr>
                           <tr>
                               <td>それ以降はこのページにアクセスできません。</td>
                           </tr>
                       </tbody>
                    </table>

            <button type="button" class="btn btn-primary" id="btnReceipt">領収書PDF発行</button>

        @else
           <table class="table table-bordered" style="text-align: center; margin-top: 30px; ">
                    <tbody>
                        <tr>
                            <td>
                                こちらの領収書発行画面は期限が切れています。 <br/>
                                領収書発行期限は出荷日より90日以内となっておりますのでご了承ください。
                            </td>
                        </tr>
                    </tbody>
            </table>

            </form>
        @endif


        </div>
        <script src="{{ asset('/plugins/jquery/jquery.min.js') }}"></script>
        <script src="{{ asset('/plugins/bootstrap/js/bootstrap.min.js') }}"></script>
        <script src="{{ asset('/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js') }}"></script>
        <script src="{{ asset('/plugins/bootstrap-datepicker/locales/bootstrap-datepicker.ja.min.js') }}"  type="text/javascript"></script>
        <script type="text/javascript">

        $(function() {
            $('#receipt_date').datepicker({
                format: "yyyy-mm-dd",
                todayHighlight: true,
                autoclose: true,
                language: 'ja'
            });
        });
        $(function() {
            @if ((int)$data[0]->status !== 1)
                confirmMess = "領収書を発行してよろしいですか？\n再発行は出来ませんので、宛名･但書にお間違えがないかご確認下さい。";
            @else
                confirmMess = "領収書を発行してよろしいですか？\n再発行は出来ませんので、初めにご登録された宛名･但書で発行致します。";
            @endif
            $("#btnReceipt").click(function(){
                if (confirm(confirmMess)){
                    $('form#frmReceipt').submit();
                    $('#td_full_name').html($('#full_name').val());
                    $('#td_proviso').html($('#proviso').val());
                    $('#td_receipt_date').html($('#receipt_date').val());
                    $('#error_display').html('');
                    confirmMess = "領収書を発行してよろしいですか？\n再発行は出来ませんので、初めにご登録された宛名･但書で発行致します。";
                }
            });
        });

        </script>
        @if (isset($popup))
        <script type="text/javascript">
        alert("こちらの領収書は既に発行済です。ダウンロードは可能ですが、宛名･但書の変更は出来ませんのでご了承下さい。")
        </script>
        @endif
    </body>
</html>