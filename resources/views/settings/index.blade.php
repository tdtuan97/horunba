@extends('layouts.main')
@section('titlePage', __('messages.mail_template_header'))
@section('content')
<section class="content-header">
    <h1>
        {{__('messages.setting_template_header')}}
        <small>{{__('messages.mail_template_sub_header')}}</small>
    </h1>
    <ol class="breadcrumb">
        <li>
            <a href="{{route('orderStatusIndex')}}">
                <i class="fa fa-dashboard"></i> {{__('messages.setting_menu_title')}}
            </a>
        </li>
        <li>
            <a href="{{route('orderStatusIndex')}}">{{__('messages.setting_order_status_menu_title')}}</a>
        </li>
        <li class="active">{{__('messages.index_action')}}</li>
    </ol>
</section>

<section class="content">
    <!-- Put your code here -->
    <div id="root"></div>
</section>
@endsection
@section('javascript')
<script>
$('#system-setting-menu').addClass('active menu-open');
    $('#order-status-menu').addClass('active');
</script>
@endsection