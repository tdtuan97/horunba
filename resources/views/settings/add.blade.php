@extends('layouts.main')
@section('titlePage', __('messages.order_title'))
@section('content')
<section class="content-header">
    <h1>
        {{__('messages.add_mail_timing_header')}}
        <small>{{__('messages.add_mail_timing_sub_header')}}</small>
    </h1>
</section>

<section class="content">
    <!-- Put your code here -->
    <div id="root"></div>
</section>
@endsection
@section('javascript')
<script>
    $('#send-mail-timing-menu').addClass('active');
</script>
@endsection