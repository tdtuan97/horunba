@extends('layouts.main')
@section('titlePage', __('messages.store_order_header'))
@section('content')
<section class="content-header">
    <h1>
        {{__('messages.store_order_detail_header')}}
    </h1>
    <ol class="breadcrumb">
        <li>
            <a href="{{action('StoreOrderController@index')}}">
                {{__('messages.store_order_header')}}
            </a>
        </li>
        <li class="active">
            <a href="{{action('StoreOrderController@detail')}}">{{__('messages.store_order_detail_header')}}</a>
        </li>
    </ol>
</section>

<section class="content">
    <div id="root"><!-- content reactjs put here --></div>
    <div id="main-content" class="box" style="display: none">
        <!-- div class="loading"></div> -->
        <div class="box-header">
            <form class="form-horizontal" id="search-form">
                <div class="row">
                    <div class="col-md-3">
                        <div class="form-group">
                            <label class="col-md-3-5">{{__('messages.store_order_detail_id')}}</label>
                            <div class="col-md-6">
                                <input name="order_id" class="form-control input-sm" value="{{$storeOrderInfo->order_id}}" readonly="true">
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label class="col-md-3-5">{{__('messages.store_order_order_date')}}</label>
                            <div class="col-md-6">
                                <input name="order_date" class="form-control input-sm" value="{{date('Y/m/d', strtotime($storeOrderInfo->order_date))}}" readonly="true">
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label class="col-md-3-5">{{__('messages.store_order_detail_status')}}</label>
                            <div class="col-md-6">
                                <input name="status" class="form-control input-sm" value="{{$storeOrderStatus[$storeOrderInfo->status]??""}}" readonly="true">
                            </div>
                        </div>
                    </div>
                </div>
                
            </form>
        </div>
        <div class="box-body" id="main-contain">
            
        </div>
        <div class="box-footer">
            <a class="btn bg-purple input-sm pull-right" href="{{action('StoreOrderController@index')}}">{{__('messages.btn_cancel_store_order')}}</a>
            <button class="btn btn-success input-sm btn-larger pull-right margin-element" onclick="reOrder('{{$storeOrderInfo->order_id}}')">{{__('messages.btn_re_order')}}</button>
        </div>
    </div>
</section>
@endsection

@section('javascript')
<script id="list-template" type="text/x-handlebars-template">
    <div class="row">
        <div class="col-md-12">
            <input type="hidden" id="current-page" value="@{{data.current_page}}">
            @{{#if (gt data.last_page 1)}}
            <div class="row">
                <div class="col-md-2-5"></div>
                <div class="col-md-7 text-center">
                    <div class="text-center paginator-content"></div>
                </div>
                <div class="col-md-2-5 text-right line-height-md">
                    @{{data.from}}件 - @{{data.to}}件 【全@{{data.total}}件】
                </div>
            </div>
            @{{/if}}
            <div class="table-responsive">
                <table class="table table-striped table-custom">
                    <thead>
                        <tr>
                            <th class="visible-xs visible-sm"></th>
                            <th class="text-center text-middle">
                                {{__('messages.store_order_detail_no')}}
                            </th>
                            <th class="col-max-min-130 text-center text-middle">
                                {{__('messages.product_code')}}
                            </th>
                            <th class="col-max-min-300 text-center text-middle hidden-xs hidden-sm">
                                {{__('messages.product_name')}}
                            </th>
                            <th class="col-max-min-90 text-center text-middle">
                                {{__('messages.quantity')}}
                            </th>
                            <th class="col-max-min-100 text-center text-middle">
                                {{__('messages.store_order_process_status')}}
                            </th>
                            <th class="col-max-min-300 text-center text-middle hidden-xs hidden-sm">
                                {{__('messages.error_message')}}
                            </th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        @{{#if data.data}}
                            @{{#each data.data}}
                                <tr>
                                    <td class="visible-xs visible-sm">
                                        <b class="show-info">
                                            <i class="fa fa-angle-double-down" aria-hidden="true"></i>
                                        </b>
                                    </td>
                                    <td class="text-center" title="@{{this.index}}">
                                        @{{this.index}}
                                    </td>
                                    <td title="@{{this.product_code}}">
                                        @{{this.product_code}}
                                    </td>
                                    <td class="hidden-xs hidden-sm text-long-batch" title="@{{this.product_name}}">
                                        @{{this.product_name}}
                                    </td>
                                    <td class="text-right" title="@{{this.quantity}}">
                                        @{{this.quantity}}
                                    </td>
                                    <td class="text-center" title="@{{this.process_status}}">
                                        @{{this.process_status}}
                                    </td>
                                    <td class="hidden-xs hidden-sm text-long-batch" title="@{{this.error_message}}">
                                        @{{this.error_message}}
                                    </td>
                                    <td class="text-center">
                                        @{{#if (eq this.error_code 'E03')}}
                                        <button class="btn btn-primary btn-sm" onclick="setToOrder('@{{this.order_id}}', '@{{this.product_code}}')">{{__('messages.btn_set_to_order')}}</button>
                                        @{{/if}}
                                    </td>
                                </tr>
                                <tr class="hiden-info" style="display: none">
                                    <td colSpan="10" class="width-span1">
                                        <ul>
                                            <li> <b>{{__('messages.store_order_detail_no')}}</b> : @{{this.index}}</li>
                                            <li> <b>{{__('messages.product_code')}}</b> : @{{this.product_code}}</li>
                                            <li> <b>{{__('messages.product_name')}}</b> : @{{this.product_name}}</li>
                                            <li> <b>{{__('messages.quantity')}}</b> : @{{this.quantity}}</li>
                                            <li> <b>{{__('messages.store_order_process_status')}}</b> : @{{this.process_status}}</li>
                                            <li> <b>{{__('messages.error_message')}}</b> : @{{this.error_message}}</li>
                                        </ul>
                                    </td>
                                </tr>
                            @{{/each}}
                        @{{else}}
                            <tr><td colSpan="17" class="text-center">{{__('messages.no_data')}}</td></tr>
                        @{{/if}}
                    </tbody>
                </table>
            </div>
            @{{#if (gt data.last_page 1)}}
            <div class="text-center paginator-content"></div>
            @{{/if}}
        </div>
    </div>
</script>
@include('includes.paginate');
<script>
    var ignoreField = ['order_date', 'status'];
    var datalist = new DataList("{{action('StoreOrderController@getDetail')}}", ignoreField);
    var storeOrderProcessStatus = JSON.parse('{!! json_encode($storeOrderProcessStatus) !!}');
    function processData(_this, response) {
        var nf = new Intl.NumberFormat();
        response.data.data.map((item, index) => {
            item['index'] = ((response.data.current_page - 1) * response.data.per_page) + index + 1;
            item['class_row'] = (index % 2 === 0) ? 'odd' : 'even';
            item['process_status'] = storeOrderProcessStatus[item.process_status];
            item['quantity'] = nf.format(item.quantity);
        });
    }
    function handleClickPage(page = 1) {
        datalist.handleClickPage(page, processData);
    }
    function setToOrder(orderId, productCode) {
        $('.loading').show();
        $.ajax({
            url: "{{action('StoreOrderController@setToOrder')}}",
            data: {order_id: orderId, product_code: productCode},
            dataType: "json",
            success: function(response) {
                var icon = '';
                var color = '';
                if (response.flg === 1) {
                    icon  = 'glyphicon-ok';
                    color = 'success';
                } else {
                    icon  = 'glyphicon-warning-sign';
                    color = 'danger';
                }
                $.notify({
                    icon: 'glyphicon ' + icon,
                    message: response.msg,
                },{
                    type: color,
                    delay: 3000
                });
                var curPage = $("#current-page").val();
                datalist.handleClickPage(curPage, processData);
                $('.loading').hide();
            }
        });
    }
    function reOrder(orderId) {
        $('.loading').show();
        $.ajax({
            url: "{{action('StoreOrderController@reOrder')}}",
            data: {order_id: orderId},
            dataType: "json",
            success: function(response) {
                var icon = '';
                var color = '';
                if (response.flg === 1) {
                    icon  = 'glyphicon-ok';
                    color = 'success';
                } else {
                    icon  = 'glyphicon-warning-sign';
                    color = 'danger';
                }
                $.notify({
                    icon: 'glyphicon ' + icon,
                    message: response.msg,
                },{
                    type: color,
                    delay: 3000
                });
                var curPage = $("#current-page").val();
                datalist.handleClickPage(curPage, processData);
                $('.loading').hide();
                window.location.href = "{{action('StoreOrderController@index')}}"
            }
        });
    }
    $(document).ready(function() {
        $("#main-content").show();
        var arrHref = window.location.href.split('?');
        var dataPost = '';
        if (arrHref.length > 1) {
            dataPost = arrHref[1];
        }
        datalist.reloadData(dataPost, processData);
    });
</script>
@endsection