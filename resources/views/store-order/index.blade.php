@extends('layouts.main')
@section('titlePage', __('messages.store_order_header'))
@section('content')
<section class="content-header">
    <h1>
        {{__('messages.store_order_header')}}
    </h1>
    <ol class="breadcrumb">
        <li>
            <a href="{{action('StoreOrderController@index')}}">
                {{__('messages.store_order_header')}}
            </a>
        </li>
    </ol>
</section>

<section class="content">
    <!-- Put your code here -->
    <div id="root"></div>
</section>
@endsection
@section('javascript')
<script>
    var baseUrl     = "{{action('StoreOrderController@index')}}";
    var dataListUrl = "{{action('StoreOrderController@dataList')}}";
    var detailUrl = "{{action('StoreOrderController@detail')}}";
    $('#store-menu').addClass('active');
    $('#store-order-menu').addClass('active');
</script>
@endsection