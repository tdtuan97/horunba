@extends('layouts.main')
@section('titlePage', __('messages.send_mail_header'))
@section('content')
<section class="content-header">
    <h1>
        {{__('messages.send_mail_header')}}
        <small>{{__('messages.send_mail_sub_header')}}</small>
    </h1>
    <ol class="breadcrumb">
        <li>
            <a href="{{action('SendMailController@index')}}">
                <i class="fa fa-dashboard"></i> {{__('messages.setting_menu_title')}}
            </a>
        </li>
        <li>
            <a href="{{action('SendMailController@index')}}">{{__('messages.send_mail_breadcrumb_title')}}</a>
        </li>
        <li class="active">{{__('messages.detail_action')}}</li>
    </ol>
</section>

<section class="content">
    <!-- Put your code here -->
    <div id="root"></div>
</section>
@endsection
@section('javascript')
<script>
    $('#system-setting-menu').addClass('active');
    $('#send-mail-menu').addClass('active');
    var baseUrl      = "{{action('SendMailController@detail')}}";
    var ordelUrl     = "{{action('OrderManagementController@detail')}}";
    var iframeUrl    = "{{action('SendMailController@iframe')}}";
    var dashboardUrl = "{{action('DashboardController@index')}}";
</script>
@endsection