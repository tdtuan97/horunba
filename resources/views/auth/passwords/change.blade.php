@extends('layouts.main')
@section('titlePage', __('messages.change_password_header'))
@section('content')
<section class="content-header">
    <h1>
        {{__('messages.change_password_header')}}
    </h1>
    <ol class="breadcrumb">
        <li class="active">{{__('messages.change_password_breadcrumb_title')}}</li>
    </ol>
</section>

<section class="content">
    <!-- Put your code here -->
    <div id="root"></div>
</section>
@endsection
@section('javascript')
<script>
    @if (!empty(session('exp_mess')))
        $.notify({
            icon: 'glyphicon glyphicon-warning-sign',
            message: "{{session('exp_mess')}}",
        },{
            type: 'danger'
        });
    @endif
    $('#change_password-menu').addClass('active');
</script>
@endsection