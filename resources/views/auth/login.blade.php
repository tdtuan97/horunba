@extends('layouts.auth')

@section('css')
<link rel="stylesheet" href="{{ asset('/plugins/iCheck/square/blue.css') }}">
<link rel="stylesheet" href="{{ asset('/css/auth.css') }}">
@endsection

@section('content')
<div class="login-box">
    <div class="login-logo">
        <a href="/login"><b>{{ __('messages.layouts_auth_title') }}</b></a>
    </div>
    <div class="login-box-body">
        <p class="login-box-msg"></p>
        <form action="{{ route('login') }}" method="POST">
            {{ csrf_field() }}
            <div class="row form-group {{ $errors->has('tantou_code') ? ' has-error' : '' }}">
                <label for="email" class="col-md-3 control-label">{{__('messages.auth_login_id_title')}}</label>
                <div class="col-md-9">
                    <input type="text" class="form-control" name="tantou_code" value="{{ old('tantou_code') }}" placeholder="{{__('messages.auth_login_id_title')}}" required autofocus>
                    <span class="glyphicon glyphicon-user form-control-feedback"></span>
                    @if ($errors->has('tantou_code'))
                        <span class="help-block">
                            <strong>{{ $errors->first('tantou_code') }}</strong>
                        </span>
                    @endif
                </div>
            </div>
            <div class="row form-group {{ $errors->has('tantou_password') ? ' has-error' : '' }}">
                <label for="email" class="col-md-3 control-label">{{__('messages.auth_login_pass_title')}}</label>
                <div class="col-md-9">
                    <input type="password" class="form-control" name="tantou_password" placeholder="{{__('messages.auth_login_pass_title')}}" required>
                    <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                    @if ($errors->has('tantou_password'))
                        <span class="help-block">
                            <strong>{{ $errors->first('tantou_password') }}</strong>
                        </span>
                    @endif
                </div>
            </div>
            <div class="row">
                <div class="col-xs-8">
                    <div class="checkbox icheck">
                        <label>
                            <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}> {{__('messages.auth_login_remember_title')}}
                        </label>
                    </div>
                </div>
                <div class="col-xs-4">
                    <button type="submit" class="btn btn-primary btn-block btn-flat">{{__('messages.auth_login_btn_login_title')}}</button>
                </div>
            </div>
        </form>
    </div>
</div>
@endsection

@section('javascript')
<script src="{{ asset('/plugins/iCheck/icheck.min.js') }}"></script>
<script>
    $(function() {
        $('input').iCheck({
            checkedClass: 'checked',
            checkboxClass: 'icheckbox_square-blue',
            radioClass: 'iradio_square-blue',
            increaseArea: '20%'
        });
    });
</script>
@endsection