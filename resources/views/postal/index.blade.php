@extends('layouts.main')
@section('titlePage', __('messages.postal_header'))
@section('content')
<section class="content-header">
    <h1>
        {{__('messages.postal_header')}}
        <small>{{__('messages.postal_sub_header')}}</small>
    </h1>
    <ol class="breadcrumb">
        <li class="active">
            <a href="{{action('PostalController@index')}}">{{__('messages.postal_breadcrumb_title')}}</a>
        </li>
    </ol>
</section>

<section class="content">
    <!-- Put your code here -->
    <div id="root"></div>
</section>
@endsection
@section('javascript')
<script type="text/javascript">
    var baseUrl             = "{{action('PostalController@index')}}";
    var saveUrl             = "{{action('PostalController@save')}}";
    var changeFlagUrl       = "{{action('PostalController@changeFlag')}}";
    var dataListUrl         = "{{action('PostalController@dataList')}}";
    $('#system-setting-menu').addClass('active');
    $('#postal-menu').addClass('active');
</script>
@endsection