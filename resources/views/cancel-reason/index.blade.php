@extends('layouts.main')
@section('titlePage', __('messages.cancel_reason_header'))
@section('content')
<section class="content-header">
    <h1>
        {{__('messages.cancel_reason_header')}}
        <small>{{__('messages.cancel_reason_sub_header')}}</small>
    </h1>
    <ol class="breadcrumb">
        <li class="active">
            <a href="{{action('CancelReasonController@index')}}">{{__('messages.cancel_reason_breadcrumb_title')}}</a>
        </li>
    </ol>
</section>

<section class="content">
    <!-- Put your code here -->
    <div id="root"></div>
</section>
@endsection
@section('javascript')
<script type="text/javascript">
    var baseUrl     = "{{action('CancelReasonController@index')}}";
    var saveUrl     = "{{action('CancelReasonController@save')}}";
    var dataListUrl = "{{action('CancelReasonController@dataList')}}";
    $('#system-setting-menu').addClass('active');
    $('#cancel-reason-menu').addClass('active');
</script>
@endsection