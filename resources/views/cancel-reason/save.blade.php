@extends('layouts.main')
@section('titlePage', __('messages.cancel_reason_save_header'))
@section('content')
<section class="content-header">
    <h1>
        {{__('messages.cancel_reason_save_header')}}
        <small>{{__('messages.cancel_reason_save_sub_header')}}</small>
    </h1>
    <ol class="breadcrumb">
        <li>
            <a href="{{action('CancelReasonController@index')}}">{{__('messages.cancel_reason_breadcrumb_title')}}</a>            
        </li>
        <li class="active">
            <a href="{{Request::fullUrl()}}">{{__('messages.cancel_reason_breadcrumb_title')}}</a>
        </li>
    </ol>
</section>

<section class="content">
    <!-- Put your code here -->
    <div id="root"></div>
</section>
@endsection
@section('javascript')
<script>
    var baseUrl        = "{{action('CancelReasonController@index')}}";
    var saveUrl        = "{{action('CancelReasonController@save')}}";
    var getFormDataUrl = "{{action('CancelReasonController@getFormData')}}";
    var checkEditUrl   = "{{action('DashboardController@checkItemEdit')}}";
    $('#system-setting-menu').addClass('active');
    $('#cancel-reason-menu').addClass('active');
</script>
@endsection