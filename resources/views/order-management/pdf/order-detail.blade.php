<html>
    <head>
        <style>
            @font-face {
                font-family: 'msmincho';
                font-weight: normal;
                font-style: normal;
                font-variant: normal;
                src: url("{{url('/')}}/fonts/MS-Mincho.ttf") format("truetype");
            }
            @font-face {
                font-family: 'msmincho';
                font-weight: bold;
                font-style: bold;
                font-variant: bold;
                src: url("{{url('/')}}/fonts/MS-Mincho.ttf") format("truetype");
            }
            @font-face {
                font-family: 'msmincho';
                font-weight: italic;
                font-style: italic;
                font-variant: italic;
                src: url("{{url('/')}}/fonts/MS-Mincho.ttf") format("truetype");
            }
            .top {
                position: relative;
                font-size: 11pt;
            }
            .top-left {
                position: absolute;
                top: 0px;
                left: 0px;
            }
            .top-right {
                position: absolute;
                top: 0px;
                right: 0px;
            }
            .top-center {
                text-align: center;
                font-size: 40pt;
                font-weight: bold;
            }
            
            .top-mid {
                position: relative;
                height: 220px;
            }
            .customer-name {
                position: absolute;
                top: 50px;
                left: 0px;
            }
            .company-info-top {
                position: absolute;
                top: 50px;
                right: 0px;
                text-align: right;
            }
            .company-info-mid-text {
                position: absolute;
                top: 93px;
                right: 93px;
                text-align: right;
            }
            .daito_seal {
                position: absolute;
                top: 93px;
                right: 0px;
                text-align: right;
            }
            .company-info-bot {
                position: absolute;
                top: 180px;
                right: 0;
                text-align: right;
            }
            
            .customer-info-box {
                position: relative;
                border-top: 1px dotted black;
                border-bottom: 1px dotted black;
                width: 100%;
            }
            .customer-info {
                margin-left: 30px;
                margin-bottom: 20px;
                font-size: 11pt;
            }
            .customer-tel {
                position: absolute;
                top: 40px;
                right: 30px;
                font-size: 11pt;
            }
            
            .item-box {
                position: relative;
                border-bottom: 1px dotted black;
                padding-left: 30px;
                padding-top: 10px;
                padding-bottom: 10px;
            }
            
            .price-box {
                position: relative;
                height: 240px;
                margin-top: 10px;
                margin-left: 30px;
            }
            .table-price {
                position: absolute;
                right: 0px;
            }
            .table-price table tr td {
                border-bottom: 1px dotted black;
            }
            .table-price table tr td:nth-child(2) {
                text-align: right;
            }
            
            .header-table {
                display: inline-block;
                text-align: center;
            }
            .row-table {
                display: inline-block;
                vertical-align: top;
                margin-bottom: 3px;
            }
            .product-name-break {
                word-wrap: break-word;
                overflow-wrap: break-word;
                max-width: 400px;
            }
            .number-format {
                text-align: right;
            }
        </style>
    </head>
    <body>
        <div class="top">
            <div class="top-left">
                No. {{$data[0]->receive_id}}
            </div>
            <div class="top-right">
                {{$issue_date}}
            </div>
            <div class="top-center">
                納　品　書
            </div>
        </div>
        <div class="top-mid">
            <div class="customer-name">
                {{$customer_name}}様
            </div>
            <div class="company-info-top">
                株式会社大都<br/>
                DIY FACTORY
            </div>
            <div class="company-info-mid-text">
                〒544-0025<br/>
                大阪府大阪市生野区生野東<br/>
                2丁目5番3号<br/>
                FAX：06-6715-1666<br/>
            </div>
            <img src="{{public_path('img/daito_seal.jpg')}}" width="90px" class="daito_seal">
            <div class="company-info-bot">担当：{{$incharge_person}}</div>
        </div>
        <div class="customer-info-box">
            【配達先情報】
            <div class="customer-info">
                {{$data[0]->last_name}}{{!empty($data[0]->last_name)?'　':''}}{{$data[0]->first_name}}様<br/>
                〒{{$data[0]->zip_code}}<br/>
                {{$data[0]->prefecture . $data[0]->city . $data[0]->sub_address}}
            </div>
            <div class="customer-tel">
                TEL：{{ substr_replace(substr_replace($data[0]->tel_num, '-', 3, 0), '-', 8, 0) }}
            </div>
        </div>
        <div class="item-box">
            <div>
                <div class="header-table" style="width: 420px; text-align: left">商品名</div>
                <div class="header-table" style="width: 80px">単価</div>
                <div class="header-table" style="width: 60px">個数</div>
                <div class="header-table" style="width: 100px">金額（税込）</div>
            </div>
            <div>
                @foreach ($data as $item)
                <div>
                    <div class="row-table product-name-break" style="width: 420px;">{{$item->product_name}}</div>
                    <div class="row-table number-format" style="width: 80px">￥{{number_format($item->price)}}</div>
                    <div class="row-table number-format" style="width: 60px">{{$item->quantity}}</div>
                    <div class="row-table number-format" style="width: 100px">￥{{number_format($item->price*$item->quantity)}}</div>
                </div>
                @endforeach
            </div>
        </div>
        <div class="price-box">
            【備考欄】
            <div class="table-price">
                <table>
                    <tr>
                        <td width="100px">小計</td>
                        <td width="200px">￥{{number_format($data[0]->goods_price)}}</td>
                    </tr>
                    <tr>
                        <td>送料</td>
                        <td>￥{{number_format($data[0]->ship_charge)}}</td>
                    </tr>
                    <tr>
                        <td>代引料</td>
                        <td>￥{{number_format($data[0]->pay_after_charge)}}</td>
                    </tr>
                    <tr>
                        <td>決済料</td>
                        <td>￥{{number_format($data[0]->pay_charge)}}</td>
                    </tr>
                    <tr>
                        <td>ポイント</td>
                        <td>{{number_format($data[0]->used_point)}}</td>
                    </tr>
                    <tr>
                        <td>クーポン</td>
                        <td>{{number_format($data[0]->used_coupon)}}</td>
                    </tr>
                    <tr>
                        <td>値引き</td>
                        <td>￥{{number_format($data[0]->pay_charge)}}</td>
                    </tr>
                    <tr>
                        <td>請求金額（税込）</td>
                        <td><font style="font-size: 16pt">￥{{number_format($data[0]->request_price)}}</font></td>
                    </tr>
                </table>
            </div>
        </div>
    </body>
</html>