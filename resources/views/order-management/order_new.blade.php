@extends('layouts.main')
@section('titlePage', __('messages.order_editing_header'))
@section('content')
<section class="content-header">
    <h1>
        {{__('messages.mail_order_management_header')}}
        <small>{{__('messages.neworder_header')}}</small>
    </h1>
    <ol class="breadcrumb">
        <li>
            <a href="{{action('OrderManagementController@index')}}">{{__('messages.mail_order_management_header')}}</a>
        </li>
        <li class="active">
            <a href="{{Request::fullUrl()}}">{{__('messages.neworder_header')}}</a>
        </li>
    </ol> 
</section>

<section class="content">
    <!-- Put your code here -->
    <div id="root"></div>
</section>
@endsection
@section('javascript')
<script>
    var baseUrl             = "{{action('OrderManagementController@index')}}";
    var newOrderUrl         = "{{action('OrderManagementController@newOrder')}}";
    var saveUrl             = "{{action('OrderManagementController@orderEditing')}}";
    var detailUrl           = "{{action('OrderManagementController@detail')}}";
    var getFormOrderDataUrl = "{{action('OrderManagementController@getFormOrderData')}}";
    var checkEditUrl   = "{{action('DashboardController@checkItemEdit')}}";
    $('#order-management-menu').addClass('active');
</script>
@endsection