@extends('layouts.main')
@section('titlePage', __('messages.order_detail_editing_header'))
@section('content')
<section class="content-header">
    <h1>
        {{__('messages.order_detail_editing_header')}}
        <small>{{__('messages.order_detail_editing_sub_header')}}</small>
    </h1>
    <ol class="breadcrumb">
        <li>
            <a href="{{action('OrderManagementController@index')}}">{{__('messages.order_editing_breadcrumb_title')}}</a>
        </li>
        <li class="active">
            <a href="{{Request::fullUrl()}}">{{__('messages.order_detail_editing_header')}}</a>
        </li>
    </ol>
</section>

<section class="content">
    <!-- Put your code here -->
    <div id="root"></div>
</section>
<div class="popup-error fix-message collapse" id="popup-status">
    <div class="alert alert-dismissible alert-error">
        <h4><i class="icon fa fa-ban"></i> Alert!</h4>
        <p class="text-new-line" id="popup-message"></p>
    </div>
</div>
@endsection
@section('javascript')
<script>
    var baseUrl           = "{{action('OrderManagementController@index')}}";
    var saveUrl           = "{{action('OrderManagementController@orderDetailEditing')}}";
    var detailUrl         = "{{action('OrderManagementController@detail')}}";
    var getFormDataUrl    = "{{action('OrderManagementController@getFormOrderDetailData')}}";
    var getDataProductUrl = "{{action('OrderManagementController@getDataProduct')}}";
    var checkEditUrl      = "{{action('DashboardController@checkItemEdit')}}";
    var cancelReasonUrl   = "{{action('OrderManagementController@updateCancelOrder')}}";
    var checkCancelAll    = "{{action('OrderManagementController@checkCancelAll')}}";
    var paramsFilter      = "{{$paramsFilter}}";
    var cancelNotEnought  = "{{action('OrderManagementController@cancelNotEnought')}}";
    $('#order-management-menu').addClass('active');
</script>
@endsection