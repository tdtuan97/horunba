@extends('layouts.main')
@section('titlePage', __('messages.mail_order_management_header'))
@section('content')
<section class="content-header">
    <h1>
        {{__('messages.mail_order_management_header')}}
    </h1>
    <ol class="breadcrumb">
        <li>
            <a href="{{action('OrderManagementController@index')}}">
                {{__('messages.mail_order_management_menu')}}
            </a>
        </li>
    </ol>
</section>

<section class="content">
    <!-- Put your code here -->
    <div id="root"></div>
</section>
@endsection
@section('javascript')
<script type="text/javascript">
    var newOrderUrl     = "{{action('OrderManagementController@newOrder')}}";
    var baseUrl         = "{{action('OrderManagementController@index')}}";
    var dataListUrl     = "{{action('OrderManagementController@dataList')}}";
    var changeStatusUrl = "{{action('SettlementController@changeStatus')}}";
    var detailUrl       = "{{action('OrderManagementController@detail')}}";
    var exportCSV       = "{{action('Common@exportCSV')}}";
    var importUrl       = "{{action('OrderManagementController@import')}}";
    var csvUrl          = "{{action('Common@importCSV')}}";
    var processExportCSV    = "{{action('OrderManagementController@processExportCsv')}}";    
    $('#order-management-menu').addClass('active');
</script>
@endsection