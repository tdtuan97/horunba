@extends('layouts.main')
@section('titlePage', __('messages.order_editing_header'))
@section('content')
<section class="content-header">
    <h1>
        {{__('messages.order_editing_header')}}
        <small>{{__('messages.order_editing_sub_header')}}</small>
    </h1>
    <ol class="breadcrumb">
        <li>
            <a href="{{action('OrderManagementController@index')}}">{{__('messages.order_editing_breadcrumb_title')}}</a>
        </li>
        <li class="active">
            <a href="{{Request::fullUrl()}}">{{__('messages.order_editing_header')}}</a>
        </li>
    </ol> 
</section>

<section class="content">
    <!-- Put your code here -->
    <div id="root"></div>
</section>
@endsection
@section('javascript')
<script>
    var baseUrl             = "{{action('OrderManagementController@index')}}";
    var saveUrl             = "{{action('OrderManagementController@orderEditing')}}";
    var detailUrl           = "{{action('OrderManagementController@detail')}}";
    var getFormOrderDataUrl = "{{action('OrderManagementController@getFormOrderData')}}";
    var checkEditUrl        = "{{action('DashboardController@checkItemEdit')}}";
    var paramsFilter        = "{{$paramsFilter}}";
    $('#order-management-menu').addClass('active');
</script>
@endsection