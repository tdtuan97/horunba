@extends('layouts.main')
@section('titlePage', __('messages.order_management_detail'))
@section('content')
<section class="content-header text-center">
    <a style="float:left" href="{{$indexUrl}}">
        <i class="fa fa-arrow-left fa-2x"></i>
    </a>

    @if ($prevUrl !== null)
    <a style="margin-right: 10px" href="{{$prevUrl}}">
        <i class="fa fa-angle-double-left fa-2x"></i>
    </a>
    @else
    <a class="link-disabled" style="margin-right: 10px" href="javascript:void(0);">
        <i class="fa fa-angle-double-left fa-2x"></i>
    </a>
    @endif
    <span style="font-size:20px; margin-right: 10px ; vertical-align: text-bottom;">{{$pageOnPages}}</span>
    @if ($nextUrl !== null)
    <a href="{{$nextUrl}}">
        <i class="fa fa-angle-double-right fa-2x"></i>
    </a>
    @else
    <a class="link-disabled" href="javascript:void(0);">
        <i class="fa fa-angle-double-right fa-2x"></i>
    </a>
    @endif
    <ol class="breadcrumb">
        <li>
            <a href="{{action('OrderManagementController@index')}}">
                {{__('messages.mail_order_management_menu')}}
            </a>
        </li>
    </ol>
</section>
<div class="popup-error fix-message collapse" id="popup-status">
    <div class="alert alert-dismissible alert-error">
        <h4><i class="icon fa fa-ban"></i> Alert!</h4>
        <p class="text-new-line" id="popup-message"></p>
    </div>
</div>
<section class="content">
    <!-- Put your code here -->
    <div id="root"></div>
</section>
@endsection
@section('javascript')
<script>
    var baseUrl               = "{{action('OrderManagementController@index')}}";
    var detailUrl             = "{{action('OrderManagementController@detail')}}";
    var changeDelayUrl        = "{{action('OrderManagementController@changeDelay')}}";
    var changeIsComfirmedUrl  = "{{action('OrderManagementController@changeIsConfirmed')}}";
    var exportIssueUrl        = "{{action('OrderManagementController@exportIssue')}}";
    var getInfoLogUrl         = "{{action('OrderUpdateLogController@getInfoLog')}}";
    var returnSaveUrl         = "{{action('ReturnManagementController@save')}}";
    var orderEditingUrl       = "{{action('OrderManagementController@orderEditing')}}";
    var orderDetailEditingUrl = "{{action('OrderManagementController@orderDetailEditing')}}";
    var addMemoUrl            = "{{action('OrderUpdateLogController@addMemo')}}";
    var updateLogStatusUrl    = "{{action('OrderUpdateLogController@updateStatus')}}";
    var getItemMemoUrl        = "{{action('OrderUpdateLogController@getItem')}}";
    var receiveMailSaveUrl    = "{{action('ReceiveMailController@save')}}";
    var editSendMailUrl       = "{{action('EditSendMailController@save')}}";
    var orderReturnDetailUrl  = "{{action('ReturnManagementController@saveReturnDetail')}}";
    var orderUpdateFlgMall    = "{{action('OrderManagementController@updateFlgMall')}}";
    var orderReCalculate      = "{{action('OrderManagementController@reCalculate')}}";
    var sendMailUrl           = "{{action('SendMailController@detail')}}";
    var refundDetailSaveUrl   = "{{action('RefundDetailController@save')}}";
    var cancelReasonUrl       = "{{action('OrderManagementController@updateCancelOrder')}}";
    var updateDeliveryUrl     = "{{action('OrderManagementController@updateDelivery')}}";
    var countMemoUrl          = "{{action('MemoCheckController@countMemo')}}";
    var checkCancelAll        = "{{action('OrderManagementController@checkCancelAll')}}";
    var cancelNotEnought      = "{{action('OrderManagementController@cancelNotEnought')}}";
    var changeBackList        = "{{action('OrderManagementController@changeBackList')}}";
    var changeisClaim         = "{{action('OrderManagementController@changeisClaim')}}";
    var changeDetailClaim     = "{{action('OrderManagementController@changeDetailClaim')}}";
    var changeAddClaim        = "{{action('OrderManagementController@changeAddClaim')}}";
    $('#order-management-menu').addClass('active');
</script>
@endsection