<html>
    <head>
        <style>
            @font-face {
                font-family: 'meiryo';
                font-weight: normal;
                font-style: normal;
                font-variant: normal;
                src: url("{{url('/')}}/fonts/Meiryo.ttf") format("truetype");
            }
            @font-face {
                font-family: 'meiryo-bold';
                font-weight: bold;
                font-style: bold;
                font-variant: bold;
                src: url("{{url('/')}}/fonts/Meiryo-Bold.ttf") format("truetype");
            }
            @font-face {
                font-family: 'meiryo-italic';
                font-weight: italic;
                font-style: italic;
                font-variant: italic;
                src: url("{{url('/')}}/fonts/Meiryo-Italic.ttf") format("truetype");
            }
            body {
                font-family: meiryo;
                padding: 1px;
            }
            .top-left {
                text-align: left;
                font-size: 20px;
                font-family: 'meiryo-bold';
            }
            .table-data {
                border: 1px solid;
                border-collapse: collapse;
                width: 600px;
                word-wrap: break-word;
                /*table-layout:fixed;*/
            }
            
            .table-data tr th {
                border: 1px solid;
                font-family: 'meiryo-bold';
            }
            .table-data tr td {
                border: 1px solid;
            }
            tr.content-no-border th {
                border:none;
                border-right: 1px solid;
            }
            tr.content-no-border td {
                border:none;
                line-height: 12px;
            }
        </style>
    </head>
    <body>
        <h1 class="top-left">株式会社大都　物流業務連絡管理</h1>
        <table class="table-data">
            <tr>
                <th style="width:20%">レコード番号</th>
                <td style="width:80%" colspan="3">{{$dejeData->deje_id}}</td>
            </tr>
            <tr>
                <th>登録日時</th>
                <td style="width:35%">{{date('Y/m/d H:i:s', strtotime($dejeData->in_date))}}</td>
                <th style="width:15%">登録者</th>
                <td style="width:30%">{{$dejeData->last_name_in . $dejeData->first_name_in}}</td>
            </tr>
            <tr>
                <th>更新日時</th>
                <td>{{date('Y/m/d H:i:s', strtotime($dejeData->up_date))}}</td>
                <th>更新者</th>
                <td>{{$dejeData->last_name_up . $dejeData->first_name_up}}</td>
            </tr>
        </table>
        <br/><br/>
        <table class="table-data">
            <tr>
                <th style="width:20%">表題</th>
                <td style="width:80%" colspan="3">{{$dejeData->subject}}</td>
            </tr>
            <tr>
                <th>対応部署</th>
                <td colspan="3">{{$dejeData->object_type}}</td>
            </tr>
            <tr>
                <th>対応状況</th>
                <td style="width:35%">{{$dejeData->process_status}}</td>
                <th style="width:10%">業務種別</th>
                <td style="width:35%">{{$dejeData->style_name}}</td>
            </tr>
            <tr>
                <th>大都担当者</th>
                <td>{{$dejeData->last_name_dai . $dejeData->first_name_dai}}</td>
                <th>対応方法</th>
                <td>{{$dejeData->process_method}}</td>
            </tr>
            <tr class='content-no-border'>
                <th>
                    @if ($dejeData->posHead === 0)
                    内容
                    @endif
                </th>
                <td colspan="3" style="padding-top:5px;">
                    {{$dejeData->arrContent[0]}}
                </td>
            </tr>
            @if ($dejeData->countContent > 1)
            @for ($i = 1; $i < $dejeData->countContent; $i++)
            <tr class='content-no-border'>
                <th>
                    @if ($dejeData->posHead === $i)
                    内容
                    @endif
                </th>
                <td colspan="3" style="{{($i === ($dejeData->countContent - 1))?'padding-bottom:5px':''}}">
                    @if(empty($dejeData->arrContent[$i]))
                    <br/>
                    @else
                    {{$dejeData->arrContent[$i]}}
                    @endif
                </td>
            </tr>
            @endfor
            @endif
        </table>
    </body>
</html>