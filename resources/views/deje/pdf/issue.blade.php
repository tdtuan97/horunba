<html>
    <head>
        <style>
            @font-face {
                font-family: 'msmincho';
                font-weight: normal;
                font-style: normal;
                font-variant: normal;
                src: url("{{url('/')}}/fonts/MS-Mincho.ttf") format("truetype");
            }
            @font-face {
                font-family: 'msmincho';
                font-weight: bold;
                font-style: bold;
                font-variant: bold;
                src: url("{{url('/')}}/fonts/MS-Mincho.ttf") format("truetype");
            }
            @font-face {
                font-family: 'msmincho';
                font-weight: italic;
                font-style: italic;
                font-variant: italic;
                src: url("{{url('/')}}/fonts/MS-Mincho.ttf") format("truetype");
            }
            .top {
                position: relative;
                font-size: 11pt;
            }
            .top-left {
                position: absolute;
                top: 0px;
                left: 0px;
            }
            .top-right {
                position: absolute;
                top: 0px;
                right: 0px;
            }
            .top-center {
                text-align: center;
                font-size: 40pt;
                font-weight: bold;
            }

            .top-mid {
                position: relative;
                height: 220px;
            }
            .order-id {
                position: absolute;
                top: 50px;
                left: 0px;
            }
            .customer-name {
                position: absolute;
                top: 100px;
                left: 0px;
            }
            .company-info-top {
                position: absolute;
                top: 50px;
                right: 0px;
                text-align: right;
            }
            .company-info-mid-text {
                position: absolute;
                top: 93px;
                right: 93px;
                text-align: right;
            }
            .daito_seal {
                position: absolute;
                top: 93px;
                right: 0px;
                text-align: right;
            }
            .company-info-bot {
                position: absolute;
                top: 180px;
                right: 0;
                text-align: right;
            }

            .item-box {
                position: relative;
                border-top: 1px dotted black;
                border-bottom: 1px dotted black;
                padding-left: 30px;
                padding-top: 10px;
                padding-bottom: 10px;
            }

            .price-box {
                position: relative;
                height: 240px;
                margin-top: 10px;
                margin-left: 30px;
            }
            .table-price {
                position: absolute;
                right: 0px;
            }
            .table-price table tr td {
                border-bottom: 1px dotted black;
            }
            .table-price table tr td:nth-child(2) {
                text-align: right;
            }

            .header-table {
                display: inline-block;
                text-align: center;
            }
            .row-table {
                display: inline-block;
                vertical-align: top;
                margin-bottom: 3px;
            }
            .product-name-break {
                word-wrap: break-word;
                overflow-wrap: break-word;
                max-width: 400px;
            }
            .number-format {
                text-align: right;
            }
            .input-label {
                float:left;
            }

        </style>
    </head>
    <body>
        <div class="top">
            <div class="top-left">
                ロジコNo. {{$deje_id}}
            </div>
            <div class="top-right">
                {{$issue_date}}
            </div>
            <div class="top-center">
                納　品　書
            </div>
        </div>
        <div class="top-mid">
            <div class="order-id">
                <div class="input-label">受注番号：</div><div>{{$order_id}}</div>
            </div>
            <div class="customer-name">
                <div class="input-label">お客様名：</div><div>{!! $customer_name !!}</div>
            </div>
            <div class="company-info-top">
                株式会社大都<br/>
                DIY FACTORY
            </div>
            <div class="company-info-mid-text">
                〒544-0025<br/>
                大阪府大阪市生野区生野東<br/>
                2丁目5番3号<br/>
                FAX：06-6715-1666<br/>
            </div>
            <img src="{{public_path('img/daito_seal.jpg')}}" width="90px" class="daito_seal">
            {{-- <div class="company-info-bot">担当：{{$incharge_person}}</div> --}}
        </div>
        <div class="item-box">
            <div>
                <div class="header-table" style="width: 420px; text-align: left">商品名</div>
                <div class="header-table" style="width: 80px">単価</div>
                <div class="header-table" style="width: 60px">個数</div>
                <div class="header-table" style="width: 100px">金額（税込）</div>
            </div>
            <div>
                @foreach ($data as $item)
                <div>
                    <div class="row-table product-name-break" style="width: 420px;">{{$item->product_name_long}}</div>
                    <div class="row-table number-format" style="width: 80px">￥{{number_format($item->price_komi)}}</div>
                    <div class="row-table number-format" style="width: 60px">{{$item->quantity}}</div>
                    <div class="row-table number-format" style="width: 100px">￥{{number_format($item->price_komi*$item->quantity)}}</div>
                </div>
                @endforeach
            </div>
        </div>
        <div class="price-box">
            【備考欄】
            <div class="table-price">
                <table>
                    <tr>
                        <td width="100px">小計</td>
                        <td width="200px">￥{{number_format($total_price)}}</td>
                    </tr>
                    <tr>
                        <td>送料</td>
                        <td>0</td>
                    </tr>
                    <tr>
                        <td>代引料</td>
                        <td>0</td>
                    </tr>
                    <tr>
                        <td>決済料</td>
                        <td>0</td>
                    </tr>
                    <tr>
                        <td>ポイント</td>
                        <td>0</td>
                    </tr>
                    <tr>
                        <td>クーポン</td>
                        <td>0</td>
                    </tr>
                    <tr>
                        <td>値引き</td>
                        <td>0</td>
                    </tr>
                    <tr>
                        <td>請求金額（税込）</td>
                        <td><font style="font-size: 16pt">￥{{number_format($total_price)}}</font></td>
                    </tr>
                </table>
            </div>
        </div>
    </body>
</html>