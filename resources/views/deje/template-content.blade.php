@switch($param)
{{-- 1
@case('SC【未入荷】調査')--}}
@case('1')
EDI出荷済で未入荷

@foreach ($datas as $item)
発注番号: {!! $item['order_code'] !!}
発注数量: {!! $item['order_num'] !!}
納品番号: {!! $item['order_num'] !!}
商品コード: {!! $item['product_code'] !!}
JAN : {!! $item['product_jan'] !!}
商品名: {!! $item['ItemNm'] !!}

@endforeach
@break

{{-- 2
@case('SC【入荷】予定外') --}}
@case('2')
入荷データがない

@foreach ($datas as $item)
発注番号: {!! $item['order_code'] !!}
発注数量: {!! $item['order_num'] !!}
納品番号: {!! $item['order_num'] !!}
商品コード: {!! $item['product_code'] !!}
JAN : {!! $item['product_jan'] !!}
商品名: {!! $item['ItemNm'] !!}

@endforeach
@break

{{-- 3
@case('SC【入荷】破損')--}}
@case('3')
入荷時破損

@foreach ($datas as $item)
発注番号: {!! $item['order_code'] !!}
発注数量: {!! $item['order_num'] !!}
納品番号: {!! $item['order_num'] !!}
商品コード: {!! $item['product_code'] !!}
JAN : {!! $item['product_jan'] !!}
商品名: {!! $item['ItemNm'] !!}

@endforeach
@break

{{-- 4
@case('SC【入荷】商品確認')--}}
@case('4')
JAN型番商品名違う

@foreach ($datas as $item)
発注番号: {!! $item['order_code'] !!}
発注数量: {!! $item['order_num'] !!}
納品番号: {!! $item['order_num'] !!}
商品コード: {!! $item['product_code'] !!}
JAN : {!! $item['product_jan'] !!}
商品名: {!! $item['ItemNm'] !!}

@endforeach
@break

{{-- 5
@case('CS【顧客返送商品】')--}}
@case('5')
顧客からの返品

@foreach ($datas as $item)
受注番号:{!! $item['received_order_id'] !!}
数量:{!! $item['received_order_num'] !!}
JAN : {!! $item['product_jan'] !!}
商品コード: {!! $item['product_code'] !!}
商品名: {!! $item['product_name_long'] !!}
送付先氏名:{!! $item['delivery_name'] !!}

@endforeach
@break

{{-- 6
@case('CS【佐川JP調査依頼】')--}}
@case('6')
不在・転居・受け取り拒否で配送業者から連絡

@foreach ($datas as $item)
受注番号: {!! $item['received_order_id'] !!}
送り状番号:{!! $item['inquiry_no'] !!}
送付先氏名:{!! $item['delivery_name'] !!}
出荷日:{!! $item['delivery_real_date'] ? date("Y-m-d", strtotime($item['delivery_real_date'])) : ''!!}

@endforeach
@break

{{-- 7
@case('SC【出荷不可】')--}}
@case('7')
佐川出荷不可

@foreach ($datas as $item)
発注番号: {!! $item['order_code'] !!}
発注数量: {!! $item['order_num'] !!}
納品番号: {!! $item['order_num'] !!}
商品コード: {!! $item['product_code'] !!}
JAN : {!! $item['product_jan'] !!}
商品名: {!! $item['ItemNm'] !!}
仕入先: {!! $item['SupplierNm'] !!}
受注番号 :{!! $item['received_order_id'] !!}

@endforeach
@break

{{-- 8
@case('SC【入出荷ミス・庫内破損】')--}}
@case('8')
在庫狂い

@foreach ($datas as $item)
受注番号:{!! $item['received_order_id'] !!}
注文者氏名:{!! $item['full_name'] !!}
数量:{!! $item['received_order_num'] !!}
JAN:{!! $item['product_jan'] !!}
商品コード:{!! $item['product_code'] !!}
商品名:{!! $item['product_name_long'] !!}

@endforeach
@break

{{-- 9
@case('CS【請求】佐川JP破損')--}}
@case('9')
配達時の破損連絡

@foreach ($datas as $item)
受注番号:{!! $item['received_order_id'] !!}
送り状番号:{!! $item['inquiry_no'] !!}
送付先氏名:{!! $item['delivery_name'] !!}
出荷日:{!! $item['delivery_real_date'] ? date("Y-m-d", strtotime($item['delivery_real_date'])) : ''!!}
商品番号:{!! $item['product_code'] !!}
商品名:{!! $item['product_name'] !!}
数量:{!! $item['delivery_num'] !!}

@endforeach
@break

{{-- 10
@case('CS【引取依頼】')--}}
@case('10')
お客様へ商品引取り依頼

@foreach ($datas as $item)
受注番号:{!! $item['received_order_id'] !!}
送り状番号:{!! $item['inquiry_no'] !!}
商品名:{!! $item['product_name'] !!}
数量:{!! $item['delivery_num'] !!}

@endforeach
@break

{{-- 11
@case('CS【配送情報変更】')--}}
@case('11')
出荷後の送付先変更依頼

@foreach ($datas as $item)
受注番号:{!! $item['received_order_id'] !!}
送り状番号:{!! $item['inquiry_no'] !!}
送付先氏名:{!! $item['delivery_name'] !!}
送付先〒住所:{!! $item['full_address'] !!}
送付先TEL:{!! $item['delivery_tel'] !!}
出荷日:{!! $item['delivery_real_date'] ? date("Y-m-d", strtotime($item['delivery_real_date'])) : ''!!}

@endforeach
@break

{{-- 12
@case('CS【出荷トラブル】')--}}
@case('12')
出荷商品の不足など顧客からの問合せ

@foreach ($datas as $item)
受注番号:{!! $item['received_order_id'] !!}
送り状番号:{!! $item['inquiry_no'] !!}
送付先氏名:{!! $item['delivery_name'] !!}
出荷日:{!! $item['delivery_real_date'] ? date("Y-m-d", strtotime($item['delivery_real_date'])) : ''!!}
商品番号:{!! $item['product_code'] !!}
商品名:{!! $item['product_name'] !!}
数量:{!! $item['delivery_num'] !!}

@endforeach
@break

{{-- 13
@case('SC【資材管理】')--}}
@case('13')
パッキン、ショップバックの発注@break

{{-- 14
@case('SC【在庫確認】')--}}
@case('14')
システム在庫数と実在庫の確認
@foreach ($datas as $data)
商品コード: {!! $data['product_code'] !!}
商品名:{!! $data['product_name_long'] !!}
JAN: {!! $data['product_jan'] !!}

@endforeach
@break

{{-- 15
@case('SC【その他依頼】')--}}
@case('15')
その他の依頼案件@break

@endswitch