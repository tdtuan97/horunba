@extends('layouts.main')
@section('titlePage', __('messages.deje_management_save_header'))
@section('content')
<section class="content-header">
    <h1>
        {{__('messages.deje_header')}}
    </h1>
</section>

<section class="content">
    <!-- Put your code here -->
    <div id="root"></div>
</section>
@endsection
@section('javascript')
<script>
    var baseUrl     = "{{action('DejeController@index')}}";
    var saveUrl     = "{{action('DejeController@save')}}";
    var dataListUrl = "{{action('DejeController@dataList')}}";
    var detailUrl   = "{{action('DejeController@detail')}}";
    var deleteUrl   = "{{action('DejeController@delete')}}";
    $('#deje-menu').addClass('active');

</script>
@endsection