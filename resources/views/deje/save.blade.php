@extends('layouts.main')
@section('titlePage', __('messages.deje_management_save_header'))
@section('content')
<section class="content-header">
    <h1>
        {{__('messages.deje_management_save_header')}}
        <small></small>
    </h1>
    <ol class="breadcrumb">
        <li>
            <a href="{{action('DejeController@index')}}">
                {{__('messages.deje_management_breadcrumb_title')}}
            </a>
        </li>
        <li>
            <a href="{{Request::fullUrl()}}">
                {{__('messages.save_action')}}
            </a>
        </li>
    </ol>
</section>

<section class="content">
    <!-- Put your code here -->
    <div id="root"></div>
</section>
@endsection
@section('javascript')
<script>
    var saveUrl       = "{{action('DejeController@save')}}";
    var getDataDetail = "{{action('DejeController@getDataDetail')}}";
    var parseUrl      = "{{action('DejeController@processParseContent')}}";
    var baseUrl       = "{{action('DejeController@index')}}";
    var checkEditUrl  = "{{action('DashboardController@checkItemEdit')}}";
    var getProductUrl = "{{action('DejeController@getProduct')}}";
    var saveComtUrl   = "{{action('DejeController@detail')}}";
    var printDejeUrl  = "{{action('DejeController@printDetail')}}";
    var urlClone      = "{{action('DejeController@cloneData')}}";
    var exportIssueUrl = "{{action('DejeController@exportIssue')}}";
    $('#deje-menu').addClass('active');
    var backUrl = "{!!$backUrl!!}";
</script>
@endsection