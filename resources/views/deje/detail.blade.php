@extends('layouts.main')
@section('titlePage', __('messages.deje_management_save_header'))
@section('content')
<section class="content-header">
    <h1>
        {{__('messages.deje_management_save_header')}}
        <small></small>
    </h1>
    <ol class="breadcrumb">
        <li>
            <a href="{{action('DejeController@save')}}">
                {{__('messages.deje_management_breadcrumb_title')}}
            </a>
        </li>
        <li>
            <a href="{{Request::fullUrl()}}">
                {{__('messages.save_action')}}
            </a>
        </li>
    </ol>
</section>

<section class="content">
    <!-- Put your code here -->
    <div id="root"></div>
</section>
@endsection
@section('javascript')
<script>
    var saveUrl        = "{{action('DejeController@save')}}";
    var baseUrl        = "{{action('DejeController@index')}}";
    var getDataDetail  = "{{action('DejeController@getDataDetail')}}";
    var exportIssueUrl = "{{action('DejeController@exportIssue')}}";
    var updateDataUrl  = "{{action('DejeController@updateDataDetail')}}";
    var urlClone       = "{{action('DejeController@cloneData')}}";
    $('#deje-menu').addClass('active');
    var backUrl = "{!!$backUrl!!}";
</script>
@endsection