<!doctype html>
<html lang="{{ app()->getLocale() }}">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>@yield('titlePage')</title>
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link href="{{asset('css/app.css')}}" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="{{ asset('/plugins/bootstrap/css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('/plugins/font-awesome/css/font-awesome.min.css') }}">
    <link rel="stylesheet" href="{{ asset('/adminsystem/css/AdminLTE.min.css') }}">
    <link rel="stylesheet" href="{{ asset('/adminsystem/css/dataTables.bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('/adminsystem/css/skins.min.css') }}">
    <link rel="stylesheet" href="{{ asset('/adminsystem/css/fonts.css') }}">
    <link rel="stylesheet" href="{{ asset('/adminsystem/css/bootstrap-multiselect.css') }}">
    <link rel="stylesheet" href="{{ asset('/adminsystem/css/prettify.min.css') }}">
    <link rel="stylesheet" href="{{ asset('/adminsystem/css/custom.css') }}">
    <link rel="stylesheet" href="{{ asset('/adminsystem/css/animate.css') }}">
    <link rel="stylesheet" href="{{ asset('/adminsystem/css/ekko-lightbox.css') }}">
    <link rel="stylesheet" href="{{ asset('/adminsystem/css/combo.select.css') }}">
    @yield('css')
</head>
@php

    $sidebarToggle = '';
    if (isset($_COOKIE['sidebar_toggle'])) {
        if ($_COOKIE['sidebar_toggle'] === 'true' || $_COOKIE['sidebar_toggle'] === 'null') {
            $sidebarToggle = 'sidebar-collapse';
        } else {
            $sidebarToggle = '';
        }
    }
@endphp
<body class="hold-transition skin-{{$globalVar['setMenu']}} sidebar-mini {{$sidebarToggle}}">
    <div class="wrapper">
        @include('includes.header')
        @include('includes.navigation')
        <div class="content-wrapper" style="min-height: 86%">
            @yield('content')
        </div>
        <div class="control-sidebar-bg"></div>
        <div id="showPopup"></div>
        @include('includes.footer')
    </div>
    <script type="text/javascript">
        var uploadCsvUrl         = "{{action('UploadCsvController@save')}}";
    </script>
    <script src="{{asset('js/status.js')}}" ></script>
    <script src="{{asset('js/lang.js')}}" ></script>
    <script src="{{ asset('/plugins/jquery/jquery.min.js') }}"></script>
    <script src="{{ asset('/plugins/jquery-ui/jquery-ui.min.js') }}"></script>
    <script src="{{ asset('/plugins/bootstrap/js/bootstrap.min.js') }}"></script>
    <script src="{{ asset('/plugins/bootstrap-notify/bootstrap-notify.min.js') }}"></script>
    <script src="{{ asset('/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js') }}"></script>
    <script src="{{ asset('/plugins/bootstrap-datepicker/locales/bootstrap-datepicker.ja.min.js') }}"></script>
    <script src="{{ asset('/plugins/moment/moment.min.js') }}"></script>
    <script src="{{ asset('/adminsystem/js/bootstrap-multiselect.js') }}"></script>
    <script src="{{ asset('/adminsystem/js/prettify.min.js') }}"></script>
    <script src="{{ asset('/adminsystem/js/adminlte.min.js') }}"></script>
    <script src="{{ asset('/adminsystem/js/dragscroll.js') }}"></script>
    <script src="{{ asset('/adminsystem/js/js.cookie.js') }}"></script>
    <script src="{{ asset('/plugins/handlebars/handlebars.min-latest.js') }}"></script>
    <script src="{{ asset('/adminsystem/js/datalist.js') }}"></script>
    <script src="{{ asset('/adminsystem/js/datalist2.js') }}"></script>
    <script src="{{ asset('/adminsystem/js/dataform.js') }}"></script>
    <script src="{{ asset('/adminsystem/js/lodash.js') }}"></script>
    <script src="{{ asset('/adminsystem/js/ekko-lightbox.min.js') }}"></script>
    <script src="{{ asset('/adminsystem/js/jquery.combo.select.js') }}"></script>
    @yield('javascript')
    @if(strpos($_SERVER['HTTP_USER_AGENT'], 'MSIE') === false)
        <script src="https://cdn.polyfill.io/v2/polyfill.min.js"></script>
    @endif
    <script src="{{asset('js/index.js')}}" ></script>
    <script type="text/javascript">
        window.addEventListener("pageshow", function (event ) {
            var historyTraversal = event.persisted ||
                                 (typeof window.performance != "undefined" &&
                                      window.performance.navigation.type === 2);
            if (historyTraversal) {
                window.location.reload();
            }
        });
        var sidebarOpen = $.cookie('sidebar_toggle');
        $(document).on('click','a.sidebar-toggle', function(){
           var sidebarOpen = $.cookie('sidebar_toggle');
           if (sidebarOpen === 'null' || sidebarOpen === 'false') {
                $.cookie('sidebar_toggle', true, {path: '/'});
           } else {
                $.cookie('sidebar_toggle', false, {path: '/'});
            }
        });
        if (sidebarOpen === 'true' || sidebarOpen === 'null' || sidebarOpen === true) {
            $('body').addClass('sidebar-collapse');
        }
        $.ajaxSetup({
            dataFilter: function (data, type) {
                var dataCheck = {};
                if (type === 'json') {
                    var dataCheck = JSON.parse(data);
                }
                if (dataCheck.status === 1 && dataCheck.not_permission === 1) {
                    $('.loading').hide();
                    $.notify({
                        icon: 'glyphicon glyphicon-warning-sign',
                        message: trans('messages.not_permission'),
                    },{
                        type: 'error'
                    });
                    return '{}';
                } else {
                    return data;
                }
            }
        });
    </script>
    @if (session('not_permission'))
        <script>
           $.notify({
                icon: 'glyphicon glyphicon-warning-sign',
                message: "{{__('messages.not_permission') . ' ' . session('message')}}",
            },{
                type: 'error'
            });
        </script>
    @endif
</body>

</html>