@extends('layouts.main')
@section('titlePage', __('messages.shipping_free_below_limit_header'))
@section('content')
<section class="content-header">
    <h1>
        {{__('messages.shipping_free_below_limit_header')}}
        <small>{{__('messages.shipping_free_below_limit_sub_header')}}</small>
    </h1>
    <ol class="breadcrumb">
        <li>
            <a href="{{action('ShippingFreeBelowLimitController@index')}}">{{__('messages.shipping_free_below_limit_breadcrumb_title')}}</a>
        </li>
    </ol>
</section>

<section class="content">
    <!-- Put your code here -->
    <div id="root"></div>
</section>
@endsection
@section('javascript')
<script>
    var baseUrl         = "{{action('ShippingFreeBelowLimitController@index')}}";
    var saveUrl         = "{{action('ShippingFreeBelowLimitController@save')}}";
    var dataListUrl     = "{{action('ShippingFreeBelowLimitController@dataList')}}";
    var changeDeleteUrl = "{{action('ShippingFreeBelowLimitController@changeDelete')}}";
    $('#system-setting-menu').addClass('active');
    $('#shipping-free-below-limit-menu').addClass('active');
</script>
@endsection