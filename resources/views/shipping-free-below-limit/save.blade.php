@extends('layouts.main')
@section('titlePage', __('messages.shipping_free_below_limit_save_header'))
@section('content')
<section class="content-header">
    <h1>
        {{__('messages.shipping_free_below_limit_save_header')}}
        <small>{{__('messages.shipping_free_below_limit_save_sub_header')}}</small>
    </h1>
    <ol class="breadcrumb">
        <li>
            <a href="{{action('ShippingFreeBelowLimitController@index')}}">
                {{__('messages.shipping_free_below_limit_breadcrumb_title')}}
            </a>
        </li>
        <li>
            <a href="{{Request::fullUrl()}}">{{__('messages.shipping_free_below_limit_save_header')}}</a>
        </li>
    </ol>
</section>

<section class="content">
    <!-- Put your code here -->
    <div id="root"></div>
</section>
@endsection
@section('javascript')
<script>
    var baseUrl        = "{{action('ShippingFreeBelowLimitController@index')}}";
    var saveUrl        = "{{action('ShippingFreeBelowLimitController@save')}}";
    var getFormDataUrl = "{{action('ShippingFreeBelowLimitController@getFormData')}}";
    var checkEditUrl   = "{{action('DashboardController@checkItemEdit')}}";
    $('#system-setting-menu').addClass('active');
    $('#shipping-free-below-limit-menu').addClass('active');
</script>
@endsection