@extends('layouts.main')
@section('titlePage', __('messages.settlement_save_header'))
@section('content')
<section class="content-header">
    <h1>
        {{__('messages.settlement_save_header')}}
        <small></small>
    </h1>
    <ol class="breadcrumb">
        <li>
            <a href="{{action('SettlementController@index')}}">{{__('messages.settlement_menu_title')}}</a>
        </li>
        <li>
            <a href="{{action('SettlementController@save')}}">{{__('messages.settlement_save_header')}}</a>
        </li>
    </ol>
</section>

<section class="content">
    <!-- Put your code here -->
    <div id="root"></div>
</section>
@endsection
@section('javascript')
<script type="text/javascript">
var baseUrl            = "{{action('SettlementController@index')}}";
var formDataUrl        = "{{action('SettlementController@getFormData')}}";
var saveUrl            = "{{action('SettlementController@save')}}";
var checkEditUrl       = "{{action('DashboardController@checkItemEdit')}}";
$('#system-setting-menu').addClass('active');
$('#settlement-list-menu').addClass('active');
</script>
@endsection