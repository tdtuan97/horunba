@extends('layouts.main')
@section('titlePage', __('messages.settlement_header'))
@section('content')
<section class="content-header">
    <h1>
        {{__('messages.settlement_header')}}
        <small></small>
    </h1>
    <ol class="breadcrumb">
        <li>
            <a href="{{action('SettlementController@index')}}">{{__('messages.settlement_menu_title')}}</a>
        </li>
    </ol>
</section>
<section class="content">
    <!-- Put your code here -->
    <div id="root"></div>
</section>
@endsection
@section('javascript')
<script type="text/javascript">
var baseUrl         = "{{action('SettlementController@index')}}";
var saveUrl         = "{{action('SettlementController@save')}}";
var dataListUrl     = "{{action('SettlementController@dataList')}}";
var changeStatusUrl = "{{action('SettlementController@changeStatus')}}";
$('#system-setting-menu').addClass('active');
$('#settlement-list-menu').addClass('active');
</script>
@endsection