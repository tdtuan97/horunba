@extends('layouts.main')
@section('titlePage', __('messages.edit_send_mail_save_header'))
@section('content')
<section class="content-header">
    <h1>
        {{__('messages.edit_send_mail_save_header')}}
        <small></small>
    </h1>
    <ol class="breadcrumb">
        <li>
            <a href="{{action('OrderManagementController@index')}}">
                {{__('messages.order_detail_editing_breadcrumb_title')}}
            </a>
        </li>
        <li class="active">
            <a href="{{action('EditSendMailController@save')}}">
                {{__('messages.edit_send_mail_save_header')}}
            </a>
        </li>
    </ol>
</section>

<section class="content">
    <!-- Put your code here -->
    <div id="root"></div>
</section>
@endsection
@section('javascript')
<script>
    var saveUrl            = "{{action('EditSendMailController@save')}}";
    var getFormDataUrl     = "{{action('EditSendMailController@getFormData')}}";
    var receiveMailSaveUrl = "{{action('ReceiveMailController@save')}}";
    var previousUrl        = "{!! url()->previous() !!}";
     $('#receive-mail-menu').addClass('active');
</script>
@endsection