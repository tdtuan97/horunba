@extends('layouts.main')
@section('titlePage', __('messages.repayment_list_header'))
@section('content')
<section class="content-header">
    <h1>
        {{__('messages.repayment_list_header')}}
        <small>{{__('messages.repayment_sub_header')}}</small>
    </h1>
    <ol class="breadcrumb">
        <li>
            <a href="{{action('RePaymentController@index')}}">
                {{__('messages.repayment_list_header')}}
            </a>
        </li>
    </ol>
</section>

<section class="content">
    <!-- Put your code here -->
    <div id="root"></div>
</section>
@endsection
@section('javascript')
<script type="text/javascript">
    var changeFlagUrl       = "{{action('RePaymentController@changeFlag')}}";
    var baseUrl             = "{{action('RePaymentController@index')}}";
    var saveUrl             = "{{action('RePaymentController@save')}}";
    var dataListUrl         = "{{action('RePaymentController@dataList')}}";
    var exportCSV           = "{{action('Common@exportCSV')}}";
    var processExportCSV    = "{{action('RePaymentController@processExportCsv')}}";    
    $('#payment-menu').addClass('active');
    $('#repayment-list-menu').addClass('active');
</script>
@endsection