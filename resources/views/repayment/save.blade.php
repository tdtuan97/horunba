@extends('layouts.main')
@section('titlePage', __('messages.repayment_save_header'))
@section('content')
<section class="content-header">
    <h1>
        {{__('messages.repayment_save_header')}}
        <small>{{__('messages.repayment_sub_header')}}</small>
    </h1>
    <ol class="breadcrumb">
        <li>
            <a href="{{action('RePaymentController@index')}}">
                {{__('messages.repayment_list_header')}}
            </a>
        </li>
        <li class="active">
            <a href="{{Request::fullUrl()}}">
                {{__('messages.repayment_save_header')}}
            </a>
        </li>
    </ol>
</section>

<section class="content">
    <!-- Put your code here -->
    <div id="root"></div>
</section>
@endsection
@section('javascript')
<script>

    var formDataUrl     = "{{action('RePaymentController@getFormData')}}";
    var getInfoOrderUrl = "{{action('RePaymentController@getInfoOrderData')}}";
    var baseUrl         = "{{action('RePaymentController@index')}}";
    var checkEditUrl       = "{{action('DashboardController@checkItemEdit')}}";
    var getRepaymentPriceUrl = "{{action('ReturnManagementController@getRepaymentPrice')}}";
    $('#payment-menu').addClass('active');
    $('#repayment-list-menu').addClass('active');
</script>
@endsection