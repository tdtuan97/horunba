@extends('layouts.main')
@section('titlePage', __('messages.repayment_save_header'))
@section('content')
    <section class="content-header">
        <h1>
            CUSTOMER
            <small>{{__('messages.repayment_sub_header')}}</small>
        </h1>
        <ol class="breadcrumb">
            <li>
                <a href="{{action('RePaymentController@index')}}">
                    {{__('messages.repayment_list_header')}}
                </a>
            </li>
            <li class="active">
                <a href="{{Request::fullUrl()}}">
                    {{__('messages.repayment_save_header')}}
                </a>
            </li>
        </ol>
    </section>

    <section class="content">
        <!-- Put your code here -->
        <div id="root"></div>
    </section>
@endsection
@section('javascript')
    <script>
        var formDataUrl     = "{{action('CustomerController@getFormData')}}";
        var baseUrl         = "{{action('CustomerController@index')}}";
        var checkEditUrl       = "{{action('DashboardController@checkItemEdit')}}";
    </script>
@endsection