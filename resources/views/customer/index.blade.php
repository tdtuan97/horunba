@extends('layouts.main')
@section('titlePage',"MST CUSTOMER")
@section('content')
    <section class="content-header">
        <h1>
            MST CUSTOMER
        </h1>
        <ol class="breadcrumb">
            <li>
                <a href="{{route('customer.list')}}">
                    Customer
                </a>
            </li>
        </ol>
    </section>
    <div class="popup-error fix-message collapse" id="popup-status">
        <div class="alert alert-dismissible alert-success">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <h4><i class="icon fa fa-check"></i> Alert!</h4>
            <p class="text-new-line" id="popup-message"></p>
        </div>
    </div>
    <section class="content">
        <!-- Put your code here -->
        <div id="root"></div>
    </section>
@endsection
@section('javascript')
    <script>
        $('#customer-menu').addClass('active');
        var baseUrl          = "{{action('CustomerController@index')}}";
        var csvUrl           = "{{action('Common@importCSV')}}";
        var dataListUrl      = "{{action('CustomerController@dataList')}}";
        var saveUrl       = "{{action('CustomerController@save')}}";
        var deleteUrl       = "{{action('CustomerController@delete')}}";
        var exportCSV           = "{{action('Common@exportCSV')}}";
        var processExportCSV    = "{{action('CustomerController@processExportCsv')}}";
    </script>
@endsection
