@extends('layouts.main')
@section('titlePage', __('messages.genre_header'))
@section('content')
<section class="content-header">
    <h1>
        {{__('messages.genre_header')}}
        <small>{{__('messages.genre_sub_header')}}</small>
    </h1>
    <ol class="breadcrumb">
        <li>
            <a href="{{action('GenreController@index')}}">{{__('messages.genre_breadcrumb_title')}}</a>
        </li>
    </ol>
</section>

<section class="content">
    <!-- Put your code here -->
    <div id="root"></div>
</section>
@endsection
@section('javascript')
<script>
    var baseUrl     = "{{action('GenreController@index')}}";
    var saveUrl     = "{{action('GenreController@save')}}";
    var dataListUrl = "{{action('GenreController@dataList')}}";
    var deleteUrl   = "{{action('GenreController@delete')}}";
    $('#system-setting-menu').addClass('active');
    $('#genre-menu').addClass('active');
</script>
@endsection