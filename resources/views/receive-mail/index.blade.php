@extends('layouts.main')
@section('titlePage', __('messages.receive_mail_header'))
@section('content')
<section class="content-header">
    <h1>
        {{__('messages.receive_mail_header')}}
    </h1>
    <ol class="breadcrumb">
        <li>
            <a href="{{action('ReceiveMailController@index')}}">
                {{__('messages.receive_mail_breadcrumb_title')}}
            </a>
        </li>
    </ol>
</section>

<section class="content">
    <!-- Put your code here -->
    <div id="root"></div>
</section>
@endsection
@section('javascript')
<script>
    var baseUrl     = "{{action('ReceiveMailController@index')}}";
    var saveUrl     = "{{action('ReceiveMailController@save')}}";
    var dataListUrl = "{{action('ReceiveMailController@dataList')}}";
    var EditSendUrl = "{{action('EditSendMailController@save')}}";
    var updateUrl   = "{{action('ReceiveMailController@updateData')}}";
    $('#receive-mail-menu').addClass('active');
</script>
@endsection