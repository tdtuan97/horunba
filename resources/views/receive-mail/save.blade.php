@extends('layouts.main')
@section('titlePage', __('messages.receive_mail_save_header'))
@section('content')
<section class="content-header">

    <h1 style="float:left">
        {{__('messages.receive_mail_save_header')}}
        <small></small>
    </h1>
    <div class="next-prev-pos">
        @include('includes.next_prev_bar')
    </div>
    <div class="clearfix"></div>
    <ol class="breadcrumb">
        <li>
            <a href="{{action('ReceiveMailController@index')}}">
                {{__('messages.receive_mail_breadcrumb_title')}}
            </a>
        </li>
        <li>
            <a href="{{Request::fullUrl()}}">
                {{__('messages.receive_mail_save_header')}}
            </a>
        </li>
    </ol>
</section>

<section class="content">
    <!-- Put your code here -->
    <div id="root"></div>
</section>
@endsection
@section('javascript')
<script>
    var baseUrl        = "{{action('ReceiveMailController@index')}}";
    var saveUrl        = "{{action('ReceiveMailController@save')}}";
    var getFormDataUrl = "{{action('ReceiveMailController@getFormData')}}";
    var orderDetailUrl = "{{action('OrderManagementController@detail')}}";
    var getOrderUrl    = "{{action('ReceiveMailController@getOrder')}}";
    var checkEditUrl   = "{{action('DashboardController@checkItemEdit')}}";
    var sendMailUrl    = "{{action('EditSendMailController@save')}}";
    var iframeUrl      = "{{action('ReceiveMailController@iframe')}}";
    var previousUrl    = "{!! $backUrl !!}";
    $('#receive-mail-menu').addClass('active');
    $( document ).ready(function() {
        var $groupMail = $('#group-mail-reply');
        $groupMail.on('show.bs.collapse','.collapse', function() {
            $groupMail.find('.collapse.in').collapse('hide');
        });
    });
</script>
@endsection