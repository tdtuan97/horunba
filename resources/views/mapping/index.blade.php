@extends('layouts.main')
@section('titlePage', __('messages.web_mapping_management_header'))
@section('content')
<section class="content-header">
    <h1>
        {{__('messages.web_mapping_management_header')}}
    </h1>
    <ol class="breadcrumb">
        <li>
            <a href="{{action('WebMappingController@index')}}">
                {{__('messages.web_mapping_breadcrumb_title')}}
            </a>
        </li>
    </ol>
</section>
<div class="popup-error fix-message collapse" id="popup-status">
    <div class="alert alert-dismissible alert-success">
        <h4><i class="icon fa fa-check"></i> Alert!</h4>
        <p class="text-new-line" id="popup-message"></p>
    </div>
</div>
<div class="popup-error fix-message collapse" id="popup-error">
    <div class="alert alert-dismissible alert-error">
        <h4><i class="icon fa fa-ban"></i> Alert!</h4>
        <p class="text-new-line"></p>
    </div>
</div>
<div class="popup-error fix-message collapse" id="popup-info">
    <div class="alert alert-dismissible alert-info">
        <h4><i class="icon fa fa-info"></i> Info!</h4>
        <p class="text-new-line"></p>
    </div>
</div>
<section class="content">
    <!-- Put your code here -->
    <div id="root"></div>
</section>
@endsection
@section('javascript')
<script>
    var baseUrl               = "{{action('WebMappingController@index')}}";
    var dataListUrl           = "{{action('WebMappingController@dataList')}}";
    var updateStatusUrl       = "{{action('WebMappingController@updateStatus')}}";
    var processApiUrl         = "{{action('WebMappingController@processApi')}}";
    var importUrl             = "{{action('WebMappingController@import')}}";
    var processUpdateAll      = "{{action('WebMappingController@processUpdateAll')}}";
    var csvUrl                = "{{action('Common@importCSV')}}";
    var processAfterImportUrl = "{{action('WebMappingController@processAfterImport')}}";
    var exportCSV             = "{{action('Common@exportCSV')}}";
    var processExportCSV      = "{{action('WebMappingController@processExportCsv')}}";
    $('#mapping-menu').addClass('active');
    $('#web-mapping-menu').addClass('active');
</script>
@endsection
