@extends('layouts.main')
@section('titlePage', __('messages.membership_header'))
@section('content')
<section class="content-header">
    <h1>
        {{__('messages.membership_header')}}
    </h1>
    <ol class="breadcrumb">
        <li>
            <a href="{{action('MembershipController@index')}}">
                {{__('messages.membership_header')}}
            </a>
        </li>
    </ol>
</section>
<section class="content">
    <div id="root"><!-- content reactjs put here --></div>
    <div id="main-content" class="box" style="display: none">
        <div class="loading"></div>
        <div class="box-header">
            <div class="row">
                <div class="col-md-12">
                    <h4>{{__('messages.search_title')}}</h4>
                </div>
                <form id="search-form">
                    <div class="form-group col-md-1-5">
                        <label class="control-label">{{__('messages.member_id')}}</label>
                        <input name="member_id" class="form-control input-sm" value="{{ Request::input('member_id', '') }}">
                    </div>
                    <div class="form-group col-md-1-5">
                        <label class="control-label">{{__('messages.full_name')}}</label>
                        <input name="full_name" class="form-control input-sm" value="{{ Request::input('full_name', '') }}">
                    </div>
                    <div class="form-group col-md-1-75">
                        <label class="control-label">{{__('messages.birthday')}}</label>
                        <input name="birthday" class="datepicker form-control input-sm" placeholder="YYYY-MM-DD" value="{{ Request::input('birthday', '') }}">
                    </div>
                    <div class="form-group col-md-1-5">
                        <label class="control-label">{{__('messages.tel_no')}}</label>
                        <input name="tel_no" class="form-control input-sm" value="{{ Request::input('tel_no', '') }}">
                    </div>
                    <div class="form-group col-md-1-5">
                        <label class="control-label">{{__('messages.address')}}</label>
                        <input name="address" class="form-control input-sm" value="{{ Request::input('address', '') }}">
                    </div>
                    <div class="form-group col-md-2">
                        <label class="control-label">{{__('messages.mail_address')}}</label>
                        <input name="mail_address" class="form-control input-sm" value="{{ Request::input('mail_address', '') }}">
                    </div>
                    <div class="form-group col-md-1 per-page pull-right-md">
                        <label class="control-label">{{__('messages.per_page')}}</label>
                        <select name="per_page" class="form-control input-sm" onchange="search()">
                            @foreach([20, 40, 60, 80, 100] as $perPage)
                                @if ((int)Request::input('per_page', null) === $perPage)
                                <option selected value="{{$perPage}}">{{$perPage}}</option>
                                @else
                                <option value="{{$perPage}}">{{$perPage}}</option>
                                @endif
                            @endforeach
                        </select>
                    </div>
                </form>
            </div>

            <div class="row">
                <div class="col-md-9">
                    <a href="{{action('MembershipController@save')}}" class="btn btn-success pull-left margin-element margin-form">{{ __('messages.label_add') }}</a>
                    <a href="{{action('MembershipController@karutePdf')}}" class="btn btn-success pull-left margin-element margin-form" target="_blank">{{ __('messages.btn_pdf_karute') }}</a>
                    <button class="btn btn-success pull-left margin-element margin-form" onclick="exportCsv()">{{__('messages.csv_export')}}</button>
                </div>
                <div class="col-md-3 col-xs-8 pull-right">
                    <button class="btn btn-danger pull-right" onclick="clearSearch()">{{__('messages.btn_reset')}}</button>
                    <button class="btn btn-primary pull-right margin-element" onclick="search()">{{__('messages.btn_search')}}</button>
                </div>
            </div>
        </div>
        <div class="box-body" id="main-contain">

        </div>
    </div>
</section>
@endsection
@section('javascript')
<script id="list-template" type="text/x-handlebars-template">
    <div class="row">
        <div class="col-md-12">
            @{{#if (gt data.last_page 1)}}
            <div class="row">
                <div class="col-md-2-5"></div>
                <div class="col-md-7 text-center">
                    <div class="text-center paginator-content"></div>
                </div>
                <div class="col-md-2-5 text-right line-height-md">
                    @{{data.from}}件 - @{{data.to}}件 【全@{{data.total}}件】
                </div>
            </div>
            @{{/if}}
            <div class="table-responsive">
                <table class="table table-striped table-custom">
                    <thead>
                        <tr>
                            <th class="visible-xs visible-sm"></th>
                            <th class="col-max-min-40 text-center text-middle">
                                {{__('messages.member_id')}}
                            </th>
                            <th class="col-max-min-100 text-center text-middle">
                                {{__('messages.full_name')}}
                            </th>
                            <th class="col-max-min-90 text-center text-middle hidden-xs hidden-sm">
                                {{__('messages.birthday')}}
                            </th>
                            <th class="col-max-min-50 text-center text-middle hidden-xs hidden-sm">
                                {{__('messages.sex')}}
                            </th>
                            <th class="col-max-min-90 text-center text-middle hidden-xs hidden-sm">
                                {{__('messages.tel_no')}}
                            </th>
                            <th class="col-max-min-100 text-center text-middle hidden-xs hidden-sm">
                                {{__('messages.address')}}
                            </th>
                            <th class="col-max-min-90 text-center text-middle hidden-xs hidden-sm">
                                {{__('messages.mail_address')}}
                            </th>
                            <th class="col-max-min-60 text-center text-middle hidden-xs hidden-sm">
                                {{__('messages.remain_point')}}
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        @{{#if data.data}}
                            @{{#each data.data}}
                                <tr class="@{{this.class_row}}">
                                    <td class="visible-xs visible-sm">
                                        <b class="show-info">
                                            <i class="fa fa-angle-double-down" aria-hidden="true"></i>
                                        </b>
                                    </td>
                                    <td class="text-center" title="@{{this.member_id}}">
                                        <a href="@{{this.detail}}" class="link-detail">@{{this.member_id}}</a>
                                    </td>
                                    <td title="@{{this.full_name}}">
                                        @{{this.full_name}}
                                    </td>
                                    <td class="text-long-batch hidden-xs hidden-sm text-right" title="@{{this.birthday}}">
                                        @{{this.birthday}}
                                    </td>
                                    <td class="hidden-xs hidden-sm" title="@{{this.sex}}">
                                        @{{this.sex}}
                                    </td>
                                    <td class="hidden-xs hidden-sm text-right" title="@{{this.tel_no}}">
                                        @{{this.tel_no}}
                                    </td>
                                    <td class="hidden-xs hidden-sm" title="@{{this.address}}">
                                        @{{this.address}}
                                    </td>
                                    <td class="hidden-xs hidden-sm" title="@{{this.mail_address}}">
                                        @{{this.mail_address}}
                                    </td>
                                    <td class="hidden-xs hidden-sm text-right" title="@{{this.remain_point}}">
                                        @{{this.remain_point}}
                                    </td>
                                </tr>
                                <tr class="hiden-info" style="display: none">
                                    <td colSpan="10" class="width-span1">
                                        <ul>
                                            <li> <b>{{__('messages.member_id')}}</b> : <a href="@{{this.detail}}" class="link-detail">@{{this.member_id}}</a></li>
                                            <li> <b>{{__('messages.full_name')}}</b> : @{{this.full_name}}</li>
                                            <li> <b>{{__('messages.birthday')}}</b> : @{{this.birthday}}</li>
                                            <li> <b>{{__('messages.sex')}}</b> : @{{this.sex}}</li>
                                            <li> <b>{{__('messages.tel_no')}}</b> : @{{this.tel_no}}</li>
                                            <li> <b>{{__('messages.address')}}</b> : @{{this.address}}</li>
                                            <li> <b>{{__('messages.mail_address')}}</b> : @{{this.mail_address}}</li>
                                            <li> <b>{{__('messages.remain_point')}}</b> : @{{this.remain_point}}</li>
                                        </ul>
                                    </td>
                                </tr>
                            @{{/each}}
                        @{{else}}
                            <tr><td colSpan="17" class="text-center">{{__('messages.no_data')}}</td></tr>
                        @{{/if}}
                    </tbody>
                </table>
            </div>
            @{{#if (gt data.last_page 1)}}
            <div class="text-center paginator-content"></div>
            @{{/if}}
        </div>
    </div>
</script>
@include('includes.paginate');
<script>
    var ignoreKey = [];
    var datalist = new DataList("{{action('MembershipController@dataList')}}", ignoreKey);
    function processData(_this, response) {
        var nf = new Intl.NumberFormat();
        response.data.data.map((item, index) => {
            item['detail']    = response.link_detail + '?member_id=' + item['member_id'];
            item['index']     = ((response.data.current_page - 1) * response.data.per_page) + index + 1;
            item['class_row'] = (index % 2 === 0) ? 'odd' : 'even';
            item['birthday']  = (item['birthday']) ? moment.utc(item['birthday'], "YYYY-MM-DD").format("YYYY/MM/DD") : '';
        });
    }
    function search() {
        datalist.search(processData);
    }
    function handleClickPage(page = 1) {
        datalist.handleClickPage(page, processData);
    }
    function clearSearch() {
        datalist.clearSearch(processData);
    }
    $(document).ready(function() {
        $("select[multiple='multiple']").multiselect({
            buttonWidth: "100%",
            nonSelectedText: "",
            includeSelectAllOption: true,
            selectAllText:"{{__('messages.select_all')}}"
        });
        $(".datepicker").datepicker({
            language: "ja",
            format: "yyyy/mm/dd",
            todayHighlight: true,
        });
        $("#main-content").show();
        var arrHref = window.location.href.split('?');
        var dataPost = '';
        if (arrHref.length > 1) {
            dataPost = arrHref[1];
        }
        datalist.reloadData(dataPost, processData);
    });
    function exportCsv() {
        let dataList = $("#search-form").serialize();
        dataList += '&type=membership';
        $('.loading').show();
        $.ajax({
            url: "{{action('Common@processExportCSV')}}",
            data: dataList,
            dataType: "json",
            success: function(response) {
                $(".loading").hide();
                if (response.status === 1) {
                    window.location.href = "{{action('Common@exportCSV')}}" +"?fileName="+ response.file_name;
                }
            }
        });


    }
    $('#studiy-member-menu').addClass('active');
    $('#membership-menu').addClass('active');
</script>
@endsection