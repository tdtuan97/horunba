@extends('layouts.main')
@section('titlePage', __('messages.membership_header'))
@section('content')
<section class="content-header">
    <h1>
        {{__('messages.membership_save_header')}}
    </h1>
    <ol class="breadcrumb">
        <li>
            <a href="{{action('MembershipController@index')}}">
                {{__('messages.membership_header')}}
            </a>
            <li class="active">
                <a href="{{Request::fullUrl()}}">{{__('messages.membership_save_header')}}</a>
            </li>
        </li>
    </ol>
</section>
<section class="content">
    <div id="root"><!-- content reactjs put here --></div>
    <div class="container-fluid">

        <form class="form-horizontal" id="form-submit" style="display: none;">
            <div class="loading" style="display: none;"></div>
            <div class="box box-success">
                {{ csrf_field() }}
                <div class="box-body">
                    <div class="row" id="main-contain">
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <button type="button" class="btn btn-danger pull-right" onclick="backFunc()">{{__('messages.btn_back')}}</button>
                            <button type="button" class="btn btn-primary pull-right margin-element" onclick="submitFunc()">{{__('messages.label_add')}}</button>
                        </div>
                    </div>
                </div>
            </div>
        </form>
        <div id="popup-deny"></div>
    </div>
</section>
@endsection
@section('javascript')
<script id="form-template" type="text/x-handlebars-template">
    @{{{inputHandle
        name="action"
        type="hidden"
        value=(getValueHandle data.action.value)
    }}}
    <div class="col-md-6">
        @{{{inputHandle
            name="member_id"
            labelTitle=(trans "messages.member_id")
            class="form-control input-sm"
            fieldGroupClass="col-md-4"
            labelClass="col-md-3"
            type="text"
            errorPopup=true
            readOnly=(getValueHandle data.member_id.readonly)
            value=(getValueHandle data.member_id.value)
            hasError=(checkBooleanHandle data.member_id.is_error)
            errorMessage=(getValueHandle data.member_id.message)
        }}}
        <div class="row">
            <div class="col-sm-6">
                @{{{inputHandle
                    name="last_name"
                    labelTitle=(trans "messages.membership_last_name")
                    class="form-control input-sm"
                    fieldGroupClass="col-sm-6"
                    labelClass="col-md-6"
                    {{-- groupClass="row" --}}
                    type="text"
                    errorPopup=true
                    value=(getValueHandle data.last_name.value)
                    hasError=(checkBooleanHandle data.last_name.is_error)
                    errorMessage=(getValueHandle data.last_name.message)
                }}}
            </div>
            <div class="col-sm-6">
                @{{{inputHandle
                    name="first_name"
                    labelTitle=(trans "messages.first_name")
                    class="form-control input-sm"
                    fieldGroupClass="col-sm-6"
                    labelClass="col-md-6"
                   {{--  groupClass="row" --}}
                    type="text"
                    errorPopup=true
                    value=(getValueHandle data.first_name.value)
                    hasError=(checkBooleanHandle data.first_name.is_error)
                    errorMessage=(getValueHandle data.first_name.message)
                }}}
            </div>
        </div>
        <div class="form-group">
            <div class="col-sm-6">
                @{{{inputHandle
                    name="last_name_kana"
                    labelTitle=(trans "messages.last_name_kana")
                    class="form-control input-sm"
                    fieldGroupClass="col-sm-6"
                    labelClass="col-md-6"
                    groupClass="row"
                    type="text"
                    errorPopup=true
                    value=(getValueHandle data.last_name_kana.value)
                    hasError=(checkBooleanHandle data.last_name_kana.is_error)
                    errorMessage=(getValueHandle data.last_name_kana.message)
                }}}
            </div>
            <div class="col-sm-6">
                @{{{inputHandle
                    name="first_name_kana"
                    labelTitle=(trans "messages.first_name_kana")
                    class="form-control input-sm"
                    fieldGroupClass="col-sm-6"
                    labelClass="col-md-6"
                    groupClass="row"
                    type="text"
                    errorPopup=true
                    value=(getValueHandle data.first_name_kana.value)
                    hasError=(checkBooleanHandle data.first_name_kana.is_error)
                    errorMessage=(getValueHandle data.first_name_kana.message)
                }}}
            </div>
        </div>
            @{{{inputHandle
                name="tel_no"
                labelTitle=(trans "messages.tel_no")
                class="form-control input-sm"
                fieldGroupClass="col-sm-6"
                labelClass="col-md-3"
                type="text"
                errorPopup=true
                value=(getValueHandle data.tel_no.value)
                hasError=(checkBooleanHandle data.tel_no.is_error)
                errorMessage=(getValueHandle data.tel_no.message)
            }}}
        <div class="row">
            <div class="col-sm-6">
                @{{{selectHandle
                    name="year"
                    labelTitle=(trans "messages.birthday")
                    class="form-control input-sm"
                    fieldGroupClass="col-sm-4 padding-right-5"
                    labelClass="col-md-6"
                    {{-- groupClass="row" --}}
                    type="text"
                    errorPopup=true
                    isTitleRight=true
                    labelClassRight="col-md-2 clear-padding margin-top-5"
                    labelTitleRight=(trans "messages.year")
                    options=optYear
                    value=(getValueHandle data.year.value)
                    hasError=(checkBooleanHandle data.year.is_error)
                    errorMessage=(getValueHandle data.year.message)
                }}}
            </div>
            <div class="col-sm-3">
                @{{{selectHandle
                    name="month"
                    class="form-control input-sm"
                    fieldGroupClass="col-sm-8 padding-right-5"
                    {{-- groupClass="row" --}}
                    type="text"
                    errorPopup=true
                    isTitleLeft=false
                    isTitleRight=true
                    labelClassRight="col-md-4 clear-padding margin-top-5"
                    labelTitleRight=(trans "messages.month")
                    options=optMonth
                    value=(getValueHandle data.month.value)
                    hasError=(checkBooleanHandle data.month.is_error)
                    errorMessage=(getValueHandle data.month.message)
                }}}
            </div>
            <div class="col-sm-3">
                @{{{selectHandle
                    name="day"
                    class="form-control input-sm"
                    fieldGroupClass="col-sm-8 padding-right-5"
                    {{-- groupClass="row" --}}
                    type="text"
                    errorPopup=true
                    isTitleLeft=false
                    isTitleRight=true
                    labelClassRight="col-md-4 clear-padding margin-top-5"
                    labelTitleRight=(trans "messages.day")
                    options=optDay
                    value=(getValueHandle data.day.value)
                    hasError=(checkBooleanHandle data.day.is_error)
                    errorMessage=(getValueHandle data.day.message)
                }}}
            </div>
        </div>
        <div class="form-group">
            <fieldset id="sex">
                <div class="col-sm-6">
                    <div class="row">
                        <div class="col-sm-6 padding-right-5 col-md-offset-6 @{{#if data.sex.is_error}} text-red @{{/if}}">
                            @{{{radioHandle
                                name="sex"
                                value="0"
                                type="radio"
                                classLabel="custom-radio"
                                currentValue=(getValueHandle data.sex.value)
                            }}}{{__('messages.women')}}
                        </div>
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="row">
                        <div class="col-sm-8 padding-right-5 @{{#if data.sex.is_error}} text-red @{{/if}}">
                            @{{{radioHandle
                                name="sex"
                                value="1"
                                type="radio"
                                classLabel="custom-radio"
                                currentValue=(getValueHandle data.sex.value)
                            }}}{{__('messages.men')}}
                        </div>
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="row">
                        <div class="col-sm-8 padding-right-5 @{{#if data.sex.is_error}} text-red @{{/if}}">
                            @{{{radioHandle
                                name="sex"
                                value="2"
                                type="radio"
                                classLabel="custom-radio"
                                currentValue=(getValueHandle data.sex.value)
                            }}}{{__('messages.unknown')}}
                        </div>
                    </div>
                </div>
            </fieldset>
        </div>
        @{{{inputHandle
            name="address"
            labelTitle=(trans "messages.address")
            class="form-control input-sm"
            fieldGroupClass="col-sm-9"
            labelClass="col-md-3"
            type="text"
            errorPopup=true
            value=(getValueHandle data.address.value)
            hasError=(checkBooleanHandle data.address.is_error)
            errorMessage=(getValueHandle data.address.message)
        }}}
        @{{{inputHandle
            name="enrolment_day"
            labelTitle=(trans "messages.enrolment_day")
            class="form-control input-sm"
            fieldGroupClass="col-sm-4"
            labelClass="col-md-3"
            type="text"
            id="enrolment_day"
            errorPopup=true
            placeholder="YYYY-MM-DD"
            value=(getValueHandle data.enrolment_day.value)
            hasError=(checkBooleanHandle data.enrolment_day.is_error)
            errorMessage=(getValueHandle data.enrolment_day.message)
        }}}
        @{{{inputHandle
            name="mail_address"
            labelTitle=(trans "messages.mail_address")
            class="form-control input-sm"
            fieldGroupClass="col-sm-9"
            labelClass="col-md-3 clear-padding-right"
            type="text"
            errorPopup=true
            value=(getValueHandle data.mail_address.value)
            hasError=(checkBooleanHandle data.mail_address.is_error)
            errorMessage=(getValueHandle data.mail_address.message)
        }}}
        @{{{inputHandle
            name="nick_name"
            labelTitle=(trans "messages.nick_name")
            class="form-control input-sm"
            fieldGroupClass="col-sm-9"
            labelClass="col-md-3"
            type="text"
            errorPopup=true
            value=(getValueHandle data.nick_name.value)
            hasError=(checkBooleanHandle data.nick_name.is_error)
            errorMessage=(getValueHandle data.nick_name.message)
        }}}
        <div class="form-group">
            <fieldset id="is_chintai">
                <div class="col-sm-6">
                    <div class="row">
                        <div class="col-sm-6 padding-right-5 col-md-offset-6 @{{#if data.is_chintai.is_error}} text-red @{{/if}}">
                            @{{{radioHandle
                                name="is_chintai"
                                value="0"
                                type="radio"
                                classLabel="custom-radio"
                                currentValue=(getValueHandle data.is_chintai.value)
                            }}}{{__('messages.is_chintai_0')}}
                        </div>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="row">
                        <div class="col-sm-8 padding-right-5 @{{#if data.is_chintai.is_error}} text-red @{{/if}}">
                            @{{{radioHandle
                                name="is_chintai"
                                value="1"
                                type="radio"
                                classLabel="custom-radio"
                                currentValue=(getValueHandle data.is_chintai.value)
                            }}}{{__('messages.is_chintai_1')}}
                        </div>
                    </div>
                </div>

            </fieldset>
        </div>
    </div>

    <div class="col-sm-6">
        <div class="form-group">
            <div class="col-md-12">
                <span>■ご参加のきっかけ</span><br/>
                @{{#each opportunityOpt}}
                    <span class="pull-left padding-bottom-5 @{{#if (eq @key '64')}}clearfix row padding-left-15 opportunity-different@{{/if}}">
                    @{{{checkBoxHandle
                        name="opportunity[]"
                        value=(getValueHandle @key)
                        type="checkbox"
                        classLabel="pull-left custom-check-box"
                        currentValue=(checkMultiCheckBox ../data.opportunity.value @key)
                    }}}<span class="pull-left padding-right-10">@{{this}}@{{#if (eq @key '64')}}（@{{/if}}</span>
                        @{{#if (eq @key '64')}}
                            <span class="pull-left padding-right-20">
                            <input type="text"
                            class="form-control input-sm col-md-5 @{{#if (getValueHandle ../data.opportunity_text.is_error) }}border-red@{{/if}}"
                            name="opportunity_text"
                            value="@{{getValueHandle ../data.opportunity_text.value}}">
                            </span>
                            <span class="pull-left">）</span>
                        @{{/if}}
                    </span>
                    {{-- @{{#if (eq @key '32')}}
                    <br/>
                    @{{/if}} --}}
                @{{/each}}
            </div>
        </div>
        <div class="form-group clearfix">
            <div class="col-md-12">
                <span>■DIY経験について</span><br/>
                <fieldset id="diy_exp">
                @{{#each diyExpOpt}}
                    <span>
                    @{{{radioHandle
                        name="diy_exp"
                        value=(getValueHandle @key)
                        type="radio"
                        classLabel="custom-radio"
                        currentValue=(getValueHandle ../data.diy_exp.value)
                    }}}<span padding-right-20">@{{this}}</span>
                @{{/each}}
                </fieldset>
            </div>
        </div>
        <div class="form-group">
            <div class="col-md-12">
                <span>■DIYでしたいこと、興味のあること</span><br/>
                @{{#each interestsOpt}}
                    <span class="pull-left padding-bottom-5 @{{#if (eq @key '256')}}clearfix row padding-left-15 interests-different @{{/if}}">
                    @{{{checkBoxHandle
                        name="interests[]"
                        value=(getValueHandle @key)
                        type="checkbox"
                        classLabel="pull-left custom-check-box"
                        currentValue=(checkMultiCheckBox ../data.interests.value @key)
                    }}}<span class="pull-left padding-right-10">@{{this}}@{{#if (eq @key '256')}}（@{{/if}}</span>
                        @{{#if (eq @key '256')}}
                            <span class="pull-left padding-right-20">
                            <input type="text"
                            class="form-control input-sm col-md-5 @{{#if (getValueHandle ../data.interests_text.is_error) }}border-red@{{/if}}"
                            name="interests_text"
                            value="@{{getValueHandle ../data.interests_text.value}}">
                            </span>
                            <span class="pull-left">）</span>
                        @{{/if}}
                    </span>
                    {{-- @{{#if (eq @key '32')}}
                    <br/>
                    @{{/if}} --}}
                @{{/each}}
            </div>
        </div>
        <div class="form-group">
            <div class="col-md-12">
                <span>■開催してほしいレッスンや、作りたいものをご記入ください。</span>
                @{{{inputHandle
                    name="your_wish"
                    class="form-control input-sm"
                    fieldGroupClass="col-sm-12"
                    typeField="textarea"
                    value=(getValueHandle data.your_wish.value)
                    rows="4"
                }}}
            </div>
        </div>
        @{{{inputHandle
            name="remarks"
            labelTitle=(trans "messages.membership_remarks")
            class="form-control input-sm"
            fieldGroupClass="col-sm-9"
            labelClass="col-md-3"
            typeField="textarea"
            value=(getValueHandle data.remarks.value)
            rows="4"
        }}}

    </div>
</script>
@include('includes.popup_deny');
<script type="text/javascript">
    function initial() {
        $('#enrolment_day').datepicker({
            format: "yyyy-mm-dd",
            todayHighlight: true,
            autoclose: true,
            language: 'ja'
        });
    }
    var member_id = '{{ Request::get('member_id') }}';
    var dataform = new DataForm(
        "{{action('MembershipController@detail')}}",
        {'member_id': member_id },
        initial
    );

    function submitFunc () {
        dataform.submitForm(
            "{{action('MembershipController@submitForm')}}",
            $('#form-submit').serialize(),
            initial
        );
    }



    if (!_.isEmpty(member_id)) {
        let params = {
            accessFlg : member_id,
            query : {
                    page_key : 'MEMBERSHIP-EDITING',
                    primary_key : 'index:' + member_id
                },
            key :member_id,
            _token : $('input[name="_token"]').val()
        };
        dataform.checkRealTime("{{action('DashboardController@checkItemEdit')}}", params, "{{action('MembershipController@index')}}");

        var deny = setInterval( function() {
            dataform.checkRealTime("{{action('DashboardController@checkItemEdit')}}", params, "{{action('MembershipController@index')}}");
        }, 5000);
    }

    function backFunc () {
        window.location.href = "{{action('MembershipController@index')}}";
    }
    $(document).ready(function() {
        $('#form-submit').show();

        $(".interests-different input[type='checkbox']").click(function() {
            if ($(this).is(":checked"))
            {
                $(".interests-different input[type='text']").prop('disabled', false);
            } else {
                $(".interests-different input[type='text']").prop('disabled', true);
            }
        });
        if ($(".interests-different input[value='256']").is(":checked")) {
            $(".interests-different input[type='text']").prop('disabled', false);
        } else {
            $(".interests-different input[type='text']").prop('disabled', true);
        }

        $(".opportunity-different input[type='checkbox']").click(function() {
            if ($(this).is(":checked"))
            {
                $(".opportunity-different input[type='text']").prop('disabled', false);
            } else {
                $(".opportunity-different input[type='text']").prop('disabled', true);
            }
        });
        if ($(".opportunity-different input[value='64']").is(":checked")) {
            $(".opportunity-different input[type='text']").prop('disabled', false);
        } else {
            $(".opportunity-different input[type='text']").prop('disabled', true);
        }
    })
    $('#studiy-member-menu').addClass('active');
    $('#membership-menu').addClass('active');

</script>
@endsection