<!DOCTYPE html>
<html lang="ja">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="{{ asset('/plugins/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('/receipt-pdf/css/custom.css') }}" rel="stylesheet">
    <link href="{{ asset('/plugins/bootstrap-datepicker/css/bootstrap-datepicker.min.css') }}" rel="stylesheet">
    <title>領収書発行</title>
        <style type="text/css">
        body {
            padding: 30px 0;
        }
        hr {
            border-color: black;
        }
        .hide-border {
            border-left-color: white ! important;
            border-right-color: white ! important;
        }
        .margin-table {
            margin-top: 30px;
            margin-bottom: 3px;
        }
    </style>
    </head>
    <body style="margin-bottom: 60px;">
        <div class="container">
        <div style="text-align: center;font-size: 18px;font-weight: bold;padding-bottom: 10px;">レッスンカルテ</div>
            <form id="frmKarute" action="{{action('MembershipController@karutePdf')}}" method="post">
                <button type="button" class="btn btn-primary" id="btnReceipt">領収書PDF発行</button>

            </form>


        </div>
        <script src="{{ asset('/plugins/jquery/jquery.min.js') }}"></script>
        <script src="{{ asset('/plugins/bootstrap/js/bootstrap.min.js') }}"></script>
        <script src="{{ asset('/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js') }}"></script>
        <script src="{{ asset('/plugins/bootstrap-datepicker/locales/bootstrap-datepicker.ja.min.js') }}"  type="text/javascript"></script>
        <script type="text/javascript">
        </script>
    </body>
</html>