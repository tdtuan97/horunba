@extends('layouts.main')
@section('titlePage', __('messages.membership_header'))
@section('content')
<section class="content-header">
    <h1>
        {{__('messages.membership_save_header')}}
    </h1>
    <ol class="breadcrumb">
        <li>
            <a href="{{action('MembershipController@index')}}">
                {{__('messages.membership_header')}}
            </a>
            <li class="active">
                <a href="{{Request::fullUrl()}}">{{__('messages.membership_save_header')}}</a>
            </li>
        </li>
    </ol>
</section>
<section class="content">
    <div id="root"><!-- content reactjs put here --></div>
    <div class="container-fluid">
        <form class="form-horizontal" id="form-submit" style="display: none;">
            <div class="loading" style="display: none;"></div>
            {{ csrf_field() }}
            <input type="hidden" name="lesson_id_old" value="{{Request::get('lesson_id')}}">
            <input type="hidden" name="open_date_old" value="{{Request::get('open_date')}}">
            <div class="box box-success">
                <div class="box-body">
                    <div class="row" id="main-contain">
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <button type="button" class="btn btn-danger pull-right" onclick="backFunc()">{{__('messages.btn_back')}}</button>
                            <button type="button" class="btn btn-primary pull-right margin-element" onclick="submitFunc()">{{__('messages.label_add')}}</button>
                        </div>
                    </div>
                </div>
            </div>
        </form>
        <div class="modal fade" id="message-modal-warning-point" data-backdrop="static" data-keyboard="false" style="display: none;">
            <div class="modal-dialog modal-sm">
                <div class="modal-content modal-custom-content">
                    <div class="modal-header">
                        <h4 class="modal-title"><b>{{__('messages.model_warning_point_title')}}</b></h4>
                    </div>
                    <div class="modal-body modal-custom-body">
                        {{__('messages.model_warning_point_message')}}
                    </div>
                    <div class="modal-footer modal-custom-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">{{__('messages.issue_modal_ok')}}</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
@section('javascript')
<script id="form-template" type="text/x-handlebars-template">
    {{-- @{{log data.remarks.value}} --}}
    <div class="col-md-6">
        @{{{inputHandle
            name="member_id"
            labelTitle=(trans "messages.member_id")
            class="form-control input-sm"
            fieldGroupClass="col-md-6"
            labelClass="col-md-3"
            type="text"
            errorPopup=true
            readOnly=true
            value=(getValueHandle data.member_id.value)
            hasError=(checkBooleanHandle data.member_id.is_error)
            errorMessage=(getValueHandle data.member_id.message)
        }}}
        @{{{selectHandle
            name="category_name"
            class="form-control input-sm"
            fieldGroupClass="col-md-6"
            {{-- groupClass="row" --}}
            type="text"
            errorPopup=true
            labelClass="col-md-3"
            labelTitle=(trans "messages.lesson_category_name_opt")
            options=optCategory
            value=(getValueHandle data.category_name.value)
            hasError=(checkBooleanHandle data.category_name.is_error)
            errorMessage=(getValueHandle data.category_name.message)
            onchange="categoryNameChange()"
        }}}
        @{{{selectHandle
            name="lesson_id"
            class="form-control input-sm"
            fieldGroupClass="col-md-6"
            {{-- groupClass="row" --}}
            type="text"
            errorPopup=true
            labelClass="col-md-3"
            labelTitle=(trans "messages.lesson_name")
            options=optLesson
            value=(getValueHandle data.lesson_id.value)
            hasError=(checkBooleanHandle data.lesson_id.is_error)
            errorMessage=(getValueHandle data.lesson_id.message)
            onchange="lessonIdChange()"
        }}}
        @{{{inputHandle
            name="used_point"
            labelTitle=(trans "messages.used_point")
            class="form-control input-sm"
            fieldGroupClass="col-md-6"
            labelClass="col-md-3"
            type="text"
            errorPopup=true
            readOnly=true
            value=(getValueHandle data.used_point.value)
            hasError=(checkBooleanHandle data.used_point.is_error)
            errorMessage=(getValueHandle data.used_point.message)
        }}}
        @{{{inputHandle
            name="open_date"
            labelTitle=(trans "messages.open_date")
            class="form-control input-sm"
            fieldGroupClass="col-sm-6"
            labelClass="col-md-3"
            type="text"
            id="open_date"
            errorPopup=true
            placeholder="YYYY-MM-DD"
            value=(getValueHandle data.open_date.value)
            hasError=(checkBooleanHandle data.open_date.is_error)
            errorMessage=(getValueHandle data.open_date.message)
        }}}
    </div>
</script>
<script type="text/javascript">
    function initial() {
        $('#open_date').datepicker({
            format: "yyyy-mm-dd",
            todayHighlight: true,
            autoclose: true,
            language: 'ja',
            orientation: 'bottom auto'
        });
    }
    function initialSubmit(paramCallback) {
        $('#open_date').datepicker({
            format: "yyyy-mm-dd",
            todayHighlight: true,
            autoclose: true,
            language: 'ja',
            orientation: 'bottom auto'
        });
        if (paramCallback === 'POINT') {
            $('#message-modal-warning-point').modal('show');
        }
    }
    var lesson_id = '{{ Request::get('lesson_id') }}';
    var member_id = '{{ Request::get('member_id') }}';
    var open_date = '{{ Request::get('open_date') }}';
    var dataform = new DataForm(
        "{{action('MembershipController@takeLessonDetail')}}",
        {
            lesson_id : lesson_id,
            member_id : member_id,
            open_date : open_date
        },
        initial
    );

    function submitFunc () {
        dataform.submitForm(
            "{{action('MembershipController@takeLessonSubmit')}}",
            $('#form-submit').serialize(),
            initialSubmit
        );
    }

    function backFunc () {
        window.location.href = "{!! URL::previous() !!}";
    }

    function categoryNameChange() {
        dataform.submitForm(
            "{{action('MembershipController@getInfoOpt')}}",
            $('#form-submit').serialize(),
            initial
        );
    }

    function lessonIdChange()
    {
        dataform.submitForm(
            "{{action('MembershipController@getInfoOpt')}}",
            $('#form-submit').serialize(),
            initial
        );
    }

    $(document).ready(function() {
        $('#form-submit').show();
    })
    $('#studiy-member-menu').addClass('active');
    $('#membership-menu').addClass('active');

</script>
@endsection