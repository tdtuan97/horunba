@extends('layouts.main')
@section('titlePage', __('messages.membership_header'))
@section('content')
<section class="content-header">
    <h1>
        {{__('messages.membership_save_header')}}
    </h1>
    <ol class="breadcrumb">
        <li>
            <a href="{{action('MembershipController@index')}}">
                {{__('messages.membership_header')}}
            </a>
            <li class="active">
                <a href="{{Request::fullUrl()}}">{{__('messages.membership_save_header')}}</a>
            </li>
        </li>
    </ol>
</section>
<section class="content">
    <div id="root"><!-- content reactjs put here --></div>
    <div class="container-fluid">

        <form class="form-horizontal" id="form-submit" style="display: none;">
            <div class="loading" style="display: none;"></div>
            <div class="box box-success">
                <div class="box-body">
                    <div class="row" id="main-contain">

                        <div class="col-md-6" style="pointer-events:none">
                            <div class="form-group">
                                <label class="col-md-3">{{__('messages.member_id')}}</label>
                                <div class="col-md-4">
                                    <input value="{{$member_info['member_id']}}" type="text" class="form-control input-sm" name="member_id">
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label class="col-md-6">{{__('messages.membership_last_name')}}</label>
                                        <div class="col-sm-6">
                                            <input value="{{$member_info['last_name']}}" type="text" class="form-control input-sm" name="last_name" >
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label class="col-md-6">{{__('messages.first_name')}}</label>
                                        <div class="col-sm-6">
                                            <input value="{{$member_info['first_name']}}" type="text" class="form-control input-sm" name="first_name">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-6">
                                    <div class="row">
                                        <label class="col-md-6">{{__('messages.last_name_kana')}}</label>
                                        <div class="col-sm-6">
                                            <input value="{{$member_info['last_name_kana']}}" type="text" class="form-control input-sm" name="last_name_kana">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="row">
                                        <label class="col-md-6">{{__('messages.first_name_kana')}}</label>
                                        <div class="col-sm-6">
                                            <input value="{{$member_info['first_name_kana']}}" type="text" class="form-control input-sm" name="first_name_kana">
                                        </div>
                                    </div>
                                </div>
                            </div>
                                <div class="form-group">
                                    <label class="col-md-3">{{__('messages.tel_no')}}</label>
                                    <div class="col-sm-6">
                                        <input value="{{$member_info['tel_no']}}" type="text" class="form-control input-sm" name="tel_no">
                                    </div>
                                </div>
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label class="col-md-6">{{__('messages.birthday')}}</label>
                                        <div class="col-sm-4 padding-right-5">
                                            <input value="{{$member_info['year']}}" type="text" class="form-control input-sm" name="year">
                                        </div>
                                        <label class="col-md-2 clear-padding margin-top-5">{{__('messages.year')}}</label>
                                    </div>
                                </div>
                                <div class="col-sm-3">
                                        <div class="form-group">
                                            <div class="col-sm-8 padding-right-5">
                                                <input value="{{$member_info['month']}}" type="text" class="form-control input-sm" name="month">
                                            </div>
                                        <label class="col-md-4 clear-padding margin-top-5">{{__('messages.month')}}</label>
                                    </div>
                                </div>
                                <div class="col-sm-3">
                                    <div class="form-group">
                                        <div class="col-sm-8 padding-right-5">
                                            <input value="{{$member_info['day']}}" type="text" class="form-control input-sm" name="month">
                                        </div>
                                        <label class="col-md-4 clear-padding margin-top-5">{{__('messages.day')}}</label>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <fieldset id="sex">
                                    <div class="col-sm-6">
                                        <div class="row">
                                            <div class="col-sm-6 padding-right-5 col-md-offset-6 ">
                                                <label class="control control-radio custom-radio">
                                                    <input type="radio" value="0" name="sex" @if ($member_info['sex'] === 0) checked="checked" @endif>
                                                    <div class="nice-icon-check"></div>
                                                </label>{{__('messages.women')}}
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-3">
                                        <div class="row">
                                            <div class="col-sm-8 padding-right-5 ">
                                                <label class="control control-radio custom-radio">
                                                    <input type="radio" value="1" name="sex" @if ($member_info['sex'] === 1) checked="checked" @endif>
                                                    <div class="nice-icon-check"></div>
                                                </label>{{__('messages.men')}}
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-3">
                                        <div class="row">
                                            <div class="col-sm-8 padding-right-5 ">
                                                <label class="control control-radio custom-radio">
                                                    <input type="radio" value="2" name="sex" @if ($member_info['sex'] === 2) checked="checked" @endif>
                                                    <div class="nice-icon-check"></div>
                                                </label>{{__('messages.unknown')}}
                                            </div>
                                        </div>
                                    </div>
                                </fieldset>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3">{{__('messages.address')}}</label>
                                <div class="col-sm-9">
                                    <input value="{{$member_info['address']}}" type="text" class="form-control input-sm" name="address">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3">{{__('messages.enrolment_day')}}</label>
                                <div class="col-sm-4">
                                    <input value="{{$member_info['enrolment_day']}}" id="enrolment_day" type="text" class="form-control input-sm" name="enrolment_day">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3">{{__('messages.mail_address')}}</label>
                                <div class="col-sm-9">
                                    <input value="{{$member_info['mail_address']}}" type="text" class="form-control input-sm" name="mail_address">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3">{{__('messages.nick_name')}}</label>
                                <div class="col-sm-9">
                                    <input value="{{$member_info['nick_name']}}" type="text" class="form-control input-sm" name="nick_name">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3">{{__('messages.remain_point')}}</label>
                                <div class="col-sm-9">
                                    <input value="{{$member_info['remain_point']}}" type="text" class="form-control input-sm" name="remain_point">
                                </div>
                            </div>
                            <div class="form-group">
                                <fieldset id="is_chintai">
                                    <div class="col-sm-6">
                                        <div class="row">
                                            <div class="col-sm-6 padding-right-5 col-md-offset-6 ">
                                                <label class="control control-radio custom-radio">
                                                    <input type="radio" value="0" name="is_chintai" @if ($member_info['is_chintai'] == 0) checked="checked" @endif>
                                                    <div class="nice-icon-check"></div>
                                                </label>{{__('messages.is_chintai_0')}}
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-3">
                                        <div class="row">
                                            <div class="col-sm-8 padding-right-5 ">
                                                <label class="control control-radio custom-radio">
                                                    <input type="radio" value="1" name="is_chintai" @if ($member_info['is_chintai'] == 1) checked="checked" @endif>
                                                    <div class="nice-icon-check"></div>
                                                </label>{{__('messages.is_chintai_1')}}
                                            </div>
                                        </div>
                                    </div>
                                </fieldset>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3">{{__('messages.membership_remarks')}}</label>
                                <div class="col-sm-9">
                                    <textarea rows="4" class="form-control input-sm" name="remarks">{{$member_info['remarks']}}</textarea>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-5-75 col-md-offset-0-25">
                            <div class="row">
                                <div class="col-md-12">
                                    <label>{{__('messages.lesson_history')}}</label>
                                </div>
                            </div>
                            <div class="row">
                            <div class="col-md-12">
                            <div class="table-scroll">
                                <table class="table table-member-lesson">
                                    <tbody>
                                        @foreach ($member_lessions as $member_lession)
                                        <tr class="lesson-info">
                                            <td class="col-md-2"> {{ date('Y/m/d', strtotime($member_lession['open_date'])) }}</td>
                                            <td class="col-md-9 text-word-break">{{ $member_lession['lesson_name'] }}</td>
                                            <td class="col-md-1">{{ $member_lession['used_point'] }}</td>
                                        </tr>
                                        <tr class="lesson-process">
                                            <td class="col-md-2">{{ $member_lession['incharge_person'] }}</td>
                                            <td class="col-md-9">
                                                @if ($member_lession['used_point'] != 0)
                                                <a
                                                class="btn btn-info btn-xs pull-right"
                                                onclick="processFree(
                                                    '{{$member_lession['member_id']}}',
                                                    '{{$member_lession['lesson_id']}}',
                                                    '{{$member_lession['open_date']}}',
                                                    this
                                                    )">{{__('messages.btn_free')}}
                                                </a>
                                                @endif
                                                <a
                                                href="{{ action('MembershipController@takeLesson') . '?member_id=' . $member_lession['member_id'] . '&lesson_id=' . $member_lession['lesson_id'] . '&open_date=' . $member_lession['open_date'] }}"
                                                class="btn btn-success btn-xs pull-right @if ($member_lession['used_point'] != 0) margin-element @endif">{{__('messages.lbl_edit')}}
                                                </a>
                                            </td>
                                            <td class="col-md-1"><a
                                                class="btn btn-danger btn-xs"
                                                onclick="processDelete(
                                                    '{{$member_lession['member_id']}}',
                                                    '{{$member_lession['lesson_id']}}',
                                                    '{{$member_lession['open_date']}}'
                                                    )">{{__('messages.btn_delete')}}
                                                </a>
                                            </td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                            </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-12">
                                    <a class="btn btn-primary pull-right margin-top-5" href="{{ action('MembershipController@takeLesson') . '?member_id=' . $member_info['member_id']}}">{{__('messages.lesson_regitry')}}</a>
                                </div>
                            </div>

                            <div class="row margin-top-50">
                                <div class="col-md-12">
                                    <label>{{__('messages.history_by_point')}}</label>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                <div class="table-scroll member_purchased">
                                    <table class="table table-striped">
                                        <thead>
                                            <tr>
                                                <th class="col-max-min-30">{{__('messages.lbl_paid_date')}}</th>
                                                <th class="col-max-min-30">{{__('messages.lbl_point_num')}}</th>
                                                <th class="col-max-min-20">{{__('messages.lbl_paid_price')}}</th>
                                                <th class="col-max-min-20">{{__('messages.lbl_unit_price')}}</th>
                                                <th class="col-max-min-50">{{__('messages.lbl_note')}}</th>
                                                <th class="col-max-min-20"></th>
                                            </tr>
                                         </thead>
                                        <tbody>
                                            @foreach ($member_purchaseds as $member_purchased)
                                            <tr>
                                                <td class="col-max-min-30">{{date('Y/m/d', strtotime($member_purchased['paid_date'])) }}</td>
                                                <td class="col-max-min-30">{{$member_purchased['point_num']}}</td>
                                                <td class="col-max-min-20">{{$member_purchased['paid_price']}}</td>
                                                <td class="col-max-min-20">{{$member_purchased['unit_price']}}</td>
                                                <td class="text-word-break text-long-batch col-max-min-50" title="{{$member_purchased['note']}}">{{$member_purchased['note']}}</td>
                                                <td class="text-center col-max-min-20">
                                                    <a href="{{ action('MembershipController@buyPoint') . '?member_id=' . $member_info['member_id'] . '&detail_num=' . $member_purchased['detail_num']}}" class="btn btn-info btn-xs">{{ __('messages.lbl_edit') }}</a>
                                                </td>
                                            </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                                </div>
                            </div>
                            <div class="row margin-top-20">
                                <div class="col-md-12">
                                    <a href="{{ action('MembershipController@buyPoint') . '?member_id=' . $member_info['member_id'] }}" class="btn btn-primary pull-right">{{ __('messages.btn_buy_point')}}</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row margin-top-50">
                        <div class="col-md-12">
                            <a href="{{action('MembershipController@index')}}" class="btn btn-danger pull-right">{{ __('messages.btn_cancel_member')}}</a>
                            <a href="{{action('MembershipController@save') . '?member_id=' . Request::get('member_id')}}" class="btn btn-info pull-right margin-element">{{ __('messages.btn_mem_update')}}</a>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</section>
@endsection
@section('javascript')
<script type="text/javascript">
    function processFree (member_id, lesson_id, open_date, _this) {
        $('.loading').show();
        let dataPost = {member_id: member_id, lesson_id: lesson_id, open_date: open_date, type: 'free'};
        $.ajax({
            url: "{{action('MembershipController@processFreeOrDelete')}}",
            data: dataPost,
            dataType: "json",
            success: function(response) {
                $('.loading').hide();
                if (response.status === 1) {
                    icon  = 'glyphicon-ok';
                    color = 'success';
                    $.notify({
                        icon: 'glyphicon ' + icon,
                        message: response.message,
                    },{
                        type: color,
                        delay: 3000
                    });
                } else {
                    $.notify({
                        icon: 'glyphicon glyphicon-remove',
                        message: response.message,
                    },{
                        type: 'danger',
                        delay: 3000
                    });

                }
                location.reload();
            }
        });
    }

    function processDelete (member_id, lesson_id, open_date) {
        // $('.loading').show();
        let dataPost = {member_id: member_id, lesson_id: lesson_id, open_date: open_date, type: 'delete'};
        var current = new Date('{{ date('Y-m-d') }}');
        var openDate = new Date(open_date);
        if (!open_date || (current > openDate)) {
            $.notify({
                icon: 'glyphicon glyphicon-remove',
                message: "{{__('messages.mess_open_date_pass')}}",
            },{
                type: 'danger',
                delay: 3000
            });
            return true;
        }
        $.ajax({
            url: "{{action('MembershipController@processFreeOrDelete')}}",
            data: dataPost,
            dataType: "json",
            success: function(response) {
                $('.loading').hide();
                if (response.status === 1) {
                    icon  = 'glyphicon-ok';
                    color = 'success';
                    $.notify({
                        icon: 'glyphicon ' + icon,
                        message: response.message,
                    },{
                        type: color,
                        delay: 3000
                    });
                }
                location.reload();
            }
        });
    }

    $(document).ready(function () {
        $('#form-submit').show();
    })
    $('#studiy-member-menu').addClass('active');
    $('#membership-menu').addClass('active');
</script>
@endsection