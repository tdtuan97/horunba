<!DOCTYPE html>
<html lang="ja">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>レッスンカルテ</title>
    <style type="text/css">
        html{
            font-size: 12px;
            font-weight: bold;
        }
        .content{
            margin-left: -20px;
            margin-top: 10px;
            width: 196mm;
            padding-bottom: 12px;
            margin-bottom: 20px;
        }
        @font-face {
            font-family: 'msgothic';
            font-weight: normal;
            font-style: normal;
            font-variant: normal;
            src: url("/fonts/MS Gothic.ttf") format("truetype");
        }
        @page {margin: 20px; border: 1px solid}
        body {
            border: 1px solid;
        }
        * {
          font-family: 'msgothic';
        }
        .clear-float {
            clear: both;
        }

    </style>
</head>
<body>
    <div class="content" style="margin: 0 auto;">
        <div style="width: 60mm;text-align: center;float: left;font-size: 17px;margin: 0 auto;padding-top: 20mm;padding-bottom: 10mm;">
            レッスンカルテ
        </div>
        <div style="width: 163mm;margin: auto;" class="clear-float">
            <div style="float: right;padding-bottom: 10px;">
                <table>
                    <tr>
                        <td style="font-size: 20px">No.</td>
                        <td style="font-size: 20px;border: 1px solid #000;padding: 0 5px;">2</td>
                        <td style="border: 1px solid #000;padding: 0 10px;"></td>
                        <td style="border: 1px solid #000;padding: 0 10px;"></td>
                        <td style="border: 1px solid #000;padding: 0 10px;"></td>
                        <td style="border: 1px solid #000;padding: 0 10px;"></td>
                        <td style="border: 1px solid #000;padding: 0 10px;"></td>
                        <td style="border: 1px solid #000;padding: 0 10px;"></td>
                        <td style="border: 1px solid #000;padding: 0 10px;"></td>
                        <td style="border: 1px solid #000;padding: 0 10px;"></td>
                    </tr>
                </table>

            </div>
        <div class="clear-float"></div>
        </div>
        <div style="width: 163mm;border:1px solid #000; margin: auto;">
            <div style="width: 145mm; margin: auto;">
                <div style="padding-bottom: 30mm;">
                    <div style="padding-top: 10mm;">
                        <div style="float: left;">氏名</div>
                        <div style="float: right;">男　・　女</div>
                    </div>
                    <div style="padding-top: 10mm;" class="clear-float">
                        <div style="float: left;">生年月日</div>
                    </div>
                    <div style="padding-top: 10mm;;" class="clear-float">
                        <div style="float: left;">ご住所</div>
                        <div style="float: right;">賃貸　・　持家</div>
                    </div>
                    <div style="padding-top: 10mm;" class="clear-float">
                        <div style="float: left;width: 50%;">電話番号</div>
                        <div style="float: left;width: 50%;">E-mail</div>
                    </div>
                </div>

                <div style="padding-bottom: 30px; padding-top: 5mm" class="clear-float">
                    <div style="font-weight: bold;">
                        ■ご参加のきっかけ
                    </div>
                    <div style="padding-top: 2mm;" class="clear-float">
                        <div style="float: left; padding-right: 6mm;">□ ホームページ</div>
                        <div style="float: left; padding-right: 6mm;">□ SNS</div>
                        <div style="float: left; padding-right: 6mm;">□ テレビ </div>
                        <div style="float: left; padding-right: 6mm;">□ 新聞・雑誌</div>
                        <div style="float: left; padding-right: 6mm;">□ 店頭案内</div>
                        <div style="float: left;">□口コミ</div>
                    </div>
                    <div style="padding-top: 2mm;" class="clear-float">
                        <div style="float: left;">□ その他　（　　　　　　　　　　　　　　）</div>
                    </div>
                </div>

                <div style="padding-bottom: 10mm; padding-top: 5mm" class="clear-float">
                    <div style="font-weight: bold;">
                        ■DIY 経験について
                    </div>
                    <div style="padding-top: 2mm;" class="clear-float">
                        <div style="float: left; padding-right: 15mm;">□ 初めて</div>
                        <div style="float: left; padding-right: 8mm;">□ 少しだけやったことがある</div>
                        <div style="float: left;">□ やっている</div>
                    </div>
                </div>

                <div style="padding-bottom: 10mm; padding-top: 5mm" class="clear-float">
                    <div style="font-weight: bold;">
                        ■DIY でしたいこと、興味のあること
                    </div>
                    <div style="padding-top: 2mm;" class="clear-float">
                        <div style="float: left; padding-right: 15mm;">□ 木工の基礎</div>
                        <div style="float: left; padding-right: 8mm;">□ 工具の使い方</div>
                        <div style="float: left; padding-right: 8mm;">□ 家具作り</div>
                        <div style="float: left;">□ 雑貨や小物の製作</div>
                    </div>
                    <div style="padding-top: 2mm;" class="clear-float">
                        <div style="float: left; padding-right: 5.2mm;">□ 床や壁の張り替え</div>
                        <div style="float: left; padding-right: 5mm;">□ リノベーション</div>
                        <div style="float: left; padding-right: 5mm;">□ 補修や修復</div>
                        <div style="float: left;">□ 賃貸でできる DIY</div>
                    </div>
                    <div style="padding-top: 2mm;" class="clear-float">
                        <div style="float: left;">□ その他　（　　　　　　　　　　　　　　）</div>
                    </div>
                </div>
                <div style="padding-bottom: 10mm; padding-top: 5mm" class="clear-float">
                    <div style="font-weight: bold;">
                        ■開催してほしいレッスンや、作りたいものをご記入ください。
                    </div>
                </div>
            </div>
        </div>
        <div style="width: 163mm;margin: auto;">
            <div style="padding-top: 8mm">
                <div>
                    □「会員規約」「料金」「個人情報保護方針」に関して、説明を受け、その内容について承諾します。
                </div>
            </div>
            <div style="padding-top: 5mm">
                <div>
                    以上の事柄を確認した上で、DIY FACTORY 会員登録を行います。
                </div>
            </div>
        </div>
        <div style="width: 163mm;margin: auto;" class="clear-float">
            <div style="margin-top: 10mm; width: 50%; float: right;">
                <div style="padding-bottom: 4mm;border-bottom: 1px solid #000;">
                    ご署名
                </div>
            </div>
        </div>

        <div style="width: 163mm;margin: auto;" class="clear-float">
            <div style="margin-top: 10mm; text-align: center;font-weight: bold;font-size: 34px;">
                DIY FACTORY
            </div>
        </div>
    </div>
</body>
</html>
