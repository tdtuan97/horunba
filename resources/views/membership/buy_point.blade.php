@extends('layouts.main')
@section('titlePage', __('messages.membership_header'))
@section('content')
<section class="content-header">
    <h1>
        {{__('messages.membership_save_header')}}
    </h1>
    <ol class="breadcrumb">
        <li>
            <a href="{{action('MembershipController@index')}}">
                {{__('messages.membership_header')}}
            </a>
            <li class="active">
                <a href="{{Request::fullUrl()}}">{{__('messages.membership_save_header')}}</a>
            </li>
        </li>
    </ol>
</section>
<section class="content">
    <div id="root"><!-- content reactjs put here --></div>
    <div class="container-fluid">

        <form class="form-horizontal" id="form-submit" style="display: none;">
            <div class="loading" style="display: none;"></div>
            {{ csrf_field() }}
            <input type="hidden" name="detail_num" value="{{Request::get('detail_num')}}">
            <div class="box box-success">
                <div class="box-body">
                    <div class="row" id="main-contain">
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <button type="button" class="btn btn-danger pull-right" onclick="backFunc()">{{__('messages.btn_back')}}</button>
                            <button type="button" class="btn btn-primary pull-right margin-element" onclick="submitFunc()">{{__('messages.label_add')}}</button>
                        </div>
                    </div>
                </div>
            </div>
        </form>
        <div id="popup-deny"></div>
    </div>
</section>
@endsection
@section('javascript')
<script id="form-template" type="text/x-handlebars-template">
    {{-- @{{log data.remarks.value}} --}}
    <div class="col-md-6">
        @{{{inputHandle
            name="member_id"
            labelTitle=(trans "messages.member_id")
            class="form-control input-sm"
            fieldGroupClass="col-md-4"
            labelClass="col-md-3"
            type="text"
            errorPopup=true
            readOnly=true
            value=(getValueHandle data.member_id.value)
            hasError=(checkBooleanHandle data.member_id.is_error)
            errorMessage=(getValueHandle data.member_id.message)
        }}}
        @{{{inputHandle
            name="point_num"
            labelTitle=(trans "messages.buy_point")
            class="form-control input-sm"
            fieldGroupClass="col-sm-6"
            labelClass="col-md-3"
            type="text"
            errorPopup=true
            value=(getValueHandle data.point_num.value)
            hasError=(checkBooleanHandle data.point_num.is_error)
            errorMessage=(getValueHandle data.point_num.message)
        }}}
        @{{{inputHandle
            name="paid_price"
            labelTitle=(trans "messages.payment_price")
            class="form-control input-sm"
            fieldGroupClass="col-sm-6"
            labelClass="col-md-3"
            type="text"
            errorPopup=true
            value=(getValueHandle data.paid_price.value)
            hasError=(checkBooleanHandle data.paid_price.is_error)
            errorMessage=(getValueHandle data.paid_price.message)
        }}}
        @{{{inputHandle
            name="paid_date"
            labelTitle=(trans "messages.lbl_paid_date")
            class="form-control input-sm react-datepicker-ignore-onclickoutside"
            fieldGroupClass="col-sm-6"
            labelClass="col-md-3"
            type="text"
            id="paid_date"
            errorPopup=true
            placeholder="YYYY-MM-DD"
            value=(getValueHandle data.paid_date.value)
            hasError=(checkBooleanHandle data.paid_date.is_error)
            errorMessage=(getValueHandle data.paid_date.message)
        }}}
        @{{{inputHandle
            name="note"
            labelTitle=(trans "messages.note")
            class="form-control input-sm"
            fieldGroupClass="col-sm-9"
            labelClass="col-md-3"
            typeField="textarea"
            value=(getValueHandle data.note.value)
            rows="4"
        }}}
    </div>
</script>
@include('includes.popup_deny');
<script type="text/javascript">
    function initial() {
        $('#paid_date').datepicker({
            format: "yyyy-mm-dd",
            todayHighlight: true,
            autoclose: true,
            language: 'ja',
            orientation: 'bottom auto'
        });
    }
    var member_id = '{{ Request::get('member_id') }}';
    var detail_num = '{{Request::get('detail_num')}}';
    var dataform = new DataForm(
        "{{action('MembershipController@buyPointDetail')}}",
        {
            'member_id': member_id,
            'detail_num': detail_num
        },
        initial
    );

    function submitFunc () {
        dataform.submitForm(
            "{{action('MembershipController@buyPointSubmit')}}",
            $('#form-submit').serialize(),
            initial
        );
    }

    if (!_.isEmpty(member_id)) {
        let params = {
            accessFlg : member_id,
            query : {
                    page_key : 'BUY-POINT-EDITING',
                    primary_key : 'index:' + member_id
                },
            key :member_id,
            _token : $('input[name="_token"]').val()
        };
        dataform.checkRealTime("{{action('DashboardController@checkItemEdit')}}", params, "{{action('MembershipController@index')}}");

        var deny = setInterval( function() {
            dataform.checkRealTime("{{action('DashboardController@checkItemEdit')}}", params, "{{action('MembershipController@index')}}");
        }, 5000);
    }

    function backFunc () {
        window.location.href = "{{action('MembershipController@review') . '?member_id='}}" +  member_id;
    }
    $(document).ready(function() {
        $('#form-submit').show();
    })
    $('#studiy-member-menu').addClass('active');
    $('#membership-menu').addClass('active');

</script>
@endsection