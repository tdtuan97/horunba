@extends('layouts.main')
@section('titlePage', __('messages.receive_mail_header'))
@section('content')
<section class="content-header">
    <h1>
        {{__('messages.memo_check_header')}}
    </h1>
    <ol class="breadcrumb">
        <li>
            <a href="{{action('StockConfirmController@index')}}">
                {{__('messages.memo_check_header')}}
            </a>
        </li>
    </ol>
</section>

<section class="content">
    <!-- Put your code here -->
    <div id="root"></div>
</section>
@endsection
@section('javascript')
<script>
    var baseUrl          = "{{action('MemoCheckController@index')}}";
    var dataListUrl      = "{{action('MemoCheckController@dataList')}}";
    var saveUrl          = "{{action('MemoCheckController@save')}}";
    var orderDetailUrl   = "{{action('OrderManagementController@detail')}}";
    var countMemoUrl     = "{{action('MemoCheckController@countMemo')}}";
</script>
@endsection