@extends('layouts.main')
@section('titlePage', __('messages.not_payment_list_header'))
@section('content')
<section class="content-header">
    <h1>
        {{__('messages.not_payment_list_header')}}
        <small></small>
    </h1>
    <ol class="breadcrumb">
        <li>
            <a href="{{action('NotPaymentListController@index')}}">{{__('messages.not_payment_list_menu_title')}}</a>
        </li>
    </ol>
</section>
<section class="content">
    <!-- Put your code here -->
    <div id="root"></div>
</section>
@endsection
@section('javascript')
<script type="text/javascript">
var baseUrl         = "{{action('NotPaymentListController@index')}}";
var dataListUrl     = "{{action('NotPaymentListController@dataList')}}";
$('#payment-menu').addClass('active');
$('#not-payment-list-menu').addClass('active');
</script>
@endsection