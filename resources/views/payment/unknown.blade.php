@extends('layouts.main')
@section('titlePage', __('messages.payment_unk_management_title'))
@section('content')
<section class="content-header">
    <h1>
        {{__('messages.payment_unk_management_title')}}
    </h1>
    <ol class="breadcrumb">
        <li>
            <a href="{{route('payment.unknown.list')}}">
                {{__('messages.payment_unk_management_title')}}
            </a>
        </li>
    </ol>
</section>

<section class="content">
    <!-- Put your code here -->
    <div id="root"></div>
</section>
@endsection
@section('javascript')
<script>
    $('#payment-menu').addClass('active');
    $('#payment-unknown-list-menu').addClass('active');
    var getOrderReceive = "{{action('PaymentController@getOrderByReceive')}}";
    var dataListUrl     = "{{action('PaymentController@dataListUnknown')}}";
    var deleteUrl       = "{{action('PaymentController@deleteItem')}}";
    var saveReceive     = "{{action('PaymentController@saveReceive')}}";
</script>
@endsection