@extends('layouts.main')
@section('titlePage', __('messages.payment_management_title'))
@section('content')
<section class="content-header">
    <h1>
        {{__('messages.payment_management_title')}}
    </h1>
    <ol class="breadcrumb">
        <li>
            <a href="{{route('payment.info.list')}}">
                {{__('messages.payment_management_title')}}
            </a>
        </li>        
    </ol>
</section>
<div class="popup-error fix-message collapse" id="popup-status">
    <div class="alert alert-dismissible alert-success">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <h4><i class="icon fa fa-check"></i> Alert!</h4>
        <p class="text-new-line" id="popup-message"></p>
    </div>
</div>
<section class="content">
    <!-- Put your code here -->
    <div id="root"></div>
</section>
@endsection
@section('javascript')
<script>
    $('#payment-menu').addClass('active');
    $('#payment-info-list-menu').addClass('active');
    var baseUrl          = "{{action('PaymentController@index')}}";
    var importUrl        = "{{action('PaymentController@import')}}";
    var csvUrl           = "{{action('Common@importCSV')}}";
    var dataListUrl      = "{{action('PaymentController@dataList')}}";
    var updateReceiveUrl = "{{action('PaymentController@updateReceive')}}";
</script>
@endsection