@extends('layouts.main')
@section('titlePage', __('messages.lesson_header'))
@section('content')
<section class="content-header">
    <h1>
        {{__('messages.lesson_save_header')}}
    </h1>
    <ol class="breadcrumb">
        <li>
            <a href="{{action('LessonController@index')}}">
                {{__('messages.lesson_header')}}
            </a>
        </li>
        <li class="active">
            <a href="{{Request::fullUrl()}}">{{__('messages.lesson_save_header')}}</a>
        </li>
    </ol>
</section>
<section class="content">
    <div id="root"><!-- content reactjs put here --></div>
    <div class="container-fluid">

        <form class="form-horizontal" id="form-submit" style="display: none;">
            {{ csrf_field() }}
            <div class="loading" style="display: none;"></div>
            <div class="box box-success">
                <div class="box-body">
                    <div class="row" id="main-contain">
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <button type="button" class="btn btn-danger pull-right" onclick="backFunc()">{{__('messages.btn_back')}}</button>
                            <button type="button" class="btn btn-primary pull-right margin-element" onclick="submitFunc()">{{__('messages.label_add')}}</button>
                        </div>
                    </div>
                </div>
            </div>
        </form>
        <div id="popup-deny"></div>
    </div>
</section>
@endsection
@section('javascript')
<script id="form-template" type="text/x-handlebars-template">
    @{{{inputHandle
        name="action"
        type="hidden"
        value=(getValueHandle data.action.value)
    }}}
    <div class="col-md-6">
        @{{{inputHandle
            name="lesson_id"
            labelTitle=(trans "messages.lesson_id")
            class="form-control input-sm"
            fieldGroupClass="col-md-6"
            labelClass="col-md-3"
            type="text"
            errorPopup=true
            readOnly=(getValueHandle data.lesson_id.readonly)
            value=(getValueHandle data.lesson_id.value)
            hasError=(checkBooleanHandle data.lesson_id.is_error)
            errorMessage=(getValueHandle data.lesson_id.message)
        }}}
        @{{{inputHandle
            name="category_name"
            labelTitle=(trans "messages.lesson_category_name")
            class="form-control input-sm"
            fieldGroupClass="col-md-6"
            labelClass="col-md-3"
            type="text"
            errorPopup=true
            value=(getValueHandle data.category_name.value)
            hasError=(checkBooleanHandle data.category_name.is_error)
            errorMessage=(getValueHandle data.category_name.message)
        }}}
        @{{{inputHandle
            name="lesson_name"
            labelTitle=(trans "messages.lesson_name")
            class="form-control input-sm"
            fieldGroupClass="col-md-6"
            labelClass="col-md-3"
            type="text"
            errorPopup=true
            value=(getValueHandle data.lesson_name.value)
            hasError=(checkBooleanHandle data.lesson_name.is_error)
            errorMessage=(getValueHandle data.lesson_name.message)
        }}}
        @{{{inputHandle
            name="require_point"
            labelTitle=(trans "messages.require_point")
            class="form-control input-sm"
            fieldGroupClass="col-md-6"
            labelClass="col-md-3"
            type="text"
            errorPopup=true
            value=(getValueHandle data.require_point.value)
            hasError=(checkBooleanHandle data.require_point.is_error)
            errorMessage=(getValueHandle data.require_point.message)
        }}}
        @{{{inputHandle
            name="priod"
            labelTitle=(trans "messages.lesson_priod")
            class="form-control input-sm"
            fieldGroupClass="col-md-6"
            labelClass="col-md-3"
            type="text"
            beforeField="<div class='input-group'>"
            afterField="<span class='input-group-addon'>分</span></div>"
            errorPopup=true
            value=(getValueHandle data.priod.value)
            hasError=(checkBooleanHandle data.priod.is_error)
            errorMessage=(getValueHandle data.priod.message)
        }}}
        @{{{selectHandle
            name="incharge_person"
            class="form-control input-sm"
            fieldGroupClass="col-md-6"
            {{-- groupClass="row" --}}
            type="text"
            errorPopup=true
            labelClass="col-md-3"
            labelTitle=(trans "messages.incharge_person")
            options=optTantou
            value=(getValueHandle data.incharge_person.value)
            hasError=(checkBooleanHandle data.incharge_person.is_error)
            errorMessage=(getValueHandle data.incharge_person.message)
        }}}
    </div>
</script>
@include('includes.popup_deny');
<script type="text/javascript">
    var lesson_id = '{{ Request::get('lesson_id') }}';
    var dataform = new DataForm(
        "{{action('LessonController@detail')}}",
        {'lesson_id': lesson_id }
    );

    function submitFunc () {
        dataform.submitForm(
            "{{action('LessonController@submitForm')}}",
            $('#form-submit').serialize()
        );
    }
    function backFunc () {
        window.location.href = "{{action('LessonController@index')}}";
    }

    if (!_.isEmpty(lesson_id)) {
        let params = {
            accessFlg : lesson_id,
            query : {
                    page_key : 'BUY-POINT-EDITING',
                    primary_key : 'index:' + lesson_id
                },
            key :lesson_id,
            _token : $('input[name="_token"]').val()
        };
        dataform.checkRealTime("{{action('DashboardController@checkItemEdit')}}", params, "{{action('LessonController@index')}}");

        var deny = setInterval( function() {
            dataform.checkRealTime("{{action('DashboardController@checkItemEdit')}}", params, "{{action('LessonController@index')}}");
        }, 5000);
    }

    $(document).ready(function() {
        $('#form-submit').show();
    });
    $('#studiy-member-menu').addClass('active');
    $('#lesson-menu').addClass('active');
</script>
@endsection