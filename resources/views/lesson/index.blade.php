@extends('layouts.main')
@section('titlePage', __('messages.lesson_header'))
@section('content')
<section class="content-header">
    <h1>
        {{__('messages.lesson_header')}}
    </h1>
    <ol class="breadcrumb">
        <li>
            <a href="{{action('LessonController@index')}}">
                {{__('messages.lesson_header')}}
            </a>
        </li>
    </ol>
</section>
<section class="content">
    <div id="root"><!-- content reactjs put here --></div>
    <div id="main-content" class="box" style="display: none">
        <div class="loading"></div>
        <div class="box-header">
            <div class="row">
                <div class="col-md-12">
                    <h4>{{__('messages.search_title')}}</h4>
                </div>
                <form id="search-form">
                    <div class="form-group col-md-2">
                        <label class="control-label">{{__('messages.lesson_id')}}</label>
                        <input name="lesson_id" class="form-control input-sm" value="{{ Request::input('lesson_id', '') }}">
                    </div>
                    <div class="form-group col-md-2">
                        <label class="control-label">{{__('messages.lesson_category_name')}}</label>
                        <input name="category_name" class="form-control input-sm" value="{{ Request::input('category_name', '') }}">
                    </div>
                    <div class="form-group col-md-2">
                        <label class="control-label">{{__('messages.lesson_name')}}</label>
                        <input name="lesson_name" class="form-control input-sm" value="{{ Request::input('lesson_name', '') }}">
                    </div>
                    <div class="form-group col-md-2">
                        <label class="control-label">{{__('messages.lesson_incharge_person')}}</label>
                        <input name="incharge_person" class="form-control input-sm" value="{{ Request::input('incharge_person', '') }}">
                    </div>
                    <div class="form-group col-md-1 per-page pull-right-md">
                        <label class="control-label">{{__('messages.per_page')}}</label>
                        <select name="per_page" class="form-control input-sm" onchange="search()">
                            @foreach([20, 40, 60, 80, 100] as $perPage)
                                @if ((int)Request::input('per_page', null) === $perPage)
                                <option selected value="{{$perPage}}">{{$perPage}}</option>
                                @else
                                <option value="{{$perPage}}">{{$perPage}}</option>
                                @endif
                            @endforeach
                        </select>
                    </div>
                </form>
            </div>

            <div class="row">
                <div class="col-md-9">
                    <a href="{{action('LessonController@save')}}" class="btn btn-success pull-left margin-element margin-form">{{ __('messages.label_add') }}</a>
                </div>
                <div class="col-md-3 col-xs-8 pull-right">
                    <button class="btn btn-danger pull-right" onclick="clearSearch()">{{__('messages.btn_reset')}}</button>
                    <button class="btn btn-primary pull-right margin-element" onclick="search()">{{__('messages.btn_search')}}</button>
                </div>
            </div>
        </div>
        <div class="box-body" id="main-contain">

        </div>
    </div>
</section>
@endsection
@section('javascript')
<script id="list-template" type="text/x-handlebars-template">
    <div class="row">
        <div class="col-md-12">
            @{{#if (gt data.last_page 1)}}
            <div class="row">
                <div class="col-md-2-5"></div>
                <div class="col-md-7 text-center">
                    <div class="text-center paginator-content"></div>
                </div>
                <div class="col-md-2-5 text-right line-height-md">
                    @{{data.from}}件 - @{{data.to}}件 【全@{{data.total}}件】
                </div>
            </div>
            @{{/if}}
            <div class="table-responsive">
                <table class="table table-striped table-custom">
                    <thead>
                        <tr>
                            <th class="visible-xs visible-sm"></th>
                            <th class="col-max-min-40 text-center text-middle">
                                {{__('messages.lesson_id')}}
                            </th>
                            <th class="col-max-min-90 text-center text-middle hidden-xs hidden-sm">
                                {{__('messages.lesson_category_name')}}
                            </th>
                            <th class="col-max-min-160 text-center text-middle">
                                {{__('messages.lesson_name')}}
                            </th>
                            <th class="col-max-min-60 text-center text-middle hidden-xs hidden-sm">
                                {{__('messages.require_point')}}
                            </th>
                            <th class="col-max-min-60 text-center text-middle hidden-xs hidden-sm">
                                {{__('messages.lesson_priod')}}
                            </th>
                            <th class="col-max-min-90 text-center text-middle hidden-xs hidden-sm">
                                {{__('messages.lesson_incharge_person')}}
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        @{{#if data.data}}
                            @{{#each data.data}}
                                <tr class="@{{this.class_row}}">
                                    <td class="visible-xs visible-sm">
                                        <b class="show-info">
                                            <i class="fa fa-angle-double-down" aria-hidden="true"></i>
                                        </b>
                                    </td>
                                    <td class="text-center" title="@{{this.lesson_id}}">
                                        <a class="link-detail" href="{{action('LessonController@save')}}?lesson_id=@{{this.lesson_id}}">@{{this.lesson_id}}</a>
                                    </td>
                                    <td class="hidden-xs hidden-sm" title="@{{this.category_name}}">
                                        @{{this.category_name}}
                                    </td>
                                    <td class="text-long-batch" title="@{{this.lesson_name}}">
                                        @{{this.lesson_name}}
                                    </td>
                                    <td class="hidden-xs hidden-sm text-right" title="@{{this.require_point}}">
                                        @{{this.require_point}}
                                    </td>
                                    <td class="hidden-xs hidden-sm text-right" title="@{{this.priod}}">
                                        @{{this.priod}}
                                    </td>
                                    <td class="hidden-xs hidden-sm" title="@{{this.incharge_person}}">
                                        @{{this.tantou_last_name}}
                                    </td>
                                </tr>
                                <tr class="hiden-info" style="display: none">
                                    <td colSpan="10" class="width-span1">
                                        <ul>
                                            <li> <b>{{__('messages.lesson_id')}}</b> : @{{this.lesson_id}}</li>
                                            <li> <b>{{__('messages.lesson_category_name')}}</b> : @{{this.category_name}}</li>
                                            <li> <b>{{__('messages.lesson_name')}}</b> : @{{this.lesson_name}}</li>
                                            <li> <b>{{__('messages.require_point')}}</b> : @{{this.require_point}}</li>
                                            <li> <b>{{__('messages.lesson_priod')}}</b> : @{{this.priod}}</li>
                                            <li> <b>{{__('messages.lesson_incharge_person')}}</b> : @{{this.tantou_last_name}}</li>
                                        </ul>
                                    </td>
                                </tr>
                            @{{/each}}
                        @{{else}}
                            <tr><td colSpan="17" class="text-center">{{__('messages.no_data')}}</td></tr>
                        @{{/if}}
                    </tbody>
                </table>
            </div>
            @{{#if (gt data.last_page 1)}}
            <div class="text-center paginator-content"></div>
            @{{/if}}
        </div>
    </div>
</script>
@include('includes.paginate');
<script>
    var ignoreKey = [];
    var datalist = new DataList("{{action('LessonController@dataList')}}", ignoreKey);
    function processData(_this, response) {
        var nf = new Intl.NumberFormat();
        response.data.data.map((item, index) => {
            item['class_row'] = (index % 2 === 0) ? 'odd' : 'even';
        });
    }
    function search() {
        datalist.search(processData);
    }
    function handleClickPage(page = 1) {
        datalist.handleClickPage(page, processData);
    }
    function clearSearch() {
        datalist.clearSearch(processData);
    }
    $(document).ready(function() {
        $("#main-content").show();
        var arrHref = window.location.href.split('?');
        var dataPost = '';
        if (arrHref.length > 1) {
            dataPost = arrHref[1];
        }
        datalist.reloadData(dataPost, processData);
    });
    $('#studiy-member-menu').addClass('active');
    $('#lesson-menu').addClass('active');
</script>
@endsection