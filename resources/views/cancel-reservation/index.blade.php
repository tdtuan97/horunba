@extends('layouts.main')
@section('titlePage', __('messages.cancel_reservation_header'))
@section('content')
<section class="content-header">
    <h1>
        {{__('messages.cancel_reservation_header')}}
    </h1>
    <ol class="breadcrumb">
        <li>
            <a href="{{action('CancelReservationController@index')}}">
                {{__('messages.cancel_reservation_breadcrumb_title')}}
            </a>
        </li>
    </ol>
</section>

<section class="content">
    <!-- Put your code here -->
    <div id="root"></div>
</section>
@endsection
@section('javascript')
<script>
    var baseUrl       = "{{action('CancelReservationController@index')}}";
    var saveUrl       = "{{action('CancelReservationController@save')}}";
    var dataListUrl   = "{{action('CancelReservationController@dataList')}}";
    var changeFlagUrl = "{{action('CancelReservationController@changeFlag')}}";
    $('#cancel-reservation-menu').addClass('active');
</script>
@endsection