<table>
    <thead>
    <tr>
        <th>受注番号</th>
        <th>お問合せ番号</th>
        <th>出荷日</th>
        <th>モール名</th>
        <th>入金先</th>
        <th>支払方法</th>
        <th>入金予定日</th>
        <th>実際の入金日</th>
        <th>大都入金予定金額</th>
        <th>商品金額小計</th>
        <th>送料</th>
        <th>決済料</th>
        <th>代引料</th>
        <th>送料値引</th>
        <th>値引</th>
        <th>ポイント入金予定日</th>
        <th>実際のポイント入金日</th>
        <th>利用ポイント</th>
        <th>クーポン入金予定日</th>
        <th>実際のクーポン入金日</th>
        <th>利用クーポン</th>
        <th>請求金額</th>
        <th>返品返金額</th>
        <th>ポイント入金</th>
        <th>クーポン入金</th>
        <th>請求金額の入金</th>
        <th>合計入金金額</th>
        <th>差異</th>
        <th>備考</th>
    </tr>
    </thead>
    <tbody>
    @foreach($datas as $data)
        <tr>
            <td>{{ $data['received_order_id'] or '' }}</td>
            <td>{{ $data['inquiry_no'] or '' }}</td>
            <td>{{ $data['delivery_date'] or '' }}</td>
            <td>{{ $data['mall_nm'] or '' }}</td>
            <td>{{ $data['payment_name'] or '' }}</td>
            <td>{{ $data['payment_method'] or '' }}</td>
            <td>{{ $data['payment_plan_date'] or '' }}</td>
            <td>{{ $data['payment_date'] or '' }}</td>
            <td>{{ $data['total_payment_plan'] or '' }}</td>
            <td>{{ $data['goods_price'] or '' }}</td>
            <td>{{ $data['ship_charge'] or '' }}</td>
            <td>{{ $data['pay_charge'] or '' }}</td>
            <td>{{ $data['pay_after_charge'] or '' }}</td>
            <td>{{ $data['pay_charge_discount'] or '' }}</td>
            <td>{{ $data['discount'] or '' }}</td>
            <td>{{ $data['paid_point_plan_date'] or '' }}</td>
            <td>{{ $data['point_paid_date'] or '' }}</td>
            <td>{{ $data['used_point'] or '' }}</td>
            <td colspan="{{ $data['colspan'] or 1 }}">{{ $data['coupon_paid_plan_date'] or '' }}</td>
            <td>{{ $data['coupon_paid_date'] or '' }}</td>
            <td>{{ $data['used_coupon'] or '' }}</td>
            <td>{{ $data['request_price'] or '' }}</td>
            <td>{{ $data['return_price'] or '' }}</td>
            <td>{{ $data['paid_point'] or '' }}</td>
            <td>{{ $data['paid_coupon'] or '' }}</td>
            <td>{{ $data['paid_request_price'] or '' }}</td>
            <td>{{ $data['total_paid_price'] or '' }}</td>
            <td>{{ $data['diff_price'] or '' }}</td>
            <td>{{ $data['remarks'] or '' }}</td>
        </tr>
    @endforeach
    </tbody>
</table>