@extends('layouts.main')
@section('titlePage', __('messages.users_management_save_header'))
@section('content')
<section class="content-header">
    <h1>
        {{__('messages.users_management_save_header')}}
        <small>{{__('messages.users_management_save_sub_header')}}</small>
    </h1>
    <ol class="breadcrumb">
        <li>
            <a href="{{action('UsersManagementController@index')}}">{{__('messages.users_management_breadcrumb_title')}}</a>
        </li>
        <li class="active">
            <a href="{{Request::fullUrl()}}">{{__('messages.users_management_save_header')}}</a>
        </li>
    </ol>
</section>

<section class="content">
    <!-- Put your code here -->
    <div id="root"></div>
</section>
@endsection
@section('javascript')
<script>
    var baseUrl        = "{{action('UsersManagementController@index')}}";
    var saveUrl        = "{{action('UsersManagementController@save')}}";
    var getFormDataUrl = "{{action('UsersManagementController@getFormData')}}";
    var previous       = "{!! URL::previous() !!}";
    $('#system-setting-menu').addClass('active');
    $('#users-management-menu').addClass('active');
</script>
@endsection