<html>
    <head>
        <style>
            @font-face {
                font-family: 'msgothic';
                font-weight: normal;
                font-style: normal;
                font-variant: normal;
                src: url("{{url('/')}}/fonts/MS Gothic.ttf") format("truetype");
            }
            @font-face {
                font-family: 'msgothic';
                font-weight: bold;
                font-style: bold;
                font-variant: bold;
                src: url("{{url('/')}}/fonts/MS Gothic.ttf") format("truetype");
            }
            @font-face {
                font-family: 'msgothic';
                font-weight: italic;
                font-style: italic;
                font-variant: italic;
                src: url("{{url('/')}}/fonts/MS Gothic.ttf") format("truetype");
            }
            .container {
                font-family: msgothic;
            }
            .box-top {
                margin: auto;
                border-style: solid;
                border-width: 5px;
                width: 600px;
                height: 60px;
                font-size: 38pt;
                font-weight: bold;
                text-align: center;
                vertical-align: middle;
                line-height: 50px;
                background-color: #DDDDDD;
            }
            .box-top > span {
                vertical-align: middle;
            }
            .inline {
                float: left;
            }
            .clear {
                clear: both;
            }
            .box-top-mid {
                margin: auto;
                margin-top: 20px;
                width: 600px;
            }
            .box-top-mid-left {
                width: 70%;
            }
            .box-top-mid-left > .text-greating {
                font-size: 14pt;
            }
            .box-top-mid-left > .wrap-fax-address {
                width: 100%;
                text-align: center;
            }
            .box-top-mid-left > .wrap-fax-address > .text-fax-address {
                font-size: 9pt;
            }
            .box-top-mid-right {
                width: 30%;
            }
            .box-top-mid-right > span.title {
                line-height: 35px;
            }
            .box-top-mid-right > span.normal {
                line-height: 25px;
            }
            .box-top-mid-right > span.sub {
                font-size: 10pt;
                line-height: 35px;
            }
            .box-mid {
                margin: auto;
                margin-top: 15px;
                border-style: solid;
                border-width: 3px;
                width: 600px;
                height: 36px;
                font-size: 18pt;
                font-weight: bold;
                text-align: center;
                vertical-align: middle;
                line-height: 30px;
                background-color: #DDDDDD;
            }
            .box-bot-mid {
                margin: auto;
                margin-top: 10px;
                width: 600px;
            }
            .box-bot-mid > .text-second {
                margin-left: 15px;
            }
            .footer {
                bottom: 10px;
                right: 0px;
                position: fixed;
                width: 50%;
            }
            .footer > .footer_date {
                margin-left: 15px;
            }
            .footer > .footer_time {
                margin-left: 40px;
            }
        </style>
    </head>
    <body>
        <div class="container">
            <div class="box-top">
                <span>FAX送付状</span>
            </div>
            <div class="box-top-mid">
                <div class="inline box-top-mid-left">
                    <span class="text-greating">{{$data['fax_name']}}</span><br/><br/><br/>
                    <div class="wrap-fax-address">
                        <span class="text-fax-address">送付先FAX番号： {{$data['fax_num']}}</span>
                    </div>
                </div>
                <div class="inline box-top-mid-right">
                    <span>株式会社大都</span><br>
                    <span class="title">担当：{{$data['login_user']}}</span><br>
                    <span class="normal">電話：06-6715-1172</span><br>
                    <span class="normal">FAX：06-6715-1666</span>
                    <span class="sub">問い合わせ№ {{$data['index']}}</span>
                </div>
                <div class="clear"></div>
            </div>
            <div class="box-mid">
                <span>連　絡　事　項</span>
            </div>
            <div class="box-bot-mid">
                {!! $data['fax_content'] !!}
            </div>
            <div class="footer">
                日付<span class="footer_date">{{$data['fax_date']}}</span><span class="footer_time">{{$data['fax_time']}}</span>
            </div>
        </div>
    </body>
</html>