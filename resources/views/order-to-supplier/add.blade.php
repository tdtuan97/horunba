@extends('layouts.main')
@section('titlePage', __('messages.save_order_to_supplier_header'))
@section('content')
<section class="content-header">
    <h1>
        {{__('messages.save_order_to_supplier_header')}}
        <small>{{__('messages.order_to_supplier_sub_header')}}</small>
    </h1>
    <ol class="breadcrumb">
        <li>
            <a href="{{action('OrderToSupplierController@index')}}">
                {{__('messages.order_to_supplier_menu_title')}}
            </a>
        </li>
    </ol>
</section>

<section class="content">
    <!-- Put your code here -->
    <div id="root"></div>
</section>
@endsection
@section('javascript')
<script>
var saveUrl = "{{action('OrderToSupplierController@save')}}";
var baseUrl = "{{action('OrderToSupplierController@index')}}";
var getDataProductUrl = "{{action('OrderToSupplierController@getDataProduct')}}";
$('#order-to-supplier-menu').addClass('active');
</script>
@endsection