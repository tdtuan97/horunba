@extends('layouts.main')
@section('titlePage', __('messages.order_to_supplier_header'))
@section('content')
<section class="content-header">
    <h1>
        {{__('messages.order_to_supplier_header')}}
        <small>{{__('messages.order_to_supplier_sub_header')}}</small>
    </h1>
    <ol class="breadcrumb">
        <li>
            <a href="{{action('OrderToSupplierController@index')}}">
                {{__('messages.order_to_supplier_menu_title')}}
            </a>
        </li>
    </ol>
</section>

<section class="content">
    <!-- Put your code here -->
    <div id="root"></div>
</section>
@endsection
@section('javascript')
<script>
var baseUrl          = "{{action('OrderToSupplierController@index')}}";
var dataListUrl      = "{{action('OrderToSupplierController@dataList')}}";
var changeStatusUrl  = "{{action('OrderToSupplierController@changeStatus')}}";
var orderDetailLink  = "{{action('OrderManagementController@detail')}}";
var exportCSV        = "{{action('Common@exportCSV')}}";
var processExportCSV = "{{action('Common@processExportCSV')}}";
var newUrl           = "{{action('OrderToSupplierController@save')}}";
var reSaleUrl        = "{{action('OrderToSupplierController@reSale')}}";
var returnUrl        = "{{action('OrderToSupplierController@processReturn')}}";
var saveReturnProductUrl = "{{action('ReturnManagementController@saveReturnProduct')}}";
var prepareProcessAllUrl = "{{action('OrderToSupplierController@prepareProcessAll')}}";
var getFaxOrderUrl = "{{action('OrderToSupplierController@getFaxOrder')}}";
var getFaxOrderCommonUrl = "{{action('OrderToSupplierController@getFaxOrderCommon')}}";
var processFaxOrderUrl = "{{action('OrderToSupplierController@processFaxOrder')}}";
$('#order-to-supplier-menu').addClass('active');
</script>
@endsection