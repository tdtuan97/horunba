@extends('layouts.main')
@section('titlePage', __('messages.dashboard_title'))
@section('css')
<link rel="stylesheet" href="{{ asset('/plugins/jvectormap/jquery-jvectormap-1.2.2.css') }}">
<link rel="stylesheet" href="{{ asset('/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css') }}">
<link rel="stylesheet" href="{{ asset('/plugins/bootstrap-daterangepicker/daterangepicker.css') }}">
<link rel="stylesheet" href="{{ asset('/plugins/morris.js/morris.css') }}">
<link rel="stylesheet" href="{{ asset('/plugins/bootstrap-datepicker/css/bootstrap-datepicker.min.css') }}">
@endsection
@section('content')
<section class="content-header">
    <h1>
        {{__('messages.dashboard_header')}}
        <small>{{__('messages.dashboard_sub_header')}}</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="/"><i class="fa fa-dashboard"></i> {{__('messages.dashboard_breadcrumb_title')}}</a></li>
        <li class="active">{{__('messages.dashboard_index_breadcrumb_title')}}</li>
    </ol>
</section>

<section class="content">
    <div id="root"></div>
    <!-- Main content -->
    <section class="content">
        <!-- Small boxes (Stat box) -->
        <div class="row">
            <div class="col-md-2">
                <button onclick="refund()" type="button" class="btn btn-block btn-primary btn-lg">返品交換管理</button>
            </div>
             <!--
            <div class="col-lg-3 col-xs-6">

                <div class="small-box bg-aqua">
                    <div class="inner">
                        <h3>返品交換管理</h3>

                        <p>Management Refund Order Detail</p>
                    </div>
                    <div class="icon">
                        <i class="ion ion-bag"></i>
                    </div>
                    <a href="{{action('RefundDetailController@save')}}" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                </div>
            </div>
        -->
        </div>
    </section>
</section>
@endsection
@section('javascript')
<script>
    function refund() {
        window.location.href = '{{action('RefundDetailController@save')}}';
    }
    $('#dashboard-menu').addClass('active');
</script>
@endsection