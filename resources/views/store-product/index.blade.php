@extends('layouts.main')
@section('titlePage', __('messages.store_product_header'))
@section('content')
<section class="content-header">
    <h1>
        {{__('messages.store_product_header')}}
    </h1>
    <ol class="breadcrumb">
        <li>
            <a href="{{action('StoreProductController@index')}}">
                {{__('messages.store_product_header')}}
            </a>
        </li>
    </ol>
</section>
<div class="popup-error fix-message collapse" id="popup-success">
    <div class="alert alert-dismissible alert-success">
        <h4><i class="icon fa fa-check"></i> Alert!</h4>
        <p class="text-new-line"></p>
    </div>
</div>
<div class="popup-error fix-message collapse" id="popup-error">
    <div class="alert alert-dismissible alert-error">
        <h4><i class="icon fa fa-ban"></i> Alert!</h4>
        <p class="text-new-line"></p>
    </div>
</div>
<div class="popup-error fix-message collapse" id="popup-info">
    <div class="alert alert-dismissible alert-info">
        <h4><i class="icon fa fa-info"></i> Info!</h4>
        <p class="text-new-line"></p>
    </div>
</div>
<section class="content">
    <!-- Put your code here -->
    <div id="root"></div>
</section>
@endsection
@section('javascript')
<script>
    var baseUrl     = "{{action('StoreProductController@index')}}";
    var dataListUrl = "{{action('StoreProductController@dataList')}}";
    var changeIsUpdatedUrl = "{{action('StoreProductController@changeIsUpdated')}}";
    var processSmaregiUrl = "{{action('StoreProductController@processSmaregi')}}";
    var addNewUrl = "{{action('StoreProductController@addNew')}}";
    $('#store-menu').addClass('active');
    $('#store-product-menu').addClass('active');
</script>
@endsection