@extends('layouts.main')
@section('titlePage', __('messages.store_product_add_header'))
@section('content')
<section class="content-header">
    <h1>
        {{__('messages.store_product_add_header')}}
    </h1>
    <ol class="breadcrumb">
        <li>
            <a href="{{action('StoreProductController@index')}}">
                {{__('messages.store_product_add_header')}}
            </a>
        </li>
    </ol>
</section>
<section class="content">
    <!-- Put your code here -->
    <div id="root"></div>
</section>
@endsection
@section('javascript')
<script>
    var baseUrl     = "{{action('StoreProductController@index')}}";
    var addNewUrl = "{{action('StoreProductController@addNew')}}";
    var getDataProductUrl = "{{action('StoreProductController@getDataProduct')}}";
    $('#store-menu').addClass('active');
    $('#store-product-menu').addClass('active');
</script>
@endsection