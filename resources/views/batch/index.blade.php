@extends('layouts.main')
@section('titlePage', __('messages.batch_status_management_title'))
@section('content')
<section class="content-header">
    <h1>
        {{__('messages.menu.batch_management')}}
    </h1>
    <ol class="breadcrumb">
        <li><a href="/">{{__('messages.menu.batch_management')}}</a></li>
    </ol>
</section>

<section class="content">
    <!-- Put your code here -->
    <div id="root"></div>
</section>
@endsection
@section('javascript')
<script>
    $('#batch-status-management-menu').addClass('active');
    var baseUrl      = "{{action('BatchManagementController@index')}}";
    var dataListUrl  = "{{action('BatchManagementController@dataList')}}";
    var processEvent = "{{action('BatchManagementController@processEvent')}}";
</script>
@endsection