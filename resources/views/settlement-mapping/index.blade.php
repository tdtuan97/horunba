@extends('layouts.main')
@section('titlePage', __('messages.settlement_mapping_management_header'))
@section('content')
<section class="content-header">
    <h1>
        {{__('messages.settlement_mapping_management_header')}}
    </h1>
    <ol class="breadcrumb">
        <li>
            <a href="{{action('SettlementMappingController@index')}}">
                {{__('messages.settlement_mapping_breadcrumb_title')}}
            </a>
        </li>
    </ol>
</section>
<div class="popup-error fix-message collapse" id="popup-status">
    <div class="alert alert-dismissible alert-success">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <h4><i class="icon fa fa-check"></i> Alert!</h4>
        <p class="text-new-line" id="popup-message"></p>
    </div>
</div>
<section class="content">
    <!-- Put your code here -->
    <div id="root"></div>
</section>
@endsection
@section('javascript')
<script>
    var baseUrl               = "{{action('SettlementMappingController@index')}}";
    var dataListUrl           = "{{action('SettlementMappingController@dataList')}}";
    var updateStatusUrl       = "{{action('SettlementMappingController@updateStatus')}}";
    var processApiUrl         = "{{action('SettlementMappingController@processApi')}}";
    var importUrl             = "{{action('SettlementMappingController@import')}}";
    var processUpdateAll      = "{{action('SettlementMappingController@processUpdateAll')}}";
    var csvUrl                = "{{action('Common@importCSV')}}";
    var processAfterImportUrl = "{{action('SettlementMappingController@processAfterImport')}}";
    var exportCSV             = "{{action('Common@exportCSV')}}";
    var processExportCSV      = "{{action('SettlementMappingController@processExportCsv')}}";
    $('#mapping-menu').addClass('active');
    $('#settlement-mapping-menu').addClass('active');
</script>
@endsection