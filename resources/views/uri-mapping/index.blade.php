@extends('layouts.main')
@section('titlePage', __('messages.uri_header'))
@section('content')
<section class="content-header">
    <h1>
        {{__('messages.uri_header')}}
    </h1>
    <ol class="breadcrumb">
        <li>
            <a href="{{action('UriMappingController@index')}}">
                {{__('messages.uri_header')}}
            </a>
        </li>
    </ol>
</section>
<section class="content">
    <div id="root"><!-- content reactjs put here --></div>
    <div id="main-content" class="box" style="display: none">
        <div class="loading"></div>
        <div class="box-header">
            <div class="row">
                <div class="col-md-12">
                    <h4>{{__('messages.search_title')}}</h4>
                </div>
                <form id="search-form">
                    <div class="col-md-10">
                    <div class="row">
                    <div class="form-group col-md-2">
                        <label class="control-label">{{__('messages.mall_name')}}</label>
                        <div>
                            <select id="name_jp" name="name_jp[]" multiple='multiple' class="form-control input-sm">
                                @foreach ($mallDatas as $mallData)
                                <option value={{ $mallData->key }}>{{ $mallData->value }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="form-group col-md-2">
                        <label class="control-label">{{__('messages.received_order_id')}}</label>
                        <input name="received_order_id" class="form-control input-sm" value="{{ Request::input('received_order_id', '') }}">
                    </div>
                    <div class="form-group col-md-1-5">
                        <label class="control-label">{{__('messages.pay_method_name')}}</label>
                        <div>
                            <select id="payment_method" name="payment_method[]" multiple='multiple' class="form-control input-sm">
                                @foreach ($settDatas as $settData)
                                <option value={{ $settData->key }}>{{ $settData->value }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="form-group col-md-1-5">
                        <label class="control-label">{{__('messages.delivery_date_from')}}</label>
                        <input id="delivery_date_from" name="delivery_date_from" class="form-control input-sm" value="{{ Request::input('delivery_date_from', '') }}">
                    </div>
                    <div class="form-group col-md-1-5">
                        <label class="control-label">{{__('messages.delivery_date_to')}}</label>
                        <input id="delivery_date_to" name="delivery_date_to" class="form-control input-sm" value="{{ Request::input('delivery_date_to', '') }}">
                    </div>
                    <div class="form-group col-md-2">
                        <label class="control-label">{{__('messages.uri_payment_name')}}</label>
                        <input name="payment_name" class="form-control input-sm" value="{{ Request::input('payment_name', '') }}">
                    </div>

                    <div class="form-group col-md-1-5">
                        <label class="control-label">{{__('messages.diff_price')}}</label>
                        <div>
                            <select name="is_corrected" class="form-control input-sm">
                                @foreach ($diffPriceOpts as $diffPriceOpt)
                                <option value={{ $diffPriceOpt['key'] }}>{{ $diffPriceOpt['value'] }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="form-group col-md-1-5 unset-float">
                        <label class="control-label">{{__('messages.disparity_of_price')}}</label>
                        <input name="diff_price" class="form-control input-sm" value="{{ Request::input('diff_price', '') }}">
                    </div>
                    </div>
                    </div>
                    <div class="form-group col-md-1-5 per-page pull-right-md">
                        <label class="control-label">{{__('messages.per_page')}}</label>
                        <select name="per_page" class="form-control input-sm" onchange="search()">
                            @foreach([20, 40, 60, 80, 100] as $perPage)
                                @if ((int)Request::input('per_page', null) === $perPage)
                                <option selected value="{{$perPage}}">{{$perPage}}</option>
                                @else
                                <option value="{{$perPage}}">{{$perPage}}</option>
                                @endif
                            @endforeach
                        </select>
                    </div>

                </form>
            </div>

            <div class="row">
                <div class="col-md-8">
                    <button  onclick="" class="btn btn-success pull-left margin-element margin-form" data-toggle="modal" data-target="#csv-modal" data-button="rakuten_pay_csv">{{ __('messages.button_rakuten_pay_csv') }}</button>
                    <button  onclick="" class="btn btn-success pull-left margin-element margin-form" data-toggle="modal" data-target="#csv-modal" data-button="yahoo_pay_csv">{{ __('messages.button_yahoo') }}</button>
                    <button  onclick="" class="btn btn-success pull-left margin-element margin-form" data-toggle="modal" data-target="#csv-modal" data-button="amazon_pay_csv">{{ __('messages.button_amazon_pay_csv') }}</button>
                    <button  onclick="" class="btn btn-success pull-left margin-element margin-form" data-toggle="modal" data-target="#csv-modal" data-button="pro_mfk_csv">{{ __('messages.button_MFK_csv') }}</button>
                    <button  onclick="" class="btn btn-success pull-left margin-element margin-form" data-toggle="modal" data-target="#csv-modal" data-button="pro_paygent_csv">{{ __('messages.button_pro_paygent_csv') }}</button>
                    <button  onclick="" class="btn btn-success pull-left margin-element margin-form" data-toggle="modal" data-target="#csv-modal" data-button="rakuten_coupon_csv">{{ __('messages.button_rakuten_coupon_csv') }}</button>
                    <button  onclick="" class="btn btn-success pull-left margin-element margin-form" data-toggle="modal" data-target="#csv-modal" data-button="jp_post_csv">{{ __('messages.button_jp_post_csv') }}</button>
                    <button  onclick="" class="btn btn-success pull-left margin-element margin-form" data-toggle="modal" data-target="#csv-modal" data-button="sagawa_pay_csv">{{ __('messages.button_sagawa_pay_csv') }}</button>
                    <button  onclick="" class="btn btn-success pull-left margin-element margin-form" data-toggle="modal" data-target="#csv-modal" data-button="seinou_csv">{{ __('messages.button_seinou_csv') }}</button>
                </div>
                <div class="col-md-4 col-xs-8 pull-right">

                    <button class="btn btn-danger pull-right" onclick="clearSearch()">{{__('messages.btn_reset')}}</button>
                    <button class="btn btn-primary pull-right margin-element" onclick="search()">
                    {{__('messages.btn_search')}}</button>
                    <button class="btn btn-primary pull-right margin-element btn-inverse-custom" onclick="exportExcel()">
                    {{__('messages.btn_export_excel')}}</button>
                </div>
            </div>
        </div>
        <div class="box-body" id="main-contain">

        </div>
        <div class="modal fade" id="access-memo-modal">
           <div class="loading" style=></div>
           <div class="modal-dialog ">
                <div class="modal-content">
                    <div class="modal-header"><b>{{__('messages.accept_title')}}</b></div>
                    <div class="modal-body modal-custom-body">
                        <div class="row">
                            <div class="col-md-12 message-popup">
                                <p>{{__('messages.memo_popup_ask')}}</p>
                            </div>
                        </div>
                    </div>
                    <form id="popup-form">
                        <input type="hidden" id="name" name="name" value="">
                        <input type="hidden" id="value" name="value" value="">
                        <div class="modal-footer modal-custom-footer">
                            <button type="button" class="btn btn-primary btn-sm modal-custom-button" onclick="saveDataAll()">{{__('messages.btn_accept_ok')}}</button>
                            <button type="button" class="btn bg-purple btn-sm modal-custom-button" data-dismiss="modal">{{__('messages.btn_accept_cancel')}}</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="modal fade" id="add-new-modal">
           <div class="loading" style=></div>
           <div class="modal-dialog ">
                <div class="modal-content">
                    <div class="modal-header"><b>{{__('messages.btn_add')}}</b></div>
                    <div class="modal-body modal-custom-body">
                        <form id="add-popup-form" class="form-horizontal">
                            <input type="hidden" id="name" name="name" value="">
                            <input type="hidden" id="condition" name="condition" value="">
                            <div class="row">
                                <div class="col-md-12 margin-form parent-input">
                                    <label class="col-md-3 field-name"></label>
                                    <div class="col-md-9">
                                        <input name="value-input" class="form-control input-sm" value="">
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="modal-footer modal-custom-footer">
                        <button type="button" class="btn btn-primary btn-sm modal-custom-button" onclick="saveDataOption()">{{__('messages.btn_accept_ok')}}</button>
                        <button type="button" class="btn bg-purple btn-sm modal-custom-button" data-dismiss="modal">{{__('messages.btn_accept_cancel')}}</button>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade in" id="csv-modal">
            <div class="modal-dialog">
                <div class="modal-content modal-custom-content ">
                    <div class="modal-body modal-custom-body form-csv">
                        <p id="modalTitle">銀行データ取込</p>
                        <form id="form-csv" class="form-horizontal" enctype="multipart/form-data; boundary=----WebKitFormBoundaryrSTmi6C5Dte6fNNtEfWl7u0FSTNpHKvq9qon0Ki7">
                            <input type="hidden" id="action-modal" value="" name="name_csv">
                            {{ csrf_field() }}
                            <div class="form-group" id="form-group-field">
                              <label class="control-label"></label>
                              <div class="col-md-12" id="form-field">
                                 <div class="input-group input-group-sm">
                                     <input class="form-control input-sm" value="" name="attached_file_path">
                                     <span class="input-group-btn">
                                         <button type="button" id="add_file" class="btn btn-info btn-flat btn-largest">ファイル選択</button>
                                         <input type="file" name="attached_file" class="hidden"  onchange="handleChangeFile()">
                                     </span>
                                 </div>
                              </div>
                            </div>
                            <div class="modal-footer modal-custom-footer">
                                <button type="submit" id="submit-upload" class="btn btn-primary modal-custom-button">{{ __('messages.issue_modal_ok')}}</button>
                                <button type="button" class="btn bg-purple modal-custom-button" data-dismiss="modal">{{ __('messages.issue_modal_cancel')}}</button>
                            </div>
                        </form>
                    </div>
                    <div class="modal-body modal-custom-body link-error">
                        <p> Have error please refer attachment <a href="#" class="link-detail">file</a></p>
                        <div class="modal-footer modal-custom-footer">
                            <button type="submit" id="submit-upload" class="btn btn-primary modal-custom-button" disabled>{{ __('messages.issue_modal_ok')}}</button>
                            <button type="button" class="btn bg-purple modal-custom-button" data-dismiss="modal">{{ __('messages.issue_modal_cancel')}}</button>
                        </div>
                    </div>
                </div>
                <div class="progress collapse progress-bar-csv">
                    <div class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100"></div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
@section('javascript')
<script id="list-template" type="text/x-handlebars-template">
    <div class="row">
        <div class="col-md-12">
            @{{#if (gt pageData.last_page 1)}}
            <div class="row">
                <div class="col-md-2-5"></div>
                <div class="col-md-7 text-center">
                    <div class="text-center paginator-content"></div>
                </div>
                <div class="col-md-2-5 text-right line-height-md">
                    @{{pageData.from}}件 - @{{pageData.to}}件 【全@{{pageData.total}}件】
                </div>
            </div>
            @{{/if}}
            <div class="table-responsive">
                <table class="table table-striped table-custom">
                    <thead>
                        <tr>
                            <th class="visible-xs visible-sm col-max-min-60" rowspan="2"></th>
                            <th class="col-max-min-60 text-center text-middle" rowspan="2">

                            </th>
                            <th class="col-max-min-150 text-center text-middle" rowspan="2">
                                {{__('messages.received_order_id')}}
                            </th>
                            <th class="col-max-min-90 text-center text-middle hidden-xs hidden-sm">
                                {{__('messages.payment_order_date')}}
                            </th>
                            <th class="col-max-min-90 text-center text-middle hidden-xs hidden-sm">
                                {{__('messages.mall_name')}}
                            </th>
                            <th class="col-max-min-90 text-center text-middle hidden-xs hidden-sm">
                                {{__('messages.pay_method_name')}}
                            </th>
                            <th class="col-max-min-90 text-center text-middle hidden-xs hidden-sm">
                                {{__('messages.payment_plan_date')}}
                            </th>
                            <th class="col-max-min-90 text-center text-middle hidden-xs hidden-sm" rowspan="2">
                                {!!__('messages.daito_total_price')!!}
                            </th>
                            <th class="col-max-min-90 text-center text-middle hidden-xs hidden-sm">
                                {{__('messages.ship_charge')}}
                            </th>
                            <th class="col-max-min-90 text-center text-middle hidden-xs hidden-sm">
                                {{__('messages.pay_charge')}}
                            </th>
                            <th class="col-max-min-90 text-center text-middle hidden-xs hidden-sm">
                                {{__('messages.lbl_point_paid_plan_date')}}
                            </th>
                            <th class="col-max-min-90 text-center text-middle hidden-xs hidden-sm">
                                {{__('messages.uri_used_point')}}
                            </th>
                            <th class="col-max-min-90 text-center text-middle hidden-xs hidden-sm">
                                {{__('messages.lbl_coupon_paid_plan_date')}}
                            </th>
                            <th class="col-max-min-90 text-center text-middle hidden-xs hidden-sm">
                                {{__('messages.uri_used_coupon')}}
                            </th>
                            <th class="col-max-min-90 text-center text-middle hidden-xs hidden-sm">
                                {{__('messages.request_price')}}
                            </th>
                            <th class="col-max-min-90 text-center text-middle hidden-xs hidden-sm">
                                {{__('messages.paid_request_price')}}
                            </th>
                            <th class="col-max-min-90 text-center text-middle hidden-xs hidden-sm" rowspan="2">
                                {{__('messages.uri_diff_price')}}
                            </th>
                            <th class="col-max-min-150 text-center text-middle">
                                {{__('messages.occur_reason')}}
                            </th>
                            <th class="col-max-min-150 text-center text-middle">
                                {{__('messages.remarks_uri')}}
                            </th>
                            <th class="text-center text-middle">
                                {{__('messages.finished_date')}}
                            </th>
                        </tr>
                        <tr>
                            <th class="col-max-min-90 text-center text-middle hidden-xs hidden-sm">
                                {{__('messages.delivery_date')}}
                            </th>
                            <th class="col-max-min-60 text-center text-middle hidden-xs hidden-sm">
                                {{__('messages.uri_payment_name')}}
                            </th>
                            <th class="col-max-min-60 text-center text-middle hidden-xs hidden-sm">
                                {{__('messages.status_name')}}
                            </th>
                            <th class="col-max-min-90 text-center text-middle hidden-xs hidden-sm">
                                {{__('messages.uri_payment_date')}}
                            </th>
                            <th class="col-max-min-90 text-center text-middle hidden-xs hidden-sm">
                                {{__('messages.pay_charge_discount')}}
                            </th>
                            <th class="col-max-min-90 text-center text-middle hidden-xs hidden-sm">
                                {{__('messages.pay_after_charge')}}
                            </th>
                            <th class="col-max-min-90 text-center text-middle hidden-xs hidden-sm">
                                {{__('messages.lbl_point_paid_date')}}
                            </th>
                            <th class="col-max-min-90 text-center text-middle hidden-xs hidden-sm">
                                {{__('messages.uri_paid_point')}}
                            </th>
                            <th class="col-max-min-90 text-center text-middle hidden-xs hidden-sm">
                                {{__('messages.lbl_coupon_paid_date')}}
                            </th>
                            <th class="col-max-min-90 text-center text-middle hidden-xs hidden-sm">
                                {{__('messages.uri_paid_coupon')}}
                            </th>
                            <th class="col-max-min-90 text-center text-middle hidden-xs hidden-sm">
                                {{__('messages.uri_return_price')}}
                            </th>
                            <th class="col-max-min-90 text-center text-middle hidden-xs hidden-sm">
                                {{__('messages.uri_total_paid_price')}}
                            </th>
                            <th class="col-max-min-90 text-middle hidden-xs hidden-sm">
                                <div class="col-md-10 col-xs-10 clear-padding-all"  style="color:#000;">
                                    <select id="occur_reason" class="form-control input-sm select-all-reset" onchange="saveDataSelectAll(this, 'occur_reason')">
                                        <option value="">----</option>
                                        @{{{this.optionOccurReasonAll}}}
                                    </select>
                                </div>
                                <div class="col-md-2 col-xs-2 clear-padding-all">
                                    <button type="button" name="edit" class="btn btn-box-tool btn-save" style="margin-top: 5px;" onclick="addNewOption('occur_reason', 'all')">
                                        <i class="fa fa-plus"></i>
                                    </button>
                                </div>
                            </th>
                            <th class="col-max-min-90 text-middle hidden-xs hidden-sm">
                                <div class="col-md-10 col-xs-10 clear-padding-all" style="color:#000;">
                                    <select id="occur_reason" class="form-control input-sm select-all-reset" onchange="saveDataSelectAll(this, 'remarks')">
                                        <option value="">----</option>
                                        @{{{this.optionRemarksAll}}}
                                    </select>
                                </div>
                                <div class="col-md-2 col-xs-2 clear-padding-all">
                                    <button type="button" name="edit" class="btn btn-box-tool btn-save" style="margin-top: 5px;" onclick="addNewOption('remarks', 'all')">
                                        <i class="fa fa-plus"></i>
                                    </button>
                                </div>
                            </th>
                            <th class="text-center text-middle">
                                <button class="btn btn-primary text-center" name="is_corrected" data-toggle="modal" onclick="saveDataSelectAll(this, 'button_update_all')">{{__('messages.btn_correct_uri')}}</button>
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        @{{#if data.data}}
                            @{{#each data.data}}
                                <tr class="@{{this.class_row}}">
                                    <td class="visible-xs visible-sm text-center" rowspan="2">
                                        <b class="show-info">
                                            <i class="fa fa-angle-double-down" aria-hidden="true"></i>
                                        </b>
                                    </td>
                                    <td class="cut-text text-middle">
                                        <a target="_blank" href="@{{this.link_order_detail}}">
                                            <img src="/img/uri_img_order_detail.png" width="35" />
                                        </a>
                                    </td>
                                    <td class="cut-text text-middle" title="@{{this.received_order_id}}" rowspan="2">
                                        @{{this.received_order_id}}
                                    </td>
                                    <td class="hidden-xs hidden-sm" title="@{{this.order_date}}">
                                        @{{this.order_date}}
                                    </td>
                                    <td class="hidden-xs hidden-sm" title="@{{this.name_jp}}">
                                        @{{this.name_jp}}
                                    </td>
                                    <td class="hidden-xs hidden-sm text-right" title="@{{this.payment_method}}">
                                        @{{this.payment_method}}
                                    </td>
                                    <td class="hidden-xs hidden-sm" title="@{{this.payment_plan_date}}">
                                        @{{this.payment_plan_date}}
                                    </td>
                                    <td class="hidden-xs hidden-sm text-right" title="@{{this.total_payment_plan}}" rowspan="2">
                                        @{{this.total_payment_plan}}
                                    </td>
                                    <td class="hidden-xs hidden-sm text-right" title="@{{this.ship_charge}}">
                                        @{{this.ship_charge}}
                                    </td>
                                    <td class="hidden-xs hidden-sm text-right" title="@{{this.pay_charge}}">
                                        @{{this.pay_charge}}
                                    </td>
                                    <td class="hidden-xs hidden-sm text-right" title="@{{this.paid_point_plan_date}}">
                                        @{{this.paid_point_plan_date}}
                                    </td>
                                    <td class="hidden-xs hidden-sm text-right" title="@{{this.used_point}}">
                                        @{{this.used_point}}
                                    </td>
                                    <td class="hidden-xs hidden-sm text-right" title="@{{this.coupon_paid_plan_date}}">
                                        @{{this.coupon_paid_plan_date}}
                                    </td>
                                    <td class="hidden-xs hidden-sm text-right" title="@{{this.used_coupon}}">
                                        @{{this.used_coupon}}
                                    </td>
                                    <td class="hidden-xs hidden-sm text-right" title="@{{this.request_price}}">
                                        @{{this.request_price}}
                                    </td>
                                    <td class="hidden-xs hidden-sm text-right" title="@{{this.paid_request_price}}">
                                        @{{this.paid_request_price}}
                                    </td>
                                    <td class="hidden-xs hidden-sm text-right text-middle" title="@{{this.diff_price}}" rowspan="2">
                                        @{{this.diff_price}}
                                    </td>
                                    <td class="text-middle" title="@{{this.occur_reason}}" rowspan="2">
                                        <div class="col-md-10 col-xs-10 clear-padding-all">
                                            <select id="occur_reason" class="form-control input-sm" onchange="saveData(this, '@{{this.received_order_id}}', 'occur_reason')" style="margin-top: 5px;">
                                                <option value="">----</option>
                                                @{{{this.optionOccurReason}}}
                                            </select>
                                        </div>
                                        <div class="col-md-2 col-xs-2 clear-padding-all">
                                            <button type="button" name="edit" class="btn btn-box-tool btn-save"
                                            onclick="addNewOption('occur_reason', '@{{this.received_order_id}}')" style="margin-top: 5px;">
                                                <i class="fa fa-plus"></i>
                                            </button>
                                        </div>
                                    </td>
                                    <td class="text-middle" title="@{{this.remarks}}" rowspan="2">
                                        <div class="col-md-10 col-xs-10 clear-padding-all" >
                                            <select id="remarks" class="form-control input-sm" onchange="saveData(this, '@{{this.received_order_id}}', 'remarks')">
                                                <option value="">----</option>
                                                @{{{this.optionRemarks}}}
                                            </select>
                                        </div>
                                        <div class="col-md-2 col-xs-2 clear-padding-all">
                                            <button type="button" name="edit" class="btn btn-box-tool btn-save"
                                            onclick="addNewOption('remarks', '@{{this.received_order_id}}')" style="margin-top: 5px;">
                                                <i class="fa fa-plus"></i>
                                            </button>
                                        </div>
                                    </td>
                                    <td class="text-right text-middle" title="@{{this.finished_date}}">
                                        @{{this.finished_date}}
                                    </td>
                                </tr>
                                <tr class="@{{this.class_row}}">
                                    <td class="cut-text text-middle text-center">
                                        <a target="_blank" href="@{{this.link_order_web}}">
                                            <img src="/img/uri_img_web_detail.png" width="29" />
                                        </a>
                                    </td>
                                    <td class="hidden-xs hidden-sm" title="@{{this.delivery_date}}">
                                        @{{this.delivery_date}}
                                    </td>
                                    <td class="hidden-xs hidden-sm" title="@{{this.payment_name}}">
                                        @{{this.payment_name}}
                                    </td>
                                    <td class="hidden-xs hidden-sm text-right" title="@{{this.order_status}}">
                                        @{{this.order_status}}
                                    </td>
                                    <td class="hidden-xs hidden-sm text-right" title="@{{this.payment_date}}">
                                        @{{this.payment_date}}
                                    </td>
                                    <td class="hidden-xs hidden-sm text-right" title="@{{this.pay_charge_discount}}">
                                        @{{this.pay_charge_discount}}
                                    </td>
                                    <td class="hidden-xs hidden-sm text-right" title="@{{this.pay_after_charge}}">
                                        @{{this.pay_after_charge}}
                                    </td>
                                    <td class="hidden-xs hidden-sm text-right" title="@{{this.point_paid_date}}">
                                        @{{this.point_paid_date}}
                                    </td>
                                    <td class="hidden-xs hidden-sm text-right" title="@{{this.paid_point}}">
                                        @{{this.paid_point}}
                                    </td>
                                    <td class="hidden-xs hidden-sm text-right" title="@{{this.coupon_paid_date}}">
                                        @{{this.coupon_paid_date}}
                                    </td>
                                    <td class="hidden-xs hidden-sm text-right" title="@{{this.paid_coupon}}">
                                        @{{this.paid_coupon}}
                                    </td>
                                    <td class="hidden-xs hidden-sm text-right" title="@{{this.return_price}}">
                                        @{{this.return_price}}
                                    </td>
                                    <td class="hidden-xs hidden-sm text-right" title="@{{this.total_paid_price}}">
                                        @{{this.total_paid_price}}
                                    </td>
                                    <td class="text-center">
                                        @{{#if (eq this.is_corrected 0)}}
                                            <button class="btn btn-primary text-center" name="is_corrected" onclick="saveData(this, '@{{this.received_order_id}}', 'match')">{{__('messages.match_button')}}</button>
                                        @{{else}}
                                            <button class="btn btn-primary text-center" name="is_corrected" onclick="saveData(this, '@{{this.received_order_id}}', 'active')">{{__('messages.mapping_active')}}</button>
                                        @{{/if}}
                                    </td>
                                </tr>
                                <tr class="hiden-info" style="display: none">
                                    <td colSpan="10" class="width-span1">
                                        <ul>
                                            <li> <b>{{__('messages.payment_order_date')}}</b> : @{{this.order_date}}</li>
                                            <li> <b>{{__('messages.delivery_date')}}</b> : @{{this.delivery_date}}</li>
                                            <li> <b>{{__('messages.mall_name')}}</b> : @{{this.name_jp}}</li>
                                            <li> <b>{{__('messages.uri_payment_name')}}</b> : @{{this.payment_name}}</li>
                                            <li> <b>{{__('messages.pay_method_name')}}</b> : @{{this.payment_method}}</li>
                                            <li> <b>{{__('messages.status_name')}}</b> : @{{this.order_status}}</li>
                                            <li> <b>{{__('messages.payment_plan_date')}}</b> : @{{this.payment_plan_date}}</li>
                                            <li> <b>{{__('messages.uri_payment_date')}}</b> : @{{this.payment_date}}</li>
                                            <li> <b>{{__('messages.daito_total_price')}}</b> : @{{this.total_payment_plan}}</li>
                                            <li> <b>{{__('messages.ship_charge')}}</b> : @{{this.ship_charge}}</li>
                                            <li> <b>{{__('messages.pay_charge_discount')}}</b> : @{{this.pay_charge_discount}}</li>
                                            <li> <b>{{__('messages.pay_charge')}}</b> : @{{this.pay_charge}}</li>
                                            <li> <b>{{__('messages.pay_after_charge')}}</b> : @{{this.pay_after_charge}}</li>
                                            <li> <b>{{__('messages.lbl_point_paid_plan_date')}}</b> : @{{this.paid_point_plan_date}}</li>
                                            <li> <b>{{__('messages.lbl_point_paid_date')}}</b> : @{{this.point_paid_date}}</li>
                                            <li> <b>{{__('messages.uri_used_point')}}</b> : @{{this.used_point}}</li>
                                            <li> <b>{{__('messages.uri_paid_point')}}</b> : @{{this.paid_point}}</li>
                                            <li> <b>{{__('messages.lbl_coupon_paid_plan_date')}}</b> : @{{this.coupon_paid_plan_date}}</li>
                                            <li> <b>{{__('messages.lbl_coupon_paid_date')}}</b> : @{{this.coupon_paid_date}}</li>
                                            <li> <b>{{__('messages.uri_used_coupon')}}</b> : @{{this.used_coupon}}</li>
                                            <li> <b>{{__('messages.uri_paid_coupon')}}</b> : @{{this.paid_coupon}}</li>
                                            <li> <b>{{__('messages.request_price')}}</b> : @{{this.request_price}}</li>
                                            <li> <b>{{__('messages.uri_return_price')}}</b> : @{{this.return_price}}</li>
                                            <li> <b>{{__('messages.paid_request_price')}}</b> : @{{this.paid_request_price}}</li>
                                            <li> <b>{{__('messages.uri_total_paid_price')}}</b> : @{{this.total_paid_price}}</li>
                                            <li> <b>{{__('messages.uri_diff_price')}}</b> : @{{this.diff_price}}</li>
                                            <li> <b>{{__('messages.note')}}</b> : @{{this.remarks}}</li>
                                        </ul>
                                    </td>
                                </tr>
                            @{{/each}}
                        @{{else}}
                            <tr><td colSpan="17" class="text-center">{{__('messages.no_data')}}</td></tr>
                        @{{/if}}
                    </tbody>
                </table>
            </div>
            @{{#if (gt pageData.last_page 1)}}
            <div class="text-center paginator-content"></div>
            @{{/if}}
        </div>
    </div>
</script>
@include('includes.paginate');
<script>
    var ignoreKey = [];
    function initial () {
        $('table select').each(function(index, item) {
            $(item).comboSelect();
        });
    }
    var datalist = new DataList2("{{action('UriMappingController@dataList')}}", ignoreKey, {});
    function processData(_this, response) {
        var nf = new Intl.NumberFormat();
        response.data.data.map((item, index) => {
            item['class_row'] = (index % 2 === 0) ? 'odd' : 'even';
            item['disable']   = '';
            if (item['is_corrected'] === 1) {
                item['class_row'] = 'color-mapping-status';
                item['disable']   = 'disabled';
            }
        });
    }
    function search() {
        datalist.search(processData);
    }
    function handleClickPage(page = 1) {
        datalist.handleClickPage(page, processData);
    }
    function clearSearch() {
        datalist.clearSearch(processData);
    }
    function handleChangeFile() {
        const target = event.target;
        if (target.files !== null && target.files.length !== 0) {
            const name = target.name;
            let tmpName = name + '_path';
            document.getElementsByName(tmpName)[0].value = target.files[0].name;
        }
    }
    $(document).ready(function() {
        $("#main-content").show();
        var arrHref = window.location.href.split('?');
        var dataPost = '';
        if (arrHref.length > 1) {
            dataPost = arrHref[1];
        }
        datalist.reloadData(dataPost, processData);
    });
    $('#name_jp').multiselect({
        buttonWidth: '100%',
        nonSelectedText: '',
        includeSelectAllOption: true,
        selectAllText:'{{__('messages.select_all')}}'
    });
    $('#payment_method').multiselect({
        buttonWidth: '100%',
        nonSelectedText: '',
        includeSelectAllOption: true,
        selectAllText:'{{__('messages.select_all')}}'
    });

    $(document).ready(function() {
        @php
            $name_jp = Request::input('name_jp');
        @endphp
        @if (!empty($name_jp))
            $('#name_jp').multiselect('deselectAll', false);
            @if (is_array($name_jp))
                @foreach ($name_jp as $item)
                    $('#name_jp').multiselect('select', {{ $item }});
                @endforeach
            @else
                $('#name_jp').multiselect('select', {{ $name_jp }});
            @endif
        @endif

        @php
            $payment_method = Request::input('payment_method');
        @endphp
        @if (!empty($payment_method))
            $('#payment_method').multiselect('deselectAll', false);
            @if (is_array($payment_method))
                @foreach ($payment_method as $item)
                    $('#payment_method').multiselect('select', {{ $item }});
                @endforeach
            @else
                $('#payment_method').multiselect('select', {{ $payment_method }});
            @endif
        @endif
    })
    $('#order_date_to').datepicker({
        format: "yyyy-mm-dd",
        todayHighlight: true,
        autoclose: true,
        language: 'ja'
    });
    $('#order_date_from').datepicker({
        format: "yyyy-mm-dd",
        todayHighlight: true,
        autoclose: true,
        language: 'ja'
    });
    $('#delivery_date_to').datepicker({
        format: "yyyy-mm-dd",
        todayHighlight: true,
        autoclose: true,
        language: 'ja'
    });
    $('#delivery_date_from').datepicker({
        format: "yyyy-mm-dd",
        todayHighlight: true,
        autoclose: true,
        language: 'ja'
    });
    $('#add_file').click(function() {
        document.getElementsByName("attached_file")[0].click();
    });
    $('#form-csv').submit(function(event) {
        $(".loading").show();
        event.preventDefault();
        let file = $('input[name=attached_file_path]').val();
        $('#form-group-field').removeClass('has-error');
        $('span[class=help-block]').remove();
        $.ajax({
            url:"{{action('Common@importCSV')}}",
            method:"POST",
            dataType:'JSON',
            contentType: false,
            data: new FormData($(this)[0]),
            processData: false,
            success:function(data) {
                if (data.flg === 0) {
                    $(".loading").hide();
                    $('#form-group-field').addClass('has-error');
                    $('#form-field').append('<span class="help-block"><strong class="error-new-line">' + data.msg +'</strong></span>');
                    return;
                }
                if (!$('#csv-modal .modal-content').hasClass('collapse')) {
                    $('#csv-modal .modal-content').addClass('hidden-div-csv');
                }
                processUploadCsv(data);
            }
       })

    });

    function processUploadCsv (data) {
        let formData   = new FormData();
        Object.keys(data).map((item) => {
            formData.append(item, data[item]);
        });
        formData.append('type', $('#action-modal').val());
        formData.append('_token', $('input[name=_token]').val());
        $.ajax({
            url:"{{action('UriMappingController@import')}}",
            method:"POST",
            dataType:'JSON',
            contentType: false,
            data: formData,
            processData: false,
            success:function(data) {
                if (data.flg === 0) {
                    if (data.msg === 'Finish') {
                        $('.progress-bar-csv .active').css('width', '100%' );
                        $(".loading").hide();
                        $.notify({
                            icon: 'glyphicon glyphicon-warning-sign',
                            message: 'Import success!!!',
                        },{
                            type: 'success',
                            delay: 2000
                        });
                        if (data.error == 0) {
                            $("#csv-modal").modal('hide');
                        } else {
                            $('#csv-modal .link-error').removeClass('collapse');
                            $('#csv-modal .modal-content').removeClass('hidden-div-csv');
                            $('#csv-modal .form-csv').addClass('collapse');
                            $('.progress-bar-csv').addClass('collapse');
                            $("#csv-modal .link-error a").attr("href", '/downloaderror/' + data.filename);
                        }
                        processAfterImport($('#action-modal').val());
                    } else {
                        $(".loading").hide();
                        let messages = data.msg;
                        if (messages == false) {
                            if ($('#action-modal').val() === 'amazon_pay_csv') {
                                messages = 'Data settlement and total is error OR File is Error.'
                            } else {
                                messages = 'File is Error.'
                            }
                        }
                        $('#csv-modal .modal-content').removeClass('hidden-div-csv');
                        $('#form-group-field').addClass('has-error');
                        $('#form-field').append('<span class="help-block"><strong class="error-new-line">' + messages +'</strong></span>');
                        return;
                    }
                } else {
                    $('.progress-bar-csv').removeClass('collapse');
                    $('.progress-bar-csv .active').css('width', ((data.currPage/data.timeRun) * 100) + '%' );
                    processUploadCsv(data);
                }
            }
       })
    }

    function processAfterImport(action) {
        $(".loading").show();
        $.notify({
            icon: 'glyphicon glyphicon-warning-sign',
            message: 'Process data after import!!!',
        },{
            type: 'info',
            delay: 2000
        });
        let formData   = new FormData();
        formData.append('type', $('#action-modal').val());
        formData.append('_token', $('input[name=_token]').val());
        $.ajax({
            url:"{{action('UriMappingController@processDataImport')}}",
            method:"POST",
            dataType:'JSON',
            contentType: false,
            data: formData,
            processData: false,
            success:function(data) {
                if (data.status === 1) {
                    $.notify({
                        icon: 'glyphicon glyphicon-warning-sign',
                        message: 'Added process into queue success.!!!',
                    },{
                        type: 'success',
                        delay: 2000
                    });
                } else {
                    $.notify({
                        icon: 'glyphicon glyphicon-warning-sign',
                        message: data.error,
                    },{
                        type: 'info',
                        delay: 2000
                    });
                }
                $(".loading").hide();
                $("#main-content").show();
                var arrHref = window.location.href.split('?');
                var dataPost = '';
                if (arrHref.length > 1) {
                    dataPost = arrHref[1];
                }
                datalist.reloadData(dataPost, processData);
            }
       })
    }

    function exportExcel () {
        let param = $('#search-form').serialize();
        window.location.href = "{{action('UriMappingController@processExportExcel')}}" + '?' + param;
     }

    $('#csv-modal').on('show.bs.modal', function (e) {
        var $trigger = $(e.relatedTarget);
        $('#action-modal').val($trigger.data('button'));
        if (!$('.progress-bar-csv').hasClass('collapse')) {
            $('.progress-bar-csv').addClass('collapse');
        }
        if (!$('#csv-modal .link-error').hasClass('collapse')) {
            $('#csv-modal .link-error').addClass('collapse');
        }
        $('#csv-modal .modal-content').removeClass('hidden-div-csv');
        $('#csv-modal .form-csv').removeClass('collapse');
        $('.progress-bar-csv .active').css('width', '' );
    });

    $('#csv-modal').on('hidden.bs.modal', function () {
        $("input[name='attached_file_path']").val('');
        $("input[name='attached_file']").val('');
        $('#form-group-field').removeClass('has-error');
        $('span[class=help-block]').remove();
        $('#action-modal').val('');
    })
    $('#mapping-menu').addClass('active');
    $('#uri-mapping-menu').addClass('active');

    function saveData (_this, received_order_id, name) {
        let param = {
            'received_order_id' : received_order_id,
            'name' : name,
            '_token' : $('input[name=_token]').val()
        };
        let value = '';
        if (name === 'occur_reason' || name === 'remarks') {
            param.value = $(_this).val();
        }
        $.ajax({
            url:"{{action('UriMappingController@processSaveData')}}",
            method:"POST",
            data: param,
            dataType: "json",
            success:function(data) {
                if (data.status === 1) {
                    var arrHref = window.location.href.split('?');
                    var dataPost = '';
                    if (arrHref.length > 1) {
                        dataPost = arrHref[1];
                    }
                    datalist.reloadData(dataPost, processData);
                }
            }
       })
    }

    function saveDataAll() {
        $('.loading').show();
        var arrHref = window.location.href.split('?');
        var dataPost = '';
        if (arrHref.length > 1) {
            dataPost = arrHref[1];
            dataPost += '&';
        }
        dataPost += $('#popup-form').serialize() + '&';

        dataPost += '_token=' + $('input[name=_token]').val();
        dataPost += '&flg_update_all=1';
        $.ajax({
            url:"{{action('UriMappingController@dataList')}}",
            method:"POST",
            data: dataPost,
            dataPost: "json",
            success:function(data) {
                $('.loading').hide();
                if (data.status === 1) {
                    var arrHref = window.location.href.split('?');
                    var dataPost = '';
                    if (arrHref.length > 1) {
                        dataPost = arrHref[1];
                    }
                    $('#access-memo-modal').modal('hide');
                    datalist.reloadData(dataPost, processData);
                }
            }
       })
    }

    function saveDataSelectAll (_this, name) {
        $('#access-memo-modal #name').val(name);
        if (name === 'occur_reason' || name === 'remarks') {
            value = $(_this).val();
            $('#access-memo-modal #value').val(value);
        }
        $('#access-memo-modal').modal('show');
    }

    function addNewOption(name, condition) {
        if (name === 'occur_reason') {
            $('.field-name').text("{{__('messages.occur_reason')}}");
        } else {
            $('.field-name').text("{{__('messages.remarks_uri')}}");
        }
        $('#add-new-modal #name').val(name);
        $('#add-new-modal #condition').val(condition);
        $('#add-new-modal').modal('show');
    }

    function saveDataOption() {
        $('.loading').show();
        var arrHref = window.location.href.split('?');
        var dataPost = '';
        if (arrHref.length > 1) {
            dataPost = arrHref[1];
            dataPost += '&';
        }
        dataPost += $('#add-popup-form').serialize() + '&';

        dataPost += '_token=' + $('input[name=_token]').val();
        $.ajax({
            url:"{{action('UriMappingController@processSaveOption')}}",
            method:"POST",
            data: dataPost,
            dataPost: "json",
            success:function(data) {
                $('.loading').hide();
                if (data.status === 1) {
                    var arrHref = window.location.href.split('?');
                    var dataPost = '';
                    if (arrHref.length > 1) {
                        dataPost = arrHref[1];
                    }
                    $('#add-new-modal').modal('hide');
                    datalist.reloadData(dataPost, processData);
                } else {
                    $('.parent-input').addClass('has-error');
                    $('.parent-input div').addClass('has-feedback');
                }
            }
       })
    }

    $('#access-memo-modal').on('hidden.bs.modal', function () {
        $('#access-memo-modal #name').val('');
        $('#access-memo-modal #value').val('');
        $(".select-all-reset").prop('selectedIndex',0);
    })

    $('#add-new-modal').on('hidden.bs.modal', function () {
        $('#add-new-modal #name').val('');
        $('#add-new-modal input[name=value-input]').val('');
        $('#add-new-modal #condition').val('');
        $('.parent-input').removeClass('has-error');
        $('.parent-input div').removeClass('has-feedback');
    })
</script>
@endsection
