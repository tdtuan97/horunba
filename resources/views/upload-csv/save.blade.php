@extends('layouts.main')
@section('titlePage', __('messages.edit_send_mail_save_header'))
@section('content')
<section class="content-header">
    <h1>
        {{__('messages.edit_send_mail_save_header')}}
        <small></small>
    </h1>
    <ol class="breadcrumb">
        <li>
            <a href="{{action('DashboardController@index')}}">
                <i class="fa fa-dashboard"></i> {{__('messages.dashboard_index_breadcrumb_title')}}
            </a>
        </li>
        <li class="active">{{__('messages.save_action')}}</li>
    </ol>
</section>

<section class="content">
    <!-- Put your code here -->
    <div id="root"></div>
</section>
@endsection
@section('javascript')
<script>
    var saveUrl            = "{{action('UploadCsvController@save')}}";
    var previousUrl        = "{{url()->previous()}}";
</script>
@endsection