@extends('layouts.main')
@section('titlePage', __('messages.mail_template_header'))
@section('content')
<section class="content-header">
    <h1>
        {{__('messages.mail_template_header')}}
        <small>{{__('messages.mail_template_sub_header')}}</small>
    </h1>
    <ol class="breadcrumb">
        <li>
            <a href="{{action('MailTemplateController@index')}}">{{__('messages.mail_template_breadcrumb_title')}}</a>
        </li>
    </ol>
</section>

<section class="content">
    <!-- Put your code here -->
    <div id="root"></div>
</section>
@endsection
@section('javascript')
<script>
    var baseUrl     = "{{action('MailTemplateController@index')}}";
    var saveUrl     = "{{action('MailTemplateController@save')}}";
    var dataListUrl = "{{action('MailTemplateController@dataList')}}";
    var detailUrl   = "{{action('MailTemplateController@detail')}}";
    $('#system-setting-menu').addClass('active');
    $('#mail-template-menu').addClass('active');
</script>
@endsection