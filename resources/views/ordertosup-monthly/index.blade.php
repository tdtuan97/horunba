@extends('layouts.main')
@section('titlePage', __('messages.order2supmonthly_management_header'))
@section('content')
<section class="content-header">
    <h1>
        {{__('messages.order2supmonthly_management_header')}}
    </h1>
    <ol class="breadcrumb">
        <li>
            <a href="{{action('OrderToSupplierMonthlyController@index')}}">
                {{__('messages.order2supmonthly_management_breadcrumb_title')}}
            </a>
        </li>
    </ol>
</section>
<div class="popup-error fix-message collapse" id="popup-status">
    <div class="alert alert-dismissible alert-success">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <h4><i class="icon fa fa-check"></i> Alert!</h4>
        <p class="text-new-line" id="popup-message"></p>
    </div>
</div>
<section class="content">
    <!-- Put your code here -->
    <div id="root"></div>
</section>
@endsection
@section('javascript')
<script>
    var baseUrl     = "{{action('OrderToSupplierMonthlyController@index')}}";
    var dataListUrl = "{{action('OrderToSupplierMonthlyController@dataList')}}";
    var importUrl   = "{{action('OrderToSupplierMonthlyController@import')}}";
    var saveUrl     = "{{action('OrderToSupplierMonthlyController@save')}}";
    var O2SUrl      = "{{action('OrderToSupplierMonthlyController@processOrderToSupplier')}}";
    var updateUrl   = "{{action('OrderToSupplierMonthlyController@updateByCheckBox')}}";
    var csvUrl      = "{{action('Common@importCSV')}}";
    $('#order2supmonthly-management-menu').addClass('active');
</script>
@endsection