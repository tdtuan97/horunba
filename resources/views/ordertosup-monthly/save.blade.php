@extends('layouts.main')
@section('titlePage', __('messages.order2supmonthly_management_save_header'))
@section('content')
<section class="content-header">
    <h1>
        {{__('messages.order2supmonthly_management_save_header')}}
        <small></small>
    </h1>
    <ol class="breadcrumb">
        <li>
            <a href="{{action('OrderToSupplierMonthlyController@index')}}">
                {{__('messages.order2supmonthly_management_breadcrumb_title')}}
            </a>
        </li>
        <li class="active">
            <a href="{{Request::fullUrl()}}">
                {{__('messages.order2supmonthly_management_save_header')}}
            </a>        
        </li>
    </ol>
</section>

<section class="content">
    <!-- Put your code here -->
    <div id="root"></div>
</section>
@endsection
@section('javascript')
<script>
    var baseUrl           = "{{action('OrderToSupplierMonthlyController@index')}}";
    var saveUrl           = "{{action('OrderToSupplierMonthlyController@save')}}";
    var getDataProductUrl = "{{action('OrderToSupplierMonthlyController@getDataProduct')}}";
    var getDataSupplierUrl = "{{action('OrderToSupplierMonthlyController@getDataSupplier')}}";
    var checkEditUrl      = "{{action('DashboardController@checkItemEdit')}}";
    $('#order2supmonthly-management-menu').addClass('active');
</script>
@endsection