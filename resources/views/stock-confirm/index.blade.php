@extends('layouts.main')
@section('titlePage', __('messages.receive_mail_header'))
@section('content')
<section class="content-header">
    <h1>
        {{__('messages.stock_confirm_header')}}
    </h1>
    <ol class="breadcrumb">
        <li>
            <a href="{{action('StockConfirmController@index')}}">
                {{__('messages.stock_confirm_header')}}
            </a>
        </li>
    </ol>
</section>

<section class="content">
    <!-- Put your code here -->
    <div id="root"></div>
</section>
@endsection
@section('javascript')
<script>
    var baseUrl          = "{{action('StockConfirmController@index')}}";
    var dataListUrl      = "{{action('StockConfirmController@dataList')}}";
    var orderDetailUrl   = "{{action('OrderManagementController@detail')}}";
    var exportCSV        = "{{action('Common@exportCSV')}}";
    var processExportCSV = "{{action('Common@processExportCSV')}}";
    $('#stock-management-menu').addClass('active');
</script>
@endsection