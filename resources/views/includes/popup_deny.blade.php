<script id="deny-template" type="text/x-handlebars-template">
<div class="modal fade" id="message-modal-deny" data-backdrop="static" data-keyboard="false" style="display: none;">
    <div class="modal-dialog modal-custom">
        <div class="modal-content modal-custom-content">
            <div class="modal-header">
                <h4 class="modal-title text-center"><b>{{__('messages.deny_edit')}}</b></h4>
            </div>
            <div class="modal-body modal-custom-body">
                @{{message}}
            </div>
            <div class="modal-footer modal-custom-footer">
                <a href="@{{url}}" type="button" class="btn bg-purple modal-custom-button"  >{{__('messages.back')}}</a>
            </div>
        </div>
    </div>
</div>
</script>