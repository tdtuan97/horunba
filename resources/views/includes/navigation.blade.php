<aside class="main-sidebar">
    <section class="sidebar">
        <ul class="sidebar-menu" data-widget="tree">
            @if(Helper::checkPermission('OrderManagementController'))
            <li id="order-management-menu">
                <a href="{{route('order.info.list')}}">
                    <i class="fa fa-file-text-o"></i><span>{{__('messages.menu.order_management')}}</span>
                </a>
            </li>
            @endif
            @if(Helper::checkPermission('ReceiveMailController'))
            <li id="receive-mail-menu">
                <a href="{{route('receive.mail.list')}}">
                    <i class="fa fa-file-text-o"></i><span>{{__('messages.menu.receive_mail')}}</span>
                </a>
            </li>
            @endif
            @if(Helper::checkPermission('RefundDetailController'))
            <li id="refund-menu">
                <a href="{{route('refundDetail.save')}}">
                    <i class="fa fa-file-text-o"></i><span>{{__('messages.menu.refund')}}</span>
                </a>
            </li>
            @endif
            @if(Helper::checkPermission('CancelReservationController'))
            <li id="cancel-reservation-menu">
                <a href="{{route('cancel.reservation.list')}}">
                    <i class="fa fa-file-text-o"></i><span>{{__('messages.menu.system_setting.cancel_reservation_list')}}</span>
                </a>
            </li>
            @endif
            @if(Helper::checkPermission('PaymentController') ||
                Helper::checkPermission('RePaymentController') ||
                Helper::checkPermission('NotPaymentListController'))
            <li class="treeview" id="payment-menu">
                <a href="#">
                    <i class="fa fa-credit-card"></i>
                    <span>{{__('messages.menu.payment_management.payment_parent')}}</span>
                    <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu">
                    @if(Helper::checkPermission('PaymentController'))
                    <li id="payment-info-list-menu">
                        <a href={{route('payment.info.list')}}>
                            ◎ <span>{{__('messages.menu.payment_management.payment_info_list')}}</span>
                        </a>
                    </li>
                    @endif
                    @if(Helper::checkPermission('PaymentController'))
                    <li id="payment-unknown-list-menu">
                        <a href={{route('payment.unknown.list')}}>
                            ◎ <span>{{__('messages.menu.payment_management.payment_unkown_list')}}</span>
                        </a>
                    </li>
                    @endif
                    @if(Helper::checkPermission('RePaymentController'))
                    <li id="repayment-list-menu">
                        <a href={{route('repayment.list')}}>
                            ◎ <span>{{__('messages.menu.payment_management.repayment_list')}}</span>
                        </a>
                    </li>
                    @endif
                    @if(Helper::checkPermission('NotPaymentListController'))
                    <li id="not-payment-list-menu">
                        <a href={{route('not_payment_list.list')}}>
                            ◎ <span>{{__('messages.menu.payment_management.not_payment_list')}}</span>
                        </a>
                    </li>
                    @endif
                </ul>
            </li>
            @endif
            @if(Helper::checkPermission('OrderToSupplierController'))
            <li id="order-to-supplier-menu">
                <a href="{{route('orderToSupplier.list')}}">
                    <i class="fa fa-file-text-o"></i><span>{{__('messages.menu.order_to_supplier_management')}}</span>
                </a>
            </li>
            @endif
            @if(Helper::checkPermission('StockConfirmController'))
            <li id="stock-management-menu">
                <a href="{{route('stock.confirm.list')}}">
                    <i class="fa fa-file-text-o"></i><span>{{__('messages.menu.stock_management')}}</span>
                </a>
            </li>
            @endif
            @if(Helper::checkPermission('DeliveryController'))
            <li id="delivery-menu">
                <a href="{{route('delivery.list')}}">
                    <i class="fa fa-file-text-o"></i><span>{{__('messages.menu.delivery_management')}}</span>
                </a>
            </li>
            @endif
            @if(Helper::checkPermission('OrderToSupplierMonthlyController'))
            <li id="order2supmonthly-management-menu">
                <a href="{{route('order2supmonthly.list')}}">
                    <i class="fa fa-file-text-o"></i><span>{{__('messages.menu.order2supmonthly_management')}}</span>
                </a>
            </li>
            @endif
            @if(Helper::checkPermission('ReturnManagementController'))
            <li id="return-management-menu">
                <a href="{{route('return.list')}}">
                    <i class="fa fa-file-text-o"></i><span>{{__('messages.menu.return_management')}}</span>
                </a>
            </li>
            @if(Helper::checkPermission('StockStatusController'))
            <li id="stock-status-management-menu">
                <a href="{{route('stock.status.list')}}">
                    <i class="fa fa-file-text-o"></i><span>{{__('messages.menu.stock_status_management')}}</span>
                </a>
            </li>
            @endif
            @endif
            @if(Helper::checkPermission('StoreOrderController') || Helper::checkPermission('StoreProductController'))
            <li class="treeview" id="store-menu">
                <a href="#">
                    <i class="fa fa-credit-card"></i>
                    <span>{{__('messages.menu.store_manage')}}</span>
                    <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu">
                    @if(Helper::checkPermission('StoreOrderController'))
                    <li id="store-order-menu">
                        <a href="{{route('store.order.list')}}">
                            <i class="fa fa-file-text-o"></i><span>{{__('messages.menu.store_order')}}</span>
                        </a>
                    </li>
                    @endif
                    @if(Helper::checkPermission('StoreProductController'))
                    <li id="store-product-menu">
                        <a href="{{route('store.product.list')}}">
                            <i class="fa fa-file-text-o"></i><span>{{__('messages.menu.store_product')}}</span>
                        </a>
                    </li>
                    @endif
                </ul>
            </li>
            @endif

            @if(Helper::checkPermission('MembershipController') || Helper::checkPermission('LessonController'))
            <li class="treeview" id="studiy-member-menu">
                <a href="#">
                    <i class="fa fa-credit-card"></i>
                    <span>{{__('messages.menu.studiy_member_manage')}}</span>
                    <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu">
                    @if(Helper::checkPermission('MembershipController'))
                    <li id="membership-menu">
                        <a href="{{route('membership.list')}}">
                            <i class="fa fa-file-text-o"></i><span>{{__('messages.menu.membership_manage')}}</span>
                        </a>
                    </li>
                    @endif
                    @if(Helper::checkPermission('LessonController'))
                    <li id="lesson-menu">
                        <a href="{{route('lesson.list')}}">
                            <i class="fa fa-file-text-o"></i><span>{{__('messages.menu.lesson_manage')}}</span>
                        </a>
                    </li>
                    @endif
                </ul>
            </li>
            @endif

            @if(Helper::checkPermission('WebMappingController') || Helper::checkPermission('SettlementMappingController'))
            <li class="treeview" id="mapping-menu">
                <a href="#">
                    <i class="fa fa-credit-card"></i>
                    <span>{{__('messages.menu.mapping_manage')}}</span>
                    <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu">
                    @if(Helper::checkPermission('WebMappingController'))
                    <li id="web-mapping-menu">
                        <a href="{{route('web.mapping.list')}}">
                            <i class="fa fa-file-text-o"></i><span>{{__('messages.menu.web_mapping')}}</span>
                        </a>
                    </li>
                    @endif
                    <!-- @if(Helper::checkPermission('SettlementMappingController'))
                    <li id="settlement-mapping-menu">
                        <a href="{{route('settlement.mapping.list')}}">
                            <i class="fa fa-file-text-o"></i><span>{{__('messages.menu.settlement_mapping')}}</span>
                        </a>
                    </li>
                    @endif -->
                    @if(Helper::checkPermission('UriMappingController'))
                    <li id="uri-mapping-menu">
                        <a href="{{route('uri.mapping.list')}}">
                            <i class="fa fa-file-text-o"></i><span>{{__('messages.menu.uri_mapping')}}</span>
                        </a>
                    </li>
                    @endif
                </ul>
            </li>
            @endif
            @if(Helper::checkPermission('ReceiptController'))
            <li id="receipt-management-menu">
                <a href="{{route('receipt.list')}}">
                    <i class="fa fa-file-text-o"></i><span>{{__('messages.menu.receipt_management')}}</span>
                </a>
            </li>
            @endif
            @if(Helper::checkPermission('DejeController'))
            <li id="deje-menu">
                <a href="{{route('deje.list')}}">
                    <i class="fa fa-file-text-o"></i><span>{{__('messages.menu.deje_management')}}</span>
                </a>
            </li>
            @endif
            @if(Helper::checkPermission('GenreController') ||
                Helper::checkPermission('MailTemplateController') ||
                Helper::checkPermission('SendMailTimingController') ||
                Helper::checkPermission('CancelReasonController') ||
                Helper::checkPermission('SettlementController') ||
                Helper::checkPermission('ShippingFreeBelowLimitController') ||
                Helper::checkPermission('PostalController') ||
                Helper::checkPermission('ReturnAddressController') ||
                Helper::checkPermission('UsersManagementController'))
            <li class="treeview" id="system-setting-menu">
                <a href="#">
                    <i class="fa fa-cog"></i><span>{{__('messages.menu.system_setting.system_setting')}}</span>
                    <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu">
                    @if(Helper::checkPermission('GenreController'))
                    <li id="genre-menu">
                        <a href="{{route('genre')}}">
                            ◎ <span>{{__('messages.menu.system_setting.genre')}}</span>
                        </a>
                    </li>
                    @endif
                    @if(Helper::checkPermission('MailTemplateController'))
                    <li id="mail-template-menu">
                        <a href="{{route('mail.template')}}">
                            ◎ <span>{{__('messages.menu.system_setting.mail_template')}}</span>
                        </a>
                    </li>
                    @endif
                    <!--
                    <li id="order-status-menu">
                        <a href="/settings/order-status">
                            ◎ <span>{{__('messages.menu.system_setting.order_status')}}</span>
                        </a>
                    </li> -->
                    <!--
                    <li id="send-mail-menu">
                        <a href="{{route('send.mail')}}">
                            ◎ <span>{{__('messages.menu.system_setting.send_mail')}}</span>
                        </a>
                    </li>-->
                    @if(Helper::checkPermission('SendMailTimingController'))
                    <li id="auto-send-mail-timing-menu">
                        <a href="{{route('mail.timing')}}">
                            ◎ <span>{{__('messages.menu.system_setting.auto_send_mail_timing')}}</span>
                        </a>
                    </li>
                    @endif
                    @if(Helper::checkPermission('CancelReasonController'))
                    <li id="cancel-reason-menu">
                        <a href="{{route('cancel.reason.list')}}">
                            ◎ <span>{{__('messages.menu.system_setting.cancel_reason_list')}}</span>
                        </a>
                    </li>
                    @endif
                    @if(Helper::checkPermission('SettlementController'))
                    <li id="settlement-list-menu">
                        <a href="{{route('settlement')}}">
                            ◎ <span>{{__('messages.menu.system_setting.settlement_list')}}</span>
                        </a>
                    </li>
                    @endif
                    @if(Helper::checkPermission('ShippingFreeBelowLimitController'))
                    <li id="shipping-free-below-limit-menu">
                        <a href="{{route('shipping.list')}}">
                            ◎ <span>{{__('messages.menu.system_setting.shipping_free_below_limit_list')}}</span>
                        </a>
                    </li>
                    @endif
                    @if(Helper::checkPermission('PostalController'))
                    <li id="postal-menu">
                        <a href="{{route('postal.list')}}">
                            ◎ <span>{{__('messages.menu.system_setting.postal_list')}}</span>
                        </a>
                    </li>
                    @endif
                    @if(Helper::checkPermission('ReturnAddressController'))
                    <li id="return-address-menu">
                        <a href="{{route('return.address.list')}}">
                            ◎ <span>{{__('messages.menu.system_setting.return_address')}}</span>
                        </a>
                    </li>
                    @endif
                    @if(Helper::checkPermission('UsersManagementController'))
                    <li id="users-management-menu">
                        <a href="{{route('users.list')}}">
                            ◎ <span>{{__('messages.menu.system_setting.users_management')}}</span>
                        </a>
                    </li>
                    @endif
                    @if(Helper::checkPermission('PermissionController'))
                    <li id="permission-menu">
                        <a href="{{route('permission.list')}}">
                            ◎ <span>{{__('messages.menu.system_setting.permission')}}</span>
                        </a>
                    </li>
                    @endif
                    @if(Helper::checkPermission('ControlController'))
                    <li id="controller-menu">
                        <a href="{{route('controller.list')}}">
                            ◎ <span>{{__('messages.menu.system_setting.controller')}}</span>
                        </a>
                    </li>
                    @endif
                </ul>
            </li>
            @endif
            @if(Helper::checkPermission('BatchManagementController'))
            <li id="batch-status-management-menu">
                <a href="{{route('batch')}}">
                    <i class="fa fa-cog"></i><span>{{__('messages.menu.batch_management')}}</span>
                </a>
            </li>
            @endif
                @if(Helper::checkPermission('CustomerController'))
                    <li id="customer-menu">
                        <a href="{{route('customer.list')}}">
                            <i class="fa fa-file-text-o"></i><span>Customer</span>
                        </a>
                    </li>
                @endif
        </ul>
    </section>
</aside>