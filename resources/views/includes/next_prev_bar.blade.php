@if ($indexUrl !== null)
<a style="float:left" href="{{$indexUrl}}">
    <i class="fa fa-arrow-left fa-2x"></i>
</a>
@endif
@if ($prevUrl !== null)
<a style="margin-right: 10px" href="{{$prevUrl}}">
    <i class="fa fa-angle-double-left fa-2x"></i>
</a>
@else
<a class="link-disabled" style="margin-right: 10px" href="javascript:void(0);">
    <i class="fa fa-angle-double-left fa-2x"></i>
</a>
@endif
<span style="font-size:20px; margin-right: 10px ; vertical-align: text-bottom;">{{$pageOnPages}}</span>
@if ($nextUrl !== null)
<a href="{{$nextUrl}}">
    <i class="fa fa-angle-double-right fa-2x"></i>
</a>
@else
<a class="link-disabled" href="javascript:void(0);">
    <i class="fa fa-angle-double-right fa-2x"></i>
</a>
@endif
