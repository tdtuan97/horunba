<script id="paginator-template" type="text/x-handlebars-template">
    <ul class="pagination pagination-fix">
        <li class="@{{classDisL}}">
            <a onclick="@{{#if (eq classDisL "disabled")}}return false;@{{else}}handleClickPage(1)@{{/if}}" >
                <span class="first-p">{{__('messages.first_p')}}</span>
            </a>
        </li>
        <li class="@{{classDisL}}">
            <a onclick="@{{#if (eq classDisL "disabled")}}return false;@{{else}}handleClickPage(@{{prevPage}})@{{/if}}" >
                <span class="prev-p">&lt;</span>
            </a>
        </li>
        @{{#each pages}}
            <li class="@{{#if (eq this ../currentPage)}}active@{{/if}}">
                <a class="page-item" onclick="@{{#if (eq this ../currentPage)}}return false;@{{else}}handleClickPage(@{{this}})@{{/if}}">@{{this}}</a>
            </li>
        @{{/each}}
        <li class="@{{classDisR}}">
            <a  onclick="@{{#if (eq classDisR "disabled")}}return false;@{{else}}handleClickPage(@{{nextPage}})@{{/if}}" >
                <span class="next-p">&gt;</span>
            </a>
        </li>
        <li class="@{{classDisR}}">
            <a onclick="@{{#if (eq classDisR "disabled")}}return false;@{{else}}handleClickPage(@{{totalPages}})@{{/if}}" >
                <span class="last-p">{{__('messages.last_p')}}</span>
            </a>
        </li>
    </ul>
</script>