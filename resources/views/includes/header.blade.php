<header class="main-header">
    <a href="/" class="logo">
        <span class="logo-mini"><b>{{ __('messages.layouts_main_logo_mini') }}</b></span>
        <span class="logo-lg"><b>{{ __('messages.layouts_main_logo') }}</b></span>
    </a>
    <nav class="navbar navbar-static-top">
        <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
            <span class="sr-only">Toggle navigation</span>
        </a>
        <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
                @if (!empty($globalVar['expirDate']))
                    <li>
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" style="padding-bottom: 0px">
                            <span class="hidden-xs" style="color: white">
                                <marquee direction="left" bgcolor="red"><b>{{__('messages.layouts_notify_rakuten')}}({{$globalVar['expirDate']}})<b></marquee>
                            </span>
                        </a>
                    </li>
                @endif
                <li class="dropdown notifications-menu" id="notifications">
                    <a href="{{action('MemoCheckController@index')}}">
                      <i class="fa fa-bell-o"></i>
                        @if (!empty($globalVar['countDataNotify']))
                            <span class="label label-danger label-notify">{{$globalVar['countDataNotify']}}</span>
                        @else
                            <span class="label label-notify"></span>
                        @endif
                    </a>
                </li>
                <li class="dropdown user user-menu">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <span class="hidden-xs">{{__('messages.layouts_main_welcome_mess')}}<b>{{ Auth::user()->tantou_last_name }}</b></span>
                    </a>
                </li>
                <li class="dropdown user user-menu @if(!Helper::checkPermission('UploadCsvController')) hidden @endif" id="uploadCsv">
                    <a>{{__('messages.upload_csv')}}</a>
                </li>

                <li class="dropdown user user-menu">
                    <a href="{{ route('change-password') }}">
                        <span class="hidden-xs">{{__('messages.layouts_main_button_change_password')}}</span>
                    </a>
                </li>
                <li class="dropdown user user-menu">
                    <a href="{{ route('logout') }}">
                        <span class="hidden-xs">{{__('messages.layouts_main_button_logout')}}</span>
                    </a>
                </li>
            </ul>
        </div>
    </nav>
</header>