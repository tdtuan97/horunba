@extends('layouts.main')
@section('titlePage', __('messages.permission_header'))
@section('content')
<section class="content-header">
    <h1>
        {{__('messages.permission_header')}}
    </h1>
    <ol class="breadcrumb">
        <li>
            <a href="{{action('PermissionController@index')}}">{{__('messages.permission_breadcrumb_title')}}</a>
        </li>
    </ol>
</section>

<section class="content">
    <!-- Put your code here -->
    <div id="root"></div>
</section>
@endsection
@section('javascript')
<script>
    var baseUrl             = "{{action('PermissionController@index')}}";
    var dataListUrl         = "{{action('PermissionController@dataList')}}";
    var updatePermissionUrl = "{{action('PermissionController@updatePermission')}}";
    var saveCtrlUrl         = "{{action('PermissionController@saveController')}}";
    var savePerUrl          = "{{action('PermissionController@savePermission')}}";
    var deletePerUrl        = "{{action('PermissionController@deletePermission')}}";
    $('#system-setting-menu').addClass('active');
    $('#permission-menu').addClass('active');
</script>
@endsection