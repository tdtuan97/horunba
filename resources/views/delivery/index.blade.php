@extends('layouts.main')
@section('titlePage', __('messages.store_order_header'))
@section('content')
<section class="content-header">
    <h1>
        {{__('messages.delivery_header')}}
    </h1>
    <ol class="breadcrumb">
        <li>
            <a href="{{action('DeliveryController@index')}}">
                {{__('messages.delivery_header')}}
            </a>
        </li>
    </ol>
</section>

<section class="content">
    <!-- Put your code here -->
    <div id="root"></div>
</section>
@endsection
@section('javascript')
<script>
    var baseUrl     = "{{action('DeliveryController@index')}}";
    var dataListUrl = "{{action('DeliveryController@dataList')}}";
    var saveUrl   = "{{action('DeliveryController@save')}}";
    $('#delivery-menu').addClass('active');
  </script>
@endsection