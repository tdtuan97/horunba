@extends('layouts.main')
@section('titlePage', __('messages.receive_mail_save_header'))
@section('content')
<section class="content-header">
    <h1>
        {{__('messages.delivery_save_header')}}
        <small></small>
    </h1>
    <ol class="breadcrumb">
        <li>
            <a href="{{action('DeliveryController@index')}}">
                {{__('messages.delivery_header')}}
            </a>
        </li>
        <li>
            <a href="{{Request::fullUrl()}}">
                {{__('messages.delivery_save_header')}}
            </a>
        </li>
    </ol>
</section>

<section class="content">
    <!-- Put your code here -->
    <div id="root"></div>
</section>
@endsection
@section('javascript')
<script>
    var baseUrl        = "{{action('DeliveryController@index')}}";
    var saveUrl        = "{{action('DeliveryController@save')}}";
    var checkEditUrl   = "{{action('DashboardController@checkItemEdit')}}";
    var formDataUrl    = "{{action('DeliveryController@getFormData')}}";
    var checkUrl       = "{{action('DeliveryController@checkValidate')}}";
    var previous       = "{!! URL::previous() !!}";
    $('#delivery-menu').addClass('active');
</script>
@endsection