@extends('layouts.main')
@section('titlePage', __('messages.return_management_save_header'))
@section('content')
<section class="content-header">
    <h1>
        {{__('messages.return_address_header_save')}}
        <small></small>
    </h1>
    <ol class="breadcrumb">
        <li>
            <a href="{{action('ReturnAddressController@index')}}">
                <i class="fa fa-dashboard"></i> {{__('messages.return_address_breadcrumb_title')}}
            </a>
        </li>
    </ol>
</section>

<section class="content">
    <!-- Put your code here -->
    <div id="root"></div>
</section>
@endsection
@section('javascript')
<script>
    var baseUrl        = "{{action('ReturnAddressController@index')}}";
    var saveUrl        = "{{action('ReturnAddressController@save')}}";
    var getFormDataUrl = "{{action('ReturnAddressController@getFormDataAddress')}}";
    var getDataSupplierUrl = "{{action('ReturnAddressController@getDataSupplier')}}";
    var checkEditUrl   = "{{action('DashboardController@checkItemEdit')}}";
    $('#return-address-menu').addClass('active');
    $('#system-setting-menu').addClass('active');
    $(document).ready(function() {
        var width = $(document).width();
        if (width <= 992) {
            $('#message-pro-name').insertAfter('.input-pro-name');
        }
    });
</script>
@endsection