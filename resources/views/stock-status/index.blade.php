@extends('layouts.main')
@section('titlePage', __('messages.stock_status_header'))
@section('content')
<section class="content-header">
    <h1>
        {{__('messages.stock_status_header')}}
    </h1>
    <ol class="breadcrumb">
        <li>
            <a href="{{action('StockStatusController@index')}}">
                {{__('messages.stock_status_header')}}
            </a>
        </li>
    </ol>
</section>

<section class="content">
    <!-- Put your code here -->
    <div id="root"></div>
</section>
@endsection
@section('javascript')
<script>
    var baseUrl     = "{{action('StockStatusController@index')}}";
    var dataListUrl = "{{action('StockStatusController@dataList')}}";
    $('#stock-status-management-menu').addClass('active');
</script>
@endsection