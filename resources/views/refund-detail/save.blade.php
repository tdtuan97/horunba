@extends('layouts.main')
@section('titlePage', __('messages.refund_detail_save_header'))
@section('content')
<section class="content-header">
    <h1>
        {{__('messages.refund_detail_header')}}
        <small></small>
    </h1>
    <ol class="breadcrumb">
        <li>
            <a href="/">
                {{__('messages.refund_detail_header')}}
            </a>
        </li>
    </ol>
</section>

<section class="content">
    <!-- Put your code here -->
    <div id="root"></div>
</section>
@endsection
@section('javascript')
<script>
    var formDataUrl     = "{{action('RefundDetailController@getFormData')}}";
    var baseUrl         = "{{action('RefundDetailController@save')}}";
    var getInfoOrderUrl = "{{action('RefundDetailController@getInfoOrderData')}}";
    var dashboardUrl    = "{{action('DashboardController@index')}}";
    var detailUrl      = "{{action('OrderManagementController@detail')}}";
    $('#refund-menu').addClass('active');
</script>
@endsection