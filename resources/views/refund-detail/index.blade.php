@extends('layouts.main')
@section('titlePage', __('messages.postal_header'))
@section('content')
<section class="content-header">
    <h1>
        {{__('messages.refund_detail_header')}}
        <small></small>
    </h1>
    <ol class="breadcrumb">
        <li>
            <a href="{{action('DashboardController@index')}}">
                <i class="fa fa-dashboard"></i> {{__('messages.dashboard_title')}}
            </a>
        </li>
        <li>
            <a href="{{action('RefundDetailController@save')}}">{{__('messages.refund_detail_breadcrumb_title')}}</a>
        </li>
        <li class="active">{{__('messages.save_action')}}</li>
    </ol>    
</section>

<section class="content">
    <!-- Put your code here -->
    <div id="root"></div>
</section>
@endsection
@section('javascript')
<script type="text/javascript">
    var baseUrl             = "{{action('RePaymentController@index')}}";
    var dashboardUrl        = "{{action('DashboardController@index')}}";    
    var saveUrl             = "{{action('RePaymentController@save')}}";
    var dataListUrl         = "{{action('RePaymentController@dataList')}}";
    $('#payment-menu').addClass('active');
    $('#repayment-list-menu').addClass('active');
</script>
@endsection